package au.com.target.tgtutility.registry;

import de.hybris.platform.core.ItemDeployment;
import de.hybris.platform.core.Registry;

public class RegistryFacadeImpl implements RegistryFacade {
  
  @Override
  public void setCurrentTenantByID(String tenantID) {
    Registry.setCurrentTenantByID(tenantID);
  }
  
  @Override
  public ItemDeployment getItemDeploymentTypeByTypeCode(int typeCode) {
    return Registry.getPersistenceManager().getItemDeployment(typeCode);
  }
  
  @Override
  public ItemDeployment getItemDeploymentTypeByTypeCode(String typeCode) {
    return Registry.getPersistenceManager().getItemDeployment(typeCode);
  }
}
