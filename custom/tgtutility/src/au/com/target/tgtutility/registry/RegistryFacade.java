package au.com.target.tgtutility.registry;

import de.hybris.platform.core.ItemDeployment;

/**
 * Call Registry methods
 */
public interface RegistryFacade {
  
  /**
   * Sets Tenant
   * @param tenantID
   */
  void setCurrentTenantByID(String tenantID);
  
  /**
   * Get ItemDeploymentType
   * @param typeCode
   * @return ItemDeploymentType
   */
  ItemDeployment getItemDeploymentTypeByTypeCode(int typeCode);
  
  /**
   * Get ItemDeploymentType
   * @param typeCode
   * @return ItemDeploymentType
   */
  ItemDeployment getItemDeploymentTypeByTypeCode(String typeCode);
  
}
