/**
 * 
 */
package au.com.target.tgtutility.http.factory;

import javax.net.ssl.SSLSocketFactory;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.DefaultHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 * @author rmcalave
 * 
 */
public class HttpClientFactory {

    private HttpHost hostProxy;
    private String proxyUrl;
    private String proxyPort;

    private String[] protocols;
    private String[] cipherSuites;

    /**
     * Create a new {@link HttpClient}
     * 
     * @return a new {@link HttpClient}
     */

    public HttpClient createHttpClient() {
        return createHttpClientBuilder().build();
    }


    /**
     * Sets proxy only for dev
     * 
     * @return a new {@link HttpClient}
     */
    public HttpClient createHttpClientWithProxy() {
        return createHttpClientBuilderWithProxy().build();
    }

    public HttpClientBuilder createHttpClientBuilderWithProxy() {
        hostProxy = getProxyHost();
        if (null != hostProxy) {
            return createHttpClientBuilder().setProxy(hostProxy);
        }
        else {
            return createHttpClientBuilder();
        }
    }

    private HttpClientBuilder createHttpClientBuilder() {
        SSLConnectionSocketFactory socketFactory = null;
        if (ArrayUtils.isNotEmpty(protocols) && ArrayUtils.isNotEmpty(cipherSuites)) {
            socketFactory = new SSLConnectionSocketFactory(
                    (SSLSocketFactory)SSLSocketFactory.getDefault(),
                    protocols, cipherSuites,
                    new DefaultHostnameVerifier());
        }

        final HttpClientBuilder builder = HttpClientBuilder.create();
        if (socketFactory != null) {
            builder.setSSLSocketFactory(socketFactory);
        }

        return builder;
    }

    /**
     * @param hostProxy
     *            the hostProxy to set
     */
    public void setHostProxy(final HttpHost hostProxy) {
        this.hostProxy = hostProxy;
    }


    /**
     * Returns the Proxy
     * 
     * @return HttpHost
     */
    public HttpHost getProxyHost() {
        if (StringUtils.isNotEmpty(proxyUrl) && StringUtils.isNotEmpty(proxyPort)) {
            hostProxy = new HttpHost(proxyUrl, Integer.valueOf(proxyPort).intValue());
            return hostProxy;
        }
        return null;

    }

    /**
     * @param proxyUrl
     *            the proxyUrl to set
     */
    public void setProxyUrl(final String proxyUrl) {
        this.proxyUrl = proxyUrl;
    }

    /**
     * @param proxyPort
     *            the proxyPort to set
     */
    public void setProxyPort(final String proxyPort) {
        this.proxyPort = proxyPort;
    }

    /**
     * @param protocols
     *            the protocols to set
     */
    public void setProtocols(final String[] protocols) {
        this.protocols = protocols;
    }

    /**
     * @param cipherSuites
     *            the cipherSuites to set
     */
    public void setCipherSuites(final String[] cipherSuites) {
        this.cipherSuites = cipherSuites;
    }

}
