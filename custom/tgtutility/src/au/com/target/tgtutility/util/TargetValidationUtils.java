package au.com.target.tgtutility.util;

import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;



/**
 * 
 */
public final class TargetValidationUtils {

    /**
     * utility class
     */
    private TargetValidationUtils() {
        //do nothing
    }

    /**
     * 
     * @param value
     *            string for validation
     * @param pattern
     *            pattern for checking
     * @param min
     *            minimum size
     * @param max
     *            max size
     * @param isOptional
     *            optional value
     * @return boolean
     */
    public static boolean matchRegularExpression(final String value, final Pattern pattern, final int min,
            final int max, final boolean isOptional) {

        if (StringUtils.isEmpty(value)) {
            return isOptional;
        }
        final int length = StringUtils.length(value);
        return (length >= min) && (length <= max) && (pattern.matcher(value).matches());
    }

    /**
     * 
     * @param value
     *            value to validate
     * @param optional
     *            optional
     * @return boolean
     */
    public static boolean checkOptional(final String value, final boolean optional) {
        if (StringUtils.isEmpty(value)) {
            return optional;
        }
        return true;
    }

}
