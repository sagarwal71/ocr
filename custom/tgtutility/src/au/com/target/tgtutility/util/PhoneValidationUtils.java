package au.com.target.tgtutility.util;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtutility.util.TargetValidationCommon.Phone;


/**
 * The Class PhoneValidationUtils.
 */
public class PhoneValidationUtils
{
    private static final String DOMESTIC_PHONE_PREFIX = "+61";

    private PhoneValidationUtils() {
        //private constructor.
    }

    /**
     * Validate phone number.
     * 
     * @param phoneNumber
     *            the phone number
     * @return true, if successful
     */
    public static boolean validatePhoneNumber(final String phoneNumber)
    {
        final String phone = preparePhoneNumber(phoneNumber);
        if ((StringUtils.isNotBlank(phone)) && (phone.length() >= Phone.MIN_SIZE) &&
                (phone.length() <= Phone.MAX_SIZE)) {
            return true;
        }
        return false;
    }

    /**
     * Translates given phone numbers with next rules: <br>
     * - Remove extra characters (spaces, hyphens, brackets etc.) <br>
     * - If phone number begins in "0", drop "0" and replace with "+61" (assumed domestic) <br>
     * - If number begins in "+" retain "+" and the numbers that follow (assume international)
     * 
     * 
     * @param phone
     *            given phone to translate
     * 
     * @return translated phone or given phone if null or empty
     */
    public static String preparePhoneNumber(final String phone) {
        if (StringUtils.isBlank(phone)) {
            return phone;
        }
        // Get rid of anything that isn't a digit or a '+'
        String replaced = phone.replaceAll("[^\\d+]", "");

        if (replaced.isEmpty()) {
            return replaced;
        }

        // If the first character is a '+' retain it, but get rid of all other non-digit characters
        // (which by now should only be '+').
        replaced = (replaced.charAt(0) == '+') ? "+" + replaced.replaceAll("\\+", "") : replaced.replaceAll(
                "\\+", "");

        // If the first character is 0 replace it with the domestic phone prefix...
        if (replaced.charAt(0) == '0') {
            return replaced.replaceFirst("0", DOMESTIC_PHONE_PREFIX);
        }
        // ... otherwise if there's a 0 immediately after the domestic phone prefix
        // that's already there then get rid of it.
        else if (StringUtils.startsWith(replaced, DOMESTIC_PHONE_PREFIX + '0')) {
            return replaced.replaceFirst("0", "");
        }

        return replaced;
    }
}