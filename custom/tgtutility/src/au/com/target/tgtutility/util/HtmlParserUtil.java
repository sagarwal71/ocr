/**
 * 
 */
package au.com.target.tgtutility.util;

import java.util.HashMap;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


/**
 * @author mjanarth
 *
 */
public final class HtmlParserUtil {

    private static final String INPUT_TAG = "input";
    private static final String NAME = "name";
    private static final String VALUE = "value";



    private HtmlParserUtil() {
        //avoid instantiation
    }

    /**
     * Parse the html string and populate the attributes with input tag
     * 
     * @param html
     * @return HashMap
     */
    public static HashMap<String, String> parseHtmlFormInputParameters(
            final String html) {
        HashMap<String, String> parsedHtmlKeyValueMap = null;
        if (StringUtils.isNotEmpty(html)) {
            parsedHtmlKeyValueMap = new HashMap<String, String>();
            final Document document = Jsoup.parse(html);
            final Elements elements = document.getElementsByTag(INPUT_TAG);
            for (final Element inputElement : elements) {
                if (StringUtils.isNotEmpty(inputElement.attr(NAME))) {
                    parsedHtmlKeyValueMap.put(inputElement.attr(NAME), inputElement.attr(VALUE));
                }
            }
        }
        return parsedHtmlKeyValueMap;
    }
}
