/**
 * 
 */
package au.com.target.tgtutility.util;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;


/**
 * @author pthoma20
 *
 */
public class ExcludeHttpRequestPathHelper {

    private ExcludeHttpRequestPathHelper() {
        //Prevent instantiation
    }

    /**
     * This method checks whether the path or the forwarded url path is present in the excluded list.
     * 
     * @param request
     * @param excludedPaths
     * @return boolean
     */
    public static boolean isExcluded(final HttpServletRequest request, final List<String> excludedPaths) {

        if (CollectionUtils.isEmpty(excludedPaths)) {
            return false;
        }
        final String requestPath = request.getServletPath();

        // This value is for the tags in pages - their request paths don't match the original request
        // but this value does.
        final String forwardRequestUrl = (String)request.getAttribute("javax.servlet.forward.request_uri");

        for (final String path : excludedPaths) {
            if (StringUtils.startsWith(requestPath, path) || StringUtils.startsWith(forwardRequestUrl, path)) {
                return true;
            }
        }

        return false;
    }

}
