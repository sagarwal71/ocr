package au.com.target.tgtutility.util;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Test;


/**
 * The Class PhoneValidationUtilsTest.
 */
@UnitTest
public class PhoneValidationUtilsTest {

    @Test
    public void testNullPhoneNumber() {
        PhoneValidationUtils.validatePhoneNumber(null);
    }

    @Test
    public void testValidatePhoneNumberMax() {
        final boolean valid = PhoneValidationUtils.validatePhoneNumber("423112344567345678");
        Assert.assertFalse(valid);
    }

    @Test
    public void testValidatePhoneNumberMin() {
        final boolean valid = PhoneValidationUtils.validatePhoneNumber("4234567");
        Assert.assertFalse(valid);
    }

    @Test
    public void testValidatePhoneNumberEmpty() {
        final boolean valid = PhoneValidationUtils.validatePhoneNumber("");
        Assert.assertFalse(valid);
    }

    @Test
    public void testValidatePhoneNumberValid() {
        final boolean valid = PhoneValidationUtils.validatePhoneNumber("412345639");
        Assert.assertTrue(valid);
    }

    @Test
    public void testTranslationRules() {
        Assert.assertEquals("123456789", PhoneValidationUtils.preparePhoneNumber("123456789"));

        Assert.assertEquals("123456789", PhoneValidationUtils.preparePhoneNumber("1234567abcd89"));

        Assert.assertEquals("+123456789", PhoneValidationUtils.preparePhoneNumber("+123456789"));

        Assert.assertEquals("+123456789", PhoneValidationUtils.preparePhoneNumber("+123456abcd789"));

        Assert.assertEquals("+6123456789", PhoneValidationUtils.preparePhoneNumber("023456789"));

        Assert.assertEquals("+6123456789", PhoneValidationUtils.preparePhoneNumber("023456abcd789"));

        Assert.assertEquals("+123456789", PhoneValidationUtils.preparePhoneNumber("  +123456789"));

        Assert.assertEquals("123456789", PhoneValidationUtils.preparePhoneNumber("12345+6789"));

        Assert.assertEquals("+123456789", PhoneValidationUtils.preparePhoneNumber("++123456789"));

        Assert.assertEquals("+61352462000", PhoneValidationUtils.preparePhoneNumber("(03) 5246 2000"));

        Assert.assertEquals("+61812345678", PhoneValidationUtils.preparePhoneNumber("+610812345678"));

        Assert.assertEquals("+61414141414", PhoneValidationUtils.preparePhoneNumber("(+61)414141414"));

        Assert.assertEquals("6105123456781234", PhoneValidationUtils.preparePhoneNumber("6105123456781234+"));

        Assert.assertEquals("+61212345678", PhoneValidationUtils.preparePhoneNumber("+(61)(02) 12345678"));
    }
}
