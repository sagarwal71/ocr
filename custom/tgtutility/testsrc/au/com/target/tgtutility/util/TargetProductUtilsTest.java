/**
 * 
 */
package au.com.target.tgtutility.util;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


@UnitTest
public class TargetProductUtilsTest {


    @Test
    public void testCheckIfStockDataHasStockWithStockLevel0() {
        final StockData stockData = new StockData();
        stockData.setStockLevel(Long.valueOf(0));
        assertThat(TargetProductUtils.checkIfStockDataHasStock(stockData)).isFalse();
    }

    @Test
    public void testCheckIfStockDataHasStockWithStockLevel1() {
        final StockData stockData = new StockData();
        stockData.setStockLevel(Long.valueOf(1));
        assertThat(TargetProductUtils.checkIfStockDataHasStock(stockData)).isTrue();
    }

    @Test
    public void testCheckIfStockDataHasStockWithOutOfStockStatus() {
        final StockData stockData = new StockData();
        stockData.setStockLevelStatus(StockLevelStatus.OUTOFSTOCK);
        assertThat(TargetProductUtils.checkIfStockDataHasStock(stockData)).isFalse();
    }

    @Test
    public void testCheckIfStockDataHasStockWithInStockStatus() {
        final StockData stockData = new StockData();
        stockData.setStockLevelStatus(StockLevelStatus.INSTOCK);
        assertThat(TargetProductUtils.checkIfStockDataHasStock(stockData)).isTrue();
    }

    @Test
    public void testCheckIfStockDataHasStockWithNoStockLevelOrStatus() {
        final StockData stockData = new StockData();
        assertThat(TargetProductUtils.checkIfStockDataHasStock(stockData)).isFalse();
    }

    @Test
    public void testCheckIfStockDataHasStockWithNull() {
        assertThat(TargetProductUtils.checkIfStockDataHasStock(null)).isFalse();
    }

    @Test
    public void testCheckIfStockDataExistsWithStockLevel0() {
        final StockData stockData = new StockData();
        stockData.setStockLevel(Long.valueOf(0));
        assertThat(TargetProductUtils.checkIfStockDataExists(stockData)).isTrue();
    }

    @Test
    public void testCheckIfStockDataExistsWithStockLevel1() {
        final StockData stockData = new StockData();
        stockData.setStockLevel(Long.valueOf(1));
        assertThat(TargetProductUtils.checkIfStockDataExists(stockData)).isTrue();
    }

    @Test
    public void testCheckIfStockDataExistsWithStockLevelNull() {
        final StockData stockData = new StockData();
        stockData.setStockLevel(null);
        assertThat(TargetProductUtils.checkIfStockDataExists(stockData)).isFalse();
    }

    @Test
    public void testProductOpenForPreOrderSuccess() {
        final TargetColourVariantProductModel product = new TargetColourVariantProductModel();

        final Calendar start = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), Calendar.FEBRUARY, 15, 0, 0, 0);

        final Calendar end = Calendar.getInstance();
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 2, 22, 0, 0, 0);

        product.setPreOrderStartDateTime(start.getTime());
        product.setPreOrderEndDateTime(end.getTime());
        assertThat(TargetProductUtils.isProductOpenForPreOrder(product)).isTrue();
    }

    @Test
    public void testProductOpenForPreOrderEndDateBeforeToday() {
        final TargetColourVariantProductModel product = new TargetColourVariantProductModel();

        final Calendar start = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), Calendar.FEBRUARY, 15, 0, 0, 0);

        // end date in past
        final Calendar end = Calendar.getInstance();
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) - 1, 13, 0, 0, 0);

        product.setPreOrderStartDateTime(start.getTime());
        product.setPreOrderEndDateTime(end.getTime());
        assertThat(TargetProductUtils.isProductOpenForPreOrder(product)).isFalse();
    }

    @Test
    public void testProductOpenForPreOrderStartDateAfterToday() {
        final TargetColourVariantProductModel product = new TargetColourVariantProductModel();

        // start date in future
        final Calendar start = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), start.get(Calendar.MONTH) + 2, 15, 0, 0, 0);

        final Calendar end = Calendar.getInstance();
        end.set(end.get(Calendar.YEAR), Calendar.APRIL, 22, 0, 0, 0);

        product.setPreOrderStartDateTime(start.getTime());
        product.setPreOrderEndDateTime(end.getTime());
        assertThat(TargetProductUtils.isProductOpenForPreOrder(product)).isFalse();
    }

    @Test
    public void testProductOpenForPreOrderNullDates() {
        final TargetColourVariantProductModel product = new TargetColourVariantProductModel();

        assertThat(TargetProductUtils.isProductOpenForPreOrder(product)).isFalse();
    }

    @Test
    public void testProductOpenForPreOrderNullColourVariant() {
        final AbstractTargetVariantProductModel product = new AbstractTargetVariantProductModel();

        assertThat(TargetProductUtils.isProductOpenForPreOrder(product)).isFalse();
    }

    @Test
    public void testIsProductEmbargoedWithNoColourVariant() {
        final AbstractTargetVariantProductModel product = new AbstractTargetVariantProductModel();

        assertThat(TargetProductUtils.isProductEmbargoed(product)).isFalse();
    }

    @Test
    public void testIsEmbargoedProductEmbargoDateInFuture() {

        final Calendar embargoDate = Calendar.getInstance();
        // future date
        embargoDate.set(embargoDate.get(Calendar.YEAR), embargoDate.get(Calendar.MONTH) + 2, 22, 0, 0, 0);

        final TargetColourVariantProductModel colourVariant = new TargetColourVariantProductModel();
        colourVariant.setNormalSaleStartDateTime(embargoDate.getTime());

        assertThat(TargetProductUtils.isProductEmbargoed(colourVariant)).isTrue();
    }

    @Test
    public void testIsEmbargoedProductEmbargoDateInPast() {

        final Calendar embargoDate = Calendar.getInstance();
        // future date
        embargoDate.set(embargoDate.get(Calendar.YEAR), embargoDate.get(Calendar.MONTH) - 2, 22, 0, 0, 0);

        final TargetColourVariantProductModel colourVariant = new TargetColourVariantProductModel();
        colourVariant.setNormalSaleStartDateTime(embargoDate.getTime());

        assertThat(TargetProductUtils.isProductEmbargoed(colourVariant)).isFalse();
    }

    @Test
    public void testIsEmbargoedProductEmbargoDateNull() {

        final TargetColourVariantProductModel colourVariant = new TargetColourVariantProductModel();

        assertThat(TargetProductUtils.isProductEmbargoed(colourVariant)).isFalse();
    }

    @Test
    public void testIsEmbargoedProductVariantNull() {

        assertThat(TargetProductUtils.isProductEmbargoed(null)).isFalse();
    }

    @Test
    public void testGetAllSellableVariantsSizes() {

        final ProductModel product = mock(ProductModel.class);
        final VariantProductModel colourVariant1 = mock(TargetColourVariantProductModel.class);
        final VariantProductModel colourVariant2 = mock(TargetColourVariantProductModel.class);

        final TargetSizeVariantProductModel sizeVariant1 = mock(TargetSizeVariantProductModel.class);
        final TargetSizeVariantProductModel sizeVariant2 = mock(TargetSizeVariantProductModel.class);
        final TargetSizeVariantProductModel sizeVariant3 = mock(TargetSizeVariantProductModel.class);
        final TargetSizeVariantProductModel sizeVariant4 = mock(TargetSizeVariantProductModel.class);

        final Collection<VariantProductModel> colours = new ArrayList();
        final Collection<VariantProductModel> sizes1 = new ArrayList();
        final Collection<VariantProductModel> sizes2 = new ArrayList();
        final Collection<VariantProductModel> expectedSizes = new ArrayList();

        sizes1.add(sizeVariant1);
        sizes1.add(sizeVariant2);
        sizes2.add(sizeVariant3);
        sizes2.add(sizeVariant4);

        given(colourVariant1.getVariants()).willReturn(sizes1);
        given(colourVariant2.getVariants()).willReturn(sizes2);

        colours.add(colourVariant1);
        colours.add(colourVariant2);

        given(product.getVariants()).willReturn(colours);

        expectedSizes.add(sizeVariant1);
        expectedSizes.add(sizeVariant2);
        expectedSizes.add(sizeVariant3);
        expectedSizes.add(sizeVariant4);

        assertThat(TargetProductUtils.getAllSellableVariants(product)).isEqualTo((List<?>)expectedSizes);
    }

    @Test
    public void testGetAllSellableVariantsColours() {

        final ProductModel product = mock(ProductModel.class);
        final VariantProductModel colourVariant1 = mock(TargetColourVariantProductModel.class);
        final VariantProductModel colourVariant2 = mock(TargetColourVariantProductModel.class);
        final Collection<VariantProductModel> expectedColours = new ArrayList();
        expectedColours.add(colourVariant1);
        expectedColours.add(colourVariant2);

        given(product.getVariants()).willReturn(expectedColours);

        assertThat(TargetProductUtils.getAllSellableVariants(product)).isEqualTo((List<?>)expectedColours);
    }

    @Test
    public void testGetAllSellableVariantCodesSizes() {

        final ProductModel product = mock(ProductModel.class);

        final VariantProductModel colourVariant1 = mock(TargetColourVariantProductModel.class);
        final VariantProductModel colourVariant2 = mock(TargetColourVariantProductModel.class);

        final TargetSizeVariantProductModel sizeVariant1 = mock(TargetSizeVariantProductModel.class);
        final TargetSizeVariantProductModel sizeVariant2 = mock(TargetSizeVariantProductModel.class);
        final TargetSizeVariantProductModel sizeVariant3 = mock(TargetSizeVariantProductModel.class);
        final TargetSizeVariantProductModel sizeVariant4 = mock(TargetSizeVariantProductModel.class);

        given(sizeVariant1.getCode()).willReturn("P1000_black_S");
        given(sizeVariant2.getCode()).willReturn("P1000_black_L");
        given(sizeVariant3.getCode()).willReturn("P1000_red_S");
        given(sizeVariant4.getCode()).willReturn("P1000_red_L");

        final Collection<VariantProductModel> colours = new ArrayList();
        final Collection<VariantProductModel> sizes1 = new ArrayList();
        final Collection<VariantProductModel> sizes2 = new ArrayList();
        final Collection<String> expectedSizeCodes = new ArrayList();

        sizes1.add(sizeVariant1);
        sizes1.add(sizeVariant2);
        sizes2.add(sizeVariant3);
        sizes2.add(sizeVariant4);

        given(colourVariant1.getVariants()).willReturn(sizes1);
        given(colourVariant2.getVariants()).willReturn(sizes2);

        colours.add(colourVariant1);
        colours.add(colourVariant2);

        given(product.getVariants()).willReturn(colours);

        expectedSizeCodes.add("P1000_black_S");
        expectedSizeCodes.add("P1000_black_L");
        expectedSizeCodes.add("P1000_red_S");
        expectedSizeCodes.add("P1000_red_L");

        assertThat(TargetProductUtils.getAllSellableVariantCodes(product)).isEqualTo(expectedSizeCodes);
    }

    @Test
    public void testGetAllSellableVariantCodesColours() {
        final ProductModel product = mock(ProductModel.class);

        final VariantProductModel colourVariant1 = mock(TargetColourVariantProductModel.class);
        final VariantProductModel colourVariant2 = mock(TargetColourVariantProductModel.class);

        given(colourVariant1.getCode()).willReturn("P1000_black");
        given(colourVariant2.getCode()).willReturn("P1000_red");


        final Collection<VariantProductModel> expectedColours = new ArrayList();
        final Collection<String> expectedColourCodes = new ArrayList();
        expectedColours.add(colourVariant1);
        expectedColours.add(colourVariant2);

        given(product.getVariants()).willReturn(expectedColours);

        expectedColourCodes.add("P1000_black");
        expectedColourCodes.add("P1000_red");

        assertThat(TargetProductUtils.getAllSellableVariantCodes(product)).isEqualTo(expectedColourCodes);
    }


    /**
     * Method to verify the normal sale start date for colour variant product.
     */
    @Test
    public void testGetNormalSaleStartDateTimeWithDateInColourVariant() {

        final TargetColourVariantProductModel targetColourVariantProductModel = mock(
                TargetColourVariantProductModel.class);
        final Calendar embargoDate = Calendar.getInstance();
        embargoDate.set(embargoDate.get(Calendar.YEAR), embargoDate.get(Calendar.MONTH) + 4, 25, 0, 0, 0);
        given(targetColourVariantProductModel.getNormalSaleStartDateTime()).willReturn(embargoDate.getTime());
        final Date normalSalesStartDateTime = TargetProductUtils
                .getNormalSaleStartDateTime(targetColourVariantProductModel);
        assertThat(normalSalesStartDateTime).isEqualTo(embargoDate.getTime());

    }


    /**
     * Method to verify the normal sale start date(none) for colour variant product.
     */
    @Test
    public void testGetNormalSaleStartDateTimeWithoutDateInColourVariant() {

        final TargetColourVariantProductModel targetColourVariantProductModel = mock(
                TargetColourVariantProductModel.class);
        final Date normalSalesStartDateTime = TargetProductUtils
                .getNormalSaleStartDateTime(targetColourVariantProductModel);
        assertThat(normalSalesStartDateTime).isNull();
    }

    /**
     * Method to verify the normal sale start date for size variant product.
     */
    @Test
    public void testGetNormalSaleStartDateTimeWithDateInSizeVariant() {
        final TargetColourVariantProductModel targetColourVariantProductModel = mock(
                TargetColourVariantProductModel.class);

        final TargetSizeVariantProductModel targetSizeVariantProductModel = mock(
                TargetSizeVariantProductModel.class);
        final Calendar embargoDate = Calendar.getInstance();
        embargoDate.set(embargoDate.get(Calendar.YEAR), embargoDate.get(Calendar.MONTH) + 4, 25, 0, 0, 0);
        given(targetColourVariantProductModel.getNormalSaleStartDateTime()).willReturn(embargoDate.getTime());
        given(targetSizeVariantProductModel.getBaseProduct()).willReturn(targetColourVariantProductModel);
        final Date normalSalesStartDateTime = TargetProductUtils
                .getNormalSaleStartDateTime(targetSizeVariantProductModel);
        assertThat(normalSalesStartDateTime).isEqualTo(embargoDate.getTime());

    }

    /**
     * Method to verify to the normal sale start date(none) for size variant product.
     */
    @Test
    public void testGetNormalSaleStartDateTimeWithoutDateInSizeVariant() {
        final TargetColourVariantProductModel targetColourVariantProductModel = mock(
                TargetColourVariantProductModel.class);
        final TargetSizeVariantProductModel targetSizeVariantProductModel = mock(
                TargetSizeVariantProductModel.class);
        given(targetSizeVariantProductModel.getBaseProduct()).willReturn(targetColourVariantProductModel);

        final Date normalSalesStartDateTime = TargetProductUtils
                .getNormalSaleStartDateTime(targetSizeVariantProductModel);
        assertThat(normalSalesStartDateTime).isNull();

    }

    /**
     * Method to verify the pre order end date for colour variant product.
     */
    @Test
    public void testGetPreOrderEndDateColourVariant() {

        final TargetColourVariantProductModel targetColourVariantProductModel = mock(
                TargetColourVariantProductModel.class);
        final Calendar endDate = Calendar.getInstance();
        endDate.set(endDate.get(Calendar.YEAR), endDate.get(Calendar.MONTH) + 4, 25, 0, 0, 0);
        given(targetColourVariantProductModel.getPreOrderEndDateTime()).willReturn(endDate.getTime());
        final Date preOrderEndDate = TargetProductUtils
                .getPreOrderEndDate(targetColourVariantProductModel);
        assertThat(preOrderEndDate).isNotNull();
        assertThat(preOrderEndDate).isEqualTo(endDate.getTime());
    }


    /**
     * Method to verify the pre order end date for colour variant product.
     */
    @Test
    public void testGetPreOrderEndDateColourVariantNullDate() {

        final TargetColourVariantProductModel targetColourVariantProductModel = mock(
                TargetColourVariantProductModel.class);
        given(targetColourVariantProductModel.getPreOrderEndDateTime()).willReturn(null);
        final Date preOrderEndDate = TargetProductUtils
                .getPreOrderEndDate(targetColourVariantProductModel);
        assertThat(preOrderEndDate).isNull();
    }

    /**
     * Method to verify the pre order end date for size variant product.
     */
    @Test
    public void testGetPreOrderEndDateSizeVariant() {
        final TargetColourVariantProductModel targetColourVariantProductModel = mock(
                TargetColourVariantProductModel.class);

        final TargetSizeVariantProductModel targetSizeVariantProductModel = mock(
                TargetSizeVariantProductModel.class);
        final Calendar endDate = Calendar.getInstance();
        endDate.set(endDate.get(Calendar.YEAR), endDate.get(Calendar.MONTH) + 4, 25, 0, 0, 0);
        given(targetColourVariantProductModel.getPreOrderEndDateTime()).willReturn(endDate.getTime());
        given(targetSizeVariantProductModel.getBaseProduct()).willReturn(targetColourVariantProductModel);
        final Date preOrderEndDate = TargetProductUtils
                .getPreOrderEndDate(targetSizeVariantProductModel);
        assertThat(preOrderEndDate).isNotNull();
        assertThat(preOrderEndDate).isEqualTo(endDate.getTime());

    }


}
