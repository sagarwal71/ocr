/**
 * 
 */
package au.com.target.tgtutility.util;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;


/**
 * @author mjanarth
 *
 */
@UnitTest
public class HtmlParserUtilTest {


    @Test
    public void testParseHtmlFormParametersWithNull() {
        final String html = null;
        final HashMap<String, String> output = HtmlParserUtil.parseHtmlFormInputParameters(html);
        Assert.assertNull(output);
    }

    @Test
    public void testParseHtmlFormParametersWithEmptyString() {
        final String html = "";
        final HashMap<String, String> output = HtmlParserUtil.parseHtmlFormInputParameters(html);
        Assert.assertNull(output);
    }

    @Test
    public void testParseHtmlFormParametersWithHtmlString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<html><body><form>");
        stringBuilder.append("<input type=\"hidden\" name=\"SessionStored\" value=\"False\" />");
        stringBuilder.append("</form></body></html>");
        final HashMap<String, String> output = HtmlParserUtil.parseHtmlFormInputParameters(stringBuilder.toString());
        Assert.assertNotNull(output);
        Assert.assertEquals(output.size(), 1);
        Assert.assertEquals(output.get("SessionStored"), "False");
    }

    @Test
    public void testParseHtmlFormParametersWithValidHtml() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<html><body><form>");
        stringBuilder.append("<input type=\"hidden\" name=\"SessionStored\" value=\"True\" />");
        stringBuilder.append("<input type=\"hidden\" name=\"SST\" value=\"RSG7282-sjdkjd-17832-NSHS\" />");
        stringBuilder.append("</form></body></html>");
        final HashMap<String, String> output = HtmlParserUtil.parseHtmlFormInputParameters(stringBuilder.toString());
        Assert.assertNotNull(output);
        Assert.assertEquals(output.size(), 2);
        Assert.assertEquals(output.get("SessionStored"), "True");
        Assert.assertEquals(output.get("SST"), "RSG7282-sjdkjd-17832-NSHS");
    }

    @Test
    public void testParseHtmlFormParametersWithNoInputTag() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<html><body><form>");
        stringBuilder.append("</form></body></html>");
        final HashMap<String, String> output = HtmlParserUtil.parseHtmlFormInputParameters(stringBuilder.toString());
        Assert.assertNotNull(output);
        Assert.assertEquals(output.size(), 0);
    }

    @Test
    public void testParseHtmlFormParametersWithClassAttr() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<html><body><form>");
        stringBuilder.append("<input type=\"hidden\" class=\"SessionStored\" value=\"False\" />");
        stringBuilder.append("</form></body></html>");
        final HashMap<String, String> output = HtmlParserUtil.parseHtmlFormInputParameters(stringBuilder.toString());
        Assert.assertNotNull(output);
        Assert.assertEquals(output.size(), 0);
    }
}
