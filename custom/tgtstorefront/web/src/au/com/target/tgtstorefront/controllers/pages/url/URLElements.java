/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.url;

/**
 * @author quan.le
 * 
 */
public class URLElements {
    private String viewAs;
    private String sortCode;
    private String itemsPerPage;

    /**
     * @param viewAs
     * @param sortCode
     * @param itemsPerPage
     */
    public URLElements(final String viewAs, final String sortCode, final String itemsPerPage) {
        super();
        this.viewAs = viewAs;
        this.sortCode = sortCode;
        this.itemsPerPage = itemsPerPage;
    }

    /**
     * 
     */
    public URLElements() {
        //Default Constructor
    }

    public String getViewAs() {
        return viewAs;
    }

    public void setViewAs(final String viewAs) {
        this.viewAs = viewAs;
    }

    public String getSortCode() {
        return sortCode;
    }

    public void setSortCode(final String sortCode) {
        this.sortCode = sortCode;
    }

    public String getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(final String itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }
}
