/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtwebcore.model.cms2.components.TargetCustomerSubscriptionComponentModel;


/**
 * @author bhuang3
 *
 */
@Controller("TargetCustomerSubscriptionComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.TARGET_CUSTOMER_SUBSCRIPTION_COMPONENT)
public class TargetCustomerSubscriptionComponentController extends
        AbstractCMSComponentController<TargetCustomerSubscriptionComponentModel> {


    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
            final TargetCustomerSubscriptionComponentModel component) {
        model.addAttribute("title", component.getTitle());
        if (component.getCustomerSubscriptionTypeEnum() != null) {
            model.addAttribute("customerSubscriptionType", component.getCustomerSubscriptionTypeEnum().getCode());
        }
    }

}
