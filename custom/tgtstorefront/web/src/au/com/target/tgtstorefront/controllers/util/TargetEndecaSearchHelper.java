/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.ui.Model;

import au.com.target.endeca.infront.assemble.impl.EndecaPageAssemble;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.converter.EndecaToFacetsConverter;
import au.com.target.endeca.infront.converter.EndecaToSortConverter;
import au.com.target.endeca.infront.data.EndecaDimension;
import au.com.target.endeca.infront.data.EndecaProduct;
import au.com.target.endeca.infront.data.EndecaRecords;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaWrapperException;
import au.com.target.endeca.infront.transform.TargetEndecaURLHelper;
import au.com.target.tgtfacades.category.data.TargetCategoryData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.search.data.TargetProductCategorySearchPageData;
import au.com.target.tgtfacades.sort.TargetSortData;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.controllers.pages.data.SeachEvaluatorData;
import au.com.target.tgtstorefront.controllers.pages.data.ViewPreferencesInitialData;
import au.com.target.tgtstorefront.controllers.pages.preferences.StoreViewPreferencesHandler;
import au.com.target.tgtstorefront.util.CommonUtils;
import au.com.target.tgtstorefront.util.SearchQuerySanitiser;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * @author bhuang3
 *
 */
@SuppressWarnings("deprecation")
public class TargetEndecaSearchHelper {

    private static final Logger LOG = Logger.getLogger(TargetEndecaSearchHelper.class);

    private static final Integer DEFAULT_MAX_SEARCH_TEXT_LENGTH = Integer.valueOf(50);

    private Integer maxSearchTextLength;

    private EndecaPageAssemble endecaPageAssemble;

    private EndecaToFacetsConverter endecaToFacetsConverter;

    private Converter<EndecaProduct, TargetProductListerData> targetProductListerConverter;

    private TargetEndecaURLHelper targetEndecaURLHelper;

    private EndecaToSortConverter endecaToSortConverter;

    private StoreViewPreferencesHandler storeViewPreferencesHandler;

    private ConfigurationService configurationService;

    private SearchQuerySanitiser searchQuerySanitiser;

    /**
     * Sanitize the search text
     * 
     * @param searchText
     * @return sanitized text
     */
    public String sanitizeSearchText(final String searchText, final Model model) {
        String safeSearchText = searchText;
        if (safeSearchText.length() > getMaxSearchTextLength().intValue()) {
            safeSearchText = safeSearchText.substring(0, getMaxSearchTextLength().intValue());
            if (null != model) {
                GlobalMessages.addErrorMessage(model, "search.text.size", new Object[] { getMaxSearchTextLength() });
            }
        }
        return searchQuerySanitiser.sanitiseSearchText(safeSearchText);
    }

    /**
     * handle preferences initial data
     * 
     * @param request
     * @param response
     * @param categoryCode
     * @param navigationState
     * @param sortCode
     * @param viewAs
     * @param pageSize
     * @return SeachEvaluatorData
     */
    public SeachEvaluatorData handlePreferencesInitialData(final HttpServletRequest request,
            final HttpServletResponse response, final String categoryCode,
            final String navigationState,
            final String sortCode,
            final String viewAs,
            final String pageSize, final String searchText) {
        final SeachEvaluatorData data = new SeachEvaluatorData();
        final int pageSizeInt = NumberUtils.toInt(pageSize, 0);
        final ViewPreferencesInitialData viewData = new ViewPreferencesInitialData(request, categoryCode,
                navigationState);
        final String viewAsFn = storeViewPreferencesHandler.getViewAsPreference(response, viewData, viewAs);
        final int itemPerPageFn = storeViewPreferencesHandler.getItemPerPagePreference(response, viewData, pageSizeInt);
        String sortCodeFn = sortCode;
        if (StringUtils.isEmpty(searchText)) {
            sortCodeFn = storeViewPreferencesHandler.getSortCodePreference(response, viewData, sortCode);
        }
        data.setItemPerPageFn(itemPerPageFn);
        data.setSortCodeFn(sortCodeFn);
        data.setViewAsFn(viewAsFn);
        return data;
    }

    /**
     * Get endeca search result
     * 
     * @param request
     * @param endecaSearchStateData
     * @return EndecaSearch
     */
    public EndecaSearch getEndecaSearchResult(final HttpServletRequest request,
            final EndecaSearchStateData endecaSearchStateData) {
        EndecaSearch searchResults = null;
        if (null != endecaSearchStateData) {
            try {
                String pageUrl = StringUtils.EMPTY;
                final Map<String, String> dims = new HashMap<>();
                if (CollectionUtils.isNotEmpty(endecaSearchStateData.getSearchTerm())) {
                    pageUrl = EndecaConstants.EndecaPageUris.ENDECA_SEARCH_PAGE_URI;
                }
                else if (endecaSearchStateData.getBrand() != null) {
                    dims.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND, endecaSearchStateData.getBrand()
                            .getName());
                    pageUrl = EndecaConstants.EndecaPageUris.ENDECA_BRAND_PAGE_URI;
                }
                else if (endecaSearchStateData.getCategoryData() != null) {
                    pageUrl = this.createDimAndPageUrlForCategory(endecaSearchStateData, dims);

                }
                if (StringUtils.isNotEmpty(pageUrl)) {
                    searchResults = endecaPageAssemble.assemble(request, pageUrl, endecaSearchStateData, dims);
                    LOG.info(TgtutilityConstants.InfoCode.REQUEST_URI + " = " + request.getRequestURI()
                            + ", " + TgtutilityConstants.InfoCode.TOTAL_RECORDS_RETURNED + " = "
                            + (searchResults.getRecords() != null ? searchResults.getRecords().getTotalCount() : "0")
                            + "," + TgtutilityConstants.InfoCode.QUERY_STRING + " = " + request.getQueryString());
                }
            }
            catch (final TargetEndecaWrapperException e) {
                LOG.error(SplunkLogFormatter.formatMessage(
                        "Error thrown inside Endeca Assemble",
                        TgtutilityConstants.ErrorCode.ERR_TGTSTORE_TEXT_SEARCH, ""), e);
            }
            catch (final JAXBException e) {
                LOG.error(SplunkLogFormatter.formatMessage(
                        "Occurred when request is marshalled ",
                        TgtutilityConstants.ErrorCode.ERR_TGTSTORE_TEXT_SEARCH, ""), e);
            }
            catch (final TransformerException e) {
                LOG.error(SplunkLogFormatter.formatMessage(
                        "Occurred when transformation occured ",
                        TgtutilityConstants.ErrorCode.ERR_TGTSTORE_TEXT_SEARCH, ""), e);
            }
            catch (final IOException e) {
                LOG.error(SplunkLogFormatter.formatMessage(
                        "Occured when retrieving search results ",
                        TgtutilityConstants.ErrorCode.ERR_TGTSTORE_TEXT_SEARCH, ""), e);
            }
        }
        return searchResults;
    }

    public void populateSearchPageData(
            final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData,
            final EndecaSearch searchResults, final EndecaSearchStateData searchStateData) {
        if (searchResults == null) {
            searchPageData.setErrorStatus(EndecaConstants.EndecaErrorCodes.ENDECA_ERROR_STATUS);
            return;
        }
        searchPageData.setCurrentQuery(searchStateData);

        if (StringUtils.isNotBlank(searchResults.getRedirectLink())) {
            searchPageData.setKeywordRedirectUrl(searchResults.getRedirectLink());
            return;
        }

        final String categoryCode = searchStateData.getCategoryData() != null ? searchStateData.getCategoryData()
                .getCode() : StringUtils.EMPTY;
        //populate facets
        searchPageData.setFacets(endecaToFacetsConverter.convertToFacetData(searchResults, searchStateData,
                categoryCode,
                searchStateData.getBrand()));

        this.populateSearchRecords(searchResults, searchPageData, searchStateData);

    }

    /**
     * Populates the clear all refinements url
     * 
     * @param searchPageData
     * @param searchResults
     * @param breadcrumbs
     */
    public void populateClearAllUrl(
            final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData,
            final EndecaSearch searchResults, final List<Breadcrumb> breadcrumbs) {
        searchPageData.setShowClearAllUrl(hasSelectedMultiSelectRefinement(searchResults,
                (EndecaSearchStateData)searchPageData.getCurrentQuery()));
        final String clearAllUrl = breadcrumbs.get(breadcrumbs.size() - 1).getUrl();
        searchPageData.setClearAllUrl(appendPaginationData(clearAllUrl, searchPageData.getPagination()));
        searchPageData.setClearAllQueryUrl(
                appendPaginationData(getClearAllQueryUrl(clearAllUrl), searchPageData.getPagination()));
    }

    /**
     * Append pagination information to the given url.
     * 
     * @param url
     * @param paginationData
     * @return url with paginationData
     */
    protected String appendPaginationData(final String url, final PaginationData paginationData) {
        if (paginationData == null) {
            return url;
        }
        final StringBuilder modifiedUrl = new StringBuilder(url);
        if (url.indexOf('?') != -1) {
            modifiedUrl.append(EndecaConstants.QUERY_PARAM_SEPERATOR_AMP);
        }
        else {
            modifiedUrl.append(EndecaConstants.QUERY_PARAM_SEPERATOR_QNMRK);
        }
        modifiedUrl.append(EndecaConstants.PAGE_SIZE_KEY + "=" + paginationData.getPageSize());
        return modifiedUrl.toString();
    }

    /**
     * Create the endpoint friendly version of clearAllUrl.
     * 
     * @param clearAllUrl
     * @return clearAllQueryUrl
     */
    protected String getClearAllQueryUrl(final String clearAllUrl) {
        final StringBuilder clearAllQueryUrl = new StringBuilder();
        if (clearAllUrl.startsWith("/c/")) {
            clearAllQueryUrl.append(EndecaConstants.QUERY_PARAM_SEPERATOR_QNMRK)
                    .append(EndecaConstants.WS_URL_CATEGORY)
                    .append(clearAllUrl.substring(clearAllUrl.lastIndexOf('/') + 1));
        }
        else if (clearAllUrl.startsWith("/b/")) {
            clearAllQueryUrl.append(EndecaConstants.QUERY_PARAM_SEPERATOR_QNMRK).append(EndecaConstants.WS_URL_BRAND);
            if (clearAllUrl.indexOf(EndecaConstants.QUERY_PARAM_SEPERATOR_QNMRK) != -1) {
                clearAllQueryUrl.append(clearAllUrl.substring(clearAllUrl.lastIndexOf('/') + 1,
                        clearAllUrl.indexOf(EndecaConstants.QUERY_PARAM_SEPERATOR_QNMRK)))
                        .append(EndecaConstants.QUERY_PARAM_SEPERATOR_AMP).append(clearAllUrl
                                .substring(clearAllUrl.indexOf(EndecaConstants.QUERY_PARAM_SEPERATOR_QNMRK) + 1));
            }
            else {
                clearAllQueryUrl.append(clearAllUrl.substring(clearAllUrl.lastIndexOf('/') + 1));
            }
        }
        else if (clearAllUrl.contains(EndecaConstants.QUERY_PARAM_SEPERATOR_QNMRK)) {
            clearAllQueryUrl.append(EndecaConstants.QUERY_PARAM_SEPERATOR_QNMRK)
                    .append(clearAllUrl.split("\\" + EndecaConstants.QUERY_PARAM_SEPERATOR_QNMRK)[1]);
        }
        return clearAllQueryUrl.toString();
    }

    private void populateSearchRecords(final EndecaSearch searchResults,
            final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData,
            final EndecaSearchStateData searchStateData) {
        final EndecaRecords records = searchResults.getRecords();
        final List<ProductData> productData = new ArrayList<>();
        searchPageData.setResults(productData);
        if (records != null) {
            final List<EndecaProduct> productsList = records.getProduct();
            //populate product list
            if (CollectionUtils.isNotEmpty(productsList)) {
                for (final EndecaProduct endecaProduct : productsList) {
                    final TargetProductListerData targetProduct = targetProductListerConverter.convert(endecaProduct);
                    productData.add(targetProduct);
                }
            }
            if (records.getCurrentNavState() != null) {
                searchStateData.setUrl(targetEndecaURLHelper.constructCurrentPageUrl(
                        searchStateData.getRequestUrl(),
                        records.getCurrentNavState()));
                searchStateData.setWsUrl(targetEndecaURLHelper.constructCurrentWsPageUrl(searchStateData,
                        records.getCurrentNavState()));
            }
            else {
                LOG.error(SplunkLogFormatter.formatMessage(
                        "Missing currentNav state: " + searchStateData.getRequestUrl(),
                        TgtutilityConstants.ErrorCode.ERR_TGTSTORE_CATEGORY_SEARCH));
            }

            //Set the pagination details
            final int totalCount = CommonUtils.convertToPositiveInt(searchResults.getRecords()
                    .getTotalCount());
            searchPageData.setPagination(createPaginationData(searchStateData.getItemsPerPage(),
                    searchStateData.getPageNumber(), totalCount));
            //set the sort data 
            if (null != records.getOption()) {
                final List<TargetSortData> sortData = endecaToSortConverter.covertToTargetSortData(searchResults
                        .getRecords().getOption(), searchStateData);
                searchPageData.setTargetSortData(sortData);
            }

            if (CollectionUtils.isNotEmpty(searchResults.getSpellingSuggestions())) {
                searchPageData.setSpellingSuggestions(searchResults.getSpellingSuggestions());
            }
        }

        if (null != searchResults.getAutoCorrectedTerm()) {
            searchPageData.setAutoCorrectedTerm(searchResults.getAutoCorrectedTerm());
        }

    }

    /**
     * add dims for category and return the pageUrl
     * 
     * @param endecaSearchStateData
     * @param dims
     * @return pageUrl
     */
    private String createDimAndPageUrlForCategory(final EndecaSearchStateData endecaSearchStateData,
            final Map<String, String> dims) {
        final TargetCategoryData cateogryData = endecaSearchStateData.getCategoryData();
        final String pageUrl;
        if (cateogryData.isQualifierDealCategory()) {
            dims.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_QUALIFIER_CATEGORY,
                    cateogryData.getCode());
            pageUrl = EndecaConstants.EndecaPageUris.ENDECA_DEAL_QUALIFIER_CATEGORY_PAGE_URI;
        }
        else if (cateogryData.isRewardDealCategory()) {
            dims.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_REWARD_CATEGORY,
                    cateogryData.getCode());
            pageUrl = EndecaConstants.EndecaPageUris.ENDECA_DEAL_REWARD_CATEGORY_PAGE_URI;
        }

        else {
            dims.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY, cateogryData.getCode());
            pageUrl = EndecaConstants.EndecaPageUris.ENDECA_CATEGORY_PAGE_URI;
        }
        if (null != endecaSearchStateData.getStoreNumber()) {
            dims.put(EndecaConstants.EndecaRecordSpecificFields.BULKYBOARD,
                    endecaSearchStateData.getStoreNumber());
        }
        return pageUrl;
    }

    /**
     * Create pagination Data
     * 
     * @param pageSize
     * @param page
     * @param totalNumberOfResults
     * @return PaginationData
     */
    private PaginationData createPaginationData(final int pageSize, final int page, final int totalNumberOfResults) {
        final PaginationData paginationData = new PaginationData();

        // Set the page size and and don't allow it to be less than 1
        paginationData.setPageSize(pageSize);

        //Set total number for results
        paginationData.setTotalNumberOfResults(totalNumberOfResults);

        // Calculate the number of pages
        paginationData.setNumberOfPages((int)Math.ceil(((double)paginationData.getTotalNumberOfResults())
                / paginationData.getPageSize()));

        // Work out the current page, fixing any invalid page values
        paginationData.setCurrentPage(Math.max(0,
                Math.min(paginationData.getNumberOfPages(), page)));

        return paginationData;
    }

    /**
     * Checks whether there are selected multi-select refinements
     * 
     * @param searchResults
     * @param endecaSearchStateData
     * @return hasSelectedMultiSelectRefinement
     */
    private boolean hasSelectedMultiSelectRefinement(final EndecaSearch searchResults,
            final EndecaSearchStateData endecaSearchStateData) {
        boolean hasSelectedMultiSelectRefinement = false;
        if (CollectionUtils.isNotEmpty(searchResults.getDimension())) {
            for (final EndecaDimension dimension : searchResults.getDimension()) {
                if (dimension.isMultiSelect() && dimension.isSelected()
                        && !(endecaSearchStateData.getBrand() != null
                                && dimension.getDimensionName()
                                        .equals(EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND))) {
                    hasSelectedMultiSelectRefinement = true;
                    break;
                }
            }
        }
        return hasSelectedMultiSelectRefinement;
    }

    private Integer getMaxSearchTextLength() {
        if (maxSearchTextLength == null) {
            maxSearchTextLength = configurationService.getConfiguration().getInteger(
                    "storefront.search.maxSearchTextLength", DEFAULT_MAX_SEARCH_TEXT_LENGTH);
        }
        return maxSearchTextLength;
    }

    /**
     * @param endecaPageAssemble
     *            the endecaPageAssemble to set
     */
    @Required
    public void setEndecaPageAssemble(final EndecaPageAssemble endecaPageAssemble) {
        this.endecaPageAssemble = endecaPageAssemble;
    }

    /**
     * @param endecaToFacetsConverter
     *            the endecaToFacetsConverter to set
     */
    @Required
    public void setEndecaToFacetsConverter(final EndecaToFacetsConverter endecaToFacetsConverter) {
        this.endecaToFacetsConverter = endecaToFacetsConverter;
    }

    /**
     * @param targetProductListerConverter
     *            the targetProductListerConverter to set
     */
    @Required
    public void setTargetProductListerConverter(
            final Converter<EndecaProduct, TargetProductListerData> targetProductListerConverter) {
        this.targetProductListerConverter = targetProductListerConverter;
    }

    /**
     * @param targetEndecaURLHelper
     *            the targetEndecaURLHelper to set
     */
    @Required
    public void setTargetEndecaURLHelper(final TargetEndecaURLHelper targetEndecaURLHelper) {
        this.targetEndecaURLHelper = targetEndecaURLHelper;
    }

    /**
     * @param endecaToSortConverter
     *            the endecaToSortConverter to set
     */
    @Required
    public void setEndecaToSortConverter(final EndecaToSortConverter endecaToSortConverter) {
        this.endecaToSortConverter = endecaToSortConverter;
    }

    /**
     * @param storeViewPreferencesHandler
     *            the storeViewPreferencesHandler to set
     */
    @Required
    public void setStoreViewPreferencesHandler(final StoreViewPreferencesHandler storeViewPreferencesHandler) {
        this.storeViewPreferencesHandler = storeViewPreferencesHandler;
    }

    /**
     * @param configurationService
     *            the configurationService to set
     */
    @Required
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    /**
     * @param searchQuerySanitiser
     *            the searchQuerySanitiser to set
     */
    @Required
    public void setSearchQuerySanitiser(final SearchQuerySanitiser searchQuerySanitiser) {
        this.searchQuerySanitiser = searchQuerySanitiser;
    }
}
