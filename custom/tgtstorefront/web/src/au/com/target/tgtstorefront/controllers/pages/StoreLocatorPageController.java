/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.storefinder.StoreFinderFacade;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.storelocator.exception.GeoLocatorException;
import de.hybris.platform.storelocator.exception.MapServiceException;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;

import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtstorefront.breadcrumb.impl.StorefinderBreadcrumbBuilder;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.checkout.AbstractCheckoutController.SelectOption;
import au.com.target.tgtstorefront.controllers.util.AddressDataHelper;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.forms.StoreFinderForm;
import au.com.target.tgtstorefront.forms.StorePositionForm;
import au.com.target.tgtstorefront.util.MetaSanitizerUtil;
import au.com.target.tgtstorefront.util.SearchQuerySanitiser;


/**
 * Controller for store locator search and detail pages. Provides display data for these two pages. Search result amount
 * is limited to the {@link #getInitialLocationsToDisplay()} value. Increasing number of displayed stores is possible by
 * giving proper argument for {@link #runLocationSearch(String, Model, int)} method but is limited to max value of
 * {@link #getMaxLocationsToDisplay()} stores.
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(value = ControllerConstants.STORE_FINDER)
public class StoreLocatorPageController extends AbstractPageController
{
    protected static final Logger LOG = Logger.getLogger(StoreLocatorPageController.class);

    private static final String STORE_FINDER_CMS_PAGE_LABEL = "storefinder";

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Resource(name = "storefinderBreadcrumbBuilder")
    private StorefinderBreadcrumbBuilder storefinderBreadcrumbBuilder;

    @Resource(name = "storeFinderFacade")
    private StoreFinderFacade storeFinderFacade;

    @Resource(name = "targetStoreLocatorFacade")
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Resource(name = "searchQuerySanitiser")
    private SearchQuerySanitiser searchQuerySanitiser;

    @Resource(name = "addressDataHelper")
    private AddressDataHelper addressDataHelper;

    @ModelAttribute("states")
    public List<SelectOption> getStates()
    {
        return addressDataHelper.getStates();
    }

    // Method to get the empty search form
    @RequestMapping(method = RequestMethod.GET)
    public String getStoreFinderPage(final Model model) throws CMSItemNotFoundException
    {
        setUpPageForms(model);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, storefinderBreadcrumbBuilder.getBreadcrumbs());
        storeCmsPageInModel(model, getStoreFinderPage());
        setUpMetaDataForContentPage(model, (ContentPageModel)getStoreFinderPage());
        return ControllerConstants.Views.Pages.StoreFinder.STORE_FINDER_SEARCH_PAGE;
    }

    @RequestMapping(value = ControllerConstants.STORE_FINDER_STATE_VARIABLE_PATTERN, method = RequestMethod.GET)
    public String getStoresByState(@PathVariable("state") final String state, final Model model)
            throws CMSItemNotFoundException
    {
        if (StringUtils.isNotBlank(state) && validateState(state.toUpperCase())) {
            setUpPageForms(model);
            model.addAttribute(WebConstants.BREADCRUMBS_KEY, storefinderBreadcrumbBuilder.getBreadcrumbs());
            storeCmsPageInModel(model, getStoreFinderPage());
            final List<TargetPointOfServiceData> stores = targetStoreLocatorFacade
                    .getStoresByState(state.toUpperCase());
            final StoreFinderSearchPageData<TargetPointOfServiceData> searchResult = new StoreFinderSearchPageData<>();
            model.addAttribute("storeSearchPageData", searchResult);
            searchResult.setResults(stores);
            model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                    storefinderBreadcrumbBuilder.getBreadcrumbsForLocationSearch(state.toUpperCase()));
            return ControllerConstants.Views.Pages.StoreFinder.STORE_FINDER_SEARCH_PAGE;
        }
        return ControllerConstants.Forward.ERROR_404;
    }

    @RequestMapping(method = RequestMethod.GET, params = "q")
    public String findSores(
            @RequestParam(value = "q") final String locationQuery,
            @RequestParam(value = "more", required = false) final Integer requestedPageSize,
            final Model model)
            throws GeoLocatorException,
            MapServiceException, CMSItemNotFoundException
    {
        final String cleanLocationQuery = searchQuerySanitiser.sanitiseSearchText(locationQuery);

        setUpPageForms(model);

        if (StringUtils.isBlank(cleanLocationQuery))
        {
            GlobalMessages.addErrorMessage(model, "storelocator.error.no.results.subtitle");
            model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                    storefinderBreadcrumbBuilder.getBreadcrumbsForLocationSearch(cleanLocationQuery));
            storeCmsPageInModel(model, getStoreFinderPage());
            return ControllerConstants.Views.Pages.StoreFinder.STORE_FINDER_SEARCH_PAGE;
        }

        final int pageSize = getPageSize(requestedPageSize);

        // Run the location search & populate the model
        runLocationSearch(cleanLocationQuery, model, pageSize);
        model.addAttribute("metaRobots", "no-index,follow");
        final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(cleanLocationQuery);
        final String metaDescription = MetaSanitizerUtil.sanitizeDescription(getSiteName()
                + " "
                + getMessageSource().getMessage("storeFinder.meta.description.results", null,
                        getI18nService().getCurrentLocale())
                + " " + cleanLocationQuery);
        setUpMetaData(model, metaKeywords, metaDescription);
        updatePageTitle(cleanLocationQuery, model);
        return ControllerConstants.Views.Pages.StoreFinder.STORE_FINDER_SEARCH_PAGE;
    }

    private void runLocationSearch(final String locationQuery, final Model model, final int pageSize)
            throws CMSItemNotFoundException
    {
        // Run the location search & populate the model
        final PageableData pageableData = preparePage(pageSize);
        final StoreFinderSearchPageData<PointOfServiceData> searchResult = storeFinderFacade.locationSearch(
                locationQuery,
                pageableData);
        model.addAttribute("storeSearchPageData", searchResult);
        model.addAttribute("locationQuery", StringEscapeUtils.escapeHtml(searchResult.getLocationText()));
        if (searchResult.getResults().isEmpty())
        {
            GlobalMessages.addErrorMessage(model, "storelocator.error.no.results.subtitle");
        }

        model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                storefinderBreadcrumbBuilder.getBreadcrumbsForLocationSearch(locationQuery));
        storeCmsPageInModel(model, getStoreFinderPage());

        if (isLimitNotExceeded(searchResult))
        {
            final int nextMoreSize = searchResult.getPagination().getPageSize() + getLocationsIncrement();
            final String showMoreUrl = UriComponentsBuilder.fromPath(ControllerConstants.STORE_FINDER)
                    .queryParam("q", locationQuery)
                    .queryParam("more", Integer.valueOf(nextMoreSize)).build().toString();
            model.addAttribute("showMoreUrl", showMoreUrl);
        }
    }

    private void setUpPageForms(final Model model)
    {
        final StoreFinderForm storeFinderForm = new StoreFinderForm();
        model.addAttribute("storeFinderForm", storeFinderForm);
        final StorePositionForm storePositionForm = new StorePositionForm();
        model.addAttribute("storePositionForm", storePositionForm);
    }

    @RequestMapping(value = ControllerConstants.STORE_FINDER_POSITION, method = { RequestMethod.GET, RequestMethod.POST })
    public String searchByCurrentPosition(
            final StorePositionForm storePositionForm,
            @RequestParam(value = "more", required = false) final Integer requestedPageSize,
            final Model model) throws GeoLocatorException, MapServiceException, CMSItemNotFoundException
    {
        final GeoPoint geoPoint = new GeoPoint();
        geoPoint.setLatitude(storePositionForm.getLatitude());
        geoPoint.setLongitude(storePositionForm.getLongitude());

        final int pageSize = getPageSize(requestedPageSize);

        // Run the location search & populate the model
        final PageableData pageableData = preparePage(pageSize);
        final StoreFinderSearchPageData<PointOfServiceData> searchResult = storeFinderFacade.positionSearch(
                geoPoint, pageableData);
        model.addAttribute("storeSearchPageData", searchResult);
        model.addAttribute("locationQuery", StringEscapeUtils.escapeHtml(searchResult.getLocationText()));

        model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                storefinderBreadcrumbBuilder.getBreadcrumbsForCurrentPositionSearch());
        storeCmsPageInModel(model, getStoreFinderPage());
        setUpPageForms(model);


        if (isLimitNotExceeded(searchResult))
        {
            final int nextMoreSize = searchResult.getPagination().getPageSize() + getLocationsIncrement();
            final String showMoreUrl = UriComponentsBuilder
                    .fromPath(ControllerConstants.STORE_FINDER + ControllerConstants.STORE_FINDER_POSITION)
                    .queryParam("latitude", Double.valueOf(storePositionForm.getLatitude()))
                    .queryParam("longitude", Double.valueOf(storePositionForm.getLongitude()))
                    .queryParam("more", Integer.valueOf(nextMoreSize)).build().toString();
            model.addAttribute("showMoreUrl", showMoreUrl);
        }

        return ControllerConstants.Views.Pages.StoreFinder.STORE_FINDER_SEARCH_PAGE;
    }

    protected PageableData preparePage(final int pageSize)
    {
        final PageableData pageableData = new PageableData();
        pageableData.setCurrentPage(0); // Always on the first page
        pageableData.setPageSize(pageSize); // Adjust pageSize to see more
        return pageableData;
    }

    private boolean isLimitNotExceeded(final StoreFinderSearchPageData<PointOfServiceData> searchResult)
    {
        return searchResult.getPagination().getNumberOfPages() > 1
                && searchResult.getPagination().getPageSize() < getMaxLocationsToDisplay();
    }

    protected AbstractPageModel getStoreFinderPage() throws CMSItemNotFoundException
    {
        return getContentPageForLabelOrId(STORE_FINDER_CMS_PAGE_LABEL);
    }

    protected void updatePageTitle(final String searchText, final Model model)
    {
        storeContentPageTitleInModel(
                model,
                getPageTitleResolver().resolveContentPageTitle(
                        getMessageSource().getMessage("storeFinder.meta.title", null,
                                getI18nService().getCurrentLocale())
                                + " "
                                + searchText));
    }

    /**
     * Calculates the current page size based on provided {@code requestedPageSize}, which can be {@code null}.
     * 
     * @param requestedPageSize
     *            the requested page size
     * @return the calculated page size
     */
    protected int getPageSize(final Integer requestedPageSize) {
        Integer pageSize = requestedPageSize;
        if (pageSize == null) {
            pageSize = Integer.valueOf(getInitialLocationsToDisplay());
        }
        return Math.min(getMaxLocationsToDisplay(), pageSize.intValue());
    }

    protected boolean validateState(final String state) {
        final List<SelectOption> states = addressDataHelper.getStates();
        for (final SelectOption stateItem : states) {
            if (stateItem != null && state.equals(stateItem.getCode())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the initial number of stores to show on the page.
     * 
     * @return the initial number of a stores
     */
    public int getInitialLocationsToDisplay() {
        return configurationService.getConfiguration().getInt("storefront.storelocator.pageSize", 5);
    }

    /**
     * Returns the increment for store locator page size, triggered by pressing 'more' button.
     * 
     * @return the increment for store locator page size
     */
    public int getLocationsIncrement() {
        return configurationService.getConfiguration().getInt("storefront.storelocator.pageSize.increment", 5);
    }

    /**
     * Returns the maximum number of stores that can be shown on a one page.
     * 
     * @return the maximum number of stores on a one page
     */
    public int getMaxLocationsToDisplay() {
        return configurationService.getConfiguration().getInt("storefront.storelocator.pageSize.limit", 100);
    }
}
