/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.search.facetdata.BreadcrumbData;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import au.com.target.endeca.infront.assemble.impl.EndecaPageAssemble;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.converter.EndecaToFacetsConverter;
import au.com.target.endeca.infront.converter.EndecaToSortConverter;
import au.com.target.endeca.infront.data.EndecaDimension;
import au.com.target.endeca.infront.data.EndecaProduct;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.querybuilder.EndecaDimensionQueryParamBuilder;
import au.com.target.endeca.infront.transform.TargetEndecaURLHelper;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.deals.TargetDealService;
import au.com.target.tgtcore.deals.strategy.TargetDealCategoryStrategy;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.product.BrandService;
import au.com.target.tgtfacades.data.CanonicalData;
import au.com.target.tgtfacades.product.data.BrandData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.search.data.TargetProductCategorySearchPageData;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.breadcrumb.impl.SearchBreadcrumbBuilder;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.kiosk.util.BulkyBoardHelper;
import au.com.target.tgtstorefront.controllers.pages.data.SeachEvaluatorData;
import au.com.target.tgtstorefront.controllers.pages.preferences.StoreViewPreferencesHandler;
import au.com.target.tgtstorefront.controllers.pages.url.URLElements;
import au.com.target.tgtstorefront.controllers.util.BrandDataHelper;
import au.com.target.tgtstorefront.controllers.util.CategoryDataHelper;
import au.com.target.tgtstorefront.controllers.util.TargetEndecaSearchHelper;
import au.com.target.tgtstorefront.util.MetaDescriptionUtil;
import au.com.target.tgtstorefront.util.MetaSanitizerUtil;
import au.com.target.tgtutility.util.JsonConversionUtil;
import au.com.target.tgtwebcore.enums.CachedPageType;
import au.com.target.tgtwebcore.model.cms2.pages.BrandPageModel;
import au.com.target.tgtwebcore.servicelayer.services.TargetCMSPageService;


/**
 * Controller for a category page
 */
@Controller
@RequestMapping(value = "/?/**/**")
public class CategoryPageController extends AbstractSearchPageController {
    protected static final Logger LOG = Logger.getLogger(CategoryPageController.class);

    private static final String PRODUCT_GRID_PAGE = "category/productGridPage";
    private static final String PRODUCT_LOOK_PAGE = "category/productLookPage";

    static {
        PAGE_TO_VIEW_AS_TYPE_MAP.put(PAGE_ROOT + PRODUCT_GRID_PAGE, ViewAsType.Grid);
        PAGE_TO_VIEW_AS_TYPE_MAP.put(PAGE_ROOT + PRODUCT_LOOK_PAGE, ViewAsType.Look);
    }

    /**
     * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
     * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
     * the issue and future resolution.
     */
    private static final String CATEGORY_CODE_PATH_VARIABLE_PATTERN = "/c/**/{categoryCode:.*}";
    private static final String BRAND_CODE_PATH_VARIABLE_PATTERN = "/b/**/{brandCode:.*}";
    private static final String DEAL_CATEGORY_CODE_PATH_VARIABLE_PATTERN = "/d/**/{dealCategoryCode:.*}";

    private static final String LANDING_PAGE_TEMPLATE_UID = "CategoryLandingPageTemplate";

    private static final String MOBILE_APP_CMS_PAGE = "mobile-app-category-page";

    @Resource(name = "targetCMSPageService")
    private TargetCMSPageService targetCMSPageService;

    @Resource(name = "commerceCategoryService")
    private CommerceCategoryService commerceCategoryService;

    @Resource(name = "searchBreadcrumbBuilder")
    private SearchBreadcrumbBuilder searchBreadcrumbBuilder;

    @Resource(name = "categoryModelUrlResolver")
    private UrlResolver<CategoryModel> categoryModelUrlResolver;

    @Resource(name = "brandModelUrlResolver")
    private UrlResolver<BrandModel> brandModelUrlResolver;

    @Resource(name = "dealCategoryModelUrlResolver")
    private UrlResolver<TargetDealCategoryModel> dealCategoryModelUrlResolver;

    @Resource(name = "storeViewPreferencesHandler")
    private StoreViewPreferencesHandler storeViewPreferencesHandler;

    @Resource(name = "brandService")
    private BrandService brandService;

    @Resource(name = "bulkyBoardHelper")
    private BulkyBoardHelper bulkyBoardHelper;

    @Resource(name = "endecaPageAssemble")
    private EndecaPageAssemble endecaPageAssemble;

    @Resource(name = "endecaToFacetsConverter")
    private EndecaToFacetsConverter endecaToFacetsConverter;

    @Resource(name = "targetProductListerConverter")
    private Converter<EndecaProduct, TargetProductListerData> productConverter;

    @Resource(name = "targetEndecaURLHelper")
    private TargetEndecaURLHelper targetEndecaURLHelper;

    @Resource(name = "endecaToSortConverter")
    private EndecaToSortConverter endecaToSortConverter;

    @Resource(name = "metaDescriptionUtil")
    private MetaDescriptionUtil metaDescriptionUtil;

    @Resource(name = "endecaDimensionQueryParamBuilder")
    private EndecaDimensionQueryParamBuilder endecaDimensionQueryParamBuilder;

    @Resource(name = "targetDealService")
    private TargetDealService targetDealService;

    @Resource(name = "targetDealCategoryStrategy")
    private TargetDealCategoryStrategy targetDealCategoryStrategy;

    @Resource(name = "targetBrandConverter")
    private Converter<BrandModel, BrandData> targetBrandConverter;

    @Resource(name = "targetEndecaSearchHelper")
    private TargetEndecaSearchHelper targetEndecaSearchHelper;

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    public String category(@PathVariable("categoryCode") final String categoryCode,

            @RequestParam(value = "page", defaultValue = "0") final String pageNoString,
            @RequestParam(value = "Ns", required = false) final String sortCode,
            @RequestParam(value = "viewAs", required = false) final String viewAs,
            @RequestParam(value = "Nrpp", defaultValue = "0") final String pageSize,
            @RequestParam(value = "N", required = false) final String navigationState,

            final Model model,
            final HttpServletRequest request, final HttpServletResponse response) throws UnsupportedEncodingException {

        final CategoryModel category = commerceCategoryService.getCategoryForCode(categoryCode);

        final String redirection = checkRequestUrl(request, response, categoryModelUrlResolver.resolve(category));
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }
        model.addAttribute("pageType", PageType.Category);
        return doCategory(category, pageNoString, sortCode, viewAs, pageSize, navigationState,
                model, null, request, response);
    }


    @RequestMapping(value = BRAND_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    public String brand(@PathVariable("brandCode") final String brandCode,

            @RequestParam(value = "page", defaultValue = "0") final String pageNoString,
            @RequestParam(value = "Ns", required = false) final String sortCode,
            @RequestParam(value = "viewAs", required = false) final String viewAs,
            @RequestParam(value = "Nrpp", defaultValue = "0") final String pageSize,
            @RequestParam(value = "N", required = false) final String navigationState,

            final Model model,
            final HttpServletRequest request, final HttpServletResponse response) throws UnsupportedEncodingException {

        final CategoryModel category = commerceCategoryService
                .getCategoryForCode(TgtCoreConstants.Category.ALL_PRODUCTS);
        final BrandModel brand = brandService.getBrandForFuzzyName(brandCode, false);
        if (brand == null) {
            // display 404
            throw new UnknownIdentifierException("Brand with code '" + brandCode + "' not found!");
        }

        final String redirection = checkRequestUrl(request, response, brandModelUrlResolver.resolve(brand));
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }
        model.addAttribute(WebConstants.BRAND_SEARCH_NAVIGATION_STATE, navigationState);
        return doBrand(category, brand, pageNoString, sortCode, viewAs, pageSize, navigationState,
                model, request, response);
    }

    /*
     * Map an alias to the bulky board root level to make it easy to link to.
     * Based on AP01 like brands.
     * For only Store number
     */
    @RequestMapping(value = ControllerConstants.BULKYBOARD_STORENUMBER, method = RequestMethod.GET)
    public String bulkyBoardRoot(@PathVariable("storeNumber") final String storeNumber,
            @RequestParam(value = "page", defaultValue = "0") final String pageNoString,
            @RequestParam(value = "Ns", required = false) final String sortCode,
            @RequestParam(value = "viewAs", required = false) final String viewAs,
            @RequestParam(value = "Nrpp", defaultValue = "0") final String pageSize,
            @RequestParam(value = "N", required = false) final String navigationState,
            final Model model,
            final HttpServletRequest request, final HttpServletResponse response) throws UnsupportedEncodingException {

        final CategoryModel category = commerceCategoryService
                .getCategoryForCode(TgtCoreConstants.Category.ALL_PRODUCTS);
        model.addAttribute("pageType", PageType.Category);
        // category does not exist
        return doCategory(category, pageNoString, sortCode, viewAs, pageSize, navigationState, model, storeNumber,
                request, response);
    }


    /*
       * Map an alias to the bulky board root level to make it easy to link to.
       * For selected Category.
       * For combination of store number and category
      */

    @RequestMapping(value = ControllerConstants.BULKYBOARD_STORENUMBER_CATEGORY, method = RequestMethod.GET)
    public String bulkyBoardRoot(@PathVariable("storeNumber") final String storeNumber,
            @PathVariable("categoryCode") final String categoryCode,
            @RequestParam(value = "page", defaultValue = "0") final String pageNoString,
            @RequestParam(value = "Ns", required = false) final String sortCode,
            @RequestParam(value = "viewAs", required = false) final String viewAs,
            @RequestParam(value = "Nrpp", defaultValue = "0") final String pageSize,
            @RequestParam(value = "N", required = false) final String navigationState,
            final Model model,
            final HttpServletRequest request, final HttpServletResponse response) throws UnsupportedEncodingException {

        final CategoryModel category = commerceCategoryService.getCategoryForCode(categoryCode);

        model.addAttribute("pageType", PageType.Category);
        return doCategory(category, pageNoString, sortCode, viewAs, pageSize, navigationState, model, storeNumber,
                request, response);
    }

    @RequestMapping(value = DEAL_CATEGORY_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    public String dealCategory(@PathVariable("dealCategoryCode") final String dealCategoryCode,

            @RequestParam(value = "page", defaultValue = "0") final String pageNoString,
            @RequestParam(value = "Ns", required = false) final String sortCode,
            @RequestParam(value = "viewAs", required = false) final String viewAs,
            @RequestParam(value = "Nrpp", defaultValue = "0") final String pageSize,
            @RequestParam(value = "N", required = false) final String navigationState,

            final Model model,
            final HttpServletRequest request, final HttpServletResponse response) throws UnsupportedEncodingException {
        CategoryModel categoryModel = null;
        try {
            categoryModel = commerceCategoryService.getCategoryForCode(dealCategoryCode);
        }
        catch (final UnknownIdentifierException e) {
            return redirectErrorPageForUnknownDealCategory("category with code '" + dealCategoryCode
                    + "' not found!", request);
        }

        if (!(categoryModel instanceof TargetDealCategoryModel)) {
            // display 404
            return redirectErrorPageForUnknownDealCategory("category with code '" + dealCategoryCode
                    + "' is not a deal category", request);
        }
        final TargetDealCategoryModel category = (TargetDealCategoryModel)categoryModel;

        final AbstractSimpleDealModel abstractSimpleDealModel = targetDealService.getDealByCategory(category);
        if (!targetDealCategoryStrategy.isActiveDeal(abstractSimpleDealModel)) {
            // display 404
            return redirectErrorPageForUnknownDealCategory("Deal with category code '" + dealCategoryCode
                    + "' is not active!", request);
        }

        final String redirection = checkRequestUrl(request, response, dealCategoryModelUrlResolver.resolve(category));
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }
        model.addAttribute("pageType", PageType.DealCategory);
        return doCategory(category, pageNoString, sortCode, viewAs, pageSize, navigationState, model, null, request,
                response);
    }


    protected String doCategory(final CategoryModel category, final String pageNoString, final String sortCode,
            final String viewAs,
            final String pageSize, final String navigationState, final Model model, final String storeNumber,
            final HttpServletRequest request,
            final HttpServletResponse response) throws UnsupportedEncodingException {
        final String categoryCode = category.getCode();

        //For bulky board store number
        Integer bulkyBoardStoreNumber = null;
        if (null != storeNumber) {
            try {
                bulkyBoardStoreNumber = Integer.valueOf(storeNumber);
            }
            catch (final NumberFormatException e) {
                bulkyBoardStoreNumber = null;
            }
        }
        final SeachEvaluatorData seachEvaluatorData = targetEndecaSearchHelper.handlePreferencesInitialData(request,
                response, categoryCode, navigationState, sortCode, viewAs, pageSize, StringUtils.EMPTY);
        model.addAttribute(
                "urlElements",
                new URLElements(seachEvaluatorData.getViewAsFn(), seachEvaluatorData.getSortCodeFn(), String
                        .valueOf(seachEvaluatorData.getItemPerPageFn())));

        //cms
        AbstractPageModel cmsPage = null;
        if (null != bulkyBoardStoreNumber) {
            cmsPage = bulkyBoardHelper.getContentPage();
        }
        else if (getSalesApplicationFacade().isSalesChannelMobileApp()) {
            cmsPage = getMobileAppPage();
        }
        else {
            cmsPage = getCategoryPage(category);
        }

        final CategorySearchEvaluator categorySearch = doCategorySearch(categoryCode, null, pageNoString,
                seachEvaluatorData.getSortCodeFn(), seachEvaluatorData.getItemPerPageFn(),
                seachEvaluatorData.getViewAsFn(), navigationState, cmsPage, storeNumber, request);
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData = categorySearch
                .getSearchPageData();
        final EndecaSearch searchResults = categorySearch.getSearchResults();
        populateCategoryData(model, category, categorySearch);
        storeContinueUrl(request);
        populateBreadcrumbData(category, null, searchResults, searchPageData, model);
        populateReactData(searchPageData, model);
        model.addAttribute(WebConstants.CMS_PAGE_MODEL, categorySearch.getCmsPage());
        CategoryDataHelper.setCurrentCategory(request, categoryCode);
        //AkamaiCache
        if (isPageCacheable(categorySearch.getCmsPage().getMasterTemplate())) {
            addAkamaiCacheAttributes(CachedPageType.CATEGORYLANDING, response, model);
        }
        if (category instanceof TargetDealCategoryModel) {
            updateDealCategoryPageTitle((TargetDealCategoryModel)category, model);
            storeMetaRobotsNoIndexNoFollow(model);
        }
        else {
            if (searchResults != null) {
                final String facetRefinementName = facetAttributeForPageTitle(searchResults.getDimension());
                updatePageTitle(category, null, cmsPage, model, facetRefinementName);
            }
        }
        setUpMetaData(categorySearch.getCmsPage(), category, model);
        final String viewPage = getViewPage(seachEvaluatorData.getViewAsFn(), categorySearch.getCmsPage());
        populateActualViewAsType(viewPage, model);
        return viewPage;
    }

    protected String doBrand(final CategoryModel category, final BrandModel brand, final String pageNoString,
            final String sortCode,
            final String viewAs,
            final String pageSize, final String navigationState, final Model model,
            final HttpServletRequest request,
            final HttpServletResponse response) throws UnsupportedEncodingException {
        final String categoryCode = category.getCode();

        final SeachEvaluatorData seachEvaluatorData = targetEndecaSearchHelper.handlePreferencesInitialData(request,
                response, categoryCode, navigationState, sortCode, viewAs, pageSize, StringUtils.EMPTY);

        model.addAttribute(
                "urlElements",
                new URLElements(seachEvaluatorData.getViewAsFn(), seachEvaluatorData.getSortCodeFn(), String
                        .valueOf(seachEvaluatorData.getItemPerPageFn())));
        //cms
        final AbstractPageModel cmsPage = getBrandPage(brand);
        final CategorySearchEvaluator categorySearch = doCategorySearch(categoryCode,
                getTargetBrandConverter().convert(brand), pageNoString,
                seachEvaluatorData.getSortCodeFn(), seachEvaluatorData.getItemPerPageFn(),
                seachEvaluatorData.getViewAsFn(), navigationState, cmsPage, null, request);
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData = categorySearch
                .getSearchPageData();
        final EndecaSearch searchResults = categorySearch.getSearchResults();
        populateBrandData(model, brand, categorySearch);
        storeContinueUrl(request);
        populateBreadcrumbData(category, brand, searchResults, searchPageData, model);
        model.addAttribute(WebConstants.CMS_PAGE_MODEL, categorySearch.getCmsPage());
        CategoryDataHelper.setCurrentCategory(request, categoryCode);
        BrandDataHelper.setCurrentBrand(request, brand.getCode());
        //AkamaiCache
        if (isPageCacheable(categorySearch.getCmsPage().getMasterTemplate())) {
            addAkamaiCacheAttributes(CachedPageType.BRANDLANDING, response, model);
        }
        final String facetRefinementName = facetAttributeForPageTitle(searchResults.getDimension());
        updatePageTitle(category, brand, cmsPage, model, facetRefinementName);
        setUpMetaData(categorySearch.getCmsPage(), category, model);
        final String viewPage = getViewPage(seachEvaluatorData.getViewAsFn(), categorySearch.getCmsPage());
        populateActualViewAsType(viewPage, model);
        return viewPage;
    }

    protected CategorySearchEvaluator doCategorySearch(final String categoryCode, final BrandData brand,
            final String pageNoString, final String sortCodeFn, final int itemPerPageFn, final String viewAsFn,
            final String navigationState,
            final AbstractPageModel cmsPage, final String storeNumber,
            final HttpServletRequest request) {
        final CategorySearchEvaluator categorySearch = new CategorySearchEvaluator(categoryCode, brand,
                pageNoString, sortCodeFn, navigationState, cmsPage, getPageSize(itemPerPageFn), viewAsFn, storeNumber,
                request);
        categorySearch.doSearch();
        return categorySearch;
    }

    protected void populateBreadcrumbData(final CategoryModel category, final BrandModel brand,
            final EndecaSearch searchResults,
            final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData,
            final Model model) {
        //TODO fix up removing facet breadcrumb  removeFacetBreadcrumbData(searchPageData, facetsToRemove);
        String departmentName = StringUtils.EMPTY;
        List<Breadcrumb> breadcrumbs = null;
        if (null != searchPageData && null == searchPageData.getErrorStatus()) {
            populateModel(model, searchPageData);
            populateCanonicalData(model, searchPageData);

            List<BreadcrumbData<EndecaSearchStateData>> refinements = searchPageData.getBreadcrumbs();

            if (refinements == null) {
                refinements = new ArrayList<BreadcrumbData<EndecaSearchStateData>>();
                searchPageData.setBreadcrumbs(refinements);
            }

            if (null != searchResults) {
                breadcrumbs = searchBreadcrumbBuilder.getBreadcrumbs(category, brand,
                        refinements, searchPageData.getFreeTextSearch(), searchResults);

                if (CollectionUtils.isNotEmpty(breadcrumbs)) {
                    departmentName = breadcrumbs.get(0).getName();

                    targetEndecaSearchHelper.populateClearAllUrl(searchPageData, searchResults, breadcrumbs);
                }
                //check if the the results are for bulkyboad and set the bulkyboard store number this will
                //used by the UI to show the colored menus.
                final Integer bbNumber = bulkyBoardHelper.getBulkyBoardNumber(searchResults);
                if (null != bbNumber) {
                    bulkyBoardHelper.populateBulkyBoard(model, category.getCode(),
                            bbNumber);
                    bulkyBoardHelper.updateBreadcrumb(breadcrumbs, bbNumber, category.getCode());
                }
            }

            model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);
            model.addAttribute(WebConstants.DEPARTMENT_NAME, departmentName);
        }
    }

    protected void setUpMetaData(final AbstractPageModel cmsPageModel, final CategoryModel category,
            final Model model) {

        String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(category.getKeywords());
        String metaDescription = null;

        if (category instanceof TargetProductCategoryModel) {
            final TargetProductCategoryModel targetProductCategoryModel = (TargetProductCategoryModel)category;
            metaDescription = targetProductCategoryModel.getMetaDescription();
            if (StringUtils.isEmpty(metaDescription)) {
                metaDescription = metaDescriptionUtil.getDefaultCategoryMetaDescription(targetProductCategoryModel
                        .getName());
            }
            else {
                metaDescription = metaDescriptionUtil.replaceTokenInCategoryMetaDescription(metaDescription,
                        targetProductCategoryModel
                                .getName());
            }

            model.addAttribute("contextualLinks", targetProductCategoryModel.getContextualLinks());
        }
        metaDescription = MetaSanitizerUtil.sanitizeDescription(metaDescription);

        // For brand landing pages, populate keywords from page model
        if (cmsPageModel instanceof BrandPageModel && BooleanUtils.isFalse(cmsPageModel.getDefaultPage())) {
            metaKeywords = MetaSanitizerUtil.sanitizeKeywords(cmsPageModel.getMetaKeywords());
            metaDescription = MetaSanitizerUtil.sanitizeDescription(cmsPageModel.getMetaDescription());
        }

        setUpMetaData(model, metaKeywords, metaDescription);
    }

    /**
     * This method will populate a product list which will be used by react
     * framework for listing page
     *
     * @param searchPageData
     * @param model
     */
    private void populateReactData(
            final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData,
            final Model model) {
        if (targetFeatureSwitchFacade.isReactListingEnabled()) {
            if (null != searchPageData && null == searchPageData.getErrorStatus()) {
                final List<ProductData> productList = searchPageData.getResults();
                final Map<String, ProductData> productsDataMap = new HashMap<>();
                for (ProductData productData : productList) {
                    productsDataMap.put(productData.getCode(), productData);
                }
                model.addAttribute("productList", JsonConversionUtil.convertToJsonString(productsDataMap));
            }
        }
    }

    /**
     * Is this page expected to be cached?
     * 
     * @param masterTemplate
     * @return true if this page is expected to be cached and false otherwise
     */
    protected boolean isPageCacheable(final PageTemplateModel masterTemplate) {
        if (StringUtils.containsIgnoreCase(masterTemplate.getUid(), LANDING_PAGE_TEMPLATE_UID)) {
            return true;
        }
        return false;
    }

    protected void removeFacetBreadcrumbData(
            final ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData,
            final List<String> facetsToRemove) {
        if (searchPageData == null || CollectionUtils.isEmpty(facetsToRemove)) {
            return;
        }

        final List<BreadcrumbData<SearchStateData>> facetNavs = searchPageData.getBreadcrumbs();
        if (CollectionUtils.isNotEmpty(facetNavs)) {
            for (final Iterator iterator = facetNavs.iterator(); iterator.hasNext();) {
                final BreadcrumbData<SearchStateData> breadcrumbData = (BreadcrumbData<SearchStateData>)iterator
                        .next();
                if (breadcrumbData != null && facetsToRemove.contains(breadcrumbData.getFacetCode())) {
                    iterator.remove();
                }
            }
        }
    }

    /**
     * @param model
     * @param category
     * @param categorySearch
     */
    protected void populateCategoryData(final Model model, final CategoryModel category,
            final CategorySearchEvaluator categorySearch) {
        model.addAttribute("showCategoriesOnly", Boolean.valueOf(categorySearch.isShowCategoriesOnly()));
        model.addAttribute("categoryCode", category.getCode());
        model.addAttribute("categoryBanner", category.getPicture());
        if (category instanceof TargetDealCategoryModel) {
            final AbstractSimpleDealModel dealModel = targetDealService
                    .getDealByCategory((TargetDealCategoryModel)category);
            if (dealModel != null) {
                model.addAttribute("categoryName", dealModel.getPageTitle());
            }
        }
        else {
            model.addAttribute("categoryName", category.getName());
        }

        //TODO fix up hot products  and will be addressed when bulkyBoardStoreNumber is addressed
        //        if (!model.containsAttribute(BulkyBoardHelper.BULKY_BOARD_STORE_NUMBER_LABEL)) {
        //            // Hot Items Carousel
        //            final List<ProductData> hotProducts = getHotProducts(category.getCode());
        //            model.addAttribute("hotProducts", hotProducts);
        //        }
    }

    /**
     * @param model
     * @param brand
     * @param categorySearch
     */
    protected void populateBrandData(final Model model, final BrandModel brand,
            final CategorySearchEvaluator categorySearch) {
        model.addAttribute("showCategoriesOnly", Boolean.valueOf(categorySearch.isShowCategoriesOnly()));
        model.addAttribute("categoryCode", brand.getCode());
        model.addAttribute("categoryName", brand.getName());
        model.addAttribute("categoryBanner", null);
        model.addAttribute("pageType", PageType.Brand);
    }

    /**
     * 
     * @param cmsPage
     * @return boolean
     */
    protected boolean categoryHasDefaultPage(final AbstractPageModel cmsPage) {
        return Boolean.TRUE.equals(cmsPage.getDefaultPage());
    }

    /**
     * 
     * @param brand
     * @return BrandPageModel
     */
    protected BrandPageModel getBrandPage(final BrandModel brand) {
        try {
            return targetCMSPageService.getPageForBrand(brand);
        }
        //CHECKSTYLE:OFF
        catch (final CMSItemNotFoundException ignore) {
            // Ignore
        }
        //CHECKSTYLE:ON
        return null;
    }

    /**
     * 
     * @param category
     * @return CategoryPageModel
     */
    protected CategoryPageModel getCategoryPage(final CategoryModel category) {
        try {
            return targetCMSPageService.getPageForCategory(category);
        }
        //CHECKSTYLE:OFF
        catch (final CMSItemNotFoundException ignore) {
            // Ignore
        }
        //CHECKSTYLE:ON
        return null;
    }

    /**
     * 
     * @return CategoryPageModel
     */
    protected CategoryPageModel getDefaultCategoryPage() {
        try {
            return targetCMSPageService.getPageForCategory(null);
        }
        //CHECKSTYLE:OFF
        catch (final CMSItemNotFoundException ignore) {
            // Ignore
        }
        //CHECKSTYLE:ON
        return null;
    }


    private AbstractPageModel getMobileAppPage() {
        try {
            return targetCMSPageService.getPageForLabelOrId(MOBILE_APP_CMS_PAGE);
        }
        //CHECKSTYLE:OFF
        catch (final CMSItemNotFoundException ignore) {
            // Ignore
        }
        //CHECKSTYLE:ON
        return null;
    }


    /**
     * 
     * @param category
     * @param model
     */
    protected void updatePageTitle(final CategoryModel category, final BrandModel brand,
            final AbstractPageModel cmsPage,
            final Model model, final String facetRefinementName) {
        if (brand == null) {
            final String pageTitle = getPageTitleResolver().resolveCategoryPageTitle(category, facetRefinementName);

            storeContentPageTitleInModel(model, pageTitle);
        }
        else {
            storeContentPageTitleInModel(model, getPageTitleResolver().resolveBrandPageTitle(brand, cmsPage));
        }
    }


    /**
     * @param dimension
     * @return String
     */
    protected String facetAttributeForPageTitle(final List<EndecaDimension> dimension) {
        String attributeRefinementName = null;
        if (CollectionUtils.isNotEmpty(dimension)) {
            final List<EndecaDimension> selectedFacets = getSelectedFacets(dimension);

            // don't proceed if multiple facet selected or no facet selected
            if (!(CollectionUtils.isEmpty(selectedFacets) || selectedFacets.size() > 1)) {
                // only one facet selected, now check if follow up flag is enabled and if yes, get the attribute name
                final EndecaDimension selectedFacet = selectedFacets.get(0);
                if (selectedFacet.isActualFollowup()) {
                    attributeRefinementName = getAttributeRefinementNameFromFacet(selectedFacet);
                }
            }
        }
        return attributeRefinementName;

    }


    /**
     * @param selectedFacet
     * @return attributeRefinementName
     */
    private String getAttributeRefinementNameFromFacet(final EndecaDimension selectedFacet) {
        String attributeRefinementName = null;
        int selectedIndex = 0;
        for (final EndecaDimension refinementAttribute : selectedFacet.getRefinement()) {
            if (refinementAttribute.isSelected()) {
                if (selectedIndex < 1) {
                    attributeRefinementName = filterFacetName(refinementAttribute.getName());
                }
                else {
                    //  more than one attribute refinement selected in facet return null
                    return null;
                }
                selectedIndex++;
            }
        }
        return attributeRefinementName;
    }


    /**
     * check if refinement name is to filtered out from '*'. If none found, then return the actual name.
     * 
     * @param attributeRefinementName
     */
    private String filterFacetName(final String attributeRefinementName) {
        if (attributeRefinementName != null) {
            final int lastIndexOf = attributeRefinementName.lastIndexOf('*');
            if (lastIndexOf > 0) {
                return attributeRefinementName.substring(lastIndexOf + 1,
                        attributeRefinementName.length());
            }
        }
        return attributeRefinementName;
    }


    /**
     * @param dimension
     * @return list of {@link EndecaDimension}
     */
    private List<EndecaDimension> getSelectedFacets(final List<EndecaDimension> dimension) {
        final List<EndecaDimension> selectedFacets = new ArrayList<>();
        for (final EndecaDimension endecaDimnesion : dimension) {
            for (final EndecaDimension refinementAttribute : endecaDimnesion.getRefinement()) {
                if (refinementAttribute.isSelected()) {
                    // all attributes selected on page
                    selectedFacets.add(endecaDimnesion);
                    // one facet may have multiple attribute selected
                    break;
                }
            }
        }
        return selectedFacets;
    }

    /**
     * update the deal category page title
     * 
     * @param dealCategoryModel
     * @param model
     */
    protected void updateDealCategoryPageTitle(final TargetDealCategoryModel dealCategoryModel, final Model model) {
        final String pageTitle = getPageTitleResolver().resolveDealCategoryPageTitle(dealCategoryModel);
        storeContentPageTitleInModel(model, pageTitle);
    }

    protected String getViewPage(final String viewAs, final AbstractPageModel cmsPage) {
        if (cmsPage != null) {
            if (cmsPage.getDefaultPage().booleanValue()) { // is default page
                if (StringUtils.isNotBlank(viewAs)) {
                    final ViewAsType viewAsType = getViewAsType(viewAs);

                    if (ViewAsType.Grid.equals(viewAsType)) {
                        return PAGE_ROOT + PRODUCT_GRID_PAGE;
                    }
                    else if (ViewAsType.Look.equals(viewAsType)) {
                        return PAGE_ROOT + PRODUCT_LOOK_PAGE;
                    }
                }
            }
            else {
                final String targetPage = getViewForPage(cmsPage);

                if (StringUtils.isNotBlank(targetPage)) {
                    return targetPage;
                }
            }
        }

        return PAGE_ROOT + PRODUCT_GRID_PAGE;
    }

    @ExceptionHandler(UnknownIdentifierException.class)
    public String handleUnknownIdentifierException(final UnknownIdentifierException exception,
            final HttpServletRequest request) {
        request.setAttribute("message", exception.getMessage());
        return ControllerConstants.Forward.ERROR_404;
    }

    /**
     * redirect to 404 page for not valid deal category
     */
    protected String redirectErrorPageForUnknownDealCategory(final String message, final HttpServletRequest request) {
        request.setAttribute("message", message);
        request.setAttribute("notFoundPageType", PageType.DealCategory);
        return ControllerConstants.Forward.ERROR_404;
    }



    /**
     * 
     * @param model
     * @param searchPageData
     */
    @SuppressWarnings("deprecation")
    protected void populateCanonicalData(final Model model,
            final ProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData) {

        String currentQueryUrl = searchPageData.getCurrentQuery().getUrl();
        currentQueryUrl = StringUtils.substringBefore(currentQueryUrl, WebConstants.QUERY_STRING_START_CHARACTER);

        final int currentPage = searchPageData.getPagination() == null ? 0
                : searchPageData.getPagination()
                        .getCurrentPage();
        final int nextPage = currentPage + 1;
        final CanonicalData canonicalData = new CanonicalData();
        canonicalData.setPrevUrl(createCanonicalURL(currentQueryUrl, currentPage - 1));
        canonicalData.setUrl(createCanonicalURL(currentQueryUrl, currentPage));
        if (null != searchPageData.getPagination() &&
                nextPage < searchPageData.getPagination().getNumberOfPages()) {
            canonicalData.setNextUrl(createCanonicalURL(currentQueryUrl, nextPage));
        }
        else {
            canonicalData.setNextUrl(StringUtils.EMPTY);
        }
        model.addAttribute("canonical", canonicalData);
    }

    protected String createCanonicalURL(final String url, final int pageNumber) {
        if (pageNumber < 0) {
            return StringUtils.EMPTY;
        }
        else if (pageNumber == 0) {
            return url;
        }
        else {
            return url + WebConstants.CANONICAL_QUERY_STRING + pageNumber;
        }
    }

    protected class CategorySearchEvaluator {

        private final String categoryCode;
        private final BrandData brand;
        private final String sortCode;
        private AbstractPageModel cmsPage;
        private final PageSize pageSize;
        private final String navigationState;
        private final String viewAs;
        private boolean showCategoriesOnly;
        private final String storeNumber;

        private TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData;
        private final HttpServletRequest request;
        private EndecaSearch searchResults;
        private int page;
        private final String pageNoString;

        /**
         * 
         * @param categoryCode
         * @param brand
         * @param pageNoString
         * @param sortCode
         * @param navigationState
         * @param cmsPage
         * @param pageSize
         * @param request
         */
        public CategorySearchEvaluator(final String categoryCode, final BrandData brand,
                final String pageNoString, final String sortCode, final String navigationState,
                final AbstractPageModel cmsPage,
                final PageSize pageSize, final String viewAs, final String storeNumber,
                final HttpServletRequest request) {
            this.categoryCode = categoryCode;
            this.brand = brand;
            this.sortCode = sortCode;
            this.cmsPage = cmsPage;
            this.pageSize = pageSize;
            this.request = request;
            this.navigationState = navigationState;
            this.viewAs = viewAs;
            this.pageNoString = pageNoString;
            this.storeNumber = storeNumber;
            //If any invalid data is send through this parameter
            //set a default value to 0;
            try {

                this.page = Integer.parseInt(this.pageNoString);

            }
            catch (final NumberFormatException e) {
                page = 0;

            }
        }

        public boolean isShowCategoriesOnly() {
            return showCategoriesOnly;
        }

        public TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> getSearchPageData() {
            return searchPageData;
        }

        public EndecaSearch getSearchResults() {
            return searchResults;
        }

        public void doSearch() {
            showCategoriesOnly = false;
            final int pSize = (null != pageSize ? pageSize.getValue() : 0);
            final EndecaSearchStateData searchStateData = targetEndecaURLHelper.buildEndecaSearchStateData(
                    navigationState, null, sortCode, pSize, page, viewAs, request.getRequestURI(), categoryCode, null);
            searchStateData.setStoreNumber(storeNumber);
            searchStateData.setBrand(brand);
            searchStateData.setCurrentQueryParam(targetEndecaURLHelper.getWsUrlQueryParameters(searchStateData));

            searchPageData = new TargetProductCategorySearchPageData();
            searchResults = targetEndecaSearchHelper.getEndecaSearchResult(request, searchStateData);
            setSearchData(searchStateData);

        }

        private void setSearchData(final EndecaSearchStateData searchStateData) {

            targetEndecaSearchHelper.populateSearchPageData(searchPageData, searchResults, searchStateData);
            if (searchResults != null) {
                showCategoriesOnly = false;
                if (StringUtils.isBlank(navigationState)) {
                    if (cmsPage != null) {
                        showCategoriesOnly = !categoryHasDefaultPage(cmsPage)
                                && hasSubCategories(searchResults.getDimension());
                    }
                }
            }

            if (StringUtils.isNotBlank(navigationState)) {
                if (cmsPage == null || !categoryHasDefaultPage(cmsPage)) {
                    // Load the default category page
                    cmsPage = getDefaultCategoryPage();
                }
            }
        }


        /**
         * @param dimensions
         * @return boolean true if there are subcategories and false otherwise
         */
        private boolean hasSubCategories(final List<EndecaDimension> dimensions) {
            if (CollectionUtils.isNotEmpty(dimensions)) {
                for (final EndecaDimension dimension : dimensions) {
                    if (dimension.getName().equals(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY)) {
                        return true;
                    }
                }
            }
            return false;
        }

        /**
         * @return the cmsPage
         */
        protected AbstractPageModel getCmsPage() {
            return cmsPage;
        }


    }

    /**
     * Checks request URL against properly resolved URL and returns null if url is proper or redirection string if not.
     * 
     * @param request
     *            - request that contains current URL
     * @param response
     *            - response to write "301 Moved Permanently" status to if redirected
     * @param resolvedUrl
     *            - properly resolved URL
     * @param queryString
     *            - querystring to redirect with
     * @return null if url is properly resolved or redirection string if not
     * @throws UnsupportedEncodingException
     */
    protected String buildRedirectUrl(final HttpServletRequest request, final HttpServletResponse response,
            final String resolvedUrl, final String queryString)
            throws UnsupportedEncodingException {


        request.setAttribute(View.RESPONSE_STATUS_ATTRIBUTE, HttpStatus.MOVED_PERMANENTLY);
        if (queryString != null && !queryString.isEmpty()) {
            return UrlBasedViewResolver.REDIRECT_URL_PREFIX + resolvedUrl + "?" + queryString;
        }
        return UrlBasedViewResolver.REDIRECT_URL_PREFIX + resolvedUrl;


    }


    /**
     * @return the targetBrandConverter
     */
    protected Converter<BrandModel, BrandData> getTargetBrandConverter() {
        return targetBrandConverter;
    }

}
