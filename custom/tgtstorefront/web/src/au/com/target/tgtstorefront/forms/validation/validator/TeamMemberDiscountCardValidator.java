/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtfacades.order.TargetDiscountFacade;
import au.com.target.tgtstorefront.forms.validation.TeamMemberDiscountCard;


/**
 * @author asingh78
 * 
 */
public class TeamMemberDiscountCardValidator extends AbstractTargetValidator implements
        ConstraintValidator<TeamMemberDiscountCard, String> {
    @Resource(name = "targetDiscountFacade")
    private TargetDiscountFacade targetDiscountFacade;


    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
     */
    @Override
    public void initialize(final TeamMemberDiscountCard arg0) {
        field = FieldTypeEnum.teamMemberCode;

    }

    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
     */
    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }

    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
     */
    @Override
    public boolean isAvailable(final String teamMemberCard) {
        // by default always return true it must be override
        return StringUtils.isNotBlank(teamMemberCard)
                && targetDiscountFacade.validateTeamMemberDiscountCard(teamMemberCard);
    }



}
