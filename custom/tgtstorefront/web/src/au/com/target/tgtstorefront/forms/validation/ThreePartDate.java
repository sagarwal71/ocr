/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.validation.Constraint;
import javax.validation.Payload;

import au.com.target.tgtstorefront.forms.validation.validator.ThreePartDateValidator;


@Retention(RUNTIME)
@Constraint(validatedBy = ThreePartDateValidator.class)
@Documented
public @interface ThreePartDate {

    String message();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String[] value() default { "day", "month", "year" };

    boolean mandatory() default true;

}
