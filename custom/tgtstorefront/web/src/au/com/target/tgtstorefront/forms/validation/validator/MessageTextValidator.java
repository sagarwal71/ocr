/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtstorefront.forms.validation.MessageText;
import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * @author smishra1
 * 
 */
public class MessageTextValidator extends AbstractTargetValidator implements
        ConstraintValidator<MessageText, String> {

    @Override
    public void initialize(final MessageText arg0) {
        field = arg0.field();
        sizeRange = new int[] { TargetValidationCommon.MessageText.MIN_SIZE,
                TargetValidationCommon.MessageText.MAX_SIZE };
        isMandatory = arg0.mandatory();
        isSizeRange = true;

        // No pattern validation, we rely just on the XSSFilter
        mustMatch = false;
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        String normalizedString = null;
        if (StringUtils.isNotBlank(value)) {
            normalizedString = value.replaceAll("\r\n", "\n");
        }
        return isValidCommon(normalizedString, context);
    }

}
