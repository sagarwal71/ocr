package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;

import java.util.Collections;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import au.com.target.endeca.infront.dto.CustomerSubscriptionInfo;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionType;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.forms.ENewsSubscriptionForm;
import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * Controller for enews subscription page.
 */
@Controller
public class ENewsSubscriptionPageController extends AbstractCustomerSubscriptionController
{

    private static final String SITE_CMS_LABEL_ENEWS_PAGE = "enews-subscription";

    private static final String SITE_CMS_LABEL_ENEWS_PREFERENCES_PAGE = "enews-preferences";


    private static final String SITE_CMS_LABEL_ENEWS_QUICK_POPUP = "enews-quick-popup";

    protected String getEnewsSubscriptionView()
    {
        return ControllerConstants.Views.Pages.Account.ACCOUNT_ENEWS_SUBSCRIPTION_PAGE;
    }

    protected String getEnewsPreferencesView()
    {
        return ControllerConstants.Views.Pages.Account.ACCOUNT_ENEWS_PREFERENCES_PAGE;
    }

    /**
     * 
     * @param model
     * @param subscriptionSource
     * @return RedirectView
     */
    @RequestMapping(value = { ControllerConstants.ENEWS_SUBSCRIPTION_REDIRECT,
            ControllerConstants.ENEWS_OLD_SUBSCRIPTION_REDIRECT }, method = RequestMethod.GET)
    public RedirectView getRedirectEnews(@SuppressWarnings("unused") final Model model,
            @RequestParam(value = "accredit", required = false) final String subscriptionSource) {
        final RedirectView eNewsRedirect;
        if (StringUtils.isNotEmpty(subscriptionSource)) {
            final String redirectUrl = ControllerConstants.ENEWS_SUBSCRIPTION + ControllerConstants.ACCREDIT_URL
                    + subscriptionSource;
            eNewsRedirect = new RedirectView(redirectUrl);
        }
        else {
            eNewsRedirect = new RedirectView(ControllerConstants.ENEWS_SUBSCRIPTION);
        }
        eNewsRedirect.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
        return eNewsRedirect;
    }

    @RequestMapping(value = ControllerConstants.ENEWS_PREFERENCES, method = RequestMethod.GET)
    public String getEnewsPreferences(final Model model)
            throws CMSItemNotFoundException {
        addBreadcrumbs(model);
        storeCmsPageInModel(model, getCmsPage(SITE_CMS_LABEL_ENEWS_PREFERENCES_PAGE));

        return getEnewsPreferencesView();
    }

    @RequestMapping(value = ControllerConstants.ENEWS_SUBSCRIPTION, method = RequestMethod.GET)
    public String getEnewsSubscribe(
            @RequestParam(value = "accredit", required = false) final String subscriptionSource, final Model model)
            throws CMSItemNotFoundException {
        final ENewsSubscriptionForm enewsSubscriptionForm = new ENewsSubscriptionForm();
        enewsSubscriptionForm.setSubscriptionSource(subscriptionSource);
        model.addAttribute(enewsSubscriptionForm);
        addBreadcrumbs(model);
        storeCmsPageInModel(model, getCmsPage(SITE_CMS_LABEL_ENEWS_PAGE));
        return getEnewsSubscriptionView();
    }

    @RequestMapping(value = ControllerConstants.ENEWS_SUBSCRIPTION, method = RequestMethod.POST)
    public String doSubscribe(@Valid final ENewsSubscriptionForm eNewsSubscriptionForm,
            final BindingResult bindingResult, final Model model, final HttpServletResponse response,
            final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {

        if (!bindingResult.hasErrors()
                && processSubscribe(eNewsSubscriptionForm, response)) {
            GlobalMessages.addFlashConfMessage(redirectAttributes,
                    "enews.subscription.confirmation.message.title");
            return ControllerConstants.Redirection.ENEWS_SUBSCRIPTION;
        }
        return handleSubscriptionError(model, eNewsSubscriptionForm);
    }

    /**
     * Handles Get the Request for Enews quick modal popup.
     * 
     * @param model
     * @return view for Enews Quick Modal popup
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.ENEWS_QUICK, method = RequestMethod.GET)
    public String enewsQuickPage(final Model model)
            throws CMSItemNotFoundException {
        model.addAttribute(new ENewsSubscriptionForm());
        storeCmsPageInModel(model, getCmsPage(SITE_CMS_LABEL_ENEWS_QUICK_POPUP));
        return ControllerConstants.Views.Fragments.Enews.ENEWS_QUICK_POPUP_FRAGMENT;
    }

    /**
     * This method will accept the post request with an email and use the information for subscription. Will return a
     * Json Response indicating whether it was successful or not along with the success/failure msg.
     * 
     * @param model
     * @param subscriptionInfo
     * @param response
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.ENEWS_QUICK, method = RequestMethod.POST)
    public String enewsQuick(final Model model,
            @RequestBody final CustomerSubscriptionInfo subscriptionInfo,
            final HttpServletResponse response)
            throws CMSItemNotFoundException {
        boolean isEmailValid = true;
        String failMessage = null;

        if (StringUtils.isEmpty(subscriptionInfo.getEmail())) {
            isEmailValid = false;
            failMessage = getMessageFromProperties("enews.email.invalid.empty");
        }
        else if (!TargetValidationCommon.Email.PATTERN.matcher(subscriptionInfo.getEmail()).matches()) {
            isEmailValid = false;
            failMessage = getMessageFromProperties("enews.email.invalid.pattern");
        }
        final ENewsSubscriptionForm eNewsSubscriptionForm = new ENewsSubscriptionForm();
        eNewsSubscriptionForm.setEmail(subscriptionInfo.getEmail());
        eNewsSubscriptionForm.setSubscriptionSource(subscriptionInfo.getSubscriptionSource());
        if (isEmailValid
                && processSubscribe(eNewsSubscriptionForm, response)) {
            setJsonResponseMsg(model, WebConstants.ResponseMessageType.Success,
                    TargetCustomerSubscriptionType.NEWSLETTER, getSuccessfulSubscriptionMsg());
        }
        else {
            setJsonResponseMsg(model, WebConstants.ResponseMessageType.Error,
                    TargetCustomerSubscriptionType.NEWSLETTER, failMessage);
            LOG.warn("Unsuccessful Subscription");
        }
        return ControllerConstants.Views.Fragments.Enews.ENEWS_JSON_FRAGMENT;

    }

    protected String handleSubscriptionError(final Model model, final ENewsSubscriptionForm eNewsSubscriptionForm)
            throws CMSItemNotFoundException
    {
        model.addAttribute(eNewsSubscriptionForm);
        GlobalMessages.addErrorMessage(model, "form.global.error");
        addBreadcrumbs(model);
        storeCmsPageInModel(model, getCmsPage(SITE_CMS_LABEL_ENEWS_PAGE));
        setUpMetaDataForContentPage(model, (ContentPageModel)getCmsPage(SITE_CMS_LABEL_ENEWS_PAGE));
        return getEnewsSubscriptionView();
    }

    private void addBreadcrumbs(final Model model) {
        final Breadcrumb enewsBreadcrumbEntry = new Breadcrumb("#", getMessageSource().getMessage("enews.breadcrumb",
                null,
                getI18nService().getCurrentLocale()), null);
        model.addAttribute("breadcrumbs", Collections.singletonList(enewsBreadcrumbEntry));
    }
}
