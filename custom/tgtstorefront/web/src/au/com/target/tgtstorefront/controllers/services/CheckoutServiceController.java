/**
 * 
 */
package au.com.target.tgtstorefront.controllers.services;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.servicelayer.session.SessionService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.customer.TargetCustomerSubscriptionFacade;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.flow.impl.SessionOverrideCheckoutFlowFacade;
import au.com.target.tgtfacades.order.TargetCheckoutResponseFacade;
import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;
import au.com.target.tgtfacades.order.data.ClickAndCollectDeliveryDetailData;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.Error;
import au.com.target.tgtfacades.response.data.LoginSuccessResponseData;
import au.com.target.tgtfacades.response.data.PaymentResponseData;
import au.com.target.tgtfacades.response.data.RegisterUserResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionSource;
import au.com.target.tgtstorefront.annotations.RequireHardLogIn;
import au.com.target.tgtstorefront.checkout.request.dto.CncPickupDetailsDTO;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.checkout.AbstractCheckoutController;
import au.com.target.tgtstorefront.controllers.util.AddressDataHelper;
import au.com.target.tgtstorefront.controllers.util.FlybuysDataHelper;
import au.com.target.tgtstorefront.controllers.util.TargetPaymentHelper;
import au.com.target.tgtstorefront.controllers.util.WebServiceExceptionHelper;
import au.com.target.tgtstorefront.forms.TargetAddressForm;
import au.com.target.tgtstorefront.forms.TargetAddressForm.DomesticAddressValidationGroup;
import au.com.target.tgtstorefront.forms.TargetAddressForm.InternationalAddressValidationGroup;
import au.com.target.tgtstorefront.forms.checkout.FlybuysLoginForm;
import au.com.target.tgtstorefront.forms.checkout.FlybuysRedeemForm;
import au.com.target.tgtstorefront.forms.checkout.RegisterForm;
import au.com.target.tgtstorefront.security.AutoLoginStrategy;
import au.com.target.tgtstorefront.util.TargetSessionTokenUtil;


/**
 * This is a sample controller for single page checkout ws-api calls
 *
 */
@Controller
@RequestMapping(value = "/ws-api/v1/{baseSiteId}/checkout")
@RequireHardLogIn
public class CheckoutServiceController extends AbstractCheckoutController {

    protected static final Logger LOG = Logger.getLogger(CheckoutServiceController.class);

    @Autowired
    private TargetCheckoutResponseFacade targetCheckoutResponseFacade;

    @Autowired
    private SmartValidator validator;

    @Autowired
    private TargetPaymentHelper targetPaymentHelper;

    @Resource(name = "addressDataHelper")
    private AddressDataHelper addressDataHelper;

    @Resource(name = "webServiceExceptionHelper")
    private WebServiceExceptionHelper webServiceExceptionHelper;

    @Resource(name = "flybuysDataHelper")
    private FlybuysDataHelper flybuysDataHelper;

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Autowired
    private AutoLoginStrategy autoLoginStrategy;

    @Autowired
    private TargetCustomerFacade targetCustomerFacade;

    @Autowired
    private TargetCustomerSubscriptionFacade targetCustomerSubscriptionFacade;

    @Autowired
    private SessionService sessionService;

    @RequestMapping(value = "/delivery/applicable-modes", method = RequestMethod.GET)
    @ResponseBody
    public Response getApplicableModes() {
        return targetCheckoutResponseFacade.getApplicableDeliveryModes();
    }

    @RequestMapping(value = "/delivery/stores/search", method = RequestMethod.POST)
    @ResponseBody
    public Response getCncStores(@RequestParam(value = "locationText", required = false) final String locationText) {
        return targetCheckoutResponseFacade.searchCncStores(locationText, setPageableDataForCncStore());
    }

    @RequestMapping(value = "/delivery/set-mode", method = RequestMethod.POST)
    @ResponseBody
    public Response setDeliveryMode(
            @RequestParam(value = "id", required = false) final String deliveryMode) {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            return targetCheckoutResponseFacade.setDeliveryMode(deliveryMode);
        }
    }

    @RequestMapping(value = "/delivery/stores/set-pickup", method = RequestMethod.POST)
    @ResponseBody
    public Response setClickAndCollectStore(
            @RequestParam(value = "storeNumber", required = false) final Integer storeNumber) {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            return targetCheckoutResponseFacade.setCncStore(storeNumber);
        }
    }

    @RequestMapping(value = "/delivery/addresses", method = RequestMethod.GET)
    @ResponseBody
    public Response getSavedAddresses() {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            return targetCheckoutResponseFacade.getDeliveryAddresses();
        }
    }

    @RequestMapping(value = "/delivery/addresses/set-address", method = RequestMethod.POST)
    @ResponseBody
    public Response setDeliveryAddress(@RequestParam(value = "addressId", required = false) final String addressId) {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            return targetCheckoutResponseFacade.selectDeliveryAddress(addressId);
        }
    }

    @RequestMapping(value = "/cart/detail", method = RequestMethod.GET)
    @ResponseBody
    public Response getCartDetail(final HttpServletRequest request) {
        if (TargetSessionTokenUtil.isPlaceOrderInProgress(request)) {
            final Response response = new Response(false);
            final BaseResponseData baseResponseData = new BaseResponseData();
            baseResponseData.setError(new Error("ERR_PLACEORDER_IN_PROGRESS"));
            response.setData(baseResponseData);
            return response;
        }
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            return targetCheckoutResponseFacade.getCartDetail();
        }
    }

    @RequestMapping(value = "/cart/summary", method = RequestMethod.GET)
    @ResponseBody
    public Response getCartSummary() {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            return targetCheckoutResponseFacade.getCartSummary();
        }
    }

    @RequestMapping(value = "/delivery/stores/set-pickup-details", method = RequestMethod.POST)
    @ResponseBody
    public Response setClickAndCollectPickupDetails(
            @Valid final CncPickupDetailsDTO cncPickupDetails) {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            final ClickAndCollectDeliveryDetailData cncDeliveryData = new ClickAndCollectDeliveryDetailData();
            cncDeliveryData.setTitle(cncPickupDetails.getTitle());
            cncDeliveryData.setFirstName(cncPickupDetails.getFirstName());
            cncDeliveryData.setLastName(cncPickupDetails.getLastName());
            cncDeliveryData.setTelephoneNumber(cncPickupDetails.getMobileNumber());
            cncDeliveryData.setStoreNumber(cncPickupDetails.getStoreNumber());
            return targetCheckoutResponseFacade.setCncPickupDetails(cncDeliveryData);
        }
    }

    @RequestMapping(value = "/promotions/apply-tmd", method = RequestMethod.POST)
    @ResponseBody
    public Response applyTmd(
            @RequestParam(value = "code", required = false) final String tmdNumber) {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            return targetCheckoutResponseFacade.applyTmd(tmdNumber);
        }
    }

    @RequestMapping(value = "/promotions/apply-voucher", method = RequestMethod.POST)
    @ResponseBody
    public Response applyVoucher(@RequestParam(required = false, value = "code") final String voucherCode) {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            return targetCheckoutResponseFacade.applyVoucher(voucherCode);
        }
    }

    @RequestMapping(value = "/promotions/remove-voucher", method = RequestMethod.POST)
    @ResponseBody
    public Response removeVoucher() {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            return targetCheckoutResponseFacade.removeVoucher();
        }
    }

    @RequestMapping(value = "/promotions/apply-flybuys", method = RequestMethod.POST)
    @ResponseBody
    public Response applyFlybuys(@RequestParam(required = false, value = "code") final String flybuysNumber) {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            return targetCheckoutResponseFacade.applyFlybuys(flybuysNumber);
        }
    }

    @RequestMapping(value = "/delivery/addresses/create", method = RequestMethod.POST)
    @ResponseBody
    public Response createDeliveryAddress(final TargetAddressForm addressForm, final BindingResult bindingResult)
            throws BindException {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            validator.validate(addressForm, bindingResult, TargetAddressForm.ContactValidationGroup.class);
            if (bindingResult.hasErrors()) {
                throw new BindException(bindingResult);
            }

            TargetAddressData newAddressData = null;
            if (StringUtils.isNotEmpty(addressForm.getSingleLineId())
                    && StringUtils.isNotEmpty(addressForm.getSingleLineLabel())) {
                newAddressData = addressDataHelper
                        .createVerifiedTargetAustralianShippingAddressData(addressForm);
            }
            else {
                validator.validate(addressForm, bindingResult, DomesticAddressValidationGroup.class);
                if (bindingResult.hasErrors()) {
                    throw new BindException(bindingResult);
                }
                newAddressData = addressDataHelper
                        .createTargetAustralianShippingAddressData(addressForm);
            }
            return targetCheckoutResponseFacade.createAddress(newAddressData);
        }
    }

    @RequestMapping(value = { "/billing/set-address", "/billing/addresses/create" }, method = RequestMethod.POST)
    @ResponseBody
    public Response createBillingAddress(final TargetAddressForm addressForm, final BindingResult bindingResult)
            throws BindException {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            TargetAddressData newAddressData = null;
            if (!TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA.equals(addressForm.getCountryCode())) {
                addressForm.setInternationalPostalcode(addressForm.getPostalCode());
            }
            if (StringUtils.isNotEmpty(addressForm.getSingleLineId())
                    && StringUtils.isNotEmpty(addressForm.getSingleLineLabel())) {
                newAddressData = addressDataHelper.createVerifiedTargetAustralianBillingAddressData(addressForm);
            }
            else {
                if (StringUtils.isNotEmpty(addressForm.getInternationalPostalcode())) {
                    validator.validate(addressForm, bindingResult, InternationalAddressValidationGroup.class);
                }
                else {
                    validator.validate(addressForm, bindingResult, DomesticAddressValidationGroup.class);
                }
                if (bindingResult.hasErrors()) {
                    throw new BindException(bindingResult);
                }
                newAddressData = addressDataHelper
                        .createTargetBillingAddressData(addressForm);
            }
            return targetCheckoutResponseFacade.createBillingAddress(newAddressData);
        }
    }

    @RequestMapping(value = "/billing/addresses/set-address", method = RequestMethod.POST)
    @ResponseBody
    public Response setBillingAddress(@RequestParam(value = "addressId", required = false) final String addressId) {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            return targetCheckoutResponseFacade.selectBillingAddress(addressId);
        }
    }

    @RequestMapping(value = "/payment/applicable-methods", method = RequestMethod.GET)
    @ResponseBody
    public Response getApplicablePaymentModes() {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            return targetCheckoutResponseFacade.getApplicablePaymentMethods();
        }
    }

    @RequestMapping(value = "/delivery/addresses/search", method = RequestMethod.POST)
    @ResponseBody
    public Response searchAddress(@RequestParam(value = "text", required = false) final String keyword) {
        return targetCheckoutResponseFacade
                .searchAddress(keyword, addressDataHelper.getMaxSearchSizeForAddressLookup());
    }

    @RequestMapping(value = "/payment/set-ipg-mode", method = RequestMethod.POST)
    @ResponseBody
    public Response setIpgPaymentMode(@RequestParam(value = "id", required = false) final String ipgPaymentMode,
            final HttpServletRequest request) {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            targetCheckoutResponseFacade.removeExistingPayment();
            final String returnUrl = getDomainUrl(request) + ControllerConstants.SPC_IPG_IFRAME_RETURN;
            return targetCheckoutResponseFacade.setIpgPaymentMode(ipgPaymentMode, returnUrl);
        }
    }

    @RequestMapping(value = "/payment/ipg-status", method = RequestMethod.GET)
    @ResponseBody
    public Response getIpgStatus() {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            return targetCheckoutResponseFacade.getIpgPaymentStatus();
        }
    }

    @RequestMapping(value = "/payment/set-paypal-mode", method = RequestMethod.POST)
    @ResponseBody
    public Response setPaypalPaymentMode(final HttpServletRequest request) {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            Assert.notNull(request, "Request object cannot be null");
            targetCheckoutResponseFacade.removeExistingPayment();
            final String payPalReturnUrl = targetPaymentHelper.getBaseSecureUrl().concat(ControllerConstants.PAYMENT)
                    .concat(
                            ControllerConstants.PAYMENT_PAYPAL_RETURN)
                    .concat(ControllerConstants.SINGLE_PAGE_CHECKOUT);
            String payPalCancelUrl = targetPaymentHelper.getBaseSecureUrl().concat(
                    ControllerConstants.SINGLE_PAGE_CHECKOUT);
            if (targetFeatureSwitchFacade.isSpcLoginAndCheckout()) {
                payPalCancelUrl = targetPaymentHelper.getBaseSecureUrl().concat(
                        ControllerConstants.SINGLE_PAGE_CHECKOUT_AND_LOGIN);
            }
            final Response response = targetCheckoutResponseFacade.setPaypalPaymentMode(payPalReturnUrl,
                    payPalCancelUrl);

            if (null != response.getData() && response.isSuccess()) {
                final PaymentResponseData paymentResponseData = (PaymentResponseData)(response.getData());
                final String paypalSessionToken = paymentResponseData.getPaypalSessionToken();
                if (StringUtils.isNotEmpty(paypalSessionToken)) {
                    request.getSession().setAttribute(ControllerConstants.SESSION_PARAM_PAYPAL_EC_TOKEN,
                            paypalSessionToken);
                    paymentResponseData.setPaypalUrl(targetPaymentHelper.getPayPalHostedPaymentFormBaseUrl()
                            .concat(paypalSessionToken));
                }
            }
            return response;
        }
    }

    @RequestMapping(value = "/payment/set-afterpay-mode", method = RequestMethod.POST)
    @ResponseBody
    public Response setAfterpayPaymentMode(final HttpServletRequest request) {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            Assert.notNull(request, "Request object cannot be null");
            targetCheckoutResponseFacade.removeExistingPayment();
            final String afterpayReturnUrl = targetPaymentHelper.getBaseSecureUrl().concat(ControllerConstants.PAYMENT)
                    .concat(ControllerConstants.PAYMENT_AFTERPAY_RETURN)
                    .concat(ControllerConstants.SINGLE_PAGE_CHECKOUT);
            final String afterpayCancelUrl = targetPaymentHelper.getBaseSecureUrl().concat(ControllerConstants.PAYMENT)
                    .concat(ControllerConstants.PAYMENT_AFTERPAY_CANCEL).concat(
                            ControllerConstants.SINGLE_PAGE_CHECKOUT);
            final Response response = targetCheckoutResponseFacade.setAfterpayPaymentMode(afterpayReturnUrl,
                    afterpayCancelUrl);

            if (null != response.getData() && response.isSuccess()) {
                final PaymentResponseData paymentResponseData = (PaymentResponseData)(response.getData());
                final String afterpaySessionToken = paymentResponseData.getAfterpaySessionToken();
                if (StringUtils.isNotEmpty(afterpaySessionToken)) {
                    request.getSession().setAttribute(ControllerConstants.SESSION_PARAM_AFTERPAY_TOKEN,
                            afterpaySessionToken);
                }
            }
            return response;
        }
    }

    @RequestMapping(value = "/payment/set-zippay-mode", method = RequestMethod.POST)
    @ResponseBody
    public Response setZippayPaymentMode(final HttpServletRequest request) {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            Assert.notNull(request, "Request object cannot be null");
            targetCheckoutResponseFacade.removeExistingPayment();
            final String zippayRedirectUrl = targetPaymentHelper.getBaseSecureUrl().concat(ControllerConstants.PAYMENT)
                    .concat(ControllerConstants.PAYMENT_ZIPPAY_REDIRECT)
                    .concat(ControllerConstants.SINGLE_PAGE_CHECKOUT);
            final Response response = targetCheckoutResponseFacade.setZippayPaymentMode(zippayRedirectUrl);

            if (null != response.getData() && response.isSuccess()) {
                final PaymentResponseData paymentResponseData = (PaymentResponseData)(response.getData());
                final String zippaySessionToken = paymentResponseData.getZipPaymentSessionToken();
                if (StringUtils.isNotEmpty(zippaySessionToken)) {
                    request.getSession().setAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN,
                            zippaySessionToken);
                }
            }
            return response;
        }
    }


    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Response handleException(final HttpServletRequest request,
            final Exception exception) {
        return webServiceExceptionHelper.handleException(request, exception);
    }

    @RequestMapping(value = "/billing/countries", method = RequestMethod.GET)
    @ResponseBody
    public Response getBillCountries() {
        return targetCheckoutResponseFacade.getBillingCountries();
    }

    @RequestMapping(value = "/promotions/remove-flybuys", method = RequestMethod.POST)
    @ResponseBody
    public Response removeFlybuys() {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            return targetCheckoutResponseFacade.removeFlybuys();
        }
    }

    @RequestMapping(value = "/promotions/login-flybuys", method = RequestMethod.POST)
    @ResponseBody
    public Response loginFlybuys(
            @Valid final FlybuysLoginForm flybuysLoginForm) {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            return targetCheckoutResponseFacade.loginFlybuys(flybuysDataHelper
                    .convertToFlybuysLoginData(flybuysLoginForm));
        }
    }

    @RequestMapping(value = "/promotions/redeem-flybuys", method = RequestMethod.POST)
    @ResponseBody
    public Response redeemFlybuys(@Valid final FlybuysRedeemForm flybuysRedeemForm) {
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            return targetCheckoutResponseFacade.redeemFlybuys(flybuysRedeemForm.getRedeemCode());
        }
    }

    @RequestMapping(value = "/promotions/flybuys-redemption-option", method = RequestMethod.GET)
    @ResponseBody
    public Response showFlybuysRedemptionOption() {
        return targetCheckoutResponseFacade.showFlybuysRedemptionOption();
    }

    @RequestMapping(value = "/thank-you/{orderId:.*}", method = RequestMethod.GET)
    @ResponseBody
    public Response thankYou(final HttpServletRequest request, @PathVariable("orderId") final String orderId) {
        resetSessionOverrides();
        final Response response = validateOrderId(request, orderId);
        if (response != null) {
            return response;
        }
        final AdjustedCartEntriesData sohUpdates = (AdjustedCartEntriesData)request.getSession()
                .getAttribute(ControllerConstants.SOH_UPDATES);
        request.getSession().removeAttribute(ControllerConstants.SOH_UPDATES);
        return targetCheckoutResponseFacade.getOrderDetail(orderId, sohUpdates);
    }

    @RequestMapping(value = "/thank-you/check", method = RequestMethod.GET)
    @ResponseBody
    public Response thankYouCheck() {
        getSessionService().removeAttribute(TgtCoreConstants.SESSION_POSTCODE);
        return targetCheckoutResponseFacade.checkoutProblem();
    }

    @RequestMapping(value = "/thank-you/{orderId:.*}/register", method = RequestMethod.POST)
    @ResponseBody
    public Response register(@PathVariable("orderId") final String orderId,
            @Valid final RegisterForm registerForm, final HttpServletRequest request,
            final HttpServletResponse httpServletResponse) {
        Response response = validateOrderId(request, orderId);
        if (response != null) {
            return response;
        }
        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            response = targetCheckoutResponseFacade.registerCustomer(orderId, registerForm.getPassword());
            if (response.isSuccess()) {
                autoLoginStrategy.login(((RegisterUserResponseData)response.getData()).getEmail(),
                        registerForm.getPassword(), request, httpServletResponse);
                final CustomerData customer = targetCustomerFacade.getCurrentCustomer();
                performCustomerSubscription(customer, registerForm.isOptIntoMarketing());
            }
            return response;
        }
    }

    /**
     * Perform customer subscription
     * 
     * @param customer
     * @param optIntoMarketing
     */
    private void performCustomerSubscription(final CustomerData customer, final boolean optIntoMarketing) {
        final TargetCustomerSubscriptionRequestDto subscriptionDetails = new TargetCustomerSubscriptionRequestDto();
        subscriptionDetails.setCustomerEmail(customer.getUid());
        subscriptionDetails.setFirstName(customer.getFirstName());
        subscriptionDetails.setLastName(customer.getLastName());
        subscriptionDetails.setRegisterForEmail(optIntoMarketing);
        subscriptionDetails.setCustomerSubscriptionSource(TargetCustomerSubscriptionSource.WEBCHECKOUT);
        try {
            targetCustomerSubscriptionFacade.performCustomerSubscription(subscriptionDetails);
        }
        catch (final Exception e) {
            LOG.warn("Newsletter subscription failed for email=" + customer.getUid(), e);
        }
    }

    /**
     * Validate the given orderId against the one in session.
     * 
     * @param request
     * @param orderId
     * @return Response
     */
    private Response validateOrderId(final HttpServletRequest request, final String orderId) {
        final String orderIdFromSession = (String)request.getSession().getAttribute(
                ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE);
        if (null == orderIdFromSession || !orderId.equals(orderIdFromSession)) {
            final Response response = new Response(false);
            final LoginSuccessResponseData data = new LoginSuccessResponseData();
            data.setRedirectUrl(ControllerConstants.CART);
            response.setData(data);
            return response;
        }
        return null;
    }

    /**
     * @see SessionOverrideCheckoutFlowFacade
     */
    private void resetSessionOverrides() {
        sessionService.removeAttribute(SessionOverrideCheckoutFlowFacade.SESSION_KEY_SUBSCRIPTION_PCI_OPTION);
        sessionService.removeAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE);
    }
}