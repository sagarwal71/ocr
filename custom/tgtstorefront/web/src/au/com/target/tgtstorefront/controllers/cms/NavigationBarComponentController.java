/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.acceleratorcms.model.components.NavigationBarComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.NavigationNodeHelper;
import au.com.target.tgtstorefront.navigation.megamenu.MegaMenuDepartment;


/**
 * 
 */
@Controller("NavigationBarComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.NAVIGATION_BAR_COMPONENT)
public class NavigationBarComponentController extends AbstractCMSComponentController<NavigationBarComponentModel>
{

    @Resource(name = "navigationNodeHelper")
    private NavigationNodeHelper navigationNodeHelper;

    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
            final NavigationBarComponentModel component)
    {
        if (component.getDropDownLayout() != null)
        {
            model.addAttribute("dropDownLayout", component.getDropDownLayout().getCode().toLowerCase());
        }

        final CMSNavigationNodeModel megaMenuNavNode = component.getNavigationNode();

        final List<MegaMenuDepartment> megaMenuDepartments = new ArrayList<>();

        if (megaMenuNavNode != null && megaMenuNavNode.isVisible()) {
            for (final CMSNavigationNodeModel child : megaMenuNavNode.getChildren()) {
                if (child.isVisible()) {
                    megaMenuDepartments.add(buildDepartment(child));
                }
            }
        }

        model.addAttribute("departments", megaMenuDepartments);
        model.addAttribute("departmentsSVGUrl", navigationNodeHelper.getSVGSpriteUrl(megaMenuNavNode));
    }

    protected MegaMenuDepartment buildDepartment(final CMSNavigationNodeModel departmentNavigationNode) {
        return navigationNodeHelper.buildDepartment(departmentNavigationNode, true);
    }

    protected String getNavigationNodeLink(final CMSNavigationNodeModel navigationNode) {
        return navigationNodeHelper.getNavigationNodeLink(navigationNode);
    }

    protected String getNavigationNodeTitle(final CMSNavigationNodeModel navigationNode, final boolean fallbackToName) {
        return navigationNodeHelper.getNavigationNodeTitle(navigationNode, fallbackToName);
    }
}