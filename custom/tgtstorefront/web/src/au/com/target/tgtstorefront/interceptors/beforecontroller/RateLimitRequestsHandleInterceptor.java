package au.com.target.tgtstorefront.interceptors.beforecontroller;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.Predicate;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;


/**
 * An interceptor which is used for rate controlling specified url patterns The method for which the rate control needs
 * to be applied can be injected. The feature switch corresponding to the method can also be injected. The url patterns
 * for the specific method can be injected.
 */
public class RateLimitRequestsHandleInterceptor extends HandlerInterceptorAdapter {

    private static final Logger LOG = Logger.getLogger(RateLimitRequestsHandleInterceptor.class);

    private static final String RATE_LIMIT_LOGGER = "RATE_LIMIT:";

    private static final String ACTIVE_REQUESTS = "actReqs";

    private int ratelimitCount = 5;

    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    private String featureSwitchKey;

    private List<String> rateLimitRequestPathPattern;

    private String rateLimitMethod;


    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler)
            throws Exception {
        final String requestUri = request.getRequestURI();
        final String requestMethod = request.getMethod();
        final String requestQueryString = request.getQueryString();

        if (!shouldRequestBeRateLimited(requestMethod, requestUri)) {
            return true;
        }
        return evaluateIncomingRequest(request, response,
                requestMethod, requestUri, requestQueryString);

    }

    private boolean evaluateIncomingRequest(final HttpServletRequest request, final HttpServletResponse response,
            final String requestMethod, final String requestUri, final String requestQueryString) throws IOException {
        final String requestKey = getActiveUrlKey(requestMethod, requestUri, requestQueryString);
        if (LOG.isDebugEnabled()) {
            LOG.debug(RATE_LIMIT_LOGGER + "Inside RateLimit Interceptor PreHandle for request" + requestKey
                    + " has session attribute" + request.getSession().getAttribute(ACTIVE_REQUESTS));
        }
        ConcurrentMap<String, Integer> activeUrlsMap = (ConcurrentMap<String, Integer>)request.getSession()
                .getAttribute(ACTIVE_REQUESTS);
        if (activeUrlsMap != null) {
            if (shouldRequestBeForbidden(activeUrlsMap, request, requestKey, response)) {
                return false;
            }
            request.getSession().setAttribute(ACTIVE_REQUESTS, activeUrlsMap);
        }
        else {
            if (LOG.isDebugEnabled()) {
                LOG.debug(RATE_LIMIT_LOGGER + "Map is null : Creating Map and Adding Url=" + requestKey + " count=1");
            }
            activeUrlsMap = new ConcurrentHashMap<>();
            activeUrlsMap.put(requestKey, Integer.valueOf(1));
            request.getSession().setAttribute(ACTIVE_REQUESTS, activeUrlsMap);
        }
        return true;
    }

    private boolean shouldRequestBeForbidden(final ConcurrentMap<String, Integer> activeUrlsMap,
            final HttpServletRequest request,
            final String requestKey, final HttpServletResponse response) throws IOException {
        if (MapUtils.isNotEmpty(activeUrlsMap) && activeUrlsMap.containsKey(requestKey)) {
            if (request.getParameter("_") != null && activeUrlsMap.containsKey(requestKey)) {
                return sendForbidError(response, "Same Timestamp", requestKey, activeUrlsMap);
            }
            int count = activeUrlsMap.get(requestKey).intValue();
            if (count >= ratelimitCount) {
                return sendForbidError(response, "Exceeded Limit", requestKey, activeUrlsMap);
            }
            count++;
            if (LOG.isDebugEnabled()) {
                LOG.debug(RATE_LIMIT_LOGGER + "Incrementing count for requestKey=" + requestKey + " count=" + count);
            }
            activeUrlsMap.put(requestKey, Integer.valueOf(count));
        }
        else {
            if (LOG.isDebugEnabled()) {
                LOG.debug(RATE_LIMIT_LOGGER + "Map is Empty : Adding requestKey=" + requestKey + " count=1");
            }
            activeUrlsMap.putIfAbsent(requestKey, Integer.valueOf(1));
        }
        return false;
    }

    private boolean sendForbidError(final HttpServletResponse response, final String reason, final String requestKey,
            final ConcurrentMap<String, Integer> activeUrlsMap) throws IOException {
        LOG.error(RATE_LIMIT_LOGGER + "Forbidding Request : reason=" + reason + " : requestKeyString="
                + requestKey + " activeUrlsMap=" + activeUrlsMap);
        response.setStatus(HttpStatus.SC_FORBIDDEN);
        response.sendError(HttpServletResponse.SC_FORBIDDEN);
        return true;
    }

    @Override
    public void afterCompletion(final HttpServletRequest request,
            final HttpServletResponse response,
            final Object handler,
            final Exception ex)
                    throws Exception {
        adjustUrlCountInSession(request);
    }


    private boolean shouldRequestBeRateLimited(final String httpMethod, final String requestPath) {
        return rateLimitMethod.equalsIgnoreCase(httpMethod)
                && targetFeatureSwitchFacade.isFeatureEnabled(featureSwitchKey)
                && isPathIncluded(requestPath, rateLimitRequestPathPattern);
    }

    private String getActiveUrlKey(final String requestMethod, final String requestUri,
            final String requestQueryString) {
        return requestMethod + "-" + requestUri +
                (requestQueryString != null ? "?" + requestQueryString : "");
    }


    private void adjustUrlCountInSession(final HttpServletRequest request) {
        final String requestUri = request.getRequestURI();
        final String requestMethod = request.getMethod();
        final String requestQueryString = request.getQueryString();

        if (!shouldRequestBeRateLimited(requestMethod, requestUri)) {
            return;
        }
        final String requestKey = getActiveUrlKey(requestMethod, requestUri, requestQueryString);

        final ConcurrentMap<String, Integer> activeUrls = (ConcurrentMap<String, Integer>)request.getSession()
                .getAttribute(ACTIVE_REQUESTS);
        if (LOG.isDebugEnabled()) {
            LOG.debug(RATE_LIMIT_LOGGER + "Inside RateLimit Interceptor Post Handle for request" + requestKey
                    + " has session attribute"
                    + activeUrls);
        }
        if (activeUrls == null || !activeUrls.containsKey(requestKey)) {
            return;
        }

        int counter = activeUrls.get(requestKey).intValue();
        counter--;
        if (counter <= 0) {
            activeUrls.remove(requestKey);
            if (LOG.isDebugEnabled()) {
                LOG.debug(RATE_LIMIT_LOGGER + "Removing requestKey=" + requestKey);
            }
        }
        else {
            activeUrls.put(requestKey, Integer.valueOf(counter));
            if (LOG.isDebugEnabled()) {
                LOG.debug(RATE_LIMIT_LOGGER + "Decrementing requestKey=" + requestKey + " Count=" + counter);
            }
        }
        request.getSession().setAttribute(ACTIVE_REQUESTS, activeUrls);

    }

    private static boolean isPathIncluded(final String requestPath, final List<String> includedPaths) {
        return CollectionUtils.exists(includedPaths, new Predicate() {
            @Override
            public boolean evaluate(final Object includedPath) {
                return requestPath.startsWith((String)includedPath);
            }
        });
    }


    /**
     * @param targetFeatureSwitchFacade
     *            the targetFeatureSwitchFacade to set
     */
    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }

    /**
     * @param ratelimitCount
     *            the ratelimitCount to set
     */
    @Required
    public void setRatelimitCount(final int ratelimitCount) {
        this.ratelimitCount = ratelimitCount;
    }


    /**
     * @param rateLimitRequestPathPattern
     *            the rateLimitRequestPathPattern to set
     */
    @Required
    public void setRateLimitRequestPathPattern(final List<String> rateLimitRequestPathPattern) {
        this.rateLimitRequestPathPattern = rateLimitRequestPathPattern;
    }

    /**
     * @param rateLimitMethod
     *            the rateLimitMethod to set
     */
    @Required
    public void setRateLimitMethod(final String rateLimitMethod) {
        this.rateLimitMethod = rateLimitMethod;
    }

    /**
     * @param featureSwitchKey
     *            the featureSwitchKey to set
     */
    @Required
    public void setFeatureSwitchKey(final String featureSwitchKey) {
        this.featureSwitchKey = featureSwitchKey;
    }

}
