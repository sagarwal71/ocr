/**
 * 
 */
package au.com.target.tgtstorefront.forms.checkout;


public class DeliveryModePostcodeForm {


    private String postcode;

    /**
     * @return the postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * @param postcode
     *            the postcode to set
     */
    public void setPostcode(final String postcode) {
        this.postcode = postcode;
    }

}
