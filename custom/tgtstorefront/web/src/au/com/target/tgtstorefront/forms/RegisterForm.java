/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.forms;

import au.com.target.tgtstorefront.forms.validation.Email;
import au.com.target.tgtstorefront.forms.validation.FirstName;
import au.com.target.tgtstorefront.forms.validation.LastName;
import au.com.target.tgtstorefront.forms.validation.MobilePhone;
import au.com.target.tgtstorefront.forms.validation.Password;
import au.com.target.tgtstorefront.forms.validation.Title;




/**
 * Form object for registration
 */
public class RegisterForm {

    private String uid;

    @Title
    private String titleCode;

    @FirstName
    private String firstName;

    @LastName
    private String lastName;

    @Email
    private String email;

    @Password
    private String pwd;

    @MobilePhone
    private String mobileNumber;

    private String subscriptionSource;

    private Boolean registerForMail;

    /**
     * @return the registerForMail
     */
    public Boolean getRegisterForMail() {
        return registerForMail;
    }

    /**
     * @param registerForMail
     *            the registerForMail to set
     */
    public void setRegisterForMail(final Boolean registerForMail) {
        this.registerForMail = registerForMail;
    }

    /**
     * @return the titleCode
     */
    public String getTitleCode() {
        return titleCode;
    }

    /**
     * @param titleCode
     *            the titleCode to set
     */
    public void setTitleCode(final String titleCode) {
        this.titleCode = titleCode;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * @return the pwd
     */
    public String getPwd() {
        return pwd;
    }

    /**
     * @param pwd
     *            the pwd to set
     */
    public void setPwd(final String pwd) {
        this.pwd = pwd;
    }

    /**
     * @return the mobileNumber
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * @param mobileNumber
     *            the mobileNumber to set
     */
    public void setMobileNumber(final String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * @return the uid
     */
    public String getUid() {
        return uid;
    }

    /**
     * @param uid
     *            the uid to set
     */
    public void setUid(final String uid) {
        this.uid = uid;
    }

    /**
     * @return the subscriptionSource
     */
    public String getSubscriptionSource() {
        return subscriptionSource;
    }

    /**
     * @param subscriptionSource
     *            the subscriptionSource to set
     */
    public void setSubscriptionSource(final String subscriptionSource) {
        this.subscriptionSource = subscriptionSource;
    }
}
