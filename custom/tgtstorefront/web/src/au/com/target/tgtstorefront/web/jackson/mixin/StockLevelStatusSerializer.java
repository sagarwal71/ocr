/**
 * 
 */
package au.com.target.tgtstorefront.web.jackson.mixin;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;



/**
 * @author mgazal
 *
 */
public class StockLevelStatusSerializer extends JsonSerializer<StockLevelStatus> {

    @Override
    public void serialize(final StockLevelStatus value, final JsonGenerator gen, final SerializerProvider serializers)
            throws IOException, JsonProcessingException {

        gen.writeString(value.getCode());
    }

}
