/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation;

import static java.lang.annotation.ElementType.FIELD;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtstorefront.forms.validation.validator.NameOnPaymentCardValidator;




@Documented
@Constraint(validatedBy = { NameOnPaymentCardValidator.class })
@Target({ FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface NameOnCard {
    String message() default StringUtils.EMPTY;

    Class<?>[] groups() default {};

    Class<?>[] payload() default {};
}