/**
 * 
 */
package au.com.target.tgtstorefront.interceptors.beforeview;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import au.com.target.tgtstorefront.controllers.ControllerConstants;



public class DisableBrowserCacheBeforeViewHandler implements BeforeViewHandler
{


    @Override
    public void beforeView(final HttpServletRequest request, final HttpServletResponse response,
            final ModelAndView modelAndView)
    {
        if (request != null && request.getServletPath().startsWith(ControllerConstants.CHECKOUT)) {
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            response.setDateHeader("Expires", -1);
        }
    }


}
