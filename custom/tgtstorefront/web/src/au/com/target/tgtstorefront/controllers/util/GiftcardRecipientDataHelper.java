/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import au.com.target.tgtfacades.order.data.GiftRecipientData;
import au.com.target.tgtstorefront.forms.GiftRecipientForm;


/**
 * @author smishra1
 *
 */
public class GiftcardRecipientDataHelper {

    public void populateRecipientDetailsForm(final GiftRecipientForm form, final GiftRecipientData recipientData) {
        form.setFirstName(recipientData.getFirstName());
        form.setLastName(recipientData.getLastName());
        form.setRecipientEmailAddress(recipientData.getRecipientEmailAddress());
        form.setMessageText(recipientData.getMessageText());
    }
}
