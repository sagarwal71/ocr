/**
 * 
 */
package au.com.target.tgtstorefront.navigation.megamenu;

/**
 * @author rmcalave
 * 
 */
public class MegaMenuEntry {
    private String text;

    private String linkUrl;

    private String imageUrl;

    private String imageAltText;

    private String style;

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text
     *            the text to set
     */
    public void setText(final String text) {
        this.text = text;
    }

    /**
     * @return the linkUrl
     */
    public String getLinkUrl() {
        return linkUrl;
    }

    /**
     * @param linkUrl
     *            the linkUrl to set
     */
    public void setLinkUrl(final String linkUrl) {
        this.linkUrl = linkUrl;
    }

    /**
     * @return the imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * @param imageUrl
     *            the imageUrl to set
     */
    public void setImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * @return the imageAltText
     */
    public String getImageAltText() {
        return imageAltText;
    }

    /**
     * @param imageAltText
     *            the imageAltText to set
     */
    public void setImageAltText(final String imageAltText) {
        this.imageAltText = imageAltText;
    }

    /**
     * @return the style
     */
    public String getStyle() {
        return style;
    }

    /**
     * @param style
     *            the style to set
     */
    public void setStyle(final String style) {
        this.style = style;
    }


}
