/**
 * 
 */
package au.com.target.tgtstorefront.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtstorefront.util.CSRFTokenManager;


/**
 * @author rmcalave
 * 
 */
@Deprecated
public class CSRFTokenTag extends SimpleTagSupport {
    private boolean plainToken = false;

    @Override
    public void doTag() throws JspException {
        final String token = CSRFTokenManager.getTokenForSession(((PageContext)getJspContext()).getSession());
        if (!StringUtils.isBlank(token)) {
            try {
                if (plainToken) {
                    getJspContext().getOut().write(token);
                }
                else {
                    getJspContext().getOut().write(
                            String.format("<input type=\"hidden\" name=\"%1$s\" id=\"%1$s\" value=\"%2$s\" />",
                                    CSRFTokenManager.CSRF_PARAM_NAME, token));
                }
            }
            catch (final IOException ioe) {
                throw new JspException(ioe);
            }
        }
    }

    /**
     * Flag to generate a plain text token, instead of a html hidden input tag. Can be used for AJAX calls.
     * 
     * @param plainToken
     */
    public void setPlainToken(final boolean plainToken) {
        this.plainToken = plainToken;
    }

    public static String getTokenParameterName() {
        return CSRFTokenManager.CSRF_PARAM_NAME;
    }
}
