/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CardTypeData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.user.UserService;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtfacades.cart.TargetOrderErrorHandlerFacade;
import au.com.target.tgtfacades.delivery.TargetDeliveryFacade;
import au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedeemConfigData;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.TargetDiscountFacade;
import au.com.target.tgtfacades.order.TargetOrderFacade;
import au.com.target.tgtfacades.order.TargetPlaceOrderFacade;
import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;
import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.order.enums.TargetPlaceOrderResultEnum;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.user.data.IpgPaymentInfoData;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtlayby.order.TargetLaybyCommerceCheckoutService;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.AbstractPageController;
import au.com.target.tgtstorefront.controllers.util.AddressDataHelper;
import au.com.target.tgtstorefront.controllers.util.CartDataHelper;
import au.com.target.tgtstorefront.controllers.util.CookieStringBuilder;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.controllers.util.RequestHeadersHelper;
import au.com.target.tgtstorefront.forms.validation.validator.PaymentDetailsValidator;
import au.com.target.tgtverifyaddr.TargetAddressVerificationService;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;





/**
 * Base controller for all page controllers. Provides common functionality for all page controllers.
 */
@SuppressWarnings("deprecation")
public abstract class AbstractCheckoutController extends AbstractPageController {

    protected static final Logger LOG = Logger.getLogger(AbstractCheckoutController.class);

    protected static final String DEFAULT_SOH_REDIRECT_URL = StringUtils.EMPTY;

    @Resource(name = "targetUserFacade")
    private UserFacade userFacade;

    @Resource(name = "targetCheckoutFacade")
    private TargetCheckoutFacade checkoutFacade;

    @Resource(name = "paymentDetailsValidator")
    private PaymentDetailsValidator paymentDetailsValidator;

    @Resource(name = "productFacade")
    private ProductFacade productFacade;

    @Resource(name = "targetCartService")
    private TargetLaybyCartService targetCartService;

    @Resource(name = "targetCommerceCartService")
    private TargetCommerceCartService targetCommerceCartService;

    @Resource(name = "targetCommerceCheckoutService")
    private TargetLaybyCommerceCheckoutService targetCommerceCheckoutService;

    @Resource(name = "targetPurchaseOptionHelper")
    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;

    @Resource(name = "targetStoreLocatorFacade")
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Resource(name = "targetOrderFacade")
    private TargetOrderFacade targetOrderFacade;

    @Resource(name = "userService")
    private UserService userService;

    @Resource(name = "targetDeliveryFacade")
    private TargetDeliveryFacade targetDeliveryFacade;

    @Resource(name = "targetAddressVerificationService")
    private TargetAddressVerificationService targetAddressVerificationService;

    @Resource(name = "checkoutSteps")
    private List<CheckoutSteps> checkoutSteps;

    @Resource(name = "targetDiscountFacade")
    private TargetDiscountFacade targetDiscountFacade;

    @Resource(name = "targetDeliveryModeHelper")
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Resource(name = "targetPlaceOrderFacade")
    private TargetPlaceOrderFacade targetPlaceOrderFacade;

    @Resource(name = "requestHeadersHelper")
    private RequestHeadersHelper requestHeadersHelper;

    @Resource(name = "addressDataHelper")
    private AddressDataHelper addressDataHelper;

    @Resource(name = "cookieStringBuilder")
    private CookieStringBuilder cookieStringBuilder;

    @Resource(name = "salesApplicationFacade")
    private SalesApplicationFacade salesApplicationFacade;

    @Resource(name = "flybuysDiscountFacade")
    private FlybuysDiscountFacade flybuysDiscountFacade;

    @Resource(name = "priceDataFactory")
    private PriceDataFactory priceDataFactory;


    @Resource(name = "targetOrderErrorHandlerFacade")
    private TargetOrderErrorHandlerFacade targetOrderErrorHandlerFacade;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;


    /**
     *
     * @return boolean
     */
    public boolean isAnonymousUser() {
        return userService.isAnonymousUser(userService.getCurrentUser());
    }

    /**
     * 
     * @return userFacade.getTitles()
     */
    @ModelAttribute("titles")
    public Collection<TitleData> getTitles() {
        return getUserFacade().getTitles();
    }

    /**
     * 
     * @return getCheckoutFacade().getDeliveryCountries()
     */
    @ModelAttribute("countries")
    public Collection<CountryData> getCountries() {
        return getCheckoutFacade().getDeliveryCountries();
    }

    /**
     * 
     * @return getCheckoutFacade().getBillingCountries()
     */
    @ModelAttribute("billingCountries")
    public Collection<CountryData> getBillingCountries() {
        return getCheckoutFacade().getBillingCountries();
    }

    /**
     * 
     * @return getCheckoutFacade().getSupportedCardTypes()
     */
    @ModelAttribute("cardTypes")
    public Collection<CardTypeData> getCardTypes() {

        return getCheckoutFacade().getSupportedCardTypes();
    }

    @ModelAttribute("threatMatrixSessionID")
    public String getThreatMatrixSessionID() {
        return getCheckoutFacade().getThreatMatrixSessionID();
    }

    @ModelAttribute("pageType")
    public Enum getCheckoutPageType() {
        return PageType.Checkout;
    }

    @ModelAttribute("isAnonymousCheckout")
    public boolean isAnonymousCheckout() {
        return isAnonymousUser();
    }

    /**
     * Checks if there are any items in the cart.
     * 
     * @return returns true if items found in cart.
     */
    protected boolean hasItemsInCart(final TargetCartData cartData) {

        return (CollectionUtils.isNotEmpty(cartData.getEntries()));
    }


    /**
     * 
     * @param model
     *            the spring mvc model
     * @param redirectAttributes
     *            the spring redirect attributes
     * @param redirectUrl
     *            the URL you want to redirect to if it triggers the SOH update
     * @return redirection String when necessary
     */
    protected String getRedirectionAndCheckSOH(final Model model, final RedirectAttributes redirectAttributes,
            final String redirectUrl, final TargetCartData cartData) {

        if (hasNoActivePurchaseOption() || !hasItemsInCart(cartData)) {
            return ControllerConstants.Redirection.CART;
        }
        if (cartData.isInsufficientAmount()) {
            return ControllerConstants.Redirection.CART + CartDataHelper.TYPE + cartData.getPurchaseOptionCode();
        }

        // Remove missing products (eg ones that have become unapproved)
        final boolean removedMissingProducts = targetCommerceCartService.removeMissingProducts(targetCartService
                .getSessionCart());
        if (removedMissingProducts) {
            LOG.info("Removed missing products from cart! " + targetCartService.getSessionCart().getCode());
            GlobalMessages.addFlashInfoMessage(redirectAttributes, "checkout.information.missing.products.removed");
            return ControllerConstants.Redirection.CART + CartDataHelper.TYPE + cartData.getPurchaseOptionCode();
        }

        return getCartModification(model, redirectAttributes, redirectUrl, cartData);
    }

    /**
     * @param model
     * @param redirectAttributes
     * @param redirectUrl
     * @param cartData
     */
    private String getCartModification(final Model model, final RedirectAttributes redirectAttributes,
            final String redirectUrl, final TargetCartData cartData) {
        final AdjustedCartEntriesData adjustedCartEntriesData = getCheckoutFacade().adjustCart();
        if (adjustedCartEntriesData != null && adjustedCartEntriesData.isAdjusted()) {
            redirectAttributes.addFlashAttribute(ControllerConstants.SOH_UPDATES,
                    Collections.singleton(adjustedCartEntriesData));
            final TargetCartData updatedCartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
            if (!updatedCartData.hasEntries() || updatedCartData.isInsufficientAmount()) {
                return ControllerConstants.Redirection.CART;
            }
            model.addAttribute(ControllerConstants.SOH_UPDATES, Collections.singleton(adjustedCartEntriesData));
            cartData.setDataStale(true);
            return StringUtils.isBlank(redirectUrl) ? StringUtils.EMPTY : redirectUrl;
        }
        return StringUtils.EMPTY;
    }

    /**
     * Gets the redirection if flybuys reassessed.
     * 
     * @param redirectAttributes
     *            the redirect attributes
     * @return the redirection if flybuys reassessed
     */
    protected String getRedirectionIfFlybuysReassessed(final RedirectAttributes redirectAttributes) {
        if (getFlybuysDiscountFacade().reassessFlybuysDiscountOnCheckoutCart()) {
            final FlybuysRedeemConfigData flybuysRedeemConfigData = flybuysDiscountFacade
                    .getFlybuysRedeemConfigData();

            final BigDecimal minCartValueAmount = BigDecimal.valueOf(flybuysRedeemConfigData
                    .getMinCartValue().doubleValue());

            final TargetCartData cart = (TargetCartData)getCheckoutFacade().getCheckoutCart();
            final PriceData cartSubtotal = cart.getSubTotal();

            if (cartSubtotal.getValue().compareTo(minCartValueAmount) < 0) {
                final PriceData minCartValuePriceData = priceDataFactory.create(PriceDataType.BUY, minCartValueAmount,
                        cartSubtotal.getCurrencyIso());

                GlobalMessages.addFlashInfoMessage(redirectAttributes, "basket.information.flybuys.discount.removed",
                        new Object[] { minCartValuePriceData.getFormattedValue() });
            }
            else {
                GlobalMessages.addFlashInfoMessage(redirectAttributes, "checkout.error.flybuys.removed");
            }

            return ControllerConstants.Redirection.CHECKOUT_PAYMENT;
        }

        return StringUtils.EMPTY;
    }


    public String validateTMD(final Model model,
            final String tmdCardNumber) throws CMSItemNotFoundException {
        boolean isTMDCardValid = targetDiscountFacade.validateTeamMemberDiscountCard(tmdCardNumber);

        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            if (isTMDCardValid) {
                isTMDCardValid = getCheckoutFacade().setTeamMemberDiscountCardNumber(tmdCardNumber);
                if (flybuysDiscountFacade.reassessFlybuysDiscountOnCheckoutCart()) {
                    GlobalMessages.addInfoMessage(model, "validation.flybuys.warning");
                }

            }

            final TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
            model.addAttribute("cartData", cartData);
        }

        model.addAttribute("isTMDCardValid", Boolean.valueOf(isTMDCardValid));

        return ControllerConstants.Views.Pages.MultiStepCheckout.VALIDATE_TMD_PAGE;
    }


    public String getRedirectionIfNecessary() {
        return StringUtils.EMPTY;
    }

    /**
     * apply the Team Member Discount if it is not already applied. Will also asses the flybuys discount to see if it is
     * still valid given the new cart subtotal.
     * 
     * @param redirectAttributes
     * @param cartData
     * @param tmdCardNumber
     * @return redirection String when necessary, else null
     */
    protected String applyTMDIfItNotApplied(final RedirectAttributes redirectAttributes, final TargetCartData cartData,
            final String tmdCardNumber) {
        if (StringUtils.isNotBlank(tmdCardNumber)
                && StringUtils.isBlank(cartData.getTmdNumber())) {
            getCheckoutFacade().setTeamMemberDiscountCardNumber(tmdCardNumber);
            if (flybuysDiscountFacade.reassessFlybuysDiscountOnCheckoutCart()) {
                getCheckoutFacade().setTeamMemberDiscountCardNumber(tmdCardNumber);
                GlobalMessages.addFlashErrorMessage(redirectAttributes, "validation.flybuys.warning");
                return ControllerConstants.Redirection.CHECKOUT_PAYMENT;
            }
        }
        return null;
    }

    /**
     * Checks if a product with physical delivery modes is added to the cart.
     * 
     * @return true if a product with physical delivery modes is added to the cart
     */
    protected boolean isCartUpdatedWithPhysicalProducts() {
        final CartModel cart = targetCartService.getSessionCart();
        Assert.notNull(cart, "Cart cannot be null!");
        final List<AbstractOrderEntryModel> entries = cart.getEntries();
        if (CollectionUtils.isNotEmpty(entries)) {
            for (final AbstractOrderEntryModel entry : entries) {
                if (entry.getProduct() instanceof AbstractTargetVariantProductModel) {
                    final AbstractTargetVariantProductModel product = (AbstractTargetVariantProductModel)entry
                            .getProduct();
                    if (targetDeliveryModeHelper.doesProductHavePhysicalDeliveryMode(product)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Handle case of payment succeeded but something else went wrong with place order
     * 
     * @param placeOrderResult
     * @param cartCode
     * @return redirect
     */
    protected String handleOrderCreationFailed(final TargetPlaceOrderResultEnum placeOrderResult,
            final String cartCode) {

        this.targetOrderErrorHandlerFacade.handleOrderCreationFailed(placeOrderResult, cartCode);

        return ControllerConstants.Redirection.CHECKOUT_THANK_YOU_CHECK + "?cart=" + cartCode;

    }

    public PageableData setPageableDataForCncStore() {
        final PageableData pageableData = new PageableData();
        pageableData.setCurrentPage(0);
        pageableData.setPageSize(getConfigurationService().getConfiguration().getInt(
                ControllerConstants.CNC_STORE_NUMBER_PAGESIZE));
        return pageableData;
    }

    protected boolean hasNoActivePurchaseOption() {
        return getCheckoutFacade().getActivePurchaseOption() == null;
    }

    /**
     * 
     * @param cartData
     * @return true if there is no delivery address set in cart else false
     */
    protected boolean hasNoDeliveryAddress(final CartData cartData) {

        return null == cartData.getDeliveryAddress();
    }

    /**
     * 
     * @param cartData
     * @return true if there is no payment info set in cart else false
     */
    public boolean hasNoPaymentInfo(final CartData cartData) {
        return null == cartData.getPaymentInfo();
    }


    /**
     * Checks whether payment is associated with IPG
     * 
     * @param paymentInfo
     * @return is IPG payment
     */
    protected boolean isIpgPayment(final CCPaymentInfoData paymentInfo) {
        boolean isIpg = false;
        if (paymentInfo instanceof TargetCCPaymentInfoData) {
            isIpg = ((TargetCCPaymentInfoData)paymentInfo).isIpgPaymentInfo();
        }
        return isIpg;
    }

    /**
     * Reverse gift card payment if there are any existing
     * 
     * @param cartData
     *            - cart
     */
    protected void reverseExistingGiftCardPayments(final TargetCartData cartData) {
        final String sessionToken = getCheckoutFacade().getIpgSessionTokenFromCart(cartData);

        if (StringUtils.isNotEmpty(sessionToken)) {
            getTargetPlaceOrderFacade().reverseGiftCardPayment();
        }
    }

    /**
     * Determine whether the provided <code>CCPaymentInfoData</code> is an IPG Credit Card payment.
     * 
     * @param paymentInfo
     *            the <code>CCPaymentInfoData</code> to check
     * @return true if it's a credit card payment, false otherwise
     */
    protected boolean isIpgCreditCardPayment(final CCPaymentInfoData paymentInfo) {
        final IpgPaymentInfoData ipgPaymentInfoData = getIpgPaymentInfoData(paymentInfo);

        return ipgPaymentInfoData != null
                && IpgPaymentTemplateType.CREDITCARDSINGLE.equals(ipgPaymentInfoData.getIpgPaymentTemplateType());
    }

    /**
     * Determine whether the provided <code>CCPaymentInfoData</code> is an IPG Gift Card payment.
     * 
     * @param paymentInfo
     *            the <code>CCPaymentInfoData</code> to check
     * @return true if it's a gift card payment, false otherwise
     */
    protected boolean isIpgGiftCardPayment(final CCPaymentInfoData paymentInfo) {
        final IpgPaymentInfoData ipgPaymentInfoData = getIpgPaymentInfoData(paymentInfo);

        return ipgPaymentInfoData != null
                && IpgPaymentTemplateType.GIFTCARDMULTIPLE.equals(ipgPaymentInfoData.getIpgPaymentTemplateType());
    }

    public String getDomainUrl(final HttpServletRequest request) {
        return request.getRequestURL().toString().replace(request.getRequestURI(), request.getContextPath());
    }

    private IpgPaymentInfoData getIpgPaymentInfoData(final CCPaymentInfoData paymentInfo) {
        if (paymentInfo instanceof TargetCCPaymentInfoData) {
            final TargetCCPaymentInfoData targetCcPaymentInfo = (TargetCCPaymentInfoData)paymentInfo;

            if (targetCcPaymentInfo.isIpgPaymentInfo()) {
                return targetCcPaymentInfo.getIpgPaymentInfoData();
            }
        }

        return null;
    }

    /**
     * 
     * @param request
     * @return List<CheckoutSteps>
     */
    @ModelAttribute("checkoutSteps")
    protected List<CheckoutSteps> addCheckoutStepsToModel(final HttpServletRequest request) {
        return getCheckoutSteps();
    }

    /**
     * 
     * @return List<SelectOption>
     */
    @ModelAttribute("months")
    public List<SelectOption> getMonths() {
        return WebConstants.Payment.MONTHS;
    }

    /**
     * 
     * @return List<SelectOption>
     */
    @ModelAttribute("startYears")
    public List<SelectOption> getStartYears() {
        return WebConstants.Payment.START_YEARS;
    }

    /**
     * 
     * @return List<SelectOption>
     */
    @ModelAttribute("expiryYears")
    public List<SelectOption> getExpiryYears() {
        return WebConstants.Payment.EXPIRY_YEARS;
    }

    /**
     * 
     * @return List<SelectOption>
     */
    @ModelAttribute("states")
    public List<SelectOption> getStates() {
        return addressDataHelper.getStates();
    }

    @ModelAttribute("daysOfMonth")
    public List<SelectOption> getDaysOfMonth() {
        return WebConstants.Payment.DAYS_OF_MONTH;
    }

    /**
     * Data class used to hold a drop down select option value. Holds the code identifier as well as the display name.
     */
    public static class SelectOption implements Comparable<SelectOption> {
        private final String code;
        private final String name;

        public SelectOption(final String code, final String name) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

        @Override
        public int compareTo(final SelectOption s) {
            return this.name.compareTo(s.getName());
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((code == null) ? 0 : code.hashCode());
            result = prime * result + ((name == null) ? 0 : name.hashCode());
            return result;
        }

        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final SelectOption other = (SelectOption)obj;
            if (code == null) {
                if (other.code != null) {
                    return false;
                }
            }
            else if (!code.equals(other.code)) {
                return false;
            }
            if (name == null) {
                if (other.name != null) {
                    return false;
                }
            }
            else if (!name.equals(other.name)) {
                return false;
            }
            return true;
        }

    }

    public static class CheckoutSteps {
        private final String stepName;
        private final String url;

        public CheckoutSteps(final String stepName, final String url) {
            this.stepName = stepName;
            this.url = url;
        }

        /**
         * @return the stepName
         */
        public String getStepName() {
            return stepName;
        }

        /**
         * @return the URL
         */
        public String getUrl() {
            return url;
        }
    }

    /**
     * @return the userFacade
     */
    protected UserFacade getUserFacade() {
        return userFacade;
    }

    /**
     * @return the checkoutFacade
     */
    protected TargetCheckoutFacade getCheckoutFacade() {
        return checkoutFacade;
    }

    /**
     * @return the paymentDetailsValidator
     */
    protected PaymentDetailsValidator getPaymentDetailsValidator() {
        return paymentDetailsValidator;
    }

    /**
     * @return the productFacade
     */
    protected ProductFacade getProductFacade() {
        return productFacade;
    }


    /**
     * @return the targetCartService
     */
    protected TargetLaybyCartService getTargetCartService() {
        return targetCartService;
    }

    /**
     * @return the targetStoreLocatorFacade
     */
    protected TargetStoreLocatorFacade getTargetStoreLocatorFacade() {
        return targetStoreLocatorFacade;
    }

    /**
     * @return the orderFacade
     */
    protected TargetOrderFacade getOrderFacade() {
        return targetOrderFacade;
    }

    /**
     * @return the targetAddressVerificationService
     */
    protected TargetAddressVerificationService getTargetAddressVerificationService() {
        return targetAddressVerificationService;
    }

    /**
     * @return the checkoutSteps
     */
    protected List<CheckoutSteps> getCheckoutSteps() {
        return checkoutSteps;
    }

    /**
     * @return the targetDeliveryModeHelper
     */
    protected TargetDeliveryModeHelper getTargetDeliveryModeHelper() {
        return targetDeliveryModeHelper;
    }

    /**
     * @return the targetPlaceOrderFacade
     */
    protected TargetPlaceOrderFacade getTargetPlaceOrderFacade() {
        return targetPlaceOrderFacade;
    }

    /**
     * @return the requestHeadersHelper
     */
    protected RequestHeadersHelper getRequestHeadersHelper() {
        return requestHeadersHelper;
    }

    /**
     * @return the targetDeliveryFacade
     */
    protected TargetDeliveryFacade getTargetDeliveryFacade() {
        return targetDeliveryFacade;
    }

    /**
     * @return the cookieStringBuilder
     */
    public CookieStringBuilder getCookieStringBuilder() {
        return cookieStringBuilder;
    }


    /**
     * @return the configurationService
     */
    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    /**
     * @return the flybuysDiscountFacade
     */
    protected FlybuysDiscountFacade getFlybuysDiscountFacade() {
        return flybuysDiscountFacade;
    }

    /**
     * @param targetOrderErrorHandlerFacade
     *            the targetOrderErrorHandlerFacade to set
     */
    public void setTargetOrderErrorHandlerFacade(final TargetOrderErrorHandlerFacade targetOrderErrorHandlerFacade) {
        this.targetOrderErrorHandlerFacade = targetOrderErrorHandlerFacade;
    }

}
