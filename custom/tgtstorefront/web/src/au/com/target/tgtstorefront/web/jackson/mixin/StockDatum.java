/**
 * 
 */
package au.com.target.tgtstorefront.web.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * @author mgazal
 *
 */
public abstract class StockDatum {

    @JsonIgnore
    abstract Integer getAtsHdQty();

    @JsonIgnore
    abstract Integer getAtsCcQty();

    @JsonIgnore
    abstract Integer getAtsEdQty();

    @JsonIgnore
    abstract Integer getConsolidatedStoreStockQty();

    @JsonIgnore
    abstract Integer getStoreSohQty();

    @JsonIgnore
    abstract Integer getAtsPoQty();
}
