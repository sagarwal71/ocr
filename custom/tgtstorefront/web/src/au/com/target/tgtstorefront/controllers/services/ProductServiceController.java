/**
 * 
 */
package au.com.target.tgtstorefront.controllers.services;

import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.transform.TargetEndecaURLHelper;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.Error;
import au.com.target.tgtfacades.response.data.ProductSearchResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.search.data.TargetProductCategorySearchPageData;
import au.com.target.tgtstorefront.controllers.kiosk.util.BulkyBoardHelper;
import au.com.target.tgtstorefront.controllers.pages.data.SeachEvaluatorData;
import au.com.target.tgtstorefront.controllers.util.TargetEndecaSearchHelper;
import au.com.target.tgtstorefront.util.CommonUtils;


/**
 * @author bhuang3
 *
 */
@Controller
@RequestMapping(value = "/ws-api/v1/{baseSiteId}/products")
public class ProductServiceController {

    protected static final Logger LOG = Logger.getLogger(ProductServiceController.class);

    private static final String ERROR_EXCEPTION = "ProductServiceController : Exception with product search ";

    private static final String INVALID_QUERY_PARAM = "Invalid query parameters";

    @Resource(name = "targetEndecaSearchHelper")
    private TargetEndecaSearchHelper targetEndecaSearchHelper;

    @Resource(name = "targetEndecaURLHelper")
    private TargetEndecaURLHelper targetEndecaURLHelper;

    @Resource(name = "productSearchResponseDataConverter")
    private Converter<TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData>, ProductSearchResponseData> productSearchResponseDataConverter;

    @Resource(name = "bulkyBoardHelper")
    private BulkyBoardHelper bulkyBoardHelper;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @ResponseBody
    public Response getProductListingPageData(
            @RequestParam(value = "category", required = false) final String categoryCode,
            @RequestParam(value = "brand", required = false) final String brandCode,
            @RequestParam(value = "text", required = false) final String searchText,
            @RequestParam(value = "storeNumber", required = false) final String storeNumber,
            @RequestParam(value = "skipRedirect", required = false, defaultValue = "false") final String skipRedirect,
            @RequestParam(value = "page", defaultValue = "0") final String pageNoString,
            @RequestParam(value = "Ns", required = false) final String sortCode,
            @RequestParam(value = "viewAs", required = false) final String viewAs,
            @RequestParam(value = "Nrpp", defaultValue = "0") final String pageSize,
            @RequestParam(value = "N", required = false) final String navigationState,
            final HttpServletRequest request, final HttpServletResponse response) {

        if (!this.availableSearch(searchText, categoryCode, brandCode, storeNumber)) {
            return createFailedResponseData(INVALID_QUERY_PARAM, null);
        }
        try {
            //search text  
            List<String> clearSearchTexts = null;
            if (StringUtils.isNotEmpty(searchText)) {
                clearSearchTexts = new ArrayList<>(1);
                clearSearchTexts.add(targetEndecaSearchHelper.sanitizeSearchText(searchText, null));
            }
            //page no
            final int pageNo = CommonUtils.convertToPositiveInt(pageNoString);

            final SeachEvaluatorData seachEvaluatorData = targetEndecaSearchHelper.handlePreferencesInitialData(
                    request,
                    response, categoryCode, navigationState, sortCode, viewAs, pageSize, searchText);

            final EndecaSearchStateData searchStateData = targetEndecaURLHelper.buildEndecaSearchStateData(
                    navigationState, clearSearchTexts, seachEvaluatorData.getSortCodeFn(),
                    seachEvaluatorData.getItemPerPageFn(), pageNo, seachEvaluatorData.getViewAsFn(),
                    request.getRequestURI(),
                    categoryCode,
                    brandCode);
            searchStateData.setSkipRedirect(BooleanUtils.toBoolean(skipRedirect));
            searchStateData.setCurrentQueryParam(targetEndecaURLHelper.getWsUrlQueryParameters(searchStateData));
            searchStateData.setStoreNumber(storeNumber);
            final EndecaSearch endecaSearchResults = targetEndecaSearchHelper.getEndecaSearchResult(request,
                    searchStateData);
            if (endecaSearchResults == null) {
                //Empty result
                return new Response(true);
            }
            final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData = new TargetProductCategorySearchPageData<>();
            targetEndecaSearchHelper.populateSearchPageData(searchPageData, endecaSearchResults, searchStateData);
            this.popualteBulkyBoardNubmer(endecaSearchResults, searchPageData);
            final ProductSearchResponseData productSearchResponseData = productSearchResponseDataConverter
                    .convert(searchPageData);
            final Response returnResponse = new Response(true);
            returnResponse.setData(productSearchResponseData);
            return returnResponse;
        }
        catch (final Exception e) {
            LOG.error(ERROR_EXCEPTION, e);
            return createFailedResponseData("ERR_UNKNOWN", null);
        }

    }

    private boolean availableSearch(final String searchText, final String categoryCode, final String brandCode,
            final String storeNumber) {
        return StringUtils.isNotEmpty(searchText) || StringUtils.isNotEmpty(categoryCode)
                || StringUtils.isNotEmpty(brandCode) || StringUtils.isNotEmpty(storeNumber);
    }

    private void popualteBulkyBoardNubmer(final EndecaSearch endecaSearchResults,
            final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData) {
        final Integer bbNumber = bulkyBoardHelper.getBulkyBoardNumber(endecaSearchResults);
        searchPageData.setBulkyBoardNumber(bbNumber);
    }

    private Response createFailedResponseData(final String errorCode, final String errorMessage) {
        final Response response = new Response(false);
        final BaseResponseData baseResponseData = new BaseResponseData();
        final Error error = new Error(errorCode);
        error.setMessage(errorMessage);
        baseResponseData.setError(error);
        response.setData(baseResponseData);
        return response;
    }

}
