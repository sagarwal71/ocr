/**
 * 
 */
package au.com.target.tgtstorefront.forms.checkout;

import au.com.target.tgtstorefront.forms.validation.ExistingCardId;


/**
 * @author gbaker2
 * 
 */
public class ExistingCardPaymentForm extends AbstractPaymentDetailsForm {
    @ExistingCardId
    private String cardId;

    // Security Code not in use as of 15/07/13
    private String securityNumber;



    /**
     * Populate super constructor
     */
    public ExistingCardPaymentForm() {
        super();

    }

    /**
     * Populate form action
     */
    public ExistingCardPaymentForm(final String action) {
        setAction(action);
    }

    /**
     * @return the cardId
     */
    public String getCardId() {
        return cardId;
    }

    /**
     * @param cardId
     *            the cardId to set
     */
    public void setCardId(final String cardId) {
        this.cardId = cardId;
    }

    /**
     * @return the securityNumber
     */
    public String getSecurityNumber() {
        return securityNumber;
    }

    /**
     * @param securityNumber
     *            the securityNumber to set
     */
    public void setSecurityNumber(final String securityNumber) {
        this.securityNumber = securityNumber;
    }



}
