/**
 * 
 */
package au.com.target.tgtstorefront.data;

import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.product.data.VariantOptionQualifierData;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class TargetProductDetailColourData extends TargetProductGenericData {
    private String swatchUrl;
    private String swatch;
    private String colourImageUrl;

    public TargetProductDetailColourData(final VariantOptionData variantOptionData) {
        super(variantOptionData);
        setUrl(variantOptionData.getUrl());
        for (final VariantOptionQualifierData variantOptionQualifierData : variantOptionData
                .getVariantOptionQualifiers()) {
            if ("swatch".equalsIgnoreCase(variantOptionQualifierData.getQualifier())) {
                this.swatch = variantOptionQualifierData.getValue();
                if (variantOptionQualifierData.getImage() != null) {
                    swatchUrl = variantOptionQualifierData.getImage().getUrl();
                }
            }
            else if ("colourName".equalsIgnoreCase(variantOptionQualifierData.getQualifier())) {
                setName(variantOptionQualifierData.getValue());
            }
            else if ("colour".equalsIgnoreCase(
                    variantOptionQualifierData.getQualifier()) && variantOptionQualifierData.getImage() != null) {
                colourImageUrl = variantOptionQualifierData.getImage().getUrl();
            }
        }
    }

    /**
     * @return the swatchUrl
     */
    public String getSwatchUrl() {
        return swatchUrl;
    }

    /**
     * @param swatchUrl
     *            the swatchUrl to set
     */
    public void setSwatchUrl(final String swatchUrl) {
        this.swatchUrl = swatchUrl;
    }

    /**
     * @return the swatch
     */
    public String getSwatch() {
        return swatch;
    }

    /**
     * @param swatch
     *            the swatch to set
     */
    public void setSwatch(final String swatch) {
        this.swatch = swatch;
    }

    /**
     * @return the colourImageUrl - current image format is grid
     */
    public String getColourImageUrl() {
        return colourImageUrl;
    }

    /**
     * Set url of the grid image
     * 
     * @param colourImageUrl
     *            the colourImageUrl
     */
    public void setColourImageUrl(final String colourImageUrl) {
        this.colourImageUrl = colourImageUrl;
    }


}
