package au.com.target.tgtstorefront.data.sitemap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Represents a site map document.
 */
@XmlRootElement(name = "urlset")
@XmlAccessorType(XmlAccessType.FIELD)
public class SiteMapData {

    @XmlElement(name = "url", required = true)
    private List<SiteMapUrlData> urls;

    /**
     * Returns the list of URLs for this site map.
     *
     * @return the list of URLs
     */
    public List<SiteMapUrlData> getUrls() {
        return urls;
    }

    /**
     * Sets the list of URLs for this site map.
     *
     * @param urls the list of URLs to set
     */
    public void setUrls(final List<SiteMapUrlData> urls) {
        this.urls = urls;
    }
}
