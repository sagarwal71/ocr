/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import java.util.regex.Matcher;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtstorefront.forms.validation.Phone;
import au.com.target.tgtutility.util.PhoneValidationUtils;
import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class PhoneValidator extends AbstractTargetValidator implements ConstraintValidator<Phone, String> {

    @Override
    public void initialize(final Phone arg0) {
        field = FieldTypeEnum.phone;
        sizeRange = new int[] { TargetValidationCommon.Phone.MIN_SIZE,
                TargetValidationCommon.Phone.MAX_SIZE };
        isMandatory = true;
        isSizeRange = true;
        mustMatch = true;
        pattern = TargetValidationCommon.Phone.PATTERN;
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        final String preparedValue = PhoneValidationUtils.preparePhoneNumber(value);
        return isValidCommon(preparedValue, context);
    }

    @Override
    protected boolean isSizeOutOfRange(final String value) {
        final Matcher replace = TargetValidationCommon.Phone.CLEAN_REGEX.matcher(value);
        final String cleanValue = replace.replaceAll(StringUtils.EMPTY);
        return super.isSizeOutOfRange(cleanValue);
    }

    @Override
    protected String getInvalidPatternMessage() {
        return getInvalidSizeRangeSpecCaraSlash(field, sizeRange[0], sizeRange[1]);
    }

    @Override
    protected String getInvalidSizeRangeSpecCaraSlash(final FieldTypeEnum fieldType, final int min, final int max) {
        return messageSource.getMessage(
                INVALID_PREFIX.concat(ErrorTypeEnum.sizeRangeSpecCaraPhone.toString()),
                new Object[] { getFieldString(fieldType), String.valueOf(min), String.valueOf(max),
                        getFieldString(fieldType).toLowerCase() },
                i18nService.getCurrentLocale());
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtstorefront.forms.validation.validator.AbstractTargetValidator#getInvalidSizeRange(au.com.target.tgtstorefront.forms.validation.validator.FieldTypeEnum, int, int)
     */
    @Override
    protected String getInvalidSizeRange(final FieldTypeEnum fieldType, final int min, final int max) {
        return messageSource.getMessage(
                INVALID_PREFIX.concat(ErrorTypeEnum.sizeRangeDigits.toString()),
                new Object[] { getFieldString(fieldType), String.valueOf(min), String.valueOf(max) },
                i18nService.getCurrentLocale());
    }
}
