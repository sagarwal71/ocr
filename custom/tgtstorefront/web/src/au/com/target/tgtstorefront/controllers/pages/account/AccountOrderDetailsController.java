/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.account;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.order.data.TargetOrderHistoryData;
import au.com.target.tgtfacades.order.exception.TargetUpdateCardException;
import au.com.target.tgtstorefront.annotations.RequireHardLogIn;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.data.BreadcrumbData;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.forms.MakePaymentForm;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(value = ControllerConstants.MY_ACCOUNT)
@RequireHardLogIn
public class AccountOrderDetailsController extends AbstractAccountController {

    private static final Logger LOG = Logger.getLogger(AccountOrderDetailsController.class);

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_ORDERS, method = RequestMethod.GET)
    public String orders(@RequestParam(value = "page", defaultValue = "0") final int page,
            @RequestParam(value = "sort", required = false) final String sortCode, final Model model)
                    throws CMSItemNotFoundException {
        // Handle paged search results
        final PageableData pageableData = createPageableData(page, PageSize.getDefault().getValue(), sortCode);
        final SearchPageData<TargetOrderHistoryData> searchPageData = (SearchPageData<TargetOrderHistoryData>)(SearchPageData<?>)targetOrderFacade
                .getPagedOrderHistoryForStatuses(pageableData);
        populateModel(model, searchPageData);

        storeCmsPageInModel(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.MY_ACCOUNT_ORDER_HISTORY));
        setUpMetaDataForContentPage(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.MY_ACCOUNT_ORDER_HISTORY));
        model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.myOrderDetails"));
        model.addAttribute("metaRobots", "no-index,no-follow");
        return ControllerConstants.Views.Pages.Account.ACCOUNT_ORDER_HISTORY_PAGE;
    }

    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_ORDER
            + ControllerConstants.MY_ACCOUNT_ORDER_CODE, method = RequestMethod.GET)
    public String order(@PathVariable("orderCode") final String orderCode, final Model model,
            final RedirectAttributes redirectModel)
                    throws CMSItemNotFoundException {
        try {
            final TargetOrderData targetOrderData = targetOrderFacade
                    .getOrderDetailsForCodeUnderCurrentCustomer(orderCode);
            model.addAttribute("orderData", targetOrderData);

            insertDataForCNCStoreNumber(model, targetOrderData.getCncStoreNumber());

            final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(
                    new BreadcrumbData("text.account.myOrderDetails", null, targetUrlsMapping.getMyAccountOrders()),
                    new BreadcrumbData("text.account.order.orderBreadcrumb",
                            new Object[] { targetOrderData.getCode() }, null));

            model.addAttribute("breadcrumbs", breadcrumbs);

        }
        catch (final UnknownIdentifierException e) {
            LOG.warn("Attempted to load a order that does not exist or is not visible", e);
            GlobalMessages.addFlashErrorMessage(redirectModel, "order.details.not.found.error");
            return ControllerConstants.Redirection.MY_ACCOUNT_ORDERS;
        }

        storeCmsPageInModel(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.MY_ACCOUNT_ORDER_DETAIL));
        model.addAttribute("metaRobots", "no-index,no-follow");
        setUpMetaDataForContentPage(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.MY_ACCOUNT_ORDER_DETAIL));
        storeAnalyticsPageUrl(model,
                ControllerConstants.MY_ACCOUNT.concat(ControllerConstants.MY_ACCOUNT_ORDER));
        return ControllerConstants.Views.Pages.Account.ACCOUNT_ORDER_PAGE;
    }

    /**
     * Method to display the update card IPG iFrame.
     * 
     * @param orderCode
     * @param model
     * @param request
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_ORDER
            + ControllerConstants.MY_ACCOUNT_ORDER_CODE
            + ControllerConstants.MY_ACCOUNT_ORDER_UPDATE_CARD, method = RequestMethod.GET)
    public String updateCard(@PathVariable("orderCode") final String orderCode, final Model model,
            final RedirectAttributes redirectModel,
            final HttpServletRequest request) throws CMSItemNotFoundException {

        if (!targetFeatureSwitchFacade.isPreOrderUpdateCardEnabled()) {
            return ControllerConstants.Redirection.ERROR_404;
        }

        final String orderDetailsUrl = orderDetailsPageUrl(orderCode, request);
        final String returnUrl = getDomainUrl(request) + ControllerConstants.MY_ACCOUNT_IPG + orderCode
                + ControllerConstants.MY_ACCOUNT_ORDER_UPDATE_CARD_IPG;
        final String retryUrl = orderDetailsUrl + ControllerConstants.MY_ACCOUNT_ORDER_UPDATE_CARD;

        try {
            final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(
                    new BreadcrumbData("text.account.myOrderDetails", null, targetUrlsMapping.getMyAccountOrders()),
                    new BreadcrumbData("text.account.order.orderBreadcrumb",
                            new Object[] { orderCode }, targetUrlsMapping.getMyAccountOrderDetails() + orderCode),
                    new BreadcrumbData("text.account.updateCard", null, null));

            model.addAttribute("breadcrumbs", breadcrumbs);
            final String iFrameUrl = targetOrderFacade.createIpgUrl(orderCode, orderDetailsUrl, returnUrl);
            model.addAttribute("iFrameUrl", iFrameUrl);
        }
        catch (final AdapterException e) {
            LOG.warn(e.getMessage());
            GlobalMessages.addFlashErrorMessage(redirectModel, ControllerConstants.IPG_SERVICE_UNAVAILABLE,
                    new Object[] { retryUrl });
            return ControllerConstants.Redirection.MY_ACCOUNT_ORDER_DETAILS + orderCode;
        }
        catch (final TargetUpdateCardException e) {
            LOG.info(e.getMessage());
            return updateCardErrorMessage(orderCode, redirectModel, e);
        }

        model.addAttribute("metaRobots", "no-index,no-follow");
        storeCmsPageInModel(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.MY_ACCOUNT_ORDER_DETAIL));
        return ControllerConstants.Views.Pages.Account.ACCOUNT_ORDER_UPDATE_CARD_PAGE;
    }

    /**
     * @param orderCode
     * @param redirectModel
     * @param e
     * @return String
     */
    private String updateCardErrorMessage(final String orderCode, final RedirectAttributes redirectModel,
            final TargetUpdateCardException e) {

        String redirectionValue = null;
        switch (e.getErrorType()) {
            case INVALID_ORDER_STATE:
                GlobalMessages.addFlashErrorMessage(redirectModel, ControllerConstants.UPDATE_CARD_ERROR);
                redirectionValue = ControllerConstants.Redirection.MY_ACCOUNT_ORDER_DETAILS + orderCode;
                break;
            default:
                GlobalMessages.addFlashErrorMessage(redirectModel, ControllerConstants.ORDER_NOT_FOUND_ERROR);
                redirectionValue = ControllerConstants.Redirection.MY_ACCOUNT_ORDERS;
                break;
        }

        return redirectionValue;
    }

    private String orderDetailsPageUrl(final String orderCode, final HttpServletRequest request) {
        return getDomainUrl(request) + ControllerConstants.MY_ACCOUNT
                + ControllerConstants.MY_ACCOUNT_ORDER + orderCode;
    }

    /**
     * Display the modal window, to select an amount in order to initiate a new adhoc payment
     * 
     * @param orderCode
     * @param model
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_ORDERS
            + ControllerConstants.MY_ACCOUNT_ORDER_MAKE_PAYMENT_MODAL
            + ControllerConstants.MY_ACCOUNT_ORDER_CODE, method = RequestMethod.GET)
    public String makePayment(@PathVariable("orderCode") final String orderCode, final Model model,
            final HttpServletResponse response)
                    throws CMSItemNotFoundException {
        model.addAttribute("makePaymentForm", new MakePaymentForm());
        try {
            final TargetOrderData targetOrderData = targetOrderFacade.getOrderDetailsForCode(orderCode);
            model.addAttribute("orderData", targetOrderData);
        }
        catch (final UnknownIdentifierException e) {
            model.addAttribute("noOrderData", Boolean.TRUE);
            LOG.warn("Attempted to load a order that does not exist or is not visible", e);
        }
        storeAnalyticsPageUrl(model,
                ControllerConstants.MY_ACCOUNT.concat(ControllerConstants.MY_ACCOUNT_ORDER_MAKE_PAYMENT_MODAL));
        model.addAttribute("metaRobots", "no-index,no-follow");

        // Add cache control header to prevent cache
        response.setHeader("Cache-Control", "no-cache");

        return ControllerConstants.Views.Pages.Account.ACCOUNT_ORDER_MAKE_PAYMENT_MODAL_PAGE;
    }

    /**
     * @param model
     * @param cncStoreNumber
     */
    private void insertDataForCNCStoreNumber(final Model model, final Integer cncStoreNumber) {
        if (cncStoreNumber != null) {
            model.addAttribute("selectedStore", targetStoreLocatorFacade.getPointOfService(cncStoreNumber));
        }
    }
}
