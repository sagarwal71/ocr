/**
 * 
 */
package au.com.target.tgtstorefront.web.jackson.mixin;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.storelocator.data.OpeningScheduleData;
import de.hybris.platform.commercefacades.user.data.AddressData;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import au.com.target.tgtfacades.user.data.TargetAddressData;


/**
 * @author htan3
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class TargetPointOfServiceData {

    abstract OpeningScheduleData getOpeningHours();

    @JsonIgnore
    abstract AddressData getAddress();

    @JsonIgnore
    abstract Set<String> getAcceptedProductTypes();

    @JsonProperty("address")
    abstract TargetAddressData getTargetAddressData();

    @JsonProperty("available")
    abstract Boolean getAvailableForPickup();

    @JsonIgnore
    abstract Collection<ImageData> getStoreImages();

    @JsonIgnore
    abstract Map<String, String> getFeatures();

    @JsonIgnore
    abstract Boolean getAcceptLayBy();

    @JsonIgnore
    abstract Boolean getClosed();

    @JsonIgnore
    abstract Boolean getAcceptCNC();

}
