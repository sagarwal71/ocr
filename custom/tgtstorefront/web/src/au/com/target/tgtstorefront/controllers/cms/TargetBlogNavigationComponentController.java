/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;


import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.core.model.ItemModel;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.CMSComponentHelper;
import au.com.target.tgtwebcore.model.cms2.components.TargetBlogNavigationComponentModel;
import au.com.target.tgtwebcore.model.cms2.pages.TargetBlogPageModel;


@Controller("TargetBlogNavigationComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.TARGET_BLOG_NAVIGATION_COMPONENT)
public class TargetBlogNavigationComponentController extends
        AbstractCMSComponentController<TargetBlogNavigationComponentModel> {

    /**
     * This component takes a Nav Nodes children and populates the model with three distinct list of TargetBlogPages.
     * This component defines a quantity for each list. For example, there could be 1 large, 3 medium and 100 small. If
     * quantity is 0, then the list will be skipped. If all are 0, then there will be nothing to display.
     * 
     * @param request
     * @param model
     * @param component
     * 
     */
    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
            final TargetBlogNavigationComponentModel component) {

        if (component.getNavigationNode() == null || component.getNavigationNode().getChildren() == null) {
            return;
        }

        final List<CMSNavigationNodeModel> children = component.getNavigationNode().getChildren();
        final TargetBlogPageModel page = getTargetBlogPageModel(component.getNavigationNode());
        model.addAttribute("blogLeadPage", page);

        //To indicate that the current cms page is content page
        final Object currentPage = request.getAttribute(WebConstants.CMS_PAGE_MODEL);
        if (currentPage != null && currentPage instanceof ContentPageModel) {
            model.addAttribute("isContentPage", Boolean.TRUE);
        }

        // Calculate index limits for the the different sizes
        int cursor = 0;

        final int limitLarge = CMSComponentHelper.safeQuantity(component.getQuantityLarge());
        final int limitMedium = CMSComponentHelper.safeQuantity(component.getQuantityMedium()) + limitLarge;
        final int limitSmall = CMSComponentHelper.safeQuantity(component.getQuantitySmall()) + limitMedium;

        final List<TargetBlogPageModel> blogLargeList = new ArrayList<>();
        final List<TargetBlogPageModel> blogMediumList = new ArrayList<>();
        final List<TargetBlogPageModel> blogSmallList = new ArrayList<>();

        for (final CMSNavigationNodeModel navigationNodeModel : children) {
            final TargetBlogPageModel childPage = getTargetBlogPageModel(navigationNodeModel);
            if (childPage != null) {
                if (cursor < limitLarge) {
                    blogLargeList.add(childPage);
                }
                else if (cursor < limitMedium) {
                    blogMediumList.add(childPage);
                }
                else if (cursor < limitSmall) {
                    blogSmallList.add(childPage);
                }
                else {
                    break;
                }
                cursor++;
            }
        }

        model.addAttribute("blogLargeList", blogLargeList);
        model.addAttribute("blogMediumList", blogMediumList);
        model.addAttribute("blogSmallList", blogSmallList);
    }

    private TargetBlogPageModel getTargetBlogPageModel(final CMSNavigationNodeModel navigationNodeModel) {
        if (navigationNodeModel.isVisible()) {
            final List<CMSNavigationEntryModel> entries = navigationNodeModel.getEntries();
            for (final CMSNavigationEntryModel entry : entries) {
                final ItemModel entryItem = entry.getItem();
                if (entryItem instanceof TargetBlogPageModel) {
                    return (TargetBlogPageModel)entryItem;
                }
            }
        }
        return null;
    }

}
