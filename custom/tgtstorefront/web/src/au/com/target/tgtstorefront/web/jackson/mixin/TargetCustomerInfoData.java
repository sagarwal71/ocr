/**
 * 
 */
package au.com.target.tgtstorefront.web.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * Mixing type to change DTO fields returned to UI.
 * 
 * @author jjayawa1
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class TargetCustomerInfoData {
    @JsonIgnore
    abstract String getUid();

}
