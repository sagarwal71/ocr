/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.interceptors.beforeview;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.ModelAndView;


public class HostPropertiesBeforeViewHandler implements BeforeViewHandler {
    private static final String FQDN_KEY = "fullyQualifiedDomainName";

    private String fqdn;

    @Override
    public void beforeView(final HttpServletRequest request, final HttpServletResponse response,
            final ModelAndView modelAndView) {
        modelAndView.addObject(FQDN_KEY, fqdn);
    }

    @Required
    public void setFqdn(final String fqdn) {
        this.fqdn = fqdn;
    }

}
