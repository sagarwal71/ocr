package au.com.target.tgtstorefront.url;

import de.hybris.platform.cms2.model.preview.PreviewDataModel;
import de.hybris.platform.commerceservices.url.UrlResolver;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtwebcore.model.cms2.pages.BrandPageModel;
import au.com.target.tgtwebcore.model.cms2.preview.TargetPreviewDataModel;


/**
 * Extended version to support generating preview URL for brand pages.
 */
public class TargetPreviewDataModelUrlResolver implements UrlResolver<TargetPreviewDataModel> {

    private UrlResolver<PreviewDataModel> defaultPreviewDataUrlResolver;
    private UrlResolver<BrandModel> brandModelUrlResolver;

    @Override
    public String resolve(final TargetPreviewDataModel targetPreviewDataModel) {
        if (targetPreviewDataModel != null && targetPreviewDataModel.getPage() instanceof BrandPageModel) {
            return getBrandModelUrlResolver().resolve(targetPreviewDataModel.getPreviewBrand());
        }
        return defaultPreviewDataUrlResolver.resolve(targetPreviewDataModel);
    }

    protected UrlResolver<BrandModel> getBrandModelUrlResolver() {
        return brandModelUrlResolver;
    }

    @Required
    public void setBrandModelUrlResolver(final UrlResolver<BrandModel> brandModelUrlResolver) {
        this.brandModelUrlResolver = brandModelUrlResolver;
    }

    protected UrlResolver<PreviewDataModel> getDefaultPreviewDataUrlResolver() {
        return defaultPreviewDataUrlResolver;
    }

    @Required
    public void setDefaultPreviewDataUrlResolver(final UrlResolver<PreviewDataModel> defaultPreviewDataUrlResolver) {
        this.defaultPreviewDataUrlResolver = defaultPreviewDataUrlResolver;
    }

}
