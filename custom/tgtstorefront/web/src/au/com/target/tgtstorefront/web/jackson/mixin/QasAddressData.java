package au.com.target.tgtstorefront.web.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * This is the jackson mixin configuration for au.com.target.tgtverifyaddr.data.AddressData
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class QasAddressData {

    @JsonProperty("label")
    abstract String getPartialAddress();

    @JsonProperty("id")
    abstract String getMoniker();

    @JsonIgnore
    abstract String getPostCode();

    @JsonIgnore
    abstract int getScore();
}
