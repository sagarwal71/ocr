/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.account;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.commercefacades.user.exceptions.PasswordMismatchException;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;

import java.util.List;

import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtcore.exception.NoModificationRequiredException;
import au.com.target.tgtfacades.user.data.TargetCustomerData;
import au.com.target.tgtstorefront.annotations.RequireHardLogIn;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.data.BreadcrumbData;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.forms.UpdateEmailForm;
import au.com.target.tgtstorefront.forms.UpdatePasswordForm;
import au.com.target.tgtstorefront.forms.UpdateProfileForm;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
@Controller
@RequestMapping(value = ControllerConstants.MY_ACCOUNT)
@RequireHardLogIn
public class AccountPersonalDetailsContoller extends AbstractAccountController {

    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_PERSONAL_DETAILS, method = RequestMethod.GET)
    public String profile(final Model model) throws CMSItemNotFoundException {
        final List<TitleData> titles = targetUserFacade.getTitles();

        final CustomerData customerData = targetCustomerFacade.getCurrentCustomer();

        if (customerData.getTitleCode() != null) {
            model.addAttribute("title", CollectionUtils.find(titles, new Predicate() {
                @Override
                public boolean evaluate(final Object object) {
                    if (object instanceof TitleData) {
                        return customerData.getTitleCode().equals(((TitleData)object).getCode());
                    }
                    return false;
                }
            }));
        }

        model.addAttribute("customerData", customerData);

        storeCmsPageInModel(model, getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.MY_ACCOUNT_PROFILE));
        setUpMetaDataForContentPage(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.MY_ACCOUNT_PROFILE));
        model.addAttribute("breadcrumbs",
                accountBreadcrumbBuilder.getBreadcrumbs("text.account.myPersonalDetails"));
        model.addAttribute("metaRobots", "no-index,no-follow");
        return ControllerConstants.Views.Pages.Account.ACCOUNT_PROFILE_PAGE;
    }


    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_UPDATE_EMAIL, method = RequestMethod.GET)
    public String editEmail(final Model model) throws CMSItemNotFoundException {
        final UpdateEmailForm updateEmailForm = new UpdateEmailForm();

        model.addAttribute("updateEmailForm", updateEmailForm);

        storeCmsPageInModel(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.MY_ACCOUNT_UPDATE_EMAIL));
        setUpMetaDataForContentPage(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.MY_ACCOUNT_UPDATE_EMAIL));
        model.addAttribute("breadcrumbs",
                accountBreadcrumbBuilder.getBreadcrumbs(new BreadcrumbData("text.account.myPersonalDetails", null,
                        targetUrlsMapping.getMyAccountPersonalDetails()),
                        new BreadcrumbData("text.account.myPersonalDetails.changeEmailAddress")));

        model.addAttribute("metaRobots", "no-index,no-follow");
        return ControllerConstants.Views.Pages.Account.ACCOUNT_PROFILE_EMAIL_EDIT_PAGE;
    }

    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_UPDATE_EMAIL, method = RequestMethod.POST)
    public String updateEmail(@Valid final UpdateEmailForm updateEmailForm, final BindingResult bindingResult,
            final Model model,
            final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {
        String page = ControllerConstants.Redirection.MY_ACCOUNT_PROFILE_PAGE;
        if (bindingResult.hasErrors()) {
            GlobalMessages.addErrorMessage(model, "form.global.error");
            storeCmsPageAndSetUpMetadata(model, ControllerConstants.CmsPageLabel.MY_ACCOUNT_PROFILE);
            model.addAttribute("breadcrumbs",
                    accountBreadcrumbBuilder.getBreadcrumbs(new BreadcrumbData("text.account.myPersonalDetails", null,
                            targetUrlsMapping.getMyAccountPersonalDetails()),
                            new BreadcrumbData("text.account.myPersonalDetails.changeEmailAddress")));
            return ControllerConstants.Views.Pages.Account.ACCOUNT_PROFILE_EMAIL_EDIT_PAGE;
        }
        else {
            try {
                targetCustomerFacade.changeUid(updateEmailForm.getEmail(), updateEmailForm.getPassword());

                //temporary solution to set original UID - with new version of commerceservices it will not be necessary
                final TargetCustomerData customerData = (TargetCustomerData)targetCustomerFacade.getCurrentCustomer();
                customerData.setDisplayUid(updateEmailForm.getEmail());
                targetCustomerFacade.updateProfile(customerData);
                //end of temporary solution

                // Replace the spring security authentication with the new UID
                final String newUid = targetCustomerFacade.getCurrentCustomer().getUid().toLowerCase();
                final Authentication oldAuthentication = SecurityContextHolder.getContext().getAuthentication();
                final UsernamePasswordAuthenticationToken newAuthentication = new UsernamePasswordAuthenticationToken(
                        newUid, null,
                        oldAuthentication.getAuthorities());
                newAuthentication.setDetails(oldAuthentication.getDetails());
                SecurityContextHolder.getContext().setAuthentication(newAuthentication);
                GlobalMessages.addFlashConfMessage(redirectAttributes, "text.account.profile.confirmationUpdated");
            }
            catch (final DuplicateUidException e) {
                if (e.getCause() instanceof NoModificationRequiredException) {
                    bindingResult.rejectValue("email", "validation.checkEmail.noModification");
                }
                else {
                    bindingResult.rejectValue("email", "registration.error.account.exists.title");
                }
            }
            catch (final PasswordMismatchException passwordMismatchException) {
                bindingResult.rejectValue("password", "profile.currentPassword.invalid");
            }
            finally {
                if (bindingResult.hasErrors()) {
                    GlobalMessages.addErrorMessage(model, "form.global.error");
                    storeCmsPageAndSetUpMetadata(model, ControllerConstants.CmsPageLabel.MY_ACCOUNT_PROFILE);
                    model.addAttribute("breadcrumbs",
                            accountBreadcrumbBuilder.getBreadcrumbs(new BreadcrumbData(
                                    "text.account.myPersonalDetails", null,
                                    targetUrlsMapping.getMyAccountPersonalDetails()),
                                    new BreadcrumbData("text.account.myPersonalDetails.changeEmailAddress")));
                    page = ControllerConstants.Views.Pages.Account.ACCOUNT_PROFILE_EMAIL_EDIT_PAGE;
                }
            }
        }
        return page;
    }

    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_UPDATE_PERSONAL_DETAILS, method = RequestMethod.GET)
    public String editProfile(final Model model) throws CMSItemNotFoundException {
        model.addAttribute("titleData", targetUserFacade.getTitles());

        final TargetCustomerData customerData = (TargetCustomerData)targetCustomerFacade.getCurrentCustomer();
        final UpdateProfileForm updateProfileForm = new UpdateProfileForm();

        updateProfileForm.setTitleCode(customerData.getTitleCode());
        updateProfileForm.setFirstName(customerData.getFirstName());
        updateProfileForm.setLastName(customerData.getLastName());
        updateProfileForm.setAllowTracking(Boolean.valueOf(customerData.isAllowTracking()));

        model.addAttribute("updateProfileForm", updateProfileForm);

        storeCmsPageInModel(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.MY_ACCOUNT_UPDATE_PROFILE));
        setUpMetaDataForContentPage(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.MY_ACCOUNT_UPDATE_PROFILE));

        model.addAttribute("breadcrumbs",
                accountBreadcrumbBuilder.getBreadcrumbs(new BreadcrumbData("text.account.myPersonalDetails", null,
                        targetUrlsMapping.getMyAccountPersonalDetails()),
                        new BreadcrumbData("text.account.myPersonalDetails.changeDetails")));
        model.addAttribute("metaRobots", "no-index,no-follow");
        return ControllerConstants.Views.Pages.Account.ACCOUNT_PROFILE_EDIT_PAGE;
    }


    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_UPDATE_PERSONAL_DETAILS, method = RequestMethod.POST)
    public String updateProfile(@Valid final UpdateProfileForm updateProfileForm, final BindingResult bindingResult,
            final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {
        model.addAttribute("titleData", targetUserFacade.getTitles());
        storeCmsPageAndSetUpMetadata(model, ControllerConstants.CmsPageLabel.MY_ACCOUNT_PROFILE);
        model.addAttribute("breadcrumbs",
                accountBreadcrumbBuilder.getBreadcrumbs(new BreadcrumbData("text.account.myPersonalDetails", null,
                        targetUrlsMapping.getMyAccountPersonalDetails()),
                        new BreadcrumbData("text.account.myPersonalDetails.changeDetails")));
        if (bindingResult.hasErrors()) {
            GlobalMessages.addErrorMessage(model, "form.global.error");
            return ControllerConstants.Views.Pages.Account.ACCOUNT_PROFILE_EDIT_PAGE;
        }
        else {
            try {
                final CustomerData currentCustomerData = targetCustomerFacade.getCurrentCustomer();
                final TargetCustomerData customerData = new TargetCustomerData();
                customerData.setTitleCode(updateProfileForm.getTitleCode());
                customerData.setFirstName(updateProfileForm.getFirstName());
                customerData.setLastName(updateProfileForm.getLastName());
                customerData.setUid(currentCustomerData.getUid());
                customerData.setDisplayUid(currentCustomerData.getDisplayUid());
                customerData.setAllowTracking(updateProfileForm.getAllowTracking().booleanValue());
                targetCustomerFacade.updateProfile(customerData);
                GlobalMessages.addFlashConfMessage(redirectAttributes, "text.account.profile.confirmationUpdated");
            }
            catch (final DuplicateUidException e) {
                bindingResult.rejectValue("email", "registration.error.account.exists.title");
                GlobalMessages.addErrorMessage(model, "form.global.error");
            }
        }
        return ControllerConstants.Redirection.MY_ACCOUNT_PROFILE_PAGE;
    }

    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_UPDATE_PASSWORD, method = RequestMethod.GET)
    public String updatePassword(final Model model) throws CMSItemNotFoundException {
        final UpdatePasswordForm updatePasswordForm = new UpdatePasswordForm();

        model.addAttribute("updatePasswordForm", updatePasswordForm);

        storeCmsPageInModel(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.MY_ACCOUNT_UPDATE_PASSWORD));
        setUpMetaDataForContentPage(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.MY_ACCOUNT_UPDATE_PASSWORD));

        model.addAttribute("breadcrumbs",
                accountBreadcrumbBuilder.getBreadcrumbs(new BreadcrumbData("text.account.myPersonalDetails", null,
                        targetUrlsMapping.getMyAccountPersonalDetails()),
                        new BreadcrumbData("text.account.myPersonalDetails.changePassword")));
        model.addAttribute("metaRobots", "no-index,no-follow");
        return ControllerConstants.Views.Pages.Account.ACCOUNT_CHANGE_PASSWORD_PAGE;
    }

    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_UPDATE_PASSWORD, method = RequestMethod.POST)
    public String updatePassword(@Valid final UpdatePasswordForm updatePasswordForm, final BindingResult bindingResult,
            final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {

        if (bindingResult.hasErrors()) {
            GlobalMessages.addErrorMessage(model, "form.global.error");
            storeCmsPageAndSetUpMetadata(model, ControllerConstants.CmsPageLabel.MY_ACCOUNT_PROFILE);
            model.addAttribute("breadcrumbs",
                    accountBreadcrumbBuilder.getBreadcrumbs(new BreadcrumbData("text.account.myPersonalDetails", null,
                            targetUrlsMapping.getMyAccountPersonalDetails()),
                            new BreadcrumbData("text.account.myPersonalDetails.changePassword")));
            return ControllerConstants.Views.Pages.Account.ACCOUNT_CHANGE_PASSWORD_PAGE;
        }
        else {
            try {
                targetCustomerFacade.changePassword(updatePasswordForm.getCurrentPassword(),
                        updatePasswordForm.getNewPassword());
            }
            catch (final PasswordMismatchException localException) {
                bindingResult.rejectValue("currentPassword", "profile.currentPassword.invalid");
                GlobalMessages.addErrorMessage(model, "form.global.error");
                storeCmsPageAndSetUpMetadata(model, ControllerConstants.CmsPageLabel.MY_ACCOUNT_PROFILE);
                model.addAttribute("breadcrumbs",
                        accountBreadcrumbBuilder.getBreadcrumbs(new BreadcrumbData("text.account.myPersonalDetails",
                                null,
                                targetUrlsMapping.getMyAccountPersonalDetails()),
                                new BreadcrumbData("text.account.myPersonalDetails.changePassword")));
                return ControllerConstants.Views.Pages.Account.ACCOUNT_CHANGE_PASSWORD_PAGE;
            }

            GlobalMessages.addFlashConfMessage(redirectAttributes, "text.account.confirmation.password.updated");
            return ControllerConstants.Redirection.MY_ACCOUNT_PROFILE_PAGE;
        }
    }
}
