package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.querybuilder.EndecaProductQueryBuilder;
import au.com.target.tgtfacades.product.data.CustomerProductInfo;
import au.com.target.tgtfacades.product.data.CustomerProductsListData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.EndecaQueryResultsHelper;

import com.endeca.navigation.ENEQueryResults;


/**
 * Controller for customer recently viewed products
 * 
 * 
 */
@Controller
public class RecentlyViewedProductController extends AbstractPageController {


    @Resource(name = "endecaProductQueryBuilder")
    private EndecaProductQueryBuilder endecaProductQueryBuilder;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Resource(name = "endecaQueryResultsHelper")
    private EndecaQueryResultsHelper endecaQueryResultsHelper;



    /**
     * This method gets the JSON Object and populate the product info from Endeca
     * 
     * @param products
     * @param request
     * @return String
     */
    @RequestMapping(value = ControllerConstants.CUSTOMER_RECENTLY_VIEWED_PRODUCTS, method = RequestMethod.POST)
    public String getRecentlyViewedProductDetails(
            @RequestBody final CustomerProductsListData products,
            final HttpServletRequest request, final Model model) {

        if (products != null) {
            final EndecaSearchStateData endecaSearchStateData = populateEndecaSearchStateData(products);
            final ENEQueryResults queryResults = endecaProductQueryBuilder.getQueryResults(endecaSearchStateData, null,
                    getMaxProducts());
            final List<TargetProductListerData> productDataList = endecaQueryResultsHelper
                    .getTargetProductsListForColorVariant(queryResults);
            model.addAttribute("productData", productDataList);
        }
        return ControllerConstants.Views.Fragments.Product.RECENTLY_VIEWED_PRODUCTS;
    }

    /**
     * Populates endeca search state data
     * 
     * @param products
     * @return EndecaSearchStateData
     */
    public EndecaSearchStateData populateEndecaSearchStateData(final CustomerProductsListData products) {
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        final List<CustomerProductInfo> productInfos = products.getProducts();
        final List<String> productCodes = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(productInfos)) {
            for (final CustomerProductInfo productInfo : productInfos) {
                productCodes.add(productInfo.getSelectedVariantCode());
            }
            endecaSearchStateData.setMatchMode(EndecaConstants.MATCH_MODE_ANY);
            endecaSearchStateData.setSkipDefaultSort(true);
            endecaSearchStateData.setSkipInStockFilter(true);
            endecaSearchStateData.setSearchField(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING);
            endecaSearchStateData.setSearchTerm(productCodes);
            endecaSearchStateData.setFieldListConfig(EndecaConstants.FieldList.COMPONENT_FIELDLIST);
        }
        return endecaSearchStateData;
    }


    protected int getMaxProducts() {
        return configurationService.getConfiguration().getInt("storefront.recentlyviewedproducts.component.size", 50);
    }

}