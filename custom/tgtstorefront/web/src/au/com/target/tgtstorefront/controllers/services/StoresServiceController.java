/**
 * 
 */
package au.com.target.tgtstorefront.controllers.services;

import de.hybris.platform.commercefacades.storefinder.StoreFinderFacade;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.CookieGenerator;
import org.springframework.web.util.UriUtils;

import au.com.target.tgtfacades.config.TargetSharedConfigFacade;
import au.com.target.tgtfacades.response.data.ProductStockSearchResultResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.storefinder.TargetStoreFinderStockFacade;
import au.com.target.tgtstorefront.controllers.util.WebServiceExceptionHelper;
import au.com.target.tgtstorefront.util.SearchQuerySanitiser;


/**
 * @author rmcalave
 *
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(value = "/ws-api/v1/{baseSiteId}/stores")
public class StoresServiceController {

    private static final Logger LOG = Logger.getLogger(StoresServiceController.class);

    private static final String ERR_STOCK_VIS_COOKIE = "Unable to store cookie for the location={0}";

    private static final String DEFAULT_DELIVERY_LOCATION_CODE = "default.location";

    @Resource
    private TargetStoreFinderStockFacade targetStoreFinderStockFacade;

    @Resource(name = "inStoreStockSearchQuerySanitiser")
    private SearchQuerySanitiser inStoreStockSearchQuerySanitiser;

    @Resource(name = "inStoreStockSearchProductCodeSanitiser")
    private SearchQuerySanitiser inStoreStockSearchProductCodeSanitiser;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Resource(name = "webServiceExceptionHelper")
    private WebServiceExceptionHelper webServiceExceptionHelper;

    @Resource(name = "storeStockPostCodeCookieGenerator")
    private CookieGenerator storeStockPostCodeCookieGenerator;

    @Resource(name = "storeFinderFacade")
    private StoreFinderFacade storeFinderFacade;

    @Resource(name = "targetSharedConfigFacade")
    private TargetSharedConfigFacade targetSharedConfigFacade;

    @ResponseBody
    @RequestMapping(value = "/nearest", method = RequestMethod.GET)
    public Response getNearestStores(
            @RequestParam(required = false) final Double longitude,
            @RequestParam(required = false) final Double latitude,
            @RequestParam(required = false) final String location,
            @RequestParam(required = false) final String pc,
            @RequestParam(required = false) final Integer sn,
            @RequestParam(required = false, defaultValue = "0") final int currentPage,
            final HttpServletResponse httpServletResponse) {

        if (LOG.isDebugEnabled()) {
            LOG.debug("getNearestStores: pc=" + pc + " | location=" + location + " | latitude="
                    + latitude
                    + " | longitude=" + longitude
                    + " | sn=" + sn);
        }

        String sanitisedLocation = null;
        if (StringUtils.isNotEmpty(location)) {
            sanitisedLocation = inStoreStockSearchQuerySanitiser.sanitiseSearchText(location);
        }

        String sanitisedProductCode = null;
        if (StringUtils.isNotEmpty(pc)) {
            sanitisedProductCode = inStoreStockSearchProductCodeSanitiser.sanitiseSearchText(pc);
        }

        final PageableData pageableData = createPageableData();
        pageableData.setPageSize(configurationService.getConfiguration().getInt("storefront.storelocator.pageSize", 5));
        pageableData.setCurrentPage(currentPage);

        final Response response = targetStoreFinderStockFacade.doSearchProductStock(sanitisedProductCode,
                sanitisedLocation, latitude,
                longitude,
                sn, pageableData, false);

        if (response.isSuccess() && StringUtils.isNotEmpty(sanitisedLocation)) {
            final ProductStockSearchResultResponseData responseData = (ProductStockSearchResultResponseData)response
                    .getData();
            final String sanitisedSelectedLocation = inStoreStockSearchQuerySanitiser
                    .sanitiseSearchText(responseData
                            .getSelectedLocation());

            try {
                storeStockPostCodeCookieGenerator.addCookie(httpServletResponse,
                        UriUtils.encodePath(sanitisedSelectedLocation, "utf-8"));
            }
            catch (final UnsupportedEncodingException ex) {
                LOG.error(MessageFormat.format(ERR_STOCK_VIS_COOKIE, sanitisedLocation));
            }
        }

        return response;
    }

    @ResponseBody
    @RequestMapping(value = "leadtimes/{deliveryType}", method = RequestMethod.POST)
    public Response getNearestCncHdDeliveryLocation(@PathVariable final String deliveryType,
            @RequestParam(required = false) final String location,
            @RequestParam(required = false) final Integer storeNumber,
            @RequestParam(required = false) final Integer akGeo) {
        if (LOG.isDebugEnabled()) {
            LOG.debug(MessageFormat.format(
                    "getNearestHomeDeliveryLocation with location={0}, storeNumber={1} and akGeo={2}", location,
                    storeNumber, akGeo));
        }
        String sanitisedLocation = null;
        if (StringUtils.isNotEmpty(location)) {
            sanitisedLocation = inStoreStockSearchQuerySanitiser.sanitiseSearchText(location);
        }
        //If no data provided then default value for the location will be set.
        sanitisedLocation = StringUtils.isEmpty(location)
                ? targetSharedConfigFacade.getConfigByCode(DEFAULT_DELIVERY_LOCATION_CODE) : sanitisedLocation;
        final PageableData pageableData = createPageableData();
        pageableData.setPageSize(1);
        final Response response = targetStoreFinderStockFacade.nearestStoreLocation(deliveryType,
                    sanitisedLocation,
                    storeNumber, pageableData);
        if (response != null && response.isSuccess() && Integer.valueOf(1).equals(akGeo)) {
            LOG.info("akamaiGeoLocation=" + sanitisedLocation);
        }
        return response;
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Response handleException(final HttpServletRequest request,
            final Exception exception) {
        return webServiceExceptionHelper.handleException(request, exception);
    }

    protected PageableData createPageableData() {
        return new PageableData();
    }

}
