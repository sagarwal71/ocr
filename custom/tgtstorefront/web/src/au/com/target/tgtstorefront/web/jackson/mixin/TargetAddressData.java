/**
 * 
 */
package au.com.target.tgtstorefront.web.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author htan3
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class TargetAddressData {

    @JsonIgnore
    abstract String getFormattedAddress();

    @JsonIgnore
    abstract boolean isVisibleInAddressBook();

    @JsonIgnore
    abstract boolean isBillingAddress();

    @JsonIgnore
    abstract boolean isShippingAddress();
}
