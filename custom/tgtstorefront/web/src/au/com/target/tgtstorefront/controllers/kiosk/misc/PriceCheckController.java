/**
 * 
 */
package au.com.target.tgtstorefront.controllers.kiosk.misc;

import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.Arrays;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.target.tgtfacades.product.data.TargetPOSProductData;
import au.com.target.tgtfacades.product.data.TargetPOSProductData.ErrorType;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.AbstractProductPageController;


/**
 * Controller for populating model with product data from both pos and hybris
 * 
 */
@Controller
public class PriceCheckController extends AbstractProductPageController {

    protected static final Logger LOG = Logger.getLogger(PriceCheckController.class);

    @Resource(name = "messageSource")
    protected MessageSource messageSource;

    @Resource(name = "i18nService")
    protected I18NService i18nService;

    @RequestMapping(value = "/k/pricecheck", method = RequestMethod.GET)
    public String priceCheckBarcode(
            @RequestParam(value = "barcode", defaultValue = StringUtils.EMPTY) final String barcode,
            @RequestParam(value = "storeNumber", defaultValue = StringUtils.EMPTY) final String storeNumber,
            final Model model, final HttpServletRequest request, final HttpServletResponse response)
    {
        response.setHeader("Cache-Control", "no-cache");
        if (StringUtils.isNotEmpty(barcode) && StringUtils.isNotEmpty(storeNumber)) {

            // Get the POS price data
            final TargetPOSProductData posProduct = getTargetProductFacade().getPOSProductDetails(storeNumber, barcode);
            model.addAttribute("posProduct", posProduct);

            if (posProduct != null) {

                // Override error message for non-pos errors for simplicity - could be SYSTEM_ERROR from service 
                // or INVALID_DATA from facade...
                if (posProduct.getErrorType().equals(ErrorType.SYSTEM_ERROR)) {
                    posProduct.setFailureReason(getErrorMessage("kiosk.pricecheck.error.message.systemerror"));
                }

                if (posProduct.getErrorType().equals(ErrorType.INVALID_DATA)) {
                    posProduct.setFailureReason(getErrorMessage("kiosk.pricecheck.error.message.invaliddata"));
                }

                // If possible get product from hybris which may be available to enrich display with review and image data
                if (posProduct.getErrorType().equals(ErrorType.NONE)
                        && StringUtils.isNotEmpty(posProduct.getItemCode())) {

                    try {
                        final ProductModel productModel = getProductService().getProductForCode(
                                posProduct.getItemCode());
                        populateHybrisProductDetail(productModel, model, request);
                    }
                    catch (final UnknownIdentifierException e) {
                        LOG.info("Could not load hybris product model for pos item code: " + posProduct.getItemCode());
                    }
                }
            }
        }

        return ControllerConstants.Views.Fragments.Kiosk.PRICECHECK;
    }


    private String getErrorMessage(final String id) {
        return messageSource.getMessage(id, null, i18nService.getCurrentLocale());
    }

    protected void populateHybrisProductDetail(final ProductModel productModel,
            final Model model,
            final HttpServletRequest request)
    {
        final TargetProductData productData = (TargetProductData)getTargetProductFacade()
                .getProductForCodeAndOptions(
                        productModel.getCode(),
                        Arrays.asList(
                                ProductOption.BASIC,
                                ProductOption.GALLERY,
                                ProductOption.REVIEW));

        populateProductData(productData, model, request);
    }

}
