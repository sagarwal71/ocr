/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers;

import de.hybris.platform.acceleratorcms.model.components.CategoryFeatureComponentModel;
import de.hybris.platform.acceleratorcms.model.components.MiniCartComponentModel;
import de.hybris.platform.acceleratorcms.model.components.NavigationBarComponentModel;
import de.hybris.platform.acceleratorcms.model.components.ProductFeatureComponentModel;
import de.hybris.platform.acceleratorcms.model.components.ProductReferencesComponentModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;
import de.hybris.platform.cms2lib.model.components.RotatingImagesComponentModel;

import org.springframework.web.servlet.view.UrlBasedViewResolver;

import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtwebcore.model.cms2.components.ArticleItemComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.ArticleListComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.CoordinateLinkComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.FeaturedProductsComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.ProductEngagementComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.PurchasedProductReferencesComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.RichBannerComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.SubcategoryGridComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.TargetBlogNavigationComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.TargetCustomerSubscriptionComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.TargetDepartmentsCategoryNavigationComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.TargetDepartmentsNavigationComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.TargetExternalRecommendationsCarouselComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.TargetFeedbackContainerComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.TargetFlexiBannerContainerComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.TargetNavigationComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.TargetProductGridComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.TargetProductGroupCarouselComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.TargetRecentlyViewedProductsComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.TargetRecommendationsComponentModel;


public interface ControllerConstants {

    /**
     * 
     * All Mapping constants They are not nested into interfaces to be easily injected into the view
     * 
     */
    String ROOT = "/";
    String NON_INDEXED = "/~";

    String LOGIN = "/login";
    String LOGOUT = "/logout";
    String LOGOUT_KIOSK = LOGOUT + "?forward=kiosk";

    String SEARCH = "/search";

    String MOBILE_APP_SEARCH = "/mobile-app/search";
    String MOBILE_APP_AUTO_SUGGESTION = "/auto-suggestions";
    String MOBILE_APP_AUTO_SEARCH = MOBILE_APP_SEARCH + MOBILE_APP_AUTO_SUGGESTION;
    String CUSTOMER_SEGMENTED = "/customer/dynamic";

    String MOBILE_CUSTOMER_INFO = "/mobile-app/customerinfo";

    String CHECKOUT = "/checkout";
    String SINGLE_PAGE_CHECKOUT = "/spc";
    String THANKYOU = "/thankyou";
    String THANKYOUCHECK = "/thankyoucheck";
    String CHECKOUT_LOGIN = CHECKOUT + LOGIN;
    String SINGLE_PAGE_CHECKOUT_AND_LOGIN = SINGLE_PAGE_CHECKOUT + "/order";
    String SINGLE_PAGE_CHECKOUT_PASSWORD_RESET = SINGLE_PAGE_CHECKOUT + "/set-password";
    String SINGLE_PAGE_CHECKOUT_AND_LOGIN_THANKYOU = SINGLE_PAGE_CHECKOUT_AND_LOGIN + THANKYOU + ROOT;
    String SINGLE_PAGE_CHECKOUT_AND_LOGIN_THANKYOUCHECK = SINGLE_PAGE_CHECKOUT_AND_LOGIN + THANKYOUCHECK + ROOT;
    String SINGLE_PAGE_CHECKOUT_AND_LOGIN_PLACE_ORDER_ERROR = SINGLE_PAGE_CHECKOUT_AND_LOGIN + "/place-order/error?e=";

    String REGISTER = CHECKOUT + "/register";
    String NEW_CUSTOMER = "/newcustomer";

    //ADDRESS PAGE
    String YOUR_ADDRESS = CHECKOUT + "/your-address";
    String YOUR_ADDRESS_ADD = "/add";
    String YOUR_ADDRESS_ADD_CONFIRM = YOUR_ADDRESS_ADD + "/confirm";
    String YOUR_ADDRESS_SELECT = "/select";
    String YOUR_ADDRESS_CONTINUE = "/continue";
    String YOUR_ADDRESS_EDIT = "/edit";
    String YOUR_ADDRESS_EDIT_CONFIRM = YOUR_ADDRESS_EDIT + "/confirm";
    String REMOVE_ENTRIES = "/remove-entries";
    String UPDATE_DELIVERY_MODE = "/update-delivery-mode";
    String STORE_CNC_SEARCH_POSITION = "/store-search-position";
    String STORE_CNC_SEARCH_LOCATION = "/store-search-location";

    //PAYMENT PAGE
    String PAYMENT = CHECKOUT + "/payment-details";
    String PAYMENT_EXISTING = "/existing";
    String PAYMENT_PAYPAL = "/paypal";
    String PAYMENT_PAYPAL_RETURN = "/paypal-return";
    String PAYMENT_AFTERPAY_RETURN = "/afterpay-return";
    String PAYMENT_AFTERPAY_CANCEL = "/afterpay-return-cancel";
    String PAYMENT_TNS_RETURN = "/tns-return";
    String PAYMENT_VALIDATE_TMD = "/validate-tmd";
    String PAYMENT_VALIDATE_VOUCHER = "/validate-voucher";
    String PAYMENT_REMOVE_VOUCHER = "/remove-voucher";
    String PAYMENT_BILLING = "/billing";
    String PAYMENT_GIFT_CARD_BILLING = "/giftcard-billing";
    String PAYMENT_FLYBUYS_VALIDATE = "/flybuys/validate";
    String PAYMENT_FLYBUYS_LOGIN = "/flybuys/login";
    String PAYMENT_FLYBUYS_SELECT_OPTION = "/flybuys/select-option";
    //Zip Pay
    String PAYMENT_ZIPPAY_REDIRECT = "/zippay-return";

    String SESSION_PARAM_PAYPAL_EC_TOKEN = "paypalECToken";
    String SESSION_PARAM_AFTERPAY_TOKEN = "afterpayToken";
    String SESSION_PARAM_ZIPPAY_TOKEN = "zippayToken";

    //REVIEW PAGE
    String REVIEW = CHECKOUT + "/review-your-order";
    String IPG_PARAM_QS = "qs";
    String IPG_PARAM_TOKEN = "SST";
    String IPG_PARAM_SESSIONID = "SessionId";

    //PLACE ORDER
    String PLACE_ORDER = CHECKOUT + "/place-order";
    String PLACE_ORDER_IPG = "/ipg";
    String PLACE_ORDER_HOLDING = "/holding";
    String PLACE_IPG_ORDER_HOLDING = "/ipg-holding";
    String PLACE_ORDER_PREPARE = "/prepare";
    String PLACE_ORDER_IPG_IFRAME_RETURN = "/return-ipg";

    //SPC IPG 
    String SPC_IPG_IFRAME_RETURN = CHECKOUT + "/spc-return-ipg";

    //THANK YOU PAGE
    String THANK_YOU = CHECKOUT + "/thank-you/";
    String THANK_YOU_ORDER_CODE = "{orderCode:.*}";
    String THANK_YOU_CHECK = "check";

    // BARCODE 
    String GENERATE_BARCODE = TgtFacadesConstants.ORDER_BARCODE + "{format}";

    String GUEST_REGISTER = "register/";

    //E NEWS SUBSCRIPTION PAGE
    String ENEWS_SUBSCRIPTION_REDIRECT = "/eNewsSubscription";
    String ENEWS_OLD_SUBSCRIPTION_REDIRECT = "/enews";
    String ENEWS_SUBSCRIPTION = "/enewsletter";
    String ENEWS_PREFERENCES = "/enewsletter-prefs";
    String ENEWS_QUICK = "/enews/quick";

    //MUMSHUB SUBSCRIPTION PAGE
    String MUMS_HUB_SUBSCRIPTION = "/customer-subscription/mumshub";
    String MUMS_HUB_PERSONALISE = "/mumshub/personalise";
    String SESSION_PARAM_MUMS_HUB_SUBSCRIPTION_EMAIL = "customerSubscriptionEmail";
    String MUMS_HUB_LANDING = "/mumshub";

    //INSTORE WIFI PAGE
    String INSTORE = "/instore";
    String OLD_INSTORE_WIFI = INSTORE + "/wifi";
    String INSTORE_WIFI = NON_INDEXED + INSTORE + "/wifi";
    String INSTORE_WIFI_AUTHENTICATED = INSTORE_WIFI + "/authenticated";

    //DONATE REQUEST PAGE
    String DONATE_REQUEST = "/communities/donate";

    //MY ACCOUNT PAGE
    String MY_ACCOUNT = "/my-account";
    String MY_ACCOUNT_ORDERS = "/order-details";
    String MY_ACCOUNT_ORDER = "/order-details/";
    String MY_ACCOUNT_ORDER_MAKE_PAYMENT = "/make-payment/";
    String MY_ACCOUNT_IPG = "/my-account-ipg";
    String MY_ACCOUNT_ORDER_UPDATE_CARD_IPG = "/ipg-return";
    String MY_ACCOUNT_ORDER_MAKE_PAYMENT_MODAL = MY_ACCOUNT_ORDER_MAKE_PAYMENT + "modal/";
    String MY_ACCOUNT_ORDER_MAKE_PAYMENT_EXISTING = MY_ACCOUNT_ORDER_MAKE_PAYMENT + "existing/";
    String MY_ACCOUNT_ORDER_MAKE_PAYMENT_PAYPAL = MY_ACCOUNT_ORDER_MAKE_PAYMENT + "paypal/";
    String MY_ACCOUNT_ORDER_MAKE_PAYMENT_PAYPAL_RETURN = MY_ACCOUNT_ORDER_MAKE_PAYMENT + "paypal-return/";
    String MY_ACCOUNT_ORDER_MAKE_PAYMENT_TNS_RETURN = MY_ACCOUNT_ORDER_MAKE_PAYMENT + "tns-return/";
    String MY_ACCOUNT_ORDER_CODE = "{orderCode:.*}";
    String MY_ACCOUNT_ORDER_UPDATE_CARD = "/update-card";
    String MY_ACCOUNT_PERSONAL_DETAILS = "/personal-details";
    String MY_ACCOUNT_UPDATE_EMAIL = "/change-email-address";
    String MY_ACCOUNT_UPDATE_PERSONAL_DETAILS = "/change-personal-details";
    String MY_ACCOUNT_UPDATE_PASSWORD = "/change-password";
    String MY_ACCOUNT_ADDRESS_DETAILS = "/address-details";
    String MY_ACCOUNT_ADDRESS_ADD = "/add-address";
    String MY_ACCOUNT_ADDRESS_EDIT = "/edit-address/";
    String MY_ACCOUNT_ADDRESS_ADD_CONFIRM = MY_ACCOUNT_ADDRESS_ADD + "/confirm";
    String MY_ACCOUNT_ADDRESS_EDIT_CONFIRM = MY_ACCOUNT_ADDRESS_EDIT + "confirm";
    String MY_ACCOUNT_ADDRESS_CODE = "{addressCode:.*}";
    String MY_ACCOUNT_ADDRESS_REMOVE = "/remove-address/";
    String MY_ACCOUNT_ADDRESS_SET_DEFAULT = "/set-default-address/";
    String MY_ACCOUNT_PAYMENT = "/payment-details";
    String MY_ACCOUNT_PAYMENT_SET_DEFAULT = "/set-default-payment-details";
    String MY_ACCOUNT_PAYMENT_REMOVE = "/remove-payment-method";

    //PASSWORD
    String FORGOTTEN_PASSWORD = "/login/forgotten-password";
    String PASSWORD_RESET = "/reset";

    //CART PAGE
    String CART = "/basket";
    String CART_UPDATE = "/update";
    String CART_UPDATE_GIFTCARDS = "/update-giftcards";
    String CART_CHECKOUT = "/checkout";
    String CART_CONTINUE_SHOPPING = "/continue";
    String CART_LOGIN = CART + "/login";
    String CART_DELIVERY_MODE_UPDATE = "/update-delivery-mode";
    String CART_DELIVERY_MODE_DISPLAY = "/show-delivery-mode";
    String CART_DELIVERY_MODE_REMOVAL = "/remove-delivery-mode";

    String REQUEST_PARAM_PAYPAL_TOKEN = "token";
    String REQUEST_PARAM_PAYPAL_PAYER_ID = "PayerID";

    String REQUEST_PARAM_AFTERPAY_ORDER_TOKEN = "orderToken";
    String REQUEST_PARAM_AFTERPAY_STATUS = "status";

    String SESSION_PARAM_PLACE_ORDER_IN_PROGRESS = "placeOrderInProgress";

    String SESSION_PARAM_PLACE_ORDER_RESULT = "placeOrderResult";
    String SESSION_PARAM_PLACE_ORDER_ORDER_CODE = "placeOrderOrderCode";

    //STORE PAGE
    String STORE = "/store";
    String STORE_CODE_PATH_PATTERN = STORE + "/**/**";
    String STORE_CODE_PATH_VARIABLE_PATTERN = STORE + "/**/{storeNumber}";
    String STORE_CODE_HOURS_PATTERN = STORE + "/{storeNumber}" + "/hours";

    //STORE FINDER PAGE    
    String STORE_FINDER = "/store-finder";
    String STORE_FINDER_POSITION = "/position";
    String STORE_FINDER_STATE = "/state";
    String STORE_FINDER_STATE_VARIABLE_PATTERN = STORE_FINDER_STATE + "/{state}";
    String CNC_STORE_NUMBER_PAGESIZE = "tgtstorefront.checkoutAddress.cncStore.pageSize";

    // PRODUCT PAGE
    String PRODUCT_REVIEW = "/review";
    String PRODUCT_REVIEW_EMAIL = "/review-email";

    // CUSTOMER
    String CUSTOMER_GET_INFO = "/customer/getinfo";
    String CUSTOMER_LOCATION = "/customer/location";

    // BULKY BAORD
    String BULKYBOARD = "/~/bulkyboard/";
    String BULKYBOARD_STORENUMBER = BULKYBOARD + "{storeNumber:.*}";
    String BULKYBOARD_STORENUMBER_CATEGORY = BULKYBOARD + "{storeNumber:.*}/{categoryCode:.*}";


    //SIZE PAGE REQUEST MAPPING
    String SIZE_PAGE = "/size-chart/**/**";

    //Endeca Cache management

    // CUSTOMER
    String ENDECA_CLEAR_CACHE = "/endeca-target/clearcache";

    //CUSTOOMER RECENTLY VIEWED PRODUCTS
    String CUSTOMER_RECENTLY_VIEWED_PRODUCTS = "/customer-recently-viewed-products";

    //Customer wishlist favourites
    String CUSTOMER_WISHLIST_FAVOURITES = "/favourites";


    //CONSTANTS FOR ACCREDIT PARAM
    String ACCREDIT_CHECKOUT = "checkout";
    String ACCREDIT_WIFI = "wifi";
    String ACCREDIT_NEWSLETTER = "newsletter";
    String ACCREDIT_URL = "?accredit=";
    String ACCREDIT_MUMSHUB = "mumshub";

    String ERROR = "error";
    String SOH_UPDATES = "sohUpdates";

    String WS_API = "/ws-api/v1/{baseSiteId}";
    String WS_API_CHECKOUT = WS_API + "/checkout";
    String WS_API_CHECKOUT_LOGIN = WS_API_CHECKOUT + "/login";
    String WS_API_CHECKOUT_LOGIN_SUCCESS = "/success";
    String WS_API_CHECKOUT_LOGIN_FAILURE = "/failure";
    String WS_API_CHECKOUT_LOGIN_CUSTOMER_REGISTERED = "/customer/registered";
    String WS_API_CHECKOUT_LOGIN_GUEST = "/guest";
    String WS_API_CHECKOUT_LOGIN_REGISTER = "/register";
    String WS_API_CHECKOUT_LOGIN_ENEWS_SUBSCRIPTION = "/enews";
    String WS_API_CHECKOUT_LOGIN_RESET_PASSWORD = "/reset-password";
    String WS_API_CHECKOUT_LOGIN_SET_PASSWORD = "/set-password";
    String REQUEST_PARAM_NOTAVAILABLE_ONLINE_AND_INSTORE = "notAvailableOnlineAndInStore";
    String CLEAR_CACHE = "/clearCache";

    //CONSTANTS FOR UPDATE CARD
    String UPDATE_CARD_ERROR = "order.details.card.update.error";
    String ORDER_NOT_FOUND_ERROR = "order.details.not.found.error";
    String IPG_SERVICE_UNAVAILABLE = "order.details.ipg.service.unavailable";

    // zipPay attributes

    String REQUEST_PARAM_ZIPPAY_RESULT = "result";
    String REQUEST_PARAM_ZIPPAY_CHECKOUT_TOKEN = "checkoutId";
    String REQUEST_PARAM_ZIPPAY_STATUS_APPROVED = "approved";
    String REQUEST_PARAM_ZIPPAY_STATUS_REFERRED = "referred";
    String REQUEST_PARAM_ZIPPAY_STATUS_DECLINED = "declined";
    String REQUEST_PARAM_ZIPPAY_STATUS_CANCELLED = "cancelled";



    public interface Redirection {
        String HOME = UrlBasedViewResolver.REDIRECT_URL_PREFIX + ControllerConstants.ROOT;
        String LOGIN = UrlBasedViewResolver.REDIRECT_URL_PREFIX + ControllerConstants.LOGIN;

        String CART = UrlBasedViewResolver.REDIRECT_URL_PREFIX + ControllerConstants.CART;
        String CART_TYPE = CART + "?type=";

        String CHECKOUT_LOGIN = UrlBasedViewResolver.REDIRECT_URL_PREFIX + ControllerConstants.CHECKOUT_LOGIN;
        String CHECKOUT = UrlBasedViewResolver.REDIRECT_URL_PREFIX + ControllerConstants.CHECKOUT;
        String CHECKOUT_YOUR_ADDRESS = UrlBasedViewResolver.REDIRECT_URL_PREFIX + ControllerConstants.YOUR_ADDRESS;
        String CHECKOUT_YOUR_ADDRESS_ADD = UrlBasedViewResolver.REDIRECT_URL_PREFIX + ControllerConstants.YOUR_ADDRESS
                + ControllerConstants.YOUR_ADDRESS_ADD;
        String CHECKOUT_PAYMENT = UrlBasedViewResolver.REDIRECT_URL_PREFIX + ControllerConstants.PAYMENT;
        String CHECKOUT_PAYMENT_TNS_RETURN = UrlBasedViewResolver.REDIRECT_URL_PREFIX + ControllerConstants.PAYMENT
                + ControllerConstants.PAYMENT_TNS_RETURN;
        String CHECKOUT_ORDER_CONFIRMATION = UrlBasedViewResolver.REDIRECT_URL_PREFIX
                + ControllerConstants.THANK_YOU
                + ControllerConstants.THANK_YOU_ORDER_CODE;
        String CHECKOUT_ORDER_SUMMARY = UrlBasedViewResolver.REDIRECT_URL_PREFIX + REVIEW;
        String CHECKOUT_ORDER_PLACE = UrlBasedViewResolver.REDIRECT_URL_PREFIX + PLACE_ORDER;
        String CHECKOUT_ORDER_PLACE_HOLDING = UrlBasedViewResolver.REDIRECT_URL_PREFIX + PLACE_ORDER
                + PLACE_ORDER_HOLDING;
        String CHECKOUT_THANK_YOU_CHECK = UrlBasedViewResolver.REDIRECT_URL_PREFIX + THANK_YOU
                + THANK_YOU_CHECK;
        String CHECKOUT_THANK_YOU = UrlBasedViewResolver.REDIRECT_URL_PREFIX + THANK_YOU;

        String MY_ACCOUNT_ADDRESS_DETAILS_PAGE = UrlBasedViewResolver.REDIRECT_URL_PREFIX
                + ControllerConstants.MY_ACCOUNT + ControllerConstants.MY_ACCOUNT_ADDRESS_DETAILS;
        String MY_ACCOUNT_PAYMENT_INFO_PAGE = UrlBasedViewResolver.REDIRECT_URL_PREFIX + ControllerConstants.MY_ACCOUNT
                + ControllerConstants.MY_ACCOUNT_PAYMENT;
        String MY_ACCOUNT_PROFILE_PAGE = UrlBasedViewResolver.REDIRECT_URL_PREFIX + ControllerConstants.MY_ACCOUNT
                + ControllerConstants.MY_ACCOUNT_PERSONAL_DETAILS;
        String MY_ACCOUNT_ORDERS = UrlBasedViewResolver.REDIRECT_URL_PREFIX + ControllerConstants.MY_ACCOUNT
                + ControllerConstants.MY_ACCOUNT_ORDERS;
        String MY_ACCOUNT_ORDER_DETAILS = UrlBasedViewResolver.REDIRECT_URL_PREFIX + ControllerConstants.MY_ACCOUNT
                + ControllerConstants.MY_ACCOUNT_ORDER;
        String MY_ACCOUNT = UrlBasedViewResolver.REDIRECT_URL_PREFIX + ControllerConstants.MY_ACCOUNT;

        String ENEWS_SUBSCRIPTION = UrlBasedViewResolver.REDIRECT_URL_PREFIX + ControllerConstants.ENEWS_SUBSCRIPTION;

        String FORGOTTEN_PASSWORD = UrlBasedViewResolver.REDIRECT_URL_PREFIX + ControllerConstants.FORGOTTEN_PASSWORD;

        String ERROR_404 = UrlBasedViewResolver.REDIRECT_URL_PREFIX + "/404";

        String STORE_FINDER = UrlBasedViewResolver.REDIRECT_URL_PREFIX + ControllerConstants.STORE_FINDER;

        String MUMS_HUB_LANDING = UrlBasedViewResolver.REDIRECT_URL_PREFIX + ControllerConstants.MUMS_HUB_LANDING;

        String EXPEDITE_CHECKOUT_ORDER_SUMMARY = CHECKOUT_ORDER_SUMMARY + "?expedited=yes";

        String SINGLE_PAGE_CHECKOUT = UrlBasedViewResolver.REDIRECT_URL_PREFIX
                + ControllerConstants.SINGLE_PAGE_CHECKOUT + '/';
        String SINGLE_PAGE_CHECKOUT_AND_LOGIN = UrlBasedViewResolver.REDIRECT_URL_PREFIX
                + ControllerConstants.SINGLE_PAGE_CHECKOUT_AND_LOGIN + '/';
        String SINGLE_PAGE_CHECKOUT_PASSWORD_RESET = UrlBasedViewResolver.REDIRECT_URL_PREFIX
                + ControllerConstants.SINGLE_PAGE_CHECKOUT_PASSWORD_RESET;
        String SINGLE_PAGE_CHECKOUT_AND_LOGIN_THANKYOU = UrlBasedViewResolver.REDIRECT_URL_PREFIX
                + ControllerConstants.SINGLE_PAGE_CHECKOUT_AND_LOGIN_THANKYOU;
        String SINGLE_PAGE_CHECKOUT_AND_LOGIN_THANKYOUCHECK = UrlBasedViewResolver.REDIRECT_URL_PREFIX
                + ControllerConstants.SINGLE_PAGE_CHECKOUT_AND_LOGIN_THANKYOUCHECK;
        String SINGLE_PAGE_CHECKOUT_AND_LOGIN_PLACE_ORDER_ERROR = UrlBasedViewResolver.REDIRECT_URL_PREFIX
                + ControllerConstants.SINGLE_PAGE_CHECKOUT_AND_LOGIN_PLACE_ORDER_ERROR;
    }

    public interface Ipg {
        String IPG_ABORT_ACTION = "ipg_abort_session";
        String IPG_PAYMENT_TYPE_ABORT = "IPG_PAYMENT_ABORT";
        String IPG_PAYMENT_TYPE_CANCEL = "IPG_PAYMENT_CANCEL";
        String IPG_PAYMENT_TYPE_COMPLETE = "IPG_PAYMENT_COMPLETE";
    }

    public interface Forward {
        String ERROR_404 = UrlBasedViewResolver.FORWARD_URL_PREFIX + "/404";
    }

    public interface CmsPageLabel {
        String CHECKOUT_REGISTER = "checkout-register";
        String CHECKOUT_CHOOSE_DELIVERY_ADDRESS_CMS_PAGE_LABEL = "choose-delivery-address";
        String CHECKOUT_ADD_EDIT_ADDRESS_CMS_PAGE_LABEL = "add-edit-address";
        String CHECKOUT_NEW_PAYMENT_CMS_PAGE_LABEL = "payment-details";
        String CHECKOUT_REVIEW_YOUR_ORDER_CMS_PAGE_LABEL = "review-your-order";
        String CHECKOUT_THANK_YOU_CHECK = "thank-you-check-order";
        String CHECKOUT_THANK_YOU_CMS_PAGE_LABEL = "thank-you";

        String MY_ACCOUNT = "account";
        String MY_ACCOUNT_PROFILE = "profile";
        String MY_ACCOUNT_UPDATE_PROFILE = "update-profile";
        String MY_ACCOUNT_UPDATE_PASSWORD = "update-password";
        String MY_ACCOUNT_UPDATE_EMAIL = "update-email";
        String MY_ACCOUNT_ADDRESS_DETAILS = "address-details";
        String MY_ACCOUNT_ADD_EDIT_ADDRESS = "add-edit-address-details";
        String MY_ACCOUNT_PAYMENT_DETAILS = "my-payment-details";
        String MY_ACCOUNT_ORDER_HISTORY = "orders";
        String MY_ACCOUNT_ORDER_DETAIL = "order";
        String MY_ACCOUNT_MAKE_A_PAYMENT = "make-a-payment";
    }

    public interface CustomDimensions {
        String ASSORTED = "dimension17";
        String DISPLAY_ONLY = "dimension21";
        String END_OF_LIFE = "dimension27";
    }

    /**
     * Class with action name constants
     */
    public interface Actions {
        public interface Cms {
            String PREFIX = "/view/";
            String SUFFIX = "Controller";

            /**
             * Default CMS component controller
             */
            String DEFAULT_CMS_COMPONENT = PREFIX + "DefaultCMSComponentController";

            /**
             * CMS components that have specific handlers
             */
            String PURCHASED_PRODUCT_REFERENCES_COMPONENT = PREFIX + PurchasedProductReferencesComponentModel._TYPECODE
                    + SUFFIX;
            String PRODUCT_REFERENCES_COMPONENT = PREFIX + ProductReferencesComponentModel._TYPECODE + SUFFIX;
            String PRODUCT_CAROUSEL_COMPONENT = PREFIX + ProductCarouselComponentModel._TYPECODE + SUFFIX;
            String TARGET_RECOMMENDATIONS_COMPONENT = PREFIX + TargetRecommendationsComponentModel._TYPECODE + SUFFIX;
            String MINI_CART_COMPONENT = PREFIX + MiniCartComponentModel._TYPECODE + SUFFIX;
            String PRODUCT_FEATURE_COMPONENT = PREFIX + ProductFeatureComponentModel._TYPECODE + SUFFIX;
            String CATEGORY_FEATURE_COMPONENT = PREFIX + CategoryFeatureComponentModel._TYPECODE + SUFFIX;
            String NAVIGATION_BAR_COMPONENT = PREFIX + NavigationBarComponentModel._TYPECODE + SUFFIX;
            String CMS_LINK_COMPONENT = PREFIX + CMSLinkComponentModel._TYPECODE + SUFFIX;
            String TARGET_NAVIGATION_COMPONENT = PREFIX + TargetNavigationComponentModel._TYPECODE + SUFFIX;
            String TARGET_BLOG_NAVIGATION_COMPONENT = PREFIX + TargetBlogNavigationComponentModel._TYPECODE + SUFFIX;
            String TARGET_DEPARTMENT_NAVIGATION_COMPONENT = PREFIX
                    + TargetDepartmentsNavigationComponentModel._TYPECODE + SUFFIX;
            String ARTICLE_ITEM_COMPONENT = PREFIX + ArticleItemComponentModel._TYPECODE + SUFFIX;
            String ARTICLE_LIST_COMPONENT = PREFIX + ArticleListComponentModel._TYPECODE + SUFFIX;
            String ROTATING_IMAGE_COMPONENT = PREFIX + RotatingImagesComponentModel._TYPECODE + SUFFIX;
            String FEATURED_PRODUCTS_COMPONENT = PREFIX + FeaturedProductsComponentModel._TYPECODE + SUFFIX;
            String SUBCATEGORY_GRID_COMPONENT = PREFIX + SubcategoryGridComponentModel._TYPECODE + SUFFIX;
            String RICH_BANNER_COMPONENT = PREFIX + RichBannerComponentModel._TYPECODE + SUFFIX;
            String COORDINATE_LINK_COMPONENT = PREFIX + CoordinateLinkComponentModel._TYPECODE + SUFFIX;
            String PRODUCT_ENGAGEMENT_COMPONENT = PREFIX + ProductEngagementComponentModel._TYPECODE + SUFFIX;
            String TARGET_PRODUCT_GRID_COMPONENT = PREFIX + TargetProductGridComponentModel._TYPECODE + SUFFIX;
            String TARGET_FLEXI_BANNERS_CONTAINER_COMPONENT = PREFIX
                    + TargetFlexiBannerContainerComponentModel._TYPECODE + SUFFIX;
            String TARGET_RECENTLY_VIEWED_PRODUCTS_COMPONENT = PREFIX
                    + TargetRecentlyViewedProductsComponentModel._TYPECODE + SUFFIX;
            String TARGET_CUSTOMER_SUBSCRIPTION_COMPONENT = PREFIX
                    + TargetCustomerSubscriptionComponentModel._TYPECODE + SUFFIX;
            String TARGET_DEPT_CAT_NAV_COMPONENT = PREFIX
                    + TargetDepartmentsCategoryNavigationComponentModel._TYPECODE + SUFFIX;
            String TARGET_PRODUCT_GROUP_CAROUSEL_COMPONENT = PREFIX
                    + TargetProductGroupCarouselComponentModel._TYPECODE
                    + SUFFIX;
            String TARGET_FEEDBACK_CONTAINER_COMPONENT = PREFIX + TargetFeedbackContainerComponentModel._TYPECODE
                    + SUFFIX;
            String TARGET_EXTERNAL_RECOMMENDATIONS_CAROUSEL = PREFIX
                    + TargetExternalRecommendationsCarouselComponentModel._TYPECODE + SUFFIX;
        }
    }

    /**
     * Class with view name constants
     */
    public interface Views {
        public interface Cms {
            String COMPONENT_PREFIX = "cms/";
        }

        public interface Pages {
            public interface Account {
                String ACCOUNT_LOGIN_PAGE = "pages/account/accountLoginPage";
                String ACCOUNT_HOME_PAGE = "pages/account/accountHomePage";
                String ACCOUNT_ORDER_HISTORY_PAGE = "pages/account/accountOrderHistoryPage";
                String ACCOUNT_ORDER_PAGE = "pages/account/accountOrderPage";
                String ACCOUNT_ORDER_MAKE_PAYMENT_MODAL_PAGE = "pages/account/accountOrderMakePaymentModalPage";
                String ACCOUNT_ORDER_MAKE_PAYMENT_PAGE = "pages/account/accountOrderMakePaymentPage";
                String ACCOUNT_ORDER_UPDATE_CARD_PAGE = "pages/account/updateCardPage";
                String ACCOUNT_PROFILE_PAGE = "pages/account/accountProfilePage";
                String ACCOUNT_PROFILE_EDIT_PAGE = "pages/account/accountProfileEditPage";
                String ACCOUNT_PROFILE_EMAIL_EDIT_PAGE = "pages/account/accountProfileEmailEditPage";
                String ACCOUNT_CHANGE_PASSWORD_PAGE = "pages/account/accountChangePasswordPage";
                String ACCOUNT_ADDRESS_DETAILS_PAGE = "pages/account/accountAddressBookPage";
                String ACCOUNT_EDIT_ADDRESS_PAGE = "pages/account/accountEditAddressPage";
                String ACCOUNT_PAYMENT_INFO_PAGE = "pages/account/accountPaymentInfoPage";
                String ACCOUNT_REGISTER_PAGE = "pages/account/accountRegisterPage";
                String ACCOUNT_CONFIRM_ADDRESS_PAGE = "pages/account/accountConfirmAddressPage";
                String ACCOUNT_ENEWS_SUBSCRIPTION_PAGE = "pages/account/enewsSubscriptionPage";
                String ACCOUNT_ENEWS_PREFERENCES_PAGE = "pages/account/enewsPreferencesPage";
                String ACCOUNT_MUMS_HUB_PERSONALISE_PAGE = "pages/account/mumsHubPersonalisePage";
            }

            public interface Checkout {
                String CHECKOUT_LOGIN_PAGE = "pages/checkout/checkoutLandingPage";
                String CHECKOUT_IPG_RETURN_PAGE = "pages/checkout/returnFromIpg";
                String CHECKOUT_SPC_IPG_RETURN_PAGE = "pages/checkout/single/returnFromIpg";
            }

            public interface MultiStepCheckout {
                // Your Address
                String CHOOSE_DELIVERY_ADDRESS_PAGE = "pages/checkout/multi/chooseDeliveryAddressPage";
                String ADD_EDIT_DELIVERY_ADDRESS_PAGE = "pages/checkout/multi/addEditDeliveryAddressPage";
                String UPDATE_DELIVERY_MODE_PAGE = "pages/checkout/multi/updateDeliveryModePage";
                String CONFIRM_DELIVERY_ADDRESS_PAGE = "pages/checkout/multi/confirmDeliveryAddressPage";

                String VALIDATE_TMD_PAGE = "pages/checkout/multi/validateTmd";
                String VALIDATE_VOUCHER_PAGE = "pages/checkout/multi/validateVoucher";
                String REMOVE_VOUCHER_PAGE = "pages/checkout/multi/removeVoucher";

                String STORE_CNC_SEARCH_POSITION = "pages/checkout/multi/storeSearchPosition";
                String STORE_CNC_SEARCH_LOCATION = "pages/checkout/multi/storeSearchLocation";

                // Payment Details
                String CHOOSE_PAYMENT_METHOD_PAGE = "pages/checkout/multi/choosePaymentMethodPage";

                // Review You Order
                String REVIEW_YOUR_ORDER_PAGE = "pages/checkout/multi/reviewYourOrderPage";
                String VIEW_STORE_DETAIL_PAGE = "pages/checkout/multi/viewStoreDetails";

                // Place Order
                String PLACE_ORDER_HOLDING_PAGE = "pages/checkout/multi/checkoutHolding";
                String PLACE_ORDER_PREPARE_PAGE = "pages/checkout/multi/preparePlaceOrder";

                // Thank You
                String THANK_YOU_PAGE = "pages/checkout/multi/thankYouPage";
                String THANK_YOU_CHECK_PROBLEM_PAGE = "pages/checkout/multi/checkoutProblem";
            }

            public interface Password {
                String PASSWORD_RESET_REQUEST_PAGE = "pages/password/passwordResetRequestPage";
                String PASSWORD_RESET_CHANGE_PAGE = "pages/password/passwordResetChangePage";
            }

            public interface Error {
                String ERROR_NOT_FOUND_PAGE = "pages/error/errorNotFoundPage";
            }

            public interface Cart {
                String CART_PAGE = "pages/cart/cartPage";
                String CART_LOGIN_PAGE = "pages/cart/cartLoginPage";
            }

            public interface StoreFinder {
                String STORE_FINDER_SEARCH_PAGE = "pages/storeFinder/storeFinderSearchPage";
                String STORE_FINDER_DETAILS_PAGE = "pages/storeFinder/storeFinderDetailsPage";
            }

            public interface Misc {
                String MISC_ROBOTS_PAGE = "pages/misc/miscRobotsPage";
            }

            public interface Product {
                String REVIEW = "pages/product/review";
            }

            public interface SiteMap {
                String SITE_MAP = "pages/siteMap/siteMapPage";
            }

            public interface SiteFurniture {
                String HEADER_PAGE = "pages/siteFurniture/headerPage";
                String FOOTER_PAGE = "pages/siteFurniture/footerPage";
            }

            public interface Kiosk {
                String LAUNCH = "pages/kiosk/launch";
                String LAUNCH_SSL = "pages/kiosk/launchSSL";
                String RESET = "pages/kiosk/reset";
            }

            public interface InStore {
                String INSTORE_WIFI_PAGE = "pages/instore/inStoreWifiPage";
                String INSTORE_WIFI_AUTHENTICATED_PAGE = "pages/instore/inStoreWifiAuthenticatedPage";
            }

            public interface SizePage {
                String SIZE_PAGE = "pages/size/sizePage";
            }

            public interface DonateRequest {
                String DONATE_REQUEST_PAGE = "pages/forms/donateRequest";
            }

            public interface LooksCollection {
                String COLLECTION_PAGE = "/pages/product/productCollectionPage";

            }

            public interface LookView {
                String LOOK_PAGE = "/pages/product/productGroupPage";

            }

            public interface Favourites {
                String FAVOURITES_PAGE = "/pages/user/favouritesPage";
            }

            public interface Fragment {
                String FRAGMENT_PAGE = "pages/fragment/fragmentPage";
            }

            public interface JsonCmsContent {
                String JSON_CMS_CONTENT_PAGE = "pages/json/jsonPage";
            }

        }

        public interface Fragments {
            public interface Kiosk {
                String PRICECHECK = "fragments/kiosk/priceCheck";
            }

            public interface Customer {
                String GETINFO = "fragments/customer/getInfo";
                String LOCATION = "fragments/customer/location";
            }

            public interface Cart {
                String ADD_TO_CART_POPUP = "fragments/cart/addToCartPopup";
                String MINI_CART_PANEL = "fragments/cart/miniCartPanel";
                String MINI_CART_ERROR_PANEL = "fragments/cart/miniCartErrorPanel";
                String CART_POPUP = "fragments/cart/cartPopup";
                String CART_SHOW_DELIVERY_MODE = "fragments/cart/showDeliveryMode";
                String UPDATE_GIFTCARD = "fragments/cart/updateGiftCard";
            }

            public interface SingleStepCheckout {
                String DELIVERY_ADDRESS_FORM_POPUP = "fragments/checkout/single/deliveryAddressFormPopup";
                String PAYMENT_DETAILS_FORM_POPUP = "fragments/checkout/single/paymentDetailsFormPopup";
            }

            public interface Flybuys {
                String FLYBUYS_VALIDATE_RESULT = "fragments/checkout/multi/flybuysValidateJson";
                String FLYBUYS_LOGIN_FORM = "fragments/checkout/multi/flybuysLoginFormPopup";
                String FLYBUYS_LOGIN_FORM_RESULT = "fragments/checkout/multi/flybuysLoginFormJson";
                String FLYBUYS_SELECT_OPTION = "fragments/checkout/multi/flybuysSelectOptionPopup";
                String FLYBUYS_SELECT_OPTION_RESULT = "fragments/checkout/multi/flybuysSelectOptionJson";
                String FLYBUYS_REMOVE_OPTION_RESULT = "fragments/checkout/multi/flybuysRemoveOptionJson";

            }

            public interface Product {
                String QUICK_VIEW_POPUP = "fragments/product/quickViewPopup";
                String VARIANT_PARTIAL_DETAIL = "fragments/product/variantPartialDetail";
                String VARIANT_PARTIAL = "fragments/product/variantPartial";
                String VARIANT_STOCK = "fragments/product/variantStock";
                String PRODUCT_JSON = "fragments/product/productJson";
                String REVIEWS_TAB = "fragments/product/reviewsTab";
                String RECENTLY_VIEWED_PRODUCTS = "fragments/product/recentlyViewedProducts";
                String SERIES_VIEW = "fragments/product/seriesView";
                String FIND_NEAREST_STORE = "fragments/product/findNearestStore";
            }

            public interface Enews {
                String ENEWS_JSON_FRAGMENT = "fragments/enews/enewsFragment";
                String ENEWS_WIFI_JSON_FRAGMENT = "fragments/enews/enewsWifiFragment";
                String ENEWS_QUICK_POPUP_FRAGMENT = "fragments/enews/enewsQuickPopup";

            }

            public interface Search {
                String SEARCH_AUTO_SUGGESTIONS = "fragments/search/autoSuggestionsJson";
            }

            public interface Address {
                String ADDRESS_AUTO_SUGGESTIONS = "fragments/address/addressSuggestionsJson";
                String ADDRESS_FORMAT = "fragments/address/formattedAddressJson";
            }

            public interface Wishlist {
                String FAVOURITES_FRAGMENT = "fragments/wishlist/favourites";
                String FAVOURITES_FRAGMENT_ADD_REMOVE = "fragments/wishlist/favouritesAddRemove";
                String FAVOURITES_SHARE = "/fragments/wishlist/favouritesShare";
                String FAVOURITES_SHARE_RESPONSE = "/fragments/wishlist/favouritesShareResponse";

            }

            public interface STORE {
                String STORE_HOURS = "/fragments/store/storeHours";
            }
        }

        public interface PageNames {
            String BASKET = "basket";
        }

        public interface ErrorActions {
            String CHECKOUT = "checkout";
        }
    }

}
