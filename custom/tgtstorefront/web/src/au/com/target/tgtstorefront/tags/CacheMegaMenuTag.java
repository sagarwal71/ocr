/**
 * 
 */
package au.com.target.tgtstorefront.tags;

import de.hybris.platform.cms2.model.preview.CMSPreviewTicketModel;
import de.hybris.platform.cms2.model.preview.PreviewDataModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPreviewService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;


/**
 * @author dcwillia
 * 
 */
public class CacheMegaMenuTag extends BodyTagSupport {

    private static Object syncObj = new Object();
    private static Map<String, CacheDetail> cache;
    private static final Logger LOG = Logger.getLogger(CacheMegaMenuTag.class);
    private static ConfigurationService configurationService =
            (ConfigurationService)Registry.getApplicationContext().getBean("configurationService");

    static {
        cache = new ConcurrentHashMap<String, CacheDetail>();
    }

    private String cacheKey;

    private class CacheDetail {
        private final String cachedContent;
        private final Date whenCacheExpire;

        public CacheDetail(final String content) {
            cachedContent = content;
            final int timeout =
                    configurationService.getConfiguration().getInt("tgtstorefront.megamenu.cache.timeout", 60);
            whenCacheExpire = DateUtils.addSeconds(new Date(), timeout);
        }
    }

    @Override
    public int doStartTag() throws JspException {
        if (isPreviewDataModelValid()) {
            return EVAL_BODY_INCLUDE;
        }

        if (isCacheStale()) {
            return EVAL_BODY_BUFFERED;
        }
        else {
            LOG.debug("Using cached mega menu. cacheKey = " + cacheKey);
            try {
                final CacheDetail detail = cache.get(cacheKey);
                if (detail != null) {
                    pageContext.getOut().write(detail.cachedContent);
                }
                else {
                    LOG.debug("Cached mega menu was null. cacheKey = " + cacheKey);
                }
            }
            catch (final IOException ex) {
                throw new JspException(ex);
            }
            return SKIP_BODY;
        }
    }

    @Override
    public int doAfterBody() throws JspException {
        if (isPreviewDataModelValid()) {
            LOG.debug("Preview mode, no cacheing of mega menu. cacheKey = " + cacheKey);
            return SKIP_BODY;
        }

        LOG.debug("Rebuild of mega menu cache has occured. cacheKey = " + cacheKey);
        final String content = bodyContent.getString().trim();
        if (content != null) {
            synchronized (syncObj) {
                if (isCacheStale()) {
                    final CacheDetail newDetail = new CacheDetail(content);
                    cache.put(cacheKey, newDetail);
                }
            }
            try {
                bodyContent.writeOut(bodyContent.getEnclosingWriter());
            }
            catch (final IOException ex) {
                throw new JspException(ex);
            }
        }

        return SKIP_BODY;
    }

    private boolean isCacheStale() {
        final CacheDetail detail = cache.get(cacheKey);
        if (detail == null) {
            return true;
        }

        if (detail.whenCacheExpire.before(new Date())) {
            return true;
        }

        return false;
    }

    // Code stolen from the hybris CMSBodyTag to ascertain if it is live edit 
    protected boolean isLiveEdit() {
        return isPreviewDataModelValid()
                && Boolean.TRUE.equals(getPreviewData(pageContext.getRequest()).getLiveEdit());
    }

    protected boolean isPreviewDataModelValid() {

        return getPreviewData(pageContext.getRequest()) != null;
    }

    protected String getPreviewTicketId(final ServletRequest httpRequest) {
        String id = httpRequest.getParameter("cmsTicketId");
        final WebApplicationContext appContext =
                WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
        final SessionService sessionService = (SessionService)appContext.getBean("sessionService");
        if (StringUtils.isBlank(id)) {
            id = (String)sessionService.getAttribute("cmsTicketId");
        }

        return id;
    }

    protected PreviewDataModel getPreviewData(final ServletRequest httpRequest) {
        return getPreviewData(getPreviewTicketId(httpRequest));
    }

    protected PreviewDataModel getPreviewData(final String ticketId) {
        PreviewDataModel ret = null;
        final WebApplicationContext appContext =
                WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
        final CMSPreviewService cmsPreviewService = (CMSPreviewService)appContext.getBean("cmsPreviewService");
        final CMSPreviewTicketModel previewTicket = cmsPreviewService.getPreviewTicket(ticketId);
        if (previewTicket != null) {
            ret = previewTicket.getPreviewData();
        }

        return ret;
    }

    protected ServletContext getServletContext() {
        return pageContext.getServletContext();
    }

    public void setCacheKey(final String cacheKey) {
        this.cacheKey = cacheKey;
    }


}
