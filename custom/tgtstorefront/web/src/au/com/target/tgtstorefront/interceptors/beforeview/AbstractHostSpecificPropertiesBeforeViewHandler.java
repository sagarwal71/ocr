/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.interceptors.beforeview;

import de.hybris.platform.acceleratorservices.config.HostConfigService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.ModelAndView;


public abstract class AbstractHostSpecificPropertiesBeforeViewHandler implements BeforeViewHandler
{
    @Resource(name = "hostConfigService")
    protected HostConfigService hostConfigService;

    @Resource(name = "commonI18NService")
    protected CommonI18NService commonI18NService;

    protected String hostSuffix;

    protected void addHostProperty(final String serverName, final ModelAndView modelAndView, final String configKey,
            final String modelKey)
    {
        modelAndView.addObject(modelKey,
                hostConfigService.getProperty(configKey, (serverName != null ? serverName : StringUtils.EMPTY)));
    }

    protected void addHostProperty(final ModelAndView modelAndView, final String modelKey, final String modelValue)
    {
        modelAndView.addObject(modelKey, modelValue);
    }

    @Required
    public void setHostSuffix(final String hostSuffix) {
        this.hostSuffix = hostSuffix;
    }
}
