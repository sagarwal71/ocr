/**
 * 
 */
package au.com.target.tgtstorefront.web.jackson.mixin;

import de.hybris.platform.commercefacades.storelocator.data.SpecialOpeningDayData;
import de.hybris.platform.commercefacades.storelocator.data.WeekdayOpeningDayData;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import au.com.target.tgtfacades.storelocator.data.TargetOpeningWeekData;


/**
 * @author rmcalave
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class TargetShortOpeningScheduleData {

    @JsonIgnore
    abstract List<WeekdayOpeningDayData> getWeekDayOpeningList();

    @JsonIgnore
    abstract List<SpecialOpeningDayData> getSpecialDayOpeningList();

    @JsonIgnore
    abstract List<TargetOpeningWeekData> getTargetOpeningWeeks();
}
