/**
 *
 */
package au.com.target.tgtstorefront.controllers.util;



/**
 * @author bmcalis2
 *
 */
public final class CMSComponentHelper {

    private CMSComponentHelper() {
        // prevent construction
    }

    public static int safeQuantity(final Integer quantity) {
        if (quantity != null) {
            final int qty = quantity.intValue();
            if (qty >= 0) {
                return qty;
            }
        }
        return 0;
    }
}
