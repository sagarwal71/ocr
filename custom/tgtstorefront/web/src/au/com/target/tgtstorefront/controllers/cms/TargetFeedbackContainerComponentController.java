/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.CMSRestrictionService;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtwebcore.model.cms2.components.TargetFeedbackComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.TargetFeedbackContainerComponentModel;


@Controller("TargetFeedbackContainerComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.TARGET_FEEDBACK_CONTAINER_COMPONENT)
public class TargetFeedbackContainerComponentController<T extends TargetFeedbackContainerComponentModel> extends
        AbstractCMSComponentController<TargetFeedbackContainerComponentModel> {

    @Autowired
    private CMSRestrictionService cmsRestrictionService;

    /**
     * set allowed feedbacks to model
     */
    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
            final TargetFeedbackContainerComponentModel component) {
        if (CollectionUtils.isNotEmpty(component.getItems())) {
            model.addAttribute(WebConstants.FEEDBACKS, getCustomerFeedbacks(component, request));
        }
    }

    protected List getCustomerFeedbacks(final TargetFeedbackContainerComponentModel cmsComponent,
            final HttpServletRequest httpRequest) {
        final boolean previewEnabled = isPreviewEnabled(httpRequest);
        final List feedbacks = new ArrayList();
        for (final TargetFeedbackComponentModel component : cmsComponent.getItems())
        {
            boolean allowed = BooleanUtils.isTrue(component.getVisible());
            if (allowed && CollectionUtils.isNotEmpty(component.getRestrictions()) && !previewEnabled)
            {
                final RestrictionData data = populate(httpRequest);
                allowed = cmsRestrictionService.evaluateCMSComponent(component, data);
            }
            if (allowed) {
                feedbacks.add(component);
            }
        }
        return feedbacks;
    }
}
