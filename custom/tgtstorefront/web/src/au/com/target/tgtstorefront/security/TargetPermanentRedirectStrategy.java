/**
 * 
 */
package au.com.target.tgtstorefront.security;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.util.UrlUtils;


/**
 * @author pthoma20
 *
 */
public class TargetPermanentRedirectStrategy extends DefaultRedirectStrategy {

    public static final String LOCATION = "Location";

    @Override
    public void sendRedirect(final HttpServletRequest request, final HttpServletResponse response, final String url)
            throws IOException {
        response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
        response.setHeader(LOCATION, response.encodeURL(calculateRedirectUrl(request.getContextPath(), url)));
    }

    @Override
    protected String calculateRedirectUrl(final String contextPath, final String url) {
        if (!UrlUtils.isAbsoluteUrl(url)) {

            return contextPath + url;
        }
        return url;
    }

}
