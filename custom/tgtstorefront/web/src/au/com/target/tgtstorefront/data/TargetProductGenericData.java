/**
 * 
 */
package au.com.target.tgtstorefront.data;

import de.hybris.platform.commercefacades.product.data.VariantOptionData;

import au.com.target.tgtutility.util.TargetProductUtils;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class TargetProductGenericData {
    private boolean active = false;
    private String name;
    private String code;
    private boolean outOfStock = false;
    private String url;

    public TargetProductGenericData(final VariantOptionData variantOptionData) {
        code = variantOptionData.getCode();
        if (!TargetProductUtils.checkIfStockDataHasStock(variantOptionData.getStock())) {
            outOfStock = true;
        }
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active
     *            the active to set
     */
    public void setActive(final boolean active) {
        this.active = active;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     *            the url to set
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    /**
     * @return the outOfStock
     */
    public boolean isOutOfStock() {
        return outOfStock;
    }

    /**
     * @param outOfStock
     *            the outOfStock to set
     */
    public void setOutOfStock(final boolean outOfStock) {
        this.outOfStock = outOfStock;
    }

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }
}
