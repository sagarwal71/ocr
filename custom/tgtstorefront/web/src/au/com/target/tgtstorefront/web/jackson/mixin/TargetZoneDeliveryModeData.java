/**
 * 
 */
package au.com.target.tgtstorefront.web.jackson.mixin;

import de.hybris.platform.commercefacades.product.data.PriceData;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;




/**
 * @author htan3
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class TargetZoneDeliveryModeData {
    @JsonProperty("id")
    abstract String getCode();

    @JsonProperty("fee")
    abstract PriceData getDeliveryCost();

    @JsonIgnore
    abstract int getDisplayOrder();

    @JsonIgnore
    abstract boolean isPostCodeNotSupported();

    @JsonIgnore
    abstract boolean isDigitalDelivery();

    @JsonIgnore
    abstract String getIncentiveMetMessage();

    @JsonIgnore
    abstract String getPotentialIncentiveMessage();
}
