/**
 * 
 */
package au.com.target.tgtstorefront.web.jackson.mixin;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;


/**
 * This is the jackson mixin configuration for {@link de.hybris.platform.basecommerce.enums.StockLevelStatus}
 * 
 * @author mgazal
 *
 */
@JsonSerialize(using = StockLevelStatusSerializer.class)
public abstract class StockLevelStatus {
    // intentionally empty
}
