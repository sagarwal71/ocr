/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * Checkout Login Controller. Handles login and and redirect to the basket page used to direct ppl to there persistant
 * carts
 */
@Controller
@RequestMapping(value = ControllerConstants.CART_LOGIN)
public class CartLoginController extends AbstractLoginPageController
{
    @Resource(name = "targetCheckoutFacade")
    private TargetCheckoutFacade checkoutFacade;

    @Resource(name = "targetUserFacade")
    private TargetUserFacade targetUserFacade;

    /**
     * @return the checkoutFacade
     */
    protected TargetCheckoutFacade getCheckoutFacade() {
        return checkoutFacade;
    }

    @Override
    protected String getView()
    {
        return ControllerConstants.Views.Pages.Cart.CART_LOGIN_PAGE;
    }

    @Override
    protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
    {
        return getContentPageForLabelOrId("cart-landing");
    }

    /**
     * 
     * @param session
     * @param model
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(method = RequestMethod.GET)
    public String doCartLogin(final HttpSession session, final Model model) throws CMSItemNotFoundException
    {
        // If the current user is know, then we can direct them to the basket page.
        if (!targetUserFacade.isCurrentUserAnonymous()) {
            return ControllerConstants.Redirection.CART;
        }

        model.addAttribute("pageType", PageType.CartLogin);
        return getDefaultLoginPage(session, model);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtstorefront.controllers.pages.AbstractRegisterPageController#getSuccessRedirect(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response) {
        return null;
    }

}
