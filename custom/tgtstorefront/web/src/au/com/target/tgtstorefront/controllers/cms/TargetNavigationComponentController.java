/**
 *
 */
package au.com.target.tgtstorefront.controllers.cms;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtfacades.navigation.NavigationMenuItem;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.navigation.TargetNavigationComponentMenuBuilder;
import au.com.target.tgtwebcore.model.cms2.components.TargetNavigationComponentModel;


@Controller("TargetNavigationComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.TARGET_NAVIGATION_COMPONENT)
public class TargetNavigationComponentController extends AbstractCMSComponentController<TargetNavigationComponentModel> {
    @Autowired
    private TargetNavigationComponentMenuBuilder navigationComponentMenuBuilder;

    /* (non-Javadoc)
     * @see au.com.target.tgtstorefront.controllers.cms.AbstractCMSComponentController#fillModel(javax.servlet.http.HttpServletRequest, org.springframework.ui.Model, de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel)
     */
    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
            final TargetNavigationComponentModel component) {
        final NavigationMenuItem navigationComponentMenuItem = navigationComponentMenuBuilder
                .createNavigationComponentMenu(component);

        if (navigationComponentMenuItem != null) {
            model.addAttribute(WebConstants.NAVIGATION_COMPONENT_MENU_ITEM, navigationComponentMenuItem);
        }

        if (component.getShowTitle() != null) {
            model.addAttribute(WebConstants.NAVIGATION_COMPONENT_SHOW_TITLE, component.getShowTitle());
        }
    }

    public void setNavigationComponentMenuBuilder(final TargetNavigationComponentMenuBuilder navigationComponentMenuBuilder) {
        this.navigationComponentMenuBuilder = navigationComponentMenuBuilder;
    }
}
