/**
 * 
 */
package au.com.target.tgtstorefront.forms.checkout;

import javax.validation.Valid;

import au.com.target.tgtstorefront.forms.BillingAddressForm;
import au.com.target.tgtstorefront.forms.validation.NameOnCard;
import au.com.target.tgtstorefront.forms.validation.PaymentCardExpiryMonth;
import au.com.target.tgtstorefront.forms.validation.PaymentCardExpiryYear;
import au.com.target.tgtstorefront.forms.validation.PaymentCardNumber;


/**
 * @author gbaker2
 * 
 */
public class NewCardPaymentDetailsForm extends AbstractPaymentDetailsForm {


    @NameOnCard
    private String nameOnCard;
    @PaymentCardNumber
    private String gatewayCardNumber;
    @PaymentCardExpiryMonth
    private String gatewayCardExpiryDateMonth;
    @PaymentCardExpiryYear
    private String gatewayCardExpiryDateYear;
    private String gatewayCardSecurityCode;
    private String gatewayCardScheme;
    private String gatewayFormResponse;
    private Boolean saveInAccount;
    private String paymentSessionId;
    @Valid
    private BillingAddressForm billingAddress;


    /**
     * Default Constructor
     */
    public NewCardPaymentDetailsForm() {
        super();

    }


    public NewCardPaymentDetailsForm(final String action) {
        setAction(action);
    }

    /**
     * @return the nameOnCard
     */
    public String getNameOnCard() {
        return nameOnCard;
    }

    /**
     * @param nameOnCard
     *            the nameOnCard to set
     */
    public void setNameOnCard(final String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }


    /**
     * @return the saveInAccount
     */
    public Boolean getSaveInAccount() {
        return saveInAccount;
    }

    /**
     * @param saveInAccount
     *            the saveInAccount to set
     */
    public void setSaveInAccount(final Boolean saveInAccount) {
        this.saveInAccount = saveInAccount;
    }

    /**
     * @return the billingAddress
     */
    public BillingAddressForm getBillingAddress() {
        return billingAddress;
    }

    /**
     * @param billingAddress
     *            the billingAddress to set
     */
    public void setBillingAddress(final BillingAddressForm billingAddress) {
        this.billingAddress = billingAddress;
    }

    /**
     * @return the gatewayCardNumber
     */
    public String getGatewayCardNumber() {
        return gatewayCardNumber;
    }

    /**
     * @param gatewayCardNumber
     *            the gatewayCardNumber to set
     */
    public void setGatewayCardNumber(final String gatewayCardNumber) {
        this.gatewayCardNumber = gatewayCardNumber;
    }

    /**
     * @return the gatewayCardExpiryDateMonth
     */
    public String getGatewayCardExpiryDateMonth() {
        return gatewayCardExpiryDateMonth;
    }

    /**
     * @param gatewayCardExpiryDateMonth
     *            the gatewayCardExpiryDateMonth to set
     */
    public void setGatewayCardExpiryDateMonth(final String gatewayCardExpiryDateMonth) {
        this.gatewayCardExpiryDateMonth = gatewayCardExpiryDateMonth;
    }

    /**
     * @return the gatewayCardExpiryDateYear
     */
    public String getGatewayCardExpiryDateYear() {
        return gatewayCardExpiryDateYear;
    }

    /**
     * @param gatewayCardExpiryDateYear
     *            the gatewayCardExpiryDateYear to set
     */
    public void setGatewayCardExpiryDateYear(final String gatewayCardExpiryDateYear) {
        this.gatewayCardExpiryDateYear = gatewayCardExpiryDateYear;
    }

    /**
     * @return the gatewayCardSecurityCode
     */
    public String getGatewayCardSecurityCode() {
        return gatewayCardSecurityCode;
    }

    /**
     * @param gatewayCardSecurityCode
     *            the gatewayCardSecurityCode to set
     */
    public void setGatewayCardSecurityCode(final String gatewayCardSecurityCode) {
        this.gatewayCardSecurityCode = gatewayCardSecurityCode;
    }

    /**
     * @return the paymentSessionId
     */
    public String getPaymentSessionId() {
        return paymentSessionId;
    }

    /**
     * @param paymentSessionId
     *            the paymentSessionId to set
     */
    public void setPaymentSessionId(final String paymentSessionId) {
        this.paymentSessionId = paymentSessionId;
    }

    /**
     * @return the gatewayCardScheme
     */
    public String getGatewayCardScheme() {
        return gatewayCardScheme;
    }

    /**
     * @param gatewayCardScheme
     *            the gatewayCardScheme to set
     */
    public void setGatewayCardScheme(final String gatewayCardScheme) {
        this.gatewayCardScheme = gatewayCardScheme;
    }


    /**
     * @return the gatewayFormResponse
     */
    public String getGatewayFormResponse() {
        return gatewayFormResponse;
    }

    /**
     * @param gatewayFormResponse
     *            the gatewayFormResponse to set
     */
    public void setGatewayFormResponse(final String gatewayFormResponse) {
        this.gatewayFormResponse = gatewayFormResponse;
    }

}
