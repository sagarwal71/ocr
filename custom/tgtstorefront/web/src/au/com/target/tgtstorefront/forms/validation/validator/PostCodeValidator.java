/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtstorefront.forms.validation.PostCode;
import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class PostCodeValidator extends AbstractTargetValidator implements ConstraintValidator<PostCode, String> {

    @Override
    public void initialize(final PostCode arg0) {
        field = FieldTypeEnum.postCode;
        sizeRange = new int[] { TargetValidationCommon.PostCode.SIZE };
        isMandatory = true;
        isSizeFixed = true;
        mustMatch = true;
        loadPattern(TargetValidationCommon.PostCode.class);
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }

}
