/**
 * 
 */
package au.com.target.tgtstorefront.interceptors.beforecontroller;

import de.hybris.platform.servicelayer.user.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.method.HandlerMethod;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtstorefront.customersegment.impl.CustomerSegmentCookieStrategy;


/**
 * @author rsamuel3
 *
 */
public class CustomerSegmentCookieBeforeHandler implements BeforeControllerHandler {

    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    private UserService userService;

    private CustomerSegmentCookieStrategy customerSegmentCookieStrategy;

    /* (non-Javadoc)
     * @see au.com.target.tgtstorefront.interceptors.beforecontroller.BeforeControllerHandler#beforeController(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.web.method.HandlerMethod)
     */
    @Override
    public boolean beforeController(final HttpServletRequest request, final HttpServletResponse response,
            final HandlerMethod handlerMethod) {
        if (!targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.CUSTOMER_SEGMENT_COOKIE)) {
            return true;
        }
        if (userService.isAnonymousUser(userService.getCurrentUser())) {
            return true;
        }

        customerSegmentCookieStrategy.setCookie(request, response);

        return true;
    }



    /**
     * @param targetFeatureSwitchFacade
     *            the targetFeatureSwitchFacade to set
     */
    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @param customerSegmentCookieStrategy
     *            the customerSegmentCookieStrategy to set
     */
    @Required
    public void setCustomerSegmentCookieStrategy(final CustomerSegmentCookieStrategy customerSegmentCookieStrategy) {
        this.customerSegmentCookieStrategy = customerSegmentCookieStrategy;
    }

}
