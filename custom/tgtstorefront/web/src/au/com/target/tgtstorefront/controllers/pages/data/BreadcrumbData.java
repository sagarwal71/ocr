/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.data;

/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class BreadcrumbData {
    private String key;
    private Object[] params;
    private String link;

    /**
     * @param key
     */
    public BreadcrumbData(final String key) {
        super();
        this.key = key;
    }

    /**
     * @param key
     * @param params
     * @param link
     */
    public BreadcrumbData(final String key, final Object[] params, final String link) {
        super();
        this.key = key;
        this.params = params;
        this.link = link;
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key
     *            the key to set
     */
    public void setKey(final String key) {
        this.key = key;
    }

    /**
     * @return the params
     */
    public Object[] getParams() {
        return params;
    }

    /**
     * @param params
     *            the params to set
     */
    public void setParams(final Object[] params) {
        this.params = params;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link
     *            the link to set
     */
    public void setLink(final String link) {
        this.link = link;
    }

}
