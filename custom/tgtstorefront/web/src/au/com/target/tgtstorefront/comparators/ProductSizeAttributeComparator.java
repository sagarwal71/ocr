/**
 * 
 */
package au.com.target.tgtstorefront.comparators;

import java.util.Comparator;

import org.apache.commons.lang.ObjectUtils;

import au.com.target.tgtfacades.product.data.TargetProductSizeData;


/**
 * @author rsamuel3
 *
 */
public class ProductSizeAttributeComparator implements Comparator<TargetProductSizeData> {

    /* (non-Javadoc)
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(final TargetProductSizeData productSizeData1, final TargetProductSizeData productSizeData2) {
        if (productSizeData1 == null && productSizeData2 == null) {
            return 0;
        }
        if (productSizeData1 == null) {
            return 1;
        }
        if (productSizeData2 == null) {
            return -1;
        }

        return ObjectUtils.compare(productSizeData1.getPosition(), productSizeData2.getPosition(), true);
    }
}
