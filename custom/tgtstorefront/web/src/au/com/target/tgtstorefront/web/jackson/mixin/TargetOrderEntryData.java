/**
 * 
 */
package au.com.target.tgtstorefront.web.jackson.mixin;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import au.com.target.tgtfacades.order.data.GiftRecipientData;


/**
 * @author htan3
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class TargetOrderEntryData {
    @JsonIgnore
    abstract String getAffiliateOfferCategory();

    @JsonIgnore
    abstract String getDepartmentName();

    @JsonIgnore
    abstract boolean isUpdateable();

    @JsonIgnore
    abstract List<GiftRecipientData> getRecipients();
}
