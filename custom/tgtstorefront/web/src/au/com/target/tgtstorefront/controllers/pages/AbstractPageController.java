/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.acceleratorservices.config.HostConfigService;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.commercefacades.storesession.data.CurrencyData;
import de.hybris.platform.commercefacades.storesession.data.LanguageData;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.util.UriUtils;

import au.com.target.endeca.infront.assemble.impl.EndecaPageAssemble;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaWrapperException;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.config.TargetSharedConfigFacade;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.user.data.TargetCustomerData;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.AbstractController;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.controllers.util.ProductDataHelper;
import au.com.target.tgtstorefront.util.PageTitleResolver;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;
import au.com.target.tgtwebcore.cache.service.AkamaiCacheConfigurationService;
import au.com.target.tgtwebcore.constants.TgtwebcoreConstants;
import au.com.target.tgtwebcore.enums.CachedPageType;
import au.com.target.tgtwebcore.model.AkamaiCacheConfigurationModel;


/**
 * Base controller for all page controllers. Provides common functionality for all page controllers.
 */
public abstract class AbstractPageController extends AbstractController {
    public static final String PAGE_ROOT = "pages/";
    public static final String CMS_PAGE_MODEL = "cmsPage";

    public static final String CMS_PAGE_TITLE = "pageTitle";
    public static final String CANONICAL_URL = "canonicalUrl";
    public static final String COMPRESSION_CONFIG_KEY_PREFIX = "enable.compression.css.js";

    public static final String TRACK_PAGE_VIEW_URL = "trackPageViewUrlOverride";
    protected static final String EXPEDITE_KEY = "expedite";
    protected static final String SPC_KEY = "spc";
    protected static final String TURNED_ON = "on";
    protected static final String EXPEDITED_YES = "yes";
    protected static final Logger LOG = Logger.getLogger(AbstractPageController.class);
    private static final String HEADER_EDGE_CONTROL = "Edge-Control";
    private static final String HEADER_EDGE_CONTROL_NO_STORE = "no-store";
    private static final String HEADER_EDGE_CONTROL_MAX_AGE = "max-age=";
    private static final String ANONYMOUS_CACHABLE = "anonymousCachable";

    public static enum PageType {
        Home("Home"), //
        ProductSearch("ProductSearch"), //
        Category("Category"), //
        Brand("Brand"), //
        Product("Product"), //
        Cart("Cart"), //
        OrderConfirmation("OrderConfirmation"), //
        Checkout("Checkout"), //
        CheckoutLogin("CheckoutLogin"), //
        CartLogin("CartLogin"), //
        DealCategory("DealCategory"), //
        Favourite("Favourite");

        private final String value;

        private PageType(final String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }


    @Resource(name = "cmsSiteService")
    private CMSSiteService cmsSiteService;

    @Resource(name = "cmsPageService")
    private CMSPageService cmsPageService;

    @Resource(name = "storeSessionFacade")
    private StoreSessionFacade storeSessionFacade;

    @Resource(name = "targetCustomerFacade")
    private TargetCustomerFacade customerFacade;

    @Resource(name = "pageTitleResolver")
    private PageTitleResolver pageTitleResolver;

    @Resource(name = "sessionService")
    private SessionService sessionService;

    @Resource(name = "hostConfigService")
    private HostConfigService hostConfigService;

    @Resource(name = "messageSource")
    private MessageSource messageSource;

    @Resource(name = "i18nService")
    private I18NService i18nService;

    @Resource(name = "siteConfigService")
    private SiteConfigService siteConfigService;

    @Resource(name = "cartFacade")
    private CartFacade cartFacade;

    @Resource(name = "akamaiCacheConfigurationService")
    private AkamaiCacheConfigurationService akamaiCacheConfigurationService;

    @Resource(name = "endecaPageAssemble")
    private EndecaPageAssemble endecaPageAssemble;

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Resource(name = "salesApplicationFacade")
    private SalesApplicationFacade salesApplicationFacade;

    @Resource(name = "targetSharedConfigFacade")
    private TargetSharedConfigFacade targetSharedConfigFacade;

    @Resource(name = "priceDataFactory")
    private PriceDataFactory priceDataFactory;

    protected SiteConfigService getSiteConfigService() {
        return siteConfigService;
    }

    protected CMSSiteService getCmsSiteService() {
        return cmsSiteService;
    }

    protected CMSPageService getCmsPageService() {
        return cmsPageService;
    }

    protected StoreSessionFacade getStoreSessionFacade() {
        return storeSessionFacade;
    }

    protected TargetCustomerFacade getCustomerFacade() {
        return customerFacade;
    }

    protected SessionService getSessionService() {
        return sessionService;
    }

    protected HostConfigService getHostConfigService() {
        return hostConfigService;
    }

    protected MessageSource getMessageSource() {
        return messageSource;
    }

    protected I18NService getI18nService() {
        return i18nService;
    }

    @ModelAttribute("languages")
    public Collection<LanguageData> getLanguages() {
        return storeSessionFacade.getAllLanguages();
    }

    @ModelAttribute("currencies")
    public Collection<CurrencyData> getCurrencies() {
        return storeSessionFacade.getAllCurrencies();
    }

    @ModelAttribute("currentLanguage")
    public LanguageData getCurrentLanguage() {
        return storeSessionFacade.getCurrentLanguage();
    }

    @ModelAttribute("currentCurrency")
    public CurrencyData getCurrentCurrency() {
        return storeSessionFacade.getCurrentCurrency();
    }

    @ModelAttribute("miniCartData")
    public CartData getCartData() {
        return cartFacade.getMiniCart();
    }

    @ModelAttribute("siteName")
    public String getSiteName() {
        final CMSSiteModel site = cmsSiteService.getCurrentSite();
        return site != null ? site.getName() : "";
    }

    @ModelAttribute("user")
    public TargetCustomerData getUser() {
        return (TargetCustomerData)customerFacade.getCurrentCustomer();
    }

    protected void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage) {
        if (model != null && cmsPage != null) {
            model.addAttribute(WebConstants.CMS_PAGE_MODEL, cmsPage);
            if (cmsPage instanceof ContentPageModel) {
                storeContentPageTitleInModel(model,
                        getPageTitleResolver().resolveContentPageTitle(cmsPage.getTitle()));
                model.addAttribute(CANONICAL_URL, ((ContentPageModel)cmsPage).getCanonicalUrl());
            }
        }
    }

    protected void storeContentPageTitleInModel(final Model model, final String title) {
        model.addAttribute(CMS_PAGE_TITLE, title);
    }

    protected String getViewForPage(final Model model) {
        if (model.containsAttribute(WebConstants.CMS_PAGE_MODEL)) {
            final AbstractPageModel page = (AbstractPageModel)model.asMap().get(WebConstants.CMS_PAGE_MODEL);
            if (page != null) {
                return getViewForPage(page);
            }
        }
        return null;
    }

    protected String getViewForPage(final AbstractPageModel page) {
        if (page != null) {
            final PageTemplateModel masterTemplate = page.getMasterTemplate();
            if (masterTemplate != null) {
                final String targetPage = cmsPageService.getFrontendTemplateName(masterTemplate);
                if (targetPage != null && !targetPage.isEmpty()) {
                    return PAGE_ROOT + targetPage;
                }
            }
        }
        return null;
    }

    /**
     * Checks request URL against properly resolved URL and returns null if url is proper or redirection string if not.
     * 
     * @param request
     *            - request that contains current URL
     * @param response
     *            - response to write "301 Moved Permanently" status to if redirected
     * @param resolvedUrl
     *            - properly resolved URL
     * @return null if url is properly resolved or redirection string if not
     * @throws UnsupportedEncodingException
     */
    protected String checkRequestUrl(final HttpServletRequest request, final HttpServletResponse response,
            final String resolvedUrl)
            throws UnsupportedEncodingException {

        final String requestURI = UriUtils.decode(request.getRequestURI(), StandardCharsets.UTF_8.toString());
        final String decoded = UriUtils.decode(resolvedUrl, StandardCharsets.UTF_8.toString());
        if (StringUtils.isNotEmpty(requestURI) && requestURI.endsWith(decoded)) {
            return null;
        }
        else {
            request.setAttribute(View.RESPONSE_STATUS_ATTRIBUTE, HttpStatus.MOVED_PERMANENTLY);
            final String queryString = request.getQueryString();
            if (queryString != null && !queryString.isEmpty()) {
                return UrlBasedViewResolver.REDIRECT_URL_PREFIX + resolvedUrl + "?" + queryString;
            }
            return UrlBasedViewResolver.REDIRECT_URL_PREFIX + resolvedUrl;
        }

    }

    protected ContentPageModel getContentPageForLabelOrId(final String labelOrId) throws CMSItemNotFoundException {
        String key = labelOrId;
        if (StringUtils.isEmpty(labelOrId)) {
            // Fallback to site home page
            final ContentPageModel homePage = cmsPageService.getHomepage();
            if (homePage != null) {
                key = cmsPageService.getLabelOrId(homePage);
            }
            else {
                // Fallback to site start page label
                final CMSSiteModel site = cmsSiteService.getCurrentSite();
                if (site != null) {
                    key = cmsSiteService.getStartPageLabelOrId(site);
                }
            }
        }

        // Actually resolve the label or id - running cms restrictions
        return cmsPageService.getPageForLabelOrId(key);
    }

    protected PageTitleResolver getPageTitleResolver() {
        return pageTitleResolver;
    }

    protected void storeContinueUrl(final HttpServletRequest request) {
        final StringBuilder url = new StringBuilder();
        url.append(request.getServletPath());
        final String queryString = request.getQueryString();
        if (queryString != null && !queryString.isEmpty()) {
            url.append('?').append(queryString);
        }
        getSessionService().setAttribute(WebConstants.CONTINUE_URL, url.toString());
    }

    protected void setUpMetaData(final Model model, final String metaKeywords, final String metaDescription) {
        model.addAttribute("metaKeywords", metaKeywords);
        model.addAttribute("metaDescription", metaDescription);
    }

    protected void setUpMetaDataForContentPage(final Model model, final ContentPageModel contentPage) {
        setUpMetaData(model, contentPage.getMetaKeywords(), contentPage.getMetaDescription());
        model.addAttribute("metaImage", contentPage.getMetaImage());
    }

    /**
     * Add page and metadata in model
     * 
     * @param model
     * @param pageLabel
     * @throws CMSItemNotFoundException
     */
    protected void storeCmsPageAndSetUpMetadata(final Model model, final String pageLabel)
            throws CMSItemNotFoundException {
        final ContentPageModel pageModel = getContentPageForLabelOrId(pageLabel);
        storeCmsPageInModel(model,
                pageModel);
        setUpMetaDataForContentPage(model,
                pageModel);

    }

    /**
     * sets up the required attributes to support Akamai caching. Sets the edge-control headers if akamai model exists
     * for a cachepagetype also sets up the anonymousCachable attribute in the model if akamai model exists and it is
     * enabled
     * 
     * If the model contains any messages, then caching will be explicitly disabled.
     * 
     * @param cachePageType
     * @param response
     * @param model
     */
    protected void addAkamaiCacheAttributes(final CachedPageType cachePageType, final HttpServletResponse response,
            final Model model) {
        if (cachePageType != null) {
            final AkamaiCacheConfigurationModel akamaiModel = akamaiCacheConfigurationService
                    .getAkamaiCacheConfiguration(cachePageType);
            if (isAkamaiAttributesValid(akamaiModel)) {
                if (GlobalMessages.hasMessages(model)) {
                    response.setHeader(HEADER_EDGE_CONTROL, HEADER_EDGE_CONTROL_NO_STORE);
                    model.addAttribute(ANONYMOUS_CACHABLE, Boolean.FALSE);
                }
                else {
                    final StringBuilder params = new StringBuilder();
                    if (StringUtils.isNotBlank(akamaiModel.getAppendParam())) {
                        params.append(akamaiModel.getAppendParam()).append(',');
                    }
                    params.append(HEADER_EDGE_CONTROL_MAX_AGE).append(akamaiModel.getMaxAge().toString());
                    response.setHeader(HEADER_EDGE_CONTROL, params.toString());
                    model.addAttribute(ANONYMOUS_CACHABLE, akamaiModel.getEnabled());
                }
            }
        }
    }

    /**
     * This method checks for if the model is null, is enabled and has both the parameters set(maxAge and appendParams).
     * 
     * @param akamaiModel
     * @return true if valid and false otherwise
     */
    private boolean isAkamaiAttributesValid(final AkamaiCacheConfigurationModel akamaiModel) {
        return akamaiModel != null && akamaiModel.getEnabled().booleanValue() && akamaiModel.getMaxAge() != null
                && akamaiModel.getMaxAge().intValue() > 0;
    }

    /**
     * Add meta for robots (no-index and no-follow)
     * 
     * @param model
     */
    protected void storeMetaRobotsNoIndexNoFollow(final Model model) {
        model.addAttribute("metaRobots", "noindex,nofollow");
    }

    /**
     * Populate recommended products details.
     *
     * @param request
     *            the request
     * @param endecaPageURI
     *            the endeca page uri
     */
    protected void populateRecommendedProductsDetails(final HttpServletRequest request, final String endecaPageURI) {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        searchStateData.setProductCodes(Arrays.asList(ProductDataHelper.getCurrentProduct(request)));
        try {
            request.setAttribute(WebConstants.RECOMMENDED_PRODUCTS_KEY,
                    endecaPageAssemble.getContentItemForPage(request, endecaPageURI, null, searchStateData));
        }
        catch (final TargetEndecaWrapperException e) {
            LOG.error(SplunkLogFormatter.formatMessage("Error thrown inside Endeca Assembler.",
                    TgtutilityConstants.ErrorCode.ERR_TGTSTORE_PRODUCT_SEARCH), e);
        }
    }

    protected void storeMetaRobotsNoIndex(final Model model) {
        model.addAttribute("metaRobots", "noindex");
    }

    protected void storeAnalyticsPageUrl(final Model model, final String url) {
        model.addAttribute(TRACK_PAGE_VIEW_URL, url);
    }

    protected String getMessageFromProperties(final String key) {
        return messageSource.getMessage(key, null, null, i18nService.getCurrentLocale());
    }

    protected String getMessageFromProperties(final String key, final String defaultMessage) {
        return messageSource.getMessage(key, null, defaultMessage, i18nService.getCurrentLocale());
    }

    protected void setPageNotCached(final HttpServletResponse response) {
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.setDateHeader("Expires", -1);
    }

    /**
     * @return the endecaPageAssemble
     */
    public EndecaPageAssemble getEndecaPageAssemble() {
        return endecaPageAssemble;
    }

    /**
     * This method determines whether the user should be sent to the SPC checkout.
     * 
     * @return boolean
     */
    protected boolean shouldGoToSPC() {
        if (salesApplicationFacade.isKioskApplication()) {
            return false;
        }
        return targetFeatureSwitchFacade.isSpcLoginAndCheckout();
    }

    /**
     * This method provides the zip payment Threshold Amount.
     *
     * @return PriceData
     */
    protected PriceData getZipPaymentThresholdAmount() {
        String zipPaymentThreshold = targetSharedConfigFacade
            .getConfigByCode(TgtFacadesConstants.ZIP_PAYMENT_THRESHOLD_AMOUNT);
        if (StringUtils.isBlank(zipPaymentThreshold)) {
            zipPaymentThreshold = "0";
        }
        return priceDataFactory.create(PriceDataType.BUY, new BigDecimal(zipPaymentThreshold),
            TgtCoreConstants.AUSTRALIAN_DOLLARS);
    }

    /**
     * @param model
     */
    protected void populateZipPaymentModal(final Model model) {
        if (targetFeatureSwitchFacade.isZipEnabled()) {
            try {
                final ContentPageModel page = cmsPageService.getPageForLabelOrId(TgtwebcoreConstants.ZIPPAYMENT_MODAL_ID);
                model.addAttribute("zipPaymentModal", page.getLabel());
            }
            catch (final CMSItemNotFoundException e) {
                model.addAttribute("zipPaymentModal", "");
            }
        }
    }
}
