/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.CMSComponentHelper;
import au.com.target.tgtwebcore.model.cms2.components.TargetRecentlyViewedProductsComponentModel;


@Controller("TargetRecentlyViewedProductsComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.TARGET_RECENTLY_VIEWED_PRODUCTS_COMPONENT)
public class TargetRecentlyViewedProductsComponentController extends
        AbstractCMSComponentController<TargetRecentlyViewedProductsComponentModel> {


    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
            final TargetRecentlyViewedProductsComponentModel component) {

        final int limit = CMSComponentHelper.safeQuantity(component.getLimit());

        model.addAttribute("title", component.getTitle());
        model.addAttribute("limit", Integer.valueOf(limit));

    }
}
