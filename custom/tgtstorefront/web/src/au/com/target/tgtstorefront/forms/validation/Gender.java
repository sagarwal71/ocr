/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation;

import static java.lang.annotation.ElementType.FIELD;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtstorefront.forms.validation.validator.GenderValidator;


/**
 * @author bhuang3
 * 
 */
@Documented
@Constraint(validatedBy = { GenderValidator.class })
@Target({ FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Gender {

    String message() default StringUtils.EMPTY;

    Class<?>[] groups() default {};

    Class<?>[] payload() default {};

    boolean mandatory() default false;
}
