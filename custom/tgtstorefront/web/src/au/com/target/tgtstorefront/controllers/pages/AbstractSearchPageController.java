/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.BreadcrumbData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanPropertyValueEqualsPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.ui.Model;


/**
 */
@SuppressWarnings("deprecation")
public abstract class AbstractSearchPageController extends AbstractPageController {
    protected static final Map<String, ViewAsType> PAGE_TO_VIEW_AS_TYPE_MAP = new HashMap<String, ViewAsType>();
    protected static final String CATEGORY_SHOW_MORE_LIMIT = "tgtstorefront.categoryFacet.showMore.limit";

    public static enum PageSize {
        Thirty("30", 30), Ninety("90", 90);

        private static final Map<Integer, PageSize> PAGE_SIZE_VALUE_LOOKUP_MAP = new HashMap<Integer, PageSize>();

        private final String displayValue;
        private final int value;

        static {
            for (final PageSize pageSize : EnumSet.allOf(PageSize.class)) {
                PAGE_SIZE_VALUE_LOOKUP_MAP.put(Integer.valueOf(pageSize.getValue()), pageSize);
            }
        }

        private PageSize(final String displayValue, final int value) {
            this.displayValue = displayValue;
            this.value = value;
        }

        public static PageSize getDefault() {
            return Thirty;
        }

        public String getDisplayValue() {
            return displayValue;
        }

        public int getValue() {
            return value;
        }

        public static PageSize getByValue(final int value) {
            return PAGE_SIZE_VALUE_LOOKUP_MAP.get(Integer.valueOf(value));
        }

        public static boolean isValidValue(final int value) {
            return PAGE_SIZE_VALUE_LOOKUP_MAP.containsValue(
                    PageSize.getByValue(value));
        }
    }

    public static enum ViewAsType {
        Grid("grid", "Grid"), Look("look", "Look Book");

        private static final Map<String, ViewAsType> VIEW_AS_TYPE_VALUE_LOOKUP_MAP = new HashMap<String, ViewAsType>();

        private final String value;
        private final String displayValue;

        static {
            for (final ViewAsType viewAsType : EnumSet.allOf(ViewAsType.class)) {
                VIEW_AS_TYPE_VALUE_LOOKUP_MAP.put(viewAsType.getValue(), viewAsType);
            }
        }

        private ViewAsType(final String value, final String displayValue) {
            this.value = value;
            this.displayValue = displayValue;
        }

        public String getValue() {
            return value;
        }

        public String getDisplayValue() {
            return displayValue;
        }

        public static ViewAsType getDefault() {
            return ViewAsType.Grid;
        }

        public static ViewAsType getByValue(final String value) {
            return VIEW_AS_TYPE_VALUE_LOOKUP_MAP.get(value);
        }

        public static boolean isValidValue(final String value) {
            return VIEW_AS_TYPE_VALUE_LOOKUP_MAP.containsValue(
                    ViewAsType.getByValue(value));
        }
    }

    protected int optimizePageSizeFromURL(final String pageSize) {
        return NumberUtils.toInt(pageSize, 0);
    }

    protected PageableData createPageableData(final int pageNumber, final int pageSize, final String sortCode) {
        final PageableData pageableData = new PageableData();
        pageableData.setCurrentPage(pageNumber);
        pageableData.setSort(sortCode);
        pageableData.setPageSize(getPageSize(pageSize).value);

        return pageableData;
    }

    /**
     * 
     * @param model
     * @param searchPageData
     */
    protected void populateModel(final Model model, final SearchPageData<?> searchPageData) {
        model.addAttribute("searchPageData", searchPageData);
        model.addAttribute("itemsPagePageOptions", PageSize.values());
        model.addAttribute("viewAsTypes", ViewAsType.values());
        model.addAttribute("categoryShowMoreLimit", getSiteConfigService().getProperty(CATEGORY_SHOW_MORE_LIMIT));
    }

    protected Map<String, FacetData<SearchStateData>> convertBreadcrumbsToFacets(
            final List<BreadcrumbData<SearchStateData>> breadcrumbs) {
        final Map<String, FacetData<SearchStateData>> facets = new HashMap<String, FacetData<SearchStateData>>();
        if (breadcrumbs == null) {
            return facets;
        }

        for (final BreadcrumbData<SearchStateData> breadcrumb : breadcrumbs) {
            FacetData<SearchStateData> facet = facets.get(breadcrumb.getFacetName());
            if (facet == null) {
                facet = new FacetData<>();
                facet.setName(breadcrumb.getFacetName());
                facet.setCode(breadcrumb.getFacetCode());
                facets.put(breadcrumb.getFacetName(), facet);
            }

            final List<FacetValueData<SearchStateData>> facetValues = facet.getValues() != null
                    ? new ArrayList<FacetValueData<SearchStateData>>(
                            facet.getValues())
                    : new ArrayList<FacetValueData<SearchStateData>>();
            final FacetValueData<SearchStateData> facetValueData = new FacetValueData<>();
            facetValueData.setSelected(true);
            facetValueData.setName(breadcrumb.getFacetValueName());
            facetValueData.setCode(breadcrumb.getFacetValueCode());
            facetValueData.setCount(0L);
            facetValueData.setQuery(breadcrumb.getRemoveQuery());
            facetValues.add(facetValueData);
            facet.setValues(facetValues);
        }
        return facets;
    }

    protected List<FacetData<SearchStateData>> refineFacets(final List<FacetData<SearchStateData>> facets,
            final Map<String, FacetData<SearchStateData>> selectedFacets) {
        final List<FacetData<SearchStateData>> refinedFacets = new ArrayList<FacetData<SearchStateData>>();
        for (final FacetData<SearchStateData> facet : facets) {
            facet.setTopValues(Collections.<FacetValueData<SearchStateData>> emptyList());
            final List<FacetValueData<SearchStateData>> facetValues = new ArrayList<FacetValueData<SearchStateData>>(
                    facet.getValues());

            for (final FacetValueData<SearchStateData> facetValueData : facetValues) {
                if (selectedFacets.containsKey(facet.getName())) {
                    final boolean foundFacetWithName = null != CollectionUtils.find(selectedFacets.get(facet.getName())
                            .getValues(),
                            new BeanPropertyValueEqualsPredicate("name", facetValueData.getName(), true));
                    facetValueData.setSelected(foundFacetWithName);
                }
            }

            if (selectedFacets.containsKey(facet.getName())) {
                facetValues.addAll(selectedFacets.get(facet.getName()).getValues());
                selectedFacets.remove(facet.getName());
            }

            refinedFacets.add(facet);
        }

        if (!selectedFacets.isEmpty()) {
            refinedFacets.addAll(selectedFacets.values());
        }

        return refinedFacets;
    }

    public static class SearchResultsData<RESULT> {
        private List<RESULT> results;
        private PaginationData pagination;

        public List<RESULT> getResults() {
            return results;
        }

        public void setResults(final List<RESULT> results) {
            this.results = results;
        }

        public PaginationData getPagination() {
            return pagination;
        }

        public void setPagination(final PaginationData pagination) {
            this.pagination = pagination;
        }
    }

    protected PageSize getPageSize(final int pageSize) {
        PageSize actualPageSize = PageSize.getByValue(pageSize);

        if (actualPageSize == null) {
            actualPageSize = PageSize.getDefault();
        }

        return actualPageSize;
    }

    protected PageSize getDefaultPageSize() {
        return PageSize.getDefault();
    }

    protected ViewAsType getViewAsType(final String viewAsType) {
        ViewAsType actualViewAsType = ViewAsType.getByValue(viewAsType);

        if (actualViewAsType == null) {
            actualViewAsType = ViewAsType.getDefault();
        }

        return actualViewAsType;
    }

    protected void populateActualViewAsType(final String viewPage, final Model model) {
        final ViewAsType pageViewAsType = PAGE_TO_VIEW_AS_TYPE_MAP.get(viewPage);
        if (pageViewAsType != null) {
            model.addAttribute("actualViewAsType", pageViewAsType.getValue());
        }
    }

    protected PaginationData createPaginationData(final int pageSize, final int page, final int totalNumberOfResults) {
        final PaginationData paginationData = new PaginationData();

        // Set the page size and and don't allow it to be less than 1
        paginationData.setPageSize(pageSize);

        //Set total number for results
        paginationData.setTotalNumberOfResults(totalNumberOfResults);

        // Calculate the number of pages
        paginationData.setNumberOfPages((int)Math.ceil(((double)paginationData.getTotalNumberOfResults())
                / paginationData.getPageSize()));

        // Work out the current page, fixing any invalid page values
        paginationData.setCurrentPage(Math.max(0,
                Math.min(paginationData.getNumberOfPages(), page)));

        return paginationData;
    }
}
