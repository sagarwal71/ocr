/**
 *
 */
package au.com.target.tgtstorefront.interceptors.beforeview;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;


/**
 * If there were modifications to the cart during login, then add a global message.
 *
 */
public class LoginCartModificationBeforeViewHandler implements BeforeViewHandler {

    @Override
    public void beforeView(final HttpServletRequest request, final HttpServletResponse response,
            final ModelAndView modelAndView) {

        final Object isCartEntryModified = request.getSession().getAttribute(
                TgtFacadesConstants.LOGIN_CART_MODIFICATIONS_SESSION_ATTRIBUTE);
        if (isCartEntryModified == null || !(isCartEntryModified instanceof Boolean)) {
            return;
        }
        if (((Boolean)isCartEntryModified).booleanValue()) {

            GlobalMessages.addInfoMessage(modelAndView.getModel(), "login.cart.modifications");
        }

        request.getSession().removeAttribute(TgtFacadesConstants.LOGIN_CART_MODIFICATIONS_SESSION_ATTRIBUTE);
    }

}
