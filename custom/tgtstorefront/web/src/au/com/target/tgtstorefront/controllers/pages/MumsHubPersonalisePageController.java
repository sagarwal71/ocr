package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.TitleData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtfacades.customer.TargetCustomerSubscriptionFacade;
import au.com.target.tgtmail.dto.TargetCustomerChildDetailsDto;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionSource;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionType;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.checkout.AbstractCheckoutController.SelectOption;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.forms.MumsHubChildForm;
import au.com.target.tgtstorefront.forms.MumsHubPersonaliseForm;
import au.com.target.tgtstorefront.util.CommonUtils;


/**
 * Controller for Mumshub Personalise page.
 */
@Controller
public class MumsHubPersonalisePageController extends AbstractPageController
{
    private static final Logger LOG = Logger.getLogger(MumsHubPersonalisePageController.class);
    private static final String SITE_CMS_LABEL_MUMS_HUB_PAGE = "mums-hub-personalise";

    @Resource(name = "userFacade")
    private UserFacade userFacade;

    @Resource(name = "targetCustomerSubscriptionFacade")
    private TargetCustomerSubscriptionFacade targetCustomerSubscriptionFacade;

    @RequestMapping(value = ControllerConstants.MUMS_HUB_PERSONALISE, method = RequestMethod.GET)
    public String getEnewsSubscribe(final Model model, final HttpServletRequest request,
            final RedirectAttributes redirectAttributes)
            throws CMSItemNotFoundException
    {
        final String emailInSession = (String)(request.getSession().getAttribute(
                ControllerConstants.SESSION_PARAM_MUMS_HUB_SUBSCRIPTION_EMAIL));
        if (StringUtils.isEmpty(emailInSession)) {
            GlobalMessages.addFlashErrorMessage(redirectAttributes, "mumsHubSubscription.request.session.timeout");
            return redirectToMumsHubLandingPage();
        }
        model.addAttribute("mumsHubPersonaliseForm", new MumsHubPersonaliseForm());
        storeCmsPageInModel(model, getContentPageForLabelOrId(SITE_CMS_LABEL_MUMS_HUB_PAGE));
        return ControllerConstants.Views.Pages.Account.ACCOUNT_MUMS_HUB_PERSONALISE_PAGE;
    }

    @RequestMapping(value = ControllerConstants.MUMS_HUB_PERSONALISE, method = RequestMethod.POST)
    public String doSubscribe(@Valid final MumsHubPersonaliseForm mumsHubPersonaliseForm,
            final BindingResult bindingResult, final Model model, final HttpServletRequest request,
            final RedirectAttributes redirectAttributes
            ) throws CMSItemNotFoundException
    {
        final String emailInSession = (String)(request.getSession().getAttribute(
                ControllerConstants.SESSION_PARAM_MUMS_HUB_SUBSCRIPTION_EMAIL));
        if (StringUtils.isEmpty(emailInSession)) {
            GlobalMessages.addFlashErrorMessage(redirectAttributes, "mumsHubSubscription.request.session.timeout");
            return redirectToMumsHubLandingPage();
        }

        if (!bindingResult.hasErrors()) {
            mumsHubPersonaliseForm.setEmail(emailInSession);
            targetCustomerSubscriptionFacade.processCustomerPersonalDetails(transformFormToDto(mumsHubPersonaliseForm));
            request.getSession().removeAttribute(ControllerConstants.SESSION_PARAM_MUMS_HUB_SUBSCRIPTION_EMAIL);
            GlobalMessages.addFlashConfMessage(redirectAttributes, "mumsHubSubscription.request.success.message");
            return redirectToMumsHubLandingPage();
        }
        model.addAttribute("mumsHubPersonaliseForm", mumsHubPersonaliseForm);
        storeCmsPageInModel(model, getContentPageForLabelOrId(SITE_CMS_LABEL_MUMS_HUB_PAGE));
        GlobalMessages.addErrorMessage(model, "mumsHubSubscription.request.formentry.invalid");
        return ControllerConstants.Views.Pages.Account.ACCOUNT_MUMS_HUB_PERSONALISE_PAGE;
    }

    @ModelAttribute("titles")
    public Collection<TitleData> getTitles() {
        return userFacade.getTitles();
    }

    /**
     * 
     * @return 12 months
     */
    @ModelAttribute("months")
    public List<SelectOption> getMonths()
    {
        return WebConstants.Payment.MONTHS;
    }

    /**
     * 
     * @return days of month
     */
    @ModelAttribute("daysOfMonth")
    public List<SelectOption> getDaysOfMonth()
    {
        return WebConstants.Payment.DAYS_OF_MONTH;
    }

    /**
     * 
     * @return due date years
     */
    @ModelAttribute("dueDateYears")
    public List<SelectOption> getDueDateYear() {
        return WebConstants.UserInformation.DUE_DAY_YEARS;
    }

    /**
     * 
     * @return birthday years
     */
    @ModelAttribute("birthYears")
    public List<SelectOption> getbirthYears() {
        return WebConstants.Payment.BIRTH_YEARS;
    }

    /**
     * 
     * @return genders
     */
    @ModelAttribute("genders")
    public List<SelectOption> getGenders() {
        return WebConstants.UserInformation.GENDERS;
    }

    /**
     * 
     * @return baby genders
     */
    @ModelAttribute("babyGenders")
    public List<SelectOption> getBabyGenders() {
        return WebConstants.UserInformation.BABY_GENDERS;
    }

    /**
     * 
     * @return expected baby genders
     */
    @ModelAttribute("expectedBabyGenders")
    public List<SelectOption> getExpectedBabyGenders() {
        return WebConstants.UserInformation.EXPECTED_BABY_GENDERS;
    }

    /**
     * transform form object to dto
     * 
     * @param form
     * @return TargetCustomerSubscriptionRequestDto
     */
    protected TargetCustomerSubscriptionRequestDto transformFormToDto(final MumsHubPersonaliseForm form) {
        if (form != null) {
            final TargetCustomerSubscriptionRequestDto dto = new TargetCustomerSubscriptionRequestDto();
            dto.setUpdateCustomerPersonalDetails(true);
            dto.setSubscriptionType(TargetCustomerSubscriptionType.MUMS_HUB);
            dto.setCustomerEmail(form.getEmail());
            dto.setTitle(form.getTitleCode());
            dto.setFirstName(form.getFirstName());
            dto.setLastName(form.getLastName());
            dto.setGender(form.getGender());
            dto.setCustomerSubscriptionSource(TargetCustomerSubscriptionSource.MUMSHUB);
            dto.setBirthday(CommonUtils.transformThreePartDateFormToDate(form.getDateOfBirth()));
            dto.setDueDate(CommonUtils.transformThreePartDateFormToDate(form.getDueDate()));
            dto.setBabyGender(form.getExpectedGender());
            final List<MumsHubChildForm> childFormList = form.getChildren();
            if (CollectionUtils.isNotEmpty(childFormList)) {
                final int childFormSize = childFormList.size() > 5 ? 5 : childFormList.size();
                final List<TargetCustomerChildDetailsDto> childrenDetails = new ArrayList<>();
                for (int i = 0; i < childFormSize; i++) {
                    final TargetCustomerChildDetailsDto childDetailsDto = new TargetCustomerChildDetailsDto();
                    childDetailsDto.setFirstName(childFormList.get(i).getFirstName());
                    childDetailsDto.setGender(childFormList.get(i).getGender());
                    childDetailsDto
                            .setBirthday(CommonUtils.transformThreePartDateFormToDate(childFormList.get(i)
                                    .getDateOfBirth()));
                    childrenDetails.add(childDetailsDto);
                }
                dto.setChildrenDetails(childrenDetails);
            }
            return dto;
        }
        return null;
    }

    /**
     * redirect to mumshub landing page. if cms page cannot be found, redirect to the home page
     * 
     * @return String
     */
    protected String redirectToMumsHubLandingPage() {
        try {
            getContentPageForLabelOrId(ControllerConstants.MUMS_HUB_LANDING);
            return ControllerConstants.Redirection.MUMS_HUB_LANDING;
        }
        catch (final CMSItemNotFoundException e) {
            LOG.error("Cannot find the mums hub landing page by label " + ControllerConstants.MUMS_HUB_LANDING, e);
            return ControllerConstants.Redirection.HOME;
        }
    }

}