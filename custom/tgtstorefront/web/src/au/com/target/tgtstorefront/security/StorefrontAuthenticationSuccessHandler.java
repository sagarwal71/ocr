/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.security;

import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commerceservices.enums.UiExperienceLevel;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import au.com.target.tgtfacades.login.TargetAuthenticationFacade;
import au.com.target.tgtfacades.order.TargetOrderFacade;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * Success handler initializing user settings and ensuring the cart is handled correctly
 */
public class StorefrontAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    private CustomerFacade customerFacade;

    private UiExperienceService uiExperienceService;

    private TargetAuthenticationFacade targetAuthenticationFacade;

    private TargetOrderFacade targetOrderFacade;

    private boolean alwaysUseDefaultTargetUrlForMobile = false;

    private boolean alwaysUseDefaultTargetUrlForDesktop = false;

    public UiExperienceService getUiExperienceService() {
        return uiExperienceService;
    }

    @Required
    public void setUiExperienceService(final UiExperienceService uiExperienceService) {
        this.uiExperienceService = uiExperienceService;
    }

    @Override
    public void onAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response,
            final Authentication authentication) throws IOException, ServletException {
        getTargetAuthenticationFacade().resetFailedLoginAttemptCounter(request.getParameter("j_username"));

        getCustomerFacade().loginSuccess();
        super.onAuthenticationSuccess(request, response, authentication);
        linkOrderToCustomer(request.getSession());
    }

    /**
     * Link current order to currently signed in user
     * 
     * @param session
     */
    private void linkOrderToCustomer(final HttpSession session) {
        final String orderId = (String)session.getAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE);
        if (StringUtils.isNotBlank(orderId)) {
            targetOrderFacade.linkOrderToCustomer(orderId);
        }
    }

    protected CustomerFacade getCustomerFacade() {
        return customerFacade;
    }

    @Required
    public void setCustomerFacade(final CustomerFacade customerFacade) {
        this.customerFacade = customerFacade;
    }

    /*
     * @see org.springframework.security.web.authentication.AbstractAuthenticationTargetUrlRequestHandler#
     * isAlwaysUseDefaultTargetUrl()
     */
    @Override
    protected boolean isAlwaysUseDefaultTargetUrl() {
        if (UiExperienceLevel.DESKTOP.equals(uiExperienceService.getUiExperienceLevel())) {
            return isAlwaysUseDefaultTargetUrlForDesktop();
        }
        else {
            return isAlwaysUseDefaultTargetUrlForMobile();
        }
    }


    public boolean isAlwaysUseDefaultTargetUrlForMobile() {
        return alwaysUseDefaultTargetUrlForMobile;
    }

    public void setAlwaysUseDefaultTargetUrlForMobile(final boolean alwaysUseDefaultTargetUrlForMobile) {
        this.alwaysUseDefaultTargetUrlForMobile = alwaysUseDefaultTargetUrlForMobile;
    }

    public boolean isAlwaysUseDefaultTargetUrlForDesktop() {
        return alwaysUseDefaultTargetUrlForDesktop;
    }

    public void setAlwaysUseDefaultTargetUrlForDesktop(final boolean alwaysUseDefaultTargetUrlForDesktop) {
        this.alwaysUseDefaultTargetUrlForDesktop = alwaysUseDefaultTargetUrlForDesktop;
    }

    /**
     * @return the targetAuthenticationFacade
     */
    protected TargetAuthenticationFacade getTargetAuthenticationFacade() {
        return targetAuthenticationFacade;
    }

    /**
     * @param targetAuthenticationFacade
     *            the targetAuthenticationFacade to set
     */
    @Required
    public void setTargetAuthenticationFacade(final TargetAuthenticationFacade targetAuthenticationFacade) {
        this.targetAuthenticationFacade = targetAuthenticationFacade;
    }

    /**
     * @param targetOrderFacade
     *            the targetOrderFacade to set
     */
    @Required
    public void setTargetOrderFacade(final TargetOrderFacade targetOrderFacade) {
        this.targetOrderFacade = targetOrderFacade;
    }
}
