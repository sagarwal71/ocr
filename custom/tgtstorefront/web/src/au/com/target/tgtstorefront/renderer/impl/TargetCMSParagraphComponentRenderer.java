/**
 * 
 */
package au.com.target.tgtstorefront.renderer.impl;

import de.hybris.platform.acceleratorcms.component.renderer.impl.CMSParagraphComponentRenderer;
import de.hybris.platform.cms2.model.contents.components.CMSParagraphComponentModel;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.StringUtils;


/**
 * @author thomasadolfsson
 *
 */
public class TargetCMSParagraphComponentRenderer extends CMSParagraphComponentRenderer {

    /* (non-Javadoc)
     * @see de.hybris.platform.acceleratorcms.component.renderer.impl.CMSParagraphComponentRenderer#renderComponent(javax.servlet.jsp.PageContext, de.hybris.platform.cms2.model.contents.components.CMSParagraphComponentModel)
     */
    @Override
    public void renderComponent(final PageContext pageContext, final CMSParagraphComponentModel component)
            throws ServletException, IOException {
        final JspWriter out = pageContext.getOut();
        out.write(component.getContent() == null ? StringUtils.EMPTY : component.getContent());
    }

}
