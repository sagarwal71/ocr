/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.account;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.target.tgtstorefront.annotations.RequireHardLogIn;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * @author gsing236
 *
 */
@Controller
@RequireHardLogIn
public class AccountOrderDetailsIpgReturnController extends AbstractAccountController {

    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_IPG + ControllerConstants.MY_ACCOUNT_ORDER_CODE
            + ControllerConstants.MY_ACCOUNT_ORDER_UPDATE_CARD_IPG, method = RequestMethod.POST)
    public String fromIpg(
            @RequestParam(value = ControllerConstants.IPG_PARAM_TOKEN) final String ipgToken,
            @RequestParam(value = ControllerConstants.IPG_PARAM_QS, required = false) final String qsParam,
            @PathVariable("orderCode") final String orderCode,
            final Model model) {

        if (ControllerConstants.Ipg.IPG_ABORT_ACTION.equals(qsParam)) {
            model.addAttribute("postMessageActionType", ControllerConstants.Ipg.IPG_PAYMENT_TYPE_ABORT);
        }
        else {
            final boolean isSuccessfull = targetOrderFacade.isUpdateCreditCardDetailsSuccessful(orderCode, ipgToken);
            if (isSuccessfull) {
                model.addAttribute("postMessageActionType", ControllerConstants.Ipg.IPG_PAYMENT_TYPE_COMPLETE);
            }
            else {
                model.addAttribute("postMessageActionType", ControllerConstants.Ipg.IPG_PAYMENT_TYPE_CANCEL);
            }
        }
        final String ipgRedirectUrl = ControllerConstants.MY_ACCOUNT + ControllerConstants.MY_ACCOUNT_ORDER + orderCode;
        model.addAttribute("ipgRedirectUrl", ipgRedirectUrl);

        return ControllerConstants.Views.Pages.Checkout.CHECKOUT_IPG_RETURN_PAGE;

    }

}
