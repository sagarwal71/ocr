/**
 * 
 */
package au.com.target.tgtstorefront.controllers.kiosk;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.tgtstorefront.controllers.AbstractController;
import au.com.target.tgtstorefront.security.TargetRememberMeServices;


/**
 * @author rmcalave
 *
 */
@Controller
@RequestMapping("/~/k/logout")
public class KioskLogoutController extends AbstractController {

    @Resource(name = "securityContextLogoutHandler")
    private SecurityContextLogoutHandler securityContextLogoutHandler;

    @Resource(name = "rememberMeServices")
    private TargetRememberMeServices targetRememberMeServices;

    @RequestMapping(method = RequestMethod.GET)
    public String logout(final HttpServletRequest request, final HttpServletResponse response) {
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        securityContextLogoutHandler.logout(request, response, auth);
        targetRememberMeServices.logout(request, response, auth);

        return "forward:/~/k/reset";
    }

}
