/**
 * 
 */
package au.com.target.tgtstorefront.tags;

import de.hybris.platform.core.Registry;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import au.com.target.tgtfacades.customer.TargetCustomerFacade;


/**
 * @author smudumba
 * 
 */
public class InvalidateSessionTag extends SimpleTagSupport {

    private static TargetCustomerFacade targetCustomerFacade =
            (TargetCustomerFacade)Registry.getApplicationContext().getBean("targetCustomerFacade");

    /* 
     * Clear current user session.
     */
    @Override
    public void doTag() throws JspException {

        targetCustomerFacade.invalidateUserSession();
    }

}
