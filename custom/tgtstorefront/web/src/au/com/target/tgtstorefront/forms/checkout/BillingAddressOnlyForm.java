/**
 * 
 */
package au.com.target.tgtstorefront.forms.checkout;

import javax.validation.Valid;

import au.com.target.tgtstorefront.forms.BillingAddressForm;


/**
 * 
 * @author jjayawa1
 * 
 */
public class BillingAddressOnlyForm extends AbstractPaymentDetailsForm {

    @Valid
    private BillingAddressForm billingAddress;

    /**
     * Default Constructor
     */
    public BillingAddressOnlyForm() {
        super();
    }

    public BillingAddressOnlyForm(final String action, final String cardType) {
        setAction(action);
        setCardType(cardType);
    }

    /**
     * @return the billingAddress
     */
    public BillingAddressForm getBillingAddress() {
        return billingAddress;
    }

    /**
     * @param billingAddress
     *            the billingAddress to set
     */
    public void setBillingAddress(final BillingAddressForm billingAddress) {
        this.billingAddress = billingAddress;
    }
}
