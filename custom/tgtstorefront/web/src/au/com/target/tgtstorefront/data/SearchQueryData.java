/**
 * 
 */
package au.com.target.tgtstorefront.data;

import java.io.Serializable;
import java.util.List;

import au.com.target.tgtcore.model.BrandModel;


/**
 * @author rsamuel3
 * 
 */
public class SearchQueryData implements Serializable {
    private String freeTextSearch;
    private String categoryCode;
    private List<SearchQueryTermData> filterTerms;
    private String sort;
    private BrandModel brand;

    /**
     * @return the freeTextSearch
     */
    public String getFreeTextSearch() {
        return freeTextSearch;
    }

    /**
     * @param freeTextSearch
     *            the freeTextSearch to set
     */
    public void setFreeTextSearch(final String freeTextSearch) {
        this.freeTextSearch = freeTextSearch;
    }

    /**
     * @return the categoryCode
     */
    public String getCategoryCode() {
        return categoryCode;
    }

    /**
     * @param categoryCode
     *            the categoryCode to set
     */
    public void setCategoryCode(final String categoryCode) {
        this.categoryCode = categoryCode;
    }

    /**
     * @return the filterTerms
     */
    public List<SearchQueryTermData> getFilterTerms() {
        return filterTerms;
    }

    /**
     * @param filterTerms
     *            the filterTerms to set
     */
    public void setFilterTerms(final List<SearchQueryTermData> filterTerms) {
        this.filterTerms = filterTerms;
    }

    /**
     * @return the sort
     */
    public String getSort() {
        return sort;
    }

    /**
     * @param sort
     *            the sort to set
     */
    public void setSort(final String sort) {
        this.sort = sort;
    }

    /**
     * @return the brand
     */
    public BrandModel getBrand() {
        return brand;
    }

    /**
     * @param brand
     *            the brand to set
     */
    public void setBrand(final BrandModel brand) {
        this.brand = brand;
    }
}
