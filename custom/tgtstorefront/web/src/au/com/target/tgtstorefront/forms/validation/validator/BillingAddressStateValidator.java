/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import de.hybris.platform.servicelayer.i18n.I18NService;

import javax.annotation.Resource;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtstorefront.forms.BillingAddressForm;


/**
 * @author asingh78
 * 
 */
@Component("billingAddressStateValidator")
public class BillingAddressStateValidator implements Validator {


    @Resource(name = "messageSource")
    protected MessageSource messageSource;

    @Resource(name = "i18nService")
    protected I18NService i18nService;

    /* (non-Javadoc)
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(final Class<?> aClass) {
        return BillingAddressForm.class.equals(aClass);
    }


    /* (non-Javadoc)
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(final Object object, final Errors errors) {
        final BillingAddressForm form = (BillingAddressForm)object;
        if (StringUtils.isBlank(form.getState())) {
            errors.rejectValue(
                    "billingAddress.state",
                    "field.invalid.empty",
                    new Object[] { BooleanUtils.toIntegerObject(FieldTypeEnum.state.getConsonant()),
                            messageSource.getMessage("field.state", null,
                                    i18nService.getCurrentLocale()) }, null);
        }
        else if ((TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA.equalsIgnoreCase(form.getCountryIso()) && "N/A"
                .equalsIgnoreCase(form.getState()))
                || (!TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA.equalsIgnoreCase(form.getCountryIso()) && !"N/A"
                        .equalsIgnoreCase(form.getState()))) {
            errors.rejectValue(
                    "billingAddress.state",
                    "field.invalid.notAvailable",
                    new Object[] { messageSource.getMessage("field.state", null,
                            i18nService.getCurrentLocale()) }, null);
        }

    }
}
