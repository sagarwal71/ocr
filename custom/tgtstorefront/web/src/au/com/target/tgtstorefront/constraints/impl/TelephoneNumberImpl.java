/**
 * 
 */
package au.com.target.tgtstorefront.constraints.impl;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.CharUtils;

import au.com.target.tgtstorefront.constraints.TelephoneNumber;


/**
 * @author rmcalave
 * 
 */
public class TelephoneNumberImpl implements ConstraintValidator<TelephoneNumber, String> {
    private static final Pattern PHONE_NUMBER_PATTERN = Pattern.compile("[0-9()\\-+][0-9()\\-]*");

    private int minDigits;
    private int maxDigits;

    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
     */
    @Override
    public void initialize(final TelephoneNumber telephoneNumber) {
        minDigits = telephoneNumber.minDigits();
        maxDigits = telephoneNumber.maxDigits();
    }

    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
     */
    @Override
    public boolean isValid(final String telephoneNumber, final ConstraintValidatorContext constraintContext) {
        if (telephoneNumber == null) {
            return true;
        }

        if (!PHONE_NUMBER_PATTERN.matcher(telephoneNumber).matches()) {
            return false;
        }

        int numberOfDigits = 0;
        for (final char character : telephoneNumber.toCharArray()) {
            if (CharUtils.isAsciiNumeric(character)) {
                numberOfDigits++;
            }
        }

        if (numberOfDigits < minDigits || numberOfDigits > maxDigits) {
            return false;
        }

        return true;
    }

}
