/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import au.com.target.tgtfacades.login.TargetAuthenticationFacade;


public class LoginAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler
{
    private TargetAuthenticationFacade targetAuthenticationFacade;

    @Override
    public void onAuthenticationFailure(final HttpServletRequest request, final HttpServletResponse response,
            final AuthenticationException exception) throws IOException, ServletException
    {
        final String username = request.getParameter("j_username");

        // Store the j_username in the session
        request.getSession().setAttribute("SPRING_SECURITY_LAST_USERNAME", username);

        // Increment the failed login attempts counter
        if (StringUtils.isNotBlank(username)) {
            getTargetAuthenticationFacade().incrementFailedLoginAttemptCounter(username);
        }

        super.onAuthenticationFailure(request, response, exception);
    }

    /**
     * @return the targetAuthenticationFacade
     */
    protected TargetAuthenticationFacade getTargetAuthenticationFacade() {
        return targetAuthenticationFacade;
    }

    /**
     * @param targetAuthenticationFacade
     *            the targetAuthenticationFacade to set
     */
    @Required
    public void setTargetAuthenticationFacade(final TargetAuthenticationFacade targetAuthenticationFacade) {
        this.targetAuthenticationFacade = targetAuthenticationFacade;
    }
}
