package au.com.target.tgtstorefront.data.sitemap;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

import org.joda.time.DateTime;


/**
 * Represents a single {@code &lt;url&gt;} node in site map.
 * 
 * @see SiteMapData
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SiteMapUrlData {

    /**
     * Enumerates allowed values for page content change frequency.
     */
    @XmlEnum
    public static enum ChangeFrequency {
        @XmlEnumValue("always")
        ALWAYS,
        @XmlEnumValue("hourly")
        HOURLY,
        @XmlEnumValue("daily")
        DAILY,
        @XmlEnumValue("weekly")
        WEEKLY,
        @XmlEnumValue("monthly")
        MONTHLY,
        @XmlEnumValue("yearly")
        YEARLY,
        @XmlEnumValue("never")
        NEVER
    }

    @XmlElement(name = "loc", required = true)
    private String location;
    @XmlElement(name = "lastmod", required = true)
    private DateTime lastModified;
    @XmlElement(name = "changefreq")
    private ChangeFrequency changeFrequency;
    @XmlElement(name = "priority")
    private BigDecimal priority;


    /**
     * Constructs new empty site map url object.
     */
    public SiteMapUrlData() {
        // intentional empty constructor
    }

    /**
     * Constructs site map url with given {@code location} and {@code lastModified} parameters set.
     * 
     * @param location
     *            the url location
     * @param lastModified
     *            the resource last modification timestamp
     */
    public SiteMapUrlData(final String location, final DateTime lastModified) {
        this.location = location;
        this.lastModified = lastModified;
    }

    /**
     * Constructs site map url with {@code location}, {@code lastModified}, {@code changeFrequency} and {@code priority}
     * parameters set.
     * 
     * @param location
     *            the url location
     * @param lastModified
     *            the resource last modification timestamp
     * @param changeFrequency
     *            the anticipated resource change frequency
     * @param priority
     *            the resource priority
     */
    public SiteMapUrlData(final String location, final DateTime lastModified,
            final ChangeFrequency changeFrequency, final BigDecimal priority) {
        this.location = location;
        this.lastModified = lastModified;
        this.changeFrequency = changeFrequency;
        this.priority = priority;
    }

    /**
     * Returns the url location.
     * 
     * @return the url location
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the url location.
     * 
     * @param location
     *            the url location to set
     */
    public void setLocation(final String location) {
        this.location = location;
    }

    /**
     * Returns the last modified timestamp for the url.
     * 
     * @return the last modified timestamp for the url
     */
    public DateTime getLastModified() {
        return lastModified;
    }

    /**
     * Sets the last modified timestamp for the url
     * 
     * @param lastModified
     *            the last modified timestamp for the url to set
     */
    public void setLastModified(final DateTime lastModified) {
        this.lastModified = lastModified;
    }

    /**
     * Returns the expected change frequency for the page context.
     * 
     * @return the expected change frequency for the page context
     */
    public ChangeFrequency getChangeFrequency() {
        return changeFrequency;
    }

    /**
     * Sets the expected change frequency for the page context.
     * 
     * @param changeFrequency
     *            the expected change frequency to set
     */
    public void setChangeFrequency(final ChangeFrequency changeFrequency) {
        this.changeFrequency = changeFrequency;
    }

    /**
     * Returns the priority of this URL relative to other URLs on your site.
     * 
     * @return the priority of this URL
     */
    public BigDecimal getPriority() {
        return priority;
    }

    /**
     * Sets the the priority of this URL relative to other URLs on your site.
     * 
     * @param priority
     *            the priority to set
     */
    public void setPriority(final BigDecimal priority) {
        this.priority = priority;
    }
}
