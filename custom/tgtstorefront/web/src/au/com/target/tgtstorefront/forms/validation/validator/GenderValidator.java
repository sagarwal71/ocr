/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;


import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.pages.checkout.AbstractCheckoutController.SelectOption;



/**
 * @author bhuang3
 * 
 */
public class GenderValidator extends AbstractTargetValidator implements
        ConstraintValidator<au.com.target.tgtstorefront.forms.validation.Gender, String> {

    @Override
    public void initialize(final au.com.target.tgtstorefront.forms.validation.Gender arg0) {
        field = FieldTypeEnum.gender;
        isMandatory = arg0.mandatory();
        isSizeRange = false;
        mustMatch = false;
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }

    @Override
    protected boolean isAvailable(final String value) {
        final List<SelectOption> genders = WebConstants.UserInformation.GENDERS;
        for (final SelectOption gender : genders) {
            if (gender.getCode().equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }
}
