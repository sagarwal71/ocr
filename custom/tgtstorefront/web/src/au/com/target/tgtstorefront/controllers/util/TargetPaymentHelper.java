/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commerceservices.customer.CustomerEmailResolutionService;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtfacades.user.data.IpgPaymentInfoData;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.forms.BillingAddressForm;
import au.com.target.tgtstorefront.forms.checkout.AbstractPaymentDetailsForm;
import au.com.target.tgtstorefront.forms.checkout.BillingAddressOnlyForm;
import au.com.target.tgtstorefront.forms.checkout.ExistingCardPaymentForm;
import au.com.target.tgtstorefront.forms.checkout.NewCardPaymentDetailsForm;
import au.com.target.tgtstorefront.forms.checkout.VendorPaymentForm;


/**
 * @author asingh78
 * 
 */
public class TargetPaymentHelper {
    protected static final Logger LOG = Logger.getLogger(TargetPaymentHelper.class);

    private static String tnsHostedPaymentFormBaseUrl = null;

    private static String baseSecureUrl = null;

    @Resource(name = "targetCheckoutFacade")
    private TargetCheckoutFacade checkoutFacade;

    @Resource(name = "targetUserFacade")
    private TargetUserFacade userFacade;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Resource(name = "targetDeliveryModeHelper")
    private TargetDeliveryModeHelper deliveryModeHelper;

    @Resource
    private CommonI18NService commonI18NService;

    @Resource
    private I18NFacade i18NFacade;

    @Resource
    private CustomerEmailResolutionService customerEmailResolutionService;

    @Resource
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Value("#{configurationService.configuration.getProperty('tgtstorefront.host.fqdn')}")
    private String fqdnSecure;

    @Value("#{configurationService.configuration.getProperty('paypalhere.accept.methods')}")
    private String payPalHereAccept;

    @Value("#{configurationService.configuration.getProperty('tgtpaymentprovider.paypal.imageUrl')}")
    private String payPalImgUrl;

    @Value("#{configurationService.configuration.getProperty('tgtstorefront.email.contact.emailaddress')}")
    private String targetContactEmail;

    @Value("#{configurationService.configuration.getProperty('tgtpaymentprovider.payment.feature.tns')}")
    private String tnsFeature;

    @Value("#{configurationService.configuration.getProperty('tgtpaymentprovider.payment.feature.paypal')}")
    private String paypalFeature;

    /**
     * Populate all payment form
     * 
     * @param model
     * @param existingCardPaymentForm
     * @param vendorPaymentForm
     * @param newCardPaymentDetailsForm
     * @param cartData
     */
    public void populatePaymentDefaultForms(final Model model, final ExistingCardPaymentForm existingCardPaymentForm,
            final VendorPaymentForm vendorPaymentForm, final NewCardPaymentDetailsForm newCardPaymentDetailsForm,
            final TargetCartData cartData, final AbstractPaymentDetailsForm sourceForm) {

        final String appliedVoucher = populateFormWithVoucher(cartData, sourceForm);
        final boolean tnsEnabled = getPaymentFeature(tnsFeature);
        final boolean paypalEnabled = getPaymentFeature(paypalFeature);
        if (existingCardPaymentForm != null) {
            if (tnsEnabled) {
                existingCardPaymentForm.setEnabled(true);
                existingCardPaymentForm.setVoucher(appliedVoucher);
                if (sourceForm != null) {
                    existingCardPaymentForm.setFlyBuys(sourceForm.getFlyBuys());
                    existingCardPaymentForm.setTeamMemberCode(sourceForm.getTeamMemberCode());
                    existingCardPaymentForm.setActive(existingCardPaymentForm.equals(sourceForm));
                }
                else {
                    populateFormWithCartData(cartData, existingCardPaymentForm);

                    existingCardPaymentForm.setActive(true);
                }

                if (existingCardPaymentForm.isActive()) {
                    TargetCCPaymentInfoData targetCCPaymentInfoData = null;
                    if (null != cartData) {
                        targetCCPaymentInfoData = (TargetCCPaymentInfoData)cartData.getPaymentInfo();
                    }
                    if (null != targetCCPaymentInfoData && targetCCPaymentInfoData.isPaymentSucceeded()) {
                        existingCardPaymentForm.setCardId(targetCCPaymentInfoData.getId());
                    }
                }
            }
            else {
                existingCardPaymentForm.setEnabled(false);
            }
            model.addAttribute("existingCardPaymentForm", existingCardPaymentForm);
        }

        if (vendorPaymentForm != null) {
            if (paypalEnabled) {
                vendorPaymentForm.setVoucher(appliedVoucher);
                vendorPaymentForm.setEnabled(true);
                if (sourceForm != null) {
                    vendorPaymentForm.setFlyBuys(sourceForm.getFlyBuys());
                    vendorPaymentForm.setTeamMemberCode(sourceForm.getTeamMemberCode());
                }
                else {
                    populateFormWithCartData(cartData, vendorPaymentForm);
                }
            }
            else {
                vendorPaymentForm.setEnabled(false);
            }
            model.addAttribute("vendorPaymentForm", vendorPaymentForm);
        }

        if (newCardPaymentDetailsForm != null) {
            if (tnsEnabled && null != cartData) {
                newCardPaymentDetailsForm.setVoucher(appliedVoucher);
                populatePaymentInfo(cartData, newCardPaymentDetailsForm);

                newCardPaymentDetailsForm.setEnabled(true);
                if (BooleanUtils.isNotTrue(deliveryModeHelper
                        .getDeliveryModeModel(cartData.getDeliveryMode().getCode()).getIsDeliveryToStore())
                        && cartData.getPaymentInfo() == null) {
                    final TargetAddressData deliveryAddress = (TargetAddressData)cartData.getDeliveryAddress();
                    if (deliveryAddress != null) {
                        copyDeliveryAddressToBillingAddress(newCardPaymentDetailsForm, deliveryAddress);
                    }
                }

                if (sourceForm != null) {
                    newCardPaymentDetailsForm.setFlyBuys(sourceForm.getFlyBuys());
                    newCardPaymentDetailsForm.setTeamMemberCode(sourceForm.getTeamMemberCode());
                    newCardPaymentDetailsForm.setActive(newCardPaymentDetailsForm.equals(sourceForm));
                }
                else {
                    populateFormWithCartData(cartData, newCardPaymentDetailsForm);
                }
            }
            else {
                newCardPaymentDetailsForm.setEnabled(false);
            }

            model.addAttribute("newCardPaymentDetailsForm", newCardPaymentDetailsForm);
        }

    }

    private boolean getPaymentFeature(final String feature) {
        return targetFeatureSwitchFacade.isFeatureEnabled(feature);
    }

    /**
     * Populate BillingAddressOnlyForm which is the form that gets populated for a pin pad transaction.
     * 
     * @param model
     * @param billingAddressOnlyForm
     * @param cartData
     * @param sourceForm
     */
    public void populateBillingAddressDetailsForm(final Model model,
            final BillingAddressOnlyForm billingAddressOnlyForm,
            final TargetCartData cartData, final AbstractPaymentDetailsForm sourceForm) {

        final String appliedVoucher = populateFormWithVoucher(cartData, sourceForm);

        if (null != cartData) {
            billingAddressOnlyForm.setVoucher(appliedVoucher);
            populatePaymentInfo(cartData, billingAddressOnlyForm);

            if (BooleanUtils.isNotTrue(deliveryModeHelper
                    .getDeliveryModeModel(cartData.getDeliveryMode().getCode()).getIsDeliveryToStore())
                    && cartData.getPaymentInfo() == null) {
                final TargetAddressData deliveryAddress = (TargetAddressData)cartData.getDeliveryAddress();
                if (deliveryAddress != null) {
                    copyDeliveryAddressToBillingAddress(billingAddressOnlyForm, deliveryAddress);
                }
            }

            if (sourceForm != null) {
                billingAddressOnlyForm.setFlyBuys(sourceForm.getFlyBuys());
                billingAddressOnlyForm.setTeamMemberCode(sourceForm.getTeamMemberCode());
                billingAddressOnlyForm.setActive(billingAddressOnlyForm.equals(sourceForm));
            }
            else {
                populateFormWithCartData(cartData, billingAddressOnlyForm);
            }
        }
    }

    /**
     * Populate all existing credit card a Tns return url
     * 
     * @param model
     * @param returnUrl
     */
    public void populatePaymentDefault(final Model model, final String returnUrl) {
        final List<CCPaymentInfoData> paymentMethods = userFacade.getCCPaymentInfos(true);
        model.addAttribute("paymentMethods", paymentMethods);
        model.addAttribute("paymentReturnUrl", getBaseSecureUrl()
                + returnUrl);
    }


    /**
     * Populate cart data
     * 
     * @param model
     * @param cartData
     */
    public void populatePaymentCart(final Model model, final TargetCartData cartData) {

        model.addAttribute("cartData", cartData);
        model.addAttribute("deliveryAddress", cartData.getDeliveryAddress());
        model.addAttribute("deliveryMethod", cartData.getDeliveryMode());


    }

    /**
     * site base URL
     * 
     * @return baseSecureUrl
     */
    public String getBaseSecureUrl() {
        if (null == baseSecureUrl) {
            baseSecureUrl = configurationService.getConfiguration().getString(
                    WebConstants.BASE_SECURE_URL_KEY);
        }
        return baseSecureUrl;
    }

    /**
     * Creating CCPaymentInfoData
     * 
     * @param form
     * @return CCPaymentInfoData
     */
    public CCPaymentInfoData createCCPaymentInfoData(final NewCardPaymentDetailsForm form) {
        final CCPaymentInfoData paymentInfoData = new CCPaymentInfoData();
        paymentInfoData.setId(form.getPaymentSessionId());
        paymentInfoData.setCardType(checkoutFacade.getCreditCardType(form.getGatewayCardScheme()).getCode());
        paymentInfoData.setAccountHolderName(form.getNameOnCard());
        paymentInfoData.setCardNumber(form.getGatewayCardNumber());
        paymentInfoData.setExpiryMonth(form.getGatewayCardExpiryDateMonth());
        paymentInfoData.setExpiryYear(form.getGatewayCardExpiryDateYear());
        paymentInfoData.setSaved(Boolean.TRUE.equals(form.getSaveInAccount()));
        return paymentInfoData;
    }

    /**
     * Populate Address data
     * 
     * @return TargetAddressData
     */
    public TargetAddressData createTargetAddressData(final BillingAddressForm addressForm) {

        final TargetAddressData addressData = new TargetAddressData();
        if (addressForm != null) {
            addressData.setId(addressForm.getAddressId());
            addressData.setTitleCode(addressForm.getTitleCode());
            addressData.setFirstName(addressForm.getFirstName());
            addressData.setLastName(addressForm.getLastName());
            addressData.setLine1(addressForm.getLine1());
            addressData.setLine2(addressForm.getLine2());
            addressData.setTown(addressForm.getTownCity());
            addressData.setPostalCode(addressForm.getPostcode());
            addressData.setState(addressForm.getState());
            addressData.setCountry(i18NFacade.getCountryForIsocode(addressForm.getCountryIso()));
            addressData.setBillingAddress(true);
        }
        return addressData;
    }

    private void copyDeliveryAddressToBillingAddress(final AbstractPaymentDetailsForm paymentDetailsForm,
            final TargetAddressData deliveryAddress) {
        final BillingAddressForm billingAddressForm = new BillingAddressForm();
        billingAddressForm.setAddressId(deliveryAddress.getId());
        billingAddressForm.setTitleCode(deliveryAddress.getTitleCode());
        billingAddressForm.setFirstName(deliveryAddress.getFirstName());
        billingAddressForm.setLastName(deliveryAddress.getLastName());
        billingAddressForm.setLine1(deliveryAddress.getLine1());
        billingAddressForm.setLine2(deliveryAddress.getLine2());
        billingAddressForm.setTownCity(deliveryAddress.getTown());
        billingAddressForm.setPostcode(deliveryAddress.getPostalCode());
        billingAddressForm.setCountryIso(deliveryAddress.getCountry().getIsocode());
        billingAddressForm.setState(deliveryAddress.getState());

        if (paymentDetailsForm instanceof NewCardPaymentDetailsForm) {
            final NewCardPaymentDetailsForm newCardPaymentDetailsForm = (NewCardPaymentDetailsForm)paymentDetailsForm;
            newCardPaymentDetailsForm.setBillingAddress(billingAddressForm);
        }
        else if (paymentDetailsForm instanceof BillingAddressOnlyForm) {
            final BillingAddressOnlyForm billingAddressOnlyForm = (BillingAddressOnlyForm)paymentDetailsForm;
            billingAddressOnlyForm.setBillingAddress(billingAddressForm);
        }

    }

    private String populateFormWithVoucher(final TargetCartData cartData,
            final AbstractPaymentDetailsForm sourceForm) {
        String appliedVoucher = null;
        if (cartData != null) {// cartData will always be not-null in checkout, will always be null in 'My Account'
            appliedVoucher = checkoutFacade.getFirstAppliedVoucher();
            if (sourceForm != null) {
                sourceForm.setVoucher(appliedVoucher);
            }
        }
        return appliedVoucher;
    }

    private void populateFormWithCartData(final TargetCartData cartData,
            final AbstractPaymentDetailsForm form) {
        if (null != cartData) {
            form.setTeamMemberCode(cartData.getTmdNumber());
            form.setFlyBuys(cartData.getFlybuysNumber());
        }
    }

    private void populatePaymentInfo(final TargetCartData cartData, final AbstractPaymentDetailsForm form) {
        if (null != cartData && cartData.getPaymentInfo() instanceof TargetCCPaymentInfoData) {
            final TargetCCPaymentInfoData targetCCPaymentInfoData = (TargetCCPaymentInfoData)cartData.getPaymentInfo();
            if (null != targetCCPaymentInfoData && !targetCCPaymentInfoData.isPaymentSucceeded()) {
                final BillingAddressForm addressForm = new BillingAddressForm();
                final TargetAddressData addressData = (TargetAddressData)targetCCPaymentInfoData.getBillingAddress();
                if (null != addressData) {
                    addressForm.setAddressId(addressData.getId());
                    addressForm.setTitleCode(addressData.getTitleCode());
                    addressForm.setFirstName(addressData.getFirstName());
                    addressForm.setLastName(addressData.getLastName());
                    addressForm.setLine1(addressData.getLine1());
                    addressForm.setLine2(addressData.getLine2());
                    addressForm.setTownCity(addressData.getTown());
                    addressForm.setPostcode(addressData.getPostalCode());
                    addressForm.setCountryIso(addressData.getCountry().getIsocode());
                    addressForm.setState(addressData.getState());
                }

                if (form instanceof NewCardPaymentDetailsForm) {
                    final NewCardPaymentDetailsForm newCardPaymentDetailsForm = (NewCardPaymentDetailsForm)form;
                    newCardPaymentDetailsForm.setBillingAddress(addressForm);
                    newCardPaymentDetailsForm.setGatewayCardExpiryDateMonth(targetCCPaymentInfoData.getExpiryMonth());
                    newCardPaymentDetailsForm.setGatewayCardExpiryDateYear(targetCCPaymentInfoData.getExpiryYear());
                    newCardPaymentDetailsForm.setNameOnCard(targetCCPaymentInfoData.getAccountHolderName());
                    newCardPaymentDetailsForm.setSaveInAccount(Boolean.valueOf(targetCCPaymentInfoData.isSaved()));
                    form.setActive(true);
                }
                else if (form instanceof BillingAddressOnlyForm) {
                    final BillingAddressOnlyForm billingAddressOnlyForm = (BillingAddressOnlyForm)form;
                    billingAddressOnlyForm.setBillingAddress(addressForm);
                    if (null != targetCCPaymentInfoData.getIpgPaymentInfoData()) {
                        final IpgPaymentInfoData ipgPaymentInfoData = targetCCPaymentInfoData.getIpgPaymentInfoData();
                        if (IpgPaymentTemplateType.CREDITCARDSINGLE.equals(ipgPaymentInfoData
                                .getIpgPaymentTemplateType())
                                && TgtFacadesConstants.CREDIT_CARD.equalsIgnoreCase(billingAddressOnlyForm
                                        .getCardType())) {
                            billingAddressOnlyForm.setActive(true);
                        }
                        else if (IpgPaymentTemplateType.GIFTCARDMULTIPLE.equals(ipgPaymentInfoData
                                .getIpgPaymentTemplateType())
                                && TgtFacadesConstants.GIFT_CARD.equalsIgnoreCase(billingAddressOnlyForm
                                        .getCardType())) {
                            billingAddressOnlyForm.setActive(true);
                        }
                    }
                }

                form.setTeamMemberCode(cartData.getTmdNumber());
                form.setFlyBuys(cartData.getFlybuysNumber());

            }
        }

    }

    /**
     * Tns hosted URL
     * 
     * @return TNS URL
     */
    public String getTnsHostedPaymentFormBaseUrl() {
        if (null == tnsHostedPaymentFormBaseUrl) {
            tnsHostedPaymentFormBaseUrl = configurationService.getConfiguration().getString(
                    WebConstants.TNS_HOSTED_PAYMENT_URL_KEY);
        }
        return tnsHostedPaymentFormBaseUrl;

    }

    /**
     * Paypal Hosted URL
     * 
     * @return PayPal URL
     */
    public String getPayPalHostedPaymentFormBaseUrl() {
        return configurationService.getConfiguration().getString(WebConstants.PAYPAL_HOSTED_PAYMENT_URL_KEY);
    }

    /**
     * Populate Error message when TNS is down
     * 
     * @param model
     */
    public void populateTnsErrorMessage(final Model model) {
        GlobalMessages.addErrorMessage(model, "payment.tns.down");
        model.addAttribute("paymentCreditCardDisabled", Boolean.TRUE);
    }

    /**
     * Populate Error message when IPG is down
     * 
     * @param model
     */
    public void populateIpgErrorMessage(final Model model) {
        GlobalMessages.addErrorMessage(model, "payment.ipg.down");
        model.addAttribute("paymentCreditCardDisabled", Boolean.TRUE);
    }

    /**
     * 
     * @param model
     * @return TNSSession token
     */
    public String getTNSSessionToken(final Model model) {
        String tnsSessionToken = StringUtils.EMPTY;
        try {
            tnsSessionToken = checkoutFacade.getTNSSessionToken().getRequestToken();
        }
        catch (final AdapterException adapterException) {
            populateTnsErrorMessage(model);
        }
        return tnsSessionToken;
    }

    /**
     * 
     * @return true if ipg is enabled, false if not
     */
    public boolean checkIfIPGFeatureIsEnabled() {
        final boolean featureEnabled = targetFeatureSwitchFacade.isFeatureEnabled("payment.ipg");
        return featureEnabled;
    }

    /**
     * 
     * @return true if giftcard is enabled.
     */
    public boolean isGiftCardEnable() {
        final boolean featureEnabled = targetFeatureSwitchFacade.isFeatureEnabled("payment.giftcard");
        return featureEnabled;
    }

}