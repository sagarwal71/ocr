/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.interceptors.beforeview;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.ModelAndView;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.constants.ThirdPartyConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.util.SalesApplicationPropertyResolver;


public class ThirdPartyPropertiesBeforeViewHandler extends AbstractHostSpecificPropertiesBeforeViewHandler {
    private static final String ANALYTICS_DISPLAY_ADVERTISER_SUPPORT = "analyticsDisplayAdvertiserSupport";
    private static final String UNIVERSAL_ANALYTICS_ACCOUNT_ID = "universalAnalyticsId";
    private static final String UNIVERSAL_ANALYTICS_COOKIE_DOMAIN = "universalAnalyticsCookieDomain";
    private static final String TAG_MANAGER_CONTAINER = "tagManagerContainer";
    private static final String GOPTIMIZE_CONTAINER = "gOptimizeContainer";
    private static final String GMAPS_API_VERSION = "gmapsApiVersion";
    private static final String GMAPS_API_CLIENT = "gmapsApiClient";
    private static final String ADDTHIS_PUB_ID = "addThisPubId";
    private static final String BAZAAR_VOICE_API_URI = "bazaarVoiceApiUri";

    private static final String ANDROID_SMARTAPPBANNER_APP_ID = "androidSmartAppBannerAppId";
    private static final String ANDROID_SMARTAPPBANNER_APP_SCHEME = "androidSmartAppBannerAppScheme";
    private static final String ANDROID_SMARTAPPBANNER_APP_STORE_URL = "androidSmartAppBannerAppStoreUrl";
    private static final String APPLE_SMARTAPPBANNER_APP_ID = "appleSmartAppBannerAppId";
    private static final String APPLE_SMARTAPPBANNER_APP_SCHEME = "appleSmartAppBannerAppScheme";
    private static final String APPLE_SMARTAPPBANNER_APP_STORE_URL = "appleSmartAppBannerAppStoreUrl";

    private SalesApplicationPropertyResolver salesApplicationPropertyResolver;

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Override
    public void beforeView(final HttpServletRequest request, final HttpServletResponse response,
            final ModelAndView modelAndView) {

        final String analyticsID = salesApplicationPropertyResolver.getGAValueForSalesApplication();
        if (StringUtils.isNotEmpty(analyticsID)) {
            addHostProperty(modelAndView, UNIVERSAL_ANALYTICS_ACCOUNT_ID,
                    analyticsID);
        }

        addHostProperty(hostSuffix, modelAndView, ThirdPartyConstants.Analytics.DISPLAY_ADVERTISER_SUPPORT_ENABLED,
                ANALYTICS_DISPLAY_ADVERTISER_SUPPORT);

        addHostProperty(hostSuffix, modelAndView, ThirdPartyConstants.TagManager.CONTAINER,
                TAG_MANAGER_CONTAINER);

        addHostProperty(hostSuffix, modelAndView, ThirdPartyConstants.GOptimize.CONTAINER,
                GOPTIMIZE_CONTAINER);

        addHostProperty(hostSuffix, modelAndView, ThirdPartyConstants.GMaps.API_VERSION, GMAPS_API_VERSION);
        addHostProperty(hostSuffix, modelAndView, ThirdPartyConstants.GMaps.API_CLIENT, GMAPS_API_CLIENT);

        addHostProperty(hostSuffix, modelAndView, ThirdPartyConstants.AddThis.PUB_ID, ADDTHIS_PUB_ID);

        /* TODO: Remove this one when feature switch is cleaned */
        if (!targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_DETAIL_PANEL)) {
            addHostProperty(hostSuffix, modelAndView, ThirdPartyConstants.BazaarVoice.API_URI, BAZAAR_VOICE_API_URI);
        }

        addHostProperty(hostSuffix, modelAndView, ThirdPartyConstants.Analytics.UNIVERSAL_ANALYTICS_COOKIE_DOMAIN,
                UNIVERSAL_ANALYTICS_COOKIE_DOMAIN);

        addHostProperty(hostSuffix, modelAndView, ThirdPartyConstants.Android.SMARTAPPBANNER_APP_ID,
                ANDROID_SMARTAPPBANNER_APP_ID);
        addHostProperty(hostSuffix, modelAndView, ThirdPartyConstants.Android.SMARTAPPBANNER_APP_SCHEME,
                ANDROID_SMARTAPPBANNER_APP_SCHEME);
        addHostProperty(hostSuffix, modelAndView, ThirdPartyConstants.Android.SMARTAPPBANNER_APP_STORE_URL,
                ANDROID_SMARTAPPBANNER_APP_STORE_URL);

        addHostProperty(hostSuffix, modelAndView, ThirdPartyConstants.Apple.SMARTAPPBANNER_APP_ID,
                APPLE_SMARTAPPBANNER_APP_ID);
        addHostProperty(hostSuffix, modelAndView, ThirdPartyConstants.Apple.SMARTAPPBANNER_APP_SCHEME,
                APPLE_SMARTAPPBANNER_APP_SCHEME);
        addHostProperty(hostSuffix, modelAndView, ThirdPartyConstants.Apple.SMARTAPPBANNER_APP_STORE_URL,
                APPLE_SMARTAPPBANNER_APP_STORE_URL);

    }

    /**
     * @param salesApplicationPropertyResolver
     *            the salesApplicationPropertyResolver to set
     */
    @Required
    public void setSalesApplicationPropertyResolver(
            final SalesApplicationPropertyResolver salesApplicationPropertyResolver) {
        this.salesApplicationPropertyResolver = salesApplicationPropertyResolver;
    }

}
