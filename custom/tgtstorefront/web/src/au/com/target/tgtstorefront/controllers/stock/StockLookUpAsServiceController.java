package au.com.target.tgtstorefront.controllers.stock;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.product.stock.StockAvailabilitiesData;
import au.com.target.tgtfacades.stock.TargetStockLookUpFacade;
import au.com.target.tgtstorefront.controllers.AbstractController;


@Controller
@RequestMapping("/stocklookup")
public class StockLookUpAsServiceController extends AbstractController {

    @Resource(name = "targetStockLookupFacade")
    private TargetStockLookUpFacade targetStockLookupFacade;


    @RequestMapping(value = "/{source}", method = RequestMethod.POST)
    @ResponseBody
    public StockAvailabilitiesData getStockForRequest(
            @RequestBody final StockAvailabilitiesData stockAvailabilitiesData,
            @PathVariable("source") final String source) {
        return targetStockLookupFacade.getStockFromWarehouse(stockAvailabilitiesData, source, null);
    }

}