/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.data;

/**
 * @author bhuang3
 * 
 */
public class SeachEvaluatorData {

    private String sortCodeFn;

    private String viewAsFn;

    private int itemPerPageFn;

    /**
     * @return the sortCodeFn
     */
    public String getSortCodeFn() {
        return sortCodeFn;
    }

    /**
     * @param sortCodeFn
     *            the sortCodeFn to set
     */
    public void setSortCodeFn(final String sortCodeFn) {
        this.sortCodeFn = sortCodeFn;
    }

    /**
     * @return the viewAsFn
     */
    public String getViewAsFn() {
        return viewAsFn;
    }

    /**
     * @param viewAsFn
     *            the viewAsFn to set
     */
    public void setViewAsFn(final String viewAsFn) {
        this.viewAsFn = viewAsFn;
    }

    /**
     * @return the itemPerPageFn
     */
    public int getItemPerPageFn() {
        return itemPerPageFn;
    }

    /**
     * @param itemPerPageFn
     *            the itemPerPageFn to set
     */
    public void setItemPerPageFn(final int itemPerPageFn) {
        this.itemPerPageFn = itemPerPageFn;
    }



}
