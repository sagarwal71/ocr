package au.com.target.endeca.infront.cartridge;


import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.RecordSpotlightConfig;


public class TgtRecommendationBoughtBoughtConfig extends RecordSpotlightConfig {

    private static final String MAX_NUM_RECORDS_PER_PRODUCT = "maxNumRecordsPerProduct";

    public TgtRecommendationBoughtBoughtConfig() {
        // empty constructor
    }

    public TgtRecommendationBoughtBoughtConfig(final String pType) {
        super(pType);
    }

    public TgtRecommendationBoughtBoughtConfig(final ContentItem pContentItem) {
        super(pContentItem);
    }

    public int getMaxNumRecordsPerProduct() {
        return getIntProperty(MAX_NUM_RECORDS_PER_PRODUCT, 3);
    }

    public void setMaxNumRecordsPerProduct(final int maxNumRecordsPerProduct) {
        put(MAX_NUM_RECORDS_PER_PRODUCT, Integer.valueOf(maxNumRecordsPerProduct));
    }

}
