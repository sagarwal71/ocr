package au.com.target.endeca.infront.cartridge;

import org.apache.log4j.Logger;

import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.ResultsListRequestParamMarshaller;
import com.endeca.infront.navigation.model.SortOption;


public class TgtResultsListRequestParamMarshaller extends
        ResultsListRequestParamMarshaller
{
    protected static final Logger LOG = Logger.getLogger(TgtResultsListRequestParamMarshaller.class);

    @Override
    public void override(final ContentItem contentItem)
    {
        final ContentItem marshalledConfig = marshall();
        compareAndSetDefaultSortOption(contentItem, marshalledConfig);
        contentItem.putAll(marshalledConfig);
    }

    /**
     * This method checks the requested sortoption is same as the default sort option and sets if yes marks the option
     * to default.
     * 
     * @param contentItem
     * @param marshalledConfig
     */
    private void compareAndSetDefaultSortOption(final ContentItem contentItem, final ContentItem marshalledConfig) {

        if (null != contentItem.get("sortOption")
                && null != marshalledConfig.get("sortOption")
                && contentItem.get("sortOption").toString()
                        .equalsIgnoreCase(marshalledConfig.get("sortOption").toString())) {
            final SortOption sortOption = (SortOption)marshalledConfig.get("sortOption");
            sortOption.setDefault(true);
            marshalledConfig.put("sortOption", sortOption);
        }
    }
}
