/**
 * 
 */
package au.com.target.endeca.http.request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;


/**
 * This class is for overriding the HttpServletRequest with a dummy another object to modify query String after the
 * request is received in java.
 * 
 * @author pthoma20
 * 
 */
public class TgtHttpServletRequest extends HttpServletRequestWrapper {

    private String queryString;


    public TgtHttpServletRequest(final HttpServletRequest request) {
        super(request);
    }

    /**
     * Returns the parameter by checking whether it is present in the current query String. If not then check query
     * String in the parent.
     */
    @Override
    public String getParameter(final String paramName) {

        final List<NameValuePair> parameters = extractParameterFromQueryString();
        for (final NameValuePair nameValuePairObj : parameters) {
            if (nameValuePairObj != null && StringUtils.equals(nameValuePairObj.getName(), paramName)) {
                return nameValuePairObj.getValue();
            }
        }
        return super.getParameter(paramName);
    }

    /**
     * Extract NameValue Pairs from Query String
     * 
     * @return parameters
     */
    @SuppressWarnings("deprecation")
    private List<NameValuePair> extractParameterFromQueryString() {
        final List<NameValuePair> parameters = new ArrayList<>();
        URLEncodedUtils.parse(parameters, new Scanner(queryString), "UTF-8");
        return parameters;
    }

    /**
     * Returns the Parameter map by constructing it out of the query String.
     */
    @SuppressWarnings("deprecation")
    @Override
    public Map<String, String[]> getParameterMap() {
        final List<NameValuePair> parameters = extractParameterFromQueryString();
        final Map<String, String[]> parameterMap = new HashMap<String, String[]>();
        URLEncodedUtils.parse(parameters, new Scanner(queryString), "UTF-8");
        for (final NameValuePair nameValuePairObj : parameters) {
            parameterMap.put(nameValuePairObj.getName(), getParameterValues(nameValuePairObj.getName()));
        }
        return parameterMap;
    }

    /**
     * @return the queryString
     */
    @Override
    public String getQueryString() {
        return queryString;
    }

    /**
     * @param queryString
     *            the queryString to set
     */
    public void setQueryString(final String queryString) {
        this.queryString = queryString;
    }

}
