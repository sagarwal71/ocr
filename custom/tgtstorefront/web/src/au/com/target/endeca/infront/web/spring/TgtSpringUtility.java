/**
 * 
 */
package au.com.target.endeca.infront.web.spring;

import javax.servlet.http.HttpServletRequest;

import au.com.target.endeca.http.request.TgtHttpServletRequest;

import com.endeca.infront.web.spring.SpringUtility;


/**
 * This class overrides existing defined class for returning the customized httpRequest.
 * 
 * @author pthoma20
 * 
 */
public class TgtSpringUtility extends SpringUtility {


    private static final String ENDECA_MODIFIED_REQUEST = "TgtHttpServletRequest";

    @Override
    public HttpServletRequest getHttpServletRequest() {
        return (TgtHttpServletRequest)super.getHttpServletRequest().getAttribute(ENDECA_MODIFIED_REQUEST);
    }

    /**
     * @param tgtHttpServletRequest
     *            the tgtHttpServletRequest to set
     */
    public void setTgtHttpServletRequest(final TgtHttpServletRequest tgtHttpServletRequest) {
        super.getHttpServletRequest().setAttribute(ENDECA_MODIFIED_REQUEST, tgtHttpServletRequest);
    }


}
