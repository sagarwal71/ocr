/**
 * 
 */
package au.com.target.endeca.infront.cartridge;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.tgtfacades.url.impl.TargetBrandDimensionSearchUrlResolver;
import au.com.target.tgtfacades.url.impl.TargetCategoryDimensionSearchUrlResolver;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.DimensionSearchResults;
import com.endeca.infront.cartridge.DimensionSearchResultsConfig;
import com.endeca.infront.cartridge.DimensionSearchResultsHandler;
import com.endeca.infront.cartridge.model.DimensionSearchGroup;
import com.endeca.infront.cartridge.model.DimensionSearchValue;


/**
 * @author bhuang3
 *
 */
public class TgtDimensionSearchResultsHandler extends DimensionSearchResultsHandler {


    private TargetCategoryDimensionSearchUrlResolver targetCategoryDimensionSearchUrlResolver;

    private TargetBrandDimensionSearchUrlResolver targetBrandDimensionSearchUrlResolver;

    @Override
    public DimensionSearchResults process(final DimensionSearchResultsConfig cartridgeConfig)
            throws CartridgeHandlerException {
        final DimensionSearchResults dimensionSearchResults = super.process(cartridgeConfig);
        populateCategoryAndBrandUrl(dimensionSearchResults);
        return dimensionSearchResults;
    }

    /**
     * resolve the url for category and brand page.
     * 
     * @param dimensionSearchResults
     * @return DimensionSearchResults
     */
    protected DimensionSearchResults populateCategoryAndBrandUrl(final DimensionSearchResults dimensionSearchResults) {
        if (dimensionSearchResults != null) {
            final String dimensionSearchResultsType = (String)dimensionSearchResults
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_TYPE);
            if (EndecaConstants.EndecaRecordSpecificFields.ENDECA_DIMENSION_SEARCH_AUTO_SUGGEST_ITEM
                    .equalsIgnoreCase(dimensionSearchResultsType)) {
                final List<DimensionSearchGroup> contentItemList = dimensionSearchResults
                        .getDimensionSearchGroups();
                if (CollectionUtils.isEmpty(contentItemList)) {
                    return dimensionSearchResults;
                }
                for (final DimensionSearchGroup dimensionSearchGroup : contentItemList) {
                    if (EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY.equals(dimensionSearchGroup
                            .getDimensionName())) {
                        final List<DimensionSearchValue> dimensionSearchValueList = dimensionSearchGroup
                                .getDimensionSearchValues();
                        for (final DimensionSearchValue dimensionSearchValue : dimensionSearchValueList) {
                            dimensionSearchValue.setNavigationState(targetCategoryDimensionSearchUrlResolver
                                    .resolve(dimensionSearchValue));
                        }
                    }
                    else if (EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND.equals(dimensionSearchGroup
                            .getDimensionName())) {
                        final List<DimensionSearchValue> dimensionSearchValueList = dimensionSearchGroup
                                .getDimensionSearchValues();
                        for (final DimensionSearchValue dimensionSearchValue : dimensionSearchValueList) {
                            dimensionSearchValue.setNavigationState(targetBrandDimensionSearchUrlResolver
                                    .resolve(dimensionSearchValue));
                        }
                    }
                }
            }
        }
        return dimensionSearchResults;
    }

    @Required
    public void setTargetCategoryDimensionSearchUrlResolver(
            final TargetCategoryDimensionSearchUrlResolver targetCategoryDimensionSearchUrlResolver) {
        this.targetCategoryDimensionSearchUrlResolver = targetCategoryDimensionSearchUrlResolver;
    }

    /**
     * @param targetBrandDimensionSearchUrlResolver
     *            the targetBrandDimensionSearchUrlResolver to set
     */
    @Required
    public void setTargetBrandDimensionSearchUrlResolver(
            final TargetBrandDimensionSearchUrlResolver targetBrandDimensionSearchUrlResolver) {
        this.targetBrandDimensionSearchUrlResolver = targetBrandDimensionSearchUrlResolver;
    }



}
