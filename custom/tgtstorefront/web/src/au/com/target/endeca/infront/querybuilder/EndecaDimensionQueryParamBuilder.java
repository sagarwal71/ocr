/**
 * 
 */
package au.com.target.endeca.infront.querybuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.assemble.impl.EndecaPageAssemble;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.constants.EndecaConstants.EndecaRecordSpecificFields;
import au.com.target.tgtstorefront.util.SearchQuerySanitiser;


/**
 * @author smudumba
 * 
 */
public class EndecaDimensionQueryParamBuilder {

    private static final String QUERY_SEPERATOR = ":";
    private static final String PRICE = "price";

    private static final String VALIDDIMENSIONROOTS[] = { EndecaRecordSpecificFields.ENDECA_CATEGORY,
            EndecaRecordSpecificFields.ENDECA_BRAND, EndecaRecordSpecificFields.ENDECA_COLOUR,
            EndecaRecordSpecificFields.ENDECA_PROMOTIONAL_STATUS, EndecaRecordSpecificFields.ENDECA_SIZE,
            EndecaRecordSpecificFields.ENDECA_GENDER, EndecaRecordSpecificFields.ENDECA_SHOP_BY_PRICE };

    private EndecaPageAssemble endecaPageAssemble;

    private SearchQuerySanitiser searchQuerySanitiser;


    /**
     * Method converts queryString to a endecaQuery string
     * 
     * @param queryParams
     * @param type
     * @param code
     * @param request
     * @return String
     */
    public String getEndecaQueryString(final String queryParams, final String type, final String code,
            final HttpServletRequest request) {
        MultiValueMap dimsList = null;
        String result = null;

        final String sanitisedText = searchQuerySanitiser.sanitiseSearchText(queryParams);
        //build the dimensions list from the query params
        dimsList = buildDims(sanitisedText);
        //add the category entry
        if (null != dimsList) {
            //Use sub category if exists otherwise use the main category 
            if (!dimsList.containsKey(EndecaRecordSpecificFields.ENDECA_CATEGORY)) {
                //Adding it as an array list to simplify processing
                final List codeList = new ArrayList<>();
                codeList.add(code);
                dimsList.put(type, codeList);
            }
        }
        else {
            dimsList = new MultiValueMap();
            final List codeList = new ArrayList<>();
            codeList.add(code);
            //Adding it as an arraylist to simplify processing
            dimsList.put(type, codeList);
        }

        final String dimQuery = endecaPageAssemble.getQueryParmasNavigationState(request, dimsList);
        if (null != dimQuery) {
            result = EndecaConstants.EndecaFilterQuery.ENDECA_NAV_STATE_TERM + dimQuery;
        }
        return result;

    }



    /**
     * Method populates the name value pairs from the query string
     * 
     * @param queryString
     * @return MultiValueMap
     */
    private MultiValueMap buildKeyValueMap(final String queryString) {
        final MultiValueMap resultMap = new MultiValueMap();
        //value is a name value pairs concatenated with :  processing rule is,  
        final String pattern = QUERY_SEPERATOR;
        final Pattern splitter = Pattern.compile(pattern);
        //Reading all the values into an array
        final String[] resultValues = splitter.split(queryString);
        String[] keyValueArray = new String[2];
        //Process it an create a map with key and values
        int j = 0;
        for (int i = 0; i < resultValues.length; i++) {

            final String resultValue = resultValues[i];
            if (j > 1) {

                if (null != keyValueArray[0] && StringUtils.isNotEmpty(keyValueArray[0])) {
                    resultMap.put(keyValueArray[0], keyValueArray[1]);
                }

                keyValueArray = new String[2];
                j = 0;
                if (StringUtils.isEmpty(resultValue)) {
                    continue;
                }

            }

            keyValueArray[j] = resultValues[i];
            j++;
        }

        //cater for the last value 

        if (null != keyValueArray[0]) {
            resultMap.put(keyValueArray[0], keyValueArray[1]);
        }
        return resultMap;
    }



    /**
     * Method prepares a map which contains only those elements which need transformation
     * 
     * @param queryString
     * @return MultiValueMap
     */
    private MultiValueMap buildDims(final String queryString) {
        MultiValueMap keyValueMap = null;
        if (null != queryString) {
            keyValueMap = this.buildKeyValueMap(queryString);
        }
        final MultiValueMap dimensionValues = new MultiValueMap();
        if (null != keyValueMap) {
            final Set keys = keyValueMap.keySet();
            for (final Object k : keys) {
                Object dimKey = null;
                if (null != k) {
                    if (PRICE.equalsIgnoreCase(k.toString())) {
                        dimKey = EndecaRecordSpecificFields.ENDECA_SHOP_BY_PRICE;
                    }
                    else {
                        dimKey = k;
                    }
                }
                if (Arrays.asList(VALIDDIMENSIONROOTS).contains(dimKey)) {
                    dimensionValues.put(dimKey, keyValueMap.get(k));
                }
            }

        }
        return dimensionValues;

    }

    /**
     * @param endecaPageAssemble
     *            the endecaPageAssemble to set
     */
    @Required
    public void setEndecaPageAssemble(final EndecaPageAssemble endecaPageAssemble) {
        this.endecaPageAssemble = endecaPageAssemble;
    }

    /**
     * @param searchQuerySanitiser
     *            the searchQuerySanitiser to set
     */
    @Required
    public void setSearchQuerySanitiser(final SearchQuerySanitiser searchQuerySanitiser) {
        this.searchQuerySanitiser = searchQuerySanitiser;
    }
}
