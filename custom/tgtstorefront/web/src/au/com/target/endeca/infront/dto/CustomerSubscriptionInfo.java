/**
 * 
 */
package au.com.target.endeca.infront.dto;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author mjanarth
 *
 */
public class CustomerSubscriptionInfo {

    @JsonProperty
    private String email;
    @JsonProperty
    private String subscriptionSource;

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * @return the subscriptionSource
     */
    public String getSubscriptionSource() {
        return subscriptionSource;
    }

    /**
     * @param subscriptionSource
     *            the subscriptionSource to set
     */
    public void setSubscriptionSource(final String subscriptionSource) {
        this.subscriptionSource = subscriptionSource;
    }

}
