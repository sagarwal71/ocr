/**
 * 
 */
package au.com.target.endeca.infront.assemble.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.ContentSlotConfig;

import au.com.target.endeca.infront.assemble.EndecaAssemble;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaWrapperException;


/**
 * @author Paul
 * 
 */
public class EndecaAutoSuggestAssemble extends EndecaAssemble {
    /**
     * This method is responsible for taking in the request along with the search term and doing a search to get the
     * results. Also when a filter is applied on that page this method will take care for the same. The search term is
     * present in the EndecaSearchStateData. This method does not check for the current dimensions as it just needs to
     * retrieve the results.
     * 
     * @param request
     * @param endecaSearchStateData
     * @throws TargetEndecaWrapperException
     */
    public ContentItem assemble(final HttpServletRequest request, final EndecaSearchStateData endecaSearchStateData)
            throws TargetEndecaWrapperException {
        final ContentSlotConfig contentSlotConfig = new ContentSlotConfig();
        final List<String> contentPaths = new ArrayList<>();
        contentPaths.add(EndecaConstants.EndecaPageUris.ENDECA_AUTOSUGGEST_SEARCH_CONTENT_PATH);
        contentSlotConfig.setContentPaths(contentPaths);
        final List<String> templateTypes = new ArrayList<>();
        templateTypes.add(EndecaConstants.EndecaPageUris.ENDECA_AUTOSUGGEST_SEARCH_TEMPLATE_TYPE);
        contentSlotConfig.setTemplateTypes(templateTypes);
        contentSlotConfig.setRuleLimit(3);
        return dynamicContentAssemble(request, endecaSearchStateData, contentSlotConfig);
    }
}
