/**
 * 
 */
package au.com.target.endeca.infront.assemble.impl;

import static au.com.target.tgtwebcore.constants.TgtwebcoreConstants.LOG_IDENTIFIER_REQUEST_USERAGENT;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.MDC;
import org.springframework.util.Assert;

import au.com.target.endeca.infront.assemble.EndecaAssemble;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaDimension;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.exception.TargetEndecaWrapperException;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;

import com.endeca.infront.assembler.ContentItem;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.MutableDimLocation;
import com.endeca.navigation.MutableDimLocationList;
import com.endeca.navigation.MutableDimVal;
import com.endeca.soleng.urlformatter.UrlFormatException;
import com.google.common.collect.Lists;


/**
 * This class is responsible for assembling the entire Page.
 * 
 * @author pthoma20
 * 
 */

public class EndecaPageAssemble extends EndecaAssemble {

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;


    /**
     * This is the method which gets called for assembling the data for page. First it makes a call to the assembler for
     * using dimension search to get the navigation state which endeca understands and then using that it calls the
     * assembler page for getting the Navigation state.
     * 
     * @param request
     * @param searchStateData
     * @return Endeca Search object
     * @throws IOException
     * @throws TransformerException
     * @throws JAXBException
     */
    public EndecaSearch assemble(final HttpServletRequest request, final String pageUri,
            final EndecaSearchStateData searchStateData,
            final Map<String, String> dims)
            throws JAXBException,
            TransformerException, IOException, TargetEndecaWrapperException {

        Assert.notNull(searchStateData, "searchStateData should not be null!");
        //check if navigation state already exists. If it is present then no need for doing a dimension search again.
        String dimensionNavigationState = null;
        if (CollectionUtils.isEmpty(searchStateData.getNavigationState()) && MapUtils.isNotEmpty(dims)) {
            dimensionNavigationState = endecaDimensionService.getDimensionNavigationState(dims);
        }

        dimensionNavigationState = applyAvailableOnlinePreference(request, searchStateData, dimensionNavigationState);

        final ContentItem contentItem = getContentItemForPage(request, pageUri, dimensionNavigationState,
                searchStateData);
        final EndecaSearch searchResult = endecaDataParser
                .convertSourceToSearchObjects(Collections.singletonList(contentItem));

        setFacetPreference(request, searchResult);

        return searchResult;
    }

    /**
     * In the case when there is no category Code / brand code this method will be called - Example _ search page.
     * 
     * @param request
     * @param searchStateData
     * @return EndecaSearch
     * @throws JAXBException
     * @throws TransformerException
     * @throws IOException
     */
    public EndecaSearch assemble(final HttpServletRequest request, final String pageUri,
            final EndecaSearchStateData searchStateData) throws JAXBException, TransformerException, IOException,
            TargetEndecaWrapperException {
        return assemble(request, pageUri, searchStateData, null);
    }

    /**
     * Calls the page defined in the experience manager and returns the contentItem corresponding to that page.
     * 
     * @param request
     * @param dimensionNavigationState
     * @param searchStateData
     * @return ContentItem
     */
    public ContentItem getContentItemForPage(final HttpServletRequest request, final String pageUri,
            final String dimensionNavigationState, final EndecaSearchStateData searchStateData)
            throws TargetEndecaWrapperException {
        if (StringUtils.isNotEmpty(dimensionNavigationState)) {
            searchStateData.setNavigationState(Arrays.asList(dimensionNavigationState));
        }

        return callAssembler(request, pageUri, searchStateData);
    }



    /**
     * Save user preference about applied facets in session.
     * 
     * @param request
     * @param searchResult
     */
    protected void setFacetPreference(final HttpServletRequest request, final EndecaSearch searchResult) {
        //set customer preference of availableOnline to session
        if (searchResult == null) {
            return;
        }
        boolean availableOnlineApplied = false;
        if (searchResult.getDimension() != null) {
            for (final EndecaDimension appliedFacet : searchResult.getDimension()) {
                if (EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAILABLE_ONLINE
                        .equals(appliedFacet.getDimensionName()) && appliedFacet.isSelected()) {
                    request.getSession().setAttribute(
                            EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAILABLE_ONLINE,
                            getAvailableOnlineDimVal());
                    availableOnlineApplied = true;
                }
            }
        }
        if (!availableOnlineApplied) {
            request.getSession()
                    .removeAttribute(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAILABLE_ONLINE);
        }

    }

    /**
     * Adding the available online preference to the searchStateData and return the updated dimensionNavigationState
     * 
     * @param request
     * @param searchStateData
     * @param dimensionNavigationState
     * @return the updated dimensionNavigationState
     */
    protected String applyAvailableOnlinePreference(final HttpServletRequest request,
            final EndecaSearchStateData searchStateData, final String dimensionNavigationState) {
        String navigationState = dimensionNavigationState;
        final String availableOnlineDimVal = (String)request.getSession()
                .getAttribute(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAILABLE_ONLINE);
        if (targetFeatureSwitchFacade.isFindInStoreStockVisibilityEnabled()) {
            if (!shouldApplyAvailableOnlinePreference(request) || availableOnlineDimVal == null
                    || !searchStateData.isPersistAvailableOnlineFacet()) {
                return navigationState;
            }
        }
        else {
            if (!shouldApplyAvailableOnlinePreference(request) || availableOnlineDimVal == null) {
                return navigationState;
            }
        }
        if (StringUtils.isNotEmpty(availableOnlineDimVal)) {
            navigationState = appendNavState(searchStateData, dimensionNavigationState, availableOnlineDimVal);
        }
        return navigationState;

    }

    private String appendNavState(final EndecaSearchStateData searchStateData, final String dimensionNavigationState,
            final String availableOnlineNavState) {
        String navigationState = null;
        if (StringUtils.isEmpty(dimensionNavigationState)
                && CollectionUtils.isEmpty(searchStateData.getNavigationState())) {
            navigationState = availableOnlineNavState;
        }
        else if (StringUtils.isNotEmpty(dimensionNavigationState)
                && !StringUtils.contains(dimensionNavigationState, availableOnlineNavState)) {
            navigationState = dimensionNavigationState + EndecaConstants.QUERY_PARAM_JOIN
                    + availableOnlineNavState;
        }
        else if (CollectionUtils.isNotEmpty(searchStateData.getNavigationState())) {
            String navState = searchStateData.getNavigationState().get(0);
            if (!StringUtils.contains(navState, availableOnlineNavState)) {
                navState = navState + EndecaConstants.QUERY_PARAM_JOIN + availableOnlineNavState;
                searchStateData.setNavigationState(Lists.newArrayList(navState));
            }
        }
        return navigationState;
    }



    /**
     * This is to determine whether the request is coming from search/category/search or the left side facets. If it is
     * a new search, it should apply the preference however should not apply if the request is by the facets.
     * 
     * @param request
     * @return true if Nrpp is a parameter in the request
     */
    protected boolean shouldApplyAvailableOnlinePreference(final HttpServletRequest request) {
        return request.getParameter(EndecaConstants.PAGE_SIZE_KEY) == null;
    }


    /**
     * This method calls the assembler dimension search for getting the navigation state for corresponding category
     * 
     * @param request
     * @param dimensionIdMap
     * @return java.lang.String
     */

    public String getQueryParmasNavigationState(final HttpServletRequest request,
            final MultiValueMap dimensionIdMap) {
        String encodedNavState = "";
        final MutableDimLocationList dimLocationList = new MutableDimLocationList();
        try {

            final Set keys = dimensionIdMap.keySet();
            if (null != keys) {
                for (final Object key : keys) {
                    // get the key values for key
                    final List list = (List)dimensionIdMap.get(key);
                    if (null != list) {
                        for (int j = 0; j < list.size(); j++) {
                            //for multiple values for the same key
                            final List subList = (List)list.get(j);
                            if (null != subList) {

                                for (int k = 0; k < subList.size(); k++) {

                                    if (!StringUtils.isEmpty(endecaDimensionCacheService.getDimension((String)key,
                                            (String)subList.get(k)))) {
                                        final MutableDimLocation dimLocation = new MutableDimLocation();
                                        final MutableDimVal dimVal = new MutableDimVal();
                                        dimVal.setDimValId(Long.parseLong(endecaDimensionCacheService.getDimension(
                                                (String)key,
                                                (String)subList.get(k))));
                                        dimLocation.setDimValue(dimVal);
                                        dimLocationList.appendDimLocation(dimLocation);
                                        LOG.info("DIMENSION Details in the Cache - for key "
                                                + (String)key
                                                + ":refinement:"
                                                + (String)subList.get(k) + ":dimension:"
                                                + endecaDimensionCacheService.getDimension((String)key,
                                                        (String)subList.get(k)));
                                    }
                                    else {
                                        LOG.error("DIMENSION_NOT_FOUND_IN_CACHE - for key "
                                                + (String)key
                                                + ":"
                                                + (String)subList.get(k)
                                                + endecaDimensionCacheService.getDimension((String)key,
                                                        (String)subList.get(k))
                                                + " for useragent="
                                                + MDC.get(LOG_IDENTIFIER_REQUEST_USERAGENT));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            encodedNavState = endecaDimensionService.getEncodedNavigationState(dimLocationList);
        }
        catch (final UrlFormatException e) {
            LOG.error(SplunkLogFormatter.formatMessage(
                    "URL FORMATTER EXCEPTION",
                    TgtutilityConstants.ErrorCode.ERR_SEARCH, ""), e);
        }
        catch (final NumberFormatException e) {
            LOG.error(SplunkLogFormatter.formatMessage(
                    " Dimenstion navigation state is not a number",
                    TgtutilityConstants.ErrorCode.ERR_SEARCH, ""), e);
        }
        catch (final ENEQueryException e) {
            LOG.error(SplunkLogFormatter.formatMessage(
                    " Exception when querying endeca",
                    TgtutilityConstants.EndecaErrorCodes.ENDECA_ENE_QUERY_EXCEPTION, ""), e);
        }
        catch (final TargetEndecaException e) {
            LOG.error(SplunkLogFormatter.formatMessage(
                    "CATEGORY CODE NOT FOUND IN CACHE FOR CATEGORY",
                    TgtutilityConstants.ErrorCode.ERR_SEARCH, ""), e);
        }
        return encodedNavState;

    }

    /**
     * Get available online dimension value
     * 
     * @return dimension Value
     */
    protected String getAvailableOnlineDimVal() {

        return getSingleDimensionNavState(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAILABLE_ONLINE,
                EndecaConstants.DimensionValue.AVAILABLE_ONLINE_TRUE);
    }

    public String getSingleDimensionNavState(final String dimId,
            final String dimVal) {
        final Map<String, String> dimensionValues = new HashMap<>();
        dimensionValues.put(dimId, dimVal);
        final String availableOnlineNavState = endecaDimensionService.getDimensionNavigationState(dimensionValues);
        return availableOnlineNavState;
    }

}
