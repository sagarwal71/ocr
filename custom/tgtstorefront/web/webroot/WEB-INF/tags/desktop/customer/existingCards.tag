<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="paymentMethod" required="true" type="de.hybris.platform.commercefacades.order.data.CCPaymentInfoData" %>
<%@ attribute name="isSelected" required="false" type="java.lang.Boolean" %>
<%@ attribute name="radio" required="false" type="java.lang.Boolean" %>
<%@ attribute name="manage" required="false" type="java.lang.Boolean" %>
<%@ attribute name="count" required="false" type="java.lang.Integer" %>
<%@ attribute name="remove" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<spring:theme code="checkout.multi.paymentDetails.continue${featuresEnabled.featureIPGEnabled ? '.review' : ''}" var="continueText" />
<spring:theme code="icon.right-arrow-large" var="largeArrow" />
<c:set var="largeArrow">
	<c:out escapeXml="true" value="${largeArrow}" />
</c:set>

<c:if test="${not empty paymentMethod.accountHolderName}">
	<c:set var="accountHolderName" value="${paymentMethod.accountHolderName}, "/>
</c:if>

<c:if test="${not empty paymentMethod.expiryYear and not empty paymentMethod.expiryMonth}">
	<c:set var="expiryDate"><spring:theme code="text.expiresDate" arguments="${paymentMethod.expiryMonth},${paymentMethod.expiryYear}"/></c:set>
</c:if>

<li class="list-item ${isSelected ? 'active ' : ''} pay-mode" data-continue-form="existingCardPaymentForm"
 data-mode-type="existing-${count}" data-continue-disabled="${paymentCreditCardDisabled ? 'true' : 'false'}" data-ec-option="tns" data-continue-text="${continueText}${largeArrow}">
	<div class="summary">
		<c:if test="${radio}">
			<form:radiobutton id="existing-card-${count}" path="cardId" cssClass="radio pay-radio" value="${paymentMethod.id}" />
		</c:if>
		<label for="exisiting-card-${count}" class="summary-label">
			<span class="card card-existing card-type-${fn:escapeXml(paymentMethod.cardTypeData.code)}">
				<span class="visuallyhidden">${fn:escapeXml(paymentMethod.cardTypeData.name)}</span>
			</span>
			<p class="card-summary ${empty paymentMethod.billingAddress ? ' without-address' : ''}">
				<%-- TODO: Simplify this once TNS is gone --%>
				<c:choose>
					<c:when test="${empty paymentMethod.billingAddress}">
						<strong>${accountHolderName}${fn:escapeXml(paymentMethod.cardNumber)}, ${expiryDate}</strong>
					</c:when>
					<c:otherwise>
						<strong>${accountHolderName}${fn:escapeXml(paymentMethod.cardNumber)}, ${expiryDate}</strong>
						<br />
						${fn:escapeXml(paymentMethod.billingAddress.line1)},
						<c:if test="${not empty paymentMethod.billingAddress.line2}">
							${fn:escapeXml(paymentMethod.billingAddress.line2)},
						</c:if>
						${fn:escapeXml(paymentMethod.billingAddress.town)},
						${fn:escapeXml(paymentMethod.billingAddress.state)},
						${fn:escapeXml(paymentMethod.billingAddress.postalCode)},
						${fn:escapeXml(paymentMethod.billingAddress.country.name)}
					</c:otherwise>
				</c:choose>
			</p>

			<c:if test="${not empty remove}">
				<div class="PanelActions PanelActions--slim">
					<c:url value="${remove}" var="removePaymentActionUrl"/>
					<form:form id="removePaymentDetails${paymentMethod.id}" action="${removePaymentActionUrl}" method="post">
						<input type="hidden" name="paymentInfoId" value="${paymentMethod.id}"/>
						<target:csrfInputToken/>
						<button type="submit" class="button-anchor button-anchor-red"><spring:theme code="text.account.manageCards.remove"/></button>
					</form:form>
				</div>
			</c:if>
		</label>
	</div>

	<c:if test="${paymentCreditCardDisabled}">
		<div class="item-detail">
			<p class="important-note"><spring:theme code="payment.info.newAndExisting.unavailable"/></p>
		</div>
	</c:if>

</li>
