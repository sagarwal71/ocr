<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="customer" tagdir="/WEB-INF/tags/desktop/customer" %>

<%@ attribute name="titles" required="true" type="java.util.Collection" %>
<%@ attribute name="selectedTitle" required="true" %>
<%@ attribute name="states" required="true" type="java.util.Collection" %>
<%@ attribute name="selectedState" required="true" %>
<%@ attribute name="countries" required="true" type="java.util.Collection" %>
<%@ attribute name="selectedCountry" required="true" %>
<%@ attribute name="allowDefault" required="false" %>
<%@ attribute name="singleInputActive" required="false" type="java.lang.Boolean" %>

<form:hidden path="addressId"/>
<formElement:formSelectBox idKey="address.title" labelKey="address.title" path="titleCode" skipBlank="false" skipBlankMessageKey="address.title.pleaseSelect" items="${titles}" selectedValue="${selectedTitle}" mandatory="false"/>
<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="text" mandatory="true"/>
<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="text" mandatory="true"/>

<c:choose>
	<c:when test="${singleLineAddressLookup}">
		<customer:addressLookup singleInputActive="${singleInputActive}">
			<customer:addressLocationFields states="${states}" selectedState="${addressForm.state}" />
		</customer:addressLookup>
	</c:when>
	<c:otherwise>
		<customer:addressLocationFields states="${states}" selectedState="${addressForm.state}" />
	</c:otherwise>
</c:choose>

<formElement:formSelectBox idKey="address.country" labelKey="address.country" path="countryIso" selectCSSClass="full-select" mandatory="true" skipBlank="false" skipBlankWhenSingle="true" skipBlankMessageKey="address.selectCountry" items="${countries}" itemValue="isocode" selectedValue="${selectedCountry}" hint="address.country.hint"/>
<formElement:formInputBox idKey="address.phoneNumber" labelKey="address.phoneNumber" path="phoneNumber" inputCSS="text" mandatory="true" type="tel"/>
<c:choose>
	<c:when test="${allowDefault}">
		<formElement:formOptCheckbox idKey="address.default" labelKey="address.default" path="defaultAddress" checked="${defaultAddress}"  mandatory="false"/>
	</c:when>
	<c:otherwise>
	 	<form:hidden path="defaultAddress"/>
	</c:otherwise>
</c:choose>
