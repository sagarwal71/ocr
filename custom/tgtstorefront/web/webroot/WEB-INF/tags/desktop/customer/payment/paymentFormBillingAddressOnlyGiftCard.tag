<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="beforeForm" required="false" fragment="true" %>
<%@ attribute name="afterForm" required="false" fragment="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="customer-payment" tagdir="/WEB-INF/tags/desktop/customer/payment" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<spring:theme code="checkout.multi.paymentDetails.continue.review" var="continueText" />
<spring:theme code="icon.right-arrow-large" var="largeArrow" />
<c:set var="largeArrow">
	<c:out escapeXml="true" value="${largeArrow}" />
</c:set>

<c:set var="paymentModeUnavailable" value="${(not isGiftCardPaymentAllowedForCart) or ipgGiftCardUnavailable}" />
<c:set var="formName" value="giftCardBillingAddressOnlyForm" />

<li class="list-item pay-mode ${isSelected or not multiplePaymentMode ? 'active' : ''}"
	data-continue-form="${formName}"
	data-continue-text="${continueText}${largeArrow}"
	data-continue-disabled="${paymentModeUnavailable}"
	data-mode-type="giftcard"
	data-ec-option="ipg-new-giftcard">

	<form:form method="post" cssClass="pay-details present-cc" action="${giftCardBillingAddressOnlyForm.action}" commandName="${formName}" novalidate="true">
		<spring:hasBindErrors name="${formName}">
			<c:set var="hasErrors" value="${true}" />
		</spring:hasBindErrors>

		<c:if test="${multiplePaymentMode}">
			<customer-payment:paymentNewGiftCardOption active="${giftCardBillingAddressOnlyForm.active}" />
		</c:if>

		<div class="item-detail">
			<p><spring:theme code="checkout.multi.paymentDetails.giftcard.message" /></p>
			<c:choose>
				<c:when test="${not isGiftCardPaymentAllowedForCart}">
					<c:set var="importantNote">
						<spring:theme code="checkout.error.paymentmethod.ipg.giftcard.not.allowed" />
					</c:set>
				</c:when>
				<c:when test="${ipgGiftCardUnavailable}">
					<c:set var="importantNote">
						<spring:theme code="checkout.error.paymentmethod.ipg.unavailable" >
							<jsp:attribute name="arguments">
								<spring:theme code="checkout.paymentMethod.giftcard" />
							</jsp:attribute>
						</spring:theme>
					</c:set>
				</c:when>
			</c:choose>
			<c:if test="${not empty importantNote}">
				<p class="important-note"><c:out value="${importantNote}" /></p>
			</c:if>

			<c:if test="${not paymentModeUnavailable}">
				<jsp:invoke fragment="beforeForm"/>
				<customer-payment:paymentBillingAddress billingAddressForm="${giftCardBillingAddressOnlyForm.billingAddress}" hasErrors="${hasErrors}" formKey="giftCard" />
				<jsp:invoke fragment="afterForm"/>
			</c:if>
		</div>

		<target:csrfInputToken/>
	</form:form>
</li>
