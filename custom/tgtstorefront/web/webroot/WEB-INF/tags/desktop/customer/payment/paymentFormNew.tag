<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="beforeForm" required="false" fragment="true" %>
<%@ attribute name="afterForm" required="false" fragment="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="customer-payment" tagdir="/WEB-INF/tags/desktop/customer/payment" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<c:set var="tnsEnabled" value="${empty newCardPaymentDetailsForm ? 'true' : newCardPaymentDetailsForm.enabled}" />
<spring:theme code="checkout.multi.paymentDetails.continue${featuresEnabled.featureIPGEnabled ? '.review' : ''}" var="continueText" />
<spring:theme code="icon.right-arrow-large" var="largeArrow" />
<c:set var="largeArrow">
	<c:out escapeXml="true" value="${largeArrow}" />
</c:set>

<spring:hasBindErrors name="newCardPaymentDetailsForm">
	<c:set var="hasErrors" value="${true}" />
</spring:hasBindErrors>

<c:if test="${tnsEnabled or hasErrors}">
	<li class="list-item pay-mode" data-continue-form="newCardPaymentDetailsForm"
	 data-mode-type="newcard" data-continue-disabled="${paymentCreditCardDisabled ? 'true' : 'false'}"
	 data-ec-option="tns" data-continue-text="${continueText}${largeArrow}" >
		<form:form method="post" cssClass="pay-details new-cc"  action="${newCardPaymentDetailsForm.action}" commandName="newCardPaymentDetailsForm" class="create_update_payment_form" novalidate="true">
			<customer-payment:paymentNewCardOption active="${newCardPaymentDetailsForm.active}" />

			<div class="item-detail new-card-form">
				<c:if test="${not paymentCreditCardDisabled}">
					<jsp:invoke fragment="beforeForm"/>
					<customer-payment:paymentNewCardForm />
					<hr />
					<customer-payment:paymentBillingAddress billingAddressForm="${newCardPaymentDetailsForm.billingAddress}" hasErrors="${hasErrors}" />
					<jsp:invoke fragment="afterForm"/>
				</c:if>
				<c:if test="${paymentCreditCardDisabled}">
					<p class="important-note"><spring:theme code="payment.info.newAndExisting.unavailable"/></p>
				</c:if>
			</div>

			<target:csrfInputToken/>
		</form:form>
	</li>
</c:if>

