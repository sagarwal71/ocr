<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="customer-payment" tagdir="/WEB-INF/tags/desktop/customer/payment" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="order-summary-card" tagdir="/WEB-INF/tags/desktop/order/summaryCard" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="customer" tagdir="/WEB-INF/tags/desktop/customer" %>
<%@ attribute name="billingAddressForm" required="false" type="au.com.target.tgtstorefront.forms.BillingAddressForm" %>
<%@ attribute name="hasErrors" required="false" type="java.lang.Boolean" %>
<%@ attribute name="formKey" required="false" type="java.lang.String" %>

<c:set var="actionContent">
	<span class="modify-address">
		<a href="#" class="button-norm summary-panel--changeButton new-address button-small-text"><spring:theme code="checkout.multi.paymentDetails.billing.change" /></a>
	</span>
	<a href="#" class="button-norm summary-panel--changeButton use-address button-small-text"><spring:theme code="checkout.multi.paymentDetails.billing.use" /></a>
</c:set>

<%--  Billing Address --%>
<div class="billing-address">
	<c:choose>
		<c:when test="${not empty billingAddressForm and not hasErrors}">
			<order-summary-card:address
				heading="checkout.multi.paymentDetails.newPaymentDetails.billingAddress"
				addressData="${billingAddressForm}"
				actionContent="${actionContent}"
				formData="${true}"
				titles="${title}"
				countries="${billingCountries}" />
		</c:when>
		<c:otherwise>
			<h3><spring:theme code="checkout.multi.paymentDetails.newPaymentDetails.billingAddress"/></h3>
		</c:otherwise>
	</c:choose>

	<%-- Billing Address Details --%>
	<div class="same-address address-form ${not empty billingAddressForm and not hasErrors ? 'hide-address-form' : ''}">

		<p><spring:theme code="form.required"/></p>

		<%-- TODO do we need this? --%>
		<form:hidden path="billingAddress.addressId" />

		<formElement:formSelectBox idKey="${formKey}.address.title" dataSameAs="${fn:escapeXml(billingAddressForm.titleCode)}" dataSameAsValue="name" labelKey="address.title" path="billingAddress.titleCode" mandatory="false" skipBlank="false" skipBlankMessageKey="address.title.pleaseSelect" items="${titles}"/>
		<formElement:formInputBox idKey="${formKey}.address.firstName" dataSameAs="${fn:escapeXml(billingAddressForm.firstName)}" labelKey="address.firstName" path="billingAddress.firstName" inputCSS="text" mandatory="true" />
		<formElement:formInputBox idKey="${formKey}.address.surname" dataSameAs="${fn:escapeXml(billingAddressForm.lastName)}" labelKey="address.surname" path="billingAddress.lastName" inputCSS="text" mandatory="true"/>


		<c:set var="addressLocationFields">
			<formElement:formInputBox idKey="${formKey}.address.line1" dataSameAs="${fn:escapeXml(billingAddressForm.line1)}" labelKey="address.line1" path="billingAddress.line1" inputCSS="text address-line-1" dynamicFillable="${true}" mandatory="true"/>
			<formElement:formInputBox idKey="${formKey}.address.line2" dataSameAs="${fn:escapeXml(billingAddressForm.line2)}" labelKey="address.line2" path="billingAddress.line2" inputCSS="text address-line-2" dynamicFillable="${true}" mandatory="false"/>
			<formElement:formInputBox idKey="${formKey}.address.townCity" dataSameAs="${fn:escapeXml(billingAddressForm.townCity)}" labelKey="address.townCity" path="billingAddress.townCity" inputCSS="text address-town" dynamicFillable="${true}" mandatory="true"/>
			<formElement:formSelectBox idKey="${formKey}.address.state" dataSameAs="${fn:escapeXml(billingAddressForm.state)}" labelKey="address.state" path="billingAddress.state" selectCSSClass="state-select full-select address-state" dynamicFillable="${true}" mandatory="true" skipBlank="false" skipBlankMessageKey="address.selectState" items="${statesWithNotApplicable}"/>
			<formElement:formInputBox idKey="${formKey}.address.postcode" dataSameAs="${fn:escapeXml(billingAddressForm.postcode)}" labelKey="address.postcode" path="billingAddress.postcode" inputCSS="text mini-text address-post-code" dynamicFillable="${true}" mandatory="true"/>
		</c:set>

		<c:choose>
			<c:when test="${singleLineAddressLookup}" >
				<c:set var="addressLocationFields">
					<customer:addressLookup singleInputActive="${not hasErrors}">
						<jsp:attribute name="beforeLocationFields">
							<formElement:formSelectBox idKey="${formKey}.address.country" dataSameAs="${fn:escapeXml(billingAddressForm.countryIso)}" labelKey="address.country" path="billingAddress.countryIso" selectCSSClass="country-select full-select" dynamicFillable="${true}" mandatory="true" skipBlank="false" skipBlankMessageKey="address.selectCountry" items="${billingCountries}" itemValue="isocode"/>
						</jsp:attribute>
						<jsp:body>
							${addressLocationFields}
						</jsp:body>
					</customer:addressLookup>
				</c:set>
			</c:when>
			<c:otherwise>
				<formElement:formSelectBox idKey="${formKey}.address.country" dataSameAs="${fn:escapeXml(billingAddressForm.countryIso)}" labelKey="address.country" path="billingAddress.countryIso" selectCSSClass="country-select full-select" dynamicFillable="${true}" mandatory="true" skipBlank="false" skipBlankMessageKey="address.selectCountry" items="${billingCountries}" itemValue="isocode"/>
			</c:otherwise>
		</c:choose>

		${addressLocationFields}

	</div>

</div>
