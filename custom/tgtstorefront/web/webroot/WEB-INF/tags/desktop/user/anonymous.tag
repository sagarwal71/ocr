<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="action" required="true" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<spring:hasBindErrors name="guestForm">
	<c:set var="checkLoginView">checked="checked"</c:set>
</spring:hasBindErrors>

<div class="log-container">

	<div class="log-guest" data-ec-option="guest">
		<cms:pageSlot var="component" position="GuestLanding">
			<cms:component component="${component}"/>
		</cms:pageSlot>
		<label for="guest-trigger">
			<spring:theme code="icon.right-arrow-large-nospace" />
		</label>
		<input id="guest-trigger" class="guest-trigger login-view" name="loginView" type="radio" value="guest" ${checkLoginView}>
		<form:form action="${action}" method="post" commandName="guestForm" novalidate="true">
			<formElement:formInputBox idKey="j_username" labelKey="login.email" labelCSS="login-view-label" placeholder="login.email" path="email" inputCSS="text" mandatory="true" type="email" autoCorrect="off"/>
			<cms:pageSlot var="component" position="GuestBullets">
				<cms:component component="${component}"/>
			</cms:pageSlot>
			<user:optIn />
			<button type="submit" class="button-fwd button-wait"><spring:theme code="login.anonymous"/><spring:theme code="icon.right-arrow-small"/></button>
			<target:csrfInputToken/>
			<p class="required"><spring:theme code="form.mandatory.message"/></p>
		</form:form>
	</div>

</div>