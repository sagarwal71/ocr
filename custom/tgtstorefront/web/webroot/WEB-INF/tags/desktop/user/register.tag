<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="action" required="true" type="java.lang.String" %>
<%@ attribute name="openablePanel" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<spring:hasBindErrors name="registerForm">
	<c:set var="checkLoginView">checked="checked"</c:set>
</spring:hasBindErrors>

<c:if test="${openablePanel and not isAnonymousCheckout}">
	<h3><spring:theme code="register.header" /></h3>
	<input id="register-trigger" class="register-trigger login-view" name="loginView" type="radio" value="register" ${checkLoginView}>
	<label for="register-trigger">
		<spring:theme code="register.link" />
	</label>
</c:if>

<c:if test="${not openablePanel and not isAnonymousCheckout}">
	<h2><spring:theme code="register.header" /></h2>
</c:if>

<form:form method="post" commandName="registerForm" action="${action}" novalidate="true">
	<cms:pageSlot var="component" position="RegisterLanding">
		<cms:component component="${component}"/>
	</cms:pageSlot>
	<form:hidden path="uid"/>
	<formElement:formSelectBox idKey="register.title" path="titleCode" skipBlank="false" mandatory="false" skipBlankMessageKey="register.title" items="${titles}"/>
	<formElement:formInputBox idKey="register.firstName" labelKey="register.firstName" labelCSS="login-view-label" placeholder="register.firstName" path="firstName" inputCSS="text" mandatory="true"/>
	<formElement:formInputBox idKey="register.lastName" labelKey="register.lastName" labelCSS="login-view-label" placeholder="register.lastName" path="lastName" inputCSS="text" mandatory="true"/>
	<formElement:formInputBox idKey="register.email" labelKey="register.email" labelCSS="login-view-label" placeholder="register.email" path="email" inputCSS="text" mandatory="true" type="email" autoCorrect="off"/>
	<formElement:formPasswordBox idKey="password" labelKey="register.pwd" labelCSS="login-view-label" placeholder="register.pwd" path="pwd" inputCSS="text password strength" mandatory="true" strength="${true}" showhide="${true}" />
	<formElement:formInputBox idKey="register.mobileNumber" labelKey="register.mobileNumber" labelCSS="login-view-label" placeholder="register.mobileNumber" path="mobileNumber" inputCSS="text" type="tel"/>
	<formElement:formOptCheckbox idKey="register.registerForMail" labelKey="register.registerForMail" path="registerForMail" checked="${false}" inputCSS="reg-opt" />
	<user:inlinePrivacy />
	<user:optIn />
	<button type="submit" class="button-fwd button-wait"><spring:theme code="login.createAccount" /><spring:theme code="icon.right-arrow-small"/></button>
	<p class="required"><spring:theme code="form.mandatory.message"/></p>
	<target:csrfInputToken/>

</form:form>
