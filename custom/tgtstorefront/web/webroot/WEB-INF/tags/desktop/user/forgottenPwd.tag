<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ attribute name="cssClass" required="false" type="java.lang.String" %>

<p class="required"><spring:theme code="form.required"/></p>
<form:form method="post" commandName="forgottenPwdForm" cssClass="${cssClass}" novalidate="true">
	<formElement:formInputBox idKey="forgottenPwd.email" labelKey="forgottenPwd.email" path="email" inputCSS="text" mandatory="true" type="email" autoCorrect="off"/>
	<target:csrfInputToken />
	<div class="f-buttons">
		<button type="submit" class="button-fwd button-single"><spring:theme code='forgottenPwd.submit'/><spring:theme code="icon.right-arrow-small"/></button>
	</div>
</form:form>