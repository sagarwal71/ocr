<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="accordion" required="false" type="java.lang.Boolean" %>
<%@ attribute name="inlinePrivacySource" required="false" type="java.lang.String" %>

<c:set var="inlineTerms">
	<c:choose>
		<c:when test="${not empty inlinePrivacySource and inlinePrivacySource eq 'mumshub'}">
			<spring:theme code="customerSubscription.mumshub.inlineterms" />
		</c:when>
		<c:otherwise>
			<spring:theme code="personalInformation.inlineterms" />
		</c:otherwise>
	</c:choose>
</c:set>

<c:choose>
	<c:when test="${accordion}">
		<c:set var="attrs">href="#" class="terms-accordion"</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="attrs">href="/modal/privacy" class="lightbox" data-lightbox-type="content"</c:set>
	</c:otherwise>
</c:choose>

<cms:pageSlot var="component" position="InlinePrivacy">
	<c:set var="hasVisibleComponentsInlinePrivacy" value="${true}" />
</cms:pageSlot>

<div class="inline-terms inline-privacy">
	<c:choose>
		<c:when test="${hasVisibleComponentsInlinePrivacy}">
			<cms:pageSlot var="component" position="InlinePrivacy">
				<cms:component component="${component}"/>
			</cms:pageSlot>
		</c:when>
		<c:otherwise>
			<p>${inlineTerms}</p>
		</c:otherwise>
	</c:choose>
</div>