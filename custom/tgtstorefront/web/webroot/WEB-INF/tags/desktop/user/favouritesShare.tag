<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ attribute name="shareLimit" required="false" type="java.lang.Integer" %>

<form:form method="post" commandName="favouritesListShareForm" novalidate="true">
	<p><spring:theme code="favourites.share.form.copy" arguments="${shareLimit}" /></p>
	<formElement:formInputBox
		idKey="favourites.share.form.recipientName"
		labelKey="favourites.share.form.recipientName"
		path="fullName"
		inputCSS="text"
		mandatory="true" />
	<c:set var="emailExtra"><p class="label-note"><spring:theme code="favourites.share.form.recipientEmail.extra" /></p></c:set>
	<formElement:formInputBox
		idKey="favourites.share.form.recipientEmail"
		labelKey="favourites.share.form.recipientEmail"
		path="recipientEmailAddress"
		inputCSS="text"
		mandatory="true"
		labelExtra="${emailExtra}" />
	<c:set var="countdown"><span class="text-countdown-message"></span></c:set>
	<spring:theme code="favourites.share.form.maximum" var="maximumText" arguments="${countdown}" />
	<spring:theme code="favourites.share.form.remaining" var="remainingData" arguments="{{digit}}" />
	<spring:theme code="favourites.share.form.toomany" var="toomanyData" arguments="{{digit}}" />
	<c:set var="textAreaData">
		data-limit="120"
		data-remaining-text="${remainingData}"
		data-too-many-text="${toomanyData}"
	</c:set>
	<c:set var="textAreaLabel"><p class="label-note text-countdown-label">${maximumText}</p></c:set>
	<formElement:formTextArea
		idKey="favourites.share.form.message"
		labelKey="favourites.share.form.message"
		path="messageText"
		mandatory="false"
		elementCSS="text-countdown"
		fieldData="${textAreaData}"
		labelExtra="${textAreaLabel}"
		areaRows="3" />
	<div class="form-actions">
		<button class="button-fwd"><spring:theme code="favourites.share.send" /></button>
	</div>
</form:form>
