<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="baseProductCode" required="true" type="java.lang.String" %>
<%@ attribute name="classifications" required="true" type="java.lang.Object" %>
<%@ attribute name="sizeChartUrl" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>

<c:if test="${not empty classifications}">
	<div class="prod-classifications prod-var-list">

		<label><spring:theme code="product.variants.${product.giftCard ? 'cardvalue': 'size'}"/></label>

		<div class="prod-var-group variant-changeable">
			<ul class="classification-list var-change-list" data-prod-context="${baseProductCode}">
				<c:forEach items="${classifications}" var="classification">
					<c:set var="activeClass" value="${classification.active ? 'active' : ''}" />
					<c:set var="outOfStockClass" value="${(classification.outOfStock and not product.preview) ? ' prod-var-no-stock' : ''}" />

					<li class="classification-option prod-var-item var-change-item ${activeClass} ${outOfStockClass}">
						<c:set var="classificationTitle">${classification.name}<c:if test="${classification.outOfStock}"> : <spring:theme code="product.stockposition.soldout" /></c:if></c:set>
						<a title="${classificationTitle}" href="${classification.url}">${classification.name}</a>
					</li>
				</c:forEach>

				<c:if test="${not empty sizeChartUrl}">
					<c:url value="${sizeChartUrl}" var="sizeChartsUrl" />
					<spring:theme code="product.page.link.sizeCharts" var="sizeChartsText"/>
					<li class="prod-var-extra"><a href="${sizeChartsUrl}" title="${sizeChartsText}" class="lightbox size-chart-link" data-iw="600" data-lightbox-type="content"><span class="visuallyhidden">${sizeChartsText}</span></a></li>
				</c:if>
			</ul>
		</div>

	</div>

</c:if>
