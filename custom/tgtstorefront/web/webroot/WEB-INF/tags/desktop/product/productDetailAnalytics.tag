<%@ tag body-content="empty" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<feature:enabled name="uiTrackDisplayOnly">
	<c:set var="displayOnly" value="${isProductDisplayOnly}" />
</feature:enabled>

<c:set var="productEcommJson">
	<template:productEcommJson
		product="${product}"
		assorted="${product.assorted}"
		displayOnly="${displayOnly}" />
</c:set>
data-ec-product='${productEcommJson}'
