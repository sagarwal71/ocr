<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="cssClass" required="false" type="java.lang.String" %>

<li class="ProductVariants-item ${cssClass}">
	<jsp:doBody />
</li>
