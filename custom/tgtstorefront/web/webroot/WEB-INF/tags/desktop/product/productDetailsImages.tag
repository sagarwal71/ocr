<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="galleryImages" required="true" type="java.util.List" %>
<%@ attribute name="hidePromoStatus" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>

<c:set var="maxThumbs" value="5" />
<c:set var="hasControls" value="${fn:length(galleryImages) gt maxThumbs}" />

<div id="prod-gallery" class="prod-gallery">
	<div class="prod-images variant-super-changeable">
		<div class="prod-hero">
			<div class="hero-image">
				<c:if test="${not hidePromoStatus}">
					<product:productPromotionStatus product="${product}" isFlexible="true" hideNew="${product.preview}" />
				</c:if>
				<product:productPrimaryImage product="${product}" format="zoom" imgClass="gallery-hero gallery-hero-${product.baseProductCode}" />
			</div>
			<c:set var="hasImage" value="${ycommerce:productImage(product, 'zoom')}" />
			<c:if test="${not empty hasImage}">
				<div class="image-tools hide-for-small">
					<span class="zoom">
						<spring:theme code="product.page.link.zoom"/>
					</span>
					<ycommerce:testId code="productDetails_zoomImage_button">
						<c:set var="missingImage"><asset:resource code="img.missingProductImage.zoom" /></c:set>
						<a href="${not empty galleryImages[0].zoom.url ? galleryImages[0].zoom.url : missingImage }" class="lightbox enlarge small-link" data-rel="${product.baseProductCode}" data-lightbox-type="gallery"><spring:theme code="product.page.link.enlarge"/></a>
					</ycommerce:testId>
				</div>
			</c:if>
		</div>
		<c:if test="${fn:length(galleryImages) > 1}">
			<div class="prod-thumbs">
				<div class="Slider slider-gallery" data-slider-type="gallery">
					<c:if test="${hasControls}">
						<div class="SliderControls-button--prev">
							<a href="#prev"><span class="visuallyhidden">Prev</span></a>
						</div>
					</c:if>
					<div class="Slider-container">
						<ul class="Slider-slides">
							<c:forEach items="${galleryImages}" var="image" varStatus="varStatus">
								<c:set var="cssClass" value="${varStatus.index eq 0 ? 'active' : 'lightbox'}" />
								<li class="Slider-slideItem">
									<a href="${image.zoom.url}" class="cloudzoom-gallery ${cssClass}" data-rel="${product.baseProductCode}" data-cloudzoom="useZoom: '.gallery-hero-${product.baseProductCode}', image: '${image.zoom.url}', zoomImage: '${image.zoom.url}'" data-lightbox-type="gallery">
										<img src="${image.thumbnail.url}" />
									</a>
								</li>
							</c:forEach>
						</ul>
					</div>
					<c:if test="${hasControls}">
						<div class="SliderControls-button--next">
							<a href="#next"><span class="visuallyhidden">Next</span></a>
						</div>
					</c:if>
				</div>
			</div>
		</c:if>
	</div>
</div>
