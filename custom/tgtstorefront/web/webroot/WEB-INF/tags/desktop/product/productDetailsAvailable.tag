<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="kiosk" tagdir="/WEB-INF/tags/desktop/kiosk" %>

<c:set var="inStockOnline" value="${product.stock.stockLevelStatus.code ne 'outOfStock' }" />
<c:set var="inStockBulkyBoard" value="${not empty product.bulkyBoard and product.bulkyBoard.inStockForInStorePurchase}" />

<c:if test="${not product.preview}">
	<div class="prod-available">
	    <c:if test="${not empty product.productPromotionalDeliveryText}">
	        	<div class="delivery-promotional-message"><p><spring:theme code="icon.delivery" />${product.productPromotionalDeliveryText}</p></div>
	    </c:if>
		<c:if test="${inStockOnline or inStockBulkyBoard}">
			<h5 class="prod-heading variant-changeable"><spring:theme code="product.availableFor.title" /></h5>
			<ul class="delivery-list delivery-list-horz variant-changeable">
				<kiosk:availableForInStorePurchase inStockBulkyBoard="${inStockBulkyBoard}" />
				<c:set var="onlineConcernPrefix"><kiosk:deliveryConcernPrefix /></c:set>
				<c:forEach items="${product.deliveryModes}" var="deliveryMode">
					<li class="delivery-item has-tip" title="${deliveryMode.shortDescription}">
						<spring:theme code="${deliveryMode.available ? 'icon.tick' : 'icon.dash'}" /><span class="label">${deliveryMode.name}</span>
						<c:if test="${deliveryMode.deliveryToStore and product.clickAndCollectExcludeTargetCountry}">
							<span class="excludes"> <spring:theme code="product.page.delivery.exclude.tc" /></span>
						</c:if>
					</li>
				</c:forEach>
			</ul>
		</c:if>
	</div>
</c:if>
