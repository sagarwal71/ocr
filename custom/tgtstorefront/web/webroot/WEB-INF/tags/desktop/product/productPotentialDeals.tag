<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="promotions" required="true" type="java.util.Collection" %>
<%@ attribute name="dealDescription" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>

<spring:theme code="icon.exclamation" var="iconExclamation" />
<spring:theme code="icon.dollar" var="iconDollar" />
<c:set var="helpLink"><cart:dealsHelp /></c:set>
<c:choose>
	<c:when test="${not empty promotions}">
		<c:forEach items="${promotions}" var="promotion">
			<c:if test="${not empty promotion and not promotion.tmdPromotion}">
				<c:choose>
					<c:when test="${not anonymousCachable and not empty promotion.couldFireMessages}">
						<p class="deals deals-potential">${iconExclamation}${promotion.couldFireMessages[0]}${helpLink}</p>
					</c:when>
					<c:otherwise>
						<p class="deals deals-available">${iconDollar}${promotion.description}${helpLink}</p>
					</c:otherwise>
				</c:choose>
			</c:if>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<c:if test="${not empty dealDescription}">
			<p class="deals deals-available">${iconDollar}${dealDescription}${helpLink}</p>
		</c:if>
	</c:otherwise>
</c:choose>