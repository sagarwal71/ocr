<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%--
	Display NLP terms and conditions when the New Lower Price sticker is displayed.
--%>
<c:if test="${(empty product.productPromotionalDeliverySticker) and (not product.newArrived) and (not empty product.newLowerPriceStartDate)}">
	<c:forEach items="${product.promotionStatuses}" var="promotionStatus" begin="0" end="0">
		<spring:theme code="text.promostatus.terms.${promotionStatus.promotionClass}" var="terms">
			<jsp:attribute name="arguments">
				<fmt:formatDate value="${product.newLowerPriceStartDate}" pattern="dd/MM/yyyy"/>
			</jsp:attribute>
		</spring:theme>

		<c:if test="${not empty terms}">
			<div class="product-promo-terms">
				<p>${terms}</p>
			</div>
		</c:if>
	</c:forEach>
</c:if>
