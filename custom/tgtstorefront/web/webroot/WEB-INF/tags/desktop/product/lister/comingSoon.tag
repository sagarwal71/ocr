<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="coming-soon-info">
	<span class="promo-status promotion-sticker coming-soon-sticker">
		<spring:theme code="comingsoon.button" />
	</span>
</div>
