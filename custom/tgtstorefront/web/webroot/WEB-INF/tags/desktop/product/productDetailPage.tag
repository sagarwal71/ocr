<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/desktop/util" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<div class="ProductDetails has-react-panel">
	<product:containedProductOfferMicroformat schemaData="${schemaData}" />

	<div class="react-root">
		<util:reactInitialState
			productData='"${product.baseProductCode}": ${targetProductListerData}'
			applicationData='"page": {"type": "${pageType}","data":{"baseProductCode": "${product.baseProductCode}"}},"environment":${environmentData}'
			featureData="${uiFeaturesEnabled}"
			lookData="${shopTheLookData}" />
	</div>

	<c:set var="contentSlotContext" value="full" scope="request" />
	<util:reactContentSlot slotName="relatedProducts">
		<cms:pageSlot var="relatedProductsComponent" position="RelatedProducts">
			<cms:component component="${relatedProductsComponent}"/>
		</cms:pageSlot>
	</util:reactContentSlot>
	<util:reactContentSlot slotName="socialMedia">
		<cms:pageSlot var="comp" position="SocialMedia">
			<cms:component component="${comp}"/>
		</cms:pageSlot>
	</util:reactContentSlot>
	<util:reactContentSlot slotName="reviewSection">
		<cms:pageSlot var="comp" position="ReviewSection">
			<cms:component component="${comp}"/>
		</cms:pageSlot>
	</util:reactContentSlot>
	<util:reactContentSlot slotName="productIntroduction">
		<cms:pageSlot var="comp" position="Introduction">
			<cms:component component="${comp}"/>
		</cms:pageSlot>
	</util:reactContentSlot>

	<c:set var="promotionContent">
		<cms:pageSlot var="comp" position="Promotions">
			<cms:component component="${comp}"/>
		</cms:pageSlot>
	</c:set>
	<c:if test="${fn:length(fn:trim(promotionContent)) gt 0}">
		<util:reactContentSlot slotName="promotions">
			<div class="prod-promotions">
				${promotionContent}
			</div>
		</util:reactContentSlot>
	</c:if>
</div>
