<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="product" required="true" type="java.lang.Object" %>

<c:set var="favHeartClass" value="FavHeart FavHeart--button" />
<feature:enabled name="uiPDPaddToCart">
	<c:set var="favHeartClass" value="${favHeartClass} FavHeart--collapse" />
</feature:enabled>
<feature:enabled name="findInStore2PDP">
	<c:set var="newPDP" value="${true}" />
</feature:enabled>
<feature:enabled name="featureWishlistFavourites">
	<c:if test="${not kioskMode}">
		<c:if test="${not (product.giftCard and product.productTypeCode eq 'digital')}">
			<spring:theme code="favourites.saved" var="savedText"/>
			<spring:theme code="favourites.unsaved" var="unsavedText"/>
			<c:set var="selectedVariantCode" value="${not empty product.sellableVariantDisplayCode ? product.sellableVariantDisplayCode : product.colourVariantCode }" />
			<div class="FavouritableProducts u-alignCenter"  ${favouritesData}>
				<a href="#" class="${favHeartClass}" data-base-prod-code="${product.baseProductCode}" data-selected-variant-code="${selectedVariantCode}" data-saved-text="${savedText}" data-unsaved-text="${unsavedText}">
					<%-- TODO: Get this working with <use> tag (iOS, Android browser issue rendering on variant change) --%>
					<feature:enabled name="uiStickyHeader"><%--
						--%><svg viewBox="-6 -8 45 43"><%--
							--%><path class="FavHeart-heart" stroke-width="1.5" stroke-linejoin="round" d="M14.6,4.8L14.6,4.8C14.6,4.8,14.6,4.8,14.6,4.8 M15,25c-0.2,0-0.5,0-0.7-0.1C9,22.8,4.6,19,1.9,14C1.3,12.9,1,11.6,1,10.3 C0.9,6.5,3.5,3.2,7.1,2.3C10,1.5,13,2.4,15,4.4c2-2,4.9-2.8,7.8-2.1c3.7,0.9,6.2,4.2,6.2,8.1c0,1.3-0.3,2.5-0.9,3.7 c-2.7,5-7.1,8.9-12.5,10.9C15.4,25,15.2,25,15,25"/><%--
						--%></svg><%--
					--%></feature:enabled><%--
					--%><feature:disabled name="uiStickyHeader"><%--
						--%><svg viewBox="4 5 32 30"><%--
							--%><path class="FavHeart-heart" stroke-width="1.5" stroke-linejoin="round" d="M28.05 17.707c0-5.3-6.975-6.975-8.525-.528-1.55-6.448-8.525-4.772-8.525.527C11 22.3 19.525 28.5 19.525 28.5s8.525-6.2 8.525-10.793z"/><%--
						--%></svg><%--
						--%></feature:disabled><%--
					--%><span class="FavHeart-addText ${not newPDP ? 'hide-for-small' : ''}"></span>
				</a>
			</div>
		</c:if>
	</c:if>
</feature:enabled>
