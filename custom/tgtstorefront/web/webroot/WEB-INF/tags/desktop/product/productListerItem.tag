<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ tag import="au.com.target.tgtcore.product.data.ProductDisplayType" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="showBasketButton" required="false" %>
<%@ attribute name="listerType" required="false" type="java.lang.String" %>
<%@ attribute name="listerName" required="false" type="java.lang.String" %>
<%@ attribute name="cssClass" required="false" type="java.lang.String" %>
<%@ attribute name="favControls" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showInStock" required="false" type="java.lang.Boolean" %>
<%@ attribute name="hideDeals" required="false" type="java.lang.Boolean" %>
<%@ attribute name="cmsComponentId" required="false" %>
<%@ attribute name="position" required="false" type="java.lang.Integer" %>
<%@ attribute name="absoluteIndex" required="false" type="java.lang.Integer" %>
<%@ attribute name="hideDetails" required="false" %>
<%@ attribute name="hideRatings" required="false" %>
<%@ attribute name="unveilStartPosition" required="false" %>
<%@ attribute name="favVariants" required="false" type="java.lang.Boolean" %>
<%@ attribute name="trackDisplayOnly" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="lister" tagdir="/WEB-INF/tags/desktop/product/lister" %>

<c:set var="isGrid" value="${listerType eq 'grid'}" />
<c:set var="isLook" value="${listerType eq 'look'}" />
<c:set var="maxAvailStoreQty" value="${product.maxAvailStoreQty}"/>

<feature:enabled name="fis2StoreStockVisibility">
	<c:set var="maxAvailStoreQty" value="${product.maxActualConsolidatedStoreStock}" />
</feature:enabled>

<c:if test="${product.maxAvailOnlineQty gt 0 or maxAvailStoreQty gt 0}">
	<c:set var="availbleOnlineOrInstore" value="${true}" />
</c:if>


<c:if test="${fn:length(product.targetVariantProductListerData) > 0 and not empty product.targetVariantProductListerData[0]}">
	<c:set var="firstVariant" value="${product.targetVariantProductListerData[0]}" />
	<spring:eval var="PREORDER_AVAILABLE" expression="T(au.com.target.tgtcore.product.data.ProductDisplayType).PREORDER_AVAILABLE"/>
	<spring:eval var="COMING_SOON" expression="T(au.com.target.tgtcore.product.data.ProductDisplayType).COMING_SOON"/>
</c:if>

<c:set var="isProductDisplayTypePreOrder" value="${not empty firstVariant and firstVariant.productDisplayType eq PREORDER_AVAILABLE}" />
<c:set var="isProductDisplayTypeComingSoon" value="${not empty firstVariant and firstVariant.productDisplayType eq COMING_SOON}" />
<c:set var="isProductAvailableSoon" value="${isProductDisplayTypePreOrder or isProductDisplayTypeComingSoon}" />

<c:set value="${target:abbreviateString(product.name, (isGrid ? 65 : 100))}" var="productNameAbbreviated" />
<c:url value="${not empty firstVariant ? firstVariant.url : product.url}" var="productUrl">
	<%-- kiosk --%>
	<c:if test="${not empty bulkyBoardStoreNumber}">
		<c:param name="bbsn" value="${bulkyBoardStoreNumber}" />
	</c:if>
</c:url>

<feature:enabled name="uiPDPaddToCart">
	<c:set var="newAdd" value="${true}" />
</feature:enabled>

<feature:enabled name="featureConsolidatedStoreStockVisibility">
	<c:set var="consolidatedStoreStock" value="${true}" />
</feature:enabled>

<feature:enabled name="findInStore2PDP">
	<c:set var="newPDP" value="${true}" />
</feature:enabled>

<c:set var="stockPositionMessage">
	<c:choose>
		<c:when test="${not product.inStock}">
			<p class="stock-position"><spring:theme code="icon.dash" /><spring:theme code="product.stockposition.soldout.${product.onlineExclusive ? 'onlineonly' : 'instore'}" /></p>
		</c:when>
		<c:when test="${showInStock and product.inStock}">
			<p class="stock-position in-stock"><spring:theme code="icon.tick" /><spring:theme code="product.stockposition.inStock" /></p>
		</c:when>
		<c:when test="${product.maxAvailOnlineQty eq 0 and product.maxAvailStoreQty gt 0 and not iosAppMode and not product.onlineExclusive}">
			<a class="stock-position-fis" href="${productUrl}">
				<spring:theme code="product.stockposition.findInStore" />
			</a>
		</c:when>
	</c:choose>
</c:set>

<c:if test="${listerType eq 'look'}">
	<c:set var="listerType" value="hero" />
</c:if>

<c:set var="primaryImageUrl"><product:productListerImage product="${product}" format="${listerType}" index="0" unavailable="true" /></c:set>
<c:set var="secondaryImageUrl"><product:productListerImage product="${product}" format="${listerType}" index="1"  /></c:set>

<c:if test="${trackDisplayOnly}">
	<c:set var="displayOnly" value="${not empty firstVariant ? firstVariant.displayOnly : product.displayOnly}" />
</c:if>
<c:set var="isBaseProduct" value="${true}" />

<%-- ec.js product or impression data (200 max) --%>
<c:set var="productEcommJson"><template:productEcommJson
	product="${product}"
	assorted="${not empty firstVariant ? firstVariant.assorted : product.assorted}"
	displayOnly="${displayOnly}"
	list="${listerName}"
	position="${absoluteIndex le 200 ? absoluteIndex : ''}" /></c:set>

<c:set var="availableQuantity" value="${product.stock.stockLevelStatus.code ne 'outOfStock' ? 100 : 0}" />
<c:set var="isAllVariantsSoldOut" value="${availableQuantity eq 0}" />
<c:set var="isSelectedVariantsSoldOut" value="${false}" />
<c:set var="productCode" value="${not empty firstVariant ? firstVariant.colourVariantCode : product.code}" />
<c:set var="assorted" value="${not empty firstVariant ? firstVariant.assorted : false}" />
<c:if test="${not empty product.selectedVariantData}">
	<c:set var="isBaseProduct" value="${false}" />
	<c:url value="${product.selectedVariantData.url}" var="productUrl">
		<c:if test="${not empty bulkyBoardStoreNumber}">
			<c:param name="bbsn" value="${bulkyBoardStoreNumber}" />
		</c:if>
	</c:url>
	<c:set var="selectedVariant" value="${true}" />
	<c:set var="selectedColor" value="${product.selectedVariantData.color}" />
	<c:set var="selectedSize" value="${product.selectedVariantData.size}" />
	<c:set var="sellableVariant" value="${product.selectedVariantData.sellableVariant}" />
	<c:set var="productCode" value="${product.selectedVariantData.productCode}" />
	<c:if test="${sellableVariant}">
		<c:set var="availableQuantity" value="${product.selectedVariantData.availableQty}" />
		<c:set var="isSelectedVariantsSoldOut" value="${availableQuantity eq 0}" />
	</c:if>
	<c:set var="assorted" value="${product.selectedVariantData.assorted}" />
</c:if>
<c:set var="otherVariantsAvailable" value="${product.colourVariantCount gt 1 or firstVariant.sizeVariantCount gt 1}" />

<%-- Base product with no variants --%>
<c:set var="hideVariantInfo" value="${not otherVariantsAvailable}" />
<c:set var="hideInfoForProductEndOfLife" value="${favControls and favVariants and not availbleOnlineOrInstore and (isBaseProduct or not otherVariantsAvailable) and consolidatedStoreStock}"/>

<lister:productListItem className="${cssClass}"
	primaryImage="${primaryImageUrl}"
	secondaryImage="${secondaryImageUrl}"
	baseCode="${product.baseProductCode}"
	productCode="${productCode}"
	currentProduct="${selectedVariant ? productCode : product.baseProductCode}"
	ecProduct="${productEcommJson}"
	>
	<div class="lead">
		<a href="${productUrl}" title="${product.name}" class="thumb ga-ec-click">
			<c:choose>
				<c:when test="${not empty unveilStartPosition and position ge unveilStartPosition}">
					<lister:unveilImage src="${primaryImageUrl}" name="${product.name}" className="thumb-img" />
				</c:when>
				<c:otherwise>
					<img src="${primaryImageUrl}" alt="${product.name}" title="${product.name}" class="thumb-img"/>
				</c:otherwise>
			</c:choose>
			<product:productPromotionStatus product="${product}" listingPage="${true}" />
		</a>
		<c:if test="${(not product.giftCard) and (not favControls) or (not product.giftCard and favControls and not favVariants) }">
			<c:set var="quickView" value="Quick view" />
			<lister:quickView
				url="${productUrl}"
				title="${quickView} &ndash; ${product.name}"
				dataRel="${not empty cmsComponentId ? cmsComponentId : 'lister'}"
				name="${product.name}" />
		</c:if>
	</div>
	<c:if test="${not hideDetails}">
		<div class="detail">
			<div class="summary">
				<h3 class="name-heading">
					<a href="${productUrl}" title="${product.name}" class="ga-ec-click">${productNameAbbreviated}</a>
				</h3>
				<c:if test="${not isGrid and not empty product.description}">
					<p class="hide-for-tiny clip-for-small">${target:abbreviateHTMLString(product.description, 100)}</p>
				</c:if>
			</div>
			<c:if test="${not hideInfoForProductEndOfLife}">
				<div class="price-info">
					<product:pricePoint product="${product}" />
				</div>
			</c:if>

			<c:if test="${favControls and favVariants}">
				<div class="ProdListVariantInfo">
					<c:if test="${not empty selectedColor or not empty selectedSize}">
						<spring:theme code="product.list.variants.select.separator" var="separator" />
						<c:set var="separatorText" value="${(not empty selectedColor and not empty selectedSize) ? separator : ''}" />
						<p class="ProdListVariantInfo-selected">${selectedColor}${separatorText}${selectedSize}</p>
					</c:if>
					<c:set var="editOption" value="${sellableVariant ? '.edit' :
						otherVariantsAvailable and selectedVariant ? '.size' : '.option' }" />
					<spring:theme code="product.list.variants.select${editOption}" var="editText" />
					<c:if test="${not hideVariantInfo and not product.displayOnly and not hideInfoForProductEndOfLife}">
						<a href="${productUrl}" class="ProdListVariantInfo-edit FavEditButton"><span class="ProdListVariantInfo-editIcon"></span>${editText}</a>
					</c:if>
					<c:if test="${hideInfoForProductEndOfLife}">
						<div class="FavProducts-unavailable">
							<spring:theme code="product.page.message.soldout"/>
						</div>
					</c:if>
				</div>
			</c:if>

			<c:if test="${product.preview}">
				<lister:comingSoon />
			</c:if>
			<c:if test="${not hideRatings and not isProductAvailableSoon}">
				<product:productRatings product="${product}" />
			</c:if>
			<c:if test="${not favControls or (favControls and not favVariants)}">
				<product:productSwatchHover product="${product}" />
			</c:if>
			<c:if test="${not empty product.dealDescription and not hideDeals}">
				<lister:dealLink className="${isGrid ? 'deals-center' : ''}" description="${product.dealDescription}" />
			</c:if>
			<c:if test="${not favControls or (favControls and not favVariants)}">
				<c:if test="${not product.preview and not isProductAvailableSoon}">
					${stockPositionMessage}
				</c:if>
			</c:if>
			<c:if test="${favControls and assorted and not hideInfoForProductEndOfLife}">
				<div class="feedback-msg warning">
					<spring:theme code="product.variants.assortments.disclaimer" />
				</div>
			</c:if>
			<c:if test="${not favControls and not empty firstVariant}">
				<c:choose>
					<c:when test="${isProductDisplayTypePreOrder and not empty firstVariant.normalSaleStartDate}">
						<div class="StockInfo-availableDate StockInfo-availableDate--preOrder">
							<spring:theme code="product.stockposition.availableSoon" />&nbsp;<format:formatShortDate date="${firstVariant.normalSaleStartDate}" hideYear="true" />
						</div>
					</c:when>
					<c:when test="${isProductDisplayTypeComingSoon and not empty firstVariant.normalSaleStartDate}">
						<div class="StockInfo-availableDate StockInfo-availableDate--comingSoon">
							<spring:theme code="product.stockposition.availableSoon" />&nbsp;<format:formatShortDate date="${firstVariant.normalSaleStartDate}" hideYear="true" />
						</div>
					</c:when>
				</c:choose>
			</c:if>
		</div>
	</c:if>
	<c:if test="${favControls}">
		<div class="FavControls">
			<c:if test="${not hideInfoForProductEndOfLife}">
				<c:if test="${favVariants}">
					<c:set var="controlWrap">
						<form class="add-to-cart-form"
							action="/cart/add"
							method="POST"
							data-wait-inline="${true}"
							data-feedback-timeout="3000"
							data-new-add="${newAdd}"
							data-feedback-classes="stacked slideUp"><div class="${newAdd ? 'ProdButtons' : ''}">||</div></form>
					</c:set>
					<c:set var="controlWrap" value="${fn:split(controlWrap, '||')}" />
				</c:if>
				${controlWrap[0]}
				<a href="#" class="button-norm FavControl-button FavRemoveButton">
					<spring:theme code="icon.plain-cross" /><span class="visuallyhidden">Remove</span>
				</a>
				<c:if test="${not product.preview}">
					<c:choose>
						<c:when test="${maxAvailStoreQty gt 0 and product.maxAvailOnlineQty le 0}">
							<a href="${productUrl}#store" class="button-fwd-grey-outline FavControl-button FavStore FavStore--button">
								<i class="Icon Icon--store Icon--size150"></i>
								<span class="visuallyhidden">
									<spring:theme code="product.tab.availableInStore" />
								</span>
							</a>
						</c:when>
						<c:when test="${not favVariants}">
							<a href="${productUrl}" class="button-${assorted ? 'warning' : 'fwd'} FavControl-button FavBuyButton quick-add-action">
								<span class="FavBuyButton-icon">&nbsp;</span>
								<span class="visuallyhidden">
									<spring:theme code="text.addToCart.buynow.button" />
								</span>
							</a>
						</c:when>
						<c:otherwise>
							<input type="hidden" name="productCode" value="${not empty product.selectedVariantData ? product.selectedVariantData.productCode : product.baseProductCode}" />
							<button
								class="button-${assorted ? 'warning' : 'fwd'} FavControl-button FavBuyButton ${not sellableVariant or availableQuantity le 0 ? 'FavControl-button--disabled' : ''}"
								name="purchase"
								value="buynow"
								type="button">
								<span class="FavBuyButton-icon">&nbsp;</span>
								<span class="visuallyhidden">
									<spring:theme code="text.addToCart.buynow.button" />
								</span>
							</button>
						</c:otherwise>
					</c:choose>
				</c:if>
				<c:if test="${product.preview}">
					<spring:theme code="comingsoon.modal.href" var="comingSoonModalHref"/>
					<a class="lightbox button-fwd FavControl-button button-coming-soon" data-lightbox-type="content" href="${comingSoonModalHref}">
						<span class="icon">&nbsp;</span>
						<span class="visuallyhidden">
							<spring:theme code="comingsoon.button" />
						</span>
					</a>
				</c:if>
				${controlWrap[1]}
			</div>
			<c:if test="${favVariants and consolidatedStoreStock}">
				<c:set var="highStockLevel"><spring:theme code="product.variants.level.highstock"/></c:set>
				<c:if test="${product.maxAvailOnlineQty ge highStockLevel}">
					<c:set var="stockLevel" value="high" />
				</c:if>
				<c:if test="${product.maxAvailOnlineQty lt highStockLevel}">
					<c:set var="stockLevel" value="low" />
				</c:if>
				<c:if test="${product.maxAvailOnlineQty eq 0 and maxAvailStoreQty ge 0 }">
					<c:set var="stockLevel" value="network" />
				</c:if>
				<c:if test="${product.maxAvailOnlineQty eq 0 and maxAvailStoreQty eq 0 }">
					<c:set var="stockLevel" value="unavailable" />
				</c:if>
				<spring:theme code="product.variants.in.new.${stockLevel}stock" var="stockText" />
				<product:productStockLevelIndicator
					compact="${true}"
					row="${true}"
					showFis="${stockLevel eq 'network'}"
					productUrl="${productUrl}"
					messageOnly="${true}"
					stockLevel="${stockLevel}"
					selectMessage="${stockText}"
				/>
				<div class="ProdVariantsPanel ${newPDP ? 'is-new-pdp' : ''}">
					<button class="ProdVariantsPanel-close"><spring:theme code="icon.plain-cross" /><span class="visuallyhidden"><spring:theme code="product.variants.panel.cancel" /></span></button>
					<div class="ProdVariantsPanel-variants">
					</div>
				</div>
			</c:if>
			<c:if test="${favVariants and not consolidatedStoreStock}">
				<product:productStockLevelIndicator
					availableQuantity="${availableQuantity}"
					compact="${true}"
					row="${true}"
					showFis="${product.displayOnly or isAllVariantsSoldOut or isSelectedVariantsSoldOut}"
					productUrl="${productUrl}"
				/>
				<%-- TODO: Remove modifier and refactor once live --%>
				<div class="ProdVariantsPanel ${newPDP ? 'is-new-pdp' : ''}">
					<button class="ProdVariantsPanel-close"><spring:theme code="icon.plain-cross" /><span class="visuallyhidden"><spring:theme code="product.variants.panel.cancel" /></span></button>
					<div class="ProdVariantsPanel-variants">
					</div>
				</div>
			</c:if>
		</c:if>
	</c:if>
	<c:if test="${hideInfoForProductEndOfLife}">
		<product:favEndOfLifeControls originalCategoryCode="${product.originalCategoryCode}" />
	</c:if>
	<c:if test="${hideDetails and not product.inStock and not isProductAvailableSoon}">
		${stockPositionMessage}
	</c:if>
	<product:favourites product="${product}"/>
</lister:productListItem>
