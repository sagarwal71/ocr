<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<c:set var="storeEnabled" value="${not product.onlineExclusive and availableInStore}" />
<c:set var="onlineEnabled" value="${availableOnline or product.onlineExclusive}" />

<feature:enabled name="featureConsolidatedStoreStockVisibility">
	<c:if test="${product.productTypeCode ne 'giftCard' and ((availableOnline or availableInStore) or (product.onlineExclusive or product.showStoreStockForProduct))}">
		<div class="Tab ${not storeEnabled and not onlineEnabled ? 'Tab--unavailable' : ''}">
			<component:tabItem
				name="product.tab.availableOnline"
				selected="${onlineEnabled}"
				disabled="${not onlineEnabled}"
				glyph="online"
				triggerData="online"
				hash="#online"
			/>
			<component:tabItem
				name="product.tab.availableInStore"
				selected="${storeEnabled}"
				disabled="${not storeEnabled}"
				glyph="store"
				nameClass="${storeEnabled ? 'PreferredStoreName' : ''}"
				triggerData="store"
				hash="#store"
			>
				<jsp:attribute name="afterName">
					<c:if test="${storeEnabled}">
						<script>if (window.t_Inject) { t_Inject('preferredStoreName');}</script>
					</c:if>
				</jsp:attribute>
			</component:tabItem>
		</div>
	</c:if>
</feature:enabled>
