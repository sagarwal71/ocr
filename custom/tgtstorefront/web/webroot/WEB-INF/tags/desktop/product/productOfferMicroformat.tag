<%@ tag trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature"%>
<%@ attribute name="offerAggregate" required="true" type="java.lang.Boolean"%>
<%@ attribute name="inStock" required="true" type="java.lang.Boolean"%>
<%@ attribute name="inStockAtStore" required="true" type="java.lang.Boolean"%>

<%--
	Reference:
	http://schema.org/Offer
	http://schema.org/AggregateOffer
--%>

<spring:theme code="text.headerLogo" var="sellerName" />

<c:set var="offerAggregate"
	value="${offerAggregate and not empty product.priceRange and product.priceRange.maxPrice.value ne product.priceRange.minPrice.value}" />
<c:choose>
	<c:when test="${inStock and inStockAtStore}">
		<c:set var="itempropAvailabilityContent" value="InStock" />
	</c:when>
	<c:when test="${inStock}">
		<feature:disabled name="featureConsolidatedStoreStockVisibility">
			<c:set var="itempropAvailabilityContent" value="InStock" />
		</feature:disabled>
		<feature:enabled name="featureConsolidatedStoreStockVisibility">
			<c:set var="itempropAvailabilityContent" value="OnlineOnly" />
		</feature:enabled>
	</c:when>
	<c:when test="${inStockAtStore}">
		<c:set var="itempropAvailabilityContent" value="InStoreOnly" />
	</c:when>
	<c:otherwise>
		<c:set var="itempropAvailabilityContent" value="OutOfStock" />
	</c:otherwise>
</c:choose>


<div itemprop="offers" itemscope="itemscope" itemtype="${offerAggregate ? 'http://schema.org/AggregateOffer' : 'http://schema.org/Offer' }">
	<meta itemprop="category" content="${product.categories[fn:length(product.categories) - 1].name}" />
	<meta itemprop="itemCondition" itemtype="http://schema.org/OfferItemCondition" content="http://schema.org/NewCondition" />
	<meta itemprop="priceCurrency" content="AUD" />
	<meta itemprop="seller" content="${sellerName}" />
	<c:choose>
		<c:when test="${offerAggregate}">
			<meta itemprop="highPrice"
				content="${product.priceRange.maxPrice.value}" />
			<meta itemprop="lowPrice"
				content="${product.priceRange.minPrice.value}" />
		</c:when>
		<c:otherwise>
			<meta itemprop="price" content="${not empty product.price ? product.price.value : ''}" />
			<c:set var="itempropAvailability" scope="request">itemprop="availability" content="http://schema.org/${itempropAvailabilityContent}"</c:set>
		</c:otherwise>
	</c:choose>

	<jsp:doBody />

	<c:remove var="itempropAvailability" scope="request" />
</div>
