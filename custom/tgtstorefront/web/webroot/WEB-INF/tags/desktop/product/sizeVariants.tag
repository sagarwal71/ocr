<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="sizes" required="true" type="java.lang.Object" %>
<%@ attribute name="sizeChartUrl" required="false" type="java.lang.String" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<feature:enabled name="featureConsolidatedStoreStockVisibility">
	<c:set var="consolidatedStock" value="${true}" />
</feature:enabled>

<c:if test="${not empty sizes}">
	<div class="ProductVariants ProductVariants-sizes" data-new-variant="true" data-prod-context="${product.baseProductCode}">
		<div class="tse-scrollable horizontal">
			<div class="tse-content">
				<ul class="ProductVariants-list ProductVariants-sizeList">
					<c:if test="${not empty sizeChartUrl}">
						<product:variantItem cssClass="ProductVariants-sizing">
							<c:url value="${sizeChartUrl}" var="sizeChartsUrl" />
							<spring:theme code="product.page.link.sizing" var="sizeChartsText"/>
							<a href="${sizeChartsUrl}" title="${sizeChartsText}" data-type="size" class="ProductVariants-sizingLink ProductVariants-sizeLink  lightbox" data-iw="600" data-lightbox-type="content">
								<span class="ProductVariants-sizingName"><spring:theme code="product.page.link.sizing.accessible" /></span>
							</a>
						</product:variantItem>
					</c:if>
					<c:forEach items="${sizes}" var="size">
						<c:if test="${not consolidatedStock}">
							<c:set var="stockClass" value="ProductVariants-link--${size.outOfStock ? 'no' : 'in'}" />
						</c:if>
						<product:variantItem>
							<a href="${size.url}" title="${size.name}" data-type="size" data-code="${size.code}" class="ProductVariants-link ${stockClass} ProductVariants-sizeLink ${size.active ? ' is-selected' : ''}"><span class="ProductVariants-sizeName"><spring:theme code="product.variants.size.select" arguments="${size.name}" /></span></a>
						</product:variantItem>
					</c:forEach>
				</ul>
			</div>
		</div>
	</div>
</c:if>
