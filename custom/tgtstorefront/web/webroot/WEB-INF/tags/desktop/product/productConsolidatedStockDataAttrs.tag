<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ attribute name="pageMode" required="false" type="java.lang.String" %>
<%@ attribute name="availableForStore" required="false" type="java.lang.String" %>
<%@ attribute name="multipleModes" required="false" type="java.lang.Boolean" %>

<feature:enabled name="featureConsolidatedStoreStockVisibility">
	<c:set var="stockDataJson">
		<product:productStockLevelJson
			variants="${variantsStock}"
			availableForStore="${product.showStoreStockForProduct}"
		/>
	</c:set>
	<c:set var="stockMsg"><product:productStockMessageJson /></c:set>
	<c:set var="stockDataAttr"> data-online-stock='${stockDataJson}' data-stock-msg='${stockMsg}' data-consolidated-stock="true" data-page-mode="${pageMode}" data-multiple-modes="${multipleModes}" </c:set>
</feature:enabled>

${stockDataAttr}
