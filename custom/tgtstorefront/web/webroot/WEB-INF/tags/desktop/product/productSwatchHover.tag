<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>
<%@ taglib prefix="lister" tagdir="/WEB-INF/tags/desktop/product/lister" %>


<%-- Variants with the colour name No Colour are not true colour variants --%>
<c:if test="${fn:length(product.targetVariantProductListerData) > 1}">

	<c:set var="placeholder"><asset:resource code="img.missingProductImage.thumbnail" /></c:set>

	<c:set var="limitForDesktop" value="5" />
	<c:set var="limitForSmall" value="3" />

	<%-- We don't want whitespace in between the <li> nodes
	--%><ul class="swat-list"><%--
		--%><c:forEach items="${product.targetVariantProductListerData}" var="colour" end="${limitForDesktop - 1}" varStatus="status"><%--
			--%><lister:swatchItem className="${not colour.inStock ? 'swat-list-item-oos ' : ''} ${status.count > limitForSmall ? 'hide-for-small' : ''}" gridImage="${fn:length(colour.gridImageUrls) > 0 ? target:splitEndecaUrlField(colour.gridImageUrls[0]) : placeholder}">
					<c:set var="colourTitle">${colour.swatchColour}<c:if test="${not colour.inStock}"> : <spring:theme code="product.stockposition.soldout" /></c:if></c:set>
					<a title="${colourTitle}" href="${colour.url}" class="ga-ec-click">
						<c:if test="${not colour.inStock}">
							<span class="swat-item-icon-oos"><spring:theme code="icon.plain-cross"/></span>
						</c:if>
						<c:set var="imgSrc" value="${fn:length(colour.thumbImageUrl) > 0 ? target:splitEndecaUrlField(colour.thumbImageUrl[0]) : placeholder}" />
						<lister:unveilImage className="swat-list-img" name="${colourTitle}" src="${imgSrc}" />
					</a>
				</lister:swatchItem><%--
		--%></c:forEach><%--
	--%></ul>

	<c:if test="${fn:length(product.targetVariantProductListerData) > limitForSmall}">
		<spring:theme code="text.viewMoreColours" var="viewMoreColours" />
		<p class="swat-list-note ${fn:length(product.targetVariantProductListerData) <= limitForDesktop ? 'only-for-small' : ''}"><a title="${viewMoreColours}" href="${product.url}">${viewMoreColours}</a></p>
	</c:if>

</c:if>
