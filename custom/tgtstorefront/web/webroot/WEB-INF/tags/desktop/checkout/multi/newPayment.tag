<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>
<%@ taglib prefix="customer-payment" tagdir="/WEB-INF/tags/desktop/customer/payment" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>

<c:if test="${not kioskMode and not empty paymentMethods and existingCardPaymentForm.enabled}">
	<h3><spring:theme code="checkout.multi.paymentDetails.newPaymentDetailsOr" /></h3>
</c:if>

<ul class="block-list card-list">

	<c:if test="${not kioskMode}">
		<customer-payment:paymentFormVendor>
			<jsp:attribute name="afterForm">
				<multi-checkout:paymentCommonHiddenFields />
				<div class="vendor-terms">
					<multi-checkout:termsCheck />
				</div>
			</jsp:attribute>
		</customer-payment:paymentFormVendor>

		<%-- PayPal --%>
		<c:if test="${not featuresEnabled.featureIPGEnabled}">
			<customer-payment:paymentFormNew>
				<jsp:attribute name="afterForm">
					<multi-checkout:paymentCommonHiddenFields />
				</jsp:attribute>
			</customer-payment:paymentFormNew>
		</c:if>

		<%-- credit card --%>
		<c:if test="${featuresEnabled.featureIPGEnabled}" >
			<customer-payment:paymentFormBillingAddressOnlyCreditCard>
				<jsp:attribute name="afterForm">
					<multi-checkout:paymentCommonHiddenFields />
				</jsp:attribute>
			</customer-payment:paymentFormBillingAddressOnlyCreditCard>
		</c:if>

		<%-- gift card --%>
		<c:if test="${featuresEnabled.featureIPGEnabled and featuresEnabled.featureGiftCardPaymentEnabled}">
			<customer-payment:paymentFormBillingAddressOnlyGiftCard>
				<jsp:attribute name="afterForm">
					<multi-checkout:paymentCommonHiddenFields />
				</jsp:attribute>
			</customer-payment:paymentFormBillingAddressOnlyGiftCard>
		</c:if>


	</c:if>

	<%-- kiosk --%>
	<c:if test="${kioskMode}">
		<customer-payment:paymentFormBillingAddressOnlyKiosk>
			<jsp:attribute name="afterForm">
				<multi-checkout:paymentCommonHiddenFields />
			</jsp:attribute>
		</customer-payment:paymentFormBillingAddressOnlyKiosk>
	</c:if>

</ul>
