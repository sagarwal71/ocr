<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="store" required="false" type="au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData" %>
<%@ attribute name="checked" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--

	To save on page load, each line has comments to remove whitespace
	class names are short
	and tag names are short

	--%><c:if test="${not empty checked}"><%--
		--%><c:set var="checkedAttr">checked="checked"</c:set><%--
	--%></c:if><%--

	--%><li class="list-item"><%--
		--%><input id="sn-${store.storeNumber}" name="sSN" class="r" type="radio" value="${store.storeNumber}" ${checkedAttr}><%--
		--%><label for="sn-${store.storeNumber}"><%--
			--%><div class="detail"><%--
				--%><b class="t-d-s">${fn:escapeXml(store.name)} - ${fn:escapeXml(store.type)}</b><%--
				--%><b class="s-d-t">${fn:escapeXml(store.type)} - ${fn:escapeXml(store.name)}</b><%--

				--%><i><br /><%--
					--%><c:if test="${not empty store.targetAddressData}"><%--
						--%>${fn:escapeXml(store.targetAddressData.line1)}<br /><%--
						--%>${fn:escapeXml(store.targetAddressData.town)}, ${fn:escapeXml(store.targetAddressData.state)}, ${fn:escapeXml(store.targetAddressData.postalCode)}, ${fn:escapeXml(store.targetAddressData.country.name)}<br/><%--
					--%></c:if><%--
					--%><c:if test="${not empty store.formattedDistance}"><%--
						--%><spring:theme code="checkout.multi.deliveryAddress.storeDistance" text="Distance - {0}" arguments="${store.formattedDistance}" argumentSeparator=";;;" /><%--
					--%></c:if><%--
					--%><b class="ph"><br><br><b><spring:theme code="storeFinder.details.phone" /></b> ${fn:escapeXml(store.targetAddressData.phone)}</b><%--
				--%></i><%--
			--%></div><%--
		--%></label><%--
		--%><p class="actions hfs"><a href="store?ssn=${store.storeNumber}" class="lb" data-iw="750"><spring:theme code="checkout.multi.deliveryAddress.viewStoreDetails" text="View store details"/></a></p><%--
	--%></li><%--

EOF --%>
