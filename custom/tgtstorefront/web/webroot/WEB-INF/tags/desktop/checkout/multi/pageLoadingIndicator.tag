<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<p class="co-loading-msg"><spring:theme code="checkout.page.loading" text="Please wait . . . ."/></p>
