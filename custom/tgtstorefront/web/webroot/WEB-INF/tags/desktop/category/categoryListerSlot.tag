<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ attribute name="slotName" required="true" %>
<%@ attribute name="divider" required="false" %>

<c:set var="contentSlotContext" value="wide" scope="request" />
<cms:pageSlot var="listerComponent" position="slotName]">
	<cms:component component="${listerComponent}"/>
</cms:pageSlot>
<c:remove var="contentSlotContext" scope="request" />