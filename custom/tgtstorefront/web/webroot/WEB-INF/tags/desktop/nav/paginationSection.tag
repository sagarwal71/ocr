<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchUrl" required="true" %>
<%@ attribute name="wsUrl" required="true" %>
<%@ attribute name="themeMsgKey" required="false" %>
<%@ attribute name="pageClassName" required="false" %>
<%@ attribute name="currentPage" required="true" %>
<%@ attribute name="numberOfPages" required="true" %>
<%@ attribute name="section" required="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set var="wsUrl" value="${fn:replace(wsUrl, '%7BrecordsPerPage%7D', searchPageData.pagination.pageSize)}" />
<c:if test="${not kioskMode || section eq 'bottom'}">
	<div class="RefineMenu-section RefineMenu-section--collapse refine-pagination ${pageClassName} ${kioskMode ? 'RefineMenu-section--noBorder' : ''}">
		<c:if test="${not kioskMode}">
			<nav:pagerLink
				isEnabled="${searchPageData.pagination.currentPage > 0}"
				pagerWide="${false}"
				code="Previous"
				direction="left"
				number="${currentPage - 1}"
				searchUrl="${searchUrl}"
				wsUrl="${wsUrl}"
				themeMsgKey="${themeMsgKey}"
				cssClass="RefineMenu-arrow refine-arrow prev ${section eq 'bottom' ? 'scroll-top' : ''}"
				disabledLink="${currentPage eq 0}"
			/>
		</c:if>
		<c:set var="sortOptions">
			<c:forEach begin="1" end="${searchPageData.pagination.numberOfPages}" var="pageNum">
				<c:set var="selectedAttribute">${(searchPageData.pagination.currentPage + 1) eq pageNum ? 'selected="selected"' : ''}</c:set>
				<c:set var="displayValue">
					<c:if test="${kioskMode}">
						<spring:theme code="search.page.currentPage" arguments="${pageNum}||${numberOfPages}" argumentSeparator="||" />
					</c:if>
					<c:if test="${not kioskMode}">
						${pageNum}
					</c:if>
				</c:set>
				<option value="${wsUrl}&page=${pageNum - 1}" ${selectedAttribute}>${displayValue}</option>
			</c:forEach>
		</c:set>

		<c:choose>
			<c:when test="${not kioskMode}">
				<div class="RefineMenu-pageInfo">
					<spring:theme code="search.page.currentPage" arguments="${currentPage + 1}||${numberOfPages}" argumentSeparator="||" />
					&nbsp;<i class="Icon Icon--arrowDown is-grey"></i>
					<select name="page-select" id="refine-form" class="RefineMenu-sortSelect page-select ${section eq 'bottom' ? 'scroll-top' : ''}">
						${sortOptions}
					</select>
				</div>
			</c:when>
			<c:otherwise>
				<form id="sort-form" method="get">
					<select name="page-select" class="chosen-select sort-options-multi">
						${sortOptions}
					</select>
				</form>
			</c:otherwise>
		</c:choose>
		<c:if test="${not kioskMode}">
			<nav:pagerLink
				isEnabled="${(searchPageData.pagination.currentPage + 1) < searchPageData.pagination.numberOfPages}"
				pagerWide="${false}"
				code="Next"
				direction="right"
				number="${currentPage + 1}"
				searchUrl="${searchUrl}"
				wsUrl="${wsUrl}"
				themeMsgKey="${themeMsgKey}"
				cssClass="RefineMenu-arrow refine-arrow next ${section eq 'bottom' ? 'scroll-top' : ''}"
				disabledLink="${currentPage eq (numberOfPages - 1)}"
			/>
		</c:if>
	</div>
</c:if>
