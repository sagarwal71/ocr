<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="facet" tagdir="/WEB-INF/tags/desktop/nav/facet" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ attribute name="facets" required="true" type="java.lang.Object" %>
<%@ attribute name="facetName" required="true" %>
<%@ attribute name="additionalAttributes" required="false" %>

<feature:enabled name="newSubCategoriesUx">
	<c:set var="container">
		<div class="show-more ShowHideToggle no-toggle-for-desktop" data-more-message="More" data-less-message="Less" data-item-height="34">||</div>
	</c:set>
	<c:set var="container" value="${fn:split(container, '||')}"/>
</feature:enabled>

<c:if test="${not empty container}">
	<c:out value="${container[0]}" escapeXml="false" />
</c:if>
<div class="facet-category">
	<ul class="FacetCategory">
		<c:if test="${not empty facets.values}">
			<facet:section facets="${facets.values}" type="categoryList" facetName="${facetName}" additionalAttributes="${additionalAttributes}" />
		</c:if>
	</ul>
</div>
<c:if test="${not empty container}">
	<c:out value="${container[1]}" escapeXml="false" />
</c:if>
