<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="breadcrumbs" required="true" type="java.util.List" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%-- https://developers.google.com/search/docs/data-types/breadcrumbs --%>
<c:if test="${not empty breadcrumbs and fn:length(breadcrumbs) > 0}">
	<div class="breadcrumb hfma">
		<c:url value="${fullyQualifiedDomainName}" var="homeUrl"/>
		<ol vocab="http://schema.org/" typeof="BreadcrumbList">
			<li class="home" property="itemListElement" typeof="ListItem">
				<a class="crumb" property="item" typeof="WebPage" href="${homeUrl}">
					<span property="name"><spring:message code="breadcrumb.home"/></span>
				</a>
				<meta property="position" content="1" />
			</li>
			<c:forEach items="${breadcrumbs}" var="breadcrumb" varStatus="status">
				<li class="${breadcrumb.linkClass}" property="itemListElement" typeof="ListItem">
					<c:choose>
						<c:when test="${breadcrumb.linkClass eq 'active'}">
							<meta property="item" typeof="WebPage" href="${breadcrumb.url}" />
							<span class="crumb current-crumb ${not empty product ? 'hide-for-tiny' : ''}" property="name">${breadcrumb.name}</span>
						</c:when>
						<c:otherwise>
							<c:url value="${breadcrumb.url}" var="breadcrumbUrl"/>
							<a href="${breadcrumbUrl}" class="crumb" property="item" typeof="WebPage">
								<span property="name">${breadcrumb.name}</span>
							</a>
						</c:otherwise>
					</c:choose>
					<meta property="position" content="${status.count + 1}" />
				</li>
			</c:forEach>
		</ol>
	</div>
</c:if>
