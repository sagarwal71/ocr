<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchUrl" required="true" %>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ attribute name="msgKey" required="false" %>
<%@ attribute name="linkClass" required="false" %>
<%@ attribute name="labelClass" required="false" %>
<%@ attribute name="wsUrl" required="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<c:set var="themeMsgKey" value="${not empty msgKey ? msgKey : 'search.page'}"/>

<div class="pager-refine">
	<ul class="pager pager-pages">
		<li class="previous option">
			<nav:pagerLink
				isEnabled="${not mobileAppMode and searchPageData.pagination.currentPage > 0}"
				pagerWide="${(searchPageData.pagination.currentPage + 1) eq searchPageData.pagination.numberOfPages}"
				code="Previous"
				direction="left"
				number="${searchPageData.pagination.currentPage - 1}"
				searchUrl="${searchUrl}"
				themeMsgKey="${themeMsgKey}"
				cssClass="refine-arrow prev scroll-top ${linkClass}"
				wsUrl="${fn:replace(wsUrl, '%7BrecordsPerPage%7D', searchPageData.pagination.pageSize)}"
			/>
		</li>
		<c:if test="${(searchPageData.pagination.numberOfPages eq 1)}">
			<c:set var="labelClass" value="${labelClass} is-hidden" />
		</c:if>
		<li class="label refine-pagination ${labelClass}">
			<spring:theme
				code="${themeMsgKey}.currentPage"
				arguments="${searchPageData.pagination.currentPage + 1},${searchPageData.pagination.numberOfPages},${searchPageData.pagination.totalNumberOfResults}"
				/>
		</li>
		<li class="next option">
			<nav:pagerLink
				isEnabled="${(searchPageData.pagination.currentPage + 1) < searchPageData.pagination.numberOfPages}"
				pagerWide="${mobileAppMode or searchPageData.pagination.currentPage eq 0}"
				code="Next"
				direction="right"
				number="${searchPageData.pagination.currentPage + 1}"
				searchUrl="${searchUrl}"
				themeMsgKey="${themeMsgKey}"
				cssClass="refine-arrow next scroll-top ${linkClass}"
				wsUrl="${fn:replace(wsUrl, '%7BrecordsPerPage%7D', searchPageData.pagination.pageSize)}"
			/>
		</li>
	</ul>
</div>
