<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="selected" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<div class="nav-seg left-nav">
	<spring:theme code="text.account.myAccount" text="My Account" var="myAccount"/>
	<h3><a href="${targetUrlsMapping.myAccount}" title="${myAccount}">${myAccount}</a></h3>
	<ul class="nav-level-1">
		<li>
			<nav:accountNavLink url="${targetUrlsMapping.myAccountPersonalDetails}" titleProperty="text.account.myPersonalDetails" selected="${selected}" />
			<ul class="nav-level-2">
				<li><nav:accountNavLink url="${targetUrlsMapping.myAccountUpdatePersonalDetails}" titleProperty="text.account.myPersonalDetails.changeDetails" selected="${selected}" /></li>
				<li><nav:accountNavLink url="${targetUrlsMapping.myAccountUpdatePassword}" titleProperty="text.account.myPersonalDetails.changePassword" selected="${selected}" /></li>
			</ul>
		</li>
		<li>
			<nav:accountNavLink url="${targetUrlsMapping.myAccountOrders}" titleProperty="text.account.myOrderDetails" selected="${selected}" />
		</li>
		<li>
			<nav:accountNavLink url="${targetUrlsMapping.myAccountPayment}" titleProperty="text.account.myPaymentDetails" selected="${selected}" />
		</li>
		<li>
			<nav:accountNavLink url="${targetUrlsMapping.myAccountAddressDetails}" titleProperty="text.account.myAddressDetails" selected="${selected}" />
		</li>
		<li class="only-for-small">
			<form:form action="${targetUrlsMapping.logout}" method="post">
				<button type="submit"  class="button-anchor nav-seg-button cust-state-trig"><spring:theme code="header.link.logout"/></button>
				<target:csrfInputToken/>
			</form:form>
		</li>
	</ul>
</div>
