<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ attribute name="entry" required="true" type="au.com.target.tgtstorefront.navigation.megamenu.MegaMenuEntry" %>

<c:set var="categoryItemClass">mCi</c:set>
<c:if test="${not empty entry.imageUrl}">
	<c:set var="categoryItemClass">${categoryItemClass} mCi--banner</c:set>
</c:if>
<c:choose>
	<c:when test="${entry.style eq 'NewLink'}">
		<c:set var="categoryItemClass">${categoryItemClass} mCi--new</c:set>
	</c:when>
	<c:when test="${entry.style eq 'SaleLink'}">
		<c:set var="categoryItemClass">${categoryItemClass} mCi--sale</c:set>
	</c:when>
	<c:when test="${entry.style eq 'DividedLink'}">
		<c:set var="categoryItemClass">${categoryItemClass} mCi--divided</c:set>
	</c:when>
</c:choose>

<c:set var="container">
	<c:choose>
		<c:when test="${not empty entry.linkUrl}">
			<a href="${target:validUrlString(entry.linkUrl)}" title="${entry.text}" class="${categoryItemClass}">|</a>
		</c:when>
		<c:otherwise>
			<span class="${categoryItemClass}" title="${entry.text}">|</span>
		</c:otherwise>
	</c:choose>
</c:set>

<c:set var="wrapElement" value="${fn:split(container,'|')}" />
<c:out value="${wrapElement[0]}" escapeXml="false" />

<c:choose>
	<c:when test="${not empty entry.imageUrl}">
		<img class="hide-for-small mm-defer" alt="${entry.imageAltText}" data-src="${entry.imageUrl}" data-screen-support="large"/>
		<span class="only-for-small">${entry.text}</span>
	</c:when>
	<c:otherwise>
		<spring:theme code="link.withArrow" arguments="${entry.text}" />
	</c:otherwise>
</c:choose>
<c:out value="${wrapElement[1]}" escapeXml="false" />
