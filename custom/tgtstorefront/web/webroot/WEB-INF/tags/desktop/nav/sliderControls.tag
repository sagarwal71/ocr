<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ attribute name="hidePause" required="false" type="java.lang.Boolean"%>

<c:if test="${not hidePause}">
	<div class="SliderControls-button--pause play-control">
		<a href="#pause">
			<span class="visuallyhidden"><spring:theme code="text.slidercontrols.pause" /></span>
		</a>
	</div>
</c:if>

<ul class="SliderControls">
	<li class="SliderControls-button--prev SliderControls-button--sibling">
		<a href="#prev">
			<span class="visuallyhidden"><spring:theme code="text.slidercontrols.previous" /></span>
		</a>
	</li><li class="SliderControls-button--next SliderControls-button--sibling">
		<a href="#next">
			<span class="visuallyhidden"><spring:theme code="text.slidercontrols.next" /></span>
		</a>
	</li>
</ul>
