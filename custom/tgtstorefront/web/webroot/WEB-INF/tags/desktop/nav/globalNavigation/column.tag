<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="globalNavigation" tagdir="/WEB-INF/tags/desktop/nav/globalNavigation" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="column" required="true" type="au.com.target.tgtstorefront.navigation.megamenu.MegaMenuColumn" %>
<%@ attribute name="department" type="au.com.target.tgtstorefront.navigation.megamenu.MegaMenuDepartment" %>
<%@ attribute name="lastColumn" required="true" type="java.lang.Boolean" %>
<%@ attribute name="isAlwaysOpen" required="true" type="java.lang.Boolean" %>


<c:set var="columnClass">Menu-column</c:set>
<c:if test="${column.style eq 'HighlightedColumn'}">
	<c:choose>
		<c:when test="${isAlwaysOpen}">
			<c:set var="columnClass">${columnClass} Menu-column--highlighted</c:set>
		</c:when>
		<c:otherwise>
			<c:set var="columnClass">${columnClass} Menu-column--multiCategory</c:set>
		</c:otherwise>
	</c:choose>

</c:if>
<c:if test="${lastColumn}">
	<c:set var="columnClass">${columnClass} is-last</c:set>
</c:if>
<c:if test="${not isAlwaysOpen}">
	<c:set var="columnClass">${columnClass} is-closed</c:set>
</c:if>
<c:set var="columnName" value="${department.title} -> ${column.title}" />
<c:set var="columnID" value="${fn:replace(fn:replace(columnName, ' ', ''), '->', ':')}" />

<section class="${columnClass}">
	<c:choose>
		<c:when test="${not isAlwaysOpen}">
			<input type="checkbox" name="${department.title}" value="${columnName}" id="${columnID}" class="visuallyhidden accordion-toggle" />
			<label class="Menu-columnName" for="${columnID}">${column.title}</label>
		</c:when>
		<c:otherwise>
			<c:if test="${not empty column.title}">
				<div class="Menu-columnName" for="${column.title}">${column.title}</div>
			</c:if>
		</c:otherwise>
	</c:choose>
	<div class="Menu-columnSection accordion-content ${not isAlwaysOpen ? 'Menu-columnSection--inset' : ''}">
		<c:forEach items="${column.sections}" var="section">
			<globalNavigation:section section="${section}" parentName="${columnName}" />
		</c:forEach>
	</div>
	<c:if test="${isAlwaysOpen and not empty department}">
		<a href="${department.link}" class="only-for-small Menu-allLink" data-title="${department.title}" title="${department.title}">
			<spring:theme code="link.withArrow" arguments="All ${department.title}" />
		</a>
	</c:if>
</section>
