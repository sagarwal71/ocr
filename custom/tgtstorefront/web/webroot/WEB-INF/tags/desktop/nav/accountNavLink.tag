<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>

<%@ attribute name="url" required="true" type="java.lang.String" %>
<%@ attribute name="titleProperty" required="true" type="java.lang.String" %>
<%@ attribute name="titleDefault" required="false" type="java.lang.String" %>
<%@ attribute name="selected" required="false" type="java.lang.String" %>

<c:set var="title">
	<spring:theme code="${titleProperty}" text="${titleDefault}"/>
</c:set>
<c:if test="${selected eq titleProperty}">
	<c:set var="classAttr">class="current"</c:set>
</c:if>
<a href="${url}" title="${title}" ${classAttr} ${ycommerce:getTestIdAttribute(titleProperty, pageContext)}>${title}</a>
