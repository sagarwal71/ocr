<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>
<%@ attribute name="extraClass" %>

<div class="search-lead ${extraClass}" data-autocorrected="${not empty searchPageData.autoCorrectedTerm}">
	<c:if test="${not empty searchPageData}">
		<c:set var="searchTerm" value="${not empty searchPageData.autoCorrectedTerm ? searchPageData.autoCorrectedTerm : searchPageData.freeTextSearch}" />
		<c:set var="searchTermPill">
			<span class="Pill">${searchTerm}</span>
		</c:set>

		<h1 class="heading listing-heading">
			<spring:theme code="search.page.searchText.result" arguments="${searchTermPill};;;${searchPageData.pagination.totalNumberOfResults}" argumentSeparator=";;;" />
		</h1>

		<%-- Search term Autocorrected --%>
		<c:if test="${not empty searchPageData.autoCorrectedTerm}">
			<div class="search-msg">
				<h3 class="strong-heading">
					<spring:theme code="search.page.searchText.undo.message" arguments="${searchPageData.freeTextSearch};;;${searchPageData.autoCorrectedTerm}" argumentSeparator=";;;" />
				</h3>
			</div>
		</c:if>

	</c:if>

	<nav:searchSpellingSuggestion
		spellingSuggestions="${searchPageData.spellingSuggestions}"
		hasResults="${searchPageData.pagination.totalNumberOfResults gt 0}" />
</div>
