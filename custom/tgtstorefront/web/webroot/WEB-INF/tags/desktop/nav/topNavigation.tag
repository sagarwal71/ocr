<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<c:if test="${not empty breadcrumbs and fn:length(breadcrumbs) > 0}">
	<c:set var="topCategoryUrl" value="${breadcrumbs[0].url}"/>
</c:if>
<c:set var="newMegamenu" value="${false}" />
<feature:enabled name="uiNewMegamenuDisplay">
	<c:set var="menuClass" value="Menu" />
	<c:set var="newMegamenu" value="${true}" />
</feature:enabled>
<c:set var="outerMenuClass"><target:megaMenuCache cacheKey="megamenu.outerClass">mm-Menu ${menuClass}</target:megaMenuCache></c:set>
<div id="megamenu" class="${outerMenuClass}" data-top-category-url="${topCategoryUrl}">
	<div class="mm-Menu-inner">
		<header class="mm-Menu-nav">
			<c:if test="${not newMegamenu}">
				<span class="mm-Header-close only-for-small device-menu-trigger">
					<spring:theme code="icon.plain-cross" /><br>
					<span class="mm-Header-close-label"><spring:theme code="header.link.menu" /></span>
				</span>
			</c:if>
			<ul class="mm-Departments mm-Departments--horizontal">
				<target:megaMenuCache cacheKey="megamenu.navigation">
					<c:set var="navBarSlot" value="${slots['NavigationBar'].cmsComponents}" />
					<cms:pageSlot var="component" position="NavigationBar">
						<c:choose>
							<c:when test="${newMegamenu}">
								<c:if test="${component.uid eq 'TargetNewCategoriesBarComponent'}">
									<cms:component component="${component}"/>
								</c:if>
							</c:when>
							<c:otherwise>
								<c:if test="${component.uid eq 'TargetCategoriesBarComponent'}">
									<cms:component component="${component}"/>
								</c:if>
							</c:otherwise>
						</c:choose>
					</cms:pageSlot>
				</target:megaMenuCache>
				<feature:enabled name="featureLocationServices">
					<li class="mm-Departments-utility only-for-small" >
						<div class="my-account" data-location-service="${true}">
							<header:myAccountIcons smallScreen="${smallScreen}" />
							<c:if test="${anonymousCachable}">
								<script>{if(window.t_Inject){t_Inject('account');}}</script>
							</c:if>
						</div>
					</li>
				</feature:enabled>

				<li class="mm-Departments-utility only-for-small">
					<cms:pageSlot var="component" position="UtilityMobile">
						<cms:component component="${component}"/>
					</cms:pageSlot>
				</li>
			</ul>
		</header>
	</div>
</div>
