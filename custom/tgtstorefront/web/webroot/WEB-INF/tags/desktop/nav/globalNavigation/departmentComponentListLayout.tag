<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="iconAfter" required="false" type="java.lang.String" %>

<c:forEach items="${departments}" var="department" varStatus="departmentStatus">
	<li>
		<a href="${department.link}" title="${department.title}">
			<span><c:out value="${department.title}" /></span>
			<c:if test="${not empty iconAfter}">
				<spring:theme code="${iconAfter}" />
			</c:if>
		</a>
	</li>
</c:forEach>
