<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ attribute name="tense" required="false" type="java.lang.String" %>
<%@ attribute name="customerCode" required="false" type="java.lang.String" %>

<feature:enabled name="featureSellingGiftCards">

	<c:set var="absoluteOrderData" value="${not empty orderData ? orderData : cartData}" />
	<c:set var="customerCode" value="${not empty customerCode ? customerCode : 'checkout'}" />

	<c:if test="${empty tense}">
		<c:set var="tense" value="current" />
	</c:if>

	<c:if test="${absoluteOrderData.hasDigitalProducts}">
		<common:summaryPanel headingCode="order.summarycard.${customerCode}.heading.giftCard">
			<p><spring:theme code="order.summarycard.checkout.description.giftCard.${tense}" /></p>
		</common:summaryPanel>
	</c:if>

</feature:enabled>
