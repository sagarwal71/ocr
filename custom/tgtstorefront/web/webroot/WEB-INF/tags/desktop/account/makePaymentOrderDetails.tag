<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<c:set var="oddClass" value="odd" />
<c:set var="evenClass" value="even" />
<c:set var="rowClass" value="${oddClass}" />

<h2 class="co-heading shopping-heading"><spring:theme code="account.page.orderSummary"/></h2>
<div class="summary-table-container">
	<table class="summary-table" data-updating-text="${summaryUpdatingText}" data-error-text="${summaryErrorText}">
		<thead>
			<tr>
				<th id="entry-item" class="item"><spring:theme code="account.page.entry.item"/></th>
				<th id="entry-quantity" class="quantity"><spring:theme code="account.page.entry.quantity"/></th>
				<th id="entry-price" class="price"><spring:theme code="account.page.entry.price"/></th>
			</tr>
		</thead>
		<tbody>

			<tr class="product-entry">
				<td headers="entry-item" class="item">
					<spring:theme code="account.page.entry.record.item"/>
				</td>
				<td headers="entry-quantity" class="quantity">
					<spring:theme code="account.page.entry.record.quantity"/>
				</td>
				<td headers="entry-price" class="price">
					<format:price priceData="${paymentAmount}" displayFreeForZero="false"/>
				</td>
			</tr>
			<%-- Sub Total --%>
			<tr class="summary-row row-subtotal ${rowClass}">
				<th colspan="2" id="st-subtotal" class="title">
					<spring:theme code="account.page.totals.subtotal"/>
				</th>
				<td headers="st-subtotal" class="price">
					<format:price priceData="${paymentAmount}" displayFreeForZero="false"/>
				</td>
			</tr>
			<c:set var="rowClass" value="${rowClass eq evenClass ? oddClass : evenClass}" />
			
			<%-- Due Today --%>
			<tr class="summary-row row-total paytoday">
				<th colspan="2" id="st-total" class="title">
					<spring:theme code="account.page.totals.paymentToday"/>
				</th>
				<td headers="st-total" class="price">
					<format:price priceData="${paymentAmount}" displayFreeForZero="false"/>
				</td>
			</tr>
		</tbody>
	</table>

</div>