<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true" %>
<%@ attribute name="breadcrumbs" required="true" type="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header" %>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/desktop/common/footer" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<%-- to be replaced to tgt:tag --%>
<cms:pageSlot var="component" position="Supplement">
	<c:set var="hasVisibleComponentsSupplement" value="${true}" />
</cms:pageSlot>

<template:page pageTitle="${pageTitle}" disableFooterEnews="${true}">
	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<common:globalMessages/>
	<div class="inner-content has-aside">
		<div class="main">
			
			<jsp:doBody />

			<c:if test="${hasVisibleComponentsSupplement}">
				<div class="supplement contained">
					<c:set var="contentSlotContext" value="wide" scope="request" />
					<cms:pageSlot var="supplementComponent" position="Supplement">
						<cms:component component="${supplementComponent}"/>
					</cms:pageSlot>
					<c:remove var="contentSlotContext" scope="request" />
				</div>
			</c:if>

		</div>
		
		<div class="aside">
			<nav:accountNav/>

			<div class="promos hide-for-small">
				<c:set var="contentSlotContext" value="narrow" scope="request" />
				<cms:pageSlot var="leftColumnComponent" position="Aside">
					<cms:component component="${leftColumnComponent}"/>
				</cms:pageSlot>
				<c:remove var="contentSlotContext" scope="request" />
			</div>

		</div>
		
	</div>
</template:page>
