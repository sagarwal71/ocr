<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="posProduct" required="true" type="au.com.target.tgtfacades.product.data.TargetPOSProductData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="kiosk" tagdir="/WEB-INF/tags/desktop/kiosk" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>


<div class="pricechk-basic prod-basic" data-product-code="${posProduct.itemCode}">
	<h1 class="title">${posProduct.description}</h1>
</div>

<div class="pricechk-ratings bv-summary">
	<product:productRatings product="${product}" />
</div>

<div class="pricechk-variants prod-variants">
	<div class="pricechk-price prod-price">
		<kiosk:productCheckPricePoint price="${posProduct.price}" wasPrice="${posProduct.wasPrice}" />
	</div>	
</div>

<div class="pricechk-code prod-code">
	<spring:theme code="product.itemCode.label" text="{0}" arguments="${posProduct.itemCode}" />
</div>
