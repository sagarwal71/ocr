<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ attribute name="pageData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.FacetSearchPageData" %>

<c:if test="${not empty bulkyBoardStoreNumber}">
	<c:set var="showAll" value="${true}" />
	<c:if test="${bulkyBoardTopLevel}">
		<c:forEach items="${pageData.facets}" var="facetData" varStatus="facetStatus">
			<c:if test="${facetData.category and fn:length(facetData.values) gt 1}">
				<c:remove var="showAll" />
				<ul class="bulky-board-links">
					<c:forEach items="${facetData.values}" var="facetValue" varStatus="facetStatus">
						<li>
							<c:url value="${facetValue.query.url}" var="facetValueQueryUrl">
								<c:param name="viewAs" value="${urlElements.viewAs}" />
								<c:param name="sort" value="${urlElements.sortCode}" />
								<c:param name="itemsPerPage" value="${urlElements.itemsPerPage}" />
							</c:url>
							<a href="${facetValueQueryUrl}" title="${facetValue.name}" class="button-fwd-alt" rel="nofollow"><c:out value="${target:abbreviateString(facetValue.name, 32)}" /></a>
						</li>
					</c:forEach>
				</ul>
			</c:if>
		</c:forEach>
	</c:if>
	<c:if test="${showAll}">
		<ul class="bulky-board-links"><li><a href="${targetUrlsMapping.bulkyBoard}${bulkyBoardStoreNumber}" class="button-fwd-alt" rel="nofollow">Show All</a></li></ul>
	</c:if>
</c:if>