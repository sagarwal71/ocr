<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="price" required="true" type="de.hybris.platform.commercefacades.product.data.PriceData" %>
<%@ attribute name="wasPrice" required="false" type="de.hybris.platform.commercefacades.product.data.PriceData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<c:set var="classname" value="price-regular" />
<c:if test="${not empty wasPrice}">
	<c:set var="classname" value="price-reduced" />
</c:if>

<span class="price ${classname}"><format:priceRange price="${price}" /></span>
<c:if test="${not empty wasPrice}">
	<span class="was-price"><format:wasPriceRange price="${wasPrice}" /></span>
</c:if>