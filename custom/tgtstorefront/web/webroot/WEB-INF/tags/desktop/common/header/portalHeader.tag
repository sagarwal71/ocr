<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header"  %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="portalTitle" required="false" type="java.lang.String" %> 

<div class="header header-portal">
	<div class="header-inner">
		<div class="site-logo site-logo-portal">
			<header:sitelogo print="true" />
		</div>
		<c:if test="${not empty portalTitle}">
			<div class="portal-title">
				<h2><spring:theme code="${portalTitle}" /></h2>
			</div>
		</c:if>
	</div>
</div>