<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>


<feature:enabled name="uiTrackDisplayOnly">
	<c:set var="displayOnly" value="{{displayOnly}}" />
</feature:enabled>

<c:set var="dataEcProduct">
	data-ec-product='<template:productEcommAttributesJson
		id="{{id}}"
		name="{{escapeQuotes name}}"
		list="{{list}}"
		category="{{{topLevelCategory}}}"
		brand="{{{brand}}}"
		assorted="{{assorted}}"
		displayOnly="${displayOnly}"
		position="{{add position 1}}"
	/>'
</c:set>
<c:set var="emptyImg"><asset:resource code="img.missingProductImage.grid" /></c:set>
<c:set var="emptypixel"><asset:resource code="img.emptypixel" /></c:set>
<c:set var="primaryImage" value="{{#compare primaryImageUrl.length 'ne' 0}}{{primaryImageUrl}}{{else}}${emptyImg}{{/compare}}" />

<script type="text/x-handlebars-template" id="ProductCarouselItem">
	<comp:carouselItem parentClasses="ga-ec-impression" listAttributes="data-product-code='{{id}}' ${dataEcProduct}" url="{{url}}" linkClass="ga-ec-click">
		<jsp:attribute name="details">
			<a href="{{url}}" class="ga-ec-click" title="{{name}}">
				<h3 class="title">{{{abbreviate name 34}}}</h3>
				<p class="price-wrap hide-for-small">
					<span class="price {{#if isPriceRange}}price-range{{/if}} {{#if showWasPrice}}price-reduced{{else}}price-regular{{/if}}">
						{{formattedPrice}}
					</span>
					{{#if showWasPrice}}
						<span class="was-price"><spring:theme code="product.price.was" arguments="{{formattedWasPrice}}" /></span>
					{{/if}}
				</p>
			</a>
		</jsp:attribute>
		<jsp:attribute name="image">
			{{#compare position 'gte' 6}}
				<img src="${emptypixel}" data-src="${primaryImage}" alt="{{name}}" title="{{name}}" class="defer-img unveil-img" />
			{{else}}
				<img src="${primaryImage}" alt="{{name}}" title="{{name}}" />
			{{/compare}}
		</jsp:attribute>
	</comp:carouselItem>
</script>
