<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript">
(function(w, d) {
	if (w.location != w.parent.location) {
		var links = d.getElementsByTagName('a');
		for (var i = 0; i < links.length; i++) {
			links[i].target = '_top';
		}
	}
	var search = document.getElementById('search-header');
	if( search ) {
		search.target = "_top";
	}
}(window, document));
</script>
