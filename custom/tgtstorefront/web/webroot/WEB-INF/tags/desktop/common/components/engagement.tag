<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="marketing" tagdir="/WEB-INF/tags/desktop/marketing" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>

<%@ attribute name="list" required="true" type="java.lang.Object" %>
<%@ attribute name="large" required="false" type="java.lang.Boolean" %>
<%@ attribute name="secondary" required="false" type="java.lang.Boolean" %>
<%@ attribute name="impressionPosition" required="false" type="java.lang.Integer" %>
<%@ attribute name="cmsComponentId" required="false" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<feature:enabled name="uiTrackDisplayOnly">
	<c:set var="trackDisplayOnly" value="${true}" />
</feature:enabled>

<c:if test="${not empty list and fn:length(list) gt 0}">

	<c:if test="${large}">
		<c:set var="cssClass">${cssClass} large</c:set>
	</c:if>
	<c:if test="${secondary}">
		<c:set var="cssClass">${cssClass} secondary</c:set>
	</c:if>

	<c:set var="impressionPosition" value="${not empty impressionPosition ? impressionPosition : 1}" />
	<c:forEach items="${list}" var="item" varStatus="status">

		<c:if test="${not large}">
			<c:set var="cssClass">${cssClass} eng-small</c:set>
			<c:if test="${status.count le '2'}">
				<c:set var="cssClass">${cssClass} eng-complement</c:set>
			</c:if>
		</c:if>

		<c:remove var="dataEcProduct" />
		<c:remove var="firstVariant" />
		<c:if test="${not empty item.productData.url}">
			<c:if test="${fn:length(item.productData.targetVariantProductListerData) > 0 and not empty item.productData.targetVariantProductListerData[0]}">
				<c:set var="firstVariant" value="${item.productData.targetVariantProductListerData[0]}" />
			</c:if>
			<c:if test="${trackDisplayOnly}">
				<c:set var="displayOnly" value="${not empty firstVariant ? firstVariant.displayOnly : product.displayOnly}" />
			</c:if>
			<c:set var="productEcommJson"><template:productEcommJson
				product="${item.productData}"
				assorted="${not empty firstVariant ? firstVariant.assorted : product.assorted}"
				displayOnly="${displayOnly}"
				list="Engagement"
				position="${impressionPosition}" /></c:set>
			<c:set var="dataProductCode">data-product-code="${not empty firstVariant ? firstVariant.colourVariantCode : product.code}" </c:set>
			<c:set var="dataEcProduct">data-ec-product='${productEcommJson}' </c:set>
			<c:set var="impressionPosition" value="${impressionPosition + 1}" />
			<c:set var="cssClass">${cssClass} ga-ec-impression</c:set>
		</c:if>

		<c:if test="${not empty item.banner or not empty item.productData.url}">
			<div class="eng-tile ${cssClass}" ${dataProductCode} ${dataEcProduct}>
				<div class="eng-square">
					<c:choose>
						<c:when test="${not empty item.banner}">
							<cms:component component="${item.banner}"/>
						</c:when>
						<c:when test="${not empty item.productData.url}">
							<a href="${item.productData.url}" class="ga-ec-click">
								<c:set var="primaryImageUrl"><product:productListerImage product="${item.productData}" format="large" index="0" unavailable="true" /></c:set>
								<img src="${primaryImageUrl}" alt="${product.name}" title="${product.name}" />
							</a>
							<c:set value="${target:abbreviateString(item.productData.name, 34)}" var="productNameAbbreviated" />
							<a href="${item.productData.url}" class="eng-prod-info only-for-small ga-ec-click">${productNameAbbreviated}</a>
							<c:if test="${not item.productData.giftCard}">
								<a href="${item.productData.url}" class="quick-add-text ga-ec-quick-click hide-for-small" data-rel="${cmsComponentId}"><spring:theme code="text.quickViewWrap" /></a>
							</c:if>
							<product:favourites product="${item.productData}"/>
						</c:when>
					</c:choose>
				</div>
			</div>
		</c:if>

		<c:remove var="cssClass" />

	</c:forEach>
</c:if>
