<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>
<%@ attribute name="cmsEnewsQuick" required="false" fragment="true" %>
<%@ attribute name="afterForm" required="false" fragment="true" %>
<%@ attribute name="source" required="false" type="java.lang.String" %>
<%@ attribute name="customerSubscriptionType" required="false" type="java.lang.String" %>
<%@ attribute name="customerSubscriptionHeading" required="false" type="java.lang.String" %>
<%@ attribute name="customerSubscriptionUrl" required="true" type="java.lang.String" %>
<%@ attribute name="customerSubscriptionUpdateUrl" required="false" type="java.lang.String" %>

<c:choose>
	<c:when test="${not empty customerSubscriptionType and customerSubscriptionType eq 'mumshub'}">
		<c:set var= "subType" value="mumshub"/>
		<c:set var="quickHeading">${customerSubscriptionHeading}</c:set>
	</c:when>
	<c:otherwise>
		<c:set var= "subType" value="newsletter"/>
		<c:set var="quickHeading"><spring:theme code="enews.quick.heading" /></c:set>	
	</c:otherwise>
</c:choose>

<spring:theme code="enews.quick.input.placeholder" var="placeholder" />

<div class="quick-form enews-quick hfma" data-enews-src="${source}" data-subscription-source="${subType}" data-subscription-url="${customerSubscriptionUrl}" data-subscription-update-url="${customerSubscriptionUpdateUrl}">
	
	<c:if test="${not empty quickHeading}">
		<h3 class="quick-heading">${quickHeading}</h3>
	</c:if>
	
	<jsp:invoke fragment="cmsEnewsQuick"/>
	<form>
		<div class="f-element">
			<label for="enews-quick-email" class="visuallyhidden">${placeholder}</label>
			<c:choose>
				<c:when test="${subType eq 'mumshub' }">
					<div class="subscription-input-submit">
						<input class="text enews-quick-email" id="enews-quick-email" name="email" type="text" placeholder="${placeholder}" />
						<button type="button" class="btn-inline button-fwd button-submit wide-for-tiny wide-for-small"><spring:theme code='enews.Submit'/><spring:theme code="icon.right-arrow-small" /></button>                                               
					</div>
				</c:when>
				<c:otherwise>
					<input class="text enews-quick-email" id="enews-quick-email" name="email" type="text" placeholder="${placeholder}" />
				</c:otherwise>
			</c:choose>
			<div class="signup-controls">
				<user:inlinePrivacy inlinePrivacySource="${customerSubscriptionType}"/>
				<c:if test="${subType eq 'newsletter' }">
					<button type="button" class="button-fwd button-submit wide-for-tiny"><spring:theme code='enews.Submit'/><spring:theme code="icon.right-arrow-small" /></button>
				</c:if>	
			</div>
			<jsp:invoke fragment="afterForm" />
		</div>
	</form>
	<script type="text/x-handlebars-template" id="xhr-error-feeback">
		<feedback:message type="error" size="small">
			<p><spring:theme code="xhr.error.occurred" /></p>
		</feedback:message>
	</script>
</div>
