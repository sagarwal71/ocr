<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<c:set var="buttons">
	<button class="SmartAppBanner-button SmartAppBanner-useApp button-vibrant button-small-text">Use the app</button>
	<button class="SmartAppBanner-button SmartAppBanner-dismiss button-norm-border button-small-text">No thanks</button>
</c:set>

<cms:pageSlot var="component" position="SmartAppBannerApple">
	<c:set var="hasSmartAppBannerApple" value="${true}" />
</cms:pageSlot>

<c:if test="${hasSmartAppBannerApple}">
	<div class="SmartAppBanner"
		data-type="apple"
		data-app-id="${appleSmartAppBannerAppId}"
		data-scheme="${appleSmartAppBannerAppScheme}"
		data-app-store-url="${appleSmartAppBannerAppStoreUrl}">
		<div class="SmartAppBanner-inner">
			<cms:pageSlot var="component" position="SmartAppBannerApple">
				<cms:component component="${component}"/>
			</cms:pageSlot>
			${buttons}
		</div>
	</div>
</c:if>

<cms:pageSlot var="component" position="SmartAppBannerAndroid">
	<c:set var="hasSmartAppBannerAndroid" value="${true}" />
</cms:pageSlot>

<c:if test="${hasSmartAppBannerAndroid}">
	<div class="SmartAppBanner"
		data-type="android"
		data-app-id="${androidSmartAppBannerAppId}"
		data-scheme="${androidSmartAppBannerAppScheme}"
		data-app-store-url="${androidSmartAppBannerAppStoreUrl}">
		<div class="SmartAppBanner-inner">
			<cms:pageSlot var="component" position="SmartAppBannerAndroid">
				<cms:component component="${component}"/>
			</cms:pageSlot>
			${buttons}
		</div>
	</div>
</c:if>
