<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<cms:pageSlot var="component" position="SitewideNotification">
	<c:set var="hasVisibleComponentsSitewideNotification" value="${true}" />
</cms:pageSlot>

<c:if test="${hasVisibleComponentsSitewideNotification}">
	<div class="sitewide-notification hfma">
		<div class="notification-content">
			<button type="button" class="close-notification">
				<spring:theme code="icon.thin-cross" />
			</button>
			<cms:pageSlot var="component" position="SitewideNotification">
				<cms:component component="${component}"/>
			</cms:pageSlot>
		</div>
	</div>
</c:if>