<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<c:set var="customerFeedback">
	<cms:pageSlot var="component" position="CustomerFeedback">
		<cms:component component="${component}" />
	</cms:pageSlot>
</c:set>

<c:if test="${not empty customerFeedback}">
	${customerFeedback}
</c:if>
