<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="formId" required="false" type="java.lang.String" %>
<%@ attribute name="placeholderText" required="false" type="java.lang.String" %>
<%@ attribute name="className" required="false" type="java.lang.String" %>

<%-- TODO: Add this back in conditionally based on ${homepage} -> Verifying in CMS first.
<c:url var="searchUrl" value="/search"/>
<div itemscope itemtype="http://schema.org/WebSite">
	<meta itemprop="url" content="${fullyQualifiedDomainName}" />
	<form name="search_form" id="search-header" method="get" autocapitalize="off" autocorrect="off" action="${searchUrl}" itemprop="potentialAction" itemscope itemtype="http://schema.org/SearchAction">
		<meta itemprop="target" content="${fullyQualifiedDomainName}${searchUrl}?text={text}"/>
		<spring:theme code="text.search" var="searchText"/>
		<label class="visuallyhidden" for="search">${searchText}</label>
		<spring:theme code="search.placeholder" var="searchPlaceholder"/>
		<input class="site-search text" type="text" name="text" placeholder="${searchPlaceholder}" autocomplete="off" itemprop="query-input" required />
		<spring:theme code="text.searchButton" var="searchButtonText"/>
		<button class="button" type="submit"><span class="visuallyhidden">${searchButtonText}</span></button>
	</form>
</div>
--%>


<c:url var="searchUrl" value="/search"/>
<div>
	<c:set var="fid" value="${not empty formId ? formId : 'search-header'}" />
	<form name="search_form" id="${fid}" method="get" autocapitalize="off" autocorrect="off" action="${searchUrl}">
		<spring:theme code="text.search" var="searchText"/>
		<label class="visuallyhidden" for="text">${searchText}</label>
		<spring:theme code="search.placeholder" var="searchPlaceholder"/>
		<input class="${className} site-search text" type="text" name="text" value="${freeTextSearch}" placeholder="${not empty placeholderText ? placeholderText : searchPlaceholder}" autocomplete="off" required />
		<spring:theme code="text.searchButton" var="searchButtonText"/>
		<button class="button-clear" type="button">
			<span class="visuallyhidden"><spring:theme code="text.searchButton.clear" /></span>
		</button>
		<button class="button" type="submit">
			<svg width="22px" height="22px" viewBox="0 0 23 23" class="search-icon" focusable="false">
				<use xlink:href="#MagnifyingGlassIcon"></use>
			</svg>
			<span class="visuallyhidden">${searchButtonText}</span>
		</button>
	</form>
</div>
