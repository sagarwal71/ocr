<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<cms:pageSlot var="component" position="Addendum">
	<c:set var="hasVisibleComponentsAddendum" value="${true}" />
</cms:pageSlot>
<c:if test="${hasVisibleComponentsAddendum}">
	<c:set var="contentSlotContext" value="full" scope="request" />
	<div class="addendum">
		<cms:pageSlot var="component" position="Addendum">
			<cms:component component="${component}"/>
		</cms:pageSlot>
	</div>
	<c:remove var="contentSlotContext" scope="request" />
</c:if>
