<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<noscript>
	<div class="js-alert alert-bar">
		<div class="alert-bar-inner">
			<p><spring:theme code="alertbar.nojs" /></p>
		</div>
	</div>
</noscript>

<div class="cookies-alert alert-bar">
	<div class="alert-bar-inner">
		<p><spring:theme code="alertbar.nocookies" /></p>
	</div>
</div>

<div class="browser-alert alert-bar">
	<div class="alert-bar-inner">
		<p><spring:theme code="alertbar.browser" /></p>
	</div>
</div>