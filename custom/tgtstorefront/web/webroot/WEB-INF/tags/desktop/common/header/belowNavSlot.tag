<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<cms:pageSlot var="component" position="BelowNavigationBar">
	<c:set var="hasVisibleComponentsBelowNavigation" value="${true}" />
</cms:pageSlot>
<c:if test="${hasVisibleComponentsBelowNavigation}">
	<div class="below-nav-bar hide-for-small">
		<div class="below-nav-outer">
			<div class="below-nav-inner">
				<cms:pageSlot var="component" position="BelowNavigationBar">
					<cms:component component="${component}"/>
				</cms:pageSlot>
			</div>
		</div>
	</div>
</c:if>
