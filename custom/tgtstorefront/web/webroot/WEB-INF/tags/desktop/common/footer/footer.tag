<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>

<%-- to be replaced to tgt:tag --%>
<cms:pageSlot var="component" position="FooterSupportContainer">
	<c:set var="hasVisibleComponentsFooterSupport" value="${true}" />
</cms:pageSlot>
<cms:pageSlot var="component" position="FooterSecurityContainer">
	<c:set var="hasVisibleComponentsFooterSecurity" value="${true}" />
</cms:pageSlot>
<cms:pageSlot var="component" position="FooterUtilityLeftContainer">
    <c:set var="hasVisibleComponentsFooterUtilityLeft" value="${true}" />
</cms:pageSlot>
<cms:pageSlot var="component" position="FooterUtilityRightContainer">
    <c:set var="hasVisibleComponentsFooterUtilityRight" value="${true}" />
</cms:pageSlot>
<cms:pageSlot var="component" position="FooterLegalContainer">
	<c:set var="hasVisibleComponentsFooterLegal" value="${true}" />
</cms:pageSlot>
<cms:pageSlot var="component" position="FooterSocialContainer">
	<c:set var="hasVisibleComponentsFooterSocial" value="${true}" />
</cms:pageSlot>
<cms:pageSlot var="component" position="FooterLinksColumn1">
	<c:set var="hasVisibleComponentsFooterLinksColumn1" value="${true}" />
</cms:pageSlot>
<cms:pageSlot var="component" position="FooterLinksColumn2">
	<c:set var="hasVisibleComponentsFooterLinksColumn2" value="${true}" />
</cms:pageSlot>
<cms:pageSlot var="component" position="FooterLinksColumn3">
	<c:set var="hasVisibleComponentsFooterLinksColumn3" value="${true}" />
</cms:pageSlot>
<c:set var="hasVisibleComponentsFooterLinks" value="${hasVisibleComponentsFooterLinksColumn1 or hasVisibleComponentsFooterLinksColumn2 or hasVisibleComponentsFooterLinksColumn3 or hasVisibleComponentsFooterLegal}" />

<footer>
	<div class="footer">
		<div class="inner-footer">
			<div class="footer-wrap">

				<div class="company-info">
					<c:if test="${hasVisibleComponentsFooterSupport}">
						<div class="contact-info">
							<cms:pageSlot var="component" position="FooterSupportContainer">
								<cms:component component="${component}"/>
							</cms:pageSlot>
						</div>
					</c:if>


				</div>

				<c:if test="${hasVisibleComponentsFooterLinks}">
					<div class="footer-nav-group">
						<c:if test="${hasVisibleComponentsFooterLinksColumn1}">
							<div class="footer-nav content-nav">
								<c:set var="hideSubNavigationNodes" scope="request" value="${true}" />
									<cms:pageSlot var="component" position="FooterLinksColumn1">
										<cms:component component="${component}"/>
									</cms:pageSlot>
								<c:remove var="hideSubNavigationNodes" />
							</div>
						</c:if>

						<c:if test="${hasVisibleComponentsFooterLinksColumn2}">
							<div class="footer-nav content-nav">
								<c:set var="hideSubNavigationNodes" scope="request" value="${true}" />
									<cms:pageSlot var="component" position="FooterLinksColumn2">
										<cms:component component="${component}"/>
									</cms:pageSlot>
								<c:remove var="hideSubNavigationNodes" />
							</div>
						</c:if>

						<c:if test="${hasVisibleComponentsFooterLinksColumn3}">
							<div class="footer-nav content-nav">
								<c:set var="hideSubNavigationNodes" scope="request" value="${true}" />
									<cms:pageSlot var="component" position="FooterLinksColumn3">
										<cms:component component="${component}"/>
									</cms:pageSlot>
								<c:remove var="hideSubNavigationNodes" />
							</div>
						</c:if>

						<c:if test="${hasVisibleComponentsFooterSocial}">
							<div class="footer-social hfk">
								<cms:pageSlot var="component" position="FooterSocialContainer">
									<cms:component component="${component}"/>
								</cms:pageSlot>
							</div>
						</c:if>
					</div>
				</c:if>

				<c:if test="${hasVisibleComponentsFooterUtilityLeft}">
					<div class="footer-utility-left" ${ycommerce:getTestIdAttribute('footer-utility', pageContext)}>
						<cms:pageSlot var="component" position="FooterUtilityLeftContainer">
							<cms:component component="${component}"/>
					    </cms:pageSlot>
					</div>
				</c:if>

				<c:if test="${hasVisibleComponentsFooterUtilityRight}">
					<div class="footer-utility-right" ${ycommerce:getTestIdAttribute('footer-utility', pageContext)}>
						<cms:pageSlot var="component" position="FooterUtilityRightContainer">
							<cms:component component="${component}"/>
						</cms:pageSlot>
					</div>
				</c:if>

				<c:if test="${hasVisibleComponentsFooterLegal}">
					<div class="footer-legal" ${ycommerce:getTestIdAttribute('footer-legal', pageContext)}>
						<cms:pageSlot var="component" position="FooterLegalContainer">
							<cms:component component="${component}"/>
						</cms:pageSlot>
					</div>
				</c:if>

			</div>
		</div>
	</div>
</footer>
