<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="assets" tagdir="/WEB-INF/tags/desktop/assets" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<feature:enabled name="uiStickyHeader">
	<div class="TargetHeader-child is-favourites">
		<a href="/favourites" class="TargetHeader-link TargetHeader-link--favourites">
			<span class="favourite-count TargetHeader-favCount"></span>
			<assets:svgFavHeart heartType="fatHeart" />
		</a>
	</div>
</feature:enabled>
<feature:disabled name="uiStickyHeader">
	<div class="FavHeader">
		<a href="/favourites" class="FavHeader-link">
			<span class="FavCount favourite-count"></span>
			<span class="visuallyhidden"><spring:theme code="favourites.view" /></span>
			<assets:svgFavHeart heartType="header" useClass="FavHeader-heart" />
		</a>
	</div>
</feature:disabled>
