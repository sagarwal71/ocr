<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header"  %>
<%@ attribute name="disabledCart" required="false" type="java.lang.Boolean" %>
<%@ attribute name="smallScreen" required="false" %>

<ul class="Utility only-for-small">
	<cms:pageSlot var="menuItem" position="UtilityMenu">
		<li class="Utility-item">
			<cms:component component="${menuItem}"/>
		</li>
	</cms:pageSlot>

	<li class="Utility-item">
		<c:url var="myAccountUrl" value="${fullyQualifiedDomainName}/my-account"/>
		<a href="${myAccountUrl}"><spring:theme code="header.link.account"/></a>
	</li>

	<cms:pageSlot var="menuItem" position="MyAccountMenu">
		<li class="Utility-item">
			<cms:component component="${menuItem}"/>
		</li>
	</cms:pageSlot>
</ul>
