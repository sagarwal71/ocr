<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/desktop/util" %>
<%@ attribute name="smallScreen" required="false" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>


<c:if test="${not smallScreen and not anonymousCachable}">
	<feature:enabled name="featureLocationServices">
		<c:set var="className" value="has-location"/>
		<c:set var="locationMenu">
			<header:flyoutItem divider="${true}" className="shopping-location">
				<util:loading inline="${true}" />
			</header:flyoutItem>
		</c:set>
	</feature:enabled>
	<sec:authorize access="hasRole('ROLE_CUSTOMERGROUP')">
		<c:url var="myAccountUrl" value="${fullyQualifiedDomainName}/my-account"/>
		<c:set var="iconUrl" value="${myAccountUrl}" />
		<c:set var="iconClass" value="is-registered" />
		<spring:theme code="header.link.account" var="iconContent" />
		<c:set var="menu">
			<header:flyoutItem className="FlyOut-item--account">
				<i class="Icon Icon--accountWhite Icon--size125"></i>
				<span class="FlyOut-iconText FlyOut-account">
					<a href="${myAccountUrl}" class="TargetHeader-link TargetHeader-link--account">
						<span class="TargetHeader-name"><%--
						--%><spring:theme code="header.welcome" arguments="${user.firstName}" htmlEscape="true"/><%--
					--%></span><%--
					--%>&nbsp;<spring:theme code="header.link.account" /><%--
				--%></a>
				</span>
			</header:flyoutItem>
			<c:out value="${locationMenu}" escapeXml="${false}" />
			<header:flyoutItem divider="${true}">
				<form:form action="${targetUrlsMapping.logout}" method="post">
					<button type="submit" class="TargetHeader-link TargetHeader-link--logout">
						<spring:theme code="header.link.logout" />
					</button>
					<target:csrfInputToken/>
				</form:form>
			</header:flyoutItem>
		</c:set>
	</sec:authorize>
	<sec:authorize access="!hasRole('ROLE_CUSTOMERGROUP')">
		<c:url var="loginUrl" value="${fullyQualifiedDomainName}/login" />
		<c:set var="iconUrl" value="${loginUrl}" />
		<c:set var="iconClass" value="" />
		<spring:theme var="iconContent" code="header.link.login" />
		<c:set var="menu">
			<header:flyoutItem className="FlyOut-item--account">
				<i class="Icon Icon--accountWhite Icon--size125"></i>
				<span class="FlyOut-iconText FlyOut-account">
					<a href="${loginUrl}" class="TargetHeader-link TargetHeader-link--account">
						<spring:theme code="header.link.createaccount.or.signin" />
					</a>
				</span>
			</header:flyoutItem>
			<c:out value="${locationMenu}" escapeXml="${false}" />
		</c:set>
	</sec:authorize>
	<a href="${iconUrl}" class="TargetHeader-icon TargetHeader-icon--account ${iconClass}">
		<span class="visuallyhidden">${iconContent}</span>
	</a>
	<table class="FlyOut FlyOut--table ${className}">
		<c:out value="${menu}" escapeXml="${false}" />
	</table>
</c:if>

<c:if test="${smallScreen}">
	<c:url var="myAccountUrl" value="${fullyQualifiedDomainName}/my-account"/>
	<a href="${myAccountUrl}"><spring:theme code="header.link.account"/></a>
</c:if>
