<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<%-- to be replaced to tgt:tag --%>
<cms:pageSlot var="component" position="FooterSupportContainer">
	<c:set var="hasVisibleComponentsFooterSupport" value="${true}" />
</cms:pageSlot>

<cms:pageSlot var="component" position="FooterUtilityLeftContainer">
    <c:set var="hasVisibleComponentsFooterUtilityLeft" value="${true}" />
</cms:pageSlot>

<cms:pageSlot var="component" position="FooterUtilityRightContainer">
    <c:set var="hasVisibleComponentsFooterUtilityRight" value="${true}" />
</cms:pageSlot>

<cms:pageSlot var="component" position="FooterLegalContainer">
	<c:set var="hasVisibleComponentsFooterLegal" value="${true}" />
</cms:pageSlot>

<cms:pageSlot var="component" position="FooterSecurityContainer">
	<c:set var="hasVisibleComponentsFooterSecurity" value="${true}" />
</cms:pageSlot>

<footer>
	<div class="footer">
		<div class="inner-footer">
			<div class="footer-wrap">
				<div class="co-footer">

					<c:if test="${hasVisibleComponentsFooterSupport}">
						<div class="company-info">
							<div class="contact-info">
								<cms:pageSlot var="component" position="FooterSupportContainer">
									<cms:component component="${component}"/>
								</cms:pageSlot>
							</div>
						</div>
					</c:if>

					<div class="site-links">
						<c:if test="${hasVisibleComponentsFooterUtilityLeft}">
							<div class="footer-utility-left">
								<cms:pageSlot var="component" position="FooterUtilityLeftContainer">
									<cms:component component="${component}"/>
							    </cms:pageSlot>
							</div>
						</c:if>
					</div>

					<div class="site-links">
						<c:if test="${hasVisibleComponentsFooterUtilityRight}">
							<div class="footer-utility-right">
								<cms:pageSlot var="component" position="FooterUtilityRightContainer">
									<cms:component component="${component}"/>
								</cms:pageSlot>
							</div>
						</c:if>
					</div>
					
					<div class="site-links">
						<c:if test="${hasVisibleComponentsFooterLegal}">
							<div class="footer-legal">
								<cms:pageSlot var="component" position="FooterLegalContainer">
									<cms:component component="${component}"/>
								</cms:pageSlot>
							</div>
						</c:if>
					</div>

				</div>
			</div>
		</div>
	</div>
</footer>
