<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="print" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>

<%-- to be replaced to tgt:tag --%>
<cms:pageSlot var="logo" position="SiteLogo">
	<c:set var="hasVisibleComponentsSiteLogo" value="${true}" />
</cms:pageSlot>
<c:choose>
	<c:when test="${hasVisibleComponentsSiteLogo}">
		<cms:pageSlot var="logo" position="SiteLogo">
			<cms:component component="${logo}"/>
		</cms:pageSlot>
	</c:when>
	<c:otherwise>
		<spring:theme code="text.headerLogo" var="headerLogo"/>
		<c:set var="linkOut"><a class="logo" href="${fullyQualifiedDomainName}/" title="${headerLogo}"><c:out value="${headerLogo}" /></a></c:set>
		<c:choose>
			<c:when test="${homepage}">
				<h1><c:out value="${linkOut}" escapeXml="false" /></h1>
			</c:when>
			<c:otherwise>
				<h2><c:out value="${linkOut}" escapeXml="false" /></h2>
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>

<c:if test="${print}">
	<div id="print-logo">
		<c:set var="headerPrintLogo"><asset:resource code="img.headerPrintLogo" /></c:set>
		<c:set var="headerPrintLogoSvg"><asset:resource code="img.headerPrintLogoSvg" /></c:set>
		<spring:theme code="text.headerLogo" var="headerLogo"/>
		<noscript>
			<img class="print-logo" src="${headerPrintLogo}" alt="${headerLogo}" />
		</noscript>
		<script>
				(function(d, m){
					if ( typeof( m ) == 'function' && m.jitPrint ){
						var c = 0;
						function print(p) {
							if( c ) { return; }
							var i = d.createElement('img');
							i.setAttribute('class', 'print-logo');
							i.src = (m && m.svg) ? '${headerPrintLogoSvg}' : '${headerPrintLogo}';
							d.getElementById('print-logo').appendChild(i);
							c = 1;
						}
						m.jitPrint(print);
					}
				}(document, this.Modernizr));
		</script>
	</div>
</c:if>
