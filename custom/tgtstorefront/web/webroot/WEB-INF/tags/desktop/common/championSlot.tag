<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ attribute name="slotName" required="true" %>

<cms:pageSlot var="component" position="slotName]">
	<c:set var="hasVisibleComponentsListerChampion" value="${true}" />
</cms:pageSlot>

<c:if test="${hasVisibleComponentsListerChampion}">
	<div class="champion">
		<c:set var="contentSlotContext" value="full" scope="request" />
		<cms:pageSlot var="listerChampionComponent" position="slotName]">
			<cms:component component="${listerChampionComponent}"/>
		</cms:pageSlot>
		<c:remove var="contentSlotContext" scope="request" />
	</div>
</c:if>