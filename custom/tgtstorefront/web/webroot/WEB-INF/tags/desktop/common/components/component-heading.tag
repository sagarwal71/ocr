<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<%@ attribute name="title" required="true" %>
<%@ attribute name="compLink" required="false" type="java.lang.Object" %>
<%@ attribute name="staticLink" required="false" type="java.lang.String" %>

<c:if test="${not empty compLink or not empty staticLink}">
	<c:set var="link">
		<c:choose>
			<c:when test="${not empty compLink}">
				<cms:component component="${compLink}"/>
			</c:when>
			<c:otherwise>
				${staticLink}
			</c:otherwise>
		</c:choose>
	</c:set>
</c:if>

<div class="comp-heading">
	<h3>${title}</h3>
	<c:if test="${not empty link}">
		<span class="comp-link">
			${link}
		</span>
	</c:if>
</div>
