<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<c:forEach items="${target:reverseArrayList(cartData.deliveryModes)}" var="deliveryMode">
	<c:if test="${not empty deliveryMode.incentiveMetMessage}">

		<c:choose>
			<c:when test="${empty incentiveMetMessage}">
				<c:set var="incentiveMetMessage">${deliveryMode.incentiveMetMessage}</c:set>
			</c:when>

			<c:when test="${not empty incentiveMetMessage and not incentiveMetMessageSplit}">
				<c:set var="incentiveMetMessage"><spring:theme code="basket.incentive.met.two" text="{0} and {1}" argumentSeparator=";;" arguments="${deliveryMode.incentiveMetMessage};;${incentiveMetMessage}" /></c:set>
				<c:set var="incentiveMetMessageSplit" value="${true}"/>
			</c:when>

			<c:when test="${not empty incentiveMetMessage}">
				<c:set var="incentiveMetMessage"><spring:theme code="basket.incentive.met.morethantwo" text="{0}, {1}" argumentSeparator=";;" arguments="${deliveryMode.incentiveMetMessage};;${incentiveMetMessage}" /></c:set>
			</c:when>
		</c:choose>

	</c:if>

	<c:if test="${not empty deliveryMode.potentialIncentiveMessage}">

		<c:choose>
			<c:when test="${empty potentialIncentiveMessage}">
				<c:set var="potentialIncentiveMessage">${deliveryMode.potentialIncentiveMessage}</c:set>
			</c:when>

			<c:when test="${not empty potentialIncentiveMessage and not potentialIncentiveMessageSplit}">
				<c:set var="potentialIncentiveMessage"><spring:theme code="basket.incentive.potential.two" text="{0} or {1}" argumentSeparator=";;" arguments="${deliveryMode.potentialIncentiveMessage};;${potentialIncentiveMessage}" /></c:set>
				<c:set var="potentialIncentiveMessageSplit" value="${true}"/>
			</c:when>

			<c:when test="${not empty potentialIncentiveMessage}">
				<c:set var="potentialIncentiveMessage"><spring:theme code="basket.incentive.potential.morethantwo" text="{0}, {1}" argumentSeparator=";;" arguments="${deliveryMode.potentialIncentiveMessage};;${potentialIncentiveMessage}" /></c:set>
			</c:when>
		</c:choose>

	</c:if>
</c:forEach>

<c:if test="${not empty incentiveMetMessage or not empty potentialIncentiveMessage}">
	<div class="del-incentives">
		<c:if test="${not empty incentiveMetMessage}">
			<c:set var="secondClassSuffix" value="-subtle" />
			<h2 class="del-incentive">
				<spring:theme code="basket.incentive.met.complete" text="Your order qualifies for {0}" argumentSeparator=";;" arguments="${incentiveMetMessage}" />
			</h2>
		</c:if>

		<c:if test="${not empty potentialIncentiveMessage}">
			<h2 class="del-incentive${secondClassSuffix}">
				<spring:theme code="basket.incentive.potential.complete" text="Spend {0}" argumentSeparator=";;" arguments="${potentialIncentiveMessage}" />
			</h2>
		</c:if>
	</div>
</c:if>
