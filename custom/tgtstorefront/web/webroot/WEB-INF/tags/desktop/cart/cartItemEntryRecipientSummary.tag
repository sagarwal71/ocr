<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="recipient" required="true" type="au.com.target.tgtfacades.order.data.GiftRecipientData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<p class="Recipient-summary">
	<strong><spring:theme code="basket.page.gift.recipient.label" arguments="${recipient.firstName};;${recipient.lastName}" argumentSeparator=";;" /></strong><br />
	<spring:theme code="basket.page.gift.email.label" arguments="${recipient.recipientEmailAddress}" argumentSeparator=";;" /><br />
	<c:if test="${not disabledMessage and not empty recipient.messageText}">
		<spring:theme code="basket.page.gift.message.label" arguments="${target:replaceAllNewLineWithBreak(recipient.messageText)}" argumentSeparator=";;" />
	</c:if>
</p>
