<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>

<div class="next-action">
	<a class="continue continue-large hfma" href="${targetUrlsMapping.cartContinueShopping}">
		<spring:theme code="icon.left-arrow-small" /><span><spring:theme code="cart.page.continue" /></span>
	</a>
	<c:choose>
		<c:when test="${not cartData.hasValidDeliveryModes or cartData.insufficientAmount}">
			<span class="button-fwd button-large forward disabled-button" >
				<spring:theme code="cart.page.checkout" /><spring:theme code="icon.right-arrow-small" />
			</span>
		</c:when>
		<c:otherwise>
			<c:if test="${kioskMode}">
				<spring:theme code="kiosk.cart.pinpad.error.url" var="pinpadErrorUrl" />
				<c:set var="errorModal">data-error-modal="${pinpadErrorUrl}"</c:set>
			</c:if>
			<a class="button-fwd button-large forward ${not kioskMode ? 'button-wait' : 'basket-proceed'}" href="${targetUrlsMapping.cartCheckout}" ${errorModal} >
				<spring:theme code="cart.page.checkout" /><spring:theme code="icon.right-arrow-small" />
			</a>
		</c:otherwise>
	</c:choose>
</div>
