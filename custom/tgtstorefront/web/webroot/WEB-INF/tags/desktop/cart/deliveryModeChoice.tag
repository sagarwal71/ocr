<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ attribute name="deliveryPostcode" required="false" type="java.lang.String" %>
<%@ attribute name="wrapDiv" required="false" type="java.lang.Boolean" %>

<c:set var="wrapDiv" value="${empty wrapDiv or wrapDiv}" />

<c:if test="${wrapDiv}">
	<c:set var="divDeliveryModeChoice"><div class="delivery-mode-choice">||</div></c:set>
	<c:set var="divDeliveryModeChoice" value="${fn:split(divDeliveryModeChoice, '||')}"/>
</c:if>

<c:if test="${not empty divDeliveryModeChoice}">
	<c:out value="${divDeliveryModeChoice[0]}" escapeXml="false" />
</c:if>

	<c:if test="${not isDeliveryModeSelectionByPostcodeEnabled or not empty deliveryPostcode}">

		<c:forEach items="${cartData.deliveryModes}"
						var="deliveryMode">
			<c:set var="delSelected" value="${deliveryMode.available and deliveryMode.code eq cartData.deliveryMode.code ? true : false}"/>
			<div class="order-delivery-mode del-mode ${deliveryMode.available ? ' avail ' : ' unavail '} ${delSelected ? ' del-mode-active' : ''}" data-mode-type="${deliveryMode.code}">
				<div class="${deliveryMode.available ? 'item-selectable' : ''}">
					<c:if test="${deliveryMode.available}">
						<input type="radio" name="delMode" value="${deliveryMode.code}" class="radio list-item-radio" ${delSelected ? ' checked="checked" ' : ''} />
					</c:if>
				<h5 class="label">${deliveryMode.name}</h5>
					<c:if test="${deliveryMode.available and not empty deliveryMode.deliveryCost }">
						<span class="price"><strong><format:price priceData="${deliveryMode.deliveryCost}" displayFreeForZero="true"/></strong></span>
					</c:if>
					<c:if test="${not empty deliveryMode.shortDescription}">
						<p>${deliveryMode.shortDescription}</p>
					</c:if>
					<c:if test="${not deliveryMode.available}">
						<c:choose>
							<c:when test="${deliveryMode.postCodeNotSupported}">
								<p class="del-alert"><spring:theme code="icon.exclamation" />&nbsp;<spring:theme code="checkout.multi.delivery.modes.location.disabled" arguments="${deliveryMode.name}"/></p>
							</c:when>
							<c:otherwise>
								<p class="del-alert"><spring:theme code="icon.exclamation" />&nbsp;<spring:theme code="checkout.multi.delivery.modes.disabled" /></p>
							</c:otherwise>
						</c:choose>
					</c:if>
				</div>
			</div>
		</c:forEach>
		<c:if test="${not cartData.hasValidDeliveryModes}">
			<p class="delivery-concerns">
				<spring:theme code="icon.exclamation" /><span class="delivery-concern"><spring:theme code="cart.page.invalid.deliverymode" /></span>
			</p>
		</c:if>
	</c:if>

<c:if test="${not empty divDeliveryModeChoice}">
	<c:out value="${divDeliveryModeChoice[1]}" escapeXml="false" />
</c:if>

