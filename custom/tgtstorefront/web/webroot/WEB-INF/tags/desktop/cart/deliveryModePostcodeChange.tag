<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ attribute name="deliveryPostcode" required="false" type="java.lang.String" %>


<div class="delivery-mode-postcode-change">
	<span class="postcode-success-message"><spring:theme code="deliveryMode.postcode.input.success.message" arguments="${deliveryPostcode}" argumentSeparator=";;;" /></span>
	<button type="button" class="button-change button-norm"><spring:theme code="deliveryMode.postcode.input.change"/></button>
</div>



