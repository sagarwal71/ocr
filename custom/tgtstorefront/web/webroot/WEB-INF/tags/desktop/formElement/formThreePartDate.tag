<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="idKey" required="true" type="java.lang.String" %>
<%@ attribute name="labelKey" required="true" type="java.lang.String" %>
<%@ attribute name="skipBlankMessageKey" required="true" type="java.lang.String" %>
<%@ attribute name="path" required="true" type="java.lang.String" %>
<%@ attribute name="mandatory" required="false" type="java.lang.Boolean" %>
<%@ attribute name="labelCSS" required="false" type="java.lang.String" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ attribute name="years" required="true" type="java.util.List" %>

<template:formElement path="${path}" >

	<label class="${labelCSS}" for="${idKey}">
		<spring:theme code="${labelKey}"/>
		<formElement:label mandatory="${mandatory}" />
	</label>

	<div class="f-set-group f-set-group-inline">
		<formElement:formSelectBox idKey="${idKey}.day" skipBlankMessageKey="${skipBlankMessageKey}.day" nested="true" path="${path}.day" items="${daysOfMonth}" dataSearchThreshold="9999" />
		<formElement:formSelectBox idKey="${idKey}.month" skipBlankMessageKey="${skipBlankMessageKey}.month" nested="true" path="${path}.month" elementCSS="${fHiddenError}" items="${months}" dataSearchThreshold="9999" />
		<formElement:formSelectBox idKey="${idKey}.year" skipBlankMessageKey="${skipBlankMessageKey}.year" nested="true" path="${path}.year" elementCSS="${fHiddenError}" items="${years}" dataSearchThreshold="9999" />
	</div>

</template:formElement>
