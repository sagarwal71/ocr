<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="idKey" required="true" type="java.lang.String" %>
<%@ attribute name="labelKey" required="false" type="java.lang.String" %>
<%@ attribute name="path" required="true" type="java.lang.String" %>
<%@ attribute name="mandatory" required="false" type="java.lang.Boolean" %>
<%@ attribute name="labelCSS" required="false" type="java.lang.String" %>
<%@ attribute name="inputCSS" required="false" type="java.lang.String" %>
<%@ attribute name="errorPath" required="false" type="java.lang.String" %>
<%@ attribute name="strength" required="false" type="java.lang.Boolean" %>
<%@ attribute name="placeholder" required="false" type="java.lang.String" %>
<%@ attribute name="showhide" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>

<template:formElement path="${path}" errorPath="${errorPath}">
	<c:if test="${not empty labelKey}">
		<label class="${labelCSS}" for="${idKey}">
			<spring:theme code="${labelKey}"/>
			<formElement:label mandatory="${mandatory}" />
		</label>
	</c:if>
	<c:if test="${strength}">
		<c:set var="strengthCss" value="password-strength" />
	</c:if>
	<c:if test="${showhide}">
		<c:set var="showhideCss" value=" ShowHide" />
		<c:set var="showhideInput" value=" ShowHide-input hasAction" />
	</c:if>
	<div class="f-set ${strengthCss} ${showhideCss}">
		<spring:theme code="${placeholder}" var="placeholder" text="" />
		<form:password cssClass="${inputCSS} ${showhideInput}" id="${idKey}" path="${path}" placeholder="${placeholder}"/>
		<c:if test="${showhide}">
			<spring:theme code="form.field.show" var="showText" />
			<spring:theme code="form.field.hide" var="hideText" />
			<button class="ShowHide-toggle" type="button" tabindex="-1" data-show-text="${showText}" data-hide-text="${hideText}">
				${showText}
			</button>
		</c:if>
		<span class="f-feedback"></span>
		<c:if test="${strength}">
			<div class="strength-indicator">
				<p class="pass-strength">Password strength: <span class="strength"></span></p>
				<div class="strength-bar">
					<div class="indicator"></div>
				</div>
			</div>
		</c:if>
	</div>
</template:formElement>

