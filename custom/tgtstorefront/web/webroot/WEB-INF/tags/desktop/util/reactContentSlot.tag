<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="slotName" required="true" type="java.lang.String" %>

<c:set var="content"><jsp:doBody /></c:set>
<c:if test="${fn:length(fn:trim(content)) gt 0 and not empty slotName}">
	<script type="text/content-slot" id="${slotName}">
		${fn:trim(content)}
	</script>
</c:if>
