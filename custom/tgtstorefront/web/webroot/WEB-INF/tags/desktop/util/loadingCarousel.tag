<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>

<c:set var="placeholderTitle">
	<div class="Slider-title">&nbsp;</div>
</c:set>

<comp:carousel type="product" carouselClassName="Slider--products Slider--loader" perPage="6" itemCount="12">
	<jsp:attribute name="itemList">
		<c:forEach begin="1" end="6">
			<comp:carouselItem linkClass="Slider-link">
				<jsp:attribute name="image">
					<div class="Slider-image"></div>
				</jsp:attribute>
			</comp:carouselItem>
		</c:forEach>
	</jsp:attribute>
	<jsp:attribute name="heading">
		<comp:component-heading title="${placeholderTitle}" />
	</jsp:attribute>
</comp:carousel>
