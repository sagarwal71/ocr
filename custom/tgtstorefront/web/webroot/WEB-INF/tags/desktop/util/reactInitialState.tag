<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ attribute name="productData" type="java.lang.String" %>
<%@ attribute name="listingData" type="java.lang.String" %>
<%@ attribute name="applicationData" type="java.lang.String" %>
<%@ attribute name="featureData" type="java.lang.String" %>
<%@ attribute name="entityData" type="java.lang.String" %>
<%@ attribute name="lookData" type="java.lang.String" %>

<c:if test="${not empty productData}">
	<c:set var="products">"products": { ${productData} }</c:set>
</c:if>
<c:if test="${not empty listingData}">
	<c:set var="products">"products": ${listingData}</c:set>
</c:if>
<c:if test="${not empty featureData}">
	<c:set var="features">"features": ${featureData}</c:set>
</c:if>
<c:if test="${not empty lookData}">
	<c:set var="looks">"lookData": ${lookData}</c:set>
</c:if>
<script type="react/state" class="__react_initial_state__">
	{
		"entities": {
			${products}
			${not empty products and not empty features ? ', ' : ''}${features}
			${(not empty products or not empty features) and not empty looks ? ', ' : ''}${looks}
			${(not empty products or not empty features or not empty looks) and not empty entityData ? ', ' : ''}${entityData}
		},
		"application": {
			${applicationData}
		}
	}
</script>
