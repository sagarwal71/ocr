<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="size" required="true" type="java.lang.String" %>
<%@ attribute name="noImage" required="false" type="java.lang.Boolean" %>
<%@ attribute name="truncate" required="true" type="java.lang.Integer" %>
<%@ attribute name="blogPage" required="true" type="au.com.target.tgtwebcore.model.cms2.pages.TargetBlogPageModel" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<div class="blog-item blog-item-${size}">
	<c:if test="${not noImage and not empty blogPage.blogImage}">
		<a class="blog-image" href="${blogPage.label}" title="${blogPage.title}"><img alt="${blogPage.title}" src="${blogPage.blogImage.url}"/></a>
	</c:if>
	<h2 class="blog-title">
		<a href="${blogPage.label}" title="${blogPage.title}">${blogPage.title}</a>
	</h2>
	<p class="blog-summary">
		${target:abbreviateString(blogPage.blogSummary, truncate)}
		<a class="blog-view" href="${blogPage.label}" title="${blogPage.title}">
			<spring:theme code="text.viewMore" /><spring:theme code="icon.right-arrow-large" />
		</a>
	</p>
</div>
