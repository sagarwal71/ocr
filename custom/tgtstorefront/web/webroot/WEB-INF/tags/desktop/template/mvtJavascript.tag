<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:forEach items="${breadcrumbs}" var="breadcrumb" varStatus="breadcrumbStatus">
	<%-- The last breadcrumb object has no categoryCode when on the PDP. --%>
	<c:set var="delimiter" value="${not breadcrumbStatus.first and not empty breadcrumb.categoryCode ? '|' : ''}" />
	<c:set var="categoryPath" value="${categoryPath}${delimiter}${breadcrumb.categoryCode}" />
</c:forEach>

<c:set var="brandCode" value="${pageType == 'Brand' ? categoryName : product.brand}" />

<%-- strip out any double quote chars --%>
<c:set var="categoryPath" value="${fn:replace(categoryPath, '\"', '')}" />
<c:set var="brandCode" value="${fn:replace(brandCode, '\"', '')}" />

<c:if test="${not empty categoryPath or not empty brandCode}">
	<script>
		<c:if test="${not empty categoryPath}">
			var MVT_CATEGORY_PATH = "${categoryPath}";
		</c:if>
		<c:if test="${not empty brandCode}">
			var MVT_BRAND_CODE = "${brandCode}";
		</c:if>
	</script>
</c:if>
