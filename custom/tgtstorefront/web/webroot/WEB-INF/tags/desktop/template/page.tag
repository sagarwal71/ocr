<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true" %>
<%@ attribute name="disableFooterEnews" required="false" %>
<%@ attribute name="bodyCssClass" required="false" type="java.lang.String" %>
<%@ attribute name="metaDescription" required="false" type="java.lang.String" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/desktop/common/footer" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="kioskNav" tagdir="/WEB-INF/tags/desktop/kiosk/navigation" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<template:master pageTitle="${pageTitle}" bodyCssClass="${bodyCssClass}" metaDescription="${metaDescription}">

	<jsp:attribute name="cmsExternalCss">
		<template:cmsExternalCss />
	</jsp:attribute>
	<jsp:attribute name="cmsInlineCss">
		<template:cmsInlineCss />
	</jsp:attribute>
	<jsp:attribute name="cmsExternalJavaScript">
		<template:cmsExternalJavaScript />
	</jsp:attribute>
	<jsp:attribute name="cmsInlineJavaScript">
		<template:cmsInlineJavaScript />
	</jsp:attribute>
	<jsp:attribute name="outerNavigation">
		<kioskNav:navigation />
	</jsp:attribute>
	<jsp:body>
		<header:skip/>
		<header:alertbar />
		<c:if test="${not mobileAppMode}">
			<feature:enabled name="uiStickyHeader">
				<c:set var="headerClass" value="TargetHeader" />
				<feature:enabled name="uiNewMegamenuDisplay">
					<c:set var="headerClass" value="${headerClass} is-newMenu" />
				</feature:enabled>
				<c:set var="wrap"><div class="${headerClass}">||</div></c:set>
				<c:set var="wrap" value="${fn:split(wrap, '||')}"/>
				<c:set var="pageClass" value="is-sticky" />
			</feature:enabled>
			<c:out value="${wrap[0]}" escapeXml="false" />
			<header:header/>
			<a name="skiptonavigation"></a>
			<nav:topNavigation/>
			<c:out value="${wrap[1]}" escapeXml="false" />
		</c:if>
		<c:if test="${mobileAppMode}">
			<span class="header is-mobile-app"></span>
		</c:if>
		<div id="wrapper" data-context-path="${request.contextPath}">
			<div id="page" class="${pageClass}">
				<template:flush />
				<cart:addToCart/>
				<header:belowNavSlot />
				<div class="content">
					<a name="skip-to-content"></a>
					<jsp:doBody/>
					<common:addendumSlot />
				</div>
			</div>
		</div>
		<c:if test="${not disableFooterEnews}">
			<common:enewsQuick source="footer" customerSubscriptionUrl="/enews/quick"/>
		</c:if>
		<c:if test="${not mobileAppMode}">
			<footer:footer/>
		</c:if>
		<common:customerFeedback />
	</jsp:body>

</template:master>
