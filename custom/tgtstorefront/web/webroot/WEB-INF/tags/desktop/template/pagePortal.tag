<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header" %>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true" %>
<%@ attribute name="pageHeading" required="false" rtexprvalue="true" %>
<%@ attribute name="bareMinimum" required="false" rtexprvalue="true" %>
<%@ attribute name="jsRequired" required="false" rtexprvalue="true" %>
<%@ attribute name="disableExternalScripts" required="false" rtexprvalue="true" %>
<%@ attribute name="disableSnippets" required="false" rtexprvalue="true" %>

<template:master pageTitle="${pageTitle}" bareMinimum="${bareMinimum}" jsRequired="${jsRequired}" disableExternalScripts="${disableExternalScripts}" disableSnippets="${disableSnippets}">

		<header:portalHeader portalTitle="${pageHeading}" />

		<div id="page">
			<div class="content">
				<jsp:doBody />
			</div>
		</div>

</template:master>
