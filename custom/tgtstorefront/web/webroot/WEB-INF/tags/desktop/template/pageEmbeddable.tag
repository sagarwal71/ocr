<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="customer" tagdir="/WEB-INF/tags/desktop/customer" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header" %>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/desktop/common/footer" %>

<%--Setting customer-info-disabled to true as the get info is not need for an embeddable page.
	This is passed on to the body tag which adds a class. The class is checked by javascript to disable get info call--%>
<template:master pageTitle="${pageTitle}" bodyCssClass="customer-info-disabled">
	<div id="wrapper" data-context-path="${request.contextPath}">
		<div id="page" class="embeddable-page">
			<div class="content">
				<jsp:doBody/>
			</div>
		</div>
	</div>
</template:master>
