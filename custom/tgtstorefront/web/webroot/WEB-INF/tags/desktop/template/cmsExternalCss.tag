<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty cmsPage.externalCss}">
	<c:set var="styles" value="${fn:split(cmsPage.externalCss, ',')}" />
	<c:forEach items="${styles}" var="file">
		<c:set var="query" value="${fn:contains(file, '?') ? '&' : '?'}" />
		<link rel="stylesheet" type="text/css" media="all" href="${file}${query}b=${cacheBusterTimestamp}" />
	</c:forEach>
</c:if>