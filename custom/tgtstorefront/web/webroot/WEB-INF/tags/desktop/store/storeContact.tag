<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="store" required="true" type="java.lang.Object" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/desktop/store" %>

<c:if test="${not empty store.targetAddressData}">
	<div class="store-contact-card" itemscope="itemscope" itemtype="http://schema.org/Organization">
		<store:storeAddress store="${selectedStore}"/>
	</div>
</c:if>
