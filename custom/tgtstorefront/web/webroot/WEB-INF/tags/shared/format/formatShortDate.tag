<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="hideYear" required="false" type="java.lang.Boolean" %>
<%@ attribute name="date" required="true" type="java.util.Date" %>
<%--
 Tag to render a date formatted.
--%>
<c:choose>
    <c:when test="${hideYear}">
        <c:set var="datePattern" value="d MMMM" />
    </c:when>
    <c:otherwise>
        <c:set var="datePattern" value="d MMM, yyyy" />
    </c:otherwise>
</c:choose>
<fmt:formatDate value="${date}" pattern="${datePattern}"/>