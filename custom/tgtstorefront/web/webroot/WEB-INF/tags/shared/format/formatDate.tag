<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="date" required="true" type="java.util.Date" %>
<%--
 Tag to render a date formatted.
--%>
<fmt:formatDate value="${date}" pattern="EEE d MMMM, yyyy"/>