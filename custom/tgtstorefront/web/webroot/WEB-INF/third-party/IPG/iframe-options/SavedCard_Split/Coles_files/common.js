function ShowProgress(buttonName) {
    var loading = $(".btn-" + buttonName);
    setTimeout(function () {
        loading.addClass('is-loading');
        $(".modal").show();
    }, 200);
    loading.addClass('is-loading');
    $("#frmMain").submit();
}

$(function () {
    fnAttachProgressToSubmitButton();
    $("#frmMain").submit(function (event) {
        var btn = $(this).find("input[type=submit]:focus").attr("id");
        ShowProgress(btn);
        event.preventDefault();
    });

    fnPopover('cvvpopover', "<div class='ui-tooltip'><img src='https://demo.ippayments.com.au/branding/00000254/img/cvv-vm.png' height='30' /> For Visa, MasterCard and Diners, this is the 3 digit code printed on the back of your card.<br/> <br/><img src='https://demo.ippayments.com.au/branding/00000254/img/cvv-amex.png'  height='30' /> For Amex this is the 4 digit code printed on the front of your card.</div>")

    fnPopover('cardnumberpopover', "<div class='ui-tooltip'><img src='https://demo.ippayments.com.au/branding/00000254/img/GiftCardGuideC.png' height='35' />Your number can be found on the back of your card.</div>")
    fnPopover('pinpopover', "<div class='ui-tooltip'><img src='https://demo.ippayments.com.au/branding/00000254/img/GiftCardGuideC.png' height='35' /> Your 4 digit Access Pin can be found on the back of your card.</div>")

    $('body').click(function () {
        $(".popover").hide();
    });

    $(".ccexpiry").each(function () {
        if ($(this).html().toLowerCase().indexOf("expired") >= 0) {
            $(this).html("EXPIRED");
        }
    });
    $(".pSummaryExpiry").each(function () {
        if ($(this).html().length == 8 || !$(this).html().toLowerCase().indexOf("/")) {
            $(this).html("");
        }
    });
});

function fnPopover(popoverid, popoverhtml) {
    $('.' + popoverid).popover({
        placement: 'top',
        delay: { "show": 50, "hide": 400 },
        content: function () {
            return popoverhtml;
        }
    });
}

function fnGetConfirmation(message) {
    if (confirm(message)) {
    }
    else {
        $(this).click(function (event) {
            event.preventDefault();
        });
    }
}

function fnAttachProgressToSubmitButton() {
    var modal = $('<div />');
    modal.addClass("modal");
    $('body').append(modal);
    $('input[type="submit"]').each(function (index) {
        html = "<span class='loading btn-" + $(this).attr("name") + "'><img src='https://demo.ippayments.com.au/branding/00000254/img/loading.GIF' alt='' /></span>"
        $(html).insertAfter($(this));
    });
}

