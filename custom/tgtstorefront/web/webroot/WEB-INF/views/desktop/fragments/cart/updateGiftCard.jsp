<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>

<json:object>
	<json:property name="success" value="${success}" />
	<c:if test="${not success}">
		<json:property name="errorMsg" escapeXml="false">
			<feedback:message type="error" size="small">
				<p><spring:theme code="basket.error.product.giftRecipientValidation" /></p>
			</feedback:message>
		</json:property>
	</c:if>
	<json:property name="summaryHtml" escapeXml="false">
		<c:forEach items="${entry.recipients}" var="recipient" varStatus="recipientStatus">
			<c:if test="${recipient.id eq recipientId}">
				<cart:cartItemEntryRecipientSummary recipient="${recipient}" />
			</c:if>
		</c:forEach>
	</json:property>
	<json:property name="editorHtml" escapeXml="false">
		<form:form commandName="updateGiftCardForm">
			<product:productGiftRecipientForm modify="true" />
		</form:form>
	</json:property>
</json:object>
