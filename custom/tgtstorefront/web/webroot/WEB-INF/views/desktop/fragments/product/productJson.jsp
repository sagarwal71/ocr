<%@ page trimDirectiveWhitespaces="true" contentType="application/json" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<json:object>
	<json:property name="success" value="${not empty product}"/>
	<c:if test="${not empty product}">
		<json:object name="product">
			<json:property name="name" value="${product.name}"/>
			<json:property name="code" value="${product.code}"/>
			<json:property name="url" value="${fullyQualifiedDomainName}${product.url}"/>
			<json:property name="inStock" value="${product.stock.stockLevelStatus.code ne 'outOfStock'}" />
			
			<json:property name="priceHtml" escapeXml="false"><product:pricePoint product="${product}" /></json:property>

			<json:property name="price" escapeXml="false"><format:priceRange price="${product.price}" priceRange="${product.priceRange}" /></json:property>
			<c:if test="${product.showWasPrice}">
				<json:property name="wasPrice" escapeXml="false"><format:priceRange price="${product.wasPrice}" priceRange="${product.wasPriceRange}" /></json:property>
			</c:if>

			<c:if test="${product.numberOfReviews > 0 and product.averageRating > 0}">
				<fmt:formatNumber type="number" maxFractionDigits="2" value="${product.averageRating}" var="averageRating" />
				<json:property name="averageRating" value="${averageRating}" />
				<json:property name="numberOfReviews" value="${product.numberOfReviews}" />
			</c:if>

			<c:if test="${fn:length(galleryImages) > 0}">
				<json:array name="gallery">
					<c:forEach items="${galleryImages}" var="image" varStatus="varStatus">
						<json:object>
							<json:property name="zoom" value="${fullyQualifiedDomainName}${image.zoom.url}" />
							<json:property name="product" value="${fullyQualifiedDomainName}${image.product.url}" />
							<json:property name="thumbnail" value="${fullyQualifiedDomainName}${image.thumbnail.url}" />
						</json:object>
					</c:forEach>
				</json:array>
			</c:if>
			
			<c:if test="${not empty product.baseProductCanonical}">
				<json:object name="baseProduct">
					<json:property name="code" value="${product.baseProductCanonical.code}"/>
					<json:property name="url" value="${fullyQualifiedDomainName}${product.baseProductCanonical.url}"/>
				</json:object>	
			</c:if>
		</json:object>			
	</c:if>
</json:object>
