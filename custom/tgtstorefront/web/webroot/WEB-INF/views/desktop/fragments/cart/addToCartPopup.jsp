<%@ page trimDirectiveWhitespaces="true" contentType="application/json" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<feature:enabled name="uiPDPaddToCart">
	<c:set var="newAdd" value="${true}" />
</feature:enabled>

<json:object>
	<json:property name="success" value="${success}"/>
	<c:if test="${not success and not empty errorMsg and not isReactRequest}">
		<json:property name="errorMsg" escapeXml="false">
			<feedback:message type="error" size="small">
				<p><spring:theme code="${errorMsg}" arguments="${realAddedQuantity}" /></p>
			</feedback:message>
		</json:property>
	</c:if>
	<c:choose>
		<c:when test="${not isReactRequest}">
			<c:if test="${not empty realAddedQuantity}">
				<json:property name="realAddedQuantity" value="${realAddedQuantity}" />
			</c:if>
			<template:gtmEcommJson abstractOrderData="${cartData}" />
			<%-- TODO Check if cartData is needed in this response --%>
			<json:object name="cartData">
				<json:property name="total" value="${cartData.totalPrice.value}"/>
				<json:array name="products" items="${cartData.entries}" var="cartEntry">
					<json:object>
						<json:property name="code" value="${cartEntry.product.code}"/>
						<json:property name="name" value="${cartEntry.product.name}"/>
						<json:property name="quantity" value="${cartEntry.quantity}"/>
						<json:property name="price" value="${cartEntry.basePrice.value}"/>
						<json:array name="categories" items="${cartEntry.product.categories}" var="category">
							<json:property value="${category.name}"/>
						</json:array>
					</json:object>
				</json:array>
			</json:object>
			<json:object name="productData" escapeXml="false">
				<c:set var="productImageMissing"><asset:resource code="img.missingProductImage.thumbnail" /></c:set>
				<c:set var="productImageObject" value="${ycommerce:productImage(product, 'store')}" />
				<c:set var="productImage" value="${not empty productImageObject and not empty productImageObject.url ? productImageObject.url : productImageMissing}" />
				<json:object name="product">
					<json:property name="baseCode" value="${product.baseProductCode}"/>
					<json:property name="code" value="${product.code}"/>
					<json:property name="name" value="${product.name}"/>
					<json:property name="quantity" value="${quantity}"/>
					<json:property name="price" value="${product.price.value}"/>
					<json:property name="image" value="${productImage}"/>
					<json:property name="assorted" value="${product.assorted}" />
				</json:object>
				<json:property name="url" value="${fullyQualifiedDomainName}${product.url}"/>
				<template:productEcommJson product="${product}" assorted="${product.assorted}" />
				<c:if test="${success}">
					<json:object name="fragments">
						<json:property name="dealsHtml" escapeXml="false">
							<product:productPotentialDeals promotions="${product.potentialPromotions}" dealDescription="${product.dealDescription}" />
						</json:property>
					</json:object>
				</c:if>
			</json:object>
			<c:if test="${giftCard and digitalGiftCard}">
				<json:property name="recipientFormHtml" escapeXml="false">
					<form:form method="post" commandName="addToCartForm">
						<product:productGiftRecipientForm />
					</form:form>
				</json:property>
			</c:if>
			<c:if test="${success}">
				<json:property name="feedbackHtml" escapeXml="false">
					<feedback:message type="success" size="small" cssClass="${empty realAddedQuantity ? ' ProductAdded ' : ''}">
						<p>
							<c:if test="${empty realAddedQuantity}">
								<spring:theme code="text.addToCart.hasBeenAdded" /> &ndash; <a href="${targetUrlsMapping.cart}"><spring:theme code="cart.view" /></a>
							</c:if>
							<c:if test="${not empty realAddedQuantity}">
								<spring:theme code="text.addToCart.amendedQuantityHasBeenAdded" arguments="${realAddedQuantity}" argumentSeparator=";;;" /> &ndash; <a href="${targetUrlsMapping.cart}"><spring:theme code="cart.view" /></a>
							</c:if>
						</p>
					</feedback:message>
				</json:property>
			</c:if>
		</c:when>
		<c:otherwise>
			<json:object name="data">
				<c:if test="${not empty realAddedQuantity}">
					<json:property name="adjustedQuantity" value="${realAddedQuantity}" />
				</c:if>
				<template:gtmEcommJson abstractOrderData="${cartData}" />
				<json:object name="product" escapeXml="false">
					<c:set var="productImageMissing"><asset:resource code="img.missingProductImage.thumbnail" /></c:set>
					<c:set var="productImageObject" value="${ycommerce:productImage(product, 'store')}" />
					<c:set var="productImage" value="${not empty productImageObject and not empty productImageObject.url ? productImageObject.url : productImageMissing}" />
					<json:object name="product">
						<json:property name="baseCode" value="${product.baseProductCode}"/>
						<json:property name="code" value="${product.code}"/>
						<json:property name="name" value="${product.name}"/>
						<json:property name="quantity" value="${quantity}"/>
						<json:property name="price" value="${product.price.value}"/>
						<json:property name="image" value="${productImage}"/>
						<json:property name="assorted" value="${product.assorted}" />
					</json:object>
					<json:property name="url" value="${fullyQualifiedDomainName}${product.url}"/>
					<template:productEcommJson product="${product}" assorted="${product.assorted}" />
					<c:if test="${success}">
						<json:object name="fragments">
							<json:property name="dealsHtml" escapeXml="false">
								<product:productPotentialDeals promotions="${product.potentialPromotions}" dealDescription="${product.dealDescription}" />
							</json:property>
						</json:object>
					</c:if>
				</json:object>
				<c:if test="${not empty errorMsg}">
					<json:object name="error" escapeXml="false">
						<json:property name="code" value="${errorMsg}" />
					</json:object>
				</c:if>
			</json:object>
		</c:otherwise>
	</c:choose>
</json:object>
