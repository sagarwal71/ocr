<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>


<div class="main">
	<div class="FavShareForm" data-post-url="/favourites/share" data-share-url="/favourites/share" data-success-publish="/favourites/share/success" data-fail-publish="/favourites/share/fail" data-share-limit="${shareLimit}">
		<h1 class="FavShareForm-heading"><spring:theme code="favourites.share.form.heading" /></h1>
		<user:favouritesShare
			shareLimit="${shareLimit}"/>
	</div>
</div>
<a href="javascript:history.back();" class="modal-back-button hfma">Back</a>

