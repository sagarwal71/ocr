<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<div class="main">
	<div class="flybuys-login-form"
		data-post-url="${targetUrlsMapping.paymentFlybuysLogin}"
		data-success-publish="/flybuys/login/success"
		data-fail-publish="/flybuys/login/fail">
		<h3><spring:theme code="flybuys.login.heading"/></h3>
		<multi-checkout:flybuysLogin />
	</div>
</div>
