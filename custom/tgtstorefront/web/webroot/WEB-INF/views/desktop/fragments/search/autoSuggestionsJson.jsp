<%@ page trimDirectiveWhitespaces="true" contentType="application/json" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:theme code="icon.right-arrow-small" var="arrowIcon" />

<c:set var="breadcrumbTrail"><div class="breadcrumb-trail">||</div></c:set>
<c:set var="breadcrumbTrail" value="${fn:split(breadcrumbTrail, '||')}" />
<spring:theme var="categoryType" code="search.autosuggestion.heading.category" />

<json:array name="suggestions">
	<c:if test="${not empty autoSuggestSearchResult
		and not empty (autoSuggestSearchResult.contents) and (fn:length(autoSuggestSearchResult.contents[0]) gt 0)
		and not empty (autoSuggestSearchResult.contents[0].autoSuggest) and (fn:length(autoSuggestSearchResult.contents[0].autoSuggest) gt 0)}">

		<c:forEach items="${autoSuggestSearchResult.contents[0].autoSuggest}" var="autoSuggest">

			<c:choose>
				<c:when test="${not empty autoSuggest.dimensionSearchGroups and fn:length(autoSuggest.dimensionSearchGroups) gt 0}">
					<c:forEach items="${autoSuggest.dimensionSearchGroups}" var="group">
						<c:if test="${not empty group.dimensionSearchValues and fn:length(group.dimensionSearchValues) gt 0}">
							<json:object>
								<spring:theme var="type" code="search.autosuggestion.heading.${group.displayName}" text="${group.displayName}" />
								<json:property name="type" value="${type}"/>
								<json:array name="items">
									<c:forEach items="${group.dimensionSearchValues}" var="item">
										<c:if test="${(not empty item.ancestors) and (fn:length(item.ancestors) gt 0)}">
											<c:set var="multiLabel">${breadcrumbTrail[0]}</c:set>
											<c:forEach items="${item.ancestors}" var="ancestor">
												<spring:theme var="multiLabel" code="link.withArrow" arguments="${multiLabel} ${ancestor.label}" />
											</c:forEach>
											<c:set var="multiLabel">
												${multiLabel}
												${breadcrumbTrail[1]}
												<br />
												<span class="category">${item.label}</span>
											</c:set>
										</c:if>
										<c:set var="singleLabel">${item.label}</c:set>
										<c:if test="${type eq categoryType}">
											<c:set var="singleLabel"><span class="category category-single">${item.label}</span></c:set>
										</c:if>
										<c:set var="label" value="${not empty multiLabel ? multiLabel : singleLabel}" />
										<json:object>
											<json:property name="label" value="${label}" escapeXml="false" />
											<json:property name="value" value="${item.navigationState}"/>
											<json:property name="url" value="${item.navigationState}"/>
										</json:object>
										<c:remove var="multiLabel" />
									</c:forEach>
								</json:array>
							</json:object>
						</c:if>
					</c:forEach>
				</c:when>

				<%-- Products --%>
				<c:when test="${not empty autoSuggest.records and fn:length(autoSuggest.records) gt 0}">
					<json:object>
						<spring:theme var="type" code="search.autosuggestion.heading.product" text="product" />
						<json:property name="type" value="${type}"/>
						<json:array name="items">
							<c:forEach items="${autoSuggest.records}" var="record">
								<json:object>
									<json:property name="label" value="${record.attributes.productName}" escapeXml="false" />
									<json:property name="value" value="${record.detailsAction.recordState}"/>
									<json:property name="url" value="${record.detailsAction.recordState}"/>
								</json:object>
							</c:forEach>
						</json:array>
					</json:object>
				</c:when>
			</c:choose>

		</c:forEach>
	</c:if>
</json:array>
