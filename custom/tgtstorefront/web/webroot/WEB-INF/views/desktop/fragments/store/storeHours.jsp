<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/desktop/store" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<common:globalMessages/>

<div class="main">

	<div class="store-locator-store StoreDetails--modal">
		<div class="store-details">
			<store:storeContact store="${store}"/>
			<store:storeHours openingSchedule="${store.openingHours}"/>
			<c:if test="${not empty store.targetAddressData}">
				<c:url value="${targetUrlsMapping.storeFinder}" var="nearbyUrl">
					 <c:param name="q" value="${store.targetAddressData.postalCode}"/>
				</c:url>
			</c:if>
		</div>
	</div>
</div>
