<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/desktop/util" %>

<c:set var="pageMode" value="${not availableOnline and availableInStore ? 'store' : 'online'}" />
<c:set var="stockDataAttr"><product:productConsolidatedStockDataAttrs pageMode="${pageMode}" /></c:set>
<c:set var="availQtyInStoreorOnline" value="${availableOnline or availableInStore}" />
<feature:enabled name="featureConsolidatedStoreStockVisibility">
	<c:set var="consolidatedPDP" value="${true}" />
</feature:enabled>
<feature:enabled name="uiReactPDPDetailPanel">
	<c:set var="reactDetailsPanel" value="${true}" />
</feature:enabled>

<c:set var="carouselType" value="cloudzoom" />

<c:set var="showDescTab" value="${not empty product.youTubeLink or not empty product.description}" />
<c:set var="showFeatureTab" value="${not empty product.productFeatures or not empty product.materials or not empty product.careInstructions or not empty product.extraInfo}" />

<c:choose>
	<c:when test="${not reactDetailsPanel}">
		<c:set var="reactDetailClasses"><%--
			--%>has-detail-panel is-${pageMode}-mode ${not availQtyInStoreorOnline and consolidatedPDP ? ' is-sold-out' : ''}<%--
		--%></c:set>
	</c:when>
	<c:otherwise>
		<c:set var="reactDetailClasses"><%--
			--%>has-react-panel<%--
		--%></c:set>
	</c:otherwise>
</c:choose>

<c:set var="ecProductJson"><product:productDetailAnalytics /></c:set>
<div class="prod-detail ProductDetails ${reactDetailClasses} is-${carouselType} is-no-store-search"
	${ecProductJson}
	${stockDataAttr}
	data-colour-variant-code="${product.colourVariantCode}"
	data-base-prod-code="${product.baseProductCode}"
	data-prod-code="${product.code}"
	data-prod-context="${product.baseProductCode}"
	data-no-store-search="true"
	data-stock-status="${not availQtyInStoreorOnline ? 'soldout' : ''}">
	<div class="main">
		<c:choose>
			<c:when test="${reactDetailsPanel}">
				<div class="react-root" data-component="shopTheLookItem">
					<util:reactInitialState
						productData='"${product.baseProductCode}": ${targetProductListerData}'
						applicationData='"page": {"type": "${pageType}","data":{"baseProductCode": "${product.baseProductCode}", "selectedVariant": "${product.code}"}},"environment":${environmentData}'
						featureData="${uiFeaturesEnabled}"
						lookData="${shopTheLookData}" />
				</div>
				<script>t_msg.publish('/react/reload')</script>
			</c:when>
			<c:otherwise>
				<product:productDetailsImages product="${product}" galleryImages="${galleryImages}" hidePromoStatus="${true}" />
				<product:productPageTabs
					product="${product}"
					tabClass="product-group-tabs"
					tabContentClass="product-group-tab-content"
					showDetails="true"
					tabMenuClass="product-group-tab-menu"
					pageMode="${pageMode}"/>
			</c:otherwise>
		</c:choose>
	</div>
</div>
