<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/desktop/util" %>

<c:set var="storeData">"storeData": ${storeJson}</c:set>

<div class="react-root" data-component="customerLocation" data-root-url="${not empty type ? type : '/find-store'}">
	<util:reactInitialState entityData="${storeData}" featureData="${uiFeaturesEnabled}" />
	<util:loading />
</div>
<script>setTimeout(function(){t_msg.publish('/react/reload')}, 500)</script>
