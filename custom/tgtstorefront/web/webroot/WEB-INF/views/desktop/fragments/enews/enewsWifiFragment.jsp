<%@ page trimDirectiveWhitespaces="true" contentType="application/json" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/desktop/account" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<json:object>
	<json:property name="success" value="${success}"/>
	<json:property name="message" value="${message}"/>
	<json:property name="formHTML" escapeXml="false">
		<account:subscribe actionNameKey="store.wifi.action">
			<jsp:attribute name="beforeFields">
				<h4 ><spring:theme code="store.wifi.intro" /></h4>
			</jsp:attribute>
		</account:subscribe>
	</json:property>
</json:object>
