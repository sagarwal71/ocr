<%@ page trimDirectiveWhitespaces="true" contentType="application/json" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:theme code="field.flyBuys" var="flybuysText" />
<json:object>
	<json:property name="success" value="${success}"/>
	<json:property name="message">
		<c:if test="${not success}">
			<spring:theme code="field.invalid.notAvailable" arguments="${flybuysText}" />
		</c:if>
	</json:property>
</json:object>	