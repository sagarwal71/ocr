<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/desktop/store" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>


<c:if test="${not empty productData}">
	<c:set var="carsouselHtml">
		<product:productCarousel products="${productData}" width="${contentSlotContext}" unveil="true" listType="Recently Viewed" />
	</c:set>
</c:if>

<json:object>
	<json:array name="products">
		<c:forEach items="${productData}" var="product">
				<json:property name="productCode" value="${product.baseProductCode}" />
		</c:forEach>
	</json:array>
	<json:property name="carsouselHtml" escapeXml="false">
		<c:if test="${not empty carsouselHtml}">
			${carsouselHtml}
		</c:if>
	</json:property>
</json:object>
