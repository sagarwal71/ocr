<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>

<div class="cart-title">
	<h4 class="title"><spring:theme code="popup.cart.title"/></h4>
	<spring:theme code="popup.close" var="popupClose" />
	<a href="#" class="close" title="${popupClose}">${popupClose}</a>
</div>
<c:if test="${empty numberItemsInCart or numberItemsInCart le 0}">
	<div class="cart-entry cart-empty">
		<p><spring:theme code="popup.cart.empty"/></p>
	</div>
</c:if>

<c:if test="${numberShowing > 0 }">
	<c:forEach items="${entries}" var="entry" end="${numberShowing - 1}">
		<cart:cartEntryContent entry="${entry}" />
	</c:forEach>
</c:if>

<div class="cart-total">
	<p class="subtotal">
		<span class="label"><spring:theme code="popup.cart.subtotal"/></span>
		<span class="price"><format:price priceData="${cartData.subTotal}"/></span>
	</p>
	<c:if test="${numberShowing > 0 }">
		<p class="legend">
			<spring:theme code="popup.cart.showing" arguments="${numberShowing},${numberItemsInCart}"/>&nbsp;<spring:theme code="popup.cart.subtotalsNote" />
		</p>
	</c:if>
</div>

<div class="cart-links">
	<a class="continue close" href="#">
		<spring:theme code="icon.left-arrow-small" /><span><spring:theme code="cart.continue" /></span>
	</a>
	<a class="button-fwd checkout" href="${targetUrlsMapping.cart}">
		<spring:theme code="cart.view" />
	</a>
</div>
