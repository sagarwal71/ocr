<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>


<div class="main">
	<div class="flybuys-redeem-list"
		data-post-url="${targetUrlsMapping.paymentFlybuysSelectOption}"
		data-success-publish="/flybuys/redeem/change">
		<dl class="flybuys-attr flybuys-attr-col">
			<dt><spring:theme code="payment.flyBuys.redeem.label"/></dt>
			<dd>${cartData.flybuysNumber}</dd>
		</dl>
		<dl class="flybuys-attr flybuys-attr-col">
			<dt><spring:theme code="payment.flyBuys.points.label"/></dt>
			<dd><strong>${redeemOptions.availablePoints}</strong></dd>
		</dl>
		<multi-checkout:flybuysOptions />
	</div>
</div>
<a href="javascript:history.back();" class="modal-back-button hfma">Back</a>
