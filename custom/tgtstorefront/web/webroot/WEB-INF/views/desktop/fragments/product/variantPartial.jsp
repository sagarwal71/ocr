<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/desktop/util" %>

<c:set var="pageMode" value="online" />
<c:set var="stockDataAttr"><product:productConsolidatedStockDataAttrs pageMode="${pageMode}" /></c:set>
<c:set var="variantSelected" value="${not empty product.sellableVariantDisplayCode}" />

<feature:enabled name="featureConsolidatedStoreStockVisibility">
	<c:set var="consolidatedPDP" value="${true}" />
</feature:enabled>
<c:set var="availQtyInStoreorOnline" value="${availableOnline or availableInStore}" />

<c:set var="carouselType" value="cloudzoom" />
<feature:enabled name="uiReactPDPCarousel">
	<c:set var="carouselType" value="react" />
</feature:enabled>
<feature:enabled name="uiReactPDPDetailPanel">
	<c:set var="reactDetailsPanel" value="${true}" />
</feature:enabled>

<div>
	<div class="prod-detail ProductDetails is-${pageMode}-mode is-${carouselType} ${not availQtyInStoreorOnline and consolidatedPDP ? ' is-sold-out' : ''} ${reactDetailsPanel ? ' has-react-panel' : ' has-detail-panel'}" data-stock-status="${not availQtyInStoreorOnline ? 'soldout' : ''}"  data-url-suffix="/variants" data-colour-variant-code="${product.colourVariantCode}" data-base-prod-code="${product.baseProductCode}" data-prod-code="${product.code}" ${stockDataAttr} >
		<div class="main">
			<c:choose>
				<c:when test="${reactDetailsPanel}">
					<div class="react-root" data-component="favouritesView">
						<util:reactInitialState
							productData='"${product.baseProductCode}": ${targetProductListerData}'
							applicationData='"page": {"type": "${pageType}","data":{"baseProductCode": "${product.baseProductCode}", "selectedVariant": "${product.code}"}},"environment":${environmentData}'
							featureData="${uiFeaturesEnabled}" />
					</div>
					<script>t_msg.publish('/react/reload')</script>
				</c:when>
				<c:otherwise>
					<div class="prod-variants">
						<h5 class="ProdVariantsPanel-heading"><strong><spring:theme code="product.variants.panel.heading${(not empty colours and fn:length(colours) eq 1) or (empty sizes) ? '.single' : ''}" /></strong></h5>
						<feature:enabled name="findInStore2PDP">
							<product:colourVariants
								colours="${colours}" />
							<product:sizeVariants
								sizes="${sizes}" />
							<c:if test="${consolidatedPDP and availQtyInStoreorOnline}">
								<div class="consolidated-stock-container variant-changeable">
									<c:if test="${not variantSelected}">
										<product:productStockLevel
											product="${product}"
											variantSelected="${variantSelected}"
											colours="${colours}"
											sizes="${sizes}"
											messageOnly="${newPDP}"
										/>
									</c:if>
									<c:if test="${variantSelected}">
										<div class="StockContainer">

										</div>
									</c:if>
								</div>
							</c:if>
						</feature:enabled>
						<c:if test="${not availQtyInStoreorOnline and consolidatedPDP}">
							<div class="FavProducts-unavailable">
								<spring:theme code="product.page.message.soldout"/>
							</div>
						</c:if>
						<feature:disabled name="findInStore2PDP">
							<product:productColoursPanel
								colours="${colours}"
								product="${product}" />
							<product:productClassificationSelectionPanel
								baseProductCode="${product.baseProductCode}"
								classifications="${sizes}" />
						</feature:disabled>
						<div class="ProdVariantsPanel-controls">
								<p class="ProdVariantsPanel-error"></p>
								<button class="button-fwd-alt ProdVariantsPanel-confirm button-wide ${empty product.sellableVariantDisplayCode ? 'disabled-button ProdVariantsPanel-confirm--disabled' : ''}"
									data-selected-variant="${product.code}"
									data-base-prod-code="${product.baseProductCode}"
									data-sellable="${not empty product.sellableVariantDisplayCode}"><spring:theme code="product.variants.panel.done" /></button>
								<button class="button-anchor ProdVariantsPanel-cancel button-wide"><spring:theme code="product.variants.panel.cancel" /></button>
							<c:if test="${not availQtyInStoreorOnline and consolidatedPDP}">
								<product:favEndOfLifeControls originalCategoryCode="${product.originalCategoryCode}"/>
							</c:if>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
</div>
