<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="megamenu" tagdir="/WEB-INF/tags/desktop/nav/megamenu"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:choose>
	<c:when test="${layoutType eq 'MOBILE'}">
		<nav class="only-for-small">
			<ul class="mm-Departments mm-Departments--vertical">
				<megamenu:navigation />
			</ul>
		</nav>
	</c:when>
	<c:when test="${layoutType eq 'LIST'}">
		<div class="DepartmentNavList">
			<h2><spring:theme code="text.Browse" /></h2>
			<ul class="PlainList">
				<megamenu:departmentComponentListLayout
					iconAfter="icon.right-arrow-small" />
			</ul>
		</div>
	</c:when>
	<c:otherwise>
		<nav class="only-for-small">
			<ul class="mm-Departments mm-Departments--vertical">
				<megamenu:navigation />
			</ul>
		</nav>
	</c:otherwise>
</c:choose>
