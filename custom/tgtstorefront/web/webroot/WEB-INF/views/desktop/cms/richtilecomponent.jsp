<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:if test="${not empty component.kioskFunctions}">
	<c:set var="kioskFunctions" value="kiosk-${fn:toLowerCase(component.kioskFunctions)}" />
</c:if>

<div class="rich-tile ${kioskFunctions}">
	<c:set var="tileBody">
		<span class="heading">
			<span class="head-content">
				<c:if test="${not empty component.media and not empty component.media.url}">
					<span class="rich-icon" style="background-image:url('${component.media.url}');"></span>
				</c:if>
				<span class="text">${component.headline}</span>
			</span>
			<c:if test="${kioskLandingSlotContext}">
				<spring:theme code="icon.right-arrow-large-nospace"/>
			</c:if>
		</span>
		<span class="description">${component.descriptionContent}</span>
	</c:set>

	<c:choose>
		<c:when test="${not empty component.cmsLink}">
			<c:set var="cmsLinkBodyOverride" value="${tileBody}" scope="request" />
			<cms:component component="${component.cmsLink}"/>
			<c:remove var="cmsLinkBodyOverride" scope="request" />
		</c:when>
		<c:otherwise>
			<span class="static">
				<c:out value="${tileBody}" escapeXml="false" />
			</span>
		</c:otherwise>
	</c:choose>
</div>