<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<c:set var="bannerNum" value="${fn:length(banners)}"/>

<c:if test="${bannerNum gt 0}">
	<c:set var="rowNums" value="${target:validateNumberListToArray(component.bannersPerRow)}" />
	<c:set var="start" value="0" />
	<c:set var="bannersRemaining" value="${bannerNum}" />

	<c:if test="${empty rowNums}">
		<c:set var="rowNums" value="${fn:split(bannerNum,',')}" />
	</c:if>
	<c:choose>
		<c:when test="${not empty component.title}">
			<comp:component-heading
				title="${component.title}"
				compLink="${component.link}"/>
		</c:when>
		<c:otherwise>
			<c:set var="cssClass" value="flexi-collapse-above" />
		</c:otherwise>
	</c:choose>
	<div class="flexi-banners ${cssClass}">
		<c:forEach items="${rowNums}" var="itemNum">
			<c:set var="itemNum" value="${itemNum gt bannersRemaining ? bannersRemaining : itemNum}" />
			<div class="flexi-banner-contains-${itemNum}">
				<c:forEach items="${banners}" var="banner" varStatus="status" begin="${start}" end="${start + (itemNum - 1)}">
					<cms:component component="${banner}"/>
					<c:set var="start" value="${start + 1}" />
					<c:set var="bannersRemaining" value="${bannersRemaining - 1}" />
				</c:forEach>
			</div>
		</c:forEach>
	</div>
</c:if>