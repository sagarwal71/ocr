<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<h2><spring:theme code="search.box.heading" /></h2>
<div class="search-box mini-search">
	<header:search />
</div>
