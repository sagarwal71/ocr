<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<c:set var="feedbackItems">
	<json:array name="feedbackItems">
		<c:forEach items="${feedbacks}" var="feedback">
			<cms:component component="${feedback}"/>
		</c:forEach>
	</json:array>
</c:set>

<%-- TODO: Have option to only output JSON array for SPC --%>
<c:if test="${not kioskMode and not mobileAppMode}">
	<div class="CustomerFeedback" data-feedback-items='${feedbackItems}' data-min-session="${component.minSession}" data-exclude="${component.globalExclusion}">
	</div>
</c:if>
	