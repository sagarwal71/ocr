<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>

<c:if test="${not empty customerSubscriptionType}">
	<c:set var= "additionalClass" value="${fn:toLowerCase(customerSubscriptionType)}-subscription"/>
	<c:set var= "subscriptionType" value="${fn:toLowerCase(customerSubscriptionType)}"/>
	<c:set var= "source" value="${fn:toLowerCase(customerSubscriptionType)}"/>
	<c:set var="subscriptionUrl" value="/customer-subscription/${fn:toLowerCase(customerSubscriptionType)}"/>
	<c:set var="subscriptionUpdateUrl" value="${fullyQualifiedDomainName}/${fn:toLowerCase(customerSubscriptionType)}/personalise"/>

	<c:if test="${customerSubscriptionType eq 'Newsletter'}">
		<c:set var="subscriptionUrl" value="/enews/quick"/>
		<c:remove var="subscriptionUpdateUrl" />
	</c:if>
</c:if>

<div class="customer-subscription ${additionalClass}">
	<common:enewsQuick source="${source}" customerSubscriptionType="${subscriptionType}" customerSubscriptionHeading="${component.title}" customerSubscriptionUrl="${subscriptionUrl}" customerSubscriptionUpdateUrl="${subscriptionUpdateUrl}"/>
</div>
