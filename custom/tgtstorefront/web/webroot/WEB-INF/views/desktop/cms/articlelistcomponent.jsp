<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<c:if test="${fn:length(components) > 0}">
	<div class="article-listing">
		<c:if test="${not empty title}"><h3>${title}</h3></c:if>

		<c:forEach var="comp" items="${components}" varStatus="status">

			<div class="article-item ${status.index % 3 eq 0 ? 'first-third' : ''}">
				<cms:component component="${comp}"/>
			</div>

		</c:forEach>

	</div>
</c:if>
