<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:if test="${not empty subcategoryGridItems}">
	<c:if test="${not empty component.title}">
		<h3><c:out value="${component.title}" escapeXml="${true}" /></h3>
	</c:if>
	<div class="subcategories-container">
		<ul class="subcategories-grid">
			<c:forEach var="subcategoryGridItem" items="${subcategoryGridItems}" varStatus="status">
				<c:url value="${subcategoryGridItem.linkUrl}" var="linkUrl" />
				<li class="${status.index % 3 eq 0 ? 'third' : ''}"><a href="${linkUrl}" title="${subcategoryGridItem.title}" class="link">${subcategoryGridItem.title}</a></li>
			</c:forEach>
		</ul>
	</div>
</c:if>