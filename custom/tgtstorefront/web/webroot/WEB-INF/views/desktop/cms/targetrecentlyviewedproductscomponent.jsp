<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<c:if test="${not kioskMode}">
	<div class="recently-viewed-products" data-carousel-limit="${not empty component.limit ? component.limit : 10 }" data-carousel-url="/customer-recently-viewed-products">
		<c:if test="${not empty component.title}">
			<c:set var="link">
				<a href="#" class="clear-all"><spring:theme code="icon.plain-cross" /><spring:theme code="component.recentlyViewedProducts.clearAll"/></a>
			</c:set>
			<comp:component-heading
				title="${component.title}"
				staticLink="${link}"/>
		</c:if>
	</div>
</c:if>
