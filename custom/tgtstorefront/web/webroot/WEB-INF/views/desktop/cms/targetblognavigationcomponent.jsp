<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="blog" tagdir="/WEB-INF/tags/desktop/blog" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<div class="blog">

	<c:if test="${component.showTitle}">
		<div class="blog-lead">
			<h1 class="blog-heading">
				<c:choose>
					<c:when test="${isContentPage and cmsPage.label ne blogLeadPage.label}">
						<a href="${blogLeadPage.label}" title="${blogLeadPage.title}">${blogLeadPage.title}</a>
					</c:when>
					<c:otherwise>
						${blogLeadPage.title}
					</c:otherwise>
				</c:choose>
			</h1>
		</div>
	</c:if>

	<div class="blog-section">
		<c:forEach items="${blogLargeList}" var="blogLarge">
			<blog:summary size="large" blogPage="${blogLarge}" truncate="350" />
		</c:forEach>
	</div>

	<div class="blog-section">
		<c:forEach items="${blogMediumList}" var="blogMedium">
			<blog:summary size="medium" blogPage="${blogMedium}" truncate="190" />
		</c:forEach>
	</div>

	<div class="blog-section">
		<c:forEach items="${blogSmallList}" var="blogSmall">
			<blog:summary size="small" blogPage="${blogSmall}" truncate="110" noImage="true" />
		</c:forEach>
	</div>
</div>
