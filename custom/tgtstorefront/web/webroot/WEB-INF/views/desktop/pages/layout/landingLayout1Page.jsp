<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>


<template:page pageTitle="${pageTitle}">
	<common:globalMessages/>
	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<common:championSlot slotName="ListerChampion" />

	<div class="hero">
		<c:set var="contentSlotContext" value="wide" scope="request" />
		<cms:pageSlot var="heroComponent" position="Hero">
			<cms:component component="${heroComponent}"/>
		</cms:pageSlot>
		<c:remove var="contentSlotContext" scope="request" />
	</div>

	<div class="aside hero-aside">
		<div class="promos">
			<c:set var="contentSlotContext" value="narrow" scope="request" />
			<cms:pageSlot var="asideComponent" position="Aside">
				<cms:component component="${asideComponent}"/>
			</cms:pageSlot>
			<c:remove var="contentSlotContext" scope="request" />
		</div>
	</div>

	<div class="main">
		<c:set var="contentSlotContext" value="full" scope="request" />
		<cms:pageSlot var="mainComponent" position="Main">
			<cms:component component="${mainComponent}"/>
		</cms:pageSlot>
		<c:remove var="contentSlotContext" scope="request" />
	</div>

	<div class="supplement">
		<c:set var="contentSlotContext" value="full" scope="request" />
		<cms:pageSlot var="supplementComponent" position="Supplement">
			<cms:component component="${supplementComponent}"/>
		</cms:pageSlot>
		<c:remove var="contentSlotContext" scope="request" />
	</div>

</template:page>