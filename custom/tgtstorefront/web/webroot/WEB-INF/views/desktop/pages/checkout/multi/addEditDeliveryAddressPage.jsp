<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="customer" tagdir="/WEB-INF/tags/desktop/customer" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>

<%-- to be replaced to tgt:tag --%>
<cms:pageSlot var="component" position="Supplement">
	<c:set var="hasVisibleComponentsSupplement" value="${true}" />
</cms:pageSlot>

<template:checkoutPage stepName="yourAddress" hideHome="true" pageTitle="${pageTitle}">

	<common:globalMessages/>
	<cart:sohMessage />

	<div class="co-page co-status-init">
		<div class="main">

			<h2 class="co-heading"><spring:theme code="checkout.multi.deliveryAddress" /></h2>
			<multi-checkout:pageLoadingIndicator />
			<div class="co-groupe co-interactive del-mode del-mode-active del-mode-single del-mode-${activeDeliveryMode.deliveryModeCode}">

				<div class="del-summary">
					<h3>
						<spring:theme code="checkout.multi.delivery.modes.${activeDeliveryMode.deliveryModeCode}" />
						<c:if test="${delMode.available and not empty delMode.fee}">-
							<strong><format:price priceData="${activeDeliveryMode.deliveryFee}" displayFreeForZero="true"/></strong>
						</c:if>
					</h3>
					<c:if test="${not empty activeDeliveryMode.message}">
						<p>${activeDeliveryMode.message}</p>
					</c:if>
					<c:if test="${not empty activeDeliveryMode.disclaimer}">
						<p class="note">${activeDeliveryMode.disclaimer}</p>
					</c:if>
				</div>

				<div class="del-detail">
					<h3>
						<spring:theme code="checkout.multi.addressDetails" text="Address Details"/>
					</h3>
					<p class="required"><spring:theme code="form.mandatory" text="*Mandatory"/></span>

					<p>
						<spring:theme code="checkout.multi.addEditform" text="Please use this form to add/edit an address."/>

					</p>

					<form:form method="post" action="${edit ? targetUrlsMapping.yourAddressEdit : targetUrlsMapping.yourAddressAdd}" commandName="addressForm" cssClass="${singleLineAddressLookup ? '' : 'del-address-form' }" data-confirm-url="${edit ? targetUrlsMapping.yourAddressEdit : targetUrlsMapping.yourAddressAdd}" novalidate="true">
						<spring:hasBindErrors name="addressForm">
							<c:set var="hasErrors" value="${true}" />
						</spring:hasBindErrors>
						<input type="hidden" name="deliveryModeCode" value="${activeDeliveryMode.deliveryModeCode}"/>
						<c:set var="defaultAddress" value="${edit ? addressForm.defaultAddress : false}"/>
						<customer:addressFields
							titles="${titles}"
							selectedTitle="${addressForm.titleCode}"
							states="${states}"
							selectedState="${addressForm.state}"
							countries="${countries}"
							selectedCountry="${addressForm.countryIso}"
							allowDefault="${hasAddress and not defaultAddress and cartData.allowSaveAddress}"
							singleInputActive="${not hasErrors and not edit}" />

						<div class="f-buttons">
							<a class="button-norm" href="${cancelTarget}">
								<spring:theme code="checkout.multi.cancel" text="Cancel"/>
							</a>

							<button class="button-fwd"><spring:theme code="checkout.multi.saveAddress" text="Save address"/></button>
						</div>
						<target:csrfInputToken/>
					</form:form>
				</div>
			</div>
		</div>

		<div class="aside">
			<multi-checkout:checkoutOrderDetails cartData="${cartData}" />
		</div>

		<c:if test="${hasVisibleComponentsSupplement}">
			<div class="supplement">
				<c:set var="contentSlotContext" value="full" scope="request" />
				<cms:pageSlot var="supplementComponent" position="Supplement">
					<cms:component component="${supplementComponent}"/>
				</cms:pageSlot>
				<c:remove var="contentSlotContext" scope="request" />
			</div>
		</c:if>
	</div>

	<script type="text/x-handlebars-template" id="checkout-error-server">
		<div class="global-messages">
			<feedback:message type="error">
				<p><spring:theme code="checkout.error.server" /></p>
			</feedback:message>
		</div>
	</script>
</template:checkoutPage>
