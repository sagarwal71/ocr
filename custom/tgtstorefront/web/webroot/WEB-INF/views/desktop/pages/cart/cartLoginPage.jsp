<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<template:page pageTitle="${pageTitle}">

	<common:globalMessages/>

	<div class="main">

		<h1><spring:theme code="cart.login.heading" text="Login to view your basket" /></h1>

		<ul class="contained login-landing">

			<li class="log-option">

				<%-- Login --%>

				<div class="log-container">

					<div class="log-account">

						<cms:pageSlot var="component" position="LoginLanding">
							<cms:component component="${component}"/>
						</cms:pageSlot>
						<c:url value="/basket/j_spring_security_check" var="url" />
						<user:login action="${url}" />

					</div>

				</div>

			</li>

		</ul>

		<div class="supplement">
			<cms:pageSlot var="component" position="Supplement">
				<cms:component component="${component}"/>
			</cms:pageSlot>
		</div>

	</div>

</template:page>
