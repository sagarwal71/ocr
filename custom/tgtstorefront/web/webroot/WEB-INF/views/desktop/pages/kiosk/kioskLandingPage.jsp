<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/kiosk" %>
<%@ taglib prefix="kiosk" tagdir="/WEB-INF/tags/desktop/kiosk" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header"  %>

<c:set var="kioskMode" value="${true}" scope="request" />

<template:pageKioskLanding pageTitle="${pageTitle}">

	<jsp:body>
		<div class="kiosk-landing">
			<div class="main kiosk-wide">
				<c:set var="contentSlotContext" value="full" scope="request" />
				<cms:pageSlot var="mainComponent" position="Main">
					<cms:component component="${mainComponent}"/>
				</cms:pageSlot>
				<c:remove var="contentSlotContext" scope="request" />
			</div>
			<div class="search-bar">
				<kiosk:signOut />
				<p class="find"><spring:theme code="kiosk.text.landingPage.findProducts" /></p>
				<div class="mini-search">
					<header:search />
				</div>
			</div>
			<div class="supplement kiosk-links">
				<c:set var="kioskLandingSlotContext" value="true" scope="request" />
				<cms:pageSlot var="supplementComponent" position="Supplement">
					<cms:component component="${supplementComponent}"/>
				</cms:pageSlot>
				<c:remove var="kioskLandingSlotContext" scope="request" />
			</div>
		</div>
	</jsp:body>
	
</template:pageKioskLanding>

<c:remove var="kioskMode" scope="request" />