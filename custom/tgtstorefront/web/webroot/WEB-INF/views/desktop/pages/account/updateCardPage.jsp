<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/desktop/account" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/desktop/util" %>

<c:set var="cancelUrl" value="${breadcrumbs[fn:length(breadcrumbs) - 2].url}" />

<account:page pageTitle="${pageTitle}" breadcrumbs="${breadcrumbs}">
    <p>
        <spring:theme code="icon.left-arrow-large"/><a href="${cancelUrl}"><spring:theme code="order.summarycard.myaccount.updateCard.cancel"/></a>
    </p>
    <h4 class="u-arial">
        <strong>
            <spring:theme code="order.summarycard.myaccount.updateCard.header" />
        </strong>
    </h4>
	<div class="summary-detail-info">
		<spring:theme code="icon.info" />
		<p>
			<spring:theme code="order.summarycard.myaccount.updateCard.termsPrefix" />
			<a class="lightbox" href="/modal/terms-conditions">
				<spring:theme code="order.summarycard.myaccount.updateCard.termsConditions" />
			</a>
            <spring:theme code="order.summarycard.myaccount.updateCard.termsConnectLinks" />
			<a class="lightbox" href="/modal/privacy">
                <spring:theme code="order.summarycard.myaccount.updateCard.termsPrivacy" />
            </a>
		</p>
	</div>
    <div class="IPGFrameContainer">
        <div class="IPGFrameLoading">
            <util:loading color="green" inline="${true}" />
        </div>
        <div class="IPGFrameLoaded">
            <iframe class="IPGFrame" src="about:blank" width="100%" height="360px" scrolling="auto" data-ipg-src="${iFrameUrl}" >
            </iframe>
        </div>
    </div>
    <p class="u-alignCenter"><spring:theme code="order.summarycard.myaccount.updateCard.processNotice" /></p>
</account:page>