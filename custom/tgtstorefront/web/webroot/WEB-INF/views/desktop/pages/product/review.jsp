<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<feature:enabled name="uiBazaarvoiceOverwrite">
	<c:set var="bvClass" value="bv-overwrite" />
</feature:enabled>

<template:page pageTitle="${pageTitle}" bodyCssClass="${bvClass}">
	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<div class="main">
		<div id="BVSubmissionContainer" class="bv" data-api="${bazaarVoiceApiUri}" data-base-url="${fullyQualifiedDomainName}${productData.baseProductCanonical.url}" data-task="write"></div>
	</div>
</template:page>
