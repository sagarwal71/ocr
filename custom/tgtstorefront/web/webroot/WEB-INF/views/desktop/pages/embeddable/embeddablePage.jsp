<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>

<template:pageEmbeddable pageTitle="${pageTitle}">
	<jsp:body>
		<div class="main">
			<c:set var="contentSlotContext" value="full" scope="request" />
			<cms:pageSlot var="mainComponent" position="Main">
				<cms:component component="${mainComponent}"/>
			</cms:pageSlot>
			<c:remove var="contentSlotContext" scope="request" />
		</div>
	</jsp:body>

</template:pageEmbeddable>