<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>

<template:page pageTitle="${pageTitle}">
	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<cms:pageSlot var="feature" position="Section1">
		<div class="span-24 section1 cms_banner_slot">
			<cms:component component="${feature}"/>
		</div>
	</cms:pageSlot>
	<div class="span-24 section2">
		<div class="span-4 zone_a cms_banner_slot">
			<cms:pageSlot var="feature" position="Section2A">
				<cms:component component="${feature}"/>
			</cms:pageSlot>
		</div>
		<div class="span-20 zone_b last">
			<cms:pageSlot var="feature" position="Section2B">
				<cms:component component="${feature}"/>
			</cms:pageSlot>
		</div>
	</div>
	<div class="span-24 section3 cms_banner_slot">
		<cms:pageSlot var="feature" position="Section3">
			<cms:component component="${feature}"/>
		</cms:pageSlot>
	</div>
</template:page>