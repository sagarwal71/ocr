<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html><html>
	<body>
		<target:csrfInputToken/>
		<script>
			try {
				if (window !== window.parent) {
					var csrfToken = document.querySelector('input[name=_csrf]').value;
					var port = location.port ? ':' + location.port : '';
					var origin = location.protocol + '//' + location.hostname + port;
					window.parent.postMessage({
						csrfToken: csrfToken,
						fsa: {
							type: "${postMessageActionType}"
						}
					}, origin);
				}
			} catch(e) {
				document.write('Something went wrong (ipg return).');
			}

		</script>
	</body>
</html>
