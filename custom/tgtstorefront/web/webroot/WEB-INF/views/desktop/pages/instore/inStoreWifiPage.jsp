<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/desktop/account" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<template:pagePortal pageTitle="${pageTitle}"  pageHeading="store.wifi.title" bareMinimum="${true}" jsRequired="${true}" disableExternalScripts="${true}" disableSnippets="${true}">

	<div class="main instore-wifi instore-wifi-form" data-subscription-source="wifi" data-subscription-url="/enews/quick">
		<cms:pageSlot var="component" position="Main">
			<cms:component component="${component}"/>
		</cms:pageSlot>
		<div class="instore-wifi-time">
			<p class="time-message"><spring:theme code="icon.clock" /><spring:theme code="store.wifi.time" /></p>
		</div>
		<h4 ><spring:theme code="store.wifi.intro" /></h4>
		<form>
			<div class="f-element">
				<label for="enews-quick-email" class="visuallyhidden">${placeholder}</label>
				<input class="text enews-quick-email" id="enews-quick-email" name="email" type="text" placeholder="${placeholder}" />
			</div>
			<div class="signup-controls">
				<button type="button" class="button-fwd button-submit wide-for-tiny"><spring:theme code='store.wifi.action'/><spring:theme code="icon.right-arrow-small" /></button>
			</div>
		</form>
		<p class="inline-terms"><spring:theme code="store.wifi.terms"/></p>
		<div class="wifi-terms">
			<cms:pageSlot var="component" position="Supplement">
				<cms:component component="${component}"/>
			</cms:pageSlot>
		</div>
		<form method="post" id="wifi-form">
			<input type="hidden" id="buttonClicked" name="buttonClicked" size="16" maxlength="15" value="0">
			<input type="hidden" id="redirect_url" name="redirect_url" size="255" maxlength="255" value="">
			<input type="hidden" name="err_flag" size="16" maxlength="15" value="0">
		</form>
		<script type="text/x-handlebars-template" id="xhr-error-feeback">
			<feedback:message type="error" size="small">
				<p><spring:theme code="xhr.error.occurred" /></p>
			</feedback:message>
		</script>
		<script type="text/x-handlebars-template" id="authentication-error-feeback">
			<feedback:message type="error" size="small">
				<p><spring:theme code="store.wifi.authentication.error" /></p>
			</feedback:message>
		</script>
	</div>

</template:pagePortal>
