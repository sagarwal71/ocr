<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/desktop/account" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>

<account:page pageTitle="${pageTitle}" breadcrumbs="${breadcrumbs}">
	
	<h1 class="heading hfma"><spring:theme code="text.account.myAccount" /></h1>

	<div class="menu-grid-layout">
		<div class="menu-grid-row">
			<div class="menu-item account-personal">
				<h4><a href="${targetUrlsMapping.myAccountPersonalDetails}"><spring:theme code="text.account.myPersonalDetails" /></a></h4>
			</div>
			<div class="menu-item account-order">
				<h4><a href="${targetUrlsMapping.myAccountOrders}"><spring:theme code="text.account.myOrderDetails" /></a></h4>
			</div>
			<div class="menu-item account-payment">
				<h4><a href="${targetUrlsMapping.myAccountPayment}"><spring:theme code="text.account.myPaymentDetails" /></a></h4>
			</div>
			<div class="menu-item account-address">
				<h4><a href="${targetUrlsMapping.myAccountAddressDetails}"><spring:theme code="text.account.myAddressDetails" /></a></h4>
			</div>
		</div>
	</div>

</account:page>
