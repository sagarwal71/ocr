<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="customer" tagdir="/WEB-INF/tags/desktop/customer" %>

<div class="main">
	<customer:confirmAddressForm
		actionUrl="${edit ? targetUrlsMapping.yourAddressEditConfirm : targetUrlsMapping.yourAddressAddConfirm}" 
		deliveryModeCode="${deliveryModeCode}"/>
</div>