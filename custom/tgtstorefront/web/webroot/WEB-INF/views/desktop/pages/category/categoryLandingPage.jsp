<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="category" tagdir="/WEB-INF/tags/desktop/category" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<c:set var="featureNewSubCategoriesUx" value="${false}" />
<template:page pageTitle="${pageTitle}">
	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<common:globalMessages/>
	<category:categoryListerChampionSlot slotName="ListerChampion" searchPageData="${seachPageData}" />
	<div class="inner-content has-aside">
		<div class="aside">
			<feature:enabled name="newSubCategoriesUx">
				<c:set var="featureNewSubCategoriesUx" value="${true}" />
				<category:categoryListerHead extraClass="only-for-small" />
			</feature:enabled>
			<nav:categoryNav pageData="${searchPageData}" disableMultiselect="${true}" />
			<div class="promos hide-for-small">
				<c:set var="contentSlotContext" value="narrow" scope="request" />
				<cms:pageSlot var="leftColumnComponent" position="Aside">
					<cms:component component="${leftColumnComponent}"/>
				</cms:pageSlot>
				<c:remove var="contentSlotContext" scope="request" />
			</div>
		</div>
		<div class="main">
			<category:categoryListerHead extraClass="${featureNewSubCategoriesUx ? 'hide-for-small' : ''}" />
			<c:set var="contentSlotContext" value="wide" scope="request" />
			<cms:pageSlot var="pageContentComponent" position="Main">
				<cms:component component="${pageContentComponent}"/>
			</cms:pageSlot>
			<c:remove var="contentSlotContext" scope="request" />
		</div>
	</div>
</template:page>
