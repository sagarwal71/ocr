<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/desktop/util" %>

<c:set var="isFiltered" value="${false}" />

<feature:disabled name="featureStepShopTheLook">
	<c:set var="title" value="${collectionData.name}" />
	<c:set var="description" value="${collectionData.description}" />
</feature:disabled>
<feature:enabled name="featureStepShopTheLook">
	<c:set var="title" value="${shopTheLookPageData.title}" />
	<c:set var="description" value="${shopTheLookPageData.description}" />
	<c:set var="pageTitle">
		${shopTheLookPageData.title}&nbsp;<spring:theme code="page.title.separator" />&nbsp;<spring:theme code="page.title.suffix" />
	</c:set>
	<c:set var="metaDescription">
		<c:choose>
			<c:when test="${empty shopTheLookPageData.description}">
				${shopTheLookPageData.collections[0].description}
			</c:when>
			<c:otherwise>
				${shopTheLookPageData.description}
			</c:otherwise>
		</c:choose>
	</c:set>
	<c:set var="viewAllData">data-description="${shopTheLookPageData.description}" data-title="&nbsp;"</c:set>
	<c:if test="${fn:length(shopTheLookPageData.collections) gt 1}">
		<c:set var="isFiltered" value="${true}" />
	</c:if>
</feature:enabled>

<template:page pageTitle="${pageTitle}" metaDescription="${metaDescription}">
	<div class="global-messages">
		<common:globalMessages />
	</div>
	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<common:championSlot slotName="ListerChampion" />

	<div class="main">
		<div class="ProductCollection ${isFiltered ? 'ProductCollection--filtered' : ''}">
			<h1 class="ProductCollection-heading">${title}</h1>
			<feature:disabled name="featureStepShopTheLook">
				<c:if test="${not empty collectionData.imageUrl}">
					<img src="${collectionData.imageUrl}" alt="${collectionData.imgAltTxt}" />
				</c:if>
			</feature:disabled>
			<feature:enabled name="featureStepShopTheLook">
				<c:choose>
					<c:when test="${isFiltered}">
						<nav:collectionFilters filters="${shopTheLookPageData.collections}"
							viewAll="${true}" viewAllData="${viewAllData}" viewAllLink="#/all" viewAllName="View All"
							viewAllTotal="${shopTheLookPageData.countOfLooks}" />
						<div class="ProductCollection-subCategory">
							<div class="ProductCollection-results"></div>
							<div class="ProductCollection-title"></div>
						</div>
						<span class="ProductCollection-description"></span>
						<util:loading color="green" className="collection-loader" />
					</c:when>
					<c:otherwise>
						<span class="ProductCollection-description">${description}</span>
					</c:otherwise>
				</c:choose>
			</feature:enabled>
			<feature:disabled name="featureStepShopTheLook">
				<span class="ProductCollection-description">${description}</span>
			</feature:disabled>
			<product:productCollectionListing isFiltered="${isFiltered}" />

			<c:set var="contentSlotContext" value="full" scope="request" />
			<cms:pageSlot var="mainComponent" position="Main">
				<cms:component component="${mainComponent}"/>
			</cms:pageSlot>
			<c:remove var="contentSlotContext" scope="request" />
		</div>
	</div>

	<div class="supplement">
		<c:set var="contentSlotContext" value="full" scope="request" />
		<cms:pageSlot var="supplementComponent" position="Supplement">
			<cms:component component="${supplementComponent}"/>
		</cms:pageSlot>
		<c:remove var="contentSlotContext" scope="request" />
	</div>
</template:page>
