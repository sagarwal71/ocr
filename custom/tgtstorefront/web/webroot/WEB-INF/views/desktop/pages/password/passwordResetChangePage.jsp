<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>

<template:page pageTitle="${pageTitle}">

	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<common:globalMessages/>

	<div class="has-aside content">

		<div class="aside hide-for-small">
			<div class="nav-seg">
				<h3><a href="${targetUrlsMapping.home}"><spring:theme code="text.home" /></a></h3>
			</div>
		</div>

		<div class="main">
			<h1 class="heading"><spring:theme code="text.account.forgottenPasswordUpdate.heading"/></h1>
			<h2 class="subheading"><spring:theme code="text.account.forgottenPasswordUpdate.subheading"/></h2>
			<user:updatePwd cssClass="f-narrow"/>
		</div>

		<div class="supplement">
			<cms:pageSlot var="component" position="Supplement">
				<cms:component component="${component}"/>
			</cms:pageSlot>
		</div>
		

	</div>
</template:page>