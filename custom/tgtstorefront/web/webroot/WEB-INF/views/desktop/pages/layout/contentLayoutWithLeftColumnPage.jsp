<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>


<template:page pageTitle="${pageTitle}">
	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<common:championSlot slotName="ListerChampion" />
	<div class="inner-content has-aside">
		<div class="main">
			<c:set var="contentSlotContext" value="wide" scope="request" />
			<cms:pageSlot var="pageContentComponent" position="PageContent">
				<cms:component component="${pageContentComponent}"/>
			</cms:pageSlot>
			<c:remove var="contentSlotContext" scope="request" />
		</div>
		<div class="aside">
			<nav:leftNav />
			<div class="promos hide-for-small">
				<c:set var="contentSlotContext" value="narrow" scope="request" />
				<cms:pageSlot var="leftColumnComponent" position="LeftColumn">
					<cms:component component="${leftColumnComponent}"/>
				</cms:pageSlot>
				<c:remove var="contentSlotContext" scope="request" />
			</div>


			<%-- This element is gives the left menu height when no components assigned. --%>
			<span>&nbsp;</span>
		</div>
	</div>
</template:page>