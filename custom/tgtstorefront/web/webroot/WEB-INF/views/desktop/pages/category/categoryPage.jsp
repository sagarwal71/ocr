<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>

<template:page pageTitle="${pageTitle}">
	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<common:globalMessages/>
	<cms:pageSlot var="feature" position="Section1">
		<div class="span-24 section1 cms_banner_slot">
			<cms:component component="${feature}"/>
		</div>
	</cms:pageSlot>
	<div class="span-24">
		<div class="span-4">
			<nav:categoryNav pageData="${searchPageData}"/>
			
			<cms:pageSlot var="feature" position="Section4">
				<div class="section4 small_detail">
					<cms:component component="${feature}"/>
				</div>
			</cms:pageSlot>
		</div>
		<div class="span-20 last">
			<cms:pageSlot var="feature" position="Section2">
				<div class="span-20 section2 cms_banner_slot last">
					<cms:component component="${feature}"/>
				</div>
			</cms:pageSlot>
			<div class="span-20 last">
				<cms:pageSlot var="feature" position="Section3">
					<div class="span-5 section3 cms_banner_slot ${(elementPos%4 == 3) ? 'last' : ''}">
						<cms:component component="${feature}"/>
					</div>
				</cms:pageSlot>
			</div>
		</div>
	</div>
</template:page>