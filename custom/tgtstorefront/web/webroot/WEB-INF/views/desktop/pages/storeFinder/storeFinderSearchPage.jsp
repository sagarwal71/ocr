<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/desktop/store" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>

<template:page pageTitle="${pageTitle}">

	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<common:globalMessages/>

	<div class="main">

		<store:storeSearch errorNoResults="${errorNoResults}"/>
		<store:storesMap storeSearchPageData="${storeSearchPageData}" />
		<store:storeListForm storeSearchPageData="${storeSearchPageData}" locationQuery="${locationQuery}" showMoreUrl="${showMoreUrl}"/>

	</div>

</template:page>