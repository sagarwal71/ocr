<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/desktop/checkout" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<c:set var="hasDigital" value="${fn:length(cartData.digitalDeliveryModes) gt 0}" />
<c:set var="hasPhysical" value="${fn:length(cartData.deliveryModes) gt 0}" />

<%-- to be replaced to tgt:tag --%>
<cms:pageSlot var="component" position="Supplement">
	<c:set var="hasVisibleComponentsSupplement" value="${true}" />
</cms:pageSlot>

<template:checkoutPage stepName="yourAddress" hideHome="true" pageTitle="${pageTitle}">

	<common:globalMessages/>
	<cart:sohMessage />

	<div class="co-page co-status-init">
		<div class="main">

			<multi-checkout:pageLoadingIndicator />

			<div class="co-interactive">

				<%-- Digital Section --%>
				<c:if test="${hasDigital}">

					<h2 class="co-heading"><spring:theme code="checkout.multi.giftcarddelivery.heading" /></h2>

					<cart:digitalDeliveryInfoMessage cartData="${cartData}" mixedMode="${hasDigital and hasPhysical}" />

				</c:if>

				<%-- Physical Section --%>
				<c:if test="${hasPhysical}">

					<h2 class="${not hasDigital ? 'co-heading' : ''}"><spring:theme code="checkout.multi.delivery.heading" /></h2>

					<div class="del-modes" data-change-mode-publish="/del-mode/change">

						<c:forEach items="${availableDeliveryModes}" var="delMode">

							<c:if test="${not delMode.deliverToStores}">
								<c:choose>
									<c:when test="${not empty delMode.deliveryAddresses}">
										<c:set var="dataAttr">data-continue-location="${targetUrlsMapping.yourAddressContinue}?deliveryModeCode=${delMode.deliveryModeCode}"</c:set>
									</c:when>
									<c:otherwise>
										<c:set var="dataAttr">data-continue-location="${targetUrlsMapping.yourAddressAdd}?deliveryModeCode=${delMode.deliveryModeCode}"</c:set>
									</c:otherwise>
								</c:choose>
							</c:if>
							<c:if test="${delMode.deliverToStores}">
								<c:set var="dataAttr">data-continue-form="cnc-delivery-form"</c:set>
							</c:if>

							<div class="co-groupe del-mode del-mode-${delMode.deliveryModeCode} ${delMode.available ? ' del-avail' : ' del-unavail'} ${delMode.selected ? ' del-mode-active' : ' '}" data-mode-type="${delMode.deliveryModeCode}" ${dataAttr}>
								<div class="del-summary ${delMode.available ? 'item-selectable' : ''}">
									<c:if test="${delMode.available}">
										<input type="radio" name="delMode" value="${delMode.deliveryModeCode}" class="radio list-item-radio" ${delMode.selected ? 'checked="checked"' : ''} />
									</c:if>
									<h3>
										<spring:theme code="checkout.multi.delivery.modes.${delMode.deliveryModeCode}" />
										<c:if test="${delMode.available and not empty delMode.fee}">
											- <strong><format:price priceData="${delMode.fee}" displayFreeForZero="true"/></strong>
										</c:if>
									</h3>
									<c:if test="${delMode.available and not empty delMode.message}">
										<p>${delMode.message}</p>
									</c:if>
									<c:if test="${not empty delMode.disclaimer}">
										<h5 class="del-alert">${delMode.disclaimer}</h5>
									</c:if>
									<c:if test="${not delMode.available}">
										<c:choose>
											<c:when test="${delMode.postCodeNotSupported}">
												<spring:theme code="checkout.multi.delivery.modes.${delMode.deliveryModeCode}" var="deliveryName" />
												<h5 class="del-alert"><spring:theme code="checkout.multi.delivery.modes.location.disabled" arguments="${deliveryName}" /></h5>
											</c:when>
											<c:otherwise>
												<h5 class="del-alert"><spring:theme code="checkout.multi.delivery.modes.disabled" /></h5>
											</c:otherwise>
										</c:choose>
									</c:if>
								</div>
								<c:if test="${delMode.available and (not empty delMode.deliveryAddresses or delMode.deliverToStores)}">
									<div class="del-detail">

											<%-- IF the delivery mode has deliveryAddresses then render them
											 as existing addresses list, provide a new address button --%>
											<c:if test="${not empty delMode.deliveryAddresses}">
												<multi-checkout:deliveryAddressSelector deliveryAddresses="${delMode.deliveryAddresses}" selectedAddressId="${delMode.selectedDeliveryAddressId}" deliveryModeCode="${delMode.deliveryModeCode}" />

												<c:if test="${not isGuestCheckout}">
													<a class="button-norm wide-for-small" href="${targetUrlsMapping.yourAddressAdd}?deliveryModeCode=${delMode.deliveryModeCode}">
														<spring:theme code="checkout.multi.deliveryAddress.addAddress" text="Add new address"/>
													</a>
												</c:if>
											</c:if>

											<%-- IF the delivery mode has storeAddresses then render the store list
											 and the minimalist cnc form to go with it. --%>
											<c:if test="${delMode.deliverToStores}">
											    <multi-checkout:deliveryClickAndCollect selectedStore="${delMode.selectedStore}" deliveryModeCode="${delMode.deliveryModeCode}" />
											</c:if>

									</div>

								</c:if>
							</div>
						</c:forEach>

					</div>

				</c:if>

			</div>

		</div>

		<div class="aside">
			<multi-checkout:checkoutOrderDetails cartData="${cartData}" />
		</div>

		<div class="co-controls">
			<c:choose>
				<c:when test="${hasPhysical}">
					<multi-checkout:nextActions
						backCode="checkout.multi.deliveryMethod.backToShoppingBasket"
						backUrl="${targetUrlsMapping.cart}"
						nextCode="checkout.multi.deliveryMethod.continue"
						 />
				</c:when>
				<c:otherwise>
					<multi-checkout:nextActions
						backCode="checkout.multi.deliveryMethod.backToShoppingBasket"
						backUrl="${targetUrlsMapping.cart}"
						nextUrl="${targetUrlsMapping.payment}"
						nextCode="checkout.multi.deliveryMethod.continue"
						 />
				</c:otherwise>
			</c:choose>
		</div>

		<c:if test="${hasVisibleComponentsSupplement}">
			<div class="supplement">
			<c:set var="contentSlotContext" value="full" scope="request" />
			<cms:pageSlot var="supplementComponent" position="Supplement">
				<cms:component component="${supplementComponent}"/>
			</cms:pageSlot>
			<c:remove var="contentSlotContext" scope="request" />
			</div>
		</c:if>
	</div>

	<%-- Hidden form for additonal hidden inputs used in the ajax POST --%>
	<form:form id="updateDeliveryModeForm" command="clickAndCollectDetailsForm" action=".">
		<target:csrfInputToken/>
	</form:form>


</template:checkoutPage>
