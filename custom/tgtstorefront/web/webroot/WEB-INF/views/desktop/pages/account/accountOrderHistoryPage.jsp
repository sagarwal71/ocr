<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/desktop/account" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>

<account:page pageTitle="${pageTitle}" breadcrumbs="${breadcrumbs}">

	<h1 class="heading hfma"><spring:theme code="text.account.myOrders" /></h1>	
	<p><spring:theme code="text.account.myOrders.instruction" /></p>

	<c:if test="${empty searchPageData.results}">
		<p><spring:theme code="text.account.myOrders.noOrders" /></p>
	</c:if>

	<c:if test="${not empty searchPageData.results}">
		<table class="normal-table order-history-table">
			<thead>
				<tr>
					<th id="header1"><spring:theme code="text.account.myOrders.orderNumber" text="Order Number"/></th>
					<th id="header2" class="hide-for-small"><spring:theme code="text.account.myOrders.dateCreated" text="Date Created"/></th>
					<th id="header3"><spring:theme code="text.account.myOrders.status" text="Status"/></th>
					<th id="header4"><spring:theme code="text.account.myOrders.totals" text="Totals"/></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${searchPageData.results}" var="order" varStatus="status">
					<tr class="${status.index mod 2 eq 0 ? 'even' : 'odd'}">
						<td headers="header1" class="tight">
							<a href="${targetUrlsMapping.myAccountOrderDetails}${order.code}" title="View order ${order.code}">${order.code}</a>
							<span class="only-for-small">
								<format:formatShortDate date="${order.placed}" />
							</span>
						</td>
						<td headers="header2" class="tight hide-for-small">
							<format:formatDateAndTime date="${order.placed}" />
						</td>
						<td headers="header3">
							<c:choose>
								<c:when test="${order.status eq 'PARKED'}">
									<spring:theme code="text.account.myOrders.status.parked" text="Status"/>
								</c:when>
								<c:otherwise>
									${order.statusDisplay}
								</c:otherwise>
							</c:choose>
						</td>
						<td headers="header4">
							<format:price priceData="${order.total}"/>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

		<nav:pagination searchPageData="${searchPageData}" searchUrl="${targetUrlsMapping.myAccountOrders}" />

	</c:if>

</account:page>
