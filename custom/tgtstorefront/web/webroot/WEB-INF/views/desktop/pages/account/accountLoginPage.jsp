<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<template:page pageTitle="${pageTitle}">

	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>


	<common:globalMessages/>

	<div class="contained login-landing">

		<div class="log-option">

			<div class="log-container">

				<div class="log-account">
					<cms:pageSlot var="component" position="LoginLanding">
						<cms:component component="${component}"/>
					</cms:pageSlot>
					<c:url value="/j_spring_security_check" var="loginActionUrl" />
					<user:login action="${loginActionUrl}" />
				</div>
			</div>
		</div>

		<div class="log-option">

			<div class="log-container">

				<div class="log-register">
					<c:url value="/login/register" var="registerActionUrl" />
					<user:register action="${registerActionUrl}"/>
				</div>

			</div>

		</div>

	</div>

	<div class="supplement">
		<cms:pageSlot var="component" position="Supplement">
			<cms:component component="${component}"/>
		</cms:pageSlot>
	</div>

</template:page>
