
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>

<div class="main">
	<div class="size-chart-container">
		<div class="view-all-chart">
			<c:url value="/size-chart/viewallsize" var="viewAllSizeUrl" />
			<c:set var="viewAllSizeCharts"><spring:theme code="size.page.viewAllSizeCharts"/></c:set>
				<c:if test="${sizeTypeCode ne 'viewallsize'}">
					<a class="lightbox size-chart-link" data-iw="600" data-lightbox-type="content" title="${viewAllSizeCharts}" href="${viewAllSizeUrl}">${viewAllSizeCharts}</a>
				</c:if>
		</div>
		<h1 class="size-chart-heading">${cmsPage.title}</h1>
		<c:if test="${not empty navigation}">
			<div class="size-chart-nav">
				<h3>${navigation.name}</h3>
				<ul class="size-node-list">
					<c:set var="sizeTypeCodeWithSlash">/${sizeTypeCode}</c:set>
					<c:forEach items="${navigation.children}" var="navNode">
						<c:url value="${not empty navNode.link ? navNode.link : ''}" var="navNodeLink" />
						<li>
							<a class="lightbox size-chart-link ${fn:endsWith(navNode.link,sizeTypeCodeWithSlash) ? 'active' : '' }" href="${navNodeLink}" title="${navNode.name}" data-iw="600" data-lightbox-type="content">${navNode.name}</a>
						</li>
					</c:forEach>
				</ul>
			</div>
		</c:if>
		<div class="size-chart-content">
			<c:set var="contentSlotContext" value="full" scope="request" />
			<cms:pageSlot var="mainComponent" position="Main">
				<cms:component component="${mainComponent}"/>
			</cms:pageSlot>
			<c:remove var="contentSlotContext" scope="request" />
		</div>
	</div>
</div>
<common:autoPrintScript />