<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/desktop/checkout" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<template:checkoutPage stepName="reviewYourOrder" hideHome="true" pageTitle="${pageTitle}">

	<common:globalMessages/>
	<cart:sohMessage />

	<c:set var="actionUrl" value="${targetUrlsMapping.placeOrder}" />
	<c:set var="cssClass"  value="${kioskMode ? 'pin-pad-payment' : '' }" />
	<c:set var="displayContinue" value="${kioskMode or (not empty cartData and not empty cartData.paymentInfo and not empty cartData.paymentInfo.cardTypeData)}" />

	<div class="co-page">
		<form:form action="${actionUrl}" cssClass="${cssClass}" commandName="placeOrderForm" data-order-number="${cartData.code}" data-amount="${cartData.totalPrice.formattedValue}">
			<div class="main">
				<c:set var="modeCode" value="${featuresEnabled.featureIPGEnabled or kioskMode ? '' : '.earlyPay'}" />
				<h2 class="co-heading"><spring:theme code="checkout.multi.reviewYourOrder.heading${modeCode}" /></h2>
				<div class="review-groupe">
					<multi-checkout:reviewYourOrder />

					<c:if test="${featuresEnabled.featureIPGEnabled}">
						<c:if test="${not kioskMode and not empty IPGIframeUrl}">
							<h3><spring:theme code="checkout.multi.reviewYourOrder.ipg.payment" /></h3>
							<div class="IPGContainer">
								<c:choose>
									<c:when test="${giftCardPaymentMode}">
										<c:set var="IframeClass" value="IPGGiftCard" />
									</c:when>
									<c:otherwise>
										<c:set var="IframeClass" value="IPGNewCard" />
									</c:otherwise>
								</c:choose>
								<iframe class="IPGFrame ${IframeClass}" src="about:blank" width="100%" scrolling="auto" data-ipg-src="${IPGIframeUrl}"></iframe>
							</div>
						</c:if>
					</c:if>
				</div>
			</div>

			<div class="aside">
				<multi-checkout:checkoutOrderDetails cartData="${cartData}" />
			</div>

			<div class="co-controls">
				<div class="terms hfk">
					<multi-checkout:termsCheck />
				</div>
				<multi-checkout:nextActions
					backCode="checkout.multi.reviewYourOrder.backTo${modeCode}"
					backUrl="${targetUrlsMapping.payment}"
					nextCode="${displayContinue ? 'checkout.multi.reviewYourOrder.continue' : ''}">
				</multi-checkout:nextActions>
			</div>
			<input type="hidden" name="${tokenName}" value="${tokenValue}" />
			<target:csrfInputToken/>
		</form:form>
	</div>
</template:checkoutPage>
