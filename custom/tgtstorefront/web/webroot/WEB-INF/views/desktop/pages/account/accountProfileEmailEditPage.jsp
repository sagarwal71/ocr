<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/desktop/account" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<account:page pageTitle="${pageTitle}" breadcrumbs="${breadcrumbs}">
	<h1 class="heading hfma"><spring:theme code="text.account.changeEmail.heading"/></h1>
	<p class="required"><spring:theme code="form.mandatory.message"/></p>
	<form:form action="${targetUrlsMapping.myAccountUpdateEmail}" method="post" commandName="updateEmailForm" cssClass="single-action f-narrow" novalidate="true">
		<formElement:formInputBox idKey="profile.email" labelKey="text.account.changeEmail.newEmail" path="email" inputCSS="text" mandatory="true" type="email"/>
		<formElement:formInputBox idKey="profile.checkEmail"  labelKey="text.account.changeEmail.confirmNewEmail" path="chkEmail" inputCSS="text" mandatory="true" errorPath="updateEmailForm" type="email"/>
		<formElement:formPasswordBox idKey="profile.pwd" labelKey="profile.pwd" path="password" inputCSS="text" mandatory="true"/>
		<div class="f-buttons">
			<button class="button-fwd button-single"><spring:theme code="text.account.profile.saveUpdates" text="Save Updates"/></button>
		</div>
		<target:csrfInputToken/>
	</form:form>
</account:page>
