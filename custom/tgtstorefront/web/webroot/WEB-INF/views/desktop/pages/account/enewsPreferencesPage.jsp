<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/desktop/account" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<template:page pageTitle="${pageTitle}" disableFooterEnews="${true}">

	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<common:globalMessages />
		
		<div class="main">

			<c:set var="contentSlotContext" value="full" scope="request" />
			<cms:pageSlot var="component" position="Main">
				<cms:component component="${component}"/>
			</cms:pageSlot>
			<c:remove var="contentSlotContext" scope="request" />

			<c:set var="contentSlotContext" value="full" scope="request" />
			<cms:pageSlot var="component" position="Alternative">
				<cms:component component="${component}"/>
			</cms:pageSlot>
			<c:remove var="contentSlotContext" scope="request" />

		</div>

		<div class="supplement">
			<c:set var="contentSlotContext" value="full" scope="request" />
			<cms:pageSlot var="component" position="Supplement">
				<cms:component component="${component}"/>
			</cms:pageSlot>
			<c:remove var="contentSlotContext" scope="request" />
		</div>

</template:page>