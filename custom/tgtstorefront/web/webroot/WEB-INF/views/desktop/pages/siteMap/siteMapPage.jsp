<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<template:page pageTitle="${pageTitle}">
	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
		<div class="main">
			<h1><spring:theme code="sitemap.page.h1" /></h1>
			<target:megaMenuCache cacheKey="sitemap.navigation">
				<div class="contained">
					<div class="half-width nav-seg">
						<ul class="root-nav">
							<nav:navTree collection="${siteMapStructureMap['categories']}" depth="${0}" />
						</ul>
					</div>
					<div class="half-width nav-seg">
						<ul class="root-nav">
							<nav:navTree collection="${siteMapStructureMap['content']}" depth="${0}"/>
						</ul>
					</div>
				</div>
			</target:megaMenuCache>
		</div>
</template:page>
