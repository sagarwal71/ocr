<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>


<template:page pageTitle="${pageTitle}">
	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<common:globalMessages/>
	<c:choose>
		<c:when test="${empty searchPageData.errorStatus}">
			<div class="main">
				<nav:searchHeading searchPageData="${searchPageData}" />
			</div>
			<c:set var="contentSlotContext" value="full" scope="request" />
			<div class="pillar">
				<cms:pageSlot var="mainComponent" position="Main" >
					<cms:component component="${mainComponent}"/>
				</cms:pageSlot>
			</div>
			<div class="pillar">
				<cms:pageSlot var="middleMainComponent" position="MiddleMain">
					<cms:component component="${middleMainComponent}"/>
				</cms:pageSlot>
			</div>
			<div class="pillar">
				<cms:pageSlot var="rightMainComponent" position="RightMain">
					<cms:component component="${rightMainComponent}"/>
				</cms:pageSlot>
			</div>
			<c:remove var="contentSlotContext" scope="request" />
		</c:when>
		<c:otherwise>
			<div class="main">
				<div class="search-lead results-problem" data-results="Search" data-problem="Error">
					<h3>
						<span class="tiny-break"><spring:theme code="search.products.error" /></span>
						<%-- RFC 2396 4.2 A URI reference that does not contain a URI is a reference to the current document. --%>
						<span class="tiny-break"><a href=""><spring:theme code="search.products.tryagain" /></a></span>
					</h3>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
</template:page>
