<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<c:set var="fragmentComponents">
	<cms:pageSlot var="mainComponent" position="Main">
		<cms:component component="${mainComponent}"/>
	</cms:pageSlot>
</c:set>

<c:if test="${not empty fragmentComponents}">
	<div class="PersonaliseSegment-components">
		${fragmentComponents}
	</div>
</c:if>
