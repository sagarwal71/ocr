<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>

<template:checkoutPage stepName="thankYou" stepsFinished="true" hideHome="false" pageTitle="${pageTitle}">

	<common:globalMessages/>
	<multi-checkout:signOutAfterOrder />

	<div class="co-page">
		<c:choose>
			<c:when test="${paidValueMatchesCartTotal}">
				<div class="supplement">
					<h2 class="co-heading">
						<spring:theme code="checkout.multi.thankYou.check.heading" />
					</h2>
				</div>
				<div class="supplement">
					<spring:theme code="checkout.multi.thankYou.check.cartnumber" arguments="${cartNumber}" var="cartNumberMessage" />
					<c:set var="content">
						<cms:pageSlot var="supplementComponent" position="Supplement">
							<cms:component component="${supplementComponent}"/>
						</cms:pageSlot>
					</c:set>
					<c:out value="${fn:replace(content, '{{cartNumberMessage}}', (not empty cartNumber ? cartNumberMessage : ''))}"  escapeXml="false"  />
				</div>
			</c:when>
			<c:otherwise>
				<div class="supplement">
					<p>
						<spring:theme code="checkout.multi.thankYou.check.error.message"/>
						<c:if test="${not empty cartNumber }">
							<spring:theme code="checkout.multi.thankYou.check.cartnumber" arguments="${cartNumber}" />
						</c:if>
					</p>	
				</div>
			</c:otherwise>
		</c:choose>		

		<div class="co-controls">
			<multi-checkout:nextActions
				endUrl="${targetUrlsMapping.home}"
				endCode="checkout.multi.thankYou.continue" />
		</div>

	</div>
</template:checkoutPage>
<%-- Invalidate current user session when there is an issue when converting a cart to an order --%>
<target:invalidateSession/>
