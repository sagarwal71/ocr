<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/desktop/account" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<account:page pageTitle="${pageTitle}" breadcrumbs="${breadcrumbs}">
	<h1 class="heading hfma"><spring:theme code="text.account.changeProfile.heading"/></h1>
	<p class="required"><spring:theme code="form.mandatory.message"/></p>
	<form:form action="${targetUrlsMapping.myAccountUpdatePersonalDetails}" method="post" commandName="updateProfileForm" cssClass="single-action f-narrow">
		<formElement:formSelectBox idKey="profile.title" labelKey="profile.title" path="titleCode" mandatory="false" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${titleData}"/>
		<formElement:formInputBox idKey="profile.firstName" labelKey="profile.firstName" path="firstName" inputCSS="text" mandatory="true"/>
		<formElement:formInputBox idKey="profile.lastName" labelKey="profile.lastName" path="lastName" inputCSS="text" mandatory="true"/>
		<formElement:formOptCheckbox idKey="profile.allowTracking" labelKey="profile.allowTracking" path="allowTracking" mandatory="false"/>
		<div class="f-buttons">
			<button class="button-fwd button-single"><spring:theme code="text.account.button.saveChanges" /></button>
		</div>	
		<target:csrfInputToken/>
	</form:form>
</account:page>
