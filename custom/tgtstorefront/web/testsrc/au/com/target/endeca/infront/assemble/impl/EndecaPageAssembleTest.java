/**
 * 
 */
package au.com.target.endeca.infront.assemble.impl;

import static au.com.target.endeca.infront.constants.EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAILABLE_ONLINE;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commons.jalo.TransformerException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBException;

import org.apache.commons.collections.map.MultiValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.cache.EndecaDimensionCacheService;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaBreadCrumb;
import au.com.target.endeca.infront.data.EndecaDimension;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.exception.TargetEndecaWrapperException;
import au.com.target.endeca.infront.parser.EndecaDataParser;
import au.com.target.endeca.infront.service.EndecaDimensionService;
import au.com.target.endeca.infront.web.spring.TgtSpringUtility;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;

import com.endeca.infront.assembler.Assembler;
import com.endeca.infront.assembler.AssemblerException;
import com.endeca.infront.assembler.AssemblerFactory;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.MutableDimLocationList;
import com.endeca.soleng.urlformatter.UrlFormatException;
import com.endeca.soleng.urlformatter.seo.SeoNavStateEncoder;


/**
 * Test case to test EndecaPageAssemble class
 * 
 * @author jjayawa1
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EndecaPageAssembleTest {

    /**
     * 
     */
    private static final String AVAILABLE_ONLINE = "Available Online";

    private final EndecaSearchStateData searchStateData = new EndecaSearchStateData();

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpSession session;

    @Mock
    private EndecaDimensionCacheService endecaDimensionCacheService;

    @Mock
    private EndecaDimensionService endecaDimensionService;

    @Mock
    private AssemblerFactory assemblerFactory;

    @Mock
    private Assembler assembler;

    @Mock
    private EndecaDataParser endecaDataParser;

    @Mock
    private TgtSpringUtility springUtility;

    @Mock
    private EndecaSearch searchResult;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @InjectMocks
    @Spy
    private final EndecaPageAssemble endecaPageAssemble = new EndecaPageAssemble();

    private Map<String, String> iosAppRecordFilters;

    @Before
    public void setupTestData()
            throws AssemblerException, JAXBException, javax.xml.transform.TransformerException, IOException {
        final SeoNavStateEncoder navStateEncoder = new SeoNavStateEncoder();
        navStateEncoder.setParamKey("N");

        given(request.getSession()).willReturn(session);
        given(assemblerFactory.createAssembler()).willReturn(assembler);
        given(endecaDataParser.convertSourceToSearchObjects(any(List.class))).willReturn(searchResult);

        iosAppRecordFilters = new HashMap<String, String>();
        iosAppRecordFilters.put("productDisplayFlag", "1");
        iosAppRecordFilters.put("availableOnline", "Available Online");

        endecaPageAssemble.setIosAppRecordFilters(iosAppRecordFilters);
    }

    @Test
    public void testNavigationStateEncoding() throws JAXBException, TransformerException, IOException,
            ENEQueryException, AssemblerException, TargetEndecaException, javax.xml.transform.TransformerException,
            TargetEndecaWrapperException, UrlFormatException {
        given(
                endecaDimensionCacheService.getDimension(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY,
                        "W93742"))
                .willReturn(
                        "2271506221");

        final Map map = new HashMap();
        map.put("category", "W93742");
        given(endecaDimensionService.getDimensionNavigationState(map))
                .willReturn("11ke9f1");
        given(assemblerFactory.createAssembler()).willReturn(assembler);

        endecaPageAssemble.setSpringUtility(springUtility);
        final Map<String, String> dims = new HashMap<String, String>();
        dims.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY, "W93742");
        endecaPageAssemble.assemble(request, EndecaConstants.EndecaPageUris.ENDECA_CATEGORY_PAGE_URI, searchStateData,
                dims);
        assertThat(searchStateData).isNotNull();

        final String encodedNavState = searchStateData.getNavigationState().get(0);

        assertThat(encodedNavState).isNotEqualTo("?N=11ke9f1");
        assertThat(encodedNavState).isEqualTo("11ke9f1");
    }

    @Test
    public void testgetQueryParmasNavigationState() throws JAXBException, TransformerException, IOException,
            ENEQueryException, AssemblerException, TargetEndecaException, javax.xml.transform.TransformerException,
            UrlFormatException {
        given(
                endecaDimensionCacheService.getDimension(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY,
                        "W93742"))
                .willReturn(
                        "2271506221");
        given(endecaDimensionService.getEncodedNavigationState(any(MutableDimLocationList.class)))
                .willReturn("?N=11ke21");

        endecaPageAssemble.setSpringUtility(springUtility);
        final MultiValueMap dims = new MultiValueMap();
        final List<String> params = new ArrayList<>();
        params.add("W93742");
        dims.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY, params);
        final String result = endecaPageAssemble.getQueryParmasNavigationState(request, dims);
        assertThat(result).isNotEqualTo("?N=11ke9f1");
    }

    @Test
    public void testAssembleWithNavStateShouldIgnoreDimensions()
            throws JAXBException, javax.xml.transform.TransformerException, IOException, TargetEndecaWrapperException {
        final Map<String, String> dims = mock(Map.class);
        searchStateData.setNavigationState(Collections.singletonList("?N=1"));
        endecaPageAssemble.assemble(request, EndecaConstants.EndecaPageUris.ENDECA_CATEGORY_PAGE_URI, searchStateData,
                dims);
        verifyZeroInteractions(dims);
    }

    @Test
    public void testAssembleWithSearchText()
            throws JAXBException, javax.xml.transform.TransformerException, IOException, TargetEndecaWrapperException {
        searchStateData.setNavigationState(null);
        searchStateData.setSearchField("Text");
        endecaPageAssemble.assemble(request, EndecaConstants.EndecaPageUris.ENDECA_CATEGORY_PAGE_URI, searchStateData);
        verify(endecaPageAssemble).assemble(request, EndecaConstants.EndecaPageUris.ENDECA_CATEGORY_PAGE_URI,
                searchStateData, null);
    }

    @Test
    public void testAssembleWithNoDimNoNavState()
            throws JAXBException, javax.xml.transform.TransformerException, IOException, TargetEndecaWrapperException {
        searchStateData.setNavigationState(null);
        endecaPageAssemble.assemble(request, EndecaConstants.EndecaPageUris.ENDECA_CATEGORY_PAGE_URI, searchStateData,
                null);
        verifyZeroInteractions(endecaDimensionCacheService);
    }

    @Test
    public void testApplyAvailableOnlineDuringAssemble()
            throws JAXBException, javax.xml.transform.TransformerException, IOException, TargetEndecaWrapperException {
        searchStateData.setNavigationState(Collections.singletonList("?N=1"));
        given(session.getAttribute(ENDECA_AVAILABLE_ONLINE))
                .willReturn(AVAILABLE_ONLINE);
        endecaPageAssemble.assemble(request, EndecaConstants.EndecaPageUris.ENDECA_CATEGORY_PAGE_URI, searchStateData,
                null);
        verify(endecaPageAssemble).applyAvailableOnlinePreference(request, searchStateData, null);
        verify(endecaPageAssemble).setFacetPreference(request, searchResult);
    }

    @Test
    public void testShouldNotApplyAvailableOnlinePreferenceWhenPageSizeInRequestParam() {
        given(request.getParameter(EndecaConstants.PAGE_SIZE_KEY)).willReturn("30");
        assertThat(endecaPageAssemble.shouldApplyAvailableOnlinePreference(request)).isFalse();
    }

    @Test
    public void testShouldApplyAvailableOnlinePreferenceWhenPageSizeNotInRequestParam() {
        given(request.getParameter(EndecaConstants.PAGE_SIZE_KEY)).willReturn(null);
        assertThat(endecaPageAssemble.shouldApplyAvailableOnlinePreference(request)).isTrue();
    }

    @Test
    public void testSaveAvailableOnlinePreferenceWithNoResult() {
        endecaPageAssemble.setFacetPreference(request, null);
        verifyZeroInteractions(request);
    }

    @Test
    public void testSaveAvailableOnlinePreference() throws ENEQueryException, TargetEndecaException {
        final EndecaDimension endecaDimension = new EndecaDimension();
        endecaDimension.setDimensionName(ENDECA_AVAILABLE_ONLINE);
        endecaDimension.setSelected(true);
        given(searchResult.getDimension()).willReturn(Collections.singletonList(endecaDimension));
        given(endecaDimensionCacheService.getDimension(ENDECA_AVAILABLE_ONLINE, "Available Online"))
                .willReturn("15");
        final Map map = new HashMap();
        map.put("availableOnline", "Available Online");
        given(endecaDimensionService.getDimensionNavigationState(map))
                .willReturn("11ke9f1");
        given(endecaDimensionService.getDimensionNavigationState(map))
                .willReturn("f");
        endecaPageAssemble.setFacetPreference(request, searchResult);
        verify(session).setAttribute(ENDECA_AVAILABLE_ONLINE,
                "f");
    }

    @Test
    public void testRemoveAvailableOnlinePreferenceWithNoBreadCrumb() {
        given(searchResult.getBreadCrumb()).willReturn(null);
        endecaPageAssemble.setFacetPreference(request, searchResult);
        verify(session).removeAttribute(ENDECA_AVAILABLE_ONLINE);
    }

    @Test
    public void testRemoveAvailableOnlinePreferenceWithNoMatchingBreadCrumb() {
        final EndecaBreadCrumb endecaBreadCrumb = new EndecaBreadCrumb();
        endecaBreadCrumb.setDimensionType(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY);
        endecaBreadCrumb.setElement(AVAILABLE_ONLINE);
        given(searchResult.getBreadCrumb()).willReturn(Collections.singletonList(endecaBreadCrumb));
        endecaPageAssemble.setFacetPreference(request, searchResult);
        verify(session).removeAttribute(ENDECA_AVAILABLE_ONLINE);
    }

    @Test
    public void testApplyAvailableOnlinePreferenceWithoutNavStateOrDim() {
        searchStateData.setNavigationState(null);
        given(session.getAttribute(ENDECA_AVAILABLE_ONLINE))
                .willReturn("17");
        doReturn("17").when(endecaPageAssemble).getSingleDimensionNavState(ENDECA_AVAILABLE_ONLINE,
                AVAILABLE_ONLINE);
        final String dimState = endecaPageAssemble.applyAvailableOnlinePreference(request, searchStateData, null);
        assertThat(dimState).isEqualTo("17");
    }

    @Test
    public void testApplyAvailableOnlinePreferenceWithDim() {
        searchStateData.setNavigationState(null);
        given(session.getAttribute(ENDECA_AVAILABLE_ONLINE))
                .willReturn("17");
        doReturn("17").when(endecaPageAssemble).getSingleDimensionNavState(ENDECA_AVAILABLE_ONLINE,
                AVAILABLE_ONLINE);
        final String dimState = endecaPageAssemble.applyAvailableOnlinePreference(request, searchStateData, "26q");
        assertThat(dimState).isEqualTo("26qZ17");
    }

    @Test
    public void testApplyAvailableOnlinePreferenceWithNavigationState() {
        searchStateData.setNavigationState(Collections.singletonList("?N=1"));
        given(session.getAttribute(ENDECA_AVAILABLE_ONLINE))
                .willReturn("17");
        doReturn("17").when(endecaPageAssemble).getSingleDimensionNavState(ENDECA_AVAILABLE_ONLINE,
                "17");
        final String dimState = endecaPageAssemble.applyAvailableOnlinePreference(request, searchStateData, null);
        assertThat(dimState).isNull();
        assertThat(searchStateData.getNavigationState()).containsExactly("?N=1Z17");
    }

    @Test
    public void testApplyAvailableOnlinePreferenceWithAlreadyNavigationState() {
        searchStateData.setNavigationState(Collections.singletonList("?N=17Z3434"));
        given(session.getAttribute(ENDECA_AVAILABLE_ONLINE))
                .willReturn("17");
        doReturn("17").when(endecaPageAssemble).getSingleDimensionNavState(ENDECA_AVAILABLE_ONLINE,
                AVAILABLE_ONLINE);
        final String dimState = endecaPageAssemble.applyAvailableOnlinePreference(request, searchStateData, null);
        assertThat(dimState).isNull();
        assertThat(searchStateData.getNavigationState()).containsExactly("?N=17Z3434");
    }

    /**
     * Tests skipping persisting the availableOnline facet with null dim
     */
    @Test
    public void testApplyAvailableOnlinePreferenceWithAlreadyNavigationStateAndSkipPersist() {
        searchStateData.setNavigationState(Collections.singletonList("?N=17Z3434"));
        given(session.getAttribute(ENDECA_AVAILABLE_ONLINE))
                .willReturn("17");
        given(endecaPageAssemble.getSingleDimensionNavState(ENDECA_AVAILABLE_ONLINE, AVAILABLE_ONLINE))
                .willReturn("17");
        searchStateData.setPersistAvailableOnlineFacet(false);
        final String dimState = endecaPageAssemble.applyAvailableOnlinePreference(request, searchStateData, null);
        assertThat(dimState).isNull();
        assertThat(searchStateData.getNavigationState()).containsExactly("?N=17Z3434");
    }


    /**
     * Tests skipping persisting the availableOnline facet with dim value
     */
    @Test
    public void testApplyAvailableOnlinePreferenceWithDimSkipPersistFeatureOn() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isFindInStoreStockVisibilityEnabled();
        searchStateData.setNavigationState(null);
        given(session.getAttribute(ENDECA_AVAILABLE_ONLINE))
                .willReturn("17");
        given(endecaPageAssemble.getSingleDimensionNavState(ENDECA_AVAILABLE_ONLINE, AVAILABLE_ONLINE))
                .willReturn("17");
        searchStateData.setPersistAvailableOnlineFacet(false);
        final String dimState = endecaPageAssemble.applyAvailableOnlinePreference(request, searchStateData, "26q");
        assertThat(dimState).isEqualTo("26q");
    }

    /**
     * Tests skipping persisting the availableOnline facet with dim value,with feature switch off
     */
    @Test
    public void testApplyAvailableOnlinePreferenceWithDimSkipPersistFeatureOff() {
        searchStateData.setNavigationState(null);
        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isFindInStoreStockVisibilityEnabled();
        given(session.getAttribute(ENDECA_AVAILABLE_ONLINE))
                .willReturn("17");
        given(endecaPageAssemble.getSingleDimensionNavState(ENDECA_AVAILABLE_ONLINE, AVAILABLE_ONLINE))
                .willReturn("17");
        searchStateData.setPersistAvailableOnlineFacet(false);
        final String dimState = endecaPageAssemble.applyAvailableOnlinePreference(request, searchStateData, "26q");
        assertThat(dimState).isEqualTo("26qZ17");
    }

    @Test
    public void testApplyAvailableOnlinePreferenceWithNprr() {
        given(request.getParameter(EndecaConstants.PAGE_SIZE_KEY)).willReturn("30");
        endecaPageAssemble.applyAvailableOnlinePreference(request, searchStateData, null);
        verify(endecaPageAssemble, times(0)).getSingleDimensionNavState(ENDECA_AVAILABLE_ONLINE,
                AVAILABLE_ONLINE);
    }

}
