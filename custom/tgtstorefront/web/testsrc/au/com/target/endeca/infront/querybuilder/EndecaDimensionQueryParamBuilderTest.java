/**
 * 
 */
package au.com.target.endeca.infront.querybuilder;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;

import org.apache.commons.collections.map.MultiValueMap;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;

import au.com.target.endeca.infront.assemble.impl.EndecaPageAssemble;
import au.com.target.tgtstorefront.util.SearchQuerySanitiser;


/**
 * @author smudumba
 * 
 */
public class EndecaDimensionQueryParamBuilderTest {

    @Mock
    private SearchQuerySanitiser searchQuerySanitiser;

    @Mock
    private EndecaPageAssemble endecaPageAssemble;

    @Test
    public void testGetEndecaQueryString() {

        final EndecaDimensionQueryParamBuilder endecaParamBuilder = new EndecaDimensionQueryParamBuilder();
        final HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        final SearchQuerySanitiser mockSqs = Mockito.mock(SearchQuerySanitiser.class);
        BDDMockito.given(mockSqs.sanitiseSearchText(Mockito.anyString())).willReturn("%3Alatest%3Acategory%3AW128450");
        final MultiValueMap dims = new MultiValueMap();
        final List list = new ArrayList();
        list.add("W128450");
        dims.put("category", list);
        final EndecaPageAssemble mockEpa = Mockito.mock(EndecaPageAssemble.class);
        BDDMockito.given(mockEpa.getQueryParmasNavigationState(request, dims)).willReturn("27ph");
        final String expectedValue = "N=27ph";
        endecaParamBuilder.setEndecaPageAssemble(mockEpa);
        endecaParamBuilder.setSearchQuerySanitiser(mockSqs);
        final String result = endecaParamBuilder.getEndecaQueryString("%3Alatest%3Acategory%3AW128450", "category",
                "W128450",
                request);

        Assert.assertEquals(result, expectedValue);

    }

}
