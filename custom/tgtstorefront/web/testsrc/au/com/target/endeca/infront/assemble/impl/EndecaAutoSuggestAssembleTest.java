/**
 * 
 */
package au.com.target.endeca.infront.assemble.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.endeca.infront.assembler.Assembler;
import com.endeca.infront.assembler.AssemblerException;
import com.endeca.infront.assembler.AssemblerFactory;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.ContentSlotConfig;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaWrapperException;
import au.com.target.endeca.infront.web.spring.TgtSpringUtility;


/**
 * @author bhuang3
 *
 */
public class EndecaAutoSuggestAssembleTest {

    @Mock
    private AssemblerFactory assemblerFactory;


    @Mock
    private TgtSpringUtility springUtility;

    @InjectMocks
    private final EndecaAutoSuggestAssemble endecaAssemble = new EndecaAutoSuggestAssemble();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAssemble() throws AssemblerException, TargetEndecaWrapperException, JAXBException,
            TransformerException, IOException {
        final EndecaSearchStateData endecaSearchStateData = Mockito.mock(EndecaSearchStateData.class);
        final HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        final Assembler assembler = Mockito.mock(Assembler.class);
        given(request.getRequestURI()).willReturn("/search");
        given(endecaSearchStateData.getSearchTerm()).willReturn(Arrays.asList("red dress"));
        given(assemblerFactory.createAssembler()).willReturn(assembler);
        final ContentSlotConfig contentSlotConfig = new ContentSlotConfig();
        final List<String> contentPaths = new ArrayList<>();
        contentPaths.add(EndecaConstants.EndecaPageUris.ENDECA_AUTOSUGGEST_SEARCH_CONTENT_PATH);
        contentSlotConfig.setContentPaths(contentPaths);
        final List<String> templateTypes = new ArrayList<>();
        templateTypes.add(EndecaConstants.EndecaPageUris.ENDECA_AUTOSUGGEST_SEARCH_TEMPLATE_TYPE);
        contentSlotConfig.setTemplateTypes(templateTypes);
        contentSlotConfig.setRuleLimit(3);
        final ContentItem responseContentItem = Mockito.mock(ContentItem.class);
        given(assembler.assemble(contentSlotConfig)).willReturn(responseContentItem);
        endecaAssemble.setSpringUtility(springUtility);

        final ContentItem result = endecaAssemble.assemble(request, endecaSearchStateData);
        assertThat(result.hashCode()).isEqualTo(responseContentItem.hashCode());

    }
}
