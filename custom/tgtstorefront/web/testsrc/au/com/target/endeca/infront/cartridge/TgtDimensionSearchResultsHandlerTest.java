/**
 * 
 */
package au.com.target.endeca.infront.cartridge;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.tgtfacades.url.impl.TargetBrandDimensionSearchUrlResolver;
import au.com.target.tgtfacades.url.impl.TargetCategoryDimensionSearchUrlResolver;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.DimensionSearchResults;
import com.endeca.infront.cartridge.DimensionSearchResultsConfig;
import com.endeca.infront.cartridge.model.DimensionSearchGroup;
import com.endeca.infront.cartridge.model.DimensionSearchValue;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TgtDimensionSearchResultsHandlerTest {

    @Mock
    private TargetCategoryDimensionSearchUrlResolver targetCategoryDimensionSearchUrlResolver;

    @Mock
    private TargetBrandDimensionSearchUrlResolver targetBrandDimensionSearchUrlResolver;

    @InjectMocks
    private final TgtDimensionSearchResultsHandler handler = new TgtDimensionSearchResultsHandler();


    @Before
    public void setUp() throws NoSuchMethodException, SecurityException
    {

        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testProcess() throws CartridgeHandlerException, NoSuchMethodException, SecurityException {
        final DimensionSearchResultsConfig cartridgeConfig = new DimensionSearchResultsConfig(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DIMENSION_SEARCH_AUTO_SUGGEST_ITEM);
        final DimensionSearchResults dimensionSearchResults = new DimensionSearchResults(cartridgeConfig);
        final List<DimensionSearchGroup> contentItemList = new ArrayList<>();
        final DimensionSearchGroup categoryDimensionSearchGroup = new DimensionSearchGroup();
        categoryDimensionSearchGroup.setDimensionName("category");
        final List<DimensionSearchValue> categoryDimensionSearchValueList = new ArrayList<>();
        final DimensionSearchValue categoryDimensionSearchValue = new DimensionSearchValue();
        categoryDimensionSearchValueList.add(categoryDimensionSearchValue);
        BDDMockito.when(targetCategoryDimensionSearchUrlResolver.resolve(categoryDimensionSearchValue)).thenReturn(
                "categoryTestUrl");
        categoryDimensionSearchGroup.setDimensionSearchValues(categoryDimensionSearchValueList);
        contentItemList.add(categoryDimensionSearchGroup);


        final DimensionSearchGroup brandDimensionSearchGroup = new DimensionSearchGroup();
        brandDimensionSearchGroup.setDimensionName("brand");
        final List<DimensionSearchValue> brandDimensionSearchValueList = new ArrayList<>();
        final DimensionSearchValue brandDimensionSearchValue = new DimensionSearchValue();
        brandDimensionSearchValueList.add(brandDimensionSearchValue);
        BDDMockito.when(targetBrandDimensionSearchUrlResolver.resolve(brandDimensionSearchValue)).thenReturn(
                "brandTestUrl");
        brandDimensionSearchGroup.setDimensionSearchValues(brandDimensionSearchValueList);
        contentItemList.add(brandDimensionSearchGroup);

        dimensionSearchResults.setDimensionSearchGroups(contentItemList);

        final DimensionSearchResults result = handler.populateCategoryAndBrandUrl(dimensionSearchResults);
        final List<DimensionSearchGroup> resultList = result.getDimensionSearchGroups();
        Assert.assertEquals(2, resultList.size());
        for (final DimensionSearchGroup dimensionSearchGroup : resultList) {
            if (EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY.equals(dimensionSearchGroup
                    .getDimensionName())) {
                Assert.assertEquals("categoryTestUrl", dimensionSearchGroup.getDimensionSearchValues().get(0)
                        .getNavigationState());
            }
            else if (EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND.equals(dimensionSearchGroup
                    .getDimensionName())) {
                Assert.assertEquals("brandTestUrl", dimensionSearchGroup.getDimensionSearchValues().get(0)
                        .getNavigationState());
            }
        }

    }

    @Test
    public void testProcessWithCategoryOnly() throws CartridgeHandlerException, NoSuchMethodException,
            SecurityException {
        final DimensionSearchResultsConfig cartridgeConfig = new DimensionSearchResultsConfig(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DIMENSION_SEARCH_AUTO_SUGGEST_ITEM);
        final DimensionSearchResults dimensionSearchResults = new DimensionSearchResults(cartridgeConfig);
        final List<DimensionSearchGroup> contentItemList = new ArrayList<>();
        final DimensionSearchGroup categoryDimensionSearchGroup = new DimensionSearchGroup();
        categoryDimensionSearchGroup.setDimensionName("category");
        final List<DimensionSearchValue> categoryDimensionSearchValueList = new ArrayList<>();
        final DimensionSearchValue categoryDimensionSearchValue = new DimensionSearchValue();
        categoryDimensionSearchValueList.add(categoryDimensionSearchValue);
        BDDMockito.when(targetCategoryDimensionSearchUrlResolver.resolve(categoryDimensionSearchValue)).thenReturn(
                "categoryTestUrl");
        categoryDimensionSearchGroup.setDimensionSearchValues(categoryDimensionSearchValueList);
        contentItemList.add(categoryDimensionSearchGroup);

        dimensionSearchResults.setDimensionSearchGroups(contentItemList);

        final DimensionSearchResults result = handler.populateCategoryAndBrandUrl(dimensionSearchResults);
        final List<DimensionSearchGroup> resultList = result.getDimensionSearchGroups();
        Assert.assertEquals(1, resultList.size());
        final DimensionSearchGroup dimensionSearchGroup = resultList.get(0);
        Assert.assertEquals(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY,
                dimensionSearchGroup.getDimensionName());
        Assert.assertEquals("categoryTestUrl", dimensionSearchGroup.getDimensionSearchValues().get(0)
                .getNavigationState());
    }

    @Test
    public void testProcessWithBrandOnly() throws CartridgeHandlerException, NoSuchMethodException,
            SecurityException {
        final DimensionSearchResultsConfig cartridgeConfig = new DimensionSearchResultsConfig(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DIMENSION_SEARCH_AUTO_SUGGEST_ITEM);
        final DimensionSearchResults dimensionSearchResults = new DimensionSearchResults(cartridgeConfig);
        final List<DimensionSearchGroup> contentItemList = new ArrayList<>();

        final DimensionSearchGroup brandDimensionSearchGroup = new DimensionSearchGroup();
        brandDimensionSearchGroup.setDimensionName("brand");
        final List<DimensionSearchValue> brandDimensionSearchValueList = new ArrayList<>();
        final DimensionSearchValue brandDimensionSearchValue = new DimensionSearchValue();
        brandDimensionSearchValueList.add(brandDimensionSearchValue);
        BDDMockito.when(targetBrandDimensionSearchUrlResolver.resolve(brandDimensionSearchValue)).thenReturn(
                "brandTestUrl");
        brandDimensionSearchGroup.setDimensionSearchValues(brandDimensionSearchValueList);
        contentItemList.add(brandDimensionSearchGroup);

        dimensionSearchResults.setDimensionSearchGroups(contentItemList);

        final DimensionSearchResults result = handler.populateCategoryAndBrandUrl(dimensionSearchResults);
        final List<DimensionSearchGroup> resultList = result.getDimensionSearchGroups();
        Assert.assertEquals(1, resultList.size());
        final DimensionSearchGroup dimensionSearchGroup = resultList.get(0);
        Assert.assertEquals(EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND,
                dimensionSearchGroup.getDimensionName());
        Assert.assertEquals("brandTestUrl", dimensionSearchGroup.getDimensionSearchValues().get(0)
                .getNavigationState());
    }

    @Test
    public void testProcessWithEmplyResult() throws CartridgeHandlerException, NoSuchMethodException,
            SecurityException {
        final DimensionSearchResultsConfig cartridgeConfig = new DimensionSearchResultsConfig(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DIMENSION_SEARCH_AUTO_SUGGEST_ITEM);
        final DimensionSearchResults dimensionSearchResults = new DimensionSearchResults(cartridgeConfig);
        final List<DimensionSearchGroup> contentItemList = new ArrayList<>();

        dimensionSearchResults.setDimensionSearchGroups(contentItemList);

        final DimensionSearchResults result = handler.populateCategoryAndBrandUrl(dimensionSearchResults);
        final List<DimensionSearchGroup> resultList = result.getDimensionSearchGroups();
        Assert.assertEquals(0, resultList.size());
    }

}
