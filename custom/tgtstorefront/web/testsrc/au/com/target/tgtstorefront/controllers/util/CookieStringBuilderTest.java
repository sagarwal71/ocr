package au.com.target.tgtstorefront.controllers.util;

import de.hybris.bootstrap.annotations.UnitTest;

import javax.servlet.http.Cookie;

import org.junit.Assert;

import org.junit.Test;


/**
 * Unit test for {@link CookieStringBuilder}
 */
@UnitTest
public class CookieStringBuilderTest {

    private final CookieStringBuilder cookieStringBuilder = new CookieStringBuilder();

    @Test
    public void buildCookieStringWithNullCookies() {
        final String result = cookieStringBuilder.buildCookieString(null);
        Assert.assertEquals("", result);
    }

    @Test
    public void buildCookieStringWithEmptyCookies() {
        final Cookie[] cookies = {};
        final String result = cookieStringBuilder.buildCookieString(cookies);
        Assert.assertEquals("", result);
    }

    @Test
    public void buildCookieString() {
        final Cookie cookie1 = new Cookie("macaroon", "almond");
        final Cookie cookie2 = new Cookie("oreo", " white creme");

        final Cookie[] cookies = { cookie1, cookie2 };
        final String result = cookieStringBuilder.buildCookieString(cookies);
        Assert.assertEquals("macaroon=almond;oreo= white creme;", result);
    }
}
