/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.CatalogPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.servicelayer.session.SessionService;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import au.com.target.endeca.infront.assemble.impl.EndecaPageAssemble;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.config.TargetSharedConfigFacade;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtstorefront.controllers.util.ProductDataHelper;
import au.com.target.tgtstorefront.data.MessageKeyArguments;
import au.com.target.tgtstorefront.util.PageTitleResolver;
import au.com.target.tgtwebcore.cache.service.AkamaiCacheConfigurationService;
import au.com.target.tgtwebcore.constants.TgtwebcoreConstants;
import au.com.target.tgtwebcore.enums.CachedPageType;
import au.com.target.tgtwebcore.model.AkamaiCacheConfigurationModel;


/**
 * @author rsamuel3
 * 
 */
@PrepareForTest(AbstractPageController.class)
@RunWith(MockitoJUnitRunner.class)
public class AbstractPageControllerTest {

    private static final String TEST_PRODUCT_CODE = "productCode";

    @Mock
    private AkamaiCacheConfigurationService mockAkamaiCacheConfigurationService;

    @Mock
    private AkamaiCacheConfigurationModel mockAkamaiCacheModel;

    @Mock
    private HttpServletRequest request;

    @Mock
    private PageTitleResolver pageTitleResolver;

    @Mock
    private EndecaPageAssemble endecaPageAssemble;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Mock
    private SalesApplicationFacade salesApplicationFacade;

    @Mock
    private SessionService sessionService;

    @Mock
    private TargetSharedConfigFacade targetSharedConfigFacade;

    @Mock
    private PriceDataFactory priceDataFactory;

    @Mock
    private CMSPageService cmsPageService;

    @Mock
    private Model mockModel;



    @InjectMocks
    private final AbstractPageController pageController = Mockito.mock(AbstractPageController.class,
            Mockito.CALLS_REAL_METHODS);

    @Test
    public void testAkamaiCacheHeaders() {
        final String noStore = "!nostore";
        final int maxAge = 300;
        final Model model = new BindingAwareModelMap();
        final MockHttpServletResponse response = new MockHttpServletResponse();
        Mockito.when(mockAkamaiCacheConfigurationService.getAkamaiCacheConfiguration(CachedPageType.PRODUCT))
                .thenReturn(
                        mockAkamaiCacheModel);
        Mockito.when(mockAkamaiCacheModel.getEnabled()).thenReturn(Boolean.TRUE);
        Mockito.when(mockAkamaiCacheModel.getAppendParam()).thenReturn(noStore);
        Mockito.when(mockAkamaiCacheModel.getMaxAge()).thenReturn(new Integer(maxAge));
        pageController.addAkamaiCacheAttributes(CachedPageType.PRODUCT, response, model);
        Assert.assertTrue(model.containsAttribute("anonymousCachable"));
        final Map<String, Object> mapAttributes = model.asMap();
        Assert.assertTrue(((Boolean)mapAttributes.get("anonymousCachable")).booleanValue());
        Assert.assertTrue(response.containsHeader("Edge-Control"));
        final String cacheHeader = response.getHeader("Edge-Control");
        final String[] headers = StringUtils.split(cacheHeader, ",");
        Assert.assertEquals("testing of the appendParams", noStore, headers[0]);
        Assert.assertEquals("testing of the appendParams", "max-age=" + maxAge, headers[1]);
    }

    @Test
    public void testAkamaiCacheHeadersNoPageType() {
        final Model model = new BindingAwareModelMap();
        final HttpServletResponse response = new MockHttpServletResponse();
        Mockito.when(mockAkamaiCacheConfigurationService.getAkamaiCacheConfiguration(CachedPageType.PRODUCT))
                .thenReturn(
                        mockAkamaiCacheModel);
        Mockito.when(mockAkamaiCacheModel.getEnabled()).thenReturn(Boolean.TRUE);
        Mockito.when(mockAkamaiCacheModel.getAppendParam()).thenReturn("!nostore");
        Mockito.when(mockAkamaiCacheModel.getMaxAge()).thenReturn(new Integer(300));
        pageController.addAkamaiCacheAttributes(null, response, model);
        Assert.assertFalse(model.containsAttribute("anonymousCachable"));
        Assert.assertFalse(response.containsHeader("Edge-Control"));
    }

    @Test
    public void testAkamaiCacheHeadersEnabledFalse() {
        final Model model = new BindingAwareModelMap();
        final HttpServletResponse response = new MockHttpServletResponse();
        Mockito.when(mockAkamaiCacheConfigurationService.getAkamaiCacheConfiguration(CachedPageType.PRODUCT))
                .thenReturn(
                        mockAkamaiCacheModel);
        Mockito.when(mockAkamaiCacheModel.getEnabled()).thenReturn(Boolean.FALSE);
        Mockito.when(mockAkamaiCacheModel.getAppendParam()).thenReturn("!nostore");
        Mockito.when(mockAkamaiCacheModel.getMaxAge()).thenReturn(new Integer(300));
        pageController.addAkamaiCacheAttributes(CachedPageType.PRODUCT, response, model);
        Assert.assertFalse(model.containsAttribute("anonymousCachable"));
        Assert.assertFalse(response.containsHeader("Edge-Control"));
    }

    @Test
    public void testAkamaiCacheHeadersNoConfig() {
        final Model model = new BindingAwareModelMap();
        final HttpServletResponse response = new MockHttpServletResponse();
        Mockito.when(mockAkamaiCacheConfigurationService.getAkamaiCacheConfiguration(CachedPageType.PRODUCT))
                .thenReturn(null);
        pageController.addAkamaiCacheAttributes(CachedPageType.PRODUCT, response, model);
        Assert.assertFalse(model.containsAttribute("anonymousCachable"));
        Assert.assertFalse(response.containsHeader("Edge-Control"));

        Mockito.verify(mockAkamaiCacheConfigurationService).getAkamaiCacheConfiguration(CachedPageType.PRODUCT);
        Mockito.verifyNoMoreInteractions(mockAkamaiCacheConfigurationService);
    }

    @Test
    public void testAkamaiCacheHeadersEmptyParams() {
        final Model model = new BindingAwareModelMap();
        final HttpServletResponse response = new MockHttpServletResponse();
        Mockito.when(mockAkamaiCacheConfigurationService.getAkamaiCacheConfiguration(CachedPageType.PRODUCT))
                .thenReturn(
                        mockAkamaiCacheModel);
        Mockito.when(mockAkamaiCacheModel.getEnabled()).thenReturn(Boolean.TRUE);
        Mockito.when(mockAkamaiCacheModel.getAppendParam()).thenReturn("");
        Mockito.when(mockAkamaiCacheModel.getMaxAge()).thenReturn(new Integer(300));
        pageController.addAkamaiCacheAttributes(null, response, model);
        Assert.assertFalse(model.containsAttribute("anonymousCachable"));
        Assert.assertFalse(response.containsHeader("Edge-Control"));
    }

    @Test
    public void testAkamaiCacheHeadersNullParams() {
        final Model model = new BindingAwareModelMap();
        final HttpServletResponse response = new MockHttpServletResponse();
        Mockito.when(mockAkamaiCacheConfigurationService.getAkamaiCacheConfiguration(CachedPageType.PRODUCT))
                .thenReturn(
                        mockAkamaiCacheModel);
        Mockito.when(mockAkamaiCacheModel.getEnabled()).thenReturn(Boolean.TRUE);
        Mockito.when(mockAkamaiCacheModel.getAppendParam()).thenReturn(null);
        Mockito.when(mockAkamaiCacheModel.getMaxAge()).thenReturn(new Integer(300));
        pageController.addAkamaiCacheAttributes(null, response, model);
        Assert.assertFalse(model.containsAttribute("anonymousCachable"));
        Assert.assertFalse(response.containsHeader("Edge-Control"));
    }

    @Test
    public void testAkamaiCacheHeadersNullMaxAge() {
        final Model model = new BindingAwareModelMap();
        final HttpServletResponse response = new MockHttpServletResponse();
        Mockito.when(mockAkamaiCacheConfigurationService.getAkamaiCacheConfiguration(CachedPageType.PRODUCT))
                .thenReturn(
                        mockAkamaiCacheModel);
        Mockito.when(mockAkamaiCacheModel.getEnabled()).thenReturn(Boolean.TRUE);
        Mockito.when(mockAkamaiCacheModel.getAppendParam()).thenReturn("!nostore");
        Mockito.when(mockAkamaiCacheModel.getMaxAge()).thenReturn(null);
        pageController.addAkamaiCacheAttributes(null, response, model);
        Assert.assertFalse(model.containsAttribute("anonymousCachable"));
        Assert.assertFalse(response.containsHeader("Edge-Control"));
    }

    @Test
    public void testAkamaiCacheHeadersMaxAge0() {
        final Model model = new BindingAwareModelMap();
        final MockHttpServletResponse response = new MockHttpServletResponse();
        Mockito.when(mockAkamaiCacheConfigurationService.getAkamaiCacheConfiguration(CachedPageType.PRODUCT))
                .thenReturn(
                        mockAkamaiCacheModel);
        Mockito.when(mockAkamaiCacheModel.getEnabled()).thenReturn(Boolean.TRUE);
        Mockito.when(mockAkamaiCacheModel.getAppendParam()).thenReturn("!nostore");
        Mockito.when(mockAkamaiCacheModel.getMaxAge()).thenReturn(new Integer(0));
        pageController.addAkamaiCacheAttributes(CachedPageType.PRODUCT, response, model);
        Assert.assertFalse(model.containsAttribute("anonymousCachable"));
        Assert.assertFalse(response.containsHeader("Edge-Control"));
    }

    @Test
    public void testAkamaiCacheHeadersEmptyAppendParam() {
        final int maxAge = 300;
        final Model model = new BindingAwareModelMap();
        final MockHttpServletResponse response = new MockHttpServletResponse();
        Mockito.when(mockAkamaiCacheConfigurationService.getAkamaiCacheConfiguration(CachedPageType.PRODUCT))
                .thenReturn(
                        mockAkamaiCacheModel);
        Mockito.when(mockAkamaiCacheModel.getEnabled()).thenReturn(Boolean.TRUE);
        Mockito.when(mockAkamaiCacheModel.getAppendParam()).thenReturn("");
        Mockito.when(mockAkamaiCacheModel.getMaxAge()).thenReturn(new Integer(maxAge));
        pageController.addAkamaiCacheAttributes(CachedPageType.PRODUCT, response, model);
        Assert.assertTrue(model.containsAttribute("anonymousCachable"));
        final Map<String, Object> mapAttributes = model.asMap();
        Assert.assertTrue(((Boolean)mapAttributes.get("anonymousCachable")).booleanValue());
        Assert.assertTrue(response.containsHeader("Edge-Control"));
        final String cacheHeader = response.getHeader("Edge-Control");
        final String[] headers = StringUtils.split(cacheHeader, ",");
        Assert.assertEquals("testing of the appendParams", "max-age=" + maxAge, headers[0]);
    }

    @Test
    public void testAkamaiCacheHeadersNullAppendParam() {
        final int maxAge = 300;
        final Model model = new BindingAwareModelMap();
        final MockHttpServletResponse response = new MockHttpServletResponse();
        Mockito.when(mockAkamaiCacheConfigurationService.getAkamaiCacheConfiguration(CachedPageType.PRODUCT))
                .thenReturn(
                        mockAkamaiCacheModel);
        Mockito.when(mockAkamaiCacheModel.getEnabled()).thenReturn(Boolean.TRUE);
        Mockito.when(mockAkamaiCacheModel.getAppendParam()).thenReturn(null);
        Mockito.when(mockAkamaiCacheModel.getMaxAge()).thenReturn(new Integer(maxAge));
        pageController.addAkamaiCacheAttributes(CachedPageType.PRODUCT, response, model);
        Assert.assertTrue(model.containsAttribute("anonymousCachable"));
        final Map<String, Object> mapAttributes = model.asMap();
        Assert.assertTrue(((Boolean)mapAttributes.get("anonymousCachable")).booleanValue());
        Assert.assertTrue(response.containsHeader("Edge-Control"));
        final String cacheHeader = response.getHeader("Edge-Control");
        final String[] headers = StringUtils.split(cacheHeader, ",");
        Assert.assertEquals("testing of the appendParams", "max-age=" + maxAge, headers[0]);
    }


    @Test
    public void testAddAkamaiCacheAttributesWithMessages() {
        final Map<String, Object> modelMap = new HashMap<String, Object>();
        modelMap.put("accConfMsgs", Collections.singletonList(new MessageKeyArguments()));

        final Model mockModel = Mockito.mock(Model.class);
        BDDMockito.given(mockModel.asMap()).willReturn(modelMap);

        BDDMockito.given(mockAkamaiCacheModel.getEnabled()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockAkamaiCacheModel.getAppendParam()).willReturn(null);
        BDDMockito.given(mockAkamaiCacheModel.getMaxAge()).willReturn(new Integer(300));

        BDDMockito.given(mockAkamaiCacheConfigurationService.getAkamaiCacheConfiguration(CachedPageType.PRODUCT))
                .willReturn(mockAkamaiCacheModel);

        final HttpServletResponse mockResponse = Mockito.mock(HttpServletResponse.class);

        pageController.addAkamaiCacheAttributes(CachedPageType.PRODUCT, mockResponse, mockModel);

        Mockito.verify(mockResponse).setHeader("Edge-Control", "no-store");
        Mockito.verify(mockModel).addAttribute("anonymousCachable", Boolean.FALSE);
    }

    @Test
    public void testPopulateRecommendedProductsDetails() {
        final String endecaPageURI = EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI;
        given(request.getAttribute(ProductDataHelper.CURRENT_PRODUCT)).willReturn(TEST_PRODUCT_CODE);

        pageController.populateRecommendedProductsDetails(request, endecaPageURI);
    }

    @Test
    public void testSetCanonicalUrlForContentPage() {
        final Model model = mock(Model.class);
        final ContentPageModel contentPageModel = mock(ContentPageModel.class);
        when(contentPageModel.getCanonicalUrl()).thenReturn("http://www.target.com.au");

        pageController.storeCmsPageInModel(model, contentPageModel);

        verify(model).addAttribute(AbstractPageController.CANONICAL_URL, "http://www.target.com.au");
    }

    @Test
    public void testNotSetCanonicalUrlForContentPage() {
        final Model model = mock(Model.class);
        final CatalogPageModel pageModel = mock(CatalogPageModel.class);

        pageController.storeCmsPageInModel(model, pageModel);

        verify(model, times(0)).addAttribute(AbstractPageController.CANONICAL_URL, "http://www.target.com.au");
    }

    @Test
    public void testShouldGoSpcWhenKiosk() {
        given(Boolean.valueOf(salesApplicationFacade.isKioskApplication())).willReturn(Boolean.TRUE);
        assertThat(pageController.shouldGoToSPC()).isFalse();

    }

    @Test
    public void testShouldGoSpcWhenNotKioskAndSpcEnabled() {
        given(Boolean.valueOf(targetFeatureSwitchFacade.isSpcLoginAndCheckout()))
                .willReturn(Boolean.TRUE);
        assertThat(pageController.shouldGoToSPC()).isTrue();
    }

    @Test
    public void testShouldGoSpcWhenNotKioskAndSpcDisabled() {
        given(Boolean.valueOf(targetFeatureSwitchFacade.isSpcLoginAndCheckout()))
                .willReturn(Boolean.FALSE);
        given(sessionService.getAttribute("spc")).willReturn("on");
        assertThat(pageController.shouldGoToSPC()).isFalse();
    }

    @Test
    public void testPopulateZipPayModal() throws CMSItemNotFoundException {
        final ContentPageModel zipPaymentModal = mock(ContentPageModel.class);
        given(cmsPageService.getPageForLabelOrId(TgtwebcoreConstants.ZIPPAYMENT_MODAL_ID))
            .willReturn(zipPaymentModal);
        given(zipPaymentModal.getLabel()).willReturn("/modal/zippayment");
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isZipEnabled();
        pageController.populateZipPaymentModal(mockModel);
        verify(mockModel).addAttribute("zipPaymentModal", "/modal/zippayment");
    }

    @Test
    public void testPopulateZipPaymentModalDisabled() throws CMSItemNotFoundException {
        final String zipPaymentModalID = TgtwebcoreConstants.ZIPPAYMENT_MODAL_ID;
        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isZipEnabled();
        pageController.populateZipPaymentModal(mockModel);
        verify(cmsPageService, never()).getPageForLabelOrId(zipPaymentModalID);
    }

    @Test
    public void testGetZipPaymentThresholdAmount(){
        final PriceData priceData = new PriceData();
        priceData.setCurrencyIso(TgtCoreConstants.AUSTRALIAN_DOLLARS);
        priceData.setPriceType(PriceDataType.BUY);
        priceData.setValue(new BigDecimal("1000"));
        given(priceDataFactory.create(PriceDataType.BUY, new BigDecimal("1000"),
            TgtCoreConstants.AUSTRALIAN_DOLLARS)).willReturn(priceData);
        given(targetSharedConfigFacade.getConfigByCode(TgtFacadesConstants.ZIP_PAYMENT_THRESHOLD_AMOUNT)).willReturn("1000");
        assertThat(pageController.getZipPaymentThresholdAmount()).isEqualTo(priceData);
    }
}
