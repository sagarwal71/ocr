package au.com.target.tgtstorefront.url;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ProductPageModel;
import de.hybris.platform.cms2.model.preview.PreviewDataModel;
import de.hybris.platform.commerceservices.url.UrlResolver;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtwebcore.model.cms2.pages.BrandPageModel;
import au.com.target.tgtwebcore.model.cms2.preview.TargetPreviewDataModel;


/**
 * Unit test for {@link TargetPreviewDataModelUrlResolver}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPreviewDataModelUrlResolverTest {

    @InjectMocks
    private final TargetPreviewDataModelUrlResolver targetPreviewDataModelUrlResolver = new TargetPreviewDataModelUrlResolver();

    @Mock
    private UrlResolver<PreviewDataModel> defaultPreviewDataUrlResolver;

    @Mock
    private UrlResolver<BrandModel> brandModelUrlResolver;

    @Test
    public void testResolveForNull() {
        targetPreviewDataModelUrlResolver.resolve(null);
        Mockito.verifyZeroInteractions(brandModelUrlResolver);
        Mockito.verify(defaultPreviewDataUrlResolver).resolve(null);
    }

    @Test
    public void testResolveForNonBrandPage() {
        final AbstractPageModel productPage = Mockito.mock(ProductPageModel.class);
        final TargetPreviewDataModel targetPreviewDataModel = Mockito.mock(TargetPreviewDataModel.class);

        BDDMockito.given(targetPreviewDataModel.getPage()).willReturn(productPage);
        targetPreviewDataModelUrlResolver.resolve(targetPreviewDataModel);
        Mockito.verifyZeroInteractions(brandModelUrlResolver);
        Mockito.verify(defaultPreviewDataUrlResolver).resolve(targetPreviewDataModel);
    }

    @Test
    public void testResolveForBrandPage() {
        final AbstractPageModel brandPage = Mockito.mock(BrandPageModel.class);
        final BrandModel brand = Mockito.mock(BrandModel.class);
        final TargetPreviewDataModel targetPreviewDataModel = Mockito.mock(TargetPreviewDataModel.class);

        BDDMockito.given(targetPreviewDataModel.getPage()).willReturn(brandPage);
        BDDMockito.given(targetPreviewDataModel.getPreviewBrand()).willReturn(brand);

        targetPreviewDataModelUrlResolver.resolve(targetPreviewDataModel);
        Mockito.verify(brandModelUrlResolver).resolve(brand);
        Mockito.verifyZeroInteractions(defaultPreviewDataUrlResolver);
    }

}
