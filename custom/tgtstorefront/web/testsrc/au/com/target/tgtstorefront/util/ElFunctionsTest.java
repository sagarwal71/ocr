/**
 * 
 */
package au.com.target.tgtstorefront.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.springframework.util.CollectionUtils;
import org.springframework.web.util.UriComponents;


/**
 * @author rmcalave, gbaker2
 * 
 */
public class ElFunctionsTest {

    private String timestamp;

    @Before
    public void setUp() {
        timestamp = Long.toString(new Date().getTime());
    }

    @Test
    public void testAbbreviateStringWithMaxLengthLongerThanString() {
        final String stringToAbbreviate = "The quick brown fox jumped over the lazy dog";
        final int maxLength = 50;

        Assert.assertEquals(stringToAbbreviate, ElFunctions.abbreviateString(stringToAbbreviate, maxLength));
    }

    @Test
    public void testAbbreviateStringWithMaxLengthInMiddleOfWord() {
        final String stringToAbbreviate = "The quick brown fox jumped over the lazy dog";
        final int maxLength = 25;

        Assert.assertEquals("The quick brown fox...", ElFunctions.abbreviateString(stringToAbbreviate, maxLength));
    }

    @Test
    public void testAbbreviateStringWithMaxLengthOnASpace() {
        final String stringToAbbreviate = "The quick brown fox jumped over the lazy dog";
        final int maxLength = 16;

        Assert.assertEquals("The quick...", ElFunctions.abbreviateString(stringToAbbreviate, maxLength));
    }

    @Test
    public void testAbbreviateStringWithMaxLength3CharsAfterASpace() {
        final String stringToAbbreviate = "The quick brown fox jumped over the lazy dog";
        final int maxLength = 19;

        Assert.assertEquals("The quick brown...", ElFunctions.abbreviateString(stringToAbbreviate, maxLength));
    }

    @Test
    public void testAbbreviateStringWithMaxLengthEqualToStringLength() {
        final String stringToAbbreviate = "The quick brown fox jumped over the lazy dog";
        final int maxLength = stringToAbbreviate.length();

        Assert.assertEquals("The quick brown fox jumped over the lazy dog",
                ElFunctions.abbreviateString(stringToAbbreviate, maxLength));
    }

    @Test
    public void testAbbreviateStringWithSingleLongWord() {
        final String stringToAbbreviate = "Thequickbrownfoxjumpedoverthelazydog";
        final int maxLength = 15;

        Assert.assertEquals("Thequickbrow...",
                ElFunctions.abbreviateString(stringToAbbreviate, maxLength));
    }

    @Test
    public void testAbbreviateStringWithMaxLengthTooShort() {
        final String stringToAbbreviate = "The quick brown fox jumped over the lazy dog";
        final int maxLength = 2;

        try {
            ElFunctions.abbreviateString(stringToAbbreviate, maxLength);
        }
        catch (final IllegalArgumentException ex) {
            return;
        }

        Assert.fail("Expected IllegalArgumentException to be thrown");
    }

    @Test
    public void testAbbreviateHtmlStringWithContainingTags() {
        final String stringToAbbreviate = "<p>The quick brown fox jumped over the lazy dog</p>";
        final int maxLength = 50;

        Assert.assertEquals("The quick brown fox jumped over the lazy dog",
                ElFunctions.abbreviateHTMLString(stringToAbbreviate, maxLength));
    }

    @Test
    public void testAbbreviateHtmlStringWithInternalTags() {
        final String stringToAbbreviate = "The quick <em class=\"foxy\" style=\"color:brown;\">brown fox</em> jumped over the lazy dog";
        final int maxLength = 50;

        Assert.assertEquals("The quick brown fox jumped over the lazy dog",
                ElFunctions.abbreviateHTMLString(stringToAbbreviate, maxLength));
    }

    @Test
    public void cacheBusterURL() {
        final String expected = "/_ui/build/js/jquery.js";
        final String result = ElFunctions.cacheBusterUrl(true, null, expected);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void cacheBusterURLnull() {
        final String expected = "/_ui/build/js/jquery.js";
        final String result = ElFunctions.cacheBusterUrl(false, "", expected);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void cacheBusterURLnull2() {
        final String result = ElFunctions.cacheBusterUrl(false, timestamp, null);
        Assert.assertEquals("", result);
    }

    @Test
    public void cacheBusterEnabledURLcss() {
        final String expected = "/_ui/build/css/screen." + timestamp + ".css";
        final String result = ElFunctions.cacheBusterUrl(true, timestamp, "/_ui/build/css/screen.css");
        Assert.assertEquals(expected, result);
    }

    @Test
    public void cacheBusterEnabledURLjs() {
        final String expected = "/_ui/build/js/jquery." + timestamp + ".js";
        final String result = ElFunctions.cacheBusterUrl(true, timestamp, "/_ui/build/js/jquery.js");
        Assert.assertEquals(expected, result);
    }

    @Test
    public void cacheBusterEnabledURLother() {
        final String expected = "/_ui/build/images/other-image.jpg";
        final String result = ElFunctions.cacheBusterUrl(true, timestamp, expected);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void cacheBusterNotEnabledURLcssQs() {
        final String expected = "/_ui/build/css/screen.css?b=" + timestamp;
        final String result = ElFunctions.cacheBusterUrl(false, timestamp, "/_ui/build/css/screen.css");
        Assert.assertEquals(expected, result);
    }

    @Test
    public void cacheBusterNotEnabledURLjsQs() {
        final String expected = "/_ui/build/js/jquery.js?b=" + timestamp;
        final String result = ElFunctions.cacheBusterUrl(false, timestamp, "/_ui/build/js/jquery.js");
        Assert.assertEquals(expected, result);
    }

    @Test
    public void cacheBusterNotEnabledURLotherQs() {
        final String expected = "/_ui/build/images/other-image.jpg";
        final String result = ElFunctions.cacheBusterUrl(false, timestamp, expected);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void validUrlStringHttp() {
        final String expected = "http://google.com";
        final String result = ElFunctions.validUrlString("http://google.com");
        Assert.assertEquals(expected, result);
    }

    @Test
    public void validUrlStringHttps() {
        final String expected = "https://google.com";
        final String result = ElFunctions.validUrlString("https://google.com");
        Assert.assertEquals(expected, result);
    }

    @Test
    public void validUrlStringRelative() {
        final String expected = "/kids";
        final String result = ElFunctions.validUrlString("/kids");
        Assert.assertEquals(expected, result);
    }

    @Test
    public void validUrlStringProtocolRelative() {
        final String expected = "//kids";
        final String result = ElFunctions.validUrlString("//kids");
        Assert.assertEquals(expected, result);
    }

    @Test
    public void validUrlStringWhiteSpace() {
        final String expected = "/kids";
        final String result = ElFunctions.validUrlString("  /kids");
        Assert.assertEquals(expected, result);
    }

    @Test
    public void validUrlStringMisconfigured() {
        final String expected = "/kids";
        final String result = ElFunctions.validUrlString("kids");
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testGetUriComponents() {
        final String expected = "http://www.target.com.au/c/womens/W1234?N=k38d5j&Nrpp=30&page=2";
        final UriComponents result = ElFunctions.getUriComponents(expected);
        Assert.assertEquals(expected, result.toUriString());
    }

    @Test
    public void testGetUriComponentsWithEncoding() {
        final String expected = "http://www.target.com.au/c/womens/W1234?N=k38d5j&Ns=price%7C1&Nrpp=30&page=2";
        final UriComponents result = ElFunctions.getUriComponents(expected);
        Assert.assertEquals(expected, result.encode().toUriString());
    }

    @Test
    public void testGetUriComponentsAppearsInvalidButIsJustARelativeLink() {
        final String expected = "lalalalala";
        final UriComponents result = ElFunctions.getUriComponents(expected);
        Assert.assertEquals(expected, result.toUriString());
    }

    @Test
    public void testGetUriComponentsEmpty() {
        final String expected = "";
        final UriComponents result = ElFunctions.getUriComponents(expected);
        Assert.assertNull(result);
    }

    @Test
    public void testEndecaUrlSplit() {
        final String expected = "/media/url/for/an/image.jpg";
        final String result = ElFunctions.splitEndecaUrlField("01**".concat(expected));
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testEndecaUrlSplitWithoutStars() {
        final String expected = "/media/url/for/an/image.jpg";
        final String result = ElFunctions.splitEndecaUrlField(expected);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testEndecaUrlSplitWithNull() {
        final String result = ElFunctions.splitEndecaUrlField(null);
        Assert.assertNull(result);
    }

    @Test
    public void testEndecaUrlSplitWithOnlyStarts() {
        final String expected = "";
        final String result = ElFunctions.splitEndecaUrlField("**");
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testEndecaUrlSplitWithFirstHalf() {
        final String expected = "";
        final String result = ElFunctions.splitEndecaUrlField("01**");
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testEndecaUrlSplitWithSecondHalf() {
        final String expected = "/media/url/for/an/image.jpg";
        final String result = ElFunctions.splitEndecaUrlField("**".concat(expected));
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testValidateNumberListToArrayWithValidOneCharacterString() {
        final String stringToConvert = "1";
        final List<String> listToExpect = new ArrayList<>();
        listToExpect.add("1");

        Assert.assertEquals(listToExpect, ElFunctions.validateNumberListToArray(stringToConvert));
    }

    @Test
    public void testValidateNumberListToArrayWithValidString() {
        final String stringToConvert = "1,20,2,60000";
        final List<String> listToExpect = new ArrayList<>();
        listToExpect.add("1");
        listToExpect.add("20");
        listToExpect.add("2");
        listToExpect.add("60000");

        Assert.assertEquals(listToExpect, ElFunctions.validateNumberListToArray(stringToConvert));
    }

    @Test
    public void testValidateNumberListToArrayWithEmptyString() {
        final String stringToConvert = "";
        final List<String> listToExpect = new ArrayList<>();

        Assert.assertEquals(listToExpect, ElFunctions.validateNumberListToArray(stringToConvert));
    }

    @Test
    public void testValidateNumberListToArrayWithInvalidNumberString() {
        final String stringToConvert = "1,2,,3,";
        final List<String> listToExpect = new ArrayList<>();

        Assert.assertEquals(listToExpect, ElFunctions.validateNumberListToArray(stringToConvert));
    }

    @Test
    public void testValidateNumberListToArrayWithInvalidString() {
        final String stringToConvert = "Such string, very invalid";
        final List<String> listToExpect = new ArrayList<>();

        Assert.assertEquals(listToExpect, ElFunctions.validateNumberListToArray(stringToConvert));
    }

    @Test
    public void testReverseArray() {
        final List<String> listToActual = new ArrayList<String>(CollectionUtils.arrayToList(new String[] { "1", "2",
                "3", "4" }));
        final List<String> listToExpect = new ArrayList<String>(CollectionUtils.arrayToList(new String[] { "4", "3",
                "2", "1" }));
        Assert.assertEquals(listToExpect, ElFunctions.reverseArrayList((ArrayList<String>)listToActual));
    }

    @Test
    public void testReverseArrayNull() {
        Assert.assertNull(ElFunctions.reverseArrayList(null));
    }


    @Test
    public void testReplaceAllNewLineWithBreak() {
        Assert.assertEquals("A<br />B<br />C", ElFunctions.replaceAllNewLineWithBreak("A\nB\nC"));
        Assert.assertEquals("<br />B<br />C", ElFunctions.replaceAllNewLineWithBreak("\nB\nC"));
        Assert.assertEquals("A<br />B<br /><br />", ElFunctions.replaceAllNewLineWithBreak("A\nB\n\n"));
    }

    @Test
    public void testReplaceAllNewLineWithBreakNull() {
        Assert.assertNull(ElFunctions.replaceAllNewLineWithBreak(null));
    }

}
