/**
 * 
 */
package au.com.target.tgtstorefront.controllers.misc;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

import com.google.gson.Gson;

import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.CookieRetrievalHelper;
import au.com.target.tgtstorefront.controllers.util.MiniCartDataHelper;
import au.com.target.tgtstorefront.security.cookie.EnhancedCookieGenerator;
import au.com.target.tgtstorefront.util.SearchQuerySanitiser;


/**
 * @author mjanarth
 *
 */
@UnitTest
public class CustomerControllerTest {

    @InjectMocks
    private final CustomerController customerController = new CustomerController();

    @Mock
    private MiniCartDataHelper miniCartDataHelper;

    @Mock
    private TargetProductFacade productFacade;

    @Mock
    private TargetCustomerFacade customerFacade;

    @Mock
    private CookieRetrievalHelper cookieRetrievalHelper;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Mock
    private TargetPointOfServiceData selectedStore;

    @Mock
    private TargetFeatureSwitchFacade featureSwitchFacade;

    @Mock
    private Model model;

    @Mock
    private TargetProductData productData;

    @Mock
    private EnhancedCookieGenerator preferredStoreCookieGenerator;

    @Mock
    private SearchQuerySanitiser searchQuerySanitiser;

    private String productCode;

    private String fragment;

    @Before
    public void prepareController() {
        MockitoAnnotations.initMocks(this);
        given(Boolean.valueOf(featureSwitchFacade.isConsolidatedStoreStockAvailable())).willReturn(Boolean.TRUE);
    }

    @Test
    public void getCustomerInfoWithEmptyProductCodeAndCookieFeatureSwitchOn() {
        productCode = "";
        fragment = customerController.getCustomerInfo(model, productCode, request, response);
        verify(miniCartDataHelper).populateMiniCartData(model);
        verifyNoMoreInteractions(model);
        assertThat(fragment).isEqualTo(ControllerConstants.Views.Fragments.Customer.GETINFO);
    }


    @Test
    public void getCustomerInfoWithProductCodeNotFoundAndEmptyCookieFeatureSwitchOn() {
        final UnknownIdentifierException ex = new UnknownIdentifierException("test");
        productCode = "P1000";
        given(cookieRetrievalHelper.getCookieValue(request, "t_psn")).willReturn("");
        given(productFacade.getProductForCodeAndOptionsWithDealMessage(
                productCode,
                Arrays.asList(ProductOption.PROMOTIONS))).willThrow(ex);
        fragment = customerController.getCustomerInfo(model, productCode, request, response);
        verify(miniCartDataHelper).populateMiniCartData(model);
        verifyNoMoreInteractions(model);
        assertThat(fragment).isEqualTo(ControllerConstants.Views.Fragments.Customer.GETINFO);

    }

    @Test
    public void getCustomerInfoWithProductCodeAndEmptyCookieFeatureSwitchOn() {
        productCode = "P1000";
        given(cookieRetrievalHelper.getCookieValue(request, "t_psn")).willReturn("");
        given(productFacade.getProductForCodeAndOptionsWithDealMessage(
                productCode,
                Arrays.asList(ProductOption.PROMOTIONS))).willReturn(productData);
        fragment = customerController.getCustomerInfo(model, productCode, request, response);
        verify(miniCartDataHelper).populateMiniCartData(model);
        verify(model).addAttribute("product", productData);
        verifyNoMoreInteractions(model);
        assertThat(fragment).isEqualTo(ControllerConstants.Views.Fragments.Customer.GETINFO);

    }

    @Test
    public void getCustomerInfoWithProductCodeAndCookieFeatureSwitchOn() {
        productCode = "P1000";
        given(cookieRetrievalHelper.getCookieValue(request, "t_psn")).willReturn("7001");
        given(targetStoreLocatorFacade.getPointOfService(Integer.valueOf(7001))).willReturn(selectedStore);
        given(productFacade.getProductForCodeAndOptionsWithDealMessage(
                productCode,
                Arrays.asList(ProductOption.PROMOTIONS))).willReturn(productData);
        fragment = customerController.getCustomerInfo(model, productCode, request, response);
        verify(miniCartDataHelper).populateMiniCartData(model);
        verify(model).addAttribute("product", productData);
        verify(model).addAttribute("store", selectedStore);
        assertThat(fragment).isEqualTo(ControllerConstants.Views.Fragments.Customer.GETINFO);
    }

    @Test
    public void getCustomerInfoWithProductCodeAndCookieFeatureSwitchOff() {
        productCode = "P1000";
        given(Boolean.valueOf(featureSwitchFacade.isConsolidatedStoreStockAvailable())).willReturn(Boolean.FALSE);
        given(productFacade.getProductForCodeAndOptionsWithDealMessage(
                productCode,
                Arrays.asList(ProductOption.PROMOTIONS))).willReturn(productData);
        fragment = customerController.getCustomerInfo(model, productCode, request, response);
        verifyNoMoreInteractions(targetStoreLocatorFacade);
        verifyNoMoreInteractions(cookieRetrievalHelper);
        verify(miniCartDataHelper).populateMiniCartData(model);
        verify(model).addAttribute("product", productData);
        verifyNoMoreInteractions(model);
        assertThat(fragment).isEqualTo(ControllerConstants.Views.Fragments.Customer.GETINFO);
    }

    @Test
    public void getCustomerInfoWithProductCodeAndCookieStoreClosedFeatureSwitchOn() {
        productCode = "P1000";
        given(cookieRetrievalHelper.getCookieValue(request, "t_psn")).willReturn("7001");
        given(selectedStore.getClosed()).willReturn(Boolean.TRUE);
        given(targetStoreLocatorFacade.getPointOfService(Integer.valueOf(7001))).willReturn(selectedStore);
        given(productFacade.getProductForCodeAndOptionsWithDealMessage(
                productCode,
                Arrays.asList(ProductOption.PROMOTIONS))).willReturn(productData);
        fragment = customerController.getCustomerInfo(model, productCode, request, response);
        verify(miniCartDataHelper).populateMiniCartData(model);
        verify(preferredStoreCookieGenerator).removeCookie(response);
        verify(model).addAttribute("product", productData);
        verifyNoMoreInteractions(model);
        assertThat(fragment).isEqualTo(ControllerConstants.Views.Fragments.Customer.GETINFO);
    }

    @Test
    public void getCustomerInfoWithProductCodeAndCookieEmptyStoreFeatureSwitchOn() {
        productCode = "P1000";
        given(cookieRetrievalHelper.getCookieValue(request, "t_psn")).willReturn("7001");
        given(selectedStore.getClosed()).willReturn(Boolean.TRUE);
        given(targetStoreLocatorFacade.getPointOfService(Integer.valueOf(7001))).willReturn(null);
        given(productFacade.getProductForCodeAndOptionsWithDealMessage(
                productCode,
                Arrays.asList(ProductOption.PROMOTIONS))).willReturn(productData);
        fragment = customerController.getCustomerInfo(model, productCode, request, response);
        verify(miniCartDataHelper).populateMiniCartData(model);
        verify(preferredStoreCookieGenerator).removeCookie(response);
        verify(model).addAttribute("product", productData);
        verifyNoMoreInteractions(model);
        assertThat(fragment).isEqualTo(ControllerConstants.Views.Fragments.Customer.GETINFO);

    }

    @Test
    public void getCustomerInfoWithProductCodeAndCookieNonNumberFeatureSwitchOn() {
        productCode = "P1000";
        given(cookieRetrievalHelper.getCookieValue(request, "t_psn")).willReturn("test");
        given(productFacade.getProductForCodeAndOptionsWithDealMessage(
                productCode,
                Arrays.asList(ProductOption.PROMOTIONS))).willReturn(productData);
        fragment = customerController.getCustomerInfo(model, productCode, request, response);
        verify(miniCartDataHelper).populateMiniCartData(model);
        verify(model).addAttribute("product", productData);
        verifyNoMoreInteractions(model);
        assertThat(fragment).isEqualTo(ControllerConstants.Views.Fragments.Customer.GETINFO);
    }

    @Test
    public void getCustomerLocationWithEmptyPostCode() {
        fragment = customerController.getCustomerLocation(model, "", "", response);
        verifyZeroInteractions(targetStoreLocatorFacade);
        verifyZeroInteractions(model);
        verifyZeroInteractions(preferredStoreCookieGenerator);
        assertThat(fragment).isEqualTo(ControllerConstants.Views.Fragments.Customer.LOCATION);
    }

    @Test
    public void getCustomerLocationWithNonNumeric() {
        fragment = customerController.getCustomerLocation(model, "test", "", response);
        verifyZeroInteractions(targetStoreLocatorFacade);
        verifyZeroInteractions(model);
        verifyZeroInteractions(preferredStoreCookieGenerator);
        assertThat(fragment).isEqualTo(ControllerConstants.Views.Fragments.Customer.LOCATION);
    }

    @Test
    public void getCustomerLocationValidLocationAndClosedStore() {
        given(targetStoreLocatorFacade.getPointOfService(Integer.valueOf(3215))).willReturn(selectedStore);
        given(selectedStore.getClosed()).willReturn(Boolean.TRUE);
        fragment = customerController.getCustomerLocation(model, "3215", "", response);
        verify(preferredStoreCookieGenerator).removeCookie(response);
        assertThat(fragment).isEqualTo(ControllerConstants.Views.Fragments.Customer.LOCATION);
        verifyZeroInteractions(model);
    }

    @Test
    public void getCustomerLocationValidLocationAndNoStoreReturned() {
        given(targetStoreLocatorFacade.getPointOfService(Integer.valueOf(3215))).willReturn(null);
        fragment = customerController.getCustomerLocation(model, "3215", "", response);
        verify(preferredStoreCookieGenerator).removeCookie(response);
        verify(targetStoreLocatorFacade).getPointOfService(Integer.valueOf(3215));
        assertThat(fragment).isEqualTo(ControllerConstants.Views.Fragments.Customer.LOCATION);
        verifyZeroInteractions(model);
    }

    @Test
    public void getCustomerLocationValidLocationAndStore() {
        final TargetPointOfServiceData data = new TargetPointOfServiceData();
        data.setStoreNumber(Integer.valueOf(3215));
        data.setDisplayName("Geelong");
        final ArgumentCaptor<String> storeJSON = ArgumentCaptor
                .forClass(String.class);
        given(targetStoreLocatorFacade.getPointOfService(Integer.valueOf(3215))).willReturn(data);
        fragment = customerController.getCustomerLocation(model, "3215", "", response);
        verify(model).addAttribute(eq("storeJson"), storeJSON.capture());
        final String actual = storeJSON.getValue();
        final TargetPointOfServiceData capturedData = new Gson().fromJson(actual, TargetPointOfServiceData.class);
        assertThat(capturedData.getDisplayName()).isEqualTo("Geelong");
        assertThat(capturedData.getStoreNumber()).isEqualTo(Integer.valueOf(3215));
        verifyZeroInteractions(preferredStoreCookieGenerator);
        assertThat(fragment).isEqualTo(ControllerConstants.Views.Fragments.Customer.LOCATION);
        verify(targetStoreLocatorFacade).getPointOfService(Integer.valueOf(3215));
    }

    @Test
    public void getCustomerLocationWithTypeProvided() {
        final String sanitisedPageType = "sanitised-page-type";
        given(searchQuerySanitiser.sanitiseSearchText("/page-type")).willReturn(sanitisedPageType);
        fragment = customerController.getCustomerLocation(model, "test", "/page-type", response);
        verifyZeroInteractions(targetStoreLocatorFacade);
        verifyZeroInteractions(preferredStoreCookieGenerator);
        verify(model).addAttribute("type", sanitisedPageType);
        assertThat(fragment).isEqualTo(ControllerConstants.Views.Fragments.Customer.LOCATION);
    }

    @Test
    public void getCustomerLocationWithNoSanitisedResult() {
        given(searchQuerySanitiser.sanitiseSearchText("/page-type")).willReturn("");
        fragment = customerController.getCustomerLocation(model, "test", "/page-type", response);
        verifyZeroInteractions(targetStoreLocatorFacade);
        verifyZeroInteractions(preferredStoreCookieGenerator);
        verify(model, never()).addAttribute("type", "");
        assertThat(fragment).isEqualTo(ControllerConstants.Views.Fragments.Customer.LOCATION);
    }
}
