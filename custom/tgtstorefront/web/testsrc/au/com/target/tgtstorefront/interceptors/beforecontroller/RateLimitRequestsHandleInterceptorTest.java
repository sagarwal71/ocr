/**
 * 
 */
package au.com.target.tgtstorefront.interceptors.beforecontroller;

import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class RateLimitRequestsHandleInterceptorTest {

    @Mock
    private MockHttpServletRequest request;

    @Mock
    private MockHttpServletResponse response;

    @Mock
    private MockHttpSession httpSession;

    @InjectMocks
    private final RateLimitRequestsHandleInterceptor interceptor = new RateLimitRequestsHandleInterceptor();

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    private final String featureSwitchKey = "featureSwitchKey";

    @Before
    public void setup() {
        interceptor.setRatelimitCount(3);
        interceptor.setRateLimitMethod(HttpMethod.POST.toString());
        interceptor.setRateLimitRequestPathPattern(Arrays.asList("/ws-api"));
        interceptor.setFeatureSwitchKey(featureSwitchKey);
        BDDMockito.given(Boolean.valueOf(targetFeatureSwitchFacade.isFeatureEnabled(featureSwitchKey)))
                .willReturn(Boolean.TRUE);
    }

    //Feature Switch off scenarios
    @Test
    public void testFeatureSwitchOffPreHandle() throws Exception {
        BDDMockito.given(Boolean.valueOf(targetFeatureSwitchFacade.isFeatureEnabled(featureSwitchKey)))
                .willReturn(Boolean.FALSE);
        BDDMockito.given(request.getRequestURI()).willReturn("requestUri");
        BDDMockito.given(request.getMethod()).willReturn(HttpMethod.POST.toString());
        Assertions.assertThat(interceptor.preHandle(request, response, new Object())).isTrue();

    }

    @Test
    public void testFeatureSwitchOffAfterCompletion() throws Exception {
        BDDMockito.given(Boolean.valueOf(targetFeatureSwitchFacade.isFeatureEnabled(featureSwitchKey)))
                .willReturn(Boolean.FALSE);
        BDDMockito.given(request.getRequestURI()).willReturn("requestUri");
        BDDMockito.given(request.getMethod()).willReturn(HttpMethod.POST.toString());
        interceptor.afterCompletion(request, response, new Object(), new Exception());
        verify(request, Mockito.never()).getSession();
    }

    //Feature Switch On Scenarios
    @Test
    public void testWhenUrlDoesNotMatchPreHandle() throws Exception {
        BDDMockito.given(request.getRequestURI()).willReturn("/abcd/efgh");
        BDDMockito.given(request.getMethod()).willReturn(HttpMethod.POST.toString());
        Assertions.assertThat(interceptor.preHandle(request, response, new Object())).isTrue();
    }

    @Test
    public void testWhenUrlDoesNotMatchAfterCompletion() throws Exception {
        BDDMockito.given(request.getRequestURI()).willReturn("/abcd/efgh");
        BDDMockito.given(request.getMethod()).willReturn(HttpMethod.POST.toString());
        interceptor.afterCompletion(request, response, new Object(), new Exception());
        verify(request, Mockito.never()).getSession();
    }


    @Test
    public void testWhenUrlMatchAndSessionNotContainingActiveMapPreHandle() throws Exception {
        BDDMockito.given(request.getRequestURI()).willReturn("/ws-api");
        BDDMockito.given(request.getMethod()).willReturn(HttpMethod.POST.toString());
        BDDMockito.given(request.getSession()).willReturn(httpSession);
        final ArgumentCaptor<Map> activeUrlsMap = ArgumentCaptor
                .forClass(Map.class);
        final ArgumentCaptor<String> key = ArgumentCaptor
                .forClass(String.class);
        Assertions.assertThat(interceptor.preHandle(request, response, new Object())).isTrue();
        verify(httpSession).setAttribute(key.capture(), activeUrlsMap.capture());
        Assertions.assertThat(key.getValue()).isEqualTo("actReqs");
        final Map<String, Integer> activeUrlMap = activeUrlsMap.getValue();
        final String requestKey = getActiveUrlKey(HttpMethod.POST.toString(), "/ws-api", null);
        Assertions.assertThat(activeUrlMap.get(requestKey)).isEqualTo(1);
    }

    @Test
    public void testWhenUrlMatchAndSessionContainingActiveMapWithCountOnePreHandle() throws Exception {
        BDDMockito.given(request.getRequestURI()).willReturn("/ws-api");
        BDDMockito.given(request.getMethod()).willReturn(HttpMethod.POST.toString());
        BDDMockito.given(request.getSession()).willReturn(httpSession);
        final String requestKey = getActiveUrlKey(HttpMethod.POST.toString(), "/ws-api", null);
        final Map<String, Integer> activeUrlsMapAlreadyPresent = new ConcurrentHashMap<>();
        activeUrlsMapAlreadyPresent.put(requestKey, new Integer(1));
        BDDMockito.given(httpSession.getAttribute("actReqs")).willReturn(activeUrlsMapAlreadyPresent);

        final ArgumentCaptor<Map> activeUrlsMap = ArgumentCaptor
                .forClass(Map.class);
        final ArgumentCaptor<String> key = ArgumentCaptor
                .forClass(String.class);
        Assertions.assertThat(interceptor.preHandle(request, response, new Object())).isTrue();
        verify(httpSession).setAttribute(key.capture(), activeUrlsMap.capture());
        Assertions.assertThat(key.getValue()).isEqualTo("actReqs");
        final Map<String, Integer> activeUrlMap = activeUrlsMap.getValue();
        Assertions.assertThat(activeUrlMap.get(requestKey)).isEqualTo(2);
    }

    @Test
    public void testWhenUrlMatchAndSessionContainingActiveMapWithCountAlreadyAtMaxLimitPreHandle() throws Exception {
        BDDMockito.given(request.getRequestURI()).willReturn("/ws-api");
        BDDMockito.given(request.getMethod()).willReturn(HttpMethod.POST.toString());
        BDDMockito.given(request.getSession()).willReturn(httpSession);
        final String requestKey = getActiveUrlKey(HttpMethod.POST.toString(), "/ws-api", null);
        final Map<String, Integer> activeUrlsMapAlreadyPresent = new ConcurrentHashMap<>();
        activeUrlsMapAlreadyPresent.put(requestKey, new Integer(3));
        BDDMockito.given(httpSession.getAttribute("actReqs")).willReturn(activeUrlsMapAlreadyPresent);
        Assertions.assertThat(interceptor.preHandle(request, response, new Object())).isFalse();
        verify(response).setStatus(HttpStatus.SC_FORBIDDEN);
        verify(response).sendError(HttpServletResponse.SC_FORBIDDEN);
        verify(httpSession, Mockito.never()).setAttribute(Mockito.anyString(), Mockito.anyMap());
    }


    @Test
    public void testWhenUrlMatchAndSessionContainingActiveMapWithCountOneForTheRequestedUrlWithSameTimeStamp()
            throws Exception {
        BDDMockito.given(request.getRequestURI()).willReturn("/ws-api");
        BDDMockito.given(request.getMethod()).willReturn(HttpMethod.POST.toString());
        BDDMockito.given(request.getParameter("_")).willReturn("123123");
        BDDMockito.given(request.getQueryString()).willReturn("_=123123&abcd=jack");
        BDDMockito.given(request.getSession()).willReturn(httpSession);
        final String requestKey = getActiveUrlKey(HttpMethod.POST.toString(), "/ws-api", "_=123123&abcd=jack");
        final Map<String, Integer> activeUrlsMapAlreadyPresent = new ConcurrentHashMap<>();
        activeUrlsMapAlreadyPresent.put(requestKey, new Integer(1));
        BDDMockito.given(httpSession.getAttribute("actReqs")).willReturn(activeUrlsMapAlreadyPresent);
        Assertions.assertThat(interceptor.preHandle(request, response, new Object())).isFalse();
        verify(response).setStatus(HttpStatus.SC_FORBIDDEN);
        verify(response).sendError(HttpServletResponse.SC_FORBIDDEN);
        verify(httpSession, Mockito.never()).setAttribute(Mockito.anyString(), Mockito.anyMap());
    }

    @Test
    public void testWhenUrlMatchAndSessionContainingActiveMapWithCountOneForTheRequestedUrlAfterCompletion()
            throws Exception {
        BDDMockito.given(request.getRequestURI()).willReturn("/ws-api");
        BDDMockito.given(request.getMethod()).willReturn(HttpMethod.POST.toString());
        BDDMockito.given(request.getSession()).willReturn(httpSession);
        final String requestKey = getActiveUrlKey(HttpMethod.POST.toString(), "/ws-api", null);
        final Map<String, Integer> activeUrlsMapAlreadyPresent = new ConcurrentHashMap<>();
        activeUrlsMapAlreadyPresent.put(requestKey, new Integer(1));
        BDDMockito.given(httpSession.getAttribute("actReqs")).willReturn(activeUrlsMapAlreadyPresent);

        final ArgumentCaptor<Map> activeUrlsMap = ArgumentCaptor
                .forClass(Map.class);
        final ArgumentCaptor<String> key = ArgumentCaptor
                .forClass(String.class);
        interceptor.afterCompletion(request, response, new Object(), null);
        verify(httpSession).setAttribute(key.capture(), activeUrlsMap.capture());
        Assertions.assertThat(key.getValue()).isEqualTo("actReqs");
        final Map<String, Integer> activeUrlMap = activeUrlsMap.getValue();
        Assertions.assertThat(activeUrlMap.containsKey(requestKey)).isFalse();
    }

    @Test
    public void testWhenUrlMatchAndSessionContainingActiveMapWithCountMoreThanOneForTheRequestedUrlAfterCompletion()
            throws Exception {
        BDDMockito.given(request.getRequestURI()).willReturn("/ws-api");
        BDDMockito.given(request.getMethod()).willReturn(HttpMethod.POST.toString());
        BDDMockito.given(request.getSession()).willReturn(httpSession);
        final String requestKey = getActiveUrlKey(HttpMethod.POST.toString(), "/ws-api", null);
        final Map<String, Integer> activeUrlsMapAlreadyPresent = new ConcurrentHashMap<>();
        activeUrlsMapAlreadyPresent.put(requestKey, new Integer(2));
        BDDMockito.given(httpSession.getAttribute("actReqs")).willReturn(activeUrlsMapAlreadyPresent);

        final ArgumentCaptor<Map> activeUrlsMap = ArgumentCaptor
                .forClass(Map.class);
        final ArgumentCaptor<String> key = ArgumentCaptor
                .forClass(String.class);
        interceptor.afterCompletion(request, response, new Object(), null);
        verify(httpSession).setAttribute(key.capture(), activeUrlsMap.capture());
        Assertions.assertThat(key.getValue()).isEqualTo("actReqs");
        final Map<String, Integer> activeUrlMap = activeUrlsMap.getValue();
        Assertions.assertThat(activeUrlMap.get(requestKey)).isEqualTo(1);
    }



    private String getActiveUrlKey(final String requestMethod, final String requestUri,
            final String requestQueryString) {
        return requestMethod + "-" + requestUri +
                (requestQueryString != null ? "?" + requestQueryString : "");
    }
}
