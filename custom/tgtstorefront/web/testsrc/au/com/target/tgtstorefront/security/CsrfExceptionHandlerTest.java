/**
 * 
 */
package au.com.target.tgtstorefront.security;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.csrf.InvalidCsrfTokenException;
import org.springframework.security.web.csrf.MissingCsrfTokenException;


/**
 * @author mgazal
 *
 */
@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class CsrfExceptionHandlerTest {

    @Mock
    private HttpServletRequest mockRequest;

    @Mock
    private HttpServletResponse mockResponse;

    private final String redirectUrl = "http://test";

    private final CsrfExceptionHandler csrfTokenHandler = new CsrfExceptionHandler();

    @Before
    public void before() {
        csrfTokenHandler.setRedirectUrl(redirectUrl);
    }

    @Test
    public void testInvalidCsrf() throws IOException, ServletException {
        final AccessDeniedException accessDeniedException = mock(InvalidCsrfTokenException.class);
        given(mockResponse.encodeRedirectURL(redirectUrl)).willReturn(redirectUrl);
        csrfTokenHandler.handle(mockRequest, mockResponse, accessDeniedException);
        verify(mockResponse).sendRedirect(redirectUrl);
    }

    @Test
    public void testMissingCsrf() throws IOException, ServletException {
        final AccessDeniedException accessDeniedException = mock(MissingCsrfTokenException.class);
        given(mockResponse.encodeRedirectURL(redirectUrl)).willReturn(redirectUrl);
        csrfTokenHandler.handle(mockRequest, mockResponse, accessDeniedException);
        verify(mockResponse).sendRedirect(redirectUrl);
    }

    @Test
    public void testOtherAccessDeniedException() throws IOException, ServletException {
        final AccessDeniedException accessDeniedException = mock(AccessDeniedException.class);
        given(mockResponse.encodeRedirectURL(redirectUrl)).willReturn(redirectUrl);
        csrfTokenHandler.handle(mockRequest, mockResponse, accessDeniedException);
        verify(mockResponse).sendError(403, accessDeniedException.getMessage());
        verify(mockResponse, never()).sendRedirect(redirectUrl);
    }
}
