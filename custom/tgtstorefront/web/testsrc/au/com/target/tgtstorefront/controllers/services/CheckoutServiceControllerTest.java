/**
 * 
 */
package au.com.target.tgtstorefront.controllers.services;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.MapBindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.customer.TargetCustomerSubscriptionFacade;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.order.TargetCheckoutResponseFacade;
import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;
import au.com.target.tgtfacades.order.data.ClickAndCollectDeliveryDetailData;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;
import au.com.target.tgtfacades.response.data.LoginSuccessResponseData;
import au.com.target.tgtfacades.response.data.PaymentResponseData;
import au.com.target.tgtfacades.response.data.RegisterUserResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionSource;
import au.com.target.tgtstorefront.checkout.request.dto.CncPickupDetailsDTO;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.AddressDataHelper;
import au.com.target.tgtstorefront.controllers.util.TargetPaymentHelper;
import au.com.target.tgtstorefront.controllers.util.WebServiceExceptionHelper;
import au.com.target.tgtstorefront.forms.TargetAddressForm;
import au.com.target.tgtstorefront.forms.TargetAddressForm.DomesticAddressValidationGroup;
import au.com.target.tgtstorefront.forms.TargetAddressForm.InternationalAddressValidationGroup;
import au.com.target.tgtstorefront.forms.checkout.RegisterForm;
import au.com.target.tgtstorefront.security.AutoLoginStrategy;


/**
 * Test cases for CheckoutServiceController
 * 
 * @author htan3
 * 
 */
@SuppressWarnings("deprecation")
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CheckoutServiceControllerTest {

    @InjectMocks
    @Spy
    private final CheckoutServiceController controller = new CheckoutServiceController();

    @Mock
    private TargetCheckoutResponseFacade targetCheckoutResponseFacade;

    @Mock
    private AddressDataHelper addressDataHelper;

    @Mock
    private SmartValidator validator;

    @Mock
    private TargetPaymentHelper targetPaymentHelper;

    @Mock
    private WebServiceExceptionHelper mockWebServiceExceptionHelper;

    @Mock
    private RequestAttributes attributes;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Mock
    private AutoLoginStrategy autoLoginStrategy;

    @Mock
    private TargetCustomerFacade targetCustomerFacade;

    @Mock
    private TargetCustomerSubscriptionFacade targetCustomerSubscriptionFacade;

    @Mock
    private SessionService sessionService;

    @Before
    public void setUp() {
        RequestContextHolder.setRequestAttributes(attributes);
        given(attributes.getSessionMutex()).willReturn("mutex");
    }

    private class ValidationErrorAnswer implements Answer<BindingResult> {
        @Override
        public BindingResult answer(final InvocationOnMock invocation) throws Exception {
            final BindingResult bindingResult = (BindingResult)invocation.getArguments()[1];
            bindingResult.reject("Validation Error");
            return bindingResult;
        }

    }

    @Test
    public void testGetApplicableDeliveryModes() {
        controller.getApplicableModes();
        verify(targetCheckoutResponseFacade).getApplicableDeliveryModes();
    }

    @Test
    public void verifySetDeliveryMode() {
        controller.setDeliveryMode("deliveryMode");
        verify(targetCheckoutResponseFacade).setDeliveryMode("deliveryMode");
    }

    @Test
    public void handleExceptions() {
        final HttpServletRequest request = Mockito.mock(MockHttpServletRequest.class);
        final Exception exception = new Exception();
        final Response mockResponse = mock(Response.class);
        given(mockWebServiceExceptionHelper.handleException(request, exception)).willReturn(mockResponse);
        final Response response = controller.handleException(request, exception);
        assertThat(response).isEqualTo(mockResponse);
    }


    @Test
    public void testHandleValidationErrorWithException() {
        final Response mockResponse = mock(Response.class);

        final Exception exception = Mockito.mock(Exception.class);
        given(mockWebServiceExceptionHelper.handleException(httpServletRequest, exception))
                .willReturn(mockResponse);

        final Response response = controller.handleException(httpServletRequest, exception);

        assertThat(response).isEqualTo(mockResponse);
    }

    @Test
    public void testSearchCncStore() {
        final PageableData pageableData = mock(PageableData.class);
        doReturn(pageableData).when(controller).setPageableDataForCncStore();
        controller.getCncStores("test");
        verify(targetCheckoutResponseFacade).searchCncStores("test", pageableData);
    }

    @Test
    public void verifySetPickupStore() {
        final Integer storeNumber = Integer.valueOf(1);
        controller.setClickAndCollectStore(storeNumber);
        verify(targetCheckoutResponseFacade).setCncStore(storeNumber);
    }

    @Test
    public void testGetCartDetail() {
        final Response cartDetailResponse = new Response(true);
        final CartDetailResponseData cartResponseData = new CartDetailResponseData();
        cartDetailResponse.setData(cartResponseData);
        given(targetCheckoutResponseFacade.getCartDetail()).willReturn(cartDetailResponse);
        final HttpServletRequest request = Mockito.mock(MockHttpServletRequest.class);
        final HttpSession session = Mockito.mock(MockHttpSession.class);
        given(request.getSession()).willReturn(session);
        final Response response = controller.getCartDetail(request);
        assertThat(response).isNotNull();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData()).isInstanceOfAny(CartDetailResponseData.class);
    }

    @Test
    public void testGetCartDetailWhenPlaceOrderIsInProgress() {
        final Response cartDetailResponse = new Response(true);
        final CartDetailResponseData cartResponseData = new CartDetailResponseData();
        cartDetailResponse.setData(cartResponseData);
        given(targetCheckoutResponseFacade.getCartDetail()).willReturn(cartDetailResponse);
        final HttpServletRequest request = Mockito.mock(MockHttpServletRequest.class);
        final HttpSession session = Mockito.mock(MockHttpSession.class);
        given(request.getSession()).willReturn(session);
        given(session.getAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS)).willReturn(Boolean.TRUE);

        final Response response = controller.getCartDetail(request);
        assertThat(response).isNotNull();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData()).isInstanceOfAny(BaseResponseData.class);
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_PLACEORDER_IN_PROGRESS");
    }

    @Test
    public void verifyGetCartSummary() {
        controller.getCartSummary();
        verify(targetCheckoutResponseFacade).getCartSummary();
    }

    @Test
    public void testGetDeliveryAddressesWithNoDeliveryMode() {
        controller.getSavedAddresses();
        verify(targetCheckoutResponseFacade).getDeliveryAddresses();
    }

    @Test
    public void testApplyTmd() {
        final String tmdNumber = "tmd";
        controller.applyTmd(tmdNumber);
        verify(targetCheckoutResponseFacade).applyTmd(tmdNumber);
    }

    @Test
    public void verifySetClickAndCollectPickupDetails() {
        final CncPickupDetailsDTO cncDetail = Mockito.mock(CncPickupDetailsDTO.class);
        controller.setClickAndCollectPickupDetails(cncDetail);
        verify(targetCheckoutResponseFacade)
                .setCncPickupDetails(Mockito.any(ClickAndCollectDeliveryDetailData.class));
    }

    @Test
    public void verifyApplyVoucher() {
        final String voucherCode = "A1234";
        controller.applyVoucher(voucherCode);
        verify(targetCheckoutResponseFacade).applyVoucher(voucherCode);
    }

    @Test
    public void verifyRemoveVoucher() {
        controller.removeVoucher();
        verify(targetCheckoutResponseFacade).removeVoucher();
    }

    @Test
    public void verifySetAddress() {
        final String addressId = "addressId";
        controller.setDeliveryAddress(addressId);
        verify(targetCheckoutResponseFacade).selectDeliveryAddress(addressId);
    }

    @Test
    public void verifyGetApplicablePaymentModes() {
        controller.getApplicablePaymentModes();
        verify(targetCheckoutResponseFacade).getApplicablePaymentMethods();
    }

    @Test
    public void createDeliveryAddressForCompleteFormWithoutValidationFailure() throws BindException {
        final TargetAddressForm addressForm = mock(TargetAddressForm.class);
        final BindingResult bindingResult = mock(BindingResult.class);
        final TargetAddressData targetAddressData = mock(TargetAddressData.class);
        given(addressDataHelper.createTargetAustralianShippingAddressData(addressForm)).willReturn(targetAddressData);
        willReturn(Boolean.FALSE).given(bindingResult).hasErrors();

        controller.createDeliveryAddress(addressForm, bindingResult);
        verify(addressDataHelper).createTargetAustralianShippingAddressData(addressForm);
        verify(targetCheckoutResponseFacade).createAddress(targetAddressData);
    }

    @Test
    public void testSearchAddress() {
        doReturn(Integer.valueOf(10)).when(addressDataHelper).getMaxSearchSizeForAddressLookup();
        controller.searchAddress("address");
        verify(targetCheckoutResponseFacade).searchAddress("address", 10);
    }

    @Test(expected = BindException.class)
    public void createDeliveryAddressForCommonValidationFailure() throws BindException {
        final TargetAddressForm addressForm = mock(TargetAddressForm.class);
        final BindingResult bindingResult = new MapBindingResult(new HashMap<String, String>(), "targetAddressForm");
        Mockito.doAnswer(new ValidationErrorAnswer()).when(validator)
                .validate(addressForm, bindingResult, TargetAddressForm.ContactValidationGroup.class);
        final TargetAddressData targetAddressData = mock(TargetAddressData.class);

        controller.createDeliveryAddress(addressForm, bindingResult);
        verify(addressDataHelper, Mockito.times(0)).createTargetAustralianShippingAddressData(addressForm);
        verify(targetCheckoutResponseFacade, Mockito.times(0)).createAddress(targetAddressData);
    }

    @Test(expected = BindException.class)
    public void createDeliveryAddressForCompleteFormWithValidationFailure() throws BindException {
        final TargetAddressForm addressForm = mock(TargetAddressForm.class);
        final BindingResult bindingResult = new MapBindingResult(new HashMap<String, String>(), "targetAddressForm");
        Mockito.doAnswer(new ValidationErrorAnswer()).when(validator)
                .validate(addressForm, bindingResult, DomesticAddressValidationGroup.class);
        final TargetAddressData targetAddressData = mock(TargetAddressData.class);

        controller.createDeliveryAddress(addressForm, bindingResult);
        verify(addressDataHelper, Mockito.times(0)).createTargetAustralianShippingAddressData(addressForm);
        verify(targetCheckoutResponseFacade, Mockito.times(0)).createAddress(targetAddressData);
    }

    @Test
    public void createDeliveryAddressForValidatedAddress() throws BindException {
        final TargetAddressForm addressForm = mock(TargetAddressForm.class);
        given(addressForm.getSingleLineId()).willReturn("singleLineId");
        given(addressForm.getSingleLineLabel()).willReturn("singleLineLabel");
        final BindingResult bindingResult = mock(BindingResult.class);
        final TargetAddressData targetAddressData = mock(TargetAddressData.class);
        given(addressDataHelper.createVerifiedTargetAustralianShippingAddressData(addressForm)).willReturn(
                targetAddressData);

        controller.createDeliveryAddress(addressForm, bindingResult);
        verify(addressDataHelper).createVerifiedTargetAustralianShippingAddressData(addressForm);
        verify(targetCheckoutResponseFacade).createAddress(targetAddressData);
    }

    @Test
    public void setIpgPaymentMode() {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        doReturn("https://www.target.com.au").when(controller).getDomainUrl(request);
        controller.setIpgPaymentMode("ipg", request);
        verify(targetCheckoutResponseFacade).removeExistingPayment();
        verify(targetCheckoutResponseFacade).setIpgPaymentMode("ipg",
                "https://www.target.com.au/checkout/spc-return-ipg");
    }

    @Test
    public void verifyApplyFlybuys() {
        controller.applyFlybuys("FlyBuysNumber");
        verify(targetCheckoutResponseFacade).applyFlybuys("FlyBuysNumber");
    }

    @Test(expected = IllegalArgumentException.class)
    public void setPaypalPaymentModeWithNullRequest() {
        controller.setPaypalPaymentMode(null);
    }

    @Test
    public void setPaypalPaymentModeWhenResponseFacadeCallHasNoData() {
        final Response response = new Response(true);
        final HttpServletRequest request = mock(HttpServletRequest.class);
        given(targetPaymentHelper.getBaseSecureUrl()).willReturn(
                "https://www.target.com.au");
        given(targetCheckoutResponseFacade.setPaypalPaymentMode(Mockito.anyString(), Mockito.anyString())).willReturn(
                response);
        assertThat(controller.setPaypalPaymentMode(request)).isSameAs(response);
    }

    @Test
    public void setPaypalPaymentModeWhenResponseFacadeCallIsNotSuccess() {
        final Response response = new Response(false);
        final BaseResponseData baseResponseData = new BaseResponseData();
        baseResponseData.setError(new au.com.target.tgtfacades.response.data.Error("ERROR"));
        response.setData(baseResponseData);
        final HttpServletRequest request = mock(HttpServletRequest.class);
        given(targetPaymentHelper.getBaseSecureUrl()).willReturn(
                "https://www.target.com.au");
        given(targetCheckoutResponseFacade.setPaypalPaymentMode(Mockito.anyString(), Mockito.anyString())).willReturn(
                response);
        assertThat(controller.setPaypalPaymentMode(request)).isSameAs(response);
    }

    @Test
    public void setPaypalPaymentModeWhenResponseFacadeCallHasNoSessionToken() {
        final Response mockResponse = new Response(true);
        final PaymentResponseData paymentResponseData = new PaymentResponseData();
        mockResponse.setData(paymentResponseData);
        final HttpServletRequest request = mock(HttpServletRequest.class);
        given(targetPaymentHelper.getBaseSecureUrl()).willReturn(
                "https://www.target.com.au");
        given(targetCheckoutResponseFacade.setPaypalPaymentMode(Mockito.anyString(), Mockito.anyString())).willReturn(
                mockResponse);
        final Response response = controller.setPaypalPaymentMode(request);
        final PaymentResponseData responseData = (PaymentResponseData)response.getData();
        assertThat(responseData.getPaypalSessionToken()).isNull();
    }

    @Test
    public void setPaypalPaymentModeWhenResponseFacadeCallContainsSessionToken() {
        final Response mockResponse = new Response(true);
        final PaymentResponseData paymentResponseData = new PaymentResponseData();
        paymentResponseData.setPaypalSessionToken("EC-7E42147767118710X");
        mockResponse.setData(paymentResponseData);
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpSession session = mock(HttpSession.class);
        given(request.getSession()).willReturn(session);
        given(targetPaymentHelper.getBaseSecureUrl()).willReturn(
                "https://www.target.com.au");
        given(targetPaymentHelper.getPayPalHostedPaymentFormBaseUrl()).willReturn(
                "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&amp;useraction=commit&amp;token=");
        given(targetCheckoutResponseFacade.setPaypalPaymentMode(Mockito.anyString(), Mockito.anyString())).willReturn(
                mockResponse);
        final Response response = controller.setPaypalPaymentMode(request);
        final PaymentResponseData responseData = (PaymentResponseData)response.getData();
        assertThat(responseData.getPaypalSessionToken()).isEqualTo("EC-7E42147767118710X");
        assertThat(responseData.getPaypalUrl()).isEqualTo(
                "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&amp;useraction=commit&amp;token=EC-7E42147767118710X");
    }

    @Test
    public void verifyPaypalUrls() {
        final HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        final Response mockResponse = new Response(true);
        given(targetPaymentHelper.getBaseSecureUrl()).willReturn(
                "https://www.target.com.au");
        given(targetCheckoutResponseFacade.setPaypalPaymentMode(Mockito.anyString(), Mockito.anyString())).willReturn(
                mockResponse);
        controller.setPaypalPaymentMode(mockRequest);
        verify(targetCheckoutResponseFacade).setPaypalPaymentMode(
                "https://www.target.com.au/checkout/payment-details/paypal-return/spc",
                "https://www.target.com.au/spc");
    }

    @Test
    public void verifyPaypalUrlsWithFeatureSwitchToNewSpcOrderPage() {
        final HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        final Response mockResponse = new Response(true);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isSpcLoginAndCheckout();
        given(targetPaymentHelper.getBaseSecureUrl()).willReturn(
                "https://www.target.com.au");
        given(targetCheckoutResponseFacade.setPaypalPaymentMode(Mockito.anyString(), Mockito.anyString())).willReturn(
                mockResponse);
        controller.setPaypalPaymentMode(mockRequest);
        verify(targetCheckoutResponseFacade).setPaypalPaymentMode(
                "https://www.target.com.au/checkout/payment-details/paypal-return/spc",
                "https://www.target.com.au/spc/order");
    }

    @Test
    public void setAfterpayPaymentModeWhenResponseFacadeCallHasNoData() {
        final Response response = new Response(true);
        final HttpServletRequest request = mock(HttpServletRequest.class);
        given(targetPaymentHelper.getBaseSecureUrl()).willReturn("https://www.target.com.au");
        given(targetCheckoutResponseFacade.setAfterpayPaymentMode(Mockito.anyString(), Mockito.anyString()))
                .willReturn(response);
        assertThat(controller.setAfterpayPaymentMode(request)).isSameAs(response);
    }

    @Test
    public void setAfterpayPaymentModeWhenResponseFacadeCallIsNotSuccess() {
        final Response response = new Response(false);
        final BaseResponseData baseResponseData = new BaseResponseData();
        baseResponseData.setError(new au.com.target.tgtfacades.response.data.Error("ERROR"));
        response.setData(baseResponseData);
        final HttpServletRequest request = mock(HttpServletRequest.class);
        given(targetPaymentHelper.getBaseSecureUrl()).willReturn("https://www.target.com.au");
        given(targetCheckoutResponseFacade.setAfterpayPaymentMode(Mockito.anyString(), Mockito.anyString()))
                .willReturn(response);
        assertThat(controller.setAfterpayPaymentMode(request)).isSameAs(response);
    }

    @Test
    public void setAfterpayPaymentModeWhenResponseFacadeCallHasNoSessionToken() {
        final Response mockResponse = new Response(true);
        final PaymentResponseData paymentResponseData = new PaymentResponseData();
        mockResponse.setData(paymentResponseData);
        final HttpServletRequest request = mock(HttpServletRequest.class);
        given(targetPaymentHelper.getBaseSecureUrl()).willReturn(
                "https://www.target.com.au");
        given(targetCheckoutResponseFacade.setAfterpayPaymentMode(Mockito.anyString(), Mockito.anyString())).willReturn(
                mockResponse);
        final Response response = controller.setAfterpayPaymentMode(request);
        final PaymentResponseData responseData = (PaymentResponseData)response.getData();
        assertThat(responseData.getAfterpaySessionToken()).isNull();
    }

    @Test
    public void setAfterpayPaymentModeWhenResponseFacadeCallContainsSessionToken() {
        final Response mockResponse = new Response(true);
        final PaymentResponseData paymentResponseData = new PaymentResponseData();
        paymentResponseData.setAfterpaySessionToken("q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu");
        mockResponse.setData(paymentResponseData);
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpSession session = mock(HttpSession.class);
        given(request.getSession()).willReturn(session);
        given(targetPaymentHelper.getBaseSecureUrl()).willReturn(
                "https://www.target.com.au");
        given(targetCheckoutResponseFacade.setAfterpayPaymentMode(Mockito.anyString(), Mockito.anyString())).willReturn(
                mockResponse);
        final Response response = controller.setAfterpayPaymentMode(request);
        final PaymentResponseData responseData = (PaymentResponseData)response.getData();
        assertThat(responseData.getAfterpaySessionToken())
                .isEqualTo("q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu");
        verify(session).setAttribute(ControllerConstants.SESSION_PARAM_AFTERPAY_TOKEN,
                "q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu");
    }

    @Test
    public void verifyAfterpayUrls() {
        final HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        final Response mockResponse = new Response(true);
        given(targetPaymentHelper.getBaseSecureUrl()).willReturn(
                "https://www.target.com.au");
        given(targetCheckoutResponseFacade.setAfterpayPaymentMode(Mockito.anyString(), Mockito.anyString())).willReturn(
                mockResponse);
        controller.setAfterpayPaymentMode(mockRequest);
        verify(targetCheckoutResponseFacade).setAfterpayPaymentMode(
                "https://www.target.com.au/checkout/payment-details/afterpay-return/spc",
                "https://www.target.com.au/checkout/payment-details/afterpay-return-cancel/spc");
    }

    @Test
    public void verifyAfterpayUrlsWithFeatureSwitchToNewSpcOrderPage() {
        final HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        final Response mockResponse = new Response(true);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isSpcLoginAndCheckout();
        given(targetPaymentHelper.getBaseSecureUrl()).willReturn(
                "https://www.target.com.au");
        given(targetCheckoutResponseFacade.setAfterpayPaymentMode(Mockito.anyString(), Mockito.anyString())).willReturn(
                mockResponse);
        controller.setAfterpayPaymentMode(mockRequest);
        verify(targetCheckoutResponseFacade).setAfterpayPaymentMode(
                "https://www.target.com.au/checkout/payment-details/afterpay-return/spc",
                "https://www.target.com.au/checkout/payment-details/afterpay-return-cancel/spc");
    }

    @Test
    public void testCreateBillingAddressForValidatedAddress() throws BindException {
        final TargetAddressForm addressForm = mock(TargetAddressForm.class);
        given(addressForm.getSingleLineId()).willReturn("singleLineId");
        given(addressForm.getSingleLineLabel()).willReturn("singleLineLabel");
        final BindingResult bindingResult = mock(BindingResult.class);
        final TargetAddressData targetAddressData = mock(TargetAddressData.class);
        given(addressDataHelper.createVerifiedTargetAustralianBillingAddressData(addressForm)).willReturn(
                targetAddressData);

        controller.createBillingAddress(addressForm, bindingResult);
        verify(addressDataHelper).createVerifiedTargetAustralianBillingAddressData(addressForm);
        verify(targetCheckoutResponseFacade).createBillingAddress(targetAddressData);
    }

    @Test
    public void testCreateBillingAddressForCompleteFormWithoutValidationFailure() throws BindException {
        final TargetAddressForm addressForm = mock(TargetAddressForm.class);
        final BindingResult bindingResult = mock(BindingResult.class);
        final TargetAddressData targetAddressData = mock(TargetAddressData.class);
        given(addressDataHelper.createTargetBillingAddressData(addressForm)).willReturn(targetAddressData);
        willReturn(Boolean.FALSE).given(bindingResult).hasErrors();

        controller.createBillingAddress(addressForm, bindingResult);
        verify(addressDataHelper).createTargetBillingAddressData(addressForm);
        verify(targetCheckoutResponseFacade).createBillingAddress(targetAddressData);
    }

    @Test(expected = BindException.class)
    public void testCreateBillingAddressWithValidationFailure() throws BindException {
        final TargetAddressForm addressForm = mock(TargetAddressForm.class);
        final BindingResult bindingResult = mock(BindingResult.class);
        final TargetAddressData targetAddressData = mock(TargetAddressData.class);
        given(addressDataHelper.createTargetBillingAddressData(addressForm)).willReturn(targetAddressData);
        willReturn(Boolean.TRUE).given(bindingResult).hasErrors();
        controller.createBillingAddress(addressForm, bindingResult);
    }

    @Test
    public void testCreateInternationalBillingAddress() throws BindException {
        final TargetAddressForm addressForm = mock(TargetAddressForm.class);
        final BindingResult bindingResult = mock(BindingResult.class);
        final TargetAddressData targetAddressData = mock(TargetAddressData.class);
        given(addressForm.getCountryCode()).willReturn("NZ");
        given(addressForm.getInternationalPostalcode()).willReturn("11234");
        given(addressDataHelper.createTargetBillingAddressData(addressForm)).willReturn(targetAddressData);
        controller.createBillingAddress(addressForm, bindingResult);
        verify(validator).validate(addressForm, bindingResult, InternationalAddressValidationGroup.class);
    }

    @Test
    public void testCreateDomesticBillingAddress() throws BindException {
        final TargetAddressForm addressForm = mock(TargetAddressForm.class);
        final BindingResult bindingResult = mock(BindingResult.class);
        final TargetAddressData targetAddressData = mock(TargetAddressData.class);
        given(addressForm.getCountryCode()).willReturn("AU");
        given(addressDataHelper.createTargetBillingAddressData(addressForm)).willReturn(targetAddressData);
        controller.createBillingAddress(addressForm, bindingResult);
        verify(validator).validate(addressForm, bindingResult, DomesticAddressValidationGroup.class);
    }

    @Test
    public void verifyRemoveFlybuys() {
        controller.removeFlybuys();
        verify(targetCheckoutResponseFacade).removeFlybuys();
    }

    @Test
    public void setBillingAddress() {
        controller.setBillingAddress("addressId");
        verify(targetCheckoutResponseFacade).selectBillingAddress("addressId");
    }

    @Test
    public void getIpgStatus() {
        controller.getIpgStatus();
        verify(targetCheckoutResponseFacade).getIpgPaymentStatus();
    }

    @Test
    public void testThankYou() {
        final HttpSession session = mock(HttpSession.class);
        given(httpServletRequest.getSession()).willReturn(session);
        given(session.getAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE)).willReturn("1234");
        final AdjustedCartEntriesData sohUpdates = mock(AdjustedCartEntriesData.class);
        given(session.getAttribute(ControllerConstants.SOH_UPDATES)).willReturn(sohUpdates);
        final Response expectedResponse = mock(Response.class);
        given(targetCheckoutResponseFacade.getOrderDetail("1234", sohUpdates)).willReturn(expectedResponse);
        final Response response = controller.thankYou(httpServletRequest, "1234");
        verify(session).getAttribute(ControllerConstants.SOH_UPDATES);
        verify(session).removeAttribute(ControllerConstants.SOH_UPDATES);
        assertThat(response).isEqualTo(expectedResponse);
    }

    @Test
    public void testThankYouForInvalidOrderId() {
        final HttpSession session = mock(HttpSession.class);
        given(httpServletRequest.getSession()).willReturn(session);
        given(session.getAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE)).willReturn("4321");
        final Response response = controller.thankYou(httpServletRequest, "1234");
        verify(session, never()).getAttribute(ControllerConstants.SOH_UPDATES);
        verify(session, never()).removeAttribute(ControllerConstants.SOH_UPDATES);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isInstanceOf(LoginSuccessResponseData.class);
        assertThat(((LoginSuccessResponseData)response.getData()).getRedirectUrl()).isEqualTo("/basket");
    }

    @Test
    public void testThankYouWhenNoOrderInSession() {
        final HttpSession session = mock(HttpSession.class);
        given(httpServletRequest.getSession()).willReturn(session);
        final Response response = controller.thankYou(httpServletRequest, "1234");
        verify(session, never()).getAttribute(ControllerConstants.SOH_UPDATES);
        verify(session, never()).removeAttribute(ControllerConstants.SOH_UPDATES);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isInstanceOf(LoginSuccessResponseData.class);
        assertThat(((LoginSuccessResponseData)response.getData()).getRedirectUrl()).isEqualTo("/basket");
    }

    @Test
    public void testRegister() {
        final HttpSession session = mock(HttpSession.class);
        given(httpServletRequest.getSession()).willReturn(session);
        given(session.getAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE)).willReturn("1234");
        final Response expectedResponse = mock(Response.class);
        willReturn(Boolean.TRUE).given(expectedResponse).isSuccess();
        final RegisterUserResponseData data = mock(RegisterUserResponseData.class);
        given(data.getEmail()).willReturn("user@company.com");
        given(expectedResponse.getData()).willReturn(data);
        final CustomerData customer = mock(CustomerData.class);
        given(targetCustomerFacade.getCurrentCustomer()).willReturn(customer);
        given(customer.getUid()).willReturn("user@company.com");
        given(customer.getFirstName()).willReturn("first");
        given(customer.getLastName()).willReturn("last");
        given(targetCheckoutResponseFacade.registerCustomer("1234", "password")).willReturn(expectedResponse);
        final RegisterForm registerForm = mock(RegisterForm.class);
        given(registerForm.getPassword()).willReturn("password");
        willReturn(Boolean.TRUE).given(registerForm).isOptIntoMarketing();

        final Response response = controller.register("1234", registerForm, httpServletRequest, httpServletResponse);
        assertThat(response).isEqualTo(expectedResponse);
        verify(autoLoginStrategy).login("user@company.com", "password", httpServletRequest, httpServletResponse);
        final ArgumentCaptor<TargetCustomerSubscriptionRequestDto> subscriptionCaptor = ArgumentCaptor
                .forClass(TargetCustomerSubscriptionRequestDto.class);
        verify(targetCustomerSubscriptionFacade).performCustomerSubscription(subscriptionCaptor.capture());
        final TargetCustomerSubscriptionRequestDto subscriptionRequestDto = subscriptionCaptor.getValue();
        assertThat(subscriptionRequestDto).isNotNull();
        assertThat(subscriptionRequestDto.getCustomerEmail()).isEqualTo("user@company.com");
        assertThat(subscriptionRequestDto.getFirstName()).isEqualTo("first");
        assertThat(subscriptionRequestDto.getLastName()).isEqualTo("last");
        assertThat(subscriptionRequestDto.isRegisterForEmail()).isTrue();
        assertThat(subscriptionRequestDto.getCustomerSubscriptionSource())
                .isEqualTo(TargetCustomerSubscriptionSource.WEBCHECKOUT);
    }

    @Test
    public void testRegisterFail() {
        final HttpSession session = mock(HttpSession.class);
        given(httpServletRequest.getSession()).willReturn(session);
        given(session.getAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE)).willReturn("1234");
        final Response expectedResponse = mock(Response.class);
        willReturn(Boolean.FALSE).given(expectedResponse).isSuccess();
        given(targetCheckoutResponseFacade.registerCustomer("1234", "password")).willReturn(expectedResponse);
        final RegisterForm registerForm = mock(RegisterForm.class);
        given(registerForm.getPassword()).willReturn("password");

        final Response response = controller.register("1234", registerForm, httpServletRequest, httpServletResponse);
        assertThat(response).isEqualTo(expectedResponse);
        verify(autoLoginStrategy, never()).login(Mockito.anyString(), Mockito.anyString(),
                Mockito.any(HttpServletRequest.class), Mockito.any(HttpServletResponse.class));
        verify(targetCustomerSubscriptionFacade, never())
                .performCustomerSubscription(Mockito.any(TargetCustomerSubscriptionRequestDto.class));
    }

    @Test
    public void testThankYouCheck() {
        final Response expectedResponse = mock(Response.class);
        given(targetCheckoutResponseFacade.checkoutProblem()).willReturn(expectedResponse);
        final Response response = controller.thankYouCheck();
        assertThat(response).isEqualTo(expectedResponse);
        verify(sessionService).removeAttribute(TgtCoreConstants.SESSION_POSTCODE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setAterpayPaymentModeWithNullRequest() {
        controller.setAfterpayPaymentMode(null);
    }

    // Added for Zip Pay
    @Test
    public void setZippayPaymentModeWhenResponseFacadeCallHasNoData() {
        final Response response = new Response(true);
        final HttpServletRequest request = mock(HttpServletRequest.class);
        given(targetPaymentHelper.getBaseSecureUrl()).willReturn("https://www.target.com.au");
        given(targetCheckoutResponseFacade.setZippayPaymentMode(Mockito.anyString()))
                .willReturn(response);
        assertThat(controller.setZippayPaymentMode(request)).isSameAs(response);
    }

    @Test
    public void setZippayPaymentModeWhenResponseFacadeCallIsNotSuccess() {
        final Response response = new Response(false);
        final BaseResponseData baseResponseData = new BaseResponseData();
        baseResponseData.setError(new au.com.target.tgtfacades.response.data.Error("ERROR"));
        response.setData(baseResponseData);
        final HttpServletRequest request = mock(HttpServletRequest.class);
        given(targetPaymentHelper.getBaseSecureUrl()).willReturn("https://www.target.com.au");
        given(targetCheckoutResponseFacade.setZippayPaymentMode(Mockito.anyString()))
                .willReturn(response);
        assertThat(controller.setZippayPaymentMode(request)).isSameAs(response);
    }

    @Test
    public void setZippayPaymentModeWhenResponseFacadeCallHasNoSessionTokenAndRedirectUrl() {
        final Response mockResponse = new Response(true);
        final PaymentResponseData paymentResponseData = new PaymentResponseData();
        mockResponse.setData(paymentResponseData);
        final HttpServletRequest request = mock(HttpServletRequest.class);
        given(targetPaymentHelper.getBaseSecureUrl()).willReturn(
                "https://www.target.com.au");
        given(targetCheckoutResponseFacade.setZippayPaymentMode(Mockito.anyString())).willReturn(
                mockResponse);
        final Response response = controller.setZippayPaymentMode(request);
        final PaymentResponseData responseData = (PaymentResponseData)response.getData();
        assertThat(responseData.getZipPaymentSessionToken()).isNull();
    }

    @Test
    public void setZippayPaymentModeWhenResponseFacadeCallContainsSessionTokenAndRedirectUrl() {
        final Response mockResponse = new Response(true);
        final PaymentResponseData paymentResponseData = new PaymentResponseData();
        paymentResponseData.setZipPaymentSessionToken("co_NNb4qU1DCqE1Dvu2kqcTJ0");
        paymentResponseData.setZipPaymentRedirectUrl(
                "https://account.sandbox.zipmoney.com.au/?co=co_NNb4qU1DCqE1Dvu2kqcTJ0&m=59bff7d0-5bfa-4d03-ab91-a071ecc5c64e");
        mockResponse.setData(paymentResponseData);
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpSession session = mock(HttpSession.class);
        given(request.getSession()).willReturn(session);
        given(targetPaymentHelper.getBaseSecureUrl()).willReturn(
                "https://www.target.com.au");
        given(targetCheckoutResponseFacade.setZippayPaymentMode(Mockito.anyString())).willReturn(
                mockResponse);
        final Response response = controller.setZippayPaymentMode(request);
        final PaymentResponseData responseData = (PaymentResponseData)response.getData();
        assertThat(responseData.getZipPaymentSessionToken())
                .isEqualTo("co_NNb4qU1DCqE1Dvu2kqcTJ0");
        assertThat(responseData.getZipPaymentRedirectUrl())
                .isEqualTo(
                        "https://account.sandbox.zipmoney.com.au/?co=co_NNb4qU1DCqE1Dvu2kqcTJ0&m=59bff7d0-5bfa-4d03-ab91-a071ecc5c64e");
        verify(session).setAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN,
                "co_NNb4qU1DCqE1Dvu2kqcTJ0");
    }
}
