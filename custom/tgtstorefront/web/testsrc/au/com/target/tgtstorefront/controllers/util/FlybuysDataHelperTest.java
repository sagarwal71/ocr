/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import de.hybris.bootstrap.annotations.UnitTest;

import java.text.SimpleDateFormat;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.voucher.data.TargetFlybuysLoginData;
import au.com.target.tgtstorefront.forms.checkout.FlybuysLoginForm;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FlybuysDataHelperTest {

    @InjectMocks
    private final FlybuysDataHelper helper = new FlybuysDataHelper();

    @Mock
    private TargetCheckoutFacade checkoutFacade;

    @Mock
    private TargetCartData cartData;

    private final SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");

    @Before
    public void setup() {
        BDDMockito.given(cartData.getFlybuysNumber()).willReturn("9876543210");
    }

    @Test
    public void testConvertToFlybuysLoginData() {
        final FlybuysLoginForm flybuysLoginForm = new FlybuysLoginForm();
        flybuysLoginForm.setBirthDay("11");
        flybuysLoginForm.setBirthMonth("02");
        flybuysLoginForm.setBirthYear("1988");
        flybuysLoginForm.setPostCode("3000");
        BDDMockito.given(checkoutFacade.getCheckoutCart()).willReturn(cartData);
        final TargetFlybuysLoginData result = helper.convertToFlybuysLoginData(flybuysLoginForm);
        Assert.assertEquals("9876543210", result.getCardNumber());
        Assert.assertEquals("3000", result.getPostCode());
        Assert.assertEquals(sdf.format(ThreePartDateHelper.convertToDate(flybuysLoginForm.getBirthDay(),
                flybuysLoginForm.getBirthMonth(),
                flybuysLoginForm.getBirthYear())), sdf.format(result.getDateOfBirth()));
    }
}
