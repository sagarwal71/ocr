/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtcore.flybuys.dto.response.FlybuysAuthenticateResponseDto;
import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedemptionData;
import au.com.target.tgtfacades.flybuys.data.FlybuysRequestStatusData;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.TargetPlaceOrderFacade;
import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.order.TargetLaybyCommerceCheckoutService;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.AddressDataHelper;
import au.com.target.tgtstorefront.controllers.util.TargetPaymentHelper;
import au.com.target.tgtstorefront.forms.checkout.AbstractPaymentDetailsForm;
import au.com.target.tgtstorefront.forms.checkout.BillingAddressOnlyForm;
import au.com.target.tgtstorefront.forms.checkout.ExistingCardPaymentForm;
import au.com.target.tgtstorefront.forms.checkout.FlybuysLoginForm;
import au.com.target.tgtstorefront.forms.checkout.FlybuysRedeemForm;
import au.com.target.tgtstorefront.forms.checkout.NewCardPaymentDetailsForm;
import au.com.target.tgtstorefront.forms.checkout.VendorPaymentForm;
import au.com.target.tgtstorefront.forms.validation.validator.BillingAddressStateValidator;
import au.com.target.tgtstorefront.util.PageTitleResolver;
import org.junit.Assert;


/**
 * @author gbaker2
 * 
 */
public class CheckoutPaymentControllerTest {

    @Mock
    protected MessageSource mockMessageSource;
    @Mock
    protected I18NService mockI18nService;
    @Mock
    private TargetCheckoutFacade mockTargetCheckoutFacade;

    @Mock
    private TargetPlaceOrderFacade mockTargetPlaceOrderFacade;

    @Mock
    private RequestAttributes requestAttributes;

    @Mock
    private FlybuysDiscountFacade flybuysDiscountFacade;

    @Mock
    private SessionService sessionService;

    @Mock
    private SalesApplicationFacade salesApplicationFacade;

    @Mock
    private TargetCommerceCartService targetCommerceCartService;

    @Mock
    private TargetLaybyCartService targetCartService;

    @Mock
    private TargetLaybyCommerceCheckoutService targetCommerceCheckoutService;

    @Mock
    private TargetPaymentHelper targetPaymentHelper;

    @Mock
    private CMSPageService cmsPageService;

    @Mock
    private PageTitleResolver pageTitleResolver;

    @Mock
    private BillingAddressStateValidator billingAddressStateValidator;

    @Mock
    private HttpSession session;

    @Mock
    private AddressDataHelper addressDataHelper;

    @InjectMocks
    private final CheckoutPaymentController checkoutPaymentController = new CheckoutPaymentController();

    private TargetCartData mockCartData;

    @Mock
    private HttpServletRequest request;

    @Before
    public void startUp() throws CMSItemNotFoundException {
        MockitoAnnotations.initMocks(this);
        mockCartData = Mockito.mock(TargetCartData.class);
        BDDMockito.given(mockTargetCheckoutFacade.getCheckoutCart()).willReturn(mockCartData);

        // Setup messages
        BDDMockito.given(mockMessageSource
                .getMessage("payment.voucherCode.savings", new Object[] { "$12.00" },
                        mockI18nService.getCurrentLocale()))
                .willReturn("Savings $12.00");
        BDDMockito.given(mockMessageSource
                .getMessage("payment.voucherCode.applied", new Object[] { "XYZ-123" },
                        mockI18nService.getCurrentLocale()))
                .willReturn("Applied");
        BDDMockito.given(mockMessageSource
                .getMessage("payment.voucherCode.invalid", null,
                        mockI18nService.getCurrentLocale()))
                .willReturn("Invalid");
        BDDMockito.given(requestAttributes.getSessionMutex()).willReturn(session);
        BDDMockito.given(request.getSession()).willReturn(session);
        RequestContextHolder.setRequestAttributes(requestAttributes);

        // Flybuys
        BDDMockito.given(mockMessageSource
                .getMessage("checkout.error.flybuys.flybuysothererror", null,
                        mockI18nService.getCurrentLocale()))
                .willReturn("flybuys_other_error");

        final List<OrderEntryData> orderEntries = new ArrayList<>();
        final OrderEntryData orderEntry = new OrderEntryData();
        orderEntries.add(orderEntry);
        final AddressData addressData = new AddressData();
        final DeliveryModeData deliveryMode = new DeliveryModeData();
        final ContentPageModel pageModel = mock(ContentPageModel.class);

        when(mockCartData.getEntries()).thenReturn(orderEntries);
        when(mockCartData.getDeliveryAddress()).thenReturn(addressData);
        when(mockCartData.getDeliveryMode()).thenReturn(deliveryMode);
        when(cmsPageService.getPageForLabelOrId(anyString())).thenReturn(pageModel);

    }

    @SuppressWarnings("boxing")
    @Test
    public void testValidateVoucher() throws CMSItemNotFoundException {
        final String voucher = "XYZ-123";
        final Model model = new ExtendedModelMap();
        final PriceData priceData = new PriceData();
        priceData.setFormattedValue("$12.00");
        BDDMockito.given(mockTargetCheckoutFacade.getFirstAppliedVoucher()).willReturn(null);
        BDDMockito.given(mockTargetCheckoutFacade.applyVoucher(voucher)).willReturn(Boolean.TRUE);
        BDDMockito.given(mockTargetCheckoutFacade.getAppliedVoucherPrice(voucher)).willReturn(priceData);
        Assert.assertEquals(checkoutPaymentController.validateVoucher(model, voucher),
                ControllerConstants.Views.Pages.MultiStepCheckout.VALIDATE_VOUCHER_PAGE);
        Assert.assertTrue((Boolean)model.asMap().get("isVoucherValid"));
        Assert.assertEquals(voucher, model.asMap().get("appliedVoucher"));
        Assert.assertEquals(model.asMap().get("cartData"), mockCartData);
        Assert.assertEquals(model.asMap().get("voucherSuccessMsg"), "Applied Savings $12.00");
        Assert.assertNull(model.asMap().get("errorMsg"));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testValidateVoucherMissingSavings() throws CMSItemNotFoundException {
        final String voucher = "XYZ-123";
        final Model model = new ExtendedModelMap();
        BDDMockito.given(mockTargetCheckoutFacade.getFirstAppliedVoucher()).willReturn(null);
        BDDMockito.given(mockTargetCheckoutFacade.applyVoucher(voucher)).willReturn(Boolean.TRUE);
        BDDMockito.given(mockTargetCheckoutFacade.getAppliedVoucherPrice(voucher)).willReturn(null);
        Assert.assertEquals(checkoutPaymentController.validateVoucher(model, voucher),
                ControllerConstants.Views.Pages.MultiStepCheckout.VALIDATE_VOUCHER_PAGE);
        Assert.assertTrue((Boolean)model.asMap().get("isVoucherValid"));
        Assert.assertEquals(voucher, model.asMap().get("appliedVoucher"));
        Assert.assertEquals(model.asMap().get("cartData"), mockCartData);
        Assert.assertEquals(model.asMap().get("voucherSuccessMsg"), "Applied");
        Assert.assertNull(model.asMap().get("errorMsg"));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testValidateVoucherNotValid() throws CMSItemNotFoundException {
        final String voucher = "XYZ-123";
        final Model model = new ExtendedModelMap();
        BDDMockito.given(mockTargetCheckoutFacade.getFirstAppliedVoucher()).willReturn(null);
        BDDMockito.given(mockTargetCheckoutFacade.applyVoucher(voucher)).willReturn(Boolean.FALSE);
        Assert.assertEquals(checkoutPaymentController.validateVoucher(model, voucher),
                ControllerConstants.Views.Pages.MultiStepCheckout.VALIDATE_VOUCHER_PAGE);
        Assert.assertFalse((Boolean)model.asMap().get("isVoucherValid"));
        Assert.assertNull(model.asMap().get("appliedVoucher"));
        Assert.assertEquals(model.asMap().get("cartData"), mockCartData);
        Assert.assertEquals(model.asMap().get("errorMsg"), "Invalid");
        Assert.assertNull(model.asMap().get("voucherSuccessMsg"));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testValidateVoucherBadPattern() throws CMSItemNotFoundException {
        final String voucher = "xyz-?^90$3";
        final Model model = new ExtendedModelMap();
        BDDMockito.given(mockTargetCheckoutFacade.getFirstAppliedVoucher()).willReturn(null);
        BDDMockito.given(mockTargetCheckoutFacade.applyVoucher(voucher)).willReturn(Boolean.FALSE);
        Assert.assertEquals(checkoutPaymentController.validateVoucher(model, voucher),
                ControllerConstants.Views.Pages.MultiStepCheckout.VALIDATE_VOUCHER_PAGE);
        Assert.assertFalse((Boolean)model.asMap().get("isVoucherValid"));
        Assert.assertNull(model.asMap().get("appliedVoucher"));
        Assert.assertEquals(model.asMap().get("cartData"), mockCartData);
        Assert.assertEquals(model.asMap().get("errorMsg"), "Invalid");
        Assert.assertNull(model.asMap().get("voucherSuccessMsg"));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testValidateWontApplySecondVoucher() throws CMSItemNotFoundException {
        final String voucher = "XYZ-986";
        final String additionalVoucher = "XYZ-123";
        final Model model = new ExtendedModelMap();
        BDDMockito.given(mockTargetCheckoutFacade.getFirstAppliedVoucher()).willReturn(voucher);
        Assert.assertEquals(
                checkoutPaymentController.validateVoucher(model, additionalVoucher),
                ControllerConstants.Views.Pages.MultiStepCheckout.VALIDATE_VOUCHER_PAGE);
        Mockito.verify(mockTargetCheckoutFacade, Mockito.never()).applyVoucher(Mockito.anyString());
        Assert.assertFalse((Boolean)model.asMap().get("isVoucherValid"));
        Assert.assertNull(model.asMap().get("appliedVoucher"));
        Assert.assertEquals(model.asMap().get("cartData"), mockCartData);
        Assert.assertEquals(model.asMap().get("errorMsg"), "Invalid");
        Assert.assertNull(model.asMap().get("voucherSuccessMsg"));
    }

    @Test
    public void testRemoveVoucher() throws CMSItemNotFoundException {
        final String voucher = "XYZ-123";
        final Model model = new ExtendedModelMap();
        BDDMockito.given(mockTargetCheckoutFacade.getFirstAppliedVoucher()).willReturn(voucher);
        Assert.assertEquals(checkoutPaymentController.removeVoucher(model),
                ControllerConstants.Views.Pages.MultiStepCheckout.REMOVE_VOUCHER_PAGE);
        Mockito.verify(mockTargetCheckoutFacade, Mockito.times(1)).removeVoucher(voucher);
        Assert.assertEquals(model.asMap().get("cartData"), mockCartData);
    }

    @Test
    public void testRemoveNullVoucher() throws CMSItemNotFoundException {
        final Model model = new ExtendedModelMap();
        BDDMockito.given(mockTargetCheckoutFacade.getFirstAppliedVoucher()).willReturn(null);
        Assert.assertEquals(checkoutPaymentController.removeVoucher(model),
                ControllerConstants.Views.Pages.MultiStepCheckout.REMOVE_VOUCHER_PAGE);
        Mockito.verify(mockTargetCheckoutFacade, Mockito.never()).removeVoucher(null);
        Assert.assertEquals(model.asMap().get("cartData"), mockCartData);
    }


    @SuppressWarnings("boxing")
    @Test
    public void testAuthenticateFlybuysSubmitSuccess() {

        final String dummyMonth = "03";
        final String dummyYear = "2014";
        final String dummyDay = "31";


        final String dummyFLybuysCardNumber = "12345678912345667";
        final String dummyPostCode = "3000";
        final BindingResult bindingResult = Mockito.mock(BindingResult.class);
        final FlybuysRequestStatusData flybuysRequestStatusData = new FlybuysRequestStatusData();
        final FlybuysAuthenticateResponseDto flybuysAuthenticateResponseDto = Mockito
                .mock(FlybuysAuthenticateResponseDto.class);
        final TargetCartData mockTargetCartData = Mockito.mock(TargetCartData.class);
        final Model mockModel = Mockito.mock(ExtendedModelMap.class);
        final FlybuysLoginForm flybuysLoginForm = new FlybuysLoginForm();

        flybuysLoginForm.setBirthDay(dummyDay);
        flybuysLoginForm.setBirthMonth(dummyMonth);
        flybuysLoginForm.setBirthYear(dummyYear);
        flybuysLoginForm.setCardNumber(dummyFLybuysCardNumber);
        flybuysLoginForm.setPostCode(dummyPostCode);
        BDDMockito.given(bindingResult.hasErrors()).willReturn(false);
        BDDMockito.given(flybuysAuthenticateResponseDto.getResponse()).willReturn(FlybuysResponseType.SUCCESS);
        BDDMockito.given(mockTargetCheckoutFacade.getCheckoutCart()).willReturn(mockTargetCartData);
        BDDMockito.given(mockTargetCartData.getFlybuysNumber()).willReturn(dummyFLybuysCardNumber);
        flybuysRequestStatusData.setResponse(FlybuysResponseType.SUCCESS);

        BDDMockito.given(
                flybuysDiscountFacade.getFlybuysRedemptionData(Mockito.eq(dummyFLybuysCardNumber),
                        Mockito.any(Date.class),
                        Mockito.eq(dummyPostCode)))
                .willReturn(flybuysRequestStatusData);

        Assert.assertEquals(
                checkoutPaymentController.authenticateFlybuysSubmit(mockModel, flybuysLoginForm, bindingResult),
                ControllerConstants.Views.Fragments.Flybuys.FLYBUYS_LOGIN_FORM_RESULT);

        Assert.assertEquals(flybuysRequestStatusData.getErrorMessage(), null);

        BDDMockito.verify(mockModel).addAttribute("flybuysRequestStatusData", flybuysRequestStatusData);
        BDDMockito.verify(mockModel).addAttribute("success", Boolean.TRUE);

    }

    @SuppressWarnings({ "unused", "boxing" })
    @Test
    public void testAuthenticateFlybuysSubmitError() {

        final String dummyMonth = "08";
        final String dummyYear = "2016";
        final String dummyDay = "31";

        final String dummyFLybuysCardNumber = "22345678912345667";
        final String dummyPostCode = "3009";
        final BindingResult bindingResult = Mockito.mock(BindingResult.class);
        final FlybuysRequestStatusData flybuysRequestStatusData = new FlybuysRequestStatusData();
        final FlybuysAuthenticateResponseDto flybuysAuthenticateResponseDto = Mockito
                .mock(FlybuysAuthenticateResponseDto.class);
        final TargetCartData mockTargetCartData = Mockito.mock(TargetCartData.class);
        final Model mockModel = Mockito.mock(ExtendedModelMap.class);
        final FlybuysLoginForm flybuysLoginForm = new FlybuysLoginForm();

        flybuysLoginForm.setBirthDay(dummyDay);
        flybuysLoginForm.setBirthMonth(dummyMonth);
        flybuysLoginForm.setBirthYear(dummyYear);
        flybuysLoginForm.setCardNumber(dummyFLybuysCardNumber);
        flybuysLoginForm.setPostCode(dummyPostCode);

        BDDMockito.given(bindingResult.hasErrors()).willReturn(false);
        BDDMockito.given(flybuysAuthenticateResponseDto.getResponse()).willReturn(
                FlybuysResponseType.FLYBUYS_OTHER_ERROR);
        BDDMockito.given(mockTargetCheckoutFacade.getCheckoutCart()).willReturn(mockTargetCartData);
        BDDMockito.given(mockTargetCartData.getFlybuysNumber()).willReturn(dummyFLybuysCardNumber);
        flybuysRequestStatusData.setResponse(FlybuysResponseType.FLYBUYS_OTHER_ERROR);

        BDDMockito.given(
                flybuysDiscountFacade.getFlybuysRedemptionData(Mockito.eq(dummyFLybuysCardNumber),
                        Mockito.any(Date.class),
                        Mockito.eq(dummyPostCode)))
                .willReturn(flybuysRequestStatusData);



        Assert.assertEquals(
                checkoutPaymentController.authenticateFlybuysSubmit(mockModel, flybuysLoginForm, bindingResult),
                ControllerConstants.Views.Fragments.Flybuys.FLYBUYS_LOGIN_FORM_RESULT);

        Assert.assertNotNull(flybuysRequestStatusData.getErrorMessage());
        BDDMockito.verify(mockModel).addAttribute("flybuysRequestStatusData", flybuysRequestStatusData);
        BDDMockito.verify(mockModel).addAttribute("success", Boolean.FALSE);


    }

    @SuppressWarnings({ "unused", "boxing" })
    @Test
    public void testAuthenticateFlybuysResultHasErrors() {

        final String dummyMonth = "09";
        final String dummyYear = "2019";
        final String dummyDay = "30";

        final String dummyFLybuysCardNumber = "32345678912345667";
        final String dummyPostCode = "3010";
        final BindingResult bindingResult = Mockito.mock(BindingResult.class);
        final FlybuysRequestStatusData flybuysRequestStatusData = new FlybuysRequestStatusData();
        final Model mockModel = Mockito.mock(ExtendedModelMap.class);
        final FlybuysLoginForm flybuysLoginForm = new FlybuysLoginForm();
        flybuysLoginForm.setBirthDay(dummyDay);
        flybuysLoginForm.setBirthMonth(dummyMonth);
        flybuysLoginForm.setBirthYear(dummyYear);
        flybuysLoginForm.setCardNumber(dummyFLybuysCardNumber);
        flybuysLoginForm.setPostCode(dummyPostCode);
        BDDMockito.given(bindingResult.hasErrors()).willReturn(true);
        Assert.assertEquals(
                checkoutPaymentController.authenticateFlybuysSubmit(mockModel, flybuysLoginForm, bindingResult),
                ControllerConstants.Views.Fragments.Flybuys.FLYBUYS_LOGIN_FORM_RESULT);
        Assert.assertNull(flybuysRequestStatusData.getErrorMessage());
        BDDMockito.verify(mockModel).addAttribute("success", Boolean.FALSE);

    }

    @SuppressWarnings("boxing")
    @Test
    public void testApplyFlybuysReedemTierOptionWithBindingError() {
        final FlybuysRedeemForm flybuysRedeemForm = new FlybuysRedeemForm();
        final Model model = new ExtendedModelMap();

        final BindingResult bindingResult = mock(BindingResult.class);
        given(bindingResult.hasErrors()).willReturn(Boolean.TRUE);

        final FlybuysRedemptionData flybuysRedemptionData = new FlybuysRedemptionData();
        given(flybuysDiscountFacade.getFlybuysRedemptionDataForCheckoutCart()).willReturn(flybuysRedemptionData);

        final String result = checkoutPaymentController.applyFlybuysReedemTierOption(model, flybuysRedeemForm,
                bindingResult);

        assertThat(result).isEqualTo(ControllerConstants.Views.Fragments.Flybuys.FLYBUYS_SELECT_OPTION_RESULT);
        assertThat(model.asMap().get("flybuysRedeemForm")).isEqualTo(flybuysRedeemForm);
        assertThat(model.asMap().get("success")).isEqualTo(Boolean.FALSE);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testApplyFlybuysReedemTierOptionWithRemoveDiscountFailure() {
        final FlybuysRedeemForm flybuysRedeemForm = new FlybuysRedeemForm();
        flybuysRedeemForm.setRedeemCode("remove");

        final Model model = new ExtendedModelMap();

        final BindingResult bindingResult = mock(BindingResult.class);
        given(bindingResult.hasErrors()).willReturn(Boolean.FALSE);

        final FlybuysRedemptionData flybuysRedemptionData = new FlybuysRedemptionData();
        given(flybuysDiscountFacade.getFlybuysRedemptionDataForCheckoutCart()).willReturn(flybuysRedemptionData);

        given(flybuysDiscountFacade.removeFlybuysDiscountFromCheckoutCart()).willReturn(Boolean.FALSE);

        final String result = checkoutPaymentController.applyFlybuysReedemTierOption(model, flybuysRedeemForm,
                bindingResult);

        assertThat(result).isEqualTo(ControllerConstants.Views.Fragments.Flybuys.FLYBUYS_SELECT_OPTION_RESULT);
        assertThat(model.asMap().get("flybuysRedeemForm")).isEqualTo(flybuysRedeemForm);
        assertThat(model.asMap().get("success")).isEqualTo(Boolean.FALSE);
        assertThat(model.asMap().get("cartData")).isEqualTo(mockCartData);

        verify(flybuysDiscountFacade).removeFlybuysDiscountFromCheckoutCart();
        verify(flybuysDiscountFacade, never()).applyFlybuysDiscountToCheckoutCart(anyString());

        verify(bindingResult).rejectValue("redeemCode", "payment.flybuys.redeem.label.voucher");
    }

    @SuppressWarnings("boxing")
    @Test
    public void testApplyFlybuysReedemTierOptionWithRemoveDiscountSuccess() {
        final FlybuysRedeemForm flybuysRedeemForm = new FlybuysRedeemForm();
        flybuysRedeemForm.setRedeemCode("remove");

        final Model model = new ExtendedModelMap();

        final BindingResult bindingResult = mock(BindingResult.class);
        given(bindingResult.hasErrors()).willReturn(Boolean.FALSE);

        final FlybuysRedemptionData flybuysRedemptionData = new FlybuysRedemptionData();
        given(flybuysDiscountFacade.getFlybuysRedemptionDataForCheckoutCart()).willReturn(flybuysRedemptionData);

        given(flybuysDiscountFacade.removeFlybuysDiscountFromCheckoutCart()).willReturn(Boolean.TRUE);

        final String result = checkoutPaymentController.applyFlybuysReedemTierOption(model, flybuysRedeemForm,
                bindingResult);

        assertThat(result).isEqualTo(ControllerConstants.Views.Fragments.Flybuys.FLYBUYS_SELECT_OPTION_RESULT);
        assertThat(model.asMap().get("flybuysRedeemForm")).isEqualTo(flybuysRedeemForm);
        assertThat(model.asMap().get("success")).isEqualTo(Boolean.TRUE);
        assertThat(model.asMap().get("cartData")).isEqualTo(mockCartData);

        verify(flybuysDiscountFacade).removeFlybuysDiscountFromCheckoutCart();
        verify(flybuysDiscountFacade, never()).applyFlybuysDiscountToCheckoutCart(anyString());

        verify(bindingResult, never()).rejectValue("redeemCode", "payment.flybuys.redeem.label.voucher");
    }

    @SuppressWarnings("boxing")
    @Test
    public void testApplyFlybuysReedemTierOptionWithApplyDiscountFailure() {
        final String redeemCode = "redeemCode";

        final FlybuysRedeemForm flybuysRedeemForm = new FlybuysRedeemForm();
        flybuysRedeemForm.setRedeemCode(redeemCode);

        final Model model = new ExtendedModelMap();

        final BindingResult bindingResult = mock(BindingResult.class);
        given(bindingResult.hasErrors()).willReturn(Boolean.FALSE);

        final FlybuysRedemptionData flybuysRedemptionData = new FlybuysRedemptionData();
        given(flybuysDiscountFacade.getFlybuysRedemptionDataForCheckoutCart()).willReturn(flybuysRedemptionData);

        given(flybuysDiscountFacade.applyFlybuysDiscountToCheckoutCart(redeemCode)).willReturn(Boolean.FALSE);

        final String result = checkoutPaymentController.applyFlybuysReedemTierOption(model, flybuysRedeemForm,
                bindingResult);

        assertThat(result).isEqualTo(ControllerConstants.Views.Fragments.Flybuys.FLYBUYS_SELECT_OPTION_RESULT);
        assertThat(model.asMap().get("flybuysRedeemForm")).isEqualTo(flybuysRedeemForm);
        assertThat(model.asMap().get("success")).isEqualTo(Boolean.FALSE);
        assertThat(model.asMap().get("cartData")).isEqualTo(mockCartData);

        verify(flybuysDiscountFacade, never()).removeFlybuysDiscountFromCheckoutCart();
        verify(flybuysDiscountFacade).applyFlybuysDiscountToCheckoutCart(redeemCode);

        verify(bindingResult).rejectValue("redeemCode", "payment.flybuys.redeem.label.voucher");
    }

    @SuppressWarnings("boxing")
    @Test
    public void testApplyFlybuysReedemTierOptionWithApplyDiscountSuccess() {
        final String redeemCode = "redeemCode";

        final FlybuysRedeemForm flybuysRedeemForm = new FlybuysRedeemForm();
        flybuysRedeemForm.setRedeemCode(redeemCode);

        final Model model = new ExtendedModelMap();

        final BindingResult bindingResult = mock(BindingResult.class);
        given(bindingResult.hasErrors()).willReturn(Boolean.FALSE);

        final FlybuysRedemptionData flybuysRedemptionData = new FlybuysRedemptionData();
        given(flybuysDiscountFacade.getFlybuysRedemptionDataForCheckoutCart()).willReturn(flybuysRedemptionData);

        given(flybuysDiscountFacade.applyFlybuysDiscountToCheckoutCart(redeemCode)).willReturn(Boolean.TRUE);

        final String result = checkoutPaymentController.applyFlybuysReedemTierOption(model, flybuysRedeemForm,
                bindingResult);

        assertThat(result).isEqualTo(ControllerConstants.Views.Fragments.Flybuys.FLYBUYS_SELECT_OPTION_RESULT);
        assertThat(model.asMap().get("flybuysRedeemForm")).isEqualTo(flybuysRedeemForm);
        assertThat(model.asMap().get("success")).isEqualTo(Boolean.TRUE);
        assertThat(model.asMap().get("cartData")).isEqualTo(mockCartData);

        verify(flybuysDiscountFacade, never()).removeFlybuysDiscountFromCheckoutCart();
        verify(flybuysDiscountFacade).applyFlybuysDiscountToCheckoutCart(redeemCode);

        verify(bindingResult, never()).rejectValue("redeemCode", "payment.flybuys.redeem.label.voucher");
    }

    @SuppressWarnings("boxing")
    @Test
    public void testValidateFlybuysNumberWhenFlybuysIsNotSavedInCart() {
        final String flybuysnumber = "12345";
        final Model model = new ExtendedModelMap();
        given(mockTargetCheckoutFacade.isFlybuysDiscountAlreadyApplied(flybuysnumber)).willReturn(true);
        checkoutPaymentController.validateFlybuysNumber(model, flybuysnumber);
        assertThat(model.asMap().get("flybuysalreadyapplied")).isEqualTo(true);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testValidateFlybuysNumberWhenFlybuysIsSavedInCart() {
        final String flybuysnumber = "12345";
        final Model model = new ExtendedModelMap();
        given(mockTargetCheckoutFacade.isFlybuysDiscountAlreadyApplied(flybuysnumber)).willReturn(false);
        checkoutPaymentController.validateFlybuysNumber(model, flybuysnumber);
        assertThat(model.asMap().get("flybuysalreadyapplied")).isNull();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testChoosePaymentWhenIPGEnabled() throws CMSItemNotFoundException {
        final Model model = mock(Model.class);
        final RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
        when(targetPaymentHelper.checkIfIPGFeatureIsEnabled()).thenReturn(true);

        checkoutPaymentController.doChoosePaymentMethod(model, redirectAttributes);
        verify(targetPaymentHelper).checkIfIPGFeatureIsEnabled();
        verify(targetPaymentHelper, times(0)).getTNSSessionToken(model);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testChoosePaymentWhenIPGIsDisabled() throws CMSItemNotFoundException {
        final Model model = mock(Model.class);
        final RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
        when(targetPaymentHelper.checkIfIPGFeatureIsEnabled()).thenReturn(false);
        final PurchaseOptionModel purchaseOptionModel = mock(PurchaseOptionModel.class);
        when(mockTargetCheckoutFacade.getActivePurchaseOption()).thenReturn(purchaseOptionModel);
        checkoutPaymentController.doChoosePaymentMethod(model, redirectAttributes);
        verify(targetPaymentHelper).checkIfIPGFeatureIsEnabled();
        verify(targetPaymentHelper, times(1)).getTNSSessionToken(model);
        verify(model).addAttribute("multiplePaymentMode", true);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testIPGPostWithBadAddress() throws Exception {
        final Model model = mock(Model.class);
        final RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
        when(targetPaymentHelper.checkIfIPGFeatureIsEnabled()).thenReturn(true);
        final BillingAddressOnlyForm form = new BillingAddressOnlyForm();
        final BindingResult result = mock(BindingResult.class);
        when(result.hasErrors()).thenReturn(true);
        when(this.salesApplicationFacade.getCurrentSalesApplication()).thenReturn(SalesApplication.WEB);

        final String forwardPage = checkoutPaymentController.billingPost(model, redirectAttributes, form, result);
        verify(targetPaymentHelper).populatePaymentDefaultForms(any(Model.class),
                any(ExistingCardPaymentForm.class), any(VendorPaymentForm.class), any(NewCardPaymentDetailsForm.class),
                any(TargetCartData.class),
                any(AbstractPaymentDetailsForm.class));
        verify(model).addAttribute("multiplePaymentMode", true);
        assertEquals(ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_PAYMENT_METHOD_PAGE, forwardPage);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testKioskPostWithBadAddress() throws Exception {
        final Model model = mock(Model.class);
        final RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
        when(targetPaymentHelper.checkIfIPGFeatureIsEnabled()).thenReturn(true);
        final BillingAddressOnlyForm form = new BillingAddressOnlyForm();
        final BindingResult result = mock(BindingResult.class);
        when(result.hasErrors()).thenReturn(true);
        when(this.salesApplicationFacade.getCurrentSalesApplication()).thenReturn(SalesApplication.KIOSK);

        final String forwardPage = checkoutPaymentController.billingPost(model, redirectAttributes, form, result);
        verify(targetPaymentHelper, times(0)).populatePaymentDefaultForms(any(Model.class),
                any(ExistingCardPaymentForm.class), any(VendorPaymentForm.class), any(NewCardPaymentDetailsForm.class),
                any(TargetCartData.class),
                any(AbstractPaymentDetailsForm.class));
        verify(model).addAttribute("multiplePaymentMode", false);
        assertEquals(ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_PAYMENT_METHOD_PAGE, forwardPage);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testIPGPostWithGoodAddress() throws Exception {
        final Model model = mock(Model.class);
        final RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
        when(targetPaymentHelper.checkIfIPGFeatureIsEnabled()).thenReturn(true);
        final BillingAddressOnlyForm form = new BillingAddressOnlyForm();
        form.setCardType("creditcard");
        final BindingResult result = mock(BindingResult.class);
        when(result.hasErrors()).thenReturn(false);

        final String forwardPage = checkoutPaymentController.billingPost(model, redirectAttributes, form, result);
        assertEquals(ControllerConstants.Redirection.CHECKOUT_ORDER_SUMMARY, forwardPage);
        verify(mockTargetCheckoutFacade).createIpgPaymentInfo(any(TargetAddressData.class),
                any(IpgPaymentTemplateType.class));
        verify(mockTargetCheckoutFacade).setPaymentMode(TgtFacadesConstants.IPG);
    }

    @Test
    public void testGiftPostWithGoodAddress() throws Exception {
        final Model model = mock(Model.class);
        final RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
        when(Boolean.valueOf(targetPaymentHelper.checkIfIPGFeatureIsEnabled())).thenReturn(Boolean.TRUE);
        final BillingAddressOnlyForm form = new BillingAddressOnlyForm();
        form.setCardType("giftcard");
        final BindingResult result = mock(BindingResult.class);
        when(Boolean.valueOf(result.hasErrors())).thenReturn(Boolean.FALSE);

        final String forwardPage = checkoutPaymentController.giftCardBillingPost(model, redirectAttributes, form,
                result);
        assertEquals(ControllerConstants.Redirection.CHECKOUT_ORDER_SUMMARY, forwardPage);
        verify(mockTargetCheckoutFacade).createIpgPaymentInfo(any(TargetAddressData.class),
                any(IpgPaymentTemplateType.class));
        verify(mockTargetCheckoutFacade).setPaymentMode(TgtFacadesConstants.GIFT_CARD);
    }

    @Test
    public void testIsIpgPaymentWhenPaymentInfoIsNotTargetCCPaymentInfoData() {
        final CCPaymentInfoData paymentInfoData = mock(CCPaymentInfoData.class);
        final boolean isIpg = checkoutPaymentController.isIpgPayment(paymentInfoData);
        assertFalse(isIpg);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testIsIpgPaymentWhenPaymentInfoIsTargetCCPaymentInfoDataNotIpg() {
        final TargetCCPaymentInfoData paymentInfoData = mock(TargetCCPaymentInfoData.class);
        given(paymentInfoData.isIpgPaymentInfo()).willReturn(false);
        final boolean isIpg = checkoutPaymentController.isIpgPayment(paymentInfoData);
        assertFalse(isIpg);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testIsIpgPaymentWhenPaymentInfoIsTargetCCPaymentInfoDataIpg() {
        final TargetCCPaymentInfoData paymentInfoData = mock(TargetCCPaymentInfoData.class);
        given(paymentInfoData.isIpgPaymentInfo()).willReturn(true);
        final boolean isIpg = checkoutPaymentController.isIpgPayment(paymentInfoData);
        assertTrue(isIpg);
    }

    @Test
    public void testReverseExistingGiftCardPaymentsWhenIpgSessionIsNull() {
        final TargetCartData cartData = mock(TargetCartData.class);
        given(mockTargetCheckoutFacade.getIpgSessionTokenFromCart(cartData)).willReturn(null);
        checkoutPaymentController.reverseExistingGiftCardPayments(cartData);
        verify(mockTargetPlaceOrderFacade, times(0)).reverseGiftCardPayment();
    }

    @Test
    public void testReverseExistingGiftCardPaymentsWhenIpgSessionIsEmpty() {
        final TargetCartData cartData = mock(TargetCartData.class);
        given(mockTargetCheckoutFacade.getIpgSessionTokenFromCart(cartData)).willReturn("");
        checkoutPaymentController.reverseExistingGiftCardPayments(cartData);
        verify(mockTargetPlaceOrderFacade, times(0)).reverseGiftCardPayment();
    }

    @Test
    public void testReverseExistingGiftCardPaymentsWhenValidIpgSession() {
        final TargetCartData cartData = mock(TargetCartData.class);
        given(mockTargetCheckoutFacade.getIpgSessionTokenFromCart(cartData)).willReturn("123456");
        checkoutPaymentController.reverseExistingGiftCardPayments(cartData);
        verify(mockTargetPlaceOrderFacade, times(1)).reverseGiftCardPayment();
    }
}