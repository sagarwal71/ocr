/**
 * 
 */
package au.com.target.tgtstorefront.controllers.mobileapp;

import static org.mockito.Mockito.verify;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.configuration.Configuration;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.assemble.impl.EndecaAutoSuggestAssemble;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaWrapperException;
import au.com.target.endeca.infront.transform.TargetEndecaURLHelper;
import au.com.target.tgtstorefront.controllers.pages.preferences.StoreViewPreferencesHandler;
import au.com.target.tgtstorefront.util.SearchQuerySanitiser;

import com.endeca.infront.assembler.ContentItem;


/**
 * @author pthoma20
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class MobileAppSearchAutoSuggestControllerTest {

    @Mock
    private SearchQuerySanitiser searchQuerySanitiser;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private StoreViewPreferencesHandler storeViewPreferencesHandler;

    @Mock
    private TargetEndecaURLHelper targetEndecaURLHelper;

    @Mock
    private EndecaAutoSuggestAssemble endecaAutoSuggestAssemble;

    @Mock
    private Configuration mockConfiguration;

    @InjectMocks
    private final MobileAppSearchAutoSuggestController mobileSearchAutoSuggestController = new MobileAppSearchAutoSuggestController();


    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
        BDDMockito.given(configurationService.getConfiguration()).willReturn(mockConfiguration);
        BDDMockito.given(mockConfiguration.getInteger(Mockito.anyString(),
                Mockito.any(Integer.class)))
                .willReturn(Integer.valueOf(10));
    }

    @Test
    public void testAutoCompleteWhenSuccessful() throws TargetEndecaWrapperException {
        BDDMockito.given(searchQuerySanitiser.sanitiseSearchText("bi@cyc!le")).willReturn("bicycle");
        final EndecaSearchStateData endecaSearchStateDataMock = Mockito.mock(EndecaSearchStateData.class);
        BDDMockito.given(
                targetEndecaURLHelper.buildEndecaSearchStateData(null, Arrays.asList("bicycle"), null, 0, 0, null,
                        null, null, null))
                .willReturn(endecaSearchStateDataMock);
        final ContentItem contentItem = Mockito.mock(ContentItem.class);
        BDDMockito.given(endecaAutoSuggestAssemble.assemble(request, endecaSearchStateDataMock))
                .willReturn(contentItem);
        final ContentItem contentItemResult = mobileSearchAutoSuggestController.getAutocompleteSuggestions("bi@cyc!le",
                request, response);

        Assertions.assertThat(contentItem).isEqualTo(contentItemResult);


    }

    @SuppressWarnings("boxing")
    @Test
    public void testAutoCompleteWhenSearchThrowsException() throws TargetEndecaWrapperException {
        BDDMockito.given(searchQuerySanitiser.sanitiseSearchText("bi@cyc!le")).willReturn("bicycle");
        final EndecaSearchStateData endecaSearchStateDataMock = Mockito.mock(EndecaSearchStateData.class);
        BDDMockito.given(
                targetEndecaURLHelper.buildEndecaSearchStateData(null, Arrays.asList("bicycle"), null, 0, 0, null,
                        null, null, null))
                .willReturn(endecaSearchStateDataMock);
        BDDMockito.given(endecaAutoSuggestAssemble.assemble(request, endecaSearchStateDataMock))
                .willThrow(new TargetEndecaWrapperException());
        mobileSearchAutoSuggestController.getAutocompleteSuggestions("bi@cyc!le",
                request, response);
        final ArgumentCaptor<Integer> argument = ArgumentCaptor.forClass(Integer.class);
        verify(response).setStatus(argument.capture());
        Assertions.assertThat(argument.getValue()).isEqualTo(HttpServletResponse.SC_SERVICE_UNAVAILABLE);


    }

    @SuppressWarnings("boxing")
    @Test
    public void testAutoCompleteWhenEmptySearchTermIsProvided() throws TargetEndecaWrapperException {
        mobileSearchAutoSuggestController.getAutocompleteSuggestions("bi@cyc!le",
                request, response);
        final ArgumentCaptor<Integer> argument = ArgumentCaptor.forClass(Integer.class);
        verify(response).setStatus(argument.capture());
        Assertions.assertThat(argument.getValue()).isEqualTo(HttpServletResponse.SC_BAD_REQUEST);
    }
}
