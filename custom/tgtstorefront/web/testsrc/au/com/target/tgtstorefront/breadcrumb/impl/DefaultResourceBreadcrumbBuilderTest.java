/**
 * 
 */
package au.com.target.tgtstorefront.breadcrumb.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.List;
import java.util.Locale;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;

import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.controllers.pages.data.BreadcrumbData;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
@UnitTest
public class DefaultResourceBreadcrumbBuilderTest {

    @InjectMocks
    private final DefaultResourceBreadcrumbBuilder defaultResourceBreadcrumbBuilder = new DefaultResourceBreadcrumbBuilder() {
        @Override
        protected MessageSource getMessageSource() {
            return messageSource;
        }
    };

    @Mock
    private I18NService i18nService;

    @Mock
    private MessageSource messageSource;

    private final Locale locale = Locale.ENGLISH;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        BDDMockito.given(i18nService.getCurrentLocale()).willReturn(locale);
        defaultResourceBreadcrumbBuilder.setParentBreadcrumbResourceKey("parentKey");
    }

    @Test
    public void generateBreadcrumbFromNullString() {
        final List<Breadcrumb> breadCrumb = defaultResourceBreadcrumbBuilder.getBreadcrumbs();
        Assert.assertTrue(breadCrumb.size() == 1);
    }

    @Test
    public void generateBreadcrumbFromOneString() {
        final List<Breadcrumb> breadCrumb = defaultResourceBreadcrumbBuilder.getBreadcrumbs("FirstMessageRessourceKey");
        Assert.assertTrue(breadCrumb.size() == 2);
    }

    @Test
    public void generateBreadcrumbFromTwoBreadcrumdata() {
        final List<Breadcrumb> breadCrumb = defaultResourceBreadcrumbBuilder.getBreadcrumbs(new BreadcrumbData(
                "FirstMessageRessourceKey"), new BreadcrumbData("SecondMessageRessourceKey"));
        Assert.assertTrue(breadCrumb.size() == 3);
    }
}
