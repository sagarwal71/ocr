/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


public class RequestHeadersHelperTest {

    private static final String IP_HEADER = "True-Client-IP";

    private static final String IP = "10.10.10.10";

    @InjectMocks
    private final RequestHeadersHelper requestHeadersHelper = new RequestHeadersHelper();

    @Mock
    private HttpServletRequest request;

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
        requestHeadersHelper.setClientIPHeaderName(IP_HEADER);

    }

    @Test
    public void testGetRemoteIPAddressNone() {

        BDDMockito.given(request.getHeader(IP_HEADER)).willReturn(null);
        BDDMockito.given(request.getRemoteAddr()).willReturn(null);

        final String result = requestHeadersHelper.getRemoteIPAddress(request);
        Assert.assertNull(result);
    }

    @Test
    public void testGetRemoteIPAddressWithHeader() {

        BDDMockito.given(request.getHeader(IP_HEADER)).willReturn(IP);
        BDDMockito.given(request.getRemoteAddr()).willReturn(null);

        final String result = requestHeadersHelper.getRemoteIPAddress(request);
        Assert.assertNotNull(result);
        Assert.assertEquals(IP, result);
    }

    @Test
    public void testGetRemoteIPAddressWithoutHeader() {

        BDDMockito.given(request.getHeader(IP_HEADER)).willReturn(null);
        BDDMockito.given(request.getRemoteAddr()).willReturn(IP);

        final String result = requestHeadersHelper.getRemoteIPAddress(request);
        Assert.assertNotNull(result);
        Assert.assertEquals(IP, result);
    }

    @Test
    public void testXhrRequestFalse() {
        BDDMockito.given(request.getHeader(RequestHeadersHelper.X_REQUESTED_WITH)).willReturn(null);
        Assert.assertFalse(requestHeadersHelper.isXhrRequest(request));
    }

    @Test
    public void testXhrRequestTrue() {
        BDDMockito.given(request.getHeader(RequestHeadersHelper.X_REQUESTED_WITH)).willReturn(RequestHeadersHelper.XHR);
        Assert.assertTrue(requestHeadersHelper.isXhrRequest(request));
    }


}
