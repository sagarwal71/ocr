/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.pages;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.endeca.infront.dto.DeliveryModePostcodeDto;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.flybuys.dao.impl.FlybuysRedeemConfigDaoImpl;
import au.com.target.tgtcore.flybuys.service.FlybuysRedeemConfigService;
import au.com.target.tgtcore.order.TargetCommerceCartModificationStatus;
import au.com.target.tgtfacades.config.TargetSharedConfigFacade;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedeemConfigData;
import au.com.target.tgtfacades.order.TargetCartFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.payment.AfterpayConfigFacade;
import au.com.target.tgtfacades.payment.data.AfterpayConfigData;
import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtstorefront.breadcrumb.ResourceBreadcrumbBuilder;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.AbstractPageController.PageType;
import au.com.target.tgtstorefront.controllers.util.CartDataHelper;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.data.MessageKeyArguments;
import au.com.target.tgtstorefront.forms.UpdateQuantityForm;
import au.com.target.tgtstorefront.util.PageTitleResolver;
import au.com.target.tgtwebcore.constants.TgtwebcoreConstants;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;
import au.com.target.tgtwebcore.servicelayer.services.TargetCMSPageService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CartPageControllerTest {

    @Mock
    private SiteConfigService siteConfigService;

    @Mock
    private Model mockModel;

    @Mock
    private HttpServletRequest mockRequest;

    @Mock
    private HttpServletResponse mockResponse;

    @Mock
    private CMSComponentService cmsComponentService;

    @Mock
    private SessionService sessionService;

    @Mock
    private TargetCartFacade targetCartFacade;

    @Mock
    private TargetLaybyCartService targetCartService;

    @Mock
    private TargetCommerceCartService targetCommerceCartService;

    @Mock
    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;

    @Mock
    private Model model;

    @Mock
    private Entry<String, TargetCartData> cart;

    @Mock
    private TargetCartData targetCartData;

    @Mock
    private PurchaseOptionModel purchaseOptionModel;

    @Mock
    private UpdateQuantityForm updateQuantityForm;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private RedirectAttributes redirectModel;

    @Mock
    private CartModificationData cartModificationData;

    @Mock
    private FlybuysDiscountFacade flybuysDiscountFacade;

    @Mock
    private FlybuysRedeemConfigDaoImpl flybuysRedeemConfigDao;

    @Mock
    private FlybuysRedeemConfigService mockFlybuysRedeemConfigService;

    @Mock
    private PriceDataFactory priceDataFactory;

    @Mock
    private CommonI18NService commonI18NService;

    @Mock
    private HttpServletResponse response;

    @Mock
    private TargetUserFacade targetUserFacade;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Mock
    private TargetCheckoutFacade targetCheckoutFacade;

    @Mock
    private HttpSession mockSession;

    @Mock
    private RequestAttributes requestAttributes;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private AdjustedCartEntriesData adjustedCartEntriesData;

    @Mock
    private PageTitleResolver pageTitleResolver;

    @Mock
    private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

    @Mock
    private AfterpayConfigFacade afterpayConfigFacade;

    @Mock
    private TargetCMSPageService targetCMSPageService;

    @InjectMocks
    private final CartPageController controller = Mockito.spy(new CartPageController());

    @Mock
    private TargetProductFacade targetProductFacade;

    @Mock
    private CartModel cartModel;

    @Mock
    private TargetSharedConfigFacade targetSharedConfigFacade;


    @Before
    public void prepare() {
        given(cart.getValue()).willReturn(targetCartData);
        given(targetPurchaseOptionHelper.getPurchaseOptionModel(Mockito.anyString())).willReturn(purchaseOptionModel);
        willReturn(Boolean.FALSE).given(targetUserFacade).isCurrentUserAnonymous();
        given(requestAttributes.getSessionMutex()).willReturn(mockSession);
        given(targetCheckoutFacade.adjustCart()).willReturn(adjustedCartEntriesData);
        given(targetCartService.getSessionCart()).willReturn(cartModel);
        given(targetSharedConfigFacade.getConfigByCode(TgtFacadesConstants.ZIP_PAYMENT_THRESHOLD_AMOUNT)).willReturn("1000");
        given(controller.getZipPaymentThresholdAmount()).willReturn(new PriceData());
    }

    @Test
    public void testNullProperty() {
        given(siteConfigService.getProperty(CartPageController.SHOW_CHECKOUT_STRATEGY_OPTIONS)).willReturn(null);

        assertThat(controller.isCheckoutStrategyVisible()).isFalse();
    }

    @Test
    public void testSomeStringProperty() {
        given(siteConfigService.getProperty(CartPageController.SHOW_CHECKOUT_STRATEGY_OPTIONS))
                .willReturn("someString");

        assertThat(controller.isCheckoutStrategyVisible()).isFalse();
    }

    @Test
    public void testTrueStringProperty() {
        given(siteConfigService.getProperty(CartPageController.SHOW_CHECKOUT_STRATEGY_OPTIONS)).willReturn("true");

        assertThat(controller.isCheckoutStrategyVisible()).isTrue();
    }

    @Test
    public void testZeroStringProperty() {
        given(siteConfigService.getProperty(CartPageController.SHOW_CHECKOUT_STRATEGY_OPTIONS)).willReturn("0");

        assertThat(controller.isCheckoutStrategyVisible()).isFalse();
    }

    @Test
    public void testEmptyStringProperty() {
        given(siteConfigService.getProperty(CartPageController.SHOW_CHECKOUT_STRATEGY_OPTIONS)).willReturn("");

        assertThat(controller.isCheckoutStrategyVisible()).isFalse();
    }

    @Test
    public void testEmptyEmptyStringProperty() {
        given(siteConfigService.getProperty(CartPageController.SHOW_CHECKOUT_STRATEGY_OPTIONS)).willReturn(" ");

        assertThat(controller.isCheckoutStrategyVisible()).isFalse();
    }


    @Test
    public void cartCheckRedirectionCheckout() throws CMSItemNotFoundException {
        willReturn(Boolean.TRUE).given(controller).isOnlyOneCartNotEmpty(Mockito.anyMap());
        willReturn(Boolean.FALSE).given(controller).areCartsAllEmpty(Mockito.anyMap());
        given(controller.getOnlyCart(Mockito.anyMap())).willReturn(cart);
        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isFeatureEnabled(Mockito.anyString());
        given(controller.getOnlyCartPurchaseOption(Mockito.anyMap())).willReturn(StringUtils.EMPTY);
        willReturn(Boolean.FALSE).given(targetCartData).isInsufficientAmount();
        willReturn(Boolean.TRUE).given(targetCartData).gethasValidDeliveryModes();
        willReturn(Boolean.TRUE).given(targetCartFacade).hasSessionCart();
        assertThat(controller.cartCheck(mockModel, null, httpServletRequest, response))
                .isEqualTo(ControllerConstants.Redirection.CHECKOUT);
        verify(sessionService, only()).setAttribute(
                TgtFacadesConstants.ACTIVE_PURCHASE_OPTION,
                StringUtils.EMPTY);
    }


    @Test
    public void cartCheckRedirectionCartInsufficientAmount() throws CMSItemNotFoundException {
        willReturn(Boolean.TRUE).given(controller).isOnlyOneCartNotEmpty(Mockito.anyMap());
        willReturn(Boolean.FALSE).given(controller).areCartsAllEmpty(Mockito.anyMap());
        given(controller.getOnlyCart(Mockito.anyMap())).willReturn(cart);
        willReturn(Boolean.FALSE).given(targetCartData).isInsufficientAmount();
        willReturn(Boolean.FALSE).given(targetCartData).gethasValidDeliveryModes();
        willReturn(Boolean.TRUE).given(targetCartFacade).hasSessionCart();

        assertThat(controller.cartCheck(mockModel, null, httpServletRequest, response))
                .isEqualTo(ControllerConstants.Redirection.CART);
    }


    @Test
    public void cartCheckRedirectionCartInvalidDeliveryMode() throws CMSItemNotFoundException {
        willReturn(Boolean.TRUE).given(controller).isOnlyOneCartNotEmpty(Mockito.anyMap());
        willReturn(Boolean.FALSE).given(controller).areCartsAllEmpty(Mockito.anyMap());
        given(controller.getOnlyCart(Mockito.anyMap())).willReturn(cart);
        willReturn(Boolean.TRUE).given(targetCartData).isInsufficientAmount();
        willReturn(Boolean.TRUE).given(targetCartFacade).hasSessionCart();

        assertThat(controller.cartCheck(mockModel, null, httpServletRequest, response))
                .isEqualTo(ControllerConstants.Redirection.CART);
    }

    @Test
    public void updateQtySuccess() throws CMSItemNotFoundException, CommerceCartModificationException,
            TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        given(targetCartFacade.updateCartEntry(Mockito.anyString(), Mockito.anyLong()))
                .willReturn(cartModificationData);
        given(cartModificationData.getStatusCode()).willReturn(TargetCommerceCartModificationStatus.SUCCESS);
        given(updateQuantityForm.getAction()).willReturn("update");
        RequestContextHolder.setRequestAttributes(requestAttributes);
        assertThat(controller.updateCartQuantities(4L, mockModel, updateQuantityForm, bindingResult, redirectModel,
                StringUtils.EMPTY, httpServletRequest, response))
                        .isEqualTo(ControllerConstants.Redirection.CART);
    }

    @Test
    public void updateQtyCommerceCartModificationException()
            throws CMSItemNotFoundException, CommerceCartModificationException,
            TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        given(targetCartFacade.updateCartEntry(Mockito.anyString(), Mockito.anyLong()))
                .willThrow(new CommerceCartModificationException("Update cart Error"));
        given(updateQuantityForm.getAction()).willReturn("update");
        RequestContextHolder.setRequestAttributes(requestAttributes);
        final ContentPageModel contentPageModel = mock(ContentPageModel.class);
        given(targetCMSPageService.getPageForLabelOrId("cart")).willReturn(contentPageModel);
        assertThat(controller.updateCartQuantities(4L, mockModel, updateQuantityForm, bindingResult, redirectModel,
                StringUtils.EMPTY, httpServletRequest, response))
                        .isEqualTo(ControllerConstants.Views.Pages.Cart.CART_PAGE);
    }

    @Ignore
    @Test
    public void testUpdateCartQuantitiesFBDiscountNotRemoved() throws CMSItemNotFoundException,
            CommerceCartModificationException,
            TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        final String isoCode = "AU";
        final PriceData cartPriceData = mock(PriceData.class);
        final CurrencyModel currencyModel = mock(CurrencyModel.class);
        final FlybuysRedeemConfigData flybuysRedeemConfigData = new FlybuysRedeemConfigData();
        final BigDecimal orderSubTotal = BigDecimal.valueOf(15.0);

        given(targetCartFacade.updateCartEntry(Mockito.anyString(), Mockito.anyLong()))
                .willReturn(cartModificationData);
        given(cartModificationData.getStatusCode()).willReturn(TargetCommerceCartModificationStatus.SUCCESS);
        given(updateQuantityForm.getAction()).willReturn("update");

        given(controller.getOnlyCart(Mockito.anyMap())).willReturn(cart);
        given(flybuysDiscountFacade.getFlybuysRedeemConfigData()).willReturn(flybuysRedeemConfigData);

        given(targetCartData.getSubTotal()).willReturn(cartPriceData);
        given(cartPriceData.getValue()).willReturn(orderSubTotal);

        given(flybuysDiscountFacade.getFlybuysRedeemConfigData()).willReturn(flybuysRedeemConfigData);
        flybuysRedeemConfigData.setMinCartValue(Double.valueOf(35d));

        given(requestAttributes.getSessionMutex()).willReturn(mockSession);
        RequestContextHolder.setRequestAttributes(requestAttributes);

        given(commonI18NService.getCurrentCurrency()).willReturn(currencyModel);
        given(currencyModel.getIsocode()).willReturn(isoCode);
        given(priceDataFactory.create(PriceDataType.BUY, orderSubTotal, isoCode)).willReturn(cartPriceData);

        willReturn(Boolean.FALSE).given(flybuysDiscountFacade).reassessFlybuysDiscountOnCheckoutCart();
        assertThat(controller.updateCartQuantities(4L, mockModel, updateQuantityForm, bindingResult, redirectModel,
                StringUtils.EMPTY, httpServletRequest, response))
                        .isEqualTo(ControllerConstants.Redirection.CART_TYPE.concat(CartDataHelper.BUY_NOW));

        verify(priceDataFactory, Mockito.never()).create(PriceDataType.BUY, orderSubTotal, isoCode);
        assertThat(GlobalMessages.hasInfoMessages(mockModel)).isFalse();
    }

    @Ignore
    @Test
    public void testUpdateCartQuantitiesFBDiscountRemoved() throws CMSItemNotFoundException,
            CommerceCartModificationException, TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        final String isoCode = "AU";
        final PriceData cartPriceData = mock(PriceData.class);
        final FlybuysRedeemConfigData flybuysRedeemConfigData = new FlybuysRedeemConfigData();
        final BigDecimal orderSubTotal = BigDecimal.valueOf(15.0);
        given(
                targetCartFacade.updateCartEntry(Mockito.anyString(), Mockito.anyLong()))
                        .willReturn(cartModificationData);
        given(cartModificationData.getStatusCode()).willReturn(TargetCommerceCartModificationStatus.SUCCESS);
        given(updateQuantityForm.getAction()).willReturn("update");

        given(controller.getOnlyCart(Mockito.anyMap())).willReturn(cart);
        given(flybuysDiscountFacade.getFlybuysRedeemConfigData()).willReturn(flybuysRedeemConfigData);

        given(targetCartData.getSubTotal()).willReturn(cartPriceData);
        given(cartPriceData.getValue()).willReturn(orderSubTotal);
        given(cartPriceData.getCurrencyIso()).willReturn(isoCode);

        given(flybuysDiscountFacade.getFlybuysRedeemConfigData()).willReturn(flybuysRedeemConfigData);
        flybuysRedeemConfigData.setMinCartValue(Double.valueOf(35d));

        given(requestAttributes.getSessionMutex()).willReturn(mockSession);
        RequestContextHolder.setRequestAttributes(requestAttributes);

        given(priceDataFactory.create(eq(PriceDataType.BUY), any(BigDecimal.class), eq(isoCode))).willReturn(
                cartPriceData);

        final ModelMap modelMap = new ModelMap();
        doReturn(modelMap).when(redirectModel).getFlashAttributes();

        willReturn(Boolean.TRUE).given(flybuysDiscountFacade).reassessFlybuysDiscountOnCheckoutCart();
        assertThat(controller.updateCartQuantities(4L, mockModel, updateQuantityForm, bindingResult, redirectModel,
                StringUtils.EMPTY, httpServletRequest, response))
                        .isEqualTo(ControllerConstants.Redirection.CART_TYPE.concat(CartDataHelper.BUY_NOW));

        final ArgumentCaptor<BigDecimal> minCartValueCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        verify(priceDataFactory).create(eq(PriceDataType.BUY), minCartValueCaptor.capture(), eq(isoCode));

        assertThat(minCartValueCaptor.getValue()).isEqualByComparingTo(BigDecimal.valueOf(35d));

        final List<MessageKeyArguments> messages = (List<MessageKeyArguments>)modelMap.get("accInfoMsgs");
        assertThat(messages).hasSize(1);
        assertThat(messages.get(0).getKey()).isEqualTo("basket.information.flybuys.discount.removed");
    }

    @Test
    public void testUpdateDeliveryMode() {
        final String selectedDeliveryMode = "home-delivery";
        controller.updateDeliveryMode(selectedDeliveryMode, model);
        verify(targetCheckoutFacade).updateDeliveryMode(selectedDeliveryMode);
        verify(targetCheckoutFacade).getCheckoutCart();
    }

    @Test
    public void testShowDeliveryModeByPostCodeWhenPostCodeIsInvalid() {
        final DeliveryModePostcodeDto postcodeDto = new DeliveryModePostcodeDto();
        postcodeDto.setPostcode("ABCD");
        controller.showDeliveryModebyPostcode(postcodeDto, mockModel);
        verifyZeroInteractions(sessionService);
    }

    @Test
    public void testShowDeliveryModeByPostCodeWhenPostCodeIsValid() {
        final DeliveryModePostcodeDto postcodeDto = new DeliveryModePostcodeDto();
        postcodeDto.setPostcode("1234");
        controller.showDeliveryModebyPostcode(postcodeDto, mockModel);
        verify(sessionService).setAttribute(TgtCoreConstants.SESSION_POSTCODE, postcodeDto.getPostcode());
    }

    @Test
    public void testShowCartWithoutSessionCart() throws CMSItemNotFoundException {
        final ContentPageModel afterpayModal = mock(ContentPageModel.class);
        final ContentPageModel contentPageModel = mock(ContentPageModel.class);
        given(targetCMSPageService.getPageForLabelOrId("cart")).willReturn(contentPageModel);
        given(targetCMSPageService.getPageForLabelOrId(TgtwebcoreConstants.AFTERPAY_MODAL_ID))
                .willReturn(afterpayModal);
        willReturn(Boolean.FALSE).given(targetCartFacade).hasSessionCart();
        controller.showCart(false, model, mockRequest, mockResponse);
        verify(targetCartFacade, never()).getSessionCart();
        verify(model).addAttribute("pageType", PageType.Cart);
    }

    @Test
    public void testShowCartWithSessionCart() throws CMSItemNotFoundException {
        final ContentPageModel afterpayModal = mock(ContentPageModel.class);
        final ContentPageModel contentPageModel = mock(ContentPageModel.class);
        given(targetCMSPageService.getPageForLabelOrId("cart")).willReturn(contentPageModel);
        given(targetCMSPageService.getPageForLabelOrId(TgtwebcoreConstants.AFTERPAY_MODAL_ID))
                .willReturn(afterpayModal);
        willReturn(Boolean.TRUE).given(targetCartFacade).hasSessionCart();
        controller.showCart(false, model, mockRequest, mockResponse);
        verify(targetCartFacade).getSessionCart();
        verify(model).addAttribute("pageType", PageType.Cart);
    }

    @Test
    public void testShowCartWhenAnItemUnapprovedInBasket() throws CMSItemNotFoundException {
        final ContentPageModel afterpayModal = mock(ContentPageModel.class);
        final ContentPageModel contentPageModel = mock(ContentPageModel.class);
        given(targetCMSPageService.getPageForLabelOrId("cart")).willReturn(contentPageModel);
        given(targetCMSPageService.getPageForLabelOrId(TgtwebcoreConstants.AFTERPAY_MODAL_ID))
                .willReturn(afterpayModal);
        willReturn(Boolean.TRUE).given(targetCartFacade).hasSessionCart();
        willReturn(Boolean.TRUE).given(targetCommerceCartService).removeMissingProducts(cartModel);
        final HashMap<String, Object> modelMap = new HashMap<String, Object>();
        given(model.asMap()).willReturn(modelMap);

        controller.showCart(false, model, mockRequest, mockResponse);

        assertThat(GlobalMessages.hasInfoMessages(model)).isTrue();
        assertThat(modelMap.containsKey("accInfoMsgs"));
        final List<MessageKeyArguments> list = (List<MessageKeyArguments>)modelMap.get("accInfoMsgs");
        assertThat(list).isNotEmpty();
        assertThat(list.get(0).getKey()).isEqualTo("checkout.information.missing.products.removed");
        verify(targetCartFacade, times(2)).getSessionCart();
        verify(model).addAttribute("pageType", PageType.Cart);
    }

    @Test
    public void testPopulateAfterPayConfig() {
        final AfterpayConfigData afterpayConfigData = mock(AfterpayConfigData.class);
        given(afterpayConfigFacade.getAfterpayConfig()).willReturn(afterpayConfigData);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isAfterpayEnabled();

        controller.populateAfterPayConfig(mockModel);
        verify(mockModel).addAttribute("afterpayConfig", afterpayConfigData);
    }

    @Test
    public void testPopulateAfterPayConfigDisabled() {
        controller.populateAfterPayConfig(mockModel);
        verify(afterpayConfigFacade, never()).getAfterpayConfig();
    }

    @Test
    public void testPopulateAfterPayModal() throws CMSItemNotFoundException {
        final ContentPageModel afterpayModal = mock(ContentPageModel.class);
        given(targetCMSPageService.getPageForLabelOrId(TgtwebcoreConstants.AFTERPAY_MODAL_ID))
                .willReturn(afterpayModal);
        given(afterpayModal.getLabel()).willReturn("/modal/afterpay");
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isAfterpayEnabled();
        controller.populateAfterPayModal(mockModel);
        verify(mockModel).addAttribute("afterpayModal", "/modal/afterpay");
    }

    @Test
    public void testPopulateAfterPayModalDisabled() throws CMSItemNotFoundException {
        final String afterpayModalID = TgtwebcoreConstants.AFTERPAY_MODAL_ID;
        controller.populateAfterPayConfig(mockModel);
        verify(targetCMSPageService, never()).getPageForLabelOrId(afterpayModalID);
    }

    @Test
    public void testPopulateHideIncentiveMessageForPhysicalGiftCard() throws CMSItemNotFoundException {
        final ContentPageModel contentPageModel = mock(ContentPageModel.class);
        final TargetProductData mockTargetProduct = mock(TargetProductData.class);
        final OrderEntryData mockEntry = mock(OrderEntryData.class);

        given(targetCMSPageService.getPageForLabelOrId("cart")).willReturn(contentPageModel);
        given(mockEntry.getProduct()).willReturn(mockTargetProduct);
        given(mockTargetProduct.getProductTypeCode()).willReturn(TgtCoreConstants.PHYSICAL_GIFTCARD);
        given(mockTargetProduct.getCode()).willReturn("PGC_adrenalin");
        given(Boolean.valueOf(mockTargetProduct.isAllowDeliverToStore())).willReturn(Boolean.TRUE);

        given(Boolean.valueOf(targetCartFacade.hasSessionCart())).willReturn(Boolean.TRUE);
        given(targetCartFacade.getPostalCodeFromCartOrSession(targetCartData)).willReturn(null);
        given(targetCartFacade.getSessionCart()).willReturn(targetCartData);
        given(targetCartData.getEntries()).willReturn(Collections.singletonList(mockEntry));

        controller.showCart(false, model, mockRequest, mockResponse);
        verify(model).addAttribute("hideIncentiveMessage", Boolean.TRUE);
    }

    @Test
    public void testUpdateCartWhenItemIsUnApproved() throws CMSItemNotFoundException {
        final ContentPageModel contentPageModel = Mockito.mock(ContentPageModel.class);
        given(targetCMSPageService.getPageForLabelOrId("cart")).willReturn(contentPageModel);
        RequestContextHolder.setRequestAttributes(requestAttributes);
        final String productCode = "code";
        given(targetProductFacade.getProductForCode(productCode))
                .willThrow(new UnknownIdentifierException("Product not found"));
        willReturn(Boolean.FALSE).given(targetCartFacade).hasSessionCart();

        final HashMap<String, Object> modelMap = new HashMap<String, Object>();
        given(model.asMap()).willReturn(modelMap);
        controller.updateCartQuantities(1, model, updateQuantityForm, bindingResult, redirectModel, productCode,
                mockRequest, mockResponse);

        assertThat(GlobalMessages.hasInfoMessages(model)).isTrue();
        assertThat(modelMap.containsKey("accInfoMsgs"));
        final List<MessageKeyArguments> list = (List<MessageKeyArguments>)modelMap.get("accInfoMsgs");
        assertThat(list).isNotEmpty();
        assertThat(list.get(0).getKey()).isEqualTo("checkout.information.missing.products.removed");
        verify(targetCartFacade, never()).getSessionCart();
        verify(model).addAttribute("pageType", PageType.Cart);
    }
}
