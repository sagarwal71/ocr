package au.com.target.tgtstorefront.interceptors.beforeview;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.verifyNoMoreInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtstorefront.constants.WebConstants;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SalesApplicationBeforeViewHandlerTest {

    @Mock
    private SalesApplicationFacade mockSalesApplicationFacade;

    @InjectMocks
    private final SalesApplicationBeforeViewHandler salesApplicationBeforeViewHandler = new SalesApplicationBeforeViewHandler();

    @Mock
    private HttpServletRequest mockRequest;

    @Mock
    private HttpServletResponse mockResponse;

    @Mock
    private ModelAndView mockModelAndView;

    @Test
    public void testBeforeViewWeb() {
        given(mockSalesApplicationFacade.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);

        salesApplicationBeforeViewHandler.beforeView(mockRequest, mockResponse, mockModelAndView);

        verify(mockModelAndView).addObject(WebConstants.IS_SALES_ASSISTED, Boolean.FALSE);
        verifyNoMoreInteractions(mockModelAndView);
    }

    @Test
    public void testBeforeViewKiosk() {
        given(mockSalesApplicationFacade.getCurrentSalesApplication()).willReturn(SalesApplication.KIOSK);

        salesApplicationBeforeViewHandler.beforeView(mockRequest, mockResponse, mockModelAndView);

        verify(mockModelAndView).addObject(WebConstants.IS_SALES_ASSISTED, Boolean.FALSE);
        verify(mockModelAndView).addObject("kioskMode", Boolean.TRUE);
        verifyNoMoreInteractions(mockModelAndView);
    }

    @Test
    public void testBeforeViewMobileAppiPhone() {
        given(mockSalesApplicationFacade.getCurrentSalesApplication()).willReturn(SalesApplication.MOBILEAPP);

        final String iPhoneUiWebViewUa = "Mozilla/5.0 (iPhone; U; CPU iPhone OS 9_0 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Mobile/8B117";
        given(mockRequest.getHeader("User-Agent")).willReturn(iPhoneUiWebViewUa);

        salesApplicationBeforeViewHandler.beforeView(mockRequest, mockResponse, mockModelAndView);

        verify(mockModelAndView).addObject(WebConstants.IS_SALES_ASSISTED, Boolean.FALSE);
        verify(mockModelAndView).addObject("mobileAppMode", Boolean.TRUE);
        verify(mockModelAndView).addObject("iosAppMode", Boolean.TRUE);
        verifyNoMoreInteractions(mockModelAndView);
    }

    @Test
    public void testBeforeViewMobileAppiPad() {
        given(mockSalesApplicationFacade.getCurrentSalesApplication()).willReturn(SalesApplication.MOBILEAPP);

        final String iPadUiWebViewUa = "Mozilla/5.0 (iPad; CPU OS 9_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Mobile/98176";
        given(mockRequest.getHeader("User-Agent")).willReturn(iPadUiWebViewUa);

        salesApplicationBeforeViewHandler.beforeView(mockRequest, mockResponse, mockModelAndView);

        verify(mockModelAndView).addObject(WebConstants.IS_SALES_ASSISTED, Boolean.FALSE);
        verify(mockModelAndView).addObject("mobileAppMode", Boolean.TRUE);
        verify(mockModelAndView).addObject("iosAppMode", Boolean.TRUE);
        verifyNoMoreInteractions(mockModelAndView);
    }

    @Test
    public void testBeforeViewMobileAppiPod() {
        given(mockSalesApplicationFacade.getCurrentSalesApplication()).willReturn(SalesApplication.MOBILEAPP);

        final String iPodUiWebViewUa = "Mozilla/5.0 (iPod; CPU OS 9_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Mobile/98176";
        given(mockRequest.getHeader("User-Agent")).willReturn(iPodUiWebViewUa);

        salesApplicationBeforeViewHandler.beforeView(mockRequest, mockResponse, mockModelAndView);

        verify(mockModelAndView).addObject(WebConstants.IS_SALES_ASSISTED, Boolean.FALSE);
        verify(mockModelAndView).addObject("mobileAppMode", Boolean.TRUE);
        verify(mockModelAndView).addObject("iosAppMode", Boolean.TRUE);
        verifyNoMoreInteractions(mockModelAndView);
    }

    @Test
    public void testBeforeViewMobileAppAndroidOld() {
        given(mockSalesApplicationFacade.getCurrentSalesApplication()).willReturn(SalesApplication.MOBILEAPP);

        final String androidWebViewUa = "Mozilla/5.0 (Linux; U; Android 4.1.1; en-gb; Build/KLP) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30";
        given(mockRequest.getHeader("User-Agent")).willReturn(androidWebViewUa);

        salesApplicationBeforeViewHandler.beforeView(mockRequest, mockResponse, mockModelAndView);

        verify(mockModelAndView).addObject(WebConstants.IS_SALES_ASSISTED, Boolean.FALSE);
        verify(mockModelAndView).addObject("mobileAppMode", Boolean.TRUE);
        verify(mockModelAndView).addObject("androidAppMode", Boolean.TRUE);
        verifyNoMoreInteractions(mockModelAndView);
    }

    @Test
    public void testBeforeViewMobileAppAndroidKtoL() {
        given(mockSalesApplicationFacade.getCurrentSalesApplication()).willReturn(SalesApplication.MOBILEAPP);

        final String androidWebViewUa = "Mozilla/5.0 (Linux; Android 4.4; Nexus 5 Build/_BuildID_) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36";
        given(mockRequest.getHeader("User-Agent")).willReturn(androidWebViewUa);

        salesApplicationBeforeViewHandler.beforeView(mockRequest, mockResponse, mockModelAndView);

        verify(mockModelAndView).addObject(WebConstants.IS_SALES_ASSISTED, Boolean.FALSE);
        verify(mockModelAndView).addObject("mobileAppMode", Boolean.TRUE);
        verify(mockModelAndView).addObject("androidAppMode", Boolean.TRUE);
        verifyNoMoreInteractions(mockModelAndView);
    }

    @Test
    public void testBeforeViewMobileAppAndroidLPlus() {
        given(mockSalesApplicationFacade.getCurrentSalesApplication()).willReturn(SalesApplication.MOBILEAPP);

        final String androidWebViewUa = "Mozilla/5.0 (Linux; Android 5.1.1; Nexus 5 Build/LMY48B; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/43.0.2357.65 Mobile Safari/537.36";
        given(mockRequest.getHeader("User-Agent")).willReturn(androidWebViewUa);

        salesApplicationBeforeViewHandler.beforeView(mockRequest, mockResponse, mockModelAndView);

        verify(mockModelAndView).addObject(WebConstants.IS_SALES_ASSISTED, Boolean.FALSE);
        verify(mockModelAndView).addObject("mobileAppMode", Boolean.TRUE);
        verify(mockModelAndView).addObject("androidAppMode", Boolean.TRUE);
        verifyNoMoreInteractions(mockModelAndView);
    }

    @Test
    public void testBeforeViewMobileAppNoUserAgent() {
        given(mockSalesApplicationFacade.getCurrentSalesApplication()).willReturn(SalesApplication.MOBILEAPP);

        salesApplicationBeforeViewHandler.beforeView(mockRequest, mockResponse, mockModelAndView);

        verify(mockModelAndView).addObject(WebConstants.IS_SALES_ASSISTED, Boolean.FALSE);
        verify(mockModelAndView).addObject("mobileAppMode", Boolean.TRUE);
        verifyNoMoreInteractions(mockModelAndView);
    }
}
