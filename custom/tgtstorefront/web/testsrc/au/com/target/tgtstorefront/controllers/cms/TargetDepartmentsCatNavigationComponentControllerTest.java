/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;

import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtstorefront.controllers.util.CMSNavigationComponentHelper;
import au.com.target.tgtstorefront.controllers.util.NavigationNodeHelper;
import au.com.target.tgtwebcore.model.cms2.components.TargetDepartmentsCategoryNavigationComponentModel;


/**
 * @author mjanarth
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetDepartmentsCatNavigationComponentControllerTest {

    @Mock
    private NavigationNodeHelper navigationNodeHelper;

    @Mock
    private CMSNavigationNodeModel navNode;

    @Mock
    private CMSNavigationNodeModel childNode;

    @Mock
    private TargetDepartmentsCategoryNavigationComponentModel deptCategoryModel;

    @Mock
    private HttpServletRequest request;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Mock
    private CMSNavigationComponentHelper cmsNavigationComponentHelper;

    @Mock
    private Model model;

    @InjectMocks
    private final TargetDepartmentsCategoryNavigationComponentController controller = new TargetDepartmentsCategoryNavigationComponentController();


    @SuppressWarnings("boxing")
    @Test
    public void testFillModelWithNullNavigationModel() {
        Mockito.when(deptCategoryModel.getNavigationNode()).thenReturn(null);
        BDDMockito.given(request.getAttribute("categoryCode")).willReturn(null);
        controller.fillModel(request, model, deptCategoryModel);
        Mockito.verifyZeroInteractions(navigationNodeHelper);
        Mockito.verifyZeroInteractions(targetFeatureSwitchFacade);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testFillModelWithNullNavigationModelWhenCategoryCodePresentHavingNoNavNode() {
        Mockito.when(deptCategoryModel.getNavigationNode()).thenReturn(null);
        BDDMockito.given(request.getAttribute("categoryCode")).willReturn("W222");
        BDDMockito.given(cmsNavigationComponentHelper.getCategoryNavigationNode("W2222")).willReturn(null);
        controller.fillModel(request, model, deptCategoryModel);
        Mockito.verifyZeroInteractions(navigationNodeHelper);
        Mockito.verifyZeroInteractions(targetFeatureSwitchFacade);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testFillModelWithNoChildren() {
        Mockito.when(deptCategoryModel.getNavigationNode()).thenReturn(navNode);
        Mockito.when(navNode.getChildren()).thenReturn(null);
        controller.fillModel(request, model, deptCategoryModel);
        Mockito.verifyZeroInteractions(navigationNodeHelper);
        Mockito.verify(targetFeatureSwitchFacade, Mockito.times(1)).isFeatureEnabled(Mockito.anyString());

    }

    @SuppressWarnings("boxing")
    @Test
    public void testFillModelWithChildren() {
        Mockito.when(deptCategoryModel.getNavigationNode()).thenReturn(navNode);
        Mockito.when(navNode.getChildren()).thenReturn(Collections.singletonList(childNode));
        controller.fillModel(request, model, deptCategoryModel);
        Mockito.verify(navigationNodeHelper).buildColumn(childNode);
        Mockito.verify(targetFeatureSwitchFacade, Mockito.times(1)).isFeatureEnabled(Mockito.anyString());

    }

    @SuppressWarnings("boxing")
    @Test
    public void testFillModelWithChildrenWhenNoNavNodeInComponentForCategoryHavingNavNode() {
        BDDMockito.given(request.getAttribute("categoryCode")).willReturn("W222");
        BDDMockito.given(cmsNavigationComponentHelper.getCategoryNavigationNode("W222")).willReturn(navNode);
        Mockito.when(navNode.getChildren()).thenReturn(Collections.singletonList(childNode));
        controller.fillModel(request, model, deptCategoryModel);
        Mockito.verify(navigationNodeHelper).buildColumn(childNode);
        Mockito.verify(targetFeatureSwitchFacade, Mockito.times(1)).isFeatureEnabled(Mockito.anyString());

    }
}
