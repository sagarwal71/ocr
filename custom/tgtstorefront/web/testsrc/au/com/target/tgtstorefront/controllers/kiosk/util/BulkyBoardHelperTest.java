/**
 * 
 */
package au.com.target.tgtstorefront.controllers.kiosk.util;

import de.hybris.platform.cms2.servicelayer.services.CMSPageService;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;


/**
 * @author gbaker2
 * 
 */
public class BulkyBoardHelperTest {

    @InjectMocks
    private final BulkyBoardHelper bulkyBoardHelper = new BulkyBoardHelper();

    @Mock
    private CMSPageService mockCmsPageService;
    @Mock
    private TargetStoreLocatorFacade mockTargetStoreLocatorFacade;

    @Before
    public void prepare()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testExtractStoreNumberForSearchQuery() {
        final String searchQuery = "bulkyBoardStoreNumber:4567";
        final int expected = 4567;
        final Integer actual = bulkyBoardHelper.parseStoreNumber(searchQuery);
        Assert.assertEquals(expected, actual.intValue());
    }

    @Test
    public void testExtractStoreNumberForLongSearchQuery() {
        final String searchQuery = ":latest:bulkyBoardStoreNumber:4567:brand:Target";
        final int expected = 4567;
        final Integer actual = bulkyBoardHelper.parseStoreNumber(searchQuery);
        Assert.assertEquals(expected, actual.intValue());
    }

    @Test
    public void testNullExtractStoreNumberForSearchQuery() {
        final String searchQuery = ":latest:somethingElse:4567";
        final Integer actual = bulkyBoardHelper.parseStoreNumber(searchQuery);
        Assert.assertNull(actual);
    }

    @Test
    public void testNotNumberExtractStoreNumberForSearchQuery() {
        final String searchQuery = ":latest:bulkyBoardStoreNumber:hello";
        final Integer actual = bulkyBoardHelper.parseStoreNumber(searchQuery);
        Assert.assertNull(actual);
    }


}
