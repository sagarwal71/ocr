/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtfacades.delivery.TargetDeliveryFacade;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.order.TargetCartFacade;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.order.impl.TargetCheckoutFacadeImpl;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.data.CheckoutDeliveryTypeData;
import au.com.target.tgtstorefront.controllers.util.AddressDataHelper;
import au.com.target.tgtstorefront.forms.AddressForm;
import au.com.target.tgtstorefront.forms.validation.validator.AddressFormValidator;
import au.com.target.tgtstorefront.util.PageTitleResolver;
import au.com.target.tgtverifyaddr.data.FormattedAddressData;


/**
 * @author Benoit VanalderWeireldt
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CheckoutYourAddressControllerTest {

    private final AddressData addressData1 = new AddressData() {
        {
            setDefaultAddress(true);
            setId("1");
        }
    };
    private final AddressData addressData2 = new AddressData() {
        {
            setDefaultAddress(false);
            setId("2");
        }
    };
    private final AddressData addressData3 = new AddressData() {
        {
            setDefaultAddress(false);
            setId("3");
        }
    };
    private final AddressData addressData4 = new AddressData() {
        {
            setDefaultAddress(false);
            setId("4");
        }
    };

    @Mock
    private TargetCheckoutFacadeImpl checkoutFacade;

    @Mock
    private TargetStoreLocatorFacade mockTargetStoreLocatorFacade;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private Configuration mockConfiguration;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Mock
    private AddressDataHelper addressDataHelper;

    @Mock
    private RedirectAttributes redirectAttributes;

    @Mock
    private AddressFormValidator addressFormValidator;

    @Mock
    private UserFacade userFacade;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Mock
    private TargetDeliveryFacade targetDeliveryFacade;

    @Mock
    private TargetCartFacade targetCartFacade;

    @Mock
    private CMSPageService cmsPageService;

    @Mock
    private ContentPageModel contentPageModel;

    @Mock
    private PageTitleResolver pageTitleResolver;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpSession session;

    @Mock
    private FormattedAddressData formattedAddressData;

    @Mock
    private SessionService sessionService;

    private List<AddressData> deliveryAddresses;

    @Mock
    private Model model;

    @Mock
    private HttpServletResponse response;

    @Mock
    private TargetUserFacade targetUserFacade;

    @InjectMocks
    @Spy
    private final CheckoutYourAddressController checkoutYourAddressController = new CheckoutYourAddressController();

    @Before
    public void setUp() throws CMSItemNotFoundException {
        given(configurationService.getConfiguration()).willReturn(mockConfiguration);
        willReturn(Integer.valueOf(1)).given(mockConfiguration).getInt(anyString());
        given(cmsPageService.getPageForLabelOrId(anyString())).willReturn(contentPageModel);
        given(request.getSession()).willReturn(session);
        given(session.getAttribute(AddressDataHelper.SINGLE_LINE_ADDRESS_FORMAT)).willReturn(formattedAddressData);
    }

    @Test
    public void testOrderAddress() {
        deliveryAddresses = new ArrayList<>();
        deliveryAddresses.add(addressData2);
        deliveryAddresses.add(addressData1);
        deliveryAddresses.add(addressData3);
        given(checkoutFacade.getSupportedDeliveryAddresses(true)).willReturn(deliveryAddresses);

        final List<? extends AddressData> orderedAddresses = checkoutYourAddressController.getDeliveryAddresses();

        assertThat(orderedAddresses.get(0).getId()).isEqualTo(addressData1.getId());
    }

    @Test
    public void testOrderAddressTriangulate() {
        deliveryAddresses = new ArrayList<>();
        deliveryAddresses.add(addressData2);
        deliveryAddresses.add(addressData3);
        deliveryAddresses.add(addressData1);
        deliveryAddresses.add(addressData4);
        given(checkoutFacade.getSupportedDeliveryAddresses(true)).willReturn(deliveryAddresses);

        final List<? extends AddressData> orderedAddresses = checkoutYourAddressController.getDeliveryAddresses();

        assertThat(orderedAddresses.get(0).getId()).isEqualTo(addressData1.getId());
    }

    @Test
    public void testOrderAddressNoDefaultAddress() {
        deliveryAddresses = new ArrayList<>();
        deliveryAddresses.add(addressData2);
        deliveryAddresses.add(addressData3);
        deliveryAddresses.add(addressData4);
        given(checkoutFacade.getSupportedDeliveryAddresses(true)).willReturn(deliveryAddresses);

        final List<? extends AddressData> orderedAddresses = checkoutYourAddressController.getDeliveryAddresses();

        assertThat(orderedAddresses.get(0).getId()).isEqualTo(addressData2.getId());
    }

    @Test
    public void itShouldRemoveSelectedStoreBeforePerformSearchCncStoreByPosition() {
        checkoutYourAddressController.storeSearchPosition("lat", "lng", model);
        verify(checkoutFacade).removeSelectedCncStore();
    }

    @Test
    public void itShouldRemoveSelectedStoreBeforePerformSearchCncStoreByLocation() {
        checkoutYourAddressController.storeSearchLocation("3030", model);
        verify(checkoutFacade).removeSelectedCncStore();
    }

    @Test
    public void itShouldCheckSingleLineAddressLookupFlagWhenAddDeliveryAddress() throws CMSItemNotFoundException {
        final String deliveryModeCode = "Home";
        checkoutYourAddressController.addDeliveryAddress(deliveryModeCode, model, redirectAttributes);

        verify(addressDataHelper).checkIfSingleLineAddressFeatureIsEnabled(model);
    }

    @Test
    public void itShouldCheckSingleLineAddressLookupFlagWhenEditDeliveryAddress() throws CMSItemNotFoundException {
        final String deliveryModeCode = "Home";
        final String addressCode = "addreess1";
        checkoutYourAddressController.editDeliveryAddress(addressCode, deliveryModeCode, model, redirectAttributes);

        verify(addressDataHelper).checkIfSingleLineAddressFeatureIsEnabled(model);
    }

    @Test
    public void itShouldSkipConfirmationWhenAddressLookupIsEnabled() throws CMSItemNotFoundException {
        final AddressForm form = new AddressForm();
        final BindingResult bindingResult = mock(BindingResult.class);
        willReturn(Boolean.TRUE).given(addressDataHelper).isSingleLineLookupFeatureEnabled();
        final TargetZoneDeliveryModeModel deliveryModel = mock(TargetZoneDeliveryModeModel.class);
        given(targetDeliveryModeHelper.getDeliveryModeModel(anyString())).willReturn(deliveryModel);
        willReturn(Boolean.TRUE).given(targetDeliveryFacade).isDeliveryModePostCodeCombinationValid(anyString(),
                anyString());
        willReturn(Boolean.FALSE).given(deliveryModel).getIsDeliveryToStore();
        final TargetCartData cartData = new TargetCartData();
        final PriceData price = new PriceData();
        price.setValue(BigDecimal.valueOf(0));
        cartData.setDeliveryCost(price);
        given(checkoutYourAddressController.getCheckoutFacade().getCheckoutCart()).willReturn(cartData);

        final String viewName = checkoutYourAddressController.confirmAddDeliveryAddress(form, bindingResult, model,
                redirectAttributes, request);

        verify(addressDataHelper, times(0)).processAddressAndVerifyWithQas(any(Model.class), any(AddressForm.class),
                any(TargetAddressData.class), anyBoolean());
        verify(addressDataHelper).isAddressFormVerifiedInSession(form, session);
        assertThat(viewName).isEqualTo(ControllerConstants.Redirection.CHECKOUT_PAYMENT);
    }


    @Test
    public void itShouldMoveToPaymentPageWhenDeliveryFeeSame() throws CMSItemNotFoundException {
        final AddressForm form = new AddressForm();
        final BindingResult bindingResult = mock(BindingResult.class);
        given(Boolean.valueOf(addressDataHelper.isSingleLineLookupFeatureEnabled())).willReturn(Boolean.valueOf(true));
        final TargetZoneDeliveryModeModel deliveryModel = mock(TargetZoneDeliveryModeModel.class);
        given(targetDeliveryModeHelper.getDeliveryModeModel(anyString())).willReturn(deliveryModel);
        given(Boolean.valueOf(targetDeliveryFacade.isDeliveryModePostCodeCombinationValid(anyString(), anyString())))
                .willReturn(Boolean.valueOf(true));
        given(deliveryModel.getIsDeliveryToStore()).willReturn(Boolean.valueOf(false));
        final TargetCartData cartData = new TargetCartData();
        final PriceData price = new PriceData();
        price.setValue(BigDecimal.valueOf(0));
        cartData.setDeliveryCost(price);
        given(checkoutYourAddressController.getCheckoutFacade().getCheckoutCart()).willReturn(cartData);
        given(Boolean.valueOf(checkoutYourAddressController.getCheckoutFacade().isDeliveryFeeVariesAfterAddressChange(
                any(BigDecimal.class)))).willReturn(Boolean.valueOf(false));

        final String viewName = checkoutYourAddressController.confirmAddDeliveryAddress(form, bindingResult, model,
                redirectAttributes, request);

        verify(addressDataHelper, times(0)).processAddressAndVerifyWithQas(any(Model.class), any(AddressForm.class),
                any(TargetAddressData.class), anyBoolean());
        verify(addressDataHelper).isAddressFormVerifiedInSession(form, session);
        assertThat(viewName).isEqualTo(ControllerConstants.Redirection.CHECKOUT_PAYMENT);
    }

    @Test
    public void itShouldLandDeliveryPageWhenDeliveryFeeDifferent() throws CMSItemNotFoundException {
        final AddressForm form = new AddressForm();
        final BindingResult bindingResult = mock(BindingResult.class);
        given(Boolean.valueOf(addressDataHelper.isSingleLineLookupFeatureEnabled())).willReturn(Boolean.valueOf(true));
        final TargetZoneDeliveryModeModel deliveryModel = mock(TargetZoneDeliveryModeModel.class);
        given(targetDeliveryModeHelper.getDeliveryModeModel(anyString())).willReturn(deliveryModel);
        given(Boolean.valueOf(targetDeliveryFacade.isDeliveryModePostCodeCombinationValid(anyString(), anyString())))
                .willReturn(Boolean.valueOf(true));
        given(deliveryModel.getIsDeliveryToStore()).willReturn(Boolean.valueOf(false));
        final TargetCartData cartData = new TargetCartData();
        final PriceData price = new PriceData();
        price.setValue(BigDecimal.valueOf(0));
        cartData.setDeliveryCost(price);
        given(checkoutYourAddressController.getCheckoutFacade().getCheckoutCart()).willReturn(cartData);
        given(Boolean.valueOf(checkoutYourAddressController.getCheckoutFacade().isDeliveryFeeVariesAfterAddressChange(
                any(BigDecimal.class)))).willReturn(Boolean.valueOf(true));

        final String viewName = checkoutYourAddressController.confirmAddDeliveryAddress(form, bindingResult, model,
                redirectAttributes, request);

        verify(addressDataHelper, times(0)).processAddressAndVerifyWithQas(any(Model.class), any(AddressForm.class),
                any(TargetAddressData.class), anyBoolean());
        verify(addressDataHelper).isAddressFormVerifiedInSession(form, session);
        assertThat(viewName).isEqualTo(ControllerConstants.Redirection.CHECKOUT_YOUR_ADDRESS);
    }

    @Test
    public void itShouldPromptConfirmationWhenAddressLookupIsDisabled()
            throws CMSItemNotFoundException {
        final AddressForm form = new AddressForm();
        final BindingResult bindingResult = mock(BindingResult.class);
        willReturn(Boolean.FALSE).given(addressDataHelper).isSingleLineLookupFeatureEnabled();

        final String viewName = checkoutYourAddressController.confirmAddDeliveryAddress(form, bindingResult, model,
                redirectAttributes, request);

        verify(addressDataHelper).processAddressAndVerifyWithQas(any(Model.class), any(AddressForm.class),
                any(TargetAddressData.class), anyBoolean());
        verify(addressDataHelper, times(0)).isAddressFormVerified(form, formattedAddressData);
        verify(session, times(0)).removeAttribute(AddressDataHelper.SINGLE_LINE_ADDRESS_FORMAT);
        assertThat(viewName).isEqualTo(ControllerConstants.Views.Pages.MultiStepCheckout.CONFIRM_DELIVERY_ADDRESS_PAGE);
    }

    @Test
    public void testUpdateDeliveryMode() {
        final String selectedDeliveryMode = "home-delivery";
        checkoutYourAddressController.updateDeliveryMode(selectedDeliveryMode, model);
        verify(checkoutYourAddressController.getCheckoutFacade()).updateDeliveryMode(selectedDeliveryMode);
        verify(checkoutYourAddressController.getCheckoutFacade()).getCheckoutCart();
    }

    @Test
    public void testDoChooseDeliveryAddressForPostCodePopulated() throws CMSItemNotFoundException {
        final Model modelMap = new ExtendedModelMap();
        final TargetZoneDeliveryModeData targetZDMD = new TargetZoneDeliveryModeData();
        targetZDMD.setDeliveryToStore(true);
        targetZDMD.setCode("cnc");
        final TargetCartData cartData = new TargetCartData();
        cartData.setDeliveryMode(targetZDMD);

        final CartData sessionCart = mock(CartData.class);
        given(targetCartFacade.getSessionCart()).willReturn(sessionCart);
        given(checkoutFacade.getCheckoutCart()).willReturn(cartData);
        given(targetCartFacade.getPostalCodeFromCartOrSession(sessionCart)).willReturn("3030");
        willReturn(null).given(checkoutYourAddressController)
                .getRedirectionAndCheckSOH(modelMap, redirectAttributes,
                        CheckoutYourAddressController.DEFAULT_SOH_REDIRECT_URL, cartData);
        willDoNothing().given(checkoutYourAddressController)
                .addModelDataForSelectDeliveryAddress(cartData, modelMap, "cnc", null);
        checkoutYourAddressController.doChooseDeliveryAddress(modelMap, redirectAttributes);

        final Map<String, Object> map = modelMap.asMap();
        assertThat(map.containsKey("enteredPostCode")).isTrue();
    }

    @Test
    public void testDoChooseDeliveryAddressForPostCodeNotPopulated() throws CMSItemNotFoundException {
        final Model modelMap = new ExtendedModelMap();
        final TargetZoneDeliveryModeData targetZDMD = new TargetZoneDeliveryModeData();
        targetZDMD.setDeliveryToStore(false);
        targetZDMD.setCode("cnc");
        final TargetCartData cartData = new TargetCartData();
        cartData.setDeliveryMode(targetZDMD);

        final CartData sessionCart = mock(CartData.class);
        given(targetCartFacade.getSessionCart()).willReturn(sessionCart);
        given(checkoutFacade.getCheckoutCart()).willReturn(cartData);
        given(targetCartFacade.getPostalCodeFromCartOrSession(sessionCart)).willReturn("");
        willReturn(null).given(checkoutYourAddressController)
                .getRedirectionAndCheckSOH(modelMap, redirectAttributes,
                        CheckoutYourAddressController.DEFAULT_SOH_REDIRECT_URL, cartData);
        willDoNothing().given(checkoutYourAddressController)
                .addModelDataForSelectDeliveryAddress(cartData, modelMap, "cnc", null);
        checkoutYourAddressController.doChooseDeliveryAddress(modelMap, redirectAttributes);

        final Map<String, Object> map = modelMap.asMap();
        assertThat(map.containsKey("enteredPostCode")).isFalse();
    }

    @Test
    public void testSetupCheckoutDeliveryDataWhenPostCodeNotSupported() throws CMSItemNotFoundException {
        final TargetZoneDeliveryModeData targetZDMDED = new TargetZoneDeliveryModeData();
        targetZDMDED.setPostCodeNotSupported(true);
        final List<CheckoutDeliveryTypeData> checkoutDeliveryModes = new ArrayList<>();
        final CheckoutDeliveryTypeData checkoutDeliveryTypeData = new CheckoutDeliveryTypeData();
        given(Boolean.valueOf(
                targetDeliveryFacade.isDeliveryModePostCodeCombinationValid(anyString(), anyString())))
                        .willReturn(Boolean.FALSE);

        given(
                sessionService.getAttribute(anyString()))
                        .willReturn("3001");
        given(Boolean.valueOf(
                targetUserFacade.isCurrentUserAnonymous()))
                        .willReturn(Boolean.FALSE);
        checkoutYourAddressController.setupCheckoutDeliveryData(checkoutDeliveryModes, targetZDMDED,
                checkoutDeliveryTypeData);
        assertThat(checkoutDeliveryModes.get(0).isPostCodeNotSupported()).isFalse();
        assertThat(checkoutDeliveryModes.get(0).isAvailable()).isTrue();
    }

    @Test
    public void testSetupCheckoutDeliveryDataWhenPostCodeSupported() throws CMSItemNotFoundException {
        final TargetZoneDeliveryModeData targetZDMDED = new TargetZoneDeliveryModeData();
        targetZDMDED.setAvailable(true);
        targetZDMDED.setPostCodeNotSupported(false);
        final List<CheckoutDeliveryTypeData> checkoutDeliveryModes = new ArrayList<>();
        final CheckoutDeliveryTypeData checkoutDeliveryTypeData = new CheckoutDeliveryTypeData();
        given(Boolean.valueOf(
                targetDeliveryFacade.isDeliveryModePostCodeCombinationValid(anyString(), anyString())))
                        .willReturn(Boolean.TRUE);
        given(
                sessionService.getAttribute(anyString()))
                        .willReturn("3001");
        given(Boolean.valueOf(
                targetUserFacade.isCurrentUserAnonymous()))
                        .willReturn(Boolean.FALSE);
        checkoutYourAddressController.setupCheckoutDeliveryData(checkoutDeliveryModes, targetZDMDED,
                checkoutDeliveryTypeData);
        assertThat(checkoutDeliveryModes.get(0).isPostCodeNotSupported()).isFalse();
        assertThat(checkoutDeliveryModes.get(0).isAvailable()).isTrue();
    }

    @Test
    public void testAddModelDataForSelectDeliveryAddress() throws CMSItemNotFoundException {
        final TargetCartData cartData = mock(TargetCartData.class);
        final TargetZoneDeliveryModeData deliveryMode = mock(TargetZoneDeliveryModeData.class);
        willReturn(Arrays.asList(deliveryMode)).given(cartData).getDeliveryModes();

        checkoutYourAddressController.addModelDataForSelectDeliveryAddress(cartData, model, "cnc", null);
        verify(checkoutFacade, never()).overlayFluentStockDataOnDeliveryModes(anyList());
    }

    @Test
    public void testAddModelDataForSelectDeliveryAddressWithFluent() throws CMSItemNotFoundException {
        final TargetCartData cartData = mock(TargetCartData.class);
        final TargetZoneDeliveryModeData deliveryMode = mock(TargetZoneDeliveryModeData.class);
        willReturn(Arrays.asList(deliveryMode)).given(cartData).getDeliveryModes();
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isFluentEnabled();

        checkoutYourAddressController.addModelDataForSelectDeliveryAddress(cartData, model, "cnc", null);
        verify(checkoutFacade).overlayFluentStockDataOnDeliveryModes(Arrays.asList(deliveryMode));
    }
}
