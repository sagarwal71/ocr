/**
 * 
 */
package au.com.target.tgtstorefront.controllers.misc;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;
import au.com.target.tgtcore.order.TargetCommerceCartModificationStatus;
import au.com.target.tgtcore.product.exception.ProductNotFoundException;
import au.com.target.tgtfacades.giftcards.exceptions.GiftCardValidationException;
import au.com.target.tgtfacades.giftcards.validate.GiftCardValidationErrorType;
import au.com.target.tgtfacades.order.TargetCartFacade;
import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtstorefront.controllers.util.CartDataHelper;
import au.com.target.tgtstorefront.forms.AddToCartForm;
import au.com.target.tgtstorefront.forms.GiftRecipientForm;
import au.com.target.tgtstorefront.giftcards.converters.GiftRecipientFormToDTOConverter;
import au.com.target.tgtstorefront.security.cookie.EnhancedCookieGenerator;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
@UnitTest
public class AddToCartControllerTest {

    @InjectMocks
    private final AddToCartController addToCartController = new AddToCartController();

    @Mock
    private Model mockModel;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private TargetCartFacade targetCartFacade;

    @Mock
    private ProductFacade productFacade;

    @Mock
    private CartModificationData cartModificationData;

    @Mock
    private VariantOptionData variantOptionData;

    @Mock
    private HttpServletResponse response;

    @Mock
    private TargetUserFacade targetUserFacade;

    @Mock
    private SalesApplicationFacade salesApplicationFacade;

    @Mock
    private EnhancedCookieGenerator anonymousCartCookieGenerator;

    @Mock
    private List<SalesApplication> excludedSalesAppsToRestoreCart;

    @Mock
    private GiftRecipientFormToDTOConverter giftRecipientFormToDTOConverter;

    @Mock
    private I18NService i18nService;

    @Mock
    private MessageSource messageSource;

    @Mock
    private TargetProductFacade targetProductFacade;

    @Mock
    private TargetSharedConfigService targetSharedConfigService;

    @Mock
    private AddToCartForm addToCartForm;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        given(addToCartForm.getProductCode()).willReturn("P1");
        given(addToCartForm.getQty()).willReturn("1");
        given(addToCartForm.getIsReactRequest()).willReturn(Boolean.FALSE);
        willReturn(Boolean.FALSE).given(targetUserFacade).isCurrentUserAnonymous();
        willReturn(Boolean.FALSE).given(targetCartFacade).isProductAGiftCard("P1");
        willReturn(Boolean.FALSE).given(targetCartFacade).isProductADigitalGiftCard("P1");
        willReturn(Boolean.FALSE).given(targetCartFacade).hasVariantsForProduct("P1");
        willReturn(Boolean.FALSE).given(targetCartFacade).isProductAPhysicalGiftcard("P1");
    }

    @Test
    public void testNoSizeSelected() {
        willReturn(Boolean.TRUE).given(targetCartFacade).hasVariantsForProduct("P1");
        willReturn(Boolean.TRUE).given(targetCartFacade).hasSizeVariantsForProduct("P1");
        addToCartController.addToCart(addToCartForm, bindingResult, mockModel, response);
        verify(mockModel).addAttribute("errorMsg", "basket.error.product.size");
        verify(mockModel).addAttribute("isReactRequest", Boolean.FALSE);
    }

    @Test
    public void testSingleColourVariantNoColourOrSizeSelected() {
        willReturn(Boolean.TRUE).given(targetCartFacade).hasVariantsForProduct("P1");
        willReturn(Boolean.TRUE).given(targetCartFacade).hasSizeVariantsForProduct("P1");
        addToCartController.addToCart(addToCartForm, bindingResult, mockModel, response);
        verify(mockModel).addAttribute("errorMsg", "basket.error.product.size");
        verify(mockModel).addAttribute("isReactRequest", Boolean.FALSE);
    }

    @Test
    public void testMultipleColourVariantsNoColourOrSizeSelected() {
        willReturn(Boolean.TRUE).given(targetCartFacade).hasVariantsForProduct("P1");
        willReturn(Boolean.FALSE).given(targetCartFacade).hasSizeVariantsForProduct("P1");
        addToCartController.addToCart(addToCartForm, bindingResult, mockModel, response);
        verify(mockModel).addAttribute("errorMsg", "basket.error.product.colour");
        verify(mockModel).addAttribute("isReactRequest", Boolean.FALSE);
    }

    @Test
    public void testModificationSuccess() throws CommerceCartModificationException {
        given(cartModificationData.getStatusCode()).willReturn(TargetCommerceCartModificationStatus.SUCCESS);
        given(targetCartFacade.addToCart(Mockito.anyString(), Mockito.anyLong(), Mockito.any(GiftRecipientDTO.class)))
                .willReturn(cartModificationData);
        addToCartController.addToCart(addToCartForm, bindingResult, mockModel, response);
        verify(mockModel).addAttribute("success", Boolean.TRUE);
        verify(mockModel).addAttribute("isReactRequest", Boolean.FALSE);
    }

    @Test
    public void testModificationLowStock() throws CommerceCartModificationException {
        given(cartModificationData.getStatusCode()).willReturn(TargetCommerceCartModificationStatus.LOW_STOCK);
        given(targetCartFacade.addToCart(Mockito.anyString(), Mockito.anyLong(), Mockito.any(GiftRecipientDTO.class)))
                .willReturn(cartModificationData);
        given(addToCartForm.getQty()).willReturn("5");
        addToCartController.addToCart(addToCartForm, bindingResult, mockModel, response);
        verify(mockModel).addAttribute("success", Boolean.TRUE);
        verify(mockModel).addAttribute("errorMsg",
                CartDataHelper.CART_MESSAGES + TargetCommerceCartModificationStatus.LOW_STOCK);
        verify(mockModel).addAttribute("isReactRequest", Boolean.FALSE);
    }

    @Test
    public void testModificationPreOrderLowStock() throws CommerceCartModificationException {
        given(cartModificationData.getStatusCode()).willReturn(TargetCommerceCartModificationStatus.PREORDER_LOW_STOCK);
        given(targetCartFacade.addToCart(Mockito.anyString(), Mockito.anyLong(), Mockito.any(GiftRecipientDTO.class)))
                .willReturn(cartModificationData);
        given(addToCartForm.getQty()).willReturn("5");
        addToCartController.addToCart(addToCartForm, bindingResult, mockModel, response);
        verify(mockModel).addAttribute("success", Boolean.TRUE);
        verify(mockModel).addAttribute("errorMsg",
                CartDataHelper.CART_MESSAGES + TargetCommerceCartModificationStatus.PREORDER_LOW_STOCK);
        verify(mockModel).addAttribute("isReactRequest", Boolean.FALSE);
    }

    @Test
    public void testModificationThrowsExceptionWithPreOrderMessage() throws CommerceCartModificationException {
        final CommerceCartModificationException expection = new CommerceCartModificationException(
                TgtCoreConstants.CART.ERR_PREORDER_CART);
        given(targetCartFacade.addToCart(Mockito.anyString(), Mockito.anyLong(), Mockito.any(GiftRecipientDTO.class)))
                .willThrow(expection);
        given(addToCartForm.getQty()).willReturn("5");
        addToCartController.addToCart(addToCartForm, bindingResult, mockModel, response);
        verify(mockModel).addAttribute("success", Boolean.FALSE);
        verify(mockModel).addAttribute("errorMsg",
                "basket.preOrder.error.occurred");
        verify(mockModel).addAttribute("isReactRequest", Boolean.FALSE);
    }

    @Test
    public void testModificationThrowsExceptionWithNonPreOrderMessage() throws CommerceCartModificationException {
        final CommerceCartModificationException expection = new CommerceCartModificationException(
                "test message");
        given(targetCartFacade.addToCart(Mockito.anyString(), Mockito.anyLong(), Mockito.any(GiftRecipientDTO.class)))
                .willThrow(expection);
        given(addToCartForm.getQty()).willReturn("5");
        addToCartController.addToCart(addToCartForm, bindingResult, mockModel, response);
        verify(mockModel).addAttribute("success", Boolean.FALSE);
        verify(mockModel).addAttribute("errorMsg",
                "basket.error.occurred");
        verify(mockModel).addAttribute("isReactRequest", Boolean.FALSE);
    }

    @Test
    public void testModificationNoStock() throws CommerceCartModificationException {
        given(cartModificationData.getStatusCode()).willReturn(TargetCommerceCartModificationStatus.NO_STOCK);
        given(targetCartFacade.addToCart(Mockito.anyString(), Mockito.anyLong(), Mockito.any(GiftRecipientDTO.class)))
                .willReturn(cartModificationData);
        given(addToCartForm.getQty()).willReturn("5");
        addToCartController.addToCart(addToCartForm, bindingResult, mockModel, response);
        verify(mockModel).addAttribute("success", Boolean.FALSE);
        verify(mockModel).addAttribute("errorMsg",
                CartDataHelper.CART_MESSAGES + TargetCommerceCartModificationStatus.NO_STOCK);
        verify(mockModel).addAttribute("isReactRequest", Boolean.FALSE);
    }



    @Test
    public void testModificationInsufficientQty() throws CommerceCartModificationException {
        given(cartModificationData.getStatusCode()).willReturn(
                TargetCommerceCartModificationStatus.NO_MODFICATION_OCCURED_INSUFFICIENT_QUANTITY);
        given(targetCartFacade.addToCart(Mockito.anyString(), Mockito.anyLong(), Mockito.any(GiftRecipientDTO.class)))
                .willReturn(cartModificationData);
        given(addToCartForm.getQty()).willReturn("5");
        addToCartController.addToCart(addToCartForm, bindingResult, mockModel, response);
        verify(mockModel).addAttribute("success", Boolean.FALSE);
        verify(mockModel).addAttribute("errorMsg", CartDataHelper.CART_MESSAGES
                + TargetCommerceCartModificationStatus.NO_MODFICATION_OCCURED_INSUFFICIENT_QUANTITY);
        verify(mockModel).addAttribute("isReactRequest", Boolean.FALSE);
    }

    @Test
    public void testModificationException() throws CommerceCartModificationException {
        willReturn(Boolean.TRUE).given(targetCartFacade).isProductAGiftCard("P1");
        willReturn(Boolean.TRUE).given(targetCartFacade).isProductADigitalGiftCard("P1");
        doThrow(new CommerceCartModificationException("Cannot add to cart")).when(targetCartFacade)
                .addToCart(Mockito.anyString(), Mockito.anyLong(), Mockito.any(GiftRecipientDTO.class));
        given(addToCartForm.getQty()).willReturn("5");
        addToCartController.addToCart(addToCartForm, bindingResult, mockModel, response);
        verify(mockModel).addAttribute("success", Boolean.FALSE);
        verify(mockModel).addAttribute("errorMsg", "basket.error.occurred");
        verify(mockModel).addAttribute("quantity", Long.valueOf(0L));
        verify(mockModel).addAttribute("isReactRequest", Boolean.FALSE);
    }

    @Test
    public void testsetAnonymousCartCookie() throws CommerceCartModificationException {
        final SalesApplication salesApp = mock(SalesApplication.class);
        given(salesApplicationFacade.getCurrentSalesApplication()).willReturn(
                salesApp);
        willReturn(Boolean.FALSE).given(excludedSalesAppsToRestoreCart).contains(salesApp);
        willReturn(Boolean.TRUE).given(targetUserFacade).isCurrentUserAnonymous();
        given(targetCartFacade.getGuidForCurrentSessionCart()).willReturn("guid");
        addToCartController.setAnonymousCartCookie(response);
        verify(anonymousCartCookieGenerator).addCookie(Mockito.any(HttpServletResponse.class),
                Mockito.anyString());
    }

    @Test
    public void testsetAnonymousCartCookieIfKIOSK() throws CommerceCartModificationException {
        willReturn(Boolean.TRUE).given(targetUserFacade).isCurrentUserAnonymous();
        final SalesApplication salesApp = Mockito.mock(SalesApplication.class);
        given(salesApplicationFacade.getCurrentSalesApplication()).willReturn(
                salesApp);
        willReturn(Boolean.TRUE).given(excludedSalesAppsToRestoreCart).contains(salesApp);
        given(targetCartFacade.getGuidForCurrentSessionCart()).willReturn("guid");
        addToCartController.setAnonymousCartCookie(response);
        verifyZeroInteractions(anonymousCartCookieGenerator);
    }

    @Test
    public void testModificationWithGiftRecipient() throws CommerceCartModificationException {
        willReturn(Boolean.TRUE).given(targetCartFacade).isProductAGiftCard("P1");
        willReturn(Boolean.TRUE).given(targetCartFacade).isProductADigitalGiftCard("P1");
        given(cartModificationData.getStatusCode()).willReturn(TargetCommerceCartModificationStatus.SUCCESS);
        given(targetCartFacade.addToCart(Mockito.anyString(), Mockito.anyLong(), Mockito.any(GiftRecipientDTO.class)))
                .willReturn(cartModificationData);
        final GiftRecipientForm giftRecipientForm = mock(GiftRecipientForm.class);
        given(addToCartForm.getGiftRecipientForm()).willReturn(giftRecipientForm);
        addToCartController.addToCart(addToCartForm, bindingResult, mockModel, response);
        verify(mockModel).addAttribute("success", Boolean.TRUE);
        verify(mockModel).addAttribute("isReactRequest", Boolean.FALSE);
    }

    @Test
    public void testgivenQtyValidationException()
            throws CommerceCartModificationException, GiftCardValidationException, ProductNotFoundException {
        willReturn(Boolean.TRUE).given(targetCartFacade).isProductAGiftCard("P1");
        willReturn(Boolean.TRUE).given(targetCartFacade).isProductADigitalGiftCard("P1");
        doThrow(new GiftCardValidationException("Max quantity reached", GiftCardValidationErrorType.QUANTITY))
                .when(targetCartFacade).validateGiftCards(Mockito.anyString(), Mockito.anyLong());
        given(messageSource.getMessage(Mockito.anyString(), Mockito.any(Object[].class), Mockito.any(Locale.class)))
                .willReturn("ERROR");
        final GiftRecipientForm giftRecipientForm = mock(GiftRecipientForm.class);
        given(addToCartForm.getGiftRecipientForm()).willReturn(giftRecipientForm);
        addToCartController.addToCart(addToCartForm, bindingResult, mockModel, response);
        verify(targetCartFacade, never()).addToCart(Mockito.anyString(), Mockito.anyLong(),
                Mockito.any(GiftRecipientDTO.class));
        verify(targetCartFacade).getSessionCart();
        verify(mockModel).addAttribute("isReactRequest", Boolean.FALSE);
    }

    @Test
    public void testValidPhysicalGiftCardAddToCart() throws CommerceCartModificationException {
        willReturn(Boolean.TRUE).given(targetCartFacade).isProductAGiftCard("P1");
        willReturn(Boolean.TRUE).given(targetCartFacade).isProductADigitalGiftCard("P1");
        given(cartModificationData.getStatusCode()).willReturn(TargetCommerceCartModificationStatus.SUCCESS);
        given(targetCartFacade.addToCart(Mockito.anyString(), Mockito.anyLong(), Mockito.any(GiftRecipientDTO.class)))
                .willReturn(cartModificationData);
        final GiftRecipientForm giftRecipientForm = mock(GiftRecipientForm.class);
        given(addToCartForm.getGiftRecipientForm()).willReturn(giftRecipientForm);
        addToCartController.addToCart(addToCartForm, bindingResult, mockModel, response);
        verify(mockModel).addAttribute("success", Boolean.TRUE);
        verify(mockModel).addAttribute("isReactRequest", Boolean.FALSE);
    }

    @Test
    public void testAboveQuantityLimitPhysicalGiftCardAddToCart()
            throws CommerceCartModificationException, GiftCardValidationException, ProductNotFoundException {
        willReturn(Boolean.TRUE).given(targetCartFacade).isProductAGiftCard("P1");
        willReturn(Boolean.TRUE).given(targetCartFacade).isProductAPhysicalGiftcard("P1");
        doThrow(new GiftCardValidationException("Max quantity reached", GiftCardValidationErrorType.QUANTITY))
                .when(targetCartFacade).validatePhysicalGiftCards(Mockito.anyString(), Mockito.anyLong());
        given(messageSource.getMessage(Mockito.anyString(), Mockito.any(Object[].class), Mockito.any(Locale.class)))
                .willReturn("ERROR");
        final GiftRecipientForm giftRecipientForm = mock(GiftRecipientForm.class);
        given(addToCartForm.getGiftRecipientForm()).willReturn(giftRecipientForm);
        addToCartController.addToCart(addToCartForm, bindingResult, mockModel, response);
        verify(targetCartFacade, never()).addToCart(Mockito.anyString(), Mockito.anyLong(),
                Mockito.any(GiftRecipientDTO.class));
        verify(targetCartFacade).getSessionCart();
    }

    @Test
    public void testPopulatesIsReactRequestAttributeAsTrue() throws CommerceCartModificationException {
        given(addToCartForm.getIsReactRequest()).willReturn(Boolean.TRUE);
        given(cartModificationData.getStatusCode()).willReturn(TargetCommerceCartModificationStatus.SUCCESS);
        given(targetCartFacade.addToCart(Mockito.anyString(), Mockito.anyLong(), Mockito.any(GiftRecipientDTO.class)))
                .willReturn(cartModificationData);
        addToCartController.addToCart(addToCartForm, bindingResult, mockModel, response);
        verify(mockModel).addAttribute("success", Boolean.TRUE);
        verify(mockModel).addAttribute("isReactRequest", Boolean.TRUE);
    }
}
