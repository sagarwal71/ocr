package au.com.target.tgtstorefront.controllers.util;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class CMSComponentHelperTest {

    @Test
    public void testInteger() {
        Assert.assertEquals(10, CMSComponentHelper.safeQuantity(Integer.valueOf(10)));
    }

    @Test
    public void testNull() {
        Assert.assertEquals(0, CMSComponentHelper.safeQuantity(null));
    }

    @Test
    public void testNegative() {
        Assert.assertEquals(0, CMSComponentHelper.safeQuantity(Integer.valueOf(-10)));
    }

}
