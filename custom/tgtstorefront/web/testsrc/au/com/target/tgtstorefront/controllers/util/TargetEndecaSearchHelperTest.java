/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import au.com.target.endeca.infront.assemble.impl.EndecaPageAssemble;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.converter.EndecaToFacetsConverter;
import au.com.target.endeca.infront.converter.EndecaToSortConverter;
import au.com.target.endeca.infront.data.EndecaDimension;
import au.com.target.endeca.infront.data.EndecaOptions;
import au.com.target.endeca.infront.data.EndecaProduct;
import au.com.target.endeca.infront.data.EndecaRecords;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaWrapperException;
import au.com.target.endeca.infront.transform.TargetEndecaURLHelper;
import au.com.target.tgtfacades.category.data.TargetCategoryData;
import au.com.target.tgtfacades.product.data.BrandData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.search.data.TargetProductCategorySearchPageData;
import au.com.target.tgtfacades.sort.TargetSortData;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.controllers.pages.data.SeachEvaluatorData;
import au.com.target.tgtstorefront.controllers.pages.data.ViewPreferencesInitialData;
import au.com.target.tgtstorefront.controllers.pages.preferences.StoreViewPreferencesHandler;
import au.com.target.tgtstorefront.util.SearchQuerySanitiser;


/**
 * @author bhuang3
 *
 */
@SuppressWarnings("deprecation")
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetEndecaSearchHelperTest {

    private static final Integer DEFAULT_MAX_SEARCH_TEXT_LENGTH = Integer.valueOf(50);

    @Mock
    private EndecaPageAssemble endecaPageAssemble;
    @Mock
    private EndecaToFacetsConverter endecaToFacetsConverter;
    @Mock
    private Converter<EndecaProduct, TargetProductListerData> targetProductListerConverter;
    @Mock
    private TargetEndecaURLHelper targetEndecaURLHelper;
    @Mock
    private EndecaToSortConverter endecaToSortConverter;
    @Mock
    private StoreViewPreferencesHandler storeViewPreferencesHandler;
    @Mock
    private ConfigurationService configurationService;
    @Mock
    private SearchQuerySanitiser searchQuerySanitiser;
    @Mock
    private Configuration configuration;
    @InjectMocks
    @Spy
    private final TargetEndecaSearchHelper targetEndecaSearchHelper = new TargetEndecaSearchHelper();
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;

    @Before
    public void setup() {
        given(configurationService.getConfiguration()).willReturn(configuration);
        given(configuration.getInteger(
                "storefront.search.maxSearchTextLength", DEFAULT_MAX_SEARCH_TEXT_LENGTH)).willReturn(
                        DEFAULT_MAX_SEARCH_TEXT_LENGTH);
    }

    @Test
    public void testSanitizeSearchText() {
        final String searchText = "test";
        final Model model = mock(Model.class);
        given(searchQuerySanitiser.sanitiseSearchText(searchText)).willReturn(searchText);
        final String result = targetEndecaSearchHelper.sanitizeSearchText(searchText, model);
        verify(searchQuerySanitiser).sanitiseSearchText(searchText);
        assertThat(result).as("sanitised result").isEqualTo(searchText);
    }

    @Test
    public void testSanitizeSearchTextWithSubstring() {
        final String searchText = "012345678901234567890123456789012345678901234567890123456789";
        final String searchTextAfterSubstring = "01234567890123456789012345678901234567890123456789";
        final Model model = mock(Model.class);
        given(searchQuerySanitiser.sanitiseSearchText(searchTextAfterSubstring)).willReturn(searchTextAfterSubstring);
        final String result = targetEndecaSearchHelper.sanitizeSearchText(searchText, model);
        assertThat(result).as("sanitised result").isEqualTo(searchTextAfterSubstring);
    }

    @Test
    public void testHandlePreferencesInitialData() {
        final String categoryCode = "testCategoryCode";
        final String navigationState = "testState";
        final String sortCodeInput = "testSortCodeInput";
        final String sortCode = "testSortCode";
        final String viewAs = "testViewAs";
        final String pageSize = "30";
        final int pageSizeInt = 30;
        given(
                storeViewPreferencesHandler.getViewAsPreference(any(HttpServletResponse.class),
                        any(ViewPreferencesInitialData.class), anyString())).willReturn(viewAs);
        given(
                storeViewPreferencesHandler.getSortCodePreference(any(HttpServletResponse.class),
                        any(ViewPreferencesInitialData.class), anyString())).willReturn(sortCode);
        given(
                Integer.valueOf(storeViewPreferencesHandler.getItemPerPagePreference(any(HttpServletResponse.class),
                        any(ViewPreferencesInitialData.class), anyInt()))).willReturn(Integer.valueOf(pageSizeInt));
        final SeachEvaluatorData result = targetEndecaSearchHelper.handlePreferencesInitialData(request, response,
                categoryCode, navigationState, sortCodeInput, viewAs, pageSize, StringUtils.EMPTY);
        assertThat(result.getItemPerPageFn()).as("page size").isEqualTo(pageSizeInt);
        assertThat(result.getSortCodeFn()).as("sort code").isEqualTo(sortCode);
        assertThat(result.getViewAsFn()).as("view as").isEqualTo(viewAs);
    }

    @Test
    public void testHandlePreferencesInitialDataWithSearchText() {
        final String categoryCode = "testCategoryCode";
        final String navigationState = "testState";
        final String sortCodeInput = "testSortCodeInput";
        final String sortCode = "testSortCode";
        final String viewAs = "testViewAs";
        final String pageSize = "30";
        final int pageSizeInt = 30;
        given(
                storeViewPreferencesHandler.getViewAsPreference(any(HttpServletResponse.class),
                        any(ViewPreferencesInitialData.class), anyString())).willReturn(viewAs);
        given(
                storeViewPreferencesHandler.getSortCodePreference(any(HttpServletResponse.class),
                        any(ViewPreferencesInitialData.class), anyString())).willReturn(sortCode);
        given(
                Integer.valueOf(storeViewPreferencesHandler.getItemPerPagePreference(any(HttpServletResponse.class),
                        any(ViewPreferencesInitialData.class), anyInt()))).willReturn(Integer.valueOf(pageSizeInt));
        final SeachEvaluatorData result = targetEndecaSearchHelper.handlePreferencesInitialData(request, response,
                categoryCode, navigationState, sortCodeInput, viewAs, pageSize, "searchText");
        assertThat(result.getItemPerPageFn()).as("page size").isEqualTo(pageSizeInt);
        assertThat(result.getSortCodeFn()).as("sort code").isEqualTo(sortCodeInput);
        assertThat(result.getViewAsFn()).as("view as").isEqualTo(viewAs);
    }

    @Test
    public void testGetEndecaSearchResultWithNotPageUrl() throws JAXBException, TransformerException, IOException,
            TargetEndecaWrapperException {
        final EndecaSearch mockEndecaSearch = mock(EndecaSearch.class);
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        given(
                endecaPageAssemble.assemble(any(HttpServletRequest.class), anyString(),
                        any(EndecaSearchStateData.class), anyMap())).willReturn(mockEndecaSearch);
        final EndecaSearch searchResults = targetEndecaSearchHelper.getEndecaSearchResult(request,
                endecaSearchStateData);
        assertThat(searchResults).as("endeca search result").isEqualTo(null);
    }

    @Test
    public void testGetEndecaSearchResultWithSearch() throws JAXBException, TransformerException, IOException,
            TargetEndecaWrapperException {
        final EndecaSearch mockEndecaSearch = mock(EndecaSearch.class);
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setSearchTerm(Arrays.asList("testTerm"));
        given(
                endecaPageAssemble.assemble(any(HttpServletRequest.class), anyString(),
                        any(EndecaSearchStateData.class), anyMap())).willReturn(mockEndecaSearch);
        final EndecaSearch searchResults = targetEndecaSearchHelper.getEndecaSearchResult(request,
                endecaSearchStateData);
        assertThat(searchResults).as("endeca search result").isEqualTo(mockEndecaSearch);
        final ArgumentCaptor<Map> argumentCaptor1 = ArgumentCaptor.forClass(Map.class);
        final ArgumentCaptor<HttpServletRequest> argumentCaptor2 = ArgumentCaptor.forClass(HttpServletRequest.class);
        final ArgumentCaptor<String> argumentCaptor3 = ArgumentCaptor.forClass(String.class);
        final ArgumentCaptor<EndecaSearchStateData> argumentCaptor4 = ArgumentCaptor
                .forClass(EndecaSearchStateData.class);
        verify(endecaPageAssemble).assemble(argumentCaptor2.capture(), argumentCaptor3.capture(),
                argumentCaptor4.capture(), argumentCaptor1.capture());
        assertThat(argumentCaptor1.getValue().size()).as("dims size").isEqualTo(0);
        assertThat(argumentCaptor2.getValue()).as("request").isEqualTo(request);
        assertThat(argumentCaptor3.getValue()).as("page url").isEqualTo(
                EndecaConstants.EndecaPageUris.ENDECA_SEARCH_PAGE_URI);
        assertThat(argumentCaptor4.getValue()).as("search state data").isEqualTo(endecaSearchStateData);
    }

    @Test
    public void testGetEndecaSearchResultWithBrand() throws JAXBException, TransformerException, IOException,
            TargetEndecaWrapperException {
        final EndecaSearch mockEndecaSearch = mock(EndecaSearch.class);
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        final BrandData mockBrand = mock(BrandData.class);
        given(mockBrand.getName()).willReturn("testName");
        endecaSearchStateData.setBrand(mockBrand);
        given(
                endecaPageAssemble.assemble(any(HttpServletRequest.class), anyString(),
                        any(EndecaSearchStateData.class), anyMap())).willReturn(mockEndecaSearch);
        final EndecaSearch searchResults = targetEndecaSearchHelper.getEndecaSearchResult(request,
                endecaSearchStateData);
        assertThat(searchResults).as("endeca search result").isEqualTo(mockEndecaSearch);
        final ArgumentCaptor<Map> argumentCaptor1 = ArgumentCaptor.forClass(Map.class);
        final ArgumentCaptor<HttpServletRequest> argumentCaptor2 = ArgumentCaptor.forClass(HttpServletRequest.class);
        final ArgumentCaptor<String> argumentCaptor3 = ArgumentCaptor.forClass(String.class);
        final ArgumentCaptor<EndecaSearchStateData> argumentCaptor4 = ArgumentCaptor
                .forClass(EndecaSearchStateData.class);
        verify(endecaPageAssemble).assemble(argumentCaptor2.capture(), argumentCaptor3.capture(),
                argumentCaptor4.capture(), argumentCaptor1.capture());
        assertThat(argumentCaptor1.getValue().size()).as("dims size").isEqualTo(1);
        final Map<String, String> dims = new HashMap<String, String>();
        dims.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND, endecaSearchStateData.getBrand()
                .getName());
        assertThat(argumentCaptor1.getValue()).as("dims").isEqualTo(dims);
        assertThat(argumentCaptor2.getValue()).as("request").isEqualTo(request);
        assertThat(argumentCaptor3.getValue()).as("page url").isEqualTo(
                EndecaConstants.EndecaPageUris.ENDECA_BRAND_PAGE_URI);
        assertThat(argumentCaptor4.getValue()).as("search state data").isEqualTo(endecaSearchStateData);
    }

    @Test
    public void testGetEndecaSearchResultWithCategory() throws JAXBException, TransformerException, IOException,
            TargetEndecaWrapperException {
        final EndecaSearch mockEndecaSearch = mock(EndecaSearch.class);
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        final TargetCategoryData mockTargetCategoryData = mock(TargetCategoryData.class);
        endecaSearchStateData.setCategoryData(mockTargetCategoryData);
        given(
                endecaPageAssemble.assemble(any(HttpServletRequest.class), anyString(),
                        any(EndecaSearchStateData.class), anyMap())).willReturn(mockEndecaSearch);
        final EndecaSearch searchResults = targetEndecaSearchHelper.getEndecaSearchResult(request,
                endecaSearchStateData);
        assertThat(searchResults).as("endeca search result").isEqualTo(mockEndecaSearch);
        final ArgumentCaptor<Map> argumentCaptor1 = ArgumentCaptor.forClass(Map.class);
        final ArgumentCaptor<HttpServletRequest> argumentCaptor2 = ArgumentCaptor.forClass(HttpServletRequest.class);
        final ArgumentCaptor<String> argumentCaptor3 = ArgumentCaptor.forClass(String.class);
        final ArgumentCaptor<EndecaSearchStateData> argumentCaptor4 = ArgumentCaptor
                .forClass(EndecaSearchStateData.class);
        verify(endecaPageAssemble).assemble(argumentCaptor2.capture(), argumentCaptor3.capture(),
                argumentCaptor4.capture(), argumentCaptor1.capture());
        assertThat(argumentCaptor1.getValue().size()).as("dims size").isEqualTo(1);
        final Map<String, String> dims = new HashMap<String, String>();
        dims.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY, mockTargetCategoryData.getCode());
        assertThat(argumentCaptor1.getValue()).as("dims").isEqualTo(dims);
        assertThat(argumentCaptor2.getValue()).as("request").isEqualTo(request);
        assertThat(argumentCaptor3.getValue()).as("page url").isEqualTo(
                EndecaConstants.EndecaPageUris.ENDECA_CATEGORY_PAGE_URI);
        assertThat(argumentCaptor4.getValue()).as("search state data").isEqualTo(endecaSearchStateData);
    }

    @Test
    public void testGetEndecaSearchResultWithQualifierDealCategory() throws JAXBException, TransformerException,
            IOException,
            TargetEndecaWrapperException {
        final EndecaSearch mockEndecaSearch = mock(EndecaSearch.class);
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        final TargetCategoryData mockTargetCategoryData = mock(TargetCategoryData.class);
        given(Boolean.valueOf(mockTargetCategoryData.isQualifierDealCategory())).willReturn(Boolean.TRUE);
        endecaSearchStateData.setCategoryData(mockTargetCategoryData);
        given(
                endecaPageAssemble.assemble(any(HttpServletRequest.class), anyString(),
                        any(EndecaSearchStateData.class), anyMap())).willReturn(mockEndecaSearch);
        final EndecaSearch searchResults = targetEndecaSearchHelper.getEndecaSearchResult(request,
                endecaSearchStateData);
        assertThat(searchResults).as("endeca search result").isEqualTo(mockEndecaSearch);
        final ArgumentCaptor<Map> argumentCaptor1 = ArgumentCaptor.forClass(Map.class);
        final ArgumentCaptor<HttpServletRequest> argumentCaptor2 = ArgumentCaptor.forClass(HttpServletRequest.class);
        final ArgumentCaptor<String> argumentCaptor3 = ArgumentCaptor.forClass(String.class);
        final ArgumentCaptor<EndecaSearchStateData> argumentCaptor4 = ArgumentCaptor
                .forClass(EndecaSearchStateData.class);
        verify(endecaPageAssemble).assemble(argumentCaptor2.capture(), argumentCaptor3.capture(),
                argumentCaptor4.capture(), argumentCaptor1.capture());
        assertThat(argumentCaptor1.getValue().size()).as("dims size").isEqualTo(1);
        final Map<String, String> dims = new HashMap<String, String>();
        dims.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_QUALIFIER_CATEGORY,
                mockTargetCategoryData.getCode());
        assertThat(argumentCaptor1.getValue()).as("dims").isEqualTo(dims);
        assertThat(argumentCaptor2.getValue()).as("request").isEqualTo(request);
        assertThat(argumentCaptor3.getValue()).as("page url").isEqualTo(
                EndecaConstants.EndecaPageUris.ENDECA_DEAL_QUALIFIER_CATEGORY_PAGE_URI);
        assertThat(argumentCaptor4.getValue()).as("search state data").isEqualTo(endecaSearchStateData);
    }

    @Test
    public void testGetEndecaSearchResultWithRewardDealCategory() throws JAXBException, TransformerException,
            IOException,
            TargetEndecaWrapperException {
        final EndecaSearch mockEndecaSearch = mock(EndecaSearch.class);
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        final TargetCategoryData mockTargetCategoryData = mock(TargetCategoryData.class);
        given(Boolean.valueOf(mockTargetCategoryData.isRewardDealCategory())).willReturn(Boolean.TRUE);
        endecaSearchStateData.setCategoryData(mockTargetCategoryData);
        given(
                endecaPageAssemble.assemble(any(HttpServletRequest.class), anyString(),
                        any(EndecaSearchStateData.class), anyMap())).willReturn(mockEndecaSearch);
        final EndecaSearch searchResults = targetEndecaSearchHelper.getEndecaSearchResult(request,
                endecaSearchStateData);
        assertThat(searchResults).as("endeca search result").isEqualTo(mockEndecaSearch);
        final ArgumentCaptor<Map> argumentCaptor1 = ArgumentCaptor.forClass(Map.class);
        final ArgumentCaptor<HttpServletRequest> argumentCaptor2 = ArgumentCaptor.forClass(HttpServletRequest.class);
        final ArgumentCaptor<String> argumentCaptor3 = ArgumentCaptor.forClass(String.class);
        final ArgumentCaptor<EndecaSearchStateData> argumentCaptor4 = ArgumentCaptor
                .forClass(EndecaSearchStateData.class);
        verify(endecaPageAssemble).assemble(argumentCaptor2.capture(), argumentCaptor3.capture(),
                argumentCaptor4.capture(), argumentCaptor1.capture());
        assertThat(argumentCaptor1.getValue().size()).as("dims size").isEqualTo(1);
        final Map<String, String> dims = new HashMap<String, String>();
        dims.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_REWARD_CATEGORY,
                mockTargetCategoryData.getCode());
        assertThat(argumentCaptor1.getValue()).as("dims").isEqualTo(dims);
        assertThat(argumentCaptor2.getValue()).as("request").isEqualTo(request);
        assertThat(argumentCaptor3.getValue()).as("page url").isEqualTo(
                EndecaConstants.EndecaPageUris.ENDECA_DEAL_REWARD_CATEGORY_PAGE_URI);
        assertThat(argumentCaptor4.getValue()).as("search state data").isEqualTo(endecaSearchStateData);
    }

    @Test
    public void testGetEndecaSearchResultWithCategoryAndStoreNumber() throws JAXBException, TransformerException,
            IOException,
            TargetEndecaWrapperException {
        final EndecaSearch mockEndecaSearch = mock(EndecaSearch.class);
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        final TargetCategoryData mockTargetCategoryData = mock(TargetCategoryData.class);
        endecaSearchStateData.setCategoryData(mockTargetCategoryData);
        endecaSearchStateData.setStoreNumber("testStoreNumber");
        given(
                endecaPageAssemble.assemble(any(HttpServletRequest.class), anyString(),
                        any(EndecaSearchStateData.class), anyMap())).willReturn(mockEndecaSearch);
        final EndecaSearch searchResults = targetEndecaSearchHelper.getEndecaSearchResult(request,
                endecaSearchStateData);
        assertThat(searchResults).as("endeca search result").isEqualTo(mockEndecaSearch);
        final ArgumentCaptor<Map> argumentCaptor1 = ArgumentCaptor.forClass(Map.class);
        final ArgumentCaptor<HttpServletRequest> argumentCaptor2 = ArgumentCaptor.forClass(HttpServletRequest.class);
        final ArgumentCaptor<String> argumentCaptor3 = ArgumentCaptor.forClass(String.class);
        final ArgumentCaptor<EndecaSearchStateData> argumentCaptor4 = ArgumentCaptor
                .forClass(EndecaSearchStateData.class);
        verify(endecaPageAssemble).assemble(argumentCaptor2.capture(), argumentCaptor3.capture(),
                argumentCaptor4.capture(), argumentCaptor1.capture());
        assertThat(argumentCaptor1.getValue().size()).as("dims size").isEqualTo(2);
        final Map<String, String> dims = new HashMap<String, String>();
        dims.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY, mockTargetCategoryData.getCode());
        dims.put(EndecaConstants.EndecaRecordSpecificFields.BULKYBOARD,
                endecaSearchStateData.getStoreNumber());
        assertThat(argumentCaptor1.getValue()).as("dims").isEqualTo(dims);
        assertThat(argumentCaptor2.getValue()).as("request").isEqualTo(request);
        assertThat(argumentCaptor3.getValue()).as("page url").isEqualTo(
                EndecaConstants.EndecaPageUris.ENDECA_CATEGORY_PAGE_URI);
        assertThat(argumentCaptor4.getValue()).as("search state data").isEqualTo(endecaSearchStateData);
    }

    @Test
    public void testGetEndecaSearchResultWithTargetEndecaWrapperException() throws JAXBException, TransformerException,
            IOException,
            TargetEndecaWrapperException {
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setSearchTerm(Arrays.asList("testTerm"));
        given(
                endecaPageAssemble.assemble(any(HttpServletRequest.class), anyString(),
                        any(EndecaSearchStateData.class), anyMap())).willThrow(new TargetEndecaWrapperException());
        final EndecaSearch searchResults = targetEndecaSearchHelper.getEndecaSearchResult(request,
                endecaSearchStateData);
        assertThat(searchResults).as("endeca search result").isEqualTo(null);
    }

    @Test
    public void testGetEndecaSearchResultWithJAXBException() throws JAXBException, TransformerException,
            IOException,
            TargetEndecaWrapperException {
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setSearchTerm(Arrays.asList("testTerm"));
        given(
                endecaPageAssemble.assemble(any(HttpServletRequest.class), anyString(),
                        any(EndecaSearchStateData.class), anyMap())).willThrow(new JAXBException("error"));
        final EndecaSearch searchResults = targetEndecaSearchHelper.getEndecaSearchResult(request,
                endecaSearchStateData);
        assertThat(searchResults).as("endeca search result").isEqualTo(null);
    }

    @Test
    public void testGetEndecaSearchResultWithTransformerException() throws JAXBException, TransformerException,
            IOException,
            TargetEndecaWrapperException {
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setSearchTerm(Arrays.asList("testTerm"));
        given(
                endecaPageAssemble.assemble(any(HttpServletRequest.class), anyString(),
                        any(EndecaSearchStateData.class), anyMap())).willThrow(new TransformerException("error"));
        final EndecaSearch searchResults = targetEndecaSearchHelper.getEndecaSearchResult(request,
                endecaSearchStateData);
        assertThat(searchResults).as("endeca search result").isEqualTo(null);
    }

    @Test
    public void testGetEndecaSearchResultWithIOException() throws JAXBException, TransformerException,
            IOException,
            TargetEndecaWrapperException {
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setSearchTerm(Arrays.asList("testTerm"));
        given(
                endecaPageAssemble.assemble(any(HttpServletRequest.class), anyString(),
                        any(EndecaSearchStateData.class), anyMap())).willThrow(new IOException("error"));
        final EndecaSearch searchResults = targetEndecaSearchHelper.getEndecaSearchResult(request,
                endecaSearchStateData);
        assertThat(searchResults).as("endeca search result").isEqualTo(null);
    }

    @Test
    public void testPopulateSearchPageDataWithNullSearchResults() {
        final EndecaSearch searchResults = null;
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData = new TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData>();
        targetEndecaSearchHelper.populateSearchPageData(searchPageData, searchResults, endecaSearchStateData);
        assertThat(searchPageData.getErrorStatus()).as("error status").isEqualTo(
                EndecaConstants.EndecaErrorCodes.ENDECA_ERROR_STATUS);
    }

    @Test
    public void testPopulateSearchPageDataWithRedirectLink() {
        final EndecaSearch searchResults = new EndecaSearch();
        final String redirectLink = "testLink";
        searchResults.setRedirectLink(redirectLink);
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData = new TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData>();
        targetEndecaSearchHelper.populateSearchPageData(searchPageData, searchResults, endecaSearchStateData);
        assertThat(searchPageData.getErrorStatus()).as("error status").isEqualTo(null);
        assertThat(searchPageData.getKeywordRedirectUrl()).as("redirect link").isEqualTo(redirectLink);
    }

    @Test
    public void testPopulateSearchPageDataWithEmptyResults() {
        final EndecaSearch searchResults = new EndecaSearch();
        searchResults.setRedirectLink(null);
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        final TargetCategoryData categoryData = new TargetCategoryData();
        final String categoryCode = "testcode";
        categoryData.setCode(categoryCode);
        endecaSearchStateData.setCategoryData(categoryData);
        final BrandData brand = new BrandData();
        endecaSearchStateData.setBrand(brand);
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData = new TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData>();
        final List<FacetData<EndecaSearchStateData>> facetData = new ArrayList<>();
        given(endecaToFacetsConverter.convertToFacetData(searchResults, endecaSearchStateData, categoryCode, brand))
                .willReturn(facetData);
        searchResults.setRecords(null);
        final String autoCorrectedTerm = "testAutoCorrectedTerm";
        searchResults.setAutoCorrectedTerm(autoCorrectedTerm);
        targetEndecaSearchHelper.populateSearchPageData(searchPageData, searchResults, endecaSearchStateData);
        assertThat(searchPageData.getErrorStatus()).as("error status").isEqualTo(null);
        assertThat(searchPageData.getKeywordRedirectUrl()).as("redirect link").isEqualTo(null);
        assertThat(searchPageData.getFacets()).as("facets").isEqualTo(facetData);
        assertThat(searchPageData.getAutoCorrectedTerm()).as("Auto corrected Term").isEqualTo(autoCorrectedTerm);
        assertThat(searchPageData.getResults()).as("results").isEmpty();
    }

    @Test
    public void testPopulateSearchPageData() {
        final EndecaSearch searchResults = new EndecaSearch();
        searchResults.setRedirectLink(null);
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        final TargetCategoryData categoryData = new TargetCategoryData();
        final String categoryCode = "testcode";
        categoryData.setCode(categoryCode);
        endecaSearchStateData.setCategoryData(categoryData);
        final BrandData brand = new BrandData();
        endecaSearchStateData.setBrand(brand);
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData = new TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData>();
        final List<FacetData<EndecaSearchStateData>> facetData = new ArrayList<>();
        given(endecaToFacetsConverter.convertToFacetData(searchResults, endecaSearchStateData, categoryCode, brand))
                .willReturn(facetData);
        final EndecaRecords records = new EndecaRecords();
        searchResults.setRecords(records);
        final EndecaProduct endecaProduct1 = new EndecaProduct();
        final EndecaProduct endecaProduct2 = new EndecaProduct();
        final List<EndecaProduct> productsList = new ArrayList<EndecaProduct>(Arrays.asList(endecaProduct1,
                endecaProduct2));
        records.setProduct(productsList);
        final TargetProductListerData targetProduct1 = new TargetProductListerData();
        final TargetProductListerData targetProduct2 = new TargetProductListerData();
        given(targetProductListerConverter.convert(endecaProduct1)).willReturn(targetProduct1);
        given(targetProductListerConverter.convert(endecaProduct2)).willReturn(targetProduct2);
        final String currentNavState = "testNavState";
        records.setCurrentNavState(currentNavState);
        final String requestUrl = "testRequestUrl";
        endecaSearchStateData.setRequestUrl(requestUrl);
        final String url = "testUrl";
        given(targetEndecaURLHelper.constructCurrentPageUrl(requestUrl, currentNavState)).willReturn(url);
        final String autoCorrectedTerm = "testAutoCorrectedTerm";
        searchResults.setAutoCorrectedTerm(autoCorrectedTerm);
        final String totalCount = "40";
        records.setTotalCount(totalCount);
        final int itemsPerPage = 20;
        final int pageNumber = 1;
        endecaSearchStateData.setPageNumber(pageNumber);
        endecaSearchStateData.setItemsPerPage(itemsPerPage);
        final List<EndecaOptions> options = new ArrayList<>();
        records.setOption(options);
        final List<TargetSortData> sortData = new ArrayList<>();
        given(
                endecaToSortConverter.covertToTargetSortData(searchResults.getRecords().getOption(),
                        endecaSearchStateData))
                                .willReturn(sortData);
        final String spellingSuggestion = "testSuggestion";
        final List<String> spellingSuggestions = new ArrayList<>(Arrays.asList(spellingSuggestion));
        searchResults.setSpellingSuggestions(spellingSuggestions);

        targetEndecaSearchHelper.populateSearchPageData(searchPageData, searchResults, endecaSearchStateData);
        assertThat(searchPageData.getErrorStatus()).as("error status").isEqualTo(null);
        assertThat(searchPageData.getKeywordRedirectUrl()).as("redirect link").isEqualTo(null);
        assertThat(searchPageData.getFacets()).as("facets").isEqualTo(facetData);
        assertThat(searchPageData.getAutoCorrectedTerm()).as("Auto corrected Term").isEqualTo(autoCorrectedTerm);
        assertThat(searchPageData.getResults()).as("results").contains(targetProduct1, targetProduct2);
        assertThat(endecaSearchStateData.getUrl()).as("url").isEqualTo(url);
        assertThat(searchPageData.getPagination().getCurrentPage()).as("current page ").isEqualTo(1);
        assertThat(searchPageData.getPagination().getNumberOfPages()).as("current page number").isEqualTo(2);
        assertThat(searchPageData.getPagination().getPageSize()).as("current page size").isEqualTo(20);
        assertThat(searchPageData.getPagination().getTotalNumberOfResults()).as("total number of results")
                .isEqualTo(40);
        assertThat(searchPageData.getTargetSortData()).as("sort data").isEqualTo(sortData);
        assertThat(searchPageData.getSpellingSuggestions()).as("spelling suggestions").contains(spellingSuggestion);
    }

    @Test
    public void testClearAll() {
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData = mock(
                TargetProductCategorySearchPageData.class);
        final EndecaSearchStateData searchStateData = mock(EndecaSearchStateData.class);
        given(searchPageData.getCurrentQuery()).willReturn(searchStateData);
        final PaginationData paginationData = mock(PaginationData.class);
        given(Integer.valueOf(paginationData.getPageSize())).willReturn(Integer.valueOf(0));
        given(searchPageData.getPagination()).willReturn(paginationData);
        final EndecaSearch searchResults = mock(EndecaSearch.class);
        final List<Breadcrumb> breadcrumbs = new ArrayList<>();
        final Breadcrumb breadcrumb = mock(Breadcrumb.class);
        given(breadcrumb.getUrl()).willReturn("clearAllUrl");
        breadcrumbs.add(breadcrumb);
        final List<EndecaDimension> dimensions = new ArrayList<>();
        final EndecaDimension dimension = mock(EndecaDimension.class);
        given(Boolean.valueOf(dimension.isMultiSelect())).willReturn(Boolean.TRUE);
        given(Boolean.valueOf(dimension.isSelected())).willReturn(Boolean.TRUE);
        dimensions.add(dimension);
        given(searchResults.getDimension()).willReturn(dimensions);
        given(targetEndecaSearchHelper.getClearAllQueryUrl("clearAllUrl")).willReturn("clearAllQueryUrl");
        targetEndecaSearchHelper.populateClearAllUrl(searchPageData, searchResults, breadcrumbs);
        verify(searchPageData).setShowClearAllUrl(true);
        verify(searchPageData).setClearAllUrl("clearAllUrl?Nrpp=0");
        verify(searchPageData).setClearAllQueryUrl("clearAllQueryUrl?Nrpp=0");
    }

    @Test
    public void testClearAllNotSelected() {
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData = mock(
                TargetProductCategorySearchPageData.class);
        final EndecaSearchStateData searchStateData = mock(EndecaSearchStateData.class);
        given(searchPageData.getCurrentQuery()).willReturn(searchStateData);
        final PaginationData paginationData = mock(PaginationData.class);
        given(Integer.valueOf(paginationData.getPageSize())).willReturn(Integer.valueOf(0));
        given(searchPageData.getPagination()).willReturn(paginationData);
        final EndecaSearch searchResults = mock(EndecaSearch.class);
        final List<Breadcrumb> breadcrumbs = new ArrayList<>();
        final Breadcrumb breadcrumb = mock(Breadcrumb.class);
        given(breadcrumb.getUrl()).willReturn("clearAllUrl");
        breadcrumbs.add(breadcrumb);
        final List<EndecaDimension> dimensions = new ArrayList<>();
        final EndecaDimension dimension = mock(EndecaDimension.class);
        given(Boolean.valueOf(dimension.isMultiSelect())).willReturn(Boolean.TRUE);
        dimensions.add(dimension);
        given(searchResults.getDimension()).willReturn(dimensions);
        given(targetEndecaSearchHelper.getClearAllQueryUrl("clearAllUrl")).willReturn("clearAllQueryUrl");
        targetEndecaSearchHelper.populateClearAllUrl(searchPageData, searchResults, breadcrumbs);
        verify(searchPageData).setShowClearAllUrl(false);
        verify(searchPageData).setClearAllUrl("clearAllUrl?Nrpp=0");
        verify(searchPageData).setClearAllQueryUrl("clearAllQueryUrl?Nrpp=0");
    }

    @Test
    public void testClearAllNoDimensions() {
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData = mock(
                TargetProductCategorySearchPageData.class);
        final EndecaSearchStateData searchStateData = mock(EndecaSearchStateData.class);
        given(searchPageData.getCurrentQuery()).willReturn(searchStateData);
        final PaginationData paginationData = mock(PaginationData.class);
        given(Integer.valueOf(paginationData.getPageSize())).willReturn(Integer.valueOf(0));
        given(searchPageData.getPagination()).willReturn(paginationData);
        final EndecaSearch searchResults = mock(EndecaSearch.class);
        final List<Breadcrumb> breadcrumbs = new ArrayList<>();
        final Breadcrumb breadcrumb = mock(Breadcrumb.class);
        given(breadcrumb.getUrl()).willReturn("clearAllUrl");
        breadcrumbs.add(breadcrumb);
        given(searchResults.getDimension()).willReturn(null);
        given(targetEndecaSearchHelper.getClearAllQueryUrl("clearAllUrl")).willReturn("clearAllQueryUrl");
        targetEndecaSearchHelper.populateClearAllUrl(searchPageData, searchResults, breadcrumbs);
        verify(searchPageData).setShowClearAllUrl(false);
        verify(searchPageData).setClearAllUrl("clearAllUrl?Nrpp=0");
        verify(searchPageData).setClearAllQueryUrl("clearAllQueryUrl?Nrpp=0");
    }

    @Test
    public void testClearAllOnBrandPage() {
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData = mock(
                TargetProductCategorySearchPageData.class);
        final BrandData brandData = mock(BrandData.class);
        final EndecaSearchStateData searchStateData = mock(EndecaSearchStateData.class);
        given(searchPageData.getCurrentQuery()).willReturn(searchStateData);
        final PaginationData paginationData = mock(PaginationData.class);
        given(Integer.valueOf(paginationData.getPageSize())).willReturn(Integer.valueOf(0));
        given(searchPageData.getPagination()).willReturn(paginationData);
        given(searchStateData.getBrand()).willReturn(brandData);
        final EndecaSearch searchResults = mock(EndecaSearch.class);
        final List<Breadcrumb> breadcrumbs = new ArrayList<>();
        final Breadcrumb breadcrumb = mock(Breadcrumb.class);
        given(breadcrumb.getUrl()).willReturn("clearAllUrl");
        breadcrumbs.add(breadcrumb);
        final List<EndecaDimension> dimensions = new ArrayList<>();
        final EndecaDimension dimension = mock(EndecaDimension.class);
        given(dimension.getDimensionName()).willReturn("brand");
        given(Boolean.valueOf(dimension.isMultiSelect())).willReturn(Boolean.TRUE);
        given(Boolean.valueOf(dimension.isSelected())).willReturn(Boolean.TRUE);
        dimensions.add(dimension);
        given(searchResults.getDimension()).willReturn(dimensions);
        given(targetEndecaSearchHelper.getClearAllQueryUrl("clearAllUrl")).willReturn("clearAllQueryUrl");
        targetEndecaSearchHelper.populateClearAllUrl(searchPageData, searchResults, breadcrumbs);
        verify(searchPageData).setShowClearAllUrl(false);
        verify(searchPageData).setClearAllUrl("clearAllUrl?Nrpp=0");
        verify(searchPageData).setClearAllQueryUrl("clearAllQueryUrl?Nrpp=0");
    }

    @Test
    public void testGetClearAllQueryUrlForSearch() {
        final String clearAllQueryUrl = targetEndecaSearchHelper.getClearAllQueryUrl("/search?text=baby");
        assertThat(clearAllQueryUrl).isEqualTo("?text=baby");
    }

    @Test
    public void testGetClearAllQueryUrlForCategory() {
        final String clearAllQueryUrl = targetEndecaSearchHelper.getClearAllQueryUrl("/c/baby/W93741");
        assertThat(clearAllQueryUrl).isEqualTo("?category=W93741");
    }

    @Test
    public void testGetClearAllQueryUrlForBrand() {
        final String clearAllQueryUrl = targetEndecaSearchHelper.getClearAllQueryUrl("/b/bonds");
        assertThat(clearAllQueryUrl).isEqualTo("?brand=bonds");
    }

    @Test
    public void testGetClearAllQueryUrlForBrandWithAdditionalParams() {
        final String clearAllQueryUrl = targetEndecaSearchHelper.getClearAllQueryUrl("/b/bonds?N=1");
        assertThat(clearAllQueryUrl).isEqualTo("?brand=bonds&N=1");
    }

    @Test
    public void testAppendPaginationDataWithoutQueryParams() {
        final PaginationData paginationData = mock(PaginationData.class);
        given(Integer.valueOf(paginationData.getPageSize())).willReturn(Integer.valueOf(0));
        final String url = targetEndecaSearchHelper.appendPaginationData("/c/baby/W93741", paginationData);
        assertThat(url).isEqualTo("/c/baby/W93741?Nrpp=0");
    }

    @Test
    public void testAppendPaginationDataWithQueryParams() {
        final PaginationData paginationData = mock(PaginationData.class);
        given(Integer.valueOf(paginationData.getPageSize())).willReturn(Integer.valueOf(0));
        final String url = targetEndecaSearchHelper.appendPaginationData("/search?text=baby", paginationData);
        assertThat(url).isEqualTo("/search?text=baby&Nrpp=0");
    }

    @Test
    public void testAppendPaginationDataWithNull() {
        final String url = targetEndecaSearchHelper.appendPaginationData("/search?text=baby", null);
        assertThat(url).isEqualTo("/search?text=baby");
    }

}
