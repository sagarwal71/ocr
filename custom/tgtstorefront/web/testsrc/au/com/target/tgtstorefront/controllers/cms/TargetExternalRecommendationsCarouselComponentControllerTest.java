package au.com.target.tgtstorefront.controllers.cms;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.controllers.pages.AbstractPageController;
import au.com.target.tgtwebcore.model.cms2.components.TargetExternalRecommendationsCarouselComponentModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetExternalRecommendationsCarouselComponentControllerTest {

    /**
     * 
     */
    private static final String DEFAULT_URL = "https://123456.recs.igodigital.com/a/v2/123456/home/recommend.json";

    @Mock
    private HttpServletRequest request;

    @Mock
    private TargetExternalRecommendationsCarouselComponentModel component;

    private Model model;

    private final TargetExternalRecommendationsCarouselComponentController controller = new TargetExternalRecommendationsCarouselComponentController();

    @Before
    public void setUp() {
        ReflectionTestUtils.setField(controller, "salesforceRecommendationUrl",
                "https://{0}.recs.igodigital.com/a/v2/{0}/{1}/recommend.json{2}");
        ReflectionTestUtils.setField(controller, "clientId", "123456");
        model = new ExtendedModelMap();
    }

    @Test
    public void testFillModelNoPageType() {
        controller.fillModel(request, model, component);
        assertThat(model.containsAttribute("requestURL")).isTrue();
        final Map<String, Object> modelMap = model.asMap();
        assertThat(modelMap.get("requestURL"))
                .isEqualTo(DEFAULT_URL);
    }

    @Test
    public void testFillModelHomePage() {
        given(request.getAttribute("pageType")).willReturn(AbstractPageController.PageType.Home);
        controller.fillModel(request, model, component);
        assertThat(model.containsAttribute("requestURL")).isTrue();
        assertThat(model.asMap().get("requestURL"))
                .isEqualTo(DEFAULT_URL);
    }

    @Test
    public void testFillModelProductPageWithNull() {
        given(request.getAttribute("pageType")).willReturn(AbstractPageController.PageType.Product);
        controller.fillModel(request, model, component);
        assertThat(model.containsAttribute("requestURL")).isTrue();
        assertThat(model.asMap().get("requestURL"))
                .isEqualTo("https://123456.recs.igodigital.com/a/v2/123456/product/recommend.json?item=");
    }

    @Test
    public void testFillModelProductPage() {
        given(request.getAttribute("pageType")).willReturn(AbstractPageController.PageType.Product);
        final TargetProductData productData = mock(TargetProductData.class);
        given(request.getAttribute("product")).willReturn(productData);
        given(productData.getCode()).willReturn("1234");
        controller.fillModel(request, model, component);
        assertThat(model.containsAttribute("requestURL")).isTrue();
        assertThat(model.asMap().get("requestURL"))
                .isEqualTo("https://123456.recs.igodigital.com/a/v2/123456/product/recommend.json?item=1234");
    }

    @Test
    public void testFillModelCartPageWithEmptyCart() {
        given(request.getAttribute("pageType")).willReturn(AbstractPageController.PageType.Cart);
        controller.fillModel(request, model, component);
        assertThat(model.containsAttribute("requestURL")).isTrue();
        assertThat(model.asMap().get("requestURL"))
                .isEqualTo("https://123456.recs.igodigital.com/a/v2/123456/cart/recommend.json?cart=");
    }

    @Test
    public void testFillModelCartPage() {
        final TargetCartData cartData = mock(TargetCartData.class);
        final TargetProductData productData = mock(TargetProductData.class);
        final List<OrderEntryData> orderData = new ArrayList<>();
        final OrderEntryData data1 = mock(OrderEntryData.class);
        orderData.add(data1);

        given(request.getAttribute("pageType")).willReturn(AbstractPageController.PageType.Cart);
        given(request.getAttribute("cartData")).willReturn(cartData);
        given(data1.getProduct()).willReturn(productData);
        given(productData.getCode()).willReturn("1234");
        given(cartData.getEntries()).willReturn(orderData);
        controller.fillModel(request, model, component);
        assertThat(model.containsAttribute("requestURL")).isTrue();
        assertThat(model.asMap().get("requestURL"))
                .isEqualTo("https://123456.recs.igodigital.com/a/v2/123456/cart/recommend.json?cart=1234");
    }

    @Test
    public void testFillModelCartPageWithMultipleProducts() {

        final TargetCartData cartData = mock(TargetCartData.class);
        final List<OrderEntryData> orderData = new ArrayList<>();
        final OrderEntryData data1 = mock(OrderEntryData.class);
        final OrderEntryData data2 = mock(OrderEntryData.class);
        final TargetProductData productData1 = mock(TargetProductData.class);
        final TargetProductData productData2 = mock(TargetProductData.class);

        orderData.add(data1);
        orderData.add(data2);

        given(request.getAttribute("pageType")).willReturn(AbstractPageController.PageType.Cart);
        given(request.getAttribute("cartData")).willReturn(cartData);
        given(data1.getProduct()).willReturn(productData1);
        given(data2.getProduct()).willReturn(productData2);
        given(productData1.getCode()).willReturn("1234");
        given(productData2.getCode()).willReturn("5678");
        given(cartData.getEntries()).willReturn(orderData);
        controller.fillModel(request, model, component);
        assertThat(model.containsAttribute("requestURL")).isTrue();
        assertThat(model.asMap().get("requestURL"))
                .isEqualTo("https://123456.recs.igodigital.com/a/v2/123456/cart/recommend.json?cart=1234|5678");
    }

    @Test
    public void testFillModelFavouritePage() {
        given(request.getAttribute("pageType")).willReturn(AbstractPageController.PageType.Favourite);
        controller.fillModel(request, model, component);
        assertThat(model.containsAttribute("requestURL")).isTrue();
        assertThat(model.asMap().get("requestURL"))
                .isEqualTo(
                        "https://123456.recs.igodigital.com/a/v2/123456/category/recommend.json?wishlist={wishlist}");
    }

    @Test
    public void testFillModelCategoryPageWithNoCategory() {
        given(request.getAttribute("pageType")).willReturn(AbstractPageController.PageType.Category);
        controller.fillModel(request, model, component);
        assertThat(model.containsAttribute("requestURL")).isTrue();
        assertThat(model.asMap().get("requestURL"))
                .isEqualTo("https://123456.recs.igodigital.com/a/v2/123456/category/recommend.json?category=");
    }

    @Test
    public void testFillModelCategoryPage() {
        final List<Breadcrumb> breadcrumbs = new ArrayList<>();
        final Breadcrumb breadcrumb1 = mock(Breadcrumb.class);
        breadcrumbs.add(breadcrumb1);

        given(request.getAttribute("pageType")).willReturn(AbstractPageController.PageType.Category);
        given(request.getAttribute("breadcrumbs")).willReturn(breadcrumbs);
        given(breadcrumb1.getName()).willReturn("kids");
        controller.fillModel(request, model, component);
        assertThat(model.containsAttribute("requestURL")).isTrue();
        assertThat(model.asMap().get("requestURL"))
                .isEqualTo("https://123456.recs.igodigital.com/a/v2/123456/category/recommend.json?category=kids");
    }

    @Test
    public void testFillModelCategoryPageSubCategory() {
        final List<Breadcrumb> breadcrumbs = new ArrayList<>();
        final Breadcrumb breadcrumb1 = mock(Breadcrumb.class);
        final Breadcrumb breadcrumb2 = mock(Breadcrumb.class);
        breadcrumbs.add(breadcrumb1);
        breadcrumbs.add(breadcrumb2);

        given(request.getAttribute("pageType")).willReturn(AbstractPageController.PageType.Category);
        given(request.getAttribute("breadcrumbs")).willReturn(breadcrumbs);
        given(breadcrumb1.getName()).willReturn("kids");
        given(breadcrumb2.getName()).willReturn("boy 1-3");
        controller.fillModel(request, model, component);
        assertThat(model.containsAttribute("requestURL")).isTrue();
        assertThat(model.asMap().get("requestURL"))
                .isEqualTo("https://123456.recs.igodigital.com/a/v2/123456/category/recommend.json?category=kids");
    }
}
