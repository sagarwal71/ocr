/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.account;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtfacades.order.TargetOrderFacade;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtstorefront.breadcrumb.ResourceBreadcrumbBuilder;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.url.TargetUrlsMapping;
import au.com.target.tgtstorefront.controllers.util.AddressDataHelper;
import au.com.target.tgtstorefront.forms.AddressForm;
import au.com.target.tgtstorefront.forms.validation.validator.AddressFormValidator;
import au.com.target.tgtstorefront.util.PageTitleResolver;
import au.com.target.tgtverifyaddr.data.FormattedAddressData;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AccountAddressDetailsControllerTest {

    @InjectMocks
    private final AccountAddressDetailsController controller = new AccountAddressDetailsController();

    @Mock
    private UserFacade userFacade;

    @Mock
    private CustomerFacade customerFacade;

    @Mock
    private TargetOrderFacade targetOrderFacade;

    @Mock
    private CheckoutFacade checkoutFacade;

    @Mock
    private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

    @Mock
    private CommonI18NService commonI18NService;

    @Mock
    private I18NService i18NService;

    @Mock
    private PriceDataFactory priceDataFactory;

    @Mock
    private CurrencyModel currencyModel;

    @Mock
    private MessageSource messageSource;

    @Mock
    private CMSPageService cmsPageService;

    @Mock
    private PageTitleResolver pageTitleResolver;

    @Mock
    private ContentPageModel contentPageModel;

    @Mock
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Mock
    private Model mockedModel;

    @Mock
    private RedirectAttributes redirectModel;

    @Mock
    private RedirectAttributes redirectAttributes;

    @Mock
    private TargetOrderData targetOrderData;

    @Mock
    private PriceData priceData;

    @Mock
    private PriceData priceDataOutstandingAmount;

    @Mock
    private AddressDataHelper addressDataHelper;

    @Mock
    private TargetUrlsMapping targetUrlsMapping;

    @Mock
    private AddressFormValidator addressFormValidator;

    @Mock
    private TargetUserFacade targetUserFacade;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpSession session;

    @Mock
    private FormattedAddressData formattedAddressData;

    @Before
    public void setUp() throws CMSItemNotFoundException {
        MockitoAnnotations.initMocks(this);
        BDDMockito.when(cmsPageService.getPageForLabelOrId(BDDMockito.anyString())).thenReturn(
                contentPageModel);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(AddressDataHelper.SINGLE_LINE_ADDRESS_FORMAT)).thenReturn(formattedAddressData);
    }

    @Test
    public void itShouldCheckSingleLineAddressLookupFlagWhenAddDeliveryAddress() throws CMSItemNotFoundException {
        final Model model = mock(Model.class);
        controller.addAddress(model, redirectAttributes);

        verify(addressDataHelper).checkIfSingleLineAddressFeatureIsEnabled(model);
    }

    @Test
    public void itShouldCheckSingleLineAddressLookupFlagWhenEditDeliveryAddress() throws CMSItemNotFoundException {
        final String addressCode = "addreess1";
        final Model model = mock(Model.class);
        final TargetAddressData address = mock(TargetAddressData.class);
        when(addressDataHelper.getAddressDataFromAddressBookById(anyString())).thenReturn(address);

        controller.editAddress(addressCode, model, redirectAttributes);

        verify(addressDataHelper).checkIfSingleLineAddressFeatureIsEnabled(model);
    }

    @SuppressWarnings("boxing")
    @Test
    public void itShouldSkipConfirmationDuringEditAddressWhenAddressLookupIsEnabled() throws CMSItemNotFoundException {
        final Model model = mock(Model.class);
        final AddressForm form = new AddressForm();
        final BindingResult bindingResult = mock(BindingResult.class);
        when(addressDataHelper.isSingleLineLookupFeatureEnabled()).thenReturn(true);

        final String viewName = controller.editAddress(form, bindingResult, model, redirectAttributes, request);

        verify(addressDataHelper, times(0)).processAddressAndVerifyWithQas(any(Model.class), any(AddressForm.class),
                any(TargetAddressData.class), anyBoolean());
        verify(addressDataHelper).isAddressFormVerifiedInSession(form, session);
        assertEquals(ControllerConstants.Redirection.MY_ACCOUNT_ADDRESS_DETAILS_PAGE, viewName);
    }

    @SuppressWarnings("boxing")
    @Test
    public void itShouldPromptConfirmationDuringEditAddressWhenAddressLookupIsDisabled()
            throws CMSItemNotFoundException {
        final Model model = mock(Model.class);
        final AddressForm form = new AddressForm();
        final BindingResult bindingResult = mock(BindingResult.class);
        when(addressDataHelper.isSingleLineLookupFeatureEnabled()).thenReturn(false);

        final String viewName = controller.editAddress(form, bindingResult, model, redirectAttributes, request);

        verify(addressDataHelper).processAddressAndVerifyWithQas(any(Model.class), any(AddressForm.class),
                any(TargetAddressData.class), anyBoolean());
        verify(addressDataHelper, times(0)).isAddressFormVerified(form, formattedAddressData);
        assertEquals(ControllerConstants.Views.Pages.Account.ACCOUNT_CONFIRM_ADDRESS_PAGE, viewName);
    }

    @SuppressWarnings("boxing")
    @Test
    public void itShouldSkipConfirmationDuringAddAddressWhenAddressLookupIsEnabled() throws CMSItemNotFoundException {
        final Model model = mock(Model.class);
        final AddressForm form = new AddressForm();
        final BindingResult bindingResult = mock(BindingResult.class);
        when(addressDataHelper.isSingleLineLookupFeatureEnabled()).thenReturn(true);

        final String viewName = controller.addAddress(form, bindingResult, model, redirectAttributes, request);

        verify(addressDataHelper, times(0)).processAddressAndVerifyWithQas(any(Model.class), any(AddressForm.class),
                any(TargetAddressData.class), anyBoolean());
        verify(addressDataHelper).isAddressFormVerifiedInSession(form, session);
        assertEquals(ControllerConstants.Redirection.MY_ACCOUNT_ADDRESS_DETAILS_PAGE, viewName);
    }

    @SuppressWarnings("boxing")
    @Test
    public void itShouldPromptConfirmationDuringAddAddressWhenAddressLookupIsDisabled() throws CMSItemNotFoundException {
        final Model model = mock(Model.class);
        final AddressForm form = new AddressForm();
        final BindingResult bindingResult = mock(BindingResult.class);
        when(addressDataHelper.isSingleLineLookupFeatureEnabled()).thenReturn(false);

        final String viewName = controller.addAddress(form, bindingResult, model, redirectAttributes, request);

        verify(addressDataHelper).processAddressAndVerifyWithQas(any(Model.class), any(AddressForm.class),
                any(TargetAddressData.class), anyBoolean());
        verify(addressDataHelper, times(0)).isAddressFormVerified(form, formattedAddressData);
        assertEquals(ControllerConstants.Views.Pages.Account.ACCOUNT_CONFIRM_ADDRESS_PAGE, viewName);
    }
}
