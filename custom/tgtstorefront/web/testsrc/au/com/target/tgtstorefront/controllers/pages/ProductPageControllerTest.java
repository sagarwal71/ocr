/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.pages.ProductPageModel;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.BaseOptionData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.Model;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.util.CookieGenerator;

import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.google.gson.Gson;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.querybuilder.TargetProductDetailPageEndecaQueryBuilder;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.product.data.ProductDisplayType;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtfacades.config.TargetSharedConfigFacade;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.look.TargetLookPageFacade;
import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtfacades.payment.AfterpayConfigFacade;
import au.com.target.tgtfacades.payment.data.AfterpayConfigData;
import au.com.target.tgtfacades.payment.environment.data.EnvironmentData;
import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.product.TargetProductListerFacade;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.product.impl.TargetProductListerFacadeImpl;
import au.com.target.tgtfacades.response.data.ProductStockSearchResultResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.stock.TargetStockLookUpFacade;
import au.com.target.tgtfacades.storefinder.TargetStoreFinderStockFacade;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtfacades.util.TargetProductDataHelper;
import au.com.target.tgtstorefront.breadcrumb.impl.ProductBreadcrumbBuilder;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.data.SchemaData;
import au.com.target.tgtstorefront.controllers.util.EndecaQueryResultsHelper;
import au.com.target.tgtstorefront.forms.AddToCartForm;
import au.com.target.tgtstorefront.util.SearchQuerySanitiser;
import au.com.target.tgtstorefront.variants.VariantSortStrategy;
import au.com.target.tgtstorefront.variants.impl.TargetCustomVariantSortStrategy;
import au.com.target.tgtutility.util.JsonConversionUtil;
import au.com.target.tgtwebcore.cache.service.AkamaiCacheConfigurationService;
import au.com.target.tgtwebcore.constants.TgtwebcoreConstants;
import au.com.target.tgtwebcore.servicelayer.services.TargetCMSPageService;
import au.com.target.tgtwebcore.userreviews.TargetUserReviewService;


/**
 * @author Nandini
 *
 */
@SuppressWarnings("deprecation")
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductPageControllerTest {

    private static final String PRODUCT_LOOKS = "tgtstorefront.product.looks.maximium.size";

    private static final String MIN_LEVEL_INTEREST = "urgencyToPurchase.minLevelOfInterest";

    private static final String MAX_LEVEL_INTEREST = "urgencyToPurchase.maxLevelOfInterest";

    private static final String MIN_PRICE_THRESHOLD = "urgencyToPurchase.minPriceThreshold";

    private static final String EXCLUDE_CATEGORY = "urgencyToPurchase.excludedCategory";

    private static final String PRODUCT_CODE = "P4023";

    private static final int PRODUCT_LOOKS_MAX = 4;

    private static final String UI_REACT_PDP_CAROUSEL = "ui.react.pdp.carousel";

    @InjectMocks
    @Spy
    private final ProductPageController productPageController = new ProductPageController();

    @Mock
    private Model mockModel;

    @Mock
    private TargetProductModel productModel;

    @Mock
    private TargetColourVariantProductModel colourVariant;

    @Mock
    private TargetSizeVariantProductModel sizeVariant;

    @Mock
    private TargetLookPageFacade targetLookPageFacade;

    @Mock
    private LookDetailsData lookDetails1, lookDetails2, lookDetails3, lookDetails4, lookDetails5;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private Configuration config;

    @Mock
    private SiteConfigService siteConfigService;

    @Mock
    private TargetFeatureSwitchFacade mockTargetFeatureSwitchFacade;

    @Mock
    private SalesApplicationFacade mockSalesApplicationFacade;

    @Mock
    private TargetCMSPageService mockCmsPageService;

    @Mock
    private VariantSortStrategy variantSortStrategy;

    @Mock
    private TargetCustomVariantSortStrategy targetCustomVariantSortStrategy;

    @Mock
    private TargetSharedConfigFacade sharedConfigFacade;

    private List<LookDetailsData> lookDetailslist = new ArrayList<>();

    @Mock
    private TargetProductData targetProductData;

    @Mock
    private EndecaSearchStateData endecaData;

    @Mock
    private ENEQueryResults eneQueryResults;

    @Mock
    private TargetProductListerData listerData;

    @Mock
    private TargetProductDetailPageEndecaQueryBuilder endecaProductQueryBuilder;

    @Mock
    private EndecaQueryResultsHelper queryResultsHelper;

    private final List<TargetProductListerData> productDataList = new ArrayList<>();

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse httpresponse;

    @Mock
    private ProductService productService;

    @Mock
    private TargetProductFacade productFacade;

    @Mock
    private ProductBreadcrumbBuilder productBreadcrumbBuilder;

    @Mock
    private TargetStoreFinderStockFacade targetStoreFinderStockFacade;

    @Mock
    private TargetStoreLocatorFacade storeLocatorFacade;

    @Mock
    private CookieGenerator cookieGenerator;

    @Mock
    private UrlResolver<ProductModel> productModelUrlResolver;

    @Mock
    private TargetUserReviewService targetBazaarVoiceService;

    @Mock
    private AkamaiCacheConfigurationService akamaiCacheConfigurationService;

    @Mock
    private SearchQuerySanitiser searchQuerySanitiser;

    @Mock
    private Response response;

    @Mock
    private PageableData pageableData;

    @Mock
    private AfterpayConfigFacade afterpayConfigFacade;

    @Mock
    private TargetProductListerFacade targetProductListerFacade;

    @Mock
    private TargetProductListerFacadeImpl targetProductListerFacadeImpl;

    @Mock
    private TargetSalesApplicationService targetSalesApplicationService;

    @Mock
    private TargetProductListerData mockTargetProductListerData;

    @Mock
    private TargetStockLookUpFacade targetStockLookupFacade;

    @Mock
    private PriceDataFactory priceDataFactory;


    private final ArgumentCaptor<String> environmentDataCaptor = ArgumentCaptor
            .forClass(String.class);

    private final List<LookDetailsData> lookDetailslistObject = new ArrayList<>();

    @Before
    public void setUp() throws TargetEndecaException, ENEQueryException {
        lookDetailslist = Arrays.asList(
                lookDetails1, lookDetails2);
        given(colourVariant.getBaseProduct()).willReturn(productModel);
        given(configurationService.getConfiguration()).willReturn(config);
        given(Integer.valueOf(config.getInt(PRODUCT_LOOKS))).willReturn(Integer.valueOf(PRODUCT_LOOKS_MAX));
        given(targetLookPageFacade.getActiveLooksForProduct(productModel, PRODUCT_LOOKS_MAX))
                .willReturn(lookDetailslist);
        given(targetProductData.getBaseProductCode()).willReturn("P4023");
        given(sharedConfigFacade.getConfigByCode(MIN_PRICE_THRESHOLD)).willReturn("10.00");
        given(sharedConfigFacade.getConfigByCode(MIN_LEVEL_INTEREST)).willReturn("2");
        given(sharedConfigFacade.getConfigByCode(MAX_LEVEL_INTEREST)).willReturn("200");
        given(listerData.getBaseProductCode()).willReturn("P1000");
        given(
                Integer.valueOf(configurationService.getConfiguration().getInt(
                        "tgtstorefront.urgencytopurchase.productlist.maxSize"))).willReturn(Integer.valueOf(1));
        productDataList.add(listerData);
        given(endecaProductQueryBuilder.getQueryResults(
                endecaData, 1)).willReturn(eneQueryResults);
        given(queryResultsHelper
                .getTargetProductList(eneQueryResults)).willReturn(productDataList);
        given(productService.getProductForCode(any(String.class))).willReturn(productModel);
        given(productFacade.getBulkyBoardProductForOptions(any(ProductModel.class), any(List.class), any(String.class)))
                .willReturn(targetProductData);
        ReflectionTestUtils.setField(productPageController, "maxProductSizeEndeca", Integer.valueOf(1));
        willReturn(pageableData).given(productPageController).createPageableData();
        given(productPageController.getZipPaymentThresholdAmount()).willReturn(new PriceData());
    }

    @Test
    public void testPopulateAvailableLooksForBaseProduct() {
        productPageController.populateAvailableLooks(mockModel, productModel);
        verify(mockModel, times(1)).addAttribute("lookDetailsData", lookDetailslist);
    }

    @Test
    public void testPopulateAvailableLooksNullProduct() {
        productPageController.populateAvailableLooks(mockModel, null);
        verify(mockModel, times(0)).addAttribute("lookDetailsData", lookDetailslist);
    }

    @Test
    public void testPopulateAvailableLooksWithEmptyLooks() {
        final List<LookDetailsData> lookDetails = new ArrayList<>();
        given(targetLookPageFacade.getActiveLooksForProduct(productModel, PRODUCT_LOOKS_MAX)).willReturn(lookDetails);
        productPageController.populateAvailableLooks(mockModel, colourVariant);
        assertThat(lookDetails).hasSize(0);
        verify(mockModel, times(1)).addAttribute("lookDetailsData", lookDetails);
    }

    @Test
    public void testPopulateAvailableLooksForColourCariant() {
        productPageController.populateAvailableLooks(mockModel, colourVariant);
        verify(mockModel, times(1)).addAttribute("lookDetailsData", lookDetailslist);
    }

    @Test
    public void testPopulateAvailableLooksForSizeVariant() {
        given(sizeVariant.getBaseProduct()).willReturn(colourVariant);
        given(colourVariant.getBaseProduct()).willReturn(productModel);
        productPageController.populateAvailableLooks(mockModel, sizeVariant);
        verify(mockModel, times(1)).addAttribute("lookDetailsData", lookDetailslist);
    }

    @Test
    public void testGetPreviewRedirectWithFeatureSwitchDisabled() {
        willReturn(Boolean.FALSE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.PRODUCT_PREVIEW_MODE);

        final ProductModel mockProduct = mock(ProductModel.class);

        final String result = productPageController.getPreviewRedirect(mockProduct, true);

        assertThat(result).isEmpty();
        verifyZeroInteractions(mockSalesApplicationFacade, mockCmsPageService);
    }

    @Test
    public void testGetPreviewRedirectWithBaseProductNotPreview() {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.PRODUCT_PREVIEW_MODE);

        final TargetProductModel mockProduct = mock(TargetProductModel.class);
        willReturn(Boolean.FALSE).given(mockProduct).getPreview();

        final String result = productPageController.getPreviewRedirect(mockProduct, true);

        assertThat(result).isEmpty();
        verifyZeroInteractions(mockSalesApplicationFacade, mockCmsPageService);
    }

    @Test
    public void testGetPreviewRedirectWithVariantProductNotPreview() {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.PRODUCT_PREVIEW_MODE);

        final AbstractTargetVariantProductModel mockProduct = mock(AbstractTargetVariantProductModel.class);
        willReturn(Boolean.FALSE).given(mockProduct).getPreview();

        final String result = productPageController.getPreviewRedirect(mockProduct, true);

        assertThat(result).isEmpty();
        verifyZeroInteractions(mockSalesApplicationFacade, mockCmsPageService);
    }

    @Test
    public void testGetPreviewRedirectWithMobileApp() {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.PRODUCT_PREVIEW_MODE);

        final AbstractTargetVariantProductModel mockProduct = mock(AbstractTargetVariantProductModel.class);
        willReturn(Boolean.TRUE).given(mockProduct).getPreview();

        willReturn(Boolean.TRUE).given(mockSalesApplicationFacade).isSalesChannelMobileApp();

        final String result = productPageController.getPreviewRedirect(mockProduct, true);

        assertThat(result).isEmpty();
        verifyZeroInteractions(mockCmsPageService);
    }

    @Test
    public void testGetPreviewRedirectWithNoContentPage() throws CMSItemNotFoundException {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.PRODUCT_PREVIEW_MODE);

        final AbstractTargetVariantProductModel mockProduct = mock(AbstractTargetVariantProductModel.class);
        willReturn(Boolean.TRUE).given(mockProduct).getPreview();

        willReturn(Boolean.FALSE).given(mockSalesApplicationFacade).isSalesChannelMobileApp();

        ReflectionTestUtils.setField(productPageController, "previewRedirectPageId", "mobileOnlyPreviewRedirect");
        given(mockCmsPageService.getPageForIdWithRestrictions("mobileOnlyPreviewRedirect")).willReturn(null);

        final String result = productPageController.getPreviewRedirect(mockProduct, true);

        assertThat(result).isEmpty();
    }

    @Test
    public void testGetPreviewRedirectWithRedirect() throws CMSItemNotFoundException {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.PRODUCT_PREVIEW_MODE);

        final AbstractTargetVariantProductModel mockProduct = mock(AbstractTargetVariantProductModel.class);
        willReturn(Boolean.TRUE).given(mockProduct).getPreview();

        willReturn(Boolean.FALSE).given(mockSalesApplicationFacade).isSalesChannelMobileApp();

        ReflectionTestUtils.setField(productPageController, "previewRedirectPageId", "mobileOnlyPreviewRedirect");

        final ContentPageModel mockContentPage = mock(ContentPageModel.class);
        given(mockContentPage.getLabel()).willReturn("/jeanpaulgaultier");
        given(mockCmsPageService.getPageForIdWithRestrictions("mobileOnlyPreviewRedirect")).willReturn(mockContentPage);

        final String result = productPageController.getPreviewRedirect(mockProduct, true);

        assertThat(result).isEqualTo(UrlBasedViewResolver.REDIRECT_URL_PREFIX + "/jeanpaulgaultier");
    }

    @Test(expected = UnknownIdentifierException.class)
    public void testGetPreviewRedirectWith404() throws CMSItemNotFoundException {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.PRODUCT_PREVIEW_MODE);

        final AbstractTargetVariantProductModel mockProduct = mock(AbstractTargetVariantProductModel.class);
        willReturn(Boolean.TRUE).given(mockProduct).getPreview();

        willReturn(Boolean.FALSE).given(mockSalesApplicationFacade).isSalesChannelMobileApp();

        ReflectionTestUtils.setField(productPageController, "previewRedirectPageId", "mobileOnlyPreviewRedirect");

        final ContentPageModel mockContentPage = mock(ContentPageModel.class);
        given(mockContentPage.getLabel()).willReturn("/jeanpaulgaultier");
        given(mockCmsPageService.getPageForIdWithRestrictions("mobileOnlyPreviewRedirect")).willReturn(mockContentPage);

        productPageController.getPreviewRedirect(mockProduct, false);
    }

    @Test
    public void testSortVariantOptionDataNoBaseOptionsOrVariantOptions() {
        final ProductData productData = new ProductData();
        productPageController.sortVariantOptionData(productData);

        verifyZeroInteractions(mockTargetFeatureSwitchFacade, variantSortStrategy,
                targetCustomVariantSortStrategy);
    }

    @Test
    public void testSortVariantOptionDataBaseOptionsWithNoOptionsNoVariantOptions() {
        final ProductData productData = new ProductData();
        productData.setBaseOptions(Collections.singletonList(new BaseOptionData()));
        productPageController.sortVariantOptionData(productData);

        verifyZeroInteractions(mockTargetFeatureSwitchFacade, variantSortStrategy,
                targetCustomVariantSortStrategy);
    }

    @Test
    public void testSortVariantOptionDataBaseOptionsFeatureSwitchOffNoVariantOptions() {
        final ProductData productData = new ProductData();
        final BaseOptionData baseOptionData = new BaseOptionData();

        baseOptionData.setOptions(Arrays.asList(new VariantOptionData()));
        productData.setBaseOptions(Collections.singletonList(baseOptionData));
        willReturn(Boolean.FALSE).given(mockTargetFeatureSwitchFacade).isSizeOrderingForUIEnabled();
        productPageController.sortVariantOptionData(productData);

        verify(mockTargetFeatureSwitchFacade).isSizeOrderingForUIEnabled();
        verifyNoMoreInteractions(mockTargetFeatureSwitchFacade);
        verifyZeroInteractions(variantSortStrategy,
                targetCustomVariantSortStrategy);
    }

    @Test
    public void testSortVariantOptionDataBaseOptionsFeatureSwitchOnNoVariantOptions() {
        final ProductData productData = new ProductData();
        final BaseOptionData baseOptionData = new BaseOptionData();

        baseOptionData.setOptions(Arrays.asList(new VariantOptionData()));
        productData.setBaseOptions(Collections.singletonList(baseOptionData));
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isSizeOrderingForUIEnabled();
        productPageController.sortVariantOptionData(productData);

        verify(mockTargetFeatureSwitchFacade).isSizeOrderingForUIEnabled();
        verifyNoMoreInteractions(mockTargetFeatureSwitchFacade);
        verifyZeroInteractions(variantSortStrategy,
                targetCustomVariantSortStrategy);
    }

    @Test
    public void testSortVariantOptionDataBaseOptionsFeatureSwitchOffWithVariantOptions() {
        final ProductData productData = new ProductData();
        final BaseOptionData baseOptionData = new BaseOptionData();
        baseOptionData.setOptions(Arrays.asList(new VariantOptionData()));
        productData.setBaseOptions(Collections.singletonList(baseOptionData));
        productData.setVariantOptions(Arrays.asList(new VariantOptionData()));
        willReturn(Boolean.FALSE).given(mockTargetFeatureSwitchFacade).isSizeOrderingForUIEnabled();
        productPageController.sortVariantOptionData(productData);
        verify(mockTargetFeatureSwitchFacade, times(2)).isSizeOrderingForUIEnabled();
        verifyNoMoreInteractions(mockTargetFeatureSwitchFacade);
        verifyZeroInteractions(variantSortStrategy,
                targetCustomVariantSortStrategy);
    }

    @Test
    public void testSortVariantOptionDataBaseOptionsFeatureSwitchOnWithVariantOptions() {
        final ProductData productData = new ProductData();
        final BaseOptionData baseOptionData = new BaseOptionData();

        baseOptionData.setOptions(Arrays.asList(new VariantOptionData()));
        productData.setBaseOptions(Collections.singletonList(baseOptionData));
        productData.setVariantOptions(Arrays.asList(new VariantOptionData()));
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isSizeOrderingForUIEnabled();
        productPageController.sortVariantOptionData(productData);

        verify(mockTargetFeatureSwitchFacade, times(2)).isSizeOrderingForUIEnabled();
        verifyNoMoreInteractions(mockTargetFeatureSwitchFacade);
        verifyZeroInteractions(variantSortStrategy,
                targetCustomVariantSortStrategy);
    }

    /**
     * Empty category and price above the threshold with total interest
     * 
     * @throws ENEQueryException
     * @throws TargetEndecaException
     */

    @Test
    public void testPopulateTotalInterestWithEmptyExcludeCategoryWithoutPriceRange() throws TargetEndecaException,
            ENEQueryException {
        final PriceData price = mock(PriceData.class);
        given(sharedConfigFacade.getConfigByCode(EXCLUDE_CATEGORY)).willReturn(null);
        willReturn(endecaData).given(productPageController).populateEndecaSearchStateData(PRODUCT_CODE);
        given(targetProductData.getPrice()).willReturn(price);
        given(targetProductData.getTopLevelCategory()).willReturn("kids");
        given(price.getValue()).willReturn(new BigDecimal(40.00));
        willReturn(Boolean.TRUE).given(listerData).isInStock();
        given(listerData.getTotalInterest()).willReturn("50");
        given(listerData.getPrice()).willReturn(price);
        productPageController.populateCustomerTotalInterestNumberForPrd(targetProductData, listerData);
        verify(targetProductData).setTotalInterest(50);
        verify(targetProductData).setMaxThresholdInterest(200);
    }

    /**
     * Exclude category clearance,and product belonging to clearance category
     */

    @Test
    public void testPopulateTotalInterestWithExcludeCategoryWithoutPriceRange() {
        final PriceData price = mock(PriceData.class);
        given(sharedConfigFacade.getConfigByCode(EXCLUDE_CATEGORY)).willReturn("clearance,kids");
        willReturn(endecaData).given(productPageController).populateEndecaSearchStateData(PRODUCT_CODE);
        given(targetProductData.getPrice()).willReturn(price);
        given(targetProductData.getTopLevelCategory()).willReturn("Clearance");
        given(price.getValue()).willReturn(new BigDecimal(40.00));
        willReturn(Boolean.TRUE).given(listerData).isInStock();
        given(listerData.getTotalInterest()).willReturn("50");
        given(listerData.getPrice()).willReturn(price);
        productPageController.populateCustomerTotalInterestNumberForPrd(targetProductData, listerData);
        verify(targetProductData).getTopLevelCategory();
        verifyNoMoreInteractions(targetProductData);
    }

    /**
     * Empty exclude category,but gift card product
     */

    @Test
    public void testPopulateTotalInterestWithGiftCardPrd() {
        given(sharedConfigFacade.getConfigByCode(EXCLUDE_CATEGORY)).willReturn(null);
        given(targetProductData.getTopLevelCategory()).willReturn("Clearance");
        willReturn(Boolean.TRUE).given(targetProductData).isGiftCard();
        productPageController.populateCustomerTotalInterestNumberForPrd(targetProductData, listerData);
        verify(targetProductData).isGiftCard();
        verifyNoMoreInteractions(targetProductData);
    }

    /**
     * Empty exclude category,price range but greater than the config threshold price
     */

    @Test
    public void testPopulateTotalInterestWithEmptyExcludeCategoryWithPriceRange() {
        final PriceData price = mock(PriceData.class);
        final PriceRangeData priceRange = mock(PriceRangeData.class);
        given(sharedConfigFacade.getConfigByCode(EXCLUDE_CATEGORY)).willReturn(null);
        given(priceRange.getMaxPrice()).willReturn(price);
        willReturn(endecaData).given(productPageController).populateEndecaSearchStateData(PRODUCT_CODE);
        given(listerData.getPriceRange()).willReturn(priceRange);
        given(listerData.getTotalInterest()).willReturn("100");
        given(targetProductData.getTopLevelCategory()).willReturn("kids");
        given(price.getValue()).willReturn(new BigDecimal(40.00));
        willReturn(Boolean.TRUE).given(listerData).isInStock();
        productPageController.populateCustomerTotalInterestNumberForPrd(targetProductData, listerData);
        verify(targetProductData).setTotalInterest(100);
        verify(targetProductData).setMaxThresholdInterest(200);
    }

    /**
     * Empty exclude category,price range but greater than the config threshold price,but out of stock
     */

    @Test
    public void testPopulateTotalInterestWithEmptyExcludeCategoryWithPriceRangeOutofStock() {
        final PriceData price = mock(PriceData.class);
        final PriceRangeData priceRange = mock(PriceRangeData.class);
        given(sharedConfigFacade.getConfigByCode(EXCLUDE_CATEGORY)).willReturn(null);
        given(priceRange.getMaxPrice()).willReturn(price);
        willReturn(endecaData).given(productPageController).populateEndecaSearchStateData(PRODUCT_CODE);
        given(listerData.getPriceRange()).willReturn(priceRange);
        given(listerData.getTotalInterest()).willReturn("100");
        given(targetProductData.getTopLevelCategory()).willReturn("kids");
        given(price.getValue()).willReturn(new BigDecimal(40.00));
        willReturn(Boolean.FALSE).given(listerData).isInStock();
        productPageController.populateCustomerTotalInterestNumberForPrd(targetProductData, listerData);
        verify(targetProductData).isGiftCard();
        verifyNoMoreInteractions(targetProductData);

    }

    /**
     * Exclude category empty,price range less than configured value
     */

    @Test
    public void testPopulateTotalInterestWithEmptyExcludeCategoryWithPriceRangeLessThanConfig() {
        final PriceData price = mock(PriceData.class);
        final PriceRangeData priceRange = mock(PriceRangeData.class);
        given(sharedConfigFacade.getConfigByCode(EXCLUDE_CATEGORY)).willReturn(null);
        given(priceRange.getMaxPrice()).willReturn(price);
        willReturn(endecaData).given(productPageController).populateEndecaSearchStateData(PRODUCT_CODE);
        given(listerData.getPriceRange()).willReturn(priceRange);
        given(listerData.getTotalInterest()).willReturn("100");
        given(targetProductData.getTopLevelCategory()).willReturn("kids");
        given(price.getValue()).willReturn(new BigDecimal(10.00));
        willReturn(Boolean.FALSE).given(listerData).isInStock();
        productPageController.populateCustomerTotalInterestNumberForPrd(targetProductData, listerData);
        verify(targetProductData).isGiftCard();
        verifyNoMoreInteractions(targetProductData);

    }

    /**
     * Exclude category empty,satisfies price,stock with no minimum level of inetrest configured
     */

    @Test
    public void testPopulateTotalInterestWithEmptyExcludeCategoryWithNoMimLevelInterest() {
        given(sharedConfigFacade.getConfigByCode(MIN_LEVEL_INTEREST)).willReturn(null);
        final PriceData price = mock(PriceData.class);
        final PriceRangeData priceRange = mock(PriceRangeData.class);
        given(sharedConfigFacade.getConfigByCode(EXCLUDE_CATEGORY)).willReturn(null);
        given(priceRange.getMaxPrice()).willReturn(price);
        willReturn(endecaData).given(productPageController).populateEndecaSearchStateData(PRODUCT_CODE);
        given(listerData.getPriceRange()).willReturn(priceRange);
        given(listerData.getTotalInterest()).willReturn("100");
        given(targetProductData.getTopLevelCategory()).willReturn("kids");
        given(price.getValue()).willReturn(new BigDecimal(20.00));
        willReturn(Boolean.TRUE).given(listerData).isInStock();
        productPageController.populateCustomerTotalInterestNumberForPrd(targetProductData, listerData);
        verify(targetProductData).isGiftCard();
        verify(targetProductData).setTotalInterest(100);
        verify(targetProductData).setMaxThresholdInterest(200);
    }

    /**
     * Exclude category empty,equal price,stock with no minimum level of inetrest configured
     */

    @Test
    public void testPopulateTotalInterestWithEmptyExcludeCategoryEqPrice() {
        given(sharedConfigFacade.getConfigByCode(MIN_LEVEL_INTEREST)).willReturn(null);
        final PriceData price = mock(PriceData.class);
        final PriceRangeData priceRange = mock(PriceRangeData.class);
        given(sharedConfigFacade.getConfigByCode(EXCLUDE_CATEGORY)).willReturn(null);
        given(priceRange.getMaxPrice()).willReturn(price);
        willReturn(endecaData).given(productPageController).populateEndecaSearchStateData(PRODUCT_CODE);
        given(listerData.getPriceRange()).willReturn(priceRange);
        given(listerData.getTotalInterest()).willReturn("100");
        given(targetProductData.getTopLevelCategory()).willReturn("kids");
        given(price.getValue()).willReturn(new BigDecimal(10.00));
        willReturn(Boolean.TRUE).given(listerData).isInStock();
        productPageController.populateCustomerTotalInterestNumberForPrd(targetProductData, listerData);
        verify(targetProductData).isGiftCard();
        verify(targetProductData).setTotalInterest(100);
        verify(targetProductData).setMaxThresholdInterest(200);
    }

    /**
     * Endeca Search results null with excluded category
     */

    @Test
    public void testPopulateTotalInterestWithNullEndecaData() {
        productPageController.populateCustomerTotalInterestNumberForPrd(targetProductData, null);
        verify(targetProductData).isGiftCard();
        verifyNoMoreInteractions(targetProductData);
    }

    /**
     * Empty category and price above the threshold with total interest
     */
    @Test
    public void testPopulateTotalInterestWithEmptyExcludeCategoryWithoutPriceRangeAndNoMinPrice() {
        given(sharedConfigFacade.getConfigByCode(MIN_PRICE_THRESHOLD)).willReturn(null);
        final PriceData price = mock(PriceData.class);
        given(sharedConfigFacade.getConfigByCode(EXCLUDE_CATEGORY)).willReturn(null);
        willReturn(endecaData).given(productPageController).populateEndecaSearchStateData(PRODUCT_CODE);
        given(targetProductData.getPrice()).willReturn(price);
        given(targetProductData.getTopLevelCategory()).willReturn("kids");
        given(price.getValue()).willReturn(new BigDecimal(2.00));
        willReturn(Boolean.TRUE).given(listerData).isInStock();
        given(listerData.getTotalInterest()).willReturn("50");
        given(listerData.getPrice()).willReturn(price);
        productPageController.populateCustomerTotalInterestNumberForPrd(targetProductData, listerData);
        verify(targetProductData).setTotalInterest(50);
        verify(targetProductData).setMaxThresholdInterest(200);
    }

    @Test
    public void testIsExcludeCategory() {
        given(sharedConfigFacade.getConfigByCode(EXCLUDE_CATEGORY)).willReturn("kids,clearance");
        given(targetProductData.getTopLevelCategory()).willReturn("cleArance");
        final boolean isCatExcluded = productPageController.isCategoryExcluded(targetProductData);
        assertThat(isCatExcluded).isTrue();
    }

    @Test
    public void testIsExcludeCategoryNull() {
        given(sharedConfigFacade.getConfigByCode(EXCLUDE_CATEGORY)).willReturn(null);
        given(targetProductData.getTopLevelCategory()).willReturn("cleArance");
        final boolean isCatExcluded = productPageController.isCategoryExcluded(targetProductData);

        assertThat(isCatExcluded).isFalse();
    }

    @Test
    public void testIsExcludeCategoryEmpty() {
        given(sharedConfigFacade.getConfigByCode(EXCLUDE_CATEGORY)).willReturn("");
        given(targetProductData.getTopLevelCategory()).willReturn("cleArance");
        final boolean isCatExcluded = productPageController.isCategoryExcluded(targetProductData);
        assertThat(isCatExcluded).isFalse();
    }

    @Test
    public void testIsExcludeCategory1() {
        given(sharedConfigFacade.getConfigByCode(EXCLUDE_CATEGORY)).willReturn("kids,clearance");
        given(targetProductData.getTopLevelCategory()).willReturn("women");
        final boolean isCatExcluded = productPageController.isCategoryExcluded(targetProductData);
        assertThat(isCatExcluded).isFalse();
    }

    @Test
    public void testShowProductVariants() {
        final ArgumentCaptor<List> productOptionsCaptor = ArgumentCaptor.forClass(List.class);
        given(productFacade.getProductForCodeAndOptions(any(String.class), productOptionsCaptor.capture()))
                .willReturn(targetProductData);

        final String forward = productPageController.showProductVariants("productCode", mockModel,
                httpresponse);
        assertThat(forward).isEqualTo(ControllerConstants.Views.Fragments.Product.VARIANT_PARTIAL);
        assertThat(productOptionsCaptor.getValue()).containsExactly(ProductOption.BASIC, ProductOption.VARIANT_FULL);
        verify(mockModel).addAttribute("product", targetProductData);
        verify(productFacade).getProductForCodeAndOptions(any(String.class), any(List.class));
        verify(productService).getProductForCode(any(String.class));
    }

    @Test
    public void testShowDetailedFragment() {
        final String forward = productPageController.showDetailedFragment("productCode", mockModel, request,
                httpresponse, "bulkyBoardStoreNumber");
        verify(mockModel).addAttribute(any(AddToCartForm.class));
        assertThat(forward).isEqualTo(ControllerConstants.Views.Fragments.Product.SERIES_VIEW);
    }

    @Test
    public void testShowProductVariantContent() {
        final String forward = productPageController.showProductVariantContent("productCode", mockModel, request,
                httpresponse, "bulkyBoardStoreNumber");
        verify(mockModel).addAttribute(any(AddToCartForm.class));
        assertThat(forward).isEqualTo(ControllerConstants.Views.Fragments.Product.VARIANT_PARTIAL_DETAIL);
    }

    @Test
    public void testSearchStoreStock() {
        final ProductStockSearchResultResponseData responseData = new ProductStockSearchResultResponseData();
        responseData.setSelectedLocation("Point Cook, 3030 VIC");
        given(targetStoreFinderStockFacade.doSearchProductStock("P1001_Black_S", "3030", null, null, null,
                pageableData, true)).willReturn(response);
        given(response.getData()).willReturn(responseData);
        willReturn(Boolean.TRUE).given(response).isSuccess();
        given(searchQuerySanitiser.sanitiseSearchText("3030")).willReturn("3030");
        given(searchQuerySanitiser.sanitiseSearchText("Point Cook, 3030 VIC")).willReturn(
                "Point Cook  3030 VIC");
        productPageController.searchProductStockByLocation("P1001_Black_S", "3030", null, null, 0, httpresponse);
        verify(config).getInt("storefront.storelocator.pageSize", 5);
        verify(cookieGenerator).addCookie(httpresponse, "Point%20Cook%20%203030%20VIC");
        verify(pageableData).setCurrentPage(0);
        verify(targetStoreFinderStockFacade).doSearchProductStock("P1001_Black_S", "3030", null, null, null,
                pageableData, true);
    }

    @Test
    public void testSearchStoreStockWithEmptyLocation() {
        given(targetStoreFinderStockFacade.doSearchProductStock("P1001_Black_S", null, Double.valueOf(55.55),
                Double.valueOf(106.55), null, pageableData, true)).willReturn(response);
        willReturn(Boolean.FALSE).given(response).isSuccess();
        productPageController.searchProductStockByLocation("P1001_Black_S", "", Double.valueOf(55.55),
                Double.valueOf(106.55), 0, httpresponse);
        verify(config).getInt("storefront.storelocator.pageSize", 5);
        verifyNoMoreInteractions(cookieGenerator);
        verify(pageableData).setCurrentPage(0);
        verify(targetStoreFinderStockFacade).doSearchProductStock("P1001_Black_S", null, Double.valueOf(55.55),
                Double.valueOf(106.55), null, pageableData, true);
    }

    @Test
    public void testShowProductDetailWithStoreStockDefault() throws CMSItemNotFoundException,
            UnsupportedEncodingException {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isInStoreStockVisibilityEnabled();
        willReturn(Boolean.TRUE).given(targetProductData).isShowStoreStockForProduct();
        willReturn(null).given(productPageController).checkRequestUrl(request, httpresponse, null);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        final String forward = productPageController.productDetail("productCode", mockModel, request, httpresponse,
                "bulkyBoardStoreNumber");
        verify(productFacade).getBulkyBoardProductForOptions(productModel, Arrays.asList(
                ProductOption.BASIC, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION,
                ProductOption.GALLERY, ProductOption.CATEGORIES, ProductOption.REVIEW, ProductOption.PROMOTIONS,
                ProductOption.CLASSIFICATION, ProductOption.VARIANT_FULL, ProductOption.DELIVERY_PROMOTIONAL_MESSAGE,
                ProductOption.DELIVERY_ZONE), "bulkyBoardStoreNumber");
        verify(productPageController).populateProductStockFromVariants(targetProductData, mockModel);
        assertThat(forward).isNull();
    }

    @Test
    public void testShowProductDetailWithStoreStockConfigured() throws CMSItemNotFoundException,
            UnsupportedEncodingException {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isInStoreStockVisibilityEnabled();
        willReturn(Boolean.TRUE).given(targetProductData).isShowStoreStockForProduct();
        willReturn(null).given(productPageController).checkRequestUrl(request, httpresponse, null);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        given(sharedConfigFacade.getConfigByCode("inStoreStockVisibility.limitedStockThreshold")).willReturn("6");
        final String forward = productPageController.productDetail("productCode", mockModel, request, httpresponse,
                "bulkyBoardStoreNumber");
        verify(productFacade).getBulkyBoardProductForOptions(productModel, Arrays.asList(
                ProductOption.BASIC, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION,
                ProductOption.GALLERY, ProductOption.CATEGORIES, ProductOption.REVIEW, ProductOption.PROMOTIONS,
                ProductOption.CLASSIFICATION, ProductOption.VARIANT_FULL, ProductOption.DELIVERY_PROMOTIONAL_MESSAGE,
                ProductOption.DELIVERY_ZONE), "bulkyBoardStoreNumber");
        verify(productPageController).populateProductStockFromVariants(targetProductData, mockModel);
        assertThat(forward).isNull();
    }

    @Test
    public void testShowProductDetailWithoutStoreStock() throws CMSItemNotFoundException, UnsupportedEncodingException {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isInStoreStockVisibilityEnabled();
        willReturn(Boolean.FALSE).given(targetProductData).isShowStoreStockForProduct();
        willReturn(null).given(productPageController).checkRequestUrl(request, httpresponse, null);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        final String forward = productPageController.productDetail("productCode", mockModel, request, httpresponse,
                "bulkyBoardStoreNumber");
        verify(productFacade).getBulkyBoardProductForOptions(productModel, Arrays.asList(
                ProductOption.BASIC, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION,
                ProductOption.GALLERY, ProductOption.CATEGORIES, ProductOption.REVIEW, ProductOption.PROMOTIONS,
                ProductOption.CLASSIFICATION, ProductOption.VARIANT_FULL, ProductOption.DELIVERY_PROMOTIONAL_MESSAGE,
                ProductOption.DELIVERY_ZONE), "bulkyBoardStoreNumber");
        verify(productPageController).populateProductStockFromVariants(targetProductData, mockModel);
        verify(sharedConfigFacade, times(0)).getConfigByCode("inStoreStockVisibility.limitedStockThreshold");
        assertThat(forward).isNull();
    }

    public void testSearchStoreStockWithSpaceInLoc() {
        final String location = "waurn ponds, 3215";
        given(targetStoreFinderStockFacade.doSearchProductStock("P1001_Black_S", location, null,
                null, null, pageableData, true)).willReturn(response);
        willReturn(Boolean.TRUE).given(response).isSuccess();
        given(searchQuerySanitiser.sanitiseSearchText(location)).willReturn(location);
        productPageController.searchProductStockByLocation("P1001_Black_S", "waurn ponds", null,
                null, 0, httpresponse);
        verify(config).getInt("storefront.storelocator.pageSize", 5);
        verify(cookieGenerator).addCookie(httpresponse, "waurn%20ponds%20%203215");
        verify(pageableData).setCurrentPage(0);
        verify(targetStoreFinderStockFacade).doSearchProductStock("P1001_Black_S", location, null,
                null, null, pageableData, true);
    }

    @Test
    public void testSearchStoreStockWithError() {
        given(targetStoreFinderStockFacade.doSearchProductStock("P1001_Black_S", "3030", null, null, null,
                pageableData, true)).willReturn(response);
        willReturn(Boolean.FALSE).given(response).isSuccess();
        given(searchQuerySanitiser.sanitiseSearchText("3030")).willReturn("3030");
        productPageController.searchProductStockByLocation("P1001_Black_S", "3030", null, null, 0, httpresponse);
        verify(config).getInt("storefront.storelocator.pageSize", 5);
        verifyNoMoreInteractions(cookieGenerator);
        verify(pageableData).setCurrentPage(0);
        verify(targetStoreFinderStockFacade).doSearchProductStock("P1001_Black_S", "3030", null, null, null,
                pageableData, true);
    }

    @Test
    public void testGetAllSellableVariantsWithNoStock() {
        final List<ProductData> variants = new ArrayList<>();
        variants.add(prepareVariantsWithStock(false, false, null, "P1001_Black_S"));
        given(productFacade.getAllSellableVariantsWithOptions("P1001_Black",
                Arrays.asList(ProductOption.STOCK, ProductOption.CONSOLIDATED_STOCK)))
                        .willReturn(variants);
        final String view = productPageController.getStockForVariants("P1001_Black", mockModel, null, request);
        verify(productFacade).getAllSellableVariantsWithOptions("P1001_Black",
                Arrays.asList(ProductOption.STOCK, ProductOption.CONSOLIDATED_STOCK));
        verify(mockModel).addAttribute("variantsStock", variants);
        verify(mockModel).addAttribute("availableOnline", Boolean.FALSE);
        verify(mockModel).addAttribute("availableInStore", Boolean.FALSE);
        verify(mockModel).addAttribute("metaRobots", "noindex,nofollow");
        assertThat(view).isEqualTo(ControllerConstants.Views.Fragments.Product.VARIANT_STOCK);
    }

    @Test
    public void testGetAllSellableVariantsWithOnlineStock() {
        final List<ProductData> variants = new ArrayList<>();
        variants.add(prepareVariantsWithStock(true, false, null, "P1001_Black_S"));
        given(productFacade.getAllSellableVariantsWithOptions("P1001_Black",
                Arrays.asList(ProductOption.STOCK, ProductOption.CONSOLIDATED_STOCK))).willReturn(variants);
        final String view = productPageController.getStockForVariants("P1001_Black", mockModel, null, request);
        verify(productFacade).getAllSellableVariantsWithOptions("P1001_Black",
                Arrays.asList(ProductOption.STOCK, ProductOption.CONSOLIDATED_STOCK));
        verify(mockModel).addAttribute("variantsStock", variants);
        verify(mockModel).addAttribute("availableOnline", Boolean.TRUE);
        verify(mockModel).addAttribute("availableInStore", Boolean.FALSE);
        verify(mockModel).addAttribute("availableForStore", Boolean.FALSE);
        verify(mockModel, never()).addAttribute("metaRobots", "noindex,nofollow");
        assertThat(view).isEqualTo(ControllerConstants.Views.Fragments.Product.VARIANT_STOCK);
    }

    @Test
    public void testGetAllSellableVariantsWithBothStock() {
        final List<ProductData> variants = new ArrayList<>();
        variants.add(prepareVariantsWithStock(true, true, Long.valueOf(1), "P1001_Black_S"));
        given(productFacade.getAllSellableVariantsWithOptions("P1001_Black",
                Arrays.asList(ProductOption.STOCK, ProductOption.CONSOLIDATED_STOCK)))
                        .willReturn(variants);
        final ArgumentCaptor<String> availabilityTime = ArgumentCaptor.forClass(String.class);
        final String view = productPageController.getStockForVariants("P1001_Black", mockModel, null, null);
        verify(productFacade).getAllSellableVariantsWithOptions("P1001_Black",
                Arrays.asList(ProductOption.STOCK, ProductOption.CONSOLIDATED_STOCK));
        verify(mockModel).addAttribute("variantsStock", variants);
        verify(mockModel).addAttribute("availableOnline", Boolean.TRUE);
        verify(mockModel).addAttribute("availableInStore", Boolean.TRUE);
        verify(mockModel).addAttribute("availableForStore", Boolean.TRUE);
        verify(mockModel).addAttribute(eq("availabilityTime"), availabilityTime.capture());
        verify(mockModel, never()).addAttribute("metaRobots", "noindex,nofollow");
        assertThat(availabilityTime.getValue()).isNotEmpty();
        assertThat(view).isEqualTo(ControllerConstants.Views.Fragments.Product.VARIANT_STOCK);
    }

    @Test
    public void testGetAllSellableVariantsWithOutOfStoreStock() {
        final List<ProductData> variants = new ArrayList<>();
        variants.add(prepareVariantsWithStock(true, false, Long.valueOf(0), "P1001_Black_S"));
        given(productFacade.getAllSellableVariantsWithOptions("P1001_Black",
                Arrays.asList(ProductOption.STOCK, ProductOption.CONSOLIDATED_STOCK)))
                        .willReturn(variants);
        final String view = productPageController.getStockForVariants("P1001_Black", mockModel, null, request);
        verify(productFacade).getAllSellableVariantsWithOptions("P1001_Black",
                Arrays.asList(ProductOption.STOCK, ProductOption.CONSOLIDATED_STOCK));
        verify(mockModel).addAttribute("variantsStock", variants);
        verify(mockModel).addAttribute("availableOnline", Boolean.TRUE);
        verify(mockModel).addAttribute("availableInStore", Boolean.FALSE);
        verify(mockModel).addAttribute("availableForStore", Boolean.TRUE);
        verify(mockModel, never()).addAttribute("metaRobots", "noindex,nofollow");
        assertThat(view).isEqualTo(ControllerConstants.Views.Fragments.Product.VARIANT_STOCK);
    }

    @Test
    public void testGetAllSellableVariantsWithNoOnlineStock() {
        final List<ProductData> variants = new ArrayList<>();
        variants.add(prepareVariantsWithStock(false, true, Long.valueOf(1), "P1001_Black_S"));
        given(productFacade.getAllSellableVariantsWithOptions("P1001_Black",
                Arrays.asList(ProductOption.STOCK, ProductOption.CONSOLIDATED_STOCK)))
                        .willReturn(variants);
        final String view = productPageController.getStockForVariants("P1001_Black", mockModel, null, request);
        verify(productFacade).getAllSellableVariantsWithOptions("P1001_Black",
                Arrays.asList(ProductOption.STOCK, ProductOption.CONSOLIDATED_STOCK));
        verify(mockModel).addAttribute("variantsStock", variants);
        verify(mockModel).addAttribute("availableOnline", Boolean.FALSE);
        verify(mockModel).addAttribute("availableInStore", Boolean.TRUE);
        verify(mockModel).addAttribute("availableForStore", Boolean.TRUE);
        verify(mockModel, never()).addAttribute("metaRobots", "noindex,nofollow");
        assertThat(view).isEqualTo(ControllerConstants.Views.Fragments.Product.VARIANT_STOCK);
    }

    @Test
    public void testGetAllSellableVariantsWithStoreStock() {
        final List<ProductData> variants = mock(List.class);
        given(productFacade.getAllSellableVariantsWithStoreStockAndOptions("P1001_Black", Integer.valueOf(1),
                Arrays.asList(ProductOption.CONSOLIDATED_STOCK)))
                        .willReturn(variants);
        final String view = productPageController.getStockForVariants("P1001_Black", mockModel, Integer.valueOf(1),
                request);
        verify(productFacade).getAllSellableVariantsWithStoreStockAndOptions("P1001_Black", Integer.valueOf(1),
                Arrays.asList(ProductOption.CONSOLIDATED_STOCK));
        verify(mockModel).addAttribute("variantsStock", variants);
        assertThat(view).isEqualTo(ControllerConstants.Views.Fragments.Product.VARIANT_STOCK);
    }

    @Test
    public void testGetStockForVariantsUsingFluent() {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isFluentStockLookupEnabled();
        final List<ProductData> variants = mock(List.class);
        given(productFacade.getFluentStock("P1001_Black", "1")).willReturn(variants);

        final String view = productPageController.getStockForVariants("P1001_Black", mockModel, Integer.valueOf(1),
                request);
        verify(productFacade, never()).getAllSellableVariantsWithStoreStockAndOptions(anyString(),
                any(Integer.class), anyList());
        verify(mockModel).addAttribute("variantsStock", variants);
        assertThat(view).isEqualTo(ControllerConstants.Views.Fragments.Product.VARIANT_STOCK);
    }

    @Test
    public void testFindStorePreferredStore() {
        final String result = productPageController.findStore("12345678", false, mockModel);

        assertThat(result).isEqualTo(ControllerConstants.Views.Fragments.Product.FIND_NEAREST_STORE);
        verifyZeroInteractions(mockModel);
    }

    @Test
    public void testFindStoreFindInAnotherStore() {
        final String productCode = "12345678";

        willReturn(Boolean.FALSE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.LOCATION_SERVICES);

        final TargetProductData productData = new TargetProductData();
        given(productFacade.getProductForCodeAndOptions(productCode, Arrays.asList(ProductOption.BASIC)))
                .willReturn(productData);

        final String result = productPageController.findStore(productCode, true, mockModel);

        assertThat(result).isEqualTo(ControllerConstants.Views.Fragments.Product.FIND_NEAREST_STORE);
        verify(mockModel).addAttribute("product", productData);
        verify(mockModel).addAttribute("isFias", Boolean.TRUE);
        verify(mockModel, never()).addAttribute("productJson",
                JsonConversionUtil.convertToJsonString(productData));
    }

    @Test
    public void testFindStoreFindInAnotherStoreLocationServiceFS() {
        final String productCode = "12345678";

        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.LOCATION_SERVICES);

        final TargetProductData productData = new TargetProductData();
        given(productFacade.getProductForCodeAndOptions(productCode, Arrays.asList(ProductOption.BASIC)))
                .willReturn(productData);

        final String result = productPageController.findStore(productCode, true, mockModel);

        assertThat(result).isEqualTo(ControllerConstants.Views.Fragments.Product.FIND_NEAREST_STORE);
        verify(mockModel).addAttribute("product", productData);
        verify(mockModel).addAttribute("isFias", Boolean.TRUE);
        verify(mockModel).addAttribute("productJson", JsonConversionUtil.convertToJsonString(productData));
    }

    @Test
    public void testPopulateAfterPayConfig() {
        final AfterpayConfigData afterpayConfigData = mock(AfterpayConfigData.class);
        given(afterpayConfigFacade.getAfterpayConfig()).willReturn(afterpayConfigData);
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isAfterpayEnabled();

        productPageController.populateAfterPayConfig(mockModel);
        verify(mockModel).addAttribute("afterpayConfig", afterpayConfigData);
    }

    @Test
    public void testPopulateAfterPayConfigDisabled() {
        productPageController.populateAfterPayConfig(mockModel);
        verify(afterpayConfigFacade, never()).getAfterpayConfig();
    }

    @Test
    public void testAfterpayConfigInProductDetail() throws CMSItemNotFoundException, UnsupportedEncodingException,
            TargetEndecaException, ENEQueryException {
        given(productService.getProductForCode("P01")).willReturn(productModel);
        willReturn(StringUtils.EMPTY).given(productPageController).checkForRedirect(request, httpresponse,
                productModel);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        productPageController.productDetail("P01", mockModel, request, httpresponse, null);
        verify(productPageController).populateAfterPayConfig(mockModel);
    }

    @Test
    public void testAfterpayModalInShowQuickView() {
        productPageController.showQuickView("P01", mockModel, request, httpresponse, null);
        verify(productPageController).populateAfterPayConfig(mockModel);
    }

    @Test
    public void testAfterpayModalInShowDetailedFragment() {
        productPageController.showDetailedFragment("P01", mockModel, request, httpresponse, null);
        verify(productPageController).populateAfterPayConfig(mockModel);
    }

    @Test
    public void testPopulateAfterpayModalConfig() throws CMSItemNotFoundException {
        final ContentPageModel afterpayModal = mock(ContentPageModel.class);
        given(mockCmsPageService.getPageForLabelOrId(TgtwebcoreConstants.AFTERPAY_MODAL_ID))
                .willReturn(afterpayModal);
        given(afterpayModal.getLabel()).willReturn("/modal/afterpay");
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isAfterpayEnabled();

        productPageController.populateAfterPayModal(mockModel);
        verify(mockModel).addAttribute("afterpayModal", "/modal/afterpay");
    }

    @Test
    public void testPopulateAfterpayModalConfigDisabled() throws CMSItemNotFoundException {
        final String afterpayModalID = TgtwebcoreConstants.AFTERPAY_MODAL_ID;
        productPageController.populateAfterPayConfig(mockModel);
        verify(mockCmsPageService, never()).getPageForLabelOrId(afterpayModalID);
    }

    @Test
    public void testAfterpayModalInProductDetail() throws CMSItemNotFoundException, UnsupportedEncodingException {
        given(productService.getProductForCode("P01")).willReturn(productModel);
        willReturn(StringUtils.EMPTY).given(productPageController).checkForRedirect(request, httpresponse,
                productModel);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);

        productPageController.productDetail("P01", mockModel, request, httpresponse, null);
        verify(productPageController).populateAfterPayModal(mockModel);
    }

    @Test
    public void testAfterpayConfigIn() {
        productPageController.showQuickView("P01", mockModel, request, httpresponse, null);
        verify(productPageController).populateAfterPayModal(mockModel);
    }

    // zip - start

    @Test
    public void testZipPaymentDetailsInProductDetail() throws CMSItemNotFoundException, UnsupportedEncodingException,
            TargetEndecaException, ENEQueryException {
        given(productService.getProductForCode("P01")).willReturn(productModel);
        willReturn(StringUtils.EMPTY).given(productPageController).checkForRedirect(request, httpresponse,
                productModel);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        productPageController.productDetail("P01", mockModel, request, httpresponse, null);
        verify(productPageController).populateZipPaymentModal(mockModel);
    }

    @Test
    public void testZipPaymentModalInShowQuickView() {
        productPageController.showQuickView("P01", mockModel, request, httpresponse, null);
        verify(productPageController).populateZipPaymentModal(mockModel);
    }

    @Test
    public void testZipPaymentModalInShowDetailedFragment() {
        productPageController.showDetailedFragment("P01", mockModel, request, httpresponse, null);
        verify(productPageController).populateZipPaymentModal(mockModel);
    }

    @Test
    public void testZipPaymentModalInProductDetail() throws CMSItemNotFoundException, UnsupportedEncodingException {
        given(productService.getProductForCode("P01")).willReturn(productModel);
        willReturn(StringUtils.EMPTY).given(productPageController).checkForRedirect(request, httpresponse,
                productModel);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);

        productPageController.productDetail("P01", mockModel, request, httpresponse, null);
        verify(productPageController).populateZipPaymentModal(mockModel);
    }

    // zip -end

    @Test
    public void testPopulateReactDataForQuickViewPdpFeatureSwitchOn() {
        final TargetProductListerData targetProductListerData = new TargetProductListerData();

        final List<ProductOption> productOptions = new ArrayList<>();
        productOptions.add(ProductOption.BASIC);
        productOptions.add(ProductOption.VARIANT_FULL);
        productOptions.add(ProductOption.DELIVERY_MODE_AVAILABILITY);
        productOptions.add(ProductOption.DELIVERY_PROMOTIONAL_MESSAGE);
        productOptions.add(ProductOption.BULKY_PRODUCT_DETAIL);

        given(targetProductListerFacade.getBaseProductDataByCode(PRODUCT_CODE, productOptions))
                .willReturn(targetProductListerData);
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isFeatureEnabled(UI_REACT_PDP_CAROUSEL);

        productPageController.showQuickView(PRODUCT_CODE, mockModel, request, httpresponse, null);
        verify(targetProductListerFacade).getBaseProductDataByCode(PRODUCT_CODE, productOptions);
        verify(mockModel).addAttribute("targetProductListerData",
                JsonConversionUtil.convertToJsonString(targetProductListerData));

        assertThat(targetProductListerData).isNotNull();
    }

    @Test
    public void testPopulateReactDataForQuickViewPdpFeatureSwitchOff()
            throws CMSItemNotFoundException, UnsupportedEncodingException {
        final TargetProductListerData targetProductListerData = null;

        final List<ProductOption> productOptions = new ArrayList<>();
        productOptions.add(ProductOption.BASIC);
        productOptions.add(ProductOption.VARIANT_FULL);
        productOptions.add(ProductOption.DELIVERY_MODE_AVAILABILITY);
        productOptions.add(ProductOption.DELIVERY_PROMOTIONAL_MESSAGE);
        productOptions.add(ProductOption.BULKY_PRODUCT_DETAIL);

        willReturn(Boolean.FALSE).given(mockTargetFeatureSwitchFacade).isFeatureEnabled(UI_REACT_PDP_CAROUSEL);
        given(targetProductListerFacade.getBaseProductDataByCode(PRODUCT_CODE, productOptions))
                .willReturn(targetProductListerData);

        willReturn(null).given(productPageController).checkRequestUrl(request, httpresponse, null);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        willDoNothing().given(productPageController).populateAfterPayModal(mockModel);
        willDoNothing().given(productPageController).populateCustomerTotalInterestNumberForPrd(null, listerData);

        productPageController.showQuickView(PRODUCT_CODE, mockModel, request, httpresponse, null);

        verify(productPageController, never()).populateProductListerData(mockModel, targetProductListerData);
        verify(targetProductListerFacade, never()).getBaseProductDataByCode(PRODUCT_CODE, productOptions);
        verify(mockModel, never()).addAttribute("targetProductListerData",
                JsonConversionUtil.convertToJsonString(targetProductListerData));
        assertThat(targetProductListerData).isNull();
    }

    @Test
    public void testPopulateReactDataForDetailedFragmentPdpFeatureSwitchOn() {
        final TargetProductListerData targetProductListerData = new TargetProductListerData();

        final List<ProductOption> productOptions = new ArrayList<>();
        productOptions.add(ProductOption.BASIC);
        productOptions.add(ProductOption.VARIANT_FULL);
        productOptions.add(ProductOption.DELIVERY_MODE_AVAILABILITY);
        productOptions.add(ProductOption.DELIVERY_PROMOTIONAL_MESSAGE);
        productOptions.add(ProductOption.BULKY_PRODUCT_DETAIL);

        given(targetProductListerFacade.getBaseProductDataByCode(PRODUCT_CODE, productOptions))
                .willReturn(targetProductListerData);
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isFeatureEnabled(UI_REACT_PDP_CAROUSEL);
        productPageController.showDetailedFragment(PRODUCT_CODE, mockModel, request, httpresponse, null);
        verify(targetProductListerFacade).getBaseProductDataByCode(PRODUCT_CODE, productOptions);
        verify(mockModel).addAttribute("targetProductListerData",
                JsonConversionUtil.convertToJsonString(targetProductListerData));

        assertThat(targetProductListerData).isNotNull();
    }

    @Test
    public void testPopulateReactDataForDetailedFragmentPdpFeatureSwitchOff()
            throws CMSItemNotFoundException, UnsupportedEncodingException {
        final TargetProductListerData targetProductListerData = null;

        final List<ProductOption> productOptions = new ArrayList<>();
        productOptions.add(ProductOption.BASIC);
        productOptions.add(ProductOption.VARIANT_FULL);
        productOptions.add(ProductOption.DELIVERY_MODE_AVAILABILITY);
        productOptions.add(ProductOption.DELIVERY_PROMOTIONAL_MESSAGE);
        productOptions.add(ProductOption.BULKY_PRODUCT_DETAIL);

        willReturn(Boolean.FALSE).given(mockTargetFeatureSwitchFacade).isFeatureEnabled(UI_REACT_PDP_CAROUSEL);
        given(targetProductListerFacade.getBaseProductDataByCode(PRODUCT_CODE, productOptions))
                .willReturn(targetProductListerData);

        willReturn(null).given(productPageController).checkRequestUrl(request, httpresponse, null);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        willDoNothing().given(productPageController).populateAfterPayModal(mockModel);
        willDoNothing().given(productPageController).populateCustomerTotalInterestNumberForPrd(null, listerData);

        productPageController.showDetailedFragment(PRODUCT_CODE, mockModel, request, httpresponse, null);

        verify(productPageController, never()).populateProductListerData(mockModel, targetProductListerData);
        verify(targetProductListerFacade, never()).getBaseProductDataByCode(PRODUCT_CODE, productOptions);
        verify(mockModel, never()).addAttribute("targetProductListerData",
                JsonConversionUtil.convertToJsonString(targetProductListerData));
        assertThat(targetProductListerData).isNull();
    }

    @Test
    public void testPopulateReactDataForShowProductVariantsFeatureSwitchOn() {
        final TargetProductListerData targetProductListerData = new TargetProductListerData();

        final List<ProductOption> productOptions = new ArrayList<>();
        productOptions.add(ProductOption.BASIC);
        productOptions.add(ProductOption.VARIANT_FULL);
        productOptions.add(ProductOption.DELIVERY_MODE_AVAILABILITY);
        productOptions.add(ProductOption.DELIVERY_PROMOTIONAL_MESSAGE);
        productOptions.add(ProductOption.BULKY_PRODUCT_DETAIL);

        final ArgumentCaptor<List> productOptionsCaptor = ArgumentCaptor.forClass(List.class);
        given(productFacade.getProductForCodeAndOptions(any(String.class), productOptionsCaptor.capture()))
                .willReturn(targetProductData);

        given(targetProductListerFacade.getBaseProductDataByCode(PRODUCT_CODE, productOptions))
                .willReturn(targetProductListerData);
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isFeatureEnabled(UI_REACT_PDP_CAROUSEL);

        productPageController.showProductVariants(PRODUCT_CODE, mockModel, httpresponse);

        verify(targetProductListerFacade).getBaseProductDataByCode(PRODUCT_CODE, productOptions);
        verify(mockModel).addAttribute("targetProductListerData",
                JsonConversionUtil.convertToJsonString(targetProductListerData));

        assertThat(targetProductListerData).isNotNull();
    }

    @Test
    public void testPopulateReactDataForShowProductVariantsPdpFeatureSwitchOff()
            throws CMSItemNotFoundException, UnsupportedEncodingException {
        final TargetProductListerData targetProductListerData = null;

        final List<ProductOption> productOptions = new ArrayList<>();
        productOptions.add(ProductOption.BASIC);
        productOptions.add(ProductOption.VARIANT_FULL);
        productOptions.add(ProductOption.DELIVERY_MODE_AVAILABILITY);
        productOptions.add(ProductOption.DELIVERY_PROMOTIONAL_MESSAGE);
        productOptions.add(ProductOption.BULKY_PRODUCT_DETAIL);

        final ArgumentCaptor<List> productOptionsCaptor = ArgumentCaptor.forClass(List.class);
        given(productFacade.getProductForCodeAndOptions(any(String.class), productOptionsCaptor.capture()))
                .willReturn(targetProductData);

        willReturn(Boolean.FALSE).given(mockTargetFeatureSwitchFacade).isFeatureEnabled(UI_REACT_PDP_CAROUSEL);
        given(targetProductListerFacade.getBaseProductDataByCode(PRODUCT_CODE, productOptions))
                .willReturn(targetProductListerData);

        willReturn(null).given(productPageController).checkRequestUrl(request, httpresponse, null);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        willDoNothing().given(productPageController).populateAfterPayModal(mockModel);
        willDoNothing().given(productPageController).populateCustomerTotalInterestNumberForPrd(null, listerData);

        productPageController.showProductVariants(PRODUCT_CODE, mockModel, httpresponse);

        verify(productPageController, never()).populateProductListerData(mockModel, targetProductListerData);
        verify(targetProductListerFacade, never()).getBaseProductDataByCode(PRODUCT_CODE, productOptions);
        verify(mockModel, never()).addAttribute("targetProductListerData",
                JsonConversionUtil.convertToJsonString(targetProductListerData));
        assertThat(targetProductListerData).isNull();
    }

    @Test
    public void testAfterpayConfigInShowDetailedFragment() {
        productPageController.showDetailedFragment("P01", mockModel, request, httpresponse, null);
        verify(productPageController).populateAfterPayModal(mockModel);
    }

    @Test
    public void testGetStockForVariantsDisplayWithOnlyOnlineInStock() {
        final List<ProductData> variants = new ArrayList<>();
        variants.add(prepareVariantsWithStock(true, false, null, "P1001_Black_S"));
        variants.add(prepareVariantsWithStock(true, false, null, "P1001_Black_L"));
        given(productFacade.getAllSellableVariantsWithOptions("P1001_Black",
                Arrays.asList(ProductOption.STOCK, ProductOption.CONSOLIDATED_STOCK)))
                        .willReturn(variants);
        productPageController.getStockForVariantsDisplay("P1001_Black", mockModel);
        verify(mockModel).addAttribute("variantsStock", variants);
        verify(mockModel).addAttribute("availableOnline", Boolean.TRUE);
        verify(mockModel).addAttribute("availableForStore", Boolean.FALSE);
        verify(mockModel).addAttribute("availableInStore", Boolean.FALSE);
        verifyNoMoreInteractions(mockModel);
    }


    @Test
    public void testGetStockForVariantsDisplayWithOnlyStoreStock() {
        final List<ProductData> variants = new ArrayList<>();
        variants.add(prepareVariantsWithStock(false, true, Long.valueOf(0), "P1001_Black_S"));
        variants.add(prepareVariantsWithStock(false, true, Long.valueOf(0), "P1001_Black_L"));
        given(productFacade.getAllSellableVariantsWithOptions("P1001_Black",
                Arrays.asList(ProductOption.STOCK, ProductOption.CONSOLIDATED_STOCK)))
                        .willReturn(variants);
        productPageController.getStockForVariantsDisplay("P1001_Black", mockModel);
        verify(mockModel).addAttribute("variantsStock", variants);
        verify(mockModel).addAttribute("availableOnline", Boolean.FALSE);
        verify(mockModel).addAttribute("availableForStore", Boolean.TRUE);
        verify(mockModel).addAttribute("availableInStore", Boolean.TRUE);
        verifyNoMoreInteractions(mockModel);
    }

    @Test
    public void testGetStockForVariantsDisplayWithOnlineAndInstore() {
        final List<ProductData> variants = new ArrayList<>();
        variants.add(prepareVariantsWithStock(true, true, Long.valueOf(0), "P1001_Black_S"));
        variants.add(prepareVariantsWithStock(false, true, Long.valueOf(0), "P1001_Black_L"));
        given(productFacade.getAllSellableVariantsWithOptions("P1001_Black",
                Arrays.asList(ProductOption.STOCK, ProductOption.CONSOLIDATED_STOCK)))
                        .willReturn(variants);
        productPageController.getStockForVariantsDisplay("P1001_Black", mockModel);
        verify(mockModel).addAttribute("variantsStock", variants);
        verify(mockModel).addAttribute("availableOnline", Boolean.TRUE);
        verify(mockModel).addAttribute("availableForStore", Boolean.TRUE);
        verify(mockModel).addAttribute("availableInStore", Boolean.TRUE);
        verifyNoMoreInteractions(mockModel);
    }

    @Test
    public void testGetStockForVariantsDisplayOutofStock() {
        final List<ProductData> variants = new ArrayList<>();
        variants.add(prepareVariantsWithStock(false, false, null, "P1001_Black_S"));
        variants.add(prepareVariantsWithStock(false, false, Long.valueOf(0), "P1001_Black_L"));
        given(productFacade.getAllSellableVariantsWithOptions("P1001_Black",
                Arrays.asList(ProductOption.STOCK, ProductOption.CONSOLIDATED_STOCK))).willReturn(variants);
        productPageController.getStockForVariantsDisplay("P1001_Black", mockModel);
        verify(mockModel).addAttribute("variantsStock", variants);
        verify(mockModel).addAttribute("availableOnline", Boolean.FALSE);
        verify(mockModel).addAttribute("availableForStore", Boolean.TRUE);
        verify(mockModel).addAttribute("availableInStore", Boolean.FALSE);
        verify(mockModel).addAttribute("metaRobots", "noindex,nofollow");
    }

    @Test
    public void testGetStockForVariantsDisplayUsingFluent() {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isFluentStockLookupEnabled();
        final List<ProductData> variants = Collections.emptyList();
        willReturn(variants).given(productFacade).getFluentStock("P1001_Black", null);

        productPageController.getStockForVariantsDisplay("P1001_Black", mockModel);
        verify(mockModel).addAttribute("variantsStock", variants);
        verify(productFacade, never()).getAllSellableVariantsWithOptions(anyString(), anyList());
    }

    @Test
    public void testWriteRerview() throws CMSItemNotFoundException {
        ReflectionTestUtils.setField(productPageController, "bazaarVoiceApiUri",
                "https://targetaustralia.ugc.bazaarvoice.com/bvstaging/test.js");
        given(productService.getProductForCode("P01")).willReturn(productModel);
        given(productFacade.getProductForCodeAndOptions(productModel.getCode(), Arrays.asList(
                ProductOption.BASIC, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION,
                ProductOption.CATEGORIES, ProductOption.PROMOTIONS, ProductOption.REVIEW)))
                        .willReturn(targetProductData);
        final ProductPageModel pageModel = mock(ProductPageModel.class);
        given(productPageController.getPageForProduct(productModel)).willReturn(pageModel);
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_DETAIL_PANEL);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);

        final String reviewView = productPageController.writeReview("P01", mockModel);
        verify(productPageController).getPreviewRedirect(productModel, false);
        verify(productPageController).updatePageTitle(productModel, mockModel);
        verify(productFacade).getProductForCodeAndOptions(productModel.getCode(), Arrays.asList(
                ProductOption.BASIC, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION,
                ProductOption.CATEGORIES, ProductOption.PROMOTIONS, ProductOption.REVIEW));
        verify(mockModel).addAttribute("productData", targetProductData);
        verify(mockModel).addAttribute("bazaarVoiceApiUri",
                "https://targetaustralia.ugc.bazaarvoice.com/bvstaging/test.js");
        verify(productPageController).storeCmsPageInModel(mockModel, pageModel);
        assertThat(reviewView).isEqualTo("pages/product/review");
    }

    @Test
    public void testPopulateProductStockFromVariants() {
        final List<TargetProductData> variants = new ArrayList<>();
        final TargetProductData variant1 = mock(TargetProductData.class);
        willReturn(Boolean.TRUE).given(variant1).isInStock();
        final StockData stockData1 = mock(StockData.class);
        given(variant1.getStock()).willReturn(stockData1);
        variants.add(variant1);
        final TargetProductData variant2 = mock(TargetProductData.class);
        willReturn(Boolean.TRUE).given(variant2).isInStock();
        given(variant2.getCode()).willReturn("P01");
        given(targetProductData.getCode()).willReturn("P01");
        final Map<String, Boolean> availableMap = mock(Map.class);
        given(variant2.getAvailable()).willReturn(availableMap);
        final StockData stockData2 = mock(StockData.class);
        given(variant2.getStock()).willReturn(stockData2);
        variants.add(variant2);
        final Map<String, Object> map = new HashMap<>();
        map.put("variantsStock", variants);
        given(mockModel.asMap()).willReturn(map);

        productPageController.populateProductStockFromVariants(targetProductData, mockModel);
        verify(targetProductData, times(2)).setInStock(true);
        verify(targetProductData).setStock(stockData2);
        verify(targetProductData).setAvailable(availableMap);
    }

    @Test
    public void testPopulateProductStockFromSelectedVariant() {
        final List<TargetProductData> variants = new ArrayList<>();
        final TargetProductData variant1 = mock(TargetProductData.class);
        willReturn(Boolean.TRUE).given(variant1).isInStock();
        final StockData stockData1 = mock(StockData.class);
        given(variant1.getStock()).willReturn(stockData1);
        variants.add(variant1);
        final Map<String, Object> map = new HashMap<>();
        map.put("variantsStock", variants);
        given(mockModel.asMap()).willReturn(map);

        productPageController.populateProductStockFromVariants(targetProductData, mockModel);
        verify(targetProductData).setInStock(true);
        verify(targetProductData).setStock(stockData1);
        verify(targetProductData).setAvailable(variant1.getAvailable());
    }

    @Test
    public void testPopulateProductOutOfStockFromVariants() {
        final List<TargetProductData> variants = new ArrayList<>();
        final TargetProductData variant1 = mock(TargetProductData.class);
        variants.add(variant1);
        final Map<String, Object> map = new HashMap<>();
        map.put("variantsStock", variants);
        given(mockModel.asMap()).willReturn(map);

        productPageController.populateProductStockFromVariants(targetProductData, mockModel);
        verify(targetProductData, never()).setInStock(anyBoolean());
        final ArgumentCaptor<StockData> stockDataCaptor = ArgumentCaptor.forClass(StockData.class);
        verify(targetProductData).setStock(stockDataCaptor.capture());
        assertThat(stockDataCaptor.getValue().getStockLevel()).isEqualTo(0);
        assertThat(stockDataCaptor.getValue().getStockLevelStatus()).isEqualTo(StockLevelStatus.OUTOFSTOCK);
        verify(targetProductData, never()).setAvailable(anyMap());
    }

    @Test
    public void testFetchProductData() {
        final ArgumentCaptor<List> optionsCaptor = ArgumentCaptor.forClass(List.class);
        final ArgumentCaptor<String> storeNumberCaptor = ArgumentCaptor.forClass(String.class);
        final ArgumentCaptor<TargetProductModel> productModelCaptor = ArgumentCaptor.forClass(TargetProductModel.class);
        given(productFacade.getBulkyBoardProductForOptions(productModelCaptor.capture(), optionsCaptor.capture(),
                storeNumberCaptor.capture()))
                        .willReturn(targetProductData);
        willDoNothing().given(productPageController).populateProductData(targetProductData, mockModel, request);
        willDoNothing().given(productPageController).populateProductStockFromVariants(targetProductData, mockModel);

        final TargetProductData productData = productPageController.fetchProductData(productModel, mockModel, request,
                null, true);
        assertThat(productData).isEqualTo(targetProductData);
        assertThat(productModelCaptor.getValue()).isEqualTo(productModel);
        assertThat(optionsCaptor.getValue()).containsExactly(ProductOption.BASIC, ProductOption.PRICE,
                ProductOption.SUMMARY, ProductOption.DESCRIPTION, ProductOption.GALLERY, ProductOption.CATEGORIES,
                ProductOption.REVIEW, ProductOption.PROMOTIONS, ProductOption.CLASSIFICATION,
                ProductOption.VARIANT_FULL, ProductOption.DELIVERY_PROMOTIONAL_MESSAGE, ProductOption.DELIVERY_ZONE);
        assertThat(storeNumberCaptor.getValue()).isNull();
    }

    @Test
    public void testFetchProductDataWithoutPopulateAll() {
        final ArgumentCaptor<List> optionsCaptor = ArgumentCaptor.forClass(List.class);
        final ArgumentCaptor<String> storeNumberCaptor = ArgumentCaptor.forClass(String.class);
        final ArgumentCaptor<TargetProductModel> productModelCaptor = ArgumentCaptor.forClass(TargetProductModel.class);
        given(productFacade.getBulkyBoardProductForOptions(productModelCaptor.capture(), optionsCaptor.capture(),
                storeNumberCaptor.capture()))
                        .willReturn(targetProductData);
        willDoNothing().given(productPageController).populateProductData(targetProductData, mockModel, request);
        willDoNothing().given(productPageController).populateProductStockFromVariants(targetProductData, mockModel);

        final TargetProductData productData = productPageController.fetchProductData(productModel, mockModel, request,
                null, false);
        assertThat(productData).isEqualTo(targetProductData);
        assertThat(productModelCaptor.getValue()).isEqualTo(productModel);
        assertThat(optionsCaptor.getValue()).containsExactly(ProductOption.BASIC, ProductOption.PRICE,
                ProductOption.SUMMARY, ProductOption.DESCRIPTION, ProductOption.GALLERY, ProductOption.CATEGORIES,
                ProductOption.REVIEW, ProductOption.PROMOTIONS, ProductOption.CLASSIFICATION,
                ProductOption.VARIANT_FULL, ProductOption.DELIVERY_PROMOTIONAL_MESSAGE, ProductOption.DELIVERY_ZONE,
                ProductOption.STOCK, ProductOption.CONSOLIDATED_STOCK);
        assertThat(storeNumberCaptor.getValue()).isNull();
    }

    @Test
    public void testFetchProductDataWithoutPopulateAllUsingFluent() {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isFluentStockLookupEnabled();
        final ArgumentCaptor<List> optionsCaptor = ArgumentCaptor.forClass(List.class);
        final ArgumentCaptor<String> storeNumberCaptor = ArgumentCaptor.forClass(String.class);
        final ArgumentCaptor<TargetProductModel> productModelCaptor = ArgumentCaptor.forClass(TargetProductModel.class);
        given(productFacade.getBulkyBoardProductForOptions(productModelCaptor.capture(), optionsCaptor.capture(),
                storeNumberCaptor.capture()))
                        .willReturn(targetProductData);
        final List<ProductData> variants = mock(List.class);
        given(productFacade.getFluentStock(productModel, null)).willReturn(variants);
        willDoNothing().given(productPageController).populateProductData(targetProductData, mockModel, request);
        willDoNothing().given(productPageController).populateProductStockFromVariants(targetProductData, mockModel);

        final TargetProductData productData = productPageController.fetchProductData(productModel, mockModel, request,
                null, false);
        assertThat(productData).isEqualTo(targetProductData);
        verify(mockModel).addAttribute("variantsStock", variants);
        assertThat(productModelCaptor.getValue()).isEqualTo(productModel);
        assertThat(optionsCaptor.getValue()).containsExactly(ProductOption.BASIC, ProductOption.PRICE,
                ProductOption.SUMMARY, ProductOption.DESCRIPTION, ProductOption.GALLERY, ProductOption.CATEGORIES,
                ProductOption.REVIEW, ProductOption.PROMOTIONS, ProductOption.CLASSIFICATION,
                ProductOption.VARIANT_FULL, ProductOption.DELIVERY_PROMOTIONAL_MESSAGE, ProductOption.DELIVERY_ZONE);
        assertThat(storeNumberCaptor.getValue()).isNull();
    }


    @Test
    public void testPopulateTargetProductListerDataReactPdpFeatureSwitchOn() {
        final TargetProductListerData targetProductListerData = new TargetProductListerData();

        final List<ProductOption> productOptions = new ArrayList<>();
        productOptions.add(ProductOption.BASIC);
        productOptions.add(ProductOption.VARIANT_FULL);
        productOptions.add(ProductOption.DELIVERY_MODE_AVAILABILITY);
        productOptions.add(ProductOption.DELIVERY_PROMOTIONAL_MESSAGE);
        productOptions.add(ProductOption.BULKY_PRODUCT_DETAIL);
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isFeatureEnabled(UI_REACT_PDP_CAROUSEL);
        productPageController.populateProductListerData(mockModel, targetProductListerData);
        verify(mockModel).addAttribute("targetProductListerData",
                JsonConversionUtil.convertToJsonString(targetProductListerData));
    }

    @Test
    public void testProductDetailsTargetProductListerDataReactPdpFeatureSwitchOff()
            throws CMSItemNotFoundException, UnsupportedEncodingException {
        final TargetProductListerData targetProductListerData = null;

        final List<ProductOption> productOptions = new ArrayList<>();
        productOptions.add(ProductOption.BASIC);
        productOptions.add(ProductOption.VARIANT_FULL);
        productOptions.add(ProductOption.DELIVERY_MODE_AVAILABILITY);
        productOptions.add(ProductOption.DELIVERY_PROMOTIONAL_MESSAGE);
        productOptions.add(ProductOption.BULKY_PRODUCT_DETAIL);

        willReturn(Boolean.FALSE).given(mockTargetFeatureSwitchFacade).isFeatureEnabled(UI_REACT_PDP_CAROUSEL);
        given(targetProductListerFacade.getBaseProductDataByCode(PRODUCT_CODE, productOptions))
                .willReturn(targetProductListerData);

        willReturn(null).given(productPageController).checkRequestUrl(request, httpresponse, null);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        willDoNothing().given(productPageController).populateAfterPayModal(mockModel);
        willDoNothing().given(productPageController).populateCustomerTotalInterestNumberForPrd(null, listerData);

        productPageController.productDetail(PRODUCT_CODE, mockModel, request, httpresponse, "bulkyBoardStoreNumber");

        verify(productPageController, never()).populateProductListerData(mockModel, targetProductListerData);
        verify(targetProductListerFacade, never()).getBaseProductDataByCode(PRODUCT_CODE, productOptions);
        verify(mockModel, never()).addAttribute("targetProductListerData",
                JsonConversionUtil.convertToJsonString(targetProductListerData));
        assertThat(targetProductListerData).isNull();
    }

    /**
     * Test case to extract environment data where feature switch is ON. Method to test afterpayconfig environment data
     * in following condition: 1. Afterpay is enabled 2. UI PDP carousel is enabled.
     */
    @Test
    public void testPopulateTargetProductListerDataDeliveryPromotionalMessage() {

        final TargetProductListerData targetProductListerData = new TargetProductListerData();
        targetProductListerData.setProductPromotionalDeliverySticker("Delivery sticker message");
        targetProductListerData.setProductPromotionalDeliveryText("Delivery text message");

        final List<ProductOption> productOptions = new ArrayList<>();
        productOptions.add(ProductOption.BASIC);
        productOptions.add(ProductOption.VARIANT_FULL);
        productOptions.add(ProductOption.DELIVERY_MODE_AVAILABILITY);
        productOptions.add(ProductOption.DELIVERY_PROMOTIONAL_MESSAGE);
        productOptions.add(ProductOption.BULKY_PRODUCT_DETAIL);

        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isFeatureEnabled(UI_REACT_PDP_CAROUSEL);
        productPageController.populateProductListerData(mockModel, targetProductListerData);
        verify(mockModel).addAttribute("targetProductListerData",
                JsonConversionUtil.convertToJsonString(targetProductListerData));
        assertThat(targetProductListerData.getProductPromotionalDeliverySticker())
                .isEqualTo("Delivery sticker message");
        assertThat(targetProductListerData.getProductPromotionalDeliveryText())
                .isEqualTo("Delivery text message");


    }

    @Test
    public void testPopulateTargetProductListerDataDeliveryNullPromotionalMessage() {

        final TargetProductListerData targetProductListerData = new TargetProductListerData();

        final List<ProductOption> productOptions = new ArrayList<>();
        productOptions.add(ProductOption.BASIC);
        productOptions.add(ProductOption.VARIANT_FULL);
        productOptions.add(ProductOption.DELIVERY_MODE_AVAILABILITY);
        productOptions.add(ProductOption.DELIVERY_PROMOTIONAL_MESSAGE);
        productOptions.add(ProductOption.BULKY_PRODUCT_DETAIL);
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isFeatureEnabled(UI_REACT_PDP_CAROUSEL);
        productPageController.populateProductListerData(mockModel, targetProductListerData);
        verify(mockModel).addAttribute("targetProductListerData",
                JsonConversionUtil.convertToJsonString(targetProductListerData));

    }


    /**
     * Method to test afterpayconfig environment data in following condition: 1. Afterpay is enabled 2. UI PDP carousel
     * is enabled.
     */

    @Test
    public void testPopulateEnvironmentDataFeatureSwitchOn() {

        final PriceData minimumAmount = new PriceData();
        minimumAmount.setCurrencyIso("AUD");
        minimumAmount.setValue(new BigDecimal(10.00000));
        minimumAmount.setPriceType(PriceDataType.BUY);
        minimumAmount.setFormattedValue("$10.00");

        final PriceData maximumAmount = new PriceData();
        maximumAmount.setCurrencyIso("AUD");
        maximumAmount.setValue(new BigDecimal(2000.00000));
        maximumAmount.setPriceType(PriceDataType.BUY);
        maximumAmount.setFormattedValue("$2000.00");

        final AfterpayConfigData afterpayConfigData = new AfterpayConfigData();
        afterpayConfigData.setPaymentType("PAY_BY_INSTALLMENT");
        afterpayConfigData.setDescription("Pay over time");
        afterpayConfigData.setMinimumAmount(minimumAmount);
        afterpayConfigData.setMaximumAmount(maximumAmount);


        given(afterpayConfigFacade.getAfterpayConfig()).willReturn(afterpayConfigData);
        given(sharedConfigFacade.getConfigByCode(TgtCoreConstants.Config.PREODER_DEPOSIT_AMOUNT)).willReturn("10");
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isAfterpayEnabled();
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_CAROUSEL);
        productPageController.populateEnvironmentData(mockModel, null);
        verify(mockModel).addAttribute(eq("environmentData"), environmentDataCaptor.capture());
        final String environmentDataJSON = environmentDataCaptor.getValue();
        final EnvironmentData capturedEnvironmentData = new Gson().fromJson(environmentDataJSON, EnvironmentData.class);

        verify(afterpayConfigFacade).getAfterpayConfig();
        verify(sharedConfigFacade).getConfigByCode(TgtCoreConstants.Config.PREODER_DEPOSIT_AMOUNT);

        assertThat(capturedEnvironmentData).isNotNull();
        assertThat(capturedEnvironmentData.getPreOrderDeposit()).isEqualTo(BigDecimal.TEN);
        assertThat(capturedEnvironmentData.getAfterpayConfigData().getDescription()).isEqualTo(null);
        assertThat(capturedEnvironmentData.getAfterpayConfigData().getPaymentType()).isEqualTo(null);
        verifyPriceData(minimumAmount, capturedEnvironmentData.getAfterpayConfigData().getMinimumAmount());
        verifyPriceData(maximumAmount, capturedEnvironmentData.getAfterpayConfigData().getMaximumAmount());


    }

    /**
     * Method to test population of PreOrder Deposit
     * 
     */
    @Test
    public void testPopulateEnvironmentDataWithNullDeposit() {

        given(sharedConfigFacade.getConfigByCode(TgtCoreConstants.Config.PREODER_DEPOSIT_AMOUNT)).willReturn(null);
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isAfterpayEnabled();
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_CAROUSEL);
        productPageController.populateEnvironmentData(mockModel, null);
        verify(mockModel).addAttribute(eq("environmentData"),
                environmentDataCaptor.capture());
        final String environmentDataJSON = environmentDataCaptor.getValue();
        final EnvironmentData capturedEnvironmentData = new Gson().fromJson(environmentDataJSON, EnvironmentData.class);
        verify(sharedConfigFacade).getConfigByCode(TgtCoreConstants.Config.PREODER_DEPOSIT_AMOUNT);
        assertThat(capturedEnvironmentData).isNotNull();
        assertThat(capturedEnvironmentData.getPreOrderDeposit()).isEqualTo(BigDecimal.ZERO);
    }


    /**
     * Method to test population of bazaarVoiceApiUri
     * 
     */
    @Test
    public void testPopulateEnvironmentDataBazaarVoiceApiUri() {

        ReflectionTestUtils.setField(productPageController, "bazaarVoiceApiUri",
                "https://targetaustralia.ugc.bazaarvoice.com/bvstaging/test.js");
        productPageController.populateEnvironmentData(mockModel, null);
        final ArgumentCaptor<String> argumentCaptor = ArgumentCaptor
                .forClass(String.class);
        verify(mockModel).addAttribute(eq("environmentData"), argumentCaptor.capture());
        final String environmentDataJSON = argumentCaptor.getValue();
        final EnvironmentData capturedEnvironmentData = new Gson().fromJson(environmentDataJSON, EnvironmentData.class);
        assertThat(capturedEnvironmentData.getBazaarVoiceApiUri())
                .isEqualTo("https://targetaustralia.ugc.bazaarvoice.com/bvstaging/test.js");
    }

    /**
     * Method to test population of fqdn
     * 
     */
    @Test
    public void testPopulateEnvironmentDataFqdn() {
        ReflectionTestUtils.setField(productPageController, "fqdn",
                "https://www.target.com.au");
        productPageController.populateEnvironmentData(mockModel, null);
        final ArgumentCaptor<String> argumentCaptor = ArgumentCaptor
                .forClass(String.class);
        verify(mockModel).addAttribute(Mockito.eq("environmentData"), argumentCaptor.capture());
        final String environmentDataJSON = argumentCaptor.getValue();
        final EnvironmentData capturedEnvironmentData = new Gson().fromJson(environmentDataJSON, EnvironmentData.class);
        assertThat(capturedEnvironmentData.getFqdn())
                .isEqualTo("https://www.target.com.au");
    }

    /**
     * Method to test population of addThisClientId in EnvironmentData json response for PDP like and share component.
     * This case will return a value for addThisClientId if it is configured.
     */
    @Test
    public void testPopulateEnvironmentDataAddThisPublicId() {
        ReflectionTestUtils.setField(productPageController, "addThisClientId",
                "ra-515231cc169b7103");
        productPageController.populateEnvironmentData(mockModel, null);
        verify(mockModel).addAttribute(eq("environmentData"), environmentDataCaptor.capture());
        final EnvironmentData capturedEnvironmentData = new Gson().fromJson(environmentDataCaptor.getValue(),
                EnvironmentData.class);
        assertThat(capturedEnvironmentData.getAddThisClientId()).isNotEmpty()
                .isEqualTo("ra-515231cc169b7103");
    }

    /**
     * Method to test population of addThisClientId in EnvironmentData json response for PDP like and share component.
     * This case will return null if no value configured for addThisClientId.
     *
     */
    @Test
    public void testPopulateEnvironmentDataAddThisPublicIdIsNull() {
        productPageController.populateEnvironmentData(mockModel, null);
        verify(mockModel).addAttribute(eq("environmentData"), environmentDataCaptor.capture());
        final EnvironmentData capturedEnvironmentData = new Gson().fromJson(environmentDataCaptor.getValue(),
                EnvironmentData.class);
        assertThat(capturedEnvironmentData.getAddThisClientId()).isNull();
    }

    /**
     * Method to test population of addThisContainer in EnvironmentData json response for PDP like and share component.
     * This case will return a value of addThisContainer if it's configured.
     *
     */
    @Test
    public void testPopulateEnvironmentDataAddThisClassSelector() {
        ReflectionTestUtils.setField(productPageController, "addThisContainer",
                "addthis_inline_share_toolbox");
        productPageController.populateEnvironmentData(mockModel, null);
        verify(mockModel).addAttribute(eq("environmentData"), environmentDataCaptor.capture());
        final EnvironmentData capturedEnvironmentData = new Gson().fromJson(environmentDataCaptor.getValue(),
                EnvironmentData.class);
        assertThat(capturedEnvironmentData.getAddThisContainer()).isNotEmpty()
                .isEqualTo("addthis_inline_share_toolbox");
    }

    /**
     * Method to test population of addThisContainer in EnvironmentData json response for PDP like and share component.
     * This will return null for addThisContainer if no value configured.
     *
     */
    @Test
    public void testPopulateEnvironmentDataAddThisClassSelectorIsNull() {
        productPageController.populateEnvironmentData(mockModel, null);
        verify(mockModel).addAttribute(eq("environmentData"), environmentDataCaptor.capture());
        final EnvironmentData capturedEnvironmentData = new Gson().fromJson(environmentDataCaptor.getValue(),
                EnvironmentData.class);
        assertThat(capturedEnvironmentData.getAddThisContainer()).isNull();
    }

    /**
     * Method to test population of bulky board with valid data for kiosk
     *
     */
    @Test
    public void testPopulateEnvironmentDataBulkyBoard() {
        given(mockSalesApplicationFacade.getCurrentSalesApplication()).willReturn(SalesApplication.KIOSK);
        final TargetPointOfServiceData bulkyBoardStore = mock(TargetPointOfServiceData.class);
        given(bulkyBoardStore.getStoreNumber()).willReturn(Integer.valueOf(5098));
        given(bulkyBoardStore.getName()).willReturn("Frankston");
        productPageController.populateEnvironmentData(mockModel, bulkyBoardStore);
        final ArgumentCaptor<String> argumentCaptor = ArgumentCaptor
                .forClass(String.class);
        verify(mockModel).addAttribute(Mockito.eq("environmentData"), argumentCaptor.capture());
        final String environmentDataJSON = argumentCaptor.getValue();
        final EnvironmentData capturedEnvironmentData = new Gson().fromJson(environmentDataJSON, EnvironmentData.class);
        assertThat(capturedEnvironmentData.getBbStoreName())
                .isEqualTo("Frankston");
        assertThat(capturedEnvironmentData.getBbStoreNumber())
                .isEqualTo(5098);
    }

    /**
     * Method to test population of bulky board with invalid data for kiosk
     * 
     */
    @Test
    public void testPopulateEnvironmentDataBulkyBoardNoStoreNumber() {
        given(mockSalesApplicationFacade.getCurrentSalesApplication()).willReturn(SalesApplication.KIOSK);
        final TargetPointOfServiceData bulkyBoardStore = mock(TargetPointOfServiceData.class);
        productPageController.populateEnvironmentData(mockModel, null);
        verify(bulkyBoardStore, never()).getStoreNumber();
        verify(bulkyBoardStore, never()).getName();
    }

    /**
     * Method to test population of bulky board with invalid data for web
     * 
     */
    @Test
    public void testPopulateEnvironmentDataBulkyBoardWebNoStoreNumber() {
        given(mockSalesApplicationFacade.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        final TargetPointOfServiceData bulkyBoardStore = mock(TargetPointOfServiceData.class);
        productPageController.populateEnvironmentData(mockModel, null);
        verify(bulkyBoardStore, never()).getStoreNumber();
        verify(bulkyBoardStore, never()).getName();
    }

    /**
     * Method to test population of bulky board with valid data for web
     * 
     */
    @Test
    public void testPopulateEnvironmentDataBulkyBoardWebStoreNumber() {
        given(mockSalesApplicationFacade.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        final TargetPointOfServiceData bulkyBoardStore = mock(TargetPointOfServiceData.class);
        given(bulkyBoardStore.getStoreNumber()).willReturn(Integer.valueOf(5098));
        given(bulkyBoardStore.getName()).willReturn("Frankston");
        productPageController.populateEnvironmentData(mockModel, bulkyBoardStore);
        verify(bulkyBoardStore,
                never()).getStoreNumber();
        verify(bulkyBoardStore, never()).getName();
    }

    /**
     * Method to test afterpayconfig data in following condition: 1. Afterpay is disabled 2. UI PDP carousel is enabled.
     * Test to check AfterpayConfigData if Afterpay is not configured.
     */
    @Test
    public void testPopulateEnvironmentDataNoAfterpayConfig() {

        final EnvironmentData environmentData = new EnvironmentData();
        environmentData.setPreOrderDeposit(new BigDecimal(10));
        given(afterpayConfigFacade.getAfterpayConfig()).willReturn(null);
        given(sharedConfigFacade.getConfigByCode(TgtCoreConstants.Config.PREODER_DEPOSIT_AMOUNT)).willReturn("10");
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isAfterpayEnabled();
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_CAROUSEL);

        productPageController.populateEnvironmentData(mockModel, null);

        verify(afterpayConfigFacade).getAfterpayConfig();
        verify(mockModel).addAttribute("environmentData",
                JsonConversionUtil.convertToJsonString(environmentData));

    }

    /**
     * Method to test afterpayconfig environment data in following condition: 1. Afterpay is enabled 2. UI PDP carousel
     * is disabled.
     */

    @Test
    public void testProductDetailWithUIPDPCarouselEnabled()
            throws UnsupportedEncodingException, CMSItemNotFoundException {
        final AfterpayConfigData afterpayConfigData = mock(AfterpayConfigData.class);
        given(targetProductListerFacade.getBaseProductDataByCode(PRODUCT_CODE, productOptions())).willReturn(
                mockTargetProductListerData);
        given(mockTargetProductListerData.getBaseProductCode()).willReturn("P1000");
        given(afterpayConfigFacade.getAfterpayConfig()).willReturn(afterpayConfigData);
        willReturn(Boolean.FALSE).given(mockTargetFeatureSwitchFacade).isAfterpayEnabled();
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_CAROUSEL);

        willReturn(null).given(productPageController).checkRequestUrl(request, httpresponse, null);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        willDoNothing().given(productPageController).populateAfterPayModal(mockModel);
        willDoNothing().given(productPageController).populateCustomerTotalInterestNumberForPrd(null, listerData);

        productPageController.productDetail(PRODUCT_CODE, mockModel, request, httpresponse,
                "bulkyBoardStoreNumber");

        verify(productPageController).populateEnvironmentData(mockModel, null);
    }


    /**
     * Method to test afterpayconfig environment data in following condition: 1. Afterpay is enabled 2. UI PDP carousel
     * is disabled.
     * 
     * @throws CMSItemNotFoundException
     * @throws UnsupportedEncodingException
     */
    @Test
    public void testProductDetailWithAfterpayEnabled()
            throws UnsupportedEncodingException, CMSItemNotFoundException {
        verifyEnvironementDataForAfterpayConfig(Boolean.TRUE, Boolean.FALSE);
    }

    /**
     * Method to test afterpayconfig environment data in following condition: 1. Afterpay is disabled 2. UI PDP carousel
     * is disabled.
     * 
     * @throws CMSItemNotFoundException
     * @throws UnsupportedEncodingException
     */

    @Test
    public void testProductDetailWithUIPDPCarouselDisabled()
            throws UnsupportedEncodingException, CMSItemNotFoundException {
        verifyEnvironementDataForAfterpayConfig(Boolean.FALSE, Boolean.FALSE);
    }



    /**
     * Method to check for kiosk mode if feature switch is ON.
     * 
     * @throws UnsupportedEncodingException
     * @throws CMSItemNotFoundException
     */

    @Test
    public void testProductDetailWithPDPCarouselEnabledWithKioskMode()
            throws CMSItemNotFoundException, UnsupportedEncodingException {
        verifyEnvironmentDataForKioskMode(Boolean.TRUE, SalesApplication.KIOSK);
    }

    /**
     * Method to check for NON-kiosk mode if feature switch is ON.
     */

    @Test
    public void testProductDetailWithPDPCarouselEnabledAndNonKioskMode()
            throws UnsupportedEncodingException, CMSItemNotFoundException {
        verifyEnvironmentDataForKioskMode(Boolean.TRUE, SalesApplication.WEB);
    }

    /**
     * Method to verify environment data id UI React PDP Carousel feature switch is OFF.
     * 
     * @throws UnsupportedEncodingException
     * @throws CMSItemNotFoundException
     */
    public void testProductDetailWithPDPCarouselDisabled()
            throws UnsupportedEncodingException, CMSItemNotFoundException {
        willReturn(Boolean.FALSE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_CAROUSEL);
        willReturn(null).given(productPageController).checkRequestUrl(request, httpresponse, null);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);


        productPageController.productDetail(PRODUCT_CODE, mockModel, request, httpresponse,
                "bulkyBoardStoreNumber");

        verify(productPageController, never()).populateEnvironmentData(mockModel, null);

        verify(mockSalesApplicationFacade, never()).getCurrentSalesApplication();
    }

    /**
     * Method to verify AfterpayConfig environment data based on feature switch ON or OFF
     * 
     * @throws UnsupportedEncodingException
     * @throws CMSItemNotFoundException
     */
    private void verifyEnvironementDataForAfterpayConfig(final Boolean isAfterpayEnabled,
            final Boolean isUIReactPDPCarouselEnabled)
            throws UnsupportedEncodingException, CMSItemNotFoundException {
        final AfterpayConfigData afterpayConfigData = mock(AfterpayConfigData.class);

        given(afterpayConfigFacade.getAfterpayConfig()).willReturn(afterpayConfigData);
        willReturn(isAfterpayEnabled).given(mockTargetFeatureSwitchFacade).isAfterpayEnabled();
        willReturn(isUIReactPDPCarouselEnabled).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_CAROUSEL);

        willReturn(null).given(productPageController).checkRequestUrl(request, httpresponse, null);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        willDoNothing().given(productPageController).populateAfterPayModal(mockModel);

        productPageController.productDetail(PRODUCT_CODE, mockModel, request, httpresponse,
                "bulkyBoardStoreNumber");

        verify(productPageController, never()).populateEnvironmentData(mockModel, null);
    }

    /**
     * Method to verify Kiosk environment data based on feature switch ON or OFF
     * 
     * @param featureSwitch
     * @param salesApplication
     * @throws UnsupportedEncodingException
     * @throws CMSItemNotFoundException
     */
    private void verifyEnvironmentDataForKioskMode(final Boolean featureSwitch, final SalesApplication salesApplication)
            throws UnsupportedEncodingException, CMSItemNotFoundException {
        given(targetProductListerFacade.getBaseProductDataByCode(PRODUCT_CODE, productOptions())).willReturn(
                mockTargetProductListerData);
        given(mockTargetProductListerData.getBaseProductCode()).willReturn("P1000");
        willReturn(featureSwitch).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_CAROUSEL);
        willReturn(null).given(productPageController).checkRequestUrl(request, httpresponse, null);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        willDoNothing().given(productPageController).populateCustomerTotalInterestNumberForPrd(null, listerData);

        given(mockSalesApplicationFacade.getCurrentSalesApplication()).willReturn(salesApplication);

        productPageController.productDetail(PRODUCT_CODE, mockModel, request, httpresponse,
                "bulkyBoardStoreNumber");

        verify(productPageController).populateEnvironmentData(mockModel, null);

        verify(mockSalesApplicationFacade).getCurrentSalesApplication();
    }


    /**
     * This test will verify to set max Threshold interest and total interest will never set when UI react PDP carousel
     * feature switch is OFF
     * 
     * @throws UnsupportedEncodingException
     * @throws CMSItemNotFoundException
     */
    @Test
    public void testProductDetailsWithProductListerDataUIReactPDPCaroselOff()
            throws UnsupportedEncodingException, CMSItemNotFoundException {

        final TargetProductListerData targetProdListerData = mock(TargetProductListerData.class);

        willReturn(Boolean.FALSE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_CAROUSEL);

        willReturn(null).given(productPageController).checkRequestUrl(request, httpresponse, null);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        willDoNothing().given(productPageController).populateAfterPayModal(mockModel);

        productPageController.productDetail(PRODUCT_CODE, mockModel, request, httpresponse, "bulkyBoardStoreNumber");
        verify(productPageController, never()).populateProductListerData(mockModel, targetProdListerData);
        verify(targetProductListerFacade, never()).getBaseProductDataByCode(PRODUCT_CODE, productOptions());
        verify(targetProdListerData, never()).setTotalInterest("50");
        verify(targetProdListerData, never()).setMaxThresholdInterest(200);
    }

    /**
     * This method will verify to add shop the look to model data if UI react PDP Carousel feature switch is ON. Here UI
     * react PDP detail panel feature switch is OFF. This method will never populate shop the look model data to model
     * object.
     * 
     * @throws CMSItemNotFoundException
     * @throws UnsupportedEncodingException
     * @throws ENEQueryException
     * @throws TargetEndecaException
     */
    @Test
    public void testProductDetailForShopTheLookDataWithUICarouselFeatureSwitchOn()
            throws CMSItemNotFoundException, UnsupportedEncodingException, TargetEndecaException, ENEQueryException {

        noShopTheLookData(Boolean.TRUE, Boolean.FALSE);
    }

    /**
     * This method will verify to add shop the look to model data if UI react PDP Carousel feature switch is OFF. Here
     * UI react PDP detail panel feature switch is ON. This method will never populate shop the look model data to model
     * object.
     * 
     * @throws CMSItemNotFoundException
     * @throws UnsupportedEncodingException
     * @throws ENEQueryException
     * @throws TargetEndecaException
     */
    @Test
    public void testProductDetailForShopTheLookDataWithDetailPanelFeatureSwitchOn()
            throws CMSItemNotFoundException, UnsupportedEncodingException, TargetEndecaException, ENEQueryException {
        noShopTheLookData(Boolean.FALSE, Boolean.TRUE);
    }

    /**
     * This method will verify to add shop the look to model data if UI react PDP Carousel feature switch is ON. Here UI
     * react PDP detail panel feature switch is ON. This method will populate shop the look model data to model object
     * if Look details data object present.
     * 
     * @throws CMSItemNotFoundException
     * @throws UnsupportedEncodingException
     */
    @Test
    public void testProductDetailForShopTheLookDataPresentWithFeatureSwitchesOn()
            throws CMSItemNotFoundException, UnsupportedEncodingException {

        checkForShopTheLookData();
        lookDetailslistObject.add(lookDetails1);
        given(lookDetails1.getName()).willReturn("SummerCollection");
        given(lookDetails1.getDescription()).willReturn("Amazing summer wears");
        given(targetLookPageFacade.getActiveLooksForProduct(productModel, PRODUCT_LOOKS_MAX))
                .willReturn(lookDetailslistObject);
        willDoNothing().given(productPageController).populateCustomerTotalInterestNumberForPrd(null, listerData);
        productPageController.productDetail(PRODUCT_CODE, mockModel, request, httpresponse, "bulkyBoardStoreNumber");
        verify(productPageController).populateShopTheLookData(mockModel, productModel);
        verify(mockModel).addAttribute("shopTheLookData",
                JsonConversionUtil.convertToJsonString(lookDetailslistObject));

    }

    /**
     * This method will verify to add shop the look to model data if UI react PDP Carousel feature switch is ON. Here UI
     * react PDP detail panel feature switch is ON. This method will populate empty shop the look model data to model
     * object if Look details data object present.
     * 
     * @throws CMSItemNotFoundException
     * @throws UnsupportedEncodingException
     */
    @Test
    public void testProductDetailForShopTheLookDataAbsentWithFeatureSwitchesOn()
            throws CMSItemNotFoundException, UnsupportedEncodingException {

        checkForShopTheLookData();
        given(targetLookPageFacade.getActiveLooksForProduct(productModel, PRODUCT_LOOKS_MAX))
                .willReturn(lookDetailslistObject);

        productPageController.productDetail(PRODUCT_CODE, mockModel, request, httpresponse, "bulkyBoardStoreNumber");

        verify(productPageController).populateShopTheLookData(mockModel, productModel);

        assertThat(lookDetailslistObject).isEmpty();
    }

    private List populateDeliveryTYpes() {
        final List deliveryTypes = new ArrayList<String>();
        deliveryTypes.add(TgtCoreConstants.DELIVERY_TYPE.CC);
        deliveryTypes.add(TgtCoreConstants.DELIVERY_TYPE.ED);
        deliveryTypes.add(TgtCoreConstants.DELIVERY_TYPE.HD);
        deliveryTypes.add(TgtCoreConstants.DELIVERY_TYPE.CONSOLIDATED_STORES_SOH);
        return deliveryTypes;
    }

    private TargetProductListerData mockTargetProductListerData(final TargetProductListerData listerData,
            final boolean haveSizeVariants) {
        final List colourVariants = new ArrayList<TargetVariantProductListerData>();
        final TargetVariantProductListerData colourVariant1 = mock(TargetVariantProductListerData.class);
        final TargetVariantProductListerData colourVariant2 = mock(TargetVariantProductListerData.class);
        given(colourVariant1.getCode()).willReturn("color1");
        given(colourVariant2.getCode()).willReturn("color2");
        colourVariants.add(colourVariant1);
        colourVariants.add(colourVariant2);
        given(listerData.getCode()).willReturn(PRODUCT_CODE);
        given(listerData.getTargetVariantProductListerData()).willReturn(colourVariants);
        if (haveSizeVariants) {
            final TargetVariantProductListerData sizeVariant1 = mock(TargetVariantProductListerData.class);
            final TargetVariantProductListerData sizeVariant2 = mock(TargetVariantProductListerData.class);
            final TargetVariantProductListerData sizeVariant3 = mock(TargetVariantProductListerData.class);
            final TargetVariantProductListerData sizeVariant4 = mock(TargetVariantProductListerData.class);
            given(sizeVariant1.getCode()).willReturn("size1");
            given(sizeVariant1.getSizePosition()).willReturn(Integer.valueOf(5));
            given(sizeVariant2.getCode()).willReturn("size2");
            given(sizeVariant2.getSizePosition()).willReturn(Integer.valueOf(6));
            given(sizeVariant3.getCode()).willReturn("size3");
            given(sizeVariant3.getSizePosition()).willReturn(Integer.valueOf(5));
            given(sizeVariant4.getCode()).willReturn("size4");
            given(sizeVariant4.getSizePosition()).willReturn(Integer.valueOf(6));
            final List sizeVariantsList1 = new ArrayList<TargetVariantProductListerData>();
            final List sizeVariantsList2 = new ArrayList<TargetVariantProductListerData>();
            sizeVariantsList1.add(sizeVariant1);
            sizeVariantsList1.add(sizeVariant2);
            sizeVariantsList2.add(sizeVariant3);
            sizeVariantsList2.add(sizeVariant4);
            given(colourVariant1.getVariants()).willReturn(sizeVariantsList1);
            given(colourVariant2.getVariants()).willReturn(sizeVariantsList2);

        }
        return listerData;
    }

    /**
     * 
     * @throws UnsupportedEncodingException
     */
    private void checkForShopTheLookData() throws UnsupportedEncodingException {
        final List<ProductOption> productOptions = new ArrayList<>();
        productOptions.add(ProductOption.BASIC);
        productOptions.add(ProductOption.VARIANT_FULL);
        productOptions.add(ProductOption.DELIVERY_MODE_AVAILABILITY);
        productOptions.add(ProductOption.DELIVERY_PROMOTIONAL_MESSAGE);
        productOptions.add(ProductOption.BULKY_PRODUCT_DETAIL);
        final TargetProductListerData listerData = mock(TargetProductListerData.class);

        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_CAROUSEL);
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_DETAIL_PANEL);
        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        given(targetProductListerFacade.getBaseProductDataByCode(PRODUCT_CODE, productOptions)).willReturn(listerData);
        final List variants = TargetProductDataHelper
                .populateSellableVariantsFromListerData(listerData);
        given(
                targetStockLookupFacade.lookupStock(variants,
                        populateDeliveryTYpes(),
                        Collections.EMPTY_LIST)).willReturn(response);
        willReturn(StringUtils.EMPTY)
                .given(productPageController).checkForRedirect(request, httpresponse, productModel);

        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        willDoNothing().given(productPageController).populateProductListerData(mockModel, listerData);
        willDoNothing().given(productPageController).populateEnvironmentData(mockModel, null);
    }


    private List<ProductOption> productOptions() {
        final List<ProductOption> productOptions = new ArrayList<>();
        productOptions.add(ProductOption.BASIC);
        productOptions.add(ProductOption.VARIANT_FULL);
        productOptions.add(ProductOption.DELIVERY_MODE_AVAILABILITY);
        productOptions.add(ProductOption.DELIVERY_PROMOTIONAL_MESSAGE);
        productOptions.add(ProductOption.BULKY_PRODUCT_DETAIL);
        return productOptions;
    }



    private ProductData prepareVariantsWithStock(final boolean availableOnline, final boolean availableInStore,
            final Long inStoreStockLevel, final String productCode) {
        final TargetProductData variantWithStock = mock(TargetProductData.class);
        final StockData onlineStock = mock(StockData.class);
        final StockData storeStock = mock(StockData.class);
        if (StringUtils.isNotEmpty(productCode)) {
            given(variantWithStock.getCode()).willReturn(productCode);
        }
        if (availableOnline) {
            given(onlineStock.getStockLevelStatus()).willReturn(StockLevelStatus.INSTOCK);
        }
        if (availableInStore) {
            given(storeStock.getStockLevelStatus()).willReturn(StockLevelStatus.INSTOCK);
        }
        given(storeStock.getStockLevel()).willReturn(inStoreStockLevel);

        given(variantWithStock.getStock()).willReturn(onlineStock);
        given(variantWithStock.getConsolidatedStoreStock()).willReturn(storeStock);
        return variantWithStock;
    }

    /**
     * @throws UnsupportedEncodingException
     * @throws CMSItemNotFoundException
     * @throws ENEQueryException
     * @throws TargetEndecaException
     */

    private void noShopTheLookData(final Boolean uIReactPdpCarousel, final Boolean uIReactPdpDetail)
            throws UnsupportedEncodingException, CMSItemNotFoundException, TargetEndecaException, ENEQueryException {
        willReturn(endecaData).given(productPageController).populateEndecaSearchStateData(PRODUCT_CODE);
        willReturn(uIReactPdpCarousel).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_CAROUSEL);
        willReturn(uIReactPdpDetail).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_DETAIL_PANEL);

        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);

        willReturn(StringUtils.EMPTY)
                .given(productPageController).checkForRedirect(request, httpresponse, productModel);

        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        willReturn(Boolean.FALSE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_CAROUSEL);

        productPageController.productDetail(PRODUCT_CODE, mockModel, request, httpresponse, "bulkyBoardStoreNumber");
        verify(endecaProductQueryBuilder, never()).getQueryResults(endecaData, 1);
        verify(queryResultsHelper, never())
                .getTargetProductList(eneQueryResults);

        verify(productPageController, never()).populateShopTheLookData(mockModel, productModel);
    }

    private void verifyPriceData(final PriceData expected, final PriceData actual) {
        assertThat(actual.getCurrencyIso()).isEqualTo(expected.getCurrencyIso());
        assertThat(actual.getPriceType()).isEqualTo(expected.getPriceType());
        assertThat(actual.getValue()).isEqualTo(expected.getValue());
        assertThat(actual.getFormattedValue()).isEqualTo(expected.getFormattedValue());

    }

    @Test
    public void testproductDetailForReactWithSortingVariantPDP() {

        final TargetProductListerData targetProductListerData = mockTargetProductListerData(
                mockTargetProductListerData,
                true);
        productPageController.sortListerDataSizeVariants(targetProductListerData);
        assertThat(targetProductListerData).isNotNull();
        assertThat(targetProductListerData.getTargetVariantProductListerData()).isNotEmpty();
        for (final TargetVariantProductListerData listerColourVariant : targetProductListerData
                .getTargetVariantProductListerData()) {
            for (final TargetVariantProductListerData listerSizeVariant : listerColourVariant.getVariants()) {
                assertThat(listerSizeVariant.getSizePosition().intValue())
                        .isIn(Arrays.asList(Integer.valueOf(5), Integer.valueOf(6)));
            }
        }
    }


    @Test
    public void testProductDetailWithUIPDPCarouselEnabledVerifySchemaDataAndTotalInterest()
            throws UnsupportedEncodingException, CMSItemNotFoundException, TargetEndecaException, ENEQueryException {
        final ArgumentCaptor<EndecaSearchStateData> searchDataCaptor = ArgumentCaptor
                .forClass(EndecaSearchStateData.class);
        final ArgumentCaptor<SchemaData> schemaDataCaptor = ArgumentCaptor.forClass(SchemaData.class);

        final TargetProductListerData endecaData = populateTargetProductListerData(true, false);
        given(targetProductListerFacade.getBaseProductDataByCode(PRODUCT_CODE, productOptions())).willReturn(
                mockTargetProductListerData);
        given(queryResultsHelper.getTargetProductList(eneQueryResults)).willReturn(
                Collections.singletonList(endecaData));
        given(endecaProductQueryBuilder.getQueryResults(searchDataCaptor.capture(), eq(1))).willReturn(eneQueryResults);
        given(mockTargetProductListerData.getBaseProductCode()).willReturn("P1000");
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_CAROUSEL);

        willReturn(null).given(productPageController).checkRequestUrl(request, httpresponse, null);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        willDoNothing().given(productPageController).populateAfterPayModal(mockModel);
        willDoNothing().given(productPageController).populateCustomerTotalInterestNumberForPrd(null, listerData);

        productPageController.productDetail(PRODUCT_CODE, mockModel, request, httpresponse,
                "bulkyBoardStoreNumber");

        verify(productPageController).populateEnvironmentData(mockModel, null);
        verify(endecaProductQueryBuilder).getQueryResults(searchDataCaptor.capture(), eq(1));
        verify(queryResultsHelper).getTargetProductList(eneQueryResults);
        verify(sharedConfigFacade)
                .getConfigByCode("urgencyToPurchase.excludedCategory");
        verify(mockModel).addAttribute(eq("schemaData"), schemaDataCaptor.capture());
        final SchemaData schemaData = schemaDataCaptor.getValue();
        verifySchemaData(schemaData);
        final EndecaSearchStateData actualValue = searchDataCaptor.getValue();
        assertThat(actualValue.getSearchTerm().size()).isEqualTo(1);
        assertThat(actualValue.getSearchTerm().get(0)).isEqualTo(PRODUCT_CODE);
        assertThat(actualValue.getRecordFilterOptions().size()).isEqualTo(2);
        assertThat(actualValue.getRecordFilterOptions().get(0)).isEqualTo(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        assertThat(actualValue.getRecordFilterOptions().get(1)).isEqualTo(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE);
        assertThat(actualValue.getNuRollupField()).isEqualTo(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);

    }


    @Test
    public void testProductDetailWithUIPDPCarouselEnabledVerifySchemaDataAsBasePrd()
            throws UnsupportedEncodingException, CMSItemNotFoundException, TargetEndecaException, ENEQueryException {
        final ArgumentCaptor<EndecaSearchStateData> searchDataCaptor = ArgumentCaptor
                .forClass(EndecaSearchStateData.class);
        final ArgumentCaptor<SchemaData> schemaDataCaptor = ArgumentCaptor.forClass(SchemaData.class);
        final TargetProductListerData pdpListerData = populateTargetProductListerData(true, false);
        pdpListerData.getTargetVariantProductListerData().get(0).setCode("P1000_red");
        final TargetProductListerData endecaData = populateTargetProductListerData(true, false);
        given(targetProductListerFacade.getBaseProductDataByCode(PRODUCT_CODE, productOptions())).willReturn(
                pdpListerData);
        given(queryResultsHelper.getTargetProductList(eneQueryResults)).willReturn(
                Collections.singletonList(endecaData));
        given(endecaProductQueryBuilder.getQueryResults(searchDataCaptor.capture(), eq(1))).willReturn(eneQueryResults);
        given(mockTargetProductListerData.getBaseProductCode()).willReturn(PRODUCT_CODE);
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_CAROUSEL);

        willReturn(null).given(productPageController).checkRequestUrl(request, httpresponse, null);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        willDoNothing().given(productPageController).populateAfterPayModal(mockModel);
        willDoNothing().given(productPageController).populateCustomerTotalInterestNumberForPrd(null, listerData);

        productPageController.productDetail(PRODUCT_CODE, mockModel, request, httpresponse,
                "bulkyBoardStoreNumber");

        verify(productPageController).populateEnvironmentData(mockModel, null);
        verify(endecaProductQueryBuilder).getQueryResults(searchDataCaptor.capture(), eq(1));
        verify(queryResultsHelper).getTargetProductList(eneQueryResults);
        verify(sharedConfigFacade)
                .getConfigByCode("urgencyToPurchase.excludedCategory");
        verify(mockModel).addAttribute(eq("schemaData"), schemaDataCaptor.capture());
        final SchemaData schemaData = schemaDataCaptor.getValue();
        verifySchemaData(schemaData);
        final EndecaSearchStateData actualValue = searchDataCaptor.getValue();
        assertThat(actualValue.getSearchTerm().size()).isEqualTo(1);
        assertThat(actualValue.getSearchTerm().get(0)).isEqualTo("P1000_red");
        assertThat(actualValue.getRecordFilterOptions().size()).isEqualTo(2);
        assertThat(actualValue.getRecordFilterOptions().get(0)).isEqualTo(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        assertThat(actualValue.getRecordFilterOptions().get(1)).isEqualTo(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE);
        assertThat(actualValue.getNuRollupField()).isEqualTo(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);

    }



    @Test
    public void testProductDetailWithUIPDPCarouselOffVerifySchemaDataAsBasePrd()
            throws UnsupportedEncodingException, CMSItemNotFoundException, TargetEndecaException, ENEQueryException {
        final ArgumentCaptor<EndecaSearchStateData> searchDataCaptor = ArgumentCaptor
                .forClass(EndecaSearchStateData.class);
        final ArgumentCaptor<SchemaData> schemaDataCaptor = ArgumentCaptor.forClass(SchemaData.class);
        final TargetProductListerData pdpListerData = populateTargetProductListerData(true, false);
        pdpListerData.getTargetVariantProductListerData().get(0).setCode("P1000_red");
        final TargetProductListerData endecaData = populateTargetProductListerData(true, false);
        given(targetProductListerFacade.getBaseProductDataByCode(PRODUCT_CODE, productOptions())).willReturn(
                pdpListerData);
        given(queryResultsHelper.getTargetProductList(eneQueryResults)).willReturn(
                Collections.singletonList(endecaData));
        given(endecaProductQueryBuilder.getQueryResults(searchDataCaptor.capture(), eq(1))).willReturn(eneQueryResults);
        given(mockTargetProductListerData.getBaseProductCode()).willReturn(PRODUCT_CODE);
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_CAROUSEL);

        willReturn(null).given(productPageController).checkRequestUrl(request, httpresponse, null);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        willDoNothing().given(productPageController).populateAfterPayModal(mockModel);
        willDoNothing().given(productPageController).populateCustomerTotalInterestNumberForPrd(null, listerData);

        productPageController.productDetail(PRODUCT_CODE, mockModel, request, httpresponse,
                "bulkyBoardStoreNumber");

        verify(productPageController).populateEnvironmentData(mockModel, null);
        verify(endecaProductQueryBuilder).getQueryResults(searchDataCaptor.capture(), eq(1));
        verify(queryResultsHelper).getTargetProductList(eneQueryResults);
        verify(sharedConfigFacade)
                .getConfigByCode("urgencyToPurchase.excludedCategory");
        verify(mockModel).addAttribute(eq("schemaData"), schemaDataCaptor.capture());
        final SchemaData schemaData = schemaDataCaptor.getValue();
        verifySchemaData(schemaData);
        final EndecaSearchStateData actualValue = searchDataCaptor.getValue();
        assertThat(actualValue.getSearchTerm().size()).isEqualTo(1);
        assertThat(actualValue.getSearchTerm().get(0)).isEqualTo("P1000_red");
        assertThat(actualValue.getRecordFilterOptions().size()).isEqualTo(2);
        assertThat(actualValue.getRecordFilterOptions().get(0)).isEqualTo(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        assertThat(actualValue.getRecordFilterOptions().get(1)).isEqualTo(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE);
        assertThat(actualValue.getNuRollupField()).isEqualTo(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);

    }


    @Test
    public void testProductDetailWithUIPDPCarouselOffVerifyTotalInterest()
            throws UnsupportedEncodingException, CMSItemNotFoundException, TargetEndecaException, ENEQueryException {
        final ArgumentCaptor<EndecaSearchStateData> searchDataCaptor = ArgumentCaptor
                .forClass(EndecaSearchStateData.class);
        final TargetProductListerData endecaData = populateTargetProductListerData(true, false);
        given(queryResultsHelper.getTargetProductList(eneQueryResults)).willReturn(
                Collections.singletonList(endecaData));
        given(endecaProductQueryBuilder.getQueryResults(searchDataCaptor.capture(), eq(1))).willReturn(eneQueryResults);
        willReturn(Boolean.FALSE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_CAROUSEL);
        given(Boolean.valueOf(mockTargetFeatureSwitchFacade.isUrgenyToPurchasePDPEnabled())).willReturn(Boolean.TRUE);
        willReturn(null).given(productPageController).checkRequestUrl(request, httpresponse, null);
        willDoNothing().given(productPageController).updatePageTitle(productModel, mockModel);
        willDoNothing().given(productPageController).populateRecommendedProductsDetails(request,
                EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        willDoNothing().given(productPageController).populateAfterPayModal(mockModel);
        willDoNothing().given(productPageController).populateCustomerTotalInterestNumberForPrd(null, listerData);

        productPageController.productDetail(PRODUCT_CODE, mockModel, request, httpresponse,
                "bulkyBoardStoreNumber");

        verify(endecaProductQueryBuilder).getQueryResults(searchDataCaptor.capture(), eq(1));
        verify(queryResultsHelper).getTargetProductList(eneQueryResults);
        verify(sharedConfigFacade)
                .getConfigByCode("urgencyToPurchase.excludedCategory");
        final EndecaSearchStateData actualValue = searchDataCaptor.getValue();
        assertThat(actualValue.getSearchTerm().size()).isEqualTo(1);
        assertThat(actualValue.getSearchTerm().get(0)).isEqualTo(PRODUCT_CODE);
        assertThat(actualValue.getRecordFilterOptions().size()).isEqualTo(2);
        assertThat(actualValue.getRecordFilterOptions().get(0)).isEqualTo(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        assertThat(actualValue.getRecordFilterOptions().get(1)).isEqualTo(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE);
        assertThat(actualValue.getNuRollupField()).isEqualTo(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);

    }

    private TargetProductListerData populateTargetProductListerData(
            final boolean variants,
            final boolean priceRange) {
        final TargetProductListerData listerData = new TargetProductListerData();
        listerData.setBaseProductCode(PRODUCT_CODE);
        listerData.setTopLevelCategory("Kids Toys");
        listerData.setMaxAvailStoreQty(Integer.valueOf(199));
        listerData.setMaxAvailOnlineQty(Integer.valueOf(10));
        final PriceData price = new PriceData();
        price.setCurrencyIso("AUD");
        price.setValue(BigDecimal.TEN);
        if (priceRange) {
            final PriceRangeData range = new PriceRangeData();
            range.setMinPrice(price);
            final PriceData maxPrice = new PriceData();
            maxPrice.setCurrencyIso("AUD");
            maxPrice.setValue(BigDecimal.valueOf(100));
            range.setMaxPrice(maxPrice);
            listerData.setPriceRange(range);
        }
        else {
            listerData.setPrice(price);
        }
        if (variants) {
            final TargetVariantProductListerData variantData = new TargetVariantProductListerData();
            variantData.setProductDisplayType(ProductDisplayType.AVAILABLE_FOR_SALE);
            variantData.setNormalSaleStartDate(null);
            listerData.setTargetVariantProductListerData(Collections.singletonList(variantData));
        }

        return listerData;
    }

    private void verifySchemaData(final SchemaData schemaData) {
        assertThat(schemaData).isNotNull();
        assertThat(schemaData.getPriceRange()).isNull();
        assertThat(schemaData.getPrice().getCurrencyIso()).isEqualTo("AUD");
        assertThat(schemaData.getPrice().getValue()).isEqualTo(BigDecimal.valueOf(10));
        assertThat(schemaData.getMaxAvailQtyInStore()).isEqualTo(199);
        assertThat(schemaData.getMaxAvailQtyOnline()).isEqualTo(10);
        assertThat(schemaData.getCategoryName()).isEqualTo("Kids Toys");
        assertThat(schemaData.getProductDisplayType()).isEqualTo(ProductDisplayType.AVAILABLE_FOR_SALE);
        assertThat(schemaData.getNormalSaleDate()).isNull();
    }
}
