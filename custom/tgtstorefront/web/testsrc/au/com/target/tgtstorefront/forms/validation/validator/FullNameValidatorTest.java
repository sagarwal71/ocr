/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.i18n.I18NService;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;

import au.com.target.tgtstorefront.forms.validation.FullName;



/**
 * @author mjanarth
 *
 */
@UnitTest
public class FullNameValidatorTest {
    @Mock
    private FullName fullName;

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Mock
    private ConstraintViolationBuilder constraintViolationBuilder;

    @InjectMocks
    private final FullNameValidator fullNameValidator = new FullNameValidator() {
        {
            final MessageSource messageSourceMocked = BDDMockito.mock(MessageSource.class);
            this.messageSource = messageSourceMocked;
            final I18NService i18nServiceMocked = BDDMockito.mock(I18NService.class);
            this.i18nService = i18nServiceMocked;
        }
    };

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        BDDMockito.given(constraintValidatorContext.buildConstraintViolationWithTemplate(BDDMockito.anyString()))
                .willReturn(constraintViolationBuilder);

        BDDMockito.given(fullName.field()).willReturn(FieldTypeEnum.fullName);
        fullNameValidator.initialize(fullName);
    }

    @Test
    public void correctFullNames() {
        Assert.assertTrue(fullNameValidator.isValid("AAAAAA BBBBBB CCCCCCCC", constraintValidatorContext));
        Assert.assertTrue(fullNameValidator.isValid("Jack John LastName", constraintValidatorContext));
        Assert.assertTrue(fullNameValidator.isValid("Jack John LastNameeeeeeeeeeeeeeeeeeeeee",
                constraintValidatorContext));
        Assert.assertTrue(fullNameValidator.isValid(
                "firstNameeeeeeeeeeeeeeeeeeeeee LastNameeeeeeeeeeeeeeeeeeeee",
                constraintValidatorContext));
        Assert.assertTrue(fullNameValidator.isValid(
                "FirstNameeeeeeeeeeeee MiddleNameeeeeeeeeee LastNameeeeeeeeeeeeee",
                constraintValidatorContext));
        Assert.assertTrue(fullNameValidator.isValid("John Jacob", constraintValidatorContext));
        Assert.assertTrue(fullNameValidator.isValid("te Test", constraintValidatorContext));
        Assert.assertTrue(fullNameValidator.isValid("First Middle Last", constraintValidatorContext));
        Assert.assertTrue(fullNameValidator.isValid("M Tang", constraintValidatorContext));
        Assert.assertTrue(fullNameValidator.isValid("Warner-Tim Tyner", constraintValidatorContext));
        Assert.assertTrue(fullNameValidator.isValid("M'Tang", constraintValidatorContext));

    }

    @Test
    public void inCorrectFullNames() {
        Assert.assertFalse(fullNameValidator.isValid("A", constraintValidatorContext));
        Assert.assertFalse(fullNameValidator.isValid("A@@@@@@@@@", constraintValidatorContext));
        Assert.assertFalse(fullNameValidator.isValid("A123455", constraintValidatorContext));
        Assert.assertFalse(fullNameValidator.isValid("A+B LastName", constraintValidatorContext));
        Assert.assertFalse(fullNameValidator.isValid("'AB LastName", constraintValidatorContext));
        Assert.assertFalse(fullNameValidator.isValid("-AB LastName", constraintValidatorContext));
    }
}
