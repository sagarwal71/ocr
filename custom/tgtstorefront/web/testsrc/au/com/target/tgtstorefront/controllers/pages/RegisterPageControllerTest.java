/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.FlashMap;

import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.customer.TargetCustomerSubscriptionFacade;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionSource;
import au.com.target.tgtstorefront.forms.RegisterForm;
import au.com.target.tgtstorefront.security.AutoLoginStrategy;


/**
 * @author pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class RegisterPageControllerTest {

    private static final String FEATURE_GUEST_CHECKOUT = "checkout.allow.guest";

    @InjectMocks
    private final RegisterPageController registerPageController = new RegisterPageController();

    @Mock
    private RegisterForm registerForm;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private Model model;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private TargetCustomerSubscriptionFacade targetCustomerSubscriptionFacade;

    @Mock
    private TargetCustomerFacade customerFacade;

    @Mock
    private AutoLoginStrategy autoLoginStrategy;

    @Mock
    private HttpSessionRequestCache httpSessionRequestCache;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Before
    public void setUp() throws Exception {
        final FlashMap flashMap = new FlashMap();
        when(registerForm.getEmail()).thenReturn("TEST_EMAIL");
        when(registerForm.getTitleCode()).thenReturn("MR");
        when(registerForm.getFirstName()).thenReturn("TEST_NAME");
        when(request.getAttribute(DispatcherServlet.OUTPUT_FLASH_MAP_ATTRIBUTE)).thenReturn(flashMap);
        when(Boolean.valueOf(targetFeatureSwitchFacade.isFeatureEnabled(FEATURE_GUEST_CHECKOUT))).thenReturn(
                Boolean.FALSE);
    }

    @Test
    public void testProcessRegisterUserRequestWithOptInEmail()
            throws CMSItemNotFoundException {
        final TargetCustomerSubscriptionRequestDto subscriptionDetails = mockSubscriptionDetails();
        subscriptionDetails.setRegisterForEmail(true);
        when(registerForm.getRegisterForMail()).thenReturn(Boolean.TRUE);
        registerPageController.processRegisterUserRequest(null, registerForm, bindingResult, model, request, response);
        verify(targetCustomerSubscriptionFacade).performCustomerSubscription(Mockito.refEq(subscriptionDetails));
    }

    @Test
    public void testProcessRegisterUserRequestWithOptOutEmail()
            throws CMSItemNotFoundException {
        final TargetCustomerSubscriptionRequestDto subscriptionDetails = mockSubscriptionDetails();
        subscriptionDetails.setRegisterForEmail(false);
        when(registerForm.getRegisterForMail()).thenReturn(Boolean.FALSE);
        registerPageController.processRegisterUserRequest(null, registerForm, bindingResult, model, request, response);
        verify(targetCustomerSubscriptionFacade).performCustomerSubscription(Mockito.refEq(subscriptionDetails));
    }

    /**
     * Method to mock subscription details
     * 
     * @return TargetCustomerSubscriptionRequestDto
     */
    private TargetCustomerSubscriptionRequestDto mockSubscriptionDetails() {
        final TargetCustomerSubscriptionRequestDto subscriptionDetails = new TargetCustomerSubscriptionRequestDto();
        subscriptionDetails.setCustomerEmail("TEST_EMAIL");
        subscriptionDetails.setFirstName("TEST_NAME");
        subscriptionDetails.setTitle("MR");
        subscriptionDetails.setCustomerSubscriptionSource(TargetCustomerSubscriptionSource.WEBACCOUNT);
        return subscriptionDetails;
    }
}
