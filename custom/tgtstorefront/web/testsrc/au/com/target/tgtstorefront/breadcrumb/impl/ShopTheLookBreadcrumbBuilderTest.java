/**
 * 
 */
package au.com.target.tgtstorefront.breadcrumb.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.UrlResolver;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import au.com.target.tgtmarketing.model.TargetLookCollectionModel;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;


/**
 * @author bhuang3
 *
 */
public class ShopTheLookBreadcrumbBuilderTest {

    @Spy
    @InjectMocks
    private final ShopTheLookBreadcrumbBuilder shopTheLookBreadcrumbBuilder = new ShopTheLookBreadcrumbBuilder();

    @Mock
    private UrlResolver<TargetShopTheLookModel> targetShopTheLookUrlResolver;

    @Mock
    private UrlResolver<CategoryModel> targetCategoryModelUrlResolver;

    @Mock
    private TargetShopTheLookModel targetShopTheLookModel;

    @Mock
    private TargetLookModel targetLookModel;

    @Mock
    private TargetLookCollectionModel targetLookCollectionModel;

    @Mock
    private CategoryModel categoryModel;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(targetLookModel.getUrl()).thenReturn("/look/LOOK123456");
        Mockito.when(targetLookModel.getName()).thenReturn("kids look");
        Mockito.when(targetLookCollectionModel.getShopTheLook()).thenReturn(targetShopTheLookModel);
        Mockito.when(targetLookModel.getCollection()).thenReturn(targetLookCollectionModel);
        Mockito.when(categoryModel.getName()).thenReturn("kids");
        Mockito.when(categoryModel.getCode()).thenReturn("W123456");
        Mockito.when(targetShopTheLookModel.getName()).thenReturn("kids inspiration");
        Mockito.when(targetCategoryModelUrlResolver.resolve(categoryModel)).thenReturn("/c/kids/W123456");
        Mockito.when(targetShopTheLookUrlResolver.resolve(targetShopTheLookModel)).thenReturn(
                "/inspiration/kids/STL123456");
        Mockito.when(targetShopTheLookModel.getCategory()).thenReturn(categoryModel);
    }

    @Test
    public void testGetBreadcrumbs() {
        final List<Breadcrumb> result = shopTheLookBreadcrumbBuilder.getBreadcrumbs(targetShopTheLookModel);
        Assert.assertEquals("kids", result.get(0).getName());
        Assert.assertEquals("/c/kids/W123456", result.get(0).getUrl());
        Assert.assertEquals("kids inspiration", result.get(1).getName());
        Assert.assertEquals("/inspiration/kids/STL123456", result.get(1).getUrl());
        Assert.assertEquals("active", result.get(1).getLinkClass());
    }

    @Test
    public void testGetBreadcrumbsForLookPage() {
        final List<Breadcrumb> result = shopTheLookBreadcrumbBuilder.getBreadcrumbsForLookPage(targetLookModel);
        Assert.assertEquals("kids", result.get(0).getName());
        Assert.assertEquals("/c/kids/W123456", result.get(0).getUrl());
        Assert.assertEquals("kids inspiration", result.get(1).getName());
        Assert.assertEquals("/inspiration/kids/STL123456", result.get(1).getUrl());
        Assert.assertEquals("kids look", result.get(2).getName());
        Assert.assertEquals("/look/LOOK123456", result.get(2).getUrl());
        Assert.assertEquals("active", result.get(2).getLinkClass());
    }
}
