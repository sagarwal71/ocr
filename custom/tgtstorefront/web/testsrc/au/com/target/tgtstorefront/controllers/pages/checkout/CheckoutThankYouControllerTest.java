package au.com.target.tgtstorefront.controllers.pages.checkout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.session.SessionService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.TargetPlaceOrderFacade;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CheckoutThankYouControllerTest {

    @InjectMocks
    private final CheckoutThankYouController checkoutThankYouController = new CheckoutThankYouController();

    @Mock
    private TargetPlaceOrderFacade targetPlaceOrderFacade;

    @Mock
    private TargetCheckoutFacade targetCheckoutFacade;

    @Mock
    private CMSPageService cmsPageService;

    @Mock
    private SessionService sessionService;

    @Test
    public void testAmountPaidSoFar() {
        final Double amount = Double.valueOf(2.5d);
        final CartModel mockCartModel = mock(CartModel.class);
        given(targetPlaceOrderFacade.getAmountPaidSoFar(mockCartModel)).willReturn(amount);

        final Double amountPaid = checkoutThankYouController.getAmountPaidSoFar(mockCartModel);

        assertNotNull(amountPaid);
        assertTrue(Double.compare(amount.doubleValue(), amountPaid.doubleValue()) == 0);
    }

    @Test
    public void testIsPaidValueMatchesCartTotalWhenPaidValueLessThanCartTotal() {
        final Double amountPaidSoFar = Double.valueOf(1.5d);
        final Double cartTotal = Double.valueOf(2.5d);

        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getTotalPrice()).willReturn(cartTotal);

        final boolean isPaidValueMatchesCartTotal = checkoutThankYouController.isPaidValueMatchesCartTotal(
                mockCartModel, amountPaidSoFar);

        assertFalse(isPaidValueMatchesCartTotal);
    }

    @Test
    public void testIsPaidValueMatchesCartTotalWhenPaidValueGreaterThanCartTotal() {
        final Double amountPaidSoFar = Double.valueOf(3.5d);
        final Double cartTotal = Double.valueOf(2.5d);

        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getTotalPrice()).willReturn(cartTotal);

        final boolean isPaidValueMatchesCartTotal = checkoutThankYouController.isPaidValueMatchesCartTotal(
                mockCartModel, amountPaidSoFar);

        assertFalse(isPaidValueMatchesCartTotal);
    }

    @Test
    public void testIsPaidValueMatchesCartTotalWhenPaidValueEqualsThanCartTotal() {
        final Double amountPaidSoFar = Double.valueOf(2.5d);
        final Double cartTotal = Double.valueOf(2.5d);

        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getTotalPrice()).willReturn(cartTotal);

        final boolean isPaidValueMatchesCartTotal = checkoutThankYouController.isPaidValueMatchesCartTotal(
                mockCartModel, amountPaidSoFar);

        assertTrue(isPaidValueMatchesCartTotal);
    }

    @Test
    public void testCheckoutProblemWhenNoCart() throws CMSItemNotFoundException {
        final String cartNumber = "12345678";
        final Model model = new ExtendedModelMap();

        given(cmsPageService.getPageForLabelOrId(BDDMockito.anyString())).willReturn(null);
        given(targetCheckoutFacade.getCart()).willReturn(null);

        final String returnedView = checkoutThankYouController.checkoutProblem(model, cartNumber);

        assertEquals(ControllerConstants.Views.Pages.MultiStepCheckout.THANK_YOU_CHECK_PROBLEM_PAGE, returnedView);
        assertTrue(((Boolean)model.asMap().get("paidValueMatchesCartTotal")).booleanValue());
    }

    @Test
    public void testCheckoutProblemWhenPaidValueSoFarIsNull() throws CMSItemNotFoundException {
        final String cartNumber = "12345678";
        final Model model = new ExtendedModelMap();
        final CartModel cartModel = mock(CartModel.class);
        final Double paidValueSoFar = null;
        final Double cartTotal = Double.valueOf(5d);

        given(cmsPageService.getPageForLabelOrId(BDDMockito.anyString())).willReturn(null);
        given(targetCheckoutFacade.getCart()).willReturn(cartModel);
        given(targetPlaceOrderFacade.getAmountPaidSoFar(cartModel)).willReturn(paidValueSoFar);
        given(cartModel.getTotalPrice()).willReturn(cartTotal);

        final String returnedView = checkoutThankYouController.checkoutProblem(model, cartNumber);

        assertEquals(ControllerConstants.Views.Pages.MultiStepCheckout.THANK_YOU_CHECK_PROBLEM_PAGE, returnedView);
        assertNull(model.asMap().get("paidValueMatchesCartTotal"));
    }

    @Test
    public void testCheckoutProblemWhenPaidValueSoFarIsLessThanCartTotal() throws CMSItemNotFoundException {
        final String cartNumber = "12345678";
        final Model model = new ExtendedModelMap();
        final CartModel cartModel = mock(CartModel.class);
        final Double paidValueSoFar = Double.valueOf(2d);
        final Double cartTotal = Double.valueOf(5d);

        given(cmsPageService.getPageForLabelOrId(BDDMockito.anyString())).willReturn(null);
        given(targetCheckoutFacade.getCart()).willReturn(cartModel);
        given(targetPlaceOrderFacade.getAmountPaidSoFar(cartModel)).willReturn(paidValueSoFar);
        given(cartModel.getTotalPrice()).willReturn(cartTotal);

        final String returnedView = checkoutThankYouController.checkoutProblem(model, cartNumber);

        assertEquals(ControllerConstants.Views.Pages.MultiStepCheckout.THANK_YOU_CHECK_PROBLEM_PAGE, returnedView);
        assertNull(model.asMap().get("paidValueMatchesCartTotal"));
    }

    @Test
    public void testCheckoutProblemWhenPaidValueSoFarIsGreaterThanCartTotal() throws CMSItemNotFoundException {
        final String cartNumber = "12345678";
        final Model model = new ExtendedModelMap();
        final CartModel cartModel = mock(CartModel.class);
        final Double paidValueSoFar = Double.valueOf(6d);
        final Double cartTotal = Double.valueOf(5d);

        given(cmsPageService.getPageForLabelOrId(BDDMockito.anyString())).willReturn(null);
        given(targetCheckoutFacade.getCart()).willReturn(cartModel);
        given(targetPlaceOrderFacade.getAmountPaidSoFar(cartModel)).willReturn(paidValueSoFar);
        given(cartModel.getTotalPrice()).willReturn(cartTotal);

        final String returnedView = checkoutThankYouController.checkoutProblem(model, cartNumber);

        assertEquals(ControllerConstants.Views.Pages.MultiStepCheckout.THANK_YOU_CHECK_PROBLEM_PAGE, returnedView);
        assertNull(model.asMap().get("paidValueMatchesCartTotal"));
    }

    @Test
    public void testCheckoutProblemWhenPaidValueSoFarIsGreaterEqualsCartTotal() throws CMSItemNotFoundException {
        final String cartNumber = "12345678";
        final Model model = new ExtendedModelMap();
        final CartModel cartModel = mock(CartModel.class);
        final Double paidValueSoFar = Double.valueOf(5d);
        final Double cartTotal = Double.valueOf(5d);

        given(cmsPageService.getPageForLabelOrId(BDDMockito.anyString())).willReturn(null);
        given(targetCheckoutFacade.getCart()).willReturn(cartModel);
        given(targetPlaceOrderFacade.getAmountPaidSoFar(cartModel)).willReturn(paidValueSoFar);
        given(cartModel.getTotalPrice()).willReturn(cartTotal);

        final String returnedView = checkoutThankYouController.checkoutProblem(model, cartNumber);

        assertEquals(ControllerConstants.Views.Pages.MultiStepCheckout.THANK_YOU_CHECK_PROBLEM_PAGE, returnedView);
        assertTrue(((Boolean)model.asMap().get("paidValueMatchesCartTotal")).booleanValue());
    }

    @Test
    public void testCheckoutProblemWhenFreshCartIsAvailable() throws CMSItemNotFoundException {
        final String cartNumber = "12345678";
        final Model model = new ExtendedModelMap();
        final CartModel cartModel = mock(CartModel.class);
        final Double cartTotal = Double.valueOf(0d);

        given(cmsPageService.getPageForLabelOrId(BDDMockito.anyString())).willReturn(null);
        given(targetCheckoutFacade.getCart()).willReturn(cartModel);
        given(cartModel.getTotalPrice()).willReturn(cartTotal);

        final String returnedView = checkoutThankYouController.checkoutProblem(model, cartNumber);

        assertEquals(ControllerConstants.Views.Pages.MultiStepCheckout.THANK_YOU_CHECK_PROBLEM_PAGE, returnedView);
        assertTrue(((Boolean)model.asMap().get("paidValueMatchesCartTotal")).booleanValue());
    }
}