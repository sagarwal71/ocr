/**
 * 
 */
package au.com.target.tgtstorefront.customersegment.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.fest.assertions.Assertions;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.util.CookieGenerator;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import au.com.target.tgtfacades.user.TargetUserFacade;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CustomerSegmentCookieStrategyTest {

    @Mock
    private CookieGenerator cookieGenerator;

    @InjectMocks
    private final CustomerSegmentCookieStrategy cookieStrategy = new CustomerSegmentCookieStrategy();

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private Cookie customerSegmentCookie;

    @Mock
    private TargetUserFacade targetUserFacade;

    @Before
    public void setUp() {
        cookieStrategy.setResetPeriod("1");
    }

    @Test
    public void testSetCookieWhenCookieInRequestNull() throws JsonParseException, JsonMappingException, IOException {
        final Cookie[] cookies = {};
        BDDMockito.when(request.getCookies()).thenReturn(cookies);
        BDDMockito.when(targetUserFacade.getAllUserSegmentsForCurrentUser()).thenReturn(
                Collections.singletonList("womens"));
        cookieStrategy.setCookie(request, response);
        Mockito.verify(cookieGenerator).getCookieName();
        final ArgumentCaptor<String> cookieValue = ArgumentCaptor.forClass(String.class);
        Mockito.verify(cookieGenerator).addCookie(Mockito.eq(response), cookieValue.capture());
        Mockito.verify(targetUserFacade).getAllUserSegmentsForCurrentUser();
        Mockito.verifyNoMoreInteractions(cookieGenerator, targetUserFacade);
        final String value = cookieValue.getValue();
        Assertions.assertThat(value).isNotNull().isNotEmpty();
        final String[] cookieValues = StringUtils.split(value, CustomerSegmentCookieStrategy.SEPARATOR);
        Assertions.assertThat(cookieValues).isNotNull().isNotEmpty().hasSize(2);
        Assertions.assertThat(cookieValues[1]).isNotNull().isNotEmpty().isEqualTo("womens");
        Assertions.assertThat(cookieValues[0]).isNotNull().isNotEmpty();
    }

    @Test
    public void testSetCookieWhenCookieInRequestNullCookieValueEmpty() throws JsonParseException, JsonMappingException,
            IOException {
        final Cookie[] cookies = {};
        BDDMockito.when(request.getCookies()).thenReturn(cookies);
        BDDMockito.when(targetUserFacade.getAllUserSegmentsForCurrentUser()).thenReturn(Collections.EMPTY_LIST);
        cookieStrategy.setCookie(request, response);
        Mockito.verify(cookieGenerator).getCookieName();
        final ArgumentCaptor<String> cookieValue = ArgumentCaptor.forClass(String.class);
        Mockito.verify(cookieGenerator).addCookie(Mockito.eq(response), cookieValue.capture());
        Mockito.verify(targetUserFacade).getAllUserSegmentsForCurrentUser();
        Mockito.verifyNoMoreInteractions(cookieGenerator, targetUserFacade);
        final String value = cookieValue.getValue();
        Assertions.assertThat(value).isNotNull().isNotEmpty();
        Assertions.assertThat(value.contains(CustomerSegmentCookieStrategy.SEPARATOR)).isFalse();
    }

    @Test
    public void testSetCookieWhenCookieInRequestNotAged() {
        final Cookie[] cookies = { customerSegmentCookie };
        BDDMockito.when(customerSegmentCookie.getName()).thenReturn("tgt-seg-group");
        final String createDate = Long.toString(new Date().getTime());
        BDDMockito.when(customerSegmentCookie.getValue()).thenReturn(
                createDate + CustomerSegmentCookieStrategy.SEPARATOR + "womens");
        BDDMockito.when(cookieGenerator.getCookieName()).thenReturn("tgt-seg-group");
        BDDMockito.when(request.getCookies()).thenReturn(cookies);
        cookieStrategy.setCookie(request, response);
        Mockito.verify(cookieGenerator).getCookieName();
        Mockito.verifyNoMoreInteractions(cookieGenerator);
        Mockito.verifyZeroInteractions(targetUserFacade);
    }

    @Test
    public void testSetCookieWhenCookieInRequestAged() throws JsonParseException, JsonMappingException, IOException {
        final Cookie[] cookies = { customerSegmentCookie };
        BDDMockito.when(customerSegmentCookie.getName()).thenReturn("tgt-seg-group");
        final List<String> segList = new ArrayList<>();
        Collections.addAll(segList, "toys", "womens");
        BDDMockito.when(targetUserFacade.getAllUserSegmentsForCurrentUser())
                .thenReturn(segList);
        final Date date = DateTime.now().minusHours(2).toDate();
        final String createDate = Long.toString(date.getTime());
        BDDMockito.when(customerSegmentCookie.getValue()).thenReturn(
                createDate + CustomerSegmentCookieStrategy.SEPARATOR + "womens"
                        + CustomerSegmentCookieStrategy.SEPARATOR
                        + "toys");
        BDDMockito.when(cookieGenerator.getCookieName()).thenReturn("tgt-seg-group");
        BDDMockito.when(request.getCookies()).thenReturn(cookies);
        cookieStrategy.setCookie(request, response);
        Mockito.verify(cookieGenerator).getCookieName();
        final ArgumentCaptor<String> cookieValue = ArgumentCaptor.forClass(String.class);
        Mockito.verify(cookieGenerator).addCookie(Mockito.eq(response), cookieValue.capture());
        Mockito.verify(targetUserFacade).getAllUserSegmentsForCurrentUser();
        Mockito.verifyNoMoreInteractions(cookieGenerator, targetUserFacade);
        final String value = cookieValue.getValue();
        Assertions.assertThat(value).isNotNull().isNotEmpty();
        final String[] cookieValues = StringUtils.split(value, CustomerSegmentCookieStrategy.SEPARATOR);
        Assertions.assertThat(cookieValues).isNotNull().isNotEmpty().hasSize(3);
        Assertions.assertThat(cookieValues[1]).isEqualTo("toys");
        Assertions.assertThat(cookieValues[2]).isEqualTo("womens");
        Assertions.assertThat(cookieValues[0]).isNotNull().isNotEmpty();
    }
}
