/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.converter.AggregatedEndecaRecordToTargetProductListerConverter;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.querybuilder.EndecaProductQueryBuilder;
import au.com.target.tgtfacades.product.data.CustomerProductInfo;
import au.com.target.tgtfacades.product.data.CustomerProductsListData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.EndecaQueryResultsHelper;

import com.endeca.navigation.AggrERec;
import com.endeca.navigation.ENEQueryResults;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class RecentlyViewedProductControllerTest {

    @InjectMocks
    private RecentlyViewedProductController controller = new RecentlyViewedProductController();

    @Mock
    private EndecaProductQueryBuilder endecaProductQueryBuilder;

    @Mock
    private AggregatedEndecaRecordToTargetProductListerConverter<AggrERec, TargetProductListerData> aggrERecToProductConverter;

    @Mock
    private Model model;

    @Mock
    private HttpServletRequest request;

    @Mock
    private CustomerProductInfo productInfo;

    @Mock
    private ConfigurationService configurationService;
    @Mock
    private Configuration configuration;

    @Mock
    private EndecaQueryResultsHelper endecaQueryResultsHelper;

    @SuppressWarnings("boxing")
    @Before
    public void setUp() {
        Mockito.when(configurationService.getConfiguration()).thenReturn(configuration);
        Mockito.when(
                configurationService.getConfiguration().getInt("storefront.recentlyviewedproducts.component.size", 50))
                .thenReturn(50);
        when(productInfo.getSelectedVariantCode()).thenReturn("p1");
    }

    @Test
    public void testGetRecentlyViewedProductDetailsWithNull() {
        Assert.assertEquals(ControllerConstants.Views.Fragments.Product.RECENTLY_VIEWED_PRODUCTS,
                controller.getRecentlyViewedProductDetails(null, request, model));

        verifyZeroInteractions(endecaProductQueryBuilder);
    }

    @Test
    public void testGetRecentlyViewedProductDetails() {
        controller = spy(controller);
        final CustomerProductsListData products = createRecentlyViewedProducts();
        final EndecaSearchStateData endecaSearchStateData = mock(EndecaSearchStateData.class);
        doReturn(endecaSearchStateData).when(controller).populateEndecaSearchStateData(products);
        final List<TargetProductListerData> list = mock(List.class);
        when(endecaQueryResultsHelper.getTargetProductsListForColorVariant(any(ENEQueryResults.class)))
                .thenReturn(list);
        Assert.assertEquals(ControllerConstants.Views.Fragments.Product.RECENTLY_VIEWED_PRODUCTS,
                controller.getRecentlyViewedProductDetails(products, request, model));

        verify(endecaProductQueryBuilder).getQueryResults(endecaSearchStateData, null, 50);
        verify(model).addAttribute("productData", list);
    }

    @Test
    public void testPopulateEndecaSearchStateData() {
        final EndecaSearchStateData data = controller.populateEndecaSearchStateData(createRecentlyViewedProducts());

        assertEquals(EndecaConstants.MATCH_MODE_ANY, data.getMatchMode());
        assertEquals(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING, data.getSearchField());
        assertTrue(data.isSkipDefaultSort());
        assertTrue(data.isSkipInStockFilter());
        assertEquals(1, data.getSearchTerm().size());
        assertEquals("p1", data.getSearchTerm().get(0));
    }

    private CustomerProductsListData createRecentlyViewedProducts() {
        final CustomerProductsListData products = new CustomerProductsListData();
        final List<CustomerProductInfo> productList = new ArrayList<>();
        productList.add(productInfo);
        products.setProducts(productList);
        return products;
    }


}
