package au.com.target.tgtstorefront.controllers.cms;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.restrictions.AbstractRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.CMSDataFactory;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.CMSRestrictionService;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.product.BrandService;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtwebcore.model.cms2.components.TargetFeedbackComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.TargetFeedbackContainerComponentModel;
import au.com.target.tgtwebcore.servicelayer.data.TargetCMSRestrictionData;

import com.google.common.collect.ImmutableList;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFeedbackContainerComponentControllerTest {

    @InjectMocks
    private final TargetFeedbackContainerComponentController controller = new TargetFeedbackContainerComponentController();

    @Mock
    private CMSRestrictionService cmsRestrictionService;
    @Mock
    private TargetFeedbackContainerComponentModel component;
    @Mock
    private TargetFeedbackComponentModel feeback;
    @Mock
    private HttpServletRequest request;
    @Mock
    private Model model;
    @Mock
    private CMSDataFactory cmsDataFactory;
    @Mock
    private BrandService brandService;

    @Test
    public void testNoFeedbackToModel() {
        controller.fillModel(request, model, component);
        verifyZeroInteractions(model);
    }

    @Test
    public void testSetFeedbackToModelWithInvisibleFeedback() {
        given(component.getItems()).willReturn(ImmutableList.of(feeback));
        given(feeback.getVisible()).willReturn(Boolean.FALSE);
        controller.fillModel(request, model, component);
        verify(model).addAttribute(WebConstants.FEEDBACKS, Collections.emptyList());
    }

    @Test
    public void testSetFeedbackToModelWithRestrictedFeedback() {
        final AbstractRestrictionModel restriction = mock(AbstractRestrictionModel.class);
        final RestrictionData data = mock(RestrictionData.class);
        given(component.getItems()).willReturn(ImmutableList.of(feeback));
        given(feeback.getVisible()).willReturn(Boolean.TRUE);
        given(feeback.getRestrictions()).willReturn(ImmutableList.of(restriction));
        given(cmsDataFactory.createRestrictionData(any(String.class), any(String.class), any(String.class)))
                .willReturn(data);
        doReturn(Boolean.FALSE).when(cmsRestrictionService).evaluateCMSComponent(feeback, data);
        controller.fillModel(request, model, component);
        verify(model).addAttribute(WebConstants.FEEDBACKS, Collections.emptyList());
    }

    @Test
    public void testSetFeedbackToModelWithGoodFeedback() {
        final AbstractRestrictionModel restriction = mock(AbstractRestrictionModel.class);
        final RestrictionData data = mock(RestrictionData.class);
        given(component.getItems()).willReturn(ImmutableList.of(feeback));
        given(feeback.getVisible()).willReturn(Boolean.TRUE);
        given(feeback.getRestrictions()).willReturn(ImmutableList.of(restriction));
        given(cmsDataFactory.createRestrictionData(any(String.class), any(String.class), any(String.class)))
                .willReturn(data);
        doReturn(Boolean.TRUE).when(cmsRestrictionService).evaluateCMSComponent(feeback, data);
        controller.fillModel(request, model, component);
        verify(model).addAttribute(WebConstants.FEEDBACKS, ImmutableList.of(feeback));
    }

    @Test
    public void populateRestrictionDataWithBrand() {
        final RestrictionData data = mock(RestrictionData.class);
        final BrandModel brandModel = mock(BrandModel.class);
        given(cmsDataFactory.createRestrictionData(any(String.class), any(String.class), any(String.class)))
                .willReturn(data);
        final CatalogModel catalog = mock(CatalogModel.class);
        final CategoryModel category = mock(CategoryModel.class);
        final ProductModel product = mock(ProductModel.class);
        given(data.getCatalog()).willReturn(catalog);
        given(data.getCategory()).willReturn(category);
        given(data.getProduct()).willReturn(product);
        given(request.getAttribute("currentBrandCode")).willReturn("brand");
        given(brandService.getBrandForCode("brand")).willReturn(brandModel);
        final RestrictionData restrictionData = controller.populate(request);
        Assertions.assertThat(restrictionData).isInstanceOf(TargetCMSRestrictionData.class);
        final TargetCMSRestrictionData targetRestrictionData = (TargetCMSRestrictionData)restrictionData;
        Assertions.assertThat(targetRestrictionData.getBrand()).isEqualTo(brandModel);
        Assertions.assertThat(targetRestrictionData.getProduct()).isEqualTo(product);
        Assertions.assertThat(targetRestrictionData.getCatalog()).isEqualTo(catalog);
        Assertions.assertThat(targetRestrictionData.getCategory()).isEqualTo(category);
    }

    @Test
    public void populateRestrictionDataWithUnknownBrand() {
        final RestrictionData data = mock(RestrictionData.class);
        final BrandModel brandModel = mock(BrandModel.class);
        given(cmsDataFactory.createRestrictionData(any(String.class), any(String.class), any(String.class)))
                .willReturn(data);
        final CatalogModel catalog = mock(CatalogModel.class);
        final CategoryModel category = mock(CategoryModel.class);
        final ProductModel product = mock(ProductModel.class);
        given(data.getCatalog()).willReturn(catalog);
        given(data.getCategory()).willReturn(category);
        given(data.getProduct()).willReturn(product);
        given(request.getAttribute("currentBrandCode")).willReturn("unkown");
        given(brandService.getBrandForCode("brand")).willReturn(brandModel);
        final RestrictionData restrictionData = controller.populate(request);
        Assertions.assertThat(restrictionData).isInstanceOf(TargetCMSRestrictionData.class);
        final TargetCMSRestrictionData targetRestrictionData = (TargetCMSRestrictionData)restrictionData;
        Assertions.assertThat(targetRestrictionData.getBrand()).isNull();
        Assertions.assertThat(targetRestrictionData.getProduct()).isEqualTo(product);
        Assertions.assertThat(targetRestrictionData.getCatalog()).isEqualTo(catalog);
        Assertions.assertThat(targetRestrictionData.getCategory()).isEqualTo(category);
    }

    @Test
    public void populateRestrictionDataWithoutBrand() {
        final RestrictionData data = mock(RestrictionData.class);
        final BrandModel brandModel = mock(BrandModel.class);
        given(cmsDataFactory.createRestrictionData(any(String.class), any(String.class), any(String.class)))
                .willReturn(data);
        given(request.getAttribute("currentBrandCode")).willReturn(null);
        given(brandService.getBrandForCode("brand")).willReturn(brandModel);
        final RestrictionData restrictionData = controller.populate(request);
        Assertions.assertThat(restrictionData).isEqualTo(data);
        verifyZeroInteractions(brandService);
    }

}
