/**
 * 
 */
package au.com.target.tgtstorefront.tags;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.PromotionOrderEntryConsumedData;
import de.hybris.platform.commercefacades.product.data.PromotionResultData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author rsamuel3
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class FunctionsTest {

    @Mock
    private CartData cartData;

    @Mock
    private OrderData orderData;

    @Mock
    private PromotionResultData promoResultData;

    @Mock
    private PromotionOrderEntryConsumedData promotionEntryConsumedData;

    @Mock
    private PromotionResultData promoResultData1;

    @Mock
    private PromotionOrderEntryConsumedData promotionEntryConsumedData1;

    @Test
    public void testGetAppliedPromotionForEntryNullOrder() {
        final List<PromotionResultData> list = Functions.getAppliedPromotionForEntry(null, 0);
        Assertions.assertThat(list).isNotNull().isEmpty();
        Mockito.verifyNoMoreInteractions(orderData);
        Mockito.verifyNoMoreInteractions(promoResultData);
        Mockito.verifyNoMoreInteractions(promotionEntryConsumedData);
    }

    @Test
    public void testGetAppliedPromotionsForEntryOrderDataNullPromotions() {
        Mockito.when(orderData.getAppliedProductPromotions()).thenReturn(null);
        final List list = Functions.getAppliedPromotionForEntry(orderData, 0);
        Assertions.assertThat(list).isNotNull().isEmpty();
        Mockito.verify(orderData).getAppliedProductPromotions();
        Mockito.verifyNoMoreInteractions(orderData);
        Mockito.verifyNoMoreInteractions(promoResultData);
        Mockito.verifyNoMoreInteractions(promotionEntryConsumedData);

    }

    @Test
    public void testGetAppliedPromotionsForEntryCartDataNullPromotions() {
        Mockito.when(cartData.getAppliedProductPromotions()).thenReturn(null);
        final List list = Functions.getAppliedPromotionForEntry(orderData, 0);
        Assertions.assertThat(list).isNotNull().isEmpty();
        Mockito.verify(orderData).getAppliedProductPromotions();
        Mockito.verifyNoMoreInteractions(orderData);
        Mockito.verifyNoMoreInteractions(promoResultData);
        Mockito.verifyNoMoreInteractions(promotionEntryConsumedData);
    }

    @Test
    public void testGetAppliedPromotionsForEntryOrderDataEmptyList() {
        Mockito.when(orderData.getAppliedProductPromotions()).thenReturn(new ArrayList());
        final List list = Functions.getAppliedPromotionForEntry(orderData, 0);
        Assertions.assertThat(list).isNotNull().isEmpty();
        Mockito.verify(orderData).getAppliedProductPromotions();
        Mockito.verifyNoMoreInteractions(orderData);
        Mockito.verifyNoMoreInteractions(promoResultData);
        Mockito.verifyNoMoreInteractions(promotionEntryConsumedData);

    }

    @Test
    public void testGetAppliedPromotionsForEntryOrderDataNullConsumerEntries() {
        Mockito.when(orderData.getAppliedProductPromotions()).thenReturn(Collections.singletonList(promoResultData));
        Mockito.when(promoResultData.getConsumedEntries()).thenReturn(null);
        final List list = Functions.getAppliedPromotionForEntry(orderData, 0);
        Assertions.assertThat(list).isNotNull().isEmpty();
        Mockito.verify(orderData).getAppliedProductPromotions();
        Mockito.verify(promoResultData, Mockito.atLeastOnce()).getConsumedEntries();
        Mockito.verifyNoMoreInteractions(orderData);
        Mockito.verifyNoMoreInteractions(promoResultData);
        Mockito.verifyNoMoreInteractions(promotionEntryConsumedData);
    }

    @Test
    public void testGetAppliedPromotionsForEntryOrderDataEmptyConsumerEntries() {
        Mockito.when(orderData.getAppliedProductPromotions()).thenReturn(Collections.singletonList(promoResultData));
        Mockito.when(promoResultData.getConsumedEntries()).thenReturn(new ArrayList());
        final List list = Functions.getAppliedPromotionForEntry(orderData, 0);
        Assertions.assertThat(list).isNotNull().isEmpty();
        Mockito.verify(orderData).getAppliedProductPromotions();
        Mockito.verify(promoResultData, Mockito.atLeastOnce()).getConsumedEntries();
        Mockito.verifyNoMoreInteractions(orderData);
        Mockito.verifyNoMoreInteractions(promoResultData);
        Mockito.verifyNoMoreInteractions(promotionEntryConsumedData);
    }

    @Test
    public void testGetAppliedPromotionsForEntryOrderDataOrderEntryNull() {
        Mockito.when(orderData.getAppliedProductPromotions()).thenReturn(Collections.singletonList(promoResultData));
        Mockito.when(promoResultData.getConsumedEntries()).thenReturn(
                Collections.singletonList(promotionEntryConsumedData));
        Mockito.when(promotionEntryConsumedData.getOrderEntryNumber()).thenReturn(null);
        final List list = Functions.getAppliedPromotionForEntry(orderData, 0);
        Assertions.assertThat(list).isNotNull().isEmpty();
        Mockito.verify(orderData).getAppliedProductPromotions();
        Mockito.verify(promoResultData, Mockito.atLeastOnce()).getConsumedEntries();
        Mockito.verify(promotionEntryConsumedData, Mockito.atLeastOnce()).getOrderEntryNumber();
        Mockito.verifyNoMoreInteractions(orderData);
        Mockito.verifyNoMoreInteractions(promoResultData);
        Mockito.verifyNoMoreInteractions(promotionEntryConsumedData);

    }

    @Test
    public void testGetAppliedPromotionsForEntryOrderDataNotsameEntry() {
        Mockito.when(orderData.getAppliedProductPromotions()).thenReturn(Collections.singletonList(promoResultData));
        Mockito.when(promoResultData.getConsumedEntries()).thenReturn(
                Collections.singletonList(promotionEntryConsumedData));
        Mockito.when(promotionEntryConsumedData.getOrderEntryNumber()).thenReturn(Integer.valueOf(1));
        final List list = Functions.getAppliedPromotionForEntry(orderData, 0);
        Assertions.assertThat(list).isNotNull().isEmpty();
        Mockito.verify(orderData).getAppliedProductPromotions();
        Mockito.verify(promoResultData, Mockito.atLeastOnce()).getConsumedEntries();
        Mockito.verify(promotionEntryConsumedData, Mockito.atLeastOnce()).getOrderEntryNumber();
        Mockito.verifyNoMoreInteractions(orderData);
        Mockito.verifyNoMoreInteractions(promoResultData);
        Mockito.verifyNoMoreInteractions(promotionEntryConsumedData);
    }

    @Test
    public void testGetAppliedPromotionsForEntryOrderDataSameEntry() {
        Mockito.when(orderData.getAppliedProductPromotions()).thenReturn(Collections.singletonList(promoResultData));
        Mockito.when(promoResultData.getConsumedEntries()).thenReturn(
                Collections.singletonList(promotionEntryConsumedData));
        Mockito.when(promotionEntryConsumedData.getOrderEntryNumber()).thenReturn(Integer.valueOf(0));
        final List list = Functions.getAppliedPromotionForEntry(orderData, 0);
        Assertions.assertThat(list).isNotNull().containsOnly(promoResultData);
        Mockito.verify(orderData).getAppliedProductPromotions();
        Mockito.verify(promoResultData, Mockito.atLeastOnce()).getConsumedEntries();
        Mockito.verify(promotionEntryConsumedData, Mockito.atLeastOnce()).getOrderEntryNumber();
        Mockito.verifyNoMoreInteractions(orderData);
        Mockito.verifyNoMoreInteractions(promoResultData);
        Mockito.verifyNoMoreInteractions(promotionEntryConsumedData);

    }

    @Test
    public void testGetAppliedPromotionsForEntryOrderDataSamePromoTwice() {
        final List<PromotionResultData> promoList = new ArrayList();
        promoList.add(promoResultData);
        promoList.add(promoResultData);
        Mockito.when(orderData.getAppliedProductPromotions()).thenReturn(promoList);
        Mockito.when(promoResultData.getConsumedEntries()).thenReturn(
                Collections.singletonList(promotionEntryConsumedData));
        Mockito.when(promotionEntryConsumedData.getOrderEntryNumber()).thenReturn(Integer.valueOf(0));
        final List list = Functions.getAppliedPromotionForEntry(orderData, 0);
        Assertions.assertThat(list).isNotNull().contains(promoResultData, promoResultData);
        Mockito.verify(orderData).getAppliedProductPromotions();
        Mockito.verify(promoResultData, Mockito.atLeast(2)).getConsumedEntries();
        Mockito.verify(promotionEntryConsumedData, Mockito.atLeast(2)).getOrderEntryNumber();
        Mockito.verifyNoMoreInteractions(orderData);
        Mockito.verifyNoMoreInteractions(promoResultData);
        Mockito.verifyNoMoreInteractions(promotionEntryConsumedData);

    }

    @Test
    public void testGetAppliedPromotionsForEntryOrderDataSameEntryTwice() {
        final List<PromotionResultData> promoList = new ArrayList();
        promoList.add(promoResultData);
        Mockito.when(orderData.getAppliedProductPromotions()).thenReturn(promoList);
        final List<PromotionOrderEntryConsumedData> promoEntryList = new ArrayList<>();
        promoEntryList.add(promotionEntryConsumedData);
        promoEntryList.add(promotionEntryConsumedData);
        Mockito.when(promoResultData.getConsumedEntries()).thenReturn(
                promoEntryList);
        Mockito.when(promotionEntryConsumedData.getOrderEntryNumber()).thenReturn(Integer.valueOf(0));
        final List list = Functions.getAppliedPromotionForEntry(orderData, 0);
        Assertions.assertThat(list).isNotNull().contains(promoResultData);
        Mockito.verify(orderData).getAppliedProductPromotions();
        Mockito.verify(promoResultData, Mockito.atLeast(1)).getConsumedEntries();
        Mockito.verify(promotionEntryConsumedData, Mockito.atLeast(2)).getOrderEntryNumber();
        Mockito.verifyNoMoreInteractions(orderData);
        Mockito.verifyNoMoreInteractions(promoResultData);
        Mockito.verifyNoMoreInteractions(promotionEntryConsumedData);

    }

    @Test
    public void testGetAppliedPromotionsForEntryOrderDataDiffEntry() {
        final List<PromotionResultData> promoList = new ArrayList();
        promoList.add(promoResultData);
        promoList.add(promoResultData1);
        Mockito.when(orderData.getAppliedProductPromotions()).thenReturn(promoList);
        Mockito.when(promoResultData.getConsumedEntries()).thenReturn(
                Collections.singletonList(promotionEntryConsumedData));
        Mockito.when(promoResultData1.getConsumedEntries()).thenReturn(
                Collections.singletonList(promotionEntryConsumedData1));
        Mockito.when(promotionEntryConsumedData.getOrderEntryNumber()).thenReturn(Integer.valueOf(0));
        Mockito.when(promotionEntryConsumedData1.getOrderEntryNumber()).thenReturn(Integer.valueOf(1));
        final List list = Functions.getAppliedPromotionForEntry(orderData, 0);
        Assertions.assertThat(list).isNotNull().contains(promoResultData);
        Mockito.verify(orderData).getAppliedProductPromotions();
        Mockito.verify(promoResultData, Mockito.atLeastOnce()).getConsumedEntries();
        Mockito.verify(promoResultData1, Mockito.atLeastOnce()).getConsumedEntries();
        Mockito.verify(promotionEntryConsumedData, Mockito.atLeastOnce()).getOrderEntryNumber();
        Mockito.verify(promotionEntryConsumedData1, Mockito.atLeastOnce()).getOrderEntryNumber();
        Mockito.verifyNoMoreInteractions(orderData);
        Mockito.verifyNoMoreInteractions(promoResultData);
        Mockito.verifyNoMoreInteractions(promotionEntryConsumedData);
        Mockito.verifyNoMoreInteractions(promoResultData1);
        Mockito.verifyNoMoreInteractions(promotionEntryConsumedData1);

    }

}
