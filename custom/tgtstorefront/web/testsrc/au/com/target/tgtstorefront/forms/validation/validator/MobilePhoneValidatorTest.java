/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.Locale;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;

import au.com.target.tgtstorefront.forms.validation.MobilePhone;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
@UnitTest
public class MobilePhoneValidatorTest {

    @Mock
    private MobilePhone mobilePhone;

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Mock
    private ConstraintViolationBuilder constraintViolationBuilder;

    @Mock
    private MessageSource mockMessageSource;

    @Mock
    private I18NService mockI18nService;

    @InjectMocks
    private final MobilePhoneValidator mobilePhoneValidator = new MobilePhoneValidator();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        BDDMockito.given(constraintValidatorContext.buildConstraintViolationWithTemplate(BDDMockito.anyString()))
                .willReturn(constraintViolationBuilder);
        mobilePhoneValidator.initialize(mobilePhone);

        BDDMockito.given(mockI18nService.getCurrentLocale()).willReturn(Locale.ENGLISH);
        BDDMockito.given(mockMessageSource.getMessage("field.mobilePhone", null, Locale.ENGLISH)).willReturn(
                "Mobile Number");
    }

    @Ignore
    @Test
    public void correctMobilePhoneNumber() {
        Assert.assertTrue(mobilePhoneValidator.isValidCommon("", constraintValidatorContext));
        Assert.assertTrue(mobilePhoneValidator.isValidCommon("0412345678", constraintValidatorContext));
        Assert.assertTrue(mobilePhoneValidator.isValidCommon("0509876543", constraintValidatorContext));
        Assert.assertTrue(mobilePhoneValidator.isValidCommon("+61509876543", constraintValidatorContext));
        Assert.assertTrue(mobilePhoneValidator.isValidCommon("+61062770978", constraintValidatorContext));
        Assert.assertTrue(mobilePhoneValidator.isValidCommon("+61409876543", constraintValidatorContext));
    }

    @Test
    public void inCorrectMobilePhoneNumber() {
        Assert.assertFalse(mobilePhoneValidator.isValidCommon("0609876543", constraintValidatorContext));
        Assert.assertFalse(mobilePhoneValidator.isValidCommon("0709876543", constraintValidatorContext));
        Assert.assertFalse(mobilePhoneValidator.isValidCommon("+610509+876543", constraintValidatorContext));
        Assert.assertFalse(mobilePhoneValidator.isValidCommon("null", constraintValidatorContext));
    }
}
