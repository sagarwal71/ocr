package au.com.target.tgtstorefront.controllers.pages;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.util.PageTitleResolver;
import org.junit.Assert;


/**
 * @author rsparks1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class JsonCmsContentControllerTest {

    @InjectMocks
    private final JsonCmsContentController controller = new JsonCmsContentController();

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpSession session;

    @Mock
    private CMSPageService mockCmsPageService;

    @Mock
    private Model mockModel;

    @Mock
    private ContentPageModel contentPage;

    @Mock
    private PageTitleResolver pageTitleResolver;

    @Before
    public void setUp() {
        when(request.getSession()).thenReturn(session);
    }

    @Test
    public void itShouldRedirectToJsonCmsContentPage() throws UnsupportedEncodingException, CMSItemNotFoundException {
        BDDMockito.given(mockCmsPageService.getPageForLabelOrId("/json-cms/Test")).willReturn(contentPage);
        BDDMockito.given(pageTitleResolver.resolveContentPageTitle(BDDMockito.anyString()))
                .willReturn(BDDMockito.anyString());
        final String redirect = controller.getJsonCmsPage("Test", mockModel);
        Assert.assertEquals(redirect, ControllerConstants.Views.Pages.JsonCmsContent.JSON_CMS_CONTENT_PAGE);
    }

    @Test
    public void itShouldRedirectToJsonCmsContentPageOnError()
            throws UnsupportedEncodingException, CMSItemNotFoundException {
        BDDMockito.given(mockCmsPageService.getPageForLabelOrId("/json-cms/Test"))
                .willThrow(new CMSItemNotFoundException(""));
        final String redirect = controller.getJsonCmsPage("Test", mockModel);
        Assert.assertEquals(redirect, ControllerConstants.Views.Pages.JsonCmsContent.JSON_CMS_CONTENT_PAGE);
    }

}