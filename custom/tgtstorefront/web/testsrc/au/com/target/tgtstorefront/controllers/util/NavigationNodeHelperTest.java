/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtstorefront.navigation.megamenu.MegaMenuColumn;
import au.com.target.tgtstorefront.navigation.megamenu.MegaMenuDepartment;
import au.com.target.tgtstorefront.navigation.megamenu.MegaMenuEntry;
import au.com.target.tgtstorefront.navigation.megamenu.MegaMenuSection;
import au.com.target.tgtwebcore.enums.NavigationNodeStyleEnum;
import au.com.target.tgtwebcore.enums.NavigationNodeTypeEnum;


/**
 * @author mjanarth
 *
 */
@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class NavigationNodeHelperTest {

    @Mock
    private Configuration mockConfiguration;

    @Mock
    private ConfigurationService mockConfigurationService;

    @Mock
    private CMSNavigationNodeModel mockChildNavigationNode;

    @Mock
    private MegaMenuSection megaMenuSection;

    @InjectMocks
    private final NavigationNodeHelper navigationNodeHelper = new NavigationNodeHelper();

    private NavigationNodeHelper helper;

    @Mock
    private Converter<CategoryModel, CategoryData> mockCategoryConverter;
    @Mock
    private UrlResolver<CategoryData> mockCategoryDataUrlResolver;

    @SuppressWarnings("boxing")
    @Before
    public void setup() {
        helper = Mockito.spy(navigationNodeHelper);
        given(mockConfiguration.getInt(Mockito.eq("megamenu.max.column"), Mockito.anyInt())).willReturn(5);
        given(mockConfigurationService.getConfiguration()).willReturn(mockConfiguration);
    }

    @Test
    public void testBuildColumnWithNoEntries() {

        doReturn(megaMenuSection).when(helper)
                .buildSection(BDDMockito.any(CMSNavigationNodeModel.class));
        when(mockChildNavigationNode.getType()).thenReturn(NavigationNodeTypeEnum.SECTION);
        willReturn(Boolean.TRUE).given(mockChildNavigationNode).isVisible();
        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));

        final List<MegaMenuEntry> entries = new ArrayList<>();
        given(megaMenuSection.getEntries()).willReturn(
                entries);
        final MegaMenuColumn column = helper.buildColumn(mockNavigationNode);
        assertThat(column).isNotNull();
        assertThat(column.getSections()).isNotNull().hasSize(1);

    }

    @Test
    public void testBuildColumnWithTwoSections() {
        final CMSNavigationNodeModel mockChildNavigationNode1 = mock(CMSNavigationNodeModel.class);
        final MegaMenuEntry megaMenuEntry = mock(MegaMenuEntry.class);
        doReturn(megaMenuSection).when(helper)
                .buildSection(BDDMockito.any(CMSNavigationNodeModel.class));

        given(mockChildNavigationNode1.getType()).willReturn(NavigationNodeTypeEnum.SECTION);
        willReturn(Boolean.TRUE).given(mockChildNavigationNode1).isVisible();

        final CMSNavigationNodeModel mockChildNavigationNode2 = mock(CMSNavigationNodeModel.class);
        given(mockChildNavigationNode2.getType()).willReturn(NavigationNodeTypeEnum.SECTION);
        willReturn(Boolean.TRUE).given(mockChildNavigationNode2).isVisible();

        final List<CMSNavigationNodeModel> childList = new ArrayList<>();
        childList.add(mockChildNavigationNode1);
        childList.add(mockChildNavigationNode2);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getChildren()).willReturn(childList);
        given(mockNavigationNode.getTitle()).willReturn("Column Name");


        final List<MegaMenuEntry> entries = new ArrayList<>();
        entries.add(megaMenuEntry);
        given(megaMenuSection.getEntries()).willReturn(entries);
        final MegaMenuColumn column = helper.buildColumn(mockNavigationNode);
        assertThat(column).isNotNull();
        assertThat(column.getSections()).isNotNull().hasSize(2);
        assertThat(column.getTitle()).isEqualTo("Column Name");
    }

    @Test
    public void testBuildColumnWithOneEntry() {

        final MegaMenuEntry megaMenuEntry = mock(MegaMenuEntry.class);
        doReturn(megaMenuSection).when(helper).buildSection(BDDMockito.any(CMSNavigationNodeModel.class));
        given(mockChildNavigationNode.getType()).willReturn(NavigationNodeTypeEnum.SECTION);
        willReturn(Boolean.TRUE).given(mockChildNavigationNode).isVisible();
        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));
        given(mockNavigationNode.getTitle()).willReturn("Column Name");
        final List<MegaMenuEntry> entries = new ArrayList<>();
        entries.add(megaMenuEntry);
        given(megaMenuSection.getEntries()).willReturn(
                entries);
        final MegaMenuColumn column = helper.buildColumn(mockNavigationNode);
        assertThat(column).isNotNull();
        assertThat(column.getSections()).isNotNull().hasSize(1);
        assertThat(column.getTitle()).isEqualTo("Column Name");

    }


    @Test
    public void testBuildColumnWithNoSections() {
        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.HIGHLIGHTEDCOLUMN);
        final MegaMenuColumn column = navigationNodeHelper.buildColumn(mockNavigationNode);
        assertThat(column).isNotNull();
        assertThat(column.getSections()).isNotNull();
        assertThat(column.getSections()).isEmpty();
        assertThat(column.getStyle()).isEqualTo(NavigationNodeStyleEnum.HIGHLIGHTEDCOLUMN.getCode());
    }

    @Test
    public void testBuildColumnWithNotVisibleSection() {
        given(mockChildNavigationNode.getType()).willReturn(NavigationNodeTypeEnum.SECTION);
        willReturn(Boolean.FALSE).given(mockChildNavigationNode).isVisible();

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.HIGHLIGHTEDCOLUMN);
        final MegaMenuColumn column = navigationNodeHelper.buildColumn(mockNavigationNode);
        assertThat(column).isNotNull();
        assertThat(column.getSections()).isNotNull();
        assertThat(column.getSections()).isEmpty();
        assertThat(column.getStyle()).isEqualTo(NavigationNodeStyleEnum.HIGHLIGHTEDCOLUMN.getCode());
    }

    @Test
    public void testBuildColumnWithVisibleNotSection() {
        given(mockChildNavigationNode.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        willReturn(Boolean.TRUE).given(mockChildNavigationNode).isVisible();
        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.HIGHLIGHTEDCOLUMN);
        final MegaMenuColumn column = navigationNodeHelper.buildColumn(mockNavigationNode);
        assertThat(column).isNotNull();
        assertThat(column.getSections()).isNotNull();
        assertThat(column.getSections()).isEmpty();
        assertThat(column.getStyle()).isEqualTo(NavigationNodeStyleEnum.HIGHLIGHTEDCOLUMN.getCode());
    }

    @Test
    public void testBuildColumnWithSingleSection() {
        final MegaMenuEntry megaMenuEntry = mock(MegaMenuEntry.class);
        doReturn(megaMenuSection).when(helper)
                .buildSection(BDDMockito.any(CMSNavigationNodeModel.class));
        given(mockChildNavigationNode.getType()).willReturn(NavigationNodeTypeEnum.SECTION);
        willReturn(Boolean.TRUE).given(mockChildNavigationNode).isVisible();
        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));
        final List<MegaMenuEntry> entries = new ArrayList<>();
        entries.add(megaMenuEntry);
        given(megaMenuSection.getEntries()).willReturn(
                entries);
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.HIGHLIGHTEDCOLUMN);
        final MegaMenuColumn column = helper.buildColumn(mockNavigationNode);
        assertThat(column).isNotNull();
        assertThat(column.getSections()).isNotNull().hasSize(1);
        assertThat(column.getSections().get(0).getStyle()).isNull();
        assertThat(column.getStyle()).isEqualTo(NavigationNodeStyleEnum.HIGHLIGHTEDCOLUMN.getCode());
    }

    @Test
    public void testBuildColumnWithSingleSectionWithSaleStyle() {
        final CMSNavigationNodeModel mockNavigationNode = createMockColumnWithSingleSectionWithStyle(
                NavigationNodeStyleEnum.SALELINK);
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.HIGHLIGHTEDCOLUMN);
        final MegaMenuColumn column = navigationNodeHelper.buildColumn(mockNavigationNode);
        assertThat(column).isNotNull();
        assertThat(column.getSections()).isNotNull().hasSize(1);
        assertThat(column.getSections().get(0).getStyle()).isEqualTo(NavigationNodeStyleEnum.SALELINK.getCode());
        assertThat(column.getStyle()).isEqualTo(NavigationNodeStyleEnum.HIGHLIGHTEDCOLUMN.getCode());
    }


    private CMSNavigationNodeModel createMockColumnWithSingleSectionWithStyle(final NavigationNodeStyleEnum style) {
        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        final CMSNavigationNodeModel sectionNode = mock(CMSNavigationNodeModel.class);
        final CMSNavigationNodeModel itemNode = mock(CMSNavigationNodeModel.class);
        given(sectionNode.getType()).willReturn(NavigationNodeTypeEnum.SECTION);
        willReturn(Boolean.TRUE).given(sectionNode).isVisible();
        given(sectionNode.getStyle()).willReturn(style);
        given(itemNode.getType()).willReturn(null);
        willReturn(Boolean.TRUE).given(itemNode).isVisible();
        given(sectionNode.getChildren()).willReturn(
                Collections.singletonList(itemNode));
        given(mockNavigationNode.getChildren()).willReturn(Collections.singletonList(sectionNode));
        return mockNavigationNode;
    }

    @Test
    public void testBuildSectionWithNoEntries() {
        final String sectionTitle = "Section Title";
        final String sectionUrl = "/section/url";
        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(sectionTitle);
        doReturn(sectionUrl).when(helper).getNavigationNodeLink(mockNavigationNode);
        final MegaMenuSection section = helper.buildSection(mockNavigationNode);
        assertThat(section).isNotNull();
        assertThat(section.getTitle()).isEqualTo(sectionTitle);
        assertThat(section.getLink()).isEqualTo(sectionUrl);
        assertThat(section.getEntries()).isNotNull();
        assertThat(section.getEntries()).isEmpty();
    }

    @Test
    public void testBuildSectionWithNotVisibleEntry() {
        final String sectionTitle = "Section Title";
        final String sectionUrl = "/section/url";
        given(mockChildNavigationNode.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        willReturn(Boolean.FALSE).given(mockChildNavigationNode).isVisible();
        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(sectionTitle);
        given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));
        doReturn(sectionUrl).when(helper).getNavigationNodeLink(mockNavigationNode);
        final MegaMenuSection section = helper.buildSection(mockNavigationNode);
        assertThat(section).isNotNull();
        assertThat(section.getTitle()).isEqualTo(sectionTitle);
        assertThat(section.getLink()).isEqualTo(sectionUrl);
        assertThat(section.getEntries()).isNotNull();
        assertThat(section.getEntries()).isEmpty();
    }

    @Test
    public void testBuildSectionWithSingleEntry() {
        final String sectionTitle = "Section Title";
        final String sectionUrl = "/section/url";
        willReturn(Boolean.TRUE).given(mockChildNavigationNode).isVisible();
        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(sectionTitle);
        given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));
        doReturn(sectionUrl).when(helper).getNavigationNodeLink(mockNavigationNode);
        final MegaMenuSection section = helper.buildSection(mockNavigationNode);
        assertThat(section).isNotNull();
        assertThat(section.getTitle()).isEqualTo(sectionTitle);
        assertThat(section.getLink()).isEqualTo(sectionUrl);
        assertThat(section.getEntries()).isNotNull().hasSize(1);
    }

    @Test
    public void testBuildSectionWithTwoEntries() {
        final String sectionTitle = "Section Title";
        final String sectionUrl = "/section/url";

        final CMSNavigationNodeModel mockChildNavigationNode1 = mock(CMSNavigationNodeModel.class);
        willReturn(Boolean.TRUE).given(mockChildNavigationNode1).isVisible();

        final CMSNavigationNodeModel mockChildNavigationNode2 = mock(CMSNavigationNodeModel.class);
        willReturn(Boolean.TRUE).given(mockChildNavigationNode2).isVisible();

        final List<CMSNavigationNodeModel> childList = new ArrayList<>();
        childList.add(mockChildNavigationNode1);
        childList.add(mockChildNavigationNode2);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(sectionTitle);
        given(mockNavigationNode.getChildren()).willReturn(childList);
        doReturn(sectionUrl).when(helper).getNavigationNodeLink(mockNavigationNode);
        final MegaMenuSection section = helper.buildSection(mockNavigationNode);
        assertThat(section).isNotNull();
        assertThat(section.getTitle()).isEqualTo(sectionTitle);
        assertThat(section.getLink()).isEqualTo(sectionUrl);
        assertThat(section.getEntries()).isNotNull().hasSize(2);
    }

    @Test
    public void testCreateMegaMenuEntryWithNoEntries() {
        final String navigationNodeTitle = "Entry Title";

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(navigationNodeTitle);
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.SALELINK);
        final MegaMenuEntry megaMenuEntry = navigationNodeHelper.createMegaMenuEntry(mockNavigationNode);

        assertThat(megaMenuEntry).isNotNull();
        assertThat(megaMenuEntry.getText()).isEqualTo(navigationNodeTitle);
        assertThat(StringUtils.isBlank(megaMenuEntry.getLinkUrl())).isTrue();
        assertThat(StringUtils.isBlank(megaMenuEntry.getLinkUrl())).isTrue();
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageUrl())).isTrue();
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageAltText())).isTrue();
        assertThat(megaMenuEntry.getStyle()).isEqualTo(NavigationNodeStyleEnum.SALELINK.getCode());
    }

    @Test
    public void testCreateMegaMenuEntryWithCategoryEntry() {
        final String navigationNodeTitle = "Entry Title";
        final String categoryName = "Category Name";
        final String categoryUrl = "/category/url";

        final CategoryModel mockCategoryModel = mock(CategoryModel.class);
        final CategoryData mockCategoryData = mock(CategoryData.class);
        given(mockCategoryData.getName()).willReturn(categoryName);

        given(mockCategoryConverter.convert(mockCategoryModel)).willReturn(mockCategoryData);
        given(mockCategoryDataUrlResolver.resolve(mockCategoryData)).willReturn(categoryUrl);

        final CMSNavigationEntryModel mockNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockNavigationEntry.getItem()).willReturn(mockCategoryModel);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(navigationNodeTitle);
        given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.SALELINK);

        final MegaMenuEntry megaMenuEntry = navigationNodeHelper.createMegaMenuEntry(mockNavigationNode);

        assertThat(megaMenuEntry).isNotNull();
        assertThat(megaMenuEntry.getText()).isEqualTo(navigationNodeTitle);
        assertThat(megaMenuEntry.getLinkUrl()).isEqualTo(categoryUrl);
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageUrl())).isTrue();
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageAltText())).isTrue();
        assertThat(megaMenuEntry.getStyle()).isEqualTo(NavigationNodeStyleEnum.SALELINK.getCode());
    }

    @Test
    public void testCreateMegaMenuEntryWithCategoryEntryWithCategoryName() {
        final String navigationNodeTitle = "";
        final String categoryName = "Category Name";
        final String categoryUrl = "/category/url";

        final CategoryModel mockCategoryModel = mock(CategoryModel.class);
        final CategoryData mockCategoryData = mock(CategoryData.class);
        given(mockCategoryData.getName()).willReturn(categoryName);

        given(mockCategoryConverter.convert(mockCategoryModel)).willReturn(mockCategoryData);
        given(mockCategoryDataUrlResolver.resolve(mockCategoryData)).willReturn(categoryUrl);

        final CMSNavigationEntryModel mockNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockNavigationEntry.getItem()).willReturn(mockCategoryModel);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(navigationNodeTitle);
        given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.SALELINK);

        final MegaMenuEntry megaMenuEntry = navigationNodeHelper.createMegaMenuEntry(mockNavigationNode);

        assertThat(megaMenuEntry).isNotNull();
        assertThat(megaMenuEntry.getText()).isEqualTo(categoryName);
        assertThat(megaMenuEntry.getLinkUrl()).isEqualTo(categoryUrl);
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageUrl())).isTrue();
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageAltText())).isTrue();
        assertThat(megaMenuEntry.getStyle()).isEqualTo(NavigationNodeStyleEnum.SALELINK.getCode());
    }

    @Test
    public void testCreateMegaMenuEntryWithContentPage() {
        final String navigationNodeTitle = "Entry Title";
        final String contentPageTitle = "Content Page Title";
        final String contentPageName = "Content Page Name";
        final String contentPageUrl = "/category/url";

        final ContentPageModel mockContentPage = mock(ContentPageModel.class);
        given(mockContentPage.getLabel()).willReturn(contentPageUrl);
        given(mockContentPage.getTitle()).willReturn(contentPageTitle);
        given(mockContentPage.getName()).willReturn(contentPageName);

        final CMSNavigationEntryModel mockNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockNavigationEntry.getItem()).willReturn(mockContentPage);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(navigationNodeTitle);
        given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.SALELINK);

        final MegaMenuEntry megaMenuEntry = navigationNodeHelper.createMegaMenuEntry(mockNavigationNode);

        assertThat(megaMenuEntry).isNotNull();
        assertThat(megaMenuEntry.getText()).isEqualTo(navigationNodeTitle);
        assertThat(megaMenuEntry.getLinkUrl()).isEqualTo(contentPageUrl);
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageUrl())).isTrue();
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageAltText())).isTrue();
        assertThat(megaMenuEntry.getStyle()).isEqualTo(NavigationNodeStyleEnum.SALELINK.getCode());
    }

    @Test
    public void testCreateMegaMenuEntryWithContentPageWithTitle() {
        final String navigationNodeTitle = "";
        final String contentPageTitle = "Content Page Title";
        final String contentPageName = "Content Page Name";
        final String contentPageUrl = "/category/url";

        final ContentPageModel mockContentPage = mock(ContentPageModel.class);
        given(mockContentPage.getLabel()).willReturn(contentPageUrl);
        given(mockContentPage.getTitle()).willReturn(contentPageTitle);
        given(mockContentPage.getName()).willReturn(contentPageName);

        final CMSNavigationEntryModel mockNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockNavigationEntry.getItem()).willReturn(mockContentPage);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(navigationNodeTitle);
        given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.SALELINK);

        final MegaMenuEntry megaMenuEntry = navigationNodeHelper.createMegaMenuEntry(mockNavigationNode);

        assertThat(megaMenuEntry).isNotNull();
        assertThat(megaMenuEntry.getText()).isEqualTo(contentPageTitle);
        assertThat(megaMenuEntry.getLinkUrl()).isEqualTo(contentPageUrl);
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageUrl())).isTrue();
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageAltText())).isTrue();
        assertThat(megaMenuEntry.getStyle()).isEqualTo(NavigationNodeStyleEnum.SALELINK.getCode());
    }

    @Test
    public void testCreateMegaMenuEntryWithContentPageWithName() {
        final String navigationNodeTitle = "";
        final String contentPageTitle = "";
        final String contentPageName = "Content Page Name";
        final String contentPageUrl = "/category/url";

        final ContentPageModel mockContentPage = mock(ContentPageModel.class);
        given(mockContentPage.getLabel()).willReturn(contentPageUrl);
        given(mockContentPage.getTitle()).willReturn(contentPageTitle);
        given(mockContentPage.getName()).willReturn(contentPageName);

        final CMSNavigationEntryModel mockNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockNavigationEntry.getItem()).willReturn(mockContentPage);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(navigationNodeTitle);
        given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.SALELINK);

        final MegaMenuEntry megaMenuEntry = navigationNodeHelper.createMegaMenuEntry(mockNavigationNode);

        assertThat(megaMenuEntry).isNotNull();
        assertThat(megaMenuEntry.getText()).isEqualTo(contentPageName);
        assertThat(megaMenuEntry.getLinkUrl()).isEqualTo(contentPageUrl);
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageUrl())).isTrue();
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageAltText())).isTrue();
        assertThat(megaMenuEntry.getStyle()).isEqualTo(NavigationNodeStyleEnum.SALELINK.getCode());
    }

    @Test
    public void testCreateMegaMenuEntryWithNotVisibleLink() {
        final String navigationNodeTitle = "Entry Title";
        final String cmsLinkComponentName = "Link Name";
        final String cmsLinkComponentUrl = "/link/url";

        final CMSLinkComponentModel mockCmsLinkComponent = mock(CMSLinkComponentModel.class);
        willReturn(Boolean.FALSE).given(mockCmsLinkComponent).getVisible();
        given(mockCmsLinkComponent.getName()).willReturn(cmsLinkComponentName);
        given(mockCmsLinkComponent.getUrl()).willReturn(cmsLinkComponentUrl);

        final CMSNavigationEntryModel mockNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockNavigationEntry.getItem()).willReturn(mockCmsLinkComponent);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(navigationNodeTitle);
        given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.SALELINK);

        final MegaMenuEntry megaMenuEntry = navigationNodeHelper.createMegaMenuEntry(mockNavigationNode);

        assertThat(megaMenuEntry).isNotNull();
        assertThat(megaMenuEntry.getText()).isEqualTo(navigationNodeTitle);
        assertThat(StringUtils.isBlank(megaMenuEntry.getLinkUrl())).isTrue();
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageUrl())).isTrue();
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageAltText())).isTrue();
        assertThat(megaMenuEntry.getStyle()).isEqualTo(NavigationNodeStyleEnum.SALELINK.getCode());
    }

    @Test
    public void testCreateMegaMenuEntryWithLink() {
        final String navigationNodeTitle = "Entry Title";
        final String cmsLinkComponentName = "Link Name";
        final String cmsLinkComponentUrl = "/link/url";

        final CMSLinkComponentModel mockCmsLinkComponent = mock(CMSLinkComponentModel.class);
        given(mockCmsLinkComponent.getVisible()).willReturn(Boolean.TRUE);
        given(mockCmsLinkComponent.getName()).willReturn(cmsLinkComponentName);
        given(mockCmsLinkComponent.getUrl()).willReturn(cmsLinkComponentUrl);

        final CMSNavigationEntryModel mockNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockNavigationEntry.getItem()).willReturn(mockCmsLinkComponent);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(navigationNodeTitle);
        given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.SALELINK);

        final MegaMenuEntry megaMenuEntry = navigationNodeHelper.createMegaMenuEntry(mockNavigationNode);

        assertThat(megaMenuEntry).isNotNull();
        assertThat(megaMenuEntry.getText()).isEqualTo(navigationNodeTitle);
        assertThat(megaMenuEntry.getLinkUrl()).isEqualTo(cmsLinkComponentUrl);
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageUrl())).isTrue();
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageAltText())).isTrue();
        assertThat(megaMenuEntry.getStyle()).isEqualTo(NavigationNodeStyleEnum.SALELINK.getCode());
    }

    @Test
    public void testCreateMegaMenuEntryWithLinkWithName() {
        final String navigationNodeTitle = "";
        final String cmsLinkComponentName = "Link Name";
        final String cmsLinkComponentUrl = "/link/url";

        final CMSLinkComponentModel mockCmsLinkComponent = mock(CMSLinkComponentModel.class);
        given(mockCmsLinkComponent.getVisible()).willReturn(Boolean.TRUE);
        given(mockCmsLinkComponent.getName()).willReturn(cmsLinkComponentName);
        given(mockCmsLinkComponent.getUrl()).willReturn(cmsLinkComponentUrl);

        final CMSNavigationEntryModel mockNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockNavigationEntry.getItem()).willReturn(mockCmsLinkComponent);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(navigationNodeTitle);
        given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.SALELINK);

        final MegaMenuEntry megaMenuEntry = navigationNodeHelper.createMegaMenuEntry(mockNavigationNode);

        assertThat(megaMenuEntry).isNotNull();
        assertThat(megaMenuEntry.getText()).isEqualTo(cmsLinkComponentName);
        assertThat(megaMenuEntry.getLinkUrl()).isEqualTo(cmsLinkComponentUrl);
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageUrl())).isTrue();
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageAltText())).isTrue();
        assertThat(megaMenuEntry.getStyle()).isEqualTo(NavigationNodeStyleEnum.SALELINK.getCode());
    }

    @Test
    public void testCreateMegaMenuEntryWithNoNavNodeTitleAndNotVisibleLink() {
        final String cmsLinkComponentName = "Link Name";
        final String cmsLinkComponentUrl = "/link/url";

        final CMSLinkComponentModel mockCmsLinkComponent = mock(CMSLinkComponentModel.class);
        willReturn(Boolean.FALSE).given(mockCmsLinkComponent).getVisible();
        given(mockCmsLinkComponent.getName()).willReturn(cmsLinkComponentName);
        given(mockCmsLinkComponent.getUrl()).willReturn(cmsLinkComponentUrl);

        final CMSNavigationEntryModel mockNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockNavigationEntry.getItem()).willReturn(mockCmsLinkComponent);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.SALELINK);

        final MegaMenuEntry megaMenuEntry = navigationNodeHelper.createMegaMenuEntry(mockNavigationNode);

        assertThat(megaMenuEntry).isNotNull();
        assertThat(megaMenuEntry.getText()).isNull();
        assertThat(StringUtils.isBlank(megaMenuEntry.getLinkUrl())).isTrue();
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageUrl())).isTrue();
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageAltText())).isTrue();
        assertThat(megaMenuEntry.getStyle()).isEqualTo(NavigationNodeStyleEnum.SALELINK.getCode());
    }

    @Test
    public void testCreateMegaMenuEntryWithNonImageMedia() {
        final String navigationNodeTitle = "Entry Title";
        final String imageDownloadUrl = "/image/url";
        final String imageAltText = "Alt Text";

        final MediaModel mockMedia = mock(MediaModel.class);
        given(mockMedia.getMime()).willReturn("text/xml");
        given(mockMedia.getDownloadURL()).willReturn(imageDownloadUrl);
        given(mockMedia.getAltText()).willReturn(imageAltText);

        final CMSNavigationEntryModel mockNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockNavigationEntry.getItem()).willReturn(mockMedia);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(navigationNodeTitle);
        given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.SALELINK);

        final MegaMenuEntry megaMenuEntry = navigationNodeHelper.createMegaMenuEntry(mockNavigationNode);

        assertThat(megaMenuEntry).isNotNull();
        assertThat(megaMenuEntry.getText()).isEqualTo(navigationNodeTitle);
        assertThat(StringUtils.isBlank(megaMenuEntry.getLinkUrl())).isTrue();
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageUrl())).isTrue();
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageAltText())).isTrue();
        assertThat(megaMenuEntry.getStyle()).isEqualTo(NavigationNodeStyleEnum.SALELINK.getCode());
    }

    @Test
    public void testCreateMegaMenuEntryWithImageMedia() {
        final String navigationNodeTitle = "Entry Title";
        final String imageDownloadUrl = "/image/url";
        final String imageAltText = "Alt Text";

        final MediaModel mockMedia = mock(MediaModel.class);
        given(mockMedia.getMime()).willReturn("image/jpeg");
        given(mockMedia.getDownloadURL()).willReturn(imageDownloadUrl);
        given(mockMedia.getAltText()).willReturn(imageAltText);

        final CMSNavigationEntryModel mockNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockNavigationEntry.getItem()).willReturn(mockMedia);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(navigationNodeTitle);
        given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.SALELINK);

        final MegaMenuEntry megaMenuEntry = navigationNodeHelper.createMegaMenuEntry(mockNavigationNode);

        assertThat(megaMenuEntry).isNotNull();
        assertThat(megaMenuEntry.getText()).isEqualTo(navigationNodeTitle);
        assertThat(StringUtils.isBlank(megaMenuEntry.getLinkUrl())).isTrue();
        assertThat(megaMenuEntry.getImageUrl()).isEqualTo(imageDownloadUrl);
        assertThat(megaMenuEntry.getImageAltText()).isEqualTo(imageAltText);
        assertThat(megaMenuEntry.getStyle()).isEqualTo(NavigationNodeStyleEnum.SALELINK.getCode());
    }

    @Test
    public void testCreateMegaMenuEntryWithContentPageAndImage() {
        final String navigationNodeTitle = "Entry Title";
        final String contentPageTitle = "Content Page Title";
        final String contentPageName = "Content Page Name";
        final String contentPageUrl = "/category/url";
        final String imageDownloadUrl = "/image/url";
        final String imageAltText = "Alt Text";

        final ContentPageModel mockContentPage = mock(ContentPageModel.class);
        given(mockContentPage.getLabel()).willReturn(contentPageUrl);
        given(mockContentPage.getTitle()).willReturn(contentPageTitle);
        given(mockContentPage.getName()).willReturn(contentPageName);

        final CMSNavigationEntryModel mockContentPageNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockContentPageNavigationEntry.getItem()).willReturn(mockContentPage);

        final MediaModel mockMedia = mock(MediaModel.class);
        given(mockMedia.getMime()).willReturn("image/jpeg");
        given(mockMedia.getDownloadURL()).willReturn(imageDownloadUrl);
        given(mockMedia.getAltText()).willReturn(imageAltText);

        final CMSNavigationEntryModel mockMediaNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockMediaNavigationEntry.getItem()).willReturn(mockMedia);

        final List<CMSNavigationEntryModel> navigationEntries = new ArrayList<>();
        navigationEntries.add(mockContentPageNavigationEntry);
        navigationEntries.add(mockMediaNavigationEntry);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(navigationNodeTitle);
        given(mockNavigationNode.getEntries()).willReturn(navigationEntries);
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.SALELINK);

        final MegaMenuEntry megaMenuEntry = navigationNodeHelper.createMegaMenuEntry(mockNavigationNode);

        assertThat(megaMenuEntry).isNotNull();
        assertThat(megaMenuEntry.getText()).isEqualTo(navigationNodeTitle);
        assertThat(megaMenuEntry.getLinkUrl()).isEqualTo(contentPageUrl);
        assertThat(megaMenuEntry.getImageUrl()).isEqualTo(imageDownloadUrl);
        assertThat(megaMenuEntry.getImageAltText()).isEqualTo(imageAltText);
        assertThat(megaMenuEntry.getStyle()).isEqualTo(NavigationNodeStyleEnum.SALELINK.getCode());
    }

    @Test
    public void testCreateMegaMenuEntryWithTwoContentPages() {
        final String navigationNodeTitle = "";
        final String contentPage1Title = "Content Page 1 Title";
        final String contentPage1Name = "Content Page 1 Name";
        final String contentPage1Url = "/contentPage/url/1";
        final String contentPage2Title = "Content Page 2 Title";
        final String contentPage2Name = "Content Page 2 Name";
        final String contentPage2Url = "/contentPage/url/2";

        final ContentPageModel mockContentPage1 = mock(ContentPageModel.class);
        given(mockContentPage1.getLabel()).willReturn(contentPage1Url);
        given(mockContentPage1.getTitle()).willReturn(contentPage1Title);
        given(mockContentPage1.getName()).willReturn(contentPage1Name);

        final CMSNavigationEntryModel mockContentPage1Entry = mock(CMSNavigationEntryModel.class);
        given(mockContentPage1Entry.getItem()).willReturn(mockContentPage1);

        final ContentPageModel mockContentPage2 = mock(ContentPageModel.class);
        given(mockContentPage2.getLabel()).willReturn(contentPage2Url);
        given(mockContentPage2.getTitle()).willReturn(contentPage2Title);
        given(mockContentPage2.getName()).willReturn(contentPage2Name);

        final CMSNavigationEntryModel mockContentPage2Entry = mock(CMSNavigationEntryModel.class);
        given(mockContentPage2Entry.getItem()).willReturn(mockContentPage2);

        final List<CMSNavigationEntryModel> navigationEntryList = new ArrayList<>();
        navigationEntryList.add(mockContentPage1Entry);
        navigationEntryList.add(mockContentPage2Entry);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(navigationNodeTitle);
        given(mockNavigationNode.getEntries()).willReturn(navigationEntryList);
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.SALELINK);

        final MegaMenuEntry megaMenuEntry = navigationNodeHelper.createMegaMenuEntry(mockNavigationNode);

        assertThat(megaMenuEntry).isNotNull();
        assertThat(megaMenuEntry.getText()).isEqualTo(contentPage1Title);
        assertThat(megaMenuEntry.getLinkUrl()).isEqualTo(contentPage1Url);
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageUrl())).isTrue();
        assertThat(StringUtils.isBlank(megaMenuEntry.getImageAltText())).isTrue();
        assertThat(megaMenuEntry.getStyle()).isEqualTo(NavigationNodeStyleEnum.SALELINK.getCode());
    }

    @Test
    public void testCreateMegaMenuEntryWithTwoImages() {
        final String navigationNodeTitle = "Entry Title";
        final String image1DownloadUrl = "/image1/url";
        final String image1AltText = "Image 1 Alt Text";
        final String image2DownloadUrl = "/image2/url";
        final String image2AltText = "Image 2 Alt Text";

        final MediaModel mockMedia1 = mock(MediaModel.class);
        given(mockMedia1.getMime()).willReturn("image/jpeg");
        given(mockMedia1.getDownloadURL()).willReturn(image1DownloadUrl);
        given(mockMedia1.getAltText()).willReturn(image1AltText);

        final CMSNavigationEntryModel mockMedia1Entry = mock(CMSNavigationEntryModel.class);
        given(mockMedia1Entry.getItem()).willReturn(mockMedia1);

        final MediaModel mockMedia2 = mock(MediaModel.class);
        given(mockMedia2.getMime()).willReturn("image/jpeg");
        given(mockMedia2.getDownloadURL()).willReturn(image2DownloadUrl);
        given(mockMedia2.getAltText()).willReturn(image2AltText);

        final CMSNavigationEntryModel mockMedia2Entry = mock(CMSNavigationEntryModel.class);
        given(mockMedia2Entry.getItem()).willReturn(mockMedia2);

        final List<CMSNavigationEntryModel> navigationEntryList = new ArrayList<>();
        navigationEntryList.add(mockMedia1Entry);
        navigationEntryList.add(mockMedia2Entry);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(navigationNodeTitle);
        given(mockNavigationNode.getEntries()).willReturn(navigationEntryList);
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.SALELINK);

        final MegaMenuEntry megaMenuEntry = navigationNodeHelper.createMegaMenuEntry(mockNavigationNode);

        assertThat(megaMenuEntry).isNotNull();
        assertThat(megaMenuEntry.getText()).isEqualTo(navigationNodeTitle);
        assertThat(StringUtils.isBlank(megaMenuEntry.getLinkUrl())).isTrue();
        assertThat(megaMenuEntry.getImageUrl()).isEqualTo(image1DownloadUrl);
        assertThat(megaMenuEntry.getImageAltText()).isEqualTo(image1AltText);
        assertThat(megaMenuEntry.getStyle()).isEqualTo(NavigationNodeStyleEnum.SALELINK.getCode());
    }

    @Test
    public void testGetSvgSpriteUrl() {
        final String navigationNodeTitle = "Entry Title";
        final String imageDownloadUrl = "/image/url";
        final String imageAltText = "Alt Text";
        final String imageMediaUrl = "svgUrl";

        final MediaModel mockMedia = mock(MediaModel.class);
        given(mockMedia.getMime()).willReturn("image/svg");
        given(mockMedia.getDownloadURL()).willReturn(imageDownloadUrl);
        given(mockMedia.getAltText()).willReturn(imageAltText);
        given(mockMedia.getURL()).willReturn(imageMediaUrl);

        final CMSNavigationEntryModel mockNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockNavigationEntry.getItem()).willReturn(mockMedia);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(navigationNodeTitle);
        given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));
        given(mockNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.SALELINK);

        final String svgUrl = navigationNodeHelper.getSVGSpriteUrl(mockNavigationNode);

        assertThat(svgUrl).isNotNull();
        assertThat(svgUrl).isEqualTo("svgUrl");
    }

    @Test
    public void testBuildDeprtmentWithNoChildren() {
        final String departmentTitle = "Department Title";
        final String departmentUrl = "/department/url";

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(departmentTitle);
        doReturn(departmentUrl).when(helper).getNavigationNodeLink(mockNavigationNode);

        final MegaMenuDepartment department = helper.buildDepartment(mockNavigationNode, false);

        assertThat(department).isNotNull();
        assertThat(department.getTitle()).isEqualTo(departmentTitle);
        assertThat(department.getLink()).isEqualTo(departmentUrl);
        assertThat(department.getColumns()).isNull();
    }

    @Test
    public void testBuildDeprtmentWithInvisibleColumn() {
        final String departmentTitle = "Department Title";
        final String departmentUrl = "/department/url";

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(departmentTitle);
        doReturn(departmentUrl).when(helper).getNavigationNodeLink(mockNavigationNode);

        given(mockChildNavigationNode.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        willReturn(Boolean.FALSE).given(mockChildNavigationNode).isVisible();
        given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));

        final MegaMenuDepartment department = helper.buildDepartment(mockNavigationNode, true);

        assertThat(department).isNotNull();
        assertThat(department.getTitle()).isEqualTo(departmentTitle);
        assertThat(department.getLink()).isEqualTo(departmentUrl);
        assertThat(department.getColumns()).isNotNull();
        assertThat(CollectionUtils.isEmpty(department.getColumns())).isTrue();
    }

    @Test
    public void testBuildDeprtmentWithVisibleColumnInvisibleSection() {
        final String departmentTitle = "Department Title";
        final String departmentUrl = "/department/url";

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(departmentTitle);
        doReturn(departmentUrl).when(helper).getNavigationNodeLink(mockNavigationNode);

        given(mockChildNavigationNode.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        willReturn(Boolean.TRUE).given(mockChildNavigationNode).isVisible();
        given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));

        final CMSNavigationNodeModel mockSectionNode = mock(CMSNavigationNodeModel.class);
        given(mockSectionNode.getType()).willReturn(NavigationNodeTypeEnum.SECTION);
        willReturn(Boolean.FALSE).given(mockSectionNode).isVisible();
        given(mockChildNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockSectionNode));

        final MegaMenuDepartment department = helper.buildDepartment(mockNavigationNode, true);

        assertThat(department).isNotNull();
        assertThat(department.getTitle()).isEqualTo(departmentTitle);
        assertThat(department.getLink()).isEqualTo(departmentUrl);
        assertThat(department.getColumns()).isNotNull();
        assertThat(CollectionUtils.isEmpty(department.getColumns())).isTrue();
    }

    @Test
    public void testBuildDeprtmentWithVisibleColumnVisibleSection() {
        final String departmentTitle = "Department Title";
        final String departmentUrl = "/department/url";

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(departmentTitle);
        doReturn(departmentUrl).when(helper).getNavigationNodeLink(mockNavigationNode);

        given(mockChildNavigationNode.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        willReturn(Boolean.TRUE).given(mockChildNavigationNode).isVisible();
        given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));

        final CMSNavigationNodeModel mockSectionNode = mock(CMSNavigationNodeModel.class);
        given(mockSectionNode.getType()).willReturn(NavigationNodeTypeEnum.SECTION);
        willReturn(Boolean.TRUE).given(mockSectionNode).isVisible();
        given(mockChildNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockSectionNode));

        final MegaMenuDepartment department = helper.buildDepartment(mockNavigationNode, true);

        assertThat(department).isNotNull();
        assertThat(department.getTitle()).isEqualTo(departmentTitle);
        assertThat(department.getLink()).isEqualTo(departmentUrl);
        assertThat(department.getColumns()).isNotNull().hasSize(1);
        assertThat(CollectionUtils.isEmpty(department.getColumns())).isFalse();
    }

    @Test
    public void testBuildDeprtmentWithColumnsNotExceedingMaxColumnSizeConfiguration() {
        final String departmentTitle = "Department Title";
        final String departmentUrl = "/department/url";

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(departmentTitle);
        doReturn(departmentUrl).when(helper).getNavigationNodeLink(mockNavigationNode);

        // column 1
        final CMSNavigationNodeModel mockChildNavigationNode1 = mock(CMSNavigationNodeModel.class);
        given(mockChildNavigationNode1.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        willReturn(Boolean.TRUE).given(mockChildNavigationNode1).isVisible();

        final CMSNavigationNodeModel mockSectionNode = mock(CMSNavigationNodeModel.class);
        given(mockSectionNode.getType()).willReturn(NavigationNodeTypeEnum.SECTION);
        willReturn(Boolean.TRUE).given(mockSectionNode).isVisible();
        given(mockChildNavigationNode1.getChildren()).willReturn(
                Collections.singletonList(mockSectionNode));

        // column 2
        final CMSNavigationNodeModel mockChildNavigationNode2 = mock(CMSNavigationNodeModel.class);
        given(mockChildNavigationNode2.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        willReturn(Boolean.TRUE).given(mockChildNavigationNode2).isVisible();
        given(mockChildNavigationNode2.getChildren()).willReturn(
                Collections.singletonList(mockSectionNode));

        // column 3
        final CMSNavigationNodeModel mockChildNavigationNode3 = mock(CMSNavigationNodeModel.class);
        given(mockChildNavigationNode3.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        willReturn(Boolean.TRUE).given(mockChildNavigationNode3).isVisible();
        given(mockChildNavigationNode3.getChildren()).willReturn(
                Collections.singletonList(mockSectionNode));

        // column 4
        final CMSNavigationNodeModel mockChildNavigationNode4 = mock(CMSNavigationNodeModel.class);
        given(mockChildNavigationNode4.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        willReturn(Boolean.TRUE).given(mockChildNavigationNode4).isVisible();
        given(mockChildNavigationNode4.getChildren()).willReturn(
                Collections.singletonList(mockSectionNode));

        // column 5
        final CMSNavigationNodeModel mockChildNavigationNode5 = mock(CMSNavigationNodeModel.class);
        given(mockChildNavigationNode5.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        willReturn(Boolean.TRUE).given(mockChildNavigationNode5).isVisible();
        given(mockChildNavigationNode5.getChildren()).willReturn(
                Collections.singletonList(mockSectionNode));

        // column 6
        final CMSNavigationNodeModel mockChildNavigationNode6 = mock(CMSNavigationNodeModel.class);
        given(mockChildNavigationNode6.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        willReturn(Boolean.TRUE).given(mockChildNavigationNode6).isVisible();
        given(mockChildNavigationNode6.getChildren()).willReturn(
                Collections.singletonList(mockSectionNode));

        final List<CMSNavigationNodeModel> columns = new ArrayList<>();
        columns.add(mockChildNavigationNode1);
        columns.add(mockChildNavigationNode2);
        columns.add(mockChildNavigationNode3);
        columns.add(mockChildNavigationNode4);
        columns.add(mockChildNavigationNode5);
        columns.add(mockChildNavigationNode6);

        given(mockNavigationNode.getChildren()).willReturn(columns);

        final MegaMenuDepartment department = helper.buildDepartment(mockNavigationNode, true);

        assertThat(department).isNotNull();
        assertThat(department.getTitle()).isEqualTo(departmentTitle);
        assertThat(department.getLink()).isEqualTo(departmentUrl);
        assertThat(department.getColumns()).isNotNull();
        assertThat(CollectionUtils.isEmpty(department.getColumns())).isFalse();
        assertThat(department.getColumns()).isNotEmpty().hasSize(5);
    }

    @Test
    public void testBuildSectionWithSections() {
        final String sectionTitle = "Section Title";
        final String sectionUrl = "/section/url";

        final CMSNavigationNodeModel entry1 = mock(CMSNavigationNodeModel.class);
        given(entry1.getTitle()).willReturn("Entry 1");
        willReturn(Boolean.TRUE).given(entry1).isVisible();

        final CMSNavigationNodeModel entry2 = mock(CMSNavigationNodeModel.class);
        given(entry2.getTitle()).willReturn("Entry 2");
        willReturn(Boolean.TRUE).given(entry2).isVisible();

        final CMSNavigationNodeModel childSection1 = mock(CMSNavigationNodeModel.class);
        given(childSection1.getType()).willReturn(NavigationNodeTypeEnum.SECTION);
        given(childSection1.getTitle()).willReturn("Child Section 1");
        willReturn(Boolean.TRUE).given(childSection1).isVisible();
        doReturn("/childSection1").when(helper).getNavigationNodeLink(childSection1);
        given(childSection1.getChildren()).willReturn(Arrays.asList(entry1));

        final CMSNavigationNodeModel childSection2 = mock(CMSNavigationNodeModel.class);
        given(childSection2.getType()).willReturn(NavigationNodeTypeEnum.SECTION);
        given(childSection2.getTitle()).willReturn("Child Section 2");
        willReturn(Boolean.TRUE).given(childSection2).isVisible();
        doReturn("/childSection2").when(helper).getNavigationNodeLink(childSection2);
        given(childSection2.getChildren()).willReturn(Arrays.asList(entry2));

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.getTitle()).willReturn(sectionTitle);
        given(mockNavigationNode.getChildren()).willReturn(Arrays.asList(childSection1, childSection2));
        doReturn(sectionUrl).when(helper).getNavigationNodeLink(mockNavigationNode);

        final MegaMenuSection section = helper.buildSection(mockNavigationNode);
        assertThat(section).isNotNull();
        assertThat(section.getTitle()).isEqualTo(sectionTitle);
        assertThat(section.getLink()).isEqualTo(sectionUrl);
        assertThat(section.getSections()).isNotNull().hasSize(2);
        assertThat(section.getSections()).onProperty("title").containsExactly("Child Section 1", "Child Section 2");
        assertThat(section.getSections()).onProperty("link").containsExactly("/childSection1", "/childSection2");
        assertThat(section.getSections().get(0).getEntries()).isNotNull().hasSize(1);
        assertThat(section.getSections().get(0).getEntries().get(0).getText()).isEqualTo("Entry 1");
        assertThat(section.getSections().get(1).getEntries()).isNotNull().hasSize(1);
        assertThat(section.getSections().get(1).getEntries().get(0).getText()).isEqualTo("Entry 2");
    }

}
