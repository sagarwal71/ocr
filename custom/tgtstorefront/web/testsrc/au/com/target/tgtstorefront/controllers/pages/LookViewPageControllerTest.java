/**
 * 
 */

package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.configuration.Configuration;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import com.endeca.navigation.ENEQueryResults;

import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.querybuilder.EndecaProductQueryBuilder;
import au.com.target.tgtfacades.look.TargetLookPageFacade;
import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtfacades.stl.TargetShopTheLookFacade;
import au.com.target.tgtfacades.util.TargetPriceDataHelper;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import au.com.target.tgtstorefront.breadcrumb.impl.ShopTheLookBreadcrumbBuilder;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.EndecaQueryResultsHelper;
import au.com.target.tgtstorefront.util.PageTitleResolver;
import au.com.target.tgtwebcore.cache.service.AkamaiCacheConfigurationService;
import au.com.target.tgtwebcore.model.cms2.pages.TargetProductGroupPageModel;
import org.junit.Assert;


/**
 * @author mjanarth
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class LookViewPageControllerTest {

    @InjectMocks
    private final LookViewPageController controller = new LookViewPageController();

    @Mock
    private CMSPageService mockCmsPageService;

    @Mock
    private Model mockModel;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private TargetProductGroupPageModel grpPageModel;

    @Mock
    private ContentPageModel contentPageModel;

    @Mock
    private PageTitleResolver pageTitleResolver;

    @Mock
    private MediaModel mockMedia;

    @Mock
    private ContentPageModel contentPage;

    @Mock
    private TargetProductGroupPageModel groupPageModel;

    @Mock
    private ProductModel productModel;

    @Mock
    private EndecaQueryResultsHelper endecaQueryResultsHelper;

    @Mock
    private ENEQueryResults queryResults;

    @Mock
    private EndecaProductQueryBuilder endecaProductQueryBuilder;

    @Mock
    private TargetPriceDataHelper targetPriceDataHelper;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private Configuration configuration;

    @Mock
    private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;

    @Mock
    private TargetLookPageFacade targetLookPageFacade;

    @Mock
    private TargetShopTheLookFacade targetShopTheLookFacade;

    @Mock
    private ShopTheLookBreadcrumbBuilder shopTheLookBreadcrumbBuilder;

    @Mock
    private AkamaiCacheConfigurationService akamaiCacheConfigurationService;

    @Before
    public void init() {
        BDDMockito.given(pageTitleResolver.resolveContentPageTitle(Mockito.anyString())).willReturn("test Page");
        BDDMockito.given(productModel.getCode()).willReturn("CP2010");
        BDDMockito.given(configurationService.getConfiguration()).willReturn(configuration);

    }

    @Test
    public void testGetLookPageWithPageNotfound() throws UnsupportedEncodingException, CMSItemNotFoundException {
        final CMSItemNotFoundException expectedException = new CMSItemNotFoundException("Page not found");
        BDDMockito.given(mockCmsPageService.getPageForLabelOrId("/look/Test")).willThrow(expectedException);
        final String redirect = controller.getLookProductDetails("Test", mockModel, request, response);
        Assert.assertEquals(redirect, ControllerConstants.Forward.ERROR_404);
    }

    @Test
    public void testGetLookPageWithDiffPageModel() throws UnsupportedEncodingException, CMSItemNotFoundException {
        BDDMockito.given(mockCmsPageService.getPageForLabelOrId("/look/Test")).willReturn(contentPage);
        final String redirect = controller.getLookProductDetails("Test", mockModel, request, response);
        Assert.assertEquals(redirect, ControllerConstants.Forward.ERROR_404);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetLookPageWithProductsAndNullPrice()
            throws UnsupportedEncodingException, CMSItemNotFoundException {
        BDDMockito.given(grpPageModel.getProducts()).willReturn(Collections.singletonList(productModel));
        BDDMockito.given(mockCmsPageService.getPageForLabelOrId("/look/Test")).willReturn(grpPageModel);
        BDDMockito.given(grpPageModel.getFromPrice()).willReturn(null);

        BDDMockito.given(endecaProductQueryBuilder.getQueryResults(Mockito.any(EndecaSearchStateData.class),
                Mockito.anyList(),
                Mockito.anyInt())).willReturn(queryResults);
        final String redirect = controller.getLookProductDetails("Test", mockModel, request, response);
        BDDMockito.verify(endecaProductQueryBuilder, Mockito.times(1)).getQueryResults(
                Mockito.any(EndecaSearchStateData.class),
                Mockito.anyList(),
                Mockito.anyInt());
        BDDMockito.verify(targetPriceDataHelper, Mockito.never()).populatePriceData(Mockito.anyDouble(),
                Mockito.any(PriceDataType.class));
        BDDMockito.verify(endecaQueryResultsHelper)
                .getTargetProductsListForColorVariant(Mockito.any(ENEQueryResults.class));
        Assert.assertEquals(redirect, ControllerConstants.Views.Pages.LookView.LOOK_PAGE);
    }

    @Test
    public void testGetLookPageWithNoProducts() throws UnsupportedEncodingException, CMSItemNotFoundException {
        BDDMockito.given(grpPageModel.getProducts()).willReturn(null);
        BDDMockito.given(mockCmsPageService.getPageForLabelOrId("/look/Test")).willReturn(grpPageModel);
        final String redirect = controller.getLookProductDetails("Test", mockModel, request, response);
        BDDMockito.verify(endecaProductQueryBuilder, Mockito.never()).getQueryResults(
                Mockito.any(EndecaSearchStateData.class),
                Mockito.anyList(),
                Mockito.anyInt());
        BDDMockito.verify(endecaQueryResultsHelper, Mockito.never()).getTargetProductsListForColorVariant(
                Mockito.any(ENEQueryResults.class));
        Assert.assertEquals(redirect, ControllerConstants.Views.Pages.LookView.LOOK_PAGE);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetLookPageWithProductsAndPrice() throws UnsupportedEncodingException, CMSItemNotFoundException {
        BDDMockito.given(grpPageModel.getProducts()).willReturn(Collections.singletonList(productModel));
        BDDMockito.given(mockCmsPageService.getPageForLabelOrId("/look/Test")).willReturn(grpPageModel);
        BDDMockito.given(grpPageModel.getFromPrice()).willReturn(Double.valueOf(50.00));
        BDDMockito.given(endecaProductQueryBuilder.getQueryResults(Mockito.any(EndecaSearchStateData.class),
                Mockito.anyList(),
                Mockito.anyInt())).willReturn(queryResults);
        final String redirect = controller.getLookProductDetails("Test", mockModel, request, response);
        BDDMockito.verify(endecaProductQueryBuilder, Mockito.times(1)).getQueryResults(
                Mockito.any(EndecaSearchStateData.class),
                Mockito.anyList(),
                Mockito.anyInt());
        BDDMockito.verify(endecaQueryResultsHelper)
                .getTargetProductsListForColorVariant(Mockito.any(ENEQueryResults.class));
        Assert.assertEquals(redirect, ControllerConstants.Views.Pages.LookView.LOOK_PAGE);
    }

    @Test
    public void testGetLookPageWithLookModelNotFound() throws UnsupportedEncodingException, CMSItemNotFoundException {
        BDDMockito.given(targetShopTheLookFacade.getLookByCode("testCode")).willReturn(null);
        final String result = controller.getLookPage("testCode", mockModel, request, response);
        Assert.assertEquals(ControllerConstants.Forward.ERROR_404, result);
    }

    @Test
    public void testGetLookPageWithLookModelWithRedirect() throws UnsupportedEncodingException,
            CMSItemNotFoundException {
        final TargetLookModel targetLookModel = Mockito.mock(TargetLookModel.class);
        BDDMockito.given(targetLookModel.getUrl()).willReturn("redirectUrl");
        BDDMockito.given(targetShopTheLookFacade.getLookByCode("testCode")).willReturn(targetLookModel);
        BDDMockito.given(request.getRequestURI()).willReturn("originUrl");
        final String result = controller.getLookPage("testCode", mockModel, request, response);
        Assert.assertEquals("redirect:redirectUrl", result);
    }

    @Test
    public void testGetLookPageWithLookModelWithCmsPageNotFound() throws UnsupportedEncodingException,
            CMSItemNotFoundException {
        final TargetLookModel targetLookModel = Mockito.mock(TargetLookModel.class);
        BDDMockito.given(targetLookModel.getUrl()).willReturn("originUrl");
        BDDMockito.given(targetShopTheLookFacade.getLookByCode("testCode")).willReturn(targetLookModel);
        BDDMockito.given(request.getRequestURI()).willReturn("originUrl");

        BDDMockito.given(mockCmsPageService.getPageForLabelOrId("/look/lookDefaultPage")).willThrow(
                new CMSItemNotFoundException("Page not found"));

        try {
            controller.getLookPage("testCode", mockModel, request, response);
            Assert.assertFalse("unexpected exception", true);
        }
        catch (final CMSItemNotFoundException e) {
            Assert.assertTrue("expected exception", true);
        }
    }

    @Test
    public void testGetLookPageWithLookModel() throws UnsupportedEncodingException,
            CMSItemNotFoundException {
        final Model model = new ExtendedModelMap();
        final TargetLookModel targetLookModel = Mockito.mock(TargetLookModel.class);
        BDDMockito.given(targetLookModel.getId()).willReturn("id");
        BDDMockito.given(targetLookModel.getUrl()).willReturn("originUrl");
        final TargetLookCollectionModel collectionModel = Mockito.mock(TargetLookCollectionModel.class);
        BDDMockito.given(collectionModel.getId()).willReturn("id");
        BDDMockito.given(collectionModel.getUrl()).willReturn("collectionUrl");
        BDDMockito.given(targetLookModel.getCollection()).willReturn(collectionModel);
        BDDMockito.given(targetShopTheLookFacade.getLookByCode("testCode")).willReturn(targetLookModel);
        final TargetLookModel look1 = Mockito.mock(TargetLookModel.class);
        final TargetLookModel look2 = Mockito.mock(TargetLookModel.class);
        final List<TargetLookModel> looksInCollection = Arrays.asList(targetLookModel, look1, look2);
        final LookDetailsData lookDetail1 = Mockito.mock(LookDetailsData.class);
        final LookDetailsData lookDetail2 = Mockito.mock(LookDetailsData.class);
        BDDMockito.given(targetShopTheLookFacade.populateLookDetailsDataList(Arrays.asList(look1, look2)))
                .willReturn(Arrays.asList(lookDetail1, lookDetail2));
        BDDMockito.given(targetShopTheLookFacade.getVisibleLooksForCollection("id")).willReturn(looksInCollection);
        BDDMockito.given(request.getRequestURI()).willReturn("originUrl");
        BDDMockito.given(mockCmsPageService.getPageForLabelOrId("/look/lookDefaultPage")).willReturn(groupPageModel);
        BDDMockito.given(endecaProductQueryBuilder.getQueryResults(Mockito.any(EndecaSearchStateData.class),
                Mockito.anyList(),
                Mockito.anyInt())).willReturn(queryResults);
        final List<String> productCodes = Arrays.asList("product1", "product2");
        BDDMockito.given(targetShopTheLookFacade.getProductCodeListByLook("id")).willReturn(productCodes);
        final List<Breadcrumb> breadcrumbList = new ArrayList<>();
        BDDMockito.given(shopTheLookBreadcrumbBuilder.getBreadcrumbsForLookPage(targetLookModel)).willReturn(
                breadcrumbList);
        final String result = controller.getLookPage("testCode", model, request, response);
        BDDMockito.verify(targetShopTheLookFacade).populateLookDetailsData("id");
        BDDMockito.verify(endecaProductQueryBuilder, Mockito.times(1)).getQueryResults(
                Mockito.any(EndecaSearchStateData.class),
                Mockito.anyList(),
                Mockito.anyInt());
        BDDMockito.verify(endecaQueryResultsHelper)
                .getTargetProductsListForColorVariant(Mockito.any(ENEQueryResults.class));
        Assert.assertEquals(result, ControllerConstants.Views.Pages.LookView.LOOK_PAGE);
        final List<TargetLookModel> looks = (List<TargetLookModel>)model.asMap().get("relatedLooks");
        Assertions.assertThat(looks).isNotEmpty();
        Assertions.assertThat(looks).isEqualTo(Arrays.asList(lookDetail1, lookDetail2));
        Assertions.assertThat(model.asMap().get("relatedLooksUrl")).isEqualTo("collectionUrl");
    }

}