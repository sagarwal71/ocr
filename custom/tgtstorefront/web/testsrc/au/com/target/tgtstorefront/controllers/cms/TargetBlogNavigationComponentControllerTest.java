/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.core.model.ItemModel;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import au.com.target.tgtwebcore.model.cms2.components.TargetBlogNavigationComponentModel;
import au.com.target.tgtwebcore.model.cms2.pages.TargetBlogPageModel;


/**
 * @author gbaker2
 * 
 */
public class TargetBlogNavigationComponentControllerTest {

    private Model model;

    @Mock
    private HttpServletRequest request;

    @Mock
    private final TargetBlogNavigationComponentModel component = Mockito.mock(TargetBlogNavigationComponentModel.class);

    @InjectMocks
    private final TargetBlogNavigationComponentController targetBlogNavigationComponentController = new TargetBlogNavigationComponentController();

    private TargetBlogNavigationComponentController controller;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        model = new ExtendedModelMap();
        controller = Mockito.spy(targetBlogNavigationComponentController);
    }

    @Test
    public void testFillModelWithNoNavigationNode() {
        controller.fillModel(request, model, component);
    }

    @Test
    public void testFillModelWithNoNavigationNodeChildren() {
        final CMSNavigationNodeModel navigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);
        Mockito.when(component.getNavigationNode()).thenReturn(navigationNodeModel);
        controller.fillModel(request, model, component);
    }

    @Test
    public void testFillModelWithNoQuantity() {
        final CMSNavigationNodeModel navigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);
        final List<CMSNavigationNodeModel> children = new ArrayList<>();
        children.add(navigationNodeModel);
        Mockito.when(navigationNodeModel.getChildren()).thenReturn(children);
        Mockito.when(component.getNavigationNode()).thenReturn(navigationNodeModel);
        controller.fillModel(request, model, component);
    }

    @Test
    public void testFillModelWithQuantityNoEntries() {
        final CMSNavigationNodeModel navigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);
        final List<CMSNavigationNodeModel> children = new ArrayList<>();
        children.add(navigationNodeModel);
        Mockito.when(navigationNodeModel.getChildren()).thenReturn(children);
        Mockito.when(component.getNavigationNode()).thenReturn(navigationNodeModel);
        Mockito.when(component.getQuantityLarge()).thenReturn(new Integer(1));
        Mockito.when(component.getQuantityMedium()).thenReturn(new Integer(1));
        Mockito.when(component.getQuantitySmall()).thenReturn(new Integer(1));
        controller.fillModel(request, model, component);
    }

    @Test
    public void testFillModelWithOneTwoThree() {
        final CMSNavigationNodeModel navigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);
        final List<CMSNavigationNodeModel> children = createNavNodeListOfBlogPages(10);
        Mockito.when(navigationNodeModel.getChildren()).thenReturn(children);
        Mockito.when(component.getNavigationNode()).thenReturn(navigationNodeModel);
        Mockito.when(component.getQuantityLarge()).thenReturn(new Integer(1));
        Mockito.when(component.getQuantityMedium()).thenReturn(new Integer(2));
        Mockito.when(component.getQuantitySmall()).thenReturn(new Integer(3));

        int cursor = 0;
        controller.fillModel(request, model, component);

        int largePosition = 0;
        final List<TargetBlogPageModel> blogLargeList = (List<TargetBlogPageModel>)model.asMap().get("blogLargeList");
        Assert.assertEquals(1, blogLargeList.size());
        Assert.assertEquals(retrieveItem(children, cursor++), blogLargeList.get(largePosition++));

        int mediumPosition = 0;
        final List<TargetBlogPageModel> blogMediumList = (List<TargetBlogPageModel>)model.asMap().get("blogMediumList");
        Assert.assertEquals(2, blogMediumList.size());
        Assert.assertEquals(retrieveItem(children, cursor++), blogMediumList.get(mediumPosition++));
        Assert.assertEquals(retrieveItem(children, cursor++), blogMediumList.get(mediumPosition++));

        int smallPosition = 0;
        final List<TargetBlogPageModel> blogSmallList = (List<TargetBlogPageModel>)model.asMap().get("blogSmallList");
        Assert.assertEquals(3, blogSmallList.size());
        Assert.assertEquals(retrieveItem(children, cursor++), blogSmallList.get(smallPosition++));
        Assert.assertEquals(retrieveItem(children, cursor++), blogSmallList.get(smallPosition++));
        Assert.assertEquals(retrieveItem(children, cursor++), blogSmallList.get(smallPosition++));

    }

    @Test
    public void testFillModelWithTwoZero25() {
        final CMSNavigationNodeModel navigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);
        final List<CMSNavigationNodeModel> children = createNavNodeListOfBlogPages(10);
        Mockito.when(navigationNodeModel.getChildren()).thenReturn(children);
        Mockito.when(component.getNavigationNode()).thenReturn(navigationNodeModel);
        Mockito.when(component.getQuantityLarge()).thenReturn(new Integer(2));
        Mockito.when(component.getQuantityMedium()).thenReturn(new Integer(0));
        Mockito.when(component.getQuantitySmall()).thenReturn(new Integer(25));

        int cursor = 0;
        controller.fillModel(request, model, component);

        int largePosition = 0;
        final List<TargetBlogPageModel> blogLargeList = (List<TargetBlogPageModel>)model.asMap().get("blogLargeList");
        Assert.assertEquals(2, blogLargeList.size());
        Assert.assertEquals(retrieveItem(children, cursor++), blogLargeList.get(largePosition++));
        Assert.assertEquals(retrieveItem(children, cursor++), blogLargeList.get(largePosition++));

        final List<TargetBlogPageModel> blogMediumList = (List<TargetBlogPageModel>)model.asMap().get("blogMediumList");
        Assert.assertEquals(0, blogMediumList.size());

        int smallPosition = 0;
        final List<TargetBlogPageModel> blogSmallList = (List<TargetBlogPageModel>)model.asMap().get("blogSmallList");
        Assert.assertEquals(8, blogSmallList.size());
        for (int i = 0; i < 8; i++) {
            Assert.assertEquals(retrieveItem(children, cursor++), blogSmallList.get(smallPosition++));
        }
    }

    @Test
    public void testFillModelWithTwoNegative25() {
        final CMSNavigationNodeModel navigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);
        final List<CMSNavigationNodeModel> children = createNavNodeListOfBlogPages(10);
        Mockito.when(navigationNodeModel.getChildren()).thenReturn(children);
        Mockito.when(component.getNavigationNode()).thenReturn(navigationNodeModel);
        Mockito.when(component.getQuantityLarge()).thenReturn(new Integer(2));
        Mockito.when(component.getQuantityMedium()).thenReturn(new Integer(-10));
        Mockito.when(component.getQuantitySmall()).thenReturn(new Integer(25));

        int cursor = 0;
        controller.fillModel(request, model, component);

        int largePosition = 0;
        final List<TargetBlogPageModel> blogLargeList = (List<TargetBlogPageModel>)model.asMap().get("blogLargeList");
        Assert.assertEquals(2, blogLargeList.size());
        Assert.assertEquals(retrieveItem(children, cursor++), blogLargeList.get(largePosition++));
        Assert.assertEquals(retrieveItem(children, cursor++), blogLargeList.get(largePosition++));

        final List<TargetBlogPageModel> blogMediumList = (List<TargetBlogPageModel>)model.asMap().get("blogMediumList");
        Assert.assertEquals(0, blogMediumList.size());

        int smallPosition = 0;
        final List<TargetBlogPageModel> blogSmallList = (List<TargetBlogPageModel>)model.asMap().get("blogSmallList");
        Assert.assertEquals(8, blogSmallList.size());
        for (int i = 0; i < 8; i++) {
            Assert.assertEquals(retrieveItem(children, cursor++), blogSmallList.get(smallPosition++));
        }
    }



    @Test
    public void testFillModelWithNullNullNull() {
        final CMSNavigationNodeModel navigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);
        final List<CMSNavigationNodeModel> children = createNavNodeListOfBlogPages(10);
        Mockito.when(navigationNodeModel.getChildren()).thenReturn(children);
        Mockito.when(component.getNavigationNode()).thenReturn(navigationNodeModel);

        controller.fillModel(request, model, component);

        final List<TargetBlogPageModel> blogLargeList = (List<TargetBlogPageModel>)model.asMap().get("blogLargeList");
        Assert.assertEquals(0, blogLargeList.size());

        final List<TargetBlogPageModel> blogMediumList = (List<TargetBlogPageModel>)model.asMap().get("blogMediumList");
        Assert.assertEquals(0, blogMediumList.size());

        final List<TargetBlogPageModel> blogSmallList = (List<TargetBlogPageModel>)model.asMap().get("blogSmallList");
        Assert.assertEquals(0, blogSmallList.size());
    }

    @Test
    public void testFillModelWithVisibiliyHidden() {
        final CMSNavigationNodeModel navigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);
        final List<CMSNavigationNodeModel> children = createNavNodeListOfBlogPages(30, false);
        Mockito.when(navigationNodeModel.getChildren()).thenReturn(children);
        Mockito.when(component.getNavigationNode()).thenReturn(navigationNodeModel);
        Mockito.when(component.getQuantityLarge()).thenReturn(new Integer(4));
        Mockito.when(component.getQuantityMedium()).thenReturn(new Integer(4));
        Mockito.when(component.getQuantitySmall()).thenReturn(new Integer(4));

        int cursor = 0;
        controller.fillModel(request, model, component);

        final List<TargetBlogPageModel> blogLargeList = (List<TargetBlogPageModel>)model.asMap().get("blogLargeList");
        Assert.assertEquals(4, blogLargeList.size());
        for (int largePosition = 0; largePosition < 4; largePosition++) {
            Assert.assertEquals(retrieveItem(children, cursor++), blogLargeList.get(largePosition));
            cursor++;
        }

        final List<TargetBlogPageModel> blogMediumList = (List<TargetBlogPageModel>)model.asMap().get("blogMediumList");
        Assert.assertEquals(4, blogMediumList.size());
        for (int mediumPosition = 0; mediumPosition < 4; mediumPosition++) {
            Assert.assertEquals(retrieveItem(children, cursor++), blogMediumList.get(mediumPosition));
            cursor++;
        }

        final List<TargetBlogPageModel> blogSmallList = (List<TargetBlogPageModel>)model.asMap().get("blogSmallList");
        Assert.assertEquals(4, blogSmallList.size());
        for (int smallPosition = 0; smallPosition < 4; smallPosition++) {
            Assert.assertEquals(retrieveItem(children, cursor++), blogSmallList.get(smallPosition));
            cursor++;
        }
    }

    @Test
    public void testFillModelWhenContentCMSPage() {
        final CMSNavigationNodeModel navigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);
        final List<CMSNavigationNodeModel> children = createNavNodeListOfBlogPages(10);
        Mockito.when(navigationNodeModel.getChildren()).thenReturn(children);
        Mockito.when(component.getNavigationNode()).thenReturn(navigationNodeModel);
        final Model newModel = new ExtendedModelMap();
        final ContentPageModel contentPage = Mockito.mock(ContentPageModel.class);
        Mockito.when(request.getAttribute("cmsPage")).thenReturn(contentPage);

        controller.fillModel(request, newModel, component);
        Assert.assertEquals(Boolean.TRUE, newModel.asMap().get("isContentPage"));
    }

    @Test
    public void testFillModelWhenOtherPage() {
        final CMSNavigationNodeModel navigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);
        final List<CMSNavigationNodeModel> children = createNavNodeListOfBlogPages(10);
        Mockito.when(navigationNodeModel.getChildren()).thenReturn(children);
        Mockito.when(component.getNavigationNode()).thenReturn(navigationNodeModel);
        final Model newModel = new ExtendedModelMap();
        final CategoryPageModel cattPage = Mockito.mock(CategoryPageModel.class);
        Mockito.when(request.getAttribute("cmsPage")).thenReturn(cattPage);

        controller.fillModel(request, newModel, component);
        Assert.assertEquals(null, newModel.asMap().get("isContentPage"));
    }

    private List<CMSNavigationNodeModel> createNavNodeListOfBlogPages(final int num) {
        return createNavNodeListOfBlogPages(num, true);
    }

    @SuppressWarnings("boxing")
    private List<CMSNavigationNodeModel> createNavNodeListOfBlogPages(final int num, final boolean allVisible) {

        final List<CMSNavigationNodeModel> children = new ArrayList<>();

        for (int i = 0; i < num; i++) {
            final CMSNavigationNodeModel child = Mockito.mock(CMSNavigationNodeModel.class);
            Mockito.when(child.isVisible()).thenReturn(allVisible || (i % 2 == 0) ? Boolean.TRUE : Boolean.FALSE);
            final List<CMSNavigationEntryModel> entries = new ArrayList<>();
            children.add(child);
            Mockito.when(child.getEntries()).thenReturn(entries);

            final CMSNavigationEntryModel entry = Mockito.mock(CMSNavigationEntryModel.class);
            final TargetBlogPageModel targetBlogPageModel = Mockito.mock(TargetBlogPageModel.class);
            entries.add(entry);
            Mockito.when(entry.getItem()).thenReturn(targetBlogPageModel);
        }

        return children;


    }

    private ItemModel retrieveItem(final List<CMSNavigationNodeModel> children, final int position) {
        return children.get(position).getEntries().get(0).getItem();
    }

}
