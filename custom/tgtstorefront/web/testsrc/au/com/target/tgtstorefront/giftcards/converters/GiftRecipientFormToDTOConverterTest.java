package au.com.target.tgtstorefront.giftcards.converters;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Test;

import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;
import au.com.target.tgtstorefront.forms.GiftRecipientForm;


/**
 * 
 */

/**
 * GiftRecipientFormToDTO Converter
 * 
 * @author jjayawa1
 *
 */
@UnitTest
public class GiftRecipientFormToDTOConverterTest {
    private final GiftRecipientFormToDTOConverter converter = new GiftRecipientFormToDTOConverter();

    @Test
    public void testEmptyForm() {
        final GiftRecipientForm form = new GiftRecipientForm();
        final GiftRecipientDTO giftRecipient = converter.convert(form);
        Assert.assertNotNull(giftRecipient);
        Assert.assertNull(giftRecipient.getFirstName());
        Assert.assertNull(giftRecipient.getLastName());
        Assert.assertNull(giftRecipient.getRecipientEmailAddress());
        Assert.assertNull(giftRecipient.getMessageText());
    }

    @Test
    public void testFormWithValues() {
        final GiftRecipientForm form = new GiftRecipientForm();
        form.setFirstName("firstName");
        form.setLastName("lastName");
        form.setMessageText("Message Text");
        form.setRecipientEmailAddress("firstName@email.com");
        final GiftRecipientDTO giftRecipient = converter.convert(form);
        Assert.assertNotNull(giftRecipient);
        Assert.assertEquals("firstName", giftRecipient.getFirstName());
        Assert.assertEquals("lastName", giftRecipient.getLastName());
        Assert.assertEquals("Message Text", giftRecipient.getMessageText());
        Assert.assertEquals("firstName@email.com", giftRecipient.getRecipientEmailAddress());
    }

    @Test
    public void testFormWithValuesWithCFLF() {
        final GiftRecipientForm form = new GiftRecipientForm();
        form.setFirstName("firstName");
        form.setLastName("lastName");
        form.setMessageText("Message Text\r\nsdfdsfsdf\r\nwersdfsdf");
        form.setRecipientEmailAddress("firstName@email.com");
        final GiftRecipientDTO giftRecipient = converter.convert(form);
        Assert.assertNotNull(giftRecipient);
        Assert.assertEquals("firstName", giftRecipient.getFirstName());
        Assert.assertEquals("lastName", giftRecipient.getLastName());
        Assert.assertEquals("Message Text\r\nsdfdsfsdf\r\nwersdfsdf", giftRecipient.getMessageText());
        Assert.assertEquals("firstName@email.com", giftRecipient.getRecipientEmailAddress());
    }

}
