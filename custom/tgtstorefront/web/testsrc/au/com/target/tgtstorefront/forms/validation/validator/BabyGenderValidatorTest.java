/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import de.hybris.platform.servicelayer.i18n.I18NService;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;

import au.com.target.tgtstorefront.forms.validation.BabyGender;


/**
 * @author bhuang3
 * 
 */
public class BabyGenderValidatorTest {

    @Mock
    private BabyGender babyGender;

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Mock
    private ConstraintViolationBuilder constraintViolationBuilder;

    @InjectMocks
    private final BabyGenderValidator babyGenderValidator = new BabyGenderValidator() {
        {
            final MessageSource messageSourceMocked = BDDMockito.mock(MessageSource.class);
            this.messageSource = messageSourceMocked;
            final I18NService i18nServiceMocked = BDDMockito.mock(I18NService.class);
            this.i18nService = i18nServiceMocked;
        }
    };

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        BDDMockito.given(constraintValidatorContext.buildConstraintViolationWithTemplate(BDDMockito.anyString()))
                .willReturn(constraintViolationBuilder);
        babyGenderValidator.initialize(babyGender);
    }

    @Test
    public void correctGender() {
        Assert.assertTrue(babyGenderValidator.isAvailable("M"));
        Assert.assertTrue(babyGenderValidator.isAvailable("m"));
        Assert.assertTrue(babyGenderValidator.isAvailable("F"));
        Assert.assertTrue(babyGenderValidator.isAvailable("f"));
    }

    @Test
    public void incorrectGender() {
        Assert.assertFalse(babyGenderValidator.isAvailable("anyOther"));
        Assert.assertFalse(babyGenderValidator.isAvailable(""));
        Assert.assertFalse(babyGenderValidator.isAvailable(null));
    }

}
