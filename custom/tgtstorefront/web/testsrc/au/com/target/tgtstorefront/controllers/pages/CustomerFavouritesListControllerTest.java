/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.configuration.Configuration;
import org.fest.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import com.endeca.navigation.AggrERec;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.google.common.collect.ImmutableList;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.converter.AggregatedEndecaRecordToTargetProductListerConverter;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.querybuilder.TargetFavouritesListQueryBuilder;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.product.data.CustomerProductInfo;
import au.com.target.tgtfacades.product.data.CustomerProductsListData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetSelectedVariantData;
import au.com.target.tgtfacades.wishlist.TargetWishListFacade;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.AbstractPageController.PageType;
import au.com.target.tgtstorefront.controllers.util.EndecaQueryResultsHelper;
import au.com.target.tgtstorefront.forms.FavouritesListShareForm;
import au.com.target.tgtstorefront.util.PageTitleResolver;
import au.com.target.tgtwishlist.enums.WishListTypeEnum;
import au.com.target.tgtwishlist.exception.InvalidWishListUserException;
import org.junit.Assert;


/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CustomerFavouritesListControllerTest {



    @InjectMocks
    private CustomerFavouritesListController controller = new CustomerFavouritesListController();

    @Mock
    private TargetFavouritesListQueryBuilder targetFavouritesListQueryBuilder;

    @Mock
    private AggregatedEndecaRecordToTargetProductListerConverter<AggrERec, TargetProductListerData> aggrERecToProductConverter;

    @Mock
    private Model model;

    @Mock
    private HttpServletRequest request;

    @Mock
    private CustomerProductInfo productInfo;

    @Mock
    private CustomerProductInfo productInfo2;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private Configuration configuration;

    @Mock
    private TargetWishListFacade targetWishListFacade;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Mock
    private EndecaSearchStateData endecaSearchStateData;

    @Mock
    private FavouritesListShareForm favouritesListShareForm;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private ENEQueryResults queryResults;

    private List<CustomerProductInfo> serverProductInfoList;

    @Mock
    private EndecaQueryResultsHelper queryResultsHelper;

    @Mock
    private CMSPageService cmsPageService;

    @Mock
    private PageTitleResolver pageTitleResolver;

    @Mock
    private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;

    private final List<TargetProductListerData> productsToBeShared = mock(List.class);




    @Before
    public void setUp() throws Exception {
        given(configurationService.getConfiguration()).willReturn(configuration);
        given(Integer.valueOf(
                configuration.getInt("storefront.favourites.component.size", 99)))
                        .willReturn(Integer.valueOf(99));
        given(productInfo.getBaseProductCode()).willReturn("p2010");
        serverProductInfoList = Collections.singletonList(productInfo2);
        doReturn(serverProductInfoList).when(targetWishListFacade).retrieveFavouriteProducts();
        given(favouritesListShareForm.getRecipientEmailAddress()).willReturn("email@test.com");
        given(favouritesListShareForm.getFullName()).willReturn("Tom hanks");
        given(favouritesListShareForm.getMessageText()).willReturn("Hi,Please view my favourites");
        given(Integer.valueOf(configuration.getInt("storefront.favourites.share.maxSize", 6)))
                .willReturn(Integer.valueOf(3));
        given(Integer.valueOf(configuration.getInt("storefront.favourites.share.buffer", 10)))
                .willReturn(Integer.valueOf(3));
        given(targetFavouritesListQueryBuilder.getQueryResults(endecaSearchStateData, 6)).willReturn(queryResults);

        controller = spy(controller);

    }

    @Test(expected = TargetEndecaException.class)
    public void testGetFavouriteswithException() throws Exception {
        final CustomerProductsListData products = createProductsList();
        given(targetWishListFacade.retrieveFavouriteProducts()).willReturn(products.getProducts());
        final List<CustomerProductInfo> customerProductDataList = Collections.singletonList(productInfo);
        doReturn(endecaSearchStateData).when(controller).populateEndecaSearchStateData(customerProductDataList);
        final List<TargetProductListerData> list = mock(List.class);
        given(queryResultsHelper.getTargetProductList(any(ENEQueryResults.class))).willReturn(list);
        given(targetFavouritesListQueryBuilder.getQueryResults(endecaSearchStateData, 99))
                .willThrow(new TargetEndecaException());
        controller.getFavouritesProductDetails(Arrays.array(productInfo), request, model);
    }


    @Test
    public void testGetFavouriteswithNullLocalStorageAndPrdFromServer() throws Exception {
        final CustomerProductsListData products = createProductsList();
        given(targetWishListFacade.retrieveFavouriteProducts()).willReturn(products.getProducts());
        final List<CustomerProductInfo> customerProductDataList = Collections.singletonList(productInfo);
        doReturn(endecaSearchStateData).when(controller).populateEndecaSearchStateData(customerProductDataList);
        final List<TargetProductListerData> list = mock(List.class);
        given(queryResultsHelper.getTargetProductList(any(ENEQueryResults.class))).willReturn(list);
        doReturn(products.getProducts()).when(controller).updateCustomerProductData(customerProductDataList, list);
        assertThat(ControllerConstants.Views.Fragments.Wishlist.FAVOURITES_FRAGMENT).isEqualTo(
                controller.getFavouritesProductDetails(null, request, model));
        verify(targetFavouritesListQueryBuilder).getQueryResults(endecaSearchStateData, 99);
        verify(model).addAttribute("productData", list);
        verify(model).addAttribute("customerProductData", products.getProducts());
    }

    @Test
    public void testGetFavouritesProductDetailsWithInvalidUserAndPrdsinLocalStrge() throws Exception {
        doThrow(new InvalidWishListUserException("Error")).when(targetWishListFacade).retrieveFavouriteProducts();
        final List<CustomerProductInfo> productInfoList = Collections.singletonList(productInfo);
        final List<TargetProductListerData> list = mock(List.class);
        given(queryResultsHelper.getTargetProductList(any(ENEQueryResults.class))).willReturn(list);
        doReturn(endecaSearchStateData).when(controller).populateEndecaSearchStateData(productInfoList);
        doReturn(productInfoList).when(controller).updateCustomerProductData(productInfoList, list);
        assertThat(ControllerConstants.Views.Fragments.Wishlist.FAVOURITES_FRAGMENT).isEqualTo(
                controller.getFavouritesProductDetails(Arrays.array(productInfo), request, model));
        verify(targetFavouritesListQueryBuilder).getQueryResults(any(EndecaSearchStateData.class),
                anyInt());
        verify(model).addAttribute("productData", list);
        verify(model).addAttribute("customerProductData", productInfoList);
        verifyNoMoreInteractions(model);
    }

    @Test
    public void testGetFavouritesProductDetailsWithvalidUserPrdsinLocalStrge() throws Exception {
        serverProductInfoList = Collections.singletonList(productInfo2);
        doReturn(serverProductInfoList).when(targetWishListFacade).retrieveFavouriteProducts();
        final List<TargetProductListerData> list = mock(List.class);
        given(queryResultsHelper.getTargetProductList(any(ENEQueryResults.class))).willReturn(list);
        doReturn(endecaSearchStateData).when(controller).populateEndecaSearchStateData(serverProductInfoList);
        doReturn(serverProductInfoList).when(controller).updateCustomerProductData(serverProductInfoList, list);
        assertThat(ControllerConstants.Views.Fragments.Wishlist.FAVOURITES_FRAGMENT).isEqualTo(
                controller.getFavouritesProductDetails(Arrays.array(productInfo), request, model));
        verify(targetFavouritesListQueryBuilder).getQueryResults(any(EndecaSearchStateData.class),

                anyInt());
        verify(model).addAttribute("productData", list);
        verify(model).addAttribute("customerProductData", serverProductInfoList);
        verifyNoMoreInteractions(model);
    }


    @Test
    public void testGetFavouritesProductDetailsInvalidUserAndNullPrds() throws Exception {
        doThrow(new InvalidWishListUserException("error")).when(targetWishListFacade).retrieveFavouriteProducts();
        final List<CustomerProductInfo> productInfoList = Collections.singletonList(productInfo);
        doReturn(endecaSearchStateData).when(controller).populateEndecaSearchStateData(productInfoList);
        final List<TargetProductListerData> list = mock(List.class);
        given(queryResultsHelper.getTargetProductList(any(ENEQueryResults.class))).willReturn(list);
        assertThat(ControllerConstants.Views.Fragments.Wishlist.FAVOURITES_FRAGMENT).isEqualTo(
                controller.getFavouritesProductDetails(null, request, model));
        verify(model).addAttribute("productData", list);
        verify(model).addAttribute("customerProductData", ListUtils.EMPTY_LIST);
        verifyNoMoreInteractions(model);
    }

    @Test
    public void testGetFavouritesProductDetailsvalidUserAndNullPrds() throws Exception {
        doReturn(null).when(targetWishListFacade).retrieveFavouriteProducts();
        final List<CustomerProductInfo> productInfoList = Collections.singletonList(productInfo);
        doReturn(endecaSearchStateData).when(controller).populateEndecaSearchStateData(productInfoList);
        final List<TargetProductListerData> list = mock(List.class);
        given(queryResultsHelper.getTargetProductList(any(ENEQueryResults.class))).willReturn(list);
        assertThat(ControllerConstants.Views.Fragments.Wishlist.FAVOURITES_FRAGMENT).isEqualTo(
                controller.getFavouritesProductDetails(Arrays.array(productInfo), request, model));
        verify(model).addAttribute("productData", list);
        verify(model).addAttribute("customerProductData", CollectionUtils.EMPTY_COLLECTION);
        verifyNoMoreInteractions(model);
    }

    @Test
    public void testsendFavouritesEmailWithValidationError() {
        given(Boolean.valueOf(bindingResult.hasErrors())).willReturn(Boolean.TRUE);
        controller.sendFavouritesEmail(model, favouritesListShareForm, bindingResult);
        verify(model).addAttribute("success", Boolean.FALSE);
        verify(model).addAttribute(favouritesListShareForm);
    }

    @Test
    public void testsendFavouritesValidDataAndSendEmailFailed() throws Exception {
        final List<TargetProductListerData> list = mock(List.class);
        given(Boolean.valueOf(bindingResult.hasErrors())).willReturn(Boolean.FALSE);
        doReturn(endecaSearchStateData).when(controller).populateEndecaSearchStateData(serverProductInfoList);
        given(queryResultsHelper.getTargetProductList(any(ENEQueryResults.class))).willReturn(list);
        controller.sendFavouritesEmail(model, favouritesListShareForm, bindingResult);
        given(Boolean.valueOf(targetWishListFacade.shareWishListForUser(list, "email@test.com", "Tom hanks",
                "Hi,Please view my favourites"))).willReturn(Boolean.FALSE);
        verify(targetFavouritesListQueryBuilder).getQueryResults(endecaSearchStateData, 6);
        verify(model).addAttribute("recipientEmailAddress", "email@test.com");
        verify(model).addAttribute("success", Boolean.valueOf(false));
    }

    @Test
    public void testsendFavouritesValidDataAndSendEmailSuccess() throws Exception {
        final List<TargetProductListerData> list = mock(List.class);
        given(Boolean.valueOf(bindingResult.hasErrors())).willReturn(Boolean.FALSE);
        doReturn(endecaSearchStateData).when(controller).populateEndecaSearchStateData(serverProductInfoList);
        given(queryResultsHelper.getTargetProductList(any(ENEQueryResults.class))).willReturn(list);
        doReturn(productsToBeShared).when(controller).populateListToBeShared(list);
        given(
                Boolean.valueOf(targetWishListFacade.shareWishListForUser(productsToBeShared, "email@test.com",
                        "Tom hanks",
                        "Hi,Please view my favourites"))).willReturn(Boolean.TRUE);

        controller.sendFavouritesEmail(model, favouritesListShareForm, bindingResult);
        verify(targetFavouritesListQueryBuilder).getQueryResults(endecaSearchStateData, 6);
        verify(model).addAttribute("recipientEmailAddress", "email@test.com");
        verify(model).addAttribute("success", Boolean.valueOf(true));
    }

    @Test
    public void testsendFavouritesValidVariantDataAndSendEmailSuccess() throws Exception {
        final List<TargetProductListerData> list = mock(List.class);
        given(Boolean.valueOf(bindingResult.hasErrors())).willReturn(Boolean.FALSE);
        doReturn(Boolean.TRUE).when(targetFeatureSwitchFacade).isFeatureEnabled("wishlist.favouritesVariants");
        doReturn(endecaSearchStateData).when(controller).populateEndecaSearchStateData(serverProductInfoList);
        given(queryResultsHelper.getTargetProductList(any(ENEQueryResults.class), any(Map.class))).willReturn(list);
        doReturn(productsToBeShared).when(controller).populateListToBeShared(list);
        given(
                Boolean.valueOf(targetWishListFacade.shareWishListForUser(productsToBeShared, "email@test.com",
                        "Tom hanks",
                        "Hi,Please view my favourites"))).willReturn(Boolean.TRUE);

        controller.sendFavouritesEmail(model, favouritesListShareForm, bindingResult);
        verify(targetFavouritesListQueryBuilder).getQueryResults(endecaSearchStateData, 6);
        verify(model).addAttribute("recipientEmailAddress", "email@test.com");
        verify(model).addAttribute("success", Boolean.valueOf(true));
    }

    @Test
    public void testsendFavouritesValidDataAndSendEmailException() throws Exception {
        final List<TargetProductListerData> list = mock(List.class);
        given(Boolean.valueOf(bindingResult.hasErrors())).willReturn(Boolean.FALSE);
        doReturn(endecaSearchStateData).when(controller).populateEndecaSearchStateData(serverProductInfoList);
        given(queryResultsHelper.getTargetProductList(any(ENEQueryResults.class))).willReturn(list);
        doReturn(productsToBeShared).when(controller).populateListToBeShared(list);
        given(Boolean.valueOf(targetWishListFacade.shareWishListForUser(productsToBeShared, "email@test.com",
                "Tom hanks",
                "Hi,Please view my favourites"))).willThrow(new InvalidWishListUserException("Test"));

        controller.sendFavouritesEmail(model, favouritesListShareForm, bindingResult);
        verify(targetFavouritesListQueryBuilder).getQueryResults(endecaSearchStateData, 6);
        verify(model).addAttribute("success", Boolean.valueOf(false));
    }


    @Test
    public void testPopulateListToBeSharedGreateerThanMax() {
        final List<TargetProductListerData> productList = new ArrayList<>();
        List<TargetProductListerData> modifiedList = null;
        final TargetProductListerData prod1 = mock(TargetProductListerData.class);
        final TargetProductListerData prod2 = mock(TargetProductListerData.class);
        final TargetProductListerData prod3 = mock(TargetProductListerData.class);
        final TargetProductListerData prod4 = mock(TargetProductListerData.class);
        final TargetProductListerData prod5 = mock(TargetProductListerData.class);
        productList.add(prod1);
        productList.add(prod2);
        productList.add(prod3);
        productList.add(prod4);
        productList.add(prod5);
        given(
                Integer.valueOf(configurationService.getConfiguration().getInt("storefront.favourites.share.maxSize",
                        10)))
                                .willReturn(Integer.valueOf(3));
        modifiedList = controller.populateListToBeShared(productList);
        assertThat(3).isEqualTo(modifiedList.size());
        assertThat(prod1).isEqualTo(modifiedList.get(0));
        assertThat(prod2).isEqualTo(modifiedList.get(1));
        assertThat(prod3).isEqualTo(modifiedList.get(2));

    }

    @Test
    public void testPopulateListToBeSharedLessThanMax() {
        final List<TargetProductListerData> productList = new ArrayList<>();
        List<TargetProductListerData> modifiedList = null;
        final TargetProductListerData prod1 = mock(TargetProductListerData.class);
        final TargetProductListerData prod2 = mock(TargetProductListerData.class);
        productList.add(prod1);
        productList.add(prod2);

        modifiedList = controller.populateListToBeShared(productList);
        assertThat(2).isEqualTo(modifiedList.size());
        assertThat(prod1).isEqualTo(modifiedList.get(0));
        assertThat(prod2).isEqualTo(modifiedList.get(1));
    }

    @Test
    public void testPopulateListToBeSharedWithNull() {
        final List<TargetProductListerData> modifiedList = controller.populateListToBeShared(null);
        assertThat(modifiedList).isEmpty();

    }


    @Test
    public void testPopulateEndecaSearchStateDataWithRecordFilter() {
        final List<CustomerProductInfo> productInfoList = Collections.singletonList(productInfo);
        final EndecaSearchStateData data = controller.populateEndecaSearchStateData(productInfoList);
        assertThat(data.isSkipDefaultSort()).isTrue();
        assertThat(data.isSkipInStockFilter()).isTrue();
        assertThat(data.getRecordFilterOptions().get(0)).isEqualTo(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE);
        assertThat(data.getRecordFilterOptions().get(1)).isEqualTo(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        assertThat(data.getRecordFilterOptions().get(2)).isEqualTo(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE);

    }

    @Test
    public void testGetFavouritesProductDetailsWithSelectedVariant() throws Exception {
        final List<TargetProductListerData> list = mock(List.class);
        doReturn(Boolean.TRUE).when(targetFeatureSwitchFacade).isFeatureEnabled("wishlist.favouritesVariants");
        doReturn(endecaSearchStateData).when(controller).populateEndecaSearchStateData(any(List.class), anyBoolean());
        given(queryResultsHelper.getTargetProductList(any(ENEQueryResults.class), any(Map.class))).willReturn(list);
        final CustomerProductInfo[] productInfoArray = Arrays.array(productInfo);
        doReturn(serverProductInfoList).when(controller).updateCustomerProductData(serverProductInfoList, list);
        assertThat(ControllerConstants.Views.Fragments.Wishlist.FAVOURITES_FRAGMENT).isEqualTo(
                controller.getFavouritesProductDetails(productInfoArray, request, model));
        verify(model).addAttribute("productData", list);
        verify(model).addAttribute("customerProductData", serverProductInfoList);
        verifyNoMoreInteractions(model);
    }

    @Test
    public void testPopulateEndecaSearchStateDataWithVariant() {
        final List<CustomerProductInfo> list = ImmutableList.of(productInfo);
        given(productInfo.getBaseProductCode()).willReturn("baseProductCode");
        given(productInfo.getSelectedVariantCode()).willReturn("variantProductCode");
        final EndecaSearchStateData data = controller.populateEndecaSearchStateData(list);
        assertThat(data.getProductCodes().get(0)).isEqualTo("variantProductCode");
    }

    @Test
    public void testPopulateEndecaSearchStateDataWithSelectedVariant() {
        final List<CustomerProductInfo> list = ImmutableList.of(productInfo);
        given(productInfo.getBaseProductCode()).willReturn("baseProductCode");
        given(productInfo.getSelectedVariantCode()).willReturn("variantProductCode");
        final EndecaSearchStateData data = controller.populateEndecaSearchStateData(list, true);
        assertThat(data.getProductCodes().get(0)).isEqualTo("variantProductCode");
    }

    @Test
    public void testPopulateEndecaSearchStateDataWithEmptySelectedVariant() {
        final List<CustomerProductInfo> list = ImmutableList.of(productInfo);
        given(productInfo.getBaseProductCode()).willReturn("baseProductCode");
        given(productInfo.getSelectedVariantCode()).willReturn("");
        final EndecaSearchStateData data = controller.populateEndecaSearchStateData(list, true);
        assertThat(data.getProductCodes().get(0)).isEqualTo("baseProductCode");
    }


    private CustomerProductsListData createProductsList() {
        final CustomerProductsListData products = new CustomerProductsListData();
        final List<CustomerProductInfo> productList = new ArrayList<>();
        productList.add(productInfo);
        products.setProducts(productList);
        return products;
    }

    @Test
    public void testCreateBaseProductCodeAndSelectedVariantMapWithNull() {
        final List<CustomerProductInfo> customerProductInfos = null;
        final Map<String, String> map = controller.createBaseProductCodeAndSelectedVariantMap(customerProductInfos);
        Assert.assertNull(map);

    }

    @Test
    public void testCreateBaseProductCodeAndSelectedVariantMap() {
        final List<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        final CustomerProductInfo customerProductInfo1 = new CustomerProductInfo();
        final CustomerProductInfo customerProductInfo2 = new CustomerProductInfo();
        customerProductInfo1.setBaseProductCode("P1000");
        customerProductInfo1.setSelectedVariantCode("P1000_black");
        customerProductInfo2.setBaseProductCode("P1001");
        customerProductInfo2.setSelectedVariantCode("P1001_blue_s");
        customerProductInfos.add(customerProductInfo1);
        customerProductInfos.add(customerProductInfo2);
        final Map<String, String> map = controller.createBaseProductCodeAndSelectedVariantMap(customerProductInfos);
        assertThat(map).isNotNull();
        assertThat("P1000_black").isEqualTo(map.get("P1000"));
        assertThat("P1001_blue_s").isEqualTo(map.get("P1001"));
        assertThat(map.size()).isEqualTo(customerProductInfos.size());
    }

    @Test
    public void testCreateBaseProductCodeAndSelectedVariantMapWithoutSelectedVaraint() {
        final List<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        final CustomerProductInfo customerProductInfo1 = new CustomerProductInfo();
        final CustomerProductInfo customerProductInfo2 = new CustomerProductInfo();
        customerProductInfo1.setBaseProductCode("P1000");
        customerProductInfo1.setSelectedVariantCode("P1000_black");
        customerProductInfo2.setBaseProductCode("P1001");
        customerProductInfo2.setSelectedVariantCode(null);
        customerProductInfos.add(customerProductInfo1);
        customerProductInfos.add(customerProductInfo2);
        final Map<String, String> map = controller.createBaseProductCodeAndSelectedVariantMap(customerProductInfos);
        assertThat(map).isNotNull();
        assertThat("P1000_black").isEqualTo(map.get("P1000"));
        assertThat(map.get("P1001")).isNull();
        assertThat(map.size()).isEqualTo(customerProductInfos.size());
    }

    @Test
    public void testUpdateSelectedVariant() throws Exception {
        final String view = controller.updateVariantToFavourite(productInfo, request, model);
        verify(targetWishListFacade).addProductToList(productInfo, null, WishListTypeEnum.FAVOURITE);
        verify(model).addAttribute("success", Boolean.TRUE);
        assertThat(view).isEqualTo(ControllerConstants.Views.Fragments.Wishlist.FAVOURITES_FRAGMENT);
    }

    @Test
    public void testUpdateSelectedVariantWithAnonymouseUser() throws ENEQueryException, TargetEndecaException {
        final InvalidWishListUserException exception = new InvalidWishListUserException("test");
        doThrow(exception).when(targetWishListFacade).addProductToList(productInfo, null, WishListTypeEnum.FAVOURITE);
        final String view = controller.updateVariantToFavourite(productInfo, request, model);
        verify(targetWishListFacade).addProductToList(productInfo, null, WishListTypeEnum.FAVOURITE);
        verify(model).addAttribute("success", Boolean.TRUE);
        assertThat(view).isEqualTo(ControllerConstants.Views.Fragments.Wishlist.FAVOURITES_FRAGMENT);
    }

    @Test
    public void testUpdateSelectedVariantWithError() throws Exception {
        final RuntimeException exception = new RuntimeException("test");
        doThrow(exception).when(targetWishListFacade).addProductToList(productInfo, null, WishListTypeEnum.FAVOURITE);
        final String view = controller.updateVariantToFavourite(productInfo, request, model);
        verify(targetWishListFacade).addProductToList(productInfo, null, WishListTypeEnum.FAVOURITE);
        verify(model).addAttribute("success", Boolean.FALSE);
        assertThat(view).isEqualTo(ControllerConstants.Views.Fragments.Wishlist.FAVOURITES_FRAGMENT);
    }

    @Test
    public void testUpdateCustomerProductDataWithNullCustomerProductListAndProductData() {
        assertThat(controller.updateCustomerProductData(null, null)).isEmpty();
    }

    @Test
    public void testUpdateCustomerProductDataWithEmptyCustomerProductListAndProductData() {
        assertThat(controller.updateCustomerProductData(new ArrayList<CustomerProductInfo>(),
                new ArrayList<TargetProductListerData>())).isEmpty();
    }

    @Test
    public void testUpdateCustomerProductDataWithEmptyCustomerProductList() {
        final List<TargetProductListerData> targetProductListerData = new ArrayList<>();
        final TargetProductListerData targetProductListerData1 = mock(TargetProductListerData.class);
        given(targetProductListerData1.getBaseProductCode()).willReturn("1000");
        targetProductListerData.add(targetProductListerData1);
        assertThat(controller.updateCustomerProductData(new ArrayList<CustomerProductInfo>(),
                targetProductListerData)).isEmpty();
    }

    @Test
    public void testUpdateCustomerProductDataWithDiffProductCode() {
        final List<TargetProductListerData> targetProductListerDatas = new ArrayList<>();
        final TargetProductListerData targetProductListerDataMock = mock(TargetProductListerData.class);
        final TargetSelectedVariantData targetSelectedVariantDataMock = mock(TargetSelectedVariantData.class);
        targetProductListerDatas.add(targetProductListerDataMock);
        given(targetProductListerDataMock.getBaseProductCode()).willReturn("BP1New");
        given(targetProductListerDataMock.getSelectedVariantData()).willReturn(targetSelectedVariantDataMock);
        given(targetSelectedVariantDataMock.getProductCode()).willReturn("VC1");

        final List<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        final CustomerProductInfo customerProductInfoMock = new CustomerProductInfo();
        customerProductInfos.add(customerProductInfoMock);
        customerProductInfoMock.setBaseProductCode("BP1");
        customerProductInfoMock.setSelectedVariantCode("VC1");

        final List<CustomerProductInfo> updatedCustomerProductData = controller
                .updateCustomerProductData(customerProductInfos, targetProductListerDatas);
        assertThat(updatedCustomerProductData).isNotEmpty();
        assertThat(updatedCustomerProductData).hasSize(1);
        assertThat(updatedCustomerProductData.get(0).getBaseProductCode()).isEqualTo("BP1New");
        assertThat(updatedCustomerProductData.get(0).getSelectedVariantCode()).isEqualTo("VC1");


    }

    @Test
    public void testUpdateCustomerProductDataSameProductCode() {
        final List<TargetProductListerData> targetProductListerDatas = new ArrayList<>();
        final TargetProductListerData targetProductListerDataMock = mock(TargetProductListerData.class);
        final TargetSelectedVariantData targetSelectedVariantDataMock = mock(TargetSelectedVariantData.class);
        targetProductListerDatas.add(targetProductListerDataMock);
        given(targetProductListerDataMock.getBaseProductCode()).willReturn("BP2");
        given(targetProductListerDataMock.getSelectedVariantData()).willReturn(targetSelectedVariantDataMock);
        given(targetSelectedVariantDataMock.getProductCode()).willReturn("VC2");

        final List<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        final CustomerProductInfo customerProductInfoMock = mock(CustomerProductInfo.class);
        customerProductInfos.add(customerProductInfoMock);
        given(customerProductInfoMock.getBaseProductCode()).willReturn("BP2");
        given(customerProductInfoMock.getSelectedVariantCode()).willReturn("VC2");

        final List<CustomerProductInfo> updatedCustomerProductData = controller
                .updateCustomerProductData(customerProductInfos, targetProductListerDatas);
        assertThat(updatedCustomerProductData).isNotEmpty();
        assertThat(updatedCustomerProductData).hasSize(1);
        assertThat(updatedCustomerProductData.get(0).getBaseProductCode()).isEqualTo("BP2");
        assertThat(updatedCustomerProductData.get(0).getSelectedVariantCode()).isEqualTo("VC2");


    }

    @Test
    public void testUpdateCustomerProductDataNoSelectedVariant() {
        final List<TargetProductListerData> targetProductListerDatas = new ArrayList<>();
        final TargetProductListerData targetProductListerDataMock = mock(TargetProductListerData.class);
        targetProductListerDatas.add(targetProductListerDataMock);
        given(targetProductListerDataMock.getBaseProductCode()).willReturn("BP3");
        given(targetProductListerDataMock.getSelectedVariantData()).willReturn(null);

        final List<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        final CustomerProductInfo customerProductInfoMock = new CustomerProductInfo();
        customerProductInfos.add(customerProductInfoMock);
        customerProductInfoMock.setBaseProductCode("BP3");
        final List<CustomerProductInfo> updatedCustomerProductData = controller
                .updateCustomerProductData(customerProductInfos, targetProductListerDatas);
        assertThat(updatedCustomerProductData).isNotEmpty();
        assertThat(updatedCustomerProductData).hasSize(1);
        assertThat(updatedCustomerProductData.get(0).getBaseProductCode()).isEqualTo("BP3");
        assertThat(updatedCustomerProductData.get(0).getSelectedVariantCode()).isNull();
    }

    @Test
    public void testUpdateCustomerProductDataWithVariantNoEndecaResult() {
        final List<TargetProductListerData> targetProductListerDatas = new ArrayList<>();
        final List<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        final CustomerProductInfo customerProductInfoMock = mock(CustomerProductInfo.class);
        customerProductInfos.add(customerProductInfoMock);
        given(customerProductInfoMock.getBaseProductCode()).willReturn("BP4");
        given(customerProductInfoMock.getSelectedVariantCode()).willReturn("VC4");
        final List<CustomerProductInfo> updatedCustomerProductData = controller
                .updateCustomerProductData(customerProductInfos, targetProductListerDatas);
        assertThat(updatedCustomerProductData).isEmpty();
    }

    @Test
    public void testUpdateCustomerProductDataNoEndecaResult() {
        final List<TargetProductListerData> targetProductListerDatas = new ArrayList<>();
        final List<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        final CustomerProductInfo customerProductInfoMock = mock(CustomerProductInfo.class);
        customerProductInfos.add(customerProductInfoMock);
        given(customerProductInfoMock.getBaseProductCode()).willReturn("BP4");
        final List<CustomerProductInfo> updatedCustomerProductData = controller
                .updateCustomerProductData(customerProductInfos, targetProductListerDatas);
        assertThat(updatedCustomerProductData).isEmpty();
    }

    @Test
    public void testUpdateCustomerProductDataWithPartialEndecaOutput() {
        final List<TargetProductListerData> targetProductListerDatas = new ArrayList<>();
        final TargetProductListerData targetProductListerDataMock = mock(TargetProductListerData.class);
        final TargetSelectedVariantData targetSelectedVariantDataMock = mock(TargetSelectedVariantData.class);
        targetProductListerDatas.add(targetProductListerDataMock);
        given(targetProductListerDataMock.getBaseProductCode()).willReturn("BP5New");
        given(targetProductListerDataMock.getSelectedVariantData()).willReturn(targetSelectedVariantDataMock);
        given(targetSelectedVariantDataMock.getProductCode()).willReturn("VC5");

        final List<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        final CustomerProductInfo customerProductInfoMock1 = new CustomerProductInfo();
        customerProductInfos.add(customerProductInfoMock1);
        customerProductInfoMock1.setBaseProductCode("BP5");
        customerProductInfoMock1.setSelectedVariantCode("VC5");
        final CustomerProductInfo customerProductInfoMock2 = new CustomerProductInfo();
        customerProductInfos.add(customerProductInfoMock2);
        customerProductInfoMock2.setBaseProductCode("BP6");
        customerProductInfoMock2.setSelectedVariantCode("VC6");

        final List<CustomerProductInfo> updatedCustomerProductData = controller
                .updateCustomerProductData(customerProductInfos, targetProductListerDatas);
        assertThat(updatedCustomerProductData).isNotEmpty();
        assertThat(updatedCustomerProductData).hasSize(1);
        assertThat(updatedCustomerProductData.get(0).getBaseProductCode()).isEqualTo("BP5New");
        assertThat(updatedCustomerProductData.get(0).getSelectedVariantCode()).isEqualTo("VC5");
    }

    @Test
    public void testUpdateCustomerProductDataWithSameProductCodeAndNoSelectedVariant() {
        final List<TargetProductListerData> targetProductListerDatas = new ArrayList<>();
        final TargetProductListerData targetProductListerDataMock = mock(TargetProductListerData.class);
        targetProductListerDatas.add(targetProductListerDataMock);
        given(targetProductListerDataMock.getBaseProductCode()).willReturn("BP8");
        given(targetProductListerDataMock.getSelectedVariantData()).willReturn(null);

        final List<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        final CustomerProductInfo customerProductInfoMock = new CustomerProductInfo();
        customerProductInfos.add(customerProductInfoMock);
        customerProductInfoMock.setBaseProductCode("BP8");
        customerProductInfoMock.setSelectedVariantCode("VC8");

        final List<CustomerProductInfo> updatedCustomerProductData = controller
                .updateCustomerProductData(customerProductInfos, targetProductListerDatas);
        assertThat(updatedCustomerProductData).isNotEmpty();
        assertThat(updatedCustomerProductData).hasSize(1);
        assertThat(updatedCustomerProductData.get(0).getBaseProductCode()).isEqualTo("BP8");
        assertThat(updatedCustomerProductData.get(0).getSelectedVariantCode()).isEqualTo("VC8");
    }

    @Test
    public void testGetFavouritesProductPage() throws CMSItemNotFoundException {
        final ContentPageModel pageModel = mock(ContentPageModel.class);
        final List<Breadcrumb> breadcrumb = new ArrayList<>();
        given(cmsPageService.getPageForLabelOrId("/favourites")).willReturn(pageModel);
        given(pageModel.getTitle()).willReturn("Home");
        given(pageTitleResolver.resolveContentPageTitle("Home")).willReturn("Home");
        given(contentPageBreadcrumbBuilder.getBreadcrumbs(pageModel)).willReturn(breadcrumb);
        given(pageModel.getCanonicalUrl()).willReturn("http://target.com.au");

        controller.getFavouritesProductPage(model);
        verify(model).addAttribute(WebConstants.CMS_PAGE_MODEL, pageModel);
        verify(model).addAttribute("pageTitle", "Home");
        verify(model).addAttribute("canonicalUrl", "http://target.com.au");
        verify(model).addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumb);
        verify(model).addAttribute("pageType", PageType.Favourite);
    }
}
