/**
 * 
 */
package au.com.target.tgtstorefront.navigation;

import de.hybris.platform.acceleratorcms.model.components.NavigationBarComponentModel;
import de.hybris.platform.cms2.enums.LinkTargets;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtfacades.navigation.NavigationMenuItem;


/**
 * @author rmcalave
 * 
 */
public class LeftNavigationMenuBuilderTest {
    private final LeftNavigationMenuBuilder leftNavigationMenuBuilder = new LeftNavigationMenuBuilder();

    // public void testGetLeftNavigationMenu

    @Test
    public void testGetLeftNavigationMenuWithNoNavNodes() {
        final ContentPageModel mockContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockContentPage.getNavigationNodeList()).willReturn(new ArrayList<CMSNavigationNodeModel>());

        Assert.assertNull(leftNavigationMenuBuilder.getLeftNavigationMenu(mockContentPage));
    }

    @Test
    public void testGetLeftNavigationMenuWithoutEnoughParents() {
        final CMSNavigationNodeModel mockRootNode = createMockNavigationNode(null);
        final CMSNavigationNodeModel mockContentRootNode = createMockNavigationNode(mockRootNode);

        final List<CMSNavigationNodeModel> navNodesForPage = new ArrayList<>();
        navNodesForPage.add(mockContentRootNode);

        final ContentPageModel mockContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockContentPage.getNavigationNodeList()).willReturn(navNodesForPage);

        Assert.assertNull(leftNavigationMenuBuilder.getLeftNavigationMenu(mockContentPage));
    }

    @Test
    public void testGetLeftNavigationMenuWithNoEntries() {
        final CMSNavigationNodeModel mockRootNode = createMockNavigationNode(null);
        final CMSNavigationNodeModel mockContentRootNode = createMockNavigationNode(mockRootNode);
        final CMSNavigationNodeModel mockPageNode = createMockNavigationNode(mockContentRootNode);

        final List<CMSNavigationNodeModel> navNodesForPage = new ArrayList<>();
        navNodesForPage.add(mockPageNode);

        final ContentPageModel mockContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockContentPage.getNavigationNodeList()).willReturn(navNodesForPage);

        final NavigationMenuItem leftNavMenu = leftNavigationMenuBuilder.getLeftNavigationMenu(mockContentPage);

        Assert.assertNull(leftNavMenu);
    }

    @Test
    public void testGetLeftNavigationMenuWithOneLevel() {
        final String pageTitle = "Page Title";
        final String link = "/awesome/page";

        final CMSNavigationNodeModel mockRootNode = createMockNavigationNode(null);
        final CMSNavigationNodeModel mockContentRootNode = createMockNavigationNode(mockRootNode);
        final CMSNavigationNodeModel mockPageNode = createMockNavigationNode(mockContentRootNode);

        final List<CMSNavigationNodeModel> navNodesForPage = new ArrayList<>();
        navNodesForPage.add(mockPageNode);

        final ContentPageModel mockContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockContentPage.getNavigationNodeList()).willReturn(navNodesForPage);
        BDDMockito.given(mockContentPage.getTitle()).willReturn(pageTitle);
        BDDMockito.given(mockContentPage.getLabel()).willReturn(link);

        final CMSNavigationEntryModel mockContentPageEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockContentPageEntry.getItem()).willReturn(mockContentPage);

        final List<CMSNavigationEntryModel> entriesForPageNode = new ArrayList<>();
        entriesForPageNode.add(mockContentPageEntry);
        BDDMockito.given(mockPageNode.getEntries()).willReturn(entriesForPageNode);

        final NavigationMenuItem leftNavMenu = leftNavigationMenuBuilder.getLeftNavigationMenu(mockContentPage);

        Assert.assertNotNull(leftNavMenu);
        Assert.assertTrue(CollectionUtils.isEmpty(leftNavMenu.getChildren()));
        Assert.assertEquals(pageTitle, leftNavMenu.getName());
        Assert.assertEquals(link, leftNavMenu.getLink());
        Assert.assertFalse(leftNavMenu.isExternal());
        Assert.assertFalse(leftNavMenu.isNewWindow());
        Assert.assertTrue(leftNavMenu.isCurrentPage());
        Assert.assertFalse(leftNavMenu.isCurrentPageInChildren());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetLeftNavigationMenuWithTwoLevels() {
        final CMSNavigationNodeModel mockRootNode = createMockNavigationNode(null);
        final CMSNavigationNodeModel mockContentRootNode = createMockNavigationNode(mockRootNode);


        // Level 1
        final CMSNavigationNodeModel mockPageNode = createMockNavigationNode(mockContentRootNode);
        BDDMockito.given(mockPageNode.isVisible()).willReturn(Boolean.TRUE);

        final List<CMSNavigationNodeModel> navNodesForPage = new ArrayList<>();
        navNodesForPage.add(mockPageNode);

        final ContentPageModel mockContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockContentPage.getNavigationNodeList()).willReturn(navNodesForPage);

        final CMSNavigationEntryModel mockContentPageEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockContentPageEntry.getItem()).willReturn(mockContentPage);

        final List<CMSNavigationEntryModel> entriesForPageNode = new ArrayList<>();
        entriesForPageNode.add(mockContentPageEntry);
        BDDMockito.given(mockPageNode.getEntries()).willReturn(entriesForPageNode);


        // Level 2
        final CMSNavigationNodeModel mockSecondLevelNode = createMockNavigationNode(mockPageNode);
        BDDMockito.given(mockSecondLevelNode.isVisible()).willReturn(Boolean.TRUE);

        final List<CMSNavigationNodeModel> navNodesForSecondLevelPage = new ArrayList<>();
        navNodesForSecondLevelPage.add(mockSecondLevelNode);

        final ContentPageModel mockSecondLevelContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockSecondLevelContentPage.getNavigationNodeList()).willReturn(navNodesForSecondLevelPage);

        final CMSNavigationEntryModel mockSecondLevelContentPageEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockSecondLevelContentPageEntry.getItem()).willReturn(mockSecondLevelContentPage);

        final List<CMSNavigationEntryModel> entriesForSecondLevelPageNode = new ArrayList<>();
        entriesForSecondLevelPageNode.add(mockSecondLevelContentPageEntry);
        BDDMockito.given(mockSecondLevelNode.getEntries()).willReturn(entriesForSecondLevelPageNode);


        final NavigationMenuItem leftNavMenu = leftNavigationMenuBuilder
                .getLeftNavigationMenu(mockSecondLevelContentPage);

        // Validate the top level
        Assert.assertNotNull(leftNavMenu);
        Assert.assertTrue(CollectionUtils.isNotEmpty(leftNavMenu.getChildren()));

        final NavigationMenuItem secondLevel = leftNavMenu.getChildren().iterator().next();

        // Validate the next level
        Assert.assertNotNull(secondLevel);
        Assert.assertTrue(CollectionUtils.isEmpty(secondLevel.getChildren()));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetLeftNavigationMenuWithTwoLevelsAndNotVisible() {
        final CMSNavigationNodeModel mockRootNode = createMockNavigationNode(null);
        final CMSNavigationNodeModel mockContentRootNode = createMockNavigationNode(mockRootNode);


        // Level 1
        final CMSNavigationNodeModel mockPageNode = createMockNavigationNode(mockContentRootNode);
        BDDMockito.given(mockPageNode.isVisible()).willReturn(Boolean.TRUE);

        final List<CMSNavigationNodeModel> navNodesForPage = new ArrayList<>();
        navNodesForPage.add(mockPageNode);

        final ContentPageModel mockContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockContentPage.getNavigationNodeList()).willReturn(navNodesForPage);

        final CMSNavigationEntryModel mockContentPageEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockContentPageEntry.getItem()).willReturn(mockContentPage);

        final List<CMSNavigationEntryModel> entriesForPageNode = new ArrayList<>();
        entriesForPageNode.add(mockContentPageEntry);
        BDDMockito.given(mockPageNode.getEntries()).willReturn(entriesForPageNode);


        // Level 2
        final CMSNavigationNodeModel mockSecondLevelNode = createMockNavigationNode(mockPageNode);
        BDDMockito.given(mockSecondLevelNode.isVisible()).willReturn(Boolean.FALSE);

        final List<CMSNavigationNodeModel> navNodesForSecondLevelPage = new ArrayList<>();
        navNodesForSecondLevelPage.add(mockSecondLevelNode);

        final ContentPageModel mockSecondLevelContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockSecondLevelContentPage.getNavigationNodeList()).willReturn(navNodesForSecondLevelPage);

        final CMSNavigationEntryModel mockSecondLevelContentPageEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockSecondLevelContentPageEntry.getItem()).willReturn(mockSecondLevelContentPage);

        final List<CMSNavigationEntryModel> entriesForSecondLevelPageNode = new ArrayList<>();
        entriesForSecondLevelPageNode.add(mockSecondLevelContentPageEntry);
        BDDMockito.given(mockSecondLevelNode.getEntries()).willReturn(entriesForSecondLevelPageNode);


        final NavigationMenuItem leftNavMenu = leftNavigationMenuBuilder
                .getLeftNavigationMenu(mockSecondLevelContentPage);

        // Validate the top level
        Assert.assertNotNull(leftNavMenu);
        Assert.assertTrue(CollectionUtils.isEmpty(leftNavMenu.getChildren()));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetLeftNavigationMenuWithTwoLevelsAndNoEntries() {
        final CMSNavigationNodeModel mockRootNode = createMockNavigationNode(null);
        final CMSNavigationNodeModel mockContentRootNode = createMockNavigationNode(mockRootNode);


        // Level 1
        final CMSNavigationNodeModel mockPageNode = createMockNavigationNode(mockContentRootNode);
        BDDMockito.given(mockPageNode.isVisible()).willReturn(Boolean.TRUE);

        final List<CMSNavigationNodeModel> navNodesForPage = new ArrayList<>();
        navNodesForPage.add(mockPageNode);

        final ContentPageModel mockContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockContentPage.getNavigationNodeList()).willReturn(navNodesForPage);

        final CMSNavigationEntryModel mockContentPageEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockContentPageEntry.getItem()).willReturn(mockContentPage);

        final List<CMSNavigationEntryModel> entriesForPageNode = new ArrayList<>();
        entriesForPageNode.add(mockContentPageEntry);
        BDDMockito.given(mockPageNode.getEntries()).willReturn(entriesForPageNode);


        // Level 2
        final CMSNavigationNodeModel mockSecondLevelNode = createMockNavigationNode(mockPageNode);
        BDDMockito.given(mockSecondLevelNode.isVisible()).willReturn(Boolean.TRUE);

        final List<CMSNavigationNodeModel> navNodesForSecondLevelPage = new ArrayList<>();
        navNodesForSecondLevelPage.add(mockSecondLevelNode);

        final ContentPageModel mockSecondLevelContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockSecondLevelContentPage.getNavigationNodeList()).willReturn(navNodesForSecondLevelPage);


        final NavigationMenuItem leftNavMenu = leftNavigationMenuBuilder
                .getLeftNavigationMenu(mockSecondLevelContentPage);

        // Validate the top level
        Assert.assertNotNull(leftNavMenu);
        Assert.assertTrue(CollectionUtils.isEmpty(leftNavMenu.getChildren()));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetLeftNavigationMenuWithTwoLevelsContainingLink() {
        final String linkText = "Awesome Link";
        final String linkUrl = "http://www.target.com.au/";


        final CMSNavigationNodeModel mockRootNode = createMockNavigationNode(null);
        final CMSNavigationNodeModel mockContentRootNode = createMockNavigationNode(mockRootNode);


        // Level 1
        final CMSNavigationNodeModel mockPageNode = createMockNavigationNode(mockContentRootNode);
        BDDMockito.given(mockPageNode.isVisible()).willReturn(Boolean.TRUE);

        final List<CMSNavigationNodeModel> navNodesForPage = new ArrayList<>();
        navNodesForPage.add(mockPageNode);

        final ContentPageModel mockContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockContentPage.getNavigationNodeList()).willReturn(navNodesForPage);

        final CMSNavigationEntryModel mockContentPageEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockContentPageEntry.getItem()).willReturn(mockContentPage);

        final List<CMSNavigationEntryModel> entriesForPageNode = new ArrayList<>();
        entriesForPageNode.add(mockContentPageEntry);
        BDDMockito.given(mockPageNode.getEntries()).willReturn(entriesForPageNode);


        // Level 2
        final CMSNavigationNodeModel mockSecondLevelNode = createMockNavigationNode(mockPageNode);
        BDDMockito.given(mockSecondLevelNode.isVisible()).willReturn(Boolean.TRUE);

        final List<CMSNavigationNodeModel> navNodesForSecondLevelPage = new ArrayList<>();
        navNodesForSecondLevelPage.add(mockSecondLevelNode);

        final CMSLinkComponentModel mockLink = Mockito.mock(CMSLinkComponentModel.class);
        BDDMockito.given(mockLink.getVisible()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockLink.getLinkName()).willReturn(linkText);
        BDDMockito.given(mockLink.getUrl()).willReturn(linkUrl);
        BDDMockito.given(mockLink.getTarget()).willReturn(LinkTargets.NEWWINDOW);
        BDDMockito.given(mockLink.isExternal()).willReturn(Boolean.TRUE);

        final CMSNavigationEntryModel mockSecondLevelContentPageEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockSecondLevelContentPageEntry.getItem()).willReturn(mockLink);

        final List<CMSNavigationEntryModel> entriesForSecondLevelPageNode = new ArrayList<>();
        entriesForSecondLevelPageNode.add(mockSecondLevelContentPageEntry);
        BDDMockito.given(mockSecondLevelNode.getEntries()).willReturn(entriesForSecondLevelPageNode);

        final NavigationMenuItem leftNavMenu = leftNavigationMenuBuilder
                .getLeftNavigationMenu(mockContentPage);

        // Validate the top level
        Assert.assertNotNull(leftNavMenu);
        Assert.assertTrue(CollectionUtils.isNotEmpty(leftNavMenu.getChildren()));
        Assert.assertEquals(1, leftNavMenu.getChildren().size());


        // Validate second level
        final NavigationMenuItem secondLevel = leftNavMenu.getChildren().iterator().next();

        Assert.assertEquals(linkText, secondLevel.getName());
        Assert.assertEquals(linkUrl, secondLevel.getLink());
        Assert.assertTrue(secondLevel.isExternal());
        Assert.assertTrue(secondLevel.isNewWindow());
        Assert.assertFalse(secondLevel.isCurrentPage());
        Assert.assertFalse(secondLevel.isCurrentPageInChildren());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetLeftNavigationMenuWithTwoLevelsContainingNotVisibleLink() {
        final CMSNavigationNodeModel mockRootNode = createMockNavigationNode(null);
        final CMSNavigationNodeModel mockContentRootNode = createMockNavigationNode(mockRootNode);


        // Level 1
        final CMSNavigationNodeModel mockPageNode = createMockNavigationNode(mockContentRootNode);
        BDDMockito.given(mockPageNode.isVisible()).willReturn(Boolean.TRUE);

        final List<CMSNavigationNodeModel> navNodesForPage = new ArrayList<>();
        navNodesForPage.add(mockPageNode);

        final ContentPageModel mockContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockContentPage.getNavigationNodeList()).willReturn(navNodesForPage);

        final CMSNavigationEntryModel mockContentPageEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockContentPageEntry.getItem()).willReturn(mockContentPage);

        final List<CMSNavigationEntryModel> entriesForPageNode = new ArrayList<>();
        entriesForPageNode.add(mockContentPageEntry);
        BDDMockito.given(mockPageNode.getEntries()).willReturn(entriesForPageNode);


        // Level 2
        final CMSNavigationNodeModel mockSecondLevelNode = createMockNavigationNode(mockPageNode);
        BDDMockito.given(mockSecondLevelNode.isVisible()).willReturn(Boolean.TRUE);

        final List<CMSNavigationNodeModel> navNodesForSecondLevelPage = new ArrayList<>();
        navNodesForSecondLevelPage.add(mockSecondLevelNode);

        final CMSLinkComponentModel mockLink = Mockito.mock(CMSLinkComponentModel.class);
        BDDMockito.given(mockLink.getVisible()).willReturn(Boolean.FALSE);

        final CMSNavigationEntryModel mockSecondLevelContentPageEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockSecondLevelContentPageEntry.getItem()).willReturn(mockLink);

        final List<CMSNavigationEntryModel> entriesForSecondLevelPageNode = new ArrayList<>();
        entriesForSecondLevelPageNode.add(mockSecondLevelContentPageEntry);
        BDDMockito.given(mockSecondLevelNode.getEntries()).willReturn(entriesForSecondLevelPageNode);

        final NavigationMenuItem leftNavMenu = leftNavigationMenuBuilder
                .getLeftNavigationMenu(mockContentPage);

        // Validate the top level
        Assert.assertNotNull(leftNavMenu);
        Assert.assertTrue(CollectionUtils.isEmpty(leftNavMenu.getChildren()));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetLeftNavigationMenuWithTwoLevelsContainingNotRecognisedType() {
        final CMSNavigationNodeModel mockRootNode = createMockNavigationNode(null);
        final CMSNavigationNodeModel mockContentRootNode = createMockNavigationNode(mockRootNode);


        // Level 1
        final CMSNavigationNodeModel mockPageNode = createMockNavigationNode(mockContentRootNode);
        BDDMockito.given(mockPageNode.isVisible()).willReturn(Boolean.TRUE);

        final List<CMSNavigationNodeModel> navNodesForPage = new ArrayList<>();
        navNodesForPage.add(mockPageNode);

        final ContentPageModel mockContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockContentPage.getNavigationNodeList()).willReturn(navNodesForPage);

        final CMSNavigationEntryModel mockContentPageEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockContentPageEntry.getItem()).willReturn(mockContentPage);

        final List<CMSNavigationEntryModel> entriesForPageNode = new ArrayList<>();
        entriesForPageNode.add(mockContentPageEntry);
        BDDMockito.given(mockPageNode.getEntries()).willReturn(entriesForPageNode);


        // Level 2
        final CMSNavigationNodeModel mockSecondLevelNode = createMockNavigationNode(mockPageNode);
        BDDMockito.given(mockSecondLevelNode.isVisible()).willReturn(Boolean.TRUE);

        final List<CMSNavigationNodeModel> navNodesForSecondLevelPage = new ArrayList<>();
        navNodesForSecondLevelPage.add(mockSecondLevelNode);

        final NavigationBarComponentModel mockNavBarComponent = Mockito.mock(NavigationBarComponentModel.class);

        final CMSNavigationEntryModel mockSecondLevelContentPageEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockSecondLevelContentPageEntry.getItem()).willReturn(mockNavBarComponent);

        final List<CMSNavigationEntryModel> entriesForSecondLevelPageNode = new ArrayList<>();
        entriesForSecondLevelPageNode.add(mockSecondLevelContentPageEntry);
        BDDMockito.given(mockSecondLevelNode.getEntries()).willReturn(entriesForSecondLevelPageNode);

        final NavigationMenuItem leftNavMenu = leftNavigationMenuBuilder
                .getLeftNavigationMenu(mockContentPage);

        // Validate the top level
        Assert.assertNotNull(leftNavMenu);
        Assert.assertTrue(CollectionUtils.isEmpty(leftNavMenu.getChildren()));
    }

    private CMSNavigationNodeModel createMockNavigationNode(final CMSNavigationNodeModel mockParentNode) {
        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);

        BDDMockito.given(mockNavigationNode.getParent()).willReturn(mockParentNode);

        final List<CMSNavigationNodeModel> childNavigationNodeList = new ArrayList<>();
        BDDMockito.given(mockNavigationNode.getChildren()).willReturn(childNavigationNodeList);

        // Add child to the parent
        if (mockParentNode != null) {
            mockParentNode.getChildren().add(mockNavigationNode);
        }

        return mockNavigationNode;
    }
}
