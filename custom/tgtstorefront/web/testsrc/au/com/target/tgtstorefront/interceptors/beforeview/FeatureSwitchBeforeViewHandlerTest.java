/**
 * 
 */
package au.com.target.tgtstorefront.interceptors.beforeview;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtutility.util.JsonConversionUtil;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FeatureSwitchBeforeViewHandlerTest {

    @InjectMocks
    @Spy
    private final FeatureSwitchBeforeViewHandler featureSwitchBeforeViewHandler = new FeatureSwitchBeforeViewHandler();

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Mock
    private Map<String, Boolean> mockMapFeaturesEnabled;

    @Mock
    private Map<String, Boolean> mockMapUIFeaturesEnabled;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Mock
    private ModelAndView mockModelAndView;


    @Before
    public void setUp() {
        when(targetFeatureSwitchFacade.getEnabledFeatureMap(false)).thenReturn(mockMapFeaturesEnabled);
        when(targetFeatureSwitchFacade.getEnabledFeatureMap(true)).thenReturn(mockMapUIFeaturesEnabled);

    }

    @Test
    public void testBeforeViewFeatureSwitchEnabled() {

        featureSwitchBeforeViewHandler.beforeView(httpServletRequest, httpServletResponse, mockModelAndView);

        verify(mockModelAndView).addObject(FeatureSwitchBeforeViewHandler.FEATURES_ENABLED, mockMapFeaturesEnabled);
    }

    /**
     * This method will verify to add feature switches UI-enabled.
     */
    @Test
    public void testBeforeViewUIFeatureSwitchEnabled() {

        featureSwitchBeforeViewHandler.beforeView(httpServletRequest, httpServletResponse, mockModelAndView);

        verify(mockModelAndView).addObject(FeatureSwitchBeforeViewHandler.UI_FEATURES_ENABLED,
                JsonConversionUtil.convertToJsonString(mockMapUIFeaturesEnabled));

    }
}
