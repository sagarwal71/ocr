/**
 * 
 */
package au.com.target.tgtstorefront.controllers.services;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SpcSurveyEndControllerTest {

    @InjectMocks
    private final SpcSurveyEndController controller = new SpcSurveyEndController();

    @Test
    public void testSurveyEnd() {
        final Model model = mock(Model.class);
        final String view = controller.surveyEnd(model);
        assertThat(view).isEqualTo(ControllerConstants.Views.Pages.Checkout.CHECKOUT_SPC_IPG_RETURN_PAGE);
        verify(model).addAttribute("postMessageActionType", "SURVEY_END");
    }

}
