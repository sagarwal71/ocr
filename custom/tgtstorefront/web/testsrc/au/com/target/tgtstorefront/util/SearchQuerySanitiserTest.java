/**
 * 
 */
package au.com.target.tgtstorefront.util;

import org.junit.Assert;

import org.junit.Test;


/**
 * @author rmcalave
 * 
 */
public class SearchQuerySanitiserTest {
    private final SearchQuerySanitiser searchQuerySanitiser = new SearchQuerySanitiser("[^a-zA-Z]");

    private final SearchQuerySanitiser instoreStockLocationSearchQuery = new SearchQuerySanitiser(
            "[^a-zA-Z0-9\\'\\-]");

    @Test(expected = IllegalArgumentException.class)
    public void testSanitiseSearchTextWithNoPattern() {
        new SearchQuerySanitiser(null);
    }

    @Test
    public void testSanitiseSearchTextWithBlankString() {
        final String result = searchQuerySanitiser.sanitiseSearchText("");

        Assert.assertEquals("", result);
    }

    @Test
    public void testSanitiseSearchTextWithValidString() {
        final String testString = "A bicycle";

        final String result = searchQuerySanitiser.sanitiseSearchText(testString);

        Assert.assertEquals(testString, result);
    }

    @Test
    public void testSanitiseSearchTextWithInvalidCharacters() {
        final String testString = "A b7i@c*ycle";

        final String result = searchQuerySanitiser.sanitiseSearchText(testString);

        Assert.assertEquals("A b i c ycle", result);
    }

    @Test
    public void testSanitiseSearchTextWithPaddingOnTheEnds() {
        final String testString = "      A bicycle     ";

        final String result = searchQuerySanitiser.sanitiseSearchText(testString);

        Assert.assertEquals("A bicycle", result);
    }

    @Test
    public void testSanitiseSearchTextWithInvalidCharactersOnTheEnds() {
        final String testString = "@(66!)A bicycle^#)8@";

        final String result = searchQuerySanitiser.sanitiseSearchText(testString);

        Assert.assertEquals("A bicycle", result);
    }

    @Test
    public void testSanitiseSearchTextWithJavaScriptIMGTag() {
        final String testString = "<img src=x onerror='javascript:window.onerror=alert;throw 'test''>";
        final String result = searchQuerySanitiser.sanitiseSearchText(testString);
        Assert.assertEquals(
                "lt img src x onerror  javascript window onerror alert throw  test   gt",
                result);
    }

    @Test
    public void testSanitiseSearchTextWithJavaScriptLet() {
        final String testString = "<script>alert('e')</script>";
        final String result = searchQuerySanitiser.sanitiseSearchText(testString);
        Assert.assertEquals("lt script gt alert  e   lt  script gt", result);
    }

    @Test
    public void testSanitiseInStoreLocationInvalidChar() {
        final String testString = "madras   9e3-25=#%-9";
        final String result = instoreStockLocationSearchQuery.sanitiseSearchText(testString);
        Assert.assertEquals("madras   9e3-25   -9", result);
    }

    @Test
    public void testSanitiseInStoreLocationTrailingandLeadingSpaces() {
        final String testString = "   katoomba  ";
        final String result = instoreStockLocationSearchQuery.sanitiseSearchText(testString);
        Assert.assertEquals("katoomba", result);
    }

    @Test
    public void testSanitiseInStoreLocationWithquotes() {
        final String testString = "O'brien    ";
        final String result = instoreStockLocationSearchQuery.sanitiseSearchText(testString);
        Assert.assertEquals("O'brien", result);
    }

    @Test
    public void testSanitiseInStoreLocationWithParanthesis() {
        final String testString = "(para-ma%%%ta(tta    ";
        final String result = instoreStockLocationSearchQuery.sanitiseSearchText(testString);
        Assert.assertEquals("para-ma   ta tta", result);
    }

    @Test
    public void testSanitiseInStoreLocationNumbers() {
        final String testString = "3215-5678#%&   ";
        final String result = instoreStockLocationSearchQuery.sanitiseSearchText(testString);
        Assert.assertEquals("3215-5678", result);
    }

    @Test
    public void testSanitiseInStoreLocationNumbersWithSplCharacters() {
        final String testString = "321+5-5678@#$%^#%&!&*()|~   ";
        final String result = instoreStockLocationSearchQuery.sanitiseSearchText(testString);
        Assert.assertEquals("321 5-5678", result);
    }


    @Test
    public void testSanitiseInStoreLocationNumbersWithSplCharacters1() {
        final String testString = "^*@$#^AliceSprings|{}/&^$$$$ ";
        final String result = instoreStockLocationSearchQuery.sanitiseSearchText(testString);
        Assert.assertEquals("AliceSprings", result);
    }


}
