/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.order.TargetCartFacade;
import au.com.target.tgtfacades.order.TargetPlaceOrderFacade;
import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;
import au.com.target.tgtfacades.order.data.TargetPlaceOrderRequest;
import au.com.target.tgtfacades.order.data.TargetPlaceOrderResult;
import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderAfterpayPaymentInfoData;
import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderPayPalPaymentInfoData;
import au.com.target.tgtfacades.order.enums.TargetPlaceOrderResultEnum;
import au.com.target.tgtfacades.response.data.PlaceOrderResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.AddressDataHelper;
import au.com.target.tgtstorefront.controllers.util.CartDataHelper;
import au.com.target.tgtstorefront.controllers.util.CookieStringBuilder;
import au.com.target.tgtstorefront.controllers.util.RequestHeadersHelper;
import au.com.target.tgtstorefront.controllers.util.TargetPaymentHelper;
import au.com.target.tgtstorefront.servlet.support.TargetFlashMapManager;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SpcPlaceOrderControllerTest {

    @InjectMocks
    private final SpcPlaceOrderController controller = new SpcPlaceOrderController();
    private final String ipgToken = "token";
    private final String ipgSessionId = "sessionId";

    private final String afterpayToken = "lviap9tgm75v31vif75tdcr3ect125nc8e30vc7oprnnngch78r6";
    private static final String ZIPPAYMENT_PREFIX = "ZIPPAYMENT_";
    private final FlashMap flashMap = new FlashMap();

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse httpResponse;
    @Mock
    private HttpSession session;
    @Mock
    private TargetPlaceOrderFacade placeOrderFacade;
    @Mock
    private TargetFlashMapManager flashMapManager;
    @Mock
    private TargetPlaceOrderResult placeOrderResult;
    @Mock
    private RequestHeadersHelper requestHeadersHelper;
    @Mock
    private AddressDataHelper addressDataHelper;
    @Mock
    private CartDataHelper cartDataHelper;
    @Mock
    private CookieStringBuilder cookieStringBuilder;
    @Mock
    private RedirectAttributes redirectAttributes;
    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;
    @Mock
    private TargetPaymentHelper targetPaymentHelper;

    private static final String VENDOR_ERROR_CODE_PREFIX = "ERR_PLACEORDER_";

    @Mock
    private TargetCartFacade targetCartFacade;

    @Before
    public void setUp() {
        given(request.getAttribute(DispatcherServlet.OUTPUT_FLASH_MAP_ATTRIBUTE)).willReturn(flashMap);
        given(request.getSession()).willReturn(session);
        doNothing().when(flashMapManager).saveFlashMapToOutput((FlashMap)any(Object.class),
                (HttpServletRequest)any(Object.class),
                (HttpServletResponse)any(Object.class));
    }

    @Test
    public void testPlaceOrderSuccessfullyViaSPC() throws CMSItemNotFoundException, InvalidCartException {
        final String orderCode = "00010000";
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        given(placeOrderResult.getPlaceOrderResult()).willReturn(TargetPlaceOrderResultEnum.SUCCESS);
        given(placeOrderResult.getCartCode()).willReturn(orderCode);
        given(placeOrderFacade.placeOrder(any(TargetPlaceOrderRequest.class))).willReturn(placeOrderResult);
        given(session.getAttribute(ipgToken)).willReturn(ipgToken);
        given(request.getParameter(ControllerConstants.IPG_PARAM_TOKEN)).willReturn(ipgToken);
        given(orderModel.getCode()).willReturn(orderCode);
        given(placeOrderResult.getAbstractOrderModel()).willReturn(orderModel);

        final Response response = controller.placeIpgOrder(ipgToken, ipgSessionId, request, httpResponse);
        verify(session).setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE, orderCode);
        verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN);
        final PlaceOrderResponseData placeOrderResponseData = (PlaceOrderResponseData)response.getData();
        assertThat(response.isSuccess()).isTrue();
        assertThat(placeOrderResponseData.getRedirectUrl()).isEqualTo("/checkout/thank-you/" + orderCode);
    }

    @Test
    public void testPlaceOrderSuccessfullyViaSPCWithNewThankyouPage() throws CMSItemNotFoundException,
            InvalidCartException {
        final String orderCode = "00010000";
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        given(placeOrderResult.getPlaceOrderResult()).willReturn(TargetPlaceOrderResultEnum.SUCCESS);
        given(placeOrderResult.getCartCode()).willReturn(orderCode);
        given(placeOrderFacade.placeOrder(any(TargetPlaceOrderRequest.class))).willReturn(placeOrderResult);
        given(session.getAttribute(ipgToken)).willReturn(ipgToken);
        given(request.getParameter(ControllerConstants.IPG_PARAM_TOKEN)).willReturn(ipgToken);
        given(orderModel.getCode()).willReturn(orderCode);
        given(placeOrderResult.getAbstractOrderModel()).willReturn(orderModel);
        given(Boolean.valueOf(targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU)))
                .willReturn(Boolean.TRUE);
        final Response response = controller.placeIpgOrder(ipgToken, ipgSessionId, request, httpResponse);
        verify(session).setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE, orderCode);
        verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN);
        final PlaceOrderResponseData placeOrderResponseData = (PlaceOrderResponseData)response.getData();
        assertThat(response.isSuccess()).isTrue();
        assertThat(placeOrderResponseData.getRedirectUrl()).isEqualTo(
                ControllerConstants.SINGLE_PAGE_CHECKOUT_AND_LOGIN_THANKYOU + orderCode);
    }

    @Test
    public void testPlaceOrderPartialSuccessfullyViaSPC() throws CMSItemNotFoundException, InvalidCartException {
        final String orderCode = "00010000";
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        final AdjustedCartEntriesData adjustedData = mock(AdjustedCartEntriesData.class);
        given(placeOrderResult.getPlaceOrderResult()).willReturn(TargetPlaceOrderResultEnum.SUCCESS);
        given(placeOrderResult.getCartCode()).willReturn(orderCode);
        given(placeOrderFacade.placeOrder(any(TargetPlaceOrderRequest.class))).willReturn(placeOrderResult);
        given(session.getAttribute(ipgToken)).willReturn(ipgToken);
        given(request.getParameter(ControllerConstants.IPG_PARAM_TOKEN)).willReturn(ipgToken);
        given(orderModel.getCode()).willReturn(orderCode);
        given(placeOrderResult.getAbstractOrderModel()).willReturn(orderModel);
        willReturn(Boolean.TRUE).given(adjustedData).isAdjusted();
        given(placeOrderResult.getAdjustedCartEntriesData()).willReturn(adjustedData);

        final Response response = controller.placeIpgOrder(ipgToken, ipgSessionId, request, httpResponse);
        verify(session).setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE, orderCode);
        verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN);
        final PlaceOrderResponseData placeOrderResponseData = (PlaceOrderResponseData)response.getData();
        assertThat(response.isSuccess()).isTrue();
        assertThat(placeOrderResponseData.getRedirectUrl()).isEqualTo("/checkout/thank-you/" + orderCode);
        assertThat(placeOrderResponseData.getSohUpdates()).isEqualTo(adjustedData);
        assertThat(flashMap.containsKey(ControllerConstants.SOH_UPDATES)).isTrue();
        verify(session, never()).setAttribute(ControllerConstants.SOH_UPDATES, adjustedData);
    }

    @Test
    public void testPlaceOrderPartialSuccessfullyViaSPCWithNewThankYou()
            throws CMSItemNotFoundException, InvalidCartException {
        final String orderCode = "00010000";
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        final AdjustedCartEntriesData adjustedData = mock(AdjustedCartEntriesData.class);
        given(placeOrderResult.getPlaceOrderResult()).willReturn(TargetPlaceOrderResultEnum.SUCCESS);
        given(placeOrderResult.getCartCode()).willReturn(orderCode);
        given(placeOrderFacade.placeOrder(any(TargetPlaceOrderRequest.class))).willReturn(placeOrderResult);
        given(session.getAttribute(ipgToken)).willReturn(ipgToken);
        given(request.getParameter(ControllerConstants.IPG_PARAM_TOKEN)).willReturn(ipgToken);
        given(orderModel.getCode()).willReturn(orderCode);
        given(placeOrderResult.getAbstractOrderModel()).willReturn(orderModel);
        willReturn(Boolean.TRUE).given(adjustedData).isAdjusted();
        given(placeOrderResult.getAdjustedCartEntriesData()).willReturn(adjustedData);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU);

        final Response response = controller.placeIpgOrder(ipgToken, ipgSessionId, request, httpResponse);
        verify(session).setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE, orderCode);
        verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN);
        final PlaceOrderResponseData placeOrderResponseData = (PlaceOrderResponseData)response.getData();
        assertThat(response.isSuccess()).isTrue();
        assertThat(placeOrderResponseData.getRedirectUrl()).isEqualTo("/spc/order/thankyou/" + orderCode);
        assertThat(placeOrderResponseData.getSohUpdates()).isEqualTo(adjustedData);
        assertThat(flashMap.containsKey(ControllerConstants.SOH_UPDATES)).isTrue();
        verify(session).setAttribute(ControllerConstants.SOH_UPDATES, adjustedData);
    }

    @Test
    public void testPlaceOrderFailedUnknownViaSPC() throws CMSItemNotFoundException, InvalidCartException {
        final String orderCode = "00010000";
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        given(placeOrderResult.getPlaceOrderResult()).willReturn(TargetPlaceOrderResultEnum.UNKNOWN);
        given(placeOrderResult.getCartCode()).willReturn(orderCode);
        given(placeOrderFacade.placeOrder(any(TargetPlaceOrderRequest.class))).willReturn(placeOrderResult);
        given(session.getAttribute(ipgToken)).willReturn(ipgToken);
        given(request.getParameter(ControllerConstants.IPG_PARAM_TOKEN)).willReturn(ipgToken);
        given(orderModel.getCode()).willReturn(orderCode);
        given(placeOrderResult.getAbstractOrderModel()).willReturn(orderModel);

        final Response response = controller.placeIpgOrder(ipgToken, ipgSessionId, request, httpResponse);
        final PlaceOrderResponseData placeOrderResponseData = (PlaceOrderResponseData)response.getData();
        assertThat(response.isSuccess()).isFalse();
        assertThat(placeOrderResponseData.getError().getCode()).isEqualTo("ERR_PLACEORDER_UNKNOWN");
        assertThat(placeOrderResponseData.getRedirectUrl()).isEqualTo("/checkout/thank-you/check?cart=00010000");
    }

    @Test
    public void testPlaceOrderFailedUnknownViaSPCWithoutOrderCode()
            throws CMSItemNotFoundException, InvalidCartException {
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        given(placeOrderResult.getPlaceOrderResult()).willReturn(TargetPlaceOrderResultEnum.UNKNOWN);
        given(placeOrderFacade.placeOrder(any(TargetPlaceOrderRequest.class))).willReturn(placeOrderResult);
        given(session.getAttribute(ipgToken)).willReturn(ipgToken);
        given(request.getParameter(ControllerConstants.IPG_PARAM_TOKEN)).willReturn(ipgToken);
        given(placeOrderResult.getAbstractOrderModel()).willReturn(orderModel);

        final Response response = controller.placeIpgOrder(ipgToken, ipgSessionId, request, httpResponse);
        final PlaceOrderResponseData placeOrderResponseData = (PlaceOrderResponseData)response.getData();
        assertThat(response.isSuccess()).isFalse();
        assertThat(placeOrderResponseData.getError().getCode()).isEqualTo("ERR_PLACEORDER_UNKNOWN");
        assertThat(placeOrderResponseData.getRedirectUrl()).isEqualTo("/checkout/thank-you/check?cart=");
    }

    @Test
    public void testPlaceOrderFailedUnknownViaSPCWithNewThankyoucheckPage() throws CMSItemNotFoundException,
            InvalidCartException {
        final String orderCode = "00010000";
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        given(placeOrderResult.getPlaceOrderResult()).willReturn(TargetPlaceOrderResultEnum.UNKNOWN);
        given(placeOrderResult.getCartCode()).willReturn(orderCode);
        given(placeOrderFacade.placeOrder(any(TargetPlaceOrderRequest.class))).willReturn(placeOrderResult);
        given(session.getAttribute(ipgToken)).willReturn(ipgToken);
        given(request.getParameter(ControllerConstants.IPG_PARAM_TOKEN)).willReturn(ipgToken);
        given(orderModel.getCode()).willReturn(orderCode);
        given(placeOrderResult.getAbstractOrderModel()).willReturn(orderModel);
        given(Boolean.valueOf(targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU)))
                .willReturn(Boolean.TRUE);
        final Response response = controller.placeIpgOrder(ipgToken, ipgSessionId, request, httpResponse);
        final PlaceOrderResponseData placeOrderResponseData = (PlaceOrderResponseData)response.getData();
        assertThat(response.isSuccess()).isFalse();
        assertThat(placeOrderResponseData.getError().getCode()).isEqualTo("ERR_PLACEORDER_UNKNOWN");
        assertThat(placeOrderResponseData.getRedirectUrl()).isEqualTo(
                ControllerConstants.SINGLE_PAGE_CHECKOUT_AND_LOGIN_THANKYOUCHECK + orderCode);
    }

    @Test
    public void testPlaceOrderFailedUnknownViaSPCWithNewThankyoucheckPageWithoutOrderCode()
            throws CMSItemNotFoundException, InvalidCartException {
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        given(placeOrderResult.getPlaceOrderResult()).willReturn(TargetPlaceOrderResultEnum.UNKNOWN);
        given(placeOrderFacade.placeOrder(any(TargetPlaceOrderRequest.class))).willReturn(placeOrderResult);
        given(session.getAttribute(ipgToken)).willReturn(ipgToken);
        given(request.getParameter(ControllerConstants.IPG_PARAM_TOKEN)).willReturn(ipgToken);
        given(placeOrderResult.getAbstractOrderModel()).willReturn(orderModel);
        given(targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU))
                .willReturn(Boolean.TRUE);
        final Response response = controller.placeIpgOrder(ipgToken, ipgSessionId, request, httpResponse);
        final PlaceOrderResponseData placeOrderResponseData = (PlaceOrderResponseData)response.getData();
        assertThat(response.isSuccess()).isFalse();
        assertThat(placeOrderResponseData.getError().getCode()).isEqualTo("ERR_PLACEORDER_UNKNOWN");
        assertThat(placeOrderResponseData.getRedirectUrl())
                .isEqualTo(ControllerConstants.SINGLE_PAGE_CHECKOUT_AND_LOGIN_THANKYOUCHECK);
    }

    @Test
    public void testPlaceOrderFailedInvalidCartViaSPC() throws CMSItemNotFoundException, InvalidCartException {
        final String orderCode = "00010000";
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        given(placeOrderResult.getPlaceOrderResult()).willReturn(TargetPlaceOrderResultEnum.INVALID_CART);
        given(placeOrderResult.getCartCode()).willReturn(orderCode);
        given(placeOrderFacade.placeOrder(any(TargetPlaceOrderRequest.class))).willReturn(placeOrderResult);
        given(session.getAttribute(ipgToken)).willReturn(ipgToken);
        given(request.getParameter(ControllerConstants.IPG_PARAM_TOKEN)).willReturn(ipgToken);
        given(orderModel.getCode()).willReturn(orderCode);
        given(placeOrderResult.getAbstractOrderModel()).willReturn(orderModel);

        final Response response = controller.placeIpgOrder(ipgToken, ipgSessionId, request, httpResponse);
        final PlaceOrderResponseData placeOrderResponseData = (PlaceOrderResponseData)response.getData();
        assertThat(response.isSuccess()).isFalse();
        assertThat(placeOrderResponseData.getError().getCode()).isEqualTo("ERR_PLACEORDER_INVALID_CART");
        assertThat(placeOrderResponseData.getRedirectUrl()).isEqualTo("/basket");
    }

    @Test
    public void testPlaceOrderFailedMissingProductViaSPC() throws CMSItemNotFoundException, InvalidCartException {
        final String orderCode = "00010000";
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        given(placeOrderResult.getPlaceOrderResult()).willReturn(TargetPlaceOrderResultEnum.INVALID_PRODUCT_IN_CART);
        given(placeOrderResult.getCartCode()).willReturn(orderCode);
        final AdjustedCartEntriesData adjustedData = mock(AdjustedCartEntriesData.class);
        willReturn(Boolean.TRUE).given(adjustedData).isAdjusted();
        given(placeOrderResult.getAdjustedCartEntriesData()).willReturn(adjustedData);
        given(placeOrderFacade.placeOrder(any(TargetPlaceOrderRequest.class))).willReturn(placeOrderResult);
        given(session.getAttribute(ipgToken)).willReturn(ipgToken);
        given(request.getParameter(ControllerConstants.IPG_PARAM_TOKEN)).willReturn(ipgToken);
        given(orderModel.getCode()).willReturn(orderCode);
        given(placeOrderResult.getAbstractOrderModel()).willReturn(orderModel);

        final Response response = controller.placeIpgOrder(ipgToken, ipgSessionId, request, httpResponse);
        final PlaceOrderResponseData placeOrderResponseData = (PlaceOrderResponseData)response.getData();
        assertThat(response.isSuccess()).isFalse();
        assertThat(placeOrderResponseData.getError().getCode()).isEqualTo("ERR_PLACEORDER_INVALID_PRODUCT_IN_CART");
        assertThat(placeOrderResponseData.getRedirectUrl()).isEqualTo("/basket");
        assertThat(placeOrderResponseData.getSohUpdates()).isEqualTo(adjustedData);
        verify(session, never()).setAttribute(ControllerConstants.SOH_UPDATES, adjustedData);
    }

    @Test
    public void testPlaceOrderFailedMissingProductViaSPCWithNewThankYou()
            throws CMSItemNotFoundException, InvalidCartException {
        final String orderCode = "00010000";
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        given(placeOrderResult.getPlaceOrderResult()).willReturn(TargetPlaceOrderResultEnum.INVALID_PRODUCT_IN_CART);
        given(placeOrderResult.getCartCode()).willReturn(orderCode);
        final AdjustedCartEntriesData adjustedData = mock(AdjustedCartEntriesData.class);
        willReturn(Boolean.TRUE).given(adjustedData).isAdjusted();
        given(placeOrderResult.getAdjustedCartEntriesData()).willReturn(adjustedData);
        given(placeOrderFacade.placeOrder(any(TargetPlaceOrderRequest.class))).willReturn(placeOrderResult);
        given(session.getAttribute(ipgToken)).willReturn(ipgToken);
        given(request.getParameter(ControllerConstants.IPG_PARAM_TOKEN)).willReturn(ipgToken);
        given(orderModel.getCode()).willReturn(orderCode);
        given(placeOrderResult.getAbstractOrderModel()).willReturn(orderModel);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU);

        final Response response = controller.placeIpgOrder(ipgToken, ipgSessionId, request, httpResponse);
        final PlaceOrderResponseData placeOrderResponseData = (PlaceOrderResponseData)response.getData();
        assertThat(response.isSuccess()).isFalse();
        assertThat(placeOrderResponseData.getError().getCode()).isEqualTo("ERR_PLACEORDER_INVALID_PRODUCT_IN_CART");
        assertThat(placeOrderResponseData.getRedirectUrl()).isEqualTo("/basket");
        assertThat(placeOrderResponseData.getSohUpdates()).isEqualTo(adjustedData);
        verify(session, never()).setAttribute(ControllerConstants.SOH_UPDATES, adjustedData);
    }

    @Test
    public void testPlaceOrderAgainViaSPC() throws CMSItemNotFoundException, InvalidCartException {
        final String orderCode = "00010000";
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        given(placeOrderResult.getPlaceOrderResult()).willReturn(TargetPlaceOrderResultEnum.UNKNOWN);
        given(placeOrderFacade.placeOrder(any(TargetPlaceOrderRequest.class))).willReturn(placeOrderResult);
        given(session.getAttribute(ipgToken)).willReturn(null);
        given(request.getParameter(ControllerConstants.IPG_PARAM_TOKEN)).willReturn(ipgToken);
        given(orderModel.getCode()).willReturn(orderCode);
        given(placeOrderResult.getAbstractOrderModel()).willReturn(orderModel);

        final Response response = controller.placeIpgOrder(ipgToken, ipgSessionId, request, httpResponse);
        final PlaceOrderResponseData placeOrderResponseData = (PlaceOrderResponseData)response.getData();
        verifyZeroInteractions(placeOrderFacade);
        assertThat(response.isSuccess()).isFalse();
        assertThat(placeOrderResponseData.getRedirectUrl()).isNull();
    }

    @Test
    public void testPlaceOrderWithNoResultReturnC() throws CMSItemNotFoundException, InvalidCartException {
        given(placeOrderFacade.placeOrder(any(TargetPlaceOrderRequest.class))).willReturn(null);
        given(session.getAttribute(ipgToken)).willReturn(ipgToken);
        given(request.getParameter(ControllerConstants.IPG_PARAM_TOKEN)).willReturn(ipgToken);

        final Response response = controller.placeIpgOrder(ipgToken, ipgSessionId, request, httpResponse);
        final PlaceOrderResponseData placeOrderResponseData = (PlaceOrderResponseData)response.getData();
        assertThat(response.isSuccess()).isFalse();
        assertThat(placeOrderResponseData.getRedirectUrl()).isNull();
        assertThat(placeOrderResponseData.getError().getCode()).isEqualTo("ERR_PLACEORDER");
    }

    @Test
    public void placeOrderPaypalReturnsNoResult() {
        given(placeOrderFacade.placeOrder(Mockito.any(TargetPlaceOrderRequest.class))).willReturn(null);

        final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfo = new TargetPlaceOrderPayPalPaymentInfoData();
        payPalPaymentInfo.setPaypalPayerId("id");
        payPalPaymentInfo.setPaypalSessionToken("token");

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(payPalPaymentInfo);
        final HttpServletRequest mockRequest = new MockHttpServletRequest();
        final String responseUrl = controller.fromPaypal("paypalPayer", "token", mockRequest, redirectAttributes);
        assertThat(responseUrl).isEqualTo(ControllerConstants.Redirection.CART);
    }

    @Test
    public void testPlaceOrderPaypalFailedMissingProductViaSPC() throws CMSItemNotFoundException, InvalidCartException {
        final HttpServletRequest mockRequest = new MockHttpServletRequest();
        given(placeOrderResult.getPlaceOrderResult()).willReturn(TargetPlaceOrderResultEnum.INVALID_PRODUCT_IN_CART);
        given(placeOrderFacade.placeOrder(any(TargetPlaceOrderRequest.class))).willReturn(placeOrderResult);

        final String responseUrl = controller.fromPaypal("paypalPayer", "token", mockRequest, redirectAttributes);
        assertThat(responseUrl).isEqualTo(ControllerConstants.Redirection.CART);
    }

    @Test
    public void placeOrderPaypalHasNoPlaceOrderResult() {
        given(placeOrderFacade.placeOrder(Mockito.any(TargetPlaceOrderRequest.class))).willReturn(null);

        final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfo = new TargetPlaceOrderPayPalPaymentInfoData();
        payPalPaymentInfo.setPaypalPayerId("id");
        payPalPaymentInfo.setPaypalSessionToken("token");

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(payPalPaymentInfo);
        final HttpServletRequest mockRequest = new MockHttpServletRequest();
        final String responseUrl = controller.fromPaypal("paypalPayer", "token", mockRequest, redirectAttributes);
        assertThat(responseUrl).isEqualTo(
                ControllerConstants.Redirection.CART);
    }

    @Test
    public void placeOrderPaypalReturnsFailure() {
        final TargetPlaceOrderResult expectedPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.PAYMENT_FAILURE,
                null);
        expectedPlaceOrderResult.setCartCode("1234");
        given(placeOrderFacade.placeOrder(Mockito.any(TargetPlaceOrderRequest.class))).willReturn(
                expectedPlaceOrderResult);

        final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfo = new TargetPlaceOrderPayPalPaymentInfoData();
        payPalPaymentInfo.setPaypalPayerId("id");
        payPalPaymentInfo.setPaypalSessionToken("token");

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(payPalPaymentInfo);
        final HttpServletRequest mockRequest = new MockHttpServletRequest();
        final String responseUrl = controller.fromPaypal("paypalPayer", "token", mockRequest, redirectAttributes);
        assertThat(responseUrl).isEqualTo(
                "redirect:/spc/order/place-order/error?e=RVJSX1BMQUNFT1JERVJfUEFZTUVOVF9GQUlMVVJF");
    }

    @Test
    public void placeOrderPaypalReturnsSuccessWithoutOrderCode() {
        final TargetPlaceOrderResult expectedPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.SUCCESS, null);
        expectedPlaceOrderResult.setCartCode(null);
        given(placeOrderFacade.placeOrder(Mockito.any(TargetPlaceOrderRequest.class))).willReturn(
                expectedPlaceOrderResult);

        final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfo = new TargetPlaceOrderPayPalPaymentInfoData();
        payPalPaymentInfo.setPaypalPayerId("id");
        payPalPaymentInfo.setPaypalSessionToken("token");

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(payPalPaymentInfo);
        final HttpServletRequest mockRequest = new MockHttpServletRequest();
        final String responseUrl = controller.fromPaypal("paypalPayer", "token", mockRequest, redirectAttributes);
        assertThat(responseUrl).isEqualTo("redirect:/checkout/thank-you/");
    }

    @Test
    public void placeOrderPaypalReturnsSuccess() {
        final TargetPlaceOrderResult expectedPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.SUCCESS, null);
        expectedPlaceOrderResult.setCartCode("1234");
        given(placeOrderFacade.placeOrder(Mockito.any(TargetPlaceOrderRequest.class))).willReturn(
                expectedPlaceOrderResult);

        final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfo = new TargetPlaceOrderPayPalPaymentInfoData();
        payPalPaymentInfo.setPaypalPayerId("id");
        payPalPaymentInfo.setPaypalSessionToken("token");

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(payPalPaymentInfo);
        final HttpServletRequest mockRequest = new MockHttpServletRequest();
        final String responseUrl = controller.fromPaypal("paypalPayer", "token", mockRequest, redirectAttributes);
        assertThat(responseUrl).isEqualTo("redirect:/checkout/thank-you/1234");
    }

    @Test
    public void getPlaceOrderErrorUnknown() {
        final TargetPlaceOrderResult expectedPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.UNKNOWN,
                null);
        expectedPlaceOrderResult.setCartCode("1234");
        given(placeOrderFacade.placeOrder(Mockito.any(TargetPlaceOrderRequest.class))).willReturn(
                expectedPlaceOrderResult);

        final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfo = new TargetPlaceOrderPayPalPaymentInfoData();
        payPalPaymentInfo.setPaypalPayerId("id");
        payPalPaymentInfo.setPaypalSessionToken("token");

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(payPalPaymentInfo);
        final HttpServletRequest mockRequest = new MockHttpServletRequest();
        final String responseUrl = controller.fromPaypal("paypalPayer", "token", mockRequest, redirectAttributes);
        assertThat(responseUrl).isEqualTo("redirect:/checkout/thank-you/check?cart=1234");
    }

    @Test
    public void getPlaceOrderErrorUnknownWithNewThankyoucheckPage() {
        final TargetPlaceOrderResult expectedPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.UNKNOWN,
                null);
        final String cartCode = "1234";
        expectedPlaceOrderResult.setCartCode(cartCode);
        given(placeOrderFacade.placeOrder(Mockito.any(TargetPlaceOrderRequest.class))).willReturn(
                expectedPlaceOrderResult);
        given(Boolean.valueOf(targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU)))
                .willReturn(Boolean.TRUE);

        final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfo = new TargetPlaceOrderPayPalPaymentInfoData();
        payPalPaymentInfo.setPaypalPayerId("id");
        payPalPaymentInfo.setPaypalSessionToken("token");

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(payPalPaymentInfo);
        final HttpServletRequest mockRequest = new MockHttpServletRequest();
        final String responseUrl = controller.fromPaypal("paypalPayer", "token", mockRequest, redirectAttributes);
        assertThat(responseUrl).isEqualTo(
                ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN_THANKYOUCHECK + cartCode);
    }

    @Test
    public void getPlaceOrderErrorInsufficientStock() {
        final TargetPlaceOrderResult expectedPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.INSUFFICIENT_STOCK,
                null);
        expectedPlaceOrderResult.setCartCode("1234");
        given(placeOrderFacade.placeOrder(Mockito.any(TargetPlaceOrderRequest.class))).willReturn(
                expectedPlaceOrderResult);

        final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfo = new TargetPlaceOrderPayPalPaymentInfoData();
        payPalPaymentInfo.setPaypalPayerId("id");
        payPalPaymentInfo.setPaypalSessionToken("token");

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(payPalPaymentInfo);
        final HttpServletRequest mockRequest = new MockHttpServletRequest();
        final String responseUrl = controller.fromPaypal("paypalPayer", "token", mockRequest, redirectAttributes);
        assertThat(responseUrl).isEqualTo(
                "redirect:/spc/order/place-order/error?e=RVJSX1BMQUNFT1JERVJfSU5TVUZGSUNJRU5UX1NUT0NL");
    }

    @Test
    public void getPlaceOrderErrorPaymentFailureDueToMissmatchTotals() {
        final TargetPlaceOrderResult expectedPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.PAYMENT_FAILURE_DUETO_MISSMATCH_TOTALS,
                null);
        expectedPlaceOrderResult.setCartCode("1234");
        given(placeOrderFacade.placeOrder(Mockito.any(TargetPlaceOrderRequest.class))).willReturn(
                expectedPlaceOrderResult);

        final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfo = new TargetPlaceOrderPayPalPaymentInfoData();
        payPalPaymentInfo.setPaypalPayerId("id");
        payPalPaymentInfo.setPaypalSessionToken("token");

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(payPalPaymentInfo);
        final HttpServletRequest mockRequest = new MockHttpServletRequest();
        final String responseUrl = controller.fromPaypal("paypalPayer", "token", mockRequest, redirectAttributes);
        assertThat(responseUrl)
                .isEqualTo(
                        "redirect:/spc/order/place-order/error?e=RVJSX1BMQUNFT1JERVJfUEFZTUVOVF9GQUlMVVJFX0RVRVRPX01JU1NNQVRDSF9UT1RBTFM=");
    }

    @Test
    public void getPlaceOrderErrorIncompleteDeliveryInfo() {
        final TargetPlaceOrderResult expectedPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.INCOMPLETE_DELIVERY_INFO,
                null);
        expectedPlaceOrderResult.setCartCode("1234");
        given(placeOrderFacade.placeOrder(Mockito.any(TargetPlaceOrderRequest.class))).willReturn(
                expectedPlaceOrderResult);

        final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfo = new TargetPlaceOrderPayPalPaymentInfoData();
        payPalPaymentInfo.setPaypalPayerId("id");
        payPalPaymentInfo.setPaypalSessionToken("token");

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(payPalPaymentInfo);
        final HttpServletRequest mockRequest = new MockHttpServletRequest();
        final String responseUrl = controller.fromPaypal("paypalPayer", "token", mockRequest, redirectAttributes);
        assertThat(responseUrl).isEqualTo(
                "redirect:/spc/order/place-order/error?e=RVJSX1BMQUNFT1JERVJfSU5DT01QTEVURV9ERUxJVkVSWV9JTkZP");
    }

    @Test
    public void getPlaceOrderErrorServiceUnavailable() {
        final TargetPlaceOrderResult expectedPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.SERVICE_UNAVAILABLE,
                null);
        expectedPlaceOrderResult.setCartCode("1234");
        given(placeOrderFacade.placeOrder(Mockito.any(TargetPlaceOrderRequest.class))).willReturn(
                expectedPlaceOrderResult);

        final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfo = new TargetPlaceOrderPayPalPaymentInfoData();
        payPalPaymentInfo.setPaypalPayerId("id");
        payPalPaymentInfo.setPaypalSessionToken("token");

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(payPalPaymentInfo);
        final HttpServletRequest mockRequest = new MockHttpServletRequest();
        final String responseUrl = controller.fromPaypal("paypalPayer", "token", mockRequest, redirectAttributes);
        assertThat(responseUrl).isEqualTo(
                "redirect:/spc/order/place-order/error?e=RVJSX1BMQUNFT1JERVJfU0VSVklDRV9VTkFWQUlMQUJMRQ==");
    }

    @Test
    public void getPlaceOrderErrorPaymentMethodMismatch() {
        final TargetPlaceOrderResult expectedPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.PAYMENT_METHOD_MISMATCH,
                null);
        expectedPlaceOrderResult.setCartCode("1234");
        given(placeOrderFacade.placeOrder(Mockito.any(TargetPlaceOrderRequest.class))).willReturn(
                expectedPlaceOrderResult);

        final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfo = new TargetPlaceOrderPayPalPaymentInfoData();
        payPalPaymentInfo.setPaypalPayerId("id");
        payPalPaymentInfo.setPaypalSessionToken("token");

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(payPalPaymentInfo);
        final HttpServletRequest mockRequest = new MockHttpServletRequest();
        final String responseUrl = controller.fromPaypal("paypalPayer", "token", mockRequest, redirectAttributes);
        assertThat(responseUrl).isEqualTo(
                "redirect:/spc/order/place-order/error?e=RVJSX1BMQUNFT1JERVJfUEFZTUVOVF9NRVRIT0RfTUlTTUFUQ0g=");
    }

    @Test
    public void getPlaceOrderErrorIncompleteBillingAddress() {
        final TargetPlaceOrderResult expectedPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.INCOMPLETE_DELIVERY_INFO,
                null);
        expectedPlaceOrderResult.setCartCode("1234");
        given(placeOrderFacade.placeOrder(Mockito.any(TargetPlaceOrderRequest.class))).willReturn(
                expectedPlaceOrderResult);

        final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfo = new TargetPlaceOrderPayPalPaymentInfoData();
        payPalPaymentInfo.setPaypalPayerId("id");
        payPalPaymentInfo.setPaypalSessionToken("token");

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(payPalPaymentInfo);
        final HttpServletRequest mockRequest = new MockHttpServletRequest();
        final String responseUrl = controller.fromPaypal("paypalPayer", "token", mockRequest, redirectAttributes);
        assertThat(responseUrl).isEqualTo(
                "redirect:/spc/order/place-order/error?e=RVJSX1BMQUNFT1JERVJfSU5DT01QTEVURV9ERUxJVkVSWV9JTkZP");
    }

    @Test
    public void getPlaceOrderErrorEmpty() {
        final TargetPlaceOrderResult expectedPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.INVALID_CART,
                null);
        expectedPlaceOrderResult.setCartCode("1234");
        given(placeOrderFacade.placeOrder(Mockito.any(TargetPlaceOrderRequest.class))).willReturn(
                expectedPlaceOrderResult);

        final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfo = new TargetPlaceOrderPayPalPaymentInfoData();
        payPalPaymentInfo.setPaypalPayerId("id");
        payPalPaymentInfo.setPaypalSessionToken("token");

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(payPalPaymentInfo);
        final HttpServletRequest mockRequest = new MockHttpServletRequest();
        final String responseUrl = controller.fromPaypal("paypalPayer", "token", mockRequest, redirectAttributes);
        assertThat(responseUrl).isEqualTo("redirect:/basket");
    }

    @Test
    public void placeOrderPartialSuccessfullyViaPaypal() throws CMSItemNotFoundException, InvalidCartException {
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        final TargetPlaceOrderResult expectedPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.SUCCESS,
                orderModel);
        final String orderCode = "00010000";
        final AdjustedCartEntriesData adjustedData = mock(AdjustedCartEntriesData.class);
        placeOrderResult.setCartCode(orderCode);
        given(placeOrderFacade.placeOrder(any(TargetPlaceOrderRequest.class))).willReturn(expectedPlaceOrderResult);
        given(orderModel.getCode()).willReturn(orderCode);
        willReturn(Boolean.TRUE).given(adjustedData).isAdjusted();
        expectedPlaceOrderResult.setAdjustedCartEntriesData(adjustedData);
        final String responseUrl = controller.fromPaypal("paypalPayer", "token", request, redirectAttributes);
        assertThat(responseUrl).isEqualTo("redirect:/checkout/thank-you/");
        verify(redirectAttributes).addFlashAttribute("sohUpdates", Collections.singleton(adjustedData));
        verify(session, never()).setAttribute(ControllerConstants.SOH_UPDATES, adjustedData);
    }

    @Test
    public void placeOrderPartialSuccessfullyViaPaypalWithNewThankyouPage() throws CMSItemNotFoundException,
            InvalidCartException {
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        final TargetPlaceOrderResult expectedPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.SUCCESS,
                orderModel);
        final String orderCode = "00010000";
        final AdjustedCartEntriesData adjustedData = mock(AdjustedCartEntriesData.class);
        placeOrderResult.setCartCode(orderCode);
        given(placeOrderFacade.placeOrder(any(TargetPlaceOrderRequest.class))).willReturn(expectedPlaceOrderResult);
        given(orderModel.getCode()).willReturn(orderCode);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU);
        willReturn(Boolean.TRUE).given(adjustedData).isAdjusted();
        expectedPlaceOrderResult.setAdjustedCartEntriesData(adjustedData);
        expectedPlaceOrderResult.setCartCode(orderCode);
        final String responseUrl = controller.fromPaypal("paypalPayer", "token", request, redirectAttributes);
        assertThat(responseUrl).isEqualTo(
                ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN_THANKYOU + orderCode);
        verify(redirectAttributes).addFlashAttribute("sohUpdates", Collections.singleton(adjustedData));
        verify(session).setAttribute(ControllerConstants.SOH_UPDATES, adjustedData);
    }

    @Test
    public void getPlaceOrderErrorIncompleteBillingAddressWithFeatureSwitchToNewSpcCheckout() {
        final TargetPlaceOrderResult expectedPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.INCOMPLETE_DELIVERY_INFO,
                null);
        expectedPlaceOrderResult.setCartCode("1234");
        given(Boolean.valueOf(targetFeatureSwitchFacade.isSpcLoginAndCheckout())).willReturn(Boolean.TRUE);
        given(placeOrderFacade.placeOrder(Mockito.any(TargetPlaceOrderRequest.class))).willReturn(
                expectedPlaceOrderResult);

        final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfo = new TargetPlaceOrderPayPalPaymentInfoData();
        payPalPaymentInfo.setPaypalPayerId("id");
        payPalPaymentInfo.setPaypalSessionToken("token");

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(payPalPaymentInfo);
        final HttpServletRequest mockRequest = new MockHttpServletRequest();
        final String responseUrl = controller.fromPaypal("paypalPayer", "token", mockRequest, redirectAttributes);
        assertThat(responseUrl).isEqualTo(
                "redirect:/spc/order/place-order/error?e=RVJSX1BMQUNFT1JERVJfSU5DT01QTEVURV9ERUxJVkVSWV9JTkZP");
    }

    @Test
    public void placeOrderFundingFailureFromPaypal() {
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        final TargetPlaceOrderResult expectedPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.FUNDING_FAILURE,
                null);
        given(targetPaymentHelper.getPayPalHostedPaymentFormBaseUrl()).willReturn(
                "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&amp;useraction=commit&amp;token=");
        final String orderCode = "00010000";
        placeOrderResult.setCartCode(orderCode);
        given(placeOrderFacade.placeOrder(any(TargetPlaceOrderRequest.class))).willReturn(expectedPlaceOrderResult);
        given(orderModel.getCode()).willReturn(orderCode);
        final HttpServletRequest mockRequest = new MockHttpServletRequest();
        final String responseUrl = controller.fromPaypal("paypalPayer", "token", mockRequest, redirectAttributes);
        assertThat(responseUrl)
                .isEqualTo(
                        "redirect:https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&amp;useraction=commit&amp;token=paypalPayer");
    }

    @Test
    public void testCancelAfterpay() {
        final String cancelAfterpay = controller.cancelAfterpay("CANCELLED", request);
        verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_AFTERPAY_TOKEN);
        assertThat(cancelAfterpay).isEqualTo("redirect:/spc/order/");
    }

    @Test
    public void testFromZipPayApproved() {

        final String result = ControllerConstants.REQUEST_PARAM_ZIPPAY_STATUS_APPROVED;
        final String randomToken = mockZipPayRequest(result);
        final String expectedUrl = ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN_THANKYOU;
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU);
        final ArgumentCaptor<TargetPlaceOrderRequest> placeOrderRequestCaptor = ArgumentCaptor
                .forClass(TargetPlaceOrderRequest.class);
        final TargetPlaceOrderResult mockPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.SUCCESS,
                new CartModel());

        given(placeOrderFacade.placeOrder(placeOrderRequestCaptor.capture())).willReturn(mockPlaceOrderResult);
        final String returnUrl = controller.fromZipPay(result, randomToken, null, request, redirectAttributes);
        verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN);
        assertThat(returnUrl).isEqualTo(expectedUrl);
    }

    @Test
    public void testFromZipPayCancel() {
        final String result = ControllerConstants.REQUEST_PARAM_ZIPPAY_STATUS_CANCELLED;
        final String randomToken = mockZipPayRequest(result);
        final String returnUrl = controller.fromZipPay(result,
                randomToken, null, request, redirectAttributes);
        verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN);
        assertThat(returnUrl).isEqualTo(ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN);
    }

    @Test
    public void testFromZipPayDeclined() {
        final String result = ControllerConstants.REQUEST_PARAM_ZIPPAY_STATUS_DECLINED;
        final String randomToken = mockZipPayRequest(result);
        final String returnUrl = controller.fromZipPay(result,
                randomToken, null, request, redirectAttributes);
        verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN);
        // Base64 decode value of ERR_PLACEORDER_ZIP_DECLINED
        assertThat(returnUrl).isEqualTo(
                ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN_PLACE_ORDER_ERROR
                        + "RVJSX1BMQUNFT1JERVJfWklQX0RFQ0xJTkVE");
    }


    @Test
    public void testFromZipPayReffered() {
        final String result = ControllerConstants.REQUEST_PARAM_ZIPPAY_STATUS_REFERRED;
        final String randomToken = mockZipPayRequest(result);
        final String returnUrl = controller.fromZipPay(result,
                randomToken, null, request, redirectAttributes);
        verify(session, never()).removeAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN);
        // Base64 decode value of ERR_PLACEORDER_ZIP_REFERRED
        assertThat(returnUrl).isEqualTo(
                ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN_PLACE_ORDER_ERROR
                        + "RVJSX1BMQUNFT1JERVJfWklQX1JFRkVSUkVE");
    }

    @Test
    public void testFromZipPayCancelWithInvalidToken() {

        final String result = ControllerConstants.REQUEST_PARAM_ZIPPAY_STATUS_CANCELLED;
        final String randomToken = mockZipPayRequest(result);
        given(request.getParameter(ControllerConstants.REQUEST_PARAM_ZIPPAY_CHECKOUT_TOKEN)).willReturn(
                "invalidTokenValue");
        final String expectedUrl = ControllerConstants.Redirection.CART;
        given(targetCartFacade.hasEntries()).willReturn(Boolean.FALSE);
        final String returnUrl = controller.fromZipPay(result,
                randomToken, null, request, redirectAttributes);
        // Base64 encoded value of ERR_PLACEORDER_FAILURE
        verify(session, never()).removeAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN);
        verify(session, never()).setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS, Boolean.TRUE);
        verify(session, never()).removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);
        assertThat(returnUrl).isEqualTo(expectedUrl);
    }

    @Test
    public void testFromZipPayApprovedTokenMisMatch() {

        final String result = ControllerConstants.REQUEST_PARAM_ZIPPAY_STATUS_APPROVED;
        final String randomToken = mockZipPayRequest(result);
        given(request.getParameter(ControllerConstants.REQUEST_PARAM_ZIPPAY_CHECKOUT_TOKEN)).willReturn(
                "invalidTokenValue");
        final String expectedUrl = getExpectedUrlShort(TargetPlaceOrderResultEnum.ITEM_MISMATCH_IN_CART_AFTER_PAYMENT
                .toString());
        given(targetCartFacade.hasEntries()).willReturn(Boolean.TRUE);
        final String returnUrl = controller.fromZipPay(result,
                randomToken, null, request, redirectAttributes);
        // Base64 encoded value of ERR_PLACEORDER_FAILURE
        verify(session, never()).removeAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN);
        verify(session, never()).setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS, Boolean.TRUE);
        verify(session, never()).removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);
        assertThat(returnUrl).isEqualTo(expectedUrl);
    }

    @Test
    public void testFromZipPayInvalidStatus() {

        final String result = "invalidStatus";
        final String randomToken = mockZipPayRequest(result);
        final String expectedUrl = ControllerConstants.Redirection.CART;
        final String returnUrl = controller.fromZipPay(result,
                randomToken, null, request, redirectAttributes);
        // Base64 encoded value of ERR_PLACEORDER_FAILURE
        verify(session, never()).removeAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN);
        assertThat(returnUrl).isEqualTo(expectedUrl);
    }

    @Test
    public void testFromZipPayInvalidItems() {
        final String result = ControllerConstants.REQUEST_PARAM_ZIPPAY_STATUS_APPROVED;
        final String randomToken = mockZipPayRequest(result);
        final String expectedUrl = getExpectedUrlShort(ZIPPAYMENT_PREFIX
                + TargetPlaceOrderResultEnum.ITEM_MISMATCH_IN_CART_AFTER_PAYMENT
                        .toString());
        final ArgumentCaptor<TargetPlaceOrderRequest> placeOrderRequestCaptor = ArgumentCaptor
                .forClass(TargetPlaceOrderRequest.class);
        final TargetPlaceOrderResult mockPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.ITEM_MISMATCH_IN_CART_AFTER_PAYMENT, new CartModel());
        given(placeOrderFacade.placeOrder(placeOrderRequestCaptor.capture())).willReturn(mockPlaceOrderResult);
        final String returnUrl = controller.fromZipPay(result,
                randomToken, null, request, redirectAttributes);
        // Base64 encoded value of ERR_PLACEORDER_FAILURE
        verify(session, never()).removeAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN);
        verify(session).setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS, Boolean.TRUE);
        verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);
        assertThat(returnUrl).isEqualTo(expectedUrl);
    }


    @Test
    public void testFromZipPayPaymentFailure() {
        final String result = ControllerConstants.REQUEST_PARAM_ZIPPAY_STATUS_APPROVED;
        final String randomToken = mockZipPayRequest(result);
        final String expectedUrl = getExpectedUrlShort(ZIPPAYMENT_PREFIX
                + TargetPlaceOrderResultEnum.PAYMENT_FAILURE.toString());
        final ArgumentCaptor<TargetPlaceOrderRequest> placeOrderRequestCaptor = ArgumentCaptor
                .forClass(TargetPlaceOrderRequest.class);
        final TargetPlaceOrderResult mockPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.PAYMENT_FAILURE, new CartModel());
        given(placeOrderFacade.placeOrder(placeOrderRequestCaptor.capture())).willReturn(mockPlaceOrderResult);
        final String returnUrl = controller.fromZipPay(result,
                randomToken, null, request, redirectAttributes);
        // Base64 encoded value of ERR_PLACEORDER_FAILURE
        verify(session, never()).removeAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN);
        verify(session).setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS, Boolean.TRUE);
        verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);
        assertThat(returnUrl).isEqualTo(expectedUrl);
    }

    @Test
    public void testFromZipPayMisMatchTotals() {
        final String result = ControllerConstants.REQUEST_PARAM_ZIPPAY_STATUS_APPROVED;
        final String randomToken = mockZipPayRequest(result);
        final String expectedUrl = getExpectedUrlShort(ZIPPAYMENT_PREFIX
                + TargetPlaceOrderResultEnum.PAYMENT_FAILURE_DUETO_MISSMATCH_TOTALS
                        .toString());
        final ArgumentCaptor<TargetPlaceOrderRequest> placeOrderRequestCaptor = ArgumentCaptor
                .forClass(TargetPlaceOrderRequest.class);
        final TargetPlaceOrderResult mockPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.PAYMENT_FAILURE_DUETO_MISSMATCH_TOTALS, new CartModel());
        given(placeOrderFacade.placeOrder(placeOrderRequestCaptor.capture())).willReturn(mockPlaceOrderResult);
        final String returnUrl = controller.fromZipPay(result,
                randomToken, null, request, redirectAttributes);
        // Base64 encoded value of ERR_PLACEORDER_FAILURE
        verify(session, never()).removeAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN);
        verify(session).setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS, Boolean.TRUE);
        verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);
        assertThat(returnUrl).isEqualTo(expectedUrl);
    }

    @Test
    public void testFromZipUnkownError() {
        final String result = ControllerConstants.REQUEST_PARAM_ZIPPAY_STATUS_APPROVED;
        final String randomToken = mockZipPayRequest(result);
        final String expectedUrl = ControllerConstants.Redirection.CHECKOUT_THANK_YOU
                + ControllerConstants.THANK_YOU_CHECK + "?cart=";
        final ArgumentCaptor<TargetPlaceOrderRequest> placeOrderRequestCaptor = ArgumentCaptor
                .forClass(TargetPlaceOrderRequest.class);
        final TargetPlaceOrderResult mockPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.UNKNOWN, new CartModel());
        given(placeOrderFacade.placeOrder(placeOrderRequestCaptor.capture())).willReturn(mockPlaceOrderResult);
        final String returnUrl = controller.fromZipPay(result,
                randomToken, null, request, redirectAttributes);
        // Base64 encoded value of ERR_PLACEORDER_FAILURE
        verify(session, never()).removeAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN);
        verify(session).setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS, Boolean.TRUE);
        verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);
        assertThat(returnUrl).isEqualTo(expectedUrl);
    }


    private String getExpectedUrlShort(final String result) {
        return ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN_PLACE_ORDER_ERROR + DatatypeConverter
                .printBase64Binary((VENDOR_ERROR_CODE_PREFIX + result).getBytes(StandardCharsets.UTF_8));
    }


    private String mockZipPayRequest(final String result) {
        final String randomToken = UUID.randomUUID().toString();
        given(request.getSession()).willReturn(session);
        given(request.getSession().getAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN))
                .willReturn(randomToken);
        given(request.getParameter(ControllerConstants.REQUEST_PARAM_ZIPPAY_CHECKOUT_TOKEN)).willReturn(randomToken);
        given(request.getParameter(ControllerConstants.REQUEST_PARAM_ZIPPAY_RESULT)).willReturn(result);
        return randomToken;
    }

    @Test
    public void testFromAfterpayWithFailureStatus() {
        final String result = controller.fromAfterpay(afterpayToken, "FAILURE", request, redirectAttributes);

        // Base64 encoded value of ERR_PLACEORDER_FAILURE
        assertThat(result).isEqualTo("redirect:/spc/order/place-order/error?e=RVJSX1BMQUNFT1JERVJfRkFJTFVSRQ==");
    }

    @Test
    public void testFromAfterpayWithCancelledStatus() {
        final String result = controller.fromAfterpay(afterpayToken, "CANCELLED", request, redirectAttributes);

        // Base64 encoded value of ERR_PLACEORDER_CANCELLED
        assertThat(result).isEqualTo("redirect:/spc/order/place-order/error?e=RVJSX1BMQUNFT1JERVJfQ0FOQ0VMTEVE");
    }

    @Test
    public void testFromAfterpayWithInvalidStatus() {
        final String result = controller.fromAfterpay(afterpayToken, "ELEPHANT", request, redirectAttributes);

        // Base64 encoded value of ERR_PLACEORDER_UNKNOWN
        assertThat(result).isEqualTo("redirect:/spc/order/place-order/error?e=RVJSX1BMQUNFT1JERVJfVU5LTk9XTg==");
    }

    @Test
    public void testFromAfterpayWithSuccessStatus() {
        final String ipAddress = "8.8.8.8";
        final String cookieString = "ME LOVE COOKIES!";
        final String cartCode = "92938475";

        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU);

        given(requestHeadersHelper.getRemoteIPAddress(request)).willReturn(ipAddress);

        final Cookie cookie = new Cookie("type", "Chocolate Chip");
        final Cookie[] cookies = new Cookie[] { cookie };

        given(request.getCookies()).willReturn(cookies);
        given(cookieStringBuilder.buildCookieString(cookies)).willReturn(cookieString);

        final OrderModel mockOrder = mock(OrderModel.class);
        final TargetPlaceOrderResult actualPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.SUCCESS, mockOrder);
        actualPlaceOrderResult.setCartCode(cartCode);

        final ArgumentCaptor<TargetPlaceOrderRequest> placeOrderRequestCaptor = ArgumentCaptor
                .forClass(TargetPlaceOrderRequest.class);
        given(placeOrderFacade.placeOrder(placeOrderRequestCaptor.capture())).willReturn(actualPlaceOrderResult);

        final String result = controller.fromAfterpay(afterpayToken, "SUCCESS", request, redirectAttributes);

        assertThat(result).isEqualTo("redirect:/spc/order/thankyou/" + cartCode);

        final InOrder inOrderMocks = inOrder(session, placeOrderFacade);
        inOrderMocks.verify(session).setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS,
                Boolean.TRUE);

        inOrderMocks.verify(placeOrderFacade).placeOrder(placeOrderRequestCaptor.getValue());

        inOrderMocks.verify(session).setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE, cartCode);
        inOrderMocks.verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN);

        inOrderMocks.verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);

        final TargetPlaceOrderRequest capturedPlaceOrderRequest = placeOrderRequestCaptor.getValue();
        assertThat(capturedPlaceOrderRequest.getCookieString()).isEqualTo(cookieString);
        assertThat(capturedPlaceOrderRequest.getIpAddress()).isEqualTo(ipAddress);
        assertThat(capturedPlaceOrderRequest.getPaymentInfo())
                .isInstanceOf(TargetPlaceOrderAfterpayPaymentInfoData.class);

        verifyZeroInteractions(redirectAttributes);
    }

    @Test
    public void testFromAfterpayWithSuccessStatusAndAdjustedCartEntries() {
        final String ipAddress = "8.8.8.8";
        final String cookieString = "ME LOVE COOKIES!";
        final String cartCode = "92938475";

        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU);

        given(requestHeadersHelper.getRemoteIPAddress(request)).willReturn(ipAddress);

        final Cookie cookie = new Cookie("type", "Chocolate Chip");
        final Cookie[] cookies = new Cookie[] { cookie };

        given(request.getCookies()).willReturn(cookies);
        given(cookieStringBuilder.buildCookieString(cookies)).willReturn(cookieString);

        final OrderModel mockOrder = mock(OrderModel.class);
        final TargetPlaceOrderResult actualPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.SUCCESS, mockOrder);
        actualPlaceOrderResult.setCartCode(cartCode);

        final AdjustedCartEntriesData mockAdjustedCartEntries = mock(AdjustedCartEntriesData.class);
        willReturn(Boolean.TRUE).given(mockAdjustedCartEntries).isAdjusted();

        actualPlaceOrderResult.setAdjustedCartEntriesData(mockAdjustedCartEntries);

        final ArgumentCaptor<TargetPlaceOrderRequest> placeOrderRequestCaptor = ArgumentCaptor
                .forClass(TargetPlaceOrderRequest.class);
        given(placeOrderFacade.placeOrder(placeOrderRequestCaptor.capture())).willReturn(actualPlaceOrderResult);

        final String result = controller.fromAfterpay(afterpayToken, "SUCCESS", request, redirectAttributes);

        assertThat(result).isEqualTo("redirect:/spc/order/thankyou/" + cartCode);

        final InOrder inOrderMocks = inOrder(session, placeOrderFacade, redirectAttributes);
        inOrderMocks.verify(session).setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS,
                Boolean.TRUE);

        inOrderMocks.verify(placeOrderFacade).placeOrder(placeOrderRequestCaptor.getValue());

        final ArgumentCaptor<Set> adjustedCartEntriesCaptor = ArgumentCaptor.forClass(Set.class);
        inOrderMocks.verify(redirectAttributes).addFlashAttribute(eq(ControllerConstants.SOH_UPDATES),
                adjustedCartEntriesCaptor.capture());

        final Set capturedAdjustedCartEntries = adjustedCartEntriesCaptor.getValue();
        assertThat(capturedAdjustedCartEntries).hasSize(1).containsOnly(mockAdjustedCartEntries);

        inOrderMocks.verify(session).setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE, cartCode);
        inOrderMocks.verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN);
        inOrderMocks.verify(session).setAttribute(ControllerConstants.SOH_UPDATES, mockAdjustedCartEntries);

        inOrderMocks.verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);

        final TargetPlaceOrderRequest capturedPlaceOrderRequest = placeOrderRequestCaptor.getValue();
        assertThat(capturedPlaceOrderRequest.getCookieString()).isEqualTo(cookieString);
        assertThat(capturedPlaceOrderRequest.getIpAddress()).isEqualTo(ipAddress);
        assertThat(capturedPlaceOrderRequest.getPaymentInfo())
                .isInstanceOf(TargetPlaceOrderAfterpayPaymentInfoData.class);
    }

    @Test
    public void testFromAfterpayWithSuccessStatusPlaceOrderFailure() {
        final String ipAddress = "8.8.8.8";
        final String cookieString = "ME LOVE COOKIES!";
        final String cartCode = "92938475";

        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU);

        given(requestHeadersHelper.getRemoteIPAddress(request)).willReturn(ipAddress);

        final Cookie cookie = new Cookie("type", "Chocolate Chip");
        final Cookie[] cookies = new Cookie[] { cookie };

        given(request.getCookies()).willReturn(cookies);
        given(cookieStringBuilder.buildCookieString(cookies)).willReturn(cookieString);

        final OrderModel mockOrder = mock(OrderModel.class);
        final TargetPlaceOrderResult actualPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.UNKNOWN, mockOrder);
        actualPlaceOrderResult.setCartCode(cartCode);

        final ArgumentCaptor<TargetPlaceOrderRequest> placeOrderRequestCaptor = ArgumentCaptor
                .forClass(TargetPlaceOrderRequest.class);
        given(placeOrderFacade.placeOrder(placeOrderRequestCaptor.capture())).willReturn(actualPlaceOrderResult);

        final String result = controller.fromAfterpay(afterpayToken, "SUCCESS", request, redirectAttributes);

        assertThat(result).isEqualTo("redirect:/spc/order/thankyoucheck/" + cartCode);

        final InOrder inOrderMocks = inOrder(session, placeOrderFacade);
        inOrderMocks.verify(session).setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS,
                Boolean.TRUE);

        inOrderMocks.verify(placeOrderFacade).placeOrder(placeOrderRequestCaptor.getValue());

        inOrderMocks.verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);

        final TargetPlaceOrderRequest capturedPlaceOrderRequest = placeOrderRequestCaptor.getValue();
        assertThat(capturedPlaceOrderRequest.getCookieString()).isEqualTo(cookieString);
        assertThat(capturedPlaceOrderRequest.getIpAddress()).isEqualTo(ipAddress);
        assertThat(capturedPlaceOrderRequest.getPaymentInfo())
                .isInstanceOf(TargetPlaceOrderAfterpayPaymentInfoData.class);

        verifyZeroInteractions(redirectAttributes);
    }

    @Test
    public void testFromAfterpayWithSuccessStatusPlaceOrderFailureAndAdjustedCartEntries() {
        final String ipAddress = "8.8.8.8";
        final String cookieString = "ME LOVE COOKIES!";
        final String cartCode = "92938475";

        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU);

        given(requestHeadersHelper.getRemoteIPAddress(request)).willReturn(ipAddress);

        final Cookie cookie = new Cookie("type", "Chocolate Chip");
        final Cookie[] cookies = new Cookie[] { cookie };

        given(request.getCookies()).willReturn(cookies);
        given(cookieStringBuilder.buildCookieString(cookies)).willReturn(cookieString);

        final OrderModel mockOrder = mock(OrderModel.class);
        final TargetPlaceOrderResult actualPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.UNKNOWN, mockOrder);
        actualPlaceOrderResult.setCartCode(cartCode);

        final AdjustedCartEntriesData mockAdjustedCartEntries = mock(AdjustedCartEntriesData.class);
        willReturn(Boolean.TRUE).given(mockAdjustedCartEntries).isAdjusted();

        actualPlaceOrderResult.setAdjustedCartEntriesData(mockAdjustedCartEntries);

        final ArgumentCaptor<TargetPlaceOrderRequest> placeOrderRequestCaptor = ArgumentCaptor
                .forClass(TargetPlaceOrderRequest.class);
        given(placeOrderFacade.placeOrder(placeOrderRequestCaptor.capture())).willReturn(actualPlaceOrderResult);

        final String result = controller.fromAfterpay(afterpayToken, "SUCCESS", request, redirectAttributes);

        // Base64 encoded value of ERR_PLACEORDER_UNKNOWN
        assertThat(result).isEqualTo("redirect:/spc/order/thankyoucheck/" + cartCode);

        final InOrder inOrderMocks = inOrder(session, placeOrderFacade, redirectAttributes);
        inOrderMocks.verify(session).setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS,
                Boolean.TRUE);

        inOrderMocks.verify(placeOrderFacade).placeOrder(placeOrderRequestCaptor.getValue());

        final ArgumentCaptor<Set> adjustedCartEntriesCaptor = ArgumentCaptor.forClass(Set.class);
        inOrderMocks.verify(redirectAttributes).addFlashAttribute(eq(ControllerConstants.SOH_UPDATES),
                adjustedCartEntriesCaptor.capture());

        final Set capturedAdjustedCartEntries = adjustedCartEntriesCaptor.getValue();
        assertThat(capturedAdjustedCartEntries).hasSize(1).containsOnly(mockAdjustedCartEntries);

        inOrderMocks.verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);

        final TargetPlaceOrderRequest capturedPlaceOrderRequest = placeOrderRequestCaptor.getValue();
        assertThat(capturedPlaceOrderRequest.getCookieString()).isEqualTo(cookieString);
        assertThat(capturedPlaceOrderRequest.getIpAddress()).isEqualTo(ipAddress);
        assertThat(capturedPlaceOrderRequest.getPaymentInfo())
                .isInstanceOf(TargetPlaceOrderAfterpayPaymentInfoData.class);
    }

    @Test
    public void testFromAfterpayWithNullPlaceOrderResult() {
        final String ipAddress = "8.8.8.8";
        final String cookieString = "ME LOVE COOKIES!";

        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU);

        given(requestHeadersHelper.getRemoteIPAddress(request)).willReturn(ipAddress);

        final Cookie cookie = new Cookie("type", "Chocolate Chip");
        final Cookie[] cookies = new Cookie[] { cookie };

        given(request.getCookies()).willReturn(cookies);
        given(cookieStringBuilder.buildCookieString(cookies)).willReturn(cookieString);

        final ArgumentCaptor<TargetPlaceOrderRequest> placeOrderRequestCaptor = ArgumentCaptor
                .forClass(TargetPlaceOrderRequest.class);
        given(placeOrderFacade.placeOrder(placeOrderRequestCaptor.capture())).willReturn(null);

        final String result = controller.fromAfterpay(afterpayToken, "SUCCESS", request, redirectAttributes);

        assertThat(result).isEqualTo("redirect:/basket");

        final InOrder inOrderMocks = inOrder(session, placeOrderFacade);
        inOrderMocks.verify(session).setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS,
                Boolean.TRUE);

        inOrderMocks.verify(placeOrderFacade).placeOrder(placeOrderRequestCaptor.getValue());

        inOrderMocks.verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);

        final TargetPlaceOrderRequest capturedPlaceOrderRequest = placeOrderRequestCaptor.getValue();
        assertThat(capturedPlaceOrderRequest.getCookieString()).isEqualTo(cookieString);
        assertThat(capturedPlaceOrderRequest.getIpAddress()).isEqualTo(ipAddress);
        assertThat(capturedPlaceOrderRequest.getPaymentInfo())
                .isInstanceOf(TargetPlaceOrderAfterpayPaymentInfoData.class);

        verifyZeroInteractions(redirectAttributes);
    }

    @Test
    public void testFromAfterpayWithInvalidCart() {
        final String ipAddress = "8.8.8.8";
        final String cookieString = "ME LOVE COOKIES!";
        final String cartCode = "92938475";

        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU);

        given(requestHeadersHelper.getRemoteIPAddress(request)).willReturn(ipAddress);

        final Cookie cookie = new Cookie("type", "Chocolate Chip");
        final Cookie[] cookies = new Cookie[] { cookie };

        given(request.getCookies()).willReturn(cookies);
        given(cookieStringBuilder.buildCookieString(cookies)).willReturn(cookieString);

        final OrderModel mockOrder = mock(OrderModel.class);
        final TargetPlaceOrderResult actualPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.INVALID_CART, mockOrder);
        actualPlaceOrderResult.setCartCode(cartCode);

        final ArgumentCaptor<TargetPlaceOrderRequest> placeOrderRequestCaptor = ArgumentCaptor
                .forClass(TargetPlaceOrderRequest.class);
        given(placeOrderFacade.placeOrder(placeOrderRequestCaptor.capture())).willReturn(actualPlaceOrderResult);

        final String result = controller.fromAfterpay(afterpayToken, "SUCCESS", request, redirectAttributes);

        assertThat(result).isEqualTo("redirect:/basket");

        final InOrder inOrderMocks = inOrder(session, placeOrderFacade);
        inOrderMocks.verify(session).setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS,
                Boolean.TRUE);

        inOrderMocks.verify(placeOrderFacade).placeOrder(placeOrderRequestCaptor.getValue());

        inOrderMocks.verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);

        final TargetPlaceOrderRequest capturedPlaceOrderRequest = placeOrderRequestCaptor.getValue();
        assertThat(capturedPlaceOrderRequest.getCookieString()).isEqualTo(cookieString);
        assertThat(capturedPlaceOrderRequest.getIpAddress()).isEqualTo(ipAddress);
        assertThat(capturedPlaceOrderRequest.getPaymentInfo())
                .isInstanceOf(TargetPlaceOrderAfterpayPaymentInfoData.class);

        verifyZeroInteractions(redirectAttributes);
    }

    @Test
    public void testFromAfterpayWithInvalidProductInCart() {
        final String ipAddress = "8.8.8.8";
        final String cookieString = "ME LOVE COOKIES!";
        final String cartCode = "92938475";

        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU);

        given(requestHeadersHelper.getRemoteIPAddress(request)).willReturn(ipAddress);

        final Cookie cookie = new Cookie("type", "Chocolate Chip");
        final Cookie[] cookies = new Cookie[] { cookie };

        given(request.getCookies()).willReturn(cookies);
        given(cookieStringBuilder.buildCookieString(cookies)).willReturn(cookieString);

        final OrderModel mockOrder = mock(OrderModel.class);
        final TargetPlaceOrderResult actualPlaceOrderResult = new TargetPlaceOrderResult(
                TargetPlaceOrderResultEnum.INVALID_PRODUCT_IN_CART, mockOrder);
        actualPlaceOrderResult.setCartCode(cartCode);
        final ArgumentCaptor<TargetPlaceOrderRequest> placeOrderRequestCaptor = ArgumentCaptor
                .forClass(TargetPlaceOrderRequest.class);
        given(placeOrderFacade.placeOrder(placeOrderRequestCaptor.capture())).willReturn(actualPlaceOrderResult);

        final String result = controller.fromAfterpay(afterpayToken, "SUCCESS", request, redirectAttributes);

        assertThat(result).isEqualTo("redirect:/basket");

        final InOrder inOrderMocks = inOrder(session, placeOrderFacade);
        inOrderMocks.verify(session).setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS,
                Boolean.TRUE);

        inOrderMocks.verify(placeOrderFacade).placeOrder(placeOrderRequestCaptor.getValue());

        inOrderMocks.verify(session).removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);

        final TargetPlaceOrderRequest capturedPlaceOrderRequest = placeOrderRequestCaptor.getValue();
        assertThat(capturedPlaceOrderRequest.getCookieString()).isEqualTo(cookieString);
        assertThat(capturedPlaceOrderRequest.getIpAddress()).isEqualTo(ipAddress);
        assertThat(capturedPlaceOrderRequest.getPaymentInfo())
                .isInstanceOf(TargetPlaceOrderAfterpayPaymentInfoData.class);
        verify(redirectAttributes).getFlashAttributes();

    }


    @Test
    public void testFromAfterpayWithSeviceUnavailable() {
        final TargetPlaceOrderResultEnum errorResult = TargetPlaceOrderResultEnum.SERVICE_UNAVAILABLE;
        this.configAfterpayErrorResult(errorResult);
        final String result = controller.fromAfterpay(afterpayToken, "SUCCESS", request, redirectAttributes);
        final String base64Encode = DatatypeConverter
                .printBase64Binary(("ERR_PLACEORDER_AFTERPAY_" + errorResult.name())
                        .getBytes(StandardCharsets.UTF_8));
        assertThat(result).isEqualTo(
                ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN_PLACE_ORDER_ERROR + base64Encode);
    }

    @Test
    public void testFromAfterpayWithPaymentFailure() {
        final TargetPlaceOrderResultEnum errorResult = TargetPlaceOrderResultEnum.PAYMENT_FAILURE;
        this.configAfterpayErrorResult(errorResult);
        final String result = controller.fromAfterpay(afterpayToken, "SUCCESS", request, redirectAttributes);
        final String base64Encode = DatatypeConverter
                .printBase64Binary(("ERR_PLACEORDER_AFTERPAY_" + errorResult.name())
                        .getBytes(StandardCharsets.UTF_8));
        assertThat(result).isEqualTo(
                ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN_PLACE_ORDER_ERROR + base64Encode);
    }

    @Test
    public void testFromAfterpayWithItemMismatch() {
        final TargetPlaceOrderResultEnum errorResult = TargetPlaceOrderResultEnum.ITEM_MISMATCH_IN_CART_AFTER_PAYMENT;
        this.configAfterpayErrorResult(errorResult);
        final String result = controller.fromAfterpay(afterpayToken, "SUCCESS", request, redirectAttributes);
        final String base64Encode = DatatypeConverter
                .printBase64Binary(("ERR_PLACEORDER_AFTERPAY_" + errorResult.name())
                        .getBytes(StandardCharsets.UTF_8));
        assertThat(result).isEqualTo(
                ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN_PLACE_ORDER_ERROR + base64Encode);
    }

    @Test
    public void testFromAfterpayWithTotalMismatch() {
        final TargetPlaceOrderResultEnum errorResult = TargetPlaceOrderResultEnum.PAYMENT_FAILURE_DUETO_MISSMATCH_TOTALS;
        this.configAfterpayErrorResult(errorResult);
        final String result = controller.fromAfterpay(afterpayToken, "SUCCESS", request, redirectAttributes);
        final String base64Encode = DatatypeConverter
                .printBase64Binary(("ERR_PLACEORDER_AFTERPAY_" + errorResult.name())
                        .getBytes(StandardCharsets.UTF_8));
        assertThat(result).isEqualTo(
                ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN_PLACE_ORDER_ERROR + base64Encode);
    }

    private void configAfterpayErrorResult(final TargetPlaceOrderResultEnum resultEnum) {
        final String ipAddress = "8.8.8.8";
        final String cookieString = "ME LOVE COOKIES!";
        final String cartCode = "92938475";

        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU);

        given(requestHeadersHelper.getRemoteIPAddress(request)).willReturn(ipAddress);

        final Cookie cookie = new Cookie("type", "Chocolate Chip");
        final Cookie[] cookies = new Cookie[] { cookie };

        given(request.getCookies()).willReturn(cookies);
        given(cookieStringBuilder.buildCookieString(cookies)).willReturn(cookieString);

        final OrderModel mockOrder = mock(OrderModel.class);
        final TargetPlaceOrderResult actualPlaceOrderResult = new TargetPlaceOrderResult(resultEnum, mockOrder);
        actualPlaceOrderResult.setCartCode(cartCode);
        final ArgumentCaptor<TargetPlaceOrderRequest> placeOrderRequestCaptor = ArgumentCaptor
                .forClass(TargetPlaceOrderRequest.class);
        given(placeOrderFacade.placeOrder(placeOrderRequestCaptor.capture())).willReturn(actualPlaceOrderResult);
    }
}
