/**
 * 
 */
package au.com.target.tgtstorefront.tags;


import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author rmcalave
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class PriceFormatTagTest {
    @Mock(name = "jspContext")
    private PageContext mockPageContext;

    @Mock
    private JspWriter mockJspWriter;

    @InjectMocks
    private final PriceFormatTag priceFormatTag = new PriceFormatTag();

    @Before
    public void setUp() {
        BDDMockito.given(mockPageContext.getOut()).willReturn(mockJspWriter);
    }

    @Test
    public void testDoTagWithCents() throws Exception {
        final String price = "$11.88";

        priceFormatTag.setValue(price);
        priceFormatTag.doTag();

        Mockito.verify(mockJspWriter).write(price);
    }

    @Test
    public void testDoTagWithZeroCents() throws Exception {
        final String price = "$11.00";

        priceFormatTag.setValue(price);
        priceFormatTag.doTag();

        Mockito.verify(mockJspWriter).write("$11");
    }
}
