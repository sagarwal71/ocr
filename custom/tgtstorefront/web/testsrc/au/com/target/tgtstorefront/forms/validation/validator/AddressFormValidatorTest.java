/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.validation.Errors;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtstorefront.forms.AddressForm;


/**
 * @author Benoit VanalderWeireldt
 * 
 */
@UnitTest
public class AddressFormValidatorTest {

    @Mock
    private CommonI18NService commonI18NService;

    @Mock
    private Errors errors;

    @Mock
    private AddressForm addressForm;

    @Mock
    private CountryModel country;

    @Mock
    private RegionModel region;

    @InjectMocks
    private final AddressFormValidator addressFormValidator = new AddressFormValidator();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(commonI18NService.getCountry(TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA)).thenReturn(country);
        Mockito.when(country.getIsocode()).thenReturn("AUS");
    }

    @Test
    public void testValidFormAustralia() {
        Mockito.when(addressForm.getCountryIso()).thenReturn(TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);
        Mockito.when(addressForm.getState()).thenReturn("VIC");
        Mockito.when(commonI18NService.getRegion(country, "VIC")).thenReturn(region);

        addressFormValidator.validate(addressForm, errors);
        Mockito.verify(errors, Mockito.never()).rejectValue(Mockito.anyString(), Mockito.anyString(),
                new Object[] { Mockito.any() }, Mockito.anyString());
    }

    @Test
    public void testInValidFormAustralia() {
        Mockito.when(addressForm.getCountryIso()).thenReturn(TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);
        Mockito.when(commonI18NService.getRegion(Mockito.any(CountryModel.class), Mockito.anyString())).thenThrow(
                new UnknownIdentifierException("From mockito"));
        addressFormValidator.validate(addressForm, errors);
        Mockito.verify(errors, Mockito.only()).rejectValue(Mockito.anyString(), Mockito.anyString(),
                new Object[] { Mockito.any() }, Mockito.anyString());
    }

    @Test
    public void testValidFormOtherCountry() {
        Mockito.when(addressForm.getCountryIso()).thenReturn("FR");
        addressFormValidator.validate(addressForm, errors);
        Mockito.verify(errors, Mockito.never()).rejectValue(Mockito.anyString(), Mockito.anyString(),
                new Object[] { Mockito.any() }, Mockito.anyString());
    }

    @Test
    public void testInValidFormOtherCountry() {
        Mockito.when(addressForm.getCountryIso()).thenReturn("FR");
        Mockito.when(commonI18NService.getCountry(Mockito.anyString())).thenThrow(
                new UnknownIdentifierException("From mockito"));
        addressFormValidator.validate(addressForm, errors);
        Mockito.verify(errors, Mockito.only()).rejectValue(Mockito.anyString(), Mockito.anyString(),
                new Object[] { Mockito.any() }, Mockito.anyString());
    }

}
