/**
 * 
 */
package au.com.target.tgtstorefront.controllers.services;

import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.order.TargetCheckoutResponseFacade;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CartServiceControllerTest {

    @InjectMocks
    private final CartServiceController controller = new CartServiceController();

    @Mock
    private TargetCheckoutResponseFacade targetCheckoutResponseFacade;

    @Test
    public void testGetApplicableDeliveryModes() {
        controller.getApplicableModes();
        verify(targetCheckoutResponseFacade).getApplicableDeliveryModes();
    }

    public void testGetCartSummary() {
        controller.getCartSummary();
        verify(targetCheckoutResponseFacade).getCartSummary();
    }

    @Test
        public void testShipsterStatus() {
            controller.shipsterStatus();
            verify(targetCheckoutResponseFacade).verifyShipsterStatus();
    
        }
}
