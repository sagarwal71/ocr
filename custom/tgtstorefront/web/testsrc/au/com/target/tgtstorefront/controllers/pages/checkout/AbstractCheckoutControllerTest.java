/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtfacades.cart.TargetOrderErrorHandlerFacade;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.TargetDiscountFacade;
import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


@SuppressWarnings("deprecation")
@UnitTest
public class AbstractCheckoutControllerTest {

    @InjectMocks
    private final AbstractCheckoutController controller = new AbstractCheckoutController() {
        // nothing to implement here
    };

    private final TargetZoneDeliveryModeData homeDelivery = new TargetZoneDeliveryModeData();

    private final TargetZoneDeliveryModeData storeDelivery = new TargetZoneDeliveryModeData();

    @Mock
    private TargetCheckoutFacade checkoutFacade;

    @Mock
    private FlybuysDiscountFacade flybuysDiscountFacade;

    @Mock
    private TargetCartData cartData;

    @Mock
    private RedirectAttributes redirectAttributes;

    @Mock
    private TargetLaybyCartService targetCartService;

    @Mock
    private TargetOrderErrorHandlerFacade targetOrderErrorHandlerFacade;

    @Mock
    private DeliveryModeModel deliveryMode;

    @Mock
    private TargetZoneDeliveryModeData targetDeliveryMode;

    @Mock
    private CartModel cartModel;

    @Mock
    private AbstractOrderEntryModel orderEntry;

    @Mock
    private AbstractTargetVariantProductModel product;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private Configuration config;

    @Mock
    private SalesApplicationFacade salesApplicationFacade;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Mock
    private SessionService sessionService;

    @Mock
    private TargetDiscountFacade targetDiscountFacade;

    @Mock
    private HttpSession mockSession;

    @Mock
    private RequestAttributes requestAttributes;

    @Mock
    private Model model;


    /**
     * Initial setip
     */
    @SuppressWarnings("boxing")
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        homeDelivery.setDeliveryToStore(false);
        storeDelivery.setDeliveryToStore(true);
        given(configurationService.getConfiguration()).willReturn(config);
    }

    /**
     * ApplyTMDIfItNotAppliedNull
     */
    @Test
    public void testApplyTMDIfItNotAppliedNull() {
        assertThat(controller.applyTMDIfItNotApplied(redirectAttributes, cartData, null)).isNull();
        Mockito.verifyZeroInteractions(redirectAttributes);
    }

    /**
     * ApplyTMDIfItNotAppliedWithoutFBReaccess
     */
    @SuppressWarnings("boxing")
    @Test
    public void testApplyTMDIfItNotAppliedWithoutFBReaccess() {
        given(cartData.getTmdNumber()).willReturn(null);
        given(flybuysDiscountFacade.reassessFlybuysDiscountOnCheckoutCart()).willReturn(false);

        assertThat(controller.applyTMDIfItNotApplied(redirectAttributes, cartData, "123")).isNull();
        Mockito.verify(checkoutFacade, Mockito.times(1)).setTeamMemberDiscountCardNumber("123");
        Mockito.verifyZeroInteractions(redirectAttributes);
    }

    /**
     * ApplyTMDIfItNotAppliedWithFBReaccess
     */
    @SuppressWarnings("boxing")
    @Test
    public void testApplyTMDIfItNotAppliedWithFBReaccess() {
        final Map mockGlobalMessages = Mockito.mock(Map.class);
        given(redirectAttributes.getFlashAttributes()).willReturn(mockGlobalMessages);
        given(cartData.getTmdNumber()).willReturn(null);
        given(flybuysDiscountFacade.reassessFlybuysDiscountOnCheckoutCart()).willReturn(true);

        final String errMsg = controller.applyTMDIfItNotApplied(redirectAttributes, cartData, "123");
        assertThat(errMsg).isNotNull();
        assertThat(errMsg).isEqualTo(ControllerConstants.Redirection.CHECKOUT_PAYMENT);
        Mockito.verify(checkoutFacade, Mockito.times(2)).setTeamMemberDiscountCardNumber("123");
        Mockito.verify(mockGlobalMessages, Mockito.times(1)).put(Mockito.anyString(), Mockito.any());
    }

    /**
     * tHandleOrderCreationFailed
     */
    @SuppressWarnings("boxing")
    @Test
    public void testHandleOrderCreationFailed() {

        final String redirect = controller.handleOrderCreationFailed(null, null);
        controller.setTargetOrderErrorHandlerFacade(targetOrderErrorHandlerFacade);
        assertThat(redirect).isNotNull();
        assertThat(redirect).isEqualTo(ControllerConstants.Redirection.CHECKOUT_THANK_YOU_CHECK + "?cart=" + null);

    }

    @Test
    public void testCheckIpgPayment() {
        final TargetCCPaymentInfoData paymentInfo = new TargetCCPaymentInfoData();
        assertThat(controller.isIpgPayment(paymentInfo)).isFalse();
        paymentInfo.setIpgPaymentInfo(true);
        assertThat(controller.isIpgPayment(paymentInfo)).isTrue();
        paymentInfo.setIpgPaymentInfo(false);
        assertThat(controller.isIpgPayment(paymentInfo)).isFalse();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testSetPageableDataForCncStores() {

        given(config.getInt(ControllerConstants.CNC_STORE_NUMBER_PAGESIZE)).willReturn(5);
        final PageableData page = controller.setPageableDataForCncStore();

        assertThat(page.getPageSize()).isEqualTo(Integer.valueOf(5));
    }

    @Test
    public void testValidateTMDForPromotionsAppliedToOrder() throws CMSItemNotFoundException {

        final String tmdCardNumber = "123";
        setUpForTMDCardValidation(tmdCardNumber, true);

        final String message = controller.validateTMD(model, tmdCardNumber);
        assertThat(message).isEqualTo(ControllerConstants.Views.Pages.MultiStepCheckout.VALIDATE_TMD_PAGE);
        verify(model).addAttribute("isTMDCardValid", Boolean.TRUE);


    }

    @Test
    public void testValidateTMDForNoPromotionsAppliedToOrder() throws CMSItemNotFoundException {

        final String tmdCardNumber = "123";
        setUpForTMDCardValidation(tmdCardNumber, false);

        final String message = controller.validateTMD(model, tmdCardNumber);
        assertThat(message).isEqualTo(ControllerConstants.Views.Pages.MultiStepCheckout.VALIDATE_TMD_PAGE);
        verify(model).addAttribute("isTMDCardValid", Boolean.FALSE);


    }

    @SuppressWarnings("boxing")
    private void setUpForTMDCardValidation(final String tmdCardNumber, final boolean value) {
        given(targetDiscountFacade.validateTeamMemberDiscountCard(tmdCardNumber)).willReturn(true);
        given(requestAttributes.getSessionMutex()).willReturn(mockSession);
        RequestContextHolder.setRequestAttributes(requestAttributes);
        given(checkoutFacade.setTeamMemberDiscountCardNumber(tmdCardNumber)).willReturn(value);
        given(flybuysDiscountFacade.reassessFlybuysDiscountOnCheckoutCart()).willReturn(true);
        given(checkoutFacade.getCheckoutCart()).willReturn(cartData);
    }



}
