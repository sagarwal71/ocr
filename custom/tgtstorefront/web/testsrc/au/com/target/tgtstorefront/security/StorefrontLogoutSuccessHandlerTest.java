/**
 * 
 */
package au.com.target.tgtstorefront.security;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;

import au.com.target.tgtstorefront.cookie.TargetCookieStrategy;
import au.com.target.tgtstorefront.customersegment.impl.CustomerSegmentCookieStrategy;
import au.com.target.tgtstorefront.security.cookie.EnhancedCookieGenerator;


/**
 * Test for StorefrontLogoutSuccessHandler
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class StorefrontLogoutSuccessHandlerTest {

    private static final String DEFAULT_URL = "/default";

    @InjectMocks
    private final StorefrontLogoutSuccessHandler handler = new StorefrontLogoutSuccessHandler();

    @Mock
    private HttpServletRequest mockRequest;

    @Mock
    private HttpServletResponse mockResponse;

    @Mock
    private EnhancedCookieGenerator preferredStoreCookieGenerator;

    @Mock
    private TargetCookieStrategy guidCookieStrategy;

    @Mock
    private CustomerSegmentCookieStrategy customerSegmentCookieStrategy;

    @Mock
    private CMSSiteService cmsSiteService;

    @Mock
    private Authentication authentication;

    @Before
    public void setup() {

        handler.setDefaultTargetUrl(DEFAULT_URL);

        final Map<String, String> map = new HashMap<String, String>();
        map.put("blah", "/blah");
        map.put("kiosk", "/kiosk-reset");
        handler.setAlternateTargetUrls(map);

    }

    @Test
    public void testDetermineTargetUrlWithNoParam() {

        given(mockRequest.getParameter("forward")).willReturn(null);
        final String result = handler.determineTargetUrl(mockRequest, mockResponse);
        Assert.assertEquals(DEFAULT_URL, result);
    }

    @Test
    public void testOnLogoutSuccess() throws IOException, ServletException {

        handler.onLogoutSuccess(mockRequest, mockResponse, authentication);
        verify(guidCookieStrategy).deleteCookie(mockRequest, mockResponse);
        verify(customerSegmentCookieStrategy).deleteCookie(mockRequest, mockResponse);
        verify(preferredStoreCookieGenerator).removeCookie(mockResponse);
    }


    @Test
    public void testDetermineTargetUrlWithMissingParam() {

        given(mockRequest.getParameter("forward")).willReturn("notthere");

        final String result = handler.determineTargetUrl(mockRequest, mockResponse);
        Assert.assertEquals(DEFAULT_URL, result);
    }

    @Test
    public void testDetermineTargetUrlWithParam() {

        given(mockRequest.getParameter("forward")).willReturn("kiosk");

        final String result = handler.determineTargetUrl(mockRequest, mockResponse);
        Assert.assertEquals("/kiosk-reset", result);
    }


}
