/**
 *
 */
package au.com.target.tgtstorefront.controllers.cms;

import au.com.target.tgtstorefront.navigation.TargetNavigationComponentMenuBuilder;
import au.com.target.tgtwebcore.model.cms2.components.TargetNavigationComponentModel;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

import au.com.target.tgtfacades.navigation.NavigationMenuItem;
import au.com.target.tgtstorefront.constants.WebConstants;


/**
 * @author rmcalave
 *
 */
public class TargetNavigationComponentControllerTest {
    @InjectMocks
    private final TargetNavigationComponentController navigationComponentController = new TargetNavigationComponentController();

    @Mock
    private TargetNavigationComponentMenuBuilder mockNavigationComponentMenuBuilder;

    @Mock
    private Model mockModel;

    @Mock
    private TargetNavigationComponentModel mockComponent;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Ignore
    @Test
    public void testFillModelWithNoMenuItem() {
        BDDMockito.given(mockNavigationComponentMenuBuilder.createNavigationComponentMenu(mockComponent)).willReturn(
                null);

        navigationComponentController.fillModel(null, mockModel, mockComponent);
        Mockito.verifyZeroInteractions(mockModel);
    }

    @Test
    public void testFillModelWithMenuItem() {
        final NavigationMenuItem mockNavigationMenuItem = Mockito.mock(NavigationMenuItem.class);
        BDDMockito.given(mockNavigationComponentMenuBuilder.createNavigationComponentMenu(mockComponent)).willReturn(
                mockNavigationMenuItem);

        navigationComponentController.fillModel(null, mockModel, mockComponent);
        Mockito.verify(mockModel).addAttribute(WebConstants.NAVIGATION_COMPONENT_MENU_ITEM, mockNavigationMenuItem);
    }
}
