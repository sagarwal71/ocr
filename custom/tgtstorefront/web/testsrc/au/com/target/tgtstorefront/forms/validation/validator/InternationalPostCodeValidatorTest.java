/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import de.hybris.platform.servicelayer.i18n.I18NService;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;

import au.com.target.tgtstorefront.forms.validation.InternationalPostCode;


/**
 * @author asingh78
 * 
 */
public class InternationalPostCodeValidatorTest {


    @Mock
    private InternationalPostCode internationalPostCode;

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Mock
    private ConstraintViolationBuilder constraintViolationBuilder;

    @InjectMocks
    private final InternationalPostCodeValidator internationalPostCodeValidator = new InternationalPostCodeValidator() {
        {
            final MessageSource messageSourceMocked = BDDMockito.mock(MessageSource.class);
            this.messageSource = messageSourceMocked;
            final I18NService i18nServiceMocked = BDDMockito.mock(I18NService.class);
            this.i18nService = i18nServiceMocked;
        }
    };

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        internationalPostCodeValidator.initialize(internationalPostCode);
        BDDMockito.given(constraintValidatorContext.buildConstraintViolationWithTemplate(BDDMockito.anyString()))
                .willReturn(constraintViolationBuilder);
    }

    @Test
    public void correctInternationalPostCode() {
        Assert.assertTrue(internationalPostCodeValidator.isValid("--", constraintValidatorContext));
        Assert.assertTrue(internationalPostCodeValidator.isValid("   --    ", constraintValidatorContext));
        Assert.assertTrue(internationalPostCodeValidator.isValid("a1 --    ", constraintValidatorContext));
        Assert.assertTrue(internationalPostCodeValidator.isValid("ab--    ", constraintValidatorContext));
        Assert.assertTrue(internationalPostCodeValidator.isValid("111--    ", constraintValidatorContext));
        Assert.assertTrue(internationalPostCodeValidator.isValid("1111111111 ", constraintValidatorContext));
        Assert.assertTrue(internationalPostCodeValidator.isValid("-   - ", constraintValidatorContext));
    }

    @Test
    public void inCorrectInternationalPostCode() {
        Assert.assertFalse(internationalPostCodeValidator.isValid("-            -", constraintValidatorContext));
        Assert.assertFalse(internationalPostCodeValidator.isValid("111111111111111", constraintValidatorContext));
        Assert.assertFalse(internationalPostCodeValidator.isValid(" ", constraintValidatorContext));
        Assert.assertFalse(internationalPostCodeValidator.isValid("ab--12111111", constraintValidatorContext));
        Assert.assertFalse(internationalPostCodeValidator.isValid("ab-$", constraintValidatorContext));
        Assert.assertFalse(internationalPostCodeValidator.isValid("ab-,", constraintValidatorContext));
    }


}
