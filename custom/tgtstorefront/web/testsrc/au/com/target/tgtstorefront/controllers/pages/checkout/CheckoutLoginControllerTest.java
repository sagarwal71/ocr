/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Assert;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.order.TargetCartFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.cookie.TargetCookieStrategy;
import au.com.target.tgtstorefront.data.MessageKeyArguments;
import au.com.target.tgtstorefront.forms.GuestForm;
import au.com.target.tgtstorefront.forms.LoginForm;
import au.com.target.tgtstorefront.forms.RegisterForm;
import au.com.target.tgtstorefront.util.PageTitleResolver;


/**
 * @author asingh78
 * 
 */
@UnitTest
public class CheckoutLoginControllerTest {
    private static final String CHECKOUT_LOGIN_PAGE = "pages/checkout/checkoutLandingPage";
    private static final String FEATURE_GUEST_CHECKOUT = "checkout.allow.guest";

    @Mock
    private TargetCookieStrategy guidCookieStrategy;
    @Mock
    private TargetCustomerFacade targetCustomerFacade;
    @Mock
    private Model model;
    @Mock
    private BindingResult bindingResult;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession session;
    @Mock
    private SessionService sessionService;
    @Mock
    private TargetCheckoutFacade mockTargetCheckoutFacade;
    @Mock
    private TargetCartFacade targetCartFacade;
    @Mock
    private CMSPageService cmsPageService;
    @Mock
    private PageTitleResolver pageTitleResolver;
    @Mock
    private I18NService i18nService;
    @Mock
    private MessageSource messageSource;
    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;
    @Mock
    private SalesApplicationFacade salesApplicationFacade;
    @Mock
    private TargetCheckoutFacade checkoutFacade;

    @InjectMocks
    private final CheckoutLoginController checkoutLoginController = new CheckoutLoginController();

    @Before
    public void startUp() {
        MockitoAnnotations.initMocks(this);
    }

    @SuppressWarnings("boxing")
    @Test
    public void processAnonymousCheckoutUserRequestTest() throws CMSItemNotFoundException {
        final CartData cartData = Mockito.mock(CartData.class);
        final List<OrderEntryData> entries = new ArrayList<>();
        final OrderEntryData orderEntryData = new OrderEntryData();
        entries.add(orderEntryData);
        BDDMockito.given(cartData.getEntries()).willReturn(entries);
        BDDMockito.given(bindingResult.hasErrors()).willReturn(false);
        BDDMockito.given(mockTargetCheckoutFacade.getCheckoutCart()).willReturn(cartData);
        final GuestForm form = new GuestForm();
        form.setEmail("as@as.com");
        final String result = checkoutLoginController.processAnonymousCheckoutUserRequest(form, bindingResult, model,
                request, response);
        Assert.assertEquals(UrlBasedViewResolver.REDIRECT_URL_PREFIX + "/checkout", result);
    }

    @SuppressWarnings("boxing")
    @Test
    public void processAnonymousCheckoutUserForDuplicateUidExceptionRequestTest() throws CMSItemNotFoundException,
            DuplicateUidException {
        final CartData cartData = Mockito.mock(CartData.class);
        final List<OrderEntryData> entries = new ArrayList<>();
        final OrderEntryData orderEntryData = new OrderEntryData();
        entries.add(orderEntryData);
        BDDMockito.given(cartData.getEntries()).willReturn(entries);
        BDDMockito.given(mockTargetCheckoutFacade.getCheckoutCart()).willReturn(cartData);
        final GuestForm form = new GuestForm();
        form.setEmail("as@as.com");
        BDDMockito.given(bindingResult.hasErrors()).willReturn(false);
        final CheckoutLoginController controller = Mockito.spy(checkoutLoginController);
        Mockito.doReturn(CHECKOUT_LOGIN_PAGE).when(controller).handleRegistrationError(model);
        BDDMockito.doThrow(new DuplicateUidException()).when(targetCustomerFacade)
                .createGuestUserForAnonymousCheckout(BDDMockito.anyString(), BDDMockito.anyString());
        final String result = controller.processAnonymousCheckoutUserRequest(form, bindingResult, model,
                request, response);
        Assert.assertEquals(CHECKOUT_LOGIN_PAGE, result);
    }

    @SuppressWarnings("boxing")
    @Test
    public void processAnonymousCheckoutUserHavingBindingErrorRequestTest() throws CMSItemNotFoundException {
        final CartData cartData = Mockito.mock(CartData.class);
        final List<OrderEntryData> entries = new ArrayList<>();
        final OrderEntryData orderEntryData = new OrderEntryData();
        entries.add(orderEntryData);
        BDDMockito.given(cartData.getEntries()).willReturn(entries);
        BDDMockito.given(bindingResult.hasErrors()).willReturn(true);
        BDDMockito.given(mockTargetCheckoutFacade.getCheckoutCart()).willReturn(cartData);
        final GuestForm form = new GuestForm();
        form.setEmail("as@as.com");
        final CheckoutLoginController controller = Mockito.spy(checkoutLoginController);
        final Model myModel = new ExtendedModelMap();
        Mockito.doReturn(CHECKOUT_LOGIN_PAGE).when(controller).handleRegistrationError(myModel);
        final String result = controller.processAnonymousCheckoutUserRequest(form, bindingResult, myModel,
                request, response);
        final Map<String, Object> modelMap = myModel.asMap();
        Assert.assertTrue(modelMap.get("loginForm") instanceof LoginForm);
        Assert.assertTrue(modelMap.get("guestForm") instanceof GuestForm);
        Assert.assertTrue(modelMap.get("registerForm") instanceof RegisterForm);
        Assert.assertEquals(CHECKOUT_LOGIN_PAGE, result);
    }

    @Test
    public void verifyCheckoutLoginAddsForwardedPageNameToModel() throws CMSItemNotFoundException {
        final ContentPageModel pageModel = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(cmsPageService.getPageForLabelOrId(Mockito.anyString())).willReturn(pageModel);
        final CheckoutLoginController controller = Mockito.spy(checkoutLoginController);
        controller.doCheckoutLogin(session, model, null);
        Mockito.verify(model, Mockito.times(1)).addAttribute("forwardPage", ControllerConstants.Views.PageNames.BASKET);
    }

    @SuppressWarnings("boxing")
    @Test
    public void verifyCartDoesNotAllowGuestCheckoutWhenEGiftCardsInBasket() {
        final TargetCartData cartData = Mockito.mock(TargetCartData.class);

        given(targetFeatureSwitchFacade.isFeatureEnabled(FEATURE_GUEST_CHECKOUT)).willReturn(Boolean.TRUE);
        given(checkoutFacade.isGiftCardInCart()).willReturn(Boolean.TRUE);
        given(checkoutFacade.getCheckoutCart()).willReturn(cartData);

        checkoutLoginController.addCartData(model);

        verify(cartData, times(1)).setAllowGuestCheckout(false);
        verify(model, times(1)).addAttribute("cartData", cartData);
    }

    @SuppressWarnings("boxing")
    @Test
    public void verifyCartAllowsGuestCheckoutWhenNoEGiftCardsInBasket() {
        final TargetCartData cartData = Mockito.mock(TargetCartData.class);

        given(targetFeatureSwitchFacade.isFeatureEnabled(FEATURE_GUEST_CHECKOUT)).willReturn(Boolean.TRUE);
        given(checkoutFacade.isGiftCardInCart()).willReturn(Boolean.FALSE);
        given(checkoutFacade.getCheckoutCart()).willReturn(cartData);

        checkoutLoginController.addCartData(model);

        verify(cartData, times(0)).setAllowGuestCheckout(false);
        verify(model, times(1)).addAttribute("cartData", cartData);
    }

    @Test
    public void verifyModelWhenDoCheckoutLoginCalledWithNoErrorMessage() throws CMSItemNotFoundException {
        final Model modelMap = new BindingAwareModelMap();
        final ContentPageModel pageModel = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(cmsPageService.getPageForLabelOrId(Mockito.anyString())).willReturn(pageModel);
        checkoutLoginController.doCheckoutLogin(session, modelMap, null);
        Assertions.assertThat(modelMap.asMap().get("accErrorMsgs")).isNull();
    }

    @Test
    public void verifyModelWhenDoCheckoutLoginCalledWithErrorMessage() throws CMSItemNotFoundException {
        final Model modelMap = new BindingAwareModelMap();
        final ContentPageModel pageModel = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(cmsPageService.getPageForLabelOrId(Mockito.anyString())).willReturn(pageModel);
        checkoutLoginController.doCheckoutLogin(session, modelMap, ControllerConstants.Views.ErrorActions.CHECKOUT);
        Assertions.assertThat(modelMap.asMap().get("accErrorMsgs")).isNotNull();
        final List<MessageKeyArguments> messageKeys = (List<MessageKeyArguments>)(modelMap.asMap().get("accErrorMsgs"));
        Assertions.assertThat(messageKeys.get(0).getKey()).isEqualTo(
                "checkout.error.paymentmethod.ipg.transactiondetails.queryresult.unavailable");
    }
}
