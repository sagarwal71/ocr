/**
 * 
 */
package au.com.target.tgtmail.client;

import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionResponseDto;

/**
 * @author mjanarth
 * 
 */
public interface TargetCustomerSubscriptionClient {
    /**
     * Send Customer subscription
     * 
     * @param subscriptionDetails
     * @return TargetCustomerSubscriptionResponseDto
     */
    public TargetCustomerSubscriptionResponseDto sendCustomerSubscription(
            TargetCustomerSubscriptionRequestDto subscriptionDetails);
}
