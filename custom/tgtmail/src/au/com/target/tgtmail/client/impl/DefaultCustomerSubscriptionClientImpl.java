/**
 * 
 */
package au.com.target.tgtmail.client.impl;

import org.apache.log4j.Logger;

import au.com.target.tgtmail.client.TargetCustomerSubscriptionClient;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionResponseDto;


/**
 * @author mjanarth
 * 
 */
public class DefaultCustomerSubscriptionClientImpl implements TargetCustomerSubscriptionClient {


    private static final Logger LOG = Logger.getLogger(DefaultCustomerSubscriptionClientImpl.class);

    /* (non-Javadoc)
     * @see au.com.target.tgtmail.client.TargetCustomerSubscriptionClient#sendCustomerSubscription(au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto)
     */
    @Override
    public TargetCustomerSubscriptionResponseDto sendCustomerSubscription(
            final TargetCustomerSubscriptionRequestDto subscriptionDetails) {
        LOG.info("sendCustomerSubscription :: default implementation");
        return new TargetCustomerSubscriptionResponseDto();
    }




}
