/**
 * 
 */
package au.com.target.tgtmail.enums;

/**
 * @author mjanarth
 * 
 */
public enum TargetCustomerSubscriptionResponseType {
    SUCCESS("success"),
    ALREADY_EXISTS("alreadyExists"),
    UNAVAILABLE("unavailable"),
    INVALID("invalid"),
    OTHERS("others");

    private final String response;

    /**
     * 
     * @param response
     */
    private TargetCustomerSubscriptionResponseType(final String response)
    {

        this.response = response;
    }

    /**
     * @return the response
     */
    public String getResponse() {
        return response;
    }

}
