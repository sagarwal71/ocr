/**
 * 
 */
package au.com.target.tgtmail.service;

import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionResponseDto;



/**
 * @author rmcalave
 * 
 */
public interface TargetCustomerSubscriptionService {

    /**
     * update the customer details (eNews) via business process
     * 
     * @param updateCustomerSubscriptionDto
     * @return boolean
     */
    boolean processCustomerSubscription(TargetCustomerSubscriptionRequestDto updateCustomerSubscriptionDto);

    /**
     * update the customer details directly via web methods and return result dto.
     * 
     * @param updateCustomerSubscriptionDto
     * @return responseCode
     */
    TargetCustomerSubscriptionResponseDto createCustomerSubscription(
            TargetCustomerSubscriptionRequestDto updateCustomerSubscriptionDto);

    /**
     * update the customer personal details(Mumshub)
     * 
     * @param updateCustomerSubscriptionDto
     * @return boolean
     */
    boolean processCustomerPersonalDetails(
            TargetCustomerSubscriptionRequestDto updateCustomerSubscriptionDto);

}