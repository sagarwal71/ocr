package au.com.target.tgtmail.model;

import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * Model representation for an email address.
 * 
 * @author Olivier Lamy
 */
@XmlRootElement(name = "emailAddress")
@XmlAccessorType(XmlAccessType.FIELD)
public class EmailAddress
        implements Serializable
{

    private String displayName;

    private String address;

    public EmailAddress()
    {
        // no op
    }

    public EmailAddress(final String displayName, final String address)
    {
        this.displayName = displayName;
        this.address = address;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(final String displayName)
    {
        this.displayName = displayName;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(final String address)
    {
        this.address = address;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("EmailAddress{");
        sb.append("displayName='").append(displayName).append('\'');
        sb.append(", address='").append(address).append('\'');
        sb.append('}');
        return sb.toString();
    }


    public static class Builder {
        private String emailAddress;
        private String name;

        public Builder() {
            // no op
        }

        public Builder withAddress(final String address) {
            this.emailAddress = address;
            return this;
        }

        public Builder withDisplayName(final String displayName) {
            this.name = displayName;
            return this;
        }


        public Builder withEmailAddressModel(final EmailAddressModel emailAddressModel) {
            this.emailAddress = emailAddressModel.getEmailAddress();
            this.name = emailAddressModel.getDisplayName();
            return this;
        }

        public EmailAddress build() {
            if (this.emailAddress == null) {
                throw new IllegalArgumentException("emailAddress cannot be null");
            }
            return new EmailAddress(this.name, this.emailAddress);
        }
    }
}
