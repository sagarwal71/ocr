/**
 * 
 */
package au.com.target.tgtmail.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionResponseDto;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionResponseType;
import au.com.target.tgtmail.service.TargetCustomerSubscriptionService;


/**
 * @author bhuang3
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendCustomerSubscriptionActionTest {

    @InjectMocks
    private final SendCustomerSubscriptionAction sendCustomerSubscriptionAction = new SendCustomerSubscriptionAction();

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private TargetCustomerSubscriptionService targetCustomerSubscriptionService;

    @Mock
    private BusinessProcessModel processModel;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testExecuteInternalWithResponseSuccess() throws RetryLaterException, Exception {
        final TargetCustomerSubscriptionRequestDto requestDto = new TargetCustomerSubscriptionRequestDto();
        BDDMockito.given(orderProcessParameterHelper.getCustomerSubscriptionData(processModel)).willReturn(requestDto);
        final TargetCustomerSubscriptionResponseDto responseDto = new TargetCustomerSubscriptionResponseDto();
        responseDto.setResponseType(TargetCustomerSubscriptionResponseType.SUCCESS);
        BDDMockito.given(targetCustomerSubscriptionService.createCustomerSubscription(requestDto))
                .willReturn(responseDto);

        final String result = sendCustomerSubscriptionAction.executeInternal(processModel);
        Assert.assertEquals("OK", result);
    }

    @Test
    public void testExecuteInternalWithResponseOthers() throws RetryLaterException, Exception {
        final TargetCustomerSubscriptionRequestDto requestDto = new TargetCustomerSubscriptionRequestDto();
        BDDMockito.given(orderProcessParameterHelper.getCustomerSubscriptionData(processModel)).willReturn(requestDto);
        final TargetCustomerSubscriptionResponseDto responseDto = new TargetCustomerSubscriptionResponseDto();
        responseDto.setResponseType(TargetCustomerSubscriptionResponseType.OTHERS);
        responseDto.setResponseMessage("test others message");
        responseDto.setResponseCode(Integer.valueOf(-3));
        BDDMockito.given(targetCustomerSubscriptionService.createCustomerSubscription(requestDto))
                .willReturn(responseDto);

        final String result = sendCustomerSubscriptionAction.executeInternal(processModel);
        Assert.assertEquals("NOK", result);
    }

    @Test
    public void testExecuteInternalWithResponseAlreadyExists() throws RetryLaterException, Exception {
        final TargetCustomerSubscriptionRequestDto requestDto = new TargetCustomerSubscriptionRequestDto();
        BDDMockito.given(orderProcessParameterHelper.getCustomerSubscriptionData(processModel)).willReturn(requestDto);
        final TargetCustomerSubscriptionResponseDto responseDto = new TargetCustomerSubscriptionResponseDto();
        responseDto.setResponseType(TargetCustomerSubscriptionResponseType.ALREADY_EXISTS);
        responseDto.setResponseMessage("test others message");
        responseDto.setResponseCode(Integer.valueOf(-4));
        BDDMockito.given(targetCustomerSubscriptionService.createCustomerSubscription(requestDto))
                .willReturn(responseDto);

        final String result = sendCustomerSubscriptionAction.executeInternal(processModel);
        Assert.assertEquals("OK", result);
    }

    @Test
    public void testExecuteInternalWithResponseInvalid() throws RetryLaterException, Exception {
        final TargetCustomerSubscriptionRequestDto requestDto = new TargetCustomerSubscriptionRequestDto();
        BDDMockito.given(orderProcessParameterHelper.getCustomerSubscriptionData(processModel)).willReturn(requestDto);
        final TargetCustomerSubscriptionResponseDto responseDto = new TargetCustomerSubscriptionResponseDto();
        responseDto.setResponseType(TargetCustomerSubscriptionResponseType.INVALID);
        responseDto.setResponseMessage("test Invalid message");
        responseDto.setResponseCode(Integer.valueOf(1));
        BDDMockito.given(targetCustomerSubscriptionService.createCustomerSubscription(requestDto))
                .willReturn(responseDto);

        final String result = sendCustomerSubscriptionAction.executeInternal(processModel);
        Assert.assertEquals("NOK", result);
    }

    @Test
    public void testExecuteInternalWithResponseUnavaliable() throws Exception {
        final TargetCustomerSubscriptionRequestDto requestDto = new TargetCustomerSubscriptionRequestDto();
        BDDMockito.given(orderProcessParameterHelper.getCustomerSubscriptionData(processModel)).willReturn(requestDto);
        BDDMockito.given(processModel.getCode()).willReturn("customerspersondetailssubscription");
        final TargetCustomerSubscriptionResponseDto responseDto = new TargetCustomerSubscriptionResponseDto();
        responseDto.setResponseType(TargetCustomerSubscriptionResponseType.UNAVAILABLE);
        responseDto.setResponseMessage("test unavaliable message");
        responseDto.setResponseCode(Integer.valueOf(1));
        BDDMockito.given(targetCustomerSubscriptionService.createCustomerSubscription(requestDto))
                .willReturn(responseDto);

        try {
            sendCustomerSubscriptionAction.executeInternal(processModel);
        }
        catch (final RetryLaterException e) {
            Assert.assertEquals("Customer subscription service is unavailable, subscription details:"
                    + requestDto.toString(), e.getMessage());
        }

    }

    @Test
    public void testExecuteInternalWithResponseTypeNull() throws RetryLaterException, Exception {
        final TargetCustomerSubscriptionRequestDto requestDto = new TargetCustomerSubscriptionRequestDto();
        BDDMockito.given(orderProcessParameterHelper.getCustomerSubscriptionData(processModel)).willReturn(requestDto);
        final TargetCustomerSubscriptionResponseDto responseDto = new TargetCustomerSubscriptionResponseDto();
        responseDto.setResponseType(null);
        BDDMockito.given(targetCustomerSubscriptionService.createCustomerSubscription(requestDto))
                .willReturn(responseDto);

        final String result = sendCustomerSubscriptionAction.executeInternal(processModel);
        Assert.assertEquals("NOK", result);
    }

    @Test
    public void testExecuteInternalWithResponseDtoNull() throws RetryLaterException, Exception {
        final TargetCustomerSubscriptionRequestDto requestDto = new TargetCustomerSubscriptionRequestDto();
        BDDMockito.given(orderProcessParameterHelper.getCustomerSubscriptionData(processModel)).willReturn(requestDto);
        final TargetCustomerSubscriptionResponseDto responseDto = null;
        BDDMockito.given(targetCustomerSubscriptionService.createCustomerSubscription(requestDto))
                .willReturn(responseDto);

        final String result = sendCustomerSubscriptionAction.executeInternal(processModel);
        Assert.assertEquals("NOK", result);
    }

    @Test
    public void testExecuteInternalWithRequestDtoNull() throws RetryLaterException, Exception {
        final TargetCustomerSubscriptionRequestDto requestDto = null;
        BDDMockito.given(orderProcessParameterHelper.getCustomerSubscriptionData(processModel)).willReturn(requestDto);

        final String result = sendCustomerSubscriptionAction.executeInternal(processModel);
        Assert.assertEquals("NOK", result);
    }


}
