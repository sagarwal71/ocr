package au.com.target.tgtlayby.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.session.SessionService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtlayby.constants.TgtlaybyConstants;
import au.com.target.tgtlayby.model.PurchaseOptionModel;


/**
 * Unit test for {@link CheckoutCartRemoveInterceptor}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CheckoutCartRemoveInterceptorTest {

    @InjectMocks
    private final CheckoutCartRemoveInterceptor checkoutCartRemoveInterceptor = new CheckoutCartRemoveInterceptor();

    @Mock
    private SessionService sessionService;

    @Mock
    private InterceptorContext interceptorContext;

    @Test
    public void testOnRemoveWithNullModel() throws InterceptorException {
        checkoutCartRemoveInterceptor.onRemove(null, interceptorContext);
        Mockito.verifyZeroInteractions(sessionService);
    }

    @Test
    public void testOnRemoveWithNonCartModel() throws InterceptorException {
        checkoutCartRemoveInterceptor.onRemove("dummyObject", interceptorContext);
        Mockito.verifyZeroInteractions(sessionService);
    }

    @Test
    public void testOnRemoveWithMasterCart() throws InterceptorException {
        final CartModel cartModel = Mockito.mock(CartModel.class);
        checkoutCartRemoveInterceptor.onRemove(cartModel, interceptorContext);
        Mockito.verifyZeroInteractions(sessionService);
    }

    @Test
    public void testOnRemoveWithCheckoutCart() throws InterceptorException {
        final CartModel cartModel = Mockito.mock(CartModel.class);
        final PurchaseOptionModel purchaseOptionModel = Mockito.mock(PurchaseOptionModel.class);

        BDDMockito.given(purchaseOptionModel.getCode()).willReturn("buynow");
        BDDMockito.given(cartModel.getPurchaseOption()).willReturn(purchaseOptionModel);

        checkoutCartRemoveInterceptor.onRemove(cartModel, interceptorContext);
        Mockito.verify(sessionService).removeAttribute(TgtlaybyConstants.SESSION_CHECKOUT_CART + "buynow");
    }

}
