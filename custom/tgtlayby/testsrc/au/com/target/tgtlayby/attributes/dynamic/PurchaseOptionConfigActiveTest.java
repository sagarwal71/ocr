package au.com.target.tgtlayby.attributes.dynamic;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;


/**
 * 
 * Unit Test for {@link PurchaseOptionConfigActive}
 */
@UnitTest
public class PurchaseOptionConfigActiveTest {

    private final PurchaseOptionConfigActive attrHandler = new PurchaseOptionConfigActive();
    private Date now;
    private Date soon;
    private Date today;
    private Date yesterday;
    private Date tomorrow;

    @Before
    public void setup() {
        now = new Date();
        soon = DateUtils.addSeconds(now, 5); // more than long enough for this test to run
        today = DateUtils.truncate(now, Calendar.DAY_OF_MONTH);
        yesterday = DateUtils.addDays(today, -1);
        tomorrow = DateUtils.addDays(today, 1);
    }

    @Test
    public void testActiveForNullFromTo() {
        final PurchaseOptionConfigModel model = Mockito.mock(PurchaseOptionConfigModel.class);
        Assert.assertEquals(Boolean.TRUE, attrHandler.get(model));
    }

    @Test
    public void testActiveForNullFromPastTo() {
        final PurchaseOptionConfigModel model = Mockito.mock(PurchaseOptionConfigModel.class);
        BDDMockito.given(model.getValidTo()).willReturn(yesterday);

        Assert.assertEquals(Boolean.FALSE, attrHandler.get(model));
    }

    @Test
    public void testActiveForNullFromTodayTo() {
        final PurchaseOptionConfigModel model = Mockito.mock(PurchaseOptionConfigModel.class);
        BDDMockito.given(model.getValidTo()).willReturn(soon);

        Assert.assertEquals(Boolean.TRUE, attrHandler.get(model));
    }

    @Test
    public void testActiveForNullFromFutureTo() {
        final PurchaseOptionConfigModel model = Mockito.mock(PurchaseOptionConfigModel.class);
        BDDMockito.given(model.getValidTo()).willReturn(tomorrow);

        Assert.assertEquals(Boolean.TRUE, attrHandler.get(model));
    }

    @Test
    public void testActiveForNullToPastFrom() {
        final PurchaseOptionConfigModel model = Mockito.mock(PurchaseOptionConfigModel.class);
        BDDMockito.given(model.getValidFrom()).willReturn(yesterday);

        Assert.assertEquals(Boolean.TRUE, attrHandler.get(model));
    }

    @Test
    public void testActiveForNullToTodayFrom() {
        final PurchaseOptionConfigModel model = Mockito.mock(PurchaseOptionConfigModel.class);
        BDDMockito.given(model.getValidFrom()).willReturn(today);

        Assert.assertEquals(Boolean.TRUE, attrHandler.get(model));
    }

    @Test
    public void testActiveForNullToFutureFrom() {
        final PurchaseOptionConfigModel model = Mockito.mock(PurchaseOptionConfigModel.class);
        BDDMockito.given(model.getValidFrom()).willReturn(tomorrow);

        Assert.assertEquals(Boolean.FALSE, attrHandler.get(model));
    }

    @Test
    public void testActiveForPastToFrom() {
        final PurchaseOptionConfigModel model = Mockito.mock(PurchaseOptionConfigModel.class);
        BDDMockito.given(model.getValidTo()).willReturn(yesterday);
        BDDMockito.given(model.getValidFrom()).willReturn(yesterday);

        Assert.assertEquals(Boolean.FALSE, attrHandler.get(model));
    }

    @Test
    public void testActiveForTodayToFrom() {
        final PurchaseOptionConfigModel model = Mockito.mock(PurchaseOptionConfigModel.class);
        BDDMockito.given(model.getValidTo()).willReturn(soon);
        BDDMockito.given(model.getValidFrom()).willReturn(now);

        Assert.assertEquals(Boolean.TRUE, attrHandler.get(model));
    }

    @Test
    public void testActiveForFutureToFrom() {
        final PurchaseOptionConfigModel model = Mockito.mock(PurchaseOptionConfigModel.class);
        BDDMockito.given(model.getValidTo()).willReturn(tomorrow);
        BDDMockito.given(model.getValidFrom()).willReturn(tomorrow);

        Assert.assertEquals(Boolean.FALSE, attrHandler.get(model));
    }

    @Test
    public void testActiveForFutureToPastFrom() {
        final PurchaseOptionConfigModel model = Mockito.mock(PurchaseOptionConfigModel.class);
        BDDMockito.given(model.getValidTo()).willReturn(tomorrow);
        BDDMockito.given(model.getValidFrom()).willReturn(yesterday);

        Assert.assertEquals(Boolean.TRUE, attrHandler.get(model));
    }

    // obviously invalid, but handle it anyway
    @Test
    public void testActiveForPastToFutureFrom() {
        final PurchaseOptionConfigModel model = Mockito.mock(PurchaseOptionConfigModel.class);
        BDDMockito.given(model.getValidTo()).willReturn(yesterday);
        BDDMockito.given(model.getValidFrom()).willReturn(tomorrow);

        Assert.assertEquals(Boolean.FALSE, attrHandler.get(model));
    }

}
