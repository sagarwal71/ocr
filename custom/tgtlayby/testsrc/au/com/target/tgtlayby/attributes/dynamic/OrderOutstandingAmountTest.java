package au.com.target.tgtlayby.attributes.dynamic;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.order.strategies.impl.OrderOutstandingStrategyImpl;


/**
 * Unit test for {@link OrderOutstandingStrategyImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderOutstandingAmountTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @InjectMocks
    private final OrderOutstandingStrategyImpl orderOutstandingAmount = new OrderOutstandingStrategyImpl();

    @Mock
    private OrderModel orderModel;

    @Mock
    private PurchaseOptionConfigModel purchaseOptionConfigModel;

    @Mock
    private FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    @Mock
    private CommonI18NService commonI18NService;

    @Mock
    private CurrencyModel currencyModel;


    @Before
    public void setUp() {
        BDDMockito.given(currencyModel.getDigits()).willReturn(Integer.valueOf(2));
        BDDMockito.given(orderModel.getCurrency()).willReturn(currencyModel);
    }

    @Test
    public void testGetForNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        orderOutstandingAmount.calculateAmt(null);
    }

    @Test
    public void testGetForOrderWithNullPOConfig() {
        final double result = orderOutstandingAmount.calculateAmt(orderModel);
        Assert.assertEquals(Double.valueOf(0), Double.valueOf(result));

        Mockito.verifyZeroInteractions(findOrderTotalPaymentMadeStrategy);
    }

    @Test
    public void testGetForOrderWithPOConfigNotSupportingDues() {

        BDDMockito.given(orderModel.getPurchaseOptionConfig()).willReturn(purchaseOptionConfigModel);

        final double result = orderOutstandingAmount.calculateAmt(orderModel);
        Assert.assertEquals(Double.valueOf(0), Double.valueOf(result));

        Mockito.verifyZeroInteractions(findOrderTotalPaymentMadeStrategy);
    }

    @Test
    public void testGetForCancelledOrderWithPOConfigSupportingDues() {

        BDDMockito.given(orderModel.getStatus()).willReturn(OrderStatus.CANCELLED);
        BDDMockito.given(orderModel.getPurchaseOptionConfig()).willReturn(purchaseOptionConfigModel);
        BDDMockito.given(purchaseOptionConfigModel.getAllowPaymentDues()).willReturn(Boolean.TRUE);

        final double result = orderOutstandingAmount.calculateAmt(orderModel);
        Assert.assertEquals(Double.valueOf(0), Double.valueOf(result));

        Mockito.verifyZeroInteractions(findOrderTotalPaymentMadeStrategy);
    }

    @Test
    public void testGetForCompletedOrderWithPOConfigSupportingDues() {

        BDDMockito.given(orderModel.getStatus()).willReturn(OrderStatus.COMPLETED);
        BDDMockito.given(orderModel.getPurchaseOptionConfig()).willReturn(purchaseOptionConfigModel);
        BDDMockito.given(purchaseOptionConfigModel.getAllowPaymentDues()).willReturn(Boolean.TRUE);

        final double result = orderOutstandingAmount.calculateAmt(orderModel);
        Assert.assertEquals(Double.valueOf(0), Double.valueOf(result));

        Mockito.verifyZeroInteractions(findOrderTotalPaymentMadeStrategy);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGet() {

        final double orderTotal = 10;
        final double paidSoFar = 6;
        final double outstanding = 4;

        BDDMockito.given(orderModel.getTotalPrice()).willReturn(Double.valueOf(orderTotal));
        BDDMockito.given(orderModel.getStatus()).willReturn(OrderStatus.CREATED);
        BDDMockito.given(orderModel.getPurchaseOptionConfig()).willReturn(purchaseOptionConfigModel);

        BDDMockito.given(purchaseOptionConfigModel.getAllowPaymentDues()).willReturn(Boolean.TRUE);

        BDDMockito.given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(orderModel))
                .willReturn(Double.valueOf(paidSoFar));

        BDDMockito.given(commonI18NService.roundCurrency(orderTotal - paidSoFar, 2)).willReturn(outstanding);

        final Double result = orderOutstandingAmount.calculateAmt(orderModel);
        Assert.assertNotNull(result);
        Assert.assertEquals(Double.valueOf(outstanding), result);

        Mockito.verify(findOrderTotalPaymentMadeStrategy).getAmountPaidSoFar(orderModel);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetWithCentValues() {

        final double orderTotal = 10.54;
        final double paidSoFar = 6.07;
        final double outstanding = 4.47;

        BDDMockito.given(orderModel.getTotalPrice()).willReturn(Double.valueOf(orderTotal));
        BDDMockito.given(orderModel.getStatus()).willReturn(OrderStatus.CREATED);
        BDDMockito.given(orderModel.getPurchaseOptionConfig()).willReturn(purchaseOptionConfigModel);

        BDDMockito.given(purchaseOptionConfigModel.getAllowPaymentDues()).willReturn(Boolean.TRUE);

        BDDMockito.given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(orderModel))
                .willReturn(Double.valueOf(paidSoFar));

        BDDMockito.given(commonI18NService.roundCurrency(orderTotal - paidSoFar, 2)).willReturn(outstanding);

        final double result = orderOutstandingAmount.calculateAmt(orderModel);
        Assert.assertEquals(Double.valueOf(outstanding), Double.valueOf(result));

        Mockito.verify(findOrderTotalPaymentMadeStrategy).getAmountPaidSoFar(orderModel);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetForOverPaid() {

        final double orderTotal = 10;
        final double paidSoFar = 12;

        // If paid is greater than order total then we expect it to return 0 rather than a negative amount


        BDDMockito.given(orderModel.getTotalPrice()).willReturn(Double.valueOf(orderTotal));
        BDDMockito.given(orderModel.getStatus()).willReturn(OrderStatus.CREATED);
        BDDMockito.given(orderModel.getPurchaseOptionConfig()).willReturn(purchaseOptionConfigModel);

        BDDMockito.given(purchaseOptionConfigModel.getAllowPaymentDues()).willReturn(Boolean.TRUE);

        BDDMockito.given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(orderModel))
                .willReturn(Double.valueOf(paidSoFar));

        BDDMockito.given(commonI18NService.roundCurrency(orderTotal - paidSoFar, 2)).willReturn(-2d);

        final double result = orderOutstandingAmount.calculateAmt(orderModel);
        Assert.assertEquals(Double.valueOf(0d), Double.valueOf(result));

        Mockito.verify(findOrderTotalPaymentMadeStrategy).getAmountPaidSoFar(orderModel);
    }
}
