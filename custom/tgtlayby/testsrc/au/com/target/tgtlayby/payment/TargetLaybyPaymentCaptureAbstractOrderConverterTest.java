/**
 * 
 */
package au.com.target.tgtlayby.payment;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtpayment.dto.AmountType;
import au.com.target.tgtpayment.dto.LineItem;
import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.service.TargetPaymentAbstractOrderConverter;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetLaybyPaymentCaptureAbstractOrderConverterTest {

    @Mock
    private AbstractOrderModel orderModel;

    @Mock
    private Order order;

    @Mock
    private CurrencyModel currencyModel;

    @Mock
    private TargetPaymentAbstractOrderConverter targetPaymentAbstractOrderConverter;

    @Mock
    private LineItem item1;

    @Mock
    private AmountType at;

    @Mock
    private PurchaseOptionConfigModel poc;

    @InjectMocks
    @Spy
    private final TargetLaybyPaymentCaptureAbstractOrderConverterImpl converter = Mockito
            .spy(new TargetLaybyPaymentCaptureAbstractOrderConverterImpl());

    private List<LineItem> lineItems;

    @Before
    public void setup() {
        BDDMockito.given(orderModel.getPurchaseOptionConfig()).willReturn(poc);
        BDDMockito.given(orderModel.getCurrency()).willReturn(currencyModel);
        BDDMockito.given(currencyModel.getIsocode()).willReturn("AUD");

        BDDMockito.given(
                targetPaymentAbstractOrderConverter.convertAbstractOrderModelToOrder(orderModel,
                        BigDecimal.valueOf(10.0), PaymentCaptureType.PLACEORDER))
                .willReturn(order);
        lineItems = new ArrayList<>();
        lineItems.add(item1);
        BDDMockito.given(order.getLineItems()).willReturn(lineItems);
        BDDMockito.given(orderModel.getLayByFee()).willReturn(Double.valueOf(2.50));
        BDDMockito.given(at.getAmount()).willReturn(BigDecimal.valueOf(100.0));
        BDDMockito.given(order.getItemSubTotalAmount()).willReturn(at);
    }

    @Test
    public void testConvertAbstractOrderModelToOrderBuyNow() {
        BDDMockito.given(poc.getAllowPaymentDues()).willReturn(Boolean.FALSE);
        final Order result = converter.convertAbstractOrderModelToOrder(orderModel, BigDecimal.valueOf(10.0),
                PaymentCaptureType.PLACEORDER);
        Assert.assertNotNull(result);
    }

    @Test
    public void testPaymentCaptureTypeLaybyPayment() {
        final Order result = converter.convertAbstractOrderModelToOrder(orderModel, BigDecimal.valueOf(190.0),
                PaymentCaptureType.LAYBYPAYMENT);
        Assert.assertEquals(BigDecimal.valueOf(19000, 2), result.getItemSubTotalAmount().getAmount());
        Assert.assertEquals(BigDecimal.valueOf(19000, 2), result.getOrderTotalAmount().getAmount());
        Assert.assertEquals(BigDecimal.valueOf(0), result.getShippingTotalAmount().getAmount());
    }
}
