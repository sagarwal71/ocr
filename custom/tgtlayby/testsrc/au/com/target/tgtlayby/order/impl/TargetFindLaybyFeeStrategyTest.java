/**
 * 
 */
package au.com.target.tgtlayby.order.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.purchase.PurchaseOptionConfigService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFindLaybyFeeStrategyTest {

    @Mock
    private PurchaseOptionConfigService purchaseOptionConfigService;

    @Mock
    private AbstractOrderModel order;

    @Mock
    private PurchaseOptionModel po;

    @Mock
    private PurchaseOptionConfigModel poc;

    @InjectMocks
    private final TargetFindLaybyFeeStrategy strategy = new TargetFindLaybyFeeStrategy();

    @Test
    public void testFindLaybyFeeNoPurchaseOptionConfig() throws TargetUnknownIdentifierException {

        given(order.getPurchaseOptionConfig()).willReturn(null);

        final Double fee = strategy.getLaybyFee(order);
        Assert.assertNull(fee);
    }

    @Test
    public void testFindLaybyFee() throws TargetUnknownIdentifierException {

        given(order.getPurchaseOptionConfig()).willReturn(poc);
        given(poc.getPurchaseOption()).willReturn(po);
        given(poc.getServiceFee()).willReturn(new Double(9));

        final Double fee = strategy.getLaybyFee(order);
        Assert.assertNotNull(fee);
        Assert.assertEquals(fee, new Double(9));
    }


}
