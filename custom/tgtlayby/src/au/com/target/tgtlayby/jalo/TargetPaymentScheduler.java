package au.com.target.tgtlayby.jalo;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.ComposedType;


public class TargetPaymentScheduler extends GeneratedTargetPaymentScheduler
{
    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException
    {
        final Item item = super.createItem(ctx, type, allAttributes);
        return item;
    }

}
