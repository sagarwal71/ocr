package au.com.target.tgtlayby.cart.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtlayby.cart.dao.TargetCartDao;


/**
 * Default Implementation of {@link TargetCartDao}
 * 
 */
public class TargetCartDaoImpl extends DefaultGenericDao<CartModel> implements TargetCartDao {

    private static final Logger LOG = Logger.getLogger(TargetCartDaoImpl.class);

    private static final String QUERY_PREFIX_SELECT_FROM_CART = "SELECT { " + CartModel.PK + "} " +
            "FROM {" + CartModel._TYPECODE + "} ";

    private static final String QUERY_GET_ENTRIES = "SELECT {" + CartEntryModel.PK
            + "} FROM {" + CartEntryModel._TYPECODE + "} WHERE {" + CartEntryModel.ORDER + "} = ?cart";

    private static final String QUERY_GET_ENTRIES_BY_PRODUCT = QUERY_GET_ENTRIES
            + " AND {" + CartEntryModel.PRODUCT + "} = ?product";

    private static final String QUERY_GET_CARTS_FOR_USER = QUERY_PREFIX_SELECT_FROM_CART +
            "WHERE {" + CartModel.USER + "} = ?user " +
            "ORDER BY {" + CartModel.MODIFIEDTIME + "} DESC, {" + CartModel.DATE + "} DESC";

    public TargetCartDaoImpl() {
        super(CartModel._TYPECODE);
    }

    private List<CartEntryModel> searchEntries(final String query, final Map<String, Object> params) {
        final SearchResult<CartEntryModel> searchResult = getFlexibleSearchService().search(query, params);
        final List<CartEntryModel> result = searchResult.getResult();
        return result == null ? Collections.EMPTY_LIST : result;
    }

    @Override
    public List<CartEntryModel> findEntriesForProduct(final CartModel cartModel,
            final ProductModel productModel) {

        validateParameterNotNullStandardMessage("Cart", cartModel);
        validateParameterNotNullStandardMessage("Product", productModel);

        final Map params = new HashMap();
        params.put("cart", cartModel);
        params.put("product", productModel);
        return searchEntries(QUERY_GET_ENTRIES_BY_PRODUCT, params);
    }

    @Override
    public CartModel getMostRecentCartForUser(final CustomerModel customer)
    {
        validateParameterNotNullStandardMessage("Customer", customer);

        // There should only be one cart for a user, but just in case there is more than one then take the top
        // which will be the most recently modified
        final List<CartModel> cartList = getCartsForUserOrderByMostRecentlyModified(customer);
        if (CollectionUtils.isEmpty(cartList))
        {
            return null;
        }

        if (cartList.size() > 1)
        {
            LOG.warn("More than one master cart found for user: " + customer.getPk());
        }

        return cartList.get(0);
    }

    private List<CartModel> getCartsForUserOrderByMostRecentlyModified(final CustomerModel customer)
    {
        validateParameterNotNullStandardMessage("Customer", customer);

        final Map params = new HashMap();
        params.put("user", customer);
        return searchCarts(QUERY_GET_CARTS_FOR_USER, params);
    }

    private List<CartModel> searchCarts(final String query, final Map<String, Object> params) {
        final SearchResult<CartModel> searchResult = getFlexibleSearchService().search(query, params);
        final List<CartModel> result = searchResult.getResult();
        return result == null ? Collections.EMPTY_LIST : result;
    }

}
