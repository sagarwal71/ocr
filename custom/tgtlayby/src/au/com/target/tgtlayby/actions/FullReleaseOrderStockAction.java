/**
 * 
 */
package au.com.target.tgtlayby.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;


/**
 * Release all stock for the order
 * 
 */
public class FullReleaseOrderStockAction extends ReleaseOrderStockAction {

    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process, "OrderProcessModel must not be null");

        final OrderModel order = process.getOrder();

        Assert.notNull(order, "order is empty");

        if (!targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)) {
            releaseAllStockForOrder(order);
        }
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }
}