/**
 * 
 */
package au.com.target.tgtlayby.order.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.order.impl.TargetFindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtlayby.data.PaymentDueData;
import au.com.target.tgtlayby.model.PaymentDueConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.order.TargetLaybyPaymentDueService;


public class TargetLaybyPaymentDueServiceImpl implements TargetLaybyPaymentDueService {

    private CommonI18NService commonI18NService;
    private TargetFindOrderTotalPaymentMadeStrategy targetFindOrderTotalPaymentMadeStrategy;

    @Override
    public Set<PaymentDueData> getAllPaymentDuesForAbstractOrder(final AbstractOrderModel abstractOrder) {
        Assert.notNull(abstractOrder, "AbstractOrderModel must not be null");

        final Set<PaymentDueData> paymentDueDatas = new HashSet<>();
        final PurchaseOptionConfigModel poc = abstractOrder.getPurchaseOptionConfig();

        if (poc != null && BooleanUtils.isTrue(poc.getAllowPaymentDues())) {
            final Set<PaymentDueConfigModel> paymentDues = poc.getPaymentDueConfigs();
            if (CollectionUtils.isNotEmpty(paymentDues)) {
                for (final PaymentDueConfigModel paymentDue : paymentDues) {
                    if (paymentDue.equals(poc.getFinalPaymentDue())) {
                        continue;
                    }

                    paymentDueDatas.add(createPaymentDueDataFromPaymentDueConfig(paymentDue, abstractOrder));
                }
                return paymentDueDatas;
            }
        }
        return null;
    }

    @Override
    public PaymentDueData getPaymentDueDataForAbstractOrderAndPaymentDueConfig(final AbstractOrderModel abstractOrder,
            final PaymentDueConfigModel paymentDueConfig) {
        Assert.notNull(abstractOrder, "AbstractOrderModel must not be null");

        return createPaymentDueDataFromPaymentDueConfig(paymentDueConfig, abstractOrder);
    }

    @Override
    public PaymentDueData getInitialPaymentDue(final AbstractOrderModel abstractOrder) {
        Assert.notNull(abstractOrder, "AbstractOrderModel must not be null");

        final PurchaseOptionConfigModel poc = abstractOrder.getPurchaseOptionConfig();
        if (poc != null && BooleanUtils.isTrue(poc.getAllowPaymentDues())) {
            final PaymentDueConfigModel paymentDue = poc.getMinimumPaymentDue();
            if (paymentDue != null) {
                return createPaymentDueDataFromPaymentDueConfig(paymentDue, abstractOrder);
            }
        }
        return null;
    }

    @Override
    public Set<PaymentDueData> getIntermediatePaymentDues(final AbstractOrderModel abstractOrder) {
        Assert.notNull(abstractOrder, "AbstractOrderModel must not be null");

        final PurchaseOptionConfigModel poc = abstractOrder.getPurchaseOptionConfig();
        if (poc != null && BooleanUtils.isTrue(poc.getAllowPaymentDues())) {
            final PaymentDueConfigModel initialPayment = poc.getMinimumPaymentDue();
            final PaymentDueConfigModel finalPayment = poc.getFinalPaymentDue();
            final Set<PaymentDueConfigModel> paymentDues = poc.getPaymentDueConfigs();

            if (initialPayment != null && finalPayment != null && CollectionUtils.isNotEmpty(paymentDues)) {
                final Set<PaymentDueData> paymentDueDatas = new HashSet<>();
                paymentDues.remove(initialPayment);
                paymentDues.remove(finalPayment);
                for (final PaymentDueConfigModel paymentDue : paymentDues) {
                    paymentDueDatas.add(createPaymentDueDataFromPaymentDueConfig(paymentDue, abstractOrder));
                }
                return paymentDueDatas;
            }
        }
        return null;
    }

    @Override
    public PaymentDueData getFinalPaymentDue(final AbstractOrderModel abstractOrder) {
        Assert.notNull(abstractOrder, "AbstractOrderModel must not be null");

        final PurchaseOptionConfigModel poc = abstractOrder.getPurchaseOptionConfig();
        if (poc != null && BooleanUtils.isTrue(poc.getAllowPaymentDues())) {
            final PaymentDueConfigModel paymentDue = poc.getFinalPaymentDue();
            if (paymentDue != null) {
                return createPaymentDueDataFromPaymentDueConfig(paymentDue, abstractOrder);
            }
        }
        return null;
    }

    @Override
    public PaymentDueData getNextPaymentDue(final OrderModel order) {
        Assert.notNull(order, "Order model must not be null");

        final PurchaseOptionConfigModel poc = order.getPurchaseOptionConfig();
        if (poc != null && BooleanUtils.isTrue(poc.getAllowPaymentDues())) {
            final Set<PaymentDueConfigModel> paymentDues = poc.getPaymentDueConfigs();
            if (CollectionUtils.isNotEmpty(paymentDues)) {
                PaymentDueConfigModel nextPayment = poc.getFinalPaymentDue();
                final double paidSoFar = targetFindOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(order)
                        .doubleValue();

                for (final PaymentDueConfigModel paymentDue : paymentDues) {
                    final double dueAmount = calculateAmountFromPercentValue(order, paymentDue.getDuePercentage()
                            .doubleValue());
                    if (paidSoFar < dueAmount
                            && paymentDue.getDuePercentage().doubleValue() < nextPayment.getDuePercentage()
                                    .doubleValue()) {
                        nextPayment = paymentDue;
                    }
                }
                return createPaymentDueDataFromPaymentDueConfig(nextPayment, order);
            }
        }
        return null;
    }

    protected PaymentDueData createPaymentDueDataFromPaymentDueConfig(final PaymentDueConfigModel paymentDue,
            final AbstractOrderModel abstractOrder) {
        Assert.notNull(abstractOrder, "AbstractOrderModel must not be null");
        Assert.notNull(paymentDue, "payment due must not be null");

        final PaymentDueData paymentDueData = new PaymentDueData();
        final double duePercentage = paymentDue.getDuePercentage().doubleValue();
        paymentDueData.setPercentage(duePercentage);

        if (paymentDue.getDueDays() != null) {
            final Date dueDate = DateUtils.addDays(abstractOrder.getDate(), paymentDue.getDueDays().intValue());
            final Date roundedDate = DateUtils.round(dueDate, Calendar.DATE);
            paymentDueData.setDueDate(roundedDate);
        }
        else {
            paymentDueData.setDueDate(paymentDue.getDueDate());
        }

        paymentDueData.setAmount(calculateAmountFromPercentValue(abstractOrder, duePercentage));
        return paymentDueData;
    }

    private double calculateAmountFromPercentValue(final AbstractOrderModel abstractOrder, final double duePercentage) {
        final double percentValue = duePercentage / 100;
        final int digits = abstractOrder.getCurrency().getDigits().intValue();
        final double duePercentageAmount = roundValue(abstractOrder.getTotalPrice().doubleValue(),
                digits) * percentValue;
        final double roundedPercentageAmount = roundValue(duePercentageAmount, digits);
        return roundedPercentageAmount;
    }

    private double roundValue(final double amount, final int digits) {
        return commonI18NService.roundCurrency(amount, digits);
    }

    /**
     * 
     * @param commonI18NService
     *            the commonI18NService to set
     */
    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    /**
     * @param targetFindOrderTotalPaymentMadeStrategy
     *            the targetFindOrderTotalPaymentMadeStrategy to set
     */
    @Required
    public void setTargetFindOrderTotalPaymentMadeStrategy(
            final TargetFindOrderTotalPaymentMadeStrategy targetFindOrderTotalPaymentMadeStrategy) {
        this.targetFindOrderTotalPaymentMadeStrategy = targetFindOrderTotalPaymentMadeStrategy;
    }
}
