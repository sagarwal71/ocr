/**
 * 
 */
package au.com.target.tgtlayby.order.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.JaloObjectNoLongerValidException;
import de.hybris.platform.servicelayer.session.SessionService;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.order.impl.TargetCommerceCheckoutServiceImpl;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtlayby.constants.TgtlaybyConstants;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.order.TargetLaybyCommerceCheckoutService;


public class TargetLaybyCommerceCheckoutServiceImpl extends TargetCommerceCheckoutServiceImpl implements
        TargetLaybyCommerceCheckoutService {


    private static final Logger LOG = Logger.getLogger(TargetLaybyCommerceCheckoutServiceImpl.class);

    private SessionService sessionService;
    private TargetLaybyCartService targetLaybyCartService;
    private TargetCommerceCartService targetCommerceCartService;

    @Override
    public boolean fillCart4ClickAndCollect(final CartModel cartModel, final int cncStoreNumber, final String title,
            final String firstName,
            final String lastName,
            final String phoneNumber,
            final DeliveryModeModel deliveryMode) {
        final boolean result = super.fillCart4ClickAndCollect(cartModel, cncStoreNumber, title, firstName, lastName,
                phoneNumber,
                deliveryMode);
        getModelService().save(cartModel);
        return result;
    }

    @Override
    public boolean setTMDCardNumber(final CartModel checkoutCartModel, final String tmdCardNumber) {
        Assert.notNull(checkoutCartModel, "checkout cart model must not be null");

        boolean applied = super.setTMDCardNumber(checkoutCartModel, tmdCardNumber);

        // once applied on checkout cart, apply it on master cart
        if (applied) {
            final CartModel masterCart = targetLaybyCartService.getSessionCart();
            if (masterCart != checkoutCartModel) {
                applied = setTMDCardNumberOnMasterCart(masterCart, tmdCardNumber);
            }
        }

        return applied;
    }

    private boolean setTMDCardNumberOnMasterCart(final CartModel masterCart, final String tmdCardNumber) {
        masterCart.setTmdCardNumber(tmdCardNumber);
        getModelService().save(masterCart);
        final CommerceCartParameter cartParameter = new CommerceCartParameter();
        cartParameter.setCart(masterCart);
        return targetCommerceCartService.calculateCart(cartParameter);
    }

    @Override
    public List<CommerceCartModification> performSOHOnCart(final CartModel cart)
            throws CommerceCartModificationException {
        Assert.notNull(cart, "cart cannot be null");

        final List<AbstractOrderEntryModel> abstractOrderEntries = cart.getEntries();
        final List<CommerceCartModification> cartModifications = targetCommerceCartService.performSOHOnEntries(
                cart, abstractOrderEntries);
        return cartModifications;
    }

    @Override
    public boolean hasCheckoutCart(final PurchaseOptionModel purchaseOption) {
        return hasCheckoutCart(TgtlaybyConstants.SESSION_CHECKOUT_CART + purchaseOption.getCode());
    }

    private boolean hasCheckoutCart(final String sessionParameterName) {
        try {
            return sessionService.getAttribute(sessionParameterName) != null;
        }
        catch (final JaloObjectNoLongerValidException ex) {
            if (LOG.isInfoEnabled()) {
                LOG.info("Session Cart no longer valid. Removing from session. hasSessionCart will return false. "
                        + ex.getMessage());
            }
            sessionService.removeAttribute(sessionParameterName);
            return false;
        }
    }


    @Override
    public Map<String, CartModel> getCheckoutCarts(final CartModel masterCart) {
        final Map<String, CartModel> cartModels = new HashMap<String, CartModel>();
        cartModels.put(masterCart.getPurchaseOption().getCode(), masterCart);
        return cartModels;
    }

    @Override
    public void changeCurrentCustomerOnAllCheckoutCarts(final CartModel masterCart, final UserModel customer) {

        final Map<String, CartModel> mapCarts = getCheckoutCarts(masterCart);
        for (final CartModel cart : mapCarts.values()) {
            cart.setUser(customer);
            getModelService().save(cart);
        }

    }

    /* 
     * Override the default service to use logic from TargetCommerceCartService,<br/>
     * to work out the supported delivery modes for a given cart.
     */
    @Override
    public void validateDeliveryMode(final CommerceCheckoutParameter checkoutParameter)
    {
        validateParameterNotNull(checkoutParameter, "Checkout parameter cannot be null");
        final CartModel cartModel = checkoutParameter.getCart();
        validateParameterNotNull(cartModel, "Cart model cannot be null");

        final DeliveryModeModel currentDeliveryMode = cartModel.getDeliveryMode();
        if (currentDeliveryMode != null)
        {
            if (!targetCommerceCartService.isDeliveryModeApplicableForCart(currentDeliveryMode, cartModel)) {
                cartModel.setDeliveryMode(null);
                cartModel.setZoneDeliveryModeValue(null);
                cartModel.setDeliveryAddress(null);
                getModelService().save(cartModel);
                getModelService().refresh(cartModel);

                // Calculate cart after removing delivery mode
                final CommerceCartParameter cartParameter = new CommerceCartParameter();
                cartParameter.setCart(cartModel);
                targetCommerceCartService.calculateCart(cartParameter);
            }
        }
    }

    @Override
    public BigDecimal getAmountForPaymentCapture(final CartModel cartModel) {

        // This could be a layby or a buy now - check the purchase order config
        final boolean useCurrent = (cartModel.getPurchaseOptionConfig().getAllowPaymentDues() != null
                && cartModel.getPurchaseOptionConfig().getAllowPaymentDues().booleanValue());

        if (!useCurrent) {
            return super.getAmountForPaymentCapture(cartModel);
        }

        final Double amount = cartModel.getAmountCurrentPayment();
        if (amount != null) {
            return BigDecimal.valueOf(amount.doubleValue());
        }

        return null;
    }

    @Required
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @Required
    public void setTargetLaybyCartService(final TargetLaybyCartService targetLaybyCartService) {
        this.targetLaybyCartService = targetLaybyCartService;
    }

    @Required
    public void setTargetCommerceCartService(final TargetCommerceCartService targetCommerceCartService) {
        this.targetCommerceCartService = targetCommerceCartService;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.order.TargetCommerceCheckoutService#setAmountCurrentPayment(de.hybris.platform.core.model.order.CartModel, java.lang.Double)
     */
    @Override
    public boolean setAmountCurrentPayment(final CartModel cartModel, final Double amountCurrentPayment) {
        Assert.notNull(cartModel, "cartModel must not be null");
        cartModel.setAmountCurrentPayment(amountCurrentPayment);
        getModelService().save(cartModel);
        final CommerceCartParameter cartParameter = new CommerceCartParameter();
        cartParameter.setCart(cartModel);
        return targetCommerceCartService.calculateCart(cartParameter);
    }
}
