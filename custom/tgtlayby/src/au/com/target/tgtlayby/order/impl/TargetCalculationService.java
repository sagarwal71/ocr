/**
 * 
 */
package au.com.target.tgtlayby.order.impl;

//CHECKSTYLE:OFF TargetCart and TargetOrder are used for links in javadoc comments only
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.impl.DefaultCalculationService;
import de.hybris.platform.order.strategies.calculation.FindDeliveryCostStrategy;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.delivery.dto.DeliveryCostDto;
import au.com.target.tgtcore.order.TargetFindDeliveryCostStrategy;
import au.com.target.tgtcore.order.impl.TargetFindFeeTaxValueStrategy;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import au.com.target.tgtlayby.jalo.TargetCart;
import au.com.target.tgtlayby.jalo.TargetOrder;


//CHECKSTYLE:ON

/**
 * Extend the DefaultCalculationService to calculate the tax on fees If any changes are made to this service it must be
 * maintained in targetorder and targetcart
 */
public class TargetCalculationService extends DefaultCalculationService {

    // Strategy for finding the default tax value to apply to fees
    private TargetFindFeeTaxValueStrategy targetFindFeeTaxValueStrategy;

    // Strategy for getting the layby fee for the order
    private TargetFindLaybyFeeStrategy targetFindLaybyFeeStrategy;

    // The member in base is private
    private CommonI18NService commonI18NService;

    private TargetFindDeliveryCostStrategy targetFindDeliveryCostStrategy;

    private TargetSalesApplicationConfigService salesApplicationConfigService;


    /* 
     * Override the default to set the delivery value and layby fee in the order
     */
    @Override
    protected void resetAdditionalCosts(final AbstractOrderModel order, final Collection<TaxValue> relativeTaxValues) {
        if (order instanceof CartModel) {
            targetFindDeliveryCostStrategy.resetDeliveryValue((CartModel)order);
        }

        super.resetAdditionalCosts(order, relativeTaxValues);

        final Double laybyFee = targetFindLaybyFeeStrategy.getLaybyFee(order);
        order.setLayByFee(laybyFee);
    }

    /**
     * Override the default to add fee tax values to the map, add layby fee to total and set taxAdjustmentFactor=1<br />
     * Please note: These same operations are performed in {@link TargetOrder} and {@link TargetCart}, in the
     * calculateTotals method.
     */
    @Override
    protected void calculateTotals(final AbstractOrderModel order, final boolean recalculate,
            final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap) throws CalculationException {

        if (recalculate || requiresCalculation(order)) {

            final CurrencyModel curr = order.getCurrency();
            final int digits = curr.getDigits().intValue();

            // subtotal
            final double subtotal = order.getSubtotal().doubleValue();

            // discounts
            final double totalDiscounts = calculateDiscountValues(order, recalculate);
            final double roundedTotalDiscounts = commonI18NService.roundCurrency(totalDiscounts, digits);
            order.setTotalDiscounts(Double.valueOf(roundedTotalDiscounts));

            // Make sure the delivery cost get calculated after the discount, 
            // because the delivery cost calculation depends on the discount
            final DeliveryCostDto deliveryCostDto = targetFindDeliveryCostStrategy.getDeliveryCostDto(order);
            order.setDeliveryCost(Double.valueOf(deliveryCostDto.getDeliveryCost()));
            targetFindDeliveryCostStrategy.resetShipsterAttibutes(order, deliveryCostDto);


            // set total
            double total = subtotal + order.getPaymentCost().doubleValue() + order.getDeliveryCost().doubleValue()
                    - roundedTotalDiscounts;

            // Add layby fee
            if (order.getLayByFee() != null) {
                total += order.getLayByFee().doubleValue();
            }

            final double totalRounded = commonI18NService.roundCurrency(total, digits);
            order.setTotalPrice(Double.valueOf(totalRounded));

            // taxes 
            addFeeTaxValues(order, taxValueMap);
            calculateAndSetTotalTax(order, recalculate, taxValueMap, digits);
            getModelService().save(order);
            setCalculatedStatus(order);
        }
    }

    /**
     * Method to calculate tax based on sales application config and set it in order
     * 
     * @param order
     * @param recalculate
     * @param taxValueMap
     * @param digits
     */
    private void calculateAndSetTotalTax(final AbstractOrderModel order, final boolean recalculate,
            final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap, final int digits) {
        if (order instanceof OrderModel && salesApplicationConfigService
                .isSuppressTaxCalculation(((OrderModel)order).getSalesApplication())) {
            order.setTotalTax(Double.valueOf(0.00));
        }
        else {
            final double totalTaxes = calculateTotalTaxValues(order, recalculate, digits, 1, taxValueMap);
            final double totalRoundedTaxes = commonI18NService.roundCurrency(totalTaxes, digits);
            order.setTotalTax(Double.valueOf(totalRoundedTaxes));
        }
    }

    /**
     * Add fee tax values to the map for use in calculateTotals.<br />
     * Please note: These same operations are performed in {@link TargetOrder} and {@link TargetCart}, in the
     * addFeeTaxValues method.
     */
    protected void addFeeTaxValues(final AbstractOrderModel order,
            final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap) {
        if (order.getDeliveryCost() != null) {
            addFeeTaxValue(order.getDeliveryCost().doubleValue(), taxValueMap);
        }
        if (order.getLayByFee() != null) {
            addFeeTaxValue(order.getLayByFee().doubleValue(), taxValueMap);
        }

        final List<DiscountValue> globalDiscountValues = order.getGlobalDiscountValues();
        if (CollectionUtils.isNotEmpty(globalDiscountValues)) {
            for (final DiscountValue discount : globalDiscountValues) {
                addFeeTaxValue(-discount.getAppliedValue(), taxValueMap); // Negate the value because the applied value is positive, but it's actually a discount
            }
        }
    }

    /**
     * Calculate taxes - set taxAdjustmentFactor 1 since the tax for fees is added via extra values that are added to
     * the map in resetAllValues
     * 
     * @param order
     * @param recalculate
     * @param digits
     * @param taxValueMap
     * @return total tax
     */
    protected double calculateTotalTaxValues(final AbstractOrderModel order, final boolean recalculate,
            final int digits,
            final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap) {
        return calculateTotalTaxValues(order, recalculate, digits, 1, taxValueMap);
    }


    protected void addFeeTaxValue(final double fee, final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap) {
        // Get the right tax to use for fees
        final TaxValue defaultTaxValue = targetFindFeeTaxValueStrategy.getTaxValueForFee();
        defaultTaxValue.unapply();

        // We need to create a key out of a set containing the TaxValue
        final Set<TaxValue> relativeTaxGroupKey = Collections.singleton(defaultTaxValue);

        addRelativeEntryTaxValue(fee, defaultTaxValue, relativeTaxGroupKey, taxValueMap);
    }

    @Override
    protected void setCalculatedStatus(final AbstractOrderModel order) {
        order.setCalculated(Boolean.TRUE);
        getModelService().save(order);
        final List<AbstractOrderEntryModel> entries = order.getEntries();
        if (entries != null) {
            for (final AbstractOrderEntryModel entry : entries) {
                entry.setCalculated(Boolean.TRUE);
            }
            getModelService().saveAll(entries);
        }
    }

    @Override
    protected Collection<TaxValue> findTaxValues(final AbstractOrderEntryModel entry) throws CalculationException {
        Collection<TaxValue> taxValues = super.findTaxValues(entry);
        if (CollectionUtils.isEmpty(taxValues)) {
            // could not find any tax values, so use the current.  (happens during cancellation)
            taxValues = entry.getTaxValues();
        }
        return taxValues;
    }

    @Override
    protected PriceValue findBasePrice(final AbstractOrderEntryModel entry) throws CalculationException {
        //if the preserve item value is true then skip the DB call to get the base price of an Item
        if (BooleanUtils.isTrue(entry.getOrder().getPreserveItemValue())) {
            return new PriceValue(entry.getOrder().getCurrency().getIsocode(), entry.getBasePrice().doubleValue(),
                    BooleanUtils.isTrue(entry.getOrder().getNet()));

        }
        else {
            return super.findBasePrice(entry);
        }
    }

    /**
     * 
     * @param targetFindFeeTaxValueStrategy
     *            the targetFindFeeTaxValueStrategy to set
     */
    @Required
    public void setTargetFindFeeTaxValueStrategy(final TargetFindFeeTaxValueStrategy targetFindFeeTaxValueStrategy) {
        this.targetFindFeeTaxValueStrategy = targetFindFeeTaxValueStrategy;
    }

    /**
     * @param targetFindLaybyFeeStrategy
     *            the targetFindLaybyFeeStrategy to set
     */
    @Required
    public void setTargetFindLaybyFeeStrategy(final TargetFindLaybyFeeStrategy targetFindLaybyFeeStrategy) {
        this.targetFindLaybyFeeStrategy = targetFindLaybyFeeStrategy;
    }


    @Override
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        super.setCommonI18NService(commonI18NService);
        this.commonI18NService = commonI18NService;
    }


    @Override
    public void setFindDeliveryCostStrategy(final FindDeliveryCostStrategy findDeliveryCostStrategy) {
        Assert.isInstanceOf(TargetFindDeliveryCostStrategy.class, findDeliveryCostStrategy);
        super.setFindDeliveryCostStrategy(findDeliveryCostStrategy);
        targetFindDeliveryCostStrategy = (TargetFindDeliveryCostStrategy)findDeliveryCostStrategy;
    }

    /**
     * @param salesApplicationConfigService
     *            the salesApplicationConfigService to set
     */
    @Required
    public void setSalesApplicationConfigService(
            final TargetSalesApplicationConfigService salesApplicationConfigService) {
        this.salesApplicationConfigService = salesApplicationConfigService;
    }

}
