/**
 * 
 */
package au.com.target.tgtlayby.invoice;

import de.hybris.platform.core.model.order.OrderModel;

import org.apache.velocity.VelocityContext;

import au.com.target.tgtcore.formatter.impl.InvoiceOrderFormatterImpl;
import au.com.target.tgtcore.taxinvoice.data.TaxInvoiceItems;
import au.com.target.tgtutility.format.PriceFormatter;


/**
 * @author dcwillia
 * 
 */
public class LaybyInvoiceOrderFormatterImpl extends InvoiceOrderFormatterImpl {
    @Override
    protected void addTotals(final VelocityContext vContext, final OrderModel order, final TaxInvoiceItems lineItems) {
        super.addTotals(vContext, order, lineItems);
        final Double laybyFee = order.getLayByFee();
        vContext.put("laybyFee", PriceFormatter.twoDecimalFormat(laybyFee == null ? 0.0 : laybyFee.doubleValue()));
    }
}
