/**
 * 
 */
package au.com.target.tgtfluent.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfluent.enums.FluentBatchStatus;
import au.com.target.tgtfluent.model.AbstractFluentBatchResponseModel;
import au.com.target.tgtfluent.model.LocationFluentBatchResponseModel;
import au.com.target.tgtfluent.service.FluentBatchResponseService;
import au.com.target.tgtfluent.service.FluentFeedService;


/**
 * @author mgazal
 *
 */
public class LocationFeedStatusCheckJob extends AbstractJobPerformable<CronJobModel> {

    private FluentBatchResponseService fluentBatchResponseService;

    private FluentFeedService fluentFeedService;

    @Override
    public PerformResult perform(final CronJobModel arg0) {
        final List<AbstractFluentBatchResponseModel> batchResponses = fluentBatchResponseService.find(
                LocationFluentBatchResponseModel._TYPECODE,
                Arrays.asList(FluentBatchStatus.PENDING, FluentBatchStatus.RUNNING));
        for (final AbstractFluentBatchResponseModel batchResponse : batchResponses) {
            fluentFeedService.checkBatchStatus(batchResponse);
        }
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    /**
     * @param fluentBatchResponseService
     *            the fluentBatchResponseService to set
     */
    @Required
    public void setFluentBatchResponseService(final FluentBatchResponseService fluentBatchResponseService) {
        this.fluentBatchResponseService = fluentBatchResponseService;
    }

    /**
     * @param fluentFeedService
     *            the fluentFeedService to set
     */
    @Required
    public void setFluentFeedService(final FluentFeedService fluentFeedService) {
        this.fluentFeedService = fluentFeedService;
    }
}
