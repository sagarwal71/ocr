/**
 * 
 */
package au.com.target.tgtfluent.converter.populator;

import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;

import au.com.target.tgtfluent.data.FulfilmentItem;


/**
 * @author pvarghe2
 *
 */
public class FluentWavedConsignmentPopulator extends FluentConsignmentPopulator {

    @Override
    protected FulfilmentItem createItem(final ConsignmentEntryModel entry) {
        final FulfilmentItem fulfilmentItem = new FulfilmentItem();
        fulfilmentItem.setOrderItemRef(entry.getOrderEntry().getProduct().getCode());
        final int requestedQty = entry.getQuantity().intValue();
        fulfilmentItem.setRequestedQty(Integer.valueOf(requestedQty));
        return fulfilmentItem;
    }

}
