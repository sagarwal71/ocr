/**
 * 
 */
package au.com.target.tgtfluent.converter.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.StringUtils;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.model.GiftRecipientModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.util.ProductUtil;
import au.com.target.tgtfluent.data.Address;
import au.com.target.tgtfluent.data.Attribute;
import au.com.target.tgtfluent.data.AttributeType;
import au.com.target.tgtfluent.data.Customer;
import au.com.target.tgtfluent.data.FulfilmentChoice;
import au.com.target.tgtfluent.data.FulfilmentChoiceDeliveryType;
import au.com.target.tgtfluent.data.FulfilmentChoiceType;
import au.com.target.tgtfluent.data.GiftCardRecipient;
import au.com.target.tgtfluent.data.Order;
import au.com.target.tgtfluent.data.OrderItem;
import au.com.target.tgtfluent.data.OrderType;
import au.com.target.tgtfluent.data.Recipient;


/**
 * @author bpottass
 *
 */
public class OrderPopulator implements Populator<OrderModel, Order> {

    private static final String EXPRESS_DELIVERY_CODE = "express-delivery";

    private static final String ATTRIBUTE_NAME_SALES_APPLICATION = "SALES_APPLICATION";

    private static final String ATTRIBUTE_NAME_SHIP_TO_PHONE = "SHIP_TO_PHONE";

    private static final String ATTRIBUTE_NAME_GIFTCARD_RECIPIENT = "GIFTCARD_RECIPIENT";

    private static final String ATTRIBUTE_NAME_PREORDER = "PREORDER";

    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Override
    public void populate(final OrderModel orderModel, final Order orderRequest) throws ConversionException {
        orderRequest.setOrderRef(orderModel.getCode());
        orderRequest.setType(createOrderType(orderModel));
        orderRequest.setFulfilmentChoice(createFulFilmentChoice(orderModel));
        orderRequest.setItems(createOrderItems(orderModel));
        orderRequest.setTotalPrice(orderModel.getTotalPrice());
        orderRequest.setCustomer(createCustomer(orderModel));
        orderRequest.setAttributes(createCustomAttributes(orderModel));
    }

    /**
     * @return orderType - if click and collect order CC, HD otherwise
     */
    private OrderType createOrderType(final OrderModel orderModel) {
        if (targetDeliveryModeHelper.isDeliveryModeStoreDelivery(orderModel.getDeliveryMode())) {
            return OrderType.CC;
        }
        else {
            return OrderType.HD;
        }
    }

    /**
     * 
     * @param orderModel
     * @return fulfilmentChoice
     */
    private FulfilmentChoice createFulFilmentChoice(final OrderModel orderModel) {
        final FulfilmentChoice fulfilmentChoice = new FulfilmentChoice();
        fulfilmentChoice.setFulfilmentType(getFulfilmentType(orderModel));
        fulfilmentChoice.setCurrency(getCurrencyCode(orderModel));
        fulfilmentChoice.setAddress(createDeliveryAddress(orderModel));
        fulfilmentChoice.setFulfilmentPrice(orderModel.getDeliveryCost());
        fulfilmentChoice.setDeliveryType(getDeliveryType(orderModel));
        return fulfilmentChoice;
    }


    /**
     * @param orderModel
     * @return list of orderItem
     */
    private List<OrderItem> createOrderItems(final OrderModel orderModel) {
        final List<OrderItem> orderItems = new ArrayList();
        final String currencyCode = getCurrencyCode(orderModel);
        for (final AbstractOrderEntryModel orderEntry : orderModel.getEntries()) {
            orderItems.add(createOrderItem(currencyCode, orderEntry));
        }
        return orderItems;
    }

    /**
     * @param orderModel
     * @return customer
     */
    private Customer createCustomer(final OrderModel orderModel) {
        final Customer customer = new Customer();
        if (orderModel.getUser() instanceof TargetCustomerModel) {
            final TargetCustomerModel targetCustomer = (TargetCustomerModel)orderModel.getUser();
            customer.setCustomerRef(targetCustomer.getCustomerID());
            customer.setFirstName(targetCustomer.getFirstname());
            customer.setLastName(targetCustomer.getLastname());
            customer.setEmail(targetCustomer.getContactEmail());
            customer.setMobile(targetCustomer.getMobileNumber());
        }
        return customer;
    }

    /**
     * @param orderModel
     * @return custom attribute - sales application name and delivery address mobile number
     */
    private List<Attribute> createCustomAttributes(final OrderModel orderModel) {
        final List<Attribute> attributes = new ArrayList();
        attributes.add(createAttribute(ATTRIBUTE_NAME_SALES_APPLICATION, getSalesApplication(orderModel)));
        attributes.add(createAttribute(ATTRIBUTE_NAME_SHIP_TO_PHONE, getPhoneNumber(orderModel.getDeliveryAddress())));
        attributes.add(createAttribute(ATTRIBUTE_NAME_PREORDER, isPreOrder(orderModel).toString()));

        final List<GiftCardRecipient> giftCardRecipients = createGiftCardRecipients(orderModel);
        if (CollectionUtils.isNotEmpty(giftCardRecipients)) {
            attributes.add(createAttribute(ATTRIBUTE_NAME_GIFTCARD_RECIPIENT, giftCardRecipients,
                    AttributeType.JSON));
        }
        return attributes;
    }

    /**
     * @param orderModel
     * @return true if order contains preorder items else false
     */
    private Boolean isPreOrder(final OrderModel orderModel) {
        return orderModel.getContainsPreOrderItems() != null ? orderModel.getContainsPreOrderItems() : Boolean.FALSE;
    }

    /**
     * 
     * @param orderModel
     * @return list of giftCardRecipients
     */
    private List<GiftCardRecipient> createGiftCardRecipients(final OrderModel orderModel) {
        final List<GiftCardRecipient> giftCardRecipients = new ArrayList();
        for (final AbstractOrderEntryModel orderEntry : orderModel.getEntries()) {
            if (ProductUtil.isProductTypeDigital(orderEntry.getProduct())) {
                giftCardRecipients.add(createGiftCardRecipient(orderEntry));
            }
        }
        return giftCardRecipients;
    }

    /**
     * @param orderEntry
     * @return giftCardRecipient
     */
    private GiftCardRecipient createGiftCardRecipient(final AbstractOrderEntryModel orderEntry) {
        final GiftCardRecipient giftCardRecipient = new GiftCardRecipient();
        giftCardRecipient.setSkuRef(getSku(orderEntry));
        final List<Recipient> recipients = new ArrayList();
        for (final GiftRecipientModel giftCardRecipientModel : orderEntry.getGiftRecipients()) {
            recipients.add(createRecipient(giftCardRecipientModel));
        }
        giftCardRecipient.setRecipients(recipients);
        return giftCardRecipient;
    }

    /**
     * @param giftCardRecipientModel
     * @return recipient
     */
    private Recipient createRecipient(final GiftRecipientModel giftCardRecipientModel) {
        final Recipient recipient = new Recipient();
        recipient.setFirstName(giftCardRecipientModel.getFirstName());
        recipient.setLastName(giftCardRecipientModel.getLastName());
        recipient.setEmail(giftCardRecipientModel.getEmail());
        recipient.setMessage(giftCardRecipientModel.getMessageText());
        return recipient;
    }

    /**
     * @param deliveryAddress
     * @return phone number as string
     */
    private String getPhoneNumber(final AddressModel deliveryAddress) {
        if (deliveryAddress == null) {
            return null;
        }
        return deliveryAddress.getPhone1() != null ? deliveryAddress.getPhone1() : deliveryAddress.getCellphone();

    }

    /**
     * 
     * @param name
     * @param value
     * @return attribute
     */
    private Attribute createAttribute(final String name, final Object value) {
        final Attribute attribute = new Attribute();
        attribute.setName(name);
        attribute.setType(AttributeType.STRING);
        attribute.setValue(value);
        return attribute;
    }

    /**
     * 
     * @param name
     * @param value
     * @param type
     * @return attribute
     */
    private Attribute createAttribute(final String name, final Object value, final AttributeType type) {
        final Attribute attribute = new Attribute();
        attribute.setName(name);
        attribute.setType(type);
        attribute.setValue(value);
        return attribute;
    }

    /**
     * @param orderModel
     * @return salesApplication code
     */
    private String getSalesApplication(final OrderModel orderModel) {
        return orderModel.getSalesApplication() != null ? orderModel.getSalesApplication().getCode() : null;
    }

    /**
     * @param orderModel
     * @return FulfilmentChoiceType.CC_PFS if order delivery type is click and collect, FulfilmentChoiceType.HD_PFS
     *         otherwise
     */
    private FulfilmentChoiceType getFulfilmentType(final OrderModel orderModel) {
        if (targetDeliveryModeHelper.isDeliveryModeStoreDelivery(orderModel.getDeliveryMode())) {
            return FulfilmentChoiceType.CC_PFS;
        }
        else {
            return FulfilmentChoiceType.HD_PFS;
        }
    }

    /**
     * @param orderModel
     * @return isoCode
     */
    private String getCurrencyCode(final OrderModel orderModel) {
        return orderModel.getCurrency() != null ? orderModel.getCurrency().getIsocode() : "AUD";
    }

    /**
     * @param orderModel
     * @return delivery address
     */
    private Address createDeliveryAddress(final OrderModel orderModel) {

        final AddressModel addressModel = orderModel.getDeliveryAddress();
        if (addressModel == null) {
            return null;
        }
        final Address deliveryAddress = new Address();
        final String name = addressModel.getFirstname() + " " + addressModel.getLastname();
        deliveryAddress.setName(name);
        if (targetDeliveryModeHelper.isDeliveryModeStoreDelivery(orderModel.getDeliveryMode())) {
            deliveryAddress.setLocationRef(orderModel.getCncStoreNumber().toString());
            return deliveryAddress;
        }
        if (StringUtils.isEmpty(addressModel.getStreetnumber())) {
            deliveryAddress.setStreet(addressModel.getStreetname());
        }
        else {
            deliveryAddress.setCompanyName(addressModel.getStreetname());
            deliveryAddress.setStreet(addressModel.getStreetnumber());
        }
        deliveryAddress.setPostcode(addressModel.getPostalcode());
        deliveryAddress.setCity(addressModel.getTown());
        deliveryAddress.setState(addressModel.getDistrict());
        deliveryAddress
                .setCountry(addressModel.getCountry() != null ? addressModel.getCountry().getIsocode() : null);
        return deliveryAddress;
    }

    /**
     * @param orderModel
     * @return FulfilmentChoiceDeliveryType.EXPRESS if order delivery type is Express,
     *         FulfilmentChoiceDeliveryType.STANDARD otherwise
     */
    private FulfilmentChoiceDeliveryType getDeliveryType(final OrderModel orderModel) {
        if (isExpressDelivery(orderModel.getDeliveryMode())) {
            return FulfilmentChoiceDeliveryType.EXPRESS;
        }
        else {
            return FulfilmentChoiceDeliveryType.STANDARD;
        }
    }

    /**
     * @param currencyCode
     * @param orderEntry
     * @return orderItem
     */
    private OrderItem createOrderItem(final String currencyCode, final AbstractOrderEntryModel orderEntry) {
        final OrderItem orderItem = new OrderItem();
        orderItem.setSkuRef(getSku(orderEntry));
        orderItem.setRequestedQty(Integer.valueOf(orderEntry.getQuantity().intValue()));
        orderItem.setSkuPrice(orderEntry.getBasePrice());
        orderItem.setTotalPrice(orderEntry.getTotalPrice());
        orderItem.setCurrency(currencyCode);
        return orderItem;
    }

    /**
     * @param orderEntry
     * @return sku
     */
    private String getSku(final AbstractOrderEntryModel orderEntry) {
        return orderEntry.getProduct().getCode();
    }

    /**
     * @param deliveryModeModel
     * @return true if order is express delivery
     */
    private boolean isExpressDelivery(final DeliveryModeModel deliveryModeModel) {
        return EXPRESS_DELIVERY_CODE.equals(deliveryModeModel.getCode());
    }

    /**
     * @param targetDeliveryModeHelper
     *            the targetDeliveryModeHelper to set
     */
    @Required
    public void setTargetDeliveryModeHelper(final TargetDeliveryModeHelper targetDeliveryModeHelper) {
        this.targetDeliveryModeHelper = targetDeliveryModeHelper;
    }


}
