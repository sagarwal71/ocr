/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.constants.TgtbusprocConstants;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtfluent.client.FluentClient;
import au.com.target.tgtfluent.constants.TgtFluentConstants;
import au.com.target.tgtfluent.converter.populator.FluentConsignmentReversePopulator;
import au.com.target.tgtfluent.converter.populator.FluentConsignmentWarehouseReversePopulator;
import au.com.target.tgtfluent.converter.populator.factory.FluentConsignmentPopulatorFactory;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.data.Event;
import au.com.target.tgtfluent.data.EventResponse;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.data.Fulfilment;
import au.com.target.tgtfluent.data.FulfilmentsResponse;
import au.com.target.tgtfluent.exception.FluentBaseException;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.exception.FluentFulfilmentException;
import au.com.target.tgtfluent.service.FluentConsignmentService;
import au.com.target.tgtfluent.service.FluentFulfilmentService;
import au.com.target.tgtfluent.service.FluentFulfilmentValidator;
import au.com.target.tgtfluent.util.FluentDataMap;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntry;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntryList;


/**
 * @author bhuang3
 *
 */
public class FluentFulfilmentServiceImpl extends AbstractBusinessService implements FluentFulfilmentService {

    private static final Logger LOG = Logger.getLogger(FluentFulfilmentServiceImpl.class);
    private static final String FLUENT_EVENT_RESPONSE_ERROR = "FLUENT_EVENT_RESPONSE_ERROR: eventName={0} "
            + "eventStatus={1} eventId={2} entityId={3} errorMessage={4} ";

    private FluentClient fluentClient;

    private FluentConsignmentReversePopulator fluentConsignmentReversePopulator;

    private FluentConsignmentWarehouseReversePopulator fluentConsignmentWarehouseReversePopulator;

    private FluentFulfilmentValidator fluentFulfilmentValidator;

    private FluentConsignmentService fluentConsignmentService;

    private TargetOrderService targetOrderService;

    private List<String> warehousesList;

    private FluentConsignmentPopulatorFactory fluentConsignmentPopulatorFactory;

    @Override
    public void createConsignmentsByOrderId(final OrderModel orderModel) throws FluentFulfilmentException {
        Assert.notNull(orderModel, "Order cannot be null");
        Assert.hasLength(orderModel.getFluentId(), "fluent Order Id must not be empty");
        final FulfilmentsResponse fulfilmentsResponse = fluentClient.retrieveFulfilmentsByOrder(orderModel
                .getFluentId());
        if (CollectionUtils.isNotEmpty(fulfilmentsResponse.getErrors())) {
            throw new FluentFulfilmentException(fulfilmentsResponse.getErrors().get(0).getMessage());
        }
        if (CollectionUtils.isEmpty(fulfilmentsResponse.getFulfilments())) {
            throw new FluentFulfilmentException("Fluent fulfilments are empty");
        }
        this.createConsignmentsByFulfilmentsResponse(fulfilmentsResponse, orderModel);
    }

    @Override
    public boolean isAnyAssignedConsignment(final OrderModel orderModel) {

        if (orderModel != null && CollectionUtils.isNotEmpty(orderModel.getConsignments())) {
            for (final ConsignmentModel consignment : orderModel.getConsignments()) {
                if (consignment.getWarehouse() != null
                        && !TgtCoreConstants.CANCELLATION_WAREHOUSE.equals(consignment.getWarehouse().getCode())
                        && !ConsignmentStatus.CANCELLED.equals(consignment.getStatus())) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public TargetOrderCancelEntryList createTargetOrderCancelEntryList(final OrderModel orderModel) {
        final TargetOrderCancelEntryList targetOrderCancelEntryList = new TargetOrderCancelEntryList();
        final List<TargetOrderCancelEntry> cancelEntries = new ArrayList<>();
        if (orderModel != null && CollectionUtils.isNotEmpty(orderModel.getConsignments())) {
            for (final ConsignmentModel consignmentModel : orderModel.getConsignments()) {
                if (consignmentModel.getWarehouse() != null
                        && TgtCoreConstants.CANCELLATION_WAREHOUSE.equals(consignmentModel.getWarehouse().getCode())) {
                    final List<TargetOrderCancelEntry> entries = createTargetOrderCancelEntryListByConsignment(
                            consignmentModel);
                    addAllNotEmpty(cancelEntries, entries);
                }
            }
        }
        targetOrderCancelEntryList.setEntriesToCancel(cancelEntries);
        return targetOrderCancelEntryList;
    }

    @Override
    public void upsertFulfilment(final Fulfilment fulfilment)
            throws FluentFulfilmentException {

        final TargetConsignmentModel consignment = getConsignment(fulfilment);
        if (consignment.getOrder() == null) {
            try {
                final OrderModel order = targetOrderService.findOrderByFluentId(fulfilment.getOrderId());
                consignment.setOrder(order);
            }
            catch (final UnknownIdentifierException e) {
                LOG.warn(null, e);
                throw new IllegalArgumentException(e);
            }
        }
        fluentFulfilmentValidator.validateFulfilment(fulfilment, consignment);
        if (consignment.getWarehouse() == null) {
            fluentConsignmentWarehouseReversePopulator.populate(fulfilment, consignment);
        }
        //No status transition validation required for EXPIRED,REJECTED,SYSTEM_REJECTED, CANCELLED_NO_STOCK and SYSTEM_EXPIRED fulfilment status
        if (FluentDataMap.SYSTEM_TERMINAL_STATUS.contains(fulfilment.getStatus()) ||
                fluentFulfilmentValidator.validateStateTransition(fulfilment, consignment)) {
            fluentConsignmentReversePopulator.populate(fulfilment, consignment);
            getModelService().save(consignment);
            //pick and ship process will be started from here.
            startFluentOrderBusinessProcesses(consignment);
        }
    }

    /**
     * @param consignment
     */
    protected void startFluentConsignmentCancellationProcess(final TargetConsignmentModel consignment) {
        final List<TargetOrderCancelEntry> entries = createTargetOrderCancelEntryListByConsignment(consignment);
        if (CollectionUtils.isNotEmpty(entries)) {
            final TargetOrderCancelEntryList targetOrderCancelEntryList = new TargetOrderCancelEntryList();
            targetOrderCancelEntryList.setEntriesToCancel(entries);
            getTargetBusinessProcessService().startFluentOrderItemCancelProcess((OrderModel)consignment.getOrder(),
                    targetOrderCancelEntryList);
        }
    }

    /**
     * @param consignment
     */
    protected void startFluentOrderBusinessProcesses(final TargetConsignmentModel consignment) {

        if (null != consignment.getWarehouse() && !warehousesList.contains(consignment.getWarehouse().getCode())
                && ConsignmentStatus.PICKED.equals(consignment.getStatus())) {
            getTargetBusinessProcessService().startFluentOrderConsignmentPickedProcess(
                    (OrderModel)consignment.getOrder(),
                    consignment,
                    TgtbusprocConstants.BusinessProcess.FLUENT_CONSIGNMENT_PICKED_PROCESS);

        }
        else if (ConsignmentStatus.SHIPPED.equals(consignment.getStatus())) {
            getTargetBusinessProcessService().startFluentOrderConsignmentShippedProcess(
                    (OrderModel)consignment.getOrder(),
                    consignment,
                    TgtbusprocConstants.BusinessProcess.FLUENT_CONSIGNMENT_SHIPPED_PROCESS);
        }
        else if (ConsignmentStatus.CANCELLED.equals(consignment.getStatus())
                && TgtCoreConstants.CANCELLATION_WAREHOUSE.equals(consignment.getWarehouse().getCode())) {
            startFluentConsignmentCancellationProcess(consignment);
        }
    }

    /**
     * @param fulfilment
     * @return {@link TargetConsignmentModel}
     */
    protected TargetConsignmentModel getConsignment(final Fulfilment fulfilment) {
        return fluentConsignmentService
                .getOrCreateConsignmentForCode(String.valueOf(fulfilment.getFulfilmentId()));
    }

    /**
     * 
     * @param fluentResponse
     * @throws FluentFulfilmentException
     * @throws FluentClientException
     */
    private void checkFluentResponse(final FluentResponse fluentResponse, final String eventName)
            throws FluentFulfilmentException, FluentClientException {
        if (CollectionUtils.isNotEmpty(fluentResponse.getErrors())) {
            final Error error = fluentResponse.getErrors().iterator().next();
            throw new FluentClientException(error);
        }
        if (fluentResponse instanceof EventResponse
                && StringUtils.isNotBlank(((EventResponse)fluentResponse).getErrorMessage())) {
            final EventResponse response = (EventResponse)fluentResponse;
            final String errorInfo = MessageFormat.format(FLUENT_EVENT_RESPONSE_ERROR, eventName,
                    response.getEventId(), response.getEntityId(), response.getErrorMessage());
            throw new FluentFulfilmentException(errorInfo);
        }
    }

    /**
     * 
     */
    @Override
    public void sendReadyForPickup(final TargetConsignmentModel consignment)
            throws FluentBaseException {
        final Event event = new Event();
        event.setName(TgtFluentConstants.Event.READY_FOR_PICKUP);
        event.setEntityId(consignment.getCode());
        event.setEntityType(TgtFluentConstants.EntityType.FULFILMENT);
        event.setEntitySubtype(TgtFluentConstants.EntitySubtype.CC);
        final FluentResponse fluentResponse = fluentClient.eventSync(event);
        checkFluentResponse(fluentResponse, TgtFluentConstants.Event.READY_FOR_PICKUP);
    }

    /**
     * 
     */
    @Override
    public void sendConsignmentEventToFluent(final ConsignmentModel consignment, final String eventName,
            final boolean isCreateFulfilment)
            throws FluentBaseException {
        if (StringUtils.isEmpty(eventName)) {
            return;
        }
        final Event event = new Event();
        event.setName(eventName);
        event.setEntityId(consignment.getCode());
        event.setEntityType(TgtFluentConstants.EntityType.FULFILMENT);
        if (isCreateFulfilment) {
            final Fulfilment fulfilment = new Fulfilment();
            fluentConsignmentPopulatorFactory.getConsignmentPopulator(eventName).populate(consignment, fulfilment);
            event.setAttributes(fulfilment);
        }
        final FluentResponse fluentResponse = fluentClient.eventSync(event);
        checkFluentResponse(fluentResponse, eventName);
    }

    private void addAllNotEmpty(final List<TargetOrderCancelEntry> cancelEntries,
            final List<TargetOrderCancelEntry> entries) {
        if (CollectionUtils.isNotEmpty(entries) && cancelEntries != null) {
            cancelEntries.addAll(entries);
        }
    }

    private List<TargetOrderCancelEntry> createTargetOrderCancelEntryListByConsignment(
            final ConsignmentModel consignmentModel) {
        final List<TargetOrderCancelEntry> targetOrderCancelEntries = new ArrayList<>();
        if (consignmentModel != null && CollectionUtils.isNotEmpty(consignmentModel.getConsignmentEntries())) {
            for (final ConsignmentEntryModel consignmentEntryModel : consignmentModel.getConsignmentEntries()) {
                final TargetOrderCancelEntry targetOrderCancelEntry = new TargetOrderCancelEntry();
                targetOrderCancelEntry.setOrderEntry(consignmentEntryModel.getOrderEntry());
                targetOrderCancelEntry.setCancelQuantity(consignmentEntryModel.getQuantity());
                targetOrderCancelEntries.add(targetOrderCancelEntry);
            }
        }
        return targetOrderCancelEntries;
    }

    private void createConsignmentsByFulfilmentsResponse(final FulfilmentsResponse fulfilmentsResponse,
            final OrderModel orderModel) throws FluentFulfilmentException {
        Assert.notEmpty(fulfilmentsResponse.getFulfilments(), "Fulfilments must not be empty");
        final List<TargetConsignmentModel> toSaveList = new ArrayList<>();
        for (final Fulfilment fulfilment : fulfilmentsResponse.getFulfilments()) {
            final TargetConsignmentModel targetConsignmentModel = createConsignmentByFulfilment(fulfilment,
                    orderModel);
            toSaveList.add(targetConsignmentModel);
        }
        getModelService().saveAll(toSaveList);
        getModelService().save(orderModel);
        getModelService().refresh(orderModel);
    }

    private TargetConsignmentModel createConsignmentByFulfilment(final Fulfilment fulfilment,
            final OrderModel orderModel) {
        final TargetConsignmentModel targetConsignmentModel = fluentConsignmentService
                .getOrCreateConsignmentForCode(String.valueOf(fulfilment.getFulfilmentId()));
        if (targetConsignmentModel.getOrder() == null) {
            targetConsignmentModel.setOrder(orderModel);
        }
        if (fluentFulfilmentValidator.validateFulfilment(fulfilment, targetConsignmentModel)) {
            fluentConsignmentWarehouseReversePopulator.populate(fulfilment, targetConsignmentModel);
            fluentConsignmentReversePopulator.populate(fulfilment, targetConsignmentModel);
        }
        return targetConsignmentModel;
    }

    /**
     * Lookup for TargetBusinessProcessService
     * 
     * @return TargetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

    /**
     * @param fluentClient
     *            the fluentClient to set
     */
    @Required
    public void setFluentClient(final FluentClient fluentClient) {
        this.fluentClient = fluentClient;
    }

    /**
     * @param fluentConsignmentReversePopulator
     *            the fluentConsignmentReversePopulator to set
     */
    @Required
    public void setFluentConsignmentReversePopulator(
            final FluentConsignmentReversePopulator fluentConsignmentReversePopulator) {
        this.fluentConsignmentReversePopulator = fluentConsignmentReversePopulator;
    }

    /**
     * @param fluentConsignmentWarehouseReversePopulator
     *            the fluentConsignmentWarehouseReversePopulator to set
     */
    @Required
    public void setFluentConsignmentWarehouseReversePopulator(
            final FluentConsignmentWarehouseReversePopulator fluentConsignmentWarehouseReversePopulator) {
        this.fluentConsignmentWarehouseReversePopulator = fluentConsignmentWarehouseReversePopulator;
    }

    /**
     * @param fluentFulfilmentValidator
     *            the fluentFulfilmentValidator to set
     */
    @Required
    public void setFluentFulfilmentValidator(final FluentFulfilmentValidator fluentFulfilmentValidator) {
        this.fluentFulfilmentValidator = fluentFulfilmentValidator;
    }

    /**
     * @param fluentConsignmentService
     *            the fluentConsignmentService to set
     */
    @Required
    public void setFluentConsignmentService(final FluentConsignmentService fluentConsignmentService) {
        this.fluentConsignmentService = fluentConsignmentService;
    }

    /**
     * @param targetOrderService
     *            the targetOrderService to set
     */
    @Required
    public void setTargetOrderService(final TargetOrderService targetOrderService) {
        this.targetOrderService = targetOrderService;
    }

    /**
     * @param warehousesList
     *            the warehousesList to set
     */
    @Required
    public void setWarehousesList(final List<String> warehousesList) {
        this.warehousesList = warehousesList;
    }

    /**
     * @param fluentConsignmentPopulatorFactory
     *            the fluentConsignmentPopulatorFactory to set
     */
    @Required
    public void setFluentConsignmentPopulatorFactory(
            final FluentConsignmentPopulatorFactory fluentConsignmentPopulatorFactory) {
        this.fluentConsignmentPopulatorFactory = fluentConsignmentPopulatorFactory;
    }
}
