/**
 * 
 */
package au.com.target.tgtfluent.service;

import de.hybris.platform.core.model.order.OrderModel;

import au.com.target.tgtfluent.data.Order;


/**
 * @author bpottass
 *
 */
public interface FluentOrderValidator {

    /**
     * 
     * 
     * @param orderModel
     * @return true if all items in the order are NOT either fulfilled or refunded. Need to add up only shipped quantity
     *         since a refund modifies requested quantity of the order
     */
    boolean validateIfAnyItemInOrderIsNotShipped(OrderModel orderModel);

    /**
     * Validate shipped consignments
     * 
     * @param orderModel
     * @return true or false
     */
    boolean validateIfAnyConsignmentsAreOpen(OrderModel orderModel);



    /**
     * 
     * @param order
     * @return true if the fluent order data contains: orderId, items, status
     */
    boolean isOrderDataInValid(Order order);

}
