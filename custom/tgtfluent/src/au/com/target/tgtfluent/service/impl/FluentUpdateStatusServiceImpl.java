/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import java.util.ArrayList;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtfluent.dao.FluentUpdateStatusDao;
import au.com.target.tgtfluent.model.AbstractFluentUpdateStatusModel;
import au.com.target.tgtfluent.model.FluentUpdateFailureCauseModel;
import au.com.target.tgtfluent.service.FluentUpdateStatusService;


/**
 * @author bhuang3
 *
 */
public class FluentUpdateStatusServiceImpl extends AbstractBusinessService implements FluentUpdateStatusService {

    private FluentUpdateStatusDao fluentUpdateStatusDao;

    @Override
    public <T extends AbstractFluentUpdateStatusModel> T getFluentUpdateStatus(final Class<T> searchClass,
            final String typeCode, final String code) {
        T updateStatus;
        try {
            updateStatus = (T)fluentUpdateStatusDao.find(typeCode, code);
        }
        catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
            updateStatus = getModelService().create(searchClass);
            //set default value
            updateStatus.setCode(code);
        }
        if (CollectionUtils.isEmpty(updateStatus.getFailureCauses())) {
            updateStatus.setFailureCauses(new ArrayList<FluentUpdateFailureCauseModel>());
        }
        return updateStatus;
    }

    /**
     * @param fluentUpdateStatusDao
     *            the fluentUpdateStatusDao to set
     */
    @Required
    public void setFluentUpdateStatusDao(final FluentUpdateStatusDao fluentUpdateStatusDao) {
        this.fluentUpdateStatusDao = fluentUpdateStatusDao;
    }

}
