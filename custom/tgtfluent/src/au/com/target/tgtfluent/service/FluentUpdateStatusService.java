/**
 * 
 */
package au.com.target.tgtfluent.service;

import au.com.target.tgtfluent.model.AbstractFluentUpdateStatusModel;


/**
 * @author bhuang3
 *
 */
public interface FluentUpdateStatusService {

    /**
     * Get the FluentUpdateStatusModel by code, if no model found, create a new one
     * 
     * @param searchClass
     * @param typeCode
     * @param code
     * @return T extends AbstractFluentUpdateStatusModel
     */
    <T extends AbstractFluentUpdateStatusModel> T getFluentUpdateStatus(final Class<T> searchClass,
            final String typeCode, final String code);

}
