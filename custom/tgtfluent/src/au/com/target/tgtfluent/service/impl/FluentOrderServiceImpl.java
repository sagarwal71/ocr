/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import java.text.MessageFormat;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.constants.TgtbusprocConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.order.FluentErrorException;
import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtcore.order.FluentOrderService;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtfluent.client.FluentClient;
import au.com.target.tgtfluent.constants.FluentOrderStatus;
import au.com.target.tgtfluent.constants.TgtFluentConstants;
import au.com.target.tgtfluent.converter.populator.OrderPopulator;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.data.Event;
import au.com.target.tgtfluent.data.EventResponse;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.data.Order;
import au.com.target.tgtfluent.data.OrderResponse;
import au.com.target.tgtfluent.data.OrderType;
import au.com.target.tgtfluent.service.FluentOrderValidator;




/**
 * @author bpottass
 *
 */
public class FluentOrderServiceImpl extends AbstractBusinessService implements FluentOrderService {

    private static final String ORDER_LOG_TEMPLATE = "FLUENT_CREATE_ORDER_RESPONSE_ERROR: orderCode:{0}, errorCode:{1}, message:{2}";

    private static final String GET_ORDER_LOG_TEMPLATE = "FLUENT_GET_ORDER_RESPONSE_ERROR: fluentOrderId={0}, errorCode={1}, message={2}";

    private static final String CANCEL_ORDER_FAILED_IN_FLUENT = "The order is not able to be cancelled.";

    private static final String CANCEL_ORDER_FAILED_SYSTEM_ERROR = "The order was not able to be cancelled due to a system issue. Please retry.";

    private static final String FLUENT_ORDER_UPDATE_ERROR_TEMPLATE = "FLUENT_ORDER_UPDATE_ERROR: hybrisOrderId={0}, hybrisOrderStatus={1}, fluentOrderId={2}, fluentOrderStatus={3}, message={4}";

    private static final String FLUENT_ORDER_UPDATE_INVALID_ORDER = "FLUENT_ORDER_UPDATE_INVALID_ORDER: fluentOrderId={0}, fluentOrderStatus={1}, message={2}";


    private OrderPopulator orderPopulator;

    private FluentClient fluentClient;

    private TargetOrderService targetOrderService;

    private FluentOrderValidator fluentOrderValidator;

    private TargetDeliveryModeHelper targetDeliveryModeHelper;


    @Override
    public String createOrder(final OrderModel orderModel) throws FluentOrderException {
        final Order orderRequest = new Order();
        try {
            orderPopulator.populate(orderModel, orderRequest);
            final FluentResponse fluentResponse = fluentClient.createOrder(orderRequest);

            if (CollectionUtils.isNotEmpty(fluentResponse.getErrors())) {
                final Error error = fluentResponse.getErrors().get(0);
                throw new FluentOrderException(
                        MessageFormat.format(ORDER_LOG_TEMPLATE, orderModel.getCode(), error.getCode(),
                                error.getMessage()));
            }
            if (StringUtils.isEmpty(fluentResponse.getId())) {
                throw new FluentOrderException(
                        MessageFormat.format(ORDER_LOG_TEMPLATE, orderModel.getCode(), null, "No fluentId returned"));

            }
            return fluentResponse.getId();
        }
        catch (final Exception ex) {
            throw new FluentOrderException(
                    MessageFormat.format(ORDER_LOG_TEMPLATE, orderModel.getCode(), null, ex.getMessage()), ex);
        }

    }

    @Override
    public void cancelOrder(final OrderModel orderModel) {
        final Event event = new Event();
        event.setName(TgtFluentConstants.Event.CANCEL_ORDER);
        event.setEntityId(orderModel.getFluentId());
        event.setEntityType(TgtFluentConstants.EntityType.ORDER);
        event.setEntitySubtype(getOrderType(orderModel).name());
        final FluentResponse fluentResponse = fluentClient.eventSync(event);
        if (CollectionUtils.isNotEmpty(fluentResponse.getErrors())) {
            final Error error = fluentResponse.getErrors().iterator().next();
            throw new FluentOrderException("code=" + error.getCode() + ", message=" + error.getMessage());
        }
        if (fluentResponse instanceof EventResponse
                && StringUtils.isNotBlank(((EventResponse)fluentResponse).getErrorMessage())) {
            throw new FluentOrderException("eventStatus=" + ((EventResponse)fluentResponse).getEventStatus()
                    + ", errorMessage=" + ((EventResponse)fluentResponse).getErrorMessage());
        }
    }

    @Override
    public void resumeOrder(final OrderModel orderModel) {
        final Event event = new Event();
        event.setName(TgtFluentConstants.Event.RESUME_ORDER);
        event.setEntityId(orderModel.getFluentId());
        event.setEntityType(TgtFluentConstants.EntityType.ORDER);
        event.setEntitySubtype(getOrderType(orderModel).name());
        final FluentResponse fluentResponse = fluentClient.eventSync(event);
        if (CollectionUtils.isNotEmpty(fluentResponse.getErrors())) {
            final Error error = fluentResponse.getErrors().iterator().next();
            throw new FluentOrderException("order=" + orderModel.getCode() + ", code=" + error.getCode() + ", message="
                    + error.getMessage());
        }
        if (fluentResponse instanceof EventResponse
                && StringUtils.isNotBlank(((EventResponse)fluentResponse).getErrorMessage())) {
            throw new FluentErrorException("order=" + orderModel.getCode() + ", eventStatus="
                    + ((EventResponse)fluentResponse).getEventStatus()
                    + ", errorMessage=" + ((EventResponse)fluentResponse).getErrorMessage());
        }
    }

    @Override
    public boolean releaseOrder(final OrderModel orderModel) {
        final Event event = new Event();
        event.setName(TgtFluentConstants.Event.PREORDER_RELEASE);
        event.setEntityId(orderModel.getFluentId());
        event.setEntityType(TgtFluentConstants.EntityType.ORDER);
        event.setEntitySubtype(getOrderType(orderModel).name());
        final FluentResponse fluentResponse = fluentClient.eventSync(event);
        if (CollectionUtils.isNotEmpty(fluentResponse.getErrors())) {
            final Error error = fluentResponse.getErrors().iterator().next();
            throw new FluentOrderException("order=" + orderModel.getCode() + ", code=" + error.getCode() + ", message="
                    + error.getMessage());
        }
        if (fluentResponse instanceof EventResponse
                && StringUtils.isNotBlank(((EventResponse)fluentResponse).getErrorMessage())) {
            throw new FluentErrorException("order=" + orderModel.getCode() + ", eventStatus="
                    + ((EventResponse)fluentResponse).getEventStatus()
                    + ", errorMessage=" + ((EventResponse)fluentResponse).getErrorMessage());
        }
        return true;
    }

    @Override
    public void updateOrder(final Order order) throws FluentOrderException, FluentErrorException {
        Assert.notNull(order, "Fluent order cannot be null");
        checkIntegrityOfFluentOrder(order);
        final OrderModel orderModel = targetOrderService.findOrderByFluentId(order.getOrderId());
        Assert.notNull(orderModel, "order can't be null");

        if (FluentOrderStatus.SHIPPED.equals(FluentOrderStatus.get(order.getStatus()))) {
            checkIfOrderCanBeUpdatedToShipped(orderModel, order);
            getTargetBusinessProcessService().startFluentOrderShippedProcess(orderModel,
                    TgtbusprocConstants.BusinessProcess.FLUENT_ORDER_SHIPPED_PROCESS);
        }
    }

    @Override
    public OrderResponse getOrder(final String fluentOrderId) throws FluentOrderException {
        final FluentResponse fluentResponse = fluentClient.viewOrder(fluentOrderId);

        if (CollectionUtils.isNotEmpty(fluentResponse.getErrors())) {
            final Error error = fluentResponse.getErrors().iterator().next();
            throw new FluentOrderException(
                    MessageFormat.format(GET_ORDER_LOG_TEMPLATE, fluentOrderId, error.getCode(),
                            error.getMessage()));
        }

        return (OrderResponse)fluentResponse;
    }

    @Override
    public String customerCancelOrder(final OrderModel orderModel) {
        final Event event = new Event();
        event.setName(TgtFluentConstants.Event.CUSTOMER_CANCEL);
        event.setEntityId(orderModel.getFluentId());
        event.setEntityType(TgtFluentConstants.EntityType.ORDER);
        event.setEntitySubtype(getOrderType(orderModel).name());
        String failedReason = null;
        final FluentResponse fluentResponse = fluentClient.eventSync(event);
        if (CollectionUtils.isNotEmpty(fluentResponse.getErrors())) {
            failedReason = CANCEL_ORDER_FAILED_SYSTEM_ERROR;
        }
        if (fluentResponse instanceof EventResponse
                && StringUtils.isNotBlank(((EventResponse)fluentResponse).getErrorMessage())) {
            failedReason = CANCEL_ORDER_FAILED_IN_FLUENT;
        }
        return failedReason;
    }


    /**
     * @param order
     */
    private void checkIntegrityOfFluentOrder(final Order order) {
        if (fluentOrderValidator.isOrderDataInValid(order)) {
            throw new FluentOrderException(MessageFormat.format(FLUENT_ORDER_UPDATE_INVALID_ORDER, order.getOrderId(),
                    order.getStatus(), "Fluent order missing either orderId or status"));
        }
    }


    /**
     * Lookup for TargetBusinessProcessService
     * 
     * @return TargetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

    /**
     * 
     * @param orderModel
     * @param order
     */
    private void checkIfOrderCanBeUpdatedToShipped(final OrderModel orderModel, final Order order) {
        if (OrderStatus.COMPLETED.equals(orderModel.getStatus())
                || OrderStatus.CANCELLED.equals(orderModel.getStatus())) {
            throw new FluentOrderException(
                    MessageFormat.format(FLUENT_ORDER_UPDATE_ERROR_TEMPLATE, orderModel.getCode(),
                            orderModel.getStatus().getCode(), order.getOrderId(), order.getStatus(),
                            "Completed or Cancelled Order Cannot be updated"));
        }
        if (fluentOrderValidator.validateIfAnyConsignmentsAreOpen(orderModel)) {
            throw new FluentErrorException(
                    MessageFormat.format(FLUENT_ORDER_UPDATE_ERROR_TEMPLATE, orderModel.getCode(),
                            orderModel.getStatus().getCode(), order.getOrderId(), order.getStatus(),
                            "Failed to complete order as it has open consignment(s)"));
        }
        if (fluentOrderValidator.validateIfAnyItemInOrderIsNotShipped(orderModel)) {
            throw new FluentErrorException(
                    MessageFormat.format(FLUENT_ORDER_UPDATE_ERROR_TEMPLATE, orderModel.getCode(),
                            orderModel.getStatus().getCode(), order.getOrderId(), order.getStatus(),
                            "Failed to complete order as it has items that are not shipped or refunded"));
        }
    }

    /**
     * 
     * @param orderModel
     * @return OrderType
     */
    private OrderType getOrderType(final OrderModel orderModel) {
        if (targetDeliveryModeHelper.isDeliveryModeStoreDelivery(orderModel.getDeliveryMode())) {
            return OrderType.CC;
        }
        else {
            return OrderType.HD;
        }
    }

    /**
     * @param orderPopulator
     *            the orderPopulator to set
     */
    @Required
    public void setOrderPopulator(final OrderPopulator orderPopulator) {
        this.orderPopulator = orderPopulator;
    }

    /**
     * @param targetDeliveryModeHelper
     *            the targetDeliveryModeHelper to set
     */
    @Required
    public void setTargetDeliveryModeHelper(final TargetDeliveryModeHelper targetDeliveryModeHelper) {
        this.targetDeliveryModeHelper = targetDeliveryModeHelper;
    }

    /**
     * @param fluentClient
     *            the fluentClient to set
     */
    @Required
    public void setFluentClient(final FluentClient fluentClient) {
        this.fluentClient = fluentClient;
    }

    /**
     * @param targetOrderService
     *            the targetOrderService to set
     */
    @Required
    public void setTargetOrderService(final TargetOrderService targetOrderService) {
        this.targetOrderService = targetOrderService;
    }

    /**
     * @param fluentOrderValidator
     *            the fluentOrderValidator to set
     */
    @Required
    public void setFluentOrderValidator(final FluentOrderValidator fluentOrderValidator) {
        this.fluentOrderValidator = fluentOrderValidator;
    }

}
