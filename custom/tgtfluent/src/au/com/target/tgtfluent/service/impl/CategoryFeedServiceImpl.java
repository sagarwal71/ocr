/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.category.TargetCategoryService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtfluent.constants.TgtFluentConstants;
import au.com.target.tgtfluent.model.CategoryFluentUpdateStatusModel;
import au.com.target.tgtfluent.service.CategoryFeedService;
import au.com.target.tgtfluent.service.FluentCategoryUpsertService;
import au.com.target.tgtfluent.service.FluentUpdateStatusService;


/**
 * @author bpottass
 *
 */
public class CategoryFeedServiceImpl implements CategoryFeedService {

    private TargetCategoryService targetCategoryService;

    private CatalogVersionService catalogVersionService;

    private FluentUpdateStatusService fluentUpdateStatusService;

    private FluentCategoryUpsertService fluentCategoryUpsertService;

    @Override
    public void feedCategoriesToFluent() {
        final TargetProductCategoryModel rootCategory = getRootCategory();
        sendCategoryToFluentIfNotSynched(rootCategory);
        getSubCategoriesAndFeedToFluent(rootCategory);
    }

    /**
     * Starting from the rootnode(AP01), traverses the category tree depth-first. Feeding category to fluent and then
     * its subsequent subcategories. Parent category need to be created before child categories are created in fluent.
     * 
     * @param category
     */
    private void getSubCategoriesAndFeedToFluent(final TargetProductCategoryModel category) {
        final List<CategoryModel> subCategories = category.getCategories();
        for (final CategoryModel subCategory : subCategories) {
            if (subCategory instanceof TargetProductCategoryModel) {
                final TargetProductCategoryModel targetProductCategory = (TargetProductCategoryModel)subCategory;
                sendCategoryToFluentIfNotSynched(targetProductCategory);
                if (CollectionUtils.isNotEmpty(targetProductCategory.getCategories())) {
                    getSubCategoriesAndFeedToFluent(targetProductCategory);
                }
            }
        }
    }

    /**
     * @param category
     */
    private void sendCategoryToFluentIfNotSynched(final TargetProductCategoryModel category) {
        if (!isCategorySynchedToFluent(category)) {
            feedCategoryToFluent(category);
        }
    }


    /**
     * @param subCategory
     */
    private boolean isCategorySynchedToFluent(final TargetProductCategoryModel subCategory) {
        final CategoryFluentUpdateStatusModel fluentUpdateStatus = getCategoryFluentUpdateStatusModel(
                subCategory);
        return fluentUpdateStatus != null && fluentUpdateStatus.isLastRunSuccessful();
    }

    /**
     * 
     * @param category
     */
    private void feedCategoryToFluent(final TargetProductCategoryModel category) {
        fluentCategoryUpsertService.upsertCategory(category);
    }

    /**
     * 
     * @param category
     * @return CategoryFluentUpdateStatusModel
     */
    private CategoryFluentUpdateStatusModel getCategoryFluentUpdateStatusModel(
            final TargetProductCategoryModel category) {
        return fluentUpdateStatusService
                .getFluentUpdateStatus(CategoryFluentUpdateStatusModel.class,
                        CategoryFluentUpdateStatusModel._TYPECODE, category.getCode());
    }

    /**
     * 
     * @return TargetProductCategoryModel
     */
    private TargetProductCategoryModel getRootCategory() {
        final CatalogVersionModel offlineProductCatalog = catalogVersionService.getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS, TgtCoreConstants.Catalog.OFFLINE_VERSION);
        return (TargetProductCategoryModel)targetCategoryService
                .getCategoryForCode(offlineProductCatalog, TgtFluentConstants.Category.ALL_PRODUCTS);
    }

    /**
     * @param targetCategoryService
     *            the targetCategoryService to set
     */
    @Required
    public void setTargetCategoryService(final TargetCategoryService targetCategoryService) {
        this.targetCategoryService = targetCategoryService;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @param fluentUpdateStatusService
     *            the fluentUpdateStatusService to set
     */
    @Required
    public void setFluentUpdateStatusService(final FluentUpdateStatusService fluentUpdateStatusService) {
        this.fluentUpdateStatusService = fluentUpdateStatusService;
    }

    /**
     * @param fluentCategoryUpsertService
     *            the fluentCategoryUpsertService to set
     */
    @Required
    public void setFluentCategoryUpsertService(final FluentCategoryUpsertService fluentCategoryUpsertService) {
        this.fluentCategoryUpsertService = fluentCategoryUpsertService;
    }

}
