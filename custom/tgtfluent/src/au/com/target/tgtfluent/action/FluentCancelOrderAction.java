/**
 * 
 */
package au.com.target.tgtfluent.action;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtcore.order.FluentOrderService;


/**
 * @author mgazal
 *
 */
public class FluentCancelOrderAction extends AbstractProceduralAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(FluentCancelOrderAction.class);

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private FluentOrderService fluentOrderService;

    @Override
    public void executeAction(final OrderProcessModel orderProcessModel) throws RetryLaterException, Exception {
        if (!(targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)
                && StringUtils.isNotEmpty(orderProcessModel.getOrder().getFluentId()))) {
            return;
        }
        try {
            fluentOrderService.cancelOrder(orderProcessModel.getOrder());
        }
        catch (final FluentOrderException e) {
            LOG.error("FLUENT_CANCEL_ORDER_FAILED, cause=" + e.getLocalizedMessage());
            throw e;
        }
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * @param fluentOrderService
     *            the fluentOrderService to set
     */
    @Required
    public void setFluentOrderService(final FluentOrderService fluentOrderService) {
        this.fluentOrderService = fluentOrderService;
    }
}
