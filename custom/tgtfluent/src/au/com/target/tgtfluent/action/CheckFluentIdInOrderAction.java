/**
 * 
 */
package au.com.target.tgtfluent.action;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;
import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtcore.order.FluentOrderService;


/**
 * @author bpottass
 *
 */
public class CheckFluentIdInOrderAction extends RetryActionWithSpringConfig<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(CheckFluentIdInOrderAction.class);

    private static final String FLUENT_CREATE_ORDER_ERROR = "FLUENT_CREATE_ORDER_ERROR: orderId:{0}, message={1}";

    private static final String RETRY_ACTION = "Trigger retry for create order in fluent for order: orderId:{0}";

    private FluentOrderService fluentOrderService;

    public enum Transition {
        OK, RETRYEXCEEDED;

        public static Set<String> getStringValues() {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values()) {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    protected String executeInternal(final OrderProcessModel process) throws RetryLaterException, Exception {
        final OrderModel order = process.getOrder();
        Assert.notNull(order, "Order cannot be null");

        if (StringUtils.isEmpty(order.getFluentId())){
            order.setFluentId(createOrderInFluent(order));
            save(order);
        }

        return Transition.OK.toString();
    }

    /**
     * Creates order in fluent and returns fluent id. Services throws FluentOrderException for any exception or when
     * fluent id is null or empty
     * 
     * @param order
     * @return fluent id
     * @throws RetryLaterException
     */
    private final String createOrderInFluent(final OrderModel order) throws RetryLaterException {
        try {
            return fluentOrderService.createOrder(order);
        }
        catch (final FluentOrderException ex) {
            LOG.error(MessageFormat.format(FLUENT_CREATE_ORDER_ERROR, order.getCode(),
                    ex.getMessage()));
            throw new RetryLaterException(MessageFormat.format(RETRY_ACTION, order.getCode()));
        }
    }


    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }

    /**
     * @param fluentOrderService
     *            the fluentOrderService to set
     */
    public void setFluentOrderService(final FluentOrderService fluentOrderService) {
        this.fluentOrderService = fluentOrderService;
    }

}
