/**
 * 
 */
package au.com.target.tgtfluent.action;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;




/**
 * @author kbalasub
 * 
 *         Checks if an order is a fluent Order or not
 * 
 *
 */
public class CheckIfFluentOrderAction extends AbstractAction<OrderProcessModel> {

    private enum Transition {
        TRUE, FALSE;

        public static Set<String> getStringValues() {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values()) {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    public String execute(final OrderProcessModel process) throws RetryLaterException, Exception {
        final OrderModel order = process.getOrder();
        Assert.notNull(order, "Order cannot be null");

        return StringUtils.isEmpty(order.getFluentId()) ? Transition.FALSE.toString() : Transition.TRUE.toString();
    }

    @Override
    public Set<String> getTransitions() {
        return Transition.getStringValues();
    }


}
