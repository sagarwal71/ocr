/**
 * 
 */
package au.com.target.tgtfluent.action;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtfluent.service.FluentFulfilmentService;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntryList;


/**
 * @author bhuang3
 *
 */
public class FluentFulfilmentCancelAndRefundAction extends AbstractAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(FluentFulfilmentCancelAndRefundAction.class);

    private FluentFulfilmentService fluentFulfilmentService;

    public enum Transition
    {
        PICK, ZERO_PICK;

        public static Set<String> getStringValues()
        {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values())
            {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    public String execute(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process, "OrderProcessModel cannot be null");
        final OrderModel orderModel = process.getOrder();
        Assert.notNull(orderModel, "Order cannot be null");
        final TargetOrderCancelEntryList targetOrderCancelEntryList = fluentFulfilmentService
                .createTargetOrderCancelEntryList(orderModel);
        if (targetOrderCancelEntryList != null
                && CollectionUtils.isNotEmpty(targetOrderCancelEntryList.getEntriesToCancel())) {
            LOG.info("Start fluent order item cancel process, order=" + orderModel.getCode());
            getTargetBusinessProcessService().startFluentOrderItemCancelProcess(process.getOrder(),
                    targetOrderCancelEntryList);
        }
        if (fluentFulfilmentService.isAnyAssignedConsignment(orderModel)) {
            return Transition.PICK.toString();
        }
        else {
            return Transition.ZERO_PICK.toString();
        }

    }

    /**
     * Lookup for TargetBusinessProcessService
     * 
     * @return TargetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

    @Override
    public Set<String> getTransitions() {
        return Transition.getStringValues();
    }

    /**
     * @param fluentFulfilmentService
     *            the fluentFulfilmentService to set
     */
    @Required
    public void setFluentFulfilmentService(final FluentFulfilmentService fluentFulfilmentService) {
        this.fluentFulfilmentService = fluentFulfilmentService;
    }

}
