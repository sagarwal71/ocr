/**
 * 
 */
package au.com.target.tgtfluent.action;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.order.FluentErrorException;
import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtcore.order.FluentOrderService;


/**
 * @author cbi
 *
 */
public class FluentResumeOrderAction extends RetryActionWithSpringConfig<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(FluentResumeOrderAction.class);

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private FluentOrderService fluentOrderService;

    public enum Transition {
        OK, NOK, RETRYEXCEEDED;
        public static Set<String> getStringValues()
        {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values())
            {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    public String executeInternal(final OrderProcessModel orderProcessModel) throws RetryLaterException, Exception {
        Transition result = Transition.OK;

        if (!targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)) {
            return result.toString();
        }
        try {
            fluentOrderService.resumeOrder(orderProcessModel.getOrder());
        }
        catch (final FluentOrderException e) {
            LOG.error("FLUENT_RESUME_ORDER_FAILED with FluentOrderException, cause=" + e);
            throw new RetryLaterException("Trigger retry for resuming order failed. cause=" + e);
        }
        catch (final FluentErrorException ex) {
            LOG.error("FLUENT_RESUME_ORDER_FAILED with Exception , cause=" + ex);
            result = Transition.NOK;
        }
        return result.toString();
    }

    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * @param fluentOrderService
     *            the fluentOrderService to set
     */
    @Required
    public void setFluentOrderService(final FluentOrderService fluentOrderService) {
        this.fluentOrderService = fluentOrderService;
    }

}
