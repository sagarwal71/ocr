/**
 * 
 */
package au.com.target.tgtfluent.constants;

import java.util.Arrays;
import java.util.Collection;


/**
 * @author gsing236
 *
 */
public enum FluentOrderStatus {

    BOOKED("BOOKED"),

    PARKED("PARKED"),

    CREATED("CREATED"),

    PENDING("PENDING"),

    INPROGRESS("INPROGRESS"),

    SHIPPED("SHIPPED"),

    COMPLETED("COMPLETED");

    private final Collection<String> fluentOrderStatuses;

    /**
     * @param fluentOrderStatuses
     */
    private FluentOrderStatus(final String... fluentOrderStatuses) {
        this.fluentOrderStatuses = Arrays.asList(fluentOrderStatuses);
    }

    /**
     * @return the fluentStatuses
     */
    public Collection<String> getFluentOrderStatuses() {
        return fluentOrderStatuses;
    }

    /**
     * @param fluentOrderStatuses
     * @return {@link FluentOrderStatus}
     */
    public static FluentOrderStatus get(final String fluentOrderStatuses) {
        if (fluentOrderStatuses == null) {
            return null;
        }
        for (final FluentOrderStatus status : values()) {
            if (status.getFluentOrderStatuses().contains(fluentOrderStatuses)) {
                return status;
            }
        }
        return null;
    }
}
