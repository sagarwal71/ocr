/**
 * 
 */
package au.com.target.tgtfluent.constants;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;

import java.util.Arrays;
import java.util.Collection;


/**
 * @author mgazal
 *
 */
public interface FulfilmentStatus {

    public enum Vendor {

        Target,

        Fastline,

        Incomm;
    }

    enum Dc {

        CREATED("CREATED"),

        SENT_TO_WAREHOUSE("SENT_TO_WAREHOUSE"),

        CONFIRMED_BY_WAREHOUSE("CONFIRMED_BY_WAREHOUSE"),

        PICKED("FULFILLED", "PARTIALLY_FULFILLED"),

        SHIPPED("SHIPPED");

        private final Collection<String> fluentStatuses;

        /**
         * @param fluentStatuses
         */
        private Dc(final String... fluentStatuses) {
            this.fluentStatuses = Arrays.asList(fluentStatuses);
        }

        /**
         * @return the fluentStatuses
         */
        public Collection<String> getFluentStatuses() {
            return fluentStatuses;
        }

        /**
         * @param consignmentStatus
         * @return {@link Dc}
         */
        public static Dc get(final ConsignmentStatus consignmentStatus) {
            if (consignmentStatus == null) {
                return null;
            }
            for (final Dc status : values()) {
                if (status.name().equals(consignmentStatus.getCode())) {
                    return status;
                }
            }
            return null;
        }

        /**
         * @param fluentStatus
         * @return {@link Dc}
         */
        public static Dc get(final String fluentStatus) {
            if (fluentStatus == null) {
                return null;
            }
            for (final Dc status : values()) {
                if (status.getFluentStatuses().contains(fluentStatus)) {
                    return status;
                }
            }
            return null;
        }
    }

    enum Store {

        CREATED("CREATED"),

        SENT_TO_WAREHOUSE("AWAITING_WAVE"),

        WAVED("ASSIGNED"),

        PICKED("FULFILLED", "PARTIALLY_FULFILLED"),

        SHIPPED("SHIPPED");

        private final Collection<String> fluentStatuses;

        /**
         * @param fluentStatuses
         */
        private Store(final String... fluentStatuses) {
            this.fluentStatuses = Arrays.asList(fluentStatuses);
        }

        /**
         * @return the fluentStatuses
         */
        public Collection<String> getFluentStatuses() {
            return fluentStatuses;
        }

        /**
         * @param consignmentStatus
         * @return {@link Store}
         */
        public static Store get(final ConsignmentStatus consignmentStatus) {
            if (consignmentStatus == null) {
                return null;
            }
            for (final Store status : values()) {
                if (status.name().equals(consignmentStatus.getCode())) {
                    return status;
                }
            }
            return null;
        }

        /**
         * @param fluentStatus
         * @return {@link Store}
         */
        public static Store get(final String fluentStatus) {
            if (fluentStatus == null) {
                return null;
            }
            for (final Store status : values()) {
                if (status.getFluentStatuses().contains(fluentStatus)) {
                    return status;
                }
            }
            return null;
        }
    }

    enum Incomm {

        CREATED("CREATED"),

        SENT_TO_WAREHOUSE("SENT_TO_WAREHOUSE"),

        SHIPPED("SHIPPED");

        private final Collection<String> fluentStatuses;

        /**
         * @param fluentStatuses
         */
        private Incomm(final String... fluentStatuses) {
            this.fluentStatuses = Arrays.asList(fluentStatuses);
        }

        /**
         * @return the fluentStatuses
         */
        public Collection<String> getFluentStatuses() {
            return fluentStatuses;
        }

        /**
         * @param consignmentStatus
         * @return {@link Store}
         */
        public static Incomm get(final ConsignmentStatus consignmentStatus) {
            if (consignmentStatus == null) {
                return null;
            }
            for (final Incomm status : values()) {
                if (status.name().equals(consignmentStatus.getCode())) {
                    return status;
                }
            }
            return null;
        }

        /**
         * @param fluentStatus
         * @return {@link Store}
         */
        public static Incomm get(final String fluentStatus) {
            if (fluentStatus == null) {
                return null;
            }
            for (final Incomm status : values()) {
                if (status.getFluentStatuses().contains(fluentStatus)) {
                    return status;
                }
            }
            return null;
        }
    }

    enum IncommPhysical {

        CREATED("CREATED"),

        SENT_TO_WAREHOUSE("SENT_TO_WAREHOUSE"),

        CONFIRMED_BY_WAREHOUSE("CONFIRMED_BY_WAREHOUSE"),

        SHIPPED("SHIPPED");

        private final Collection<String> fluentStatuses;

        /**
         * @param fluentStatuses
         */
        private IncommPhysical(final String... fluentStatuses) {
            this.fluentStatuses = Arrays.asList(fluentStatuses);
        }

        /**
         * @return the fluentStatuses
         */
        public Collection<String> getFluentStatuses() {
            return fluentStatuses;
        }

        /**
         * @param consignmentStatus
         * @return {@link Store}
         */
        public static IncommPhysical get(final ConsignmentStatus consignmentStatus) {
            if (consignmentStatus == null) {
                return null;
            }
            for (final IncommPhysical status : values()) {
                if (status.name().equals(consignmentStatus.getCode())) {
                    return status;
                }
            }
            return null;
        }

        /**
         * @param fluentStatus
         * @return {@link Store}
         */
        public static IncommPhysical get(final String fluentStatus) {
            if (fluentStatus == null) {
                return null;
            }
            for (final IncommPhysical status : values()) {
                if (status.getFluentStatuses().contains(fluentStatus)) {
                    return status;
                }
            }
            return null;
        }
    }
}
