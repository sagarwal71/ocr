/**
 * 
 */
package au.com.target.tgtfluent.data;

import de.hybris.platform.commercefacades.product.data.StockData;


/**
 * @author mgazal
 *
 */
public class FluentStock {

    private StockData ats;

    private StockData consolidated;

    private StockData store;

    /**
     * @return the ats
     */
    public StockData getAts() {
        return ats;
    }

    /**
     * @param ats
     *            the ats to set
     */
    public void setAts(final StockData ats) {
        this.ats = ats;
    }

    /**
     * @return the consolidated
     */
    public StockData getConsolidated() {
        return consolidated;
    }

    /**
     * @param consolidated
     *            the consolidated to set
     */
    public void setConsolidated(final StockData consolidated) {
        this.consolidated = consolidated;
    }

    /**
     * @return the store
     */
    public StockData getStore() {
        return store;
    }

    /**
     * @param store
     *            the store to set
     */
    public void setStore(final StockData store) {
        this.store = store;
    }
}
