/**
 * 
 */
package au.com.target.tgtfluent.client.impl;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.target.tgtfluent.client.FluentClient;
import au.com.target.tgtfluent.data.AbstractEntity;
import au.com.target.tgtfluent.data.Batch;
import au.com.target.tgtfluent.data.BatchResponse;
import au.com.target.tgtfluent.data.Category;
import au.com.target.tgtfluent.data.CategoryResponse;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.data.Event;
import au.com.target.tgtfluent.data.EventResponse;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.data.FulfilmentOptionRequest;
import au.com.target.tgtfluent.data.FulfilmentOptionResponse;
import au.com.target.tgtfluent.data.FulfilmentsResponse;
import au.com.target.tgtfluent.data.Job;
import au.com.target.tgtfluent.data.JobResponse;
import au.com.target.tgtfluent.data.Location;
import au.com.target.tgtfluent.data.Order;
import au.com.target.tgtfluent.data.OrderResponse;
import au.com.target.tgtfluent.data.Product;
import au.com.target.tgtfluent.data.ProductsResponse;
import au.com.target.tgtfluent.data.Sku;
import au.com.target.tgtfluent.data.SkusResponse;


/**
 * @author mgazal
 *
 */
public class FluentClientImpl implements FluentClient {

    private static final Logger LOG = Logger.getLogger(FluentClientImpl.class);

    private static final String LOG_TEMPLATE = "FLUENT: action={0} {1}";

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private static final String RETAILER_ID = "Retailer_id";

    private static final String PRODUCT_REF = "productRef";

    private static final String CATEGORY_REF = "categoryRef";

    private static final String SKU_REF = "skuRef";

    private static final String JOB = "/api/v4.1/job/{jobId}";

    private static final String BATCH = "/api/v4.1/job/{jobId}/batch/{batchId}";

    private static final String LOCATION = "/api/v4.1/location/{locationId}";

    private static final String PRODUCT = "/api/v4.1/product/{productId}";

    private static final String SKU = "/api/v4.1/product/{productId}/sku/{skuId}";

    private static final String SKU_SEARCH = "/api/v4.1/sku";

    private static final String CATEGORY = "/api/v4.1/category/{categoryId}";

    private static final String FULFILMENT_OPTION = "/api/v4.1/fulfilmentOptions/{id}";

    private static final String ORDER = "/api/v4.1/order/{orderId}";

    private static final String EVENT = "/api/v4.1/event/{id}";

    private static final String RETRIEVE_FULFILMENTS = "/api/v4.1/order/{orderId}/fulfilment";

    private static final String RETAILER_ID_TEMPLATE = "{retailerId}";

    private static final String HASHED_VALUE = "#####";

    private String fluentBaseUrl;

    private RestTemplate fluentRestTemplate;

    private static final String CREATE_ORDER = "createOrder";

    private List<String> blacklistedFields;

    /**
     * Generic CREATE/POST entity
     * 
     * @param url
     * @param request
     * @param action
     * @param responseType
     * @param uriVariables
     * @return {@link FluentResponse}
     */
    protected FluentResponse createEntity(final String url, final AbstractEntity request, final String action,
            final Class<? extends FluentResponse> responseType, final Object... uriVariables) {
        request.setRetailerId(getRetailerId());
        logJson(action, "request=", request);
        FluentResponse response;
        try {
            response = fluentRestTemplate.postForObject(fluentBaseUrl + url, request, responseType, uriVariables);
        }
        catch (final HttpStatusCodeException e) {
            response = handleHttpStatusCodeError(e, action, responseType);
        }
        catch (final RestClientException restException) {
            response = handleRestClientException(restException, action, responseType);
        }
        logJson(action, "response=", response);

        return response;
    }

    /**
     * Generic VIEW/GET entity
     * 
     * @param url
     * @param responseType
     * @param action
     * @param queryParams
     * @param uriVariables
     * @return {@link FluentResponse}
     */
    protected FluentResponse viewEntity(final String url, final Class<? extends FluentResponse> responseType,
            final String action, final Map<String, String> queryParams, final Object... uriVariables) {
        LOG.info(MessageFormat.format(LOG_TEMPLATE, action,
                "query=" + queryParams + ",vars=" + Arrays.asList(uriVariables)));

        final UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
        if (MapUtils.isNotEmpty(queryParams)) {
            for (final Entry<String, String> queryParam : queryParams.entrySet()) {
                builder.queryParam(queryParam.getKey(), queryParam.getValue());
            }
        }

        FluentResponse response;
        try {
            response = fluentRestTemplate.getForObject(
                    fluentBaseUrl + builder.build().expand(uriVariables).encode().toUriString(), responseType);
        }
        catch (final HttpStatusCodeException e) {
            response = handleHttpStatusCodeError(e, action, responseType);
        }
        catch (final RestClientException restException) {
            response = handleRestClientException(restException, action, responseType);
        }
        logJson(action, "response=", response);
        return response;
    }

    /**
     * Generic Update entity
     * 
     * @param url
     * @param request
     * @param responseType
     * @param action
     * @param uriVariables
     * @return {@link FluentResponse}
     */
    protected FluentResponse updateEntity(final String url, final AbstractEntity request,
            final Class<? extends FluentResponse> responseType,
            final String action, final Object... uriVariables) {
        request.setRetailerId(getRetailerId());
        logJson(action, "vars=" + Arrays.asList(uriVariables) + ",request=", request);
        FluentResponse response;
        try {
            response = fluentRestTemplate.exchange(fluentBaseUrl + url, HttpMethod.PUT,
                    new HttpEntity<Object>(request),
                    responseType, uriVariables).getBody();
        }
        catch (final HttpStatusCodeException e) {
            response = handleHttpStatusCodeError(e, action, responseType);
        }
        catch (final RestClientException restException) {
            response = handleRestClientException(restException, action, responseType);
        }
        logJson(action, "response=", response);
        return response;

    }

    /**
     * Generic SEARCH/GET entities
     * 
     * @param uri
     * @param responseType
     * @param action
     * @param queryParams
     * @param uriVariables
     * @return {@link FluentResponse}
     */
    protected FluentResponse searchEntities(final String uri, final Class<? extends FluentResponse> responseType,
            final String action, final Map<String, String> queryParams, final Object... uriVariables) {
        return viewEntity(uri, responseType, action, queryParams, uriVariables);
    }

    @Override
    public FluentResponse createJob(final Job job) {
        return createEntity(JOB, job, "createJob", FluentResponse.class, new Object[] { null });
    }

    @Override
    public FluentResponse viewJob(final String jobId) {
        return viewEntity(JOB, JobResponse.class, "viewJob", null, jobId);
    }

    @Override
    public FluentResponse createBatch(final String jobId, final Batch batch) {
        return createEntity(BATCH, batch, "createBatch", FluentResponse.class, jobId, null);
    }

    @Override
    public BatchResponse viewBatch(final String jobId, final String batchId, final Map<String, String> queryParams) {
        return (BatchResponse)viewEntity(BATCH, BatchResponse.class, "viewBatch", queryParams, jobId, batchId);
    }

    @Override
    public FluentResponse createLocation(final Location location) {
        return createEntity(LOCATION, location, "createLocation", FluentResponse.class, new Object[] { null });
    }

    @Override
    public FluentResponse updateLocation(final String locationId, final Location location) {
        return updateEntity(LOCATION, location, FluentResponse.class, "updateLocation", locationId);
    }


    @Override
    public FluentResponse createCategory(final Category category) {
        return createEntity(CATEGORY, category, "createCategory", FluentResponse.class, new Object[] { null });
    }

    @Override
    public FluentResponse updateCategory(final String fluentId, final Category category) {
        return updateEntity(CATEGORY, category, FluentResponse.class, "updateCategory", fluentId);
    }

    @Override
    public CategoryResponse searchCategory(final String categoryRef) {
        final Map<String, String> queryParams = new HashMap<>();
        queryParams.put(CATEGORY_REF, categoryRef);
        return (CategoryResponse)searchEntities(CATEGORY, CategoryResponse.class, "searchCategory", queryParams,
                new Object[] { null });
    }

    @Override
    public FluentResponse createProduct(final Product product) {
        return createEntity(PRODUCT, product, "createProduct", FluentResponse.class, new Object[] { null });
    }

    @Override
    public FluentResponse updateProduct(final String fluentId, final Product product) {
        return updateEntity(PRODUCT, product, FluentResponse.class, "updateProduct", fluentId);
    }

    @Override
    public ProductsResponse searchProduct(final String id) {
        final Map<String, String> queryParams = new HashMap<>();
        queryParams.put(PRODUCT_REF, id);
        return (ProductsResponse)searchEntities(PRODUCT, ProductsResponse.class, "searchProduct", queryParams,
                new Object[] { null });
    }

    @Override
    public FluentResponse createSku(final String productId, final Sku sku) {
        return createEntity(SKU, sku, "createSku", FluentResponse.class, productId, null);
    }

    @Override
    public FluentResponse updateSku(final String productId, final String skuId, final Sku sku) {
        return updateEntity(SKU, sku, FluentResponse.class, "updateSku", productId, skuId);
    }

    @Override
    public SkusResponse searchSkus(final String skuRef) {
        final Map<String, String> queryParams = new HashMap<>();
        queryParams.put(SKU_REF, skuRef);
        return (SkusResponse)searchEntities(SKU_SEARCH, SkusResponse.class, "searchSkus", queryParams);
    }

    @Override
    public FluentResponse createFulfilmentOption(final FulfilmentOptionRequest fulfilmentOptionRequest) {
        return createEntity(FULFILMENT_OPTION, fulfilmentOptionRequest, "createFulfilmentOption",
                FulfilmentOptionResponse.class, new Object[] { null });
    }

    @Override
    public FluentResponse createOrder(final Order createOrderRequest) {
        return createEntity(ORDER, createOrderRequest, CREATE_ORDER, FluentResponse.class, new Object[] { null });
    }

    @Override
    public FluentResponse viewOrder(final String fluentOrderId) {
        return viewEntity(ORDER, OrderResponse.class, "getOrder", null, fluentOrderId);

    }

    @Override
    public FulfilmentsResponse retrieveFulfilmentsByOrder(final String fluentOrderId) {
        return (FulfilmentsResponse)viewEntity(RETRIEVE_FULFILMENTS, FulfilmentsResponse.class,
                "retrieveFulfilmentsByOrder", null, fluentOrderId);
    }

    @Override
    public FluentResponse eventSync(final Event event) {
        event.setAccountId(getAccountId());
        event.setEntityRef(getEntityRef(event.getEntityRef()));
        event.setRootEntityRef(getEntityRef(event.getRootEntityRef()));
        return createEntity(EVENT, event, "eventSync", EventResponse.class, "sync");
    }

    private FluentResponse handleHttpStatusCodeError(final HttpStatusCodeException e, final String action,
            final Class<? extends FluentResponse> responseType) {
        try {
            return OBJECT_MAPPER.readValue(e.getResponseBodyAsByteArray(), responseType);
        }
        catch (final IOException e1) {
            LOG.error(MessageFormat.format(LOG_TEMPLATE, action, "error"), e1);
            final FluentResponse fluentResponse = new FluentResponse();
            final Error error = new Error();
            error.setCode(String.valueOf(e.getStatusCode().value()));
            error.setMessage(e.getStatusCode().getReasonPhrase());
            fluentResponse.setErrors(Arrays.asList(error));
            return fluentResponse;
        }
    }

    private FluentResponse handleRestClientException(final RestClientException e, final String action,
            final Class<? extends FluentResponse> responseType) {
        LOG.error(MessageFormat.format(LOG_TEMPLATE, action, "error"), e);
        FluentResponse fluentResponse;
        try {
            fluentResponse = responseType.newInstance();
        }
        catch (final InstantiationException | IllegalAccessException e1) {
            LOG.error(MessageFormat.format(LOG_TEMPLATE, action, "error"), e1);
            fluentResponse = new FluentResponse();
        }
        final Error error = new Error();
        error.setCode("RestClientException");
        error.setMessage(e.getMessage());
        fluentResponse.setErrors(Arrays.asList(error));
        return fluentResponse;
    }

    protected void logJson(final String action, final String message, final Object json) {
        String jsonString;
        try {
            OBJECT_MAPPER.setSerializationInclusion(Include.NON_NULL);
            jsonString = OBJECT_MAPPER.writeValueAsString(json);
            jsonString = maskPIIInformation(action, jsonString);
        }
        catch (final IOException e) {
            jsonString = e.getMessage();
        }
        LOG.info(MessageFormat.format(LOG_TEMPLATE, action, message + jsonString));
    }

    /**
     * @param jsonString
     * @return mask jsonString
     */
    protected String maskPIIInformation(final String action, String jsonString) {
        if (CREATE_ORDER.equals(action)) {
            for (final String blackListField : blacklistedFields) {

                if (StringUtils.contains(jsonString, blackListField)) {
                    jsonString = jsonString.replaceAll("(?<=" + blackListField + "\":\")(.*?)(?=\")", HASHED_VALUE);
                }
            }
        }
        return jsonString;
    }

    /**
     * @return retailerId
     */
    protected String getRetailerId() {
        final Map<String, Object> additionalInformation = ((OAuth2RestTemplate)getFluentRestTemplate())
                .getAccessToken()
                .getAdditionalInformation();
        final Object retailerId = additionalInformation.get(RETAILER_ID);
        return retailerId != null ? String.valueOf(retailerId) : null;
    }

    /**
     * @return accountId
     */
    protected String getAccountId() {
        final Matcher matcher = Pattern.compile("^https?://(\\w+)\\..+$").matcher(fluentBaseUrl);
        if (matcher.matches()) {
            return matcher.group(1);
        }
        return null;
    }

    /**
     * Replaces the template with {retailerId} with retailerId
     * 
     * @param entityRef
     * @return String
     */
    protected String getEntityRef(final String entityRef) {
        if (StringUtils.isNotEmpty(entityRef)) {
            final String formattedString = entityRef.replace(RETAILER_ID_TEMPLATE, getRetailerId());
            return formattedString;
        }
        return null;
    }

    /**
     * @param fluentBaseUrl
     *            the fluentBaseUrl to set
     */
    @Required
    public void setFluentBaseUrl(final String fluentBaseUrl) {
        this.fluentBaseUrl = fluentBaseUrl;
    }

    /**
     * @return the fluentRestTemplate
     */
    protected RestTemplate getFluentRestTemplate() {
        return fluentRestTemplate;
    }

    /**
     * @param fluentRestTemplate
     *            the fluentRestTemplate to set
     */
    @Required
    public void setFluentRestTemplate(final RestTemplate fluentRestTemplate) {
        this.fluentRestTemplate = fluentRestTemplate;
    }

    /**
     * @param blacklistedFields
     *            the blacklistedFields to set
     */
    @Required
    public void setBlacklistedFields(final List<String> blacklistedFields) {
        this.blacklistedFields = blacklistedFields;
    }

}
