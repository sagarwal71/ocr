/**
 * 
 */
package au.com.target.tgtfluent.util;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtfluent.constants.TgtFluentConstants.FluentFulfilmentStatus;


/**
 * @author bhuang3
 *
 */
public final class FluentDataMap {

    public static final List<String> SYSTEM_REFUND_STATUS = ImmutableList.of(FluentFulfilmentStatus.SYSTEM_REJECTED,
            FluentFulfilmentStatus.SYSTEM_EXPIRED);

    public static final List<String> SYSTEM_TERMINAL_STATUS = ImmutableList.of(FluentFulfilmentStatus.REJECTED,
            FluentFulfilmentStatus.SYSTEM_REJECTED,
            FluentFulfilmentStatus.SYSTEM_EXPIRED, FluentFulfilmentStatus.EXPIRED,
            FluentFulfilmentStatus.CANCELLED_NO_STOCK);

    private static final HashMap<String, ConsignmentStatus> CONSIGNMENT_STATUS_MAP = new HashMap<>();

    private FluentDataMap() {
        // no instantiation from external class
    }

    static {
        CONSIGNMENT_STATUS_MAP.put(FluentFulfilmentStatus.CREATED, ConsignmentStatus.CREATED);
        CONSIGNMENT_STATUS_MAP.put(FluentFulfilmentStatus.AWAITING_WAVE, ConsignmentStatus.SENT_TO_WAREHOUSE);
        CONSIGNMENT_STATUS_MAP.put(FluentFulfilmentStatus.SENT_TO_WAREHOUSE, ConsignmentStatus.SENT_TO_WAREHOUSE);
        CONSIGNMENT_STATUS_MAP.put(FluentFulfilmentStatus.CONFIRMED_BY_WAREHOUSE,
                ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        CONSIGNMENT_STATUS_MAP.put(FluentFulfilmentStatus.ASSIGNED, ConsignmentStatus.WAVED);
        CONSIGNMENT_STATUS_MAP.put(FluentFulfilmentStatus.PICK, ConsignmentStatus.WAVED);
        CONSIGNMENT_STATUS_MAP.put(FluentFulfilmentStatus.PACK, ConsignmentStatus.WAVED);
        CONSIGNMENT_STATUS_MAP.put(FluentFulfilmentStatus.FULFILLED, ConsignmentStatus.PICKED);
        CONSIGNMENT_STATUS_MAP.put(FluentFulfilmentStatus.PARTIALLY_FULFILLED, ConsignmentStatus.PICKED);
        CONSIGNMENT_STATUS_MAP.put(FluentFulfilmentStatus.SHIPPED, ConsignmentStatus.SHIPPED);
        CONSIGNMENT_STATUS_MAP.put(FluentFulfilmentStatus.REJECTED, ConsignmentStatus.CANCELLED);
        CONSIGNMENT_STATUS_MAP.put(FluentFulfilmentStatus.SYSTEM_REJECTED, ConsignmentStatus.CANCELLED);
        CONSIGNMENT_STATUS_MAP.put(FluentFulfilmentStatus.SYSTEM_EXPIRED, ConsignmentStatus.CANCELLED);
        CONSIGNMENT_STATUS_MAP.put(FluentFulfilmentStatus.EXPIRED, ConsignmentStatus.CANCELLED);
        CONSIGNMENT_STATUS_MAP.put(FluentFulfilmentStatus.CANCELLED_NO_STOCK, ConsignmentStatus.CANCELLED);
    }

    /**
     * Get all validated consignment status
     * 
     * @return Set<String>
     */
    public static Set<String> getAllAvailableConsignmentStatus() {
        return CONSIGNMENT_STATUS_MAP.keySet();
    }

    /**
     * @return the consignmentStatusMap
     */
    public static ConsignmentStatus getConsignmentStatus(final String status) {
        return CONSIGNMENT_STATUS_MAP.get(status);
    }

}
