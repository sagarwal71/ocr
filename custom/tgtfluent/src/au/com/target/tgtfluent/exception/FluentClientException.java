/**
 * 
 */
package au.com.target.tgtfluent.exception;

import au.com.target.tgtfluent.data.Error;


/**
 * @author mgazal
 *
 */
public class FluentClientException extends FluentBaseException {

    private final Error error;

    /**
     * @param error
     */
    public FluentClientException(final Error error) {
        super();
        this.error = error;
    }

    /**
     * @return the error
     */
    public Error getError() {
        return error;
    }

    @Override
    public String getMessage() {
        if (error != null) {
            return "code=" + error.getCode() + ", message=" + error.getMessage();
        }
        return null;
    }

}
