/**
 * 
 */
package au.com.target.tgtfluent.jobs;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtfluent.exception.FluentFeedException;
import au.com.target.tgtfluent.service.FluentFeedService;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class LocationFeedJobTest {

    @Mock
    private FluentFeedService fluentFeedService;

    @Mock
    private TargetPointOfServiceService targetPointOfServiceService;

    @InjectMocks
    private final LocationFeedJob locationFeedJob = new LocationFeedJob();

    @Test
    public void testPerform() {
        final List<TargetPointOfServiceModel> openStores = new ArrayList<>();
        final TargetPointOfServiceModel pos1 = mock(TargetPointOfServiceModel.class);
        openStores.add(pos1);
        given(targetPointOfServiceService.getAllStoresForFluent()).willReturn(openStores);
        final CronJobModel cronJob = mock(CronJobModel.class);

        final PerformResult result = locationFeedJob.perform(cronJob);
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
        verify(fluentFeedService).feedLocation(openStores);
    }

    @Test
    public void testPerformFail() {
        final List<TargetPointOfServiceModel> openStores = new ArrayList<>();
        final TargetPointOfServiceModel pos1 = mock(TargetPointOfServiceModel.class);
        openStores.add(pos1);
        given(targetPointOfServiceService.getAllStoresForFluent()).willReturn(openStores);
        final CronJobModel cronJob = mock(CronJobModel.class);
        willThrow(new FluentFeedException()).given(fluentFeedService).feedLocation(openStores);

        final PerformResult result = locationFeedJob.perform(cronJob);
        assertThat(result.getResult()).isEqualTo(CronJobResult.FAILURE);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }
}
