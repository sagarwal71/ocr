/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.apache.commons.collections.SetUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtfluent.client.FluentClient;
import au.com.target.tgtfluent.converter.populator.FluentConsignmentPopulator;
import au.com.target.tgtfluent.converter.populator.FluentConsignmentReversePopulator;
import au.com.target.tgtfluent.converter.populator.FluentConsignmentWarehouseReversePopulator;
import au.com.target.tgtfluent.converter.populator.factory.FluentConsignmentPopulatorFactory;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.data.Event;
import au.com.target.tgtfluent.data.EventResponse;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.data.Fulfilment;
import au.com.target.tgtfluent.data.FulfilmentsResponse;
import au.com.target.tgtfluent.exception.FluentBaseException;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.exception.FluentFulfilmentException;
import au.com.target.tgtfluent.service.FluentConsignmentService;
import au.com.target.tgtfluent.service.FluentFulfilmentValidator;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntryList;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentFulfilmentServiceImplTest {

    @InjectMocks
    @Spy
    private final FluentFulfilmentServiceImpl fluentFulfilmentServiceImpl = new FluentFulfilmentServiceImpl();

    @Mock
    private FluentClient fluentClient;

    @Mock
    private FluentConsignmentReversePopulator fluentConsignmentReversePopulator;

    @Mock
    private FluentConsignmentWarehouseReversePopulator fluentConsignmentWarehouseReversePopulator;

    @Mock
    private FluentConsignmentService fluentConsignmentService;

    @Mock
    private FluentFulfilmentValidator fluentFulfilmentValidator;

    @Mock
    private TargetOrderService targetOrderService;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;

    @Mock
    private FluentConsignmentPopulatorFactory fluentConsignmentPopulatorFactory;

    @Before
    public void setup() {
        final ArrayList<String> warehousesList = new ArrayList();
        warehousesList.add("FastlineWarehouse");
        warehousesList.add("IncommWarehouse");
        warehousesList.add("CnpWarehouse");
        fluentFulfilmentServiceImpl.setWarehousesList(warehousesList);
    }

    @Test(expected = FluentFulfilmentException.class)
    public void testCreateConsignmentsByOrderIdWithClientError() throws FluentClientException,
            FluentFulfilmentException {
        final OrderModel orderModel = mock(OrderModel.class);
        given(orderModel.getFluentId()).willReturn("fluentId");
        final FulfilmentsResponse response = new FulfilmentsResponse();
        final Error error = new Error();
        error.setCode("error");
        error.setMessage("error message");
        response.setErrors(ImmutableList.of(error));
        given(fluentClient.retrieveFulfilmentsByOrder("fluentId")).willReturn(response);
        fluentFulfilmentServiceImpl.createConsignmentsByOrderId(orderModel);
    }

    @Test(expected = FluentFulfilmentException.class)
    public void testCreateConsignmentsByOrderIdWithEmptyFulfilment() throws FluentClientException,
            FluentFulfilmentException {
        final OrderModel orderModel = mock(OrderModel.class);
        given(orderModel.getFluentId()).willReturn("fluentId");
        final FulfilmentsResponse response = new FulfilmentsResponse();
        given(fluentClient.retrieveFulfilmentsByOrder("fluentId")).willReturn(response);
        fluentFulfilmentServiceImpl.createConsignmentsByOrderId(orderModel);
    }

    @Test
    public void testCreateConsignmentsByOrderId() throws FluentClientException,
            FluentFulfilmentException {
        final OrderModel orderModel = mock(OrderModel.class);
        given(orderModel.getFluentId()).willReturn("fluentId");
        final FulfilmentsResponse response = new FulfilmentsResponse();
        final Fulfilment fulfilment = new Fulfilment();
        fulfilment.setFulfilmentId("1001");
        response.setFulfilments(ImmutableList.of(fulfilment));
        given(fluentClient.retrieveFulfilmentsByOrder("fluentId")).willReturn(response);
        final TargetConsignmentModel targetConsignmentModel = mock(TargetConsignmentModel.class);
        given(fluentConsignmentService.getOrCreateConsignmentForCode("1001")).willReturn(targetConsignmentModel);
        given(targetConsignmentModel.getOrder()).willReturn(orderModel);
        given(Boolean.valueOf(fluentFulfilmentValidator.validateFulfilment(fulfilment, targetConsignmentModel)))
                .willReturn(Boolean.TRUE);
        fluentFulfilmentServiceImpl.createConsignmentsByOrderId(orderModel);
        verify(fluentConsignmentWarehouseReversePopulator).populate(fulfilment, targetConsignmentModel);
        verify(fluentConsignmentReversePopulator).populate(fulfilment, targetConsignmentModel);
    }

    @Test
    public void testCreateTargetOrderCancelEntryList() throws FluentClientException,
            FluentFulfilmentException {
        final OrderModel orderModel = mock(OrderModel.class);
        final ConsignmentModel consignment1 = mock(ConsignmentModel.class);
        final WarehouseModel warehouse1 = mock(WarehouseModel.class);
        given(warehouse1.getCode()).willReturn(TgtCoreConstants.CANCELLATION_WAREHOUSE);
        given(consignment1.getWarehouse()).willReturn(warehouse1);

        final OrderEntryModel oe1 = mock(OrderEntryModel.class);
        final ConsignmentEntryModel ce1 = mock(ConsignmentEntryModel.class);
        given(ce1.getOrderEntry()).willReturn(oe1);
        final Long ce1qty = Long.valueOf(2l);
        given(ce1.getQuantity()).willReturn(ce1qty);

        final OrderEntryModel oe2 = mock(OrderEntryModel.class);
        final ConsignmentEntryModel ce2 = mock(ConsignmentEntryModel.class);
        given(ce2.getOrderEntry()).willReturn(oe2);
        final Long ce2qty = Long.valueOf(1l);
        given(ce2.getQuantity()).willReturn(ce2qty);

        given(consignment1.getConsignmentEntries()).willReturn(Sets.newHashSet(ce1, ce2));

        final ConsignmentModel consignment2 = mock(ConsignmentModel.class);
        final WarehouseModel warehouse2 = mock(WarehouseModel.class);
        given(warehouse2.getCode()).willReturn("testwarehouse");
        given(consignment2.getWarehouse()).willReturn(warehouse2);

        given(orderModel.getConsignments()).willReturn(Sets.newHashSet(consignment1, consignment2));
        final TargetOrderCancelEntryList targetOrderCancelEntryList = fluentFulfilmentServiceImpl
                .createTargetOrderCancelEntryList(orderModel);
        assertThat(targetOrderCancelEntryList.getEntriesToCancel()).hasSize(2);
        assertThat(targetOrderCancelEntryList.getEntriesToCancel()).onProperty("orderEntry").containsOnly(oe1, oe2);
        assertThat(targetOrderCancelEntryList.getEntriesToCancel()).onProperty("cancelQuantity").containsOnly(
                ce1qty, ce2qty);
    }

    @Test
    public void testIsAnyAssignedConsignment() throws FluentClientException,
            FluentFulfilmentException {
        final OrderModel orderModel = mock(OrderModel.class);
        final ConsignmentModel consignment1 = mock(ConsignmentModel.class);
        final WarehouseModel warehouse1 = mock(WarehouseModel.class);
        given(warehouse1.getCode()).willReturn(TgtCoreConstants.CANCELLATION_WAREHOUSE);
        given(consignment1.getWarehouse()).willReturn(warehouse1);

        final OrderEntryModel oe1 = mock(OrderEntryModel.class);
        final ConsignmentEntryModel ce1 = mock(ConsignmentEntryModel.class);
        given(ce1.getOrderEntry()).willReturn(oe1);
        final Long ce1qty = Long.valueOf(2l);
        given(ce1.getQuantity()).willReturn(ce1qty);

        final OrderEntryModel oe2 = mock(OrderEntryModel.class);
        final ConsignmentEntryModel ce2 = mock(ConsignmentEntryModel.class);
        given(ce2.getOrderEntry()).willReturn(oe2);
        final Long ce2qty = Long.valueOf(1l);
        given(ce2.getQuantity()).willReturn(ce2qty);

        given(consignment1.getConsignmentEntries()).willReturn(Sets.newHashSet(ce1, ce2));

        final ConsignmentModel consignment2 = mock(ConsignmentModel.class);
        final WarehouseModel warehouse2 = mock(WarehouseModel.class);
        given(warehouse2.getCode()).willReturn("testwarehouse");
        given(consignment2.getWarehouse()).willReturn(warehouse2);
        given(consignment2.getStatus()).willReturn(ConsignmentStatus.CREATED);

        given(orderModel.getConsignments()).willReturn(Sets.newHashSet(consignment1, consignment2));
        final boolean result = fluentFulfilmentServiceImpl.isAnyAssignedConsignment(orderModel);
        assertThat(result).isTrue();
    }

    @Test
    public void testIsAnyAssignedConsignmentIsFalse() throws FluentClientException,
            FluentFulfilmentException {
        final OrderModel orderModel = mock(OrderModel.class);
        final ConsignmentModel consignment1 = mock(ConsignmentModel.class);
        final WarehouseModel warehouse1 = mock(WarehouseModel.class);
        given(warehouse1.getCode()).willReturn(TgtCoreConstants.CANCELLATION_WAREHOUSE);
        given(consignment1.getWarehouse()).willReturn(warehouse1);

        final OrderEntryModel oe1 = mock(OrderEntryModel.class);
        final ConsignmentEntryModel ce1 = mock(ConsignmentEntryModel.class);
        given(ce1.getOrderEntry()).willReturn(oe1);
        final Long ce1qty = Long.valueOf(2l);
        given(ce1.getQuantity()).willReturn(ce1qty);

        final OrderEntryModel oe2 = mock(OrderEntryModel.class);
        final ConsignmentEntryModel ce2 = mock(ConsignmentEntryModel.class);
        given(ce2.getOrderEntry()).willReturn(oe2);
        final Long ce2qty = Long.valueOf(1l);
        given(ce2.getQuantity()).willReturn(ce2qty);

        given(consignment1.getConsignmentEntries()).willReturn(Sets.newHashSet(ce1, ce2));

        given(orderModel.getConsignments()).willReturn(Sets.newHashSet(consignment1));
        final boolean result = fluentFulfilmentServiceImpl.isAnyAssignedConsignment(orderModel);
        assertThat(result).isFalse();
    }

    @Test
    public void testIsAnyAssignedNullConsignments() {
        final OrderModel orderModel = mock(OrderModel.class);
        given(orderModel.getConsignments()).willReturn(Sets.newHashSet(SetUtils.EMPTY_SET));
        final boolean result = fluentFulfilmentServiceImpl.isAnyAssignedConsignment(orderModel);
        assertThat(result).isFalse();
    }

    @Test
    public void testIsAnyAssignedConsignmentCancelStatus() {
        final OrderModel orderModel = mock(OrderModel.class);
        final ConsignmentModel consignment1 = mock(ConsignmentModel.class);
        final WarehouseModel warehouse1 = mock(WarehouseModel.class);
        final ConsignmentModel consignment2 = mock(ConsignmentModel.class);
        final WarehouseModel warehouse2 = mock(WarehouseModel.class);
        given(warehouse1.getCode()).willReturn(TgtCoreConstants.FASTLINE_WAREHOUSE);
        given(warehouse1.getCode()).willReturn(TgtCoreConstants.CANCELLATION_WAREHOUSE);
        given(consignment1.getWarehouse()).willReturn(warehouse1);
        given(consignment2.getWarehouse()).willReturn(warehouse2);
        given(consignment1.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        given(consignment2.getStatus()).willReturn(ConsignmentStatus.CANCELLED);


        given(orderModel.getConsignments()).willReturn(Sets.newHashSet(consignment1));
        final boolean result = fluentFulfilmentServiceImpl.isAnyAssignedConsignment(orderModel);
        assertThat(result).isFalse();
    }

    @Test
    public void testUpsertFulfilment() throws FluentFulfilmentException {
        final Fulfilment fulfilment = mock(Fulfilment.class);
        willReturn("1001").given(fulfilment).getFulfilmentId();
        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        willReturn(mock(OrderModel.class)).given(consignment).getOrder();
        willReturn(consignment).given(fluentConsignmentService).getOrCreateConsignmentForCode("1001");
        willReturn(Boolean.TRUE).given(fluentFulfilmentValidator).validateStateTransition(fulfilment, consignment);

        fluentFulfilmentServiceImpl.upsertFulfilment(fulfilment);
        verify(consignment, never()).setOrder(any(AbstractOrderModel.class));
        verify(fluentConsignmentReversePopulator).populate(fulfilment, consignment);
        verify(modelService).save(consignment);
    }

    @Test
    public void testUpsertFulfilmentNew() throws FluentFulfilmentException {
        final Fulfilment fulfilment = mock(Fulfilment.class);
        willReturn("1001").given(fulfilment).getFulfilmentId();
        willReturn("fluentId").given(fulfilment).getOrderId();
        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        willReturn(consignment).given(fluentConsignmentService).getOrCreateConsignmentForCode("1001");
        final OrderModel order = mock(OrderModel.class);
        willReturn(order).given(targetOrderService).findOrderByFluentId("fluentId");
        willReturn(Boolean.TRUE).given(fluentFulfilmentValidator).validateStateTransition(fulfilment, consignment);

        fluentFulfilmentServiceImpl.upsertFulfilment(fulfilment);
        verify(consignment).setOrder(order);
        verify(fluentConsignmentReversePopulator).populate(fulfilment, consignment);
        verify(modelService).save(consignment);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpsertFulfilmentNewFail() throws FluentFulfilmentException {
        final Fulfilment fulfilment = mock(Fulfilment.class);
        willReturn("1001").given(fulfilment).getFulfilmentId();
        willReturn("fluentId").given(fulfilment).getOrderId();
        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        willReturn(consignment).given(fluentConsignmentService).getOrCreateConsignmentForCode("1001");
        willThrow(new UnknownIdentifierException("")).given(targetOrderService).findOrderByFluentId("fluentId");
        willReturn(Boolean.TRUE).given(fluentFulfilmentValidator).validateStateTransition(fulfilment, consignment);

        fluentFulfilmentServiceImpl.upsertFulfilment(fulfilment);
    }

    @Test
    public void testUpsertFulfilmentFalseTransition() throws FluentFulfilmentException {
        final Fulfilment fulfilment = mock(Fulfilment.class);
        willReturn("1001").given(fulfilment).getFulfilmentId();
        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        willReturn(consignment).given(fluentConsignmentService).getOrCreateConsignmentForCode("1001");
        willReturn(Boolean.FALSE).given(fluentFulfilmentValidator).validateStateTransition(fulfilment, consignment);

        fluentFulfilmentServiceImpl.upsertFulfilment(fulfilment);
        verify(fluentConsignmentReversePopulator, never()).populate(fulfilment, consignment);
        verify(modelService, never()).save(consignment);
    }

    @Test(expected = FluentFulfilmentException.class)
    public void testUpsertFulfilmentFailTransition() throws FluentFulfilmentException {
        final Fulfilment fulfilment = mock(Fulfilment.class);
        willReturn("1001").given(fulfilment).getFulfilmentId();
        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        willReturn(consignment).given(fluentConsignmentService).getOrCreateConsignmentForCode("1001");
        willThrow(new FluentFulfilmentException("")).given(fluentFulfilmentValidator)
                .validateStateTransition(fulfilment, consignment);

        fluentFulfilmentServiceImpl.upsertFulfilment(fulfilment);
    }



    @Test(expected = FluentFulfilmentException.class)
    public void testReadyForPickupFail() throws FluentBaseException {
        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        final EventResponse eventResponse = mock(EventResponse.class);
        willReturn("Failed, just failed!").given(eventResponse).getErrorMessage();
        willReturn(eventResponse).given(fluentClient).eventSync(any(Event.class));
        fluentFulfilmentServiceImpl.sendReadyForPickup(consignment);
    }

    @Test(expected = FluentClientException.class)
    public void testReadyForPickupError() throws FluentBaseException {
        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        final FluentResponse fluentResponse = mock(FluentResponse.class);
        willReturn(Arrays.asList(new Error())).given(fluentResponse).getErrors();
        willReturn(fluentResponse).given(fluentClient).eventSync(any(Event.class));
        fluentFulfilmentServiceImpl.sendReadyForPickup(consignment);
    }

    @Test(expected = FluentFulfilmentException.class)
    public void testSendConsignmentEventToFluentFail() throws FluentBaseException {
        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        final EventResponse eventResponse = mock(EventResponse.class);
        willReturn("Failed, just failed!").given(eventResponse).getErrorMessage();
        willReturn(eventResponse).given(fluentClient).eventSync(any(Event.class));
        fluentFulfilmentServiceImpl.sendConsignmentEventToFluent(consignment, "eventName", false);
    }

    @Test(expected = FluentClientException.class)
    public void testSendConsignmentEventToFluentError() throws FluentBaseException {
        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        willReturn(new EventResponse()).given(fluentClient).eventSync(any(Event.class));
        final FluentResponse fluentResponse = mock(FluentResponse.class);
        willReturn(Arrays.asList(new Error())).given(fluentResponse).getErrors();
        willReturn(fluentResponse).given(fluentClient).eventSync(any(Event.class));
        fluentFulfilmentServiceImpl.sendConsignmentEventToFluent(consignment, "eventName", false);
    }

    @Test
    public void testSendConsignmentEventToFluentSuccess() throws FluentBaseException {
        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        final ArgumentCaptor<Fulfilment> captor = ArgumentCaptor.forClass(Fulfilment.class);
        final ArgumentCaptor<Event> eventCaptor = ArgumentCaptor.forClass(Event.class);
        final FluentConsignmentPopulator fluentBaseConsignmentPopulator = mock(
                FluentConsignmentPopulator.class);
        given(fluentConsignmentPopulatorFactory.getConsignmentPopulator("eventName"))
                .willReturn(fluentBaseConsignmentPopulator);
        doNothing().when(fluentBaseConsignmentPopulator).populate(
                eq(consignment),
                captor.capture());
        willReturn(new EventResponse()).given(fluentClient).eventSync(eventCaptor.capture());

        fluentFulfilmentServiceImpl.sendConsignmentEventToFluent(consignment, "eventName", true);

        verify(fluentBaseConsignmentPopulator).populate(consignment,
                captor.getValue());
        verify(fluentClient).eventSync(eventCaptor.getValue());
        final Event event = eventCaptor.getValue();
        assertThat(event.getAttributes()).isNotNull().isEqualTo(captor.getValue());

    }

    @Test
    public void testReadyForPickupSuccess() throws FluentFulfilmentException {
        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        final EventResponse eventResponse = mock(EventResponse.class);
        willReturn(eventResponse).given(fluentClient).eventSync(any(Event.class));
        try {
            fluentFulfilmentServiceImpl.sendReadyForPickup(consignment);
        }
        catch (final Exception e) {
            fail("Should not have thrown any exception");
        }

    }

    @Test
    public void testStartFluentOrderBusinessProcesseWithStoreWarehouse() throws FluentFulfilmentException {

        final WarehouseModel warehouse = mock(WarehouseModel.class);
        final OrderModel orderModel = mock(OrderModel.class);
        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        given(warehouse.getCode()).willReturn("StoreWarehouse");
        willReturn(warehouse).given(consignment).getWarehouse();
        willReturn(orderModel).given(consignment).getOrder();
        given(consignment.getStatus()).willReturn(ConsignmentStatus.PICKED);
        doReturn(targetBusinessProcessService).when(fluentFulfilmentServiceImpl).getTargetBusinessProcessService();
        willReturn(orderModel).given(targetOrderService).findOrderByFluentId("456");
        fluentFulfilmentServiceImpl.startFluentOrderBusinessProcesses(consignment);
        verify(targetBusinessProcessService).startFluentOrderConsignmentPickedProcess(orderModel,
                consignment,
                "fluentConsignmentPickedProcess");

    }

    @Test
    public void testStartFluentOrderBusinessProcesseWithExternalWarehouse() throws FluentFulfilmentException {

        final WarehouseModel warehouse = mock(WarehouseModel.class);
        final OrderModel orderModel = mock(OrderModel.class);
        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        given(warehouse.getCode()).willReturn("FastlineWarehouse");
        willReturn(warehouse).given(consignment).getWarehouse();
        willReturn(orderModel).given(targetOrderService).findOrderByFluentId("456");
        given(consignment.getOrder()).willReturn(orderModel);
        doReturn(targetBusinessProcessService).when(fluentFulfilmentServiceImpl).getTargetBusinessProcessService();

        fluentFulfilmentServiceImpl.startFluentOrderBusinessProcesses(consignment);
        verify(targetBusinessProcessService, never()).startFluentOrderConsignmentPickedProcess(orderModel, consignment,
                "fluentConsignmentPickedProcess");
    }


    @Test
    public void testStartFluentConsignmentCancellationProcessForConsignmentWithCancellationWarehouse() {
        final OrderModel orderModel = mock(OrderModel.class);
        final TargetConsignmentModel consignment1 = mock(TargetConsignmentModel.class);
        final WarehouseModel warehouse1 = mock(WarehouseModel.class);
        given(warehouse1.getCode()).willReturn(TgtCoreConstants.CANCELLATION_WAREHOUSE);
        given(consignment1.getWarehouse()).willReturn(warehouse1);

        final OrderEntryModel oe1 = mock(OrderEntryModel.class);
        final ConsignmentEntryModel ce1 = mock(ConsignmentEntryModel.class);
        given(ce1.getOrderEntry()).willReturn(oe1);
        final Long ce1qty = Long.valueOf(2l);
        given(ce1.getQuantity()).willReturn(ce1qty);

        final OrderEntryModel oe2 = mock(OrderEntryModel.class);
        final ConsignmentEntryModel ce2 = mock(ConsignmentEntryModel.class);
        given(ce2.getOrderEntry()).willReturn(oe2);
        final Long ce2qty = Long.valueOf(1l);
        given(ce2.getQuantity()).willReturn(ce2qty);

        given(consignment1.getConsignmentEntries()).willReturn(Sets.newHashSet(ce1, ce2));
        given(consignment1.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        given(consignment1.getOrder()).willReturn(orderModel);
        doReturn(targetBusinessProcessService).when(fluentFulfilmentServiceImpl).getTargetBusinessProcessService();

        fluentFulfilmentServiceImpl.startFluentOrderBusinessProcesses(consignment1);
        verify(targetBusinessProcessService).startFluentOrderItemCancelProcess(any(OrderModel.class),
                any(TargetOrderCancelEntryList.class));
    }

    @Test
    public void testStartFluentConsignmentCancellationProcessAndNotCancellationWarehouse() {
        final OrderModel orderModel = mock(OrderModel.class);
        final TargetConsignmentModel consignment1 = mock(TargetConsignmentModel.class);
        final WarehouseModel warehouse1 = mock(WarehouseModel.class);
        given(warehouse1.getCode()).willReturn(TgtCoreConstants.FASTLINE_WAREHOUSE);
        given(consignment1.getWarehouse()).willReturn(warehouse1);

        given(consignment1.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(consignment1.getOrder()).willReturn(orderModel);
        doReturn(targetBusinessProcessService).when(fluentFulfilmentServiceImpl).getTargetBusinessProcessService();

        fluentFulfilmentServiceImpl.startFluentOrderBusinessProcesses(consignment1);
        verify(targetBusinessProcessService, never()).startFluentOrderItemCancelProcess(any(OrderModel.class),
                any(TargetOrderCancelEntryList.class));
    }

    @Test
    public void testStartFluentConsignmentCancellationProcessForConsignmentEmptyEntries() {
        final OrderModel orderModel = mock(OrderModel.class);
        final TargetConsignmentModel consignment1 = mock(TargetConsignmentModel.class);
        final WarehouseModel warehouse1 = mock(WarehouseModel.class);
        given(warehouse1.getCode()).willReturn(TgtCoreConstants.CANCELLATION_WAREHOUSE);
        given(consignment1.getWarehouse()).willReturn(warehouse1);

        given(consignment1.getConsignmentEntries()).willReturn(Collections.EMPTY_SET);
        given(consignment1.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        given(consignment1.getOrder()).willReturn(orderModel);
        doReturn(targetBusinessProcessService).when(fluentFulfilmentServiceImpl).getTargetBusinessProcessService();

        fluentFulfilmentServiceImpl.startFluentOrderBusinessProcesses(consignment1);
        verify(targetBusinessProcessService, never()).startFluentOrderItemCancelProcess(any(OrderModel.class),
                any(TargetOrderCancelEntryList.class));
    }

    @Test
    public void testStartFluentOrderBusinessProcesseWithStoreShipped() throws FluentFulfilmentException {

        final WarehouseModel warehouse = mock(WarehouseModel.class);
        final OrderModel orderModel = mock(OrderModel.class);
        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        given(warehouse.getCode()).willReturn("StoreWarehouse");
        willReturn(warehouse).given(consignment).getWarehouse();
        willReturn(orderModel).given(consignment).getOrder();
        given(consignment.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        doReturn(targetBusinessProcessService).when(fluentFulfilmentServiceImpl).getTargetBusinessProcessService();
        willReturn(orderModel).given(targetOrderService).findOrderByFluentId("456");
        fluentFulfilmentServiceImpl.startFluentOrderBusinessProcesses(consignment);
        verify(targetBusinessProcessService).startFluentOrderConsignmentShippedProcess(orderModel,
                consignment,
                "fluentConsignmentShippedProcess");

    }

    @Test
    public void testStartFluentOrderBusinessProcesseWithDCShipped() throws FluentFulfilmentException {

        final WarehouseModel warehouse = mock(WarehouseModel.class);
        final OrderModel orderModel = mock(OrderModel.class);
        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        given(warehouse.getCode()).willReturn("FastlineWarehouse");
        willReturn(warehouse).given(consignment).getWarehouse();
        willReturn(orderModel).given(targetOrderService).findOrderByFluentId("456");
        given(consignment.getOrder()).willReturn(orderModel);
        doReturn(targetBusinessProcessService).when(fluentFulfilmentServiceImpl).getTargetBusinessProcessService();

        fluentFulfilmentServiceImpl.startFluentOrderBusinessProcesses(consignment);
        verify(targetBusinessProcessService, never()).startFluentOrderConsignmentShippedProcess(orderModel, consignment,
                "fluentConsignmentShippedProcess");
    }
}
