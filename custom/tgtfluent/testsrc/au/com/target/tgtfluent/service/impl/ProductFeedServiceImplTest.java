/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willAnswer;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.product.dao.TargetProductDao;
import au.com.target.tgtfluent.model.ProductFluentUpdateStatusModel;
import au.com.target.tgtfluent.model.SkuFluentUpdateStatusModel;
import au.com.target.tgtfluent.service.FluentProductUpsertService;
import au.com.target.tgtfluent.service.FluentSkuUpsertService;
import au.com.target.tgtfluent.service.FluentUpdateStatusService;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductFeedServiceImplTest {

    private static final String BATCH_SIZE = "tgtfluent.productFeed.batchSize";

    private static final String SUB_BATCH_SIZE = "tgtfluent.productFeed.sub.batchSize";

    @Mock
    private ModelService modelService;

    @Mock
    private TargetSharedConfigService targetSharedConfigService;

    @Mock
    private TargetProductDao targetProductDao;

    @Mock
    private CatalogVersionService catalogVersionService;

    @Mock
    private FluentProductUpsertService fluentProductUpsertService;

    @Mock
    private FluentSkuUpsertService fluentSkuUpsertService;

    @Mock
    private FluentUpdateStatusService fluentUpdateStatusService;

    @InjectMocks
    @Spy
    private final ProductFeedServiceImpl productFeedServiceImpl = new ProductFeedServiceImpl();

    @Mock
    private CatalogVersionModel stagedVersion;

    @Before
    public void setup() {
        given(catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION)).willReturn(stagedVersion);
    }

    @Test
    public void testFeedProducts() {
        willReturn(Integer.valueOf(4)).given(targetSharedConfigService).getInt(BATCH_SIZE, 10000);
        willReturn(Integer.valueOf(2)).given(targetSharedConfigService).getInt(SUB_BATCH_SIZE, 100);

        final List<TargetProductModel> productsList1 = getProductList("P1", "P2");
        final List<TargetProductModel> productsList2 = getProductList("P3", "P4");

        setupMockUpdateStatus(Boolean.TRUE, "P1", "P2", "P3", "P4");

        final ArgumentCaptor<CatalogVersionModel> catalogVersionCaptor = ArgumentCaptor
                .forClass(CatalogVersionModel.class);
        final ArgumentCaptor<Integer> pageSizeCaptor = ArgumentCaptor.forClass(Integer.class);
        final ArgumentCaptor<List> erroredListCaptor = ArgumentCaptor.forClass(List.class);
        given(targetProductDao.findProductsNotSyncedToFluent(catalogVersionCaptor.capture(),
                pageSizeCaptor.capture().intValue(), erroredListCaptor.capture()))
                        .willAnswer(new Answer<List<TargetProductModel>>() {
                            private int count = 0;

                            @Override
                            public List<TargetProductModel> answer(final InvocationOnMock invocation) throws Throwable {
                                if (count++ == 0) {
                                    return productsList1;
                                }
                                return productsList2;
                            }
                        });

        productFeedServiceImpl.feedProducts();
        assertThat(catalogVersionCaptor.getAllValues()).containsExactly(stagedVersion, stagedVersion);
        assertThat(pageSizeCaptor.getAllValues()).containsExactly(Integer.valueOf(2), Integer.valueOf(2));
        assertThat(erroredListCaptor.getValue()).isEqualTo(Collections.emptyList());
        verify(fluentProductUpsertService).upsertProduct(productsList1.get(0));
        verify(fluentProductUpsertService).upsertProduct(productsList1.get(1));
        verify(fluentProductUpsertService).upsertProduct(productsList2.get(0));
        verify(fluentProductUpsertService).upsertProduct(productsList2.get(1));
        verifyNoMoreInteractions(fluentProductUpsertService);
        verify(productFeedServiceImpl).sendSkus(productsList1.get(0));
        verify(productFeedServiceImpl).sendSkus(productsList1.get(1));
        verify(productFeedServiceImpl).sendSkus(productsList2.get(0));
        verify(productFeedServiceImpl).sendSkus(productsList2.get(1));
    }

    @Test
    public void testFeedProductsWithErroredProducts() {
        willReturn(Integer.valueOf(4)).given(targetSharedConfigService).getInt(BATCH_SIZE, 10000);
        willReturn(Integer.valueOf(2)).given(targetSharedConfigService).getInt(SUB_BATCH_SIZE, 100);

        final List<TargetProductModel> productsList1 = getProductList("P1", "P2");
        final List<TargetProductModel> productsList2 = getProductList("P3", "P4");

        setupMockUpdateStatus(Boolean.FALSE, "P1");
        setupMockUpdateStatus(Boolean.TRUE, "P2", "P3", "P4");

        final ArgumentCaptor<CatalogVersionModel> catalogVersionCaptor = ArgumentCaptor
                .forClass(CatalogVersionModel.class);
        final ArgumentCaptor<Integer> pageSizeCaptor = ArgumentCaptor.forClass(Integer.class);
        final ArgumentCaptor<List> erroredListCaptor = ArgumentCaptor.forClass(List.class);
        given(targetProductDao.findProductsNotSyncedToFluent(catalogVersionCaptor.capture(),
                pageSizeCaptor.capture().intValue(), erroredListCaptor.capture()))
                        .willAnswer(new Answer<List<TargetProductModel>>() {
                            private int count = 0;

                            @Override
                            public List<TargetProductModel> answer(final InvocationOnMock invocation) throws Throwable {
                                if (count++ == 0) {
                                    return productsList1;
                                }
                                return productsList2;
                            }
                        });

        productFeedServiceImpl.feedProducts();
        assertThat(catalogVersionCaptor.getAllValues()).containsExactly(stagedVersion, stagedVersion);
        assertThat(pageSizeCaptor.getAllValues()).containsExactly(Integer.valueOf(2), Integer.valueOf(2));
        assertThat(erroredListCaptor.getValue()).isEqualTo(Arrays.asList("P1"));
        verify(fluentProductUpsertService).upsertProduct(productsList1.get(0));
        verify(fluentProductUpsertService).upsertProduct(productsList1.get(1));
        verify(fluentProductUpsertService).upsertProduct(productsList2.get(0));
        verify(fluentProductUpsertService).upsertProduct(productsList2.get(1));
        verify(productFeedServiceImpl, never()).sendSkus(productsList1.get(0));
        verify(productFeedServiceImpl).sendSkus(productsList1.get(1));
        verify(productFeedServiceImpl).sendSkus(productsList2.get(0));
        verify(productFeedServiceImpl).sendSkus(productsList2.get(1));
    }

    @Test
    public void testFeedProductsSecondFetchEmpty() {
        willReturn(Integer.valueOf(4)).given(targetSharedConfigService).getInt(BATCH_SIZE, 10000);
        willReturn(Integer.valueOf(2)).given(targetSharedConfigService).getInt(SUB_BATCH_SIZE, 100);

        final List<TargetProductModel> productsList1 = getProductList("P1", "P2");
        final List<TargetProductModel> productsList2 = new ArrayList<>();

        setupMockUpdateStatus(Boolean.TRUE, "P1", "P2");

        final ArgumentCaptor<CatalogVersionModel> catalogVersionCaptor = ArgumentCaptor
                .forClass(CatalogVersionModel.class);
        final ArgumentCaptor<Integer> pageSizeCaptor = ArgumentCaptor.forClass(Integer.class);
        final ArgumentCaptor<List> erroredListCaptor = ArgumentCaptor.forClass(List.class);
        given(targetProductDao.findProductsNotSyncedToFluent(catalogVersionCaptor.capture(),
                pageSizeCaptor.capture().intValue(), erroredListCaptor.capture()))
                        .willAnswer(new Answer<List<TargetProductModel>>() {
                            private int count = 0;

                            @Override
                            public List<TargetProductModel> answer(final InvocationOnMock invocation) throws Throwable {
                                if (count++ == 0) {
                                    return productsList1;
                                }
                                return productsList2;
                            }
                        });

        productFeedServiceImpl.feedProducts();
        assertThat(catalogVersionCaptor.getAllValues()).containsExactly(stagedVersion, stagedVersion);
        assertThat(pageSizeCaptor.getAllValues()).containsExactly(Integer.valueOf(2), Integer.valueOf(2));
        assertThat(erroredListCaptor.getValue()).isEqualTo(Collections.emptyList());
        verify(fluentProductUpsertService).upsertProduct(productsList1.get(0));
        verify(fluentProductUpsertService).upsertProduct(productsList1.get(1));
        verifyNoMoreInteractions(fluentProductUpsertService);
        verify(productFeedServiceImpl).sendSkus(productsList1.get(0));
        verify(productFeedServiceImpl).sendSkus(productsList1.get(1));

    }

    @Test
    public void testFeedProductsSecondFetchLessThanPageSize() {
        willReturn(Integer.valueOf(4)).given(targetSharedConfigService).getInt(BATCH_SIZE, 10000);
        willReturn(Integer.valueOf(2)).given(targetSharedConfigService).getInt(SUB_BATCH_SIZE, 100);

        final List<TargetProductModel> productsList1 = getProductList("P1", "P2");
        final List<TargetProductModel> productsList2 = getProductList("P3");

        setupMockUpdateStatus(Boolean.TRUE, "P1", "P2", "P3");

        final ArgumentCaptor<CatalogVersionModel> catalogVersionCaptor = ArgumentCaptor
                .forClass(CatalogVersionModel.class);
        final ArgumentCaptor<Integer> pageSizeCaptor = ArgumentCaptor.forClass(Integer.class);
        final ArgumentCaptor<List> erroredListCaptor = ArgumentCaptor.forClass(List.class);
        given(targetProductDao.findProductsNotSyncedToFluent(catalogVersionCaptor.capture(),
                pageSizeCaptor.capture().intValue(), erroredListCaptor.capture()))
                        .willAnswer(new Answer<List<TargetProductModel>>() {
                            private int count = 0;

                            @Override
                            public List<TargetProductModel> answer(final InvocationOnMock invocation) throws Throwable {
                                if (count++ == 0) {
                                    return productsList1;
                                }
                                return productsList2;
                            }
                        });

        productFeedServiceImpl.feedProducts();
        assertThat(catalogVersionCaptor.getAllValues()).containsExactly(stagedVersion, stagedVersion);
        assertThat(pageSizeCaptor.getAllValues()).containsExactly(Integer.valueOf(2), Integer.valueOf(2));
        assertThat(erroredListCaptor.getValue()).isEqualTo(Collections.emptyList());
        verify(fluentProductUpsertService).upsertProduct(productsList1.get(0));
        verify(fluentProductUpsertService).upsertProduct(productsList1.get(1));
        verify(fluentProductUpsertService).upsertProduct(productsList2.get(0));
        verifyNoMoreInteractions(fluentProductUpsertService);
        verify(productFeedServiceImpl).sendSkus(productsList1.get(0));
        verify(productFeedServiceImpl).sendSkus(productsList1.get(1));
        verify(productFeedServiceImpl).sendSkus(productsList2.get(0));
    }

    @Test
    public void testSkipOverErroredProducts() {
        final List<TargetProductModel> productsList = getProductList("P1", "P2");
        willThrow(new IllegalArgumentException()).given(fluentProductUpsertService).upsertProduct(productsList.get(0));
        setupMockUpdateStatus(Boolean.TRUE, "P2");
        final List<String> erroredProducts = new ArrayList<>();

        productFeedServiceImpl.sendProducts(productsList, erroredProducts);
        verify(fluentProductUpsertService).upsertProduct(productsList.get(0));
        verify(fluentUpdateStatusService, never()).getFluentUpdateStatus(ProductFluentUpdateStatusModel.class,
                ProductFluentUpdateStatusModel._TYPECODE, "P1");
        verify(productFeedServiceImpl, never()).sendSkus(productsList.get(0));
        verify(fluentProductUpsertService).upsertProduct(productsList.get(1));
        verify(fluentUpdateStatusService).getFluentUpdateStatus(ProductFluentUpdateStatusModel.class,
                ProductFluentUpdateStatusModel._TYPECODE, "P2");
        verify(productFeedServiceImpl).sendSkus(productsList.get(1));
        assertThat(erroredProducts).containsExactly("P1");
    }

    @Test
    public void testCollectSellableVariants() {
        final TargetProductModel productModel = mock(TargetProductModel.class);
        final Collection<VariantProductModel> firstLevelVariants = new ArrayList<>();
        final TargetColourVariantProductModel colourVariant1 = mock(TargetColourVariantProductModel.class);
        given(colourVariant1.getCode()).willReturn("C01");
        final Collection<VariantProductModel> secondLevelVariants = new ArrayList<>();
        final TargetSizeVariantProductModel sizeVariant1 = mock(TargetSizeVariantProductModel.class);
        given(sizeVariant1.getCode()).willReturn("S01");
        final SkuFluentUpdateStatusModel updateStatusS01 = mock(SkuFluentUpdateStatusModel.class);
        given(fluentUpdateStatusService.getFluentUpdateStatus(SkuFluentUpdateStatusModel.class,
                SkuFluentUpdateStatusModel._TYPECODE, "S01")).willReturn(updateStatusS01);
        secondLevelVariants.add(sizeVariant1);
        final TargetSizeVariantProductModel sizeVariant2 = mock(TargetSizeVariantProductModel.class);
        given(sizeVariant2.getCode()).willReturn("S02");
        secondLevelVariants.add(sizeVariant2);
        given(colourVariant1.getVariants()).willReturn(secondLevelVariants);
        firstLevelVariants.add(colourVariant1);
        final TargetColourVariantProductModel colourVariant2 = mock(TargetColourVariantProductModel.class);
        given(colourVariant2.getCode()).willReturn("C02");
        firstLevelVariants.add(colourVariant2);
        final TargetColourVariantProductModel colourVariant3 = mock(TargetColourVariantProductModel.class);
        given(colourVariant3.getCode()).willReturn("C03");
        firstLevelVariants.add(colourVariant3);
        final SkuFluentUpdateStatusModel updateStatusC03 = mock(SkuFluentUpdateStatusModel.class);
        willReturn(Boolean.TRUE).given(updateStatusC03).isLastRunSuccessful();
        given(fluentUpdateStatusService.getFluentUpdateStatus(SkuFluentUpdateStatusModel.class,
                SkuFluentUpdateStatusModel._TYPECODE, "C03")).willReturn(updateStatusC03);
        given(productModel.getVariants()).willReturn(firstLevelVariants);

        final List<AbstractTargetVariantProductModel> sellableVariants = new ArrayList<>();
        productFeedServiceImpl.collectSellableVariantsNotSyncedToFluent(productModel, sellableVariants);
        assertThat(sellableVariants).isNotNull().hasSize(3);
        assertThat(sellableVariants).containsExactly(sizeVariant1, sizeVariant2, colourVariant2);
    }

    @Test
    public void testSendSkus() {
        final List<AbstractTargetVariantProductModel> sellableVariants = new ArrayList<>();
        final TargetColourVariantProductModel sellableVariant1 = mock(TargetColourVariantProductModel.class);
        given(sellableVariant1.getCode()).willReturn("SV01");
        sellableVariants.add(sellableVariant1);
        final TargetSizeVariantProductModel sellableVariant2 = mock(TargetSizeVariantProductModel.class);
        given(sellableVariant2.getCode()).willReturn("SV02");
        sellableVariants.add(sellableVariant2);
        willAnswer(new Answer<Void>() {

            @Override
            public Void answer(final InvocationOnMock invocation) throws Throwable {
                ((List<AbstractTargetVariantProductModel>)invocation.getArguments()[1]).addAll(sellableVariants);
                return null;
            }
        }).given(productFeedServiceImpl).collectSellableVariantsNotSyncedToFluent(Mockito.any(ProductModel.class),
                Mockito.anyList());
        final TargetProductModel product = mock(TargetProductModel.class);

        productFeedServiceImpl.sendSkus(product);
        verify(fluentSkuUpsertService).upsertSku(sellableVariant1);
        verify(fluentUpdateStatusService).getFluentUpdateStatus(SkuFluentUpdateStatusModel.class,
                SkuFluentUpdateStatusModel._TYPECODE, "SV01");
        verify(fluentSkuUpsertService).upsertSku(sellableVariant2);
        verify(fluentUpdateStatusService).getFluentUpdateStatus(SkuFluentUpdateStatusModel.class,
                SkuFluentUpdateStatusModel._TYPECODE, "SV02");
        verifyNoMoreInteractions(fluentSkuUpsertService);
    }

    @Test
    public void testSkipOverErroredSkus() {
        final List<AbstractTargetVariantProductModel> sellableVariants = new ArrayList<>();
        final TargetColourVariantProductModel sellableVariant1 = mock(TargetColourVariantProductModel.class);
        given(sellableVariant1.getCode()).willReturn("SV01");
        sellableVariants.add(sellableVariant1);
        final TargetSizeVariantProductModel sellableVariant2 = mock(TargetSizeVariantProductModel.class);
        given(sellableVariant2.getCode()).willReturn("SV02");
        sellableVariants.add(sellableVariant2);
        willAnswer(new Answer<Void>() {

            @Override
            public Void answer(final InvocationOnMock invocation) throws Throwable {
                ((List<AbstractTargetVariantProductModel>)invocation.getArguments()[1]).addAll(sellableVariants);
                return null;
            }
        }).given(productFeedServiceImpl).collectSellableVariantsNotSyncedToFluent(Mockito.any(ProductModel.class),
                Mockito.anyList());
        willThrow(new IllegalArgumentException()).given(fluentSkuUpsertService).upsertSku(sellableVariant1);
        final TargetProductModel product = mock(TargetProductModel.class);

        productFeedServiceImpl.sendSkus(product);
        verify(fluentSkuUpsertService).upsertSku(sellableVariant1);
        verify(fluentUpdateStatusService, never()).getFluentUpdateStatus(SkuFluentUpdateStatusModel.class,
                SkuFluentUpdateStatusModel._TYPECODE, "SV01");
        verify(fluentSkuUpsertService).upsertSku(sellableVariant2);
        verify(fluentUpdateStatusService).getFluentUpdateStatus(SkuFluentUpdateStatusModel.class,
                SkuFluentUpdateStatusModel._TYPECODE, "SV02");
        verifyNoMoreInteractions(fluentSkuUpsertService);
    }

    /**
     * Sets up a mock list of products for given codes
     * 
     * @param codes
     * @return list of {@link TargetProductModel}
     */
    private List<TargetProductModel> getProductList(final String... codes) {
        final List<TargetProductModel> productsList = new ArrayList<>();
        for (int i = 0; i < codes.length; i++) {
            final TargetProductModel product = mock(TargetProductModel.class);
            given(product.getCode()).willReturn(codes[i]);
            productsList.add(product);
        }
        return productsList;
    }

    /**
     * Sets up mock update statuses for given product codes
     * 
     * @param lastRunSuccessful
     * @param codes
     */
    private void setupMockUpdateStatus(final Boolean lastRunSuccessful, final String... codes) {
        for (int i = 0; i < codes.length; i++) {
            final ProductFluentUpdateStatusModel updateStatus = mock(ProductFluentUpdateStatusModel.class);
            willReturn(lastRunSuccessful).given(updateStatus).isLastRunSuccessful();
            given(fluentUpdateStatusService.getFluentUpdateStatus(ProductFluentUpdateStatusModel.class,
                    ProductFluentUpdateStatusModel._TYPECODE, codes[i])).willReturn(updateStatus);
        }

    }

}
