/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.stock.strategy.StockLevelStatusStrategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtcore.stock.TargetStockLevelStatusStrategy;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtfluent.client.FluentClient;
import au.com.target.tgtfluent.data.AvailableToSell;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.data.FluentStock;
import au.com.target.tgtfluent.data.Fulfilment;
import au.com.target.tgtfluent.data.FulfilmentItem;
import au.com.target.tgtfluent.data.FulfilmentOptionAttribute;
import au.com.target.tgtfluent.data.FulfilmentOptionAttributeType;
import au.com.target.tgtfluent.data.FulfilmentOptionItem;
import au.com.target.tgtfluent.data.FulfilmentOptionRequest;
import au.com.target.tgtfluent.data.FulfilmentOptionResponse;
import au.com.target.tgtfluent.data.FulfilmentPlan;
import au.com.target.tgtfluent.data.StockAttribute;
import au.com.target.tgtfluent.data.StockDatum;
import au.com.target.tgtfluent.exception.FluentClientException;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentStockLookupServiceImplTest {

    @Mock
    private FluentClient fluentClient;

    @Mock
    private StockLevelStatusStrategy onlineStocklevelStatusStrategy;

    @Mock
    private TargetStockLevelStatusStrategy storeStockLevelStatusStrategy;

    @InjectMocks
    @Spy
    private final FluentStockLookupServiceImpl fluentStockLookupServiceImpl = new FluentStockLookupServiceImpl();

    @Before
    public void setup() {
        given(onlineStocklevelStatusStrategy.checkStatus(Mockito.any(StockLevelModel.class)))
                .willAnswer(new Answer<StockLevelStatus>() {

                    @Override
                    public StockLevelStatus answer(final InvocationOnMock invocation) throws Throwable {
                        final StockLevelModel stockLevelModel = (StockLevelModel)invocation.getArguments()[0];
                        if (stockLevelModel == null || stockLevelModel.getAvailable() == 0) {
                            return StockLevelStatus.OUTOFSTOCK;
                        }
                        if (stockLevelModel.getAvailable() > 0 && stockLevelModel.getAvailable() <= 10) {
                            return StockLevelStatus.LOWSTOCK;
                        }
                        return StockLevelStatus.INSTOCK;
                    }
                });
        given(storeStockLevelStatusStrategy.checkStatus("null")).willReturn(StockLevelStatus.OUTOFSTOCK);
        given(storeStockLevelStatusStrategy.checkStatus("20")).willReturn(StockLevelStatus.LOWSTOCK);
        given(storeStockLevelStatusStrategy.checkStatus("200")).willReturn(StockLevelStatus.INSTOCK);
    }

    @Test
    public void testLookupStock() throws FluentClientException {
        final FulfilmentOptionRequest request = mock(FulfilmentOptionRequest.class);
        willReturn(request).given(fluentStockLookupServiceImpl).createRequest("P01", null);
        final FulfilmentOptionResponse fluentResponse = mock(FulfilmentOptionResponse.class);
        given(fluentClient.createFulfilmentOption(request)).willReturn(fluentResponse);
        final Map<String, FluentStock> response = mock(Map.class);
        willReturn(response).given(fluentStockLookupServiceImpl).createProductResponse(fluentResponse);

        assertThat(fluentStockLookupServiceImpl.lookupStock("P01", null)).isEqualTo(response);
    }

    @Test(expected = FluentClientException.class)
    public void testLookupStockFail() throws FluentClientException {
        final FulfilmentOptionRequest request = mock(FulfilmentOptionRequest.class);
        willReturn(request).given(fluentStockLookupServiceImpl).createRequest("P01", null);
        final FulfilmentOptionResponse fluentResponse = mock(FulfilmentOptionResponse.class);
        given(fluentResponse.getErrors()).willReturn(Arrays.asList(new Error()));
        given(fluentClient.createFulfilmentOption(request)).willReturn(fluentResponse);

        fluentStockLookupServiceImpl.lookupStock("P01", null);
    }

    @Test
    public void testCreateProductRequest() {
        final FulfilmentOptionRequest request = fluentStockLookupServiceImpl.createRequest("P01", "L01");
        assertThat(request).isNotNull();
        assertThat(request.getLocationRef()).isEqualTo("L01");
        assertThat(request.getItems()).isNotEmpty().hasSize(1);
        assertThat(request.getItems().get(0).getSkuRef()).isEqualTo("P01");
        assertThat(request.getItems().get(0).getRequestedQuantity()).isEqualTo(1);
        assertThat(request.getAttributes()).isNotEmpty().hasSize(1);
        assertThat(request.getAttributes().get(0).getName()).isEqualTo("type");
        assertThat(request.getAttributes().get(0).getType()).isEqualTo(FulfilmentOptionAttributeType.STRING);
        assertThat(request.getAttributes().get(0).getValue()).isEqualTo("StockLookUpUsingProduct");
    }

    @Test
    public void testCreateProductResponse() {
        final FulfilmentOptionResponse fulfilmentOptionResponse = mock(FulfilmentOptionResponse.class);
        final List<FulfilmentOptionAttribute> attributes = new ArrayList<>();
        final FulfilmentOptionAttribute attribute = mock(FulfilmentOptionAttribute.class);
        final StockAttribute stockAttribute = mock(StockAttribute.class);
        given(stockAttribute.getSkuRef()).willReturn("V01");
        final AvailableToSell ats = mock(AvailableToSell.class);
        willReturn(Integer.valueOf(10)).given(ats).getCnc();
        willReturn(Integer.valueOf(10)).given(ats).getHd();
        willReturn(Integer.valueOf(10)).given(ats).getEd();
        given(stockAttribute.getAts()).willReturn(ats);
        willReturn(Integer.valueOf(20)).given(stockAttribute).getConsolidatedStock();
        willReturn(Integer.valueOf(10)).given(stockAttribute).getStoreStock();
        given(attribute.getValue()).willReturn(stockAttribute);
        attributes.add(attribute);
        given(fulfilmentOptionResponse.getAttributes()).willReturn(attributes);

        final Map<String, FluentStock> response = fluentStockLookupServiceImpl
                .createProductResponse(fulfilmentOptionResponse);
        assertThat(response).isNotEmpty().hasSize(1);
        assertThat(response.get("V01")).isNotNull();
        assertThat(response.get("V01").getAts().getStockLevel()).isEqualTo(10);
        assertThat(response.get("V01").getConsolidated().getStockLevel()).isEqualTo(20);
        assertThat(response.get("V01").getStore().getStockLevel()).isEqualTo(10);
    }

    @Test
    public void testLookupStockWithSkuCodes() throws FluentClientException {
        final FulfilmentOptionRequest request = mock(FulfilmentOptionRequest.class);
        willReturn(request).given(fluentStockLookupServiceImpl).createRequest(Arrays.asList("S01", "S02"));
        final FulfilmentOptionResponse fluentResponse = mock(FulfilmentOptionResponse.class);
        given(fluentClient.createFulfilmentOption(request)).willReturn(fluentResponse);
        final Map<String, Integer> response = mock(Map.class);
        willReturn(response).given(fluentStockLookupServiceImpl).createSkusResponse(fluentResponse);

        assertThat(fluentStockLookupServiceImpl.lookupStock(Arrays.asList("S01", "S02"))).isEqualTo(response);
    }

    @Test(expected = FluentClientException.class)
    public void testLookupStockWithSkuCodesFail() throws FluentClientException {
        final FulfilmentOptionRequest request = mock(FulfilmentOptionRequest.class);
        willReturn(request).given(fluentStockLookupServiceImpl).createRequest(Arrays.asList("S01", "S02"));
        final FulfilmentOptionResponse fluentResponse = mock(FulfilmentOptionResponse.class);
        given(fluentResponse.getErrors()).willReturn(Arrays.asList(new Error()));
        given(fluentClient.createFulfilmentOption(request)).willReturn(fluentResponse);

        fluentStockLookupServiceImpl.lookupStock(Arrays.asList("S01", "S02"));
    }

    @Test
    public void testCreateRequestForSkuCodes() {
        final FulfilmentOptionRequest request = fluentStockLookupServiceImpl.createRequest(Arrays.asList("S01", "S02"));
        assertThat(request).isNotNull();
        assertThat(request.getItems()).isNotEmpty().hasSize(2);
        assertThat(request.getItems()).onProperty("skuRef").containsExactly("S01", "S02");
        assertThat(request.getItems()).onProperty("requestedQuantity").containsExactly(Integer.valueOf(1),
                Integer.valueOf(1));
    }

    @Test
    public void testCreateResponseForSkuCodes() {
        final FulfilmentOptionResponse fulfilmentOptionResponse = mock(FulfilmentOptionResponse.class);
        final List<FulfilmentOptionItem> items = new ArrayList<>();
        FulfilmentOptionItem item = mock(FulfilmentOptionItem.class);
        given(item.getSkuRef()).willReturn("S01");
        willReturn(Integer.valueOf(10)).given(item).getAvailableQuantity();
        items.add(item);
        item = mock(FulfilmentOptionItem.class);
        given(item.getSkuRef()).willReturn("S02");
        willReturn(Integer.valueOf(20)).given(item).getAvailableQuantity();
        items.add(item);
        given(fulfilmentOptionResponse.getItems()).willReturn(items);

        final Map<String, Integer> response = fluentStockLookupServiceImpl.createSkusResponse(fulfilmentOptionResponse);
        assertThat(response).isNotEmpty().hasSize(2);
        assertThat(response.keySet()).contains("S01", "S02");
        assertThat(response.values()).contains(Integer.valueOf(10), Integer.valueOf(20));
    }

    @Test
    public void testCreateRequest() {
        final FulfilmentOptionRequest request = fluentStockLookupServiceImpl.createRequest(
                Arrays.asList("P1000_black_L", "P2000_green_L"),
                Arrays.asList("CC", "HD", "ED", "CONSOLIDATED_STORES_SOH"), Arrays.asList("5599", "5612"));
        assertThat(request).isNotNull();
        assertThat(request.getItems()).isNotEmpty().hasSize(2);
        assertThat(request.getItems()).onProperty("skuRef").containsExactly("P1000_black_L", "P2000_green_L");
        assertThat(request.getAttributes()).isNotEmpty().hasSize(2);
        assertThat(request.getAttributes()).onProperty("name").containsExactly("deliveryTypes", "locationRefs");
        assertThat(request.getAttributes()).onProperty("type").containsExactly(FulfilmentOptionAttributeType.ARRAY,
                FulfilmentOptionAttributeType.ARRAY);
        assertThat((List<String>)request.getAttributes().get(0).getValue()).containsExactly("CC", "HD", "ED",
                "CONSOLIDATED_STORES_SOH");
        assertThat((List<String>)request.getAttributes().get(1).getValue()).containsExactly("5599", "5612");
    }

    @Test
    public void testCreateResponse() {
        final FulfilmentOptionResponse fulfilmentOptionResponse = mock(FulfilmentOptionResponse.class);
        final List<FulfilmentPlan> plans = new ArrayList<>();
        given(fulfilmentOptionResponse.getPlans()).willReturn(plans);
        final FulfilmentPlan plan = mock(FulfilmentPlan.class);
        plans.add(plan);
        final List<Fulfilment> fulfilments = new ArrayList<>();
        given(plan.getFulfilments()).willReturn(fulfilments);

        final Fulfilment cc = mock(Fulfilment.class);
        given(cc.getFulfilmentType()).willReturn("CC");
        final List<FulfilmentItem> ccItems = new ArrayList<>();
        FulfilmentItem ccItem = mock(FulfilmentItem.class);
        given(ccItem.getSkuRef()).willReturn("P1000_black_L");
        given(ccItem.getAvailableQty()).willReturn(null);
        ccItems.add(ccItem);
        ccItem = mock(FulfilmentItem.class);
        given(ccItem.getSkuRef()).willReturn("P2000_green_L");
        given(ccItem.getAvailableQty()).willReturn(Integer.valueOf(10));
        ccItems.add(ccItem);
        given(cc.getItems()).willReturn(ccItems);
        fulfilments.add(cc);

        final Fulfilment hd = mock(Fulfilment.class);
        given(hd.getFulfilmentType()).willReturn("HD");
        final List<FulfilmentItem> hdItems = new ArrayList<>();
        FulfilmentItem hdItem = mock(FulfilmentItem.class);
        given(hdItem.getSkuRef()).willReturn("P1000_black_L");
        given(hdItem.getAvailableQty()).willReturn(Integer.valueOf(10));
        hdItems.add(hdItem);
        hdItem = mock(FulfilmentItem.class);
        given(hdItem.getSkuRef()).willReturn("P2000_green_L");
        given(hdItem.getAvailableQty()).willReturn(Integer.valueOf(100));
        hdItems.add(hdItem);
        given(hd.getItems()).willReturn(hdItems);
        fulfilments.add(hd);

        final Fulfilment ed = mock(Fulfilment.class);
        given(ed.getFulfilmentType()).willReturn("ED");
        final List<FulfilmentItem> edItems = new ArrayList<>();
        FulfilmentItem edItem = mock(FulfilmentItem.class);
        given(edItem.getSkuRef()).willReturn("P1000_black_L");
        given(edItem.getAvailableQty()).willReturn(Integer.valueOf(100));
        edItems.add(edItem);
        edItem = mock(FulfilmentItem.class);
        given(edItem.getSkuRef()).willReturn("P2000_green_L");
        given(edItem.getAvailableQty()).willReturn(Integer.valueOf(10));
        edItems.add(edItem);
        given(ed.getItems()).willReturn(edItems);
        fulfilments.add(ed);

        final Fulfilment consolidated = mock(Fulfilment.class);
        given(consolidated.getFulfilmentType()).willReturn("CONSOLIDATED_STORES_SOH");
        final List<FulfilmentItem> consolidatedItems = new ArrayList<>();
        FulfilmentItem consolidatedItem = mock(FulfilmentItem.class);
        given(consolidatedItem.getSkuRef()).willReturn("P1000_black_L");
        given(consolidatedItem.getAvailableQty()).willReturn(Integer.valueOf(10));
        consolidatedItems.add(consolidatedItem);
        consolidatedItem = mock(FulfilmentItem.class);
        given(consolidatedItem.getSkuRef()).willReturn("P2000_green_L");
        given(consolidatedItem.getAvailableQty()).willReturn(null);
        consolidatedItems.add(consolidatedItem);
        given(consolidated.getItems()).willReturn(consolidatedItems);
        fulfilments.add(consolidated);

        final Fulfilment store1 = mock(Fulfilment.class);
        given(store1.getFulfilmentType()).willReturn("STORE_SOH");
        given(store1.getLocationRef()).willReturn("5599");
        final List<FulfilmentItem> store1Items = new ArrayList<>();
        FulfilmentItem store1Item = mock(FulfilmentItem.class);
        given(store1Item.getSkuRef()).willReturn("P1000_black_L");
        given(store1Item.getAvailableQty()).willReturn(null);
        store1Items.add(store1Item);
        store1Item = mock(FulfilmentItem.class);
        given(store1Item.getSkuRef()).willReturn("P2000_green_L");
        given(store1Item.getAvailableQty()).willReturn(Integer.valueOf(20));
        store1Items.add(store1Item);
        given(store1.getItems()).willReturn(store1Items);
        fulfilments.add(store1);

        final Fulfilment store2 = mock(Fulfilment.class);
        given(store2.getFulfilmentType()).willReturn("STORE_SOH");
        given(store2.getLocationRef()).willReturn("5162");
        final List<FulfilmentItem> store2Items = new ArrayList<>();
        FulfilmentItem store2Item = mock(FulfilmentItem.class);
        given(store2Item.getSkuRef()).willReturn("P1000_black_L");
        given(store2Item.getAvailableQty()).willReturn(Integer.valueOf(20));
        store2Items.add(store2Item);
        store2Item = mock(FulfilmentItem.class);
        given(store2Item.getSkuRef()).willReturn("P2000_green_L");
        given(store2Item.getAvailableQty()).willReturn(Integer.valueOf(200));
        store2Items.add(store2Item);
        given(store2.getItems()).willReturn(store2Items);
        fulfilments.add(store2);

        final List<StockDatum> stockData = fluentStockLookupServiceImpl
                .createResponse(fulfilmentOptionResponse);
        assertThat(stockData).isNotEmpty().hasSize(2);
        assertThat(stockData).onProperty("variantCode").containsExactly("P1000_black_L", "P2000_green_L");
        assertThat(stockData).onProperty("atsCc").containsExactly(StockLevelStatus.OUTOFSTOCK,
                StockLevelStatus.LOWSTOCK);
        assertThat(stockData).onProperty("atsCcQty").containsExactly(null, Integer.valueOf(10));
        assertThat(stockData).onProperty("atsHd").containsExactly(StockLevelStatus.LOWSTOCK,
                StockLevelStatus.INSTOCK);
        assertThat(stockData).onProperty("atsHdQty").containsExactly(Integer.valueOf(10), Integer.valueOf(100));
        assertThat(stockData).onProperty("atsEd").containsExactly(StockLevelStatus.INSTOCK,
                StockLevelStatus.LOWSTOCK);
        assertThat(stockData).onProperty("atsEdQty").containsExactly(Integer.valueOf(100), Integer.valueOf(10));
        assertThat(stockData).onProperty("consolidatedStoreStock")
                .containsExactly(StockLevelStatus.INSTOCK, StockLevelStatus.OUTOFSTOCK);
        assertThat(stockData).onProperty("consolidatedStoreStockQty").containsExactly(Integer.valueOf(10), null);
        assertThat(stockData.get(0).getStoreSoh().get("5599")).isEqualTo(StockLevelStatus.OUTOFSTOCK);
        assertThat(stockData.get(0).getStoreSohQty().get("5599")).isEqualTo(null);
        assertThat(stockData.get(0).getStoreSoh().get("5162")).isEqualTo(StockLevelStatus.LOWSTOCK);
        assertThat(stockData.get(0).getStoreSohQty().get("5162")).isEqualTo(20);
        assertThat(stockData.get(1).getStoreSoh().get("5599")).isEqualTo(StockLevelStatus.LOWSTOCK);
        assertThat(stockData.get(1).getStoreSohQty().get("5599")).isEqualTo(20);
        assertThat(stockData.get(1).getStoreSoh().get("5162")).isEqualTo(StockLevelStatus.INSTOCK);
        assertThat(stockData.get(1).getStoreSohQty().get("5162")).isEqualTo(200);
    }

    @Test
    public void testPopulateStockDatum() {
        final FulfilmentPlan plan = mock(FulfilmentPlan.class);
        final Fulfilment cc = mock(Fulfilment.class);
        given(cc.getFulfilmentType()).willReturn("CC");
        final List<FulfilmentItem> ccItems = new ArrayList<>();
        FulfilmentItem ccItem = mock(FulfilmentItem.class);
        given(ccItem.getSkuRef()).willReturn("P1000_black_L");
        given(ccItem.getAvailableQty()).willReturn(null);
        ccItems.add(ccItem);
        ccItem = mock(FulfilmentItem.class);
        given(ccItem.getSkuRef()).willReturn("P2000_green_L");
        given(ccItem.getAvailableQty()).willReturn(Integer.valueOf(10));
        ccItems.add(ccItem);
        given(cc.getItems()).willReturn(ccItems);
        final Map<String, StockDatum> stockDataMap = new HashMap<>();

        fluentStockLookupServiceImpl.populateStockDatum(plan, cc, stockDataMap);
        assertThat(stockDataMap).hasSize(2);
        assertThat(stockDataMap.keySet()).containsOnly("P1000_black_L", "P2000_green_L");
        assertThat(stockDataMap.values()).onProperty("atsCc").containsOnly(StockLevelStatus.OUTOFSTOCK,
                StockLevelStatus.LOWSTOCK);
        assertThat(stockDataMap.values()).onProperty("atsCcQty").containsOnly(null, Integer.valueOf(10));
        assertThat(stockDataMap.values()).onProperty("atsHd").containsOnly(null, null);
        assertThat(stockDataMap.values()).onProperty("atsHdQty").containsOnly(null, null);
        assertThat(stockDataMap.values()).onProperty("atsPo").containsOnly(null, null);
        assertThat(stockDataMap.values()).onProperty("atsPoQty").containsOnly(null, null);
        assertThat(stockDataMap.values()).onProperty("atsEd").containsOnly(null, null);
        assertThat(stockDataMap.values()).onProperty("atsEdQty").containsOnly(null, null);
        assertThat(stockDataMap.values()).onProperty("consolidatedStoreStock").containsOnly(null, null);
        assertThat(stockDataMap.values()).onProperty("consolidatedStoreStockQty").containsOnly(null, null);
        assertThat(stockDataMap.values()).onProperty("storeSoh").containsOnly(null, null);
        assertThat(stockDataMap.values()).onProperty("storeSohQty").containsOnly(null, null);
    }


    @Test
    public void testPopulateStockDatumWithPO() {
        final FulfilmentPlan plan = mock(FulfilmentPlan.class);
        final Fulfilment po = mock(Fulfilment.class);
        given(po.getFulfilmentType()).willReturn("PO");
        final List<FulfilmentItem> poItems = new ArrayList<>();
        final FulfilmentItem poItem = mock(FulfilmentItem.class);
        given(poItem.getSkuRef()).willReturn("P2000_green_L");
        given(poItem.getAvailableQty()).willReturn(Integer.valueOf(20));
        poItems.add(poItem);
        given(po.getItems()).willReturn(poItems);
        final FulfilmentItem poItem1 = mock(FulfilmentItem.class);
        given(poItem1.getSkuRef()).willReturn("P2000_green_S");
        given(poItem1.getAvailableQty()).willReturn(Integer.valueOf(0));
        poItems.add(poItem1);
        given(po.getItems()).willReturn(poItems);
        final Map<String, StockDatum> stockDataMap = new HashMap<>();

        fluentStockLookupServiceImpl.populateStockDatum(plan, po, stockDataMap);
        assertThat(stockDataMap).hasSize(2);
        assertThat(stockDataMap.keySet()).containsOnly("P2000_green_L", "P2000_green_S");
        assertThat(stockDataMap.values()).onProperty("atsPo").containsOnly(StockLevelStatus.INSTOCK,
                StockLevelStatus.OUTOFSTOCK);
        assertThat(stockDataMap.values()).onProperty("atsCcQty").containsOnly(null, null);
        assertThat(stockDataMap.values()).onProperty("atsHd").containsOnly(null, null);
        assertThat(stockDataMap.values()).onProperty("atsHdQty").containsOnly(null, null);
        assertThat(stockDataMap.values()).onProperty("atsPoQty").containsOnly(Integer.valueOf(20), Integer.valueOf(0));
        assertThat(stockDataMap.values()).onProperty("atsEd").containsOnly(null, null);
        assertThat(stockDataMap.values()).onProperty("atsEdQty").containsOnly(null, null);
        assertThat(stockDataMap.values()).onProperty("consolidatedStoreStock").containsOnly(null, null);
        assertThat(stockDataMap.values()).onProperty("consolidatedStoreStockQty").containsOnly(null, null);
        assertThat(stockDataMap.values()).onProperty("storeSoh").containsOnly(null, null);
        assertThat(stockDataMap.values()).onProperty("storeSohQty").containsOnly(null, null);
    }


    @Test
    public void testNewLookupStock() throws FluentClientException {
        final FulfilmentOptionRequest fulfilmentOptionRequest = mock(FulfilmentOptionRequest.class);
        willReturn(fulfilmentOptionRequest).given(fluentStockLookupServiceImpl).createRequest(Mockito.anyCollection(),
                Mockito.anyCollection(), Mockito.anyCollection());
        final FulfilmentOptionResponse fluentResponse = mock(FulfilmentOptionResponse.class);
        willReturn(fluentResponse).given(fluentClient).createFulfilmentOption(fulfilmentOptionRequest);
        final List<StockDatum> expectedStockData = mock(List.class);
        willReturn(expectedStockData).given(fluentStockLookupServiceImpl).createResponse(fluentResponse);
        final Collection<StockDatum> stockData = fluentStockLookupServiceImpl.lookupStock(Mockito.anyCollection(),
                Mockito.anyCollection(), Mockito.anyCollection());

        assertThat(stockData).isEqualTo(expectedStockData);
    }

    @Test(expected = FluentClientException.class)
    public void testNewLookupStockFail() throws FluentClientException {
        final FulfilmentOptionRequest fulfilmentOptionRequest = mock(FulfilmentOptionRequest.class);
        willReturn(fulfilmentOptionRequest).given(fluentStockLookupServiceImpl).createRequest(Mockito.anyList(),
                Mockito.anyList(), Mockito.anyList());
        final FulfilmentOptionResponse fluentResponse = mock(FulfilmentOptionResponse.class);
        willReturn(Arrays.asList(new Error())).given(fluentResponse).getErrors();
        willReturn(fluentResponse).given(fluentClient).createFulfilmentOption(fulfilmentOptionRequest);
        final List<StockDatum> expectedStockData = mock(List.class);
        willReturn(expectedStockData).given(fluentStockLookupServiceImpl).createResponse(fluentResponse);
        fluentStockLookupServiceImpl.lookupStock(Mockito.anyCollection(), Mockito.anyCollection(),
                Mockito.anyCollection());
    }

    @Test
    public void testLookupAts() throws FluentClientException {
        final StockDatum stockDatum1 = mock(StockDatum.class);
        willReturn("P1000_red_L").given(stockDatum1).getVariantCode();
        willReturn(Integer.valueOf(10)).given(stockDatum1).getAtsHdQty();
        final StockDatum stockDatum2 = mock(StockDatum.class);
        willReturn("P1000_green_M").given(stockDatum2).getVariantCode();
        willReturn(Integer.valueOf(10)).given(stockDatum2).getAtsCcQty();
        willReturn(Integer.valueOf(11)).given(stockDatum2).getAtsHdQty();
        willReturn(Integer.valueOf(12)).given(stockDatum2).getAtsEdQty();
        willReturn(Arrays.asList(stockDatum1, stockDatum2)).given(fluentStockLookupServiceImpl).lookupStock(
                Arrays.asList("P1000_red_L", "P1000_green_M"),
                Arrays.asList(TgtCoreConstants.DELIVERY_TYPE.CC, TgtCoreConstants.DELIVERY_TYPE.HD,
                        TgtCoreConstants.DELIVERY_TYPE.ED, TgtCoreConstants.DELIVERY_TYPE.PO),
                null);

        final Map<String, Integer> stockMap = fluentStockLookupServiceImpl.lookupAts("P1000_red_L", "P1000_green_M");
        assertThat(stockMap).isNotEmpty().hasSize(2);
        assertThat(stockMap.keySet()).containsOnly("P1000_red_L", "P1000_green_M");
        assertThat(stockMap.values()).containsOnly(Integer.valueOf(10), Integer.valueOf(12));
    }

    @Test(expected = FluentClientException.class)
    public void testLookupAtsFail() throws FluentClientException {
        willThrow(new FluentClientException(new Error())).given(fluentStockLookupServiceImpl).lookupStock(
                Arrays.asList("P1000_red_L", "P1000_green_M"),
                Arrays.asList(TgtCoreConstants.DELIVERY_TYPE.CC, TgtCoreConstants.DELIVERY_TYPE.HD,
                        TgtCoreConstants.DELIVERY_TYPE.ED, TgtCoreConstants.DELIVERY_TYPE.PO),
                null);

        fluentStockLookupServiceImpl.lookupAts("P1000_red_L", "P1000_green_M");
    }

    @Test
    public void testGetProductOnlineStockStatus() throws FluentClientException {
        final Map<String, Integer> stockMap = new HashMap<>();
        stockMap.put("P1000_red_L", Integer.valueOf(11));
        willReturn(stockMap).given(fluentStockLookupServiceImpl).lookupAts("P1000_red_L");

        assertThat(fluentStockLookupServiceImpl.getProductOnlineStockStatus("P1000_red_L"))
                .isEqualTo(StockLevelStatus.INSTOCK);
    }

    @Test
    public void testGetProductOnlineStockStatusException() throws FluentClientException {
        willThrow(new FluentClientException(new Error())).given(fluentStockLookupServiceImpl).lookupAts("P1000_red_L");

        assertThat(fluentStockLookupServiceImpl.getProductOnlineStockStatus("P1000_red_L"))
                .isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    @Test
    public void testLookupPlaceOrderAts() throws FluentClientException {
        final StockDatum stockDatum1 = mock(StockDatum.class);
        willReturn("P1000_red_L").given(stockDatum1).getVariantCode();
        willReturn(Integer.valueOf(10)).given(stockDatum1).getAtsCcQty();
        final StockDatum stockDatum2 = mock(StockDatum.class);
        willReturn("P1000_green_M").given(stockDatum2).getVariantCode();
        willReturn(Integer.valueOf(0)).given(stockDatum2).getAtsCcQty();
        willReturn(Arrays.asList(stockDatum1, stockDatum2)).given(fluentStockLookupServiceImpl).lookupStock(
                Arrays.asList("P1000_red_L", "P1000_green_M"),
                Arrays.asList(TgtCoreConstants.DELIVERY_TYPE.CC), null);

        final Map<String, Integer> stockMap = fluentStockLookupServiceImpl.lookupPlaceOrderAts(
                Arrays.asList("P1000_red_L", "P1000_green_M"), TgtCoreConstants.DELIVERY_TYPE.CC);
        assertThat(stockMap).hasSize(2);
        assertThat(stockMap.keySet()).containsOnly("P1000_red_L", "P1000_green_M");
        assertThat(stockMap.values()).containsOnly(Integer.valueOf(10), Integer.valueOf(0));
    }

    @Test(expected = FluentOrderException.class)
    public void testLookupPlaceOrderAtsException() throws FluentClientException {
        willThrow(new FluentClientException(new Error())).given(fluentStockLookupServiceImpl).lookupStock(
                Arrays.asList("P1000_red_L", "P1000_green_M"),
                Arrays.asList(TgtCoreConstants.DELIVERY_TYPE.CC), null);

        fluentStockLookupServiceImpl.lookupPlaceOrderAts(Arrays.asList("P1000_red_L", "P1000_green_M"),
                TgtCoreConstants.DELIVERY_TYPE.CC);
    }

    /**
     * Method to verify atsPoQty for pre-order items in the basket(where atsPoQty provided for delivery type PO)
     *
     * @throws FluentClientException
     */
    @Test
    public void testLookupAtsForPreOrderWithPoDeliveryType() throws FluentClientException {
        final StockDatum stockDatum = mock(StockDatum.class);
        willReturn("V1111_preOrder_1").given(stockDatum).getVariantCode();
        willReturn(Integer.valueOf(0)).given(stockDatum).getAtsCcQty();
        willReturn(Integer.valueOf(0)).given(stockDatum).getAtsHdQty();
        willReturn(Integer.valueOf(0)).given(stockDatum).getAtsEdQty();
        willReturn(Integer.valueOf(100)).given(stockDatum).getAtsPoQty();
        willReturn(Arrays.asList(stockDatum)).given(fluentStockLookupServiceImpl).lookupStock(
                Arrays.asList("V1111_preOrder_1"),
                Arrays.asList(TgtCoreConstants.DELIVERY_TYPE.CC, TgtCoreConstants.DELIVERY_TYPE.HD,
                        TgtCoreConstants.DELIVERY_TYPE.ED, TgtCoreConstants.DELIVERY_TYPE.PO),
                null);

        final Map<String, Integer> stockMap = fluentStockLookupServiceImpl.lookupAts("V1111_preOrder_1");
        assertThat(stockMap).isNotEmpty().hasSize(1);
        assertThat(stockMap.keySet()).containsOnly("V1111_preOrder_1");
        assertThat(stockMap.values()).contains(Integer.valueOf(100));
    }

    /**
     * Method to verify atsPoQty for pre-order items in the basket(where atsEdQty provided for delivery type ED)
     *
     * @throws FluentClientException
     */
    @Test
    public void testLookupAtsForPreOrderWithEdDeliveryType() throws FluentClientException {
        final StockDatum stockDatum = mock(StockDatum.class);
        willReturn("V1111_preOrder_1").given(stockDatum).getVariantCode();
        willReturn(Integer.valueOf(0)).given(stockDatum).getAtsCcQty();
        willReturn(Integer.valueOf(0)).given(stockDatum).getAtsHdQty();
        willReturn(Integer.valueOf(10)).given(stockDatum).getAtsEdQty();
        willReturn(Integer.valueOf(0)).given(stockDatum).getAtsPoQty();
        willReturn(Arrays.asList(stockDatum)).given(fluentStockLookupServiceImpl).lookupStock(
                Arrays.asList("V1111_preOrder_1"),
                Arrays.asList(TgtCoreConstants.DELIVERY_TYPE.CC, TgtCoreConstants.DELIVERY_TYPE.HD,
                        TgtCoreConstants.DELIVERY_TYPE.ED, TgtCoreConstants.DELIVERY_TYPE.PO),
                null);
        final Map<String, Integer> stockMap = fluentStockLookupServiceImpl.lookupAts("V1111_preOrder_1");
        assertThat(stockMap).isNotEmpty().hasSize(1);
        assertThat(stockMap.keySet()).containsOnly("V1111_preOrder_1");
        assertThat(stockMap.values()).contains(Integer.valueOf(10));
    }

    /**
     * Method to verify exceptional scenario for pre-order items in basket.
     *
     * @throws FluentClientException
     */
    @Test(expected = FluentClientException.class)
    public void testLookupAtsFailForPreOrder() throws FluentClientException {
        willThrow(new FluentClientException(new Error())).given(fluentStockLookupServiceImpl).lookupStock(
                Arrays.asList("V1111_preOrder_1"),
                Arrays.asList(TgtCoreConstants.DELIVERY_TYPE.CC, TgtCoreConstants.DELIVERY_TYPE.HD,
                        TgtCoreConstants.DELIVERY_TYPE.ED, TgtCoreConstants.DELIVERY_TYPE.PO),
                null);
        fluentStockLookupServiceImpl.lookupAts("V1111_preOrder_1");
    }

    @Test
    public void testLookUpStockStore() throws Exception {
        final StockDatum stockDatum = mock(StockDatum.class);
        given(stockDatum.getVariantCode()).willReturn("P1000");
        final Map<String, Integer> storeSohQty = new LinkedHashMap<>();
        storeSohQty.put("7001", Integer.valueOf(10));
        storeSohQty.put("7002", Integer.valueOf(1));
        storeSohQty.put("7003", Integer.valueOf(0));
        given(stockDatum.getStoreSohQty()).willReturn(storeSohQty);

        final StockDatum stockDatum1 = mock(StockDatum.class);
        given(stockDatum1.getVariantCode()).willReturn("P1001");
        final Map<String, Integer> storeSohQty1 = new LinkedHashMap<>();
        storeSohQty1.put("8001", Integer.valueOf(20));
        given(stockDatum1.getStoreSohQty()).willReturn(storeSohQty1);

        final List<String> skuRefs = Arrays.asList("P1000", "P1001");
        final List<String> locations = Arrays.asList("5032");
        willReturn(Arrays.asList(stockDatum, stockDatum1)).given(fluentStockLookupServiceImpl).lookupStock(skuRefs,
                Collections.<String> emptyList(), locations);

        final StockVisibilityItemLookupResponseDto responseDto = fluentStockLookupServiceImpl.lookupStock(skuRefs,
                locations);
        assertThat(responseDto).isNotNull();
        assertThat(responseDto.getItems()).isNotEmpty().hasSize(4);
        assertThat(responseDto.getItems()).onProperty("code").containsExactly("P1000", "P1000", "P1000", "P1001");
        assertThat(responseDto.getItems()).onProperty("storeNumber").containsExactly("7001", "7002", "7003", "8001");
        assertThat(responseDto.getItems()).onProperty("soh").containsExactly("10", "1", "0", "20");

    }

    @Test
    public void testLookUpStockStoreWhenEmptyStock() throws Exception {

        final List<String> skuRefs = Arrays.asList("P1000");
        final List<String> locations = Arrays.asList("5032");
        willReturn(Collections.emptyList()).given(fluentStockLookupServiceImpl).lookupStock(skuRefs,
                Collections.<String> emptyList(), locations);

        final StockVisibilityItemLookupResponseDto responseDto = fluentStockLookupServiceImpl.lookupStock(skuRefs,
                locations);
        assertThat(responseDto).isNull();

    }
}
