package au.com.target.tgtfluent.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.constants.TgtbusprocConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.order.FluentErrorException;
import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtfluent.client.FluentClient;
import au.com.target.tgtfluent.constants.TgtFluentConstants;
import au.com.target.tgtfluent.converter.populator.OrderPopulator;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.data.Event;
import au.com.target.tgtfluent.data.EventResponse;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.data.Order;
import au.com.target.tgtfluent.data.OrderResponse;
import au.com.target.tgtfluent.service.FluentOrderValidator;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentOrderServiceImplTest {
    private static final String CANCEL_ORDER_FAILED_IN_FLUENT = "The order is not able to be cancelled.";
    private static final String CANCEL_ORDER_FAILED_SYSTEM_ERROR = "The order was not able to be cancelled due to a system issue. Please retry.";


    @Mock
    private OrderPopulator orderPopulator;

    @Mock
    private FluentClient fluentClient;

    @Mock
    private OrderModel orderModel;

    @Mock
    private Order order;

    @Mock
    private FluentResponse fluentResponse;

    @Mock
    private TargetOrderService targetOrderService;

    @Mock
    private FluentOrderValidator fluentOrderValidator;

    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @InjectMocks
    @Spy
    private final FluentOrderServiceImpl fluentOrderService = new FluentOrderServiceImpl();


    @Test(expected = FluentOrderException.class)
    public void testCreateOrderFailureEmptyOrderModel() {
        fluentOrderService.createOrder(orderModel);
        verify(orderPopulator).populate(any(OrderModel.class), any(Order.class));
        verifyZeroInteractions(fluentClient);
    }

    @Test
    public void testCreateOrderSuccess() {
        given(fluentClient.createOrder(any(Order.class))).willReturn(fluentResponse);
        given(fluentResponse.getId()).willReturn("123");
        fluentOrderService.createOrder(orderModel);
        verify(orderPopulator).populate(any(OrderModel.class), any(Order.class));
        verify(fluentClient).createOrder(any(Order.class));
    }

    @Test(expected = FluentOrderException.class)
    public void testCreateOrderWithErrorsFromFluent() {
        final List<Error> errors = new ArrayList<>();
        final Error error = mock(Error.class);
        given(error.getCode()).willReturn("500");
        given(error.getMessage()).willReturn("Status 500 Error");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.createOrder(any(Order.class))).willReturn(fluentResponse);
        fluentOrderService.createOrder(orderModel);

        verify(orderPopulator).populate(any(OrderModel.class), any(Order.class));
        verify(fluentClient).createOrder(any(Order.class));
    }

    @Test(expected = FluentOrderException.class)
    public void testCreateOrderWithEmptyFluentId() {
        given(fluentClient.createOrder(any(Order.class))).willReturn(fluentResponse);
        fluentOrderService.createOrder(orderModel);
        verify(orderPopulator).populate(any(OrderModel.class), any(Order.class));
        verify(fluentClient).createOrder(any(Order.class));
    }

    @Test
    public void testCancelOrder() {
        willReturn("5623").given(orderModel).getFluentId();
        final ArgumentCaptor<Event> eventCaptor = ArgumentCaptor.forClass(Event.class);
        willReturn(new EventResponse()).given(fluentClient).eventSync(eventCaptor.capture());

        fluentOrderService.cancelOrder(orderModel);
        final Event event = eventCaptor.getValue();
        assertThat(event.getName()).isEqualTo(TgtFluentConstants.Event.CANCEL_ORDER);
        assertThat(event.getEntityId()).isEqualTo("5623");
        assertThat(event.getEntityType()).isEqualTo(TgtFluentConstants.EntityType.ORDER);
        assertThat(event.getEntitySubtype()).isEqualTo(TgtFluentConstants.EntitySubtype.HD);
    }

    @Test(expected = FluentOrderException.class)
    public void testCancelOrderFail() {
        final EventResponse eventResponse = mock(EventResponse.class);
        willReturn("Failed, just failed!").given(eventResponse).getErrorMessage();
        willReturn(eventResponse).given(fluentClient).eventSync(any(Event.class));
        fluentOrderService.cancelOrder(orderModel);
    }

    @Test(expected = FluentOrderException.class)
    public void testCancelOrderError() {
        willReturn(Arrays.asList(new Error())).given(fluentResponse).getErrors();
        willReturn(fluentResponse).given(fluentClient).eventSync(any(Event.class));
        fluentOrderService.cancelOrder(orderModel);
    }

    @Test
    public void testResumeOrder() {
        willReturn("5623").given(orderModel).getFluentId();
        final ArgumentCaptor<Event> eventCaptor = ArgumentCaptor.forClass(Event.class);
        willReturn(new EventResponse()).given(fluentClient).eventSync(eventCaptor.capture());
        fluentOrderService.resumeOrder(orderModel);
        final Event event = eventCaptor.getValue();
        assertThat(event.getName()).isEqualTo(TgtFluentConstants.Event.RESUME_ORDER);
        assertThat(event.getEntityId()).isEqualTo("5623");
        assertThat(event.getEntityType()).isEqualTo(TgtFluentConstants.EntityType.ORDER);
        assertThat(event.getEntitySubtype()).isEqualTo(TgtFluentConstants.EntitySubtype.HD);
    }

    @Test(expected = Exception.class)
    public void testResumeOrderFail() {
        final EventResponse eventResponse = mock(EventResponse.class);
        willReturn("Failed, just failed!").given(eventResponse).getErrorMessage();
        willReturn(eventResponse).given(fluentClient).eventSync(any(Event.class));
        fluentOrderService.resumeOrder(orderModel);
    }

    @Test(expected = FluentOrderException.class)
    public void testResumeOrderError() {
        willReturn(Arrays.asList(new Error())).given(fluentResponse).getErrors();
        willReturn(fluentResponse).given(fluentClient).eventSync(any(Event.class));
        fluentOrderService.resumeOrder(orderModel);
    }

    @Test
    public void testReleaseOrder() {
        given(orderModel.getFluentId()).willReturn("5623");
        final ArgumentCaptor<Event> eventCaptor = ArgumentCaptor.forClass(Event.class);
        given(fluentClient.eventSync(eventCaptor.capture())).willReturn(new EventResponse());
        final boolean fluentReleaseOrderResponse = fluentOrderService.releaseOrder(orderModel);

        final Event event = eventCaptor.getValue();
        assertThat(event.getName()).isEqualTo(TgtFluentConstants.Event.PREORDER_RELEASE);
        assertThat(event.getEntityId()).isEqualTo("5623");
        assertThat(event.getEntityType()).isEqualTo(TgtFluentConstants.EntityType.ORDER);
        assertThat(event.getEntitySubtype()).isEqualTo(TgtFluentConstants.EntitySubtype.HD);
        assertThat(fluentReleaseOrderResponse).isTrue();
    }

    @Test(expected = FluentErrorException.class)
    public void testReleaseOrderFail() {
        final ArgumentCaptor<Event> eventCaptor = ArgumentCaptor.forClass(Event.class);
        final EventResponse eventResponse = mock(EventResponse.class);
        given(eventResponse.getErrorMessage()).willReturn("Failed, just failed!");
        given(fluentClient.eventSync(eventCaptor.capture())).willReturn(eventResponse);
        fluentOrderService.releaseOrder(orderModel);
        assertThat(fluentResponse.getErrors()).isNotEmpty();
    }

    @Test(expected = FluentOrderException.class)
    public void testReleaseOrderError() {
        final ArgumentCaptor<Event> eventCaptor = ArgumentCaptor.forClass(Event.class);
        given(fluentResponse.getErrors()).willReturn(Arrays.asList(new Error()));
        given(fluentClient.eventSync(eventCaptor.capture())).willReturn(fluentResponse);
        fluentOrderService.releaseOrder(orderModel);
    }

    @Test(expected = FluentOrderException.class)
    public void testUpdateOrderWithInvalidOrder()
            throws FluentOrderException, FluentErrorException {
        willReturn(Boolean.TRUE).given(fluentOrderValidator).isOrderDataInValid(order);
        fluentOrderService.updateOrder(order);
    }

    @Test
    public void testStartFluentOrderShippedProcess()
            throws FluentOrderException, FluentErrorException {
        given(order.getOrderId()).willReturn("456");
        given(order.getStatus()).willReturn("SHIPPED");
        willReturn(orderModel).given(targetOrderService).findOrderByFluentId("456");
        willReturn(targetBusinessProcessService).given(fluentOrderService).getTargetBusinessProcessService();
        willReturn(Boolean.FALSE).given(fluentOrderValidator).isOrderDataInValid(order);
        willReturn(Boolean.FALSE).given(fluentOrderValidator).validateIfAnyConsignmentsAreOpen(orderModel);
        willReturn(Boolean.FALSE).given(fluentOrderValidator).validateIfAnyItemInOrderIsNotShipped(orderModel);
        fluentOrderService.updateOrder(order);
        verify(targetBusinessProcessService).startFluentOrderShippedProcess(orderModel,
                TgtbusprocConstants.BusinessProcess.FLUENT_ORDER_SHIPPED_PROCESS);
    }

    @Test(expected = FluentErrorException.class)
    public void testStartFluentOrderShippedProcessOpenConsignment()
            throws FluentOrderException {
        given(order.getOrderId()).willReturn("456");
        given(order.getStatus()).willReturn("SHIPPED");
        willReturn(OrderStatus.INPROGRESS).given(orderModel).getStatus();
        willReturn(orderModel).given(targetOrderService).findOrderByFluentId("456");
        willReturn(targetBusinessProcessService).given(fluentOrderService).getTargetBusinessProcessService();
        willReturn(Boolean.FALSE).given(fluentOrderValidator).isOrderDataInValid(order);
        willReturn(Boolean.TRUE).given(fluentOrderValidator).validateIfAnyConsignmentsAreOpen(orderModel);
        willReturn(Boolean.FALSE).given(fluentOrderValidator).validateIfAnyItemInOrderIsNotShipped(orderModel);
        given(orderModel.getCode()).willReturn("654");
        fluentOrderService.updateOrder(order);
    }

    @Test(expected = FluentOrderException.class)
    public void testStartFluentOrderShippedProcessOrderAlreadyCompleted()
            throws FluentErrorException {
        given(order.getOrderId()).willReturn("456");
        given(order.getStatus()).willReturn("SHIPPED");
        willReturn(orderModel).given(targetOrderService).findOrderByFluentId("456");
        willReturn(OrderStatus.COMPLETED).given(orderModel).getStatus();
        given(orderModel.getCode()).willReturn("654");

        willReturn(targetBusinessProcessService).given(fluentOrderService).getTargetBusinessProcessService();
        fluentOrderService.updateOrder(order);
    }

    @Test(expected = FluentOrderException.class)
    public void testStartFluentOrderShippedProcessOrderAlreadyCancelled()
            throws FluentErrorException {
        given(order.getOrderId()).willReturn("456");
        given(order.getStatus()).willReturn("SHIPPED");
        willReturn(orderModel).given(targetOrderService).findOrderByFluentId("456");
        willReturn(OrderStatus.CANCELLED).given(orderModel).getStatus();
        given(orderModel.getCode()).willReturn("654");

        willReturn(targetBusinessProcessService).given(fluentOrderService).getTargetBusinessProcessService();
        fluentOrderService.updateOrder(order);
    }

    @Test(expected = FluentErrorException.class)
    public void testStartFluentOrderShippedProcessNotAllItemsShipped()
            throws FluentOrderException {
        given(order.getOrderId()).willReturn("456");
        given(order.getStatus()).willReturn("SHIPPED");
        willReturn(OrderStatus.INPROGRESS).given(orderModel).getStatus();
        willReturn(orderModel).given(targetOrderService).findOrderByFluentId("456");
        willReturn(targetBusinessProcessService).given(fluentOrderService).getTargetBusinessProcessService();
        willReturn(Boolean.FALSE).given(fluentOrderValidator).isOrderDataInValid(order);
        willReturn(Boolean.FALSE).given(fluentOrderValidator).validateIfAnyConsignmentsAreOpen(orderModel);
        willReturn(Boolean.TRUE).given(fluentOrderValidator).validateIfAnyItemInOrderIsNotShipped(orderModel);
        given(orderModel.getCode()).willReturn("654");
        fluentOrderService.updateOrder(order);
    }

    @Test
    public void testStartFluentOrderShippedProcessWhenFluentUpdatesWithCreatedStatus()
            throws FluentOrderException {
        given(order.getOrderId()).willReturn("456");
        given(order.getStatus()).willReturn("CREATED");
        willReturn(orderModel).given(targetOrderService).findOrderByFluentId("456");
        willReturn(Boolean.FALSE).given(fluentOrderValidator).isOrderDataInValid(order);

        given(orderModel.getCode()).willReturn("654");
        fluentOrderService.updateOrder(order);

        verify(fluentOrderValidator, never()).validateIfAnyConsignmentsAreOpen(orderModel);
        verify(fluentOrderValidator, never()).validateIfAnyItemInOrderIsNotShipped(orderModel);
        verify(fluentOrderService, never()).getTargetBusinessProcessService();
    }

    @Test
    public void testGetOrderSucess() {

        given(fluentClient.viewOrder("orderId")).willReturn(new OrderResponse());
        final OrderResponse orderResponse = fluentOrderService.getOrder("orderId");

        assertThat(orderResponse).isNotNull();
        verify(fluentClient).viewOrder("orderId");
    }

    @Test(expected = FluentOrderException.class)
    public void testGetOrderError() {
        final FluentResponse response = mock(FluentResponse.class);
        final Error error = new Error();
        error.setCode("test");
        error.setMessage("junit");
        given(response.getErrors()).willReturn(Arrays.asList(error));
        given(fluentClient.viewOrder("orderId")).willReturn(response);
        fluentOrderService.getOrder("orderId");
    }

    @Test
    public void testCustomerCancelOrder() {
        willReturn("5623").given(orderModel).getFluentId();
        final ArgumentCaptor<Event> eventCaptor = ArgumentCaptor.forClass(Event.class);
        willReturn(new EventResponse()).given(fluentClient).eventSync(eventCaptor.capture());

        fluentOrderService.customerCancelOrder(orderModel);
        final Event event = eventCaptor.getValue();
        assertThat(event.getName()).isEqualTo(TgtFluentConstants.Event.CUSTOMER_CANCEL);
        assertThat(event.getEntityId()).isEqualTo("5623");
        assertThat(event.getEntityType()).isEqualTo(TgtFluentConstants.EntityType.ORDER);
        assertThat(event.getEntitySubtype()).isEqualTo(TgtFluentConstants.EntitySubtype.HD);
    }

    @Test
    public void testCustomerCancelOrderFail() {
        final EventResponse eventResponse = mock(EventResponse.class);
        willReturn("Failed!").given(eventResponse).getErrorMessage();
        willReturn(eventResponse).given(fluentClient).eventSync(any(Event.class));
        final String message = fluentOrderService.customerCancelOrder(orderModel);
        assertThat(message).isEqualTo(CANCEL_ORDER_FAILED_IN_FLUENT);
    }

    @Test
    public void testCustomerCancelOrderError() {
        willReturn(Arrays.asList(new Error())).given(fluentResponse).getErrors();
        willReturn(fluentResponse).given(fluentClient).eventSync(any(Event.class));
        fluentOrderService.customerCancelOrder(orderModel);
        final String message = fluentOrderService.customerCancelOrder(orderModel);
        assertThat(message).isEqualTo(CANCEL_ORDER_FAILED_SYSTEM_ERROR);
    }

}
