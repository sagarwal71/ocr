/**
 * 
 */
package au.com.target.tgtfluent.client.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import au.com.target.tgtfluent.data.Address;
import au.com.target.tgtfluent.data.Attribute;
import au.com.target.tgtfluent.data.AttributeType;
import au.com.target.tgtfluent.data.Batch;
import au.com.target.tgtfluent.data.BatchAction;
import au.com.target.tgtfluent.data.BatchRecordStatus;
import au.com.target.tgtfluent.data.BatchResponse;
import au.com.target.tgtfluent.data.BatchStatus;
import au.com.target.tgtfluent.data.Category;
import au.com.target.tgtfluent.data.CategoryResponse;
import au.com.target.tgtfluent.data.Customer;
import au.com.target.tgtfluent.data.EntityType;
import au.com.target.tgtfluent.data.Event;
import au.com.target.tgtfluent.data.EventResponse;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.data.Fulfilment;
import au.com.target.tgtfluent.data.FulfilmentChoice;
import au.com.target.tgtfluent.data.FulfilmentChoiceDeliveryType;
import au.com.target.tgtfluent.data.FulfilmentChoiceType;
import au.com.target.tgtfluent.data.FulfilmentItem;
import au.com.target.tgtfluent.data.FulfilmentOptionRequest;
import au.com.target.tgtfluent.data.FulfilmentOptionResponse;
import au.com.target.tgtfluent.data.FulfilmentsResponse;
import au.com.target.tgtfluent.data.GiftCardRecipient;
import au.com.target.tgtfluent.data.Job;
import au.com.target.tgtfluent.data.JobResponse;
import au.com.target.tgtfluent.data.JobStatus;
import au.com.target.tgtfluent.data.Location;
import au.com.target.tgtfluent.data.OpeningHours;
import au.com.target.tgtfluent.data.Order;
import au.com.target.tgtfluent.data.OrderItem;
import au.com.target.tgtfluent.data.OrderResponse;
import au.com.target.tgtfluent.data.OrderType;
import au.com.target.tgtfluent.data.Product;
import au.com.target.tgtfluent.data.ProductStatus;
import au.com.target.tgtfluent.data.ProductsResponse;
import au.com.target.tgtfluent.data.Recipient;
import au.com.target.tgtfluent.data.Reference;
import au.com.target.tgtfluent.data.Sku;
import au.com.target.tgtfluent.data.SkuStatus;
import au.com.target.tgtfluent.data.SkusResponse;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:tgtfluent-spring-test.xml")
public class FluentClientImplTest {

    @Autowired
    private FluentClientImpl fluentClient;

    private MockRestServiceServer mockServer;

    @Before
    public void setUp() {
        final DefaultOAuth2AccessToken accessToken = new DefaultOAuth2AccessToken("12345");
        final Map<String, Object> additionalInfo = new HashMap<>();
        additionalInfo.put("Retailer_id", Integer.valueOf(1));
        accessToken.setAdditionalInformation(additionalInfo);
        ((OAuth2RestTemplate)fluentClient.getFluentRestTemplate()).getOAuth2ClientContext()
                .setAccessToken(accessToken);
        mockServer = MockRestServiceServer.createServer(fluentClient.getFluentRestTemplate());
    }

    @Test
    public void testCreateJob() {
        mockServer.expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/job/"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess("{\"id\":\"9876\"}", MediaType.APPLICATION_JSON));
        final Job job = new Job();
        job.setName("Dummy Job");

        final FluentResponse fluentResponse = fluentClient.createJob(job);
        assertThat(fluentResponse).isNotNull();
        assertThat(fluentResponse.getId()).isEqualTo("9876");
        assertThat(job.getRetailerId()).isEqualTo("1");
    }

    @Test
    public void testCreateJobClientError() {
        mockServer
                .expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/job/"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(
                        withStatus(HttpStatus.CONFLICT)
                                .contentType(MediaType.APPLICATION_JSON)
                                .body(
                                        "{\"errors\":[{\"code\":\"409\",\"message\":\"product reference W200000 already exists for this retailer\"}]}"));
        final Job job = new Job();
        job.setName("Dummy Job");

        final FluentResponse fluentResponse = fluentClient.createJob(job);
        assertThat(fluentResponse).isNotNull();
        assertThat(fluentResponse.getId()).isNull();
        assertThat(fluentResponse.getErrors()).isNotEmpty();
        assertThat(fluentResponse.getErrors().get(0).getCode()).isEqualTo(String.valueOf(HttpStatus.CONFLICT.value()));
        assertThat(fluentResponse.getErrors().get(0).getMessage())
                .isEqualTo("product reference W200000 already exists for this retailer");
        assertThat(job.getRetailerId()).isEqualTo("1");
    }

    @Test
    public void testCreateJobServerError() {
        mockServer.expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/job/"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.INTERNAL_SERVER_ERROR));
        final Job job = new Job();
        job.setName("Dummy Job");

        final FluentResponse fluentResponse = fluentClient.createJob(job);
        assertThat(fluentResponse).isNotNull();
        assertThat(fluentResponse.getId()).isNull();
        assertThat(fluentResponse.getErrors()).isNotEmpty();
        assertThat(fluentResponse.getErrors().get(0).getCode())
                .isEqualTo(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
        assertThat(fluentResponse.getErrors().get(0).getMessage())
                .isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        assertThat(job.getRetailerId()).isEqualTo("1");
    }

    @Test
    public void testViewJob() {
        mockServer.expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/job/9876"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("{\"jobId\": \"9876\",\"status\": \"OPEN\",\"createdOn\":"
                        + " \"2016-07-20T12:39:43.912+0000\",\"batch\": [{\"batchId\": \"298\","
                        + "\"status\": \"COMPLETE\",\"createdOn\": \"2016-07-20T12:39:43.912+0000\"}]}",
                        MediaType.APPLICATION_JSON));

        final FluentResponse fluentResponse = fluentClient.viewJob("9876");
        assertThat(fluentResponse).isNotNull();
        assertThat(fluentResponse).isInstanceOf(JobResponse.class);
        final JobResponse jobResponse = (JobResponse)fluentResponse;
        assertThat(jobResponse.getBatch()).hasSize(1);
        assertThat(jobResponse.getStatus()).isEqualTo(JobStatus.OPEN);
    }

    @Test
    public void testCreateBatch() {
        mockServer.expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/job/9876/batch/"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess("{\"id\":\"1\"}", MediaType.APPLICATION_JSON));
        final Batch batch = new Batch();
        batch.setAction(BatchAction.UPSERT);
        batch.setEntityType(EntityType.LOCATION);
        final List<Location> locations = new ArrayList<>();
        batch.setEntities(locations);

        final FluentResponse fluentResponse = fluentClient.createBatch("9876", batch);
        assertThat(fluentResponse).isNotNull();
        assertThat(fluentResponse.getId()).isEqualTo("1");
    }

    @Test
    public void testViewBatch() {
        mockServer
                .expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/job/9876/batch/1"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(
                        withSuccess(
                                "{\"batchId\":\"1\",\"entityType\":\"LOCATION\",\"status\":\"COMPLETE\""
                                        + ",\"start\":1,\"count\":10,\"total\":1,\"results\":[{\"entityId\":\"2\",\"locationRef\":\"5001\""
                                        + ",\"skuRef\":\"\",\"responseCode\":200,\"message\":\"Agent 5001 created successfully\","
                                        + "\"createdOn\":\"2017-09-18T23:05:47.734+0000\"}],\"createdOn\":\"2017-09-18T23:05:26.392+0000\"}",
                                MediaType.APPLICATION_JSON));

        final BatchResponse batchResponse = fluentClient.viewBatch("9876", "1", null);
        assertThat(batchResponse).isNotNull();
        assertThat(batchResponse.getResults()).hasSize(1);
        assertThat(batchResponse.getResults().get(0)).isInstanceOf(BatchRecordStatus.class);
        assertThat(batchResponse.getStatus()).isEqualTo(BatchStatus.COMPLETE);
    }

    @Test
    public void testViewBatchWithCount() {
        mockServer
                .expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/job/9876/batch/1?count=10"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(
                        withSuccess(
                                "{\"batchId\":\"1\",\"entityType\":\"LOCATION\",\"status\":\"COMPLETE\""
                                        + ",\"start\":1,\"count\":10,\"total\":1,\"results\":[{\"entityId\":\"2\",\"locationRef\":\"5001\""
                                        + ",\"skuRef\":\"\",\"responseCode\":200,\"message\":\"Agent 5001 created successfully\","
                                        + "\"createdOn\":\"2017-09-18T23:05:47.734+0000\"}],\"createdOn\":\"2017-09-18T23:05:26.392+0000\"}",
                                MediaType.APPLICATION_JSON));

        final Map<String, String> queryParams = new HashMap<>();
        queryParams.put("count", "10");
        final BatchResponse batchResponse = fluentClient.viewBatch("9876", "1", queryParams);
        assertThat(batchResponse).isNotNull();
        assertThat(batchResponse.getResults()).hasSize(1);
        assertThat(batchResponse.getResults().get(0)).isInstanceOf(BatchRecordStatus.class);
        assertThat(batchResponse.getStatus()).isEqualTo(BatchStatus.COMPLETE);
    }

    @Test
    public void testCreateLocation() {
        mockServer.expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/location/"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess("{\"id\":\"9876\"}", MediaType.APPLICATION_JSON));
        final Location location = new Location();
        location.setLocationRef("1234");
        location.setStatus("ACTIVE");
        location.setName("Waurn Ponds");
        location.setEmail("waurnpondsstore@target.com.au");
        location.setSupportPhone("0412345678");
        location.setType("STORE");
        location.setTimeZone("Australia/Sydney");
        final Address address = new Address();
        address.setLocationRef("1234");
        address.setCompanyName("Target");
        address.setName("Waurn Ponds");
        address.setCity("Geelong");
        address.setCountry("AU");
        address.setPostcode("3216");
        address.setState("VIC");
        address.setStreet("Princes Highway");
        address.setLatitude("-38.200019");
        address.setLongitude("144.319059");
        location.setAddress(address);
        final OpeningHours openingHours = new OpeningHours();
        openingHours.setMonStartMin("0900");
        openingHours.setMonEndMin("1800");
        openingHours.setTueStartMin("0900");
        openingHours.setTueEndMin("1800");
        openingHours.setWedStartMin("0900");
        openingHours.setWedEndMin("1800");
        openingHours.setThuStartMin("0900");
        openingHours.setThuEndMin("1800");
        openingHours.setFriStartMin("0900");
        openingHours.setFriEndMin("1800");
        openingHours.setSatStartMin("0900");
        openingHours.setSatEndMin("1800");
        openingHours.setSunStartMin("0900");
        openingHours.setSunEndMin("1800");
        location.setOpeningHours(openingHours);
        final FluentResponse fluentResponse = fluentClient.createLocation(location);
        assertThat(fluentResponse).isNotNull();
        assertThat(fluentResponse.getId()).isEqualTo("9876");
    }

    @Test
    public void testUpdateLocation() {
        mockServer.expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/location/9876"))
                .andExpect(method(HttpMethod.PUT))
                .andRespond(withSuccess("{}", MediaType.APPLICATION_JSON));
        final Location location = new Location();
        location.setLocationRef("1234");
        location.setStatus("ACTIVE");
        location.setName("Waurn Ponds");
        location.setEmail("waurnpondsstore@target.com.au");
        location.setSupportPhone("0412345678");
        location.setType("STORE");
        location.setTimeZone("Australia/Sydney");
        final Address address = new Address();
        address.setLocationRef("1234");
        address.setCompanyName("Target");
        address.setName("Waurn Ponds");
        address.setCity("Geelong");
        address.setCountry("AU");
        address.setPostcode("3216");
        address.setState("VIC");
        address.setStreet("Princes Highway");
        address.setLatitude("-38.200019");
        address.setLongitude("144.319059");
        location.setAddress(address);
        final OpeningHours openingHours = new OpeningHours();
        openingHours.setMonStartMin("0900");
        openingHours.setMonEndMin("1800");
        openingHours.setTueStartMin("0900");
        openingHours.setTueEndMin("1800");
        openingHours.setWedStartMin("0900");
        openingHours.setWedEndMin("1800");
        openingHours.setThuStartMin("0900");
        openingHours.setThuEndMin("1800");
        openingHours.setFriStartMin("0900");
        openingHours.setFriEndMin("1800");
        openingHours.setSatStartMin("0900");
        openingHours.setSatEndMin("1800");
        openingHours.setSunStartMin("0900");
        openingHours.setSunEndMin("1800");
        location.setOpeningHours(openingHours);
        final FluentResponse fluentResponse = fluentClient.updateLocation("9876", location);
        assertThat(fluentResponse).isNotNull();
        assertThat(fluentResponse.getErrors()).isNull();
    }

    @Test
    public void testCreateProduct() {
        mockServer.expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/product/"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess("{\"id\":\"1\"}", MediaType.APPLICATION_JSON));
        final Product product = new Product();
        product.setName("Basic Maternity T-Shirt");
        product.setProductRef("W944854");
        product.setStatus(ProductStatus.ACTIVE);
        product.setCategories(Arrays.asList("cat1", "cat2"));
        final Attribute attribute1 = new Attribute();
        attribute1.setName("ProductType");
        attribute1.setType(AttributeType.STRING);
        attribute1.setValue("normal");
        product.setAttributes(Arrays.asList(attribute1));
        final FluentResponse fluentResponse = fluentClient.createProduct(product);
        assertThat(fluentResponse).isNotNull();
        assertThat(fluentResponse.getId()).isEqualTo("1");
        assertThat(product.getRetailerId()).isEqualTo("1");
    }

    @Test
    public void testUpdateProduct() {
        final String fluentId = "1";
        mockServer.expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/product/" + fluentId))
                .andExpect(method(HttpMethod.PUT))
                .andRespond(withSuccess("{\"id\":\"1\"}", MediaType.APPLICATION_JSON));
        final Product product = new Product();
        product.setName("Basic Maternity T-Shirt");
        product.setProductRef("W944854");
        product.setStatus(ProductStatus.ACTIVE);
        product.setCategories(Arrays.asList("cat1", "cat2"));
        final Attribute attribute1 = new Attribute();
        attribute1.setName("ProductType");
        attribute1.setType(AttributeType.STRING);
        attribute1.setValue("normal");
        product.setAttributes(Arrays.asList(attribute1));
        final FluentResponse fluentResponse = fluentClient.updateProduct(fluentId, product);
        assertThat(fluentResponse).isNotNull();
        assertThat(fluentResponse.getId()).isEqualTo("1");
        assertThat(product.getRetailerId()).isEqualTo("1");
    }

    @Test
    public void testSearchProduct() {
        final String productRef = "W10000";
        mockServer
                .expect(requestTo(
                        "https://target.sandbox.api.fluentretail.com/api/v4.1/product/?productRef=" + productRef))
                .andExpect(method(HttpMethod.GET))
                .andRespond(
                        withSuccess(
                                "{\"total\":1,\"results\":[{\"createdOn\":1506476506252,\"references\":[],"
                                        + "\"status\":\"ACTIVE\",\"retailerId\":\"1\",\"name\":\"Basic Maternity T-Shirt new\","
                                        + "\"prices\":[],\"categories\":[\"S3675\"],\"attributes\":[{\"name\":\"ProductType\","
                                        + "\"value\":\"bulky\",\"type\":\"STRING\"}],\"updatedOn\":1506476670614,"
                                        + "\"productRef\":\"W900000\",\"productId\":\"7\"}],\"count\":10,\"start\":1}",
                                MediaType.APPLICATION_JSON));
        final ProductsResponse productsResponse = fluentClient.searchProduct(productRef);
        assertThat(productsResponse).isNotNull();
        assertThat(productsResponse.getTotal()).isEqualTo(1);
        assertThat(productsResponse.getResults().size()).isEqualTo(1);
        assertThat(productsResponse.getStart()).isEqualTo(1);
        assertThat(productsResponse.getCount()).isEqualTo(10);
        assertThat(productsResponse.getResults().get(0).getProductId()).isEqualTo("7");
    }

    @Test
    public void testCreateSku() {
        mockServer.expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/product/1234/sku/"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess("{\"id\":\"1\"}", MediaType.APPLICATION_JSON));
        final Sku sku = new Sku();
        sku.setSkuRef("58424834");
        sku.setStatus(SkuStatus.ACTIVE);
        sku.setName("Women's essentials Long Sleeve Rib T-Shirt");
        sku.setProductRef("W920995");
        sku.setImageUrlRef("http://bo-qat.tonline.local/medias/static_content/product/images/large/56/95/A905695.jpg");
        final Attribute attribute1 = new Attribute();
        attribute1.setName("COLOUR_DESC");
        attribute1.setType(AttributeType.STRING);
        attribute1.setValue("black");
        sku.setAttributes(Arrays.asList(attribute1));
        final Reference reference1 = new Reference();
        reference1.setType("EAN");
        reference1.setValue("12313123123");
        sku.setReferences(Arrays.asList(reference1));

        final FluentResponse fluentResponse = fluentClient.createSku("1234", sku);
        assertThat(fluentResponse).isNotNull();
        assertThat(fluentResponse.getId()).isEqualTo("1");
        assertThat(sku.getRetailerId()).isEqualTo("1");
    }

    @Test
    public void testUpdateSku() {
        mockServer.expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/product/1234/sku/1"))
                .andExpect(method(HttpMethod.PUT))
                .andRespond(withSuccess("{\"id\":\"1\"}", MediaType.APPLICATION_JSON));
        final Sku sku = new Sku();
        sku.setSkuRef("58424834");
        sku.setStatus(SkuStatus.ACTIVE);
        sku.setName("Women's essentials Long Sleeve Rib T-Shirt");
        sku.setProductRef("W920995");
        sku.setImageUrlRef("http://bo-qat.tonline.local/medias/static_content/product/images/large/56/95/A905695.jpg");
        final Attribute attribute1 = new Attribute();
        attribute1.setName("COLOUR_DESC");
        attribute1.setType(AttributeType.STRING);
        attribute1.setValue("black");
        sku.setAttributes(Arrays.asList(attribute1));
        final Reference reference1 = new Reference();
        reference1.setType("EAN");
        reference1.setValue("12313123123");
        sku.setReferences(Arrays.asList(reference1));

        final FluentResponse fluentResponse = fluentClient.updateSku("1234", "1", sku);
        assertThat(fluentResponse).isNotNull();
        assertThat(fluentResponse.getId()).isEqualTo("1");
        assertThat(sku.getRetailerId()).isEqualTo("1");
    }

    @Test
    public void testSearchSku() {
        final String skuRef = "58424834";
        mockServer
                .expect(requestTo(
                        "https://target.sandbox.api.fluentretail.com/api/v4.1/sku?skuRef=" + skuRef))
                .andExpect(method(HttpMethod.GET))
                .andRespond(
                        withSuccess(
                                "{\"start\":1,\"count\":10,\"total\":1,"
                                        + "\"retailerId\":null,\"results\":[{\"skuId\":6,\"skuRef\":\"58424834\",\"productRef\":\"W900000\","
                                        + "\"name\":\"Women's essentials Long Sleeve Rib T-Shirt\",\"status\":\"ACTIVE\","
                                        + "\"imageUrl\":\"http://bo-qat.tonline.local/medias/static_content/product/images/large/56/95/A905695.jpg\","
                                        + "\"prices\":[],\"attributes\":[{\"name\":\"COLOUR_DESC\",\"value\":\"black\",\"type\":\"STRING\"}],"
                                        + "\"references\":[{\"type\":\"EAN\",\"value\":\"12313123123\"}],\"categories\":[{\"id\":1,"
                                        + "\"categoryRef\":\"S3675\",\"name\":\"Womens Skirt\",\"updatedOn\":\"2017-09-28T04:33:00.691+0000\","
                                        + "\"createdOn\":\"2017-09-25T05:44:44.217+0000\"}],\"updateOn\":\"2017-10-02T04:19:31.667+0000\","
                                        + "\"createdOn\":\"2017-10-02T04:19:31.674+0000\"}]}",
                                MediaType.APPLICATION_JSON));
        final SkusResponse skusResponse = fluentClient.searchSkus(skuRef);
        assertThat(skusResponse).isNotNull();
        assertThat(skusResponse.getTotal()).isEqualTo(1);
        assertThat(skusResponse.getResults().size()).isEqualTo(1);
        assertThat(skusResponse.getStart()).isEqualTo(1);
        assertThat(skusResponse.getCount()).isEqualTo(10);
    }

    @Test
    public void testCreateCategory() {
        mockServer.expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/category/"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess("{\"id\":\"1\"}", MediaType.APPLICATION_JSON));
        final Category category = new Category();
        category.setCategoryRef("Cat1");
        category.setName("Mens");
        final FluentResponse fluentResponse = fluentClient.createCategory(category);
        assertThat(fluentResponse).isNotNull();
        assertThat(fluentResponse.getId()).isEqualTo("1");
        assertThat(category.getRetailerId()).isEqualTo("1");
    }

    @Test
    public void testUpdateCategory() {
        mockServer.expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/category/" + "1"))
                .andExpect(method(HttpMethod.PUT))
                .andRespond(withSuccess("{\"id\":\"1\"}", MediaType.APPLICATION_JSON));
        final Category category = new Category();
        category.setId("1");
        category.setCategoryRef("Cat01");
        category.setName("Men");
        final String fluentId = "1";
        final FluentResponse fluentResponse = fluentClient.updateCategory(fluentId, category);
        assertThat(fluentResponse).isNotNull();
        assertThat(fluentResponse.getId()).isEqualTo("1");
        assertThat(category.getRetailerId()).isEqualTo("1");
    }

    @Test
    public void testSearchCategory() {
        final String categoryRef = "Cat01";
        mockServer.expect(requestTo(
                "https://target.sandbox.api.fluentretail.com/api/v4.1/category/?categoryRef=" + categoryRef))
                .andExpect(method(HttpMethod.GET)).andRespond(withSuccess(
                        "{\"start\":1,\"count\":10,\"total\":1,\"retailerId\":null,"
                                + "\"results\":[{\"createdOn\":1506476506252,\"updatedOn\":1506476670614,"
                                + "\"categoryRef\":\"Cat01\",\"id\":\"1\",\"name\":\"Men\"}]}",
                        MediaType.APPLICATION_JSON));
        final CategoryResponse categoryResponse = fluentClient.searchCategory(categoryRef);
        assertThat(categoryResponse).isNotNull();
        assertThat(categoryResponse.getTotal()).isEqualTo(1);
        assertThat(categoryResponse.getResults()).hasSize(1);
        assertThat(categoryResponse.getStart()).isEqualTo(1);
        assertThat(categoryResponse.getCount()).isEqualTo(10);
        assertThat(categoryResponse.getResults().get(0).getId()).isEqualTo("1");
    }

    // this may fail when running from eclipse because of the proxy
    @Test
    public void testGetRetailerIdGetsAccessToken() {
        ((OAuth2RestTemplate)fluentClient.getFluentRestTemplate()).getOAuth2ClientContext()
                .setAccessToken(null);
        assertThat(fluentClient.getRetailerId()).isNotEmpty();
    }

    @Test
    public void testCreateFulfilmentOption() {
        mockServer
                .expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/fulfilmentOptions/"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(
                        withSuccess(
                                "{\"entityType\":\"FULFILMENT_OPTIONS\",\"count\":1,\"total\":null,\"limit\":null,"
                                        + "\"orderType\":null,\"locationRef\":null,\"trackingId\":null,\"address\":null,"
                                        + "\"items\":[{\"skuRef\":\"58992029\",\"requestedQuantity\":null,\"availableQuantity\":null}],"
                                        + "\"attributes\":[{\"name\":\"deliveryTypes\",\"value\":[\"CC\",\"HD\",\"ED\","
                                        + "\"CONSOLIDATED_STORES_SOH\"],\"type\":\"ARRAY\"},{\"name\":\"locationRefs\","
                                        + "\"value\":[\"5599\",\"5162\"],\"type\":\"ARRAY\"}],\"retailerId\":1,"
                                        + "\"flexType\":\"FULFILMENT_OPTIONS::DEFAULT\",\"flexVersion\":9,\"plans\":"
                                        + "[{\"entityType\":\"FULFILMENT_PLAN\",\"planId\":\"66d687d1-ebfc-434a-b7df-040ad9f54c4d\","
                                        + "\"planType\":\"NETWORK_ATS_VALUES\",\"eta\":null,\"splits\":null,\"fulfilments\":"
                                        + "[{\"eta\":null,\"items\":[{\"skuRef\":\"58992029\",\"availableQty\":20}],"
                                        + "\"locationRef\":null,\"fulfilmentType\":\"CC\"},{\"eta\":null,\"items\":"
                                        + "[{\"skuRef\":\"58992029\",\"availableQty\":10}],\"locationRef\":null,"
                                        + "\"fulfilmentType\":\"HD\"},{\"eta\":null,\"items\":[{\"skuRef\":\"58992029\","
                                        + "\"availableQty\":30}],\"locationRef\":null,\"fulfilmentType\":\"ED\"},{\"eta\":null,"
                                        + "\"items\":[{\"skuRef\":\"58992029\",\"availableQty\":40}],\"locationRef\":null,"
                                        + "\"fulfilmentType\":\"CONSOLIDATED_STORES_SOH\"},{\"eta\":null,\"items\":"
                                        + "[{\"skuRef\":\"58992029\",\"availableQty\":50}],\"locationRef\":\"5599\","
                                        + "\"fulfilmentType\":\"STORE_SOH\"},{\"eta\":null,\"items\":[{\"skuRef\":\"58992029\","
                                        + "\"availableQty\":50}],\"locationRef\":\"5162\",\"fulfilmentType\":\"STORE_SOH\"}],"
                                        + "\"exceptions\":null,\"fulfilmentOptionsId\":\"b2747089-1c65-45f0-bea6-e57f3639e04d\","
                                        + "\"flexType\":\"FULFILMENT_OPTIONS::DEFAULT\",\"flexVersion\":9,\"retailerId\":\"1\","
                                        + "\"id\":\"66d687d1-ebfc-434a-b7df-040ad9f54c4d\",\"attributes\":null,\"status\":null,"
                                        + "\"createdOn\":\"2018-03-20T02:32:42.393+0000\",\"type\":\"FULFILMENT_PLAN\",\"ref\":null}],"
                                        + "\"unfulfilledItems\":[],\"status\":\"OPTIONS_PROVIDED\","
                                        + "\"id\":\"b2747089-1c65-45f0-bea6-e57f3639e04d\",\"createdOn\":null,"
                                        + "\"type\":\"FULFILMENT_OPTIONS\",\"ref\":null}",
                                MediaType.APPLICATION_JSON));
        final FluentResponse fluentResponse = fluentClient.createFulfilmentOption(new FulfilmentOptionRequest());
        assertThat(fluentResponse).isNotNull();
        assertThat(fluentResponse).isInstanceOf(FulfilmentOptionResponse.class);
        final FulfilmentOptionResponse fulfilmentOptionResponse = (FulfilmentOptionResponse)fluentResponse;
        assertThat(fulfilmentOptionResponse.getId()).isEqualTo("b2747089-1c65-45f0-bea6-e57f3639e04d");
    }

    @Test
    public void testCreateFulfilmentOptionWithError() {
        mockServer.expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/fulfilmentOptions/"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.INTERNAL_SERVER_ERROR).body(
                        "{\"errors\":[{\"code\":\"500\",\"message\":\"unkown server error\"}]}")
                        .contentType(MediaType.APPLICATION_JSON));
        final FluentResponse fluentResponse = fluentClient.createFulfilmentOption(new FulfilmentOptionRequest());
        assertThat(fluentResponse).isNotNull();
        assertThat(fluentResponse.getId()).isNull();
        assertThat(fluentResponse.getErrors()).hasSize(1);
        assertThat(fluentResponse.getErrors().get(0).getCode()).isEqualTo("500");
        assertThat(fluentResponse.getErrors().get(0).getMessage()).isEqualTo("unkown server error");
    }

    @Test
    public void testEventSync() {
        mockServer
                .expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/event/sync"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(
                        withSuccess(
                                "{\"eventId\":\"0f7a355a-0313-4e75-a4dc-eae5f539064f\",\"entityId\":\"8\",\"eventStatus\":\"SUCCESS\"}",
                                MediaType.APPLICATION_JSON));
        final Event event = new Event();
        final FluentResponse fluentResponse = fluentClient.eventSync(event);
        assertThat(fluentResponse).isInstanceOf(EventResponse.class);
        assertThat(((EventResponse)fluentResponse).getEventId()).isEqualTo("0f7a355a-0313-4e75-a4dc-eae5f539064f");
        assertThat(((EventResponse)fluentResponse).getEntityId()).isEqualTo("8");
        assertThat(((EventResponse)fluentResponse).getEventStatus()).isEqualTo("SUCCESS");
        assertThat(((EventResponse)fluentResponse).getErrorMessage()).isNull();
        assertThat(event.getAccountId()).isEqualTo("target");
    }

    @Test
    public void testEventSyncFail() {
        mockServer
                .expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/event/sync"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(
                        withStatus(HttpStatus.NOT_FOUND)
                                .body(
                                        "{\"eventId\":\"0f7a355a-0313-4e75-a4dc-eae5f539064f\",\"entityId\":\"8\",\"eventStatus\":\"FAILED\",\"errorMessage\":\"Event has Failed\"}")
                                .contentType(MediaType.APPLICATION_JSON));
        final Event event = new Event();
        final FluentResponse fluentResponse = fluentClient.eventSync(event);
        assertThat(fluentResponse).isInstanceOf(EventResponse.class);
        assertThat(((EventResponse)fluentResponse).getEventId()).isEqualTo("0f7a355a-0313-4e75-a4dc-eae5f539064f");
        assertThat(((EventResponse)fluentResponse).getEntityId()).isEqualTo("8");
        assertThat(((EventResponse)fluentResponse).getEventStatus()).isEqualTo("FAILED");
        assertThat(((EventResponse)fluentResponse).getErrorMessage()).isEqualTo("Event has Failed");
        assertThat(event.getAccountId()).isEqualTo("target");
    }

    @Test
    public void testEventSyncWithError() {
        mockServer.expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/event/sync"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.INTERNAL_SERVER_ERROR).body(
                        "{\"errors\":[{\"code\":\"500\",\"message\":\"unkown server error\"}]}")
                        .contentType(MediaType.APPLICATION_JSON));
        final Event event = new Event();
        final FluentResponse fluentResponse = fluentClient.eventSync(event);
        assertThat(fluentResponse.getId()).isNull();
        assertThat(fluentResponse.getErrors()).hasSize(1);
        assertThat(fluentResponse.getErrors().get(0).getCode()).isEqualTo("500");
        assertThat(fluentResponse.getErrors().get(0).getMessage()).isEqualTo("unkown server error");
        assertThat(event.getAccountId()).isEqualTo("target");
    }

    @Test
    public void testRetrieveFulfilmentsByOrderWithError() {
        final String fluentOrderId = "testId";
        mockServer
                .expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/order/" + fluentOrderId
                        + "/fulfilment"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(
                        "{\"errors\":[{\"code\":\"500\",\"message\":\"unkown server error\"}]}",
                        MediaType.APPLICATION_JSON));
        final FulfilmentsResponse fluentResponse = fluentClient.retrieveFulfilmentsByOrder(fluentOrderId);
        assertThat(fluentResponse).isNotNull();
        assertThat(fluentResponse.getErrors()).hasSize(1);
        assertThat(fluentResponse.getErrors().get(0).getCode()).isEqualTo("500");
        assertThat(fluentResponse.getErrors().get(0).getMessage()).isEqualTo("unkown server error");
    }

    @Test
    public void testRetrieveFulfilmentsByOrder() {
        final String fluentOrderId = "testId";
        mockServer
                .expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/order/" + fluentOrderId
                        + "/fulfilment"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(
                        withSuccess(
                                "{\r\n    \"orderId\": \"00228007\",\r\n    \"fulfilments\": [\r\n"
                                        + "        {\r\n            \"orderId\": \"00228007\",\r\n            "
                                        + "\"fulfilmentId\": 987654322,\r\n            \"deliveryType\": \"STANDARD\","
                                        + "\r\n            \"eta\": \"3H\",\r\n            \"createdOn\": \"2018-04-23T23:40:50.763+0000\""
                                        + ",\r\n            \"expiryTime\": null,\r\n            \"status\": \"REJECTED\",\r\n            "
                                        + "\"fulfilmentRef\": null,\r\n            \"fulfilmentType\": \"HD_PFDC\",\r\n            "
                                        + "\"items\": [\r\n                {\r\n                    \"fulfilmentItemId\": 987654321,"
                                        + "\r\n                    \"fulfilmentItemRef\": null,\r\n                    \"orderItemId\": 1,"
                                        + "\r\n                    \"orderItemRef\": \"P4025_blue_M\",\r\n                    "
                                        + "\"requestedQty\": 1,\r\n                    \"confirmedQty\": 0,\r\n                    "
                                        + "\"filledQty\": 0,\r\n                    \"rejectedQty\": 1,\r\n                    \"skuRef\":"
                                        + " \"P4025_blue_M\"\r\n                }\r\n            ],\r\n            \"fromAddress\": null,\r"
                                        + "\n            \"toAddress\": {\r\n                \"locationRef\": \"\",\r\n                "
                                        + "\"companyName\": \"Target\",\r\n                \"name\": null,\r\n                \"street\":"
                                        + " \"Cnr. Brougham & Moorabool Streets\",\r\n                \"city\": \"Geelong\",\r\n          "
                                        + "      \"postcode\": \"3220\",\r\n                \"state\": \"VIC\",\r\n                "
                                        + "\"country\": \"Australia\"\r\n            },\r\n            \"consignment\": null\r\n        "
                                        + "}\r\n    ]\r\n}",
                                MediaType.APPLICATION_JSON));
        final FulfilmentsResponse fluentResponse = fluentClient.retrieveFulfilmentsByOrder(fluentOrderId);
        assertThat(fluentResponse).isNotNull();
        assertThat(fluentResponse.getOrderId()).isEqualTo("00228007");
        assertThat(fluentResponse.getFulfilments()).hasSize(1);
        final Fulfilment fulfilment = fluentResponse.getFulfilments().get(0);
        assertThat(fulfilment.getOrderId()).isEqualTo("00228007");
        assertThat(fulfilment.getFulfilmentId()).isEqualTo("987654322");
        assertThat(fulfilment.getDeliveryType()).isEqualTo("STANDARD");
        assertThat(fulfilment.getEta()).isEqualTo("3H");
        assertThat(fulfilment.getCreatedOn().getTime()).isEqualTo(1524526850763l);
        assertThat(fulfilment.getExpiryTime()).isNull();
        assertThat(fulfilment.getStatus()).isEqualTo("REJECTED");
        assertThat(fulfilment.getFulfilmentRef()).isNull();
        assertThat(fulfilment.getFulfilmentType()).isEqualTo("HD_PFDC");
        assertThat(fulfilment.getFromAddress()).isNull();

        assertThat(fulfilment.getItems()).hasSize(1);
        final FulfilmentItem item = fulfilment.getItems().get(0);
        assertThat(item.getFulfilmentItemId()).isEqualTo(987654321);
        assertThat(item.getFulfilmentItemRef()).isNull();
        assertThat(item.getOrderItemId()).isEqualTo(1);
        assertThat(item.getOrderItemRef()).isEqualTo("P4025_blue_M");
        assertThat(item.getRequestedQty()).isEqualTo(1);
        assertThat(item.getConfirmedQty()).isEqualTo(0);
        assertThat(item.getFilledQty()).isEqualTo(0);
        assertThat(item.getRejectedQty()).isEqualTo(1);
        assertThat(item.getSkuRef()).isEqualTo("P4025_blue_M");
    }

    @Test
    public void testCreateOrderSuccess() {
        mockServer.expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/order/"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess("{\"id\":\"1\"}", MediaType.APPLICATION_JSON));

        final Order order = createOrderRequest();

        final FluentResponse fluentResponse = fluentClient.createOrder(order);
        assertThat(fluentResponse).isNotNull();
        assertThat(fluentResponse.getId()).isEqualTo("1");
        assertThat(fluentResponse.getErrors()).isNull();

    }

    @Test
    public void testViewOrderSuccess() {
        mockServer.expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/order/16567"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(
                        "{ \"orderId\": \"16567\", \"orderRef\": \"1442900789\", \"status\": \"BOOKED\", \"type\""
                                + ": \"HD\", \"customer\": { \"customerId\": 310728, \"customerRef\": \"08141413\""
                                + ", \"firstName\": \"Rick\", \"lastName\": \"Leathers\", \"email\": "
                                + "\"nick@parcelpoint.com.au\", \"mobile\": \"0424188192\" }, \"retailer\""
                                + ": { \"retailerId\": \"211\" }, \"fulfilments\": [ { \"orderId\": \"16567\""
                                + ", \"fulfilmentId\": \"4160\", \"fulfilmentRef\": null, \"status\": \"CREATED\""
                                + ", \"fulfilmentType\": \"HD_PFS\" } ], \"items\": [ { \"imageUrlRef\": \"\", "
                                + "\"requestedQty\": 1, \"skuId\": \"198005\", \"skuRef\": \"FX141123TPRT-1-2\","
                                + " \"skuPrice\": 59.99, \"skuTaxPrice\": null, \"totalPrice\": 59.99, "
                                + "\"totalTaxPrice\": null, \"taxType\": null, \"currency\": null } ], "
                                + "\"fulfilmentChoice\": { \"fulfilmentType\": \"HD_PFS\", "
                                + "\"fulfilmentPrice\": null, \"fulfilmentTaxPrice\": null, \"currency\": null"
                                + ", \"pickupLocationRef\": null, \"deliveryType\": \"STANDARD\", "
                                + "\"deliveryInstruction\": \"home delivery\", \"address\": { \"locationRef\""
                                + ": \"1013\", \"companyName\": null, \"name\": \"TestUser\", \"street\": "
                                + "\"24 pacific higway\", \"city\": \"wahroonga\", \"postcode\": \"123456\", "
                                + "\"state\": \"SFO\", \"country\": \"USA\" } } }",
                        MediaType.APPLICATION_JSON));


        final FluentResponse fluentResponse = fluentClient.viewOrder("16567");
        assertThat(fluentResponse).isNotNull().isInstanceOf(OrderResponse.class);
        final OrderResponse order = (OrderResponse)fluentResponse;
        assertThat(order.getOrderId()).isEqualTo("16567");
        assertThat(order.getOrderRef()).isEqualTo("1442900789");
        assertThat(fluentResponse.getErrors()).isNull();

    }

    /**
     * @return {@link Order}
     */
    private Order createOrderRequest() {
        final Order order = new Order();
        order.setOrderRef("123456");
        final FulfilmentChoice fulfilmentChoice = new FulfilmentChoice();
        final Address address = new Address();
        address.setName("Movyn John");
        address.setStreet("23 Princes Highway");
        address.setPostcode("3216");
        address.setCity("Geelong");
        address.setState("VIC");
        address.setCountry("AU");
        fulfilmentChoice.setAddress(address);
        fulfilmentChoice.setCurrency("AUD");
        fulfilmentChoice.setDeliveryType(FulfilmentChoiceDeliveryType.STANDARD);
        fulfilmentChoice.setFulfilmentPrice(Double.valueOf(10.0));
        fulfilmentChoice.setFulfilmentType(FulfilmentChoiceType.HD_PFS);
        order.setFulfilmentChoice(fulfilmentChoice);

        final Customer customer = new Customer();
        customer.setFirstName("Movyn");
        customer.setLastName("John");
        customer.setCustomerRef("123");
        customer.setEmail("movyn.john@retail.com");
        customer.setMobile("0431759049");
        order.setCustomer(customer);

        final List<OrderItem> items = new ArrayList();
        final OrderItem item = new OrderItem();
        item.setSkuRef("P1000_BLACK");
        item.setCurrency("AUD");
        item.setRequestedQty(Integer.valueOf(2));
        item.setSkuPrice(Double.valueOf(9.0));
        item.setTotalPrice(Double.valueOf(10.0));
        items.add(item);
        final OrderItem item2 = new OrderItem();
        item2.setCurrency("AUD");
        item2.setSkuRef("889691_nocolour");
        item2.setRequestedQty(Integer.valueOf(1));
        item2.setSkuPrice(Double.valueOf(9.0));
        item2.setTotalPrice(Double.valueOf(10.0));
        items.add(item2);
        order.setItems(items);

        final List<Attribute> attributes = new ArrayList();
        final Attribute attribute = new Attribute();
        attribute.setName("SALES_APPLICATION");
        attribute.setType(AttributeType.STRING);
        attribute.setValue("WEB");
        attributes.add(attribute);

        final Attribute attribute2 = new Attribute();
        attribute2.setName("SHIP_TO_PHONE");
        attribute2.setType(AttributeType.STRING);
        attribute2.setValue("0404222333");
        attributes.add(attribute2);

        final Attribute attribute3 = new Attribute();
        attribute3.setName("PREORDER");
        attribute3.setType(AttributeType.STRING);
        attribute3.setValue("false");
        attributes.add(attribute3);

        final Attribute attribute4 = new Attribute();
        attribute4.setName("GIFTCARD_RECIPIENT");
        attribute4.setType(AttributeType.JSON);

        final List<GiftCardRecipient> giftCardRecipients = new ArrayList();
        final GiftCardRecipient giftCardRecipient = new GiftCardRecipient();
        giftCardRecipient.setSkuRef("889691_nocolour");

        final List<Recipient> recipients = new ArrayList();
        final Recipient recipient = new Recipient();
        recipient.setEmail("example@email.com");
        recipient.setFirstName("Jack");
        recipient.setLastName("Daniels");
        recipient.setMessage("gift card message");
        recipients.add(recipient);

        giftCardRecipient.setRecipients(recipients);
        giftCardRecipients.add(giftCardRecipient);
        attribute3.setValue(giftCardRecipients);
        attributes.add(attribute3);

        order.setRetailerId("1");
        order.setTotalPrice(Double.valueOf(20.0));
        order.setType(OrderType.HD);
        return order;
    }

    @Test
    public void testCreateOrderWithError() {
        mockServer.expect(requestTo("https://target.sandbox.api.fluentretail.com/api/v4.1/order/"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.INTERNAL_SERVER_ERROR).body(
                        "{\"errors\":[{\"code\":\"500\",\"message\":\"unkown server error\"}]}")
                        .contentType(MediaType.APPLICATION_JSON));

        final Order order = createOrderRequest();

        final FluentResponse fluentResponse = fluentClient.createOrder(order);
        assertThat(fluentResponse).isNotNull();
        assertThat(fluentResponse.getErrors()).hasSize(1);
        assertThat(fluentResponse.getErrors().get(0).getCode()).isEqualTo("500");
        assertThat(fluentResponse.getErrors().get(0).getMessage()).isEqualTo("unkown server error");

    }

    @Test
    public void testGetEventEntityRef() {
        final String reference = "TARGET:{retailerId}";
        final String actual = fluentClient.getEntityRef(reference);
        assertThat(actual).isEqualTo("TARGET:1");
    }

    @Test
    public void testGetEventEntityRefWithNoMatchingStr() {
        final String reference = "TARGET";
        final String actual = fluentClient.getEntityRef(reference);
        assertThat(actual).isEqualTo("TARGET");
    }

    @Test
    public void testGetEventEntityRefWithNull() {
        final String actual = fluentClient.getEntityRef(null);
        assertThat(actual).isNull();
    }

    @Test
    public void testMaskPIIInformation() throws IOException {
        final String actualJsonString = "{\"customer\":{\"lastName\":\"John\",\"customerId\":\"1234\",\"email\":\"movyn.john@retail.com\",\"customerRef\":\"12916004\",\"firstName\":\"Movyn\",\"mobile\":\"0431759049\" }";
        final String maskedJsonString = "{\"customer\":{\"lastName\":\"#####\",\"customerId\":\"1234\",\"email\":\"#####\",\"customerRef\":\"12916004\",\"firstName\":\"#####\",\"mobile\":\"#####\" }";

        final String eventSyncRequestJsonBeforeMasking = "{\n" +
                "  \"accountId\": \"targetqat\",\n" +
                "  \"attribute\": \"null\",\n" +
                "  \"entityId\": \"1235\",\n" +
                "  \"entityRef\": \"TargetShopping\",\n" +
                "  \"entitySubtype\": \"CC\",\n" +
                "  \"entityType\": \"Order\",\n" +
                "  \"name\": \"elephant toy\",\n" +
                "  \"rootEntityRef\": \"null\",\n" +
                "  \"rootEntityType\": \"Soft Toys\"\n" +
                "}";


        //test createOrder masked
        final String maskedResult = fluentClient.maskPIIInformation("createOrder", actualJsonString);
        assertThat(maskedResult).isEqualTo(maskedJsonString);

        //test eventSync unmasked
        final String eventSyncRequestJsonAfterMasking = fluentClient.maskPIIInformation("eventSync",
                eventSyncRequestJsonBeforeMasking);
        assertThat(eventSyncRequestJsonAfterMasking).isEqualTo(eventSyncRequestJsonBeforeMasking);
    }

}
