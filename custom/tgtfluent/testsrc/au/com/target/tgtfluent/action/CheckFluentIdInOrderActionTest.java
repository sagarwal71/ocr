package au.com.target.tgtfluent.action;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtcore.order.FluentOrderService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CheckFluentIdInOrderActionTest {

    @Mock
    private FluentOrderService fluentOrderService;

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private final CheckFluentIdInOrderAction action = new CheckFluentIdInOrderAction();

    @Before
    public void setup() {
        willReturn(order).given(process).getOrder();
    }

    @Test(expected = RetryLaterException.class)
    public void testExecuteInternalWhenFluentOrderExceptionIsThrown() throws RetryLaterException, Exception {
        doThrow(new FluentOrderException("Fluent create order error")).when(fluentOrderService).createOrder(order);
        action.executeInternal(process);
    }

    @Test
    public void testExecuteInternalForSuccessfulOrderCreation() throws RetryLaterException, Exception {
        given(fluentOrderService.createOrder(order)).willReturn("1");
        final String result = action.executeInternal(process);
        verify(modelService).save(order);
        assertThat(result).isEqualTo("OK");
    }


    @Test
    public void testExecuteInternalForOrderHavingFluentId() throws RetryLaterException, Exception {
        given(order.getFluentId()).willReturn("1");
        final String result = action.executeInternal(process);
        verifyZeroInteractions(fluentOrderService);
        verifyZeroInteractions(modelService);
        assertThat(result).isEqualTo("OK");
    }

}
