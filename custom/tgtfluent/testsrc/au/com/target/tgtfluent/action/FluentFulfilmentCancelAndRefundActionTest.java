/**
 * 
 */
package au.com.target.tgtfluent.action;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtfluent.service.FluentFulfilmentService;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntry;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntryList;

import com.google.common.collect.ImmutableList;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentFulfilmentCancelAndRefundActionTest {

    @Spy
    @InjectMocks
    private final FluentFulfilmentCancelAndRefundAction action = new FluentFulfilmentCancelAndRefundAction();

    @Mock
    private FluentFulfilmentService fluentFulfilmentService;

    @Mock
    private OrderProcessModel orderProcessModel;

    @Mock
    private OrderModel orderModel;

    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;

    @Before
    public void setup() {
        willReturn(orderModel).given(orderProcessModel).getOrder();
        doReturn(targetBusinessProcessService).when(action).getTargetBusinessProcessService();
    }

    @Test
    public void testExecuteActionWithBusinessProcessTriggered() throws RetryLaterException, Exception {
        final TargetOrderCancelEntryList targetOrderCancelEntryList = new TargetOrderCancelEntryList();
        final TargetOrderCancelEntry targetOrderCancelEntry1 = new TargetOrderCancelEntry();
        targetOrderCancelEntryList.setEntriesToCancel(ImmutableList.of(targetOrderCancelEntry1));
        given(fluentFulfilmentService.createTargetOrderCancelEntryList(orderModel)).willReturn(
                targetOrderCancelEntryList);
        action.execute(orderProcessModel);
        verify(targetBusinessProcessService).startFluentOrderItemCancelProcess(orderModel, targetOrderCancelEntryList);
    }

    @Test
    public void testExecuteActionWithoutBusinessProcessTriggered() throws RetryLaterException, Exception {
        final TargetOrderCancelEntryList targetOrderCancelEntryList = new TargetOrderCancelEntryList();
        given(fluentFulfilmentService.createTargetOrderCancelEntryList(orderModel)).willReturn(
                targetOrderCancelEntryList);
        action.execute(orderProcessModel);
        verifyZeroInteractions(targetBusinessProcessService);
    }

    @Test
    public void testExecuteActionIsPicked() throws RetryLaterException, Exception {
        final TargetOrderCancelEntryList targetOrderCancelEntryList = new TargetOrderCancelEntryList();
        given(fluentFulfilmentService.createTargetOrderCancelEntryList(orderModel)).willReturn(
                targetOrderCancelEntryList);
        willReturn(Boolean.TRUE).given(fluentFulfilmentService).isAnyAssignedConsignment(orderModel);
        final String result = action.execute(orderProcessModel);
        assertThat(result).isEqualTo("PICK");
    }

    @Test
    public void testExecuteActionIsZeroPick() throws RetryLaterException, Exception {
        final TargetOrderCancelEntryList targetOrderCancelEntryList = new TargetOrderCancelEntryList();
        given(fluentFulfilmentService.createTargetOrderCancelEntryList(orderModel)).willReturn(
                targetOrderCancelEntryList);
        willReturn(Boolean.FALSE).given(fluentFulfilmentService).isAnyAssignedConsignment(orderModel);
        final String result = action.execute(orderProcessModel);
        assertThat(result).isEqualTo("ZERO_PICK");
    }
}
