/**
 * 
 */
package au.com.target.tgtfluent.action;

import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfluent.service.impl.FluentFulfilmentServiceImpl;


/**
 * @author cbi
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendConsignmentEventToFluentActionTest {

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private FluentFulfilmentServiceImpl fluentFulfilmentServiceImpl;

    @InjectMocks
    private final SendConsignmentEventToFluentAction sendConsignmentEventToFluentAction = new SendConsignmentEventToFluentAction();

    @Mock
    private OrderProcessModel orderProcessModel;

    @Mock
    private OrderModel orderModel;

    @Mock
    private AbstractOrderModel order;

    @Mock
    private TargetConsignmentModel targetConsignmentModel;

    @Mock
    private TargetZoneDeliveryModeModel targetZoneDeliveryModeModel;

    @Before
    public void setup() {
        willReturn(orderModel).given(orderProcessModel).getOrder();
    }

    @Test
    public void testExecuteAction() throws RetryLaterException, Exception {
        sendConsignmentEventToFluentAction.executeAction(orderProcessModel);
        verifyNoMoreInteractions(fluentFulfilmentServiceImpl);
    }

    @Test
    public void testExecuteActionWithFluent() throws RetryLaterException, Exception {

        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        final Set<TargetConsignmentModel> consignments = new HashSet<TargetConsignmentModel>();
        willReturn(ConsignmentStatus.SHIPPED).given(targetConsignmentModel).getStatus();
        consignments.add(targetConsignmentModel);
        willReturn(targetZoneDeliveryModeModel).given(targetConsignmentModel).getDeliveryMode();
        willReturn(Boolean.TRUE).given(targetZoneDeliveryModeModel).getIsDeliveryToStore();
        willReturn(consignments).given(orderModel).getConsignments();
        when(targetConsignmentModel.getOrder()).thenReturn(order);
        when(orderProcessModel.getOrder().getFluentId()).thenReturn("123");
        sendConsignmentEventToFluentAction.setEventName("Eventname");
        sendConsignmentEventToFluentAction.executeAction(orderProcessModel);

        verify(fluentFulfilmentServiceImpl).sendConsignmentEventToFluent(targetConsignmentModel, "Eventname", false);
    }

    @Test
    public void testExecuteActionWithoutFluent() throws RetryLaterException, Exception {

        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        final Set<TargetConsignmentModel> consignments = new HashSet<TargetConsignmentModel>();
        willReturn(ConsignmentStatus.SHIPPED).given(targetConsignmentModel).getStatus();
        consignments.add(targetConsignmentModel);
        willReturn(targetZoneDeliveryModeModel).given(targetConsignmentModel).getDeliveryMode();
        willReturn(Boolean.TRUE).given(targetZoneDeliveryModeModel).getIsDeliveryToStore();
        willReturn(consignments).given(orderModel).getConsignments();
        when(targetConsignmentModel.getOrder()).thenReturn(order);
        when(order.getFluentId()).thenReturn(null);
        sendConsignmentEventToFluentAction.setEventName("Eventname");
        sendConsignmentEventToFluentAction.executeAction(orderProcessModel);

        verify(fluentFulfilmentServiceImpl, never()).sendConsignmentEventToFluent(targetConsignmentModel, "Eventname",
                false);
    }

}
