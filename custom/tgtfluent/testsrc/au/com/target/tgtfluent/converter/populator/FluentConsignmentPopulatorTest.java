/**
 * 
 */
package au.com.target.tgtfluent.converter.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfluent.constants.TgtFluentConstants.FluentCarrierName;
import au.com.target.tgtfluent.constants.TgtFluentConstants.TargetCarrierName;
import au.com.target.tgtfluent.data.Article;
import au.com.target.tgtfluent.data.Fulfilment;
import au.com.target.tgtfluent.data.FulfilmentItem;
import au.com.target.tgtfulfilment.model.ConsignmentParcelModel;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;


/**
 * @author gsing236
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentConsignmentPopulatorTest {

    @InjectMocks
    private final FluentConsignmentPopulator populator = new FluentConsignmentPopulator();

    @Mock
    private TargetConsignmentModel consignmentModel;

    private final String trackingID = "12345";

    private final String trackingUrl = "https://auspost.com.au/tracking/";

    @Test
    public void testPopulate() {
        final Fulfilment fulfilment = new Fulfilment();

        populator.populate(mockConsignment(), fulfilment);

        assertThat(fulfilment.getCarrierName()).isEqualTo(FluentCarrierName.AUSTRALIA_POST);
        assertThat(fulfilment.getCarrierTrackingId()).isEqualTo(trackingID);
        assertThat(fulfilment.getTrackingUrl()).isEqualTo(trackingUrl.concat(trackingID));

        final List<FulfilmentItem> items = fulfilment.getItems();
        assertThat(items).hasSize(1);
        assertThat(items.get(0).getOrderItemRef()).isEqualTo("abc123");
        assertThat(items.get(0).getRequestedQty()).isEqualTo(2);
        assertThat(items.get(0).getConfirmedQty()).isEqualTo(1);
        assertThat(items.get(0).getRejectedQty()).isEqualTo(1);
        final List<Article> articles = fulfilment.getArticles();
        assertThat(articles).hasSize(1);
        assertThat(articles.get(0).getHeight()).isEqualTo("2.0");
        assertThat(articles.get(0).getWidth()).isEqualTo("4.0");
        assertThat(articles.get(0).getLength()).isEqualTo("3.1");
        assertThat(articles.get(0).getWeight()).isEqualTo("2.5");

        assertThat(fulfilment.getParcelCount()).isEqualTo(2);
    }

    @Test
    public void testPopulateWithNullCarrierAndArticle() {
        final Fulfilment fulfilment = new Fulfilment();
        final ConsignmentEntryModel entryModel = mock(ConsignmentEntryModel.class);
        given(entryModel.getQuantity()).willReturn(Long.valueOf(2l));
        given(consignmentModel.getConsignmentEntries()).willReturn(Collections.singleton(entryModel));
        given(consignmentModel.getParcelCount()).willReturn(null);

        final AbstractOrderEntryModel abstractEntry = mock(AbstractOrderEntryModel.class);
        given(entryModel.getOrderEntry()).willReturn(abstractEntry);

        final ProductModel product = mock(ProductModel.class);
        given(product.getCode()).willReturn("abc123");
        given(abstractEntry.getProduct()).willReturn(product);

        populator.populate(consignmentModel, fulfilment);
        assertThat(fulfilment.getCarrierName()).isNull();
        assertThat(fulfilment.getArticles()).isNull();
        final List<FulfilmentItem> items = fulfilment.getItems();
        assertThat(items).hasSize(1);
        assertThat(items.get(0).getOrderItemRef()).isEqualTo("abc123");
        assertThat(items.get(0).getRequestedQty()).isEqualTo(2);

        assertThat(fulfilment.getParcelCount()).isEqualTo(0);
    }

    @Test
    public void testPopulateWithNoTrackingId() {
        final Fulfilment fulfilment = new Fulfilment();
        final TargetConsignmentModel consignmentModel1 = mockConsignment();
        given(consignmentModel1.getTrackingID()).willReturn(null);

        populator.populate(consignmentModel1, fulfilment);

        final List<FulfilmentItem> items = fulfilment.getItems();
        assertThat(items).hasSize(1);
        assertThat(items.get(0).getOrderItemRef()).isEqualTo("abc123");

        assertThat(fulfilment.getCarrierName()).isEqualTo(FluentCarrierName.AUSTRALIA_POST);
        assertThat(fulfilment.getCarrierTrackingId()).isEqualTo(null);
        assertThat(fulfilment.getTrackingUrl()).isEqualTo(null);
    }

    @Test
    public void testPopulateWithNoCarrierTrackingUrl() {
        final Fulfilment fulfilment = new Fulfilment();
        final TargetCarrierModel carrier = mock(TargetCarrierModel.class);
        given(carrier.getCode()).willReturn(TargetCarrierName.AUSTRALIA_POST_INSTORE_HD);
        given(carrier.getTrackingUrl()).willReturn(null);
        given(consignmentModel.getTargetCarrier()).willReturn(carrier);
        given(consignmentModel.getParcelCount()).willReturn(Integer.valueOf(2));
        given(consignmentModel.getTrackingID()).willReturn(trackingID);

        populator.populate(consignmentModel, fulfilment);

        final List<FulfilmentItem> items = fulfilment.getItems();
        assertThat(items).hasSize(0);

        assertThat(fulfilment.getCarrierName()).isEqualTo(FluentCarrierName.AUSTRALIA_POST);
        assertThat(fulfilment.getCarrierTrackingId()).isEqualTo(trackingID);
        assertThat(fulfilment.getTrackingUrl()).isEqualTo(null);
    }

    @Test
    public void testPopulateWithBlankTrackingId() {
        final Fulfilment fulfilment = new Fulfilment();
        final TargetCarrierModel carrier = mock(TargetCarrierModel.class);
        given(carrier.getCode()).willReturn(TargetCarrierName.AUSTRALIA_POST_INSTORE_HD);
        given(carrier.getTrackingUrl()).willReturn(trackingUrl);
        given(consignmentModel.getTargetCarrier()).willReturn(carrier);
        given(consignmentModel.getParcelCount()).willReturn(Integer.valueOf(2));
        given(consignmentModel.getTrackingID()).willReturn(" ");

        populator.populate(consignmentModel, fulfilment);

        final List<FulfilmentItem> items = fulfilment.getItems();
        assertThat(items).hasSize(0);

        assertThat(fulfilment.getCarrierName()).isEqualTo(FluentCarrierName.AUSTRALIA_POST);
        assertThat(fulfilment.getTrackingUrl()).isEqualTo(null);
    }

    @Test
    public void testPopulateWithBlankTrackingUrl() {
        final Fulfilment fulfilment = new Fulfilment();
        final TargetCarrierModel carrier = mock(TargetCarrierModel.class);
        given(carrier.getCode()).willReturn(TargetCarrierName.AUSTRALIA_POST_INSTORE_HD);
        given(carrier.getTrackingUrl()).willReturn(" ");
        given(consignmentModel.getTargetCarrier()).willReturn(carrier);
        given(consignmentModel.getParcelCount()).willReturn(Integer.valueOf(2));
        given(consignmentModel.getTrackingID()).willReturn(trackingID);

        populator.populate(consignmentModel, fulfilment);

        final List<FulfilmentItem> items = fulfilment.getItems();
        assertThat(items).hasSize(0);

        assertThat(fulfilment.getCarrierName()).isEqualTo(FluentCarrierName.AUSTRALIA_POST);
        assertThat(fulfilment.getCarrierTrackingId()).isEqualTo(trackingID);
        assertThat(fulfilment.getTrackingUrl()).isEqualTo(null);
    }


    private TargetConsignmentModel mockConsignment() {

        final TargetCarrierModel carrier = mock(TargetCarrierModel.class);
        given(carrier.getCode()).willReturn(TargetCarrierName.AUSTRALIA_POST_INSTORE_HD);
        given(carrier.getTrackingUrl()).willReturn(trackingUrl);

        given(consignmentModel.getTargetCarrier()).willReturn(carrier);
        given(consignmentModel.getParcelCount()).willReturn(Integer.valueOf(2));
        given(consignmentModel.getTrackingID()).willReturn(trackingID);

        final ConsignmentEntryModel entryModel = mock(ConsignmentEntryModel.class);
        given(entryModel.getQuantity()).willReturn(Long.valueOf(2l));
        given(entryModel.getShippedQuantity()).willReturn(Long.valueOf(1l));
        given(consignmentModel.getConsignmentEntries()).willReturn(Collections.singleton(entryModel));

        final AbstractOrderEntryModel abstractEntry = mock(AbstractOrderEntryModel.class);
        given(entryModel.getOrderEntry()).willReturn(abstractEntry);

        final ProductModel product = mock(ProductModel.class);
        given(product.getCode()).willReturn("abc123");
        given(abstractEntry.getProduct()).willReturn(product);


        final ConsignmentParcelModel parcelModel = mock(ConsignmentParcelModel.class);
        given(parcelModel.getActualWeight()).willReturn(Double.valueOf("2.5"));
        given(parcelModel.getHeight()).willReturn(Double.valueOf("2"));
        given(parcelModel.getWidth()).willReturn(Double.valueOf("4.0"));
        given(parcelModel.getLength()).willReturn(Double.valueOf("3.1"));

        given(consignmentModel.getParcelsDetails()).willReturn(Collections.singleton(parcelModel));

        return consignmentModel;
    }
}
