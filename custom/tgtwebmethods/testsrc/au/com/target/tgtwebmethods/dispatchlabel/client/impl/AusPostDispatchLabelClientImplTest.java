/**
 * 
 */
package au.com.target.tgtwebmethods.dispatchlabel.client.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Matchers;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtauspost.integration.dto.AuspostRequestDTO;
import au.com.target.tgtfulfilment.integration.dto.IntegrationError;
import au.com.target.tgtfulfilment.integration.dto.LabelResponseDTO;
import au.com.target.tgtwebmethods.rest.ErrorDTO;
import au.com.target.tgtwebmethods.rest.JSONRequest;
import au.com.target.tgtwebmethods.rest.LabelJSONResponse;


/**
 * Unit tests to check whether there is a response object returned for a set of scenarios.
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AusPostDispatchLabelClientImplTest {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private final AusPostDispatchLabelClientImpl auspostClient = new AusPostDispatchLabelClientImpl();

    @Before
    public void setup() {
        auspostClient.setServiceEndpoint("dispatchLabelService");
    }

    @Test
    public void testNullResponse() {
        final AuspostRequestDTO request = new AuspostRequestDTO();
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                  Matchers.<Class<Object>>any())).willReturn(null);
        final LabelResponseDTO response = auspostClient.retrieveDispatchLabel(request);
        Assert.assertNotNull(response);
        Assert.assertTrue(!response.isSuccess());
        Assert.assertEquals(IntegrationError.COMMUNICATION_ERROR, response.getErrorType());
        Mockito.verify(restTemplate).postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
          Matchers.<Class<Object>>any());
        Mockito.verifyNoMoreInteractions(restTemplate);
    }

    @Test
    public void testFailedResponseWithoutError() {
        final AuspostRequestDTO request = new AuspostRequestDTO();
        final LabelJSONResponse expectedResponse = new LabelJSONResponse();
        expectedResponse.setSuccess(false);
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                  Matchers.<Class<Object>>any())).willReturn(expectedResponse);
        final LabelResponseDTO response = auspostClient.retrieveDispatchLabel(request);
        Assert.assertNotNull(response);
        Assert.assertTrue(!response.isSuccess());
        Assert.assertEquals(IntegrationError.REMOTE_SERVICE_ERROR, response.getErrorType());
        Mockito.verify(restTemplate).postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
          Matchers.<Class<Object>>any());
        Mockito.verifyNoMoreInteractions(restTemplate);
    }

    @Test
    public void testFailedResponseWithError() {
        final AuspostRequestDTO request = new AuspostRequestDTO();
        final LabelJSONResponse expectedResponse = new LabelJSONResponse();
        expectedResponse.setSuccess(false);
        final ErrorDTO error = new ErrorDTO();
        final String code = "CODE-1";
        error.setCode(code);
        final String message = "This is an error";
        error.setValue(message);
        expectedResponse.setErrors(Collections.singletonList(error));
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                  Matchers.<Class<Object>>any())).willReturn(expectedResponse);
        final LabelResponseDTO response = auspostClient.retrieveDispatchLabel(request);
        Assert.assertNotNull(response);
        Assert.assertTrue(!response.isSuccess());
        Assert.assertEquals(IntegrationError.MIDDLEWARE_ERROR, response.getErrorType());
        Mockito.verify(restTemplate).postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
          Matchers.<Class<Object>>any());
        Mockito.verifyNoMoreInteractions(restTemplate);
    }

    @Test
    public void testThrowsRestClientException() {
        final AuspostRequestDTO request = new AuspostRequestDTO();
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                  Matchers.<Class<Object>>any())).willThrow(new RestClientException("RestClientException"));
        final LabelResponseDTO response = auspostClient.retrieveDispatchLabel(request);
        Assert.assertNotNull(response);
        Assert.assertTrue(!response.isSuccess());
        Assert.assertEquals(IntegrationError.CONNECTION_ERROR, response.getErrorType());
        Mockito.verify(restTemplate).postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
          Matchers.<Class<Object>>any());
        Mockito.verifyNoMoreInteractions(restTemplate);
    }

    @Test
    public void testThrowsHttpMessageNotReadableException() {
        final AuspostRequestDTO request = new AuspostRequestDTO();
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                  Matchers.<Class<Object>>any())).willThrow(
                new HttpMessageNotReadableException("HttpMessageNotReadableException"));
        final LabelResponseDTO response = auspostClient.retrieveDispatchLabel(request);
        Assert.assertNotNull(response);
        Assert.assertTrue(!response.isSuccess());
        Assert.assertEquals(IntegrationError.COMMUNICATION_ERROR, response.getErrorType());
        Mockito.verify(restTemplate).postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
          Matchers.<Class<Object>>any());
        Mockito.verifyNoMoreInteractions(restTemplate);
    }

    @Test
    public void testFileReceivedIsEmpty() {
        final AuspostRequestDTO request = new AuspostRequestDTO();
        final LabelJSONResponse expectedResponse = new LabelJSONResponse();
        expectedResponse.setSuccess(true);
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                  Matchers.<Class<Object>>any())).willReturn(expectedResponse);
        final LabelResponseDTO response = auspostClient.retrieveDispatchLabel(request);
        Assert.assertNotNull(response);
        Assert.assertTrue(!response.isSuccess());
        Assert.assertEquals(IntegrationError.COMMUNICATION_ERROR, response.getErrorType());
        Assert.assertEquals("NOT_SPECIFIED", response.getErrorCode());
        Mockito.verify(restTemplate).postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
          Matchers.<Class<Object>>any());
        Mockito.verifyNoMoreInteractions(restTemplate);
    }

    @Test
    public void testSuccess() {
        final AuspostRequestDTO request = new AuspostRequestDTO();
        final LabelJSONResponse expectedResponse = new LabelJSONResponse();
        expectedResponse.setSuccess(true);
        expectedResponse.setAttachment("test".getBytes());
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                  Matchers.<Class<Object>>any())).willReturn(expectedResponse);
        final LabelResponseDTO response = auspostClient.retrieveDispatchLabel(request);
        Assert.assertNotNull(response);
        Mockito.verify(restTemplate).postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
          Matchers.<Class<Object>>any());
        Mockito.verifyNoMoreInteractions(restTemplate);
    }
}
