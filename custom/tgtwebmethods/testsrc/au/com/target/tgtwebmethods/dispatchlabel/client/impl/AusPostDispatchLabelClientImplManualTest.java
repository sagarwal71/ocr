/**
 * 
 */
package au.com.target.tgtwebmethods.dispatchlabel.client.impl;

import de.hybris.bootstrap.annotations.ManualTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.util.Assert;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

import au.com.target.tgtauspost.integration.dto.AddressDTO;
import au.com.target.tgtauspost.integration.dto.AuspostRequestDTO;
import au.com.target.tgtauspost.integration.dto.ConsignmentDTO;
import au.com.target.tgtauspost.integration.dto.ConsignmentDetailsDTO;
import au.com.target.tgtauspost.integration.dto.DeliveryAddressDTO;
import au.com.target.tgtauspost.integration.dto.LabelDTO;
import au.com.target.tgtauspost.integration.dto.ParcelDetailsDTO;
import au.com.target.tgtauspost.integration.dto.StoreDTO;
import au.com.target.tgtfulfilment.dto.ConsignmentParcelDTO;
import au.com.target.tgtfulfilment.integration.dto.LabelResponseDTO;


/**
 * Checks whether a connection can be established to web methods for AusPost.
 * 
 * @author jjayawa1
 *
 */
@ManualTest
public class AusPostDispatchLabelClientImplManualTest extends ServicelayerTransactionalTest {

    @Resource
    private AusPostDispatchLabelClientImpl dispatchLabelClient;

    private final AuspostRequestDTO labelRequest = new AuspostRequestDTO();

    /**
     * Setup ManifestRequestDTO
     */
    @Before
    public void setupManifestRequestData() {
        final StoreDTO storeDTO = new StoreDTO();

        final LabelDTO label = new LabelDTO();
        label.setBranding("false");
        label.setLayout("A4-1pp");

        storeDTO.setLabel(label);

        final ConsignmentDTO consignmentDTO1 = new ConsignmentDTO();

        final ConsignmentDetailsDTO consignmentDetailsDTO1 = new ConsignmentDetailsDTO();

        final ParcelDetailsDTO parcelDetailsDTO1 = new ParcelDetailsDTO();
        final ParcelDetailsDTO parcelDetailsDTO2 = new ParcelDetailsDTO();
        final ParcelDetailsDTO parcelDetailsDTO3 = new ParcelDetailsDTO();
        final ParcelDetailsDTO parcelDetailsDTO4 = new ParcelDetailsDTO();

        final ConsignmentParcelDTO consignmentParcelDTO1 = new ConsignmentParcelDTO();
        final ConsignmentParcelDTO consignmentParcelDTO2 = new ConsignmentParcelDTO();
        final ConsignmentParcelDTO consignmentParcelDTO3 = new ConsignmentParcelDTO();
        final ConsignmentParcelDTO consignmentParcelDTO4 = new ConsignmentParcelDTO();

        consignmentParcelDTO1.setHeight("1.0");
        consignmentParcelDTO1.setLength("1.0");
        consignmentParcelDTO1.setWidth("1.0");
        consignmentParcelDTO1.setWeight("1.0");

        consignmentParcelDTO2.setHeight("2.0");
        consignmentParcelDTO2.setLength("2.0");
        consignmentParcelDTO2.setWidth("2.0");
        consignmentParcelDTO2.setWeight("2.0");

        consignmentParcelDTO3.setHeight("3.0");
        consignmentParcelDTO3.setLength("3.0");
        consignmentParcelDTO3.setWidth("3.0");
        consignmentParcelDTO3.setWeight("3.0");

        consignmentParcelDTO4.setHeight("4.0");
        consignmentParcelDTO4.setLength("4.0");
        consignmentParcelDTO4.setWidth("4.0");
        consignmentParcelDTO4.setWeight("4.0");

        parcelDetailsDTO1.setParcelDetails(consignmentParcelDTO1);
        parcelDetailsDTO2.setParcelDetails(consignmentParcelDTO2);
        parcelDetailsDTO3.setParcelDetails(consignmentParcelDTO3);
        parcelDetailsDTO4.setParcelDetails(consignmentParcelDTO4);

        final List<ParcelDetailsDTO> parcels1 = new ArrayList<>();
        parcels1.add(parcelDetailsDTO1);
        parcels1.add(parcelDetailsDTO2);

        final List<ParcelDetailsDTO> parcels2 = new ArrayList<>();
        parcels2.add(parcelDetailsDTO3);
        parcels2.add(parcelDetailsDTO4);

        consignmentDetailsDTO1.setTrackingId("JDQ1234561");
        consignmentDetailsDTO1.setDeliveryAddress(getDeliveryAddressData());
        consignmentDetailsDTO1.setParcels(parcels1);


        consignmentDTO1.setConsignment(consignmentDetailsDTO1);


        storeDTO.setName("TargetGeelong");
        storeDTO.setChargeToAccount("6616510");
        storeDTO.setMerchantLocationId("JDQ");
        storeDTO.setChargeCode("S1");
        storeDTO.setProductCode("60");
        storeDTO.setServiceCode("02");
        storeDTO.setAddress(getAddressData());

        storeDTO.setConsignment(consignmentDetailsDTO1);

        labelRequest.setStore(storeDTO);
    }

    @Test
    public void testSuccess() throws JsonGenerationException, JsonMappingException, IOException {
        final LabelResponseDTO response = dispatchLabelClient.retrieveDispatchLabel(labelRequest);
        Assert.notNull(response, "response must not be null");
        Assert.isTrue(response.isSuccess(), "response must be true");
    }

    @Test
    public void testError() throws JsonGenerationException, JsonMappingException, IOException {
        final LabelResponseDTO response = dispatchLabelClient.retrieveDispatchLabel(new AuspostRequestDTO());
        Assert.notNull(response, "response must not be null");
        Assert.isTrue(!response.isSuccess(), "response must be false");
    }

    /**
     * Get Address Data
     * 
     * @return AddressDTO
     */
    private AddressDTO getAddressData() {
        final AddressDTO addressDTO = new AddressDTO();
        addressDTO.setAddressLine1("1 Main Street");
        addressDTO.setAddressLine2("add line2");
        addressDTO.setSuburb("Melbourne");
        addressDTO.setState("VIC");
        addressDTO.setPostcode("3000");
        addressDTO.setCountry("Australia");
        return addressDTO;
    }

    /**
     * Get DeliveryAddressDTO
     * 
     * @return DeliveryAddressDTO
     */
    private DeliveryAddressDTO getDeliveryAddressData() {
        final DeliveryAddressDTO deliveryAddressDTO = new DeliveryAddressDTO();
        deliveryAddressDTO.setName("Camberwell");
        deliveryAddressDTO.setCompanyName("Target");
        deliveryAddressDTO.setAddressLine1("2 Station Road");
        deliveryAddressDTO.setAddressLine2("add line2");
        deliveryAddressDTO.setSuburb("Sydney");
        deliveryAddressDTO.setState("NSW");
        deliveryAddressDTO.setPostcode("2000");
        deliveryAddressDTO.setCountry("Australia");
        return deliveryAddressDTO;
    }

}
