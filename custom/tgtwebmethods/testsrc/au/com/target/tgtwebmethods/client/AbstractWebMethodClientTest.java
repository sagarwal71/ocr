/**
 * 
 */
package au.com.target.tgtwebmethods.client;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Matchers;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtfulfilment.integration.dto.BaseResponseDTO;
import au.com.target.tgtfulfilment.integration.dto.IntegrationError;
import au.com.target.tgtwebmethods.rest.ErrorDTO;
import au.com.target.tgtwebmethods.rest.JSONRequest;
import au.com.target.tgtwebmethods.rest.JSONResponse;


/**
 * Unit tests to test common implementations in abstract web method client.
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AbstractWebMethodClientTest {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private final AbstractWebMethodsClient client = new AbstractWebMethodsClient() {
        private RestTemplate restTemplate;

        @Override
        protected String getServiceEndpoint() {
            return "ENDPOINT";
        }

        @Override
        protected RestTemplate getRestTemplate() {
            return restTemplate;
        }

        /**
         * @param restTemplate
         *            the restTemplate to set
         */
        @SuppressWarnings("unused")
        protected void setRestTemplate(final RestTemplate restTemplate) {
            this.restTemplate = restTemplate;
        }
    };

    private final AbstractWebMethodsClient clientWithNoEndPoint = new AbstractWebMethodsClient() {

        @Override
        protected String getServiceEndpoint() {
            return "";
        }

        @Override
        protected RestTemplate getRestTemplate() {
            return new RestTemplate();
        }
    };

    private final AbstractWebMethodsClient clientWithNoRestTemplate = new AbstractWebMethodsClient() {

        @Override
        protected String getServiceEndpoint() {
            return "ENDPOINT";
        }

        @Override
        protected RestTemplate getRestTemplate() {
            return null;
        }
    };

    @Test
    public void testSendWithNoRestTemplate() {
        final BaseResponseDTO response = new BaseResponseDTO();
        clientWithNoRestTemplate.send(new Object(), response, JSONResponse.class);
        Assert.assertFalse(response.isSuccess());
        Assert.assertEquals(IntegrationError.CONNECTION_ERROR, response.getErrorType());
        Assert.assertEquals("NOT_SPECIFIED", response.getErrorCode());
    }

    @Test
    public void testSendWithNoEndPoint() {
        final BaseResponseDTO response = new BaseResponseDTO();
        clientWithNoEndPoint.send(new Object(), response, JSONResponse.class);
        Assert.assertFalse(response.isSuccess());
        Assert.assertEquals(IntegrationError.CONNECTION_ERROR, response.getErrorType());
        Assert.assertEquals("NOT_SPECIFIED", response.getErrorCode());
    }

    @Test
    public void testThrowRestClientException() {
        final BaseResponseDTO response = new BaseResponseDTO();
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                        Matchers.<Class<Object>>any())).willThrow(new RestClientException("RestClientException"));
        client.send(new Object(), response, JSONResponse.class);
        Assert.assertFalse(response.isSuccess());
        Assert.assertEquals(IntegrationError.CONNECTION_ERROR, response.getErrorType());
        Assert.assertEquals("RestClientException", response.getErrorCode());
    }

    @Test
    public void testThrowHttpMessageNotReadableException() {
        final BaseResponseDTO response = new BaseResponseDTO();
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                  Matchers.<Class<Object>>any())).willThrow(
                new HttpMessageNotReadableException("HttpMessageNotReadableException"));
        client.send(new Object(), response, JSONResponse.class);
        Assert.assertFalse(response.isSuccess());
        Assert.assertEquals(IntegrationError.COMMUNICATION_ERROR, response.getErrorType());
        Assert.assertEquals("HttpMessageNotReadableException", response.getErrorCode());
    }

    @Test
    public void testThrowRuntimeException() {
        final BaseResponseDTO response = new BaseResponseDTO();
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                  Matchers.<Class<Object>>any())).willThrow(new RuntimeException("Exception"));
        client.send(new Object(), response, JSONResponse.class);
        Assert.assertFalse(response.isSuccess());
        Assert.assertEquals(IntegrationError.COMMUNICATION_ERROR, response.getErrorType());
    }

    @Test
    public void testReturnsNullJSONResponse() {
        final BaseResponseDTO response = new BaseResponseDTO();
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                  Matchers.<Class<Object>>any())).willReturn(null);
        client.send(new Object(), response, JSONResponse.class);
        Assert.assertFalse(response.isSuccess());
        Assert.assertEquals(IntegrationError.COMMUNICATION_ERROR, response.getErrorType());
        Assert.assertEquals("JSONResponseNull", response.getErrorCode());
    }

    @Test
    public void testFailedResponseWithErros() {
        final BaseResponseDTO response = new BaseResponseDTO();
        final JSONResponse jsonResponse = new JSONResponse();
        final List<ErrorDTO> errors = new ArrayList<>();
        errors.add(new ErrorDTO());
        jsonResponse.setErrors(errors);
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                  Matchers.<Class<Object>>any())).willReturn(jsonResponse);
        client.send(new Object(), response, JSONResponse.class);
        Assert.assertFalse(response.isSuccess());
        Assert.assertEquals(IntegrationError.MIDDLEWARE_ERROR, response.getErrorType());
    }

    @Test
    public void testFailedResponseWithoutErros() {
        final BaseResponseDTO response = new BaseResponseDTO();
        final JSONResponse jsonResponse = new JSONResponse();
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(JSONRequest.class),
                  Matchers.<Class<Object>>any())).willReturn(jsonResponse);
        client.send(new Object(), response, JSONResponse.class);
        Assert.assertFalse(response.isSuccess());
        Assert.assertEquals(IntegrationError.REMOTE_SERVICE_ERROR, response.getErrorType());
        Assert.assertEquals("NOT_SPECIFIED", response.getErrorCode());
    }
}
