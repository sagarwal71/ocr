/**
 * 
 */
package au.com.target.tgtwebmethods.exacttarget.client.impl;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.ResponseCreator;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtcore.sms.dto.TargetSendSmsResponseDto;
import au.com.target.tgtcore.sms.enums.TargetSendSmsResponseType;
import au.com.target.tgtwebmethods.constants.TgtwebmethodsConstants;


/**
 * @author mjanarth
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:tgtwebmethods-spring-test.xml" })
public class TargetSendSmsCncNotificationClientImplTest {

    private static final String NULL_MSG_XMLPAYLOAD_REQ = "<exactTargetSendSmsRequest>"
            + "<mobileNumber>61123456788</mobileNumber>"
            + "<timeZone>+1100</timeZone><smsText></smsText>"
            + "</exactTargetSendSmsRequest>";
    private static final String NULL_MOBNO_XMLPAYLOAD_REQ = "<exactTargetSendSmsRequest>"
            + "<mobileNumber></mobileNumber>"
            + "<timeZone>+1100</timeZone><smsText>Testing Service</smsText>"
            + "</exactTargetSendSmsRequest>";
    private static final String NULL_TIMEZONE_XMLPAYLOAD_REQ = "<exactTargetSendSmsRequest>"
            + "<mobileNumber>61123456789</mobileNumber><timeZone></timeZone>"
            + "<smsText>Testing Service</smsText>"
            + "</exactTargetSendSmsRequest>";

    private static final String RESPOSNE_XML = "<exactTargetSendSmsResponse>"
            + "<responseMessage>SMS sent successfully</responseMessage><responseCode>0</responseCode>"
            + "</exactTargetSendSmsResponse>";
    private static final String RESPOSNE_XML_NULL_VALUES = "<exactTargetSendSmsResponse>"
            + "<responseMessage>The request can't be null or empty</responseMessage>"
            + "<responseCode>-1</responseCode></exactTargetSendSmsResponse>";

    private static final String VALID_XMLPAYLOAD_REQ = "<exactTargetSendSmsRequest>"
            + "<mobileNumber>61123456788</mobileNumber>"
            + "<timeZone>+1100</timeZone><smsText>Testing Service</smsText></exactTargetSendSmsRequest>";

    private MockRestServiceServer mockServer;


    @Autowired
    private TargetSendSmsCncNotificationClientImpl targetSendSmsCncNotificationClient;

    @Autowired
    private RestTemplate exactTargetWebmethodRestTemplate;


    @Before
    public void setUp() {
        mockServer = MockRestServiceServer.createServer(exactTargetWebmethodRestTemplate);

    }

    /**
     * Test the valid request
     */
    @Test
    public void testSendSmsValidRequest() {
        final String responseXml = RESPOSNE_XML;
        mockServer
                .expect(requestTo("http://localhost:8998/rest/TGT_IN_SmsNotification.services.provider.restful.sendCncSmsNotification"))
                .andExpect(content().xml(VALID_XMLPAYLOAD_REQ))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(responseXml, MediaType.APPLICATION_XML));
        targetSendSmsCncNotificationClient.setWebmethodsBaseUrl("http://localhost:8998");
        targetSendSmsCncNotificationClient
                .setExactTargetSendSmsUrl
                ("/rest/TGT_IN_SmsNotification.services.provider.restful.sendCncSmsNotification");
        final TargetSendSmsResponseDto response = targetSendSmsCncNotificationClient.sendSmsNotification("61123456788",
                "Testing Service", "+1100");
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getResponseType(), TargetSendSmsResponseType.SUCCESS);
        mockServer.verify();
    }

    /**
     * Test the empty mobile number
     */
    @Test
    public void testSendSmsWithEmptyMobileNumberRequest() {
        final String responseXml = RESPOSNE_XML_NULL_VALUES;
        mockServer
                .expect(requestTo("http://localhost:8998/rest/TGT_IN_SmsNotification.services.provider.restful.sendCncSmsNotification"))
                .andExpect(content().xml(NULL_MOBNO_XMLPAYLOAD_REQ))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(responseXml, MediaType.APPLICATION_XML));
        targetSendSmsCncNotificationClient.setWebmethodsBaseUrl("http://localhost:8998");
        targetSendSmsCncNotificationClient
                .setExactTargetSendSmsUrl
                ("/rest/TGT_IN_SmsNotification.services.provider.restful.sendCncSmsNotification");
        final TargetSendSmsResponseDto response = targetSendSmsCncNotificationClient.sendSmsNotification("",
                "Testing Service", "+1100");
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getResponseCode(),
                TgtwebmethodsConstants.CommonErrorCodes.INVALID_ERRORCODE);
        Assert.assertEquals(response.getResponseType(), TargetSendSmsResponseType.INVALID);

    }

    /**
     * Test the empty timezone number
     */
    @Test
    public void testSendSmsWithEmptyTimeZoneRequest() {
        final String responseXml = RESPOSNE_XML_NULL_VALUES;
        mockServer
                .expect(requestTo("http://localhost:8998/rest/TGT_IN_SmsNotification.services.provider.restful.sendCncSmsNotification"))
                .andExpect(content().xml(NULL_TIMEZONE_XMLPAYLOAD_REQ))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(responseXml, MediaType.APPLICATION_XML));
        targetSendSmsCncNotificationClient.setWebmethodsBaseUrl("http://localhost:8998");
        targetSendSmsCncNotificationClient
                .setExactTargetSendSmsUrl
                ("/rest/TGT_IN_SmsNotification.services.provider.restful.sendCncSmsNotification");
        final TargetSendSmsResponseDto response = targetSendSmsCncNotificationClient.sendSmsNotification("",
                "Testing Service", "+1100");
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getResponseCode(),
                TgtwebmethodsConstants.CommonErrorCodes.INVALID_ERRORCODE);
        Assert.assertEquals(response.getResponseType(), TargetSendSmsResponseType.INVALID);
    }


    /**
     * Test the empty messge
     */
    @Test
    public void testSendSmsWithEmptyMessageRequest() {
        final String responseXml = RESPOSNE_XML_NULL_VALUES;
        mockServer
                .expect(requestTo("http://localhost:8998/rest/TGT_IN_SmsNotification.services.provider.restful.sendCncSmsNotification"))
                .andExpect(content().xml(NULL_MSG_XMLPAYLOAD_REQ))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(responseXml, MediaType.APPLICATION_XML));
        targetSendSmsCncNotificationClient.setWebmethodsBaseUrl("http://localhost:8998");
        targetSendSmsCncNotificationClient
                .setExactTargetSendSmsUrl
                ("/rest/TGT_IN_SmsNotification.services.provider.restful.sendCncSmsNotification");
        final TargetSendSmsResponseDto response = targetSendSmsCncNotificationClient.sendSmsNotification("",
                "Testing Service", "+1100");
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getResponseCode(),
                TgtwebmethodsConstants.CommonErrorCodes.INVALID_ERRORCODE);
        Assert.assertEquals(response.getResponseType(), TargetSendSmsResponseType.INVALID);
    }

    /**
     * Test the timeout scenario
     */
    @Test
    public void testSendSmsTimeOut() {
        mockServer
                .expect(requestTo("http://localhost:8998/rest/TGT_IN_SmsNotification.services.provider.restful.sendCncSmsNotification"))
                .andExpect(content().xml(VALID_XMLPAYLOAD_REQ))
                .andExpect(method(HttpMethod.POST))
                .andRespond(new TimeoutResponseCreator());
        targetSendSmsCncNotificationClient.setWebmethodsBaseUrl("http://localhost:8998");
        targetSendSmsCncNotificationClient
                .setExactTargetSendSmsUrl("/rest/TGT_IN_SmsNotification.services.provider.restful.sendCncSmsNotification");
        final TargetSendSmsResponseDto response = targetSendSmsCncNotificationClient.sendSmsNotification("61123456788",
                "Testing Service", "+1100");
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getResponseCode(),
                TgtwebmethodsConstants.CommonErrorCodes.UNAVAILABLE_ERRORCODE);
        Assert.assertEquals(response.getResponseType(), TargetSendSmsResponseType.UNAVAILABLE);
    }

    class TimeoutResponseCreator implements ResponseCreator {
        @Override
        public ClientHttpResponse createResponse(final ClientHttpRequest request) throws IOException {
            throw new RestClientException("Testing timeout exception");
        }

        public TimeoutResponseCreator withTimeout() {
            return new TimeoutResponseCreator();
        }

    }

}
