/**
 * 
 */
package au.com.target.tgtwebmethods.logger;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ConsignmentDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ConsignmentEntryDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.CustomerDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.OrderDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ProductDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.RecipientDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseRequest;


/**
 * @author pratik
 *
 */
@UnitTest
public class WMSendToWarehouseLoggerTest {

    private static final Logger LOG = Logger.getLogger(WMSendToWarehouseLoggerTest.class);

    private final WMSendToWarehouseLogger wmSendToWarehouselogger = new WMSendToWarehouseLogger() {

        @Override
        protected String getLogMessage(final SendToWarehouseRequest sendToWarehouseRequest, final String errorCode,
                final String errorValue, final boolean requireConsignmentDetails) {
            logMessage = super.getLogMessage(sendToWarehouseRequest, errorCode, errorValue, requireConsignmentDetails);
            return logMessage;
        }
    };

    private String logMessage;

    private SendToWarehouseRequest request;


    @Test
    public void testLogMessageWithMultipleProductsAtErrorLevel() {
        createMockRequest(2);
        wmSendToWarehouselogger.logMessage(LOG, request, "400208", "400208:Insufficient inventory", true, true);

        Assert.assertNotNull(logMessage);
        Assert.assertTrue(logMessage.contains("errorCode=400208"));
        Assert.assertTrue(logMessage.contains("orderCode=1234"));
        Assert.assertTrue(logMessage.contains("warehouseId=Incomm"));
        Assert.assertTrue(logMessage.contains("consignmentCode=a1234"));
        Assert.assertTrue(logMessage.contains("errorMessage=400208:Insufficient inventory"));
        Assert.assertTrue(logMessage.contains("PGC1009_GOLF2_11"));
        Assert.assertTrue(logMessage.contains("PGC1009_GOLF2_10"));
    }

    @Test
    public void testLogMessageWithSingleProductsAtErrorLevel() {
        createMockRequest(1);
        wmSendToWarehouselogger.logMessage(LOG, request, "400208", "400208:Insufficient inventory", true, true);

        Assert.assertNotNull(logMessage);
        Assert.assertTrue(logMessage.contains("errorCode=400208"));
        Assert.assertTrue(logMessage.contains("orderCode=1234"));
        Assert.assertTrue(logMessage.contains("warehouseId=Incomm"));
        Assert.assertTrue(logMessage.contains("consignmentCode=a1234"));
        Assert.assertTrue(logMessage.contains("errorMessage=400208:Insufficient inventory"));
        Assert.assertTrue(logMessage.contains("PGC1009_GOLF2_10"));
    }

    @Test
    public void testLogMessageWithErrorMessage() {
        createMockRequest(1);
        wmSendToWarehouselogger.logMessage(LOG, request, "103", "103:Order already exists", false, false);

        Assert.assertNotNull(logMessage);
        Assert.assertTrue(logMessage.contains("errorCode=103"));
        Assert.assertTrue(logMessage.contains("orderCode=1234"));
        Assert.assertTrue(logMessage.contains("warehouseId=Incomm"));
        Assert.assertTrue(logMessage.contains("consignmentCode=a1234"));
        Assert.assertTrue(logMessage.contains("errorMessage=103:Order already exists"));
    }

    @Test
    public void testLogMessageWitEmptyRequest() {
        wmSendToWarehouselogger.logMessage(LOG, new SendToWarehouseRequest(), "103", "103:Order already exists", true,
                false);

        Assert.assertNotNull(logMessage);
        Assert.assertTrue(logMessage.contains("errorCode=103"));
        Assert.assertTrue(logMessage.contains("orderCode="));
        Assert.assertTrue(logMessage.contains("warehouseId="));
        Assert.assertTrue(logMessage.contains("consignmentCode="));
        Assert.assertTrue(logMessage.contains("errorMessage=103:Order already exists"));
        Assert.assertTrue(logMessage.contains("consignmentEntries="));
    }

    private void createMockRequest(final int numberOfProds) {
        request = new SendToWarehouseRequest();
        final ConsignmentDTO consignment = new ConsignmentDTO();
        final long orderId = 1234;
        consignment.setId("a" + orderId);
        consignment.setWarehouseId("Incomm");

        final CustomerDTO customer = new CustomerDTO();
        customer.setFirstName("Jane");
        customer.setLastName("Doe");
        customer.setEmailAddress("jane.doe@test.com");
        customer.setCountry("AU");

        final OrderDTO order = new OrderDTO();
        order.setId(Long.toString(orderId));
        order.setCustomer(customer);

        final RecipientDTO recipient1 = new RecipientDTO();
        recipient1.setFirstName("James");
        recipient1.setLastName("Doe");
        recipient1.setEmailAddress("james.doe@test.com");
        recipient1.setMessage("Just Saying Hello1");

        final RecipientDTO recipient2 = new RecipientDTO();
        recipient2.setFirstName("Joe");
        recipient2.setLastName("Doe");
        recipient2.setEmailAddress("joe.doe@test.com");
        recipient2.setMessage("Just Saying Hello2");

        final List<RecipientDTO> recipients = new ArrayList<>();
        recipients.add(recipient1);
        recipients.add(recipient2);

        final List<ConsignmentEntryDTO> consignmentEntries = new ArrayList<>();

        for (int i = 0; i < numberOfProds; i++) {
            final ProductDTO product = new ProductDTO();
            product.setId("PGC1009_GOLF2_1" + i);
            product.setName("Incomm Venue 1 Golf");
            product.setDenomination("$10");
            product.setBrandId("venue1golfaus");
            product.setGiftCardStyle("");

            final ConsignmentEntryDTO consignmentEntry = new ConsignmentEntryDTO();
            consignmentEntry.setProduct(product);
            consignmentEntry.setRecipients(recipients);

            consignmentEntries.add(consignmentEntry);
        }

        consignment.setConsignmentEntries(consignmentEntries);
        consignment.setOrder(order);
        request.setConsignment(consignment);
    }

}
