package au.com.target.tgtwebmethods.stock.client.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPut;
import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import au.com.target.tgtsale.stock.dto.request.StockUpdateDto;
import au.com.target.tgtsale.stock.dto.request.StockUpdateProductDto;
import au.com.target.tgtsale.stock.dto.request.StockUpdateStoreDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateProductResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateStoreResponseDto;
import au.com.target.tgtutility.http.factory.HttpClientFactory;
import au.com.target.tgtwebmethods.exception.TargetWebMethodsClientException;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetStockUpdateClientImplTest {
    private static final String WEBMETHODS_BASE_URL = "http://webmethods:5555";
    private static final String STORE_STOCK_SERVICE_URL = "/storeStockService";

    @Mock
    private HttpClientFactory mockHttpClientFactory;

    @Mock
    private HttpClient mockHttpClient;

    @InjectMocks
    private final TargetStockUpdateClientImpl targetStockUpdateClientImpl = new TargetStockUpdateClientImpl();

    @Before
    public void setUp() {
        given(mockHttpClientFactory.createHttpClient()).willReturn(mockHttpClient);

        targetStockUpdateClientImpl.setWebmethodsBaseUrl(WEBMETHODS_BASE_URL);
        targetStockUpdateClientImpl.setStoreStockServiceUrl(STORE_STOCK_SERVICE_URL);
    }

    @Test
    public void testMarshalStockUpdateRequest() throws Exception {
        final List<StockUpdateProductDto> stockUpdateProductDtos = new ArrayList<>();

        final StockUpdateProductDto stockUpdateProductDto = new StockUpdateProductDto();
        stockUpdateProductDto.setEan("123456789");
        stockUpdateProductDto.setItemcode("87654321");
        stockUpdateProductDtos.add(stockUpdateProductDto);

        final List<StockUpdateStoreDto> stockUpdateStoreDtos = new ArrayList<>();

        final StockUpdateStoreDto stockUpdateStoreDto = new StockUpdateStoreDto();
        stockUpdateStoreDto.setStoreNumber("1234");
        stockUpdateStoreDto.setStoreState("VIC");
        stockUpdateStoreDto.setStockUpdateProductDtos(stockUpdateProductDtos);
        stockUpdateStoreDtos.add(stockUpdateStoreDto);

        final StockUpdateDto stockUpdateDto = new StockUpdateDto();
        stockUpdateDto.setStockUpdateStoreDtos(stockUpdateStoreDtos);

        final String payload = targetStockUpdateClientImpl.marshalStockUpdateRequest(stockUpdateDto);

        final StringBuilder expectedPayload = new StringBuilder();
        expectedPayload.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
        expectedPayload.append("<integration-stockUpdate>");
        expectedPayload.append("<stores>");
        expectedPayload.append("<store>");
        expectedPayload.append("<storeNumber>1234</storeNumber><storeState>VIC</storeState>");
        expectedPayload.append("<products><product>");
        expectedPayload.append("<itemcode>87654321</itemcode><ean>123456789</ean>");
        expectedPayload.append("</product></products>");
        expectedPayload.append("</store>");
        expectedPayload.append("</stores>");
        expectedPayload.append("</integration-stockUpdate>");

        assertThat(payload).isNotNull();
        XMLTestCase.assertEquals(expectedPayload.toString(), payload);
    }

    @Test
    public void testUnmarshalStockUpdateResponse() throws TargetWebMethodsClientException {
        final StringBuilder responsePayload = new StringBuilder();
        responsePayload.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
        responsePayload.append("<integration-stockUpdateResponse>");
        responsePayload.append("<stores>");
        responsePayload.append("<store>");
        responsePayload.append("<storeNumber>1234</storeNumber><success>true</success>");
        responsePayload.append("<products><product>");
        responsePayload.append("<itemcode>87654321</itemcode><soh>7</soh><success>true</success>");
        responsePayload.append("</product></products>");
        responsePayload.append("</store>");
        responsePayload.append("</stores>");
        responsePayload.append("</integration-stockUpdateResponse>");

        final StockUpdateResponseDto responseDto = targetStockUpdateClientImpl
                .unmarshalStockUpdateResponse(responsePayload
                        .toString());

        assertThat(responseDto).isNotNull();

        final List<StockUpdateStoreResponseDto> responseStores = responseDto.getStockUpdateStoreResponseDtos();
        assertThat(responseStores).isNotEmpty().hasSize(1);

        final StockUpdateStoreResponseDto responseStore = responseStores.get(0);
        assertThat(responseStore.getStoreNumber()).isEqualTo("1234");

        final List<StockUpdateProductResponseDto> responseProducts = responseStore.getStockUpdateProductResponseDtos();
        assertThat(responseProducts).isNotEmpty().hasSize(1);

        final StockUpdateProductResponseDto responseProduct = responseProducts.get(0);
        assertThat(responseProduct.getItemcode()).isEqualTo("87654321");
        assertThat(responseProduct.getSoh()).isEqualTo("7");
    }

    @Test
    public void testCallStoreStockService()
            throws ClientProtocolException, IOException, TargetWebMethodsClientException {
        final StringBuilder requestPayload = new StringBuilder();
        requestPayload.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
        requestPayload.append("<integration-stockUpdate>");
        requestPayload.append("<stores>");
        requestPayload.append("<store>");
        requestPayload.append("<storeNumber>1234</storeNumber><storeState>VIC</storeState>");
        requestPayload.append("<products><product>");
        requestPayload.append("<itemcode>87654321</itemcode><ean>123456789</ean>");
        requestPayload.append("</product></products>");
        requestPayload.append("</store>");
        requestPayload.append("</stores>");
        requestPayload.append("</integration-stockUpdate>");

        final StringBuilder responsePayload = new StringBuilder();
        responsePayload.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
        responsePayload.append("<integration-stockUpdateResponse>");
        responsePayload.append("<stores>");
        responsePayload.append("<store>");
        responsePayload.append("<storeNumber>1234</storeNumber><success>true</success>");
        responsePayload.append("<products><product>");
        responsePayload.append("<itemcode>87654321</itemcode><soh>7</soh><success>true</success>");
        responsePayload.append("</product></products>");
        responsePayload.append("</store>");
        responsePayload.append("</stores>");
        responsePayload.append("</integration-stockUpdateResponse>");

        final InputStream responseInputStream = new ByteArrayInputStream(responsePayload.toString().getBytes());

        final HttpEntity mockHttpEntity = mock(HttpEntity.class);
        given(mockHttpEntity.getContent()).willReturn(responseInputStream);

        final HttpResponse mockHttpResponse = mock(HttpResponse.class);
        given(mockHttpResponse.getEntity()).willReturn(mockHttpEntity);

        when(mockHttpClient.execute(any(HttpPut.class), Matchers.<ResponseHandler<Object>>any())).thenAnswer(new Answer() {

            /*
             * (non-Javadoc)
             * 
             * @see org.mockito.stubbing.Answer#answer(org.mockito.invocation.InvocationOnMock)
             */
            @Override
            public Object answer(final InvocationOnMock invocation) throws IOException {
                final Object[] args = invocation.getArguments();
                final ResponseHandler responseHandler = (ResponseHandler)args[1];

                return responseHandler.handleResponse(mockHttpResponse);
            }

        });

        final String response = targetStockUpdateClientImpl.callStoreStockService(requestPayload.toString());

        assertThat(response).isNotNull();
        XMLTestCase.assertEquals(responsePayload.toString(), response);

        final ArgumentCaptor<HttpPut> httpPutCaptor = ArgumentCaptor.forClass(HttpPut.class);
        final ArgumentCaptor<ResponseHandler> responseHandlerCaptor = ArgumentCaptor.forClass(ResponseHandler.class);
        verify(mockHttpClient).execute(httpPutCaptor.capture(), responseHandlerCaptor.capture());

        final HttpPut httpPut = httpPutCaptor.getValue();
        assertThat(httpPut.getURI().toString()).isEqualTo(WEBMETHODS_BASE_URL + STORE_STOCK_SERVICE_URL);

        final HttpEntity requestEntity = httpPut.getEntity();
        assertThat(requestEntity).isInstanceOf(UrlEncodedFormEntity.class);

        final UrlEncodedFormEntity urlEncodedFormEntity = (UrlEncodedFormEntity)requestEntity;
        assertThat(urlEncodedFormEntity.getContentType().getName()).isEqualTo("Content-Type");
        assertThat(urlEncodedFormEntity.getContentType().getValue()).contains("application/x-www-form-urlencoded");
    }

    @Test
    public void testCallStoreStockServiceWithNullResponseEntity() throws ClientProtocolException, IOException,
            TargetWebMethodsClientException {
        final StringBuilder requestPayload = new StringBuilder();
        requestPayload.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
        requestPayload.append("<integration-stockUpdate>");
        requestPayload.append("<stores>");
        requestPayload.append("<store>");
        requestPayload.append("<storeNumber>1234</storeNumber><storeState>VIC</storeState>");
        requestPayload.append("<products><product>");
        requestPayload.append("<itemcode>87654321</itemcode><ean>123456789</ean>");
        requestPayload.append("</product></products>");
        requestPayload.append("</store>");
        requestPayload.append("</stores>");
        requestPayload.append("</integration-stockUpdate>");

        final HttpResponse mockHttpResponse = mock(HttpResponse.class);
        given(mockHttpResponse.getEntity()).willReturn(null);

        when(mockHttpClient.execute(any(HttpPut.class), Matchers.<ResponseHandler<Object>>any())).thenAnswer(new Answer() {

            /*
             * (non-Javadoc)
             * 
             * @see org.mockito.stubbing.Answer#answer(org.mockito.invocation.InvocationOnMock)
             */
            @Override
            public Object answer(final InvocationOnMock invocation) throws IOException {
                final Object[] args = invocation.getArguments();
                final ResponseHandler responseHandler = (ResponseHandler)args[1];

                return responseHandler.handleResponse(mockHttpResponse);
            }

        });

        final String response = targetStockUpdateClientImpl.callStoreStockService(requestPayload.toString());

        assertThat(response).isNull();

        final ArgumentCaptor<HttpPut> httpPutCaptor = ArgumentCaptor.forClass(HttpPut.class);
        final ArgumentCaptor<ResponseHandler> responseHandlerCaptor = ArgumentCaptor.forClass(ResponseHandler.class);
        verify(mockHttpClient).execute(httpPutCaptor.capture(), responseHandlerCaptor.capture());

        final HttpPut httpPut = httpPutCaptor.getValue();
        assertThat(httpPut.getURI().toString()).isEqualTo(WEBMETHODS_BASE_URL + STORE_STOCK_SERVICE_URL);

        final HttpEntity requestEntity = httpPut.getEntity();
        assertThat(requestEntity).isInstanceOf(UrlEncodedFormEntity.class);

        final UrlEncodedFormEntity urlEncodedFormEntity = (UrlEncodedFormEntity)requestEntity;
        assertThat(urlEncodedFormEntity.getContentType().getName()).isEqualTo("Content-Type");
        assertThat(urlEncodedFormEntity.getContentType().getValue()).contains("application/x-www-form-urlencoded");
    }
}
