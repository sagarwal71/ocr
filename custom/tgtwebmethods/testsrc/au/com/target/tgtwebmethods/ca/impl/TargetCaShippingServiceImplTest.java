/**
 * 
 */
package au.com.target.tgtwebmethods.ca.impl;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.xml.sax.SAXException;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;
import au.com.target.tgtwebmethods.ca.data.request.SubmitOrderShipmentRequest;
import au.com.target.tgtwebmethods.constants.TgtwebmethodsConstants;


/**
 * @author bhuang3
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:tgtwebmethods-spring-test.xml" })
public class TargetCaShippingServiceImplTest {

    private static final Logger LOG = Logger.getLogger(TargetCaShippingServiceImplTest.class);

    private OrderModel orderModel;
    private MockRestServiceServer mockServer;

    private final String loadRequestXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
            "<submitOrderShipmentRequest>" +
            "<clientId>123</clientId>" +
            "<salesChannel>eBay</salesChannel>" +
            "<shipmentOrders>" +
            "<shipmentOrder>" +
            "<orderId>123456</orderId>" +
            "<shipmentType>Full</shipmentType>" +
            "<dateShippedGMT>2014-10-16T11:43:00+11:00</dateShippedGMT>" +
            "<carrierCode>TestPartnerCarrierCode</carrierCode>" +
            "<classCode>TestPartnerCarrierClass</classCode>" +
            "<trackingNumber>OT123</trackingNumber>" +
            "</shipmentOrder>" +
            "</shipmentOrders>" +
            "</submitOrderShipmentRequest>";

    private final String loadRequestXmlForPartialShipment = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
            +
            "<submitOrderShipmentRequest>" +
            "<clientId>123</clientId>" +
            "<salesChannel>TradeMe</salesChannel>" +
            "<shipmentOrders>" +
            "<shipmentOrder>" +
            "<orderId>123456</orderId>" +
            "<shipmentType>Partial</shipmentType>" +
            "<dateShippedGMT>2014-10-16T11:43:00+11:00</dateShippedGMT>" +
            "<carrierCode>TestPartnerCarrierCode</carrierCode>" +
            "<classCode>TestPartnerCarrierClass</classCode>" +
            "<trackingNumber>OT123</trackingNumber>" +
            "<lineItems>" +
            "<lineItem>" +
            "<productCode>19996</productCode>" +
            "<quantity>1</quantity>" +
            "</lineItem>" +
            "</lineItems>" +
            "</shipmentOrder>" +
            "</shipmentOrders>" +
            "</submitOrderShipmentRequest>";

    private final String loadResponseXml = "<submitOrderShipmentResponse>" +
            "<resultCode>0</resultCode>" +
            "<resultMessage></resultMessage>" +
            "<shipmentOrders>" +
            "<shipmentOrder>" +
            "<orderId>123456</orderId>" +
            "<resultData>" +
            "<status>true</status>" +
            "<messageCode>0</messageCode>" +
            "<message></message>" +
            "</resultData>" +
            "</shipmentOrder>" +
            "</shipmentOrders>" +
            "</submitOrderShipmentResponse>";

    private final String loadUnSuccessfulResponseXml = "<submitOrderShipmentResponse>" +
            "<resultCode>0</resultCode>" +
            "<resultMessage></resultMessage>" +
            "<shipmentOrders>" +
            "<shipmentOrder>" +
            "<orderId>123456</orderId>" +
            "<resultData>" +
            "<status>false</status>" +
            "<messageCode>0</messageCode>" +
            "<message></message>" +
            "</resultData>" +
            "</shipmentOrder>" +
            "</shipmentOrders>" +
            "</submitOrderShipmentResponse>";

    private final String loadUnSuccessfulResponseWithOutResultDataXml = "<submitOrderShipmentResponse>" +
            "<resultCode>0</resultCode>" +
            "<resultMessage></resultMessage>" +
            "<shipmentOrders>" +
            "<shipmentOrder>" +
            "<orderId>123456</orderId>" +
            "</shipmentOrder>" +
            "</shipmentOrders>" +
            "</submitOrderShipmentResponse>";

    private final String loadxmlRequestForReadyForPickupStatus = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
            +
            "<submitOrderShipmentRequest>" +
            "<clientId>123</clientId>" +
            "<salesChannel>eBay</salesChannel>" +
            "<shipmentOrders>" +
            "<shipmentOrder>" +
            "<orderId>123456</orderId>" +
            "<shipmentType>Partial</shipmentType>" +
            "<dateShippedGMT>2014-10-17T11:43:00+11:00</dateShippedGMT>" +
            "<carrierCode>TestPartnerCarrierCode</carrierCode>" +
            "<classCode>TestPartnerCarrierClass</classCode>" +
            "<trackingNumber>a123456</trackingNumber>" +
            "<fulfillmentType>Pickup</fulfillmentType>" +
            "<fulfillmentStatus>ReadyForPickup</fulfillmentStatus>" +
            "</shipmentOrder>" +
            "</shipmentOrders>" +
            "</submitOrderShipmentRequest>";

    private final String loadxmlRequestForReadyForPickupStatusForPartial = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
            +
            "<submitOrderShipmentRequest>" +
            "<clientId>123</clientId>" +
            "<salesChannel>eBay</salesChannel>" +
            "<shipmentOrders>" +
            "<shipmentOrder>" +
            "<orderId>123456</orderId>" +
            "<shipmentType>Partial</shipmentType>" +
            "<dateShippedGMT>2014-10-17T11:43:00+11:00</dateShippedGMT>" +
            "<carrierCode>TestPartnerCarrierCode</carrierCode>" +
            "<classCode>TestPartnerCarrierClass</classCode>" +
            "<trackingNumber>a123456</trackingNumber>" +
            "<fulfillmentType>Pickup</fulfillmentType>" +
            "<fulfillmentStatus>ReadyForPickup</fulfillmentStatus>" +
            "</shipmentOrder>" +
            "</shipmentOrders>" +
            "</submitOrderShipmentRequest>";

    private final String loadxmlRequestForPickedupStatus = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
            +
            "<submitOrderShipmentRequest>" +
            "<clientId>123</clientId>" +
            "<salesChannel>eBay</salesChannel>" +
            "<shipmentOrders>" +
            "<shipmentOrder>" +
            "<orderId>123456</orderId>" +
            "<shipmentType>Partial</shipmentType>" +
            "<dateShippedGMT>2014-10-18T11:43:00+11:00</dateShippedGMT>" +
            "<carrierCode>TestPartnerCarrierCode</carrierCode>" +
            "<classCode>TestPartnerCarrierClass</classCode>" +
            "<trackingNumber>a123456</trackingNumber>" +
            "<fulfillmentType>Pickup</fulfillmentType>" +
            "<fulfillmentStatus>Complete</fulfillmentStatus>" +
            "</shipmentOrder>" +
            "</shipmentOrders>" +
            "</submitOrderShipmentRequest>";

    private final String sampleXmlRequestForReadyForPickupStatus = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
            +
            "<submitOrderShipmentRequest>" +
            "<clientId>123</clientId>" +
            "<salesChannel>eBay</salesChannel>" +
            "<shipmentOrders>" +
            "<shipmentOrder>" +
            "<orderId>123456</orderId>" +
            "<shipmentType>Partial</shipmentType>" +
            "<dateShippedGMT>2014-10-17T11:43:00+11:00</dateShippedGMT>" +
            "<carrierCode>TestPartnerCarrierCode</carrierCode>" +
            "<classCode>TestPartnerCarrierClass</classCode>" +
            "<trackingNumber>a123456</trackingNumber>" +
            "<lineItems>" +
            "<lineItem>" +
            "<productCode>19996</productCode>" +
            "<quantity>1</quantity>" +
            "</lineItem>" +
            "</lineItems>" +
            "<fulfillmentType>Pickup</fulfillmentType>" +
            "<fulfillmentStatus>ReadyForPickup</fulfillmentStatus>" +
            "</shipmentOrder>" +
            "</shipmentOrders>" +
            "</submitOrderShipmentRequest>";

    private final String sampleXmlRequestForPickedupStatus = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
            +
            "<submitOrderShipmentRequest>" +
            "<clientId>123</clientId>" +
            "<salesChannel>eBay</salesChannel>" +
            "<shipmentOrders>" +
            "<shipmentOrder>" +
            "<orderId>123456</orderId>" +
            "<shipmentType>Partial</shipmentType>" +
            "<dateShippedGMT>2014-10-18T11:43:00+11:00</dateShippedGMT>" +
            "<carrierCode>TestPartnerCarrierCode</carrierCode>" +
            "<classCode>TestPartnerCarrierClass</classCode>" +
            "<trackingNumber>a123456</trackingNumber>" +
            "<lineItems>" +
            "<lineItem>" +
            "<productCode>19996</productCode>" +
            "<quantity>1</quantity>" +
            "</lineItem>" +
            "</lineItems>" +
            "<fulfillmentType>Pickup</fulfillmentType>" +
            "<fulfillmentStatus>Complete</fulfillmentStatus>" +
            "</shipmentOrder>" +
            "</shipmentOrders>" +
            "</submitOrderShipmentRequest>";

    private final String expectedXmlRequestForShipped = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
            +
            "<submitOrderShipmentRequest>" +
            "<clientId>123</clientId>" +
            "<salesChannel>eBay</salesChannel>" +
            "<shipmentOrders>" +
            "<shipmentOrder>" +
            "<orderId>123456</orderId>" +
            "<shipmentType>Full</shipmentType>" +
            "<dateShippedGMT>2014-10-16T11:43:00+11:00</dateShippedGMT>" +
            "<trackingNumber>OT123</trackingNumber>" +
            "</shipmentOrder>" +
            "</shipmentOrders>" +
            "</submitOrderShipmentRequest>";

    private final String expectedXmlRequestForPickedupWithoutCarrierInfo = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
            +
            "<submitOrderShipmentRequest>" +
            "<clientId>123</clientId>" +
            "<salesChannel>eBay</salesChannel>" +
            "<shipmentOrders>" +
            "<shipmentOrder>" +
            "<orderId>123456</orderId>" +
            "<shipmentType>Partial</shipmentType>" +
            "<dateShippedGMT>2014-10-18T11:43:00+11:00</dateShippedGMT>" +
            "<trackingNumber>a123456</trackingNumber>" +
            "<lineItems>" +
            "<lineItem>" +
            "<productCode>19996</productCode>" +
            "<quantity>1</quantity>" +
            "</lineItem>" +
            "</lineItems>" +
            "<fulfillmentType>Pickup</fulfillmentType>" +
            "<fulfillmentStatus>Complete</fulfillmentStatus>" +
            "</shipmentOrder>" +
            "</shipmentOrders>" +
            "</submitOrderShipmentRequest>";

    private final String expectedXmlRequestForPickedupWithAutoPickedDate = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
            +
            "<submitOrderShipmentRequest>" +
            "<clientId>123</clientId>" +
            "<salesChannel>eBay</salesChannel>" +
            "<shipmentOrders>" +
            "<shipmentOrder>" +
            "<orderId>123456</orderId>" +
            "<shipmentType>Partial</shipmentType>" +
            "<dateShippedGMT>2014-10-19T11:43:00+11:00</dateShippedGMT>" +
            "<trackingNumber>a123456</trackingNumber>" +
            "<lineItems>" +
            "<lineItem>" +
            "<productCode>19996</productCode>" +
            "<quantity>1</quantity>" +
            "</lineItem>" +
            "</lineItems>" +
            "<fulfillmentType>Pickup</fulfillmentType>" +
            "<fulfillmentStatus>Complete</fulfillmentStatus>" +
            "</shipmentOrder>" +
            "</shipmentOrders>" +
            "</submitOrderShipmentRequest>";

    private final String loadRequestXmlForPartialeBayShipment = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
            +
            "<submitOrderShipmentRequest>" +
            "<clientId>123</clientId>" +
            "<salesChannel>eBay</salesChannel>" +
            "<shipmentOrders>" +
            "<shipmentOrder>" +
            "<orderId>123456</orderId>" +
            "<shipmentType>Partial</shipmentType>" +
            "<dateShippedGMT>2014-10-16T11:43:00+11:00</dateShippedGMT>" +
            "<carrierCode>TestPartnerCarrierCode</carrierCode>" +
            "<classCode>TestPartnerCarrierClass</classCode>" +
            "<trackingNumber>OT123</trackingNumber>" +
            "<lineItems>" +
            "<lineItem>" +
            "<productCode>19996</productCode>" +
            "<quantity>1</quantity>" +
            "</lineItem>" +
            "</lineItems>" +
            "</shipmentOrder>" +
            "</shipmentOrders>" +
            "</submitOrderShipmentRequest>";

    @Autowired
    private TargetCaShippingServiceImpl targetCaShippingServiceImpl;

    @Before
    public void setUp() {
        targetCaShippingServiceImpl.setClientId("123");
        targetCaShippingServiceImpl.setWebmethodsBaseUrl("http://localhost:8998");
        targetCaShippingServiceImpl.setSubmitOrderShipmentListRequestUrl("");
        mockServer = MockRestServiceServer.createServer(targetCaShippingServiceImpl
                .getRestTemplate());
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date shippedDate = null;
        Date readyForPickupDate = null;
        Date pickedUpDate = null;
        try {
            shippedDate = sdf.parse("2014-10-16 11:43:00");
            readyForPickupDate = sdf.parse("2014-10-17 11:43:00");
            pickedUpDate = sdf.parse("2014-10-18 11:43:00");
        }
        catch (final ParseException e) {
            LOG.error(e);
        }
        XMLUnit.setIgnoreWhitespace(true);

        final TargetConsignmentModel consignment = new TargetConsignmentModel();
        consignment.setStatus(ConsignmentStatus.SHIPPED);
        consignment.setShippingDate(shippedDate);
        consignment.setCarrier("AusPost");
        consignment.setTrackingID("OT123");
        consignment.setCode("a123456");
        consignment.setReadyForPickUpDate(readyForPickupDate);
        consignment.setPickedUpDate(pickedUpDate);

        final TargetCarrierModel carrier = new TargetCarrierModel();
        carrier.setCode("TestCarrier");
        carrier.setPartnerCarrierClass("TestPartnerCarrierClass");
        carrier.setPartnerCarrierCode("TestPartnerCarrierCode");

        consignment.setTargetCarrier(carrier);
        final Set<ConsignmentModel> consignmentSet = new HashSet<>();
        consignmentSet.add(consignment);
        orderModel = new OrderModel();
        orderModel.setConsignments(consignmentSet);
        final Integer ebayOrderNumber = new Integer(123456);
        orderModel.setEBayOrderNumber(ebayOrderNumber);
        orderModel.setSalesApplication(SalesApplication.EBAY);
    }

    @Test
    public void testCreateSubmitOrderShipmentListRequest() throws Exception {
        final SubmitOrderShipmentRequest submitOrderShipmentListRequest = targetCaShippingServiceImpl
                .createDeliveryOrderShipmentRequest(orderModel);

        final JAXBContext jaxbContext = JAXBContext.newInstance(SubmitOrderShipmentRequest.class);
        final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        // output pretty printed  
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        jaxbMarshaller.marshal(submitOrderShipmentListRequest, baos);
        final String result = baos.toString();
        XMLAssert.assertXMLEqual(loadRequestXml, result);
    }

    @Test
    public void testCreateSubmitConsignmentShipmentListRequest() throws Exception {
        setConsignmentEntries();
        for (final ConsignmentModel consignment : orderModel.getConsignments()) {
            consignment.setOrder(orderModel);
            final SubmitOrderShipmentRequest submitOrderShipmentListRequest = targetCaShippingServiceImpl
                    .createDeliveryConsignmentShipmentRequest(consignment);

            final JAXBContext jaxbContext = JAXBContext.newInstance(SubmitOrderShipmentRequest.class);
            final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            // output pretty printed  
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            jaxbMarshaller.marshal(submitOrderShipmentListRequest, baos);
            final String result = baos.toString();
            XMLAssert.assertXMLEqual(loadRequestXmlForPartialeBayShipment, result);
        }
    }

    @Test
    public void testCreateSubmitOrderShipmentListRequestNullSalesApplication() throws Exception {
        orderModel.setSalesApplication(null);
        final SubmitOrderShipmentRequest submitOrderShipmentListRequest = targetCaShippingServiceImpl
                .createDeliveryOrderShipmentRequest(orderModel);

        Assert.assertNull(submitOrderShipmentListRequest);
    }

    @Test
    public void testCreateSubmitOrderShipmentListRequestForShortPick() throws Exception {
        setConsignmentEntries();

        final Set<OrderModificationRecordModel> modificationRecords = new HashSet<>();
        final OrderCancelRecordModel orderCancelRecord = new OrderCancelRecordModel();
        modificationRecords.add(orderCancelRecord);
        orderModel.setModificationRecords(modificationRecords);
        orderModel.setSalesApplication(SalesApplication.TRADEME);

        final SubmitOrderShipmentRequest submitOrderShipmentListRequest = targetCaShippingServiceImpl
                .createDeliveryOrderShipmentRequest(orderModel);
        final JAXBContext jaxbContext = JAXBContext.newInstance(SubmitOrderShipmentRequest.class);
        final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        // output pretty printed  
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        jaxbMarshaller.marshal(submitOrderShipmentListRequest, baos);
        final String result = baos.toString();
        XMLAssert.assertXMLEqual(loadRequestXmlForPartialShipment, result);
    }

    @Test
    public void testCreateSubmitOrderShipmentListRequestWithReturnNull() throws Exception {
        final OrderModel orderModelWithoutConsignment = new OrderModel();
        final Set<ConsignmentModel> consignmentSet = new HashSet<>();
        orderModelWithoutConsignment.setConsignments(consignmentSet);
        orderModelWithoutConsignment.setCode("O123456");
        final SubmitOrderShipmentRequest submitOrderShipmentListRequest = targetCaShippingServiceImpl
                .createDeliveryOrderShipmentRequest(orderModelWithoutConsignment);

        Assert.assertNull(submitOrderShipmentListRequest);
    }

    @Test
    public void testSubmitOrderShipmentList() throws Exception {
        mockServer.expect(requestTo("http://localhost:8998"))
                .andExpect(content().xml(loadRequestXml))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(loadResponseXml, MediaType.APPLICATION_XML));
        final boolean result = targetCaShippingServiceImpl.submitDeliveryOrderStatusForCompleteUpdate(orderModel);
        Assert.assertTrue(result);
    }

    @Test
    public void testSubmitOrderShipmentListUnSuccessful() throws Exception {
        mockServer.expect(requestTo("http://localhost:8998"))
                .andExpect(content().xml(loadRequestXml))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(loadUnSuccessfulResponseXml, MediaType.APPLICATION_XML));
        final boolean result = targetCaShippingServiceImpl.submitDeliveryOrderStatusForCompleteUpdate(orderModel);
        Assert.assertFalse(result);
    }

    @Test
    public void testSubmitOrderShipmentListWithoutResultData() throws Exception {
        mockServer.expect(requestTo("http://localhost:8998"))
                .andExpect(content().xml(loadRequestXml))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(loadUnSuccessfulResponseWithOutResultDataXml, MediaType.APPLICATION_XML));
        final boolean result = targetCaShippingServiceImpl.submitDeliveryOrderStatusForCompleteUpdate(orderModel);
        Assert.assertFalse(result);
    }

    @Test
    public void testCreateSubmitOrderShipmentRequestForReadyForPickupStatus()
            throws JAXBException, SAXException, IOException {
        setConsignmentEntries();

        final SubmitOrderShipmentRequest submitOrderShipmentListRequest = targetCaShippingServiceImpl
                .createCncOrderShipmentRequest(orderModel,
                        TgtwebmethodsConstants.PartnerFulFillmentConstants.STATUS_READYFORPICKUP);
        final JAXBContext jaxbContext = JAXBContext.newInstance(SubmitOrderShipmentRequest.class);
        final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        jaxbMarshaller.marshal(submitOrderShipmentListRequest, baos);
        final String result = baos.toString();
        XMLAssert.assertXMLEqual(sampleXmlRequestForReadyForPickupStatus, result);
    }

    @Test
    public void testSubmitCncOrderStatusUpdateForReadyForPickup() throws Exception {
        mockServer.expect(requestTo("http://localhost:8998"))
                .andExpect(content().xml(loadxmlRequestForReadyForPickupStatus))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(loadResponseXml, MediaType.APPLICATION_XML));
        final boolean result = targetCaShippingServiceImpl.submitCncOrderStatusForReadyForPickupUpdate(orderModel);
        Assert.assertTrue(result);
    }

    @Test
    public void testSubmitCncOrderStatusUpdateForReadyForPickupForPartial() throws Exception {
        mockServer.expect(requestTo("http://localhost:8998"))
                .andExpect(content().xml(loadxmlRequestForReadyForPickupStatusForPartial))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(loadResponseXml, MediaType.APPLICATION_XML));

        final Set<OrderModificationRecordModel> modificationRecords = new HashSet<>();
        final OrderCancelRecordModel orderCancelRecord = new OrderCancelRecordModel();
        modificationRecords.add(orderCancelRecord);
        orderModel.setModificationRecords(modificationRecords);
        final boolean result = targetCaShippingServiceImpl.submitCncOrderStatusForReadyForPickupUpdate(orderModel);
        Assert.assertTrue(result);
    }

    @Test
    public void testCreateSubmitOrderShipmentRequestForPickedupStatus()
            throws JAXBException, SAXException, IOException {
        setConsignmentEntries();

        final SubmitOrderShipmentRequest submitOrderShipmentListRequest = targetCaShippingServiceImpl
                .createCncOrderShipmentRequest(orderModel,
                        TgtwebmethodsConstants.PartnerFulFillmentConstants.STATUS_COMPLETE);
        final JAXBContext jaxbContext = JAXBContext.newInstance(SubmitOrderShipmentRequest.class);
        final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        jaxbMarshaller.marshal(submitOrderShipmentListRequest, baos);
        final String result = baos.toString();
        XMLAssert.assertXMLEqual(sampleXmlRequestForPickedupStatus, result);
    }

    @Test
    public void testSubmitCncOrderStatusUpdateForPickedup() throws Exception {
        mockServer.expect(requestTo("http://localhost:8998"))
                .andExpect(content().xml(loadxmlRequestForPickedupStatus))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(loadResponseXml, MediaType.APPLICATION_XML));
        final boolean result = targetCaShippingServiceImpl.submitCncOrderStatusForPickedupUpdate(orderModel);
        Assert.assertTrue(result);
    }

    @Test
    public void testCreateDeliveryOrderShipmentRequestWihoutCarrierDetails()
            throws JAXBException, SAXException, IOException {
        setConsignmentEntries();
        setCarrierNullInOrder();

        final SubmitOrderShipmentRequest submitOrderShipmentListRequest = targetCaShippingServiceImpl
                .createDeliveryOrderShipmentRequest(orderModel);

        final JAXBContext jaxbContext = JAXBContext.newInstance(SubmitOrderShipmentRequest.class);
        final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        jaxbMarshaller.marshal(submitOrderShipmentListRequest, baos);
        final String result = baos.toString();
        XMLAssert.assertXMLEqual(expectedXmlRequestForShipped, result);
    }

    @Test
    public void testCreateSubmitOrderShipmentRequestForPickedupStatusWithoutCarrierInfo()
            throws JAXBException, SAXException, IOException {
        setConsignmentEntries();
        setCarrierNullInOrder();

        final SubmitOrderShipmentRequest submitOrderShipmentListRequest = targetCaShippingServiceImpl
                .createCncOrderShipmentRequest(orderModel,
                        TgtwebmethodsConstants.PartnerFulFillmentConstants.STATUS_COMPLETE);

        final JAXBContext jaxbContext = JAXBContext.newInstance(SubmitOrderShipmentRequest.class);
        final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        jaxbMarshaller.marshal(submitOrderShipmentListRequest, baos);
        final String result = baos.toString();
        XMLAssert.assertXMLEqual(expectedXmlRequestForPickedupWithoutCarrierInfo, result);
    }

    @Test
    public void testCreateSubmitOrderShipmentRequestForPickedupStatusWithAutoPickedTime()
            throws JAXBException, SAXException, IOException {
        setConsignmentEntries();
        setCarrierNullInOrder();
        setAutoPickedTimeInConsignment();

        final SubmitOrderShipmentRequest submitOrderShipmentListRequest = targetCaShippingServiceImpl
                .createCncOrderShipmentRequest(orderModel,
                        TgtwebmethodsConstants.PartnerFulFillmentConstants.STATUS_COMPLETE);

        final JAXBContext jaxbContext = JAXBContext.newInstance(SubmitOrderShipmentRequest.class);
        final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        jaxbMarshaller.marshal(submitOrderShipmentListRequest, baos);
        final String result = baos.toString();
        XMLAssert.assertXMLEqual(expectedXmlRequestForPickedupWithAutoPickedDate, result);
    }

    /**
     * Method to set auto picked up date.
     */
    private void setAutoPickedTimeInConsignment() {
        for (final ConsignmentModel consignment : orderModel.getConsignments()) {
            final TargetConsignmentModel targetConsignment = (TargetConsignmentModel)consignment;
            targetConsignment.setPickedUpAutoDate(DateUtils.addDays(targetConsignment.getPickedUpDate(), 1));
            targetConsignment.setPickedUpDate(null);
        }
    }

    /**
     * Method to set 'TargetCarrier' as null in the order's consignment
     */
    private void setCarrierNullInOrder() {
        for (final ConsignmentModel consignment : orderModel.getConsignments()) {
            if (consignment instanceof TargetConsignmentModel) {
                final TargetConsignmentModel targetConsignment = (TargetConsignmentModel)consignment;
                targetConsignment.setTargetCarrier(null);
            }
        }
    }

    /**
     * Method to set consignment entries
     */
    private void setConsignmentEntries() {
        final Set<ConsignmentModel> consignmentSet = orderModel.getConsignments();

        final Set<ConsignmentEntryModel> consignments = new HashSet<>();
        final ConsignmentEntryModel consignmentEntryModel = new ConsignmentEntryModel();
        final OrderEntryModel orderEntryModel = new OrderEntryModel();
        final ProductModel product = new ProductModel();
        product.setCode("19996");
        orderEntryModel.setProduct(product);
        consignmentEntryModel.setShippedQuantity(new Long(1));
        consignmentEntryModel.setOrderEntry(orderEntryModel);
        consignments.add(consignmentEntryModel);

        for (final ConsignmentModel consignment : consignmentSet) {
            consignment.setConsignmentEntries(consignments);
        }
    }

}