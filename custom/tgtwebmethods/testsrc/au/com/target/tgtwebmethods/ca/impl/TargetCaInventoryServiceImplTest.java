/**
 * 
 */
package au.com.target.tgtwebmethods.ca.impl;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.ResponseCreator;
import org.springframework.web.client.RestClientException;


/**
 * @author bhuang3
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:tgtwebmethods-spring-test.xml" })
public class TargetCaInventoryServiceImplTest {
    private static final String LOAD_RESPONSE_XML =
            "<updateStockResponse><products><product><productCode>W83475494</productCode><result>ok</result><resultCode>0</resultCode><message></message></product></products></updateStockResponse>";

    private static final String LOAD_RESPONSE_XML_WITH_FAIL_MESSAGE =
            "<updateStockResponse><products><product><productCode>W83475494</productCode><result>error</result><resultCode>100</resultCode><message>error</message></product></products></updateStockResponse>";

    private static final String LOAD_RESPONSE_XML_WITH_PRODUCT_CODE_UNMATCH =
            "<updateStockResponse><products><product><productCode>W83475494</productCode><result>ok</result><resultCode>0</resultCode><message></message></product></products></updateStockResponse>";

    private static final String LOAD_RESPONSE_XML_WITH_WRONG_PATTERN =
            "<updateStock><products><product><productCode>W83475494</productCode><result>ok</result><resultCode>0</resultCode><message></message></product></products></updateStock>";

    private static final String LOAD_RESPONSE_XML_WITHOUT_RESULT_CODE =
            "<updateStockResponse><products><product><productCode>W83475494</productCode><result>ok</result><message></message></product></products></updateStockResponse>";

    private MockRestServiceServer mockServer;
    private final String loadRequestXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
            + "<updateStockRequest>"
            + "<clientid>123</clientid>"
            + "<products>"
            + "<product>"
            + "<productCode>W83475494</productCode>"
            + "<quantity>9999</quantity>"
            + "</product>"
            + "</products>"
            + "</updateStockRequest>";



    @Autowired
    private TargetCaInventoryServiceImpl targetCaInventoryServiceImpl;

    @Before
    public void setUp() {
        mockServer = MockRestServiceServer.createServer(targetCaInventoryServiceImpl
                .getRestTemplate());
    }


    @Test
    public void testupdateInventoryItemQuantityWithPassResponse() throws Exception {
        final String responseXml = LOAD_RESPONSE_XML;
        mockServer.expect(requestTo("http://localhost:8998"))
                .andExpect(content().xml(loadRequestXml))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(responseXml, MediaType.APPLICATION_XML));
        targetCaInventoryServiceImpl.setClientID("123");
        targetCaInventoryServiceImpl.setWebmethodsBaseUrl("http://localhost:8998");
        targetCaInventoryServiceImpl.setUpdateInventoryItemQuantityUrl("");
        final boolean response = targetCaInventoryServiceImpl.updateInventoryItemQuantity("W83475494", 9999);
        Assert.assertTrue(response);
        mockServer.verify();
    }

    @Test
    public void testupdateInventoryItemQuantityWithFailResponse() throws Exception {
        final String responseXml = LOAD_RESPONSE_XML_WITH_FAIL_MESSAGE;
        mockServer.expect(requestTo("http://localhost:8998"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(responseXml, MediaType.APPLICATION_XML));
        targetCaInventoryServiceImpl.setClientID("123");
        targetCaInventoryServiceImpl.setWebmethodsBaseUrl("http://localhost:8998");
        targetCaInventoryServiceImpl.setUpdateInventoryItemQuantityUrl("");
        final boolean response = targetCaInventoryServiceImpl.updateInventoryItemQuantity("W83475494", 9999);
        Assert.assertTrue(!response);
        mockServer.verify();
    }

    @Test
    public void testupdateInventoryItemQuantityWithProductCodeUnmatch() throws Exception {
        final String responseXml = LOAD_RESPONSE_XML_WITH_PRODUCT_CODE_UNMATCH;
        mockServer.expect(requestTo("http://localhost:8998"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(responseXml, MediaType.APPLICATION_XML));
        targetCaInventoryServiceImpl.setClientID("123");
        targetCaInventoryServiceImpl.setWebmethodsBaseUrl("http://localhost:8998");
        targetCaInventoryServiceImpl.setUpdateInventoryItemQuantityUrl("");
        final boolean response = targetCaInventoryServiceImpl.updateInventoryItemQuantity("W123456", 9999);
        Assert.assertTrue(!response);
        mockServer.verify();
    }

    @Test
    public void testupdateInventoryItemQuantityWithWrongPatternXmlResponse() throws Exception {
        final String responseXml = LOAD_RESPONSE_XML_WITH_WRONG_PATTERN;
        mockServer.expect(requestTo("http://localhost:8998"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(responseXml, MediaType.APPLICATION_XML));
        targetCaInventoryServiceImpl.setClientID("123");
        targetCaInventoryServiceImpl.setWebmethodsBaseUrl("http://localhost:8998");
        targetCaInventoryServiceImpl.setUpdateInventoryItemQuantityUrl("");
        final boolean response = targetCaInventoryServiceImpl.updateInventoryItemQuantity("W83475494", 9999);
        Assert.assertTrue(!response);
        mockServer.verify();
    }

    @Test
    public void testupdateInventoryItemQuantityWithoutResultCodeResponse() throws Exception {
        final String responseXml = LOAD_RESPONSE_XML_WITHOUT_RESULT_CODE;
        mockServer.expect(requestTo("http://localhost:8998"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(responseXml, MediaType.APPLICATION_XML));
        targetCaInventoryServiceImpl.setClientID("123");
        targetCaInventoryServiceImpl.setWebmethodsBaseUrl("http://localhost:8998");
        targetCaInventoryServiceImpl.setUpdateInventoryItemQuantityUrl("");
        final boolean response = targetCaInventoryServiceImpl.updateInventoryItemQuantity("W83475494", 9999);
        Assert.assertTrue(!response);
        mockServer.verify();
    }

    @Test
    public void testupdateInventoryItemQuantityWithReadTimeout() throws Exception {
        mockServer.expect(requestTo("http://localhost:8998"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(new TimeoutResponseCreator());
        targetCaInventoryServiceImpl.setClientID("123");
        targetCaInventoryServiceImpl.setWebmethodsBaseUrl("http://localhost:8998");
        targetCaInventoryServiceImpl.setUpdateInventoryItemQuantityUrl("");
        final boolean response = targetCaInventoryServiceImpl.updateInventoryItemQuantity("W83475494", 9999);
        Assert.assertTrue(!response);
        mockServer.verify();
    }

    /**
     * @return the targetCaInventoryServiceImpl
     */
    public TargetCaInventoryServiceImpl getTargetCaInventoryServiceImpl() {
        return targetCaInventoryServiceImpl;
    }

    /**
     * @param targetCaInventoryServiceImpl
     *            the targetCaInventoryServiceImpl to set
     */
    public void setTargetCaInventoryServiceImpl(final TargetCaInventoryServiceImpl targetCaInventoryServiceImpl) {
        this.targetCaInventoryServiceImpl = targetCaInventoryServiceImpl;
    }

    class TimeoutResponseCreator implements ResponseCreator {

        @Override
        public ClientHttpResponse createResponse(final ClientHttpRequest request) throws IOException {
            throw new RestClientException("Testing timeout exception");
        }

        public TimeoutResponseCreator withTimeout() {
            return new TimeoutResponseCreator();
        }

    }


}
