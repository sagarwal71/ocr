/**
 * 
 */
package au.com.target.tgtwebmethods.customersubscription.client.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPFaultException;

import org.apache.http.auth.UsernamePasswordCredentials;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.sun.xml.internal.ws.client.BindingProviderProperties;
import com.sun.xml.internal.ws.client.ClientTransportException;

import au.com.target.customermgmt.customersingleview.v1.customersingleviewservice.CustomerSingleViewServicePortType;
import au.com.target.customermgmt.customersingleview.v1.customersingleviewservice.ObjectFactory;
import au.com.target.customermgmt.customersingleview.v1.customersingleviewservice.ReturnResponse;
import au.com.target.customermgmt.customersingleview.v1.customersingleviewservice.SetCustomerRequest;
import au.com.target.customermgmt.customersingleview.v1.customersingleviewservice.SetCustomerResponse2;
import au.com.target.customermgmt.customersingleview.v1.customersingleviewservice.TGTCECustomerMgmtWSProviderV1CustomerSingleViewService;
import au.com.target.tgtmail.dto.TargetCustomerChildDetailsDto;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionResponseDto;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionResponseType;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionSource;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionType;
import au.com.target.tgtwebmethods.constants.TgtwebmethodsConstants;
import au.com.target.tgtwebmethods.customersubscription.client.util.CustomerSubscriptionErrorCodeProcessor;
import org.junit.Assert;


/**
 * @author mjanarth
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCustomerSubscriptionClientImplTest {
    private static final String WM_USERNAME = "dwmol";
    private static final String WM_PASSWORD = "password";
    private static final Integer CONNECT_TIMEOUT = Integer.valueOf(20000);
    private static final Integer REQUEST_TIMEOUT = Integer.valueOf(60000);

    @Mock
    private TGTCECustomerMgmtWSProviderV1CustomerSingleViewService mockCustomerSubscriptionServiceProvider;

    @Mock
    private ObjectFactory mockObjectFactory;

    @Mock
    private UsernamePasswordCredentials mockWmCredentials;

    @Mock(extraInterfaces = { BindingProvider.class })
    private CustomerSingleViewServicePortType mockCustomerSubscriptionPort;

    @Mock
    private SetCustomerRequest mockCustomerRequest;

    @Mock
    private SetCustomerResponse2 mockCustomerResponse;

    @Mock
    private ReturnResponse mokcReturnResponse;

    @Mock
    private CustomerSubscriptionErrorCodeProcessor errorCodeProcessor;

    @InjectMocks
    private final TargetCustomerSubscriptionClientImpl targetCustomerSubscriptionClientImpl = new TargetCustomerSubscriptionClientImpl();

    private TargetCustomerSubscriptionRequestDto subscriptionDetails = null;

    private Map<String, Object> requestContextMap;


    @Before
    public void setUp() {
        requestContextMap = new HashMap<String, Object>();

        given(mockCustomerSubscriptionServiceProvider.getTGTCECustomerMgmtWSProviderV1CustomerSingleViewServicePort())
                .willReturn(mockCustomerSubscriptionPort);

        given(((BindingProvider)mockCustomerSubscriptionPort).getRequestContext()).willReturn(requestContextMap);
        given(mockObjectFactory.createSetCustomerRequest()).willReturn(mockCustomerRequest);
        given(mockObjectFactory.createSetCustomerResponse2()).willReturn(mockCustomerResponse);
        given(mockWmCredentials.getUserName()).willReturn(WM_USERNAME);
        given(mockWmCredentials.getPassword()).willReturn(WM_PASSWORD);
        targetCustomerSubscriptionClientImpl.setConnectTimeoutMilliseconds(CONNECT_TIMEOUT);
        targetCustomerSubscriptionClientImpl.setRequestTimeoutMilliseconds(REQUEST_TIMEOUT);
    }

    @Test
    public void testCustomerSubscritionWithEmptyEmail() throws Exception {

        final String responseCode = "-1";
        subscriptionDetails = new TargetCustomerSubscriptionRequestDto();
        given(mockCustomerSubscriptionPort.setCustomer(mockCustomerRequest)).willReturn(
                mockCustomerResponse);
        given(mockCustomerResponse.getReturnResponse()).willReturn(mokcReturnResponse);
        final JAXBElement<String> mockResponseCodeElement = mock(JAXBElement.class);
        given(mockResponseCodeElement.getValue()).willReturn(responseCode);

        final TargetCustomerSubscriptionResponseDto response = targetCustomerSubscriptionClientImpl
                .sendCustomerSubscription(subscriptionDetails);
        assertThat(response.getResponseCode()).isEqualTo(Integer.valueOf(responseCode));
        assertThat(response.getResponseMessage()).isEqualTo(
                TgtwebmethodsConstants.CustomerSubsriptionErrorCodesAndMessages.ERROR_REQUEST_EMAILID);
        assertThat(requestContextMap.get(BindingProvider.USERNAME_PROPERTY)).isEqualTo(WM_USERNAME);
        assertThat(requestContextMap.get(BindingProvider.PASSWORD_PROPERTY)).isEqualTo(WM_PASSWORD);
        assertThat(requestContextMap.get(BindingProviderProperties.CONNECT_TIMEOUT)).isEqualTo(CONNECT_TIMEOUT);
        assertThat(requestContextMap.get(BindingProviderProperties.REQUEST_TIMEOUT)).isEqualTo(REQUEST_TIMEOUT);
    }

    @Test
    public void testCustomerSubscriptionWithNullServiceProvider() {
        targetCustomerSubscriptionClientImpl.setCustomerSubscriptionServiceProvider(null);
        final TargetCustomerSubscriptionResponseDto response = targetCustomerSubscriptionClientImpl
                .sendCustomerSubscription(subscriptionDetails);
        assertThat(response.getResponseCode()).isEqualTo(TgtwebmethodsConstants.CommonErrorCodes.UNAVAILABLE_ERRORCODE);
        assertThat(response.getResponseMessage())
                .isEqualTo(
                        TgtwebmethodsConstants.CustomerSubsriptionErrorCodesAndMessages.ERROR_CUSTOMER_SUBSCRIPTION_SERVICE_NOT_INITIALISED);
        assertThat(response.getResponseType()).isEqualTo(TargetCustomerSubscriptionResponseType.UNAVAILABLE);
    }

    @Test
    public void testCustomerSubscriptionSuccess() throws Exception {

        final String responseCode = "0";
        final String responseMessage = "Success";
        given(mockCustomerSubscriptionPort.setCustomer(mockCustomerRequest)).willReturn(
                mockCustomerResponse);
        given(mockCustomerResponse.getReturnResponse()).willReturn(mokcReturnResponse);
        final JAXBElement<String> mockResponseCodeElement = mock(JAXBElement.class);
        given(mockResponseCodeElement.getValue()).willReturn(responseCode);
        final JAXBElement<String> mockResponseMessageElement = mock(JAXBElement.class);
        given(mockResponseMessageElement.getValue()).willReturn(responseMessage);

        given(mokcReturnResponse.getReturnCode()).willReturn(mockResponseCodeElement);
        given(mokcReturnResponse.getReturnMessage()).willReturn(mockResponseMessageElement);
        given(errorCodeProcessor.getCustomerSubscriptionResponseTypeForCode("0")).willReturn(
                TargetCustomerSubscriptionResponseType.SUCCESS);
        subscriptionDetails = new TargetCustomerSubscriptionRequestDto();
        subscriptionDetails.setCustomerEmail("test@gmail.com");
        subscriptionDetails.setSubscriptionType(TargetCustomerSubscriptionType.NEWSLETTER);
        subscriptionDetails.setCustomerSubscriptionSource(TargetCustomerSubscriptionSource.NEWSLETTER);
        final TargetCustomerSubscriptionResponseDto response = targetCustomerSubscriptionClientImpl
                .sendCustomerSubscription(subscriptionDetails);
        assertThat(response.getResponseCode()).isEqualTo(Integer.valueOf(responseCode));
        assertThat(response.getResponseMessage())
                .isEqualTo(responseMessage);
        assertThat(response.getResponseType()).isEqualTo(TargetCustomerSubscriptionResponseType.SUCCESS);
    }

    @Test
    public void testCustomerSubscriptionSuccessWithNullSource() throws Exception {

        final String responseCode = "0";
        final String responseMessage = "Success";
        given(mockCustomerSubscriptionPort.setCustomer(mockCustomerRequest)).willReturn(
                mockCustomerResponse);
        given(mockCustomerResponse.getReturnResponse()).willReturn(mokcReturnResponse);
        final JAXBElement<String> mockResponseCodeElement = mock(JAXBElement.class);
        given(mockResponseCodeElement.getValue()).willReturn(responseCode);
        final JAXBElement<String> mockResponseMessageElement = mock(JAXBElement.class);
        given(mockResponseMessageElement.getValue()).willReturn(responseMessage);

        given(mokcReturnResponse.getReturnCode()).willReturn(mockResponseCodeElement);
        given(mokcReturnResponse.getReturnMessage()).willReturn(mockResponseMessageElement);
        given(errorCodeProcessor.getCustomerSubscriptionResponseTypeForCode("0")).willReturn(
                TargetCustomerSubscriptionResponseType.SUCCESS);
        subscriptionDetails = new TargetCustomerSubscriptionRequestDto();
        subscriptionDetails.setCustomerEmail("test@gmail.com");
        subscriptionDetails.setSubscriptionType(TargetCustomerSubscriptionType.NEWSLETTER);
        subscriptionDetails.setCustomerSubscriptionSource(null);
        final TargetCustomerSubscriptionResponseDto response = targetCustomerSubscriptionClientImpl
                .sendCustomerSubscription(subscriptionDetails);
        assertThat(response.getResponseCode()).isEqualTo(Integer.valueOf(responseCode));
        assertThat(response.getResponseMessage())
                .isEqualTo(responseMessage);
        assertThat(response.getResponseType()).isEqualTo(TargetCustomerSubscriptionResponseType.SUCCESS);
    }

    @Test
    public void testCustomerSubscriptionSOAPFaultException() throws Exception {
        final String exceptionMessage = "exception Occured";
        final SOAPFaultException ex = mock(SOAPFaultException.class);
        given(ex.getMessage()).willReturn(exceptionMessage);
        given(mockCustomerSubscriptionPort.setCustomer(mockCustomerRequest))
                .willThrow(ex);

        subscriptionDetails = new TargetCustomerSubscriptionRequestDto();
        subscriptionDetails.setCustomerEmail("test@gmail.com");
        subscriptionDetails.setSubscriptionType(TargetCustomerSubscriptionType.NEWSLETTER);
        final TargetCustomerSubscriptionResponseDto response = targetCustomerSubscriptionClientImpl
                .sendCustomerSubscription(subscriptionDetails);
        assertThat(response.getResponseCode()).isEqualTo(TgtwebmethodsConstants.CommonErrorCodes.UNAVAILABLE_ERRORCODE);
        assertThat(response.getResponseMessage())
                .isEqualTo(exceptionMessage);
        assertThat(response.getResponseType()).isEqualTo(TargetCustomerSubscriptionResponseType.UNAVAILABLE);

    }

    @Test
    public void testCustomerSubscriptionWithClientTransportException() throws Exception {
        final String exceptionMessage = "Exception message";
        final ClientTransportException ex = mock(ClientTransportException.class);
        given(ex.getMessage()).willReturn(exceptionMessage);
        given(mockCustomerSubscriptionPort.setCustomer(mockCustomerRequest))
                .willThrow(ex);
        subscriptionDetails = new TargetCustomerSubscriptionRequestDto();
        subscriptionDetails.setCustomerEmail("test@gmail.com");
        subscriptionDetails.setSubscriptionType(TargetCustomerSubscriptionType.NEWSLETTER);
        final TargetCustomerSubscriptionResponseDto response = targetCustomerSubscriptionClientImpl
                .sendCustomerSubscription(subscriptionDetails);
        assertThat(response.getResponseCode()).isEqualTo(TgtwebmethodsConstants.CommonErrorCodes.UNAVAILABLE_ERRORCODE);
        assertThat(response.getResponseMessage())
                .isEqualTo(exceptionMessage);
        assertThat(response.getResponseType()).isEqualTo(TargetCustomerSubscriptionResponseType.UNAVAILABLE);

    }

    @Test
    public void testCustomerSubscriptionWithPersonalDetails() throws Exception {

        final String responseCode = "0";
        final String responseMessage = "Success";

        final JAXBElement<String> mockResponseCodeElement = mock(JAXBElement.class);
        given(mockResponseCodeElement.getValue()).willReturn(responseCode);
        final JAXBElement<String> mockResponseMessageElement = mock(JAXBElement.class);
        given(mockResponseMessageElement.getValue()).willReturn(responseMessage);

        given(mokcReturnResponse.getReturnCode()).willReturn(mockResponseCodeElement);
        given(mokcReturnResponse.getReturnMessage()).willReturn(mockResponseMessageElement);
        given(errorCodeProcessor.getCustomerSubscriptionResponseTypeForCode("0")).willReturn(
                TargetCustomerSubscriptionResponseType.SUCCESS);
        given(mockCustomerResponse.getReturnResponse()).willReturn(mokcReturnResponse);
        given(mockCustomerSubscriptionPort.upsertCustomer(mockCustomerRequest)).willReturn(
                mockCustomerResponse);
        subscriptionDetails = new TargetCustomerSubscriptionRequestDto();
        populatePersonalDetails(subscriptionDetails);
        final TargetCustomerSubscriptionResponseDto response = targetCustomerSubscriptionClientImpl
                .sendCustomerSubscription(subscriptionDetails);
        assertThat(response.getResponseCode()).isEqualTo(Integer.valueOf(responseCode));
        assertThat(response.getResponseMessage())
                .isEqualTo(responseMessage);
        assertThat(response.getResponseType()).isEqualTo(TargetCustomerSubscriptionResponseType.SUCCESS);
    }

    @Test
    public void testCustomerSubscriptionWithChildDetails() throws Exception {

        final String responseCode = "0";
        final String responseMessage = "Success";

        final JAXBElement<String> mockResponseCodeElement = mock(JAXBElement.class);
        given(mockResponseCodeElement.getValue()).willReturn(responseCode);
        final JAXBElement<String> mockResponseMessageElement = mock(JAXBElement.class);
        given(mockResponseMessageElement.getValue()).willReturn(responseMessage);

        given(mokcReturnResponse.getReturnCode()).willReturn(mockResponseCodeElement);
        given(mokcReturnResponse.getReturnMessage()).willReturn(mockResponseMessageElement);
        given(errorCodeProcessor.getCustomerSubscriptionResponseTypeForCode("0")).willReturn(
                TargetCustomerSubscriptionResponseType.SUCCESS);
        given(mockCustomerResponse.getReturnResponse()).willReturn(mokcReturnResponse);
        given(mockCustomerSubscriptionPort.upsertCustomer(mockCustomerRequest)).willReturn(
                mockCustomerResponse);
        subscriptionDetails = new TargetCustomerSubscriptionRequestDto();
        populatePersonalDetails(subscriptionDetails);
        final List<TargetCustomerChildDetailsDto> childrenDetails = new ArrayList<>();
        final TargetCustomerChildDetailsDto child = new TargetCustomerChildDetailsDto();
        child.setBirthday(new Date());
        child.setGender("female");
        child.setFirstName("Firstname");
        childrenDetails.add(child);
        subscriptionDetails.setChildrenDetails(childrenDetails);
        final TargetCustomerSubscriptionResponseDto response = targetCustomerSubscriptionClientImpl
                .sendCustomerSubscription(subscriptionDetails);
        assertThat(response.getResponseCode()).isEqualTo(Integer.valueOf(responseCode));
        assertThat(response.getResponseMessage())
                .isEqualTo(responseMessage);
        assertThat(response.getResponseType()).isEqualTo(TargetCustomerSubscriptionResponseType.SUCCESS);
    }

    @Test
    public void testCreateCustomerSubscriptionRequestWithOptinEmail() throws DatatypeConfigurationException {
        final TargetCustomerSubscriptionRequestDto request = new TargetCustomerSubscriptionRequestDto();
        given(mockObjectFactory.createSetCustomerRequest()).willReturn(new SetCustomerRequest());
        populatePersonalDetails(request);
        request.setUpdateCustomerPersonalDetails(false);
        request.setRegisterForEmail(true);
        request.setSubscriptionType(TargetCustomerSubscriptionType.NEWSLETTER);
        final SetCustomerRequest customerDetails = targetCustomerSubscriptionClientImpl
                .createCustomerSubscriptionRequest(request);
        assertThat(customerDetails.getCustomer().isEnewsoptin()).isEqualTo(true);
        assertThat(customerDetails.getCustomer().isShoppingbasketoptin()).isEqualTo(true);
        assertThat(customerDetails.getCustomer().isEnewsoptin()).isEqualTo(true);

    }

    @Test
    public void testCreateCustomerSubscriptionRequestWithOptoutEmail() throws DatatypeConfigurationException {
        final TargetCustomerSubscriptionRequestDto request = new TargetCustomerSubscriptionRequestDto();
        given(mockObjectFactory.createSetCustomerRequest()).willReturn(new SetCustomerRequest());
        populatePersonalDetails(request);
        request.setUpdateCustomerPersonalDetails(false);
        request.setRegisterForEmail(false);
        final SetCustomerRequest customerDetails = targetCustomerSubscriptionClientImpl
                .createCustomerSubscriptionRequest(request);
        assertThat(customerDetails.getCustomer().isEnewsoptin()).isEqualTo(false);
        assertThat(customerDetails.getCustomer().isShoppingbasketoptin()).isEqualTo(false);
        assertThat(customerDetails.getCustomer().isEnewsoptin()).isEqualTo(false);

    }

    @Test
    public void testCreateCustomerSubscriptionRequestWithMumsHub() throws DatatypeConfigurationException {
        final TargetCustomerSubscriptionRequestDto request = new TargetCustomerSubscriptionRequestDto();
        given(mockObjectFactory.createSetCustomerRequest()).willReturn(new SetCustomerRequest());
        populatePersonalDetails(request);
        request.setUpdateCustomerPersonalDetails(false);
        request.setRegisterForEmail(true);
        request.setSubscriptionType(TargetCustomerSubscriptionType.MUMS_HUB);
        final SetCustomerRequest customerDetails = targetCustomerSubscriptionClientImpl
                .createCustomerSubscriptionRequest(request);
        assertThat(customerDetails.getCustomer().isEnewsoptin()).isEqualTo(false);
        assertThat(customerDetails.getCustomer().isShoppingbasketoptin()).isEqualTo(false);
        assertThat(customerDetails.getCustomer().isEnewsoptin()).isEqualTo(false);

    }

    private void populatePersonalDetails(final TargetCustomerSubscriptionRequestDto request) {
        request.setCustomerEmail("test@gmail.com");
        request.setSubscriptionType(TargetCustomerSubscriptionType.MUMS_HUB);
        request.setFirstName("firstName");
        request.setLastName("lastName");
        request.setGender("female");
        request.setDueDate(new Date());
        request.setUpdateCustomerPersonalDetails(true);
        request.setCustomerSubscriptionSource(null);
        request.setBirthday(new Date());
    }

    @Test
    public void testConvertDatetoXMlCalendarTimeZone() throws DatatypeConfigurationException {
        final Date date = new Date();
        final XMLGregorianCalendar calendar = targetCustomerSubscriptionClientImpl
                .convertDateToXMLCalendarWithoutTimeZone(date);
        Assert.assertEquals(calendar.getTimezone(), DatatypeConstants.FIELD_UNDEFINED);

    }

    @Test
    public void testConvertDatetoXMlCalendarWithNullDate() throws DatatypeConfigurationException {
        final Date date = null;
        final XMLGregorianCalendar calendar = targetCustomerSubscriptionClientImpl
                .convertDateToXMLCalendarWithoutTimeZone(date);
        Assert.assertEquals(calendar, null);

    }

}
