package au.com.target.tgtwebmethods.product.client.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import au.com.target.tgtsale.product.dto.request.ProductRequestDto;
import au.com.target.tgtsale.product.dto.response.ProductResponseDto;
import au.com.target.tgtutility.http.factory.HttpClientFactory;
import au.com.target.tgtwebcore.exception.TargetIntegrationException;
import au.com.target.tgtwebmethods.exception.TargetWebMethodsClientException;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPOSProductClientImplTest {
    private static final String WEBMETHODS_BASE_URL = "http://webmethods:5555";
    private static final String PRICE_CHECK_SERVICE_URL = "/priceCheckService?storeNumber={0}&storeState={1}&barcode={2}";

    @Mock
    private HttpClientFactory mockHttpClientFactory;

    @Mock
    private HttpClient mockHttpClient;

    @InjectMocks
    private final TargetPOSProductClientImpl targetPOSProductClientImpl = new TargetPOSProductClientImpl();

    @Before
    public void setUp() {
        given(mockHttpClientFactory.createHttpClient()).willReturn(mockHttpClient);

        targetPOSProductClientImpl.setWebmethodsBaseUrl(WEBMETHODS_BASE_URL);
        targetPOSProductClientImpl.setPriceCheckServiceUrl(PRICE_CHECK_SERVICE_URL);
    }

    @Test
    public void testGetPOSProductDetailsThrowingClientProtocolException() throws Exception {
        final ProductRequestDto request = new ProductRequestDto();
        request.setStoreNumber("5856");
        request.setStoreState("VIC");
        request.setEan("10181000089");

        given(mockHttpClient.execute(any(HttpGet.class), Matchers.<ResponseHandler<Object>>any())).willThrow(
                new ClientProtocolException());

        try {
            targetPOSProductClientImpl.getPOSProductDetails(request);
            Assert.fail("Expected exception to be thrown.");
        }
        catch (final TargetIntegrationException ex) {
            assertThat(ex).isInstanceOf(TargetWebMethodsClientException.class).hasMessage(
                    TargetPOSProductClientImpl.ERROR_COMMS_FAILURE);
        }
    }

    @Test
    public void testGetPOSProductDetailsThrowingIOException() throws Exception {
        final ProductRequestDto request = new ProductRequestDto();
        request.setStoreNumber("5856");
        request.setStoreState("VIC");
        request.setEan("10181000089");

        given(mockHttpClient.execute(any(HttpGet.class), Matchers.<ResponseHandler<Object>>any())).willThrow(new IOException());

        try {
            targetPOSProductClientImpl.getPOSProductDetails(request);
            Assert.fail("Expected exception to be thrown.");
        }
        catch (final TargetIntegrationException ex) {
            assertThat(ex).isInstanceOf(TargetWebMethodsClientException.class).hasMessage(
                    TargetPOSProductClientImpl.ERROR_COMMS_FAILURE);
        }
    }

    @Test
    public void testGetPOSProductDetailsWithBlankResponse() throws Exception {
        final ProductRequestDto request = new ProductRequestDto();
        request.setStoreNumber("5856");
        request.setStoreState("VIC");
        request.setEan("10181000089");

        final HttpResponse mockHttpResponse = mock(HttpResponse.class);
        given(mockHttpResponse.getEntity()).willReturn(null);

        when(mockHttpClient.execute(any(HttpGet.class), Matchers.<ResponseHandler<Object>>any())).thenAnswer(new Answer() {

            /*
             * (non-Javadoc)
             * 
             * @see org.mockito.stubbing.Answer#answer(org.mockito.invocation.InvocationOnMock)
             */
            @Override
            public Object answer(final InvocationOnMock invocation) throws IOException {
                final Object[] args = invocation.getArguments();
                final ResponseHandler responseHandler = (ResponseHandler)args[1];

                return responseHandler.handleResponse(mockHttpResponse);
            }

        });

        try {
            targetPOSProductClientImpl.getPOSProductDetails(request);
            Assert.fail("Expected exception to be thrown.");
        }
        catch (final TargetIntegrationException ex) {
            assertThat(ex).isInstanceOf(TargetWebMethodsClientException.class).hasMessage(
                    TargetPOSProductClientImpl.ERROR_EMPTY_RESPONSE);
        }
    }

    @Test
    public void testGetPOSProductDetailsWithInvalidJSON() throws Exception {
        final ProductRequestDto request = new ProductRequestDto();
        request.setStoreNumber("5856");
        request.setStoreState("VIC");
        request.setEan("10181000089");

        final StringBuilder responsePayload = new StringBuilder();
        responsePayload.append("This is not valid JSON");

        final InputStream responseInputStream = new ByteArrayInputStream(responsePayload.toString().getBytes());

        final HttpEntity mockHttpEntity = mock(HttpEntity.class);
        given(mockHttpEntity.getContent()).willReturn(responseInputStream);

        final HttpResponse mockHttpResponse = mock(HttpResponse.class);
        given(mockHttpResponse.getEntity()).willReturn(mockHttpEntity);

        when(mockHttpClient.execute(any(HttpGet.class), Matchers.<ResponseHandler<Object>>any())).thenAnswer(new Answer() {

            /*
             * (non-Javadoc)
             * 
             * @see org.mockito.stubbing.Answer#answer(org.mockito.invocation.InvocationOnMock)
             */
            @Override
            public Object answer(final InvocationOnMock invocation) throws IOException {
                final Object[] args = invocation.getArguments();
                final ResponseHandler responseHandler = (ResponseHandler)args[1];

                return responseHandler.handleResponse(mockHttpResponse);
            }

        });

        try {
            targetPOSProductClientImpl.getPOSProductDetails(request);
            Assert.fail("Expected exception to be thrown.");
        }
        catch (final TargetIntegrationException ex) {
            assertThat(ex).isInstanceOf(TargetWebMethodsClientException.class).hasMessage(
                    TargetPOSProductClientImpl.ERROR_JSON_PARSE);
        }
    }

    @Test
    public void testGetPOSProductDetailsWithErrorFromPOS() throws Exception {
        final ProductRequestDto request = new ProductRequestDto();
        request.setStoreNumber("5856");
        request.setStoreState("VIC");
        request.setEan("10181000089");

        final String rc = "268435460";
        final String msg = "Itemcode not found.";

        final StringBuilder responsePayload = new StringBuilder();
        responsePayload.append("{");
        responsePayload.append("\"success\":false,");
        responsePayload.append("\"rc\":" + rc + ",");
        responsePayload.append("\"msg\":\"" + msg + "\",");
        responsePayload.append("\"errorSource\":\"pos\"");
        responsePayload.append("}");

        final InputStream responseInputStream = new ByteArrayInputStream(responsePayload.toString().getBytes());

        final HttpEntity mockHttpEntity = mock(HttpEntity.class);
        given(mockHttpEntity.getContent()).willReturn(responseInputStream);

        final HttpResponse mockHttpResponse = mock(HttpResponse.class);
        given(mockHttpResponse.getEntity()).willReturn(mockHttpEntity);

        when(mockHttpClient.execute(any(HttpGet.class), Matchers.<ResponseHandler<Object>>any())).thenAnswer(new Answer() {

            /*
             * (non-Javadoc)
             * 
             * @see org.mockito.stubbing.Answer#answer(org.mockito.invocation.InvocationOnMock)
             */
            @Override
            public Object answer(final InvocationOnMock invocation) throws IOException {
                final Object[] args = invocation.getArguments();
                final ResponseHandler responseHandler = (ResponseHandler)args[1];

                return responseHandler.handleResponse(mockHttpResponse);
            }

        });

        final ProductResponseDto response = targetPOSProductClientImpl.getPOSProductDetails(request);

        assertThat(response).isNotNull();
        assertThat(response.getSuccess()).isFalse();
        assertThat(response.getErrorMessage()).isEqualTo(msg);
    }

    @Test
    public void testGetPOSProductDetailsWithErrorFromWebMethods() throws Exception {
        final ProductRequestDto request = new ProductRequestDto();
        request.setStoreNumber("5856");
        request.setStoreState("VIC");
        request.setEan("10181000089");

        final String msg = "Server not found";

        final StringBuilder responsePayload = new StringBuilder();
        responsePayload.append("{");
        responsePayload.append("\"success\":false,");
        responsePayload.append("\"msg\":\"" + msg + "\",");
        responsePayload.append("\"errorSource\":\"wm\"");
        responsePayload.append("}");

        final InputStream responseInputStream = new ByteArrayInputStream(responsePayload.toString().getBytes());

        final HttpEntity mockHttpEntity = mock(HttpEntity.class);
        given(mockHttpEntity.getContent()).willReturn(responseInputStream);

        final HttpResponse mockHttpResponse = mock(HttpResponse.class);
        given(mockHttpResponse.getEntity()).willReturn(mockHttpEntity);

        when(mockHttpClient.execute(any(HttpGet.class), Matchers.<ResponseHandler<Object>>any())).thenAnswer(new Answer() {

            /*
             * (non-Javadoc)
             * 
             * @see org.mockito.stubbing.Answer#answer(org.mockito.invocation.InvocationOnMock)
             */
            @Override
            public Object answer(final InvocationOnMock invocation) throws IOException {
                final Object[] args = invocation.getArguments();
                final ResponseHandler responseHandler = (ResponseHandler)args[1];

                return responseHandler.handleResponse(mockHttpResponse);
            }

        });

        try {
            targetPOSProductClientImpl.getPOSProductDetails(request);
            Assert.fail("Expected exception to be thrown.");
        }
        catch (final TargetIntegrationException ex) {
            assertThat(ex).isInstanceOf(TargetWebMethodsClientException.class).hasMessage(msg);
        }
    }

    @Test
    public void testGetPOSProductDetailsWithTimeoutFromWebMethods() throws Exception {
        final ProductRequestDto request = new ProductRequestDto();
        request.setStoreNumber("5856");
        request.setStoreState("VIC");
        request.setEan("10181000089");

        final String msg = "Timeout";

        final StringBuilder responsePayload = new StringBuilder();
        responsePayload.append("{");
        responsePayload.append("\"success\":false,");
        responsePayload.append("\"msg\":\"" + msg + "\",");
        responsePayload.append("\"errorSource\":\"wm\",");
        responsePayload.append("\"timeout\":true");
        responsePayload.append("}");

        final InputStream responseInputStream = new ByteArrayInputStream(responsePayload.toString().getBytes());

        final HttpEntity mockHttpEntity = mock(HttpEntity.class);
        given(mockHttpEntity.getContent()).willReturn(responseInputStream);

        final HttpResponse mockHttpResponse = mock(HttpResponse.class);
        given(mockHttpResponse.getEntity()).willReturn(mockHttpEntity);

        when(mockHttpClient.execute(any(HttpGet.class), Matchers.<ResponseHandler<Object>>any())).thenAnswer(new Answer() {

            /*
             * (non-Javadoc)
             * 
             * @see org.mockito.stubbing.Answer#answer(org.mockito.invocation.InvocationOnMock)
             */
            @Override
            public Object answer(final InvocationOnMock invocation) throws IOException {
                final Object[] args = invocation.getArguments();
                final ResponseHandler responseHandler = (ResponseHandler)args[1];

                return responseHandler.handleResponse(mockHttpResponse);
            }

        });

        final ProductResponseDto response = targetPOSProductClientImpl.getPOSProductDetails(request);

        assertThat(response).isNotNull();
        assertThat(response.getSuccess()).isFalse();
        assertThat(response.getErrorMessage()).isEqualTo(msg);
        assertThat(response.isTimeout()).isTrue();
    }

    @Test
    public void testGetPOSProductDetailsWithBasicProductFromPOS() throws Exception {
        final ProductRequestDto request = new ProductRequestDto();
        request.setStoreNumber("5856");
        request.setStoreState("VIC");
        request.setEan("10181000089");

        final String rc = "0";
        final String itemcode = "41906897";
        final String desc = "LIP BALM";
        final int price = 350;
        final String msg = "|41906897|LIP BALM|$3.50";

        final StringBuilder responsePayload = new StringBuilder();
        responsePayload.append("{");
        responsePayload.append("\"success\":true,");
        responsePayload.append("\"rc\":" + rc + ",");
        responsePayload.append("\"itemcode\":\"" + itemcode + "\",");
        responsePayload.append("\"desc\":\"" + desc + "\",");
        responsePayload.append("\"price\":" + price + ",");
        responsePayload.append("\"msg\":\"" + msg + "\"");
        responsePayload.append("}");

        final InputStream responseInputStream = new ByteArrayInputStream(responsePayload.toString().getBytes());

        final HttpEntity mockHttpEntity = mock(HttpEntity.class);
        given(mockHttpEntity.getContent()).willReturn(responseInputStream);

        final HttpResponse mockHttpResponse = mock(HttpResponse.class);
        given(mockHttpResponse.getEntity()).willReturn(mockHttpEntity);

        when(mockHttpClient.execute(any(HttpGet.class), Matchers.<ResponseHandler<Object>>any())).thenAnswer(new Answer() {

            /*
             * (non-Javadoc)
             * 
             * @see org.mockito.stubbing.Answer#answer(org.mockito.invocation.InvocationOnMock)
             */
            @Override
            public Object answer(final InvocationOnMock invocation) throws IOException {
                final Object[] args = invocation.getArguments();
                final ResponseHandler responseHandler = (ResponseHandler)args[1];

                return responseHandler.handleResponse(mockHttpResponse);
            }

        });

        final ProductResponseDto response = targetPOSProductClientImpl.getPOSProductDetails(request);

        assertThat(response).isNotNull();
        assertThat(response.getSuccess()).isTrue();
        assertThat(response.getItemcode()).isEqualTo(itemcode);
        assertThat(response.getDescription()).isEqualTo(desc);
        assertThat(response.getPrice()).isEqualTo(price);
        assertThat(response.getErrorMessage()).isEqualTo(msg);
    }
}
