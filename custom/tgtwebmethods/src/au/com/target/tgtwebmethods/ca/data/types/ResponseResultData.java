/**
 * 
 */
package au.com.target.tgtwebmethods.ca.data.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author bhuang3
 * 
 */
@XmlRootElement(name = "resultData")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseResultData {

    @XmlElement
    private boolean status;

    @XmlElement
    private Integer messageCode;

    @XmlElement
    private String message;

    /**
     * @return the status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(final boolean status) {
        this.status = status;
    }

    /**
     * @return the messageCode
     */
    public Integer getMessageCode() {
        return messageCode;
    }

    /**
     * @param messageCode
     *            the messageCode to set
     */
    public void setMessageCode(final Integer messageCode) {
        this.messageCode = messageCode;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(final String message) {
        this.message = message;
    }


}
