/**
 * 
 */
package au.com.target.tgtwebmethods.ca.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.logging.JaxbLogger;
import au.com.target.tgtutility.util.SplunkLogFormatter;
import au.com.target.tgtwebmethods.ca.TargetCaShippingService;
import au.com.target.tgtwebmethods.ca.data.request.SubmitOrderShipmentRequest;
import au.com.target.tgtwebmethods.ca.data.response.SubmitOrderShipmentResponse;
import au.com.target.tgtwebmethods.ca.data.types.OrderShipmentLineItem;
import au.com.target.tgtwebmethods.ca.data.types.ShipmentOrder;
import au.com.target.tgtwebmethods.client.AbstractWebMethodsClient;
import au.com.target.tgtwebmethods.constants.TgtwebmethodsConstants;


/**
 * @author bhuang3
 * 
 */
public class TargetCaShippingServiceImpl extends AbstractWebMethodsClient implements TargetCaShippingService {

    private static final Logger LOG = Logger.getLogger(TargetCaShippingServiceImpl.class);

    private static final String ERROR_CONNECTERROR = "Unable to get response from rest web service for order shipment request";
    private static final String ERROR_MESSAGE_NOT_READABLE = "The Response xml unmarshalle failure ";
    private static final String ERROR_REQUEST_NULL = "The request cannot be null ";
    private static final String ERROR_ORDER_SHIPMENT_FAILED = "Partner shipment update failed order={0}, reason={1}";
    private static final String ERROR_NULL_RESPONSE_ELEMENT = "ResultData or messagecode in Response is null";

    private String submitOrderShipmentListRequestUrl;
    private String clientId;
    private RestTemplate caWebmethodRestTemplate;


    @Override
    public boolean submitDeliveryOrderStatusForCompleteUpdate(final OrderModel orderModel) {

        final SubmitOrderShipmentRequest shipmentRequest = createDeliveryOrderShipmentRequest(
                orderModel);

        if (shipmentRequest == null) {
            LOG.error(ERROR_REQUEST_NULL);
            return false;
        }

        JaxbLogger.logXml(LOG, shipmentRequest);

        return processOrderShipmentListRequest(shipmentRequest);
    }

    @Override
    public boolean submitDeliveryConsignmentStatus(final ConsignmentModel consignmentModel) {

        final SubmitOrderShipmentRequest shipmentRequest = createDeliveryConsignmentShipmentRequest(
                consignmentModel);

        if (shipmentRequest == null) {
            LOG.error(ERROR_REQUEST_NULL);
            return false;
        }

        JaxbLogger.logXml(LOG, shipmentRequest);

        return processOrderShipmentListRequest(shipmentRequest);
    }

    @Override
    public boolean submitCncOrderStatusForReadyForPickupUpdate(final OrderModel orderModel) {

        final SubmitOrderShipmentRequest shipmentRequest = createCncOrderShipmentRequest(orderModel,
                TgtwebmethodsConstants.PartnerFulFillmentConstants.STATUS_READYFORPICKUP);

        if (shipmentRequest == null) {
            LOG.error(ERROR_REQUEST_NULL);
            return false;
        }

        JaxbLogger.logXml(LOG, shipmentRequest);

        return processOrderShipmentListRequest(shipmentRequest);
    }

    @Override
    public boolean submitCncOrderStatusForPickedupUpdate(final OrderModel orderModel) {

        final SubmitOrderShipmentRequest shipmentRequest = createCncOrderShipmentRequest(orderModel,
                TgtwebmethodsConstants.PartnerFulFillmentConstants.STATUS_COMPLETE);

        if (shipmentRequest == null) {
            LOG.error(ERROR_REQUEST_NULL);
            return false;
        }

        JaxbLogger.logXml(LOG, shipmentRequest);

        return processOrderShipmentListRequest(shipmentRequest);
    }

    /**
     * Creates the submit order shipment list request.
     *
     * @param orderModel
     *            the order model
     * @return the submit order shipment request
     */
    protected SubmitOrderShipmentRequest createDeliveryOrderShipmentRequest(final OrderModel orderModel) {

        Assert.notNull(orderModel, "Order can't be null");

        final ShipmentOrder shipmentOrder = new ShipmentOrder();
        final Integer ebayOrderNumber = orderModel.getEBayOrderNumber();
        final SalesApplication salesApplication = orderModel.getSalesApplication();

        if (null == salesApplication) {
            LOG.error(SplunkLogFormatter.formatMessage(
                    "Shipping request can not be created for partner order as sales application is null for orderNumber="
                            + orderModel.getCode() + ", partnerOrderNumber=" + ebayOrderNumber,
                    TgtutilityConstants.ErrorCode.ERR_TGTEBAY));
            return null;
        }

        if (null != ebayOrderNumber) {
            shipmentOrder.setOrderId(ebayOrderNumber.toString());
        }

        if (CollectionUtils.isNotEmpty(orderModel.getConsignments())) {
            for (final ConsignmentModel consignment : orderModel.getConsignments()) {
                if (ConsignmentStatus.SHIPPED.equals(consignment.getStatus())) {
                    addDateShippedDetails(shipmentOrder, consignment, null);
                    shipmentOrder.setTrackingNumber(consignment.getTrackingID());
                    addCarrierDetails(consignment, shipmentOrder);
                }
            }


            final SubmitOrderShipmentRequest shipmentRequest = new SubmitOrderShipmentRequest();
            final List<ShipmentOrder> shipmentList = new ArrayList<>();

            if (CollectionUtils.isNotEmpty(orderModel.getModificationRecords())) {
                addShippedLineItemsDetails(orderModel, shipmentOrder);
                shipmentOrder.setShipmentType(TgtwebmethodsConstants.PartnerFulFillmentConstants.SHIPMENT_PARTIAL);
            }
            else {
                shipmentOrder.setShipmentType(TgtwebmethodsConstants.PartnerFulFillmentConstants.SHIPMENT_FULL);
            }

            shipmentList.add(shipmentOrder);
            shipmentRequest.setClientId(clientId);
            shipmentRequest.setShipmentList(shipmentList);
            shipmentRequest.setSalesChannel(salesApplication.getCode());

            return shipmentRequest;
        }

        return null;
    }


    /**
     * Creates the submit order shipment list request.
     *
     * @param consignment
     * @return the submit order shipment request
     */
    protected SubmitOrderShipmentRequest createDeliveryConsignmentShipmentRequest(
            final ConsignmentModel consignment) {

        Assert.notNull(consignment, "consignment can't be null");

        final OrderModel orderModel = (OrderModel)consignment.getOrder();
        final ShipmentOrder shipmentOrder = new ShipmentOrder();
        final Integer ebayOrderNumber = orderModel.getEBayOrderNumber();
        final SalesApplication salesApplication = orderModel.getSalesApplication();

        if (null == salesApplication) {
            LOG.error(SplunkLogFormatter.formatMessage(
                    "Shipping request can not be created for partner order as sales application is null for orderNumber="
                            + orderModel.getCode() + ", partnerOrderNumber=" + ebayOrderNumber,
                    TgtutilityConstants.ErrorCode.ERR_TGTEBAY));
            return null;
        }

        if (null != ebayOrderNumber) {
            shipmentOrder.setOrderId(ebayOrderNumber.toString());
        }

        if (ConsignmentStatus.SHIPPED.equals(consignment.getStatus())) {
            addDateShippedDetails(shipmentOrder, consignment, null);
            shipmentOrder.setTrackingNumber(consignment.getTrackingID());
            addCarrierDetails(consignment, shipmentOrder);
        }

        final SubmitOrderShipmentRequest shipmentRequest = new SubmitOrderShipmentRequest();
        final List<ShipmentOrder> shipmentList = new ArrayList<>();
        addShippedConsignmentLineItemsDetails(consignment, shipmentOrder);
        shipmentOrder.setShipmentType(TgtwebmethodsConstants.PartnerFulFillmentConstants.SHIPMENT_PARTIAL);

        shipmentList.add(shipmentOrder);
        shipmentRequest.setClientId(clientId);
        shipmentRequest.setShipmentList(shipmentList);
        shipmentRequest.setSalesChannel(salesApplication.getCode());

        return shipmentRequest;

    }

    private ShipmentOrder addShippedConsignmentLineItemsDetails(final ConsignmentModel consignment,
            final ShipmentOrder orderShipment) {

        if (CollectionUtils.isNotEmpty(consignment.getConsignmentEntries())) {
            final List<OrderShipmentLineItem> lineItems = new ArrayList<>();
            for (final ConsignmentEntryModel consignmentEntry : consignment.getConsignmentEntries()) {
                final OrderShipmentLineItem lineItem = new OrderShipmentLineItem();
                lineItem.setProductCode(consignmentEntry.getOrderEntry().getProduct().getCode());
                lineItem.setQuantity(consignmentEntry.getShippedQuantity().longValue());
                lineItems.add(lineItem);
            }
            orderShipment.setOrderShipmentLineItems(lineItems);
        }

        return orderShipment;
    }

    /**
     * creates SubmitOrderShipmentRequest for sending order status to ca
     * 
     * @param orderModel
     * @param orderStatus
     * @return SubmitOrderShipmentRequest
     */
    protected SubmitOrderShipmentRequest createCncOrderShipmentRequest(final OrderModel orderModel,
            final String orderStatus) {
        Assert.notNull(orderModel, "Order can't be null");

        final ShipmentOrder shipmentOrder = new ShipmentOrder();
        final Integer ebayOrderNumber = orderModel.getEBayOrderNumber();
        final SalesApplication salesApplication = orderModel.getSalesApplication();

        if (null == salesApplication) {
            LOG.error(SplunkLogFormatter.formatMessage(
                    "Shipping request can not be created for partner order as sales application is null for orderNumber="
                            + orderModel.getCode() + ", partnerOrderNumber=" + ebayOrderNumber,
                    TgtutilityConstants.ErrorCode.ERR_TGTEBAY));
            return null;
        }

        if (null != ebayOrderNumber) {
            shipmentOrder.setOrderId(ebayOrderNumber.toString());
        }

        if (CollectionUtils.isNotEmpty(orderModel.getConsignments())) {
            for (final ConsignmentModel consignment : orderModel.getConsignments()) {
                if (ConsignmentStatus.SHIPPED.equals(consignment.getStatus())) {
                    addDateShippedDetails(shipmentOrder, consignment, orderStatus);
                    shipmentOrder.setTrackingNumber(consignment.getCode());
                    addCarrierDetails(consignment, shipmentOrder);
                }
            }

            final SubmitOrderShipmentRequest shipmentRequest = new SubmitOrderShipmentRequest();
            final List<ShipmentOrder> shipmentList = new ArrayList<>();

            shipmentOrder.setShipmentType(TgtwebmethodsConstants.PartnerFulFillmentConstants.SHIPMENT_PARTIAL);
            shipmentOrder.setFulfillmentType(TgtwebmethodsConstants.PartnerFulFillmentConstants.TYPE_PICKUP);
            shipmentOrder.setFulfillmentStatus(orderStatus);

            addShippedLineItemsDetails(orderModel, shipmentOrder);

            shipmentList.add(shipmentOrder);
            shipmentRequest.setClientId(clientId);
            shipmentRequest.setShipmentList(shipmentList);
            shipmentRequest.setSalesChannel(salesApplication.getCode());

            return shipmentRequest;
        }

        return null;
    }

    /**
     * Method to populate Date Shipped GMT based on order status.
     * 
     * @param shipmentOrder
     * @param consignment
     * @param orderStatus
     */
    private void addDateShippedDetails(final ShipmentOrder shipmentOrder, final ConsignmentModel consignment,
            final String orderStatus) {
        if (consignment instanceof TargetConsignmentModel) {
            final TargetConsignmentModel targetConsignment = (TargetConsignmentModel)consignment;
            if (StringUtils.equals(orderStatus,
                    TgtwebmethodsConstants.PartnerFulFillmentConstants.STATUS_READYFORPICKUP)) {
                shipmentOrder.setDateShippedGMT(targetConsignment.getReadyForPickUpDate());
            }
            else if (StringUtils.equals(orderStatus,
                    TgtwebmethodsConstants.PartnerFulFillmentConstants.STATUS_COMPLETE)) {
                shipmentOrder.setDateShippedGMT(targetConsignment.getPickedUpDate() != null
                        ? targetConsignment.getPickedUpDate() : targetConsignment.getPickedUpAutoDate());
            }
            else {
                shipmentOrder.setDateShippedGMT(targetConsignment.getShippingDate());
            }
        }
    }

    private ShipmentOrder addShippedLineItemsDetails(final OrderModel orderModel,
            final ShipmentOrder orderShipment) {

        for (final ConsignmentModel consignment : orderModel.getConsignments()) {
            if (CollectionUtils.isNotEmpty(consignment.getConsignmentEntries())) {
                final List<OrderShipmentLineItem> lineItems = new ArrayList<>();
                for (final ConsignmentEntryModel consignmentEntry : consignment.getConsignmentEntries()) {
                    final OrderShipmentLineItem lineItem = new OrderShipmentLineItem();
                    final AbstractOrderEntryModel orderEntry = consignmentEntry.getOrderEntry();
                    if (null != orderEntry) {
                        lineItem.setProductCode(orderEntry.getProduct().getCode());
                    }
                    lineItem.setQuantity(consignmentEntry.getShippedQuantity().longValue());
                    lineItems.add(lineItem);
                }
                orderShipment.setOrderShipmentLineItems(lineItems);
            }
        }

        return orderShipment;
    }

    /**
     * Process order shipment list request.
     *
     * @param submitOrderShipmentListRequest
     *            the submit order shipment list request
     * @return true, if successful
     */
    protected boolean processOrderShipmentListRequest(final SubmitOrderShipmentRequest submitOrderShipmentListRequest) {
        ResponseEntity<SubmitOrderShipmentResponse> responseEntity = null;
        try {
            responseEntity = caWebmethodRestTemplate.exchange(getWebmethodsBaseUrl()
                    + submitOrderShipmentListRequestUrl, HttpMethod.POST, new HttpEntity(
                            submitOrderShipmentListRequest),
                    SubmitOrderShipmentResponse.class);
        }
        catch (final RestClientException rce) {
            LOG.error(ERROR_CONNECTERROR, rce);
            return false;
        }

        final SubmitOrderShipmentResponse submitOrderShipmentListResponse = responseEntity.getBody();

        JaxbLogger.logXml(LOG, submitOrderShipmentListResponse);

        if (submitOrderShipmentListResponse != null) {
            final List<ShipmentOrder> shipmentList = submitOrderShipmentListResponse.getShipmentList();
            if (CollectionUtils.isNotEmpty(shipmentList)) {
                final ShipmentOrder orderShipment = shipmentList.get(0);
                if (orderShipment.getResultData() != null) {
                    if (!orderShipment.getResultData().isStatus()) {
                        LOG.error(MessageFormat.format(ERROR_ORDER_SHIPMENT_FAILED,
                                submitOrderShipmentListRequest.getShipmentList().get(0).getOrderId(),
                                orderShipment.getResultData().getMessage()));
                    }
                    return orderShipment.getResultData().isStatus();
                }
                else {
                    LOG.error(ERROR_NULL_RESPONSE_ELEMENT);
                    return false;
                }

            }
        }
        LOG.error(ERROR_MESSAGE_NOT_READABLE);
        return false;
    }

    /**
     * Method to add carrier details to shipment order
     * 
     * @param consignment
     * @param shipmentOrder
     */
    private void addCarrierDetails(final ConsignmentModel consignment, final ShipmentOrder shipmentOrder) {
        if (consignment instanceof TargetConsignmentModel) {
            final TargetConsignmentModel targetConsignment = (TargetConsignmentModel)consignment;
            final TargetCarrierModel carrier = targetConsignment.getTargetCarrier();
            if (carrier != null) {
                shipmentOrder.setCarrierCode(carrier.getPartnerCarrierCode());
                shipmentOrder.setClassCode(carrier.getPartnerCarrierClass());
            }
        }
    }

    @Override
    protected String getServiceEndpoint() {
        return submitOrderShipmentListRequestUrl;
    }

    @Override
    protected RestTemplate getRestTemplate() {
        return caWebmethodRestTemplate;
    }

    /**
     * @param submitOrderShipmentListRequestUrl
     *            the submitOrderShipmentListRequestUrl to set
     */
    @Required
    public void setSubmitOrderShipmentListRequestUrl(final String submitOrderShipmentListRequestUrl) {
        this.submitOrderShipmentListRequestUrl = submitOrderShipmentListRequestUrl;
    }

    /**
     * @param clientId
     *            the clientId to set
     */
    @Required
    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    /**
     * @param caWebmethodRestTemplate
     *            the caWebmethodRestTemplate to set
     */
    @Required
    public void setCaWebmethodRestTemplate(final RestTemplate caWebmethodRestTemplate) {
        this.caWebmethodRestTemplate = caWebmethodRestTemplate;
    }

}
