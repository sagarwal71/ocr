/**
 * 
 */
package au.com.target.tgtwebmethods.ca.data.types;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author bhuang3
 * 
 */
@XmlRootElement(name = "shipmentOrder")
@XmlAccessorType(XmlAccessType.FIELD)
public class ShipmentOrder {

    @XmlElement
    private String orderId;

    @XmlElement
    private String shipmentType;

    @XmlElement
    private Date dateShippedGMT;

    @XmlElement
    private String carrierCode;

    @XmlElement
    private String classCode;

    @XmlElement
    private String trackingNumber;

    @XmlElement
    private Double shipmentCost;

    @XmlElementWrapper(name = "lineItems")
    @XmlElement(name = "lineItem")
    private List<OrderShipmentLineItem> orderShipmentLineItems;

    @XmlElement
    private ResponseResultData resultData;

    @XmlElement
    private String fulfillmentType;

    @XmlElement
    private String fulfillmentStatus;



    /**
     * @return the orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * @param orderId
     *            the orderId to set
     */
    public void setOrderId(final String orderId) {
        this.orderId = orderId;
    }

    /**
     * @return the shipmentType
     */
    public String getShipmentType() {
        return shipmentType;
    }

    /**
     * @param shipmentType
     *            the shipmentType to set
     */
    public void setShipmentType(final String shipmentType) {
        this.shipmentType = shipmentType;
    }

    /**
     * @return the dateShippedGMT
     */
    public Date getDateShippedGMT() {
        return dateShippedGMT;
    }

    /**
     * @param dateShippedGMT
     *            the dateShippedGMT to set
     */
    public void setDateShippedGMT(final Date dateShippedGMT) {
        this.dateShippedGMT = dateShippedGMT;
    }

    /**
     * @return the carrierCode
     */
    public String getCarrierCode() {
        return carrierCode;
    }

    /**
     * @param carrierCode
     *            the carrierCode to set
     */
    public void setCarrierCode(final String carrierCode) {
        this.carrierCode = carrierCode;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * @param trackingNumber
     *            the trackingNumber to set
     */
    public void setTrackingNumber(final String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the resultData
     */
    public ResponseResultData getResultData() {
        return resultData;
    }

    /**
     * @param resultData
     *            the resultData to set
     */
    public void setResultData(final ResponseResultData resultData) {
        this.resultData = resultData;
    }

    /**
     * @return the classCode
     */
    public String getClassCode() {
        return classCode;
    }

    /**
     * @param classCode
     *            the classCode to set
     */
    public void setClassCode(final String classCode) {
        this.classCode = classCode;
    }

    /**
     * @return the shipmentCost
     */
    public Double getShipmentCost() {
        return shipmentCost;
    }

    /**
     * @param shipmentCost
     *            the shipmentCost to set
     */
    public void setShipmentCost(final Double shipmentCost) {
        this.shipmentCost = shipmentCost;
    }

    /**
     * @return the orderShipmentLineItems
     */
    public List<OrderShipmentLineItem> getOrderShipmentLineItems() {
        return orderShipmentLineItems;
    }

    /**
     * @param orderShipmentLineItems
     *            the orderShipmentLineItems to set
     */
    public void setOrderShipmentLineItems(final List<OrderShipmentLineItem> orderShipmentLineItems) {
        this.orderShipmentLineItems = orderShipmentLineItems;
    }

    /**
     * @return the fulfillmentType
     */
    public String getFulfillmentType() {
        return fulfillmentType;
    }

    /**
     * @param fulfillmentType
     *            the fulfillmentType to set
     */
    public void setFulfillmentType(final String fulfillmentType) {
        this.fulfillmentType = fulfillmentType;
    }

    /**
     * @return the fulfillmentStatus
     */
    public String getFulfillmentStatus() {
        return fulfillmentStatus;
    }

    /**
     * @param fulfillmentStatus
     *            the fulfillmentStatus to set
     */
    public void setFulfillmentStatus(final String fulfillmentStatus) {
        this.fulfillmentStatus = fulfillmentStatus;
    }

}
