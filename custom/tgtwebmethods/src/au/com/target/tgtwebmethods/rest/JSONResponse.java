/**
 * 
 */
package au.com.target.tgtwebmethods.rest;

import java.util.List;


/**
 * Response from web methods.
 * 
 * @author jjayawa1
 *
 */
public class JSONResponse {
    private boolean success;
    private int responseCode;
    private List<ErrorDTO> errors;

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success
     *            the success to set
     */
    public void setSuccess(final boolean success) {
        this.success = success;
    }

    /**
     * @return the errors
     */
    public List<ErrorDTO> getErrors() {
        return errors;
    }

    /**
     * @param errors
     *            the errors to set
     */
    public void setErrors(final List<ErrorDTO> errors) {
        this.errors = errors;
    }

    /**
     * @return the responseCode
     */
    public int getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode
     *            the responseCode to set
     */
    public void setResponseCode(final int responseCode) {
        this.responseCode = responseCode;
    }

}
