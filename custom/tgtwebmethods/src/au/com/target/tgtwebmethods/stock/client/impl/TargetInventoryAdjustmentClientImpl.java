/**
 * 
 */
package au.com.target.tgtwebmethods.stock.client.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtfulfilment.inventory.dto.InventoryAdjustmentData;
import au.com.target.tgtfulfilment.stock.client.InventoryAdjustmentClient;
import au.com.target.tgtwebcore.exception.TargetIntegrationException;
import au.com.target.tgtwebmethods.client.AbstractWebMethodsClient;
import au.com.target.tgtwebmethods.inventory.dto.request.InventoryAdjustmentDto;
import au.com.target.tgtwebmethods.inventory.dto.request.InventoryAdjustmentRequestDto;
import au.com.target.tgtwebmethods.inventory.dto.response.ErrorDetailsDto;
import au.com.target.tgtwebmethods.inventory.dto.response.InventoryAdjustmentResponseDto;
import au.com.target.tgtwebmethods.util.WMUtil;


/**
 * @author pthoma20
 *
 */
public class TargetInventoryAdjustmentClientImpl extends AbstractWebMethodsClient
        implements InventoryAdjustmentClient {

    private static final Logger LOG = Logger.getLogger(TargetInventoryAdjustmentClientImpl.class);
    private static final String LOGGING_PREFIX = "Inventory-Adjustment-WM:";
    private static final String SUCCESS_RESPONSE_CODE = "0";
    private RestTemplate restTemplate;
    private String serviceEndpoint;
    private HttpHeaders jsonHttpHeader;
    private String source;
    private String user;
    private String reasonCode;
    private String codeType;



    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.stock.client.TargetInventoryAdjustmentClient#sendInventoryAdjustment(au.com.target.tgtfulfilment.inventory.dto.request.InventoryAdjustmentRequestDto)
     */
    @Override
    public boolean sendInventoryAdjustment(final List<InventoryAdjustmentData> inventoryAdjustmentDataList)
            throws TargetIntegrationException {


        if (!(validateConnection())) {
            LOG.error(LOGGING_PREFIX + " Invalid Connection");
            return false;
        }

        if (CollectionUtils.isEmpty(inventoryAdjustmentDataList)) {
            LOG.error(LOGGING_PREFIX + " Data is null");
            return false;
        }

        final InventoryAdjustmentRequestDto requestBody = populateInventoryAdjustmentRequest(
                inventoryAdjustmentDataList);
        final InventoryAdjustmentResponseDto responseBody = postRequest(requestBody);

        return isResponseSuccessful(responseBody);
    }


    private InventoryAdjustmentRequestDto populateInventoryAdjustmentRequest(
            final List<InventoryAdjustmentData> inventoryAdjustmentDataList) {
        final InventoryAdjustmentRequestDto requestBody = new InventoryAdjustmentRequestDto();
        requestBody.setSource(this.source);
        final Date date = new Date();
        requestBody.setTimestamp(date.toString());
        requestBody.setTrackingId("IA-" + date.getTime());
        requestBody.setUser(this.user);
        final List<InventoryAdjustmentDto> inventoryAdjustmentDtoList = new ArrayList<>();
        for (final InventoryAdjustmentData inventoryAdjustmentData : inventoryAdjustmentDataList) {
            final InventoryAdjustmentDto inventoryAdjustmentDto = new InventoryAdjustmentDto();
            inventoryAdjustmentDto.setAdjustmentQty(inventoryAdjustmentData.getAdjustmentQty());
            inventoryAdjustmentDto.setCode(inventoryAdjustmentData.getCode());
            inventoryAdjustmentDto.setCodeType(codeType);
            inventoryAdjustmentDto.setStoreNum(inventoryAdjustmentData.getStoreNum());
            inventoryAdjustmentDto.setReasonCode(this.reasonCode);
            inventoryAdjustmentDtoList.add(inventoryAdjustmentDto);
        }
        requestBody.setInventoryAdjustments(inventoryAdjustmentDtoList);
        return requestBody;
    }

    private boolean isResponseSuccessful(final InventoryAdjustmentResponseDto responseBody) {
        if (responseBody == null) {
            LOG.error(LOGGING_PREFIX + " ERROR - Response body is null");
            return false;
        }
        if (CollectionUtils.isNotEmpty(responseBody.getErrorList())) {
            for (final ErrorDetailsDto errorDetailsDto : responseBody.getErrorList()) {
                LOG.error(LOGGING_PREFIX + " ERRORS returned :" +
                        "errorCode=" + errorDetailsDto.getErrorCode()
                        + "errorMessage=" + errorDetailsDto.getErrorMessage());
            }

        }
        LOG.info(LOGGING_PREFIX + " Response: code=" + responseBody.getResponseCode()
                + " responseMessage=" + responseBody.getResponseMessage());
        //Success is checked after Errors - just to print out the errors incase there is any.
        if (StringUtils.equals(SUCCESS_RESPONSE_CODE, responseBody.getResponseCode())) {
            return true;
        }
        return false;
    }

    /**
     * Post the request to the webmethods url
     * 
     * @return StockVisibilityItemResponseDto
     */
    protected InventoryAdjustmentResponseDto postRequest(final InventoryAdjustmentRequestDto requestBody) {
        InventoryAdjustmentResponseDto response = new InventoryAdjustmentResponseDto();
        try {
            LOG.info(LOGGING_PREFIX + " Sending request=" + WMUtil.convertToJSONString(requestBody));
            final ResponseEntity<InventoryAdjustmentResponseDto> responseEntity = restTemplate.exchange(
                    getWebmethodsSecureBaseUrl() + getServiceEndpoint(), HttpMethod.POST,
                    new HttpEntity<>(requestBody, jsonHttpHeader), InventoryAdjustmentResponseDto.class);
            response = responseEntity.getBody();
            LOG.info(LOGGING_PREFIX + " Receiving response=" + WMUtil.convertToJSONString(response));
            return response;
        }
        catch (final RestClientException rce) {
            LOG.error(LOGGING_PREFIX + "FAILED Unable to get response from REST web service", rce);
        }
        catch (final HttpMessageNotReadableException nre) {
            LOG.error(LOGGING_PREFIX + "FAILED Unable to unmarshall response from REST web service", nre);
        }
        catch (final Exception ex) {
            LOG.error(LOGGING_PREFIX + "FAILED Unable to get response from REST web service", ex);
        }
        return null;
    }



    /**
     * Validates the service end point and rest template
     * 
     * @return boolean
     */
    private boolean validateConnection() {

        if (StringUtils.isBlank(getServiceEndpoint())) {
            LOG.error("Service endpoint is empty");
            return false;
        }
        return true;
    }

    /**
     * @return the restTemplate
     */
    @Override
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    /**
     * @param restTemplate
     *            the restTemplate to set
     */
    @Required
    public void setRestTemplate(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * @return the serviceEndpoint
     */
    @Override
    public String getServiceEndpoint() {
        return serviceEndpoint;
    }

    /**
     * @param serviceEndpoint
     *            the serviceEndpoint to set
     */
    @Required
    public void setServiceEndpoint(final String serviceEndpoint) {
        this.serviceEndpoint = serviceEndpoint;
    }

    /**
     * @param jsonHttpHeader
     *            the jsonHttpHeader to set
     */
    @Required
    public void setJsonHttpHeader(final HttpHeaders jsonHttpHeader) {
        this.jsonHttpHeader = jsonHttpHeader;
    }


    /**
     * @param source
     *            the source to set
     */
    @Required
    public void setSource(final String source) {
        this.source = source;
    }


    /**
     * @param user
     *            the user to set
     */
    @Required
    public void setUser(final String user) {
        this.user = user;
    }


    /**
     * @param reasonCode
     *            the reasonCode to set
     */
    @Required
    public void setReasonCode(final String reasonCode) {
        this.reasonCode = reasonCode;
    }


    /**
     * @param codeType
     *            the codeType to set
     */
    @Required
    public void setCodeType(final String codeType) {
        this.codeType = codeType;
    }



}
