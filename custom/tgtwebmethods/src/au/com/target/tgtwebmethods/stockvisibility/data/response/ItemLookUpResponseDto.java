package au.com.target.tgtwebmethods.stockvisibility.data.response;

import java.util.List;


public class ItemLookUpResponseDto implements java.io.Serializable
{

    private List<ItemStockDto> item;

    /**
     * @return the item
     */
    public List<ItemStockDto> getItem() {
        return item;
    }

    /**
     * @param item
     *            the item to set
     */
    public void setItem(final List<ItemStockDto> item) {
        this.item = item;
    }



}