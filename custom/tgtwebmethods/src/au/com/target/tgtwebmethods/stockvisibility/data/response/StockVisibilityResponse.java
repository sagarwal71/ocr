package au.com.target.tgtwebmethods.stockvisibility.data.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonRootName;

import au.com.target.tgtcore.integration.dto.TargetIntegrationErrorDto;


@JsonRootName(value = "response")
public class StockVisibilityResponse implements java.io.Serializable {
    private String responseCode;

    private String responseMessage;

    private List<TargetIntegrationErrorDto> errorList;

    private PayloadDto payload;

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode
     *            the responseCode to set
     */
    public void setResponseCode(final String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage
     *            the responseMessage to set
     */
    public void setResponseMessage(final String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the errorList
     */
    public List<TargetIntegrationErrorDto> getErrorList() {
        return errorList;
    }

    /**
     * @param errorList
     *            the errorList to set
     */
    public void setErrorList(final List<TargetIntegrationErrorDto> errorList) {
        this.errorList = errorList;
    }

    /**
     * @return the payload
     */
    public PayloadDto getPayload() {
        return payload;
    }

    /**
     * @param payload
     *            the payload to set
     */
    public void setPayload(final PayloadDto payload) {
        this.payload = payload;
    }
}