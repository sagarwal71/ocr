package au.com.target.tgtwebmethods.stockvisibility.data.request;

public class StoreDto implements java.io.Serializable
{

    private String number;

    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number
     *            the number to set
     */
    public void setNumber(final String number) {
        this.number = number;
    }



}