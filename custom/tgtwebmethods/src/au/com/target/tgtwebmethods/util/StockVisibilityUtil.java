/**
 * 
 */
package au.com.target.tgtwebmethods.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemResponseDto;
import au.com.target.tgtwebmethods.stockvisibility.data.request.ItemDto;
import au.com.target.tgtwebmethods.stockvisibility.data.request.StockVisibilityRequest;
import au.com.target.tgtwebmethods.stockvisibility.data.request.ItemLookUpRequestDto;
import au.com.target.tgtwebmethods.stockvisibility.data.request.StoreDto;
import au.com.target.tgtwebmethods.stockvisibility.data.response.ItemLookUpResponseDto;
import au.com.target.tgtwebmethods.stockvisibility.data.response.ItemStockDto;
import au.com.target.tgtwebmethods.stockvisibility.data.response.PayloadDto;
import au.com.target.tgtwebmethods.stockvisibility.data.response.StockVisibilityResponse;


/**
 * @author mjanarth
 *
 */
public final class StockVisibilityUtil {

    private static final String ITEMLOOKUP_CODE = "Itemcode";

    private StockVisibilityUtil() {
        // 
    }

    /**
     * Populate the stockvisibility request
     * 
     * @param stores
     * @param itemCodes
     * @return StockVisibilityRequestDto
     */
    public static StockVisibilityRequest populateStockVisibilityRequest(final List<String> stores,
            final List<String> itemCodes) {
        if (CollectionUtils.isEmpty(stores) || CollectionUtils.isEmpty(itemCodes)) {
            return null;
        }
        final List<ItemDto> items = new ArrayList<>();
        final ItemLookUpRequestDto stockVisibilityRequestDto = new ItemLookUpRequestDto();
        final List<StoreDto> storeList = new ArrayList<>();
        for (final String item : itemCodes) {
            final ItemDto itemdto = new ItemDto();
            itemdto.setCode(item);
            itemdto.setType(ITEMLOOKUP_CODE);
            items.add(itemdto);
        }
        for (final String store : stores) {
            final StoreDto storedto = new StoreDto();
            storedto.setNumber(store);
            storeList.add(storedto);
        }
        stockVisibilityRequestDto.setItems(items);
        stockVisibilityRequestDto.setStores(storeList);
        final StockVisibilityRequest request = new StockVisibilityRequest();
        request.setItemLookup(stockVisibilityRequestDto);
        return request;

    }

    /**
     * Poulates the reponse from the webmethod response
     * 
     * @param response
     * @return StockVisibilityItemResponseDto
     */
    public static StockVisibilityItemLookupResponseDto populateStockVisibilityResponse(
            final StockVisibilityResponse response) {
        if (null == response) {
            return null;
        }
        final StockVisibilityItemLookupResponseDto itemResponse = new StockVisibilityItemLookupResponseDto();
        itemResponse.setResponseCode(response.getResponseCode());
        itemResponse.setResponseMessage(response.getResponseMessage());
        final PayloadDto responsePayload = response.getPayload();
        if (null == responsePayload || null == responsePayload.getItemLookupResponse()) {
            return itemResponse;
        }
        final ItemLookUpResponseDto itemLookUpResponse = responsePayload.getItemLookupResponse();
        final List<StockVisibilityItemResponseDto> stockList = new ArrayList<>();
        if (null != itemLookUpResponse && CollectionUtils.isNotEmpty(itemLookUpResponse.getItem())) {
            for (final ItemStockDto itemStockDto : itemLookUpResponse.getItem()) {
                final StockVisibilityItemResponseDto stockVisibilityItemResponseDto = new StockVisibilityItemResponseDto();
                stockVisibilityItemResponseDto.setCode(itemStockDto.getCode());
                stockVisibilityItemResponseDto.setSoh(itemStockDto.getSoh());
                stockVisibilityItemResponseDto.setStoreNumber(itemStockDto.getStoreNumber());
                stockList.add(stockVisibilityItemResponseDto);
            }
            itemResponse.setItems(stockList);
        }
        return itemResponse;

    }
}
