/**
 * 
 */
package au.com.target.tgtwebmethods.util;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Utility class for web methods.
 * 
 * @author jjayawa1
 *
 */
public final class WMUtil {
    private static final Logger LOG = Logger.getLogger(WMUtil.class);

    /**
     * Constructor for WMUtil
     */
    private WMUtil() {
        // Empty Constructor
    }

    /**
     * Converts a request object to a JSON String.
     * 
     * @param requestObj
     * @return String
     */
    public static String convertToJSONString(final Object requestObj) {
        String jsonString = null;
        try {
            final ObjectMapper mapper = new ObjectMapper();
            jsonString = mapper.writeValueAsString(requestObj);
        }
        catch (final IOException e) {
            LOG.error("Cannot convert object to JSON String ");
        }
        return jsonString;
    }
}
