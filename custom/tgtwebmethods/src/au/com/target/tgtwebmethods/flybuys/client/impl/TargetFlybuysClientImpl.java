/**
 * 
 */
package au.com.target.tgtwebmethods.flybuys.client.impl;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPFaultException;

import org.apache.commons.lang.time.FastDateFormat;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.sun.xml.internal.ws.client.BindingProviderProperties;
import com.sun.xml.internal.ws.client.ClientTransportException;

import au.com.target.services.services.flybuysrewards.v1.FlybuysAuthenticationRequest;
import au.com.target.services.services.flybuysrewards.v1.FlybuysAuthenticationResponse;
import au.com.target.services.services.flybuysrewards.v1.FlybuysConsumeRequest;
import au.com.target.services.services.flybuysrewards.v1.FlybuysConsumeResponse;
import au.com.target.services.services.flybuysrewards.v1.FlybuysRedemptionTier;
import au.com.target.services.services.flybuysrewards.v1.FlybuysRefundRequest;
import au.com.target.services.services.flybuysrewards.v1.FlybuysRefundResponse;
import au.com.target.services.services.flybuysrewards.v1.FlybuysRewardsServicePortType;
import au.com.target.services.services.flybuysrewards.v1.ObjectFactory;
import au.com.target.services.services.flybuysrewards.v1.TGTINFlybuysRewardsServicesEnterpriseWsProviderV1FlybuysRewardsService;
import au.com.target.tgtcore.flybuys.client.FlybuysClient;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysAuthenticateResponseDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysConsumeResponseDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysRedeemTierDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysRefundResponseDto;
import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;
import au.com.target.tgtwebmethods.flybuys.client.util.FlybuysErrorCodeProcessor;



/**
 * @author rmcalave
 * 
 */
public class TargetFlybuysClientImpl implements FlybuysClient {

    private static final Logger LOG = Logger.getLogger(TargetFlybuysClientImpl.class);
    private static final FastDateFormat FLYBUYS_DOB_FORMATTER = FastDateFormat.getInstance("yyyyMMdd");

    private static final String ERROR_TEMPLATE_FLYBUYS_AUTHENTICATE = "[flybuys Authenticate] [Error message: {0}]";
    private static final String ERROR_TEMPLATE_FLYBUYS_AUTHENTICATE_WITH_CODE = "[flybuys Authenticate] [Error code: {0}] [Error message: {1}]";

    private static final String ERROR_TEMPLATE_FLYBUYS_CONSUME = "[flybuys Consume] [Error message: {0}]";
    private static final String ERROR_TEMPLATE_FLYBUYS_CONSUME_WITH_CODE = "[flybuys Consume] [Error code: {0}] [Error message: {1}]";

    private static final String ERROR_TEMPLATE_FLYBUYS_REFUND = "[flybuys Refund] [Error message: {0}]";
    private static final String ERROR_TEMPLATE_FLYBUYS_REFUND_WITH_CODE = "[flybuys Refund] [Error code: {0}] [Error message: {1}]";

    private static final String ERROR_FLYBUYS_SERVICE_NOT_INITIALISED = "flybuysRewardsServicePort was not initialised at system startup, flybuys redemption is currently unavailable.";

    private TGTINFlybuysRewardsServicesEnterpriseWsProviderV1FlybuysRewardsService flybuysRewardsServiceProvider;
    private UsernamePasswordCredentials wmCredentials;
    private ObjectFactory objectFactory;

    private FlybuysErrorCodeProcessor flybuysErrorCodeProcessor;

    private Integer connectTimeoutMilliseconds;
    private Integer requestTimeoutMilliseconds;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.client.FlybuysClient#flybuysAuthenticate(java.lang.String, java.util.Date, java.lang.String)
     */
    @Override
    public FlybuysAuthenticateResponseDto flybuysAuthenticate(final String flybuysNumber, final Date dob,
            final String postcode) {
        final FlybuysRewardsServicePortType flybuysRewardsServicePort = getFlybuysRewardsServicePort();
        if (flybuysRewardsServicePort == null) {
            // If it's null then it failed to initialise at system startup, meaning that the web service is unavailable.
            final FlybuysAuthenticateResponseDto targetResponse = new FlybuysAuthenticateResponseDto();
            targetResponse.setResponse(FlybuysResponseType.UNAVAILABLE);
            targetResponse
                    .setErrorMessage(ERROR_FLYBUYS_SERVICE_NOT_INITIALISED);

            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat
                            .format(ERROR_TEMPLATE_FLYBUYS_AUTHENTICATE,
                                    targetResponse.getErrorMessage()),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS));

            return targetResponse;
        }

        final FlybuysAuthenticationRequest request = createFlybuysAuthenticationRequest(flybuysNumber, dob, postcode);

        FlybuysAuthenticationResponse response = null;
        FlybuysAuthenticateResponseDto targetResponse = null;
        try {
            response = flybuysRewardsServicePort.authenticateFlybuysCustomer(request);
        }
        catch (final SOAPFaultException ex) {
            targetResponse = new FlybuysAuthenticateResponseDto();
            targetResponse.setResponse(FlybuysResponseType.UNAVAILABLE);
            targetResponse.setErrorMessage(ex.getMessage());
        }
        catch (final ClientTransportException ex) {
            targetResponse = new FlybuysAuthenticateResponseDto();
            targetResponse.setResponse(FlybuysResponseType.UNAVAILABLE);
            targetResponse.setErrorMessage(ex.getMessage());
        }

        if (targetResponse != null) {
            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat
                            .format(ERROR_TEMPLATE_FLYBUYS_AUTHENTICATE,
                                    targetResponse.getErrorMessage()),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS));

            return targetResponse;
        }

        targetResponse = createFlybuysAuthenticateResponseDto(response);

        if (!FlybuysResponseType.SUCCESS.equals(targetResponse.getResponse())) {
            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat.format(ERROR_TEMPLATE_FLYBUYS_AUTHENTICATE_WITH_CODE,
                            response.getResponseCode(), response.getResponseMessage().getValue()),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS));


        }

        return targetResponse;
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.client.FlybuysClient#consumeFlybuysPoints(java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.math.BigDecimal, java.lang.String, java.lang.String)
     */
    @Override
    public FlybuysConsumeResponseDto consumeFlybuysPoints(final String flybuysCardNumber, final String securityToken,
            final String storeNumber, final Integer points, final BigDecimal dollars, final String redeemCode,
            final String transactionId) {

        FlybuysConsumeResponseDto targetResponse = null;

        final FlybuysRewardsServicePortType flybuysRewardsServicePort = getFlybuysRewardsServicePort();
        if (flybuysRewardsServicePort == null) {
            // If it's null then it failed to initialise at system startup, meaning that the web service is unavailable.
            targetResponse = new FlybuysConsumeResponseDto();
            targetResponse.setResponse(FlybuysResponseType.UNAVAILABLE);

            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat
                            .format(ERROR_TEMPLATE_FLYBUYS_CONSUME, ERROR_FLYBUYS_SERVICE_NOT_INITIALISED),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS));

            return targetResponse;
        }

        final FlybuysConsumeRequest request = createFlybuysConsumeRequest(flybuysCardNumber, dollars, points,
                redeemCode,
                securityToken, storeNumber, transactionId);

        FlybuysConsumeResponse response = null;
        try {
            response = flybuysRewardsServicePort.consumeFlybuysPoints(request);
        }
        catch (final SOAPFaultException ex) {
            targetResponse = new FlybuysConsumeResponseDto();
            targetResponse.setResponse(FlybuysResponseType.UNAVAILABLE);
            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat
                            .format(ERROR_TEMPLATE_FLYBUYS_CONSUME,
                                    ex.getMessage()),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS));
        }
        catch (final ClientTransportException ex) {
            targetResponse = new FlybuysConsumeResponseDto();
            targetResponse.setResponse(FlybuysResponseType.UNAVAILABLE);
            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat
                            .format(ERROR_TEMPLATE_FLYBUYS_CONSUME,
                                    ex.getMessage()),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS));
        }

        if (targetResponse != null) {
            return targetResponse;
        }

        targetResponse = createFlybuysConsumeResponseDto(response);

        if (!FlybuysResponseType.SUCCESS.equals(targetResponse.getResponse())) {
            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat.format(ERROR_TEMPLATE_FLYBUYS_CONSUME_WITH_CODE,
                            response.getResponseCode(), response.getResponseMessage().getValue()),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS));
        }

        return targetResponse;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.client.FlybuysClient#flybuysRefundPoints(java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.math.BigDecimal, java.util.Date, java.lang.String)
     */
    @Override
    public FlybuysRefundResponseDto refundFlybuysPoints(final String flybuysCardNumber, final String securityToken,
            final String storeNumber,
            final Integer points,
            final BigDecimal dollars, final String timeStamp, final String transactionId) {

        FlybuysRefundResponse fbRefundResponse = null;
        FlybuysRefundResponseDto targetResponseDto = null;

        final FlybuysRewardsServicePortType flybuysRewardsServicePort = getFlybuysRewardsServicePort();
        if (flybuysRewardsServicePort == null) {
            // If it's null then it failed to initialise at system startup, meaning that the web service is unavailable.
            targetResponseDto = new FlybuysRefundResponseDto();
            targetResponseDto.setResponse(FlybuysResponseType.UNAVAILABLE);
            targetResponseDto
                    .setResponseMessage(ERROR_FLYBUYS_SERVICE_NOT_INITIALISED);

            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat
                            .format(ERROR_TEMPLATE_FLYBUYS_REFUND, ERROR_FLYBUYS_SERVICE_NOT_INITIALISED),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS));

            return targetResponseDto;
        }


        final FlybuysRefundRequest request = createFlybuysRefundRequest(flybuysCardNumber, securityToken,
                storeNumber, points, dollars, timeStamp, transactionId);


        try {
            fbRefundResponse = flybuysRewardsServicePort.refundFlybuysPoints(request);
        }
        catch (final SOAPFaultException ex) {
            targetResponseDto = new FlybuysRefundResponseDto();
            targetResponseDto.setResponse(FlybuysResponseType.UNAVAILABLE);
            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat
                            .format(ERROR_TEMPLATE_FLYBUYS_REFUND,
                                    ex.getMessage()),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS));
        }
        catch (final ClientTransportException ex) {
            targetResponseDto = new FlybuysRefundResponseDto();
            targetResponseDto.setResponse(FlybuysResponseType.UNAVAILABLE);
            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat
                            .format(ERROR_TEMPLATE_FLYBUYS_REFUND,
                                    ex.getMessage()),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS));
        }

        if (targetResponseDto != null) {
            return targetResponseDto;
        }

        targetResponseDto = createFlybuysRefundResponseDto(fbRefundResponse);

        if (!FlybuysResponseType.SUCCESS.equals(targetResponseDto.getResponse())) {
            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat.format(ERROR_TEMPLATE_FLYBUYS_REFUND_WITH_CODE,
                            fbRefundResponse.getResponseCode(), fbRefundResponse.getResponseMessage().getValue()),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS));
        }

        return targetResponseDto;



    }


    private FlybuysRewardsServicePortType getFlybuysRewardsServicePort() {
        if (flybuysRewardsServiceProvider == null) {
            return null;
        }

        final FlybuysRewardsServicePortType flybuysRewardsServicePort = flybuysRewardsServiceProvider
                .getTGTINFlybuysRewardsServicesEnterpriseWsProviderV1FlybuysRewardsServicePort();
        final BindingProvider bp = (BindingProvider)flybuysRewardsServicePort;
        final Map<String, Object> requestContext = bp.getRequestContext();

        requestContext.put(BindingProvider.USERNAME_PROPERTY, wmCredentials.getUserName());
        requestContext.put(BindingProvider.PASSWORD_PROPERTY, wmCredentials.getPassword());

        requestContext.put(BindingProviderProperties.CONNECT_TIMEOUT, connectTimeoutMilliseconds);
        requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, requestTimeoutMilliseconds);

        return flybuysRewardsServicePort;
    }

    private FlybuysAuthenticationRequest createFlybuysAuthenticationRequest(final String flybuysNumber, final Date dob,
            final String postcode) {
        final FlybuysAuthenticationRequest request = objectFactory.createFlybuysAuthenticationRequest();

        request.setCard(flybuysNumber);
        request.setDob(new JAXBElement<String>(new QName("dob"), String.class, FLYBUYS_DOB_FORMATTER.format(dob)));
        request.setPostcode(postcode);

        return request;
    }

    private FlybuysAuthenticateResponseDto createFlybuysAuthenticateResponseDto(
            final FlybuysAuthenticationResponse flybuysResponse) {
        final FlybuysAuthenticateResponseDto targetResponse = new FlybuysAuthenticateResponseDto();

        if (flybuysResponse == null) {
            targetResponse.setResponse(FlybuysResponseType.UNAVAILABLE);
            return targetResponse;
        }

        final FlybuysResponseType responseType = flybuysErrorCodeProcessor
                .getFlybuysResponseTypeForCode(flybuysResponse.getResponseCode());
        targetResponse.setResponse(responseType);

        if (!FlybuysResponseType.SUCCESS.equals(responseType)) {
            targetResponse.setErrorMessage(flybuysResponse.getResponseMessage().getValue());
            return targetResponse;
        }

        targetResponse.setAvailPoints(flybuysResponse.getAvailPoints().getValue());
        targetResponse.setSecurityToken(flybuysResponse.getSecurityToken().getValue());

        final List<FlybuysRedeemTierDto> redeemTiers = new ArrayList<>();
        if (flybuysResponse.getRedeemTiers() != null && flybuysResponse.getRedeemTiers().getValue() != null) {
            for (final FlybuysRedemptionTier tier : flybuysResponse.getRedeemTiers().getValue().getTier()) {
                final FlybuysRedeemTierDto redeemTierDto = new FlybuysRedeemTierDto();

                redeemTierDto.setDollarAmt(new BigDecimal(tier.getDollars()));
                redeemTierDto.setPoints(Integer.parseInt(tier.getPoints()));
                redeemTierDto.setRedeemCode(tier.getRedeemCode());

                redeemTiers.add(redeemTierDto);
            }
        }
        targetResponse.setRedeemTiers(redeemTiers);

        return targetResponse;
    }

    private FlybuysConsumeRequest createFlybuysConsumeRequest(final String flybuysNumber, final BigDecimal dollars,
            final Integer points, final String redeemCode, final String securityToken, final String storeNumber,
            final String transactionId) {
        final FlybuysConsumeRequest request = objectFactory.createFlybuysConsumeRequest();
        request.setCard(flybuysNumber);
        request.setDollars(dollars.toString());
        request.setPoints(points.toString());
        request.setRedeemCode(redeemCode);
        request.setSecurityToken(securityToken);
        request.setStoreNumber(storeNumber);
        request.setTransid(transactionId);

        return request;
    }

    private FlybuysConsumeResponseDto createFlybuysConsumeResponseDto(final FlybuysConsumeResponse flybuysResponse) {
        final FlybuysConsumeResponseDto targetResponse = new FlybuysConsumeResponseDto();

        if (flybuysResponse == null) {
            targetResponse.setResponse(FlybuysResponseType.UNAVAILABLE);
            return targetResponse;
        }
        final FlybuysResponseType responseType = flybuysErrorCodeProcessor
                .getFlybuysResponseTypeForCode(flybuysResponse.getResponseCode());
        targetResponse.setResponse(responseType);

        targetResponse.setResponseCode(Integer.valueOf(flybuysResponse.getResponseCode()));

        if (!FlybuysResponseType.SUCCESS.equals(responseType)) {
            return targetResponse;
        }

        if (flybuysResponse.getConfirmationCode() != null) {
            targetResponse.setConfimationCode(flybuysResponse.getConfirmationCode().getValue());
        }
        return targetResponse;
    }

    /**
     * @param flybuysCardNumber
     * @param securityToken
     * @param storeNumber
     * @param points
     * @param dollars
     * @param timeStamp
     * @param transactionId
     * @return FlybuysRefundRequest
     */
    private FlybuysRefundRequest createFlybuysRefundRequest(final String flybuysCardNumber, final String securityToken,
            final String storeNumber, final Integer points, final BigDecimal dollars, final String timeStamp,
            final String transactionId) {
        final FlybuysRefundRequest refundRequest = objectFactory.createFlybuysRefundRequest();

        refundRequest.setCard(flybuysCardNumber);
        refundRequest.setSecurityToken(securityToken);
        refundRequest.setStoreNumber(storeNumber);
        refundRequest.setPoints(points.toString());
        refundRequest.setDollars(dollars.toString());
        refundRequest.setTimestamp(timeStamp);
        refundRequest.setTransid(transactionId);

        return refundRequest;

    }

    private FlybuysRefundResponseDto createFlybuysRefundResponseDto(final FlybuysRefundResponse flybuysRefundResponse) {
        final FlybuysRefundResponseDto targetResponse = new FlybuysRefundResponseDto();

        if (flybuysRefundResponse == null) {
            targetResponse.setResponse(FlybuysResponseType.UNAVAILABLE);
            return targetResponse;
        }

        final FlybuysResponseType responseType = flybuysErrorCodeProcessor
                .getFlybuysResponseTypeForCode(flybuysRefundResponse.getResponseCode());
        targetResponse.setResponse(responseType);

        targetResponse.setResponseCode(Integer.valueOf(flybuysRefundResponse.getResponseCode()));

        if (!FlybuysResponseType.SUCCESS.equals(responseType)) {
            return targetResponse;
        }

        if (flybuysRefundResponse.getConfirmationCode() != null) {
            targetResponse.setConfimationCode(flybuysRefundResponse.getConfirmationCode().getValue());
        }
        return targetResponse;
    }

    /**
     * @param flybuysRewardsServiceProvider
     *            the flybuysRewardsServiceProvider to set
     */
    @Required
    public void setFlybuysRewardsServiceProvider(
            final TGTINFlybuysRewardsServicesEnterpriseWsProviderV1FlybuysRewardsService flybuysRewardsServiceProvider) {
        this.flybuysRewardsServiceProvider = flybuysRewardsServiceProvider;
    }

    /**
     * @param wmCredentials
     *            the wmCredentials to set
     */
    @Required
    public void setWmCredentials(final UsernamePasswordCredentials wmCredentials) {
        this.wmCredentials = wmCredentials;
    }

    /**
     * @param objectFactory
     *            the objectFactory to set
     */
    @Required
    public void setObjectFactory(final ObjectFactory objectFactory) {
        this.objectFactory = objectFactory;
    }

    /**
     * @param flybuysErrorCodeProcessor
     *            the flybuysErrorCodeProcessor to set
     */
    @Required
    public void setFlybuysErrorCodeProcessor(final FlybuysErrorCodeProcessor flybuysErrorCodeProcessor) {
        this.flybuysErrorCodeProcessor = flybuysErrorCodeProcessor;
    }

    /**
     * @param connectTimeoutMilliseconds
     *            the connectTimeoutMilliseconds to set
     */
    @Required
    public void setConnectTimeoutMilliseconds(final Integer connectTimeoutMilliseconds) {
        this.connectTimeoutMilliseconds = connectTimeoutMilliseconds;
    }

    /**
     * @param requestTimeoutMilliseconds
     *            the requestTimeoutMilliseconds to set
     */
    @Required
    public void setRequestTimeoutMilliseconds(final Integer requestTimeoutMilliseconds) {
        this.requestTimeoutMilliseconds = requestTimeoutMilliseconds;
    }
}
