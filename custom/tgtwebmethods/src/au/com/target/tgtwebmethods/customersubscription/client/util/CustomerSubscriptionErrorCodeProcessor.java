/**
 * 
 */
package au.com.target.tgtwebmethods.customersubscription.client.util;

import org.apache.commons.lang.ArrayUtils;

import au.com.target.tgtmail.enums.TargetCustomerSubscriptionResponseType;


/**
 * @author mjanarth
 * 
 */
public class CustomerSubscriptionErrorCodeProcessor {


    private static final String[] SUCCESS_CODES = { "0", "-5" };
    private static final String[] UNAVAILABLE_CODES = { "-2" };
    private static final String ALREADY_EXISTS_CODE = "-4";
    private static final String[] INVALID_CODES = { "-1" };
    private static final String[] OTHER_CODES = { "-3", "2" };


    /**
     * Get the response type for corresponding error code
     * 
     * @param responseCode
     * @return TargetCustomerSubscriptionResponseType
     */
    public TargetCustomerSubscriptionResponseType getCustomerSubscriptionResponseTypeForCode(final String responseCode) {
        if (ArrayUtils.contains(SUCCESS_CODES, responseCode)) {
            return TargetCustomerSubscriptionResponseType.SUCCESS;
        }
        if (ALREADY_EXISTS_CODE.equals(responseCode)) {
            return TargetCustomerSubscriptionResponseType.ALREADY_EXISTS;
        }
        if (ArrayUtils.contains(UNAVAILABLE_CODES, responseCode)) {
            return TargetCustomerSubscriptionResponseType.UNAVAILABLE;
        }

        if (ArrayUtils.contains(INVALID_CODES, responseCode)) {
            return TargetCustomerSubscriptionResponseType.INVALID;
        }
        if (ArrayUtils.contains(OTHER_CODES, responseCode)) {
            return TargetCustomerSubscriptionResponseType.OTHERS;
        }
        return TargetCustomerSubscriptionResponseType.OTHERS;

    }

}
