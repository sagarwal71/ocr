/**
 * 
 */
package au.com.target.tgtwebmethods.customersubscription.client.impl;



import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPFaultException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.sun.xml.internal.ws.client.BindingProviderProperties;
import com.sun.xml.internal.ws.client.ClientTransportException;

import au.com.target.customermgmt.customersingleview.v1.customersingleviewservice.CustomerSingleViewServicePortType;
import au.com.target.customermgmt.customersingleview.v1.customersingleviewservice.ExceptionTGTENCanonicalCommonReturnResponse;
import au.com.target.customermgmt.customersingleview.v1.customersingleviewservice.ObjectFactory;
import au.com.target.customermgmt.customersingleview.v1.customersingleviewservice.ReturnResponse;
import au.com.target.customermgmt.customersingleview.v1.customersingleviewservice.SetCustomerRequest;
import au.com.target.customermgmt.customersingleview.v1.customersingleviewservice.SetCustomerResponse2;
import au.com.target.customermgmt.customersingleview.v1.customersingleviewservice.TGTCECustomerMgmtWSProviderV1CustomerSingleViewService;
import au.com.target.enterprise.canonicals.customermgmt.customersingleview.v1.ChildInfoType;
import au.com.target.enterprise.canonicals.customermgmt.customersingleview.v1.CustomerType;
import au.com.target.enterprise.canonicals.customermgmt.customersingleview.v1.SubscriptionType;
import au.com.target.tgtmail.client.TargetCustomerSubscriptionClient;
import au.com.target.tgtmail.dto.TargetCustomerChildDetailsDto;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionResponseDto;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionResponseType;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionType;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;
import au.com.target.tgtwebmethods.constants.TgtwebmethodsConstants;
import au.com.target.tgtwebmethods.customersubscription.client.util.CustomerSubscriptionErrorCodeProcessor;


/**
 * @author mjanarth
 * 
 */
public class TargetCustomerSubscriptionClientImpl implements TargetCustomerSubscriptionClient {


    private static final Logger LOG = Logger.getLogger(TargetCustomerSubscriptionClientImpl.class);

    private static final String ERROR_TEMPLATE_CUSTOMER_SUBSCRIPTION = "[Customer Subscription] [Error message: {0}]";
    private TGTCECustomerMgmtWSProviderV1CustomerSingleViewService customerSubscriptionServiceProvider;
    private UsernamePasswordCredentials wmCredentials;
    private CustomerSubscriptionErrorCodeProcessor customerSubscriptionErrorCodeProcessor;
    private ObjectFactory objectFactory;
    private Integer connectTimeoutMilliseconds;
    private Integer requestTimeoutMilliseconds;


    /* (non-Javadoc)
     * @see au.com.target.tgtmail.client.TargetCustomerSubscriptionClient#sendCustomerSubscription(au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto)
     */
    @Override
    public TargetCustomerSubscriptionResponseDto sendCustomerSubscription(
            final TargetCustomerSubscriptionRequestDto subscriptionDetails) {
        final CustomerSingleViewServicePortType customerSubscriptionServicePort = getCustomerSubscriptionServicePort();
        TargetCustomerSubscriptionResponseDto customerSubscriptionResponseDto = new TargetCustomerSubscriptionResponseDto();
        if (customerSubscriptionServicePort == null) {
            // If it's null then it failed to initialize at system startup, meaning that the web service is unavailable.
            customerSubscriptionResponseDto.setResponseType(TargetCustomerSubscriptionResponseType.UNAVAILABLE);
            customerSubscriptionResponseDto
                    .setResponseCode(TgtwebmethodsConstants.CommonErrorCodes.UNAVAILABLE_ERRORCODE);
            customerSubscriptionResponseDto
                    .setResponseMessage(
                            TgtwebmethodsConstants.CustomerSubsriptionErrorCodesAndMessages.ERROR_CUSTOMER_SUBSCRIPTION_SERVICE_NOT_INITIALISED);
            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat
                            .format(ERROR_TEMPLATE_CUSTOMER_SUBSCRIPTION,
                                    TgtwebmethodsConstants.CustomerSubsriptionErrorCodesAndMessages.ERROR_CUSTOMER_SUBSCRIPTION_SERVICE_NOT_INITIALISED),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS));

            return customerSubscriptionResponseDto;
        }
        if (null == subscriptionDetails || StringUtils.isEmpty(subscriptionDetails.getCustomerEmail())) {
            customerSubscriptionResponseDto.setResponseType(TargetCustomerSubscriptionResponseType.INVALID);
            customerSubscriptionResponseDto.setResponseCode(TgtwebmethodsConstants.CommonErrorCodes.INVALID_ERRORCODE);
            customerSubscriptionResponseDto
                    .setResponseMessage(
                            TgtwebmethodsConstants.CustomerSubsriptionErrorCodesAndMessages.ERROR_REQUEST_EMAILID);
            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat
                            .format(ERROR_TEMPLATE_CUSTOMER_SUBSCRIPTION,
                                    TgtwebmethodsConstants.CustomerSubsriptionErrorCodesAndMessages.ERROR_REQUEST_EMAILID),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS));

            return customerSubscriptionResponseDto;
        }
        try {
            SetCustomerResponse2 customerSubscriptionResponse = null;
            if (subscriptionDetails.isUpdateCustomerPersonalDetails()) {
                customerSubscriptionResponse = customerSubscriptionServicePort
                        .upsertCustomer(createCustomerSubscriptionRequest(subscriptionDetails));
            }
            else {
                customerSubscriptionResponse = customerSubscriptionServicePort
                        .setCustomer(createCustomerSubscriptionRequest(subscriptionDetails));
            }
            customerSubscriptionResponseDto = createCustomerSubscriptionResponse(customerSubscriptionResponse
                    .getReturnResponse());
        }
        catch (final SOAPFaultException ex) {
            customerSubscriptionResponseDto.setResponseType(TargetCustomerSubscriptionResponseType.UNAVAILABLE);
            customerSubscriptionResponseDto
                    .setResponseCode(TgtwebmethodsConstants.CommonErrorCodes.UNAVAILABLE_ERRORCODE);
            customerSubscriptionResponseDto.setResponseMessage(ex.getMessage());
            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat
                            .format(ERROR_TEMPLATE_CUSTOMER_SUBSCRIPTION,
                                    ex.getMessage()),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS));
            return customerSubscriptionResponseDto;
        }
        catch (final ClientTransportException ex) {
            customerSubscriptionResponseDto.setResponseType(TargetCustomerSubscriptionResponseType.UNAVAILABLE);
            customerSubscriptionResponseDto
                    .setResponseCode(TgtwebmethodsConstants.CommonErrorCodes.UNAVAILABLE_ERRORCODE);
            customerSubscriptionResponseDto.setResponseMessage(ex.getMessage());
            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat
                            .format(ERROR_TEMPLATE_CUSTOMER_SUBSCRIPTION,
                                    ex.getMessage()),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS));
            return customerSubscriptionResponseDto;
        }
        catch (final ExceptionTGTENCanonicalCommonReturnResponse ex) {
            if (null != ex.getFaultInfo()) {
                customerSubscriptionResponseDto = createCustomerSubscriptionResponse(ex.getFaultInfo());
                return customerSubscriptionResponseDto;
            }
            customerSubscriptionResponseDto.setResponseType(TargetCustomerSubscriptionResponseType.UNAVAILABLE);
            customerSubscriptionResponseDto
                    .setResponseCode(TgtwebmethodsConstants.CommonErrorCodes.UNAVAILABLE_ERRORCODE);
            customerSubscriptionResponseDto.setResponseMessage(ex.getMessage());
            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat
                            .format(ERROR_TEMPLATE_CUSTOMER_SUBSCRIPTION,
                                    ex.getMessage()),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS));
            return customerSubscriptionResponseDto;

        }
        catch (final DatatypeConfigurationException ex) {
            customerSubscriptionResponseDto.setResponseType(TargetCustomerSubscriptionResponseType.INVALID);
            customerSubscriptionResponseDto
                    .setResponseCode(TgtwebmethodsConstants.CommonErrorCodes.INVALID_ERRORCODE);
            customerSubscriptionResponseDto.setResponseMessage(ex.getMessage());
            LOG.error(SplunkLogFormatter.formatMessage(
                    MessageFormat
                            .format(ERROR_TEMPLATE_CUSTOMER_SUBSCRIPTION,
                                    ex.getMessage()),
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS));
            return customerSubscriptionResponseDto;
        }
        return customerSubscriptionResponseDto;

    }

    private CustomerSingleViewServicePortType getCustomerSubscriptionServicePort() {
        if (customerSubscriptionServiceProvider == null) {
            return null;
        }
        if (LOG.isTraceEnabled()) {
            System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
            System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
            System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
            System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
        }
        final CustomerSingleViewServicePortType customerSubscriptionServicePort = customerSubscriptionServiceProvider
                .getTGTCECustomerMgmtWSProviderV1CustomerSingleViewServicePort();

        final BindingProvider bp = (BindingProvider)customerSubscriptionServicePort;
        final Map<String, Object> requestContext = bp.getRequestContext();

        requestContext.put(BindingProvider.USERNAME_PROPERTY, wmCredentials.getUserName());
        requestContext.put(BindingProvider.PASSWORD_PROPERTY, wmCredentials.getPassword());

        requestContext.put(BindingProviderProperties.CONNECT_TIMEOUT, connectTimeoutMilliseconds);
        requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, requestTimeoutMilliseconds);
        return customerSubscriptionServicePort;

    }

    protected SetCustomerRequest createCustomerSubscriptionRequest(final TargetCustomerSubscriptionRequestDto request)
            throws DatatypeConfigurationException {
        final SetCustomerRequest customerRequest = objectFactory.createSetCustomerRequest();
        final CustomerType customerDetails = new CustomerType();
        customerDetails.setEmail(request.getCustomerEmail());
        customerDetails.setFirstName(request.getFirstName());
        customerDetails.setTitle(request.getTitle());
        if (null != request.getCustomerSubscriptionSource()) {
            customerDetails.setSubscriptionSource(request.getCustomerSubscriptionSource().getSource());
        }
        if (null != request.getSubscriptionType()) {
            customerDetails.setSubscription(SubscriptionType.fromValue(request.getSubscriptionType().getValue()));
        }
        if (StringUtils.isNotEmpty(request.getLastName())) {
            customerDetails.setLastName(request.getLastName());
        }
        if (request.isUpdateCustomerPersonalDetails()) {
            populateCustomerPersonalDetails(customerDetails, request);
        }
        if (request.isRegisterForEmail()
                && TargetCustomerSubscriptionType.NEWSLETTER.equals(request.getSubscriptionType())) {
            customerDetails.setEnewsoptin(true);
            customerDetails.setSmsoptin(true);
            customerDetails.setShoppingbasketoptin(true);
        }
        else {
            customerDetails.setEnewsoptin(false);
            customerDetails.setSmsoptin(false);
            customerDetails.setShoppingbasketoptin(false);
        }
        customerRequest.setCustomer(customerDetails);

        return customerRequest;

    }

    private void populateCustomerPersonalDetails(final CustomerType customerType,
            final TargetCustomerSubscriptionRequestDto requestDto) throws DatatypeConfigurationException {
        final List<ChildInfoType> childrenInfoType = new ArrayList<>();
        XMLGregorianCalendar xmlCalendar = null;
        customerType.setBabyGender(requestDto.getBabyGender());
        customerType.setGender(requestDto.getGender());
        if (null != requestDto.getBirthday()) {
            xmlCalendar = convertDateToXMLCalendarWithoutTimeZone(requestDto.getBirthday());
            customerType.setBirthday(xmlCalendar);
        }
        if (null != requestDto.getDueDate()) {
            xmlCalendar = convertDateToXMLCalendarWithoutTimeZone(requestDto.getDueDate());
            customerType.setPregnancyDueDate(xmlCalendar);
        }
        if (CollectionUtils.isNotEmpty(requestDto.getChildrenDetails())) {
            for (final TargetCustomerChildDetailsDto childDetails : requestDto.getChildrenDetails()) {
                final ChildInfoType childInfo = new ChildInfoType();
                childInfo.setChildGender(childDetails.getGender());
                childInfo.setChildName(childDetails.getFirstName());
                if (null != childDetails.getBirthday()) {
                    xmlCalendar = convertDateToXMLCalendarWithoutTimeZone(childDetails.getBirthday());
                    childInfo.setChildDob(xmlCalendar);
                }
                childrenInfoType.add(childInfo);
            }
            if (null != customerType.getChildrenInfo()) {
                customerType.getChildrenInfo().addAll(childrenInfoType);
            }

        }
    }

    /**
     * Converts Date to XMLGregorianCalendar
     * 
     * @param date
     * @return XMLGregorianCalendar
     * @throws DatatypeConfigurationException
     */
    protected XMLGregorianCalendar convertDateToXMLCalendarWithoutTimeZone(final Date date)
            throws DatatypeConfigurationException {
        if (null != date) {
            final DatatypeFactory factory = DatatypeFactory.newInstance();
            final GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            final XMLGregorianCalendar xmlGregorianCalendar = factory.newXMLGregorianCalendar(calendar);
            xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
            return xmlGregorianCalendar;
        }
        return null;
    }

    private TargetCustomerSubscriptionResponseDto createCustomerSubscriptionResponse(final ReturnResponse response) {
        final TargetCustomerSubscriptionResponseDto targetCustomerSubscriptionReponse = new TargetCustomerSubscriptionResponseDto();
        if (null == response) {
            targetCustomerSubscriptionReponse.setResponseType(TargetCustomerSubscriptionResponseType.UNAVAILABLE);
            targetCustomerSubscriptionReponse
                    .setResponseCode(TgtwebmethodsConstants.CommonErrorCodes.UNAVAILABLE_ERRORCODE);
            return targetCustomerSubscriptionReponse;
        }
        if (null != response.getReturnMessage()) {
            targetCustomerSubscriptionReponse.setResponseMessage(response.getReturnMessage().getValue());
        }
        if (null != response.getReturnCode()) {
            targetCustomerSubscriptionReponse.setResponseCode(Integer.valueOf(response.getReturnCode().getValue()));
            final TargetCustomerSubscriptionResponseType responseType = customerSubscriptionErrorCodeProcessor
                    .getCustomerSubscriptionResponseTypeForCode(response.getReturnCode().getValue());
            targetCustomerSubscriptionReponse.setResponseType(responseType);
        }
        return targetCustomerSubscriptionReponse;

    }

    /**
     * @param customerSubscriptionServiceProvider
     *            the customerSubscriptionServiceProvider to set
     */
    @Required
    public void setCustomerSubscriptionServiceProvider(
            final TGTCECustomerMgmtWSProviderV1CustomerSingleViewService customerSubscriptionServiceProvider) {
        this.customerSubscriptionServiceProvider = customerSubscriptionServiceProvider;
    }


    /**
     * @param wmCredentials
     *            the wmCredentials to set
     */
    @Required
    public void setWmCredentials(final UsernamePasswordCredentials wmCredentials) {
        this.wmCredentials = wmCredentials;
    }


    /**
     * @param connectTimeoutMilliseconds
     *            the connectTimeoutMilliseconds to set
     */
    @Required
    public void setConnectTimeoutMilliseconds(final Integer connectTimeoutMilliseconds) {
        this.connectTimeoutMilliseconds = connectTimeoutMilliseconds;
    }


    /**
     * @param requestTimeoutMilliseconds
     *            the requestTimeoutMilliseconds to set
     */
    @Required
    public void setRequestTimeoutMilliseconds(final Integer requestTimeoutMilliseconds) {
        this.requestTimeoutMilliseconds = requestTimeoutMilliseconds;
    }

    /**
     * @param objectFactory
     *            the objectFactory to set
     */
    @Required
    public void setObjectFactory(final ObjectFactory objectFactory) {
        this.objectFactory = objectFactory;
    }

    /**
     * @param customerSubscriptionErrorCodeProcessor
     *            the customerSubscriptionErrorCodeProcessor to set
     */
    @Required
    public void setCustomerSubscriptionErrorCodeProcessor(
            final CustomerSubscriptionErrorCodeProcessor customerSubscriptionErrorCodeProcessor) {
        this.customerSubscriptionErrorCodeProcessor = customerSubscriptionErrorCodeProcessor;
    }



}
