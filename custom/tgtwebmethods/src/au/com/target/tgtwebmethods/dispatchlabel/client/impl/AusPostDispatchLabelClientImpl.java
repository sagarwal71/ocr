/**
 * 
 */
package au.com.target.tgtwebmethods.dispatchlabel.client.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtauspost.dispatchlabel.client.DispatchLabelClient;
import au.com.target.tgtauspost.integration.dto.AuspostRequestDTO;
import au.com.target.tgtfulfilment.integration.dto.IntegrationError;
import au.com.target.tgtfulfilment.integration.dto.LabelResponseDTO;
import au.com.target.tgtwebmethods.client.AbstractWebMethodsClient;
import au.com.target.tgtwebmethods.client.util.IntegrationErrorCodeProcessor;
import au.com.target.tgtwebmethods.rest.LabelJSONResponse;
import au.com.target.tgtwebmethods.util.WMUtil;


/**
 * @author rsamuel3
 *
 */
public class AusPostDispatchLabelClientImpl extends AbstractWebMethodsClient implements DispatchLabelClient {

    private static final Logger LOG = Logger.getLogger(AusPostDispatchLabelClientImpl.class);
    private static final String LOGGING_PREFIX = "AusPost eParcel Dispatch: ";

    private RestTemplate restTemplate;
    private String serviceEndpoint;

    /**
     * {@inheritDoc}
     */
    @Override
    public LabelResponseDTO retrieveDispatchLabel(final AuspostRequestDTO request) {
        LOG.info(LOGGING_PREFIX + "Sending request=" + WMUtil.convertToJSONString(request));

        final LabelResponseDTO response = new LabelResponseDTO();
        final LabelJSONResponse jsonResponse = send(request, response, LabelJSONResponse.class);

        if (jsonResponse != null) {
            if (jsonResponse.isSuccess()) {
                final byte[] file = jsonResponse.getAttachment();
                if (file != null && file.length > 0) {
                    LOG.info(LOGGING_PREFIX + "SUCCEEDED and the file attachment has " + file.length + " bytes.");
                    response.setData(file);
                    return (LabelResponseDTO)IntegrationErrorCodeProcessor.getSuccessResponse(response);
                }
                else {
                    return (LabelResponseDTO)IntegrationErrorCodeProcessor.getFailedResponse(response,
                            IntegrationError.COMMUNICATION_ERROR, "NOT_SPECIFIED",
                            "AusPost eParcel Label returned an empty file");
                }
            }
        }

        return response;
    }

    /**
     * @param restTemplate
     *            the restTemplate to set
     */
    @Required
    public void setRestTemplate(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * @param serviceEndpoint
     *            the serviceEndpoint to set
     */
    @Required
    public void setServiceEndpoint(final String serviceEndpoint) {
        this.serviceEndpoint = serviceEndpoint;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.client.AbstractWebMethodsClient#getServiceEndpoint()
     */
    @Override
    protected String getServiceEndpoint() {
        return serviceEndpoint;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.client.AbstractWebMethodsClient#getRestTemplate()
     */
    @Override
    protected RestTemplate getRestTemplate() {
        return restTemplate;
    }

}
