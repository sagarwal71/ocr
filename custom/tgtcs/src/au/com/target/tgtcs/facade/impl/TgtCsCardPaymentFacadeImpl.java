/**
 * 
 */
package au.com.target.tgtcs.facade.impl;

import de.hybris.platform.cockpit.wizards.Message;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.cscockpit.services.payment.strategy.impl.DefaultCsOrderUnauthorizedTotalStrategy;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.payment.dto.BillingInfo;
import de.hybris.platform.payment.dto.CardInfo;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Form;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Textbox;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.ordercancel.data.IpgNewRefundInfoDTO;
import au.com.target.tgtcore.refund.TargetRefundService;
import au.com.target.tgtcs.components.orderdetail.dto.TargetHostedPaymentFormData;
import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtcs.facade.TgtCsCardPaymentFacade;
import au.com.target.tgtcs.util.TargetCsCockpitUtil;
import au.com.target.tgtpayment.dto.HostedSessionTokenRequest;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.service.TargetPaymentService;
import au.com.target.tgtutility.constants.TgtutilityConstants.ErrorCode;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * @author umesh
 * 
 */
public class TgtCsCardPaymentFacadeImpl implements TgtCsCardPaymentFacade {

    protected static final String SESSION_INVALID_ID = "tgtcscockpit.payment.invalidSessionId";
    protected static final String NEW_CARD_ADDED_MESSAGE = "tgtcscockpit.payment.newCard.added";
    protected static final String NEW_PAYMENT_ADDED_MESSAGE = "tgtcscockpit.newPayment.added";
    protected static final String FAILED_TO_GENERATE_TOKEN = "tgtcscockpit.payment.error.failedToGenerateToken";
    protected static final String PAYMENT_SUCCESSFUL = "tgtcscockpit.payment.successful.transactionReferenceNumber";
    protected static final String TRANSACTION_REVIEW_STATUS = "tgtcscockpit.payment.transaction.reviewStatus";
    protected static final String TRANSACTION_REJECTED = "tgtcscockpit.payment.transaction.rejected";
    protected static final String PAYMENT_FAILED = "tgtcscockpit.payment.error.paymentTransactionModelEntry.null";
    protected static final String PAYMENT_TRANSACTION_CAPTURE_FAILED = "tgtcscockpit.payment.transaction.capture.error";
    protected static final String MESSAGE_TO_CLOSE_PAYMENT_WINDOW = "tgtcscockpit.payment.window.close.msg";
    protected static final String INCORRECT_AMOUNT = "tgtcscockpit.payment.validation.amount";

    private static final Logger LOG = Logger.getLogger(TgtCsCardPaymentFacadeImpl.class);

    private TargetPaymentService targetPaymentService;
    private PaymentModeService paymentModeService;
    private PaymentInfoModel newPayment;
    private CreditCardPaymentInfoModel cardPaymentInfoModel;
    private TargetOrderService targetOrderService;
    private String acceptableCards;
    private CommonI18NService commonI18NService;
    private ModelService modelService;
    private TargetRefundService targetRefundService;
    private DefaultCsOrderUnauthorizedTotalStrategy defaultCsOrderUnauthorizedTotalStrategy;
    private IpgNewRefundInfoDTO ipgNewRefundInfoDTO;

    /**
     * Fills billing address
     * 
     * @param billingAddress
     *            the billing address
     * @param billingInfo
     *            the billing info
     */
    private void saveBillingAddress(final TargetAddressModel billingAddress,
            final BillingInfo billingInfo) {
        billingAddress.setFirstname(billingInfo.getFirstName());
        billingAddress.setLastname(billingInfo.getLastName());
        billingAddress.setLine1(billingInfo.getStreet1());
        billingAddress.setLine2(billingInfo.getStreet2());
        billingAddress.setTown(billingInfo.getCity());
        billingAddress.setDistrict(billingInfo.getState());
        billingAddress.setPostalcode(billingInfo.getPostalCode());
        billingAddress.setCountry(getCommonI18NService().getCountry(billingInfo.getCountry()));
        billingAddress.setPhone1(billingInfo.getPhoneNumber());
        billingAddress.setBillingAddress(Boolean.TRUE);
    }

    @Override
    public List<String> handleRefundPaymentFormResponse(final Execution execution) {
        final ArrayList<String> results = new ArrayList<>();
        if (execution != null) {
            execution.getDesktop().invalidate();
            LOG.info(SplunkLogFormatter.formatMessage(
                    "Adding new payment details for vpc refund. Proceeding for new payment.",
                    ErrorCode.INFO_PAYMENT));
            final TargetHostedPaymentFormData data = createTargetHostedPaymentFormData(execution);
            validateForNewCard(data);
            cleanupTNSFieldValues(data);
            final List<Message> messages = data.getMessages();
            if (!CollectionUtils.isNotEmpty(messages)) {
                final CardInfo cardInfo = createCardInfo(execution);
                final BillingInfo billingInfo = createBillingInfo(execution);
                cardInfo.setBillingInfo(billingInfo);
                final String paymentSessionId = execution.getParameter(TgtcsConstants.PAYMENT_SESSION_ID);
                final String savedToken = targetPaymentService.tokenize(paymentSessionId,
                        paymentModeService.getPaymentModeForCode(TgtcsConstants.CREDITCARD));
                if (savedToken != null) {
                    final CreditCardPaymentInfoModel cardPaymentInfo = createCreditCardPaymentInfoModel(cardInfo,
                            savedToken, execution);
                    this.setCardPaymentInfoModel(cardPaymentInfo);
                    LOG.info(SplunkLogFormatter.formatMessage(
                            "New Payment has beed added Successfully. Proceeding for refund.",
                            ErrorCode.INFO_PAYMENT));
                    results.add(Labels.getLabel(NEW_CARD_ADDED_MESSAGE));
                }
                else { // token can not generated
                    results.add(Labels.getLabel(FAILED_TO_GENERATE_TOKEN));
                }

            } // no  validation messages
            else { // has validation errors
                poulateErrorMesseges(results, messages);
                LOG.info(SplunkLogFormatter.formatMessage(
                        "Invalid card details.", ErrorCode.INVALID_CARD));
            }
        }
        return results;
    }

    @Override
    public List<String> handleCancelPaymentFormResponse(final Execution execution) {
        final ArrayList<String> results = new ArrayList<>();
        results.add(TgtcsConstants.FAILURE); //by default, we assume the following operation will fails
        clearOldPayment();
        if (execution != null) {
            final TargetHostedPaymentFormData data = createTargetHostedPaymentFormData(execution);
            final String operationType = execution.getParameter("paymentType");
            int operationTypeValue = 0; //default, just make token

            try {
                operationTypeValue = Integer.parseInt(operationType);
            }
            catch (final NumberFormatException e) {
                LOG.error(e);
            }
            validate(data, operationTypeValue, execution);
            cleanupTNSFieldValues(data);
            final List<Message> messages = data.getMessages();
            if (CollectionUtils.isEmpty(messages)) {
                final CardInfo cardInfo = createCardInfo(execution);
                final BillingInfo billingInfo = createBillingInfo(execution);
                cardInfo.setBillingInfo(billingInfo);
                final String paymentSessionId = execution.getParameter(TgtcsConstants.PAYMENT_SESSION_ID);
                final String savedToken = targetPaymentService.tokenize(paymentSessionId,
                        paymentModeService.getPaymentModeForCode(TgtcsConstants.CREDITCARD));
                if (savedToken != null) {
                    final CreditCardPaymentInfoModel cardPaymentInfo = createCreditCardPaymentInfoModel(cardInfo,
                            savedToken, execution);
                    if (operationTypeValue == TgtCsAddPaymentFormFacadeImpl.PAYMENT_OPERATION_TYPE_TOKENIZE) {
                        results.set(0, TgtcsConstants.SUCCESS);
                        results.add(Labels.getLabel(NEW_PAYMENT_ADDED_MESSAGE));
                        newPayment = cardPaymentInfo;
                    }
                    this.setCardPaymentInfoModel(cardPaymentInfo);
                }
                else { // token can not generated
                    results.add(Labels.getLabel(FAILED_TO_GENERATE_TOKEN));
                }
            } // no  validation messages
            else { // has validation errors
                poulateErrorMesseges(results, messages);
            }
        }
        return results;
    }

    @Override
    public void createPaymentInfoModelForIpg(final AbstractComponent component) {
        if (component instanceof Form) {
            final Form form = (Form)component;
            final Double amount = ((Doublebox)form.getFellow(TgtcsConstants.AMOUNT)).getValue();
            final String receiptNumber = ((Textbox)form.getFellow(TgtcsConstants.IPG_RECEIPT_NUMBER)).getValue();
            TargetCsCockpitUtil.validateManualRefundInput(amount, receiptNumber);
            ipgNewRefundInfoDTO = new IpgNewRefundInfoDTO();
            ipgNewRefundInfoDTO.setAmount(amount);
            ipgNewRefundInfoDTO.setReceiptNo(receiptNumber);
        }
    }

    /**
     * @param results
     * @param messages
     */
    private void poulateErrorMesseges(final ArrayList<String> results, final List<Message> messages) {
        if (CollectionUtils.isNotEmpty(messages)) {
            for (final Message message : messages) {
                results.add(message.getMessageText());
            }
        }
    }


    /**
     * @param data
     * @param operationTypeValue
     * @param execution
     */
    protected void validate(final TargetHostedPaymentFormData data, final int operationTypeValue,
            final Execution execution) {

        final List<Message> messages = new ArrayList<>();
        validateBillingAddress(data, messages);
        if (operationTypeValue == TgtCsAddPaymentFormFacadeImpl.PAYMENT_OPERATION_TYPE_CAPTURE) {
            try {
                validateAmount(Double.parseDouble(data.getAmount()), messages, execution);
            }
            catch (final NumberFormatException e) {
                messages.add(new Message(Message.ERROR, TgtcsConstants.AMOUNT_REQUIRED,
                        TgtcsConstants.AMOUNT));
            }
        }
        validateCardInfoGeneral(data, messages);
        validateTNSFields(data, messages);

        data.setMessages(messages);
    }


    /**
     * @param amount
     *            the amount
     * @param messages
     *            the messages
     * @param execution
     *            the execution
     */
    protected void validateAmount(final double amount, final List<Message> messages, final Execution execution) {
        final String orderid = execution.getParameter(TgtcsConstants.ORDER_ID);
        final OrderModel order = targetOrderService.findOrderModelForOrderId(orderid);

        final double roundedAmount = getCommonI18NService().roundCurrency(amount,
                order.getCurrency().getDigits().intValue());
        final BigDecimal amountToCapture = BigDecimal.valueOf(roundedAmount);

        if (amountToCapture.compareTo(BigDecimal.ZERO) <= 0)
        {
            messages.add(new Message(Message.ERROR, TgtcsConstants.AMOUNT_MUST_NOT_BE_ZERO,
                    TgtcsConstants.AMOUNT));
        }

        //also amountToCapture can not be bigger than what is unauthorized
        if (roundedAmount > getSuggestedAmountForPaymentOption(order)) {
            messages.add(new Message(Message.ERROR,
                    Labels.getLabel(INCORRECT_AMOUNT)
                            + getSuggestedAmountForPaymentOption(order), TgtcsConstants.AMOUNT));
        }
    }

    @Override
    public double getSuggestedAmountForPaymentOption(final OrderModel order) {
        return defaultCsOrderUnauthorizedTotalStrategy.getUnauthorizedTotal(order);
    }

    /**
     * Creates billing info
     * 
     * @param execution
     *            the execution
     * 
     * @return billing info
     */
    protected BillingInfo createBillingInfo(final Execution execution) {
        final BillingInfo billingInfo = new BillingInfo();
        billingInfo.setFirstName(execution.getParameter(TgtcsConstants.FIRST_NAME));
        billingInfo.setLastName(execution.getParameter(TgtcsConstants.LAST_NAME));
        billingInfo.setStreet1(execution.getParameter(TgtcsConstants.STREET1));
        billingInfo.setStreet2(execution.getParameter(TgtcsConstants.STREET2));
        billingInfo.setCity(execution.getParameter(TgtcsConstants.CITY));
        billingInfo.setPostalCode(execution.getParameter(TgtcsConstants.POSTAL_CODE));
        billingInfo.setCountry(execution.getParameter(TgtcsConstants.COUNTRY));
        billingInfo.setState(execution.getParameter(TgtcsConstants.STATE));
        billingInfo.setPhoneNumber(execution.getParameter(TgtcsConstants.PHONE_NUMBER));
        return billingInfo;
    }

    /**
     * Creates card info
     * 
     * @param execution
     *            the execution
     * 
     * @return card info
     */
    protected CardInfo createCardInfo(final Execution execution) {
        final CardInfo cardInfo = new CardInfo();
        cardInfo.setCardHolderFullName(execution.getParameter(TgtcsConstants.NAME_ON_CARD));
        final String cardname = execution.getParameter(TgtcsConstants.GATEWAY_CARD_SCHEME);
        cardInfo.setCardType(getCreditCardType(cardname));
        cardInfo.setCardNumber(execution.getParameter(TgtcsConstants.GATEWAY_CARD_NUMBER));
        final String expiryMonth = execution.getParameter(TgtcsConstants.GATEWAY_CARD_EXPIRY_DATE_MONTH);
        final String expiryYear = execution.getParameter(TgtcsConstants.GATEWAY_CARD_EXPIRY_DATE_YEAR);
        if (StringUtils.isNumeric(expiryMonth)) {
            cardInfo.setExpirationMonth(Integer.valueOf(expiryMonth));
        }
        if (StringUtils.isNumeric(expiryYear)) {
            cardInfo.setExpirationYear(Integer.valueOf(expiryYear));
        }
        cardInfo.setCv2Number(execution.getParameter(TgtcsConstants.GATEWAY_CARD_SECURITY_CODE));
        return cardInfo;
    }

    /**
     * Creates {@link CreditCardPaymentInfoModel}
     * 
     * @param cardInfo
     *            the CardInfo
     * @param savedToken
     *            the saved token
     * @param execution
     * @return {@link CreditCardPaymentInfoModel}
     */
    protected CreditCardPaymentInfoModel createCreditCardPaymentInfoModel(final CardInfo cardInfo,
            final String savedToken, final Execution execution) {
        final CreditCardPaymentInfoModel cardPaymentInfo = getModelService().create(
                CreditCardPaymentInfoModel.class);
        final String orderid = execution.getParameter(TgtcsConstants.ORDER_ID);
        final OrderModel order = targetOrderService.findOrderModelForOrderId(orderid);

        cardPaymentInfo.setCode(order.getUser().getUid() + TgtCoreConstants.Seperators.UNDERSCORE
                + UUID.randomUUID());
        cardPaymentInfo.setUser(order.getUser());
        cardPaymentInfo.setSubscriptionId(savedToken);
        if (cardInfo != null) {
            cardPaymentInfo.setNumber(cardInfo.getCardNumber());
            cardPaymentInfo.setType(cardInfo.getCardType());
            cardPaymentInfo.setCcOwner(cardInfo.getCardHolderFullName());
            cardPaymentInfo.setValidToMonth(String.valueOf(cardInfo.getExpirationMonth()));
            cardPaymentInfo.setValidToYear(String.valueOf(cardInfo.getExpirationYear()));
            cardPaymentInfo.setSaved(false);
            final TargetAddressModel billingAddress = getModelService().create(TargetAddressModel.class);
            saveBillingAddress(billingAddress, cardInfo.getBillingInfo());
            billingAddress.setOwner(cardPaymentInfo);
            cardPaymentInfo.setBillingAddress(billingAddress);
        }
        return cardPaymentInfo;
    }

    /**
     * Creates {@link TargetHostedPaymentFormData}
     * 
     * @param execution
     *            the execution
     * @return {@link TargetHostedPaymentFormData}
     */
    protected TargetHostedPaymentFormData createTargetHostedPaymentFormData(final Execution execution) {
        final TargetHostedPaymentFormData data = new TargetHostedPaymentFormData();
        data.setAmount(execution.getParameter(TgtcsConstants.AMOUNT));
        data.setCardHolderFullName(execution.getParameter(TgtcsConstants.NAME_ON_CARD));
        data.setCardType(execution.getParameter(TgtcsConstants.CARD_TYPE));
        data.setCardScheme(execution.getParameter(TgtcsConstants.GATEWAY_CARD_SCHEME));
        data.setCardNumber(execution.getParameter(TgtcsConstants.GATEWAY_CARD_NUMBER));
        data.setCardExpiryMonth(execution.getParameter(TgtcsConstants.GATEWAY_CARD_EXPIRY_DATE_MONTH));
        data.setCardExpiryYear(execution.getParameter(TgtcsConstants.GATEWAY_CARD_EXPIRY_DATE_YEAR));
        data.setCardSecurityCode(execution.getParameter(TgtcsConstants.GATEWAY_CARD_SECURITY_CODE));
        data.setFirstName(execution.getParameter(TgtcsConstants.FIRST_NAME));
        data.setLastName(execution.getParameter(TgtcsConstants.LAST_NAME));
        data.setStreetAddress(execution.getParameter(TgtcsConstants.STREET1));
        data.setStreetAddress2(execution.getParameter(TgtcsConstants.STREET2));
        data.setCity(execution.getParameter(TgtcsConstants.CITY));
        data.setPostalCode(execution.getParameter(TgtcsConstants.POSTAL_CODE));
        data.setCountry(execution.getParameter(TgtcsConstants.COUNTRY));
        data.setState(execution.getParameter(TgtcsConstants.STATE));
        data.setPhoneNumber(execution.getParameter(TgtcsConstants.PHONE_NUMBER));
        data.setGatewayFormResponse(execution.getParameter(TgtcsConstants.GATEWAY_FORM_RESPONSE));
        return data;
    }

    /**
     * Returns credit card type
     * 
     * @param creditCardType
     *            the credit card type
     * @return credit card type
     */
    protected CreditCardType getCreditCardType(final String creditCardType) {
        final CreditCardType[] cardtypes = CreditCardType.values();
        for (final CreditCardType creditCard : cardtypes) {
            if (creditCardType.startsWith(creditCard.toString())) {
                return creditCard;
            }
        }
        return null;
    }

    /**
     * Validates new card
     * 
     * @param data
     *            the TargetHostedPaymentFormData
     */
    private void validateForNewCard(final TargetHostedPaymentFormData data) {
        final List<Message> messages = new ArrayList<>();
        validateBillingAddress(data, messages);
        validateCardInfoGeneral(data, messages);
        validateTNSFields(data, messages);

        data.setMessages(messages);
    }

    /**
     * Validates card's information
     * 
     * @param data
     *            the TargetHostedPaymentFormData
     * @param messages
     *            list with error messages
     */
    private void validateCardInfoGeneral(final TargetHostedPaymentFormData data, final List<Message> messages) {
        // Validate Cardholder name
        if (StringUtils.isBlank(data.getCardHolderFullName())) {
            messages.add(new Message(Message.ERROR, Labels.getLabel(TgtcsConstants.CARD_HOLDER_FULL_NAME_REQUIRED),
                    TgtcsConstants.NAME_ON_CARD));
        }
        if (StringUtils.isBlank(data.getCardType())) {
            messages.add(new Message(Message.ERROR, Labels.getLabel(TgtcsConstants.CARD_TYPE_REQUIRED),
                    TgtcsConstants.CARD_TYPE));
        }
        //add validation to only accept configured cards.
        final List<String> cardList = getAcceptableCardList();
        if (CollectionUtils.isNotEmpty(cardList)) {
            if (!cardList.contains(data.getCardType().toUpperCase())) {
                messages.add(new Message(Message.ERROR, "Card Type Unsupported", "cardType"));
            }
        }
    }

    /**
     * Validates billing address
     * 
     * @param data
     *            the TargetHostedPaymentFormData
     * @param messages
     *            list with error messages
     */
    protected void validateBillingAddress(final TargetHostedPaymentFormData data, final List<Message> messages) {
        if (StringUtils.isBlank(data.getFirstName())) {
            messages.add(new Message(Message.ERROR, Labels.getLabel(TgtcsConstants.FIRST_NAME_REQUIRED),
                    TgtcsConstants.FIRST_NAME));
        }
        if (StringUtils.isBlank(data.getLastName())) {
            messages.add(new Message(Message.ERROR, Labels.getLabel(TgtcsConstants.LAST_NAME_REQUIRED),
                    TgtcsConstants.LAST_NAME));
        }
        if (StringUtils.isBlank(data.getStreetAddress())) {
            messages.add(new Message(Message.ERROR, Labels.getLabel(TgtcsConstants.STREET_REQUIRED),
                    TgtcsConstants.STREET1));
        }
        if (StringUtils.isBlank(data.getCity())) {
            messages.add(new Message(Message.ERROR,
                    Labels.getLabel(TgtcsConstants.CITY_SUBURB_REQUIRED), TgtcsConstants.CITY));
        }
        if (StringUtils.isBlank(data.getState())) {
            messages.add(new Message(Message.ERROR, Labels.getLabel(TgtcsConstants.STATE_REQUIRED),
                    TgtcsConstants.STATE));
        }
        if (StringUtils.isBlank(data.getCountry())) {
            messages.add(new Message(Message.ERROR, Labels.getLabel(TgtcsConstants.COUNTRY_REQUIRED),
                    TgtcsConstants.COUNTRY));
        }
        if (StringUtils.isBlank(data.getPostalCode())) {
            messages.add(new Message(Message.ERROR, Labels.getLabel(TgtcsConstants.POSTAL_CODE_REQUIRED),
                    TgtcsConstants.POSTAL_CODE));
        }
    }

    /**
     * Validates given tns field
     * 
     * @param fieldName
     *            the field name
     * @param value
     *            the field value to validate
     * @param label
     *            the field label
     * @param messages
     *            list with error messages
     */
    private void validateTNSField(final String fieldName, final String value, final String label,
            final List<Message> messages) {
        if (!StringUtils.isBlank(value)) {
            if (value.startsWith(TgtcsConstants.TNS_ERROR_TYPE_FIELD_REQUIRED)) {
                messages.add(new Message(Message.ERROR, label + TgtcsConstants.SPACE_SEPARATOR
                        + Labels.getLabel(TgtcsConstants.TNS_FIELD_REQUIRED),
                        fieldName));
            }
            else if (value.startsWith(TgtcsConstants.TNS_ERROR_TYPE_FIELD_INVALID)) {
                messages.add(new Message(Message.ERROR, Labels.getLabel(TgtcsConstants.TNS_ERROR_INVALID)
                        + TgtcsConstants.SPACE_SEPARATOR + label,
                        fieldName));
            }
        }
    }

    @Override
    public void processRefund(final ReturnRequestModel request, final OrderModel orderModel) {
        targetRefundService.apply(request, orderModel,
                getCardPaymentInfoModel(), getIpgNewRefundInfoDTO());

    }

    /**
     * Validates all tns fields
     * 
     * @param data
     *            the TargetHostedPaymentFormData
     * @param messages
     *            list with error messages
     */
    private void validateTNSFields(final TargetHostedPaymentFormData data, final List<Message> messages) {
        validateTNSField(TgtcsConstants.GATEWAY_CARD_NUMBER, data.getCardNumber(),
                Labels.getLabel(TgtcsConstants.CARD_NUMBER), messages);
        validateTNSField(TgtcsConstants.GATEWAY_CARD_EXPIRY_DATE_MONTH, data.getCardExpiryMonth(),
                Labels.getLabel(TgtcsConstants.CARD_EXPIRY_MONTH), messages);
        validateTNSField(TgtcsConstants.GATEWAY_CARD_EXPIRY_DATE_YEAR, data.getCardExpiryYear(),
                Labels.getLabel(TgtcsConstants.CARD_EXPIRY_YEAR), messages);
        validateTNSField(TgtcsConstants.GATEWAY_CARD_SECURITY_CODE, data.getCardSecurityCode(),
                Labels.getLabel(TgtcsConstants.CARD_SECURITY_CODE), messages);

        final String cardType = data.getCardScheme();

        if (!TgtcsConstants.DINERS_CLUB.equalsIgnoreCase(cardType)
                && data.getCardSecurityCode() != null
                && StringUtils.EMPTY.equals(data.getCardSecurityCode())) {
            messages.add(new Message(Message.ERROR, Labels.getLabel(TgtcsConstants.CARD_SECURITY_CODE_REQUIRED),
                    TgtcsConstants.GATEWAY_CARD_SECURITY_CODE));
        }
        if (StringUtils.isBlank(data.getGatewayFormResponse())) {
            messages.add(new Message(Message.ERROR, Labels.getLabel(TgtcsConstants.MISSING_GATEWAY_FORM_RESPONSE),
                    TgtcsConstants.GATEWAY_FORM_RESPONSE));
        }
        else if (data.getGatewayFormResponse().startsWith(TgtcsConstants.TNS_ERROR_TYPE_FIELD_INVALID)) {
            messages.add(new Message(
                    Message.ERROR,
                    Labels.getLabel(SESSION_INVALID_ID),
                    TgtcsConstants.GATEWAY_FORM_RESPONSE));
            LOG.info(Labels.getLabel(TgtcsConstants.SESSION_KEY_HAS_EXPIRED_GETTING_NEW_ONE));
        }
        else if (data.getGatewayFormResponse().startsWith(TgtcsConstants.TNS_ERROR_TYPE_FIELD_ERROR)) {
            messages.add(new Message(Message.ERROR, Labels.getLabel(TgtcsConstants.TNS_FIELD_ERRORS),
                    TgtcsConstants.GATEWAY_FORM_RESPONSE));
        }
        else if (data.getGatewayFormResponse().startsWith(TgtcsConstants.TNS_ERROR_TYPE_SYSTEM_ERROR)) {
            messages.add(new Message(Message.ERROR, Labels.getLabel(TgtcsConstants.TNS_SYSTEM_ERROR),
                    TgtcsConstants.GATEWAY_FORM_RESPONSE));
        }
    }

    /**
     * Cleans up tns fields values
     * 
     * @param data
     *            the TargetHostedPaymentFormData
     */
    protected void cleanupTNSFieldValues(final TargetHostedPaymentFormData data) {
        data.setCardExpiryMonth(cleanupTNSFieldValue(data.getCardExpiryMonth()));
        data.setCardExpiryYear(cleanupTNSFieldValue(data.getCardExpiryYear()));
        data.setCardNumber(cleanupTNSFieldValue(data.getCardNumber()));
        data.setCardSecurityCode(cleanupTNSFieldValue(data.getCardSecurityCode()));
    }

    /**
     * Cleans up given tns field value
     * 
     * @param value
     *            value to clean up
     * @return cleaned tns field value
     */
    protected String cleanupTNSFieldValue(final String value) {
        //value can be prefix with 1~, 2~, 3~, if found removing those prefixes.
        if (!StringUtils.isBlank(value)
                && (value.startsWith(TgtcsConstants.TNS_ERROR_TYPE_FIELD_REQUIRED)
                        || value.startsWith(TgtcsConstants.TNS_ERROR_TYPE_FIELD_INVALID) || value
                            .startsWith(TgtcsConstants.TNS_ERROR_TYPE_FIELD_ERROR))) {
            return value.substring(2);
        }
        return value;

    }

    /**
     * clear any old existing payment sessions
     */
    @Override
    public void clearOldPayment() {
        this.newPayment = null;
        this.cardPaymentInfoModel = null;
        this.ipgNewRefundInfoDTO = null;
    }


    /**
     * @return the cardPaymentInfoModel
     */
    public CreditCardPaymentInfoModel getCardPaymentInfoModel() {
        return cardPaymentInfoModel;
    }

    /**
     * @param cardPaymentInfoModel
     *            the cardPaymentInfoModel to set
     */
    public void setCardPaymentInfoModel(final CreditCardPaymentInfoModel cardPaymentInfoModel) {
        this.cardPaymentInfoModel = cardPaymentInfoModel;
    }

    /**
     * return the new payment
     * 
     * @return PaymentInfoModel
     */
    @Override
    public PaymentInfoModel getNewPayment() {
        return newPayment;
    }

    /**
     * @return the targetPaymentService
     */
    public TargetPaymentService getTargetPaymentService() {
        return targetPaymentService;
    }

    /**
     * @param targetPaymentService
     *            the targetPaymentService to set
     */
    @Required
    public void setTargetPaymentService(final TargetPaymentService targetPaymentService) {
        this.targetPaymentService = targetPaymentService;
    }

    /**
     * @return the paymentModeService
     */
    public PaymentModeService getPaymentModeService() {
        return paymentModeService;
    }

    /**
     * @param paymentModeService
     *            the paymentModeService to set
     */
    @Required
    public void setPaymentModeService(final PaymentModeService paymentModeService) {
        this.paymentModeService = paymentModeService;
    }

    /**
     * @param targetOrderService
     *            the targetOrderService to set
     */
    @Required
    public void setTargetOrderService(final TargetOrderService targetOrderService) {
        this.targetOrderService = targetOrderService;
    }

    /**
     * @param acceptableCards
     *            the acceptableCards to set
     */
    @Required
    public void setAcceptableCards(final String acceptableCards) {
        this.acceptableCards = acceptableCards;
    }

    /**
     * @param commonI18NService
     *            the commonI18NService to set
     */
    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    /**
     * @return the commonI18NService
     */
    public CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    /**
     * @return the modelService
     */
    public ModelService getModelService() {
        return modelService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @return the acceptableCards
     */
    public List<String> getAcceptableCardList() {
        if (StringUtils.isNotEmpty(acceptableCards)) {
            final String[] cardType = StringUtils.split(acceptableCards, TgtcsConstants.PIPE_SEPARATOR);
            return Arrays.asList(cardType);
        }
        return null;
    }

    /**
     * @param targetRefundService
     *            the targetRefundService to set
     */
    @Required
    public void setTargetRefundService(final TargetRefundService targetRefundService) {
        this.targetRefundService = targetRefundService;
    }

    /**
     * @param defaultCsOrderUnauthorizedTotalStrategy
     *            the defaultCsOrderUnauthorizedTotalStrategy to set
     */
    @Required
    public void setDefaultCsOrderUnauthorizedTotalStrategy(
            final DefaultCsOrderUnauthorizedTotalStrategy defaultCsOrderUnauthorizedTotalStrategy) {
        this.defaultCsOrderUnauthorizedTotalStrategy = defaultCsOrderUnauthorizedTotalStrategy;
    }

    @Override
    public HostedSessionTokenRequest createHostedSessionTokenRequest(final CartModel cart, final BigDecimal amount,
            final PaymentModeModel paymentMode, final String cancelUrl, final String returnUrl,
            final PaymentCaptureType pct, final String sessionId, final IpgPaymentTemplateType ipgPaymentTemplateType) {
        final HostedSessionTokenRequest hostedSessionTokenRequest = new HostedSessionTokenRequest();
        hostedSessionTokenRequest.setAmount(amount);
        hostedSessionTokenRequest.setCancelUrl(cancelUrl);
        hostedSessionTokenRequest.setOrderModel(cart);
        hostedSessionTokenRequest.setPaymentCaptureType(pct);
        hostedSessionTokenRequest.setPaymentMode(paymentMode);
        hostedSessionTokenRequest.setReturnUrl(returnUrl);
        hostedSessionTokenRequest.setSessionId(sessionId);
        hostedSessionTokenRequest.setIpgPaymentTemplateType(ipgPaymentTemplateType);
        return hostedSessionTokenRequest;
    }

    /**
     * @return the ipgNewRefundInfoDTO
     */
    @Override
    public IpgNewRefundInfoDTO getIpgNewRefundInfoDTO() {
        return ipgNewRefundInfoDTO;
    }

    /**
     * @param ipgNewRefundInfoDTO
     *            the ipgNewRefundInfoDTO to set
     */
    public void setIpgNewRefundInfoDTO(final IpgNewRefundInfoDTO ipgNewRefundInfoDTO) {
        this.ipgNewRefundInfoDTO = ipgNewRefundInfoDTO;
    }



}
