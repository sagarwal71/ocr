/**
 * 
 */
package au.com.target.tgtcs.strategies.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.session.UICockpitPerspective;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.widgets.browsers.WidgetBrowserModel;
import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.cscockpit.strategies.impl.DefaultDeliveryModeCostRetrievalStrategy;
import de.hybris.platform.cscockpit.widgets.controllers.BasketController;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;

import java.math.BigDecimal;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.order.TargetFindDeliveryCostStrategy;
import au.com.target.tgtcs.widgets.controllers.TargetBasketController;


/**
 * Target delivery cost strategy retrieve cost from current basket
 */
public class TargetDeliveryModeCostRetrievalStrategy extends DefaultDeliveryModeCostRetrievalStrategy {

    private TargetFindDeliveryCostStrategy targetFindDeliveryCostStrategy;

    @Override
    public BigDecimal getDeliveryCostForDeliveryMode(final DeliveryModeModel deliveryMode) {
        BigDecimal cost = null;
        if (deliveryMode instanceof ZoneDeliveryModeModel)
        {
            final TypedObject cartObject = getCart();


            if ((cartObject != null) && (cartObject.getObject() instanceof CartModel))
            {
                final CartModel cart = (CartModel)cartObject.getObject();
                try {
                    cost = BigDecimal.valueOf(getTargetFindDeliveryCostStrategy().getDeliveryCost(
                            (ZoneDeliveryModeModel)deliveryMode, cart));
                }
                catch (final TargetNoPostCodeException ex) {
                    //
                }
            }
        }
        return cost;


    }


    @Override
    public String getFormattedDeliveryCostForDeliveryMode(final DeliveryModeModel deliveryMode) {
        return super.getFormattedDeliveryCostForDeliveryMode(deliveryMode);
    }

    protected UICockpitPerspective getCurrentPerspective() {
        return UISessionUtils.getCurrentSession().getCurrentPerspective();
    }


    protected TypedObject getCart() {
        TypedObject cartObject = null;
        final String brauserCode = ((WidgetBrowserModel)getCurrentPerspective().getBrowserArea()
                .getFocusedBrowser()).getBrowserCode();
        final Collection<TargetBasketController> baskets = getBasketControllers();
        for (final BasketController controller : baskets) {
            if (controller.getCheckoutBrowser().getBrowserCode().equals(brauserCode)) {
                cartObject = controller.getCart();
                break;
            }

        }
        return cartObject;
    }

    /**
     * @deprecated Use {@link TargetDeliveryModeCostRetrievalStrategy#getTargetFindDeliveryCostStrategy()};
     */
    @Override
    @Deprecated
    protected DeliveryService getDeliveryService() {
        return super.getDeliveryService();
    }

    protected TargetFindDeliveryCostStrategy getTargetFindDeliveryCostStrategy() {
        return targetFindDeliveryCostStrategy;
    }

    @Required
    public void setTargetFindDeliveryCostStrategy(final TargetFindDeliveryCostStrategy targetFindDeliveryCostStrategy) {
        this.targetFindDeliveryCostStrategy = targetFindDeliveryCostStrategy;
    }


    protected Collection<TargetBasketController> getBasketControllers() {
        return Registry.getApplicationContext().getBeansOfType(TargetBasketController.class).values();
    }

}
