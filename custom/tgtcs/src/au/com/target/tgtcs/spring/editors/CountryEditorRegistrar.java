/**
 * 
 */
package au.com.target.tgtcs.spring.editors;

import de.hybris.platform.core.model.c2l.CountryModel;

import org.springframework.beans.PropertyEditorRegistrar;
import org.springframework.beans.PropertyEditorRegistry;


/**
 * @author thomasadolfsson
 *
 */
public class CountryEditorRegistrar implements PropertyEditorRegistrar {

    @Override
    public void registerCustomEditors(final PropertyEditorRegistry registry) {
        registry.registerCustomEditor(CountryModel.class, new CountryEditor());
    }

}
