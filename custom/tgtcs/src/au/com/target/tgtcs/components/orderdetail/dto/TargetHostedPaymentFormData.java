/**
 * 
 */
package au.com.target.tgtcs.components.orderdetail.dto;

import de.hybris.platform.cockpit.wizards.Message;

import java.util.Collections;
import java.util.List;


/**
 * The Class TargetHostedPaymentFormData.
 */
public class TargetHostedPaymentFormData {
    private String amount;
    private String sessionKey;
    private String email;
    private String phoneNumber;
    private String firstName;
    private String lastName;
    private String streetAddress;
    private String streetAddress2;
    private String city;
    private String state;
    private String postalCode;
    private String country;
    private String cardHolderFullName;
    private String cardType;
    private String cardScheme;
    private String cardNumber;
    private String cardExpiryMonth;
    private String cardExpiryYear;
    private String cardSecurityCode;
    private String gatewayFormResponse;
    private List<Message> messages;

    /**
     * 
     * @return empty list or unmodifiable list of messages
     */
    public List<Message> getMessages() {
        return messages == null ? Collections.EMPTY_LIST : Collections.unmodifiableList(messages);
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final String amount) {
        this.amount = amount;
    }

    /**
     * @return the sessionKey
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * @param sessionKey
     *            the sessionKey to set
     */
    public void setSessionKey(final String sessionKey) {
        this.sessionKey = sessionKey;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber
     *            the phoneNumber to set
     */
    public void setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the streetAddress
     */
    public String getStreetAddress() {
        return streetAddress;
    }

    /**
     * @param streetAddress
     *            the streetAddress to set
     */
    public void setStreetAddress(final String streetAddress) {
        this.streetAddress = streetAddress;
    }

    /**
     * @return the streetAddress2
     */
    public String getStreetAddress2() {
        return streetAddress2;
    }

    /**
     * @param streetAddress2
     *            the streetAddress2 to set
     */
    public void setStreetAddress2(final String streetAddress2) {
        this.streetAddress2 = streetAddress2;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city
     *            the city to set
     */
    public void setCity(final String city) {
        this.city = city;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(final String state) {
        this.state = state;
    }

    /**
     * @return the postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * @param postalCode
     *            the postalCode to set
     */
    public void setPostalCode(final String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country
     *            the country to set
     */
    public void setCountry(final String country) {
        this.country = country;
    }

    /**
     * @return the cardHolderFullName
     */
    public String getCardHolderFullName() {
        return cardHolderFullName;
    }

    /**
     * @param cardHolderFullName
     *            the cardHolderFullName to set
     */
    public void setCardHolderFullName(final String cardHolderFullName) {
        this.cardHolderFullName = cardHolderFullName;
    }

    /**
     * @return the cardType
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * @param cardType
     *            the cardType to set
     */
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }

    /**
     * @return the cardScheme
     */
    public String getCardScheme() {
        return cardScheme;
    }

    /**
     * @param cardScheme
     *            the cardScheme to set
     */
    public void setCardScheme(final String cardScheme) {
        this.cardScheme = cardScheme;
    }

    /**
     * @return the cardNumber
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * @param cardNumber
     *            the cardNumber to set
     */
    public void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * @return the cardExpiryMonth
     */
    public String getCardExpiryMonth() {
        return cardExpiryMonth;
    }

    /**
     * @param cardExpiryMonth
     *            the cardExpiryMonth to set
     */
    public void setCardExpiryMonth(final String cardExpiryMonth) {
        this.cardExpiryMonth = cardExpiryMonth;
    }

    /**
     * @return the cardExpiryYear
     */
    public String getCardExpiryYear() {
        return cardExpiryYear;
    }

    /**
     * @param cardExpiryYear
     *            the cardExpiryYear to set
     */
    public void setCardExpiryYear(final String cardExpiryYear) {
        this.cardExpiryYear = cardExpiryYear;
    }

    /**
     * @return the cardSecurityCode
     */
    public String getCardSecurityCode() {
        return cardSecurityCode;
    }

    /**
     * @param cardSecurityCode
     *            the cardSecurityCode to set
     */
    public void setCardSecurityCode(final String cardSecurityCode) {
        this.cardSecurityCode = cardSecurityCode;
    }

    /**
     * @return the gatewayFormResponse
     */
    public String getGatewayFormResponse() {
        return gatewayFormResponse;
    }

    /**
     * @param gatewayFormResponse
     *            the gatewayFormResponse to set
     */
    public void setGatewayFormResponse(final String gatewayFormResponse) {
        this.gatewayFormResponse = gatewayFormResponse;
    }

    /**
     * @param messages
     *            the messages to set
     */
    public void setMessages(final List<Message> messages) {
        this.messages = messages;
    }


}