/**
 * 
 */
package au.com.target.tgtcs.listeners;

import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.api.Combobox;



/**
 * This EventListener, when triggered by {@link SelectEvent}, changes dataset in <code>stateCombobox</code> to match
 * selected Store. The selected Store is retrieved from the SelectEvent that triggered this listener.
 * 
 * @author Pavel_Yakushevich
 * 
 */
public class StoreChangeEventListener implements EventListener {

    private Combobox stateCombobox;

    /**
     * Constructor
     * 
     * @param combobox
     *            the state selection combobox
     */
    public StoreChangeEventListener(final Combobox combobox) {
        setStateCombobox(combobox);
    }

    @Override
    public void onEvent(final Event event) throws Exception {
        if (event instanceof SelectEvent) {
            final Collection<Comboitem> selectedItems = ((SelectEvent)event).getSelectedItems();

            Validate.isTrue(selectedItems.size() == 1, "Expected 1 selected item, found ", selectedItems.size());

            final Object value = selectedItems.iterator().next().getValue();

            if (value instanceof PointOfServiceModel) {
                final String district = ((PointOfServiceModel)value).getAddress().getDistrict();

                if (StringUtils.isEmpty(district)) {
                    return;
                }

                // get combobox items
                final Collection<Comboitem> items = getStateCombobox().getItems();

                // select the district that matches pos's address
                for (final Comboitem item : items) {
                    if (district.equals(item.getValue())) {
                        getStateCombobox().setSelectedItemApi(item);
                        return;
                    }
                }

                // if no matching items found, deselect current item
                getStateCombobox().setSelectedIndex(-1);
            }
        }
    }

    /**
     * @return the stateCombobox
     */
    public Combobox getStateCombobox() {
        return stateCombobox;
    }

    /**
     * @param stateCombobox
     *            the stateCombobox to set
     */
    public void setStateCombobox(final Combobox stateCombobox) {
        this.stateCombobox = stateCombobox;
    }

}
