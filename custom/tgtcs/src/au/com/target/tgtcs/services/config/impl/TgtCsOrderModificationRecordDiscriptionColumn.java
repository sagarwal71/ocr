/**
 * 
 */
package au.com.target.tgtcs.services.config.impl;

import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cscockpit.services.config.impl.AbstractSimpleCustomColumnConfiguration;
import de.hybris.platform.ordercancel.model.OrderCancelRecordModel;
import de.hybris.platform.returns.model.OrderReturnRecordModel;

import java.util.Locale;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.model.AddressModificationRecordModel;


/**
 * @author umesh
 * 
 */
public class TgtCsOrderModificationRecordDiscriptionColumn extends AbstractSimpleCustomColumnConfiguration {

    private static final String CANCEL = "Cancel";
    private static final String REFUND = "Refund";
    private static final String ADDRESS = "Address change";

    @Override
    protected Object getItemValue(final ItemModel itemmodel, final Locale locale) throws ValueHandlerException {
        if (itemmodel instanceof OrderReturnRecordModel) {
            return REFUND;
        }
        if (itemmodel instanceof OrderCancelRecordModel) {
            return CANCEL;
        }
        if (itemmodel instanceof AddressModificationRecordModel) {
            return ADDRESS;
        }
        return StringUtils.EMPTY;
    }
}
