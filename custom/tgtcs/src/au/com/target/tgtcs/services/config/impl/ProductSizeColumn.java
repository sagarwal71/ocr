/**
 * 
 */
package au.com.target.tgtcs.services.config.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cscockpit.services.config.impl.AbstractProductCustomColumn;

import java.util.Locale;

import au.com.target.tgtcs.components.orderdetail.utils.ProductUtil;


/**
 * @author Nandini
 * 
 */
public class ProductSizeColumn extends AbstractProductCustomColumn {

    @Override
    protected String getProductValue(final ProductModel product, final Locale locale) {
        return ProductUtil.getProductSize(product);
    }

}
