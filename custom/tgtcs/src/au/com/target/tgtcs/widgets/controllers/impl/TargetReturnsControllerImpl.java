package au.com.target.tgtcs.widgets.controllers.impl;

import de.hybris.platform.basecommerce.enums.ReturnStatus;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.exceptions.ResourceMessage;
import de.hybris.platform.cscockpit.exceptions.ValidationException;
import de.hybris.platform.cscockpit.utils.SafeUnbox;
import de.hybris.platform.cscockpit.widgets.controllers.impl.DefaultReturnsController;
import de.hybris.platform.returns.model.RefundEntryModel;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.order.FindOrderRefundedShippingStrategy;
import au.com.target.tgtcore.refund.TargetOrderRefundException;
import au.com.target.tgtcore.refund.TargetRefundService;
import au.com.target.tgtcs.facade.TgtCsCardPaymentFacade;
import au.com.target.tgtcs.widgets.controllers.TargetReturnsController;
import au.com.target.tgtpayment.util.PriceCalculator;

import com.google.common.collect.ImmutableList;


/**
 * Target extension to {@link DefaultReturnsController} which handles additional 'delivery cost' form field.
 */
public class TargetReturnsControllerImpl extends DefaultReturnsController implements TargetReturnsController {

    private static final Logger LOG = Logger.getLogger(TargetReturnsControllerImpl.class);

    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.00");

    protected Double shippingAmountToRefund;

    private FindOrderRefundedShippingStrategy findOrderRefundedShippingStrategy;

    private TgtCsCardPaymentFacade tgtCsCardPaymentFacade;

    private TargetRefundService targetRefundService;

    @Override
    public TypedObject createRefundRequest()
    {
        ReturnRequestModel refundRequest = null;
        try
        {
            final OrderModel orderModel = getOrderModel();
            refundRequest = getReturnService().createReturnRequest(orderModel);
            final String rmaString = getReturnService().createRMA(refundRequest);
            refundRequest.setRMA(rmaString);

            applyRefunds(orderModel, refundRequest, refundDetailsList, false);

            return getCockpitTypeService().wrapItem(refundRequest);
        }
        catch (final TargetOrderRefundException re) {
            handleExceptions(refundRequest);
            throw re;
        }
        catch (final Exception e)
        {
            handleExceptions(refundRequest);
            LOG.error("Failed to create refund request", e);
        }
        finally {
            if (refundDetailsList != null) {
                refundDetailsList.clear();
                refundDetailsList = null;//NOPMD
            }
            deleteRefundOrderPreview();

        }

        return null;
    }

    @Override
    protected OrderModel applyRefunds(final OrderModel orderModel, final ReturnRequestModel request,
            final Map<Long, RefundDetails> refundDetailsList, final boolean history) {
        final List<ReturnEntryModel> entries = new ArrayList<>();
        for (final Long p : refundDetailsList.keySet())
        {
            final AbstractOrderEntryModel orderEntryModel = getOrderEntryByEntryNumber(orderModel, SafeUnbox.toLong(p));
            if (orderEntryModel != null)
            {
                final RefundDetails refundDetails = refundDetailsList.get(p);
                final RefundEntryModel refundEntry = getReturnService().createRefund(request, orderEntryModel,
                        refundDetails.getNotes(), Long.valueOf(refundDetails.getExpectedQuantity()),
                        refundDetails.getAction(), refundDetails.getReason());
                entries.add(refundEntry);
            }
            else
            {
                LOG.error("Failed to find orderEntry with entry number [" + p + "]");
            }
        }

        request.setShippingAmountToRefund(shippingAmountToRefund);
        request.setReturnEntries(entries);
        setRefundedAmount(orderModel, request);

        // avoiding creating order version on refund creation - need to be reviewed as if we create a version and modify the actual order on refund,
        // returnable qty reduces by double the actual refund qty (order entry qty reduction + refund record against the order entry).
        // also we are not modifying the order on replacements, so why on refunds?
        if (!history)
        {
            makeRefund(request, orderModel);
        }

        return orderModel;
    }

    protected void setRefundedAmount(final OrderModel order, final ReturnRequestModel returnRequest) {
        if (order.getOriginalVersion() == null) {
            final OrderModel refundOrderPreview = (OrderModel)getRefundOrderPreview().getObject();
            final BigDecimal refundedAmountWithoutShipping = PriceCalculator.subtract(
                    refundOrderPreview.getTotalPrice(), order.getTotalPrice());
            final BigDecimal refundedAmount = PriceCalculator.add(returnRequest.getShippingAmountToRefund(),
                    Double.valueOf(refundedAmountWithoutShipping.doubleValue()));
            returnRequest.setRefundedAmount(Double.valueOf(refundedAmount.doubleValue()));
            getModelService().save(returnRequest);
        }
    }

    /**
     * apply method for refund process
     * 
     * @param request
     * @param orderModel
     */

    protected void makeRefund(final ReturnRequestModel request, final OrderModel orderModel) {
        tgtCsCardPaymentFacade.processRefund(request, orderModel);

    }

    @Override
    public double getMaximumShippingAmountToRefund() {
        final TypedObject typedObject = getCurrentOrder();
        if (typedObject != null && typedObject.getObject() instanceof OrderModel) {
            final OrderModel order = (OrderModel)typedObject.getObject();
            final Double paidObj = order.getInitialDeliveryCost();
            final double paid = paidObj != null ? paidObj.doubleValue() : 0d;
            final double alreadyRefunded = findOrderRefundedShippingStrategy.getRefundedShippingAmount(order);
            return Math.max(paid - alreadyRefunded, 0d);
        }
        return 0;
    }

    @Override
    public boolean validateCreateRefundRequest(final List<ObjectValueContainer> refundEntriesValueContainers)
            throws ValidationException {
        final double maxShippingAmountToRefund = getMaximumShippingAmountToRefund();
        if (shippingAmountToRefund != null
                && (shippingAmountToRefund.doubleValue() < 0
                || shippingAmountToRefund.doubleValue() > maxShippingAmountToRefund)) {
            if (maxShippingAmountToRefund > 0) {
                throw new ValidationException(ImmutableList.of(
                        new ResourceMessage("tgtcscockpit.refundEntry.validation.shippingAmountInvalid",
                                ImmutableList.of(DECIMAL_FORMAT.format(maxShippingAmountToRefund)))));
            }
            else {
                throw new ValidationException(ImmutableList.of(
                        new ResourceMessage("tgtcscockpit.refundEntry.validation.shippingAmountAlreadyRefunded")));
            }
        }

        return validateCreateRefundRequest(refundEntriesValueContainers, shippingAmountToRefund);
    }

    /**
     * @param refundEntriesValueContainers
     * @param shippingAmount
     * @return boolean
     * @throws ValidationException
     */
    protected boolean validateCreateRefundRequest(final List<ObjectValueContainer> refundEntriesValueContainers,
            final Double shippingAmount)
            throws ValidationException {
        final List errorMessages = new ArrayList();

        final Map returnableOrderEntries = getReturnableOrderEntries();
        boolean entryProcessed = false, ok = false;
        for (final ObjectValueContainer ovc : refundEntriesValueContainers)
        {
            final List entryErrorMessages = new ArrayList();

            entryProcessed = false;
            final TypedObject orderEntry = (TypedObject)ovc.getObject();
            final int entryNumber = SafeUnbox.toInt(((AbstractOrderEntryModel)orderEntry.getObject()).getEntryNumber());
            if (returnableOrderEntries.containsKey(orderEntry))
            {
                final ObjectValueContainer.ObjectValueHolder expectedQty = getPropertyValue(ovc,
                        "ReturnEntry.expectedQuantity");
                if ((expectedQty != null) && (expectedQty.getCurrentValue() instanceof Long))
                {
                    final long expectedQtyValue = SafeUnbox.toLong((Long)expectedQty.getCurrentValue());
                    if (expectedQtyValue != 0L)
                    {
                        if (expectedQtyValue < 0L)
                        {
                            entryErrorMessages.add(
                                    new ResourceMessage("returnEntry.validation.expectedQuantity.negative",
                                            Arrays.asList(new Integer[] { Integer.valueOf(entryNumber) })));
                        }

                        if ((expectedQtyValue > 0L)
                                && (SafeUnbox.toLong((Long)returnableOrderEntries.get(orderEntry)) < expectedQtyValue))

                        {
                            entryErrorMessages.add(
                                    new ResourceMessage("returnEntry.validation.expectedQuantity.gtMaxQty",
                                            Arrays.asList(new Integer[] { Integer.valueOf(entryNumber) })));
                        }

                        final ObjectValueContainer.ObjectValueHolder reason = getPropertyValue(ovc,
                                "RefundEntry.reason");
                        if ((reason == null) || (reason.getCurrentValue() == null))

                        {
                            entryErrorMessages.add(new ResourceMessage("returnEntry.validation.reason.missing", Arrays
                                    .asList(new Integer[] {
                                            Integer.valueOf(entryNumber) })));
                        }

                        final ObjectValueContainer.ObjectValueHolder action = getPropertyValue(ovc,
                                "ReturnEntry.action");
                        if ((action == null) || (action.getCurrentValue() == null))

                        {
                            entryErrorMessages.add(new ResourceMessage("returnEntry.validation.action.missing", Arrays
                                    .asList(new Integer[] {
                                            Integer.valueOf(entryNumber) })));
                        }
                        entryProcessed = true;
                    }
                }
            }
            else
            {
                entryErrorMessages.add(new ResourceMessage("returnEntry.validation.product.notReturnable", Arrays
                        .asList(new Integer[] {
                                Integer.valueOf(entryNumber) })));
            }

            if (!(entryErrorMessages.isEmpty()))
            {
                errorMessages.addAll(entryErrorMessages);
            }

            if ((!(entryProcessed)) || (!(entryErrorMessages.isEmpty()))) {
                continue;
            }
            ok = true;
        }
        if (!ok && (errorMessages.isEmpty()) && (shippingAmount != null && shippingAmount.intValue() <= 0)) {
            errorMessages.add(new ResourceMessage("refundRequest.validation.noneSelected"));
        }

        if (!ok && (errorMessages.isEmpty()) && (shippingAmount != null && shippingAmount.intValue() > 0)) {
            ok = true;
        }

        if (!(errorMessages.isEmpty()))
        {
            throw new ValidationException(errorMessages);
        }

        return ok;
    }

    /**
     * Sets additional values for refund / return request object.
     * 
     * @param objectValueContainer
     *            the submitted form
     */
    public void setAdditionalRequestParams(final ObjectValueContainer objectValueContainer) {
        shippingAmountToRefund = (Double)
                getPropertyValue(objectValueContainer, "ReturnRequest.shippingAmountToRefund").getCurrentValue();

        if (shippingAmountToRefund == null) {
            shippingAmountToRefund = Double.valueOf(0d);
        }
    }

    /**
     * @param findOrderRefundedShippingStrategy
     *            the findOrderRefundedShippingStrategy to set
     */
    public void setFindOrderRefundedShippingStrategy(
            final FindOrderRefundedShippingStrategy findOrderRefundedShippingStrategy) {
        this.findOrderRefundedShippingStrategy = findOrderRefundedShippingStrategy;
    }

    /**
     * @param tgtCsCardPaymentFacade
     *            the tgtCsCardPaymentFacade to set
     */
    @Required
    public void setTgtCsCardPaymentFacade(final TgtCsCardPaymentFacade tgtCsCardPaymentFacade) {
        this.tgtCsCardPaymentFacade = tgtCsCardPaymentFacade;
    }

    private void handleExceptions(final ReturnRequestModel refundRequest) {
        if (refundRequest != null) {
            final List<ReturnEntryModel> entries = refundRequest.getReturnEntries();
            for (final ReturnEntryModel entry : entries) {
                entry.setStatus(ReturnStatus.CANCELED);
            }

            getModelService().saveAll(entries);
            getModelService().refresh(refundRequest);
        }
    }

    @Override
    public boolean canReturn()
    {
        final Map returnableOrderEntries = getReturnableOrderEntries();
        return ((returnableOrderEntries != null) && (!(returnableOrderEntries.isEmpty())))
                || targetRefundService.isDeliveryCostRefundable(getOrderModel());
    }

    /**
     * @param targetRefundService
     *            the targetRefundService to set
     */
    public void setTargetRefundService(final TargetRefundService targetRefundService) {
        this.targetRefundService = targetRefundService;
    }


}