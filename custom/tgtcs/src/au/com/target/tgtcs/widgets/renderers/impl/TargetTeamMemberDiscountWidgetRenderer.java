/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.renderers.impl.AbstractCsWidgetRenderer;

import org.apache.commons.lang.StringUtils;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import au.com.target.tgtcore.order.TargetCommerceCheckoutService;
import au.com.target.tgtcs.widgets.controllers.TargetBasketController;
import au.com.target.tgtcs.widgets.controllers.TargetCheckoutController;


/**
 * Checkout tab - Team Member Discount widget
 * 
 */
public class TargetTeamMemberDiscountWidgetRenderer extends AbstractCsWidgetRenderer {

    private static final String CSS_ROW_COMPONENT = "tgtcs_sectionRowComponent";

    private static final String CSS_TEXT_EDITOR = "textEditor";

    private TargetCommerceCheckoutService targetCommerceCheckoutService;

    /* (non-Javadoc)
    * @see de.hybris.platform.cscockpit.widgets.renderers.impl.AbstractCsWidgetRenderer#createContentInternal(de.hybris.platform.cockpit.widgets.Widget, org.zkoss.zk.ui.api.HtmlBasedComponent)
    */
    @Override
    protected HtmlBasedComponent createContentInternal(
            final Widget widget,
            final HtmlBasedComponent rootContainer) {

        final Div content = new Div();
        content.setSclass(CSS_ROW_COMPONENT);
        final Hbox hbox = new Hbox();
        hbox.setParent(content);
        hbox.setWidth("96%");
        hbox.setWidths("12em, none");

        final Label teamMemberCodeLabel = new Label(LabelUtils.getLabel(widget, "teamMemberCodeLabel"));
        teamMemberCodeLabel.setParent(hbox);

        final Textbox memberCodeTextbox = new Textbox();
        memberCodeTextbox.setParent(hbox);
        memberCodeTextbox.setClass(CSS_TEXT_EDITOR);

        final CartModel checkoutCartModel = ((TargetCheckoutController)widget.getWidgetController())
                .getCheckoutCartByMasterCart();
        final String memberCardNumber = checkoutCartModel.getTmdCardNumber();
        if (StringUtils.isNotBlank(memberCardNumber)) {
            memberCodeTextbox.setValue(memberCardNumber);
        }

        final Button applyMemberDiscountButton = new Button(LabelUtils.getLabel(widget, "applyMemberDiscountButton"));
        applyMemberDiscountButton.setParent(hbox);

        applyMemberDiscountButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(final Event event) throws Exception {
                handleItemEvent(memberCodeTextbox, widget);
            }
        });

        return content;
    }

    /**
     * handle fly buys code text box field after user presses enter button
     * 
     * @param memberCodeTextbox
     *            the member code text box
     * @param widget
     *            them member code widget
     * @throws InterruptedException
     */
    private void handleItemEvent(final Textbox memberCodeTextbox, final Widget widget) throws InterruptedException {
        final TargetCheckoutController checkoutController = (TargetCheckoutController)widget.getWidgetController();
        final TargetBasketController basketController = (TargetBasketController)((TargetCheckoutController)widget
                .getWidgetController()).getBasketController();
        final CartModel checkoutCartModel = checkoutController.getCheckoutCartByMasterCart();
        final boolean appliedDiscount = targetCommerceCheckoutService.setTMDCardNumber(checkoutCartModel,
                memberCodeTextbox.getValue());
        if (appliedDiscount) {
            basketController.dispatchEvent(null, basketController, null);
            checkoutController.dispatchEvent(null, checkoutController, null);
            Messagebox.show(LabelUtils.getLabel(widget, "discountApplied"),
                    LabelUtils.getLabel(widget, "teamMemberCodeLabel"), Messagebox.OK,
                    Messagebox.INFORMATION);

            widget.getWidgetController().dispatchEvent(widget.getControllerCtx(), widget, null);
        }
        else {
            Messagebox.show(LabelUtils.getLabel(widget, "invalidTeamMemberCode"),
                    LabelUtils.getLabel(widget, "teamMemberCodeLabel"),
                    Messagebox.OK,
                    Messagebox.ERROR);
        }
    }

    /**
     * @return the targetCommerceCheckoutService
     */
    public TargetCommerceCheckoutService getTargetCommerceCheckoutService() {
        return targetCommerceCheckoutService;
    }

    /**
     * @param targetCommerceCheckoutService
     *            the targetCommerceCheckoutService to set
     */
    public void setTargetCommerceCheckoutService(final TargetCommerceCheckoutService targetCommerceCheckoutService) {
        this.targetCommerceCheckoutService = targetCommerceCheckoutService;
    }

}
