/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers.impl;

import de.hybris.platform.cscockpit.services.search.generic.query.DefaultTicketSearchQueryBuilder;
import de.hybris.platform.cscockpit.services.search.impl.DefaultCsTextSearchCommand;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import org.apache.commons.lang.StringUtils;


/**
 * The query builder for ticket searching
 * 
 */
public class TargetTicketSearchQueryBuilder extends DefaultTicketSearchQueryBuilder {

    /*
     * Ticket searching by assignedAgent uid/name, assignedGroup uid/locName, ticket state fields.
     * 
     *  (non-Javadoc)
     * @see de.hybris.platform.cscockpit.services.search.generic.query.DefaultTicketSearchQueryBuilder#buildFlexibleSearchQuery(de.hybris.platform.cscockpit.services.search.impl.DefaultCsTextSearchCommand)
     */
    @Override
    protected FlexibleSearchQuery buildFlexibleSearchQuery(final DefaultCsTextSearchCommand command) {

        final String text = command.getText(TextField.SearchText);

        final boolean isSearchByText = StringUtils.isNotBlank(text);

        final StringBuilder query = new StringBuilder(300);

        query.append("SELECT DISTINCT {t:pk}, {t:creationtime} ");
        query.append("FROM {CsTicket AS t ");

        if (isSearchByText) {
            query.append(" LEFT JOIN Employee AS e ON {t:assignedAgent} = {e:pk} ");
            query.append(" LEFT JOIN CsAgentGroup AS ag ON {t:assignedGroup} = {ag:pk} ");
            query.append(" LEFT JOIN CsTicketState AS ts ON {t:state} = {ts:pk}");
            query.append(" } ");

            query.append(" WHERE {e:uid} LIKE ?searchText");
            query.append(" OR {e:name} LIKE ?searchText");
            query.append(" OR {ag:uid} LIKE ?searchText");
            query.append(" OR {ag:locName} LIKE ?searchText");
            query.append(" OR {ts:code} LIKE ?searchText");
        }
        else {
            query.append(" } ");
        }

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());

        if (isSearchByText) {
            searchQuery.addQueryParameter("searchText", '%' + text.trim() + '%');
        }

        return searchQuery;
    }



}
