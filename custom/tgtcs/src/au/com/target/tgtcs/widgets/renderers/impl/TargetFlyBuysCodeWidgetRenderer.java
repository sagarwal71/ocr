package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.renderers.impl.AbstractCsWidgetRenderer;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import au.com.target.tgtcs.widgets.controllers.TargetCheckoutController;


/**
 * FlyBuys code widget on checkout tab renderer
 * 
 */
public class TargetFlyBuysCodeWidgetRenderer extends AbstractCsWidgetRenderer {

    private static final Logger LOG = Logger.getLogger(TargetFlyBuysCodeWidgetRenderer.class);

    private static final String CSS_ROW_COMPONENT = "tgtcs_sectionRowComponent";

    private static final String CSS_TEXT_EDITOR = "textEditor";

    @Override
    protected HtmlBasedComponent createContentInternal(
            final Widget widget,
            final HtmlBasedComponent rootContainer) {

        final Div content = new Div();
        content.setSclass(CSS_ROW_COMPONENT);
        final Hbox hbox = new Hbox();
        hbox.setParent(content);
        hbox.setWidth("96%");
        hbox.setWidths("9em, none");

        final Label flyBuysCodeLabel = new Label(LabelUtils.getLabel(widget, "flybuyscodeLabel"));
        flyBuysCodeLabel.setParent(hbox);

        final Textbox flyBuysCodeTextbox = new Textbox();
        flyBuysCodeTextbox.setParent(hbox);
        flyBuysCodeTextbox.setClass(CSS_TEXT_EDITOR);
        flyBuysCodeTextbox.setValue(getDefaultValue(widget));

        flyBuysCodeTextbox.addEventListener(Events.ON_BLUR, new EventListener() {
            @Override
            public void onEvent(final Event event) throws Exception {
                handleItemEvent(flyBuysCodeTextbox, widget);
            }
        });

        return content;
    }

    /**
     * Get fly buys code from check out or customer
     * 
     * @param widget
     *            fly buys code widget
     * @return String
     */
    private String getDefaultValue(final Widget widget) {
        try {
            final TargetCheckoutController controller = (TargetCheckoutController)widget.getWidgetController();
            return controller.preLoadFlyBuysCode();
        }
        catch (final ValueHandlerException ex) {
            try {
                Messagebox.show(ex.getMessage(), LabelUtils.getLabel(widget, "unableToSaveFlyBuysCode"), Messagebox.OK,
                        Messagebox.ERROR);
            }
            catch (final InterruptedException e) {
                LOG.debug("unable to save item", ex);
                Thread.currentThread().interrupt();
            }
        }
        return "";
    }

    /**
     * handle fly buys code text box field after user presses enter button
     * 
     * @param flyBuysCodeTextbox
     *            fly buys code text box control
     * @param widget
     *            fly buys code widget
     * @throws InterruptedException
     */
    private void handleItemEvent(final Textbox flyBuysCodeTextbox, final Widget widget) throws InterruptedException {
        try {
            final TargetCheckoutController controller = (TargetCheckoutController)widget.getWidgetController();
            controller.saveFlyBuysCode(flyBuysCodeTextbox.getValue());
        }
        catch (final ValueHandlerException ex) {
            Messagebox.show(ex.getMessage(), LabelUtils.getLabel(widget, "unableToSaveFlyBuysCode"), Messagebox.OK,
                    Messagebox.ERROR);
            LOG.debug("unable to save item", ex);
        }
    }

}
