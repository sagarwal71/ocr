/**
 * 
 */
package au.com.target.tgtcs.widgets.binding;

import de.hybris.platform.core.model.user.TitleModel;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Textbox;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel;


/**
 * @author Pavel_Yakushevich
 * 
 */
public class CnCPopupDataBinder extends AbstractTgtcsBinder<SelectCnCStoreWidgetModel>
{
    private final Combobox title;
    private final Textbox firstName;
    private final Textbox lastName;
    private final Textbox phone;
    private final Combobox state;
    private final Combobox store;


    /**
     * Constructor. Accepts <code>model</code> and a set of UI components to keep model in sync with.
     * 
     * @param model
     * @param title
     * @param firstName
     * @param lastName
     * @param phone
     * @param state
     * @param store
     */
    public CnCPopupDataBinder(final SelectCnCStoreWidgetModel model, final Combobox title, final Textbox firstName,
            final Textbox lastName,
            final Textbox phone, final Combobox state, final Combobox store) {
        super(model);

        this.title = title;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.state = state;
        this.store = store;
    }

    @Override
    public void loadAll()
    {
        setSelectedItem(title, getWidgetModel().getTitle());
        firstName.setValue(getWidgetModel().getFirstName());
        lastName.setValue(getWidgetModel().getLastName());
        phone.setValue(getWidgetModel().getPhone());
        setSelectedItem(state, getWidgetModel().getState());
        setSelectedItem(store, getWidgetModel().getStore());

        Events.postEvent(new SelectEvent(Events.ON_SELECT, state, Collections.singleton(state.getSelectedItem())));
    }


    @Override
    public void saveAll()
    {
        getWidgetModel().setTitle(getSelectedItemValueNullSafe(title, TitleModel.class));
        getWidgetModel().setFirstName(firstName.getValue());
        getWidgetModel().setLastName(lastName.getValue());
        getWidgetModel().setPhone(phone.getValue());
        getWidgetModel().setState(getSelectedItemValueNullSafe(state, String.class));
        getWidgetModel().setStore(getSelectedItemValueNullSafe(store, TargetPointOfServiceModel.class));
    }

    /**
     * @param clazz
     */
    private <T> T getSelectedItemValueNullSafe(final Combobox combobox, final Class<T> clazz)
    {
        if (combobox.getSelectedItem() != null)
        {
            return (T)combobox.getSelectedItem().getValue();
        }

        return null;
    }

    private void setSelectedItem(final Combobox combobox, final Object item)
    {
        final List<Comboitem> comboitems = combobox.getChildren();

        for (final Comboitem comboitem : comboitems)
        {
            if (ObjectUtils.equals(comboitem.getValue(), item))
            {
                combobox.setSelectedItem(comboitem);
                break;
            }
        }
    }
}
