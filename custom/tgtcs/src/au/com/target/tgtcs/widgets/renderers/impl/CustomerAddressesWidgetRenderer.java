package au.com.target.tgtcs.widgets.renderers.impl;


import de.hybris.platform.cockpit.widgets.impl.DefaultListboxWidget;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.cscockpit.widgets.controllers.CustomerController;
import de.hybris.platform.cscockpit.widgets.models.impl.CustomerAddressesListWidgetModel;

import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;

import au.com.target.tgtcore.model.TargetCustomerModel;



/**
 * @author Hlib_Mykhailenko
 * 
 */
public class CustomerAddressesWidgetRenderer extends
        de.hybris.platform.cscockpit.widgets.renderers.impl.CustomerAddressesWidgetRenderer {

    @Override
    protected HtmlBasedComponent createContentInternal(
            final DefaultListboxWidget<CustomerAddressesListWidgetModel, CustomerController> widget,
            final HtmlBasedComponent rootContainer) {
        final HtmlBasedComponent root = super.createContentInternal(widget, rootContainer);
        final Div div = getDivWithSClass(root, "createNewAddress");
        final Button addNewAddressButton = (Button)div.getChildren().get(0);
        addNewAddressButton.setDisabled(isGuestUser(widget));
        return root;
    }


    private boolean isGuestUser(final DefaultListboxWidget<CustomerAddressesListWidgetModel, CustomerController> widget) {
        final TargetCustomerModel customer = (TargetCustomerModel)getCustomer(widget);
        return customer != null && CustomerType.GUEST.equals(customer.getType());
    }


    private Div getDivWithSClass(final HtmlBasedComponent root, final String sClass) {
        for (final Object child : root.getChildren()) {
            if (child instanceof Div) {
                final Div div = (Div)child;
                if (sClass.equals(div.getSclass())) {
                    return div;
                }
            }
        }
        return null;
    }
}
