/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers.strategies;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.cscockpit.widgets.controllers.strategies.BasketStrategy;


/**
 * 
 */
public interface TargetBasketStrategy extends BasketStrategy {

    /**
     * get master cart by customer
     * 
     * @param customer
     * @return CartModel
     */
    public CartModel getPersistedMasterCart(final CustomerModel customer);

    /**
     * get cart by customer
     * 
     * @param customer
     * @return CartModel
     */
    public CartModel getCart(final CustomerModel customer);

    /**
     * Return cart base on master cart, master cart will be created if it is null
     * 
     * @param customer
     * @return CartModel
     */
    public CartModel createCart(final CustomerModel customer);

    /**
     * Creates a new cart by master cart for the specified customer with the cart code specified. Master cart with cart
     * code will be created if master cart with cart code is null.
     * 
     * @param customer
     * @param cartCode
     * @return TypedObject
     */
    public TypedObject loadCart(TypedObject customer, String cartCode);

    /**
     * 
     * @return the purchaseOptionCode
     */
    public String getPurchaseOptionCode();
}
