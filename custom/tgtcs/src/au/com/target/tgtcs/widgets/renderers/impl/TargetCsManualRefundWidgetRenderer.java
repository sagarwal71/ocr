/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.components.StyledDiv;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cockpit.widgets.models.WidgetModel;
import de.hybris.platform.cockpit.widgets.renderers.WidgetRenderer;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.renderers.utils.PopupWidgetHelper;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Required;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;

import au.com.target.tgtcore.ordercancel.data.IpgNewRefundInfoDTO;
import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtcs.util.TargetCsCockpitUtil;
import au.com.target.tgtcs.widgets.controllers.impl.TargetManualRefundWidgetControllerImpl;


/**
 * @author bhuang3
 *
 */
public class TargetCsManualRefundWidgetRenderer implements
        WidgetRenderer<Widget<WidgetModel, TargetManualRefundWidgetControllerImpl>> {

    private static final String WIDGET_CAPTION_CLASS = "csWidgetCaption";
    private static final String DAFAUTL_WIDTH = "200px";

    private PopupWidgetHelper popupWidgetHelper;
    private ModelService modelService;
    private TypeService typeService;

    @Override
    public HtmlBasedComponent createCaption(final Widget<WidgetModel, TargetManualRefundWidgetControllerImpl> widget) {
        final Div caption = new StyledDiv();
        caption.setSclass(WIDGET_CAPTION_CLASS);

        final Label captionLabel = new Label(Labels.getLabel(
                widget.getWidgetTitle(), widget.getWidgetTitle()));
        captionLabel.setParent(caption);

        return caption;

    }


    @Override
    public HtmlBasedComponent createContent(final Widget<WidgetModel, TargetManualRefundWidgetControllerImpl> widget) {
        final TargetManualRefundWidgetControllerImpl widgetController = widget.getWidgetController();
        final BigDecimal refundAmount = widgetController.findRefundAmount(widgetController.getSelectedOrder());
        final Div content = new Div();
        final Grid grid = new Grid();

        final Rows rows = new Rows();
        rows.appendChild(createRefundableAmount(refundAmount.toString()));
        rows.appendChild(createFormDoubleBox(TgtcsConstants.MANUAL_REFUND_AMOUNT, TgtcsConstants.AMOUNT,
                TgtcsConstants.LABEL_WIDTH,
                TgtcsConstants.EDITOR_MAX_LENGTH,
                true));
        rows.appendChild(createFormTextBox(TgtcsConstants.IPG_RECEIPT_NUMBER_LABEL,
                TgtcsConstants.IPG_RECEIPT_NUMBER, TgtcsConstants.LABEL_WIDTH, TgtcsConstants.MAX_LENGTH_TEXTFIELD));
        grid.appendChild(rows);
        grid.setParent(content);

        final Button confirmButton = new Button(TgtcsConstants.ADD_NEW_PAYMENT);
        confirmButton.addEventListener(Events.ON_CLICK, new EventListener() {

            @Override
            public void onEvent(final Event event) throws Exception {
                try {
                    TargetCsManualRefundWidgetRenderer.this.handleAddManualRefundClickEvent(widget);
                }
                catch (final Exception e) {
                    Messagebox.show(
                            e.getMessage()
                                    + ((e.getCause() == null) ? "" : new StringBuilder(" - ").append(
                                            e.getCause().getMessage())
                                            .toString()),
                            LabelUtils.getLabel(widget, "failed", new Object[0]), 1, "z-msgbox z-msgbox-error");
                }

            }
        });
        confirmButton.setParent(content);

        return content;
    }

    protected void handleAddManualRefundClickEvent(
            final Widget<WidgetModel, TargetManualRefundWidgetControllerImpl> widget) {
        final Doublebox amount = (Doublebox)widget.getFellow(TgtcsConstants.AMOUNT);
        final Textbox receipt = (Textbox)widget.getFellow(TgtcsConstants.IPG_RECEIPT_NUMBER);
        TargetCsCockpitUtil.validateManualRefundInput(amount.getValue(), receipt.getValue());
        final TargetManualRefundWidgetControllerImpl widgetController = widget.getWidgetController();
        final IpgNewRefundInfoDTO ipgNewRefundInfoDTO = new IpgNewRefundInfoDTO();
        ipgNewRefundInfoDTO.setAmount(amount.getValue());
        ipgNewRefundInfoDTO.setReceiptNo(receipt.getValue());
        widgetController.performIpgManualRefund(ipgNewRefundInfoDTO);
        final OrderModel model = widgetController.getSelectedOrder();
        modelService.refresh(model);
        if (model != null)
        {
            final TypedObject typedObject = typeService.wrapItem(model);
            widgetController.getCallContextController().setCurrentOrder(typedObject);
        }
        Events.postEvent(new Event(Events.ON_CLOSE, popupWidgetHelper.getCurrentPopup()));
        widget.getWidgetController().dispatchEvent(null, widget, null);

    }

    private Component createRefundableAmount(final String refundableAmount) {
        final Row row = new Row();

        final Label titleLabel = new Label(TgtcsConstants.REFUNDABLE_AMOUNT);
        titleLabel.setWidth(DAFAUTL_WIDTH);
        row.appendChild(titleLabel);

        final Label amount = new Label(refundableAmount);
        amount.setWidth(DAFAUTL_WIDTH);
        row.appendChild(amount);
        return row;
    }


    private Component createFormDoubleBox(final String labelText, final String fieldName, final String width,
            final int maxLength, final boolean visible) {
        final Row row = new Row();
        final Label label = new Label();
        label.setWidth(DAFAUTL_WIDTH);
        label.setValue(labelText);

        row.appendChild(label);

        final Doublebox doublebox = new Doublebox();
        doublebox.setSclass(TgtcsConstants.TEXT_EDITOR);
        doublebox.setName(fieldName);
        doublebox.setId(fieldName);
        doublebox.setWidth(width);
        doublebox.setMaxlength(maxLength);
        doublebox.setVisible(visible);

        row.appendChild(doublebox);
        return row;
    }

    private Component createFormTextBox(final String labelText, final String fieldName, final String width,
            final int maxLength) {
        final Row row = new Row();
        final Label label = new Label();
        label.setWidth(DAFAUTL_WIDTH);
        label.setValue(labelText);

        row.appendChild(label);

        final Textbox textbox = new Textbox();
        textbox.setSclass(TgtcsConstants.TEXT_EDITOR);
        textbox.setType(TgtcsConstants.TEXT);
        textbox.setWidth(width);
        textbox.setName(fieldName);
        textbox.setId(fieldName);
        textbox.setMaxlength(maxLength);
        row.appendChild(textbox);

        return row;
    }


    /**
     * @param popupWidgetHelper
     *            the popupWidgetHelper to set
     */
    @Required
    public void setPopupWidgetHelper(final PopupWidgetHelper popupWidgetHelper) {
        this.popupWidgetHelper = popupWidgetHelper;
    }


    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }


    /**
     * @param typeService
     *            the typeService to set
     */
    @Required
    public void setTypeService(final TypeService typeService) {
        this.typeService = typeService;
    }



}
