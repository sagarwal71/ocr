package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.config.ColumnGroupConfiguration;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.util.UITools;
import de.hybris.platform.cockpit.widgets.ListboxWidget;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cscockpit.model.data.DataObject;
import de.hybris.platform.cscockpit.services.search.impl.DefaultCsTextFacetSearchCommand;
import de.hybris.platform.cscockpit.utils.CssUtils;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.utils.SafeUnbox;
import de.hybris.platform.cscockpit.utils.TypeUtils;
import de.hybris.platform.cscockpit.widgets.controllers.search.SearchCommandController;
import de.hybris.platform.cscockpit.widgets.models.impl.SearchResultWidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.impl.BasketResultWidgetRenderer;
import de.hybris.platform.stock.exception.StockLevelNotFoundException;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Longbox;
import org.zkoss.zul.Messagebox;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


public class TargetBasketResultWidgetRenderer extends BasketResultWidgetRenderer<DefaultCsTextFacetSearchCommand> {

    protected static final String AVAILABLE_AND_MAX_QTY_KEY = "cscockpit.widget.basket.search.product.stockLevel.availableAndMaxQty";
    protected static final String NOT_DEFINED_KEY = "cscockpit.widget.basket.search.product.maxOrderQty.notDefined";
    protected static final String OUT_OF_STOCK_KEY = "cscockpit.widget.basket.search.product.stockLevel.outOfStock";

    private static final Logger LOG = Logger.getLogger(TargetBasketResultWidgetRenderer.class);

    private TargetStockService targetStockService;
    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;

    @Override
    protected boolean canProductBeAddedToCart(final DataObject metaItem) {
        final PurchaseOptionModel purchaseOption = targetPurchaseOptionHelper.getCurrentPurchaseOptionModel();
        return canProductBeAddedToCartByPurchaseOption(metaItem, purchaseOption);
    }

    protected boolean canProductBeAddedToCartByPurchaseOption(final DataObject metaItem,
            final PurchaseOptionModel purchaseOption) {
        final AbstractTargetVariantProductModel productModel = TypeUtils.unwrapItem((TypedObject)metaItem.getItem(),
                AbstractTargetVariantProductModel.class);
        final TargetProductModel baseProduct = getBaseTargetProduct(productModel);
        boolean available = false;
        if (baseProduct != null) {
            available = baseProduct.getPurchaseOptions().contains(purchaseOption);
        }
        if (getProductStockLevelAmount(productModel) > 0) {
            return available;
        }
        return false;
    }

    @Override
    protected String getProductMessage(final DataObject metaItem) {
        return getProductMessageByPurchaseOption(metaItem);
    }

    protected String getProductMessageByPurchaseOption(final DataObject metaItem) {
        final ProductModel product = (ProductModel)((TypedObject)metaItem.getItem()).getObject();
        final int stockLevel = getProductStockLevelAmount(product);
        if (stockLevel > 0) {
            String maxOrderQuantity = Labels.getLabel(NOT_DEFINED_KEY);
            if (product.getMaxOrderQuantity() != null) {
                maxOrderQuantity = product.getMaxOrderQuantity().toString();
            }
            return Labels.getLabel(AVAILABLE_AND_MAX_QTY_KEY, new Object[] { Integer.valueOf(stockLevel),
                    maxOrderQuantity });
        }
        return Labels.getLabel(OUT_OF_STOCK_KEY);
    }

    private TargetProductModel getBaseTargetProduct(final AbstractTargetVariantProductModel targetVariantProduct) {
        final ProductModel product = targetVariantProduct.getBaseProduct();

        if (product != null) {
            if (product instanceof TargetProductModel) {
                return (TargetProductModel)product;
            }
            else if (product instanceof AbstractTargetVariantProductModel) {
                return getBaseTargetProduct((AbstractTargetVariantProductModel)product);
            }
        }

        return null;
    }

    private int getProductStockLevelAmount(final ProductModel product) {
        int stockLevel;
        try {
            stockLevel = targetStockService.getStockLevelAmountFromOnlineWarehouses(product);
        }
        catch (final StockLevelNotFoundException e) {
            LOG.warn("Stock level not found", e);
            stockLevel = 0;
        }
        return stockLevel;
    }


    @Override
    protected void populateMasterRow(final ListboxWidget widget, final Listitem row, final RowContext context,
            final DataObject metaItem) {
        final TypedObject item = (TypedObject)metaItem.getItem();
        if (CollectionUtils.isNotEmpty(context.getColumns())) {
            ColumnGroupConfiguration col;
            Listcell cell;
            for (final Iterator iterator = context.getColumns().iterator(); iterator
                    .hasNext(); getPropertyRendererHelper().buildMasterValue(
                            item, col, cell)) {
                col = (ColumnGroupConfiguration)iterator.next();
                cell = new Listcell();
                cell.setSclass("csMasterContentCell");
                row.appendChild(cell);
            }

        }
        row.appendChild(new Listcell(
                formatProductPrice(getProductPriceValue(metaItem))));
        final List potentialProductPromotions = getPotentialProductPromotions(metaItem);
        if (potentialProductPromotions != null
                && !potentialProductPromotions.isEmpty()) {
            row.setSclass(CssUtils.combine(new String[] { row.getSclass(),
                    "hasPromotions" }));
        }
        final boolean canBeAddToByNowCart = canProductBeAddedToCart(metaItem);
        if (!(canBeAddToByNowCart)) {
            final Listcell qtyCell = new Listcell();
            row.appendChild(qtyCell);
            final Listcell actionCell = new Listcell();
            row.appendChild(actionCell);
            final String productMessage = getProductMessage(metaItem);
            if (productMessage != null && productMessage.length() > 0) {
                final Div stockDiv = new Div();
                stockDiv.setSclass("csProductMessage");
                final Label stockMessageLabel = new Label(productMessage);
                stockDiv.appendChild(stockMessageLabel);
                actionCell.appendChild(stockDiv);
            }
            context.setAddToBasketListener(null);
        }
        else {
            final Listcell qtyCell = new Listcell();

            final Longbox quantityInput = new Longbox(1);
            quantityInput.setWidth("33px");
            quantityInput.setParent(qtyCell);
            row.appendChild(qtyCell);

            final Listcell actionCell = new Listcell();

            final Button addToBuyNowCartButton = createActionButton(
                    widget, "addToBuyNowBasketBtn", "btngreen", actionCell);
            addToBuyNowCartButton.setVisible(canBeAddToByNowCart);

            final String productAvailabilityMessage = getProductMessage(metaItem);
            createProductMessageBlock(productAvailabilityMessage, actionCell);

            if (UISessionUtils.getCurrentSession().isUsingTestIDs()) {
                final ProductModel productModel = (item.getObject() instanceof ProductModel) ? (ProductModel)item
                        .getObject() : null;
                final String itemCode = productModel != null ? productModel.getCode()
                        : "unknown";
                UITools.applyTestID(quantityInput,
                        (new StringBuilder(
                                "Cart_SearchResult_Product_Quantity_input_"))
                                        .append(itemCode).toString());
                UITools.applyTestID(addToBuyNowCartButton,
                        (new StringBuilder(
                                "Cart_SearchResult_Product_Actions_input_"))
                                        .append(itemCode).toString());
            }
            final EventListener addToBasketListener = createAddToBasketEventListener(
                    widget, item, quantityInput);

            context.setAddToBasketListener(addToBasketListener);
            addToBuyNowCartButton.addEventListener("onClick", addToBasketListener);
            row.appendChild(actionCell);
        }
    }

    protected void createProductMessageBlock(final String productMessage, final Component parent) {
        if (productMessage != null && productMessage.length() > 0) {
            final Div stockDiv = new Div();
            stockDiv.setSclass("csProductMessage");
            final Label stockMessageLabel = new Label(productMessage);
            stockDiv.appendChild(stockMessageLabel);
            parent.appendChild(stockDiv);
        }
    }

    protected Button createActionButton(final ListboxWidget widget, final String i18nPosfix, final String ccsClass,
            final Component parent) {
        final Button actionButton = new Button(LabelUtils.getLabel(widget,
                i18nPosfix));
        actionButton.setParent(parent);
        actionButton.setSclass(ccsClass);
        return actionButton;
    }


    @Override
    protected void handleAddToBasketEvent(final ListboxWidget widget, final Event event, final TypedObject item,
            final Longbox qtyInput) {
        final long quantityToAdd = SafeUnbox.toLong(qtyInput.getValue());
        if (quantityToAdd <= 0L) {
            return;
        }
        else {
            final Button actionButton = (Button)event.getTarget();
            try {
                //buy now
                if (LabelUtils.getLabel(widget, "addToBuyNowBasketBtn").equals(actionButton.getLabel())) {
                    getItemAppender().add(item, quantityToAdd);
                }
            }
            catch (final Exception ex) {
                try {
                    Messagebox.show(ex.getMessage(), LabelUtils.getLabel(widget, "failedToAdd"), Messagebox.OK,
                            Messagebox.ERROR);
                }
                catch (final InterruptedException e) {
                    LOG.warn("Show message box exception", e);
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    @Override
    protected RowContext populateHeaderRow(
            final ListboxWidget<SearchResultWidgetModel, SearchCommandController<DefaultCsTextFacetSearchCommand>> widget,
            final Listhead row) {
        final RowContext result = super.populateHeaderRow(widget, row);
        final Listheader listheader = (Listheader)row.getLastChild();
        listheader.setWidth("20%");
        return result;
    }

    /**
     * @param targetPurchaseOptionHelper
     *            the targetPurchaseOptionHelper to set
     */
    @Required
    public void setTargetPurchaseOptionHelper(final TargetPurchaseOptionHelper targetPurchaseOptionHelper) {
        this.targetPurchaseOptionHelper = targetPurchaseOptionHelper;
    }

    /**
     * @param targetStockService
     *            the targetStockService to set
     */
    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }

}
