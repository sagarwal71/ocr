/**
 * 
 */
package au.com.target.tgtcs.widgets.listener;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.util.mail.MailUtils;

import org.apache.commons.mail.EmailException;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.impl.InputElement;

import au.com.target.tgtbusproc.TargetBusinessProcessService;


/**
 *
 */
public class ResendTaxInvoiceEventListener implements EventListener {

    protected static final String CONFIRM_POPUP_TITLE_KEY = "cockpit.order.taxinvoice.confirmPopup.title";
    protected static final String TAX_INVOICE_SUCCESS_SENDING_KEY = "cockpit.order.taxinvoice.send.success";
    protected static final String BUSINESS_PROCESS_SERVICE_ID = "targetBusinessProcessService";
    protected static final String CONFIRM_POPUP_MESSAGE_KEY = "cockpit.order.taxinvoice.confirmPopup.message";
    protected static final String INVALID_EMAIL_MESSAGE_KEY = "cockpit.order.taxinvoice.validation.emailNotValid";
    private static final String ON_YES = "onYes";

    private final OrderModel order;
    private final InputElement emailAddressInput;
    private TargetBusinessProcessService targetBusinessProcessService;

    /**
     * Class constructor
     * 
     * @param order
     *            the OrderModel
     * @param emailAddressInput
     *            the emailAddressInput
     */
    public ResendTaxInvoiceEventListener(final OrderModel order, final InputElement emailAddressInput) {
        this.order = order;
        this.emailAddressInput = emailAddressInput;
    }

    /* (non-Javadoc)
     * @see org.zkoss.zk.ui.event.EventListener#onEvent(org.zkoss.zk.ui.event.Event)
     */
    @Override
    public void onEvent(final Event e) throws Exception {
        final String email = emailAddressInput.getText();
        Messagebox.show(Labels.getLabel(CONFIRM_POPUP_MESSAGE_KEY, new Object[] { email }),
                Labels.getLabel(CONFIRM_POPUP_TITLE_KEY), Messagebox.YES | Messagebox.NO,
                Messagebox.INFORMATION, new EventListener() {
                    @Override
                    public void onEvent(final Event event) throws Exception {
                        if (event.getName().equals(ON_YES)) {
                            handleSendTaxInvoice(email);
                        }
                    }

                });
    }

    /**
     * Handle sending tax invoice
     * 
     * @param email
     *            the email
     * @throws Exception
     */
    protected void handleSendTaxInvoice(final String email) throws Exception {
        try {
            MailUtils.validateEmailAddress(email, "customer email");
            getTargetBusinessProcessService().startResendTaxInvoiceProcess(order, email);
        }
        catch (final EmailException e) {
            Messagebox.show(Labels.getLabel(INVALID_EMAIL_MESSAGE_KEY),
                    Labels.getLabel(CONFIRM_POPUP_TITLE_KEY), Messagebox.YES,
                    Messagebox.ERROR);
            return;
        }
        catch (final Exception e) {
            Messagebox.show(e.getMessage(),
                    Labels.getLabel(CONFIRM_POPUP_TITLE_KEY), Messagebox.YES,
                    Messagebox.ERROR);
            return;
        }
        Messagebox.show(Labels.getLabel(TAX_INVOICE_SUCCESS_SENDING_KEY, new Object[] { email }),
                Labels.getLabel(CONFIRM_POPUP_TITLE_KEY), Messagebox.YES,
                Messagebox.INFORMATION);

    }

    /**
     * @return the targetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        if (targetBusinessProcessService == null) {
            this.targetBusinessProcessService = (TargetBusinessProcessService)Registry.getApplicationContext()
                    .getBean(BUSINESS_PROCESS_SERVICE_ID);
        }
        return targetBusinessProcessService;
    }

    /**
     * @param targetBusinessProcessService
     *            the targetBusinessProcessService to set
     */
    public void setTargetBusinessProcessService(final TargetBusinessProcessService targetBusinessProcessService) {
        this.targetBusinessProcessService = targetBusinessProcessService;
    }






}
