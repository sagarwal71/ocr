package au.com.target.tgtcs.widgets.adapters;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.cscockpit.widgets.models.impl.ShippingCartAddressWidgetModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcs.widgets.models.impl.TargetShippingCartAddressWidgetModel;


public class TargetCartShippingAddressAdapter extends TargetCartAddressAdapter {

    private DeliveryService deliveryService;

    protected DeliveryService getDeliveryService()
    {
        return deliveryService;
    }

    @Required
    public void setDeliveryService(final DeliveryService deliveryService)
    {
        this.deliveryService = deliveryService;
    }

    @Override
    protected boolean updateModel()
    {


        final CartModel cartModel = getCartModel();
        boolean changed = updateValidShippingAddress(cartModel);
        changed |= ((ShippingCartAddressWidgetModel)getWidgetModel()).setSelectedShippingAddress(UISessionUtils
                .getCurrentSession().getTypeService().wrapItem(cartModel.getDeliveryAddress()));

        final TargetDeliveryService targetDeliveryService = (TargetDeliveryService)getDeliveryService();
        final List<DeliveryModeModel> deliveryModes = targetDeliveryService
                .getSupportedDeliveryModeListForOrder(cartModel);

        final List<String> supportedDeliveryModesForCart = new ArrayList<>();
        final Map<String, Double> map = new HashedMap();
        for (final DeliveryModeModel mode : deliveryModes) {
            try {
                map.put(mode.getCode(),
                        Double.valueOf(targetDeliveryService.getDeliveryCostForDeliveryType(
                                (AbstractTargetZoneDeliveryModeValueModel)targetDeliveryService
                                        .getZoneDeliveryModeValueForAbstractOrderAndMode((ZoneDeliveryModeModel)mode,
                                                cartModel),
                                cartModel).getDeliveryCost()));
                supportedDeliveryModesForCart.add(mode.getCode());
            }
            catch (final TargetNoPostCodeException ex) {
                map.put(mode.getCode(), null);
                supportedDeliveryModesForCart.add(mode.getCode());
            }

        }

        changed |= ((TargetShippingCartAddressWidgetModel)getWidgetModel()).setDeliveryCost(map);

        changed |= ((TargetShippingCartAddressWidgetModel)getWidgetModel())
                .setSupportedDeliveryModesForOrder(supportedDeliveryModesForCart);
        return changed;
    }

    /**
     * This method populates the valid shipping address in the model for displaying in cs cockpit.
     * 
     * @param cartModel
     * @return changed
     */
    protected boolean updateValidShippingAddress(final CartModel cartModel) {
        boolean changed = false;

        final TargetDeliveryService targetDeliveryService = (TargetDeliveryService)getDeliveryService();
        final Collection<AddressModel> addresses = getAvailableAddressesForCart(cartModel);
        final Collection<AddressModel> validAddresses = new ArrayList<>();
        for (final AddressModel addressModel : addresses) {
            try {
                if (targetDeliveryService.isDeliveryModePostCodeCombinationValid(cartModel.getDeliveryMode(),
                        addressModel, cartModel)) {
                    validAddresses.add(addressModel);
                }
            }
            catch (final TargetNoPostCodeException e) {
                // Since TargetNoPostCodeException will be thrown only by home-delivery values and for home-delivery we support all postcode
                validAddresses.add(addressModel);
            }
        }

        final List<TypedObject> addressObjects = getCockpitTypeService().wrapItems(validAddresses);

        changed |= getWidgetModel().setItems(addressObjects);

        changed |= getWidgetModel().setCart(getCockpitTypeService().wrapItem(cartModel));

        return changed;
    }

    @Override
    protected Collection<AddressModel> getAvailableAddressesForCart(final CartModel cart)
    {
        return getDeliveryService().getSupportedDeliveryAddressesForOrder(cart, false);
    }



}
