/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.cockpit.widgets.InputWidget;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.controllers.dispatcher.ItemAppender;
import de.hybris.platform.cscockpit.widgets.models.impl.DefaultMasterDetailListWidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.impl.AbstractCreateItemWidgetRenderer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Messagebox;

import au.com.target.tgtcs.widgets.controllers.TargetCustomerCreateController;


/**
 * Create new customer editor widget renderer
 * 
 */
public class TargetCustomerCreateWidgetRenderer
        extends
        AbstractCreateItemWidgetRenderer<InputWidget<DefaultMasterDetailListWidgetModel<TypedObject>, TargetCustomerCreateController>> {

    private static final Logger LOG = Logger.getLogger(TargetCustomerCreateWidgetRenderer.class);

    private static final String CSS_ACTION_CONTAINER = "actionContainer";
    private ItemAppender<TypedObject> itemAppender;

    /* (non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.renderers.impl.AbstractCsWidgetRenderer#createContentInternal(de.hybris.platform.cockpit.widgets.Widget, org.zkoss.zk.ui.api.HtmlBasedComponent)
     */
    @Override
    protected HtmlBasedComponent createContentInternal(
            final InputWidget<DefaultMasterDetailListWidgetModel<TypedObject>, TargetCustomerCreateController> widget,
            final HtmlBasedComponent rootContainer) {
        final Div content = new Div();

        final ObjectValueContainer customerObjectValueContainer;

        try {
            if (StringUtils.isNotBlank(getEditorConfigurationCode())
                    && StringUtils.isNotBlank(getEditorConfigurationType())) {
                final Div customerEditorContent = new Div();
                customerEditorContent.setParent(content);
                customerObjectValueContainer = loadAndCreateEditors(widget, customerEditorContent, widget
                        .getWidgetController().getAutoFilledPropertyQualifiers());

                final Div itemCreationContent = new Div();
                itemCreationContent.setParent(content);

                final Div actionContainer = new Div();
                actionContainer.setSclass(CSS_ACTION_CONTAINER);
                actionContainer.setParent(content);

                final Button createButton = new Button(LabelUtils.getLabel(widget, "createButton"));
                createButton.setParent(actionContainer);

                createButton.addEventListener(
                        Events.ON_CLICK,
                        createCreateItemEventListener(widget, customerObjectValueContainer,
                                getEditorConfigurationType()));

            }
            else {
                LOG.error("Can not load editor widget configuration. Reason: No configuration code specified.");
            }
        }
        catch (final ValueHandlerException e) {
            LOG.error("unable to render new customer creation widget", e);
        }

        return content;
    }

    /**
     * create item event listener
     * 
     * @param widget
     *            create customer widget
     * @param customerObjectValueContainer
     *            customer value container
     * @param customerConfigurationTypeCode
     *            TargetCustomer
     * @return EventListener
     */
    private EventListener createCreateItemEventListener(
            final InputWidget<DefaultMasterDetailListWidgetModel<TypedObject>, TargetCustomerCreateController> widget,
            final ObjectValueContainer customerObjectValueContainer, final String customerConfigurationTypeCode) {

        return new CreateItemEventListener(widget, customerObjectValueContainer, customerConfigurationTypeCode);
    }

    private class CreateItemEventListener implements EventListener {

        private final InputWidget<DefaultMasterDetailListWidgetModel<TypedObject>, TargetCustomerCreateController> widget;
        private final ObjectValueContainer customerObjectValueContainer;
        private final String customerConfigurationTypeCode;

        /**
         * create event listener for customer creating button
         * 
         * @param widget
         *            create customer widget
         * @param customerObjectValueContainer
         *            customer value container
         * @param customerConfigurationTypeCode
         *            TargetCustomer
         */
        public CreateItemEventListener(
                final InputWidget<DefaultMasterDetailListWidgetModel<TypedObject>, TargetCustomerCreateController> widget,
                final ObjectValueContainer customerObjectValueContainer, final String customerConfigurationTypeCode) {

            this.widget = widget;
            this.customerObjectValueContainer = customerObjectValueContainer;
            this.customerConfigurationTypeCode = customerConfigurationTypeCode;
        }

        @Override
        public void onEvent(final Event event) throws InterruptedException {
            handleCreateItemEvent(widget, customerObjectValueContainer, customerConfigurationTypeCode);
        }
    }

    /**
     * create new customer when user clicks on Create button
     * 
     * @param widget
     *            create customer widget
     * @param customerObjectValueContainer
     *            customer value container
     * @param customerConfigurationTypeCode
     *            TargetCustomer
     * @throws InterruptedException
     *             can not create a new customer
     */
    private void handleCreateItemEvent(
            final InputWidget<DefaultMasterDetailListWidgetModel<TypedObject>, TargetCustomerCreateController> widget,
            final ObjectValueContainer customerObjectValueContainer, final String customerConfigurationTypeCode)
            throws InterruptedException {

        try {
            final TypedObject customerItem = widget.getWidgetController().createNewCustomer(
                    customerObjectValueContainer, customerConfigurationTypeCode);

            if (customerItem != null) {
                getItemAppender().add(customerItem, 1);
            }
        }
        catch (final ValueHandlerException ex) {
            Messagebox.show(ex.getMessage(), LabelUtils.getLabel(widget, "unableToCreateCustomer"), Messagebox.OK,
                    Messagebox.ERROR);
            LOG.error("unable to create item" + ex.getMessage());
        }
        catch (final DuplicateUidException ex) {
            Messagebox.show(ex.getMessage(), LabelUtils.getLabel(widget, "unableToCreateCustomer"), Messagebox.OK,
                    Messagebox.ERROR);
            LOG.error("unable to create item" + ex.getMessage());
        }

    }


    /**
     * @return the itemAppender
     */
    public ItemAppender<TypedObject> getItemAppender() {
        return itemAppender;
    }

    /**
     * @param itemAppender
     *            the itemAppender to set
     */
    public void setItemAppender(final ItemAppender<TypedObject> itemAppender) {
        this.itemAppender = itemAppender;
    }

}
