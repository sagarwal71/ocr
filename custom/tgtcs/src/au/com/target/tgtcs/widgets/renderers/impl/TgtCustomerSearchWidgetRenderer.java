package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.util.UITools;
import de.hybris.platform.cockpit.widgets.ListboxWidget;
import de.hybris.platform.cscockpit.services.search.generic.query.DefaultCustomerSearchQueryBuilder;
import de.hybris.platform.cscockpit.services.search.impl.DefaultCsTextSearchCommand;
import de.hybris.platform.cscockpit.widgets.controllers.search.SearchCommandController;
import de.hybris.platform.cscockpit.widgets.models.impl.TextSearchWidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.impl.AbstractSearcherWidgetRenderer;
import de.hybris.platform.cscockpit.widgets.renderers.impl.CustomerSearchWidgetRenderer;
import de.hybris.platform.util.localization.Localization;

import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.impl.InputElement;


/**
 * Search customer
 */
public class TgtCustomerSearchWidgetRenderer extends CustomerSearchWidgetRenderer {

    /* (non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.renderers.impl.CustomerSearchWidgetRenderer#createSearchPane(de.hybris.platform.cockpit.widgets.ListboxWidget)
     */
    @Override
    protected HtmlBasedComponent createSearchPane(
            final ListboxWidget<TextSearchWidgetModel, SearchCommandController<DefaultCsTextSearchCommand>> widget) {
        final Div searchPane = new Div();

        final Textbox searchInput = createNameField(widget, searchPane);
        final Button searchBtn = createSearchButton(widget, searchPane);

        attachSearchEventListener(widget, createSearchEventListener(widget, searchInput), new AbstractComponent[] {
                searchInput,
                searchBtn });

        return searchPane;
    }

    /**
     * create search event listener
     * 
     * @param widget
     *            search widget
     * @param searchInput
     *            search input control
     * @return search event listener
     */
    private EventListener createSearchEventListener(
            final ListboxWidget<TextSearchWidgetModel, SearchCommandController<DefaultCsTextSearchCommand>> widget,
            final InputElement searchInput) {

        return new TgtSearchEventListener(widget, searchInput);

    }

    @Override
    protected Textbox createSearchTextField(
            final ListboxWidget<TextSearchWidgetModel, SearchCommandController<DefaultCsTextSearchCommand>> widget,
            final Div parent, final String labelKey, final String currentValue, final String testId) {

        final Label fieldLabel = new Label(Localization.getLocalizedString("tgtcscockpit.widget.customer.search.name"));

        final Textbox fieldTextBox = new Textbox(currentValue != null ? currentValue : "");
        if ((UISessionUtils.getCurrentSession().isUsingTestIDs()) && (testId != null)) {
            UITools.applyTestID(fieldTextBox, testId);
        }

        parent.appendChild(fieldLabel);
        parent.appendChild(fieldTextBox);

        return fieldTextBox;
    }

    /**
     * search event listener, paging listener on customer searching popup
     */
    private class TgtSearchEventListener extends AbstractSearcherWidgetRenderer.AbstractSearchEventListener {

        private final InputElement searchInput;

        /**
         * @param widget
         * @param searchInput
         */
        private TgtSearchEventListener(
                final ListboxWidget<TextSearchWidgetModel, SearchCommandController<DefaultCsTextSearchCommand>> widget,
                final InputElement searchInput) {
            super(widget);
            this.searchInput = searchInput;
        }

        @Override
        protected void fillSearchCommand(final DefaultCsTextSearchCommand command) {

            if (searchInput != null) {
                command.setText(DefaultCustomerSearchQueryBuilder.TextField.Name, searchInput.getText());
            }
        }

    }
}
