/**
 *
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.components.StyledDiv;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cockpit.widgets.models.WidgetModel;
import de.hybris.platform.cockpit.widgets.renderers.WidgetRenderer;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.widgets.renderers.utils.PopupWidgetHelper;
import de.hybris.platform.servicelayer.model.ModelService;

import org.springframework.beans.factory.annotation.Required;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;

import au.com.target.tgtcs.widgets.controllers.impl.TargetAbstractFraudStatusWidgetController;


/**
 * @author mjanarth
 *
 */
public class TargetCsChangeFraudStatusWidgetRenderer implements
        WidgetRenderer<Widget<WidgetModel, TargetAbstractFraudStatusWidgetController>> {

    private static final String NOTEID = "noteid";
    private static final String DEFAULT_PADDING_FOR_NOTES = "padding : 3px;";
    private static final String DAFAUTL_WIDTH = "200px";
    private static final String FRAUDSTATUS_POPUP_PREFIX = "tgtcscockpit.widget.orderdetail.fraudstatus.popup";
    private static final String FRAUDSTATUS_POPUP_NOTES = FRAUDSTATUS_POPUP_PREFIX + ".notes";
    private static final String FRAUDSTATUS_POPUP_REVIEW_STATUS = FRAUDSTATUS_POPUP_PREFIX + ".reviewStatus";
    private static final String FRAUDSTATUS_POPUP_ORDER_NUMBER = FRAUDSTATUS_POPUP_PREFIX + ".orderNumber";
    private static final String FRAUDSTATUS_POPUP_CONFIRM = FRAUDSTATUS_POPUP_PREFIX + ".confirm";
    private static final String WIDGET_CAPTION_CLASS = "csWidgetCaption";

    private String newStatus;
    private PopupWidgetHelper popupWidgetHelper;
    private ModelService modelService;
    private TypeService typeService;

    @Override
    public HtmlBasedComponent createCaption(
            final Widget<WidgetModel, TargetAbstractFraudStatusWidgetController> widget) {
        final Div caption = new StyledDiv();
        caption.setSclass(WIDGET_CAPTION_CLASS);

        final Label captionLabel = new Label(Labels.getLabel(
                widget.getWidgetTitle(), widget.getWidgetTitle()));
        captionLabel.setParent(caption);

        return caption;
    }

    @Override
    public HtmlBasedComponent createContent(
            final Widget<WidgetModel, TargetAbstractFraudStatusWidgetController> widget) {
        final Div content = new Div();
        final Grid grid = new Grid();

        final Rows rows = new Rows();
        rows.appendChild(createOrderNumber(widget));
        rows.appendChild(createReviewStatus());
        rows.appendChild(createNotes());
        grid.appendChild(rows);
        grid.setParent(content);

        final Button confirmButton = new Button(Labels.getLabel(FRAUDSTATUS_POPUP_CONFIRM,
                FRAUDSTATUS_POPUP_CONFIRM));
        confirmButton.addEventListener(Events.ON_CLICK, new EventListener() {

            @Override
            public void onEvent(final Event paramEvent) throws Exception {
                TargetCsChangeFraudStatusWidgetRenderer.this.handleConfirmOrderStatusClickEvent(widget, paramEvent);

            }
        });
        confirmButton.setParent(content);

        return content;
    }

    /**
     * Handle confirmClickEvent: redirect handling to widget controller.
     *
     * @param widget
     *            The Widget
     * @param event
     *            The click event
     */
    protected void handleConfirmOrderStatusClickEvent(
            final Widget<WidgetModel, TargetAbstractFraudStatusWidgetController> widget,
            final Event event) {
        final Textbox note = (Textbox)widget.getFellow(NOTEID);
        final TargetAbstractFraudStatusWidgetController widgetController = widget.getWidgetController();
        final OrderModel model = widgetController.changeOrder(note.getValue());
        modelService.refresh(model);
        if (model != null)
        {
            final TypedObject typedObject = typeService.wrapItem(model);
            widgetController.getCallContextController().setCurrentOrder(typedObject);
        }
        Events.postEvent(new Event(Events.ON_CLOSE, popupWidgetHelper.getCurrentPopup()));
        widget.getWidgetController().dispatchEvent(null, widget, null);

    }

    /**
     *
     * @param widget
     *            The widget
     * @return row
     */
    protected Row createOrderNumber(final Widget<WidgetModel, TargetAbstractFraudStatusWidgetController> widget) {
        final Row row = new Row();

        final Label titleLabel = new Label(Labels.getLabel(FRAUDSTATUS_POPUP_ORDER_NUMBER,
                FRAUDSTATUS_POPUP_ORDER_NUMBER));
        titleLabel.setWidth(DAFAUTL_WIDTH);
        row.appendChild(titleLabel);

        final Label orderNumberLabel = new Label(widget.getWidgetController().getOrderCode());
        orderNumberLabel.setWidth(DAFAUTL_WIDTH);
        row.appendChild(orderNumberLabel);

        return row;
    }

    /**
     *
     * @return row
     */
    private Component createReviewStatus() {
        final Row row = new Row();

        final Label titleLabel = new Label(Labels.getLabel(FRAUDSTATUS_POPUP_REVIEW_STATUS,
                FRAUDSTATUS_POPUP_REVIEW_STATUS));
        titleLabel.setWidth(DAFAUTL_WIDTH);
        row.appendChild(titleLabel);

        final Label status = new Label(newStatus);
        status.setWidth(DAFAUTL_WIDTH);
        row.appendChild(status);

        return row;
    }

    /**
     *
     * @return row
     */
    private Component createNotes() {
        final Row row = new Row();

        final Label label = new Label(Labels.getLabel(FRAUDSTATUS_POPUP_NOTES,
                FRAUDSTATUS_POPUP_NOTES));
        label.setWidth(DAFAUTL_WIDTH);
        row.appendChild(label);

        final Textbox input = new Textbox();
        input.setWidth(DAFAUTL_WIDTH);
        input.setStyle(DEFAULT_PADDING_FOR_NOTES);
        input.setId(NOTEID);
        row.appendChild(input);

        return row;
    }

    /**
     *
     * @param popupWidgetHelper
     *            The popupWidgetHelper
     */
    @Required
    public void setPopupWidgetHelper(final PopupWidgetHelper popupWidgetHelper) {
        this.popupWidgetHelper = popupWidgetHelper;
    }


    /**
     *
     * @param modelService
     *            The modelService
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     *
     * @return modelService The modelService
     */
    @Required
    public ModelService getModelService() {
        return modelService;
    }


    /**
     * @param newStatus
     *            the newStatus to set
     */
    @Required
    public void setNewStatus(final String newStatus) {
        this.newStatus = newStatus;
    }


    /**
     * @param typeService
     *            the typeService to set
     */
    @Required
    public void setTypeService(final TypeService typeService) {
        this.typeService = typeService;
    }

}
