package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.widgets.InputWidget;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.controllers.BasketController;
import de.hybris.platform.cscockpit.widgets.models.impl.BasketCartWidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.impl.BasketTotalsWidgetRenderer;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;

import java.text.NumberFormat;

import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zul.Div;

import au.com.target.tgtcore.order.TargetDiscountService;
import au.com.target.tgtcs.widgets.controllers.TargetBasketController;
import au.com.target.tgtcs.widgets.controllers.TargetCheckoutController;


public class TargetBasketTotalsWidgetRenderer extends BasketTotalsWidgetRenderer {
    private TargetDiscountService targetDiscountService;

    @Override
    protected void renderOrderDetail(final InputWidget<BasketCartWidgetModel, BasketController> widget,
            final TypedObject order, final HtmlBasedComponent parent)
    {

        final Div container = new Div();
        container.setSclass("csOrderTotals");

        if ((order != null) && ((order.getObject() instanceof AbstractOrderModel)))
        {

            final AbstractOrderModel abstractOrderModel = (AbstractOrderModel)order.getObject();
            final CurrencyModel cartCurrencyModel = abstractOrderModel.getCurrency();
            final NumberFormat currencyInstance = (NumberFormat)getSessionService().executeInLocalView(
                    new SessionExecutionBody()
                    {
                        @Override
                        public Object execute()
                        {
                            getCommonI18NService().setCurrentCurrency(cartCurrencyModel);
                            return getFormatFactory().createCurrencyFormat();
                        }
                    });

            synCarts(widget, abstractOrderModel);

            final Double subtotal = abstractOrderModel.getSubtotal();
            renderRow(subtotal, LabelUtils.getLabel(widget, "subtotal", new Object[0]), currencyInstance, container);

            final Double taxes = abstractOrderModel.getTotalTax();
            renderRow(taxes, LabelUtils.getLabel(widget, "taxes", new Object[0]), currencyInstance, container);

            final Double paymentCosts = abstractOrderModel.getPaymentCost();
            renderRow(paymentCosts, LabelUtils.getLabel(widget, "paymentCosts", new Object[0]), currencyInstance,
                    container);

            final Double deliveryCosts = abstractOrderModel.getDeliveryCost();

            renderRow(deliveryCosts, LabelUtils.getLabel(widget, "deliveryCosts", new Object[0]), currencyInstance,
                    container);

            final double discounts = abstractOrderModel.getTotalDiscounts().doubleValue()
                    + targetDiscountService.getTotalTMDiscount(abstractOrderModel);
            renderRow(Double.valueOf(discounts), LabelUtils.getLabel(widget, "discounts", new Object[0]),
                    currencyInstance, container);

            final Double totalPrice = abstractOrderModel.getTotalPrice();
            renderRow(totalPrice, LabelUtils.getLabel(widget, "totalPrice", new Object[0]),
                    currencyInstance, container);

        }

        container.setParent(parent);
    }

    /**
     * Copy values from master card to current order
     * 
     * @param widget
     *            widget for BasketCartWidgetModel & BasketController
     * @param abstractOrderModel
     *            current order
     * 
     */
    private void synCarts(final InputWidget<BasketCartWidgetModel, BasketController> widget,
            final AbstractOrderModel abstractOrderModel) {

        final CartModel masterCartModel = ((TargetCheckoutController)((TargetBasketController)widget
                .getWidgetController())
                .getCheckoutController()).getCheckoutCartByMasterCart();
        abstractOrderModel.setDeliveryMode(masterCartModel.getDeliveryMode());
        abstractOrderModel.setZoneDeliveryModeValue(masterCartModel.getZoneDeliveryModeValue());
        abstractOrderModel.setSubtotal(masterCartModel.getSubtotal());
        abstractOrderModel.setTotalTax(masterCartModel.getTotalTax());
        abstractOrderModel.setDeliveryCost(masterCartModel.getDeliveryCost());
        abstractOrderModel.setPaymentCost(masterCartModel.getPaymentCost());
        abstractOrderModel.setTotalDiscounts(masterCartModel.getTotalDiscounts());
        abstractOrderModel.setTotalPrice(masterCartModel.getTotalPrice());
    }

    /**
     * @param targetDiscountService
     *            the targetDiscountService to set
     */
    @Required
    public void setTargetDiscountService(final TargetDiscountService targetDiscountService) {
        this.targetDiscountService = targetDiscountService;
    }
}