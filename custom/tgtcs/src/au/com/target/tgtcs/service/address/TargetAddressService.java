package au.com.target.tgtcs.service.address;

import au.com.target.tgtcore.model.TargetAddressModel;


public interface TargetAddressService {

    /**
     * Use model service to set one address is validate or not
     * 
     * @param addressModel
     * @param validated
     */
    public void setAddressValidated(final TargetAddressModel addressModel, final Boolean validated);
}
