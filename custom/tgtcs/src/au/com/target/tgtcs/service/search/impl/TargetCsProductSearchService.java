package au.com.target.tgtcs.service.search.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.commerceservices.search.ProductSearchService;
import de.hybris.platform.commerceservices.search.facetdata.FacetSearchPageData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SortData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cscockpit.services.search.CsFacetSearchResult;
import de.hybris.platform.cscockpit.services.search.CsFacetSearchService;
import de.hybris.platform.cscockpit.services.search.CsSort;
import de.hybris.platform.cscockpit.services.search.Pageable;
import de.hybris.platform.cscockpit.services.search.SearchException;
import de.hybris.platform.cscockpit.services.search.impl.AbstractCsSearchService;
import de.hybris.platform.cscockpit.services.search.impl.DefaultCsCommerceSearchFacetValue;
import de.hybris.platform.cscockpit.services.search.impl.DefaultCsFacetSearchResult;
import de.hybris.platform.cscockpit.services.search.impl.DefaultCsTextFacetSearchCommand;
import de.hybris.platform.cscockpit.services.search.solr.query.DefaultCsSolrSort;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.product.TargetProductSearchService;


@SuppressWarnings("deprecation")
public class TargetCsProductSearchService<STATE> extends
        AbstractCsSearchService<ProductModel, FacetSearchPageData<STATE, SearchResultValueData>> implements
        CsFacetSearchService<DefaultCsTextFacetSearchCommand, ProductModel>
{
    public static final String SEARCH_RESULT_VALUE_DATA_PRODUCT_CODE = "code";
    private static final String KEY_SELLABLE_VARIANT = "tgtcs.sellable-variant";
    private static final String ITEM_PRICE_ASC = "price-asc";
    private static final String ITEM_PRICE_DESC = "price-desc";

    private ProductSearchService<STATE, SearchResultValueData, ProductSearchPageData<STATE, SearchResultValueData>> productSearchService;
    private BaseSiteService baseSiteService;
    private TypeService typeService;
    private ProductService productService;
    private CatalogVersionService catalogVersionService;
    private ImpersonationService impersonationService;
    private TargetProductSearchService targetProductSearchService;
    private CommercePriceService commercePriceService;
    private ConfigurationService configurationService;



    public ProductSearchService<STATE, SearchResultValueData, ProductSearchPageData<STATE, SearchResultValueData>> getProductSearchService()
    {
        return productSearchService;
    }

    @Required
    public void setProductSearchService(
            final ProductSearchService<STATE, SearchResultValueData, ProductSearchPageData<STATE, SearchResultValueData>> productSearchService)
    {
        this.productSearchService = productSearchService;
    }

    protected ConfigurationService getConfigurationService()
    {
        return configurationService;
    }

    @Required
    public void setConfigurationService(final ConfigurationService configurationService)
    {
        this.configurationService = configurationService;
    }

    protected BaseSiteService getBaseSiteService()
    {
        return baseSiteService;
    }

    @Required
    public void setBaseSiteService(final BaseSiteService baseSiteService)
    {
        this.baseSiteService = baseSiteService;
    }

    protected TypeService getTypeService()
    {
        return typeService;
    }

    @Required
    public void setTypeService(final TypeService typeService)
    {
        this.typeService = typeService;
    }

    public ProductService getProductService()
    {
        return productService;
    }

    @Required
    public void setProductService(final ProductService productService)
    {
        this.productService = productService;
    }

    public CommercePriceService getCommercePriceService()
    {
        return commercePriceService;
    }

    @Required
    public void setCommercePriceService(final CommercePriceService commercePriceService)
    {
        this.commercePriceService = commercePriceService;
    }

    public TargetProductSearchService getTargetProductSearchService()
    {
        return targetProductSearchService;
    }

    @Required
    public void setTargetProductSearchService(final TargetProductSearchService targetProductSearchService)
    {
        this.targetProductSearchService = targetProductSearchService;
    }


    protected ImpersonationService getImpersonationService()
    {
        return impersonationService;
    }

    @Required
    public void setImpersonationService(final ImpersonationService impersonationService)
    {
        this.impersonationService = impersonationService;
    }

    public CatalogVersionService getCatalogVersionService()
    {
        return catalogVersionService;
    }

    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
    {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * get facet search result by criteria command, pageable If number of sellable variants is over maximum configured
     * number of sellable variants, then return null
     * 
     * @param command
     * @param pageable
     * @return CsFacetSearchResult
     */
    @Override
    public CsFacetSearchResult<DefaultCsTextFacetSearchCommand, ProductModel> search(
            final DefaultCsTextFacetSearchCommand command, final Pageable pageable) throws SearchException
    {
        final ImpersonationContext context = new ImpersonationContext();
        final List<CatalogVersionModel> targetCatalogVersionModels = new ArrayList<>();
        final int pageSize = pageable.getPageSize();
        final int pageCurrent = pageable.getPageNumber();
        if (getCatalogVersionService().getSessionCatalogVersions().contains(
                command.getCatalog().getActiveCatalogVersion()))
        {
            targetCatalogVersionModels.add(command.getCatalog().getActiveCatalogVersion());
        }
        else
        {
            for (final CatalogModel catalog : getBaseSiteService().getProductCatalogs(
                    getBaseSiteService().getCurrentBaseSite()))
            {
                targetCatalogVersionModels.add(catalog.getActiveCatalogVersion());
            }
        }
        context.setCatalogVersions(targetCatalogVersionModels);
        return executeInContext(
                context,
                new ImpersonationService.Executor<CsFacetSearchResult<DefaultCsTextFacetSearchCommand, ProductModel>, SearchException>()
                {
                    @Override
                    public CsFacetSearchResult<DefaultCsTextFacetSearchCommand, ProductModel> execute()
                            throws SearchException
                    {
                        final List<FacetSearchPageData<STATE, SearchResultValueData>> productSearchPageDataList = new ArrayList<FacetSearchPageData<STATE, SearchResultValueData>>();
                        final int maxSellableVariant = getConfigurationService().getConfiguration().getInt(
                                KEY_SELLABLE_VARIANT, 100);

                        final DefaultCsFacetSearchResult<DefaultCsTextFacetSearchCommand, ProductModel> result = new DefaultCsFacetSearchResult<DefaultCsTextFacetSearchCommand, ProductModel>();
                        result.setSearchCommand(command);
                        result.setPageable(pageable);
                        final List<ProductModel> products = getProductModelList(productSearchPageDataList,
                                maxSellableVariant, command.getText());
                        if (products == null) {
                            return null;
                        }
                        return processProductList(pageSize, pageCurrent, result, products);
                    }
                });
    }


    /**
     * 
     * @param command
     * @return DefaultCsCommerceSearchFacetValue
     */
    protected DefaultCsCommerceSearchFacetValue<STATE> getFacetToApply(final DefaultCsTextFacetSearchCommand command)
    {
        return (DefaultCsCommerceSearchFacetValue<STATE>)command.getCurrentFacet();
    }


    /**
     * 
     * @param sortData
     * @return List CsSort
     */
    protected List<CsSort> getAvailableSorts(final List<SortData> sortData)
    {
        final List<CsSort> csSorts = new ArrayList<CsSort>(sortData.size());
        for (final SortData sort : sortData)
        {
            if (!sort.getCode().equals(ITEM_PRICE_DESC) && !sort.getCode().equals(ITEM_PRICE_ASC)) {
                csSorts.add(new DefaultCsSolrSort(sort.getCode()));
            }
        }
        return csSorts;
    }

    /**
     * 
     * @param source
     * @param propertyName
     * @return Object
     */
    protected <T> T getValue(final SearchResultValueData source, final String propertyName)
    {
        if (source.getValues() == null)
        {
            return null;
        }

        // DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
        return (T)source.getValues().get(propertyName);
    }

    /**
     * get product list by page current
     * 
     * @param productSearchPageDataList
     * @param maxSellableVariant
     * @param text
     * @return LinkedList
     */
    protected List<ProductModel> getProductModelList(
            final List<FacetSearchPageData<STATE, SearchResultValueData>> productSearchPageDataList,
            final int maxSellableVariant, final String text) {
        final List<ProductModel> products = new LinkedList<>();

        final ProductModel productModel = getProductService()
                .getProductForCode(text);
        //check if the product is a sellable one
        if (null != productModel) {
            if (CollectionUtils.isEmpty(productModel.getVariants())) {
                products.add(productModel);
            }
            else {
                for (final VariantProductModel variant : productModel.getVariants()) {
                    //if the product is of color variant check if it has size variants
                    if (variant instanceof TargetColourVariantProductModel) {
                        final TargetColourVariantProductModel colorVariant = (TargetColourVariantProductModel)variant;
                        if (CollectionUtils.isEmpty(colorVariant.getVariants())) {
                            products.add(colorVariant);
                        }
                        else {
                            for (final VariantProductModel sizeVariant : colorVariant.getVariants()) {
                                //get the size variants
                                if (sizeVariant instanceof TargetSizeVariantProductModel) {
                                    final TargetSizeVariantProductModel sizeVariantModel = (TargetSizeVariantProductModel)sizeVariant;
                                    products.add(sizeVariantModel);
                                }

                            }
                        }
                    }
                    else if (variant instanceof TargetSizeVariantProductModel) {
                        final TargetSizeVariantProductModel sizevariant = (TargetSizeVariantProductModel)variant;
                        products.add(sizevariant);
                    }
                }
            }
        }
        if (products.size() > maxSellableVariant) {
            return null;
        }
        else {
            return products;
        }
    }

    /**
     * 
     * @param context
     * @param wrapper
     * @return ImpersonationService
     * @throws T
     */
    protected <R, T extends Throwable> R executeInContext(final ImpersonationContext context,
            final ImpersonationService.Executor<R, T> wrapper) throws T
    {
        return getImpersonationService().executeInContext(context, wrapper);
    }

    /**
     * initiate PageableData
     * 
     * @return PageableData
     */
    protected PageableData createPageableData()
    {
        return new PageableData();
    }

    private CsFacetSearchResult<DefaultCsTextFacetSearchCommand, ProductModel> processProductList(final int pageSize,
            final int pageCurrent,
            final DefaultCsFacetSearchResult<DefaultCsTextFacetSearchCommand, ProductModel> result,
            final List<ProductModel> products) {
        final int productLength = products.size();
        result.setTotalResultCount(productLength);
        final List<ProductModel> resultList = new ArrayList<>();
        int index = 0, segment = pageSize;
        final List<SearchResultValueData> searchResultValueDatas = new ArrayList<>();
        if (pageCurrent >= 1) {
            index = (pageCurrent * pageSize);
            segment = (pageCurrent + 1) * pageSize;
        }
        if (productLength <= segment) {
            segment = productLength;
        }
        while (index < segment) {
            resultList.add(products.get(index));
            final SearchResultValueData searchResultValueData = new SearchResultValueData();
            final Map<String, Object> valueDatas = new HashMap<String, Object>();
            valueDatas.put(products.get(index).getCode(), products.get(index));
            searchResultValueData.setValues(valueDatas);
            searchResultValueDatas.add(searchResultValueData);
            index++;
        }
        final FacetSearchPageData<STATE, SearchResultValueData> facetSearchPageData = new FacetSearchPageData<STATE, SearchResultValueData>();
        facetSearchPageData.setResults(searchResultValueDatas);
        result.setResult(createResultMetaData(resultList, facetSearchPageData));
        return result;
    }

}
