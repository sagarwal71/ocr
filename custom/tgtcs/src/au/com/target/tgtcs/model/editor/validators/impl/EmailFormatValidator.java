/**
 * 
 */
package au.com.target.tgtcs.model.editor.validators.impl;

import de.hybris.platform.cscockpit.model.editor.validators.impl.AbstractCockpitValidator;
import de.hybris.platform.util.Config;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zul.impl.api.InputElement;


/**
 * @author tuy.vo
 * 
 */
public class EmailFormatValidator extends AbstractCockpitValidator {
    /* (non-Javadoc)
     * @see de.hybris.platform.cscockpit.model.editor.validators.CockpitValidator#validate(java.lang.Object)
     */
    @Override
    public void validate(final Object value) throws WrongValueException {
        // validate email
        if (value != null && StringUtils.isNotBlank(value.toString())) {
            final String email = value.toString();
            final String emailPatternRegex = Config.getParameter("target.email.regex");
            final Pattern pattern = Pattern.compile(emailPatternRegex, Pattern.CASE_INSENSITIVE);
            final Matcher matcher = pattern.matcher(email);

            final boolean emailIsValid = matcher.matches();

            if (!emailIsValid) {
                throw new WrongValueException(getInputElement(), getI3FailLabel());
            }
        }
    }

    /**
     * @return the inputElement
     */
    @Override
    protected InputElement getInputElement() {
        return super.getInputElement();
    }

    /**
     * @return the i3FailLabel
     */
    @Override
    protected String getI3FailLabel() {
        return super.getI3FailLabel();
    }


}
