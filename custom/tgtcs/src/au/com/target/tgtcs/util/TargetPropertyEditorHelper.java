/**
 * 
 */
package au.com.target.tgtcs.util;

import de.hybris.platform.cockpit.model.editor.EditorHelper;
import de.hybris.platform.cockpit.model.editor.EditorListener;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.services.config.EditorRowConfiguration;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.session.impl.CreateContext;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cscockpit.utils.CssUtils;
import de.hybris.platform.cscockpit.utils.PropertyEditorHelper;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;


/**
 * @author ayushman
 * 
 */
public class TargetPropertyEditorHelper extends PropertyEditorHelper {

    @Override
    public void renderEditor(final HtmlBasedComponent parent,
            final EditorRowConfiguration rowConfig,
            final ObjectValueContainer currentObjectValues,
            final PropertyDescriptor propertyDescriptor, final Widget widget,
            final String isoCode, final EditorListener listener,
            final boolean includePropertyName, final boolean forceNotMandatory) {

        if (!(rowConfig.isVisible())) {
            return;
        }

        final Hbox hbox = getNewHbox();
        hbox.setParent(parent);
        hbox.setWidth("96%");
        hbox.setWidths("9em, none");

        if (includePropertyName) {
            final Label propLabel = new Label(super.getPropertyRendererHelper().getPropertyDescriptorName(
                    propertyDescriptor));
            propLabel.setParent(hbox);
            propLabel.setSclass("editorWidgetEditorLabel");
            propLabel.setTooltiptext(propertyDescriptor.getQualifier());

            if ((!(forceNotMandatory)) && (getCockpitPropertyService().isMandatory(propertyDescriptor, true))) {
                propLabel.setValue(propLabel.getValue() + "*");
                propLabel.setSclass(CssUtils.combine(new String[] { propLabel.getSclass(), "mandatory" }));
            }
        }

        final Map params = createParamsForEditor(rowConfig, propertyDescriptor, widget);

        if (propertyDescriptor.isLocalized()) {
            renderLocalizedEditor(rowConfig, currentObjectValues, propertyDescriptor, listener, hbox, params);
        }
        else {
            renderSingleEditor(rowConfig, currentObjectValues, propertyDescriptor, listener, hbox, params);
        }
    }

    /**
     * @return Hbox
     */
    protected Hbox getNewHbox() {
        return new Hbox();
    }

    /**
     *
     * @param rowConfig
     * @param propertyDescriptor
     * @param widget
     * @return map
     */
    protected Map createParamsForEditor(final EditorRowConfiguration rowConfig,
            final PropertyDescriptor propertyDescriptor, final Widget widget) {
        final CreateContext ctx = new CreateContext(null, null, propertyDescriptor, null);
        ctx.setExcludedTypes(EditorHelper.parseTemplateCodes(rowConfig.getParameter("excludeCreateTypes"),
                UISessionUtils.getCurrentSession().getTypeService()));
        ctx.setAllowedTypes(EditorHelper.parseTemplateCodes(rowConfig.getParameter("restrictToCreateTypes"),
                UISessionUtils.getCurrentSession().getTypeService()));

        final Map params = new HashMap();
        params.putAll(rowConfig.getParameters());
        params.put("createContext", ctx);
        params.put("eventSource", widget);
        return params;
    }

    /**
     * @param rowConfig
     * @param currentObjectValues
     * @param propertyDescriptor
     * @param listener
     * @param hbox
     * @param params
     */
    protected void renderLocalizedEditor(final EditorRowConfiguration rowConfig,
            final ObjectValueContainer currentObjectValues, final PropertyDescriptor propertyDescriptor,
            final EditorListener listener, final Hbox hbox, final Map params) {
        EditorHelper.renderLocalizedEditor(null, propertyDescriptor, hbox, currentObjectValues, true,
                rowConfig.getEditor(),
                params, false, listener);
    }

    /**
     * @param rowConfig
     * @param currentObjectValues
     * @param propertyDescriptor
     * @param listener
     * @param hbox
     * @param params
     */
    protected void renderSingleEditor(final EditorRowConfiguration rowConfig,
            final ObjectValueContainer currentObjectValues, final PropertyDescriptor propertyDescriptor,
            final EditorListener listener, final Hbox hbox, final Map params) {
        EditorHelper.renderSingleEditor(null, propertyDescriptor, hbox, currentObjectValues, true,
                rowConfig.getEditor(),
                params, null, false, listener);
    }

}