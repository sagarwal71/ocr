package au.com.target.tgtcs.search.services.query;


import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.cscockpit.services.search.generic.query.DefaultTicketPoolSearchQueryBuilder;
import de.hybris.platform.cscockpit.services.search.impl.DefaultCsTextSearchCommand;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.ticket.enums.CsTicketState;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;


public class TargetTicketPoolSearchQueryBuilder extends DefaultTicketPoolSearchQueryBuilder {


    public static enum DynamicSearchGroup {
        SearchText;
    }

    @Override
    protected FlexibleSearchQuery buildFlexibleSearchQuery(final DefaultCsTextSearchCommand command) {
        final SearchAgent searchAgent = command.getFlag(SearchAgent.class, SearchAgent.Any);
        final SearchGroup searchGroup = command.getFlag(SearchGroup.class, SearchGroup.Any);
        final String dynamicSearchGroup = command.getText(DynamicSearchGroup.SearchText);
        final SearchStatus searchStatus = command.getFlag(SearchStatus.class, SearchStatus.Any);

        final StringBuilder query = new StringBuilder(300);

        query.append("SELECT {pk}, {creationtime} ");
        query.append("FROM {CsTicket} ");

        final List<String> clauses = new ArrayList();
        switch (searchAgent) {
            case Any:
                break;
            case CurrentUser:
                clauses.add("{assignedAgent}=?currentUser");
                break;
            case AllUsers:
                clauses.add("{assignedAgent} IS NOT NULL");
                break;
            case CurrentUserOrUnassigned:
                clauses.add("({assignedAgent}=?currentUser OR {assignedAgent} IS NULL)");
                break;
            case Unassigned:
                clauses.add("{assignedAgent} IS NULL");
                break;
            default:
                break;
        }

        if (StringUtils.isNotBlank(dynamicSearchGroup)) {
            clauses.add("{assignedGroup}=?usergrp");
        }
        else {
            switch (searchGroup) {
                case Any:
                    break;
                case AllGroups:
                    clauses.add("{assignedGroup} IS NOT NULL");
                    break;
                case MyGroup:
                    clauses.add("{assignedGroup} IN (?currentGroups)");
                    break;
                case MyGroupOrUnassigned:
                    clauses.add("({assignedGroup} IN (?currentGroups) OR {assignedGroup} IS NULL)");
                    break;
                case Unassigned:
                    clauses.add("{assignedGroup} IS NULL");
                default:
                    break;
            }
        }

        switch (searchStatus) {
            case Any:
                break;
            case Closed:
                clauses.add("{state}=?stateClosed");
                break;
            case New:
                clauses.add("{state}=?stateNew");
                break;
            case NewOrOpen:
                clauses.add("({state}=?stateNew OR {state}=?stateOpen)");
                break;
            case Open:
                clauses.add("{state}=?stateOpen");
                break;
            default:
                break;
        }

        if (!clauses.isEmpty()) {
            query.append("WHERE ");
            query.append(StringUtils.join(clauses, " AND "));
            query.append(" ");
        }

        query.append("ORDER BY {creationtime} DESC");

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());

        final UserModel currentUser = getUserService().getCurrentUser();
        searchQuery.addQueryParameter("currentUser", currentUser);
        if (null != dynamicSearchGroup) {
            final UserGroupModel grp = getUserService().getUserGroupForUID(dynamicSearchGroup);
            searchQuery.addQueryParameter("usergrp", grp);

        }
        else {
            searchQuery.addQueryParameter("currentGroups", getAgentGroups(currentUser));

        }
        searchQuery.addQueryParameter("stateOpen", CsTicketState.OPEN);
        searchQuery.addQueryParameter("stateNew", CsTicketState.NEW);
        searchQuery.addQueryParameter("stateClosed", CsTicketState.CLOSED);

        return searchQuery;
    }

}
