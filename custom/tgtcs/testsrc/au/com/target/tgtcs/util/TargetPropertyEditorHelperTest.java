/**
 * 
 */
package au.com.target.tgtcs.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.editor.EditorHelper;
import de.hybris.platform.cockpit.model.editor.EditorListener;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.services.config.EditorRowConfiguration;
import de.hybris.platform.cockpit.services.meta.PropertyService;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.session.UISession;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cscockpit.utils.PropertyRendererHelper;

import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;

import au.com.target.tgtcore.constants.TgtCoreConstants;


/**
 * @author ayushman
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPropertyEditorHelperTest {

    @Mock
    private EditorRowConfiguration mockRowConfig;
    @Mock
    private ObjectValueContainer mockCurrentObjectValues;
    @Mock
    private Widget mockWidget;
    @Mock
    private EditorListener mockListener;
    @Mock
    private AbstractComponent mockComponent;
    @Mock
    private PropertyRendererHelper mockPropertyRendererHelper;
    @Mock
    private EditorHelper mockEditorHelper;
    @Mock
    private TypeService mockTypeService;
    @Mock
    private PropertyDescriptor mockPropertyDescriptor;
    @Mock
    private PropertyService mockPropertyService;
    @Mock
    private UISession mockSession;
    @Mock
    private Map mockParams;

    private final Hbox testHbox = new Hbox();

    protected class MyTargetPropertyEditorHelper extends TargetPropertyEditorHelper {

        @Override
        protected Hbox getNewHbox() {
            return testHbox;
        }

        @Override
        protected Map createParamsForEditor(final EditorRowConfiguration rowConfig,
                final PropertyDescriptor propertyDescriptor,
                final Widget widget) {
            return mockParams;
        }

        @Override
        protected void renderLocalizedEditor(final EditorRowConfiguration rowConfig,
                final ObjectValueContainer currentObjectValues, final PropertyDescriptor propertyDescriptor,
                final EditorListener listener, final Hbox hbox, final Map params) {
            // do nothing
        }

        @Override
        protected void renderSingleEditor(final EditorRowConfiguration rowConfig,
                final ObjectValueContainer currentObjectValues,
                final PropertyDescriptor propertyDescriptor, final EditorListener listener, final Hbox hbox,
                final Map params) {
            // do nothing
        }
    }

    @Spy
    @InjectMocks
    private final MyTargetPropertyEditorHelper editor = new MyTargetPropertyEditorHelper();

    @Before
    public void setUp() {
        editor.setCockpitPropertyService(mockPropertyService);
        editor.setCockpitTypeService(mockTypeService);
        editor.setPropertyRendererHelper(mockPropertyRendererHelper);

        Mockito.when(mockSession.getTypeService()).thenReturn(mockTypeService);
        Mockito.when(mockRowConfig.getParameter("excludeCreateTypes")).thenReturn("excludeCreateTypes");
        Mockito.when(mockRowConfig.getParameter("restrictToCreateTypes")).thenReturn("excludeCreateTypes");
        Mockito.when(Boolean.valueOf(mockRowConfig.isVisible())).thenReturn(Boolean.TRUE);
        Mockito.when(Boolean.valueOf(mockPropertyService.isMandatory(mockPropertyDescriptor, true)))
                .thenReturn(Boolean.valueOf(true));
        Mockito.when(mockPropertyRendererHelper.getPropertyDescriptorName(mockPropertyDescriptor))
                .thenReturn("LabelString");
        Mockito.when(mockPropertyDescriptor.getQualifier())
                .thenReturn("Qualifier");
    }

    @Test
    public void testRenderEditorNotVisible() {
        Mockito.when(Boolean.valueOf(mockRowConfig.isVisible())).thenReturn(Boolean.FALSE);

        final Div editorDiv = new Div();

        editor.renderEditor(editorDiv, mockRowConfig, mockCurrentObjectValues, mockPropertyDescriptor, mockWidget,
                TgtCoreConstants.AUSTRALIAN_DOLLARS,
                mockListener,
                false, false);
        Assert.assertEquals(0, editorDiv.getChildren().size());
    }

    @Test
    public void testRenderEditor() {
        final Div editorDiv = new Div();

        editor.renderEditor(editorDiv, mockRowConfig, mockCurrentObjectValues, mockPropertyDescriptor, mockWidget,
                TgtCoreConstants.AUSTRALIAN_DOLLARS,
                mockListener,
                false, false);

        final List<AbstractComponent> abstractComponents = editorDiv.getChildren();
        Assert.assertNotNull(abstractComponents);
        Assert.assertEquals(1, abstractComponents.size());

        final Hbox hbox = (Hbox)abstractComponents.get(0);
        Assert.assertEquals("96%", hbox.getWidth());
        Assert.assertEquals("9em,none", hbox.getWidths());
    }

    @Test
    public void testRenderEditorIncludePropertyNameAndForcedNotMandatory() {
        final Div editorDiv = new Div();
        editor.renderEditor(editorDiv, mockRowConfig, mockCurrentObjectValues, mockPropertyDescriptor, mockWidget,
                TgtCoreConstants.AUSTRALIAN_DOLLARS,
                mockListener,
                true, true);

        final Hbox hbox = (Hbox)editorDiv.getChildren().get(0);
        final Label propLabel = (Label)hbox.getChildren().get(0);

        Assert.assertEquals("LabelString", propLabel.getValue());
        Assert.assertEquals("editorWidgetEditorLabel", propLabel.getSclass());
        Assert.assertEquals("Qualifier", propLabel.getTooltiptext());
    }

    @Test
    public void testRenderEditorIncludePropertyNameAndForcedMandatory() {
        final Div editorDiv = new Div();
        editor.renderEditor(editorDiv, mockRowConfig, mockCurrentObjectValues, mockPropertyDescriptor, mockWidget,
                TgtCoreConstants.AUSTRALIAN_DOLLARS,
                mockListener,
                true, false);

        final Hbox hbox = (Hbox)editorDiv.getChildren().get(0);
        final Label propLabel = (Label)hbox.getChildren().get(0);

        Assert.assertTrue(propLabel.getValue().endsWith("*"));
        Assert.assertTrue(propLabel.getSclass().endsWith("mandatory"));
    }

    @Test
    public void testRendereEditorLocalizedPropertyDescriptor() {
        Mockito.when(Boolean.valueOf(mockPropertyDescriptor.isLocalized())).thenReturn(Boolean.TRUE);

        final Div editorDiv = new Div();
        editor.renderEditor(editorDiv, mockRowConfig, mockCurrentObjectValues, mockPropertyDescriptor, mockWidget,
                TgtCoreConstants.AUSTRALIAN_DOLLARS,
                mockListener,
                false, false);

        Mockito.verify(editor).renderLocalizedEditor(mockRowConfig, mockCurrentObjectValues, mockPropertyDescriptor,
                mockListener, testHbox, mockParams);

        Mockito.verify(editor, Mockito.times(0)).renderSingleEditor(mockRowConfig, mockCurrentObjectValues,
                mockPropertyDescriptor,
                mockListener, testHbox, mockParams);
    }

    @Test
    public void testRendereEditorNonLocalizedPropertyDescriptor() {
        Mockito.when(Boolean.valueOf(mockPropertyDescriptor.isLocalized())).thenReturn(Boolean.FALSE);

        final Div editorDiv = new Div();
        editor.renderEditor(editorDiv, mockRowConfig, mockCurrentObjectValues, mockPropertyDescriptor, mockWidget,
                TgtCoreConstants.AUSTRALIAN_DOLLARS,
                mockListener,
                false, false);

        Mockito.verify(editor, Mockito.times(0)).renderLocalizedEditor(mockRowConfig, mockCurrentObjectValues,
                mockPropertyDescriptor,
                mockListener, testHbox, mockParams);

        Mockito.verify(editor).renderSingleEditor(mockRowConfig, mockCurrentObjectValues,
                mockPropertyDescriptor,
                mockListener, testHbox, mockParams);
    }
}