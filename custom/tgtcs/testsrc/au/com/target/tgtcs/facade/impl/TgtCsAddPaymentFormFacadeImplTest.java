package au.com.target.tgtcs.facade.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.cscockpit.services.payment.CsCardPaymentService;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.internal.model.attribute.DynamicAttributesProvider;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;
import org.zkoss.zhtml.Form;
import org.zkoss.zhtml.Input;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtcs.facade.TgtCsCardPaymentFacade;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.dto.HostedSessionTokenRequest;
import au.com.target.tgtpayment.service.TargetPaymentService;


/**
 * @author umesh
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TgtCsAddPaymentFormFacadeImplTest {
    @Mock
    private Window window;
    @Mock
    private OrderModel order;
    @Mock
    private ApplicationContext ctx;
    @Mock
    private CommonI18NService commonI18NService;
    @Mock
    private AbstractComponent parent;
    @Mock
    private TargetPaymentService targetPaymentService;
    @Mock
    private DynamicAttributesProvider dynamicAttributesProvider;
    @Mock
    private CsCardPaymentService csCardPaymentService;
    @Mock
    private ConfigurationService configurationService;
    @Mock
    private Configuration configuration;
    @Mock
    private TgtCsCardPaymentFacade tgtCsCardPaymentFacade;
    @Mock
    private PaymentModeService paymentModeService;
    @Mock
    private PaymentModeModel paymentModeModel;
    @Mock
    private AddressModel addressModel;
    @Mock
    private CountryModel countryModel;
    @Mock
    private Hbox hbox;

    @Mock
    private TargetCreateSubscriptionResult targetCreateSubscriptionResult;

    @Mock
    private HostedSessionTokenRequest hostedSessionTokenRequest;

    private class TgtCsAddPaymentFormFacadeMockImpl extends TgtCsAddPaymentFormFacadeImpl {
        @Override
        public Hbox getNewHBox() {
            return hbox;
        }
    }

    @InjectMocks
    private final TgtCsAddPaymentFormFacadeMockImpl facade = new TgtCsAddPaymentFormFacadeMockImpl();

    @Before
    public void setup() {
        given(targetCreateSubscriptionResult.getRequestToken())
                .willReturn("sessionToken");
        given(targetPaymentService.getHostedSessionToken(hostedSessionTokenRequest))
                .willReturn(targetCreateSubscriptionResult);

        facade.setCsCardPaymentService(csCardPaymentService);
        facade.setCommonI18NService(commonI18NService);
        facade.setPaymentModeService(paymentModeService);
        facade.setTargetPaymentService(targetPaymentService);
        facade.setTgtcsbaseurl("http://localhost:9001/tgtcs");
        facade.setTnsbaseurl("https://secure.ap.tnspayments.com/form/");
        facade.setTgtCsCardPaymentFacade(tgtCsCardPaymentFacade);

    }

    @Test
    public void testPopulateformWithoutAddress() {
        final Intbox intbox = new Intbox();
        final Input input = new Input();
        final Form form = new Form();
        given(window.getFellow(TgtcsConstants.GATEWAY_CARD_EXPIRY_DATE_MONTH)).willReturn(intbox);
        given(window.getFellow(TgtcsConstants.GATEWAY_CARD_EXPIRY_DATE_YEAR)).willReturn(intbox);
        given(order.getPaymentAddress()).willReturn(null);
        given(dynamicAttributesProvider.get(order, "line1")).willReturn("streetname");
        given(dynamicAttributesProvider.get(order, "line2")).willReturn("123");
        given(order.getCode()).willReturn("o123");
        given(window.getFellow(TgtcsConstants.PAYMENT_FORM)).willReturn(form);
        given(window.getFellow(TgtcsConstants.PAYMENT_SESSION_ID)).willReturn(input);
        given(window.getFellow(TgtcsConstants.TNSACTION)).willReturn(input);
        given(window.getFellow(TgtcsConstants.GATEWAY_RETURN_URL)).willReturn(input);
        given(window.getFellow(TgtcsConstants.ORDER_ID)).willReturn(input);
        facade.populateRefundForm(order, window);

        Assert.assertNull(window.getFellow(TgtcsConstants.FIRST_NAME));
    }

    @Test
    public void testLoadFormContent() {
        final Div formcontainer = new Div();
        final Intbox intbox = new Intbox();
        final Input input = new Input();
        final Form form = new Form();
        final Textbox textBox = new Textbox();
        final Combobox combobox = new Combobox();
        given(window.getFellow(TgtcsConstants.HOSTED_PAYMENT_FORM_CONTAINER)).willReturn(formcontainer);
        given(csCardPaymentService.getSupportedCardSchemes(order)).willReturn(getCards());
        given(window.getFellow(TgtcsConstants.CARD_TYPE)).willReturn(combobox);
        given(window.getFellow(TgtcsConstants.GATEWAY_CARD_EXPIRY_DATE_MONTH)).willReturn(intbox);
        given(window.getFellow(TgtcsConstants.GATEWAY_CARD_EXPIRY_DATE_YEAR)).willReturn(intbox);
        given(order.getPaymentAddress()).willReturn(addressModel);
        given(dynamicAttributesProvider.get(order, "line1")).willReturn("streetname");
        given(dynamicAttributesProvider.get(order, "line2")).willReturn("123");
        given(order.getCode()).willReturn("o123");
        given(window.getFellow(TgtcsConstants.PAYMENT_FORM)).willReturn(form);
        given(window.getFellow(TgtcsConstants.TNSACTION)).willReturn(input);
        given(window.getFellow(TgtcsConstants.PAYMENT_SESSION_ID)).willReturn(input);
        given(window.getFellow(TgtcsConstants.GATEWAY_RETURN_URL)).willReturn(input);
        given(window.getFellow(TgtcsConstants.ORDER_ID)).willReturn(input);
        given(window.getFellow(TgtcsConstants.PAYMENT_FORM)).willReturn(form);
        given(paymentModeService.getPaymentModeForCode(TgtcsConstants.CREDITCARD)).willReturn(paymentModeModel);
        populateAddress(textBox, combobox);
        facade.loadFormContent(order, window);
    }

    @Test
    public void testPopulateformWithAddress() {
        final Intbox intbox = new Intbox();
        final Form form = new Form();
        final Input input = new Input();
        final Textbox textBox = new Textbox();
        final Combobox combobox = new Combobox();
        given(window.getFellow(TgtcsConstants.GATEWAY_CARD_EXPIRY_DATE_MONTH)).willReturn(intbox);
        given(window.getFellow(TgtcsConstants.GATEWAY_CARD_EXPIRY_DATE_YEAR)).willReturn(intbox);
        Mockito.when(order.getPaymentAddress()).thenReturn(addressModel);
        given(dynamicAttributesProvider.get(order, "line1")).willReturn("streetname");
        given(dynamicAttributesProvider.get(order, "line2")).willReturn("123");
        given(order.getCode()).willReturn("o123");
        given(window.getFellow(TgtcsConstants.PAYMENT_FORM)).willReturn(form);
        given(window.getFellow(TgtcsConstants.TNSACTION)).willReturn(input);
        given(window.getFellow(TgtcsConstants.PAYMENT_SESSION_ID)).willReturn(input);
        given(window.getFellow(TgtcsConstants.GATEWAY_RETURN_URL)).willReturn(input);
        given(window.getFellow(TgtcsConstants.ORDER_ID)).willReturn(input);
        populateAddress(textBox, combobox);
        facade.populateRefundForm(order, window);
        Assert.assertNotNull(window.getFellow(TgtcsConstants.FIRST_NAME));
    }

    @Test
    public void testFillUpCountries() {
        final Combobox combobox = new Combobox();
        given(window.getFellow(TgtcsConstants.COUNTRY)).willReturn(combobox);
        final List<CountryModel> countries = new ArrayList<>();
        countries.add(countryModel);
        given(commonI18NService.getAllCountries()).willReturn(countries);
        facade.fillUpCountries(window);
    }

    @Test
    public void testGetDeliveryCountriesWithEmptyList() {
        given(commonI18NService.getAllCountries()).willReturn(null);
        final List<CountryData> list = facade.getDeliveryCountries();
        Assert.assertEquals(0, list.size());
    }

    @Test
    public void testGetDeliveryCountriesWithNotEmptyList() {
        final List<CountryModel> countries = new ArrayList<>();
        countries.add(countryModel);
        given(commonI18NService.getAllCountries()).willReturn(countries);
        final List<CountryData> list = facade.getDeliveryCountries();
        Assert.assertEquals(1, list.size());
    }

    @Test
    public void testGetAvailableCardTypesWithEmptyList() {
        given(csCardPaymentService.getSupportedCardSchemes(order)).willReturn(new ArrayList<CreditCardType>());
        final List<CreditCardType> list = facade.getAvailableCardTypes(order);
        Assert.assertEquals(0, list.size());
    }

    @Test
    public void testGetAvailableCardTypesWithNotEmptyList() {
        given(csCardPaymentService.getSupportedCardSchemes(order)).willReturn(getCards());
        final List<CreditCardType> list = facade.getAvailableCardTypes(order);
        Assert.assertEquals(4, list.size());
    }

    private void populateAddress(final Textbox textBox, final Combobox combobox) {
        given(window.getFellow(TgtcsConstants.FIRST_NAME)).willReturn(textBox);
        given(window.getFellow(TgtcsConstants.LAST_NAME)).willReturn(textBox);
        given(window.getFellow(TgtcsConstants.STREET1)).willReturn(textBox);
        given(window.getFellow(TgtcsConstants.STREET2)).willReturn(textBox);
        given(window.getFellow(TgtcsConstants.CITY)).willReturn(textBox);
        given(window.getFellow(TgtcsConstants.POSTAL_CODE)).willReturn(textBox);
        given(window.getFellow(TgtcsConstants.COUNTRY)).willReturn(combobox);
        given(window.getFellow(TgtcsConstants.PHONE_NUMBER)).willReturn(textBox);
        given(window.getFellow(TgtcsConstants.STATE)).willReturn(textBox);
    }

    private List<CreditCardType> getCards() {
        final List<CreditCardType> types = new ArrayList<>();
        types.add(CreditCardType.AMEX);
        types.add(CreditCardType.VISA);
        types.add(CreditCardType.MASTER);
        types.add(CreditCardType.DINERS);
        return types;
    }
}
