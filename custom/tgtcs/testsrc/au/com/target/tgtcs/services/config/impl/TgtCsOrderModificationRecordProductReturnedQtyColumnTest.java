/**
 * 
 */
package au.com.target.tgtcs.services.config.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.returns.model.OrderEntryReturnRecordEntryModel;

import java.util.Locale;

import org.junit.Assert;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;


/**
 * @author umesh
 * 
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Locale.class })
public class TgtCsOrderModificationRecordProductReturnedQtyColumnTest {

    private final TgtCsOrderModificationRecordProductReturnedQtyColumn column = new TgtCsOrderModificationRecordProductReturnedQtyColumn();

    @Mock
    private Locale locale;

    @Before
    public void setup() {
        PowerMockito.mockStatic(Locale.class, Mockito.RETURNS_MOCKS);
    }

    @Test
    public void testItemValueWithNoModel() throws ValueHandlerException {
        Assert.assertEquals(StringUtils.EMPTY, column.getItemValue(null, locale));
    }

    @Test
    public void testItemValueForReturnQuantity() throws ValueHandlerException {
        final OrderEntryReturnRecordEntryModel model = Mockito.mock(OrderEntryReturnRecordEntryModel.class);
        Mockito.when(model.getReturnedQuantity()).thenReturn(new Long(1));
        Assert.assertEquals("1", column.getItemValue(model, locale));
    }
}
