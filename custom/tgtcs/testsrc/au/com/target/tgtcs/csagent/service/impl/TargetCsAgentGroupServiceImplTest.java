/**
 * 
 */
package au.com.target.tgtcs.csagent.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ticket.model.CsAgentGroupModel;

import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcs.csagent.dao.TargetCsAgentGroupDao;


/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCsAgentGroupServiceImplTest {

    @Mock
    private TargetCsAgentGroupDao targetCsAgentGroupDao;

    @Mock
    private CsAgentGroupModel grpModel;

    @InjectMocks
    private final TargetCsAgentGroupServiceImpl service = new TargetCsAgentGroupServiceImpl();

    @Before
    public void setup() {
        Mockito.when(grpModel.getUid()).thenReturn("fraudAgentGroup");
    }

    /**
     * Test for csagentgrps available
     */
    @Test
    public void testGetAllCustomerAgentGroupUid() {
        Mockito.when(targetCsAgentGroupDao.findAllCsAgentGroup()).thenReturn(Collections.singletonList(grpModel));
        final List<String> grpUids = service.getAllCustomerAgentGroupUid();
        Assert.assertNotNull(grpUids);
        Assert.assertEquals(1, grpUids.size());
        Assert.assertEquals("fraudAgentGroup", grpUids.get(0).toString());
    }

    /**
     * Test for null csagentgrp
     */
    @Test
    public void testGetAllCustomerAgentGroupUidWithNull() {
        Mockito.when(targetCsAgentGroupDao.findAllCsAgentGroup()).thenReturn(null);
        final List<String> grpUids = service.getAllCustomerAgentGroupUid();
        Assert.assertNull(grpUids);

    }

}
