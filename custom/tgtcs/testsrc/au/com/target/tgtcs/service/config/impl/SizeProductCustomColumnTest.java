package au.com.target.tgtcs.service.config.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


@UnitTest
public class SizeProductCustomColumnTest {
    private SizeProductCustomColumn customColumn;

    @Before
    public void prepare()
    {
        MockitoAnnotations.initMocks(this);
        customColumn = new SizeProductCustomColumn();
    }

    @Test
    public void testSizeColumnMatch() {
        final TargetSizeVariantProductModel product = mock(TargetSizeVariantProductModel.class);
        when(product.getSize()).thenReturn("L");
        final String result = customColumn.getProductValue(product, null);
        Assert.assertEquals("L", result);
    }

    @Test
    public void testSizeColumnNotMatch() {
        final TargetColourVariantProductModel product = mock(TargetColourVariantProductModel.class);
        final String result = customColumn.getProductValue(product, null);
        Assert.assertEquals("", result);
    }

}
