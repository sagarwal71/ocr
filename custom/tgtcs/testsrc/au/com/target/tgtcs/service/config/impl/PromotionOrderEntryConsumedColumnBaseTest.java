package au.com.target.tgtcs.service.config.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cscockpit.model.promotions.wrappers.AbstractItemModelReflectionWrapper;
import de.hybris.platform.cscockpit.model.promotions.wrappers.WrappedPromotionOrderEntryConsumedModel;
import de.hybris.platform.cscockpit.services.promotions.CsPromotionService;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;

import java.util.Locale;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;


/**
 * Base Unit test for {@link AbstractPromotionOrderEntryConsumedColumn}
 */
@Ignore
public abstract class PromotionOrderEntryConsumedColumnBaseTest {

    protected AbstractPromotionOrderEntryConsumedColumn spy;

    @Mock
    protected TypeService mockTypeService;

    @Mock
    protected CsPromotionService mockCsPromotionService;

    @Mock
    protected TypedObject poecTypedObject;

    @Mock
    protected WrappedPromotionOrderEntryConsumedModel mockWrappedPromotionOrderEntryConsumedModel;

    @Test
    public void testGetItemValueForNull() throws ValueHandlerException {
        final Object result = spy.getItemValue(null, Locale.ENGLISH);

        Assert.assertNull(result);

        Mockito.verifyZeroInteractions(mockTypeService);
        Mockito.verifyZeroInteractions(mockCsPromotionService);
    }

    @Test
    public void testGetItemValueForNotSupportedType() throws ValueHandlerException {

        final ItemModel mockItem = Mockito.mock(ItemModel.class);

        final Object result = spy.getItemValue(mockItem, Locale.ENGLISH);

        Assert.assertNull(result);

        Mockito.verifyZeroInteractions(mockTypeService);
        Mockito.verifyZeroInteractions(mockCsPromotionService);
    }

    @Test
    public void testGetItemValueCSPromotionServiceUnavailable() throws ValueHandlerException {

        final PromotionOrderEntryConsumedModel poecModel = Mockito.mock(PromotionOrderEntryConsumedModel.class);

        BDDMockito.given(Boolean.valueOf(mockCsPromotionService.isPromotionServiceAvailable())).willReturn(
                Boolean.FALSE);

        final Object result = spy.getItemValue(poecModel, Locale.ENGLISH);

        Assert.assertNull(result);
        Mockito.verify(mockCsPromotionService).isPromotionServiceAvailable();
        Mockito.verifyZeroInteractions(mockTypeService);

    }

    @Test
    public void testGetItemValueForNonSupportedReflectionWrapper() throws ValueHandlerException {

        final PromotionOrderEntryConsumedModel poecModel = Mockito.mock(PromotionOrderEntryConsumedModel.class);

        BDDMockito.given(Boolean.valueOf(mockCsPromotionService.isPromotionServiceAvailable())).willReturn(
                Boolean.TRUE);

        final AbstractItemModelReflectionWrapper reflectionWrapper = Mockito
                .mock(AbstractItemModelReflectionWrapper.class);
        BDDMockito.given(mockCsPromotionService.unwrapPromotionTypedObject(poecTypedObject)).willReturn(
                reflectionWrapper);

        final Object result = spy.getItemValue(poecModel, Locale.ENGLISH);

        Assert.assertNull(result);
        Mockito.verify(mockCsPromotionService).isPromotionServiceAvailable();
        Mockito.verify(mockTypeService).wrapItem(poecModel);
        Mockito.verify(mockCsPromotionService).unwrapPromotionTypedObject(poecTypedObject);

    }

}
