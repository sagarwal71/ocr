/**
 *
 */
package au.com.target.tgtcs.model.cellrenderers.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.listview.TableModel;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.core.model.media.MediaModel;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.Assert;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;


/**
 * @author Pradeep
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TgtCsImageCellRendererTest {

    private static final String COCKPIT_IMAGES_DUMMY_URL = "cockpit/images/dummyUrl";

    private static final String MEDIA_PATH = "/medias/static_content/product/images/thumb/-0/01/10335451-IMG-001.jpg";

    @Mock
    private TableModel tableModel;


    private final Div parentDiv = new Div();

    @Mock
    private TypedObject value;

    @Mock
    private MediaModel mediaModel;

    private final TgtCsImageCellRenderer targetImageCellRenderer = new TgtCsImageCellRenderer();


    @Test(expected = IllegalArgumentException.class)
    public void testrenderWithNullModel()
    {
        targetImageCellRenderer.render(null, 0, 0, parentDiv);

    }

    @Test
    public void testrenderWithTypedObject()
    {
        when(tableModel.getValueAt(Matchers.anyInt(), Matchers.anyInt())).thenReturn(value);
        when(value.getObject()).thenReturn(mediaModel);
        when(mediaModel.getInternalURL()).thenReturn(
                MEDIA_PATH);
        targetImageCellRenderer.render(tableModel, 0, 0, parentDiv);
        Assert.notNull(parentDiv.getFirstChild(), "First child component must not be null");
        Assert.notNull(parentDiv.getFirstChild().getFirstChild(),
                "First child component of first child must not be null");
        Assert.isInstanceOf(Image.class, parentDiv.getFirstChild().getFirstChild());
        Assert.isTrue(StringUtils.contains(((Image)parentDiv.getFirstChild().getFirstChild()).getSrc(),
                MEDIA_PATH), "The source URI of the image must be true");

    }


    @Test
    public void testrenderWithNullValue()
    {
        when(tableModel.getValueAt(Matchers.anyInt(), Matchers.anyInt())).thenReturn(value);
        when(value.getObject()).thenReturn(null);
        targetImageCellRenderer.render(tableModel, 0, 0, parentDiv);
        Assert.notNull(parentDiv.getFirstChild(), "First child component must not be null");
        Assert.notNull(parentDiv.getFirstChild().getFirstChild(),
                "First child component of first child must not be null");
        Assert.isInstanceOf(Image.class, parentDiv.getFirstChild().getFirstChild());
        Assert.isTrue(StringUtils.contains(((Image)parentDiv.getFirstChild().getFirstChild()).getSrc(),
                COCKPIT_IMAGES_DUMMY_URL), "The source URI of the image must be true");

    }


    @Test
    public void testrenderWithStringValue()
    {
        when(tableModel.getValueAt(Matchers.anyInt(), Matchers.anyInt())).thenReturn(
                MEDIA_PATH);
        targetImageCellRenderer.render(tableModel, 0, 0, parentDiv);
        Assert.notNull(parentDiv.getFirstChild(), "First child component must not be null");
        Assert.notNull(parentDiv.getFirstChild().getFirstChild(),
                "First child component of first child must not be null");
        Assert.isInstanceOf(Image.class, parentDiv.getFirstChild().getFirstChild());
        Assert.isTrue(StringUtils.contains(((Image)parentDiv.getFirstChild().getFirstChild()).getSrc(),
                MEDIA_PATH), "The source URI of the image must be true");

    }

}
