package au.com.target.tgtcs.widgets.controllers.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.cscockpit.exceptions.ValidationException;
import de.hybris.platform.cscockpit.widgets.controllers.impl.DefaultCancellationController.CancelEntryDetails;
import de.hybris.platform.cscockpit.widgets.renderers.utils.edit.BasicPropertyDescriptor;
import de.hybris.platform.ordercancel.OrderCancelException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import au.com.target.tgtcore.order.FindOrderRefundedShippingStrategy;
import au.com.target.tgtcore.ordercancel.TargetOrderCancelRequest;
import au.com.target.tgtcore.ordercancel.enums.RequestOrigin;
import au.com.target.tgtcs.components.orderdetail.controllers.TgtCsCardPaymentController;


/**
 * Test suite for {@link TargetCancellationControllerImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCancellationControllerImplTest {

    /**
     * 
     */
    private static final String ENUM = "ENUM";
    /**
     * 
     */
    private static final String LONG = "LONG";
    /**
     * 
     */
    private static final String ORDER_CANCEL_ENTRY_CANCEL_REASON = "OrderCancelEntry.cancelReason";
    /**
     * 
     */
    private static final String ORDER_CANCEL_ENTRY_CANCEL_QUANTITY = "OrderCancelEntry.cancelQuantity";

    /**
     * 
     */
    private static final String SS = "ss";

    /**
     * 
     */
    private static final String WAREHOUSE_REASON = "warehouse reason";
    /**
     * 
     */
    private static final String CANCEL_REASON = "2";
    /**
     * 
     */
    private static final String ORDER_CODE = "1234";
    private static final String VALIDATION_EXCEPTION = "validation exception";



    @Mock
    private OrderModel order;
    @Mock
    private OrderEntryModel orderEntry;

    @Mock
    private ObjectValueContainer objectValueContainer;

    @Mock
    private FindOrderRefundedShippingStrategy findOrderRefundedShippingStrategy;

    @Mock
    private TypeService cockpitTypeService;

    @Spy
    @InjectMocks
    private final TargetCancellationControllerImpl controller = new TargetCancellationControllerImpl();

    /**
     * Initializes test cases before run.
     */
    @Before
    public void setUp() {
        final TypedObject typedObject = mock(TypedObject.class);
        when(typedObject.getObject()).thenReturn(order);
        doReturn(typedObject).when(controller).getOrder();
        when(orderEntry.getOrder()).thenReturn(order);
    }


    /**
     * Verifies that controller return Target's version of partial order cancel request with submitted delivery cost to
     * return upon {@link TargetCancellationControllerImpl#buildCancelRequest(OrderModel, Map, ObjectValueContainer)}
     * invocation.
     */
    @Test
    public void testBuildPartialOrderCancelRequest() {
        final Double shipToRefund = Double.valueOf(5d);

        final TypedObject typedObject = mock(TypedObject.class);
        when(typedObject.getObject()).thenReturn(orderEntry);
        when(orderEntry.getOrder()).thenReturn(order);

        final CancelEntryDetails cancelEntryDetails = mock(CancelEntryDetails.class);

        final PropertyDescriptor descriptor = mockPropertyDescriptorValue("OrderCancelRequest.shippingAmountToRefund",
                shipToRefund);
        when(objectValueContainer.getPropertyDescriptors()).thenReturn(
                ImmutableSet.of(descriptor));

        final TgtCsCardPaymentController tgtCsCardPaymentController = Mockito.mock(TgtCsCardPaymentController.class);
        controller.setTgtCsCardPaymentController(tgtCsCardPaymentController);
        final CreditCardPaymentInfoModel newPayment = Mockito.mock(CreditCardPaymentInfoModel.class);
        when(tgtCsCardPaymentController.getNewPayment()).thenReturn(newPayment);
        when(newPayment.getCode()).thenReturn("00001_userid");
        final UserModel user = Mockito.mock(UserModel.class);
        when(user.getUid()).thenReturn("userid");
        when(newPayment.getUser()).thenReturn(user);
        when(newPayment.getSubscriptionId()).thenReturn("fakesessionid");
        when(order.getUser()).thenReturn(user);

        final Map<TypedObject, CancelEntryDetails> entriesToCancel = ImmutableMap.of(typedObject, cancelEntryDetails);
        final TargetOrderCancelRequest result = (TargetOrderCancelRequest)
                controller.buildCancelRequest(order, entriesToCancel, objectValueContainer);

        assertEquals(1, result.getEntriesToCancel().size());
        assertEquals(shipToRefund, result.getShippingAmountToRefund());
        assertEquals(null, result.getNewPayment());
    }

    @Test(expected = ValidationException.class)
    public void testInvalidQty() throws ValidationException {
        order.setCode(ORDER_CODE);

        cockpitTypeService = mock(TypeService.class);
        final TypedObject torder = mock(TypedObject.class);
        controller.setCockpitTypeService(cockpitTypeService);

        when(cockpitTypeService.wrapItem(order)).thenReturn(torder);
        when(torder.getObject()).thenReturn(order);

        final TypedObject object = controller.getOrder();

        assertTrue(object.getObject() instanceof OrderModel);

        final Map<TypedObject, Long> cancelableOrderEntries = new HashMap<TypedObject, Long>();
        final List<ObjectValueContainer> orderEntryCancelRecords = new ArrayList<>();

        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);

        final TypedObject to1 = mock(TypedObject.class);
        final TypedObject to2 = mock(TypedObject.class);

        when(to1.getObject()).thenReturn(entry1);
        when(to2.getObject()).thenReturn(entry2);
        when(entry1.getEntryNumber()).thenReturn(Integer.valueOf(0));
        when(entry2.getEntryNumber()).thenReturn(Integer.valueOf(1));

        cancelableOrderEntries.put(to1, new Long(5));
        cancelableOrderEntries.put(to2, new Long(10));

        final ObjectValueContainer currentObjectValues = new ObjectValueContainer(null, to1);
        final ObjectValueContainer currentObjectValues2 = new ObjectValueContainer(null, to2);

        final PropertyDescriptor pd = new BasicPropertyDescriptor(ORDER_CANCEL_ENTRY_CANCEL_QUANTITY, LONG);
        final PropertyDescriptor pd2 = new BasicPropertyDescriptor(ORDER_CANCEL_ENTRY_CANCEL_REASON, ENUM);
        final String languageIso = null;
        currentObjectValues.addValue(pd, languageIso, new Long(0)); // quantity should not be 0
        currentObjectValues.addValue(pd2, languageIso, SS);
        orderEntryCancelRecords.add(currentObjectValues);

        currentObjectValues2.addValue(pd, languageIso, new Long(12)); // quantity should not more than the cancelquantity: 10 in this case
        currentObjectValues2.addValue(pd2, languageIso, WAREHOUSE_REASON);
        orderEntryCancelRecords.add(currentObjectValues2);


        final boolean isvalid = controller.validateCreateCancellationRequest(order,
                cancelableOrderEntries, orderEntryCancelRecords);
        assertFalse(isvalid);

    }

    @Test
    public void testValid() {
        order.setCode(ORDER_CODE);
        cockpitTypeService = mock(TypeService.class);
        final TypedObject torder = mock(TypedObject.class);
        controller.setCockpitTypeService(cockpitTypeService);

        when(cockpitTypeService.wrapItem(order)).thenReturn(torder);
        when(torder.getObject()).thenReturn(order);

        final TypedObject object = controller.getOrder();

        assertTrue(object.getObject() instanceof OrderModel);

        final Map<TypedObject, Long> cancelableOrderEntries = new HashMap<TypedObject, Long>();
        final List<ObjectValueContainer> orderEntryCancelRecords = new ArrayList<>();

        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);

        final TypedObject to1 = mock(TypedObject.class);
        final TypedObject to2 = mock(TypedObject.class);

        when(to1.getObject()).thenReturn(entry1);
        when(to2.getObject()).thenReturn(entry2);
        when(entry1.getEntryNumber()).thenReturn(Integer.valueOf(0));
        when(entry2.getEntryNumber()).thenReturn(Integer.valueOf(1));

        cancelableOrderEntries.put(to1, new Long(5));
        cancelableOrderEntries.put(to2, new Long(10));

        final ObjectValueContainer currentObjectValues = new ObjectValueContainer(null, to1);
        final ObjectValueContainer currentObjectValues2 = new ObjectValueContainer(null, to2);

        final PropertyDescriptor pd = new BasicPropertyDescriptor(ORDER_CANCEL_ENTRY_CANCEL_QUANTITY, LONG);
        final PropertyDescriptor pd2 = new BasicPropertyDescriptor(ORDER_CANCEL_ENTRY_CANCEL_REASON, ENUM);
        final String languageIso = null;
        currentObjectValues.addValue(pd, languageIso, new Long(5));
        currentObjectValues.addValue(pd2, languageIso, SS);
        orderEntryCancelRecords.add(currentObjectValues);

        currentObjectValues2.addValue(pd, languageIso, new Long(3));
        currentObjectValues2.addValue(pd2, languageIso, CANCEL_REASON);
        orderEntryCancelRecords.add(currentObjectValues2);

        try {
            final boolean isvalid = controller.validateCreateCancellationRequest(order,
                    cancelableOrderEntries, orderEntryCancelRecords);
            assertTrue(isvalid);
        }
        catch (final ValidationException e) {
            fail(VALIDATION_EXCEPTION);
        }

    }

    @Test
    public void testMissingReason() throws ValidationException {
        order.setCode(ORDER_CODE);
        cockpitTypeService = mock(TypeService.class);
        final TypedObject torder = mock(TypedObject.class);
        controller.setCockpitTypeService(cockpitTypeService);

        when(cockpitTypeService.wrapItem(order)).thenReturn(torder);
        when(torder.getObject()).thenReturn(order);

        final Map<TypedObject, Long> cancelableOrderEntries = new HashMap<TypedObject, Long>();
        final List<ObjectValueContainer> orderEntryCancelRecords = new ArrayList<>();

        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);

        final TypedObject to1 = mock(TypedObject.class);
        final TypedObject to2 = mock(TypedObject.class);

        when(to1.getObject()).thenReturn(entry1);
        when(to2.getObject()).thenReturn(entry2);
        when(entry1.getEntryNumber()).thenReturn(Integer.valueOf(0));
        when(entry2.getEntryNumber()).thenReturn(Integer.valueOf(1));

        cancelableOrderEntries.put(to1, new Long(5));
        cancelableOrderEntries.put(to2, new Long(10));

        final ObjectValueContainer currentObjectValues = new ObjectValueContainer(null, to1);
        final ObjectValueContainer currentObjectValues2 = new ObjectValueContainer(null, to2);

        final PropertyDescriptor pd = new BasicPropertyDescriptor(ORDER_CANCEL_ENTRY_CANCEL_QUANTITY, LONG);
        final PropertyDescriptor pd2 = new BasicPropertyDescriptor(ORDER_CANCEL_ENTRY_CANCEL_REASON, ENUM);
        final String languageIso = null;
        currentObjectValues.addValue(pd, languageIso, new Long(3)); // quantity should not be 0
        currentObjectValues.addValue(pd2, languageIso, null); //cancel reason should not be null
        orderEntryCancelRecords.add(currentObjectValues);

        currentObjectValues2.addValue(pd, languageIso, new Long(4)); // quantity should not more than the cancelquantity: 10 in this case
        currentObjectValues2.addValue(pd2, languageIso, null); //cancel reason should not be null
        orderEntryCancelRecords.add(currentObjectValues2);

        try {
            final boolean isvalid = controller.validateCreateCancellationRequest(order,
                    cancelableOrderEntries, orderEntryCancelRecords);
            assertFalse(isvalid);
        }
        catch (final ValidationException e) {
            assertEquals(e.getResourceMessages().size(), 2);
        }

    }

    /**
     * Same as above, but for full cancellation request. Which sets maximum allowed delivery cost to return by default.
     */
    @Test
    public void testBuildFullOrderCancelRequest() {
        final String notes = "notes";
        final CancelReason reason = CancelReason.OTHER;
        final RequestOrigin requestOrigin = RequestOrigin.CSCOCKPIT;
        final Double shipToRefund = Double.valueOf(29d);

        final TypedObject typedObject = mock(TypedObject.class);
        when(typedObject.getObject()).thenReturn(orderEntry);

        final PropertyDescriptor descriptor = mockPropertyDescriptorValue("OrderCancelRequest.shippingAmountToRefund",
                shipToRefund);
        when(objectValueContainer.getPropertyDescriptors()).thenReturn(
                ImmutableSet.of(descriptor));
        final TgtCsCardPaymentController tgtCsCardPaymentController = Mockito.mock(TgtCsCardPaymentController.class);
        controller.setTgtCsCardPaymentController(tgtCsCardPaymentController);
        final CreditCardPaymentInfoModel newPayment = Mockito.mock(CreditCardPaymentInfoModel.class);
        when(tgtCsCardPaymentController.getNewPayment()).thenReturn(newPayment);
        when(newPayment.getCode()).thenReturn("00001_userid");
        final UserModel user = Mockito.mock(UserModel.class);
        when(user.getUid()).thenReturn("userid");
        when(newPayment.getUser()).thenReturn(user);
        when(newPayment.getSubscriptionId()).thenReturn("fakesessionid");
        when(order.getUser()).thenReturn(user);
        final PropertyDescriptor descriptor1 = mockPropertyDescriptorValue("OrderCancelRequest.notes", notes);
        final PropertyDescriptor descriptor2 = mockPropertyDescriptorValue("OrderCancelRequest.cancelReason", reason);
        final PropertyDescriptor descriptor3 = mockPropertyDescriptorValue("OrderCancelRequest.requestOrigin",
                requestOrigin);
        when(objectValueContainer.getPropertyDescriptors()).thenReturn(
                ImmutableSet.of(descriptor1, descriptor2, descriptor3));

        doReturn(shipToRefund).when(controller).getMaximumShippingAmountToRefund();

        final TargetOrderCancelRequest result = (TargetOrderCancelRequest)
                controller.buildCancelRequest(order, objectValueContainer);

        assertEquals(notes, result.getNotes());
        assertEquals(reason, result.getCancelReason());
        assertEquals(shipToRefund, result.getShippingAmountToRefund());
        assertEquals(requestOrigin, result.getRequestOrigin());
    }

    /**
     * Verifies that available shipping amount for refund is calculated from order initial delivery cost minus
     * subsequent cancellation records.
     */
    @SuppressWarnings("boxing")
    @Test
    public void testControllerProperlyCalculatesRemainingShippingCostToRefund() {
        when(order.getInitialDeliveryCost()).thenReturn(Double.valueOf(15d));
        when(findOrderRefundedShippingStrategy.getRefundedShippingAmount(order)).thenReturn(Double.valueOf(9d));

        assertEquals(6d, controller.getMaximumShippingAmountToRefund(), .001d);
    }

    /**
     * Verifies that controller validates submitted shipping amount.
     */
    @Test
    public void testShippingAmountValidation() {
        final Double shipToRefund = Double.valueOf(14d);
        final Double availableShipToRefund = Double.valueOf(9d);
        final PropertyDescriptor descriptor =
                mockPropertyDescriptorValue("OrderCancelRequest.shippingAmountToRefund", shipToRefund);

        doReturn(availableShipToRefund).when(controller).getMaximumShippingAmountToRefund();
        when(objectValueContainer.getPropertyDescriptors()).thenReturn(ImmutableSet.of(descriptor));

        try {
            controller.createPartialOrderCancellationRequest(null, objectValueContainer);
            fail();
        }
        catch (final OrderCancelException e) {
            fail();
        }
        catch (final ValidationException e) {
            assertEquals(1, e.getResourceMessages().size());
        }
    }

    /**
     * Convenient method to mock return value from property descriptor.
     * 
     * @param name
     *            the name of the property descriptor
     * @param value
     *            the current value of the property descriptor
     * @return the property descriptor
     */
    private PropertyDescriptor mockPropertyDescriptorValue(final String name, final Object value) {
        final PropertyDescriptor descriptor = mock(PropertyDescriptor.class);
        final ObjectValueContainer.ObjectValueHolder valueHolder = mock(ObjectValueContainer.ObjectValueHolder.class);
        when(descriptor.getQualifier()).thenReturn(name);
        when(objectValueContainer.getValue(eq(descriptor), anyString())).thenReturn(valueHolder);
        when(valueHolder.getCurrentValue()).thenReturn(value);
        return descriptor;
    }
}
