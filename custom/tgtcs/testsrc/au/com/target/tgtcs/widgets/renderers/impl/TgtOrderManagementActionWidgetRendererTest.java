/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;


import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcs.widgets.controllers.impl.TargetOrderManagementActionsWidgetControllerImpl;

/**
 * @author mjanarth
 *
 */
@UnitTest
public class TgtOrderManagementActionWidgetRendererTest {
  

       @Mock
       private TargetOrderManagementActionsWidgetControllerImpl  controller ; 
       @InjectMocks
       private TgtOrderManagementActionsWidgetRenderer renderer = new TgtOrderManagementActionsWidgetRenderer();

       @Before
       public void setup(){
           controller = Mockito.mock(TargetOrderManagementActionsWidgetControllerImpl.class);
           MockitoAnnotations.initMocks(this);
       }
       
       @SuppressWarnings("boxing")
       @Test
       public void testIsHoldOrderButtonDisableWithTrue(){
           BDDMockito.given(controller.isHoldOrderPossible()).willReturn(true);
           final boolean result = renderer.isHoldOrderButtonDisable(controller);
           Assert.assertEquals(result, false);
           
       }
       
       @SuppressWarnings("boxing")
       @Test
       public void testIsHoldOrderButtonDisableWithFalse(){
           BDDMockito.given(controller.isHoldOrderPossible()).willReturn(true);
           final boolean result = renderer.isHoldOrderButtonDisable(controller);
           Assert.assertEquals(result, false);
           
       }
       
       @SuppressWarnings("boxing")
       @Test
       public void testIsReviewButtonDisableWithFraudReviewTrue(){
           BDDMockito.given(controller.isOrderFraudReviewPossible()).willReturn(true);
           final boolean result = renderer.isReviewOrderButtonDisable(controller);
           Assert.assertEquals(result, false);
           
       }
       
       
       @SuppressWarnings("boxing")
       @Test
       public void testIsReviewButtonDisableWithFraudReviewFalse(){
           BDDMockito.given(controller.isOrderFraudReviewPossible()).willReturn(false);
           final boolean result = renderer.isReviewOrderButtonDisable(controller);
           Assert.assertEquals(result, true);
           
       }
       
       
      
}
