/**
 * 
 */
package au.com.target.tgtcs.widgets.adapters;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.widgets.adapters.impl.AbstractWidgetAdapter;
import de.hybris.platform.cockpit.widgets.controllers.WidgetController;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.widgets.controllers.OrderController;
import de.hybris.platform.fraud.model.FraudReportModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcs.widgets.models.impl.TargetAccertifyReportListWidgetModel;


/**
 * @author umesh
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetAccertifyHistoryListWidgetAdapterTest {

    @Mock
    private OrderController orderController;
    @Mock
    private WidgetController widgetController;
    @Mock
    private AbstractWidgetAdapter abstractWidgetAdapter;
    @Mock
    private TypedObject typedObject;
    @Mock
    private OrderModel orderModel;
    @Mock
    private TypeService typeService;
    @Mock
    private TargetAccertifyReportListWidgetModel widgetModel;

    private final TargetAccertifyHistoryListWidgetAdapter adapter = new TargetAccertifyHistoryListWidgetAdapter();

    @Before
    public void setup() {
        adapter.setWidgetController(orderController);
        adapter.setCockpitTypeService(typeService);
        adapter.setWidgetModel(widgetModel);
    }

    @Test
    public void testUpdateModelWithControllerNull() {
        adapter.setWidgetController(null);
        adapter.updateModel();
        Mockito.verify(orderController, Mockito.never()).getCurrentOrder();
    }

    @Test
    public void testUpdateModelWithDiffentController() {
        adapter.setWidgetController(widgetController);
        adapter.updateModel();
        Mockito.verify(orderController, Mockito.never()).getCurrentOrder();
    }

    @Test
    public void testUpdateModelWithOrderController() {
        adapter.setWidgetController(orderController);
        when(abstractWidgetAdapter.getWidgetController()).thenReturn(orderController);
        adapter.updateModel();
        Mockito.verify(orderController, Mockito.times(1)).getCurrentOrder();
    }

    @Test
    public void testUpdateModelWithTypedObjectNull() {
        adapter.setWidgetController(orderController);
        when(abstractWidgetAdapter.getWidgetController()).thenReturn(orderController);
        when(orderController.getCurrentOrder()).thenReturn(null);
        adapter.updateModel();
        Mockito.verify(typedObject, Mockito.never()).getObject();
    }

    @Test
    public void testUpdateModelWithObjectNull() {
        adapter.setWidgetController(orderController);
        when(abstractWidgetAdapter.getWidgetController()).thenReturn(orderController);
        when(orderController.getCurrentOrder()).thenReturn(typedObject);
        when(typedObject.getObject()).thenReturn(null);
        adapter.updateModel();
        Mockito.verify(typedObject, Mockito.times(1)).getObject();
        Mockito.verify(orderModel, Mockito.never()).getFraudReports();
    }

    @Test
    public void testUpdateModelWithFraudReportsNull() {
        adapter.setWidgetController(orderController);
        when(abstractWidgetAdapter.getWidgetController()).thenReturn(orderController);
        when(orderController.getCurrentOrder()).thenReturn(typedObject);
        when(typedObject.getObject()).thenReturn(orderModel);
        when(orderModel.getFraudReports()).thenReturn(null);
        adapter.updateModel();
        Assert.assertEquals(0, widgetModel.getItems().size());
    }

    @Test
    public void testUpdateModelWithFraudReportsEmpty() {
        final Set<FraudReportModel> frauds = new HashSet<>();
        adapter.setWidgetController(orderController);
        when(abstractWidgetAdapter.getWidgetController()).thenReturn(orderController);
        when(orderController.getCurrentOrder()).thenReturn(typedObject);
        when(typedObject.getObject()).thenReturn(orderModel);
        when(orderModel.getFraudReports()).thenReturn(frauds);
        adapter.updateModel();
        Assert.assertEquals(widgetModel.getItems().size(), 0);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testUpdateModelWithFraudReports() {
        final List<TypedObject> list = new ArrayList<>();
        list.add(typedObject);
        final Set<FraudReportModel> frauds = new HashSet<>();
        final FraudReportModel f1 = new FraudReportModel();
        f1.setCode("f123");
        f1.setOrder(orderModel);
        f1.setProvider("Accertify");
        frauds.add(f1);
        adapter.setWidgetController(orderController);
        when(abstractWidgetAdapter.getWidgetController()).thenReturn(orderController);
        when(orderController.getCurrentOrder()).thenReturn(typedObject);
        when(typedObject.getObject()).thenReturn(orderModel);
        when(orderModel.getFraudReports()).thenReturn(frauds);
        when(typeService.wrapItems(frauds)).thenReturn(list);
        when(widgetModel.setItems(list)).thenReturn(Boolean.TRUE);
        Assert.assertTrue(adapter.updateModel());
    }
}