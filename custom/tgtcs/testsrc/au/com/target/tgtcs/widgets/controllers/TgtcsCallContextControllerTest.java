package au.com.target.tgtcs.widgets.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer.ObjectValueHolder;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.cscockpit.widgets.controllers.impl.DefaultCallContextController;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtcs.widgets.controllers.impl.TgtcsCallContextController;
import au.com.target.tgtverifyaddr.TargetAddressVerificationService;
import au.com.target.tgtverifyaddr.data.AddressData;
import au.com.target.tgtverifyaddr.data.CountryEnum;
import au.com.target.tgtverifyaddr.exception.RequestTimeOutException;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


@UnitTest
public class TgtcsCallContextControllerTest {
    private TgtcsCallContextController tgtcsCallContextController;
    @Mock
    private TargetAddressVerificationService targetAddressVerificationService;
    @Mock
    private ObjectValueContainer addressContainer;
    @Mock
    private ObjectValueHolder valueHolderLine1;
    @Mock
    private ObjectValueHolder valueHolderTown;
    @Mock
    private ObjectValueHolder valueHolderDistrict;
    @Mock
    private ObjectValueHolder valueHolderPostalCode;
    @Mock
    private ObjectValueHolder valueHolderCountry;
    @Mock
    private ObjectValueHolder valueHolderAddressValidated;
    @Mock
    private SessionService sessionService;
    @Mock
    private ModelService modelService;
    @Mock
    private TypeService cockpitTypeService;
    @Mock
    private TargetBasketController basketController;
    @Mock
    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;
    @Mock
    private UserService userService;

    @Before
    public void prepare()
    {
        MockitoAnnotations.initMocks(this);
        // set up address Container
        final Set<ObjectValueHolder> allValues = new HashSet<>();
        final PropertyDescriptor propertyDescriptorLine1 = mock(PropertyDescriptor.class);
        when(propertyDescriptorLine1.getQualifier()).thenReturn(TgtcsConstants.QUALIFIER_ADDRESS_LINE1);
        when(valueHolderLine1.getPropertyDescriptor()).thenReturn(propertyDescriptorLine1);
        allValues.add(valueHolderLine1);
        final PropertyDescriptor propertyDescriptorTown = mock(PropertyDescriptor.class);
        when(propertyDescriptorTown.getQualifier()).thenReturn(TgtcsConstants.QUALIFIER_ADDRESS_TOWN);
        when(valueHolderTown.getPropertyDescriptor()).thenReturn(propertyDescriptorTown);
        allValues.add(valueHolderTown);
        final PropertyDescriptor propertyDescriptorDistrict = mock(PropertyDescriptor.class);
        when(propertyDescriptorDistrict.getQualifier()).thenReturn(TgtcsConstants.QUALIFIER_ADDRESS_DISTRICT);
        when(valueHolderDistrict.getPropertyDescriptor()).thenReturn(propertyDescriptorDistrict);
        allValues.add(valueHolderDistrict);
        final PropertyDescriptor propertyDescriptorPostalCode = mock(PropertyDescriptor.class);
        when(propertyDescriptorPostalCode.getQualifier()).thenReturn(TgtcsConstants.QUALIFIER_ADDRESS_POSTALCODE);
        when(valueHolderPostalCode.getPropertyDescriptor()).thenReturn(propertyDescriptorPostalCode);
        allValues.add(valueHolderPostalCode);
        final PropertyDescriptor propertyDescriptorCountry = mock(PropertyDescriptor.class);
        when(propertyDescriptorCountry.getQualifier()).thenReturn(TgtcsConstants.QUALIFIER_ADDRESS_COUNTRY);
        when(valueHolderCountry.getPropertyDescriptor()).thenReturn(propertyDescriptorCountry);
        final CountryModel countryModel = mock(CountryModel.class);
        when(countryModel.getName()).thenReturn("Australia");
        when(valueHolderCountry.getCurrentValue()).thenReturn(countryModel);
        allValues.add(valueHolderCountry);
        final PropertyDescriptor propertyDescriptorAddressValidated = mock(PropertyDescriptor.class);
        when(propertyDescriptorAddressValidated.getQualifier()).thenReturn(
                TgtcsConstants.QUALIFIER_TARGETADDRESS_ADDRESSVALIDATED);
        when(valueHolderAddressValidated.getPropertyDescriptor()).thenReturn(propertyDescriptorAddressValidated);
        allValues.add(valueHolderAddressValidated);
        when(addressContainer.getAllValues()).thenReturn(allValues);

        tgtcsCallContextController = new TgtcsCallContextController();
        tgtcsCallContextController.setTargetAddressVerificationService(targetAddressVerificationService);
        tgtcsCallContextController.setModelService(modelService);
        tgtcsCallContextController.setCockpitTypeService(cockpitTypeService);
        tgtcsCallContextController.setBasketController(basketController);
        tgtcsCallContextController.setUserService(userService);

    }

    @Test
    public void testCheckAddressHasNotMatchedAddress() throws Exception {
        when(valueHolderLine1.getCurrentValue()).thenReturn("kjfsd");
        when(valueHolderTown.getCurrentValue()).thenReturn("kjfsd");
        when(valueHolderDistrict.getCurrentValue()).thenReturn("kjfsd");
        when(valueHolderPostalCode.getCurrentValue()).thenReturn("123");
        when(
                targetAddressVerificationService.verifyAddress("kjfsd, kjfsd, kjfsd, 123, Australia",
                        CountryEnum.AUSTRALIA))
                .thenReturn(new ArrayList<AddressData>());
        Assert.assertEquals(0, tgtcsCallContextController.checkValidAddress(addressContainer).size());
    }

    @Test
    public void testCheckAddressHasMultipleMatchedAddresses() throws Exception {
        when(valueHolderLine1.getCurrentValue()).thenReturn("36 George Street");
        when(valueHolderTown.getCurrentValue()).thenReturn("Millicent");
        when(valueHolderDistrict.getCurrentValue()).thenReturn("SA");
        when(valueHolderPostalCode.getCurrentValue()).thenReturn("5280");
        final List<AddressData> result = new ArrayList<>();
        final AddressData addressData1 = new AddressData(null, "Shop 1 36 George Street, Millicent  SA  5280", null);
        result.add(addressData1);
        final AddressData addressData2 = new AddressData(null, "Shop 2 36 George Street, Millicent  SA  5280", null);
        result.add(addressData2);
        final AddressData addressData3 = new AddressData(null, "Shop 3 36 George Street, Millicent  SA  5280", null);
        result.add(addressData3);
        when(targetAddressVerificationService.verifyAddress("36 George Street, Millicent, SA, 5280, Australia",
                CountryEnum.AUSTRALIA)).thenReturn(result);
        final List<AddressData> actualResult = tgtcsCallContextController.checkValidAddress(addressContainer);
        Assert.assertEquals(3, actualResult.size());
        Assert.assertEquals("Shop 2 36 George Street, Millicent  SA  5280", actualResult.get(1).getPartialAddress());
    }

    @Test
    public void testCheckAddressThrowException() throws Exception {
        when(valueHolderLine1.getCurrentValue()).thenReturn("36 George Street");
        when(valueHolderTown.getCurrentValue()).thenReturn("Millicent");
        when(valueHolderDistrict.getCurrentValue()).thenReturn("SA");
        when(valueHolderPostalCode.getCurrentValue()).thenReturn("5280");
        when(targetAddressVerificationService.verifyAddress("36 George Street, Millicent, SA, 5280, Australia",
                CountryEnum.AUSTRALIA)).thenThrow(new RequestTimeOutException("Time out Exception"));
        final List<AddressData> actualResult = tgtcsCallContextController.checkValidAddress(addressContainer);
        Assert.assertEquals(0, actualResult.size());
    }

    @Test
    public void testUpdateAddressByQASFormattedAddress() {
        final String addressString = "36 George Street, Millicent  SA  5280";
        tgtcsCallContextController.updateAddressByQASFormattedAddress(addressContainer, addressString);
        for (final ObjectValueHolder ovh : addressContainer.getAllValues()) {
            final String qualifier = ovh.getPropertyDescriptor().getQualifier();
            if (qualifier.equals(TgtcsConstants.QUALIFIER_ADDRESS_LINE1)) {
                verify(ovh).setLocalValue("36 George Street");
                verify(ovh).setModified(true);
                continue;
            }
            if (qualifier.equals(TgtcsConstants.QUALIFIER_ADDRESS_TOWN)) {
                verify(ovh).setLocalValue("Millicent");
                verify(ovh).setModified(true);
                continue;
            }
            if (qualifier.equals(TgtcsConstants.QUALIFIER_ADDRESS_DISTRICT)) {
                verify(ovh).setLocalValue("SA");
                verify(ovh).setModified(true);
                continue;
            }
            if (qualifier.equals(TgtcsConstants.QUALIFIER_ADDRESS_POSTALCODE)) {
                verify(ovh).setLocalValue("5280");
                verify(ovh).setModified(true);
                continue;
            }
            if (qualifier.equals(TgtcsConstants.QUALIFIER_TARGETADDRESS_ADDRESSVALIDATED)) {
                verify(ovh).setLocalValue(Boolean.TRUE);
                verify(ovh).setModified(true);
                continue;
            }
        }

    }

    @Test
    public void testCheckValidAddressParameterStringHasNotMatchedAddress() throws Exception {
        final String addressString = "kjfsd, kjfsd, kjfsd, 123";
        when(targetAddressVerificationService.verifyAddress("kjfsd, kjfsd, kjfsd, 123", CountryEnum.AUSTRALIA))
                .thenReturn(new ArrayList<AddressData>());
        Assert.assertEquals(0, tgtcsCallContextController.checkValidAddress(addressString).size());
    }

    @Test
    public void testCheckValidAddressParameterStringHasMultipleMatchedAddress() throws Exception {
        final String addressString = "36 George Street, Millicent, SA, 5280";
        final List<AddressData> result = new ArrayList<>();
        final AddressData addressData1 = new AddressData(null, "Shop 1 36 George Street, Millicent  SA  5280", null);
        result.add(addressData1);
        final AddressData addressData2 = new AddressData(null, "Shop 2 36 George Street, Millicent  SA  5280", null);
        result.add(addressData2);
        final AddressData addressData3 = new AddressData(null, "Shop 3 36 George Street, Millicent  SA  5280", null);
        result.add(addressData3);

        when(targetAddressVerificationService.verifyAddress("36 George Street, Millicent, SA, 5280",
                CountryEnum.AUSTRALIA)).thenReturn(result);
        final List<AddressData> actualResult = tgtcsCallContextController.checkValidAddress(addressString);
        Assert.assertEquals(3, actualResult.size());
        Assert.assertEquals("Shop 2 36 George Street, Millicent  SA  5280", actualResult.get(1).getPartialAddress());
    }

    @Test
    public void testCheckValidAddressParameterStringThrowException() throws Exception {
        final String addressString = "36 George Street, Millicent, SA, 5280";
        when(targetAddressVerificationService.verifyAddress("36 George Street, Millicent, SA, 5280",
                CountryEnum.AUSTRALIA)).thenThrow(new RequestTimeOutException("Time out Exception"));
        final List<AddressData> actualResult = tgtcsCallContextController.checkValidAddress(addressString);
        Assert.assertEquals(0, actualResult.size());
    }

    @Test
    public void setCurrentCustomer() {
        final TgtcsCallContextController spy = Mockito.spy(tgtcsCallContextController);
        final CustomerModel customer = new CustomerModel();
        final String sessionAttribute = "session_checkout_cartbuynow";
        final CartModel checkoutCart = Mockito.mock(CartModel.class);
        when(sessionService.getAttribute(sessionAttribute)).thenReturn(checkoutCart);
        final TypedObject object = cockpitTypeService.wrapItem(customer);

        Mockito.doReturn(Boolean.TRUE).when((DefaultCallContextController)spy).setCurrentCustomer(object);
        final boolean result = spy.setCurrentCustomer(object);
        Assert.assertTrue(result);
    }

    @Test
    public void testTransferAnonymousCartToCustomerWithCurrentCustomer() {

        final TypedObject currentCustomer = Mockito.mock(TypedObject.class);
        final TypedObject newCustomer = Mockito.mock(TypedObject.class);

        final TgtcsCallContextController spy = Mockito.spy(tgtcsCallContextController);
        BDDMockito.given(spy.getCurrentCustomer()).willReturn(currentCustomer);

        spy.setCurrentCustomer(newCustomer);

        Mockito.verifyZeroInteractions(basketController);
        Mockito.verifyZeroInteractions(cockpitTypeService);
    }

    @Test
    public void testTransferAnonymousEmptyCartToCustomerWithNullCurrentCustomer() {

        final TypedObject customer = Mockito.mock(TypedObject.class);
        final TypedObject cartObject = Mockito.mock(TypedObject.class);
        final CartModel cartModel = Mockito.mock(CartModel.class);

        final TgtcsCallContextController spy = Mockito.spy(tgtcsCallContextController);
        BDDMockito.given(spy.getCurrentCustomer()).willReturn(null);
        BDDMockito.doReturn("cartCode").when(spy).getAnonymousCartCode();
        BDDMockito.given(basketController.getMasterCart()).willReturn(cartModel);
        BDDMockito.given(cockpitTypeService.wrapItem(cartModel)).willReturn(cartObject);

        spy.setCurrentCustomer(customer);

        Mockito.verify(basketController, Mockito.never()).transferAnonymousCartToCustomer("cartCode", customer);
    }

    @Test
    public void testTransferNonAnonymousCartToCustomerWithNullCurrentCustomer() {

        final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
        final CustomerModel anonymousCustomer = Mockito.mock(CustomerModel.class);
        final TypedObject customer = Mockito.mock(TypedObject.class);
        final TypedObject cartObject = Mockito.mock(TypedObject.class);
        final AbstractOrderEntryModel cartEntryModel = Mockito.mock(AbstractOrderEntryModel.class);
        final CartModel cartModel = Mockito.mock(CartModel.class);

        final TgtcsCallContextController spy = Mockito.spy(tgtcsCallContextController);
        BDDMockito.given(spy.getCurrentCustomer()).willReturn(null);
        BDDMockito.doReturn("cartCode").when(spy).getAnonymousCartCode();
        BDDMockito.given(cartModel.getEntries()).willReturn(Collections.singletonList(cartEntryModel));
        BDDMockito.given(cartModel.getUser()).willReturn(customerModel);
        BDDMockito.given(basketController.getMasterCart()).willReturn(cartModel);
        BDDMockito.given(cockpitTypeService.wrapItem(cartModel)).willReturn(cartObject);
        BDDMockito.given(cartObject.getObject()).willReturn(cartModel);
        BDDMockito.given(userService.getAnonymousUser()).willReturn(anonymousCustomer);

        spy.setCurrentCustomer(customer);

        Mockito.verify(basketController, Mockito.never()).transferAnonymousCartToCustomer("cartCode", customer);
    }

    @Test
    public void testTransferAnonymousCartToCustomerWithNullCurrentCustomer() {

        final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
        final TypedObject customer = Mockito.mock(TypedObject.class);
        final TypedObject cartObject = Mockito.mock(TypedObject.class);
        final AbstractOrderEntryModel cartEntryModel = Mockito.mock(AbstractOrderEntryModel.class);
        final CartModel cartModel = Mockito.mock(CartModel.class);

        final TgtcsCallContextController spy = Mockito.spy(tgtcsCallContextController);
        BDDMockito.given(spy.getCurrentCustomer()).willReturn(null);
        BDDMockito.doReturn("cartCode").when(spy).getAnonymousCartCode();
        BDDMockito.given(cartModel.getEntries()).willReturn(Collections.singletonList(cartEntryModel));
        BDDMockito.given(cartModel.getUser()).willReturn(customerModel);
        BDDMockito.given(basketController.getMasterCart()).willReturn(cartModel);
        BDDMockito.given(cockpitTypeService.wrapItem(cartModel)).willReturn(cartObject);
        BDDMockito.given(cartObject.getObject()).willReturn(cartModel);
        BDDMockito.given(userService.getAnonymousUser()).willReturn(customerModel);

        spy.setCurrentCustomer(customer);

        Mockito.verify(basketController).transferAnonymousCartToCustomer("cartCode", customer);
    }
}
