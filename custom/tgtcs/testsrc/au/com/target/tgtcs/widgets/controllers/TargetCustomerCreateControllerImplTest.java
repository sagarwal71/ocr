/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.model.meta.impl.ItemAttributePropertyDescriptor;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer.ObjectValueHolder;
import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.i18n.L10NService;
import de.hybris.platform.servicelayer.interceptor.Interceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.InterceptorRegistry;
import de.hybris.platform.servicelayer.internal.converter.ConverterRegistry;
import de.hybris.platform.servicelayer.internal.converter.ModelConverter;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import de.hybris.platform.validation.interceptors.ValidationInterceptor;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcs.widgets.controllers.impl.TargetCustomerCreateControllerImpl;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCustomerCreateControllerImplTest {

    private static final String TYPE_CODE = "TargetCustomer";

    @Mock
    private ObjectValueContainer customerObjectValueContainer;
    @Mock
    private DefaultModelService modelService;
    @Mock
    private TypeService typeService;
    @Mock
    private ConverterRegistry converterRegistry;
    @Mock
    private ModelConverter modelConverter;
    @Mock
    private InterceptorRegistry interceptorRegistry;
    @Mock
    private TargetCustomerModel customer;
    @Mock
    private L10NService l10NService;
    @Mock
    private CustomerAccountService customerAccountService;
    @Mock
    private CustomerNameStrategy mockCustomerNameStrategy;
    @InjectMocks
    private final TargetCustomerCreateControllerImpl targetCustomerCreateControllerImpl = new TargetCustomerCreateControllerImpl();

    @Before
    public void setup() {
        customer = mock(TargetCustomerModel.class);

        setupCustomerObjectValueContainer(customerObjectValueContainer);

        BDDMockito.when(l10NService.getLocalizedString("type.targetcustomer.phonenumber.name")).thenReturn(
                "Phone Number");

        BDDMockito.when(l10NService.getLocalizedString("validation.message.customer.title")).thenReturn(
                "Title");

        doReturn(interceptorRegistry).when(modelService).lookupInterceptorRegistry();
        doReturn(converterRegistry).when(modelService).lookupConverterRegistry();
        given(converterRegistry.getModelConverterByModelType((Class)anyObject())).willReturn(modelConverter);
        given(converterRegistry.getModelConverterBySourceType((String)anyObject())).willReturn(modelConverter);

        given(modelConverter.getType(anyObject())).willReturn("TargetCustomerModel");
        given(modelService.getModelType(anyObject())).willReturn("TargetCustomerModel");

        given(modelService.create(TargetCustomerModel.class)).willReturn(customer);

    }

    private void setupCustomerObjectValueContainer(final ObjectValueContainer localCustomerObjectValueContainer) {
        final Set<ObjectValueHolder> allValues = new HashSet<>();
        final Set<PropertyDescriptor> propertyDescriptors = new HashSet<>();

        final ObjectValueHolder valueHolderPhoneNumber = mock(ObjectValueHolder.class);
        final PropertyDescriptor propertyDescriptorPhone = mock(ItemAttributePropertyDescriptor.class);
        when(propertyDescriptorPhone.getQualifier()).thenReturn("TargetCustomer.phoneNumber");
        propertyDescriptors.add(propertyDescriptorPhone);
        when(valueHolderPhoneNumber.getPropertyDescriptor()).thenReturn(propertyDescriptorPhone);
        when(valueHolderPhoneNumber.getCurrentValue()).thenReturn("123456");
        allValues.add(valueHolderPhoneNumber);

        final ObjectValueHolder valueHolderUid = mock(ObjectValueHolder.class);
        final PropertyDescriptor propertyDescriptorUid = mock(ItemAttributePropertyDescriptor.class);
        when(propertyDescriptorUid.getQualifier()).thenReturn("TargetCustomer.uid");
        propertyDescriptors.add(propertyDescriptorUid);
        when(valueHolderUid.getPropertyDescriptor()).thenReturn(propertyDescriptorUid);
        when(valueHolderUid.getCurrentValue()).thenReturn("tgt_test@abc.com");
        allValues.add(valueHolderUid);

        final ObjectValueHolder valueHolderFirstName = mock(ObjectValueHolder.class);
        final PropertyDescriptor propertyDescriptorFirstName = mock(ItemAttributePropertyDescriptor.class);
        when(propertyDescriptorFirstName.getQualifier()).thenReturn("TargetCustomer.firstname");
        propertyDescriptors.add(propertyDescriptorFirstName);
        when(valueHolderFirstName.getPropertyDescriptor()).thenReturn(propertyDescriptorUid);
        when(valueHolderFirstName.getCurrentValue()).thenReturn("FirstName");
        allValues.add(valueHolderFirstName);

        final ObjectValueHolder valueHolderLastName = mock(ObjectValueHolder.class);
        final PropertyDescriptor propertyDescriptorLastName = mock(ItemAttributePropertyDescriptor.class);
        when(propertyDescriptorLastName.getQualifier()).thenReturn("TargetCustomer.lastname");
        propertyDescriptors.add(propertyDescriptorLastName);
        when(valueHolderLastName.getPropertyDescriptor()).thenReturn(propertyDescriptorUid);
        when(valueHolderLastName.getCurrentValue()).thenReturn("LastName");
        allValues.add(valueHolderLastName);

        final ObjectValueHolder valueHolderName = mock(ObjectValueHolder.class);
        final PropertyDescriptor propertyDescriptorName = mock(ItemAttributePropertyDescriptor.class);
        when(propertyDescriptorName.getQualifier()).thenReturn("Principal.name");
        propertyDescriptors.add(propertyDescriptorName);
        when(valueHolderName.getPropertyDescriptor()).thenReturn(propertyDescriptorUid);
        when(valueHolderName.getCurrentValue()).thenReturn("Name");
        allValues.add(valueHolderName);

        when(localCustomerObjectValueContainer.getAllValues()).thenReturn(allValues);
        when(localCustomerObjectValueContainer.getPropertyDescriptors()).thenReturn(propertyDescriptors);

        final ObjectValueContainer.ObjectValueHolder mockNameHolder = mock(ObjectValueContainer.ObjectValueHolder.class);
        when(localCustomerObjectValueContainer.getValue(propertyDescriptorName, null)).thenReturn(mockNameHolder);

        final ObjectValueContainer.ObjectValueHolder mockFirstNameHolder = mock(ObjectValueContainer.ObjectValueHolder.class);
        when(localCustomerObjectValueContainer.getValue(propertyDescriptorFirstName, null)).thenReturn(
                mockFirstNameHolder);

        final ObjectValueContainer.ObjectValueHolder mockLastNameHolder = mock(ObjectValueContainer.ObjectValueHolder.class);
        when(localCustomerObjectValueContainer.getValue(propertyDescriptorLastName, null)).thenReturn(
                mockLastNameHolder);
    }

    @Test
    public void testCustomerSaving() throws ValueHandlerException, DuplicateUidException {
        BDDMockito.when(customer.getPhoneNumber()).thenReturn("982378003");
        final TitleModel title = mock(TitleModel.class);
        BDDMockito.when(customer.getTitle()).thenReturn(title);
        targetCustomerCreateControllerImpl.createNewCustomer(customerObjectValueContainer,
                TYPE_CODE);
        BDDMockito.verify(customerAccountService).register(customer, null);
    }

    @Test(expected = ValueHandlerException.class)
    public void testCustomerSavingInterceptor() throws ValueHandlerException, DuplicateUidException {
        final InterceptorException interceptorException = new InterceptorException("Invalid message");
        final Interceptor interceptor = new ValidationInterceptor();
        interceptorException.setInterceptor(interceptor);
        final Throwable exception = new ModelSavingException("Save Error!", interceptorException);
        Mockito.doThrow(exception).when(modelService).save(customer);
        targetCustomerCreateControllerImpl.createNewCustomer(customerObjectValueContainer,
                TYPE_CODE);
        Assert.assertTrue("not match exception", "Invalid message".equals(exception.getMessage()));

        BDDMockito.verifyZeroInteractions(customerAccountService);
    }

    @Test(expected = ValueHandlerException.class)
    public void testCustomerSavingError() throws ValueHandlerException, DuplicateUidException {
        final Throwable exception = new ModelSavingException("Save Error!");
        Mockito.doThrow(exception).when(modelService).save(customer);
        targetCustomerCreateControllerImpl.createNewCustomer(customerObjectValueContainer,
                TYPE_CODE);

        BDDMockito.verifyZeroInteractions(customerAccountService);
    }

    @Test(expected = ValueHandlerException.class)
    public void testCustomerSavingErrorInvalidPhoneNumber() throws ValueHandlerException, DuplicateUidException {
        BDDMockito.when(customer.getPhoneNumber()).thenReturn("ABC");
        targetCustomerCreateControllerImpl.createNewCustomer(customerObjectValueContainer,
                TYPE_CODE);

        BDDMockito.verifyZeroInteractions(customerAccountService);
    }

    @Test(expected = ValueHandlerException.class)
    public void testCustomerSavingErrorInvalidTitle() throws ValueHandlerException, DuplicateUidException {
        BDDMockito.when(customer.getPhoneNumber()).thenReturn("982378003");
        BDDMockito.when(customer.getTitle()).thenReturn(null);

        targetCustomerCreateControllerImpl.createNewCustomer(customerObjectValueContainer,
                TYPE_CODE);

        BDDMockito.verifyZeroInteractions(customerAccountService);
    }
}
