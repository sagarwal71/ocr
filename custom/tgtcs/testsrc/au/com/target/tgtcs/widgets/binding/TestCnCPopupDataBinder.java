package au.com.target.tgtcs.widgets.binding;

import static org.junit.Assert.assertEquals;
import static org.mockito.Answers.CALLS_REAL_METHODS;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.TitleModel;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Textbox;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel;


@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest(Events.class)
public class TestCnCPopupDataBinder
{
    private static final String FIRSTNAME = "TestFirstName";
    private static final String LASTNAME = "TestLastName";
    private static final String PHONE = "12312312378";
    private static final String STATE = "TEST";

    private CnCPopupDataBinder testInstance;

    private SelectCnCStoreWidgetModel model;

    private Combobox titlebox;
    private Textbox firstNamebox;
    private Textbox lastNamebox;
    private Textbox phonebox;
    private Combobox statebox;
    private Combobox storebox;

    private TitleModel title;

    private TargetPointOfServiceModel store;

    @Before
    public void setUp()
    {
        // mock Events class
        mockStatic(Events.class, RETURNS_DEEP_STUBS.get());

        // mock model
        model = new SelectCnCStoreWidgetModel();

        // mock title 
        titlebox = mock(Combobox.class, CALLS_REAL_METHODS.get());
        title = mock(TitleModel.class);

        when(title.getName()).thenReturn("Ts");
        final Comboitem selectedTitle = new Comboitem(title.getName());
        selectedTitle.setValue(title);
        when(titlebox.getChildren()).thenReturn(Collections.singletonList(selectedTitle));

        // mock first name
        firstNamebox = mock(Textbox.class, CALLS_REAL_METHODS.get());

        // mock last name
        lastNamebox = mock(Textbox.class, CALLS_REAL_METHODS.get());

        // mock phone 
        phonebox = mock(Textbox.class, CALLS_REAL_METHODS.get());

        // mock state
        statebox = mock(Combobox.class, CALLS_REAL_METHODS.get());

        final Comboitem selectedState = new Comboitem(STATE);
        selectedState.setValue(STATE);
        when(statebox.getChildren()).thenReturn(Collections.singletonList(selectedState));

        // mock store 
        storebox = mock(Combobox.class, CALLS_REAL_METHODS.get());
        store = mock(TargetPointOfServiceModel.class);

        final Comboitem selectedStore = new Comboitem(store.getName());
        selectedStore.setValue(store);
        when(storebox.getChildren()).thenReturn(Collections.singletonList(selectedStore));
    }

    public void putMockDataIntoUIComponents()
    {
        final Comboitem selectedTitle = new Comboitem(title.getName());
        selectedTitle.setValue(title);
        when(titlebox.getSelectedItem()).thenReturn(selectedTitle);

        when(firstNamebox.getValue()).thenReturn(FIRSTNAME);

        when(lastNamebox.getValue()).thenReturn(LASTNAME);

        when(phonebox.getValue()).thenReturn(PHONE);

        final Comboitem selectedState = new Comboitem(STATE);
        selectedState.setValue(STATE);
        when(statebox.getSelectedItem()).thenReturn(selectedState);

        final Comboitem selectedStore = new Comboitem(store.getName());
        selectedStore.setValue(store);
        when(storebox.getSelectedItem()).thenReturn(selectedStore);
    }

    public void putMockDataIntoModel()
    {
        model.setTitle(title);
        model.setFirstName(FIRSTNAME);
        model.setLastName(LASTNAME);
        model.setPhone(PHONE);
        model.setState(STATE);
        model.setStore(store);
    }

    @Test
    public void testSave()
    {
        putMockDataIntoUIComponents();

        // construct test binder instance
        testInstance = new CnCPopupDataBinder(model, titlebox, firstNamebox, lastNamebox, phonebox, statebox, storebox);

        testInstance.saveAll();

        assertEquals("Unexpected title", model.getTitle(), title);
        assertEquals("Unexpected first name", model.getFirstName(), FIRSTNAME);
        assertEquals("Unexpected last name", model.getLastName(), LASTNAME);
        assertEquals("Unexpected phone number", model.getPhone(), PHONE);
        assertEquals("Unexpected district", model.getState(), STATE);
        assertEquals("Unexpected store", model.getStore(), store);
    }

    @Test
    public void testLoad()
    {
        putMockDataIntoModel();

        // construct test binder instance
        testInstance = new CnCPopupDataBinder(model, titlebox, firstNamebox, lastNamebox, phonebox, statebox, storebox);

        testInstance.loadAll();

        assertEquals("Unexpected title", titlebox.getSelectedItem().getValue(), title);
        assertEquals("Unexpected first name", firstNamebox.getValue(), FIRSTNAME);
        assertEquals("Unexpected last name", lastNamebox.getValue(), LASTNAME);
        assertEquals("Unexpected phone number", phonebox.getValue(), PHONE);
        assertEquals("Unexpected district", statebox.getSelectedItem().getValue(), STATE);
        assertEquals("Unexpected store", storebox.getSelectedItem().getValue(), store);
    }

    @Test
    public void testNullPhoneSave()
    {
        putMockDataIntoUIComponents();

        phonebox.setValue(null);

        // construct test binder instance
        testInstance = new CnCPopupDataBinder(model, titlebox, firstNamebox, lastNamebox, phonebox, statebox, storebox);

        testInstance.saveAll();

        assertEquals("Unexpected title", model.getTitle(), title);
        assertEquals("Unexpected first name", model.getFirstName(), FIRSTNAME);
        assertEquals("Unexpected last name", model.getLastName(), LASTNAME);
        assertEquals("Unexpected phone number", model.getPhone(), PHONE);
        assertEquals("Unexpected district", model.getState(), STATE);
        assertEquals("Unexpected store", model.getStore(), store);
    }

    @Test
    public void testEmptyStoreSave()
    {
        putMockDataIntoUIComponents();

        storebox.setSelectedIndex(-1);

        // construct test binder instance
        testInstance = new CnCPopupDataBinder(model, titlebox, firstNamebox, lastNamebox, phonebox, statebox, storebox);

        testInstance.saveAll();

        assertEquals("Unexpected title", model.getTitle(), title);
        assertEquals("Unexpected first name", model.getFirstName(), FIRSTNAME);
        assertEquals("Unexpected last name", model.getLastName(), LASTNAME);
        assertEquals("Unexpected phone number", model.getPhone(), PHONE);
        assertEquals("Unexpected district", model.getState(), STATE);
        assertEquals("Unexpected store", model.getStore(), store);
    }
}
