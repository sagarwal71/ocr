/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.config.ColumnConfiguration;
import de.hybris.platform.cockpit.services.config.impl.PropertyColumnConfiguration;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.widgets.impl.DefaultListboxWidget;
import de.hybris.platform.cscockpit.utils.ObjectGetValueUtils;
import de.hybris.platform.cscockpit.widgets.controllers.OrderController;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zul.Listbox;

import au.com.target.tgtcs.widgets.models.impl.TargetAccertifyReportListWidgetModel;



/**
 * @author umesh
 * 
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest({ ObjectGetValueUtils.class, UISessionUtils.class })
public class TargetAccertifyHistoryRendererTest {
    private final TargetAccertifyHistoryRenderer renderer = new TargetAccertifyHistoryRenderer();

    @Mock
    private TargetAccertifyReportListWidgetModel widgetModel;
    @Mock
    private OrderController orderController;
    @Mock
    private HtmlBasedComponent rootContainer;
    @Mock
    private DefaultListboxWidget<TargetAccertifyReportListWidgetModel, OrderController> widget;
    @Mock
    private TypedObject typedObject;
    @Mock
    private PropertyColumnConfiguration propertyColumnConfiguration;

    @Before
    public void setup() {
        PowerMockito.mockStatic(ObjectGetValueUtils.class, Mockito.RETURNS_MOCKS);
        PowerMockito.mockStatic(UISessionUtils.class, Mockito.RETURNS_MOCKS);
    }

    @Test
    public void testCreateContentInternal() {
        when(widget.getWidgetModel()).thenReturn(widgetModel);
        final HtmlBasedComponent component = renderer.createContentInternal(widget, rootContainer);
        Assert.assertEquals(1, component.getChildren().size());
    }

    @Test
    public void testRenderAccertifyHistoryWithNoColums() {
        final Listbox list = getListBox();
        renderer.renderAccertifyHistory(widget, typedObject, list, null);
        Assert.assertEquals(1, list.getChildren().size());
    }

    @Test
    public void testRenderAccertifyHistoryWithNoList() {
        final List<ColumnConfiguration> colList = getColumns();
        renderer.renderAccertifyHistory(widget, typedObject, null, colList);
        Mockito.verify(typedObject, Mockito.never()).getType();
    }

    @Test
    public void testRenderListboxWithItems() {
        final Listbox list = getListBox();
        final List<TypedObject> typedList = new ArrayList<>();
        typedList.add(typedObject);
        when(widget.getWidgetModel()).thenReturn(widgetModel);
        when(widgetModel.getItems()).thenReturn(typedList);
        renderer.renderListbox(list, widget, rootContainer);
        Assert.assertEquals(2, list.getChildren().size());
    }

    @Test
    public void testRenderListboxWithEmptyItems() {
        final Listbox list = getListBox();
        when(widget.getWidgetModel()).thenReturn(widgetModel);
        when(widgetModel.getItems()).thenReturn(null);
        renderer.renderListbox(list, widget, rootContainer);
        Assert.assertEquals(1, list.getChildren().size());
    }

    @Test
    public void testRenderAccertifyHistory() {
        final Listbox list = getListBox();
        final List<ColumnConfiguration> colList = getColumns();
        renderer.renderAccertifyHistory(widget, typedObject, list, colList);
    }

    private List<ColumnConfiguration> getColumns() {
        final List<ColumnConfiguration> columnConfigurations = new ArrayList<>();
        columnConfigurations.add(propertyColumnConfiguration);
        return columnConfigurations;
    }

    private Listbox getListBox() {
        final Listbox listbox = new Listbox();
        return listbox;
    }
}
