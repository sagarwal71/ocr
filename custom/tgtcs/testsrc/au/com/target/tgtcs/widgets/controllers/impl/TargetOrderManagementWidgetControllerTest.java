/**
 *
 */
package au.com.target.tgtcs.widgets.controllers.impl;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtfraud.impl.DefaultTargetFraudService;
import org.junit.Assert;


/**
 * @author mjanarth
 *
 */
@UnitTest
public class TargetOrderManagementWidgetControllerTest {

    @Mock
    private OrderModel model;
    @Mock
    private DefaultTargetFraudService fraudService;
    @Mock
    private TypedObject typedObject;
    @Mock
    private FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    private final ArrayList<SalesApplication> disableManualRefundForSalesApplications = new ArrayList<>();
    @Spy
    @InjectMocks
    private final TargetOrderManagementActionsWidgetControllerImpl controller = new TargetOrderManagementActionsWidgetControllerImpl();

    @Before
    public void prepare() {

        MockitoAnnotations.initMocks(this);
        typedObject = mock(TypedObject.class);
        when(typedObject.getObject()).thenReturn(model);
        doReturn(typedObject).when(controller).getOrder();
        disableManualRefundForSalesApplications.add(SalesApplication.TRADEME);
        controller.setDisableManualRefundForSalesApplications(disableManualRefundForSalesApplications);
    }

    @Test
    public void testIsReviewPossible() {
        controller.isOrderFraudReviewPossible();
        Mockito.verify(fraudService, Mockito.atLeastOnce()).isOrderReviewable(model);

    }


    @SuppressWarnings("boxing")
    @Test
    public void testIsReviewPossibleWithNonReviewableFraudStatus() {
        when(fraudService.isOrderReviewable(model)).thenReturn(false);
        final boolean result = controller.isOrderFraudReviewPossible();
        Assert.assertFalse(result);
        Mockito.verify(fraudService, Mockito.atLeastOnce()).isOrderReviewable(model);

    }

    @SuppressWarnings("boxing")
    @Test
    public void testIsReviewPossibleWithOrderStatusReview() {
        when(fraudService.isOrderReviewable(model)).thenReturn(true);
        final boolean result = controller.isOrderFraudReviewPossible();
        Assert.assertTrue(result);
        Mockito.verify(fraudService, Mockito.atLeastOnce()).isOrderReviewable(model);

    }

    @Test
    public void testIsHoldOnReview() {
        controller.isHoldOrderPossible();
        Mockito.verify(fraudService, Mockito.atLeastOnce()).isOrderReviewable(model);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testIsHoldOnReviewWithOrderReviewStatus() {
        when(model.getStatus()).thenReturn(OrderStatus.REVIEW);
        when(fraudService.isOrderReviewable(model)).thenReturn(true);
        final boolean result = controller.isHoldOrderPossible();
        Assert.assertTrue(result);
        Mockito.verify(fraudService, Mockito.atLeastOnce()).isOrderReviewable(model);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testIsHoldOnReviewWithOrderRejectedStatus() {
        when(model.getStatus()).thenReturn(OrderStatus.REJECTED);
        when(fraudService.isOrderReviewable(model)).thenReturn(false);
        final boolean result = controller.isHoldOrderPossible();
        Assert.assertFalse(result);
        Mockito.verify(fraudService, Mockito.atLeastOnce()).isOrderReviewable(model);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testIsHoldOnReviewWithOrderCreatedStatus() {
        when(model.getStatus()).thenReturn(OrderStatus.CREATED);
        when(fraudService.isOrderReviewable(model)).thenReturn(false);
        final boolean result = controller.isHoldOrderPossible();
        Assert.assertFalse(result);
        Mockito.verify(fraudService, Mockito.atLeastOnce()).isOrderReviewable(model);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testIsHoldOnReviewWithOrderHoldStatus() {
        when(model.getStatus()).thenReturn(OrderStatus.REVIEW_ON_HOLD);
        when(fraudService.isOrderReviewable(model)).thenReturn(true);
        final boolean result = controller.isHoldOrderPossible();
        Assert.assertFalse(result);
        Mockito.verify(fraudService, Mockito.atLeastOnce()).isOrderReviewable(model);
    }

    @Test
    public void testIsManualRefundPossibleWithoutRefundableAmount() {
        final Double orderTotalPrice = Double.valueOf(20d);
        final Double paidSoFar = Double.valueOf(20d);
        when(model.getTotalPrice()).thenReturn(orderTotalPrice);
        when(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(model)).thenReturn(paidSoFar);
        final boolean result = controller.isManualRefundPossible();
        Assert.assertFalse(result);
    }

    @Test
    public void testIsManualRefundPossibleWithRefundableAmount() {
        final Double orderTotalPrice = Double.valueOf(20d);
        final Double paidSoFar = Double.valueOf(30d);
        when(model.getTotalPrice()).thenReturn(orderTotalPrice);
        when(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(model)).thenReturn(paidSoFar);
        final boolean result = controller.isManualRefundPossible();
        Assert.assertTrue(result);
    }

    @Test
    public void testIsManualRefundPossibleTradeMeOrder() {
        when(model.getSalesApplication()).thenReturn(SalesApplication.TRADEME);
        Mockito.verifyZeroInteractions(findOrderTotalPaymentMadeStrategy);
        final boolean result = controller.isManualRefundPossible();
        Assert.assertFalse(result);
    }

    @Test
    public void testIsManualRefundPossibleWebOrder() {
        final Double orderTotalPrice = Double.valueOf(20d);
        final Double paidSoFar = Double.valueOf(30d);
        when(model.getTotalPrice()).thenReturn(orderTotalPrice);
        when(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(model)).thenReturn(paidSoFar);
        when(model.getSalesApplication()).thenReturn(SalesApplication.WEB);
        Mockito.verifyZeroInteractions(findOrderTotalPaymentMadeStrategy);
        final boolean result = controller.isManualRefundPossible();
        Assert.assertTrue(result);
    }
}
