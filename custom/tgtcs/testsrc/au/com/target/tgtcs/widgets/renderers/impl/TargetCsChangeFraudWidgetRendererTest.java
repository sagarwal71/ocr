/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cockpit.widgets.impl.DefaultInputWidget;
import de.hybris.platform.cockpit.widgets.models.WidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.utils.PopupWidgetHelper;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.zkoss.zk.ui.api.HtmlBasedComponent;

import au.com.target.tgtcs.widgets.controllers.impl.TargetAbstractFraudStatusWidgetController;
import au.com.target.tgtcs.widgets.controllers.impl.TargetApproveFraudStatusWidgetController;
import org.junit.Assert;


/**
 * @author mjanarth
 *
 */
@UnitTest
public class TargetCsChangeFraudWidgetRendererTest {
    @Mock
    private PopupWidgetHelper popupWidgetHelper;
    @Mock
    private ModelService modelService;
    @Mock
    private TypeService typeService;
    private final TargetCsChangeFraudStatusWidgetRenderer renderer = new TargetCsChangeFraudStatusWidgetRenderer();

    @Before
    public void prepare() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateCaption() {
        final Widget<WidgetModel, TargetAbstractFraudStatusWidgetController> widget = Mockito
                .mock(DefaultInputWidget.class);
        BDDMockito.given(widget.getWidgetTitle()).willReturn("title");
        final HtmlBasedComponent component = renderer.createCaption(widget);
        Assert.assertNotNull(component);

    }

    @Test
    public void testCreateOrderNumber() {
        final TargetApproveFraudStatusWidgetController controller = Mockito
                .mock(TargetApproveFraudStatusWidgetController.class);
        final Widget<WidgetModel, TargetAbstractFraudStatusWidgetController> widget = Mockito
                .mock(DefaultInputWidget.class);
        BDDMockito.given(widget.getWidgetController()).willReturn(controller);
        final HtmlBasedComponent component = renderer.createOrderNumber(widget);
        Assert.assertNotNull(component);

    }

    @Test
    public void testCreateContent() {
        final TargetApproveFraudStatusWidgetController controller = Mockito
                .mock(TargetApproveFraudStatusWidgetController.class);
        final Widget<WidgetModel, TargetAbstractFraudStatusWidgetController> widget = Mockito
                .mock(DefaultInputWidget.class);
        BDDMockito.given(widget.getWidgetController()).willReturn(controller);
        final HtmlBasedComponent component = renderer.createContent(widget);
        Assert.assertNotNull(component);

    }
}
