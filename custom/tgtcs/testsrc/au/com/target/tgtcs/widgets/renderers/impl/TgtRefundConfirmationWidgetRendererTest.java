/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.ObjectTemplate;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.config.ColumnConfiguration;
import de.hybris.platform.cockpit.services.config.impl.PropertyColumnConfiguration;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.widgets.ListboxWidget;
import de.hybris.platform.cockpit.widgets.models.impl.DefaultListWidgetModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.utils.CockpitUiConfigLoader;
import de.hybris.platform.cscockpit.utils.PropertyRendererHelper;
import de.hybris.platform.cscockpit.widgets.controllers.ReturnsController;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;


/**
 * @author Nandini
 * 
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest({ UISessionUtils.class, CockpitUiConfigLoader.class, Messagebox.class, CollectionUtils.class })
@PowerMockIgnore({ "org.apache.logging.log4j.*" })
public class TgtRefundConfirmationWidgetRendererTest {
    @InjectMocks
    private final TgtRefundConfirmationWidgetRenderer returnRequest = new TgtRefundConfirmationWidgetRenderer();

    @Mock
    private HtmlBasedComponent rootContainer;

    @Mock
    private DefaultListWidgetModel defaultListWidgetModel;

    @Mock
    private ReturnsController returnsController;

    @Mock
    private TypedObject typedObject;

    @Mock
    private TypeService typeService;

    @Mock
    private ObjectTemplate objectTemplate;

    @Mock
    private OrderModel orderModel;

    @Mock
    private List<AbstractOrderEntryModel> orderEntry;

    @Mock
    private Map<TypedObject, Long> entries;

    @Mock
    private PropertyColumnConfiguration propertyColumnConfiguration;

    @Mock
    private OrderEntryModel entryModel;

    @Mock
    private List<ReturnRequestModel> returnRequestModel;

    @Mock
    private ReturnEntryModel returnEntryModel;


    @Mock
    private PropertyDescriptor propertyDescriptor;

    @Mock
    private PropertyRendererHelper rendererHelper;

    @Mock
    private ReturnRequestModel requestModel;

    @Mock
    private List<ReturnEntryModel> entryList;

    @Mock
    private ListboxWidget<DefaultListWidgetModel<TypedObject>, ReturnsController> widget;

    @Before
    public void setUp() {
        PowerMockito.mockStatic(CockpitUiConfigLoader.class, Mockito.RETURNS_MOCKS);
        PowerMockito.mockStatic(CollectionUtils.class, Mockito.RETURNS_MOCKS);
    }

    @Test
    public void testRenderListbox() {
        final Listbox listBox = mock(Listbox.class);

        final List<ColumnConfiguration> columnConfigurations = new ArrayList<>();
        columnConfigurations.add(propertyColumnConfiguration);
        final List columns = new ArrayList();
        columns.add(propertyColumnConfiguration);
        given(widget.getWidgetModel()).willReturn(defaultListWidgetModel);
        given(widget.getWidgetController()).willReturn(returnsController);
        given(typeService.getObjectTemplate("objectTemplate")).willReturn(objectTemplate);
        given(widget.getWidgetController().getRefundOrderPreview()).willReturn(typedObject);
        given(typedObject.getObject()).willReturn(orderModel);
        given(orderModel.getEntries()).willReturn(orderEntry);
        given(propertyColumnConfiguration.getPropertyDescriptor()).willReturn(propertyDescriptor);
        Assert.assertNotNull(defaultListWidgetModel);
        Assert.assertNotNull(typedObject);

        returnRequest.renderListbox(listBox, widget, rootContainer);
        Mockito.verify(returnsController).getRefundOrderPreview();
        Mockito.verify(orderModel, Mockito.times(3)).getEntries();
    }

    @Test
    public void testGetRefundQTy() {
        final ReturnEntryModel returnEntryModell = mock(ReturnEntryModel.class);
        returnEntryModell.setCreationtime(new Date());
        final List<ReturnEntryModel> returnmodels = new ArrayList<>();
        returnmodels.add(returnEntryModell);
        given(entryModel.getOrder()).willReturn(orderModel);
        given(orderModel.getReturnRequests()).willReturn(returnRequestModel);
        given(orderModel.getReturnRequests().get(0)).willReturn(requestModel);
        given(orderModel.getReturnRequests().get(0).getReturnEntries()).willReturn(returnmodels);
        given(returnEntryModel.getOrderEntry()).willReturn(entryModel);
        returnRequest.getRefundQTy(entryModel);
    }
}
