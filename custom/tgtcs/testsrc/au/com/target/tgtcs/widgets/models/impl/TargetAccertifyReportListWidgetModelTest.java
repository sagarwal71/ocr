/**
 * 
 */
package au.com.target.tgtcs.widgets.models.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.TypedObject;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;



/**
 * @author umesh
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetAccertifyReportListWidgetModelTest {
    private final TargetAccertifyReportListWidgetModel widgetModel = new TargetAccertifyReportListWidgetModel();

    @Mock
    private TypedObject orderObj;
    @Mock
    private TypedObject order;


    @Test
    public void testSetOrderWithNullOrderModel() {
        widgetModel.setOrder(null);
        Assert.assertTrue(widgetModel.setOrder(orderObj));
    }

    @Test
    public void testSetOrderWithDiffrentOrderModel() {
        widgetModel.setOrder(order);
        Assert.assertTrue(widgetModel.setOrder(orderObj));
    }

    @Test
    public void testSetOrderWhenBothOrderModelIsSame() {
        widgetModel.setOrder(orderObj);
        Assert.assertFalse(widgetModel.setOrder(orderObj));
    }
}
