/**
 * 
 */
package au.com.target.tgtcs;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cscockpit.model.editor.validators.CockpitValidator;
import de.hybris.platform.cscockpit.model.editor.validators.impl.AbstractCockpitValidator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zul.impl.api.InputElement;

import au.com.target.tgtcs.model.editor.validators.impl.EmailFormatValidator;


/**
 * 
 */
@UnitTest
public class EmailFormatValidatorTest {

    private final EmailFormatValidator emailValidator = new EmailFormatValidator();
    @Mock
    private PropertyDescriptor propertyDescriptor;
    @Mock
    private transient InputElement inputElement;
    @Mock
    private AbstractCockpitValidator abstractCockpitValidator;
    @Mock
    private CockpitValidator cockpitValidator;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
        propertyDescriptor = Mockito.mock(PropertyDescriptor.class);
        cockpitValidator = Mockito.mock(CockpitValidator.class);
        //mock interface
        cockpitValidator.initialize(inputElement, propertyDescriptor, "");
    }

    @Test
    public void emailPass() {
        final Object value = "abc@gmail.com";
        emailValidator.validate(value);
    }

    @Test
    public void emailFailFormatThrowWrongException() {
        final Object value = "abcgmail.com";
        try {
            emailValidator.validate(value);
        }
        catch (final Exception ex) {
            Assert.assertTrue(ex.getClass().equals(WrongValueException.class));
        }
    }

}
