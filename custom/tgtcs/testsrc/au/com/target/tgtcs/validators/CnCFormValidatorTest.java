package au.com.target.tgtcs.validators;

import static au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel.FIRSTNAME_ITEM_ID;
import static au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel.LASTNAME_ITEM_ID;
import static au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel.PHONE_ITEM_ID;
import static au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel.STATE_ITEM_ID;
import static au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel.STORE_ITEM_ID;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.validation.Errors;
import org.springframework.validation.MapBindingResult;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcs.dto.CnCDetailsFormData;


@UnitTest
public class CnCFormValidatorTest {

    private final CnCFormValidator testInstance = new CnCFormValidator();

    @Test
    public void testSupportsCnCDetailsFormDataPass() {
        assertTrue(testInstance.supports(CnCDetailsFormData.class));
    }

    @Test
    public void testSupportsCnCDetailsFormDataFail() {
        Assert.assertFalse(testInstance.supports(Object.class));
    }

    @Test
    public void testValidateFirstNameFail() {
        final CnCDetailsFormData data = mock(CnCDetailsFormData.class);

        final Errors errors = new MapBindingResult(new HashMap<String, String>(), CnCDetailsFormData.class.getName());

        testInstance.validate(data, errors);
        assertTrue(errors.getFieldError(FIRSTNAME_ITEM_ID) != null);
    }

    @Test
    public void testValidateFirstNamePass() {
        final CnCDetailsFormData data = mock(CnCDetailsFormData.class);
        when(data.getFirstName()).thenReturn("firstName");

        final Errors errors = new MapBindingResult(new HashMap<String, String>(), CnCDetailsFormData.class.getName());

        testInstance.validate(data, errors);
        assertTrue(errors.getFieldError(FIRSTNAME_ITEM_ID) == null);
    }

    @Test
    public void testValidateLastNameFail() {
        final CnCDetailsFormData data = mock(CnCDetailsFormData.class);

        final Errors errors = new MapBindingResult(new HashMap<String, String>(), CnCDetailsFormData.class.getName());

        testInstance.validate(data, errors);
        assertTrue(errors.getFieldError(LASTNAME_ITEM_ID) != null);
    }

    @Test
    public void testValidateLastNamePass() {
        final CnCDetailsFormData data = mock(CnCDetailsFormData.class);
        when(data.getLastName()).thenReturn("lastName");

        final Errors errors = new MapBindingResult(new HashMap<String, String>(), CnCDetailsFormData.class.getName());

        testInstance.validate(data, errors);
        assertTrue(errors.getFieldError(LASTNAME_ITEM_ID) == null);
    }

    @Test
    public void testValidatePhoneFail() {
        final CnCDetailsFormData data = mock(CnCDetailsFormData.class);

        final Errors errors = new MapBindingResult(new HashMap<String, String>(), CnCDetailsFormData.class.getName());

        testInstance.validate(data, errors);
        assertTrue(errors.getFieldError(PHONE_ITEM_ID) != null);
    }

    @Test
    public void testValidatePhonePass() {
        final CnCDetailsFormData data = mock(CnCDetailsFormData.class);
        when(data.getPhone()).thenReturn("0123456789");

        final Errors errors = new MapBindingResult(new HashMap<String, String>(), CnCDetailsFormData.class.getName());

        testInstance.validate(data, errors);
        assertTrue(errors.getFieldError(PHONE_ITEM_ID) == null);
    }


    @Test
    public void testValidateStateFail() {
        final CnCDetailsFormData data = mock(CnCDetailsFormData.class);

        final Errors errors = new MapBindingResult(new HashMap<String, String>(), CnCDetailsFormData.class.getName());

        testInstance.validate(data, errors);
        assertTrue(errors.getFieldError(STATE_ITEM_ID) != null);
    }

    @Test
    public void testValidateStatePass() {
        final CnCDetailsFormData data = mock(CnCDetailsFormData.class);
        when(data.getState()).thenReturn("state");

        final Errors errors = new MapBindingResult(new HashMap<String, String>(), CnCDetailsFormData.class.getName());

        testInstance.validate(data, errors);
        assertTrue(errors.getFieldError(STATE_ITEM_ID) == null);
    }

    @Test
    public void testValidateStoreFail() {
        final CnCDetailsFormData data = mock(CnCDetailsFormData.class);

        final Errors errors = new MapBindingResult(new HashMap<String, String>(), CnCDetailsFormData.class.getName());

        testInstance.validate(data, errors);
        assertTrue(errors.getFieldError(STORE_ITEM_ID) != null);
    }

    @Test
    public void testValidateStorePass() {
        final CnCDetailsFormData data = mock(CnCDetailsFormData.class);
        final TargetPointOfServiceModel store = mock(TargetPointOfServiceModel.class);
        when(data.getStore()).thenReturn(store);

        final Errors errors = new MapBindingResult(new HashMap<String, String>(), CnCDetailsFormData.class.getName());

        testInstance.validate(data, errors);
        assertTrue(errors.getFieldError(STORE_ITEM_ID) == null);
    }

}
