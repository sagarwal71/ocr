/**
 * 
 */
package au.com.target.tgtwsfacades.instore.converters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtauspost.helper.ConsignmentDeliveryAddressNamer;
import au.com.target.tgtauspost.model.AusPostChargeZoneModel;
import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.postcode.service.TargetPostCodeService;
import au.com.target.tgtfulfilment.model.ConsignmentParcelModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtwsfacades.instore.dto.auspost.ConsignmentSummary;


/**
 * @author smishra1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class HardCopyManifestConsignmentConverterTest {

    private ConsignmentSummary consignmentSummary;
    private PostCodeModel postCodeModel;

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private TargetPostCodeService targetPostCodeService;

    @Mock
    private StoreFulfilmentCapabilitiesModel storeCapability;

    @Mock
    private AddressModel deliveryAddress;

    @Mock
    private ConsignmentDeliveryAddressNamer consignmentDeliveryAddressNamer;

    @Mock
    private AusPostChargeZoneModel ausPostChargeZoneModel;

    @Mock
    private HardCopyManifestConsignmentConverter consignmentConverter;

    @InjectMocks
    private final HardCopyManifestConsignmentConverter converter = new HardCopyManifestConsignmentConverter();

    @Before
    public void setup() {
        Mockito.when(consignment.getCode()).thenReturn("testConsignment-001");
        Mockito.when(consignment.getTrackingID()).thenReturn("1234567890");
        Mockito.when(consignmentDeliveryAddressNamer.getDeliveryName(consignment)).thenReturn("5126 - Robina");
        setShippingAddress();
        setChargeZone();
    }

    @Test
    public void testChargeZone() {
        consignmentSummary = converter.convert(consignment);
        Assert.assertEquals("1234567890", consignmentSummary.getConsignmentId());
        Assert.assertEquals("123456789", consignmentSummary.getChargeZone().getCode());
        Assert.assertEquals("Description for ChargeCode - 123456789", consignmentSummary.getChargeZone()
                .getDescription());
    }

    @Test
    public void testChargeZoneNull() {
        Mockito.when(postCodeModel.getAuspostChargeZone()).thenReturn(null);
        consignmentSummary = converter.convert(consignment);
        Assert.assertNull(consignmentSummary.getChargeZone().getCode());
        Assert.assertNull(consignmentSummary.getChargeZone().getDescription());
    }


    @Test
    public void testShippingAddress() {
        consignmentSummary = converter.convert(consignment);
        Assert.assertEquals("131 Lonsdale Heights", consignmentSummary.getDeliveryAddress().getAddressLine1());
        Assert.assertEquals("Lonsdale Street", consignmentSummary.getDeliveryAddress().getAddressLine2());
        Assert.assertEquals("Melbourne", consignmentSummary.getDeliveryAddress().getSuburb());
        Assert.assertEquals("VIC", consignmentSummary.getDeliveryAddress().getState());
        Assert.assertEquals("3000", consignmentSummary.getDeliveryAddress().getPostcode());
    }

    @Test
    public void testParcelWeightCalculation() {
        createParcelsWithDifferentWeights(2, 4, 6);
        consignmentSummary = converter.convert(consignment);
        Assert.assertEquals(Double.valueOf(28.5), consignmentSummary.getTotalWeight());
        createParcelsWithDifferentWeights(0, 0, 0);
        consignmentSummary = converter.convert(consignment);
        Assert.assertEquals(Double.valueOf(16.5), consignmentSummary.getTotalWeight());
        createParcelsWithDifferentWeights(-5.5, -5.5, -5.5);
        consignmentSummary = converter.convert(consignment);
        Assert.assertEquals(Double.valueOf(0.0), consignmentSummary.getTotalWeight());
        createParcelsWithDifferentWeights(99.499, 0.499);
        consignmentSummary = converter.convert(consignment);
        Assert.assertEquals(Double.valueOf(110.998), consignmentSummary.getTotalWeight());
    }

    private void setShippingAddress() {
        final AddressModel shippingAddress = Mockito.mock(AddressModel.class);
        Mockito.when(shippingAddress.getLine1()).thenReturn("131 Lonsdale Heights");
        Mockito.when(shippingAddress.getLine2()).thenReturn("Lonsdale Street");
        Mockito.when(shippingAddress.getTown()).thenReturn("Melbourne");
        Mockito.when(shippingAddress.getDistrict()).thenReturn("VIC");
        Mockito.when(shippingAddress.getPostalcode()).thenReturn("3000");

        Mockito.when(consignment.getShippingAddress()).thenReturn(shippingAddress);
    }

    private void setChargeZone() {
        postCodeModel = Mockito.mock(PostCodeModel.class);
        Mockito.when(postCodeModel.getAuspostChargeZone()).thenReturn(ausPostChargeZoneModel);
        Mockito.when(postCodeModel.getAuspostChargeZone().getChargeCode()).thenReturn("123456789");
        Mockito.when(postCodeModel.getAuspostChargeZone().getAreaName()).thenReturn(
                "Description for ChargeCode - 123456789");
        Mockito.when(targetPostCodeService.getPostCode(consignment
                .getShippingAddress().getPostalcode())).thenReturn(postCodeModel);
    }

    private void createParcelsWithDifferentWeights(final double... numbers) {
        final Set<ConsignmentParcelModel> parcels = new HashSet<>();

        for (final double number : numbers) {
            final ConsignmentParcelModel parcel = Mockito.mock(ConsignmentParcelModel.class);
            Mockito.when(parcel.getActualWeight()).thenReturn(Double.valueOf(number + 5.5));
            parcels.add(parcel);
        }
        Mockito.when(consignment.getParcelsDetails()).thenReturn(parcels);
    }


}
