package au.com.target.tgtwsfacades.instore.converters;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Date;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.model.TargetManifestModel;
import au.com.target.tgtutility.util.TargetDateUtil;
import au.com.target.tgtwsfacades.instore.dto.manifests.Manifest;


/**
 * Test class for {@link InstoreBasicManifestConverter}
 * 
 * @author Rahul
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class InstoreBasicManifestConverterTest {

    private static final String MANIFEST_CODE = "1234567";

    @Mock
    private TargetManifestModel manifestModel;

    @InjectMocks
    private final InstoreBasicManifestConverter converter = new InstoreBasicManifestConverter();

    @Test(expected = IllegalArgumentException.class)
    public void testConvertNull() {
        converter.convert(null);
    }

    @Test
    public void testConvertEmptyFields() {
        final Manifest manifest = converter.convert(manifestModel);

        Assert.assertNotNull(manifest);
        Assert.assertNull(manifest.getCode());
        Assert.assertNull(manifest.getDate());
    }

    @Test
    public void testConvert() {
        final Date date = new Date();
        Mockito.when(manifestModel.getCode()).thenReturn(MANIFEST_CODE);
        Mockito.when(manifestModel.getDate()).thenReturn(date);

        final Manifest manifest = converter.convert(manifestModel);

        Assert.assertNotNull(manifest);
        Assert.assertEquals(MANIFEST_CODE, manifest.getCode());
        Assert.assertEquals(TargetDateUtil.getDateFormattedAsString(date), manifest.getDate());
    }

}
