/**
 * 
 */
package au.com.target.tgtwsfacades.instore.converters;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.barcode.TargetBarCodeService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtwsfacades.constants.TgtwsfacadesConstants;
import au.com.target.tgtwsfacades.instore.ConsignmentStatusMapper;
import au.com.target.tgtwsfacades.instore.dto.consignments.Consignment;


/**
 *
 * 
 * @author sbryan6
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class InstoreConsignmentConverterTest {

    @Mock
    private TargetConsignmentModel consignmentModel;

    @Mock
    private AbstractOrderModel orderModel;

    @Mock
    private AddressModel deliveryAddress;

    @Mock
    private ConsignmentStatusMapper consignmentStatusMapper;

    @Mock
    private TargetBarCodeService targetBarCodeService;

    @Mock
    private OrderEntryModel orderEntry1;

    @Mock
    private OrderEntryModel orderEntry2;

    @InjectMocks
    private final InstoreConsignmentConverter converter = new InstoreConsignmentConverter() {
        @Override
        protected String getEncodedOrderId(final String code) {
            return "1233444556";
        }


    };

    @Before
    public void setup() {

        given(consignmentModel.getOrder()).willReturn(orderModel);
        given(orderModel.getDeliveryAddress()).willReturn(deliveryAddress);
        given(deliveryAddress.getTown()).willReturn("Australia");
        given(deliveryAddress.getDistrict()).willReturn("district");

        given(orderModel.getCode()).willReturn("1234");
        given(deliveryAddress.getFirstname()).willReturn("First");
        given(deliveryAddress.getLastname()).willReturn("Last");

        given(consignmentModel.getCode()).willReturn("con1234");
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        given(consignmentModel.getParcelCount()).willReturn(Integer.valueOf(3));
        given(consignmentModel.getPickConfirmDate()).willReturn(new Date(999999));
        given(consignmentModel.getPackedDate()).willReturn(new Date(888888));
        given(consignmentModel.getCreationtime()).willReturn(new Date(999999));
        given(consignmentModel.getConsignmentVersion()).willReturn(Integer.valueOf(2));
        given(targetBarCodeService.getPlainBarcode(anyString())).willReturn("123456789");

        given(consignmentStatusMapper.getOFCStatus(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE)).willReturn(
                TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_OPEN);

        given(consignmentStatusMapper.getOFCStatus(ConsignmentStatus.SHIPPED)).willReturn(
                TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_COMPLETED);
    }

    @Test
    public void testGetTotalNumberOfItemsNullEntries() {

        given(consignmentModel.getConsignmentEntries()).willReturn(null);

        final long num = converter.getTotalNumberOfItems(consignmentModel);
        assertThat(num).isEqualTo(0);
    }

    @Test
    public void testGetTotalNumberOfItemsNullOrderEntry() {

        setConsignmentModelWithNullOrderEntry();

        final long num = converter.getTotalNumberOfItems(consignmentModel);
        assertThat(num).isEqualTo(0);
    }

    @Test
    public void testGetTotalNumberOfItemsWithOneEntry() {

        setConsignmentModelWithEntryQuantities(3);
        final long num = converter.getTotalNumberOfItems(consignmentModel);
        assertThat(num).isEqualTo(3);
    }

    @Test
    public void testGetTotalNumberOfItemsWithMulitpleEntries() {

        setConsignmentModelWithEntryQuantities(3, 2, 4);
        final long num = converter.getTotalNumberOfItems(consignmentModel);
        assertThat(num).isEqualTo(9);
    }


    @Test
    public void testPopulateOrderInfoNullOrder() {

        final Consignment con = new Consignment();
        given(consignmentModel.getOrder()).willReturn(null);

        converter.populateOrderInfo(consignmentModel, con);
        assertThat(con.getOrderNumber()).isNull();
        assertThat(con.getCustomer()).isNull();
    }

    @Test
    public void testPopulateOrderInfoNullOrderButShippingAddressInConsignment() {

        given(consignmentModel.getShippingAddress()).willReturn(deliveryAddress);
        final Consignment con = new Consignment();
        given(consignmentModel.getOrder()).willReturn(null);

        converter.populateOrderInfo(consignmentModel, con);
        assertThat(con.getOrderNumber()).isNull();
        assertThat(con.getDestination()).isNotNull();
        assertThat(con.getDestination().getCity()).isNotNull().isEqualTo("Australia");
        assertThat(con.getDestination().getState()).isNotNull().isEqualTo("district");
        assertThat(con.getCustomer()).isNotNull();
        assertThat(con.getCustomer().getFirstName()).isNotNull().isEqualTo("First");
        assertThat(con.getCustomer().getLastName()).isNotNull().isEqualTo("Last");
    }

    @Test
    public void testPopulateOrderInfoWithOrderNoDeliveryAddress() {

        final Consignment con = new Consignment();
        given(orderModel.getDeliveryAddress()).willReturn(null);
        converter.populateCustomerAndDestinationInfo(null, con);

        converter.populateOrderInfo(consignmentModel, con);
        assertThat(con.getOrderNumber()).isNotNull().isEqualTo("1234");
        assertThat(con.getCustomer()).isNull();
    }

    @Test
    public void testPopulateOrderInfoWithFullOrderDetails() {

        final Consignment con = new Consignment();

        converter.populateOrderInfo(consignmentModel, con);
        assertThat(con.getCustomer()).isNotNull();
        assertThat(con.getCustomer().getFirstName()).isNotNull().isEqualTo("First");
        assertThat(con.getCustomer().getLastName()).isNotNull().isEqualTo("Last");
    }

    @Test
    public void testPopulateOrderBarcodeDetails() {

        final Consignment con = new Consignment();

        converter.populateOrderBarcodeDetails(orderModel, con);
        assertThat(con.getOrderBarcodeNumber()).isNotNull().isEqualTo("123456789");
        assertThat(con.getOrderBarcodeUrl()).isNotNull();

    }

    @Test
    public void testTotalValueOfConsignment() {
        final ConsignmentEntryModel entry1 = mock(ConsignmentEntryModel.class);
        final ConsignmentEntryModel entry2 = mock(ConsignmentEntryModel.class);
        given(entry1.getOrderEntry()).willReturn(orderEntry1);
        given(entry2.getOrderEntry()).willReturn(orderEntry2);
        given(entry1.getOrderEntry().getTotalPrice()).willReturn(Double.valueOf(25.55));
        given(entry2.getOrderEntry().getTotalPrice()).willReturn(Double.valueOf(15d));
        final Set<ConsignmentEntryModel> entries = new HashSet<>();
        entries.add(entry1);
        entries.add(entry2);
        given(consignmentModel.getConsignmentEntries()).willReturn(entries);
        assertThat(converter.getTotalValueOfConsignment(consignmentModel)).isNotNull().isEqualTo(40.55);
    }

    @Test
    public void testTotalValueWhenConsignmentEntriesIsNull() {
        given(consignmentModel.getConsignmentEntries()).willReturn(null);
        assertThat(converter.getTotalValueOfConsignment(consignmentModel)).isNotNull().isEqualTo(0);
    }

    @Test
    public void testTotalValueWhenNoEntriesArePresentInConsignmentEntryModel() {
        final Set<ConsignmentEntryModel> entries = new HashSet<>();
        given(consignmentModel.getConsignmentEntries()).willReturn(entries);
        assertThat(converter.getTotalValueOfConsignment(consignmentModel)).isNotNull().isEqualTo(0);
    }

    @Test
    public void testTotalValueOfConsignment1() {
        final ConsignmentEntryModel entry1 = mock(ConsignmentEntryModel.class);
        final ConsignmentEntryModel entry2 = mock(ConsignmentEntryModel.class);
        given(entry1.getOrderEntry()).willReturn(orderEntry1);
        given(entry2.getOrderEntry()).willReturn(orderEntry2);
        given(entry1.getOrderEntry().getTotalPrice()).willReturn(Double.valueOf(25.55));
        given(entry2.getOrderEntry().getTotalPrice()).willReturn(Double.valueOf(15.56));
        final Set<ConsignmentEntryModel> entries = new HashSet<>();
        entries.add(entry1);
        entries.add(entry2);
        given(consignmentModel.getConsignmentEntries()).willReturn(entries);
        assertThat(converter.getTotalValueOfConsignment(consignmentModel)).isNotNull().isEqualTo(41.11);
    }

    @Test
    public void testPopulateOrderBarcodeDetailsForNullCode() {

        given(orderModel.getCode()).willReturn(null);
        final Consignment con = new Consignment();

        converter.populateOrderBarcodeDetails(orderModel, con);
        assertThat(con.getOrderBarcodeNumber()).isNull();
        assertThat(con.getOrderBarcodeUrl()).isNull();

    }

    @Test
    public void testPopulateCustomerAndDestinationInfoNull() {
        given(consignmentModel.getOrder()).willReturn(null);
        given(orderModel.getDeliveryAddress()).willReturn(null);
        final Consignment con = new Consignment();
        converter.populateOrderInfo(consignmentModel, con);
        converter.populateCustomerAndDestinationInfo(orderModel.getDeliveryAddress(), con);
        assertThat(con.getDestination()).isNull();
        assertThat(con.getCustomer()).isNull();
    }

    @Test
    public void testPopulateCustomerAndDestinationDetails() {
        final Consignment con = new Consignment();
        converter.populateCustomerAndDestinationInfo(orderModel.getDeliveryAddress(), con);
        assertThat(con.getDestination()).isNotNull();
        assertThat(con.getDestination().getCity()).isNotNull().isEqualTo("Australia");
        assertThat(con.getDestination().getState()).isNotNull().isEqualTo("district");
        assertThat(con.getCustomer()).isNotNull();
        assertThat(con.getCustomer().getFirstName()).isNotNull().isEqualTo("First");
        assertThat(con.getCustomer().getLastName()).isNotNull().isEqualTo("Last");
    }

    @Test
    public void testConvert() {

        setConsignmentModelWithEntryQuantities(3);

        final Consignment con = converter.convert(consignmentModel);
        assertThat(con.getCode()).isNotNull().isEqualTo("con1234");
        assertThat(con.getStatus()).isNotNull().isEqualTo(TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_OPEN);
        assertThat(con.getDate()).isNotNull().isEqualTo("999999");
        assertThat(con.getOrderNumber()).isNotNull().isEqualTo("1234");
        assertThat(con.getTotalItems()).isEqualTo(3);
        assertThat(con.getCustomer()).isNotNull();
        assertThat(con.getCustomer().getFirstName()).isNotNull().isEqualTo("First");
        assertThat(con.getCustomer().getLastName()).isNotNull().isEqualTo("Last");
        assertThat(con.getParcels()).isNotNull().isEqualTo(3);
        assertThat(con.getPickDate()).isNotNull().isEqualTo("999999");
        assertThat(con.getPackDate()).isNotNull().isEqualTo("888888");
        assertThat(con.getDestination()).isNotNull();
        assertThat(con.getDestination().getCity()).isNotNull().isEqualTo("Australia");
        assertThat(con.getDestination().getState()).isNotNull().isEqualTo("district");
        assertThat(con.getConsignmentVersion()).isNotNull().isEqualTo(Integer.valueOf(2));
    }

    @Test
    public void testConvertWithOrderType() {
        given(consignmentModel.getOfcOrderType()).willReturn(OfcOrderType.INSTORE_PICKUP);
        final Consignment con = converter.convert(consignmentModel);
        assertThat(con.getDeliveryType()).isEqualTo(OfcOrderType.INSTORE_PICKUP.getCode());
    }

    @Test
    public void testConvertWithoutOrderType() {
        given(consignmentModel.getOfcOrderType()).willReturn(null);
        final Consignment con = converter.convert(consignmentModel);
        assertThat(con.getDeliveryType()).isNull();
    }

    @Test
    public void testConvertWithShipReceived() {

        setConsignmentModelWithEntryQuantities(3);
        given(consignmentModel.getShipConfReceived()).willReturn(Boolean.TRUE);
        final Consignment con = converter.convert(consignmentModel);
        assertThat(con.getCode()).isNotNull().isEqualTo("con1234");
        assertThat(con.getStatus()).isNotNull().isEqualTo(TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_COMPLETED);
        assertThat(con.getDate()).isNotNull().isEqualTo("999999");
        assertThat(con.getOrderNumber()).isNotNull().isEqualTo("1234");
        assertThat(con.getTotalItems()).isEqualTo(3);
        assertThat(con.getCustomer()).isNotNull();
        assertThat(con.getCustomer().getFirstName()).isNotNull().isEqualTo("First");
        assertThat(con.getCustomer().getLastName()).isNotNull().isEqualTo("Last");
        assertThat(con.getParcels()).isNotNull().isEqualTo(3);
        assertThat(con.getPickDate()).isNotNull().isEqualTo("999999");
        assertThat(con.getPackDate()).isNotNull().isEqualTo("888888");
        assertThat(con.getDestination()).isNotNull();
        assertThat(con.getDestination().getCity()).isNotNull().isEqualTo("Australia");
        assertThat(con.getDestination().getState()).isNotNull().isEqualTo("district");
        assertThat(con.getConsignmentVersion()).isNotNull().isEqualTo(Integer.valueOf(2));
    }

    @Test
    public void testConvertWithClearanceColourVariant() {
        final TargetProductModel mockProduct = createMockProduct(true, false, false);
        final TargetColourVariantProductModel mockColourVariant = createMockColourVariant(mockProduct, Boolean.FALSE);

        final Consignment consignment = converter.convert(createConsignmentFromProducts(mockColourVariant));

        assertThat(consignment.isClearance()).isTrue();
        assertThat(consignment.isBulky()).isFalse();
        assertThat(consignment.isMhd()).isFalse();
        assertThat(consignment.isOnlineExclusive()).isFalse();
    }

    @Test
    public void testConvertWithClearanceSizeVariant() {
        final TargetProductModel mockProduct = createMockProduct(true, false, false);
        final TargetColourVariantProductModel mockColourVariant = createMockColourVariant(mockProduct, Boolean.FALSE);
        final TargetSizeVariantProductModel mockSizeVariant = createMockSizeVariant(mockColourVariant);

        final Consignment consignment = converter.convert(createConsignmentFromProducts(mockSizeVariant));

        assertThat(consignment.isClearance()).isTrue();
        assertThat(consignment.isBulky()).isFalse();
        assertThat(consignment.isMhd()).isFalse();
        assertThat(consignment.isOnlineExclusive()).isFalse();
    }

    @Test
    public void testConvertWithBulkyColourVariant() {
        final TargetProductModel mockProduct = createMockProduct(false, true, false);
        final TargetColourVariantProductModel mockColourVariant = createMockColourVariant(mockProduct, Boolean.FALSE);

        final Consignment consignment = converter.convert(createConsignmentFromProducts(mockColourVariant));

        assertThat(consignment.isClearance()).isFalse();
        assertThat(consignment.isBulky()).isTrue();
        assertThat(consignment.isMhd()).isFalse();
        assertThat(consignment.isOnlineExclusive()).isFalse();
    }

    @Test
    public void testConvertWithBulkySizeVariant() {
        final TargetProductModel mockProduct = createMockProduct(false, true, false);
        final TargetColourVariantProductModel mockColourVariant = createMockColourVariant(mockProduct, Boolean.FALSE);
        final TargetSizeVariantProductModel mockSizeVariant = createMockSizeVariant(mockColourVariant);

        final Consignment consignment = converter.convert(createConsignmentFromProducts(mockSizeVariant));

        assertThat(consignment.isClearance()).isFalse();
        assertThat(consignment.isBulky()).isTrue();
        assertThat(consignment.isMhd()).isFalse();
        assertThat(consignment.isOnlineExclusive()).isFalse();
    }

    @Test
    public void testConvertWithMHDColourVariant() {
        final TargetProductModel mockProduct = createMockProduct(false, false, true);
        final TargetColourVariantProductModel mockColourVariant = createMockColourVariant(mockProduct, Boolean.FALSE);

        final Consignment consignment = converter.convert(createConsignmentFromProducts(mockColourVariant));

        assertThat(consignment.isClearance()).isFalse();
        assertThat(consignment.isBulky()).isFalse();
        assertThat(consignment.isMhd()).isTrue();
        assertThat(consignment.isOnlineExclusive()).isFalse();
    }

    @Test
    public void testConvertWithMHDSizeVariant() {
        final TargetProductModel mockProduct = createMockProduct(false, false, true);
        final TargetColourVariantProductModel mockColourVariant = createMockColourVariant(mockProduct, Boolean.FALSE);
        final TargetSizeVariantProductModel mockSizeVariant = createMockSizeVariant(mockColourVariant);

        final Consignment consignment = converter.convert(createConsignmentFromProducts(mockSizeVariant));

        assertThat(consignment.isClearance()).isFalse();
        assertThat(consignment.isBulky()).isFalse();
        assertThat(consignment.isMhd()).isTrue();
        assertThat(consignment.isOnlineExclusive()).isFalse();
    }

    @Test
    public void testConvertWithOnlineExclusiveColourVariant() {
        final TargetProductModel mockProduct = createMockProduct(false, false, false);
        final TargetColourVariantProductModel mockColourVariant = createMockColourVariant(mockProduct, Boolean.TRUE);

        final Consignment consignment = converter.convert(createConsignmentFromProducts(mockColourVariant));

        assertThat(consignment.isClearance()).isFalse();
        assertThat(consignment.isBulky()).isFalse();
        assertThat(consignment.isMhd()).isFalse();
        assertThat(consignment.isOnlineExclusive()).isTrue();
    }

    @Test
    public void testConvertWithOnlineExclusiveSizeVariant() {
        final TargetProductModel mockProduct = createMockProduct(false, false, false);
        final TargetColourVariantProductModel mockColourVariant = createMockColourVariant(mockProduct, Boolean.TRUE);
        final TargetSizeVariantProductModel mockSizeVariant = createMockSizeVariant(mockColourVariant);

        final Consignment consignment = converter.convert(createConsignmentFromProducts(mockSizeVariant));

        assertThat(consignment.isClearance()).isFalse();
        assertThat(consignment.isBulky()).isFalse();
        assertThat(consignment.isMhd()).isFalse();
        assertThat(consignment.isOnlineExclusive()).isTrue();
    }

    @Test
    public void testConvertWithColourVariantWithAllFlags() {
        final TargetProductModel mockProduct = createMockProduct(true, true, true);
        final TargetColourVariantProductModel mockColourVariant = createMockColourVariant(mockProduct, Boolean.TRUE);

        final Consignment consignment = converter.convert(createConsignmentFromProducts(mockColourVariant));

        assertThat(consignment.isClearance()).isTrue();
        assertThat(consignment.isBulky()).isTrue();
        assertThat(consignment.isMhd()).isTrue();
        assertThat(consignment.isOnlineExclusive()).isTrue();
    }

    @Test
    public void testConvertWithSizeVariantWithAllFlags() {
        final TargetProductModel mockProduct = createMockProduct(true, true, true);
        final TargetColourVariantProductModel mockColourVariant = createMockColourVariant(mockProduct, Boolean.TRUE);
        final TargetSizeVariantProductModel mockSizeVariant = createMockSizeVariant(mockColourVariant);

        final Consignment consignment = converter.convert(createConsignmentFromProducts(mockSizeVariant));

        assertThat(consignment.isClearance()).isTrue();
        assertThat(consignment.isBulky()).isTrue();
        assertThat(consignment.isMhd()).isTrue();
        assertThat(consignment.isOnlineExclusive()).isTrue();
    }

    @Test
    public void testConvertWithMultipleColourVariantsWithAllFlags() {
        // Bulky
        final TargetProductModel mockBulkyProduct = createMockProduct(false, true, false);
        final TargetColourVariantProductModel mockBulkyColourVariant = createMockColourVariant(mockBulkyProduct,
                Boolean.FALSE);

        // MHD
        final TargetProductModel mockMHDProduct = createMockProduct(false, false, true);
        final TargetColourVariantProductModel mockMHDColourVariant = createMockColourVariant(mockMHDProduct,
                Boolean.FALSE);

        // Clearance
        final TargetProductModel mockClearanceProduct = createMockProduct(true, false, false);
        final TargetColourVariantProductModel mockClearanceColourVariant = createMockColourVariant(mockClearanceProduct,
                Boolean.FALSE);

        // Online Exclusive
        final TargetProductModel mockOnlineExclusiveProduct = createMockProduct(false, false, false);
        final TargetColourVariantProductModel mockOnlineExclusiveColourVariant = createMockColourVariant(
                mockOnlineExclusiveProduct, Boolean.TRUE);

        // Normal
        final TargetProductModel mockNormalProduct = createMockProduct(false, false, false);
        final TargetColourVariantProductModel mockNormalColourVariant = createMockColourVariant(mockNormalProduct,
                Boolean.FALSE);

        final Consignment consignment = converter
                .convert(createConsignmentFromProducts(mockBulkyColourVariant, mockMHDColourVariant,
                        mockClearanceColourVariant, mockOnlineExclusiveColourVariant, mockNormalColourVariant));

        assertThat(consignment.isClearance()).isTrue();
        assertThat(consignment.isBulky()).isTrue();
        assertThat(consignment.isMhd()).isTrue();
        assertThat(consignment.isOnlineExclusive()).isTrue();
    }

    @Test
    public void testConvertWithMultipleSizeVariantsWithAllFlags() {
        // Bulky
        final TargetProductModel mockBulkyProduct = createMockProduct(false, true, false);
        final TargetColourVariantProductModel mockBulkyColourVariant = createMockColourVariant(mockBulkyProduct,
                Boolean.FALSE);
        final TargetSizeVariantProductModel mockBulkySizeVariant = createMockSizeVariant(mockBulkyColourVariant);

        // MHD
        final TargetProductModel mockMHDProduct = createMockProduct(false, false, true);
        final TargetColourVariantProductModel mockMHDColourVariant = createMockColourVariant(mockMHDProduct,
                Boolean.FALSE);
        final TargetSizeVariantProductModel mockMHDSizeVariant = createMockSizeVariant(mockMHDColourVariant);

        // Clearance
        final TargetProductModel mockClearanceProduct = createMockProduct(true, false, false);
        final TargetColourVariantProductModel mockClearanceColourVariant = createMockColourVariant(mockClearanceProduct,
                Boolean.FALSE);
        final TargetSizeVariantProductModel mockClearanceSizeVariant = createMockSizeVariant(
                mockClearanceColourVariant);

        // Online Exclusive
        final TargetProductModel mockOnlineExclusiveProduct = createMockProduct(false, false, false);
        final TargetColourVariantProductModel mockOnlineExclusiveColourVariant = createMockColourVariant(
                mockOnlineExclusiveProduct, Boolean.TRUE);
        final TargetSizeVariantProductModel mockOnlineExclusiveSizeVariant = createMockSizeVariant(
                mockOnlineExclusiveColourVariant);

        // Normal
        final TargetProductModel mockNormalProduct = createMockProduct(false, false, false);
        final TargetColourVariantProductModel mockNormalColourVariant = createMockColourVariant(mockNormalProduct,
                Boolean.FALSE);
        final TargetSizeVariantProductModel mockNormalSizeVariant = createMockSizeVariant(mockNormalColourVariant);

        final Consignment consignment = converter
                .convert(createConsignmentFromProducts(mockBulkySizeVariant, mockMHDSizeVariant,
                        mockClearanceSizeVariant, mockOnlineExclusiveSizeVariant, mockNormalSizeVariant));

        assertThat(consignment.isClearance()).isTrue();
        assertThat(consignment.isBulky()).isTrue();
        assertThat(consignment.isMhd()).isTrue();
        assertThat(consignment.isOnlineExclusive()).isTrue();
    }

    private TargetProductModel createMockProduct(final boolean isClearance, final boolean isBulky,
            final boolean isMhd) {
        final ProductTypeModel mockProductType = mock(ProductTypeModel.class);
        given(mockProductType.getCode()).willReturn(isMhd ? "mhd" : "normal");
        given(mockProductType.getBulky()).willReturn(Boolean.valueOf(isBulky));

        final TargetProductModel mockProduct = mock(TargetProductModel.class);
        given(mockProduct.getClearanceProduct()).willReturn(Boolean.valueOf(isClearance));
        given(mockProduct.getProductType()).willReturn(mockProductType);

        return mockProduct;
    }

    private TargetColourVariantProductModel createMockColourVariant(final TargetProductModel product,
            final Boolean onlineExclusive) {
        final TargetColourVariantProductModel mockColourVariant = mock(TargetColourVariantProductModel.class);
        given(mockColourVariant.getBaseProduct()).willReturn(product);
        given(mockColourVariant.getOnlineExclusive()).willReturn(onlineExclusive);

        return mockColourVariant;
    }

    private TargetSizeVariantProductModel createMockSizeVariant(final TargetColourVariantProductModel colourVariant) {
        final TargetSizeVariantProductModel mockSizeVariant = mock(TargetSizeVariantProductModel.class);
        given(mockSizeVariant.getBaseProduct()).willReturn(colourVariant);

        return mockSizeVariant;
    }

    private TargetConsignmentModel createConsignmentFromProducts(final AbstractTargetVariantProductModel... products) {
        final Set<ConsignmentEntryModel> consignmentEntries = new HashSet<>();

        for (final AbstractTargetVariantProductModel product : products) {
            final OrderEntryModel mockOrderEntry = mock(OrderEntryModel.class);
            given(mockOrderEntry.getProduct()).willReturn(product);

            final ConsignmentEntryModel mockConsignmentEntry = mock(ConsignmentEntryModel.class);
            given(mockConsignmentEntry.getOrderEntry()).willReturn(mockOrderEntry);

            consignmentEntries.add(mockConsignmentEntry);
        }

        final TargetConsignmentModel mockTargetConsignment = mock(TargetConsignmentModel.class);
        given(mockTargetConsignment.getConsignmentEntries()).willReturn(consignmentEntries);

        return mockTargetConsignment;
    }

    private void setConsignmentModelWithNullOrderEntry() {

        final Set<ConsignmentEntryModel> entries = new HashSet<>();

        final ConsignmentEntryModel consignmentEntryModel = mock(ConsignmentEntryModel.class);

        given(consignmentEntryModel.getOrderEntry()).willReturn(null);

        entries.add(consignmentEntryModel);

        given(consignmentModel.getConsignmentEntries()).willReturn(entries);
    }

    private void setConsignmentModelWithEntryQuantities(final long... qtys) {

        final Set<ConsignmentEntryModel> entries = new HashSet<>();

        for (final long qty : qtys) {
            final ConsignmentEntryModel consignmentEntryModel = mock(ConsignmentEntryModel.class);
            given(consignmentEntryModel.getQuantity()).willReturn(Long.valueOf(qty));
            entries.add(consignmentEntryModel);
        }

        given(consignmentModel.getConsignmentEntries()).willReturn(entries);
    }

}