/**
 *
 */
package au.com.target.tgtwsfacades.pickconfirm.impl;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.valueOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.dto.PickConfirmEntry;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.fulfilmentservice.TargetFulfilmentService;
import au.com.target.tgtfulfilment.util.LoggingContext;
import au.com.target.tgtwsfacades.helper.OrderConsignmentHelper;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.PickConfirmEntries;
import au.com.target.tgtwsfacades.integration.dto.PickConfirmOrder;


/**
 * Test suite for {@link PickConfirmIntegrationFacadeImpl}.
 * 
 * @author mmaki
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PickConfirmIntegrationFacadeTest {

    private static final String ORDER_NUMBER = "12345678";
    private static final String CONSIGNMENT_NUMBER = "12345678_1";
    private static final String CARRIER = "carrier";
    private static final Integer PARCEL_COUNT = Integer.valueOf(4);

    private static final String ENTRY_CODE = "123456";
    private static final Integer ENTRY_QTY = Integer.valueOf(5);

    @Mock
    private TargetFulfilmentService fulfilmentService;

    @Mock
    private OrderConsignmentHelper orderConsignmentHelper;

    @InjectMocks
    private final PickConfirmIntegrationFacadeImpl pickConfirmIntegrationFacade = new PickConfirmIntegrationFacadeImpl();

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private WarehouseModel warehouse;

    @Mock
    private OrderModel order;

    private PickConfirmOrder dto;

    /**
     * Initialises test case before run.
     */
    @Before
    public void setUp() {
        dto = new PickConfirmOrder();
        dto.setOrderNumber(ORDER_NUMBER);
        dto.setConsignmentNumber(CONSIGNMENT_NUMBER);
        dto.setCarrier(CARRIER);
        dto.setParcelCount(PARCEL_COUNT);
        final PickConfirmEntries pickConfirmEntries = new PickConfirmEntries();
        final List<PickConfirmEntry> entryList = new ArrayList<>();
        final PickConfirmEntry entry = new PickConfirmEntry();
        entry.setItemCode(ENTRY_CODE);
        entry.setQuantityShipped(ENTRY_QTY);
        entryList.add(entry);
        pickConfirmEntries.setPickConfirmEntries(entryList);
        dto.setPickConfirmEntries(pickConfirmEntries);

        BDDMockito.when(consignment.getOrder()).thenReturn(order);
        BDDMockito.when(order.getCode()).thenReturn(ORDER_NUMBER);
    }

    /**
     * Verifies "happy" scenario for "pick conf" notification processing.
     * 
     * @throws FulfilmentException
     *             if order status validation does not pass (disregard it)
     */
    @Test
    public void testHandlePickConfirm() throws FulfilmentException {
        BDDMockito.when(orderConsignmentHelper.getConsignmentBasedOrderOrConsignmentCode(ORDER_NUMBER)).thenReturn(
                consignment);
        BDDMockito.when(consignment.getWarehouse()).thenReturn(warehouse);
        final IntegrationResponseDto response = pickConfirmIntegrationFacade.handlePickConfirm(dto);
        assertEquals(ORDER_NUMBER, response.getCode());
        assertTrue(response.isSuccessStatus());
        verify(fulfilmentService).processPickConfForConsignment(
                ORDER_NUMBER, consignment, CARRIER, CONSIGNMENT_NUMBER, PARCEL_COUNT,
                dto.getPickConfirmEntries().getPickConfirmEntries(), LoggingContext.WAREHOUSE);
        verifyNoMoreInteractions(fulfilmentService);
    }

    /**
     * Verifies that negative response returned when DTO does not have order number.
     */
    @Test
    public void testHandlePickConfirmMissingOrderNumber() {
        dto.setOrderNumber(null);
        final IntegrationResponseDto response = pickConfirmIntegrationFacade.handlePickConfirm(dto);
        assertEquals(FALSE, valueOf(response.isSuccessStatus()));
    }

    /**
     * Verifies that empty, e.g. {@code null}-referenced, DTO will return negative response.
     */
    @Test
    public void testHandlePickConfirmNull() {
        final IntegrationResponseDto response = pickConfirmIntegrationFacade.handlePickConfirm(null);
        assertEquals(null, response.getCode());
        assertEquals(FALSE, valueOf(response.isSuccessStatus()));
    }

    /**
     * Verifies that if order validation will fail, negative response will be returned.
     * 
     * @throws FulfilmentException
     *             if order status validation does not pass (disregard it)
     */
    @Test
    public void testHandlePickConfirmException() throws FulfilmentException {
        doThrow(new FulfilmentException("This is a test exception"))
                .when(fulfilmentService).processPickConfForConsignment(
                        anyString(), any(TargetConsignmentModel.class), anyString(), anyString(), (Integer)any(),
                        anyListOf(PickConfirmEntry.class), any(LoggingContext.class));

        final IntegrationResponseDto response = pickConfirmIntegrationFacade.handlePickConfirm(dto);
        assertEquals(ORDER_NUMBER, response.getCode());
        assertFalse(response.isSuccessStatus());
    }

    /**
     * Verifies that if order validation throws AmbiguousIdException a negative response will be returned.
     * 
     * @throws Exception
     */
    @Test
    public void testHandlePickConfirmThrowsAmbiguousIdException() throws Exception {
        doThrow(new AmbiguousIdentifierException("This is an ambiguous identifier exception"))
                .when(fulfilmentService).processPickConfForConsignment(
                        anyString(), any(TargetConsignmentModel.class), anyString(), anyString(), (Integer)any(),
                        anyListOf(PickConfirmEntry.class), any(LoggingContext.class));

        final IntegrationResponseDto response = pickConfirmIntegrationFacade.handlePickConfirm(dto);
        assertEquals(ORDER_NUMBER, response.getCode());
        assertFalse(response.isSuccessStatus());
    }

    /**
     * Verifies that if order validation throws UnknownIdentifierException a negative response will be returned.
     * 
     * @throws Exception
     */
    @Test
    public void testHandlePickConfirmThrowsUnknownIdException() throws Exception {
        doThrow(new UnknownIdentifierException("This is an unknown identifier exception"))
                .when(fulfilmentService).processPickConfForConsignment(
                        anyString(), any(TargetConsignmentModel.class), anyString(), anyString(), (Integer)any(),
                        anyListOf(PickConfirmEntry.class), any(LoggingContext.class));

        final IntegrationResponseDto response = pickConfirmIntegrationFacade.handlePickConfirm(dto);
        assertEquals(ORDER_NUMBER, response.getCode());
        assertFalse(response.isSuccessStatus());
    }

    /**
     * Verifies that if a generic exception occurs a negative response will be returned.
     * 
     * @throws Exception
     */
    @Test(expected = Exception.class)
    public void testHandlePickConfirmThrowsGenericException() throws Exception {
        doThrow(new Exception("This is an unknown exception"))
                .when(fulfilmentService).processPickConfForConsignment(
                        anyString(), any(TargetConsignmentModel.class), anyString(), anyString(), (Integer)any(),
                        anyListOf(PickConfirmEntry.class), any(LoggingContext.class));

        final IntegrationResponseDto response2 = pickConfirmIntegrationFacade.handlePickConfirm(dto);
        assertFalse(response2.isSuccessStatus());
    }
}
