/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.validators;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetImportOnlineDatesValidatorTest {
    private final TargetImportOnlineDatesValidator validator = new TargetImportOnlineDatesValidator();

    private IntegrationProductDto productDto;

    @Before
    public void setup() {
        productDto = new IntegrationProductDto();
        productDto.setVariantCode("567890");
    }

    @Test
    public void testOnDateOffDateFormat() {
        productDto.setProductOnlineDate("25/102015 11:01:01");
        productDto.setProductOfflineDate("25/102015 11:01:01");
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNotNull()
                .isNotEmpty()
                .hasSize(2)
                .contains("Product online date has incorrect format, aborting product/variant import for: 567890",
                        "Product offline date has incorrect format, aborting product/variant import for: 567890");
    }

    @Test
    public void testOnBothDatesbeingSame() {
        productDto.setProductOnlineDate("2015-11-25 11:01:01");
        productDto.setProductOfflineDate("2015-11-25 11:01:01");
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNotNull()
                .isNotEmpty()
                .hasSize(1)
                .contains("Product offline date is before online date, aborting product/variant import for: 567890");
    }

    @Test
    public void testOfflineBeforeOnline() {
        productDto.setProductOnlineDate("2015-11-25 11:01:01");
        productDto.setProductOfflineDate("2015-11-25 11:01:00");
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNotNull()
                .isNotEmpty()
                .hasSize(1)
                .contains("Product offline date is before online date, aborting product/variant import for: 567890");
    }

    @Test
    public void testValid() {
        productDto.setProductOnlineDate("2015-11-25 11:01:01");
        productDto.setProductOfflineDate("2015-11-25 11:01:02");
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNullOrEmpty();
    }
}
