/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.validators;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetImportCategoryValidatorTest {
    @Mock
    private CategoryService categoryService;

    @InjectMocks
    private final TargetImportCategoryValidator targetImportCategoryValidator = new TargetImportCategoryValidator();

    @Mock
    private CategoryModel category;

    private IntegrationProductDto productDto;

    @Before
    public void setUp() {
        productDto = new IntegrationProductDto();
        productDto.setVariantCode("567890");
    }

    @Test
    public void testInvalidMessages() {
        final List<String> messages = targetImportCategoryValidator.validate(productDto);
        assertThat(messages)
                .isNotNull()
                .isNotEmpty()
                .hasSize(1)
                .contains(
                        "Missing PRIMARY CATEGORY (web storefront classification) for product, aborting product/variant import for: 567890");
    }

    @Test
    public void testInvalidPrimaryCategory() {
        productDto.setPrimaryCategory("W1234");
        productDto.setSecondaryCategory(Collections.singletonList("W56789"));
        productDto.setOriginalCategory("W51233");

        when(categoryService.getCategoryForCode("W1234")).thenThrow(
                new UnknownIdentifierException("Not found"));
        when(categoryService.getCategoryForCode("W56789")).thenThrow(
                new UnknownIdentifierException("Not found"));
        when(categoryService.getCategoryForCode("W51233")).thenThrow(
                new UnknownIdentifierException("Not found"));

        final List<String> messages = targetImportCategoryValidator.validate(productDto);
        assertThat(messages)
                .isNotNull()
                .isNotEmpty()
                .hasSize(3)
                .contains(
                        "Primary category W1234 does not exist in Hybris, aborting product/variant import for: 567890",
                        "Secondary category [W56789] does not exist in Hybris, aborting product/variant import for: 567890",
                        "Original category W51233 does not exist in Hybris, aborting product/variant import for: 567890");
    }


    @Test
    public void testValidValidator() {
        productDto.setPrimaryCategory("W1234");
        productDto.setSecondaryCategory(Collections.singletonList("W56789"));
        when(categoryService.getCategoryForCode("W1234")).thenReturn(category);
        when(categoryService.getCategoryForCode("W56789")).thenReturn(category);
        final List<String> messages = targetImportCategoryValidator.validate(productDto);
        assertThat(messages)
                .isNullOrEmpty();
    }

}
