/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.validators;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;


/**
 * @author gsing236
 *
 */
public class TargetImportPreOrderValidatorTest {

    private IntegrationProductDto dto;

    private final TargetImportPreOrderValidator validator = new TargetImportPreOrderValidator();

    @Before
    public void setUp() {
        validator.setHoursBetEmbargoDateAndEndDate(48);
        dto = new IntegrationProductDto();
        dto.setVariantCode("123456789");
    }

    @Test
    public void testInvalidFormatDates() {
        dto.setPreOrderEndDate("2017/03/28 12:23:56");
        dto.setPreOrderStartDate("2017/03/28 12:23:56");
        dto.setPreOrderEmbargoReleaseDate("2017/03/28 12:23:56");

        final List<String> messages = validator.validate(dto);

        assertThat(messages).isNotEmpty().hasSize(3).contains(
                "Product pre order start date has incorrect format, aborting product/variant import for: 123456789",
                "Product pre order end date has incorrect format, aborting product/variant import for: 123456789",
                "Product pre order embargo release date has incorrect format, aborting product/variant import for: 123456789");
    }

    @Test
    public void testErrorWhenPreOrderEndDateBeforeStartDate() {
        dto.setPreOrderEndDate("2018-02-28 11:01:01");
        dto.setPreOrderStartDate("2018-03-01 12:00:00");

        final List<String> messages = validator.validate(dto);

        assertThat(messages).isNotEmpty().hasSize(1).contains(
                "Product pre order end date is before pre order start date, aborting product/variant import for: 123456789");
    }

    @Test
    public void testPreOnlineQuatityError() {
        dto.setPreOrderOnlineQuantity(new Integer(-6));

        final List<String> messages = validator.validate(dto);

        assertThat(messages).isNotEmpty().hasSize(1).contains(
                "Product pre order online quantity cannot be less than zero, aborting product/variant import for: 123456789");

    }

    @Test
    public void testPreOrderEndDateAfterEmbargoDate() {
        dto.setPreOrderEndDate("2018-03-01 11:01:01");
        dto.setPreOrderEmbargoReleaseDate("2018-02-28 12:00:00");

        final List<String> messages = validator.validate(dto);

        assertThat(messages).isNotEmpty().hasSize(1).contains(
                "Pre-Order End Date/Time has to finish >48 hours before ODBMS Embargo Release Date/Time, aborting product/variant import for: 123456789");
    }

    @Test
    public void testPreOrderEndDateLessThan48HoursOfEmbargoDateFailure() {
        dto.setPreOrderEndDate("2018-02-27 12:01:01");
        dto.setPreOrderEmbargoReleaseDate("2018-03-01 12:00:00");

        final List<String> messages = validator.validate(dto);

        assertThat(messages).isNotEmpty().hasSize(1).contains(
                "Pre-Order End Date/Time has to finish >48 hours before ODBMS Embargo Release Date/Time, aborting product/variant import for: 123456789");
    }

    @Test
    public void testPreOrderEndDateFinishes3daysBeforeEmbargoDate() {
        dto.setPreOrderEndDate("2018-03-15 12:01:01");
        dto.setPreOrderEmbargoReleaseDate("2018-03-18 12:00:00");

        final List<String> messages = validator.validate(dto);

        assertThat(messages).isEmpty();
    }

    @Test
    public void testPreOrderEndDate1SecondLessThan48HoursOfEmbargoDateSuccess() {
        dto.setPreOrderEndDate("2018-02-28 11:01:01");
        dto.setPreOrderEmbargoReleaseDate("2018-03-02 11:01:02");

        final List<String> messages = validator.validate(dto);

        assertThat(messages).isEmpty();
    }

    @Test
    public void testPreOrderEndDateLessThan48HoursOfEmbargoDateEdgeCase() {
        // test difference of time-stamp
        dto.setPreOrderEndDate("2018-02-28 13:01:01");
        dto.setPreOrderEmbargoReleaseDate("2018-03-02 11:01:01");

        final List<String> messages = validator.validate(dto);

        assertThat(messages).isNotEmpty().hasSize(1).contains(
                "Pre-Order End Date/Time has to finish >48 hours before ODBMS Embargo Release Date/Time, aborting product/variant import for: 123456789");
    }

    @Test
    public void testPreOrderEndDateExactly48HoursOfEmbargoDateSuccess() {
        dto.setPreOrderEndDate("2018-03-01 11:01:01");
        dto.setPreOrderEmbargoReleaseDate("2018-03-03 11:01:01");

        final List<String> messages = validator.validate(dto);

        assertThat(messages).isEmpty();
    }
}
