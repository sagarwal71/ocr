/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.validators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.Assert;

import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;


/**
 * @author Pradeep
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetImportWarehouseValidatorTest {

    @InjectMocks
    private final TargetImportWarehouseValidator targetImportWarehouseValidator = new TargetImportWarehouseValidator();

    @Mock
    private TargetWarehouseService warehouseService;

    @Test
    public void testValidateSingleWarehouseScenario() {
        final IntegrationProductDto product = new IntegrationProductDto();
        final List<String> warehouses = new ArrayList<>();
        product.setWarehouses(warehouses);
        warehouses.add("CnpWarehouse");
        Mockito.when(warehouseService.getWarehouseForCode(Mockito.anyString())).thenReturn(new WarehouseModel());
        Assert.isTrue(CollectionUtils.isEmpty(targetImportWarehouseValidator.validate(product)),
                "Product must be empty");
    }

    @Test
    public void testValidateSingleInvalidWarehouseScenario() {
        final IntegrationProductDto product = new IntegrationProductDto();
        final List<String> warehouses = new ArrayList<>();
        product.setWarehouses(warehouses);
        warehouses.add("CnpWarehouse");
        Mockito.when(warehouseService.getWarehouseForCode(Mockito.anyString())).thenThrow(
                new UnknownIdentifierException("unknown warehouseModel"));
        Assert.isTrue(CollectionUtils.isNotEmpty(targetImportWarehouseValidator.validate(product)),
                "Product must not be empty");
        Mockito.verify(warehouseService).getWarehouseForCode(Mockito.anyString());
    }

    @Test
    public void testValidatemultipleWarehouseScenario() {
        final IntegrationProductDto product = new IntegrationProductDto();
        final List<String> warehouses = new ArrayList<>();
        product.setWarehouses(warehouses);
        warehouses.add("CnpWarehouse");
        warehouses.add("fastlinewarehouse");
        Mockito.when(warehouseService.getWarehouseForCode(Mockito.anyString())).thenReturn(new WarehouseModel());
        Assert.isTrue(CollectionUtils.isNotEmpty(targetImportWarehouseValidator.validate(product)),
                "Product must not be empty");
    }
}
