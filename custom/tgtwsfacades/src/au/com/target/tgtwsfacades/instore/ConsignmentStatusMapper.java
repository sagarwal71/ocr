/**
 * 
 */
package au.com.target.tgtwsfacades.instore;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;


/**
 * Maps hybris consignment status to ofc ones
 * 
 * @author sbryan6
 *
 */
public interface ConsignmentStatusMapper {

    /**
     * Map the given hybris ConsignmentStatus to the OFC one
     * 
     * @param status
     * @return status
     */
    String getOFCStatus(ConsignmentStatus status);

    /**
     * is consignment in open state
     * 
     * @param status
     * @return boolean
     */
    boolean isOpen(ConsignmentStatus status);

    /**
     * is consignment in InProgress state
     * 
     * @param status
     * @return boolean
     */
    boolean isInProgress(ConsignmentStatus status);


    /**
     * is consignment picked
     *
     * @param status
     *            the status
     * @return true, if is picked
     */
    boolean isPicked(ConsignmentStatus status);

    /**
     * is consignment packed
     *
     * @param status
     *            the status
     * @return true, if is picked
     */
    boolean isPacked(ConsignmentStatus status);

    /**
     * is consignment in completed state
     * 
     * @param status
     * @return boolean
     */
    boolean isCompleted(ConsignmentStatus status);

    /**
     * is consignment in rejected state
     * 
     * @param status
     * @return boolean
     */
    boolean isRejected(ConsignmentStatus status);

}
