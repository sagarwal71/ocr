/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.consignments;

import java.util.List;

import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtwsfacades.instore.dto.StandardParcel;


/**
 * @author smishra1
 *
 */
public class StandardParcelDetailsData extends BaseResponseData {
    private List<StandardParcel> standardParcels;

    /**
     * @return the standardParcels
     */
    public List<StandardParcel> getStandardParcels() {
        return standardParcels;
    }

    /**
     * @param standardParcels
     *            the standardParcels to set
     */
    public void setStandardParcels(final List<StandardParcel> standardParcels) {
        this.standardParcels = standardParcels;
    }

}
