/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.consignments;

import java.util.List;

import au.com.target.tgtfacades.response.data.BaseResponseData;


/**
 * @author smudumba
 *
 */
public class ConsignmentsResponseData extends BaseResponseData {

    private List<Consignment> consignments;
    private Pagination pagination;

    /**
     * Constructor
     */
    public ConsignmentsResponseData() {

    }

    /**
     * @return the consignments
     */
    public List<Consignment> getConsignments() {
        return consignments;
    }

    /**
     * @param consignments
     *            the consignments to set
     */
    public void setConsignments(final List<Consignment> consignments) {
        this.consignments = consignments;
    }

    /**
     * @return the pagination
     */
    public Pagination getPagination() {
        return pagination;
    }

    /**
     * @param pagination
     *            the pagination to set
     */
    public void setPagination(final Pagination pagination) {
        this.pagination = pagination;
    }



}
