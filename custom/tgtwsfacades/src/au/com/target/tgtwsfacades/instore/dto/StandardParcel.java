/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto;

/**
 * @author smishra1
 *
 */
public class StandardParcel {
    private String description;
    private Double width;
    private Double height;
    private Double length;

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return the width
     */
    public Double getWidth() {
        return width;
    }

    /**
     * @param width
     *            the width to set
     */
    public void setWidth(final Double width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public Double getHeight() {
        return height;
    }

    /**
     * @param height
     *            the height to set
     */
    public void setHeight(final Double height) {
        this.height = height;
    }

    /**
     * @return the length
     */
    public Double getLength() {
        return length;
    }

    /**
     * @param length
     *            the length to set
     */
    public void setLength(final Double length) {
        this.length = length;
    }


}
