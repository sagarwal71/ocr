/**
 * 
 */
package au.com.target.tgtwsfacades.instore.converters;

import java.text.DecimalFormat;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtauspost.helper.ConsignmentDeliveryAddressNamer;
import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.postcode.service.TargetPostCodeService;
import au.com.target.tgtfulfilment.model.ConsignmentParcelModel;
import au.com.target.tgtwsfacades.instore.dto.auspost.Address;
import au.com.target.tgtwsfacades.instore.dto.auspost.ChargeZone;
import au.com.target.tgtwsfacades.instore.dto.auspost.ConsignmentSummary;


/**
 * @author smishra1
 *
 */
public class HardCopyManifestConsignmentConverter {
    private static final Logger LOG = Logger.getLogger(HardCopyManifestConsignmentConverter.class);
    private TargetPostCodeService targetPostCodeService;
    private ConsignmentDeliveryAddressNamer consignmentDeliveryAddressNamer;

    public ConsignmentSummary convert(final TargetConsignmentModel consignment) {
        final DecimalFormat roundUp = new DecimalFormat("#.###");
        final ConsignmentSummary consignmentSummary = new ConsignmentSummary();
        consignmentSummary.setConsignmentId(consignment.getTrackingID());
        consignmentSummary.setTotalArticles(consignment.getParcelCount());
        final ChargeZone chargeZone = new ChargeZone();
        final Address deliveryAddress = new Address();
        deliveryAddress.setName(consignmentDeliveryAddressNamer.getDeliveryName(consignment));
        if (null != consignment.getShippingAddress()) {
            final PostCodeModel postCodeModel = targetPostCodeService.getPostCode(consignment
                    .getShippingAddress().getPostalcode());

            if (null != postCodeModel && null != postCodeModel.getAuspostChargeZone()) {
                chargeZone.setCode(postCodeModel.getAuspostChargeZone().getChargeCode());
                chargeZone.setDescription(postCodeModel.getAuspostChargeZone().getAreaName());
            }
            else {
                LOG.warn("No chargezone information set for : " + consignment.getCode());
            }
            deliveryAddress.setAddressLine1(consignment.getShippingAddress().getLine1());
            deliveryAddress.setAddressLine2(consignment.getShippingAddress().getLine2());
            deliveryAddress.setSuburb(consignment.getShippingAddress().getTown());
            deliveryAddress.setState(consignment.getShippingAddress().getDistrict());
            deliveryAddress.setPostcode(consignment.getShippingAddress().getPostalcode());
        }
        else {
            LOG.warn("Shipping Address null for the consignment : " + consignment.getCode());
        }
        consignmentSummary.setChargeZone(chargeZone);
        consignmentSummary.setDeliveryAddress(deliveryAddress);
        final Set<ConsignmentParcelModel> parcels = consignment.getParcelsDetails();
        double parcelTotalWeight = 0;
        if (!CollectionUtils.isEmpty(parcels)) {
            for (final ConsignmentParcelModel parcel : parcels) {
                if (null != parcel.getActualWeight()) {
                    parcelTotalWeight += parcel.getActualWeight().doubleValue();
                }
                else {
                    LOG.warn("Parcel weight not set : " + consignment.getCode());
                }
            }
        }
        else {
            LOG.warn("No parcel information found for : " + consignment.getCode());
        }
        consignmentSummary.setTotalWeight(Double.valueOf(roundUp.format(parcelTotalWeight)));
        return consignmentSummary;
    }

    @Required
    public void setTargetPostCodeService(final TargetPostCodeService targetPostCodeService) {
        this.targetPostCodeService = targetPostCodeService;
    }

    @Required
    public void setConsignmentDeliveryAddressNamer(final ConsignmentDeliveryAddressNamer
            consignmentDeliveryAddressNamer) {
        this.consignmentDeliveryAddressNamer = consignmentDeliveryAddressNamer;
    }
}
