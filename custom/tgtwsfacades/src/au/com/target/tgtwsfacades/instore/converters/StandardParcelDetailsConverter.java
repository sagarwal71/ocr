/**
 * 
 */
package au.com.target.tgtwsfacades.instore.converters;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.util.Assert;

import au.com.target.tgtfulfilment.model.TargetStandardParcelModel;
import au.com.target.tgtwsfacades.instore.dto.StandardParcel;


/**
 * @author smishra1
 *
 */
public class StandardParcelDetailsConverter implements Converter<TargetStandardParcelModel, StandardParcel> {

    @Override
    public StandardParcel convert(final TargetStandardParcelModel source)
            throws ConversionException {
        final StandardParcel parcelDetailsDto = new StandardParcel();
        return convert(source, parcelDetailsDto);
    }

    @Override
    public StandardParcel convert(final TargetStandardParcelModel model,
            final StandardParcel parcelDetailsDto)
            throws ConversionException {
        validateDaoObject(model);
        parcelDetailsDto.setDescription(model.getDescription());
        parcelDetailsDto.setHeight(model.getHeight());
        parcelDetailsDto.setLength(model.getLength());
        parcelDetailsDto.setWidth(model.getWidth());
        return parcelDetailsDto;
    }

    /**
     * Method to validate the mandatory parameters before setting it to the DTO
     * 
     * @param model
     *            The TargetStandardParcelModel
     */
    private void validateDaoObject(final TargetStandardParcelModel model) {
        Assert.notNull(model.getDescription(), "Description cannot be null");
        Assert.notNull(model.getWidth(), "Width cannot be null");
        Assert.notNull(model.getHeight(), "Height cannot be null");
        Assert.notNull(model.getLength(), "Length cannot be null");
    }


}
