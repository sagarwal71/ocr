/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.consignments;

import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author rsamuel3
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Parcel {
    private Double height;
    private Double weight;
    private Double length;
    private Double width;

    /**
     * @return the height
     */
    public Double getHeight() {
        return height;
    }

    /**
     * @param height
     *            the height to set
     */
    public void setHeight(final Double height) {
        this.height = height;
    }

    /**
     * @return the weight
     */
    public Double getWeight() {
        return weight;
    }

    /**
     * @param weight
     *            the weight to set
     */
    public void setWeight(final Double weight) {
        this.weight = weight;
    }

    /**
     * @return the length
     */
    public Double getLength() {
        return length;
    }

    /**
     * @param length
     *            the length to set
     */
    public void setLength(final Double length) {
        this.length = length;
    }

    /**
     * @return the width
     */
    public Double getWidth() {
        return width;
    }

    /**
     * @param width
     *            the width to set
     */
    public void setWidth(final Double width) {
        this.width = width;
    }

}
