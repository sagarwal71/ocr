/**
 * 
 */
package au.com.target.tgtwsfacades.instore.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.user.UserService;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.StoreEmployeeModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.Error;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfluent.exception.FluentBaseException;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.service.FluentStockLookupService;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectState;
import au.com.target.tgtfulfilment.dispatchlabel.service.DispatchLabelService;
import au.com.target.tgtfulfilment.dto.ConsignmentParcelDTO;
import au.com.target.tgtfulfilment.dto.PickConfirmEntry;
import au.com.target.tgtfulfilment.dto.TargetConsignmentPageResult;
import au.com.target.tgtfulfilment.exceptions.ConsignmentStatusValidationException;
import au.com.target.tgtfulfilment.exceptions.InvalidParameterException;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;
import au.com.target.tgtfulfilment.fulfilmentservice.PickConsignmentUpdater;
import au.com.target.tgtfulfilment.fulfilmentservice.TargetFulfilmentService;
import au.com.target.tgtfulfilment.integration.dto.LabelLayout;
import au.com.target.tgtfulfilment.integration.dto.LabelResponseDTO;
import au.com.target.tgtfulfilment.integration.dto.ManifestResponseDTO;
import au.com.target.tgtfulfilment.logger.InstoreFulfilmentLogger;
import au.com.target.tgtfulfilment.manifest.service.TransmitManifestService;
import au.com.target.tgtfulfilment.model.TargetManifestModel;
import au.com.target.tgtfulfilment.model.TargetStandardParcelModel;
import au.com.target.tgtfulfilment.service.TargetManifestService;
import au.com.target.tgtfulfilment.service.TargetStandardParcelService;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;
import au.com.target.tgtsale.stock.dto.response.StockUpdateResponseDto;
import au.com.target.tgtsale.stock.service.TargetStoreStockService;
import au.com.target.tgtwsfacades.instore.ConsignmentStatusMapper;
import au.com.target.tgtwsfacades.instore.TargetInStoreIntegrationFacade;
import au.com.target.tgtwsfacades.instore.converters.ConsignmentParcelDTOConverter;
import au.com.target.tgtwsfacades.instore.converters.HardCopyManifestConverter;
import au.com.target.tgtwsfacades.instore.converters.InstoreBasicManifestConverter;
import au.com.target.tgtwsfacades.instore.converters.InstoreConsignmentConverter;
import au.com.target.tgtwsfacades.instore.converters.InstoreConsignmentEntryConverter;
import au.com.target.tgtwsfacades.instore.converters.InstoreManifestConverter;
import au.com.target.tgtwsfacades.instore.converters.ProductStockLevelConverter;
import au.com.target.tgtwsfacades.instore.converters.StandardParcelDetailsConverter;
import au.com.target.tgtwsfacades.instore.converters.StockVisResponseToProductStockLevelInstoreConverter;
import au.com.target.tgtwsfacades.instore.dto.OfcErrorConstants;
import au.com.target.tgtwsfacades.instore.dto.StandardParcel;
import au.com.target.tgtwsfacades.instore.dto.auspost.ManifestHardCopy;
import au.com.target.tgtwsfacades.instore.dto.consignments.Consignment;
import au.com.target.tgtwsfacades.instore.dto.consignments.ConsignmentEntry;
import au.com.target.tgtwsfacades.instore.dto.consignments.ConsignmentsResponseData;
import au.com.target.tgtwsfacades.instore.dto.consignments.Pagination;
import au.com.target.tgtwsfacades.instore.dto.consignments.Parcel;
import au.com.target.tgtwsfacades.instore.dto.consignments.Parcels;
import au.com.target.tgtwsfacades.instore.dto.consignments.Product;
import au.com.target.tgtwsfacades.instore.dto.consignments.ProductStockLevelInstore;
import au.com.target.tgtwsfacades.instore.dto.consignments.StandardParcelDetailsData;
import au.com.target.tgtwsfacades.instore.dto.dashboard.ConsignmentStats;
import au.com.target.tgtwsfacades.instore.dto.dashboard.DashboardResponseData;
import au.com.target.tgtwsfacades.instore.dto.manifests.Manifest;
import au.com.target.tgtwsfacades.instore.dto.manifests.ManifestHardCopyResponseData;
import au.com.target.tgtwsfacades.instore.dto.manifests.ManifestsResponseData;
import au.com.target.tgtwsfacades.instore.dto.response.data.ProductSohResponseData;
import au.com.target.tgtwsfacades.instore.dto.user.Store;
import au.com.target.tgtwsfacades.instore.dto.user.User;
import au.com.target.tgtwsfacades.instore.dto.user.UserResponseData;
import au.com.target.tgtwsfacades.instore.mutex.StoreMutexProvider;


/**
 * Implementation of TargetInStoreIntegrationFacadeImpl
 * 
 * @author jjayawa1
 *
 */
public class TargetInStoreIntegrationFacadeImpl implements TargetInStoreIntegrationFacade {
    protected static final Logger LOG = Logger.getLogger(TargetInStoreIntegrationFacadeImpl.class);
    private static final InstoreFulfilmentLogger INSTORE_LOGGER = new InstoreFulfilmentLogger(
            TargetInStoreIntegrationFacadeImpl.class);
    private static final String FEATURESWITCH_DASHBOARD_PERFORMANCE = "ofc.dashboard.performance";

    private TargetFulfilmentService targetFulfilmentService;
    private TargetStoreConsignmentService targetStoreConsignmentService;
    private InstoreConsignmentConverter instoreConsignmentConverter;
    private InstoreConsignmentEntryConverter instoreConsignmentEntryConverter;
    private ConsignmentStatusMapper consignmentStatusMapper;
    private TargetManifestService targetManifestService;
    private TransmitManifestService transmitManifestService;
    private TargetPointOfServiceService targetPointOfServiceService;
    private InstoreManifestConverter instoreManifestConverter;
    private InstoreBasicManifestConverter instoreBasicManifestConverter;
    private UserService userService;
    private ConsignmentParcelDTOConverter consignmentParcelDTOConverter;
    private TargetStandardParcelService targetStandardParcelService;
    private StandardParcelDetailsConverter standardParcelDetailsConverter;
    private StockVisResponseToProductStockLevelInstoreConverter stockVisResponseToProductStockLevelInstoreConverter;
    private DispatchLabelService dispatchLabelService;
    private HardCopyManifestConverter hardCopyManifestConverter;
    private ProductStockLevelConverter productStockLevelConverter;
    private TargetStoreStockService targetStoreStockService;
    private TargetFeatureSwitchService featureSwitchService;
    private PickConsignmentUpdater pickConsignmentUpdater;

    private final StoreMutexProvider storeMutexProvider = new StoreMutexProvider();

    private List<ConsignmentStatus> consignmentStatusAllowedForLabel;

    private FluentStockLookupService fluentStockLookupService;

    @Override
    public Response createErrorResponse(final String message, final String code) {
        final Response response = new Response(false);
        final BaseResponseData responseData = new BaseResponseData();
        final Error error = new Error(code);
        error.setMessage(message);
        responseData.setError(error);
        response.setData(responseData);
        return response;
    }

    @Override
    public UserResponseData getLoggedInUser() {

        final UserModel userModel = userService.getCurrentUser();

        final String uid = ((userModel != null) ? userModel.getUid() : "Null");
        INSTORE_LOGGER.logActionInfo("LoginUser", uid);

        final UserResponseData userData = new UserResponseData();
        userData.setUser(convertUser(userModel));

        return userData;
    }

    @Override
    public Response getConsignmentsForStore(final Integer storeNumber, final int offset,
            final int recsPerPage, final int lastXDays) {

        INSTORE_LOGGER.logStoreAction("GetConsignmentsList", storeNumber);

        Response response = new Response(true);

        try {

            final ConsignmentsResponseData responseData = getConsignmentDtosByPage(
                    storeNumber, offset, recsPerPage, lastXDays);
            response.setData(responseData);
        }
        catch (final Exception e) {
            response = createSystemErrorResponse(e);
            INSTORE_LOGGER.logStoreActionError("GetConsignmentsList", storeNumber, e);
        }

        return response;
    }

    @Override
    public Response getConsignmentsByStatusForToday(final Integer storeNumber) {
        INSTORE_LOGGER.logStoreAction("DashboardToday", storeNumber);

        Response response = new Response(true);

        try {
            final DashboardResponseData responseData = new DashboardResponseData();
            responseData.setConsignmentStats(getConsignmentStatsForToday(storeNumber));
            response.setData(responseData);
        }
        catch (final Exception e) {
            response = createSystemErrorResponse(e);
            INSTORE_LOGGER.logStoreActionError("DashboardToday", storeNumber, e);
        }

        return response;
    }

    @Override
    public Response getConsignmentsByStatusForYesterday(final Integer storeNumber) {
        INSTORE_LOGGER.logStoreAction("DashboardYesterday", storeNumber);

        Response response = new Response(true);

        try {
            final DashboardResponseData responseData = new DashboardResponseData();
            responseData.setConsignmentStats(getConsignmentStatsForYesterday(storeNumber));
            response.setData(responseData);
        }
        catch (final Exception e) {
            response = createSystemErrorResponse(e);
            INSTORE_LOGGER.logStoreActionError("DashboardYesterday", storeNumber, e);
        }

        return response;
    }

    @Override
    public Response pickForPackConsignmentInstore(final String consignmentCode, final Integer storeNumber) {
        Response response = null;
        try {
            final TargetConsignmentModel consignmentModel = targetStoreConsignmentService
                    .getConsignmentByCode(consignmentCode);
            logStoreConsignmentAction("PickForPack", storeNumber, consignmentCode, consignmentModel);
            synchronized (storeMutexProvider.getMutex(storeNumber)) {
                targetFulfilmentService.processPickForInstoreFulfilment(consignmentCode);
            }
            logStoreConsignmentAction("PickForPackSuccess", storeNumber, consignmentCode, consignmentModel);
            response = new Response(true);
        }
        catch (final NotFoundException e) {
            response = createNotFoundErrorResponse(e);
            INSTORE_LOGGER.logStoreConsignmentActionError("PickForPack", storeNumber, consignmentCode, e);
        }
        catch (final ConsignmentStatusValidationException e) {
            response = createConsignmentStatusValidationErrorResponse(e);
            INSTORE_LOGGER.logStoreConsignmentActionError("PickForPack", storeNumber, consignmentCode, e);
        }
        catch (final Exception e) {
            response = createSystemErrorResponse(e);
            INSTORE_LOGGER.logStoreConsignmentActionError("PickForPack", storeNumber, consignmentCode, e);
        }

        return response;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.instore.TargetInStoreIntegrationFacade#completeConsignmentInstore(java.lang.String)
     */
    @Override
    public Response completeConsignmentInstore(final String consignmentCode, final Integer storeNumber,
            final Integer parcelsCount, final Parcels parcels) {
        int parcelCount = 0;
        Response response = null;
        List<ConsignmentParcelDTO> consignmentParcels = null;
        try {
            final TargetConsignmentModel consignmentModel = targetStoreConsignmentService
                    .getConsignmentByCode(consignmentCode);
            logStoreConsignmentAction("CompleteConsignment", storeNumber, consignmentCode, consignmentModel);
            if (parcelsCount != null) {
                parcelCount = validateParcelCount(parcelsCount);
            }
            else if (parcels != null) {
                consignmentParcels = getParcelDetails(parcels.getParcels());
                parcelCount = consignmentParcels.size();
            }

            if (parcelCount > 0) {

                synchronized (storeMutexProvider.getMutex(storeNumber)) {
                    targetFulfilmentService.processCompleteForInstoreFulfilment(consignmentCode, parcelCount,
                            consignmentParcels);
                }

                response = new Response(true);
                logStoreConsignmentAction("CompleteConsignmentSuccess", storeNumber, consignmentCode, consignmentModel);
            }
            else {
                throw new InvalidParameterException("Parcel count has to be positive");
            }
        }
        catch (final NotFoundException e) {
            response = createNotFoundErrorResponse(e);
            INSTORE_LOGGER.logStoreConsignmentActionError("CompleteConsignment", storeNumber, consignmentCode, e);
        }
        catch (final ConsignmentStatusValidationException e) {
            response = createConsignmentStatusValidationErrorResponse(e);
            INSTORE_LOGGER.logStoreConsignmentActionError("CompleteConsignment", storeNumber, consignmentCode, e);
        }
        catch (final InvalidParameterException e) {
            response = createInvalidParamsErrorResponse(e);
            INSTORE_LOGGER.logStoreConsignmentActionError("CompleteConsignment", storeNumber, consignmentCode, e);
        }
        catch (final FluentBaseException e) {
            response = createFluentErrorResponse(e);
            INSTORE_LOGGER.logStoreConsignmentActionError("FLUENT_OFC_SYNC_ERROR : CompleteConsignment", storeNumber,
                    consignmentCode, e);
        }
        catch (final Exception e) {
            response = createSystemErrorResponse(e);
            INSTORE_LOGGER.logStoreConsignmentActionError("CompleteConsignment", storeNumber, consignmentCode, e);
        }

        return response;
    }

    @Override
    public Response rejectConsignmentForInstore(final String consignmentCode, final Integer storeNumber,
            final String instoreRejectReasonCode, final ConsignmentRejectState rejectState) {
        Response response = null;
        try {
            final TargetConsignmentModel consignmentModel = targetStoreConsignmentService
                    .getConsignmentByCode(consignmentCode);
            logStoreConsignmentAction("RejectConsignment", storeNumber, consignmentCode, consignmentModel);
            synchronized (storeMutexProvider.getMutex(storeNumber)) {
                targetFulfilmentService.processRejectInstoreFulfilment(consignmentCode, instoreRejectReasonCode,
                        rejectState);
            }
            response = new Response(true);
        }
        catch (final NotFoundException e) {
            response = createNotFoundErrorResponse(e);
            INSTORE_LOGGER.logStoreConsignmentActionError("RejectConsignment", storeNumber, consignmentCode, e);
        }
        catch (final ConsignmentStatusValidationException e) {
            response = createConsignmentStatusValidationErrorResponse(e);
            INSTORE_LOGGER.logStoreConsignmentActionError("RejectConsignment", storeNumber, consignmentCode, e);
        }
        catch (final FluentBaseException e) {
            response = createFluentErrorResponse(e);
            INSTORE_LOGGER.logStoreConsignmentActionError("FLUENT_OFC_SYNC_ERROR : RejectConsignment", storeNumber,
                    consignmentCode, e);
        }
        catch (final Exception e) {
            response = createSystemErrorResponse(e);
            INSTORE_LOGGER.logStoreConsignmentActionError("RejectConsignment", storeNumber, consignmentCode, e);
        }

        return response;
    }

    @Override
    public Response getConsignmentStatusForInstore(final String consignmentCode, final Integer storeNumber) {

        Response response = new Response(true);

        try {
            final ConsignmentsResponseData responseData = new ConsignmentsResponseData();
            responseData.setConsignments(getConsignmentStatusDtos(consignmentCode));

            INSTORE_LOGGER.logStoreConsignmentInfo("GetConsignmentStatus", storeNumber, consignmentCode,
                    getConsignmentStatusForLogging(responseData));

            response.setData(responseData);
        }
        catch (final NotFoundException e) {
            response = createNotFoundErrorResponse(e);
            INSTORE_LOGGER.logStoreConsignmentActionError("GetConsignmentStatus", storeNumber, consignmentCode, e);
        }
        catch (final Exception e) {
            response = createSystemErrorResponse(e);
            INSTORE_LOGGER.logStoreConsignmentActionError("GetConsignmentStatus", storeNumber, consignmentCode, e);
        }

        return response;
    }

    private String getConsignmentStatusForLogging(final ConsignmentsResponseData responseData) {
        String status = null;
        if (CollectionUtils.isNotEmpty(responseData.getConsignments())) {
            final Consignment con1 = responseData.getConsignments().iterator().next();
            if (con1 != null) {
                status = con1.getStatus();
            }
        }
        return status;
    }

    @Override
    public Response getConsignmentsNotManifestedForStore(final int storeNumber) {
        INSTORE_LOGGER.logStoreAction("NotManifestedConsignments", Integer.valueOf(storeNumber));

        Response response = new Response(true);
        final ManifestsResponseData responseData = new ManifestsResponseData();

        try {
            responseData.setManifests(getUnmanifestedConsignmentsForStore(storeNumber));
            response.setData(responseData);
        }
        catch (final Exception e) {
            response = createSystemErrorResponse(e);
            INSTORE_LOGGER.logStoreActionError("NotManifestedConsignments", Integer.valueOf(storeNumber), e);
        }

        return response;
    }

    /**
     * Get list of consignment dtos for the store
     * 
     * @param storeNumber
     * @return list
     */
    protected ConsignmentsResponseData getConsignmentDtosByPage(final Integer storeNumber, final int offset,
            final int recsPerPage, final int lastXDays) {
        final ConsignmentsResponseData responseData = new ConsignmentsResponseData();
        final List<Consignment> consignmentDtos = new ArrayList<>();

        final TargetConsignmentPageResult pageResult = getConsignmentsForStoreByPage(storeNumber, offset, recsPerPage,
                lastXDays);

        final Pagination pagination = new Pagination();
        pagination.setOffset(Integer.valueOf(offset));
        pagination.setPerPage(Integer.valueOf(recsPerPage));

        if (null != pageResult) {

            final List<TargetConsignmentModel> consignmentModels = pageResult.getConsignments();
            pagination.setTotalAvailable(Integer.valueOf(pageResult.getTotal()));

            if (CollectionUtils.isNotEmpty(consignmentModels)) {
                for (final TargetConsignmentModel conModel : consignmentModels) {
                    final Consignment conDto = instoreConsignmentConverter.convert(conModel);
                    consignmentDtos.add(conDto);
                }
            }

        }
        responseData.setConsignments(consignmentDtos);

        //assigning pagination data
        responseData.setPagination(pagination);

        return responseData;
    }

    /**
     * Get consignment as list for given consignment number, populated with status.
     * 
     * @param consignmentCode
     * @return list
     * @throws NotFoundException
     */
    protected List<Consignment> getConsignmentStatusDtos(final String consignmentCode) throws NotFoundException {

        final List<TargetConsignmentModel> consignmentModels = targetStoreConsignmentService
                .getConsignmentsByCodes(Collections.singletonList(consignmentCode));

        if (CollectionUtils.isEmpty(consignmentModels)) {

            throw new NotFoundException("Consignment not found: " + consignmentCode);
        }

        final List<Consignment> consignmentDtos = new ArrayList<>();

        for (final TargetConsignmentModel conModel : consignmentModels) {

            // For this reduced API we only populate code and mapped status
            final Consignment conDto = new Consignment();
            conDto.setCode(conModel.getCode());
            conDto.setStatus(consignmentStatusMapper.getOFCStatus(conModel.getStatus()));
            conDto.setRejectState(conModel.getRejectState());
            this.setConsignmentShippedByShipConfReceived(conModel, conDto);
            consignmentDtos.add(conDto);
        }

        return consignmentDtos;
    }

    /**
     * set consignment status to completed if already received the shipConfReceived
     * 
     * @param targetConsignmentModel
     * @param conDto
     */
    private void setConsignmentShippedByShipConfReceived(final TargetConsignmentModel targetConsignmentModel,
            final Consignment conDto) {
        if (BooleanUtils.isTrue(targetConsignmentModel.getShipConfReceived())) {
            conDto.setStatus(consignmentStatusMapper.getOFCStatus(ConsignmentStatus.SHIPPED));
        }
    }

    /**
     * Gets the unmanifested consignmnets for store.
     *
     * @param storeNumber
     *            the store number
     * @return the unmanifested consignmnets for store
     */
    protected List<Manifest> getUnmanifestedConsignmentsForStore(final int storeNumber) {
        final List<TargetConsignmentModel> consignmentModels = targetStoreConsignmentService
                .getConsignmentsNotManifestedForStore(storeNumber);

        if (CollectionUtils.isNotEmpty(consignmentModels)) {
            final Manifest manifest = new Manifest();
            final List<Consignment> consignments = new ArrayList<>();
            for (final TargetConsignmentModel conModel : consignmentModels) {
                final Consignment consignment = instoreConsignmentConverter.convert(conModel);
                consignments.add(consignment);
            }
            manifest.setConsignments(consignments);
            return Collections.singletonList(manifest);
        }

        return ListUtils.EMPTY_LIST;

    }

    private List<ConsignmentParcelDTO> getParcelDetails(final List<Parcel> parcelDetails)
            throws InvalidParameterException {
        final List<ConsignmentParcelDTO> consignmentParcels = new ArrayList<>();

        try {
            if (CollectionUtils.isNotEmpty(parcelDetails)) {
                for (final Parcel parcel : parcelDetails) {
                    final ConsignmentParcelDTO consignmentParcel = consignmentParcelDTOConverter.convert(parcel);
                    consignmentParcels.add(consignmentParcel);
                }
            }
        }
        catch (final ConversionException e) {
            throw new InvalidParameterException(e.getMessage());
        }

        return consignmentParcels;
    }

    private ConsignmentStats getConsignmentStatsForToday(final Integer storeNumber)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {


        final ConsignmentStats consignmentStats = new ConsignmentStats();
        final TargetPointOfServiceModel store = targetPointOfServiceService.getPOSByStoreNumber(storeNumber);


        if (featureSwitchService.isFeatureEnabled(FEATURESWITCH_DASHBOARD_PERFORMANCE)) {
            return getConsignmentStatsForTodayNewVer(storeNumber);
        }

        final List<TargetConsignmentModel> consignmentModels = targetStoreConsignmentService
                .getConsignmentsByDayForStore(store, 0);
        int openCons = 0, inProgressCons = 0, pickedCons = 0, completedCons = 0, rejectedCons = 0, packed = 0;


        if (CollectionUtils.isNotEmpty(consignmentModels)) {
            for (final TargetConsignmentModel consignmentModel : consignmentModels) {
                final ConsignmentStatus status = consignmentModel.getStatus();
                if (consignmentStatusMapper.isOpen(status)) {
                    openCons++;
                }
                else if (consignmentStatusMapper.isInProgress(status)) {
                    inProgressCons++;
                }
                else if (consignmentStatusMapper.isPicked(status)) {
                    pickedCons++;
                }
                else if (consignmentStatusMapper.isCompleted(status)) {
                    completedCons++;
                }
                else if (consignmentStatusMapper.isRejected(status)) {
                    rejectedCons++;
                }
                else if (consignmentStatusMapper.isPacked(status)) {
                    packed++;
                }
                else {
                    final String strStatus = status != null ? status.toString() : StringUtils.EMPTY;
                    LOG.warn(MessageFormat
                            .format(
                                    "FoundInvalidStatus : Found a consignment {0} with the status {1} not mapped for Dashboard",
                                    consignmentModel.getCode(), strStatus));
                }
            }
        }

        consignmentStats.setOpen(Integer.valueOf(openCons));
        consignmentStats.setInProgress(Integer.valueOf(inProgressCons));
        consignmentStats.setPicked(Integer.valueOf(pickedCons));
        consignmentStats.setCompleted(Integer.valueOf(completedCons));
        consignmentStats.setRejected(Integer.valueOf(rejectedCons));
        consignmentStats.setPacked(Integer.valueOf(packed));

        return consignmentStats;
    }



    private ConsignmentStats getConsignmentStatsForTodayNewVer(final Integer storeNumber)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        final ConsignmentStats consignmentStats = new ConsignmentStats();
        final TargetPointOfServiceModel store = targetPointOfServiceService.getPOSByStoreNumber(storeNumber);
        final Map<ConsignmentStatus, Integer> consignmentStatusMap = targetStoreConsignmentService
                .getConsignmentCountByStatus(store, 0);
        int openCons = 0, inProgressCons = 0, pickedCons = 0, completedCons = 0, rejectedCons = 0, packed = 0;
        if (MapUtils.isNotEmpty(consignmentStatusMap)) {
            for (final Map.Entry<ConsignmentStatus, Integer> entry : consignmentStatusMap.entrySet()) {
                final ConsignmentStatus status = entry.getKey();
                if (consignmentStatusMapper.isOpen(status)) {
                    openCons += entry.getValue().intValue();
                }
                else if (consignmentStatusMapper.isInProgress(status)) {
                    inProgressCons += entry.getValue().intValue();
                }
                else if (consignmentStatusMapper.isPicked(status)) {
                    pickedCons += entry.getValue().intValue();
                }
                else if (consignmentStatusMapper.isCompleted(status)) {
                    completedCons += entry.getValue().intValue();
                }
                else if (consignmentStatusMapper.isRejected(status)) {
                    rejectedCons += entry.getValue().intValue();
                }
                else if (consignmentStatusMapper.isPacked(status)) {
                    packed += entry.getValue().intValue();
                }
                else {
                    final String strStatus = status != null ? status.toString() : StringUtils.EMPTY;
                    LOG.warn(MessageFormat
                            .format(
                                    "FoundInvalidStatus : ConsignmentStatus={0} not mapped for Dashboard", strStatus));
                }
            }
        }

        consignmentStats.setOpen(Integer.valueOf(openCons));
        consignmentStats.setInProgress(Integer.valueOf(inProgressCons));
        consignmentStats.setPicked(Integer.valueOf(pickedCons));
        consignmentStats.setCompleted(Integer.valueOf(completedCons));
        consignmentStats.setRejected(Integer.valueOf(rejectedCons));
        consignmentStats.setPacked(Integer.valueOf(packed));

        return consignmentStats;
    }




    private ConsignmentStats getConsignmentStatsForYesterday(final Integer storeNumber)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        final TargetPointOfServiceModel store = targetPointOfServiceService.getPOSByStoreNumber(storeNumber);
        if (featureSwitchService.isFeatureEnabled(FEATURESWITCH_DASHBOARD_PERFORMANCE)) {
            return getConsignmentStatsForYesterdayNewVer(storeNumber);
        }
        final ConsignmentStats consignmentStats = new ConsignmentStats();

        final List<TargetConsignmentModel> consignmentModels = targetStoreConsignmentService
                .getConsignmentsByDayForStore(store, 1);

        int completedCons = 0, rejectedCons = 0;

        if (CollectionUtils.isNotEmpty(consignmentModels)) {
            for (final TargetConsignmentModel consignmentModel : consignmentModels) {
                if (consignmentStatusMapper.isCompleted(consignmentModel.getStatus())) {
                    completedCons++;
                }
                else if (consignmentStatusMapper.isRejected(consignmentModel.getStatus())) {
                    rejectedCons++;
                }
            }
        }

        consignmentStats.setOpen(null);
        consignmentStats.setInProgress(null);
        consignmentStats.setCompleted(Integer.valueOf(completedCons));
        consignmentStats.setRejected(Integer.valueOf(rejectedCons));

        return consignmentStats;
    }

    private ConsignmentStats getConsignmentStatsForYesterdayNewVer(final Integer storeNumber)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        final ConsignmentStats consignmentStats = new ConsignmentStats();
        final TargetPointOfServiceModel store = targetPointOfServiceService.getPOSByStoreNumber(storeNumber);
        final Map<ConsignmentStatus, Integer> consignmentStatusMap = targetStoreConsignmentService
                .getConsignmentCountByStatus(store, 1);

        int completedCons = 0, rejectedCons = 0;

        if (MapUtils.isNotEmpty(consignmentStatusMap)) {
            for (final Map.Entry<ConsignmentStatus, Integer> entry : consignmentStatusMap.entrySet()) {
                final ConsignmentStatus status = entry.getKey();
                if (consignmentStatusMapper.isCompleted(status)) {
                    completedCons += entry.getValue().intValue();
                }
                else if (consignmentStatusMapper.isRejected(status)) {
                    rejectedCons += entry.getValue().intValue();
                }
            }
        }

        consignmentStats.setOpen(null);
        consignmentStats.setInProgress(null);
        consignmentStats.setCompleted(Integer.valueOf(completedCons));
        consignmentStats.setRejected(Integer.valueOf(rejectedCons));

        return consignmentStats;
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.instore.TargetInStoreIntegrationFacade#getConsignmentsByCodes(java.util.List)
     */
    @Override
    public Response getConsignmentsByCodes(final List<String> consignmentCodes, final Integer storeNumber) {
        INSTORE_LOGGER.logStoreActionInfo("GetConsignmentsByCodes", storeNumber,
                getConsignmentCodesForLogging(consignmentCodes));

        final Response response = new Response(true);
        final ConsignmentsResponseData responseData = new ConsignmentsResponseData();
        responseData.setConsignments(getConsignmentDtosByCodes(consignmentCodes));
        response.setData(responseData);
        return response;
    }

    private String getConsignmentCodesForLogging(final List<String> consignmentCodes) {
        return (CollectionUtils.isNotEmpty(consignmentCodes) ? StringUtils.join(consignmentCodes, ',') : "empty");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.instore.TargetInStoreIntegrationFacade#waveConsignmentForInstore(java.lang.String)
     */
    @Override
    public Response waveConsignmentForInstore(final String consignmentCode, final Integer storeNumber) {
        Response response = new Response(true);
        try {
            final TargetConsignmentModel consignment = targetStoreConsignmentService
                    .getConsignmentByCode(consignmentCode);
            logStoreConsignmentAction("WaveConsignment", storeNumber, consignmentCode, consignment);
            synchronized (storeMutexProvider.getMutex(storeNumber)) {
                targetFulfilmentService.processWavedForInstoreFulfilment(consignmentCode);
            }
            logStoreConsignmentAction("WaveConsignmentSuccess", storeNumber, consignmentCode, consignment);
        }
        catch (final NotFoundException e) {
            response = createNotFoundErrorResponse(e);
            INSTORE_LOGGER.logStoreConsignmentActionError("WaveConsignment", storeNumber, consignmentCode, e);
        }
        catch (final ConsignmentStatusValidationException e) {
            response = createConsignmentStatusValidationErrorResponse(e);
            INSTORE_LOGGER.logStoreConsignmentActionError("WaveConsignment", storeNumber, consignmentCode, e);
        }
        catch (final FluentBaseException e) {
            response = createFluentErrorResponse(e);
            INSTORE_LOGGER.logStoreConsignmentActionError("FLUENT_OFC_SYNC_ERROR : WaveConsignment", storeNumber,
                    consignmentCode, e);
        }
        catch (final Exception e) {
            response = createSystemErrorResponse(e);
            INSTORE_LOGGER.logStoreConsignmentActionError("WaveConsignment", storeNumber, consignmentCode, e);
        }
        return response;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.instore.TargetInStoreIntegrationFacade#transmitManifestForStore(int)
     */
    @Override
    public Response transmitManifestForStore(final int storeNumber) {
        INSTORE_LOGGER.logStoreAction("TransmitManifest", Integer.valueOf(storeNumber));

        Response response = new Response(true);

        try {
            final ManifestsResponseData responseData = new ManifestsResponseData();

            final TargetPointOfServiceModel pointOfService = targetPointOfServiceService.getPOSByStoreNumber(Integer
                    .valueOf(storeNumber));

            // Only need to synchronize on the createManifest call. 
            // If there are concurrent threads then an exception is raised on the process that is blocked,
            // so the transmit will not run.
            final TargetManifestModel manifestModel;
            synchronized (storeMutexProvider.getMutex(Integer.valueOf(storeNumber))) {
                manifestModel = targetManifestService.createManifest(pointOfService);
            }

            // If this fails it will be retried by RetransmitManifestsJob
            final ManifestResponseDTO transmitResponse = transmitManifestService.transmitManifest(pointOfService,
                    manifestModel);
            if (!transmitResponse.isSuccess()) {
                INSTORE_LOGGER.logStoreActionInfoError(
                        "TransmitManifestFailure error=" + transmitResponse.getErrorCode(),
                        Integer.valueOf(storeNumber),
                        manifestModel.getCode(), null);
            }
            else {
                INSTORE_LOGGER.logStoreActionInfo("TransmitManifestSuccess", Integer.valueOf(storeNumber),
                        manifestModel.getCode());
            }


            final Manifest manifest = instoreBasicManifestConverter.convert(manifestModel);
            responseData.setManifests(Collections.singletonList(manifest));
            response.setData(responseData);
        }
        catch (final Exception e) {
            response = createSystemErrorResponse(e);
            INSTORE_LOGGER.logStoreActionError("TransmitManifest", Integer.valueOf(storeNumber), e);
        }

        return response;
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.instore.TargetInStoreIntegrationFacade#getManifestByCode(java.lang.String)
     */
    @Override
    public Response getManifestByCode(final String manifestCode, final Integer storeNumber) {
        INSTORE_LOGGER.logStoreActionInfo("GetManifest", storeNumber, manifestCode);

        LOG.info("In get manifest by code for manifestCode : " + manifestCode);
        Response response = new Response(true);

        try {
            final ManifestsResponseData responseData = new ManifestsResponseData();
            responseData.setManifests(getManifestDto(manifestCode));
            response.setData(responseData);
        }
        catch (final NotFoundException e) {
            response = createNotFoundErrorResponse(e);
            INSTORE_LOGGER.logStoreActionInfoError("GetManifest", storeNumber, manifestCode, e);
        }
        catch (final Exception e) {
            response = createSystemErrorResponse(e);
            INSTORE_LOGGER.logStoreActionInfoError("GetManifest", storeNumber, manifestCode, e);
        }

        return response;
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.instore.TargetInStoreIntegrationFacade#getStockLevelsForProductsInConsignment(java.lang.String, java.lang.Integer)
     */
    @Override
    public Response getStockLevelsForProductsInConsignment(final String consignmentCode,
            final Integer storeNumber) {
        final Response response = new Response(true);
        final ProductSohResponseData responseData = new ProductSohResponseData();
        final TargetConsignmentModel consignment = targetStoreConsignmentService.getConsignmentByCode(consignmentCode);
        final OrderModel order = (OrderModel)consignment.getOrder();
        final List<AbstractOrderEntryModel> orderEntries = order.getEntries();
        List<AbstractTargetVariantProductModel> products = null;
        if (CollectionUtils.isNotEmpty(orderEntries)) {
            products = getProductsFromOrder(orderEntries);
        }
        if (null != products) {

            responseData.setProducts(getProductStockLevels(storeNumber, products));
        }

        response.setData(responseData);
        return response;
    }

    /**
     * @param storeNumber
     * @param products
     */
    private List<ProductStockLevelInstore> getProductStockLevels(final Integer storeNumber,
            final List<AbstractTargetVariantProductModel> products) {

        if (featureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)) {
            return getProductStockLevelInStoreUsingFluent(products, storeNumber);
        }
        else {
            if (featureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.USE_CACHE_STOCK_FOR_FULFIMENT)) {
                return getProductStockLevelInstoreUsingCache(products, storeNumber);
            }
            else {

                final StockUpdateResponseDto stockUpdateDto = targetStoreStockService.getStockForProducts(products,
                        storeNumber);
                return getProductStockLevelInstore(stockUpdateDto, storeNumber.toString());
            }
        }
    }

    private List<ProductStockLevelInstore> getProductStockLevelInstoreUsingCache(
            final List<AbstractTargetVariantProductModel> products, final Integer store) {

        final List<String> itemCodes = getProductCodes(products);
        final StockVisibilityItemLookupResponseDto stockVisibilityItemLookupResponseDto = targetStoreStockService
                .lookupStockForItemsInStores(Arrays.asList(String.valueOf(store)), itemCodes);
        return stockVisResponseToProductStockLevelInstoreConverter
                .convert(stockVisibilityItemLookupResponseDto, String.valueOf(store));
    }

    private List<ProductStockLevelInstore> getProductStockLevelInStoreUsingFluent(
            final List<AbstractTargetVariantProductModel> products, final Integer store) {

        final List<String> itemCodes = getProductCodes(products);
        final String storeNumberString = String.valueOf(store);

        StockVisibilityItemLookupResponseDto stockVisibilityItemLookupResponseDto;
        try {
            stockVisibilityItemLookupResponseDto = fluentStockLookupService
                    .lookupStock(itemCodes, ImmutableList.of(storeNumberString));
        }
        catch (final FluentClientException e) {
            stockVisibilityItemLookupResponseDto = null;
        }

        return stockVisResponseToProductStockLevelInstoreConverter
                .convert(stockVisibilityItemLookupResponseDto, storeNumberString);
    }

    /**
     * @param products
     * @return
     */
    private List<String> getProductCodes(final List<AbstractTargetVariantProductModel> products) {
        final List<String> itemCodes = new ArrayList<>();
        for (final AbstractTargetVariantProductModel product : products) {
            itemCodes.add(product.getCode());
        }
        return itemCodes;
    }



    /**
     * @param stockUpdateDto
     * @return Lists of ProductStocks
     */
    private List<ProductStockLevelInstore> getProductStockLevelInstore(final StockUpdateResponseDto stockUpdateDto,
            final String storeNumber) {
        return productStockLevelConverter.convert(stockUpdateDto,
                storeNumber);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response getHardCopyManifestData(final String manifestCode, final Integer storeNumber) {
        INSTORE_LOGGER.logStoreActionInfo("GetManifestHardCopyData", storeNumber, manifestCode);
        Response response = new Response(true);
        try {
            final TargetPointOfServiceModel tpos = targetPointOfServiceService.getPOSByStoreNumber(Integer
                    .valueOf(storeNumber.intValue()));
            final TargetManifestModel manifestModel = targetManifestService.getManifestByCode(manifestCode);
            final ManifestHardCopy manifestHardCopy = hardCopyManifestConverter.convert(tpos, manifestModel);

            final ManifestHardCopyResponseData responseData = new ManifestHardCopyResponseData();
            responseData.setManifest(manifestHardCopy);
            response.setData(responseData);
        }
        catch (final NotFoundException | TargetUnknownIdentifierException e) {
            response = createNotFoundErrorResponse(e);
            INSTORE_LOGGER.logStoreActionInfoError("GetHardCopyManifestData", storeNumber, manifestCode, e);
        }
        catch (final Exception e) {
            response = createSystemErrorResponse(e);
            INSTORE_LOGGER.logStoreActionInfoError("GetHardCopyManifestData", storeNumber, manifestCode, e);
        }

        return response;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.instore.TargetInStoreIntegrationFacade#getManifestHistoryForStore(int)
     */
    @Override
    public Response getManifestHistoryForStore(final int storeNumber) {
        INSTORE_LOGGER.logStoreAction("GetManifestHistory", Integer.valueOf(storeNumber));

        Response response = new Response(true);

        try {
            final ManifestsResponseData responseData = new ManifestsResponseData();
            responseData.setManifests(getManifestHistoryDtos(storeNumber));
            response.setData(responseData);
        }
        catch (final Exception e) {
            response = createSystemErrorResponse(e);
            INSTORE_LOGGER.logStoreActionError("GetManifestHistory", Integer.valueOf(storeNumber), e);
        }

        return response;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response getStandardParcelDetails() {
        Response response = new Response(true);
        try {
            final StandardParcelDetailsData reponseData = new StandardParcelDetailsData();
            reponseData.setStandardParcels(getStandardParcelDetailList());
            response.setData(reponseData);
        }
        catch (final Exception e) {
            response = createSystemErrorResponse(e);
        }
        return response;
    }

    @Override
    public byte[] getConsignmentDispatchLabel(final String consignmentCode, final Integer storeNumber,
            final String layout, final Boolean branding) {

        INSTORE_LOGGER.logStoreActionInfo("GetDispatchLabel", storeNumber, layout);

        // Read parameters
        final LabelLayout layoutEnum = getLabelLayout(layout);
        final boolean doBranding = getDoBranding(branding);

        // Get and Validate consignment
        final TargetConsignmentModel consignment = getValidatedConsignmentForLabel(consignmentCode, storeNumber);
        if (consignment == null) {
            return null;
        }

        try {
            final TargetPointOfServiceModel tpos = targetPointOfServiceService.getPOSByStoreNumber(storeNumber);

            final LabelResponseDTO response = dispatchLabelService.getDispatchLabel(tpos, consignment, layoutEnum,
                    doBranding);

            if (response == null) {
                INSTORE_LOGGER.logStoreConsignmentActionError("GetDispatchLabel", storeNumber, consignmentCode,
                        "Response is null");
                return null;
            }

            if (response.isSuccess()) {
                logStoreConsignmentAction("GetDispatchLabelSuccess", storeNumber, consignmentCode, consignment);
                return response.getData();
            }

            final String message = "ResponseError errorCode=" + response.getErrorCode() + ", errorValue="
                    + response.getErrorValue();
            INSTORE_LOGGER.logStoreConsignmentActionError("GetDispatchLabel", storeNumber, consignmentCode, message);
        }
        catch (final Exception e) {
            INSTORE_LOGGER.logStoreConsignmentActionError("GetDispatchLabel", storeNumber, consignmentCode, e);
        }

        return null;
    }


    @Override
    public Response updateShippedQty(final Consignment consignment, final Integer storeNumber) {


        Response response = new Response(true);
        try {
            final List<PickConfirmEntry> pickConfirmEntries = convertPickConfirmEntryFromConsignment(consignment);
            final TargetConsignmentModel consignmentModel = targetStoreConsignmentService
                    .getConsignmentByCode(consignment.getCode());
            pickConsignmentUpdater.updateConsignmentShipQuantities(consignmentModel, pickConfirmEntries);
            logStoreConsignmentAction("UpdateShippedQty", storeNumber, consignment.getCode(), consignmentModel);
        }
        catch (final Exception e) {
            response = createSystemErrorResponse(e);
            INSTORE_LOGGER.logStoreConsignmentActionError("UpdateShippedQty", storeNumber, consignment.getCode(), e);
        }
        return response;
    }

    private List<PickConfirmEntry> convertPickConfirmEntryFromConsignment(final Consignment consignment) {
        final List<PickConfirmEntry> entries = new ArrayList<>();
        final List<ConsignmentEntry> consignmentEntries = consignment.getEntries();
        if (CollectionUtils.isNotEmpty(consignmentEntries)) {
            for (final ConsignmentEntry entry : consignmentEntries) {
                if (entry.getProduct() != null && StringUtils.isNotEmpty(entry.getProduct().getCode())
                        && StringUtils.isNotEmpty(entry.getPickedQty())) {
                    final PickConfirmEntry pickConfirmEntry = new PickConfirmEntry();
                    pickConfirmEntry.setItemCode(entry.getProduct().getCode());
                    pickConfirmEntry.setQuantityShipped(Integer.valueOf(NumberUtils.toInt(entry.getPickedQty())));
                    entries.add(pickConfirmEntry);
                }
            }
        }
        return entries;
    }



    /**
     * Determine LabelLayout based on layout parameter
     * 
     * @param layout
     * @return LabelLayout
     */
    protected LabelLayout getLabelLayout(final String layout) {

        // If no layout supplied then use default
        if (StringUtils.isEmpty(layout)) {
            return LabelLayout.getDefaultLayout();
        }

        // Try to map supplied string to a layout, if we can't then warn and use default
        try {
            return (LabelLayout.valueOf(layout.trim().toLowerCase()));
        }
        catch (final Exception e) {
            LOG.warn("GetDispatchLabel: unknown layout (using default): layout=" + layout);
        }

        return LabelLayout.getDefaultLayout();
    }

    /**
     * Determine whether to apply branding to label
     * 
     * @param branding
     * 
     * @return true or false
     */
    protected boolean getDoBranding(final Boolean branding) {

        // If branding is not passed then set true
        return BooleanUtils.isNotFalse(branding);
    }

    /**
     * Get consignment and check it is in status PACKED
     * 
     * @param consignmentCode
     * @param storeNumber
     * @return TargetConsignmentModel or null if not valid
     */
    protected TargetConsignmentModel getValidatedConsignmentForLabel(final String consignmentCode,
            final Integer storeNumber) {

        // Validate consignment
        final TargetConsignmentModel consignment;
        try {
            consignment = targetStoreConsignmentService.getConsignmentByCode(consignmentCode);
        }
        catch (final Exception e) {
            INSTORE_LOGGER.logStoreConsignmentActionError("GetDispatchLabel", storeNumber, consignmentCode, e);
            return null;
        }

        if (consignment == null) {
            INSTORE_LOGGER.logStoreConsignmentActionError("GetDispatchLabel", storeNumber, consignmentCode,
                    "No consignment found");
            return null;
        }
        if (consignment.getStatus() == null || !consignmentStatusAllowedForLabel.contains(consignment.getStatus())) {
            INSTORE_LOGGER.logStoreConsignmentActionError("GetDispatchLabel", storeNumber, consignmentCode,
                    "Invalid consignment status=" + consignment.getStatus());
            return null;
        }

        return consignment;
    }

    /**
     * Method to get the List of Dto with Standard Parcel Details
     * 
     * @return List of Dto with Standard Parcel Details
     */
    private List<StandardParcel> getStandardParcelDetailList() {
        final List<StandardParcel> parcelDtoList = new ArrayList<>();
        final List<TargetStandardParcelModel> parcelDetails = targetStandardParcelService
                .getEnabledStandardParcelDetails();

        if (CollectionUtils.isNotEmpty(parcelDetails)) {
            for (final TargetStandardParcelModel model : parcelDetails) {
                if (null != model) {
                    parcelDtoList.add(standardParcelDetailsConverter.convert(model));
                }
            }
        }
        return parcelDtoList;
    }

    private List<Manifest> getManifestDto(final String manifestCode) throws NotFoundException {
        final TargetManifestModel manifestModel = targetManifestService.getManifestByCode(manifestCode);
        final Manifest manifest = instoreManifestConverter.convert(manifestModel);
        return Collections.singletonList(manifest);
    }

    private List<Manifest> getManifestHistoryDtos(final int storeNumber) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetPointOfServiceModel pointOfService = targetPointOfServiceService.getPOSByStoreNumber(Integer
                .valueOf(storeNumber));
        final List<TargetManifestModel> manifestHistory = targetManifestService
                .getManifestsHistoryForStore(pointOfService);

        if (CollectionUtils.isNotEmpty(manifestHistory)) {
            final List<Manifest> manifests = new ArrayList<>();

            for (final TargetManifestModel manifestModel : manifestHistory) {
                final Manifest manifest = instoreBasicManifestConverter.convert(manifestModel);
                manifests.add(manifest);
            }
            return manifests;
        }

        return ListUtils.EMPTY_LIST;
    }

    /**
     * Get list of consignment dtos for the store
     * 
     * @param consignmentCodes
     * @return list
     */
    protected List<Consignment> getConsignmentDtosByCodes(final List<String> consignmentCodes) {
        final List<Consignment> consignmentDtos = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(consignmentCodes)) {
            final List<TargetConsignmentModel> consignmentModels = targetStoreConsignmentService
                    .getConsignmentsByCodes(consignmentCodes);
            List<ConsignmentEntry> consignmentEntryDtos = null;

            if (CollectionUtils.isNotEmpty(consignmentModels)) {

                for (final TargetConsignmentModel conModel : consignmentModels) {

                    final Consignment conDto = instoreConsignmentConverter.convert(conModel);

                    if (CollectionUtils.isNotEmpty(conModel.getConsignmentEntries())) {

                        consignmentEntryDtos = new ArrayList<>();

                        for (final ConsignmentEntryModel conEntModel : conModel.getConsignmentEntries()) {
                            final Long consignmentEntryQuantity = conEntModel.getQuantity();
                            if (null != consignmentEntryQuantity && consignmentEntryQuantity.longValue() > 0) {
                                final ConsignmentEntry consignmentEntryDto = instoreConsignmentEntryConverter
                                        .convert(conEntModel);
                                consignmentEntryDtos.add(consignmentEntryDto);
                            }
                        }
                        sortConsignmentEntries(consignmentEntryDtos);
                    }
                    conDto.setEntries(consignmentEntryDtos);
                    consignmentDtos.add(conDto);
                }
            }

        }

        return consignmentDtos;
    }

    /**
     * sort the consignmentEntries based on the department name
     * 
     * @param consignmentEntryDtos
     */
    protected void sortConsignmentEntries(final List<ConsignmentEntry> consignmentEntryDtos) {
        Collections.sort(consignmentEntryDtos, new Comparator<ConsignmentEntry>() {

            @Override
            public int compare(final ConsignmentEntry o1, final ConsignmentEntry o2) {
                final Product product1 = o1.getProduct();
                final Product product2 = o2.getProduct();
                if (product1 == null && product2 == null) {
                    return 0;
                }
                if (product1 == null) {
                    return 1;
                }
                if (product2 == null) {
                    return -1;
                }
                if (StringUtils.isEmpty(product1.getDepartment())) {
                    return 1;
                }
                if (StringUtils.isEmpty(product2.getDepartment())) {
                    return -1;
                }
                return product1.getDepartment().compareTo(product2.getDepartment());
            }
        });
    }

    /**
     * Convert user details
     * 
     * @param userModel
     * @return UserDetails
     */
    protected User convertUser(final UserModel userModel) {

        Assert.notNull(userModel, "userModel cannot be null");

        final User user = new User();

        user.setUsername(userModel.getName());
        user.setFirstName(userModel.getDisplayName());

        if (userModel instanceof StoreEmployeeModel) {
            final StoreEmployeeModel storeEmployee = (StoreEmployeeModel)userModel;
            final TargetPointOfServiceModel store = storeEmployee.getStore();
            if (store != null) {
                user.setStore(convertStore(store));
            }
        }

        return user;
    }

    /**
     * Convert store
     * 
     * @param storeModel
     * @return Store
     */
    protected Store convertStore(final TargetPointOfServiceModel storeModel) {

        Assert.notNull(storeModel, "storeModel cannot be null");

        final Store store = new Store();
        store.setStoreName(storeModel.getName());
        store.setStoreNumber(storeModel.getStoreNumber());

        return store;
    }

    private TargetConsignmentPageResult getConsignmentsForStoreByPage(final Integer storeNumber, final int offset,
            final int recsPerPage, final int lastXDays) {
        return targetStoreConsignmentService.getConsignmentsForStore(storeNumber, offset, recsPerPage, lastXDays);
    }

    private Response createSystemErrorResponse(final Exception e) {
        final String message = e != null ? e.getMessage() : "System Error";
        return createErrorResponse(message, OfcErrorConstants.ERR_SYSTEM);
    }

    private Response createNotFoundErrorResponse(final Exception e) {
        final String message = e != null ? e.getMessage() : "Not Found";
        return createErrorResponse(message, OfcErrorConstants.ERR_NOT_FOUND);
    }

    private Response createConsignmentStatusValidationErrorResponse(final Exception e) {
        final String message = e != null ? e.getMessage() : "Consignment Status Validation Failed";
        return createErrorResponse(message, OfcErrorConstants.ERR_CONSIGNMENT_STATUS_VALIDATION_FAILED);
    }

    private Response createInvalidParamsErrorResponse(final Exception e) {
        final String message = e != null ? e.getMessage() : "Invalid Parameter";
        return createErrorResponse(message, OfcErrorConstants.ERR_INVALID_PARAM);
    }

    private Response createFluentErrorResponse(final Exception e) {
        final String message = e != null ? e.getMessage() : "Fluent unavailable";
        return createErrorResponse(message, OfcErrorConstants.FLUENT_SYSTEM_ERR);
    }

    private int validateParcelCount(final Integer parcelsCount) throws InvalidParameterException {
        int parcelCount = 0;
        try {
            if (parcelsCount != null) {
                parcelCount = parcelsCount.intValue();
            }
            Assert.isTrue(parcelCount > 0, "Parcel count has to be positive");
        }
        catch (final IllegalArgumentException e) {
            throw new InvalidParameterException(e.getMessage());
        }
        return parcelCount;
    }

    private void logStoreConsignmentAction(final String action, final Integer storeNumber,
            final String consignmentCode, final TargetConsignmentModel consignment) {
        INSTORE_LOGGER.logStoreConsignmentAction(action, storeNumber,
                targetStoreConsignmentService.getOrderCodeFromConsignment(consignment), consignmentCode,
                targetStoreConsignmentService.getDeliverFromStateFromConsignment(consignment),
                targetStoreConsignmentService.getDeliverToStoreNumberFromConsignment(consignment),
                targetStoreConsignmentService.getDeliverToStateFromConsignment(consignment));
    }


    /**
     * Get products from OrderEntryGroup
     *
     * @return products list of products
     */
    private List<AbstractTargetVariantProductModel> getProductsFromOrder(
            final List<AbstractOrderEntryModel> orderEntries) {
        final List<AbstractTargetVariantProductModel> products = new ArrayList<>();

        for (final AbstractOrderEntryModel oem : orderEntries) {
            products.add((AbstractTargetVariantProductModel)oem.getProduct());
        }
        return products;
    }


    /**
     * @param targetFulfilmentService
     *            the targetFulfilmentService to set
     */
    @Required
    public void setTargetFulfilmentService(final TargetFulfilmentService targetFulfilmentService) {
        this.targetFulfilmentService = targetFulfilmentService;
    }

    /**
     * @param instoreConsignmentConverter
     *            the instoreConsignmentConverter to set
     */
    @Required
    public void setInstoreConsignmentConverter(final InstoreConsignmentConverter instoreConsignmentConverter) {
        this.instoreConsignmentConverter = instoreConsignmentConverter;
    }

    /**
     * @param targetStoreConsignmentService
     *            the targetStoreConsignmentService to set
     */
    @Required
    public void setTargetStoreConsignmentService(final TargetStoreConsignmentService targetStoreConsignmentService) {
        this.targetStoreConsignmentService = targetStoreConsignmentService;
    }

    /**
     * @param consignmentStatusMapper
     *            the consignmentStatusMapper to set
     */
    @Required
    public void setConsignmentStatusMapper(final ConsignmentStatusMapper consignmentStatusMapper) {
        this.consignmentStatusMapper = consignmentStatusMapper;
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @param instoreConsignmentEntryConverter
     *            the instoreConsignmentEntryConverter to set
     */
    @Required
    public void setInstoreConsignmentEntryConverter(
            final InstoreConsignmentEntryConverter instoreConsignmentEntryConverter) {
        this.instoreConsignmentEntryConverter = instoreConsignmentEntryConverter;
    }

    /**
     * @param targetManifestService
     */
    @Required
    public void setTargetManifestService(final TargetManifestService targetManifestService) {
        this.targetManifestService = targetManifestService;
    }

    /**
     * @param targetPointOfServiceService
     */
    @Required
    public void setTargetPointOfServiceService(final TargetPointOfServiceService targetPointOfServiceService) {
        this.targetPointOfServiceService = targetPointOfServiceService;
    }

    /**
     * @param instoreManifestConverter
     */
    @Required
    public void setInstoreManifestConverter(final InstoreManifestConverter instoreManifestConverter) {
        this.instoreManifestConverter = instoreManifestConverter;
    }

    /**
     * @param instoreBasicManifestConverter
     *            the instoreBasicManifestConverter to set
     */
    @Required
    public void setInstoreBasicManifestConverter(final InstoreBasicManifestConverter instoreBasicManifestConverter) {
        this.instoreBasicManifestConverter = instoreBasicManifestConverter;
    }

    /**
     * @param consignmentParcelDTOConverter
     *            the consignmentParcelDTOConverter to set
     */
    @Required
    public void setConsignmentParcelDTOConverter(final ConsignmentParcelDTOConverter consignmentParcelDTOConverter) {
        this.consignmentParcelDTOConverter = consignmentParcelDTOConverter;
    }

    /**
     * @param targetStandardParcelService
     *            the targetStandardParcelService to set
     */
    @Required
    public void setTargetStandardParcelService(final TargetStandardParcelService targetStandardParcelService) {
        this.targetStandardParcelService = targetStandardParcelService;
    }

    /**
     * @param standardParcelDetailsConverter
     *            the standardParcelDetailsConverter to set
     */
    @Required
    public void setStandardParcelDetailsConverter(final StandardParcelDetailsConverter standardParcelDetailsConverter) {
        this.standardParcelDetailsConverter = standardParcelDetailsConverter;
    }

    /**
     * @param transmitManifestService
     *            the transmitManifestService to set
     */
    @Required
    public void setTransmitManifestService(final TransmitManifestService transmitManifestService) {
        this.transmitManifestService = transmitManifestService;
    }

    /**
     * @param dispatchLabelService
     *            the dispatchLabelService to set
     */
    @Required
    public void setDispatchLabelService(final DispatchLabelService dispatchLabelService) {
        this.dispatchLabelService = dispatchLabelService;
    }

    /**
     * @param hardCopyManifestConverter
     *            the hardCopyManifestConverter to set
     */
    @Required
    public void setHardCopyManifestConverter(final HardCopyManifestConverter hardCopyManifestConverter) {
        this.hardCopyManifestConverter = hardCopyManifestConverter;
    }

    /**
     * @param consignmentStatusAllowedForLabel
     *            the consignmentStatusAllowedForLabel to set
     */
    @Required
    public void setConsignmentStatusAllowedForLabel(final List<ConsignmentStatus> consignmentStatusAllowedForLabel) {
        this.consignmentStatusAllowedForLabel = consignmentStatusAllowedForLabel;
    }

    /**
     * @param targetStoreStockService
     *            the targetStoreStockService to set
     */
    @Required
    public void setTargetStoreStockService(final TargetStoreStockService targetStoreStockService) {
        this.targetStoreStockService = targetStoreStockService;
    }

    /**
     * @param productStockLevelConverter
     *            the productStockLevelConverter to set
     */
    @Required
    public void setProductStockLevelConverter(final ProductStockLevelConverter productStockLevelConverter) {
        this.productStockLevelConverter = productStockLevelConverter;
    }

    /**
     * @param featureSwitchService
     *            the featureSwitchService to set
     */
    @Required
    public void setFeatureSwitchService(final TargetFeatureSwitchService featureSwitchService) {
        this.featureSwitchService = featureSwitchService;
    }

    /**
     * @param stockVisResponseToProductStockLevelInstoreConverter
     *            the stockVisResponseToProductStockLevelInstoreConverter to set
     */
    @Required
    public void setStockVisResponseToProductStockLevelInstoreConverter(
            final StockVisResponseToProductStockLevelInstoreConverter stockVisResponseToProductStockLevelInstoreConverter) {
        this.stockVisResponseToProductStockLevelInstoreConverter = stockVisResponseToProductStockLevelInstoreConverter;
    }

    /**
     * @param pickConsignmentUpdater
     *            the pickConsignmentUpdater to set
     */
    @Required
    public void setPickConsignmentUpdater(final PickConsignmentUpdater pickConsignmentUpdater) {
        this.pickConsignmentUpdater = pickConsignmentUpdater;
    }

    /**
     * @param fluentStockLookupService
     *            the fluentStockLookupService to set
     */
    public void setFluentStockLookupService(final FluentStockLookupService fluentStockLookupService) {
        this.fluentStockLookupService = fluentStockLookupService;
    }

}
