/**
 * 
 */
package au.com.target.tgtwsfacades.instore.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtwsfacades.constants.TgtwsfacadesConstants;
import au.com.target.tgtwsfacades.instore.ConsignmentStatusMapper;


/**
 * @author sbryan6
 *
 */
public class ConsignmentStatusMapperImpl implements ConsignmentStatusMapper {

    private Map<String, List<ConsignmentStatus>> consignmentStatusMap;

    @Override
    public String getOFCStatus(final ConsignmentStatus status) {

        for (final String ofcState : consignmentStatusMap.keySet()) {

            final List<ConsignmentStatus> statuses = consignmentStatusMap.get(ofcState);
            if (CollectionUtils.isNotEmpty(statuses) && statuses.contains(status)) {

                return ofcState;
            }
        }

        return null;
    }

    @Override
    public boolean isOpen(final ConsignmentStatus status) {
        return consignmentStatusMap.get(TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_OPEN).contains(status);
    }

    @Override
    public boolean isInProgress(final ConsignmentStatus status) {
        return consignmentStatusMap.get(TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_INPROGRESS).contains(status);
    }

    @Override
    public boolean isCompleted(final ConsignmentStatus status) {
        return consignmentStatusMap.get(TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_COMPLETED).contains(
                status);
    }

    @Override
    public boolean isRejected(final ConsignmentStatus status) {
        return consignmentStatusMap.get(TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_REJECTED).contains(status);
    }

    @Override
    public boolean isPicked(final ConsignmentStatus status) {
        return consignmentStatusMap.get(TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_PICKED).contains(status);
    }

    @Override
    public boolean isPacked(final ConsignmentStatus status) {
        return consignmentStatusMap.get(TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_PACKED).contains(status);
    }

    /**
     * @param consignmentStatsStatusMap
     *            the consignmentStatsStatusMap to set
     */
    @Required
    public void setConsignmentStatusMap(final Map<String, List<ConsignmentStatus>> consignmentStatsStatusMap) {
        this.consignmentStatusMap = consignmentStatsStatusMap;
    }

}
