/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.consignments;

import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author smudumba
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Product {

    private String code;
    private String name;
    private String department;
    private Integer price;
    private String size;
    private String colour;
    private String thumbImageUrl;
    private String largeImageUrl;
    private String departmentNumber;
    private String barcode;
    private Boolean clearance;
    private Boolean mhd;
    private Boolean bulky;
    private Boolean onlineExclusive;

    /**
     * Product object
     */
    public Product() {

    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department
     *            the department to set
     */
    public void setDepartment(final String department) {
        this.department = department;
    }


    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size
     *            the size to set
     */
    public void setSize(final String size) {
        this.size = size;
    }

    /**
     * @return the colour
     */
    public String getColour() {
        return colour;
    }

    /**
     * @param colour
     *            the colour to set
     */
    public void setColour(final String colour) {
        this.colour = colour;
    }

    /**
     * @return the thumbImageUrl
     */
    public String getThumbImageUrl() {
        return thumbImageUrl;
    }

    /**
     * @param thumbImageUrl
     *            the thumbImageUrl to set
     */
    public void setThumbImageUrl(final String thumbImageUrl) {
        this.thumbImageUrl = thumbImageUrl;
    }

    /**
     * @return the largeImageUrl
     */
    public String getLargeImageUrl() {
        return largeImageUrl;
    }

    /**
     * @param largeImageUrl
     *            the largeImageUrl to set
     */
    public void setLargeImageUrl(final String largeImageUrl) {
        this.largeImageUrl = largeImageUrl;
    }

    /**
     * @return the departmentNo
     */
    public String getDepartmentNumber() {
        return departmentNumber;
    }

    /**
     * @param departmentNo
     *            the departmentNo to set
     */
    public void setDepartmentNumber(final String departmentNo) {
        this.departmentNumber = departmentNo;
    }

    /**
     * @return the price
     */
    public Integer getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(final Integer price) {
        this.price = price;
    }

    /**
     * @return the barcode
     */
    public String getBarcode() {
        return barcode;
    }

    /**
     * @param barcode
     *            the barcode to set
     */
    public void setBarcode(final String barcode) {
        this.barcode = barcode;
    }

    /**
     * @return the clearance
     */
    public Boolean getClearance() {
        return clearance;
    }

    /**
     * @param clearance
     *            the clearance to set
     */
    public void setClearance(final Boolean clearance) {
        this.clearance = clearance;
    }

    /**
     * @return the mhd
     */
    public Boolean getMhd() {
        return mhd;
    }

    /**
     * @param mhd
     *            the mhd to set
     */
    public void setMhd(final Boolean mhd) {
        this.mhd = mhd;
    }

    /**
     * @return the bulky
     */
    public Boolean getBulky() {
        return bulky;
    }

    /**
     * @param bulky
     *            the bulky to set
     */
    public void setBulky(final Boolean bulky) {
        this.bulky = bulky;
    }

    /**
     * @return the onlineExclusive
     */
    public Boolean getOnlineExclusive() {
        return onlineExclusive;
    }

    /**
     * @param onlineExclusive
     *            the onlineExclusive to set
     */
    public void setOnlineExclusive(final Boolean onlineExclusive) {
        this.onlineExclusive = onlineExclusive;
    }

}
