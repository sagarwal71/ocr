/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.consignments;


import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author smudumba
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Pagination {

    private Integer offset;
    private Integer perPage;
    private Integer totalAvailable;



    /**
     * Response object for Pagination
     * 
     */
    public Pagination() {

    }



    /**
     * @return the offset
     */
    public Integer getOffset() {
        return offset;
    }



    /**
     * @param offset
     *            the offset to set
     */
    public void setOffset(final Integer offset) {
        this.offset = offset;
    }



    /**
     * @return the perPage
     */
    public Integer getPerPage() {
        return perPage;
    }



    /**
     * @param perPage
     *            the perPage to set
     */
    public void setPerPage(final Integer perPage) {
        this.perPage = perPage;
    }



    /**
     * @return the totalAvailable
     */
    public Integer getTotalAvailable() {
        return totalAvailable;
    }



    /**
     * @param totalAvailable
     *            the totalAvailable to set
     */
    public void setTotalAvailable(final Integer totalAvailable) {
        this.totalAvailable = totalAvailable;
    }


}
