/**
 * 
 */
package au.com.target.tgtwsfacades.instore.converters;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.barcode.TargetBarCodeService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.util.OrderEncodingTools;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.util.TargetProductDataHelper;
import au.com.target.tgtutility.util.TargetDateUtil;
import au.com.target.tgtutility.util.TargetProductUtils;
import au.com.target.tgtwebcore.constants.TgtwebcoreConstants;
import au.com.target.tgtwsfacades.instore.ConsignmentStatusMapper;
import au.com.target.tgtwsfacades.instore.dto.consignments.Consignment;
import au.com.target.tgtwsfacades.instore.dto.consignments.Customer;
import au.com.target.tgtwsfacades.instore.dto.consignments.Destination;


/**
 * Converter to generate DTOs for portal REST API from ConsignmentModels
 * 
 * @author sbryan6
 *
 */
/**
 * @author smudumba
 *
 */
public class InstoreConsignmentConverter implements Converter<TargetConsignmentModel, Consignment> {

    protected static final Logger LOG = Logger.getLogger(InstoreConsignmentConverter.class);

    private ConsignmentStatusMapper consignmentStatusMapper;

    private TargetBarCodeService targetBarCodeService;

    @Override
    public Consignment convert(final TargetConsignmentModel source, final Consignment prototype)
            throws ConversionException {

        return convert(source);
    }


    @Override
    public Consignment convert(final TargetConsignmentModel source) throws ConversionException {

        Assert.notNull(source, "consignment cannot be null");

        final Consignment con = new Consignment();
        con.setCode(source.getCode());
        con.setDate(TargetDateUtil.getDateFormattedAsString(source.getCreationtime()));
        con.setParcels(source.getParcelCount());
        if (null != source.getPickConfirmDate()) {
            con.setPickDate(TargetDateUtil.getDateFormattedAsString(source.getPickConfirmDate()));
        }
        if (null != source.getPackedDate()) {
            con.setPackDate(TargetDateUtil.getDateFormattedAsString(source.getPackedDate()));
        }
        final String ofcStatus = consignmentStatusMapper.getOFCStatus(source.getStatus());
        con.setStatus(ofcStatus);
        this.setConsignmentShippedByShipConfReceived(source, con);

        if (null != source.getOfcOrderType()) {
            final String deliveryType = source.getOfcOrderType().getCode();
            con.setDeliveryType(deliveryType);
        }
        final double totalConsignmentValue = getTotalValueOfConsignment(source);
        final int consignmentValueInCents = (int)Math.round(totalConsignmentValue * 100);
        con.setConsignmentValueInCents(Integer.valueOf(consignmentValueInCents));
        populateOrderInfo(source, con);

        con.setTotalItems(Long.valueOf(getTotalNumberOfItems(source)));
        con.setConsignmentVersion(source.getConsignmentVersion());
        setFlagsForConsignment(source, con);

        // Don't do the entries here since not needed for consignment lists

        return con;
    }

    /**
     * set consignment status to completed if already received the shipConfReceived
     * 
     * @param targetConsignmentModel
     * @param conDto
     */
    private void setConsignmentShippedByShipConfReceived(final TargetConsignmentModel targetConsignmentModel,
            final Consignment conDto) {
        if (BooleanUtils.isTrue(targetConsignmentModel.getShipConfReceived())) {
            conDto.setStatus(consignmentStatusMapper.getOFCStatus(ConsignmentStatus.SHIPPED));
        }
    }

    /**
     * Populate details from order
     * 
     * @param source
     * @param con
     */
    protected void populateOrderInfo(final TargetConsignmentModel source, final Consignment con) {

        final AbstractOrderModel orderModel = source.getOrder();
        if (null != source.getShippingAddress()) {
            populateCustomerAndDestinationInfo(source.getShippingAddress(), con);
        }
        else {
            if (orderModel == null) {

                LOG.warn("Order is null in consignment: " + source.getCode());
                return;
            }
            populateCustomerAndDestinationInfo(orderModel.getDeliveryAddress(), con);
        }
        if (null != orderModel) {
            con.setOrderNumber(orderModel.getCode());
            populateOrderBarcodeDetails(orderModel, con);
        }
    }



    /**
     * @param orderModel
     * @param con
     */
    protected void populateOrderBarcodeDetails(final AbstractOrderModel orderModel, final Consignment con) {

        String barcodeSvgUrl = StringUtils.EMPTY;
        String enCodedOrderNumber = StringUtils.EMPTY;

        if (StringUtils.isNotEmpty(orderModel.getCode())) {
            enCodedOrderNumber = getEncodedOrderId(orderModel.getCode());
        }
        if (StringUtils.isNotEmpty(enCodedOrderNumber)) {
            try {
                final String urlEncodedOrderNumber = URLEncoder
                        .encode(enCodedOrderNumber, TgtFacadesConstants.ENCOD);
                barcodeSvgUrl = TgtFacadesConstants.ORDER_BARCODE
                        + TgtFacadesConstants.BARCODE_SVG_FORMAT
                        + TgtFacadesConstants.ORDER_NUMBER + urlEncodedOrderNumber;

            }
            catch (final UnsupportedEncodingException e) {
                LOG.error("Not Supported encoding", e);
            }
            con.setOrderBarcodeUrl(barcodeSvgUrl);
            con.setOrderBarcodeNumber(targetBarCodeService.getPlainBarcode(enCodedOrderNumber));

        }

    }


    /**
     * @param code
     * @return string Encoded string
     */
    protected String getEncodedOrderId(final String code) {
        if (StringUtils.isNotEmpty(code)) {
            return OrderEncodingTools.encodeOrderId(code);
        }

        return StringUtils.EMPTY;
    }

    protected void populateCustomerAndDestinationInfo(final AddressModel address, final Consignment con) {

        if (address != null) {
            final Customer customer = new Customer();
            final Destination destination = new Destination();

            customer.setFirstName(address.getFirstname());
            customer.setLastName(address.getLastname());
            destination.setCity(address.getTown());
            destination.setState(address.getDistrict());
            con.setDestination(destination);
            con.setCustomer(customer);
        }
    }


    /**
     * Get total number of items in the consignment
     * 
     * @param source
     * @return number
     */
    protected long getTotalNumberOfItems(final TargetConsignmentModel source) {

        long numItems = 0;

        final Set<ConsignmentEntryModel> entries = source.getConsignmentEntries();
        if (entries != null) {

            for (final ConsignmentEntryModel entry : entries) {

                if (entry != null) {

                    numItems += entry.getQuantity().longValue();
                }
            }
        }

        return numItems;
    }

    private void setFlagsForConsignment(final TargetConsignmentModel source, final Consignment consignment) {
        final Set<ConsignmentEntryModel> entries = source.getConsignmentEntries();
        if (entries == null) {
            return;
        }

        for (final ConsignmentEntryModel entry : entries) {
            setFlagsForConsignmentEntry(entry, consignment);
        }
    }

    private void setFlagsForConsignmentEntry(final ConsignmentEntryModel entry, final Consignment consignment) {
        final ProductModel product = getProductFromConsignmentEntry(entry);

        if (product == null) {
            return;
        }

        if (!consignment.isOnlineExclusive() && product instanceof AbstractTargetVariantProductModel) {
            final TargetColourVariantProductModel colourVariant = TargetProductUtils
                    .getColourVariantProduct((AbstractTargetVariantProductModel)product);

            consignment.setOnlineExclusive(
                    consignment.isOnlineExclusive() || Boolean.TRUE.equals(colourVariant.getOnlineExclusive()));
        }

        final ProductModel baseProduct = TargetProductDataHelper.getBaseProduct(product);
        if (baseProduct instanceof TargetProductModel) {
            final TargetProductModel prodModel = (TargetProductModel)baseProduct;

            consignment.setClearance(consignment.isClearance() || Boolean.TRUE.equals(prodModel.getClearanceProduct()));

            if (!consignment.isBulky() || !consignment.isMhd()) {
                final ProductTypeModel productType = prodModel.getProductType();
                if (productType != null) {
                    consignment.setBulky(consignment.isBulky() || Boolean.TRUE.equals(productType.getBulky()));
                    consignment.setMhd(
                            consignment.isMhd() || TgtwebcoreConstants.ProductTypes.MHD.equals(productType.getCode()));
                }
            }
        }
    }

    private ProductModel getProductFromConsignmentEntry(final ConsignmentEntryModel entry) {
        if (entry == null) {
            return null;
        }

        final AbstractOrderEntryModel orderEntry = entry.getOrderEntry();

        if (null == orderEntry) {
            return null;
        }

        return orderEntry.getProduct();
    }

    /**
     * Get total value of the consignment
     * 
     * @param source
     * @return double
     */
    protected double getTotalValueOfConsignment(final TargetConsignmentModel source) {
        double totalValue = 0;
        final Set<ConsignmentEntryModel> entries = source.getConsignmentEntries();
        if (CollectionUtils.isNotEmpty(entries)) {
            for (final ConsignmentEntryModel entry : entries) {
                if (null != entry && null != entry.getOrderEntry()) {
                    totalValue += entry.getOrderEntry().getTotalPrice().doubleValue();
                }
            }
        }
        return totalValue;
    }

    /**
     * @param consignmentStatusMapper
     *            the consignmentStatusMapper to set
     */
    @Required
    public void setConsignmentStatusMapper(final ConsignmentStatusMapper consignmentStatusMapper) {
        this.consignmentStatusMapper = consignmentStatusMapper;
    }


    /**
     * @param targetBarCodeService
     *            the targetBarCodeService to set
     */
    @Required
    public void setTargetBarCodeService(final TargetBarCodeService targetBarCodeService) {
        this.targetBarCodeService = targetBarCodeService;
    }

}