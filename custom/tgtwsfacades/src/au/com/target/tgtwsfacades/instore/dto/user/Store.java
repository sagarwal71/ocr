/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.user;

/**
 * @author sbryan6
 *
 */
public class Store {

    private Integer storeNumber;
    private String storeName;


    /**
     * @return the storeNumber
     */
    public Integer getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final Integer storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the storeName
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     * @param storeName
     *            the storeName to set
     */
    public void setStoreName(final String storeName) {
        this.storeName = storeName;
    }


}
