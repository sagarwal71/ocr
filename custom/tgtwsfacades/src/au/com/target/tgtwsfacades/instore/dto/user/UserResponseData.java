/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.user;

import com.fasterxml.jackson.annotation.JsonInclude;

import au.com.target.tgtfacades.response.data.BaseResponseData;


/**
 * @author rsamuel3
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserResponseData extends BaseResponseData {
    private User user;

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user
     *            the user to set
     */
    public void setUser(final User user) {
        this.user = user;
    }

}
