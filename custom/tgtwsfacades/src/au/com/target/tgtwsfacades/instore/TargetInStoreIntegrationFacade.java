/**
 * 
 */
package au.com.target.tgtwsfacades.instore;

import java.util.List;

import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectState;
import au.com.target.tgtwsfacades.instore.dto.consignments.Consignment;
import au.com.target.tgtwsfacades.instore.dto.consignments.Parcels;
import au.com.target.tgtwsfacades.instore.dto.user.UserResponseData;


/**
 * Facade interface to provide consignment details for stores.
 * 
 * @author smudumba
 * 
 */

public interface TargetInStoreIntegrationFacade {

    /**
     * Get the logged in user details
     * 
     * @return UserResponseData
     */
    UserResponseData getLoggedInUser();


    /**
     * Get all consignments for given store.
     * 
     * @param storeNumber
     * @param offset
     * @param recsPerPage
     * @param lastXDays
     * @return Response
     */
    Response getConsignmentsForStore(Integer storeNumber, final int offset,
            final int recsPerPage, int lastXDays);

    /**
     * Gets the consignments by status for today.
     *
     * @param storeNumber
     *            the store number
     * 
     * @return the consignments by status for today
     */
    Response getConsignmentsByStatusForToday(final Integer storeNumber);

    /**
     * Gets the consignments by status for yesterday.
     *
     * @param storeNumber
     * 
     * @return the consignments by status for today
     */
    Response getConsignmentsByStatusForYesterday(final Integer storeNumber);

    /**
     * Pick for pack an order for instore fulfilment
     * 
     * @param consignmentCode
     * @param storeNumber
     * @return Response
     */
    Response pickForPackConsignmentInstore(final String consignmentCode, final Integer storeNumber);

    /**
     * Complete an order for instore fulfilment
     * 
     * @param consignmentCode
     * @param storeNumber
     * @param parcelCount
     *            Parcel count
     * @param parcels
     *            parcel details
     * @return Response
     */
    Response completeConsignmentInstore(final String consignmentCode, final Integer storeNumber,
            final Integer parcelCount,
            Parcels parcels);

    /**
     * Indicate an order for instore fulfilment as could not be picked by store.
     * 
     * @param consignmentCode
     * @param storeNumber
     * @param instoreRejectReasonCode
     * @param rejectState
     * @return Response
     */
    Response rejectConsignmentForInstore(final String consignmentCode, final Integer storeNumber,
            final String instoreRejectReasonCode, ConsignmentRejectState rejectState);


    /**
     * @param consignmentCodes
     * @param storeNumber
     * @return Response
     */
    Response getConsignmentsByCodes(final List<String> consignmentCodes, final Integer storeNumber);

    /**
     * Get consignments status
     * 
     * @param consignmentCode
     * @param storeNumber
     * @return Response
     */
    Response getConsignmentStatusForInstore(final String consignmentCode, final Integer storeNumber);

    /**
     * @param message
     * @param code
     * @return Response
     */
    Response createErrorResponse(String message, String code);

    /**
     * Change consignment status to waved when store user accepts to pick the order.
     * 
     * @param consignmentCode
     * @param storeNumber
     * @return Response
     */
    Response waveConsignmentForInstore(final String consignmentCode, final Integer storeNumber);

    /**
     * Get all unmanifested consignments for given store.
     * 
     * @param storeNumber
     * @return Response
     */
    Response getConsignmentsNotManifestedForStore(int storeNumber);

    /**
     * Get transmitted manifest with list of consignments.
     * 
     * @param storeNumber
     * @return Response
     */
    Response transmitManifestForStore(int storeNumber);

    /**
     * Gets the manifest by code.
     *
     * @param manifestCode
     * @param storeNumber
     * @return the manifest by code
     */
    Response getManifestByCode(String manifestCode, final Integer storeNumber);

    /**
     * Gets the manifest history for store.
     *
     * @param storeNumber
     *            the store number
     * @return the manifest history for store
     */
    Response getManifestHistoryForStore(int storeNumber);

    /**
     * Gets the Standard Parcel Details
     *
     * @return the standard Parcel Details
     */
    Response getStandardParcelDetails();


    /**
     * Retrieve the dispatch label for the given consignment
     * 
     * @param consignmentCode
     * @param storeNumber
     * @param layout
     * @param branding
     * @return pdf data or null
     */
    byte[] getConsignmentDispatchLabel(String consignmentCode, Integer storeNumber, final String layout,
            final Boolean branding);

    /**
     * Retrieve the summary of Manifest for a printable hard copy
     * 
     * @param manifestCode
     * @param storeNumber
     * @return manifest data
     */
    Response getHardCopyManifestData(final String manifestCode, final Integer storeNumber);

    /**
     * Gets the stock in hand for the product
     * 
     * @param storeNumber
     * @param consignmentCode
     * @return Stock in hand
     */
    Response getStockLevelsForProductsInConsignment(final String consignmentCode,
            final Integer storeNumber);

    /**
     * update ShippedQty on consignment entries
     * 
     * @param consignment
     * @return Response
     */
    Response updateShippedQty(final Consignment consignment, final Integer storeNumber);

}
