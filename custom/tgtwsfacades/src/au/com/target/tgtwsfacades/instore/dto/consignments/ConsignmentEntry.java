/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.consignments;

import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author smudumba
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConsignmentEntry {

    private String quantity;
    private String pickedQty;
    private Product product;

    /**
     * Consignment Entry
     */
    public ConsignmentEntry() {

    }

    /**
     * @return the quantity
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(final String quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the product
     */
    public Product getProduct() {
        return product;
    }

    /**
     * @param product
     *            the product to set
     */
    public void setProduct(final Product product) {
        this.product = product;
    }

    /**
     * @return the pickedQty
     */
    public String getPickedQty() {
        return pickedQty;
    }

    /**
     * @param pickedQty
     *            the pickedQty to set
     */
    public void setPickedQty(final String pickedQty) {
        this.pickedQty = pickedQty;
    }


}
