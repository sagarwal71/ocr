/**
 * 
 */
package au.com.target.tgtwsfacades.cnc.impl;

import de.hybris.platform.core.model.order.OrderModel;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import au.com.target.tgtfacades.process.email.data.ClickAndCollectEmailData;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;
import au.com.target.tgtwsfacades.cnc.ClickAndCollectNotificationFacade;
import au.com.target.tgtwsfacades.cnc.enums.CncLetterTypeEnum;
import au.com.target.tgtwsfacades.constants.TgtwsfacadesConstants;
import au.com.target.tgtwsfacades.integration.dto.ClickAndCollectNotificationResponseDto;


/**
 * @author knemalik
 * 
 */
public class ClickAndCollectNotificationFacadeImpl implements ClickAndCollectNotificationFacade {

    private static final Logger LOG = Logger.getLogger(ClickAndCollectNotificationFacadeImpl.class);
    private static final String CNC_NOTIFICATION_PREFIX = "CnCNotification: ";
    private static final String ERR_NO_ACTIVE_CONSIGNMENT = CNC_NOTIFICATION_PREFIX
            + "No active cnc consignment found, action={0}, order={1}";
    private static final String INFO_CNC_NOTIFICATION_RECEIVED = CNC_NOTIFICATION_PREFIX
            + "Click and collect notification received, action={0}, order={1}, store={2}";
    private static final String INFO_CNC_NOTIFICATION_SUCCESS = CNC_NOTIFICATION_PREFIX
            + "Click and collect notification success, action={0}, order={1}, store={2}";
    private static final String ERR_NO_ORDER_FOUND = CNC_NOTIFICATION_PREFIX + "No order found for orderNumber={0}";


    private TargetBusinessProcessService targetBusinessProcessService;
    private TargetStoreLocatorFacade targetStoreLocatorFacade;
    private TargetOrderService targetOrderService;
    private TargetConsignmentService targetConsignmentService;
    private TargetSalesApplicationConfigService salesApplicationConfigService;

    @Override
    public boolean triggerCNCNotification(final String orderId, final String storeNumber, final String letterType) {
        final OrderModel orderModel = targetOrderService.findOrderModelForOrderId(orderId);
        final CncLetterTypeEnum mappedLetterTypeEnum = CncLetterTypeEnum.getByLetterType(letterType);
        Integer storeNumberInt = null;
        try {
            storeNumberInt = Integer.valueOf(storeNumber);
        }
        catch (final NumberFormatException numException) {
            LOG.error("Letter type must be a int value", numException);
            return false;
        }

        final TargetPointOfServiceData selectedStore = targetStoreLocatorFacade.getPointOfService(storeNumberInt);
        if (orderModel == null || mappedLetterTypeEnum == null || selectedStore == null) {
            LOG.error("Cannot find the orderModel, letterType or store from the input value");
            return false;
        }

        final ClickAndCollectEmailData clickAndCollectEmailData = new ClickAndCollectEmailData();

        clickAndCollectEmailData.setStoreNumber(storeNumber);
        clickAndCollectEmailData.setLetterType(mappedLetterTypeEnum.getLetterType());

        //Call the business process passing orderModel and clickAndCollectEmailData
        try {
            final boolean partnerUpdateRequired = isPartnerUpdateRequiredForReadyForPickupNotification(orderModel);
            updateAllConsignmentsReadyForPickupDate(orderModel);
            targetBusinessProcessService.startSendClickAndCollectNotificationProcess(orderModel,
                    clickAndCollectEmailData, partnerUpdateRequired);
        }
        catch (final Exception e) {
            LOG.error("There was an error in SendClickAndCollectNotificationProcess", e);
            return false;
        }

        return true;
    }

    /**
     * Method to check if partner update is required for ready for pickup notification
     * 
     * @param orderModel
     * @return true, if partner update is required
     */
    private boolean isPartnerUpdateRequiredForReadyForPickupNotification(final OrderModel orderModel) {
        if (salesApplicationConfigService.isPartnerChannel(orderModel.getSalesApplication())) {
            final List<TargetConsignmentModel> consignments = targetConsignmentService
                    .getActiveDeliverToStoreConsignmentsForOrder(orderModel);
            if (CollectionUtils.isNotEmpty(consignments)) {
                return !isReadyForPickUpDateUpdated(consignments);
            }
        }
        return false;
    }

    /**
     * 
     * @param consignments
     * @return boolean
     */
    private boolean isReadyForPickUpDateUpdated(final List<TargetConsignmentModel> consignments) {
        boolean updated = false;
        for (final TargetConsignmentModel targetConsignmentModel : consignments) {
            if (targetConsignmentModel != null && targetConsignmentModel.getReadyForPickUpDate() != null) {
                updated = true;
                break;
            }
        }
        return updated;
    }


    public void updateAllConsignmentsReadyForPickupDate(final OrderModel orderModel) {
        final List<TargetConsignmentModel> consignments = targetConsignmentService
                .getActiveDeliverToStoreConsignmentsForOrder(orderModel);
        for (final TargetConsignmentModel targetConsignmentModel : consignments) {
            targetConsignmentService.updateReadyForPickupDate(targetConsignmentModel);
        }
    }

    /**
     * Method to update all consignments picked up date
     * 
     * @param orderModel
     */
    public void updateAllConsignmentsPickedupDate(final OrderModel orderModel) {
        final List<TargetConsignmentModel> consignments = targetConsignmentService
                .getActiveDeliverToStoreConsignmentsForOrder(orderModel);
        for (final TargetConsignmentModel targetConsignmentModel : consignments) {
            targetConsignmentService.updatePickedupDate(targetConsignmentModel);
        }
    }

    /**
     * Method to update all consignments returned to floor Date
     * 
     * @param orderModel
     */
    public void updateAllConsignmentsReturnedToFloorDate(final OrderModel orderModel) {
        final List<TargetConsignmentModel> consignments = targetConsignmentService
                .getActiveDeliverToStoreConsignmentsForOrder(orderModel);
        for (final TargetConsignmentModel targetConsignmentModel : consignments) {
            targetConsignmentService.updateReturnedToFloorDate(targetConsignmentModel);
        }
    }


    @Override
    public ClickAndCollectNotificationResponseDto notifyCncPickedUp(final String orderNumber,
            final String storeNumber) {
        LOG.info(MessageFormat.format(INFO_CNC_NOTIFICATION_RECEIVED,
                TgtwsfacadesConstants.CNC_NOTIFICATION_ACTION_PICKEDUP, orderNumber, storeNumber));

        final ClickAndCollectNotificationResponseDto responseDto = new ClickAndCollectNotificationResponseDto();
        final OrderModel orderModel = targetOrderService.findOrderModelForOrderId(orderNumber);
        if (!validateOrderModel(orderModel, orderNumber)) {
            responseDto.setSuccess(false);
            return responseDto;
        }

        final TargetConsignmentModel consignment = targetConsignmentService
                .getActiveDeliverToStoreConsignmentForOrder(orderModel);
        if (consignment != null) {
            final boolean partnerUpdateRequired = isPartnerUpdateRequiredForPickedupNotification(consignment,
                    orderModel);
            updateAllConsignmentsPickedupDate(orderModel);
            targetBusinessProcessService.startSendClickAndCollectPickedupNotificationProcess(orderModel,
                    partnerUpdateRequired);

            LOG.info(MessageFormat.format(INFO_CNC_NOTIFICATION_SUCCESS,
                    TgtwsfacadesConstants.CNC_NOTIFICATION_ACTION_PICKEDUP, orderNumber, storeNumber));
            responseDto.setSuccess(true);
            return responseDto;
        }
        LOG.error(MessageFormat.format(ERR_NO_ACTIVE_CONSIGNMENT,
                TgtwsfacadesConstants.CNC_NOTIFICATION_ACTION_PICKEDUP, orderNumber));
        responseDto.setSuccess(false);
        return responseDto;
    }

    /**
     * Determine if partner order should be updated if picked up time is updated and auto picked up date is not set and
     * order is a partner order
     * 
     * @param consignment
     * @param orderModel
     * @return true, if picked up time is updated, auto picked up is not set and order is partner order
     */
    private boolean isPartnerUpdateRequiredForPickedupNotification(final TargetConsignmentModel consignment,
            final OrderModel orderModel) {
        if (targetConsignmentService.updatePickedupDate(consignment) && consignment.getPickedUpAutoDate() == null
                && salesApplicationConfigService.isPartnerChannel(orderModel.getSalesApplication())) {
            return true;
        }
        return false;
    }

    @Override
    public ClickAndCollectNotificationResponseDto notifyCncReturnedToFloor(final String orderNumber,
            final String storeNumber) {
        LOG.info(MessageFormat.format(INFO_CNC_NOTIFICATION_RECEIVED,
                TgtwsfacadesConstants.CNC_NOTIFICATION_ACTION_RETURNED_TO_FLOOR, orderNumber, storeNumber));

        final ClickAndCollectNotificationResponseDto responseDto = new ClickAndCollectNotificationResponseDto();
        final OrderModel orderModel = targetOrderService.findOrderModelForOrderId(orderNumber);
        if (!validateOrderModel(orderModel, orderNumber)) {
            responseDto.setSuccess(false);
            return responseDto;
        }

        final TargetConsignmentModel consignment = targetConsignmentService
                .getActiveDeliverToStoreConsignmentForOrder(orderModel);

        if (consignment != null) {
            targetConsignmentService.updateReturnedToFloorDate(consignment);
            updateAllConsignmentsReturnedToFloorDate(orderModel);
            targetBusinessProcessService.startSendClickAndCollectReturnedToFloorNotificationProcess(orderModel);
            LOG.info(MessageFormat.format(INFO_CNC_NOTIFICATION_SUCCESS,
                    TgtwsfacadesConstants.CNC_NOTIFICATION_ACTION_RETURNED_TO_FLOOR, orderNumber, storeNumber));
            responseDto.setSuccess(true);
            return responseDto;
        }
        LOG.error(MessageFormat.format(ERR_NO_ACTIVE_CONSIGNMENT,
                TgtwsfacadesConstants.CNC_NOTIFICATION_ACTION_RETURNED_TO_FLOOR, orderNumber));
        responseDto.setSuccess(false);
        return responseDto;
    }

    /**
     * Method to check if order model exists
     * 
     * @param orderModel
     * @param orderNumber
     * @return true, if order model is null
     */
    private boolean validateOrderModel(final OrderModel orderModel, final String orderNumber) {
        if (orderModel == null) {
            LOG.error(MessageFormat.format(ERR_NO_ORDER_FOUND, orderNumber));
            return false;
        }
        return true;
    }

    /**
     * @param targetOrderService
     *            the targetOrderService to set
     */
    @Required
    public void setTargetOrderService(final TargetOrderService targetOrderService) {
        this.targetOrderService = targetOrderService;
    }

    /**
     * @param targetBusinessProcessService
     *            the targetBusinessProcessService to set
     */
    @Required
    public void setTargetBusinessProcessService(final TargetBusinessProcessService targetBusinessProcessService) {
        this.targetBusinessProcessService = targetBusinessProcessService;
    }

    /**
     * @param targetStoreLocatorFacade
     *            the targetStoreLocatorFacade to set
     */
    @Required
    public void setTargetStoreLocatorFacade(final TargetStoreLocatorFacade targetStoreLocatorFacade) {
        this.targetStoreLocatorFacade = targetStoreLocatorFacade;
    }

    /**
     * @param targetConsignmentService
     *            the targetConsignmentService to set
     */
    @Required
    public void setTargetConsignmentService(final TargetConsignmentService targetConsignmentService) {
        this.targetConsignmentService = targetConsignmentService;
    }

    /**
     * @param salesApplicationConfigService
     *            the salesApplicationConfigService to set
     */
    @Required
    public void setSalesApplicationConfigService(
            final TargetSalesApplicationConfigService salesApplicationConfigService) {
        this.salesApplicationConfigService = salesApplicationConfigService;
    }

}
