/**
 * 
 */
package au.com.target.tgtwsfacades.pinpad.impl;

import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;
import au.com.target.tgtpayment.util.PaymentTransactionHelper;
import au.com.target.tgtshareddto.eftwrapper.dto.EftPaymentDto;
import au.com.target.tgtshareddto.eftwrapper.dto.HybrisResponseDto;
import au.com.target.tgtshareddto.eftwrapper.dto.PaymentDetailsDto;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;
import au.com.target.tgtwsfacades.pinpad.PinPadPaymentFacade;
import au.com.target.tgtwsfacades.pinpad.exception.PinPadPaymentUpdateException;


/**
 * @author rsamuel3
 * 
 */
public class PinPadPaymentFacadeImpl implements PinPadPaymentFacade {

    /**
     * @author rsamuel3
     * 
     */


    private static final Logger LOG = Logger.getLogger(PinPadPaymentFacadeImpl.class);

    private static final String UPDATE_ERROR_SUMMARY = "Error updating PinPad payment";
    private static final String ORDER_NOT_FOUND = "Order or Cart could not be found";
    private static final String ORDER_AMBIGUOUS = "Multiple Orders found";
    private static final String CART_AMBIGUOUS = "Multiple Carts found";

    private static final String TRANSACTION_NONE_FOUND = "No transactions found";
    private static final String TRANSACTION_NONE_IN_REVIEW = "No transactions found in review state";
    private static final String TRANSACTION_WRONG_TYPE = "The payment info is not of the PinPadPaymentInfo type";

    private TargetOrderService targetOrderService;

    private ModelService modelService;


    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.pinpad.PinPadPaymentFacade#updatePinPadPayment(au.com.target.tgtshareddto.eftwrapper.dto.EftPaymentDto)
     */
    @Override
    public HybrisResponseDto updatePinPadPayment(final EftPaymentDto paymentDto) {

        final String orderId = paymentDto.getOrderId();
        final PaymentDetailsDto paymentDetails = paymentDto.getPaymentDetails();
        final HybrisResponseDto response = new HybrisResponseDto(orderId);
        response.setSuccessStatus(true);

        // Centralised error handling
        try {
            final List<PaymentTransactionModel> paymentTransactions = findPaymentTransactions(orderId);
            if (paymentTransactions == null) {
                throw new PinPadPaymentUpdateException(TRANSACTION_NONE_FOUND);
            }

            updatePaymentTransactions(paymentTransactions, paymentDetails);
        }
        catch (final Exception e) {
            populateErrorResponse(e, response, orderId);
        }

        return response;
    }


    private void populateErrorResponse(final Exception ex, final HybrisResponseDto response, final String orderId) {

        response.setSuccessStatus(false);

        // If the exception is instance of PinPadPaymentUpdateException, we know we can't recover.
        // We may be able to recover from other exceptions.
        if (ex instanceof PinPadPaymentUpdateException) {
            response.setRecoverableError(false);
        }
        else {
            response.setRecoverableError(true);
        }

        final String message = SplunkLogFormatter.formatOrderMessage(UPDATE_ERROR_SUMMARY + ": " + ex.getMessage(),
                TgtutilityConstants.ErrorCode.ERR_PINPADPAYMENT, orderId);
        LOG.error(message);
        response.setMessages(Collections.singletonList(message));
    }


    /**
     * Find payment transactions in an order or a cart.
     * 
     * @param orderId
     * @return payment transactions
     * @throws PinPadPaymentUpdateException
     *             if no order or cart found
     */
    private List<PaymentTransactionModel> findPaymentTransactions(final String orderId)
            throws PinPadPaymentUpdateException {

        // In normal case the payment should be in the cart, however we do order first then cart, 
        // less efficient but if there is an order we should use that by preference.

        final OrderModel order = findOrderModel(orderId);
        if (order != null) {
            return order.getPaymentTransactions();
        }

        final CartModel cart = findCartModel(orderId);
        if (cart != null) {
            return cart.getPaymentTransactions();
        }

        throw new PinPadPaymentUpdateException(ORDER_NOT_FOUND);
    }

    private OrderModel findOrderModel(final String orderId) throws PinPadPaymentUpdateException {

        OrderModel order = null;

        try {
            order = targetOrderService.findOrderModelForOrderId(orderId);
        }
        //CHECKSTYLE:OFF
        catch (final UnknownIdentifierException uie) {
            // No order found - indicate this by returning null
        }
        //CHECKSTYLE:ON
        catch (final AmbiguousIdentifierException aie) {
            // We can't deal with this
            throw new PinPadPaymentUpdateException(ORDER_AMBIGUOUS, aie);
        }

        return order;
    }

    private CartModel findCartModel(final String orderId) throws PinPadPaymentUpdateException {

        CartModel cart = null;

        try {
            cart = targetOrderService.findCartModelForOrderId(orderId);
        }
        //CHECKSTYLE:OFF
        catch (final UnknownIdentifierException uie) {
            // No cart found - indicate this by returning null
        }
        //CHECKSTYLE:ON
        catch (final AmbiguousIdentifierException aie) {
            // We can't deal with this
            throw new PinPadPaymentUpdateException(CART_AMBIGUOUS, aie);
        }

        return cart;
    }


    /**
     * @param paymentTransactions
     * @param paymentDetails
     * @throws PinPadPaymentUpdateException
     *             thrown when a transaction entry in review state could not be found for the order
     */
    private void updatePaymentTransactions(final List<PaymentTransactionModel> paymentTransactions,
            final PaymentDetailsDto paymentDetails) throws PinPadPaymentUpdateException {

        final PaymentTransactionEntryModel entryInReview = PaymentTransactionHelper.getLastPayment(paymentTransactions,
                Collections.singletonList(PaymentTransactionType.CAPTURE), TransactionStatus.REVIEW);

        if (entryInReview != null) {
            final PaymentInfoModel paymentInfo = entryInReview.getPaymentTransaction().getInfo();
            final PaymentInfoModel infoToUpdate = populatePaymentInfo(paymentInfo, paymentDetails);
            if (paymentDetails.isTransactionSuccess()) {
                entryInReview.setTransactionStatus(TransactionStatus.ACCEPTED.toString());
                entryInReview.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL.name());
            }
            else {
                entryInReview.setTransactionStatus(TransactionStatus.REJECTED.toString());
                entryInReview.setTransactionStatusDetails(TransactionStatusDetails.UNKNOWN_CODE.name());
            }
            modelService.saveAll(infoToUpdate, entryInReview);
        }
        else {
            final PaymentTransactionEntryModel entryAccepted = PaymentTransactionHelper.getLastPayment(
                    paymentTransactions, Collections.singletonList(PaymentTransactionType.CAPTURE),
                    TransactionStatus.ACCEPTED);
            if (entryAccepted == null) {
                throw new PinPadPaymentUpdateException(TRANSACTION_NONE_IN_REVIEW);
            }
        }
    }

    /**
     * @param paymentInfoModel
     * @param paymentDetails
     * @throws PinPadPaymentUpdateException
     *             thrown when the paymentInfoModel is not of the pinpadPaymentInfoModel type
     */
    private PaymentInfoModel populatePaymentInfo(final PaymentInfoModel paymentInfoModel,
            final PaymentDetailsDto paymentDetails) throws PinPadPaymentUpdateException {
        if (paymentInfoModel instanceof PinPadPaymentInfoModel) {
            final PinPadPaymentInfoModel pinPadPaymentInfo = (PinPadPaymentInfoModel)paymentInfoModel;
            pinPadPaymentInfo.setAccountType(paymentDetails.getAccountType());
            final String authCode = paymentDetails.getAuthCode();
            if (StringUtils.isNotBlank(authCode)) {
                pinPadPaymentInfo.setAuthCode(authCode);
            }
            pinPadPaymentInfo.setCardType(getCreditCardType(paymentDetails.getCardType()));
            pinPadPaymentInfo.setIsPaymentSucceeded(Boolean.valueOf(paymentDetails.isTransactionSuccess()));
            pinPadPaymentInfo.setJournal(paymentDetails.getJournal());
            pinPadPaymentInfo.setJournRoll(paymentDetails.getJournalRoll());
            pinPadPaymentInfo.setMaskedCardNumber(paymentDetails.getPan());
            pinPadPaymentInfo.setRespAscii(paymentDetails.getRespAscii());
            pinPadPaymentInfo.setRrn(paymentDetails.getRrn());
            return pinPadPaymentInfo;
        }
        else {
            throw new PinPadPaymentUpdateException(TRANSACTION_WRONG_TYPE);
        }
    }

    private CreditCardType getCreditCardType(final String creditCardType) {
        if (StringUtils.isNumeric(creditCardType)) {
            final int cardType = Integer.parseInt(creditCardType);
            switch (cardType) {
                case 0x01:
                    return CreditCardType.AMEX;
                case 0x02:
                    return CreditCardType.DINERS;
                case 0x03:
                    return CreditCardType.BANKCARD;
                case 0x04:
                    return CreditCardType.MASTER;
                case 0x05:
                    return CreditCardType.VISA;
                case 0x06:
                    return CreditCardType.CASHCARD;
                case 0x07:
                    return CreditCardType.COLESMASTERCARD;
                case 0x08:
                    return CreditCardType.DEBITCARD;
                case 0x09:
                    return CreditCardType.COLESGROUPCARD;
                case 0x10:
                    return CreditCardType.INSURANCECARD;
                case 0x11:
                    return CreditCardType.TARGETGIFTCARD;
                case 0x12:
                    return CreditCardType.GIFTCARD;
                case 0x13:
                    return CreditCardType.MYERCARD;
                case 0x15:
                    return CreditCardType.FLYBUYS;
                case 0x18:
                    return CreditCardType.JCBCARD;
                case 0x28:
                    return CreditCardType.OCACARD;
                case 0x35:
                    return CreditCardType.FLYBUYSGIFTCARD;
                case 0x38:
                    return CreditCardType.BASICSCARD;
                case 0x39:
                    return CreditCardType.OTHERGIFTCARD;
                case 0x43:
                    return CreditCardType.FLYBUYSDOLLAR;
                default:
                    return CreditCardType.OTHER;

            }
        }
        return null;

    }

    /**
     * @param targetOrderService
     *            the targetOrderService to set
     */
    @Required
    public void setTargetOrderService(final TargetOrderService targetOrderService) {
        this.targetOrderService = targetOrderService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

}
