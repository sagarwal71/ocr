/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.util.List;


/**
 * @author pthoma20
 *
 */
public class ConsignmentEntries {

    private List<ConsignmentEntry> consignmentEntries;

    /**
     * @return the consignmentEntries
     */
    public List<ConsignmentEntry> getConsignmentEntries() {
        return consignmentEntries;
    }

    /**
     * @param consignmentEntries
     *            the consignmentEntries to set
     */
    public void setConsignmentEntries(final List<ConsignmentEntry> consignmentEntries) {
        this.consignmentEntries = consignmentEntries;
    }
}

