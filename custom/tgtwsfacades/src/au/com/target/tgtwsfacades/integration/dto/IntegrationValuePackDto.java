/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.io.Serializable;


/**
 * @author mmaki
 * 
 */
public class IntegrationValuePackDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String leadSKU;

    private IntegrationValuePackItemList valuePackItems;

    /**
     * @return the leadSKU
     */
    public String getLeadSKU() {
        return leadSKU;
    }

    /**
     * @param leadSKU
     *            the leadSKU to set
     */
    public void setLeadSKU(final String leadSKU) {
        this.leadSKU = leadSKU;
    }

    /**
     * @return the valuePackItems
     */
    public IntegrationValuePackItemList getValuePackItems() {
        return valuePackItems;
    }

    /**
     * @param valuePackItems
     *            the valuePackItems to set
     */
    public void setValuePackItems(final IntegrationValuePackItemList valuePackItems) {
        this.valuePackItems = valuePackItems;
    }

}
