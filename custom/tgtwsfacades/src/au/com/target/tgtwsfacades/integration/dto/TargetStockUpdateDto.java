/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author bhuang3
 *
 */
@XmlRootElement(name = "stockUpdate")
@XmlAccessorType(XmlAccessType.FIELD)
public class TargetStockUpdateDto {

    @XmlElement
    private String transactionDate;

    @XmlElement
    private String transactionNumber;

    @XmlElement
    private String warehouse;

    @XmlElement
    private String comment;

    @XmlElementWrapper(name = "items")
    @XmlElement(name = "item")
    private List<TargetStockUpdateProductDto> items;

    /**
     * @return the transactionDate
     */
    public String getTransactionDate() {
        return transactionDate;
    }

    /**
     * @param transactionDate
     *            the transactionDate to set
     */
    public void setTransactionDate(final String transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     * @return the transactionNumber
     */
    public String getTransactionNumber() {
        return transactionNumber;
    }

    /**
     * @param transactionNumber
     *            the transactionNumber to set
     */
    public void setTransactionNumber(final String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    /**
     * @return the warehouse
     */
    public String getWarehouse() {
        return warehouse;
    }

    /**
     * @param warehouse
     *            the warehouse to set
     */
    public void setWarehouse(final String warehouse) {
        this.warehouse = warehouse;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment
     *            the comment to set
     */
    public void setComment(final String comment) {
        this.comment = comment;
    }

    /**
     * @return the items
     */
    public List<TargetStockUpdateProductDto> getItems() {
        return items;
    }

    /**
     * @param items
     *            the items to set
     */
    public void setItems(final List<TargetStockUpdateProductDto> items) {
        this.items = items;
    }


}
