/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;



/**
 * @author rsamuel3
 * 
 *         <media code="67890"> <mediaContainer qualifier="100124_base_1"/> <mime>image/jpeg</mime>
 *         <realfilename>cylinder1.jpg</realfilename> <removable>true</removable>
 *         <mediaPath>file://C:/Renny/Docs/images/cylinder1.jpg</mediaPath> <folder qualifier="grid-test-test"/>
 *         <mediaFormat qualifier="grid"/> </media>
 * 
 */
@XmlRootElement(name = "integration-asset")
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationAssetImportDto {
    @XmlElement
    private String assetId;
    @XmlElement
    private String mime;
    @XmlElement
    private IntegrationMediasDto medias;
    @XmlElement
    private String name;

    /**
     * @return the code
     */
    public String getAssetId() {
        return assetId;
    }

    /**
     * @param assetId
     *            the code to set
     */
    public void setAssetId(final String assetId) {
        this.assetId = assetId;
    }

    /**
     * @return the mime
     */
    public String getMime() {
        return mime;
    }

    /**
     * @param mime
     *            the mime to set
     */
    public void setMime(final String mime) {
        this.mime = mime;
    }

    /**
     * @return the media
     */
    public IntegrationMediasDto getMedias() {
        return medias;
    }

    /**
     * @param media
     *            the media to set
     */
    public void setMedias(final IntegrationMediasDto media) {
        this.medias = media;
    }


    /**
     * 
     * @return String name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     */
    public void setName(final String name) {
        this.name = name;
    }

}
