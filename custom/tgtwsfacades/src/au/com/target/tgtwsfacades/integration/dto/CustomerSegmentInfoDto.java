/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;


/**
 * @author pthoma20
 *
 */
@JsonRootName(value = "customerSegments")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerSegmentInfoDto {

    @JsonProperty
    private String subscriptionId;

    @JsonProperty
    private String segment;

    /**
     * @return the subscriptionId
     */
    public String getSubscriptionId() {
        return subscriptionId;
    }

    /**
     * @param subscriptionId
     *            the subscriptionId to set
     */
    public void setSubscriptionId(final String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    /**
     * @return the segment
     */
    public String getSegment() {
        return segment;
    }

    /**
     * @param segment
     *            the segment to set
     */
    public void setSegment(final String segment) {
        this.segment = segment;
    }


}
