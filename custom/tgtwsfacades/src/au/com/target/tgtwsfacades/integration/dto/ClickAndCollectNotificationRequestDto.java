/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;


/**
 * @author pratik
 *
 */
@JsonRootName(value = "cnc-notification-request")
public class ClickAndCollectNotificationRequestDto {

    @JsonProperty
    private String orderNumber;

    @JsonProperty
    private String storeNumber;

    /**
     * @return the orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * @param orderNumber
     *            the orderNumber to set
     */
    public void setOrderNumber(final String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * @return the storeNumber
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

}
