/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author fkratoch
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationAddressDto {
    @XmlElement
    private String streetname;

    @XmlElement
    private String building;

    @XmlElement
    private String town;

    @XmlElement
    private String district;

    @XmlElement
    private String postCode;

    @XmlElement
    private String country;

    @XmlElement
    private String phone;

    public String getStreetname() {
        return streetname;
    }

    public void setStreetname(final String streetname) {
        this.streetname = streetname;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(final String building) {
        this.building = building;
    }

    public String getTown() {
        return town;
    }

    public void setTown(final String town) {
        this.town = town;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(final String district) {
        this.district = district;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(final String postCode) {
        this.postCode = postCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(final String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }
}
