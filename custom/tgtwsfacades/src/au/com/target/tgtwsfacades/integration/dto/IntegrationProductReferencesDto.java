/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * @author fkratoch
 * 
 */

public class IntegrationProductReferencesDto implements Serializable {

    private List<IntegrationProductReferenceDto> stepProducts;

    /**
     * 
     */
    public IntegrationProductReferencesDto() {
        super();
        setStepProducts(new ArrayList<IntegrationProductReferenceDto>());
    }

    /**
     * @return the products
     */
    public List<IntegrationProductReferenceDto> getStepProducts() {
        return stepProducts;
    }

    /**
     * @param stepProducts
     *            the stepProducts to set
     */
    public void setStepProducts(final List<IntegrationProductReferenceDto> stepProducts) {
        this.stepProducts = stepProducts;
    }

}
