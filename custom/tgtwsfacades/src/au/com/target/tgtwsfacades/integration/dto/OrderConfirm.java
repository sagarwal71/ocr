package au.com.target.tgtwsfacades.integration.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @author Olivier Lamy
 */
public class OrderConfirm
        implements Serializable
{
    private String orderNumber;

    private String manifestNumber;

    private Date date;

    public OrderConfirm()
    {
        // no op
    }

    public OrderConfirm(final String orderNumber, final String manifestNumber, final Date date)
    {
        this.orderNumber = orderNumber;
        this.manifestNumber = manifestNumber;
        this.date = date;
    }

    public String getOrderNumber()
    {
        return orderNumber;
    }

    public void setOrderNumber(final String orderNumber)
    {
        this.orderNumber = orderNumber;
    }

    public String getManifestNumber()
    {
        return manifestNumber;
    }

    public void setManifestNumber(final String manifestNumber)
    {
        this.manifestNumber = manifestNumber;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(final Date date)
    {
        this.date = date;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("OrderConfirm{");
        sb.append("orderNumber='").append(orderNumber).append('\'');
        sb.append(", manifestNumber='").append(manifestNumber).append('\'');
        sb.append(", date=").append(date);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        final OrderConfirm that = (OrderConfirm)o;

        if (!date.equals(that.date))
        {
            return false;
        }
        if (!manifestNumber.equals(that.manifestNumber))
        {
            return false;
        }
        if (!orderNumber.equals(that.orderNumber))
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = orderNumber.hashCode();
        result = 31 * result + manifestNumber.hashCode();
        result = 31 * result + date.hashCode();
        return result;
    }
}
