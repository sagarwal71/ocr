/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * @author rsamuel3
 * 
 */
public class IntegrationCrossReferenceProduct implements Serializable {
    private List<String> refProduct;

    /**
     * @return the refProduct
     */
    public List<String> getRefProduct() {
        return refProduct;
    }

    /**
     * @param refProduct
     *            the refProduct to set
     */
    public void setRefProduct(final List<String> refProduct) {
        this.refProduct = refProduct;
    }

    public void addRefProduct(final String refProductToAdd) {
        if (this.refProduct == null) {
            this.refProduct = new ArrayList<>();
        }
        this.refProduct.add(refProductToAdd);
    }


}
