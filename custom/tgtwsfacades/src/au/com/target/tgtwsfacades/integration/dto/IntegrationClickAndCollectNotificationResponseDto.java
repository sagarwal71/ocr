/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import com.fasterxml.jackson.annotation.JsonRootName;


/**
 * @author rmcalave
 * 
 */
@JsonRootName(value = "integration-cnc-notification-response")
public class IntegrationClickAndCollectNotificationResponseDto {
    private boolean success;

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success
     *            the success to set
     */
    public void setSuccess(final boolean success) {
        this.success = success;
    }
}
