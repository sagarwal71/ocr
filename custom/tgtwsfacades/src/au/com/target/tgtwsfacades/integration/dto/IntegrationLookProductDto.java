/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author mgazal
 * 
 */
@XmlRootElement(name = "look-product")
public class IntegrationLookProductDto {

    private String code;

    public IntegrationLookProductDto() {
    }

    /**
     * @param code
     */
    public IntegrationLookProductDto(final String code) {
        super();
        this.code = code;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }
}
