/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.io.Serializable;
import java.util.List;


/**
 * @author mmaki
 * 
 */
public class IntegrationValuePackItemList implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<IntegrationValuePackItem> valuePackItems;

    /**
     * @return the valuePackItems
     */
    public List<IntegrationValuePackItem> getValuePackItems() {
        return valuePackItems;
    }

    /**
     * @param valuePackItems
     *            the valuePackItems to set
     */
    public void setValuePackItems(final List<IntegrationValuePackItem> valuePackItems) {
        this.valuePackItems = valuePackItems;
    }

}
