/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.io.Serializable;


/**
 * @author mmaki
 * 
 */
public class PickConfirmOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    private String pickConfirmFileName;

    private String orderNumber;

    private String asnNumber;

    private String customerCode;

    private String shipToCode;

    private String consignmentNumber;

    private String carrier;

    private Integer parcelCount;

    private String totalWeight;

    private String totalVolume;

    private PickConfirmEntries pickConfirmEntries;

    public String getPickConfirmFileName() {
        return pickConfirmFileName;
    }

    public void setPickConfirmFileName(final String pickConfirmFileName) {
        this.pickConfirmFileName = pickConfirmFileName;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(final String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getAsnNumber() {
        return asnNumber;
    }

    public void setAsnNumber(final String asnNumber) {
        this.asnNumber = asnNumber;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(final String customerCode) {
        this.customerCode = customerCode;
    }

    public String getShipToCode() {
        return shipToCode;
    }

    public void setShipToCode(final String shipToCode) {
        this.shipToCode = shipToCode;
    }

    public String getConsignmentNumber() {
        return consignmentNumber;
    }

    public void setConsignmentNumber(final String consignmentNumber) {
        this.consignmentNumber = consignmentNumber;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(final String carrier) {
        this.carrier = carrier;
    }

    public Integer getParcelCount() {
        return parcelCount;
    }

    public void setParcelCount(final Integer parcelCount) {
        this.parcelCount = parcelCount;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(final String totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(final String totalVolume) {
        this.totalVolume = totalVolume;
    }

    public PickConfirmEntries getPickConfirmEntries() {
        return pickConfirmEntries;
    }

    public void setPickConfirmEntries(final PickConfirmEntries pickConfirmEntries) {
        this.pickConfirmEntries = pickConfirmEntries;
    }

}
