package au.com.target.tgtwsfacades.deals.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import au.com.target.tgtwsfacades.integration.dto.IntegrationPrincipalDto;


@XmlRootElement(name = "targetDealCategory")
@XmlAccessorType(XmlAccessType.FIELD)
public class TargetDealCategoryDTO {
    @XmlElement
    private String code;

    @XmlElement
    private String name;

    @XmlElement
    private String dealId;

    @XmlElementWrapper(name = "allowedPrincipals")
    @XmlElement(name = "principal")
    private List<IntegrationPrincipalDto> allowedPrincipals;

    @XmlElementWrapper(name = "dealProducts")
    @XmlElement(name = "dealProduct")
    private List<DealProductDTO> dealProducts;

    /**
     * @return the code
     */
    @XmlAttribute(name = "code")
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the allowedPrincipals
     */
    public List<IntegrationPrincipalDto> getAllowedPrincipals() {
        return allowedPrincipals;
    }

    /**
     * @param allowedPrincipals
     *            the allowedPrincipals to set
     */
    public void setAllowedPrincipals(final List<IntegrationPrincipalDto> allowedPrincipals) {
        this.allowedPrincipals = allowedPrincipals;
    }

    /**
     * @return the dealProducts
     */
    public List<DealProductDTO> getDealProducts() {
        return dealProducts;
    }

    /**
     * @param dealProducts
     *            the dealProducts to set
     */
    public void setDealProducts(final List<DealProductDTO> dealProducts) {
        this.dealProducts = dealProducts;
    }

    /**
     * @return the dealId
     */
    public String getDealId() {
        return dealId;
    }

    /**
     * @param dealId
     *            the dealId to set
     */
    public void setDealId(final String dealId) {
        this.dealId = dealId;
    }

}
