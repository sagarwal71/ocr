/**
 * 
 */
package au.com.target.tgtwsfacades.orderAck.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.fulfilmentservice.TargetFulfilmentService;
import au.com.target.tgtfulfilment.util.LoggingContext;
import au.com.target.tgtwsfacades.helper.OrderConsignmentHelper;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.Order;
import au.com.target.tgtwsfacades.integration.dto.Orders;
import au.com.target.tgtwsfacades.orderAck.OrderAckIntegrationFacade;



/**
 * @author rsamuel3
 * 
 */
public class OrderAckIntegrationFacadeImpl implements OrderAckIntegrationFacade {

    private static final Logger LOG = Logger.getLogger(OrderAckIntegrationFacadeImpl.class);

    private static final String ERR_ORDERACK = "ERR-ORDERACK-UPDATE : When updating layby order {0} and error occured with the message : {1}";

    private TargetFulfilmentService targetFulfilmentService;
    private OrderConsignmentHelper orderConsignmentHelper;

    @Override
    public List<IntegrationResponseDto> updateOrderAcknowledgements(final Orders orders) {
        final List<IntegrationResponseDto> responses = new ArrayList<>();
        for (final Order order : orders.getOrders()) {
            final String orderConsignmentIdentifier = order.getOrderNumber();
            final IntegrationResponseDto response = new IntegrationResponseDto(orderConsignmentIdentifier);

            try {
                final TargetConsignmentModel consignmentModel = orderConsignmentHelper
                        .getConsignmentBasedOrderOrConsignmentCode(orderConsignmentIdentifier);
                targetFulfilmentService.processAckForConsignment(consignmentModel.getOrder().getCode(),
                        consignmentModel,
                        LoggingContext.WAREHOUSE);
                response.setSuccessStatus(true);
            }
            catch (final Exception e) {
                response.setSuccessStatus(false);
                final String message = MessageFormat.format(ERR_ORDERACK, orderConsignmentIdentifier, e.getMessage());
                LOG.error(message, e);
                response.addMessage(message);
            }
            responses.add(response);
        }
        return responses;
    }

    /**
     * @return the targetFulfilmentService
     */
    public TargetFulfilmentService getTargetFulfilmentService() {
        return targetFulfilmentService;
    }

    /**
     * @param targetFulfilmentService
     *            the targetFulfilmentService to set
     */
    @Required
    public void setTargetFulfilmentService(final TargetFulfilmentService targetFulfilmentService) {
        this.targetFulfilmentService = targetFulfilmentService;
    }

    /**
     * @param orderConsignmentHelper
     *            the orderConsignmentHelper to set
     */
    @Required
    public void setOrderConsignmentHelper(final OrderConsignmentHelper orderConsignmentHelper) {
        this.orderConsignmentHelper = orderConsignmentHelper;
    }

}
