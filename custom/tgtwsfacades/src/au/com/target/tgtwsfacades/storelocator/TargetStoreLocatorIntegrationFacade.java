/**
 * 
 */
package au.com.target.tgtwsfacades.storelocator;

import de.hybris.platform.core.model.order.CartModel;

import java.util.List;
import java.util.Map;

import au.com.target.tgtwsfacades.integration.dto.IntegrationPointOfServiceDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;



/**
 * @author fkratoch
 * 
 */
public interface TargetStoreLocatorIntegrationFacade {
    /**
     * 
     * @param storeNumber
     * @return pointOfService dto
     * 
     */
    IntegrationPointOfServiceDto getPointOfService(Integer storeNumber);

    /**
     * 
     * @param integrationPointOfServiceDto
     * @return response entity
     * 
     */
    IntegrationResponseDto persistPointOfService(IntegrationPointOfServiceDto integrationPointOfServiceDto);

    /**
     * @param cartModel
     * @return Map of stores
     */
    Map<String, List<IntegrationPointOfServiceDto>> getStateAndStoresForCart(CartModel cartModel);

}
