/**
 * 
 */
package au.com.target.tgtwsfacades.stlimport;

/**
 * @author mgazal
 *
 */
public interface TargetStlImportConstants {

    public static final String UNIQUE = "UNIQUE";

    public static final String YES = "Y";

    public interface LogMessages {

        public static final String ERR_STL_IMPORT_EXCEPTION = "ERR-STL-IMPORT-EXCEPTION: Unable to create/update shopTheLook: {0}.";

        public static final String ERR_STL_PREIMPORT_VALIDATION = "ERR-STL-IMPORT-VALIDATION: Pre-import validation failed for shopTheLook with code: {0}, name: {1}.  Reason(s): {2}";

        public static final String INFO_STL_PREIMPORT_VALIDATION = "INFO-STL-IMPORT-VALIDATION: Pre-import validation successful for shopTheLook with code: {0}, name: {1}";

        public static final String INFO_NEW_SHOPTHELOOK = "INFO-NEW-SHOPTHELOOK: Successfully created new shopTheLook with code: {0}, name: {1}";

        public static final String INFO_NEW_LOOKCOLLECTION = "INFO-NEW-LOOKCOLLECTION: Successfully created new lookCollection with code: {0}, name: {1}";

        public static final String ERR_STL_IMPORT_LOOKCOLLECTION = "ERR_STL_IMPORT_LOOKCOLLECTION: Failed to import lookCollection with code: {0}, name: {1}";

        public static final String INFO_NEW_LOOK = "INFO-NEW-LOOK: Successfully created new look with code: {0}, name: {1}";

        public static final String ERR_STL_IMPORT_LOOK = "ERR-STL-IMPORT-LOOK: Failed to import look with code: {0}, name: {1}";

        public static final String WARN_STL_IMPORT_LOOK = "WARN-STL-IMPORT-LOOK: {0} while importing look with code: {1}, name: {2}";

        public static final String INFO_NEW_LOOKPRODUCT = "INFO-NEW-LOOKPRODUCT: Successfully created new lookProduct with code: {0}, look: {1}";

        public static final String ERR_STL_IMPORT_LOOKPRODUCT = "ERR-STL-IMPORT-LOOKPRODUCT: Failed to import lookProduct with code: {0} for look: {1}";

        public static final String WARN_STL_IMPORT_LOOKPRODUCT = "WARN_STL_IMPORT_LOOKPRODUCT: Error while importing lookProduct with code: {0} for look: {1}";
    }
}
