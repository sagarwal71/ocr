/**
 * 
 */
package au.com.target.tgtwsfacades.valuepack.impl;

import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import au.com.target.tgtcore.enums.DealTypeEnum;
import au.com.target.tgtcore.model.TargetValuePackModel;
import au.com.target.tgtcore.model.TargetValuePackTypeModel;
import au.com.target.tgtcore.valuepack.TargetValuePackTypeService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationValuePackDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationValuePackItem;
import au.com.target.tgtwsfacades.valuepack.ValuePackIntegrationFacade;


/**
 * @author mmaki
 * 
 */
public class ValuePackIntegrationFacadeImpl implements ValuePackIntegrationFacade {

    protected static final String INFO_VALUEPACK_OK = "INFO-VALUEPACK-OK: Value pack with childSKU {0} for leadSKU {1} was inserted successfully";

    protected static final String INFO_VALUEPACK_TYPE_OK = "INFO-VALUEPACK-TYPE-OK: Value pack type with leadSKU {0} was inserted successfully";

    protected static final String ERR_VALUEPACK_ERROR = "ERR-VALUEPACK-ERROR: Failed to insert TargetValuePack for "
            + "leadSKU: {0}";

    protected static final String ERR_VALUEPACK_NO_ITEMS_ERROR = "ERR-VALUEPACK-NO-ITEMS-ERROR: No IntegrationValuePackItems found in the message";

    protected static final String ERR_VALUEPACK_NULL_ERROR = "ERR-VALUEPACK-NULL-ERROR: No IntegrationValuePackDto found in the message";

    protected static final String ERR_VALUEPACK_TYPE_ERROR = "ERR-VALUEPACK-TYPE-ERROR: Insert failed for value pack with leadSKU {0}";

    protected static final String ERR_VALUEPACK_DELETE_FAILED = "ERR-VALUEPACK-DELETE-FAILED: Failed to remove old entries for leadSKU: {0}";

    private static final Logger LOG = Logger.getLogger(ValuePackIntegrationFacadeImpl.class);

    private static final String DEAL_TYPE_VALUE_BUNDLE = "Value Bundle";

    @Autowired
    private ModelService modelService;

    @Autowired
    private TargetValuePackTypeService valuePackTypeService;


    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.valuepack.ValuePackIntegrationFacade#updateValuePack(au.com.target.tgtfacades.integration.dto.IntegrationValuePackDto)
     */
    @Override
    public IntegrationResponseDto updateValuePack(final IntegrationValuePackDto valuePackDto) {

        // Check input values
        if (valuePackDto == null) {
            final IntegrationResponseDto response = new IntegrationResponseDto(null);
            response.setSuccessStatus(false);
            response.addMessage(ERR_VALUEPACK_NULL_ERROR);
            return response;
        }

        final String leadSKU = valuePackDto.getLeadSKU();
        final IntegrationResponseDto response = new IntegrationResponseDto(leadSKU);

        if (valuePackDto.getValuePackItems() == null
                || valuePackDto.getValuePackItems().getValuePackItems() == null
                || valuePackDto.getValuePackItems().getValuePackItems().size() == 0) {
            response.setSuccessStatus(false);
            response.addMessage(ERR_VALUEPACK_NO_ITEMS_ERROR);
            return response;
        }

        final List<IntegrationValuePackItem> items = valuePackDto.getValuePackItems().getValuePackItems();


        // Remove old valuePack if exists
        try {
            deleteOldVersions(leadSKU);
        }
        catch (final Exception e) {
            final String errorMsg = MessageFormat.format(ERR_VALUEPACK_DELETE_FAILED, leadSKU);
            LOG.error(errorMsg, e);
            response.setSuccessStatus(false);
            response.addMessage(errorMsg);

            return response;
        }

        // Insert TargetValuePackModels
        final List<TargetValuePackModel> vpList = new ArrayList<>();
        try {
            for (final IntegrationValuePackItem item : items) {
                final String childSKU = item.getChildSKU();
                if (childSKU != null) {

                    final TargetValuePackModel valuePack = modelService.create(TargetValuePackModel.class);
                    valuePack.setChildSKU(childSKU);
                    valuePack.setChildSKUDescription(item.getChildSKUDescription());
                    if (DEAL_TYPE_VALUE_BUNDLE.equals(item.getDealType())) {
                        valuePack.setValuePackDealType(DealTypeEnum.VALUEBUNDLE);
                    }
                    else {
                        valuePack.setValuePackDealType(DealTypeEnum.BUYGET);
                    }

                    valuePack.setPrice(item.getPrice());
                    valuePack.setQuantity(item.getQuantity());

                    modelService.save(valuePack);
                    vpList.add(valuePack);
                    LOG.info(MessageFormat.format(INFO_VALUEPACK_OK, childSKU, leadSKU));

                }
            }
        }
        catch (final Exception e) {
            final String errorMsg = MessageFormat.format(ERR_VALUEPACK_ERROR, leadSKU);
            LOG.error(errorMsg, e);
            response.setSuccessStatus(false);
            response.addMessage(errorMsg);
            return response;
        }

        // Insert TargetValuePackTypeModel
        if (leadSKU != null) {
            try {
                final TargetValuePackTypeModel type = modelService.create(TargetValuePackTypeModel.class);
                type.setLeadSKU(leadSKU);
                type.setValuePackListQualifier(vpList);
                modelService.save(type);
                LOG.info(MessageFormat.format(INFO_VALUEPACK_TYPE_OK, leadSKU));

            }
            catch (final Exception e) {
                final String errorMsg = MessageFormat.format(ERR_VALUEPACK_TYPE_ERROR, leadSKU);
                LOG.error(errorMsg, e);
                response.setSuccessStatus(false);
                response.addMessage(errorMsg);
                return response;
            }
        }

        response.setSuccessStatus(true);
        return response;
    }

    private void deleteOldVersions(final String leadSKU) {
        final TargetValuePackTypeModel type = findTargetValuePackType(leadSKU);
        if (type != null) {
            modelService.removeAll(type.getValuePackListQualifier());
            modelService.remove(type);
        }

    }

    private TargetValuePackTypeModel findTargetValuePackType(final String leadSKU) {
        try {
            return valuePackTypeService.getByLeadSku(leadSKU);
        }
        catch (final ModelNotFoundException e) {
            LOG.debug("No value pack type model found for given example");
            return null;
        }
    }

}