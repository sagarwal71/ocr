/**
 * 
 */
package au.com.target.tgtwsfacades.listeners;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.event.CatalogVersionSyncCompletionEvent;
import au.com.target.tgtcore.orderEntry.strategies.TargetFixMissingProductInOrderEntryStrategy;
import au.com.target.tgtsale.pos.service.TargetDataRequestJmsService;


/**
 * @author rsamuel3
 * 
 */
public class TargetCatalogSyncCompletionListener extends AbstractEventListener<CatalogVersionSyncCompletionEvent> {

    private static final Logger LOG = Logger.getLogger(TargetCatalogSyncCompletionListener.class);

    private TargetFixMissingProductInOrderEntryStrategy targetFixMissingProductInOrderEntryStrategy;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.event.impl.AbstractEventListener#onEvent
     * (de.hybris.platform.servicelayer.event.events.AbstractEvent)
     */
    @Override
    protected void onEvent(final CatalogVersionSyncCompletionEvent event) {
        LOG.info("INFO-CATALOGSYNC-COMPLETEDTRIGGER - Detected the completion of the catalog sync job");
        getDataRequestJmsService().triggerCatalogSyncFeedback();
        //call the service to fix the relation between orderEntry and productModel(OCR-6642)
        targetFixMissingProductInOrderEntryStrategy.fixMissingProductInOrderEntry();
    }

    /**
     * @return TargetDataRequestJmsService which is the service exposed to send message to queue
     */
    protected TargetDataRequestJmsService getDataRequestJmsService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getDataRequestJmsService().");
    }

    /**
     * @return the targetFixMissingProductInOrderEntryStrategy
     */
    protected TargetFixMissingProductInOrderEntryStrategy getTargetFixMissingProductInOrderEntryStrategy() {
        return targetFixMissingProductInOrderEntryStrategy;
    }

    /**
     * @param targetFixMissingProductInOrderEntryStrategy
     *            the targetFixMissingProductInOrderEntryStrategy to set
     */
    @Required
    public void setTargetFixMissingProductInOrderEntryStrategy(
            final TargetFixMissingProductInOrderEntryStrategy targetFixMissingProductInOrderEntryStrategy) {
        this.targetFixMissingProductInOrderEntryStrategy = targetFixMissingProductInOrderEntryStrategy;
    }



}
