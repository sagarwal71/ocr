/**
 * 
 */
package au.com.target.tgtwsfacades.consignment;

import java.util.List;

import au.com.target.tgtwsfacades.integration.dto.Consignments;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;



/**
 * @author pthoma20
 * 
 */
public interface ConsignmentIntegrationFacade {


    /**
     * Cancel the consignments for the given consignment Ids
     * 
     * @param consignments
     * @return IntegrationResponse for each order indicating whether successful or failure
     */
    List<IntegrationResponseDto> cancelConsignments(Consignments consignments);



    /**
     * Complete the consignments for the given consignment details.
     * 
     * 
     * @param consignments
     * @return IntegrationResponse for each order indicating whether successful or failure
     */
    List<IntegrationResponseDto> completeConsignments(Consignments consignments);
}
