/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.validators;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.product.TargetSizeTypeService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationVariantDto;


/**
 * @author rsamuel3
 *
 */
public class TargetImportSizeVariantValidator implements TargetProductImportValidator {

    private static final Logger LOG = Logger.getLogger(TargetImportSizeVariantValidator.class);

    private TargetSizeTypeService sizeTypeService;

    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.productimport.validators.TargetProductImportValidator#validate(au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto)
     */
    @Override
    public List<String> validate(final IntegrationProductDto dto) {
        final List<String> messages = new ArrayList<>();
        final String variationCode = dto.getVariantCode();

        // sizeType
        final boolean isSizeOnly = dto.getIsSizeOnly() == null ? false : dto.getIsSizeOnly().booleanValue();
        if (isSizeOnly || CollectionUtils.isNotEmpty(dto.getVariants())) {

            if (StringUtils.isBlank(dto.getSizeType())) {
                messages.add(MessageFormat.format(
                        "Missing 'SIZE TYPE' definition (49129), aborting product/variant import for: {0}",
                        variationCode));
            }
            else if (null == sizeTypeService.findSizeTypeForCode(dto.getSizeType())) {
                LOG.warn(
                        "WARN-PRODUCTIMPORT_SIZETYPEMODELMISSING : No SizeType model found in hybris for sizetype code "
                                + dto.getSizeType());
            }
        }

        //size
        if (isSizeOnly) {
            if (StringUtils.isBlank(dto.getSize())) {
                messages.add(MessageFormat.format(
                        "Missing 'SIZE' value (49128), aborting product/variant import for: {0}",
                        variationCode));
            }
        }

        if (!isSizeOnly && !CollectionUtils.isEmpty(dto.getVariants())) {
            for (final IntegrationVariantDto var : dto.getVariants()) {
                if (StringUtils.isBlank(var.getSize())) {
                    messages.add(MessageFormat
                            .format(
                                    "Missing 'SIZE' value (49128) for size variant, aborting product/variant import for: {0}",
                                    var.getVariantCode()));
                    break;
                }

            }
        }
        return messages;
    }

    /**
     * @param sizeTypeService
     *            the sizeTypeService to set
     */
    @Required
    public void setSizeTypeService(final TargetSizeTypeService sizeTypeService) {
        this.sizeTypeService = sizeTypeService;
    }

}
