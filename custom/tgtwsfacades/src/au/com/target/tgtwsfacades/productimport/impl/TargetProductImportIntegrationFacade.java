/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.impl;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.ClassificationSystemService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.VariantsService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.stock.exception.StockLevelNotFoundException;
import de.hybris.platform.variants.model.VariantProductModel;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.giftcards.GiftCardService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.ColourModel;
import au.com.target.tgtcore.model.GiftCardModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetProductSizeModel;
import au.com.target.tgtcore.model.TargetSizeGroupModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.product.BrandService;
import au.com.target.tgtcore.product.ColourService;
import au.com.target.tgtcore.product.ProductTypeService;
import au.com.target.tgtcore.product.TargetProductDepartmentService;
import au.com.target.tgtcore.product.TargetProductDimensionsService;
import au.com.target.tgtcore.product.TargetProductSearchService;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtcore.product.TargetSizeTypeService;
import au.com.target.tgtcore.sizegroup.service.TargetSizeOrderingService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfluent.service.FluentProductUpsertService;
import au.com.target.tgtfluent.service.FluentSkuUpsertService;
import au.com.target.tgtfulfilment.warehouseservices.TargetFulfilmentWarehouseService;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtutility.util.BarcodeTools;
import au.com.target.tgtutility.util.YouTubeUrlUtils;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;
import au.com.target.tgtwsfacades.integration.dto.IntegrationCrossReferenceProduct;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductMediaDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductReferenceDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductReferencesDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationSecondaryImagesDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationVariantDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationWideImagesDto;
import au.com.target.tgtwsfacades.productimport.ProductImportIntegrationFacade;
import au.com.target.tgtwsfacades.productimport.TargetProductImportConstants;
import au.com.target.tgtwsfacades.productimport.data.ProductMediaObjects;
import au.com.target.tgtwsfacades.productimport.services.TargetMediaImportService;
import au.com.target.tgtwsfacades.productimport.utility.TargetProductImportUtil;
import au.com.target.tgtwsfacades.productimport.validators.TargetProductImportValidator;


/**
 * @author fkratoch
 * 
 */
public class TargetProductImportIntegrationFacade implements ProductImportIntegrationFacade {

    private static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private static final Logger LOG = Logger.getLogger(TargetProductImportIntegrationFacade.class);

    public static final String ZIP_PAYMENT_CODE = "zip";

    private List<TargetProductImportValidator> validators;

    private ProductService productService;

    private ModelService modelService;

    private CategoryService categoryService;

    private CatalogVersionService catalogVersionService;

    private BrandService brandService;

    private ColourService colourService;

    private ClassificationService classificationService;

    private VariantsService variantService;

    private TargetMediaImportService targetMediaImportService;

    private TargetProductSearchService productSearchService;

    private ClassificationSystemService classificationSystemService;

    private ProductTypeService productTypeService;

    private MediaService mediaService;

    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;

    private TargetStockService stockService;

    private TargetWarehouseService warehouseService;

    private TargetDeliveryService deliveryModeService;

    private TargetProductDepartmentService targetProductDepartmentService;

    private TargetSizeTypeService sizeTypeService;

    private TargetProductDimensionsService targetProductDimensionsService;

    private TargetProductService targetProductService;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private TargetFulfilmentWarehouseService targetFulfilmentWarehouseService;

    private GiftCardService giftCardService;

    /**
     * to disable/enable medias association to the product (disable can increase performance)
     */
    private boolean associateMedia = true;

    private String homeDeliveryCode;

    private String clickAndCollectCode;

    private String expressDeliveryCode;

    private String ebayExpressDeliveryCode;

    private String ebayHomeDeliveryCode;

    private String ebayClickAndCollectDeliveryCode;

    private String digitalGiftcardCode;

    private int digitalProductStock;

    private int giftMaxOrderValue;

    private int giftMaxOrderQuantity;

    private TargetProductImportUtil targetProductImportUtil;

    private TargetSizeOrderingService targetSizeOrderingService;

    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    private FluentProductUpsertService fluentProductUpsertService;

    private FluentSkuUpsertService fluentSkuUpsertService;

    private TargetOrderService targetOrderService;

    private int hoursConstantEmbargoReleaseDate;

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.productimport.ProductImportIntegrationFacade#persistTargetProduct(
     * java.lang.String, au.com.target.tgtfacades.productimport.dto.IntegrationProductDto)
     */
    @Override
    public IntegrationResponseDto persistTargetProduct(final IntegrationProductDto dto) {

        // run initial sanity check for product code, variant code, product name
        final String productCode = dto.getProductCode();
        final String variantCode = dto.getVariantCode();
        final String productName = dto.getName();

        final String code = StringUtils.isNotBlank(variantCode) ? variantCode
                : (StringUtils.isNotBlank(productName) ? productName
                        : (StringUtils.isNotBlank(productCode) ? productCode : "???"));

        final IntegrationResponseDto response = new IntegrationResponseDto(code);
        response.setSuccessStatus(true);

        if (StringUtils.isBlank(productCode) || StringUtils.isBlank(variantCode)
                || StringUtils.isBlank(productName)) {

            response.setSuccessStatus(false);
            response.addMessage("One of required values is missing [Base Product Code, Variation Code, Product Name]");
            return response;
        }

        synchronized (productCode.intern()) {
            try {
                return processTargetProduct(dto, response);
            }
            catch (final ModelSavingException mse) {
                // this is in place because of:
                // Hybris issue SUP-9353 - Duplicate products are getting created
                // we will only re-try to create product 1 more time
                LOG.warn(
                        "WARN_PRODUCTIMPORT_EXCEPTION : Exception occured when saving a product/variant with the message : "
                                + mse.getMessage());
                if (mse.getMessage().toUpperCase().contains(TargetProductImportConstants.UNIQUE)) {
                    try {
                        LOG.warn(
                                "WARN_PRODUCTIMPORT_EXCEPTION : A unique identifier exception has been thrown. Attempting to reimport the product with code "
                                        + dto.getProductCode());
                        return processTargetProduct(dto, response);
                    }
                    catch (final Exception e) {
                        return generateExceptionResponse(response, e);
                    }
                }
                else {
                    return generateExceptionResponse(response, mse);
                }
            }
            catch (final Exception e) {
                return generateExceptionResponse(response, e);
            }
        }
    }

    private IntegrationResponseDto generateExceptionResponse(final IntegrationResponseDto response, final Exception e) {

        final String message = MessageFormat.format(
                TargetProductImportConstants.LogMessages.ERR_PRODUCTIMPORT_EXCEPTION, response.getCode());
        LOG.error(message, e);
        final String errMsg = e.getMessage() == null ? " An unexpected exception occured during the import"
                : " " + e.getMessage();
        response.setSuccessStatus(false);
        response.addMessage(message + errMsg);

        return response;
    }

    private IntegrationResponseDto processTargetProduct(
            final IntegrationProductDto dto,
            final IntegrationResponseDto response)
            throws InstantiationException {
        final String productCode = dto.getProductCode();
        final String variantCode = dto.getVariantCode();
        final String productName = dto.getName();

        if (!isImportDataValid(dto, response)) {
            if (LOG.isInfoEnabled()) {
                LOG.info(MessageFormat.format(TargetProductImportConstants.LogMessages.ERR_PREIMPORT_VALIDATION,
                        variantCode, productName,
                        response.getMessages()));
            }
            response.setSuccessStatus(false);
            return response;
        }

        ProductMediaObjects mediaObjects = null;

        if (associateMedia) {

            final IntegrationProductMediaDto productMedia = dto.getProductMedia();
            mediaObjects = populateMediaObjects(productCode, productMedia, response);

            if (mediaObjects != null) {
                if (!hasAllMedia(mediaObjects)) {
                    final String error = MessageFormat.format(
                            TargetProductImportConstants.LogMessages.ERR_PRODUCTIMPORT_SOME_MEDIAMISSING, variantCode);
                    LOG.info(error);
                    response.setSuccessStatus(false);
                    return response;
                }
                else if (CollectionUtils.isNotEmpty(mediaObjects.getErrors())) {
                    response.setSuccessStatus(false);
                }
            }
            else {
                final String error = MessageFormat.format(
                        TargetProductImportConstants.LogMessages.ERR_PRODUCTIMPORT_MEDIAMISSING, variantCode);
                LOG.info(error);
                response.setSuccessStatus(false);
                response.addMessage(error);
                return response;
            }

        }

        LOG.info(MessageFormat.format(TargetProductImportConstants.LogMessages.INFO_PREIMPORT_VALIDATION, variantCode,
                productName));
        final TargetProductModel baseProduct = getTargetProduct(dto);

        TargetColourVariantProductModel colourVariant = null;
        try {
            colourVariant = createTargetColourVariant(baseProduct, dto, mediaObjects, response);
        }
        catch (final InstantiationException e) {
            LOG.info(MessageFormat.format(TargetProductImportConstants.LogMessages.ERR_COLOUR_VARIATION, productCode,
                    productName, variantCode, e));
            response.setSuccessStatus(false);
            response.addMessage(e.getMessage());
            return response;
        }


        if (dto.getIsSizeOnly().booleanValue()) {
            try {
                createSizeVariant(colourVariant, dto, null, response);
            }
            catch (final InstantiationException e) {
                LOG.info(MessageFormat.format(TargetProductImportConstants.LogMessages.ERR_SIZE_VARIATION, productCode,
                        productName, dto.getSize(), variantCode, e));
                response.setSuccessStatus(false);
                response.addMessage(e.getMessage());
                return response;
            }
        }
        else if (!CollectionUtils.isEmpty(dto.getVariants())) {
            for (final IntegrationVariantDto sizeVar : dto.getVariants()) {
                try {
                    createSizeVariant(colourVariant, dto, sizeVar, response);
                }
                catch (final InstantiationException e) {
                    LOG.info(MessageFormat.format(TargetProductImportConstants.LogMessages.ERR_SIZE_VARIATION,
                            productCode, productName, sizeVar.getSize(), sizeVar.getVariantCode()));
                    response.setSuccessStatus(false);
                    response.addMessage(e.getMessage());
                }
            }
        }
        else {
            if (null != dto.getAvailableOnEbay()) {
                colourVariant.setAvailableOnEbay(targetProductImportUtil.getBooleanValue(dto.getAvailableOnEbay()));
            }

            final TargetProductDimensionsModel targetProductPkgDimModel = populateProductPackageDimensions(dto, null,
                    dto.getAvailableDigitalDelivery());
            if (null != targetProductPkgDimModel) {
                colourVariant.setProductPackageDimensions(targetProductPkgDimModel);
            }
            populateMerchProductStatus(dto, null, colourVariant);
            modelService.save(colourVariant);
            sendFluentVariantFeed(colourVariant);
        }

        targetProductService.setOnlineDateIfApplicable(colourVariant);
        return response;
    }

    /**
     * @param dto
     * @param baseProduct
     * @throws ModelSavingException
     */
    private void createGiftCard(final IntegrationProductDto dto, final TargetProductModel baseProduct)
            throws ModelSavingException {
        GiftCardModel giftCard = giftCardService.getGiftCard(dto.getGiftcardBrandId());
        if (giftCard == null) {
            giftCard = modelService.create(GiftCardModel.class);
            giftCard.setBrandId(dto.getGiftcardBrandId());
            giftCard.setBrandName(dto.getGiftcardBrandId());
            giftCard.setMaxOrderQuantity(Integer.valueOf(giftMaxOrderQuantity));
            giftCard.setMaxOrderValue(Integer.valueOf(giftMaxOrderValue));
            modelService.save(giftCard);
            modelService.refresh(giftCard);
            baseProduct.setGiftCard(giftCard);
        }
        else {
            baseProduct.setGiftCard(giftCard);
        }
    }

    /**
     * Method accepts the ProductDTO (baseproduct in step) and the variant DTO and extracts the product dimension
     * details. This will process the create a product dimension if it does not exist or it will update the existing one
     * with the correct value. If no values for package dimensions come from step then it does not update.
     * 
     * @param dto
     * @param vdto
     * @param digitalDelivery
     * @return TargetProductDimensionsModel
     */
    private TargetProductDimensionsModel populateProductPackageDimensions(final IntegrationProductDto dto,
            final IntegrationVariantDto vdto, final String digitalDelivery) {
        TargetProductDimensionsModel targetProductPkgDimModel = null;
        Double pkgWeight = null;
        Double pkgHeight = null;
        Double pkgWidth = null;
        Double pkgLength = null;
        String dimensionCode = null;
        String variantProductCode = null;
        String baseProductCode = null;

        if (null != vdto) {
            pkgWeight = null != vdto.getPkgWeight() ? vdto.getPkgWeight() : null;
            pkgHeight = null != vdto.getPkgHeight() ? vdto.getPkgHeight() : null;
            pkgWidth = null != vdto.getPkgWidth() ? vdto.getPkgWidth() : null;
            pkgLength = null != vdto.getPkgLength() ? vdto.getPkgLength() : null;
            dimensionCode = TargetProductImportConstants.PRODUCT_PKG_DIMENSION_KEY.concat(vdto.getVariantCode());
            variantProductCode = vdto.getVariantCode();
            baseProductCode = vdto.getBaseProduct();
        }
        else if (null != dto) {
            pkgWeight = null != dto.getPkgWeight() ? dto.getPkgWeight() : null;
            pkgHeight = null != dto.getPkgHeight() ? dto.getPkgHeight() : null;
            pkgWidth = null != dto.getPkgWidth() ? dto.getPkgWidth() : null;
            pkgLength = null != dto.getPkgLength() ? dto.getPkgLength() : null;
            dimensionCode = TargetProductImportConstants.PRODUCT_PKG_DIMENSION_KEY.concat(dto.getVariantCode());
            variantProductCode = dto.getVariantCode();
            baseProductCode = dto.getProductCode();
        }
        checkIfDimensionsAreInvalidAndLog(pkgWeight, pkgLength, pkgWidth, pkgHeight, variantProductCode,
                baseProductCode, digitalDelivery);

        //if all the values are got as null then the dimensions attributes have not come from Step
        if (null == pkgWeight && null == pkgHeight && null == pkgWidth && null == pkgLength) {
            return null;
        }
        final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION);
        targetProductPkgDimModel = targetProductDimensionsService.getProductDimensionsForCode(catalogVersionModel,
                dimensionCode);
        if (null == targetProductPkgDimModel) {
            targetProductPkgDimModel = modelService.create(TargetProductDimensionsModel.class);
            targetProductPkgDimModel.setDimensionCode(dimensionCode);
            targetProductPkgDimModel.setCatalogVersion(catalogVersionModel);
        }
        targetProductPkgDimModel.setWeight(pkgWeight);
        targetProductPkgDimModel.setHeight(pkgHeight);
        targetProductPkgDimModel.setLength(pkgLength);
        targetProductPkgDimModel.setWidth(pkgWidth);

        modelService.save(targetProductPkgDimModel);

        return targetProductPkgDimModel;
    }

    /**
     * Populate the merchProductStatus field
     * 
     * @param dto
     * @param vdto
     * @param variantProductModel
     */
    protected void populateMerchProductStatus(final IntegrationProductDto dto, final IntegrationVariantDto vdto,
            final AbstractTargetVariantProductModel variantProductModel) {
        variantProductModel
                .setMerchProductStatus(vdto != null ? vdto.getMerchProductStatus() : dto.getMerchProductStatus());
    }

    /**
     * Method to check if product's weight/dimension is invalid and then log it.
     * 
     * @param pkgWeight
     * @param pkgLength
     * @param pkgWidth
     * @param pkgHeight
     * @param variantProductCode
     * @param isDigitalDelivery
     */
    protected void checkIfDimensionsAreInvalidAndLog(final Double pkgWeight, final Double pkgLength,
            final Double pkgWidth, final Double pkgHeight, final String variantProductCode,
            final String baseProductCode, final String isDigitalDelivery) {
        if (!isDimensionValid(pkgWeight) || !isDimensionValid(pkgLength) || !isDimensionValid(pkgWidth)
                || !isDimensionValid(pkgHeight)) {
            logInvalidDimension(TargetProductImportConstants.LogMessages.INFO_DIMENSION_INVALID_EVENT,
                    variantProductCode, baseProductCode);
        }
        if ((isValidDimensionForDigitialDelivery(pkgWeight) || isValidDimensionForDigitialDelivery(pkgLength)
                || isValidDimensionForDigitialDelivery(pkgWidth) || isValidDimensionForDigitialDelivery(pkgHeight))
                && BooleanUtils.isFalse(targetProductImportUtil.getBooleanValue(isDigitalDelivery))) {
            logInvalidDimension(TargetProductImportConstants.LogMessages.INFO_DIGITIAL_PRODUCT_INVALID_EVENT,
                    variantProductCode, baseProductCode);
        }
    }

    /**
     * Method to log invalid dimension event with product code and base product code.
     * 
     * @param log
     * @param variantProductCode
     * @param baseProductCode
     */
    protected void logInvalidDimension(final String log, final String variantProductCode,
            final String baseProductCode) {
        LOG.info(MessageFormat.format(log, variantProductCode, baseProductCode));
    }

    /**
     * Method to check if value is not null and greater than 0.
     * 
     * @param value
     * @return true - if value is valid, else false.
     */
    private boolean isDimensionValid(final Double value) {
        if (value != null && value.doubleValue() >= 0) {
            return true;
        }
        return false;
    }

    private boolean isValidDimensionForDigitialDelivery(final Double value) {
        if (value != null && value.compareTo(Double.valueOf(0)) == 0) {
            return true;
        }
        return false;
    }

    /**
     * 
     * @param mediaObjects
     * @return true if all necessary images available
     */
    public boolean hasAllMedia(final ProductMediaObjects mediaObjects) {
        final List<String> imageErrors = mediaObjects.getErrors();
        return mediaObjects.hasValidPrimaryImages() && (CollectionUtils.isEmpty(imageErrors)
                || (CollectionUtils.isNotEmpty(imageErrors) && imageErrors.size() <= 1));
    }

    /**
     * populates the colourVariant with the productMedia
     * 
     * @param colourVariant
     * @param mediaObjects
     */
    private void setProductMedia(final TargetColourVariantProductModel colourVariant,
            final ProductMediaObjects mediaObjects) {
        final Map<String, MediaModel> primaryImages = mediaObjects.getPrimaryImages();
        final List<MediaModel> others = new ArrayList<>();
        if (MapUtils.isNotEmpty(primaryImages)) {
            final String heroMedia = mediaObjects.getPrimaryImageKey(TargetMediaImportService.HERO_MEDIA);
            if (primaryImages.containsKey(heroMedia)) {
                final MediaModel model = primaryImages.get(heroMedia);
                colourVariant.setPicture(model);
                colourVariant.setNormal(Collections.singletonList(model));
                others.add(model);
            }
            final String thumbMedia = mediaObjects.getPrimaryImageKey(TargetMediaImportService.THUMB_MEDIA);
            if (primaryImages.containsKey(thumbMedia)) {
                final MediaModel model = primaryImages.get(thumbMedia);
                colourVariant.setThumbnail(model);
                colourVariant.setThumbnails(Collections.singletonList(model));
                others.add(model);
            }
            final String largeMedia = mediaObjects.getPrimaryImageKey(TargetMediaImportService.LARGE_MEDIA);
            if (primaryImages.containsKey(largeMedia)) {
                others.add(primaryImages.get(largeMedia));
            }
            final String listMedia = mediaObjects.getPrimaryImageKey(TargetMediaImportService.LIST_MEDIA);
            if (primaryImages.containsKey(listMedia)) {
                others.add(primaryImages.get(listMedia));
            }
            final String gridMedia = mediaObjects.getPrimaryImageKey(TargetMediaImportService.GRID_MEDIA);
            if (primaryImages.containsKey(gridMedia)) {
                others.add(primaryImages.get(gridMedia));
            }
            final String fullMedia = mediaObjects.getPrimaryImageKey(TargetMediaImportService.FULL_MEDIA);
            if (primaryImages.containsKey(fullMedia)) {
                others.add(primaryImages.get(fullMedia));
            }

            colourVariant.setOthers(others);
        }
        final List<MediaContainerModel> mediaContainers = mediaObjects.getMediaContainers();
        if (CollectionUtils.isNotEmpty(mediaContainers)) {
            colourVariant.setGalleryImages(mediaContainers);
        }
    }

    /**
     * @param productMedia
     */
    public ProductMediaObjects populateMediaObjects(
            final String productCode,
            final IntegrationProductMediaDto productMedia,
            final IntegrationResponseDto response) {
        ProductMediaObjects mediaObjects = null;
        if (productMedia != null) {
            final IntegrationWideImagesDto wideImages = productMedia.getWideImages();
            final IntegrationSecondaryImagesDto secondaryImages = productMedia.getSecondaryImages();

            mediaObjects = targetMediaImportService.getMediaObjects(productCode, productMedia.getPrimaryImage(),
                    productMedia.getSwatchImage(),
                    secondaryImages != null ? secondaryImages.getSecondaryImage() : null,
                    wideImages != null ? wideImages.getWideImage() : null);

            if (CollectionUtils.isNotEmpty(mediaObjects.getErrors())) {
                response.addMessages(mediaObjects.getErrors());
            }
        }
        return mediaObjects;
    }

    /**
     * 
     * @param dto
     * @return Success / Failure flag.
     * @throws InstantiationException
     */
    private boolean isImportDataValid(final IntegrationProductDto dto, final IntegrationResponseDto response)
            throws InstantiationException {

        if (CollectionUtils.isNotEmpty(validators)) {
            for (final TargetProductImportValidator validator : validators) {
                final List<String> messages = validator.validate(dto);
                if (CollectionUtils.isNotEmpty(messages)) {
                    response.addMessages(messages);
                }
            }
        }

        if (!CollectionUtils.isEmpty(response.getMessages())) {
            return false;
        }


        return true;
    }








    /**
     * 
     * @param dto
     * @return Target base product
     * @throws InstantiationException
     */
    private TargetProductModel getTargetProduct(final IntegrationProductDto dto) throws InstantiationException {
        boolean isNewProduct = false;
        TargetProductModel product = (TargetProductModel)getExistingProduct(dto.getProductCode());
        if (product == null) {
            product = createNewTargetProduct(dto);
            isNewProduct = true;
            // must save product before trying to populate other properties
            // mainly: license, gender, age range
            try {
                modelService.save(product);
                LOG.info(MessageFormat.format(TargetProductImportConstants.LogMessages.INFO_NEW_BASE_PRODUCT,
                        dto.getProductCode(), dto.getName()));
            }
            catch (final ModelSavingException mse) {
                // this is in place because of:
                // Hybris issue SUP-9353 - Duplicate products are getting created
                // we will only re-try to create product 1 more time
                if (mse.getMessage().toUpperCase().contains(TargetProductImportConstants.UNIQUE)) {
                    product = (TargetProductModel)getExistingProduct(dto.getProductCode());
                    isNewProduct = false;
                }
                else {
                    throw mse;
                }
            }

        }
        populateProductDetails(product, dto, isNewProduct);
        modelService.save(product);

        sendFluentProductFeed(product);

        return product;
    }

    /**
     * Send product data to fluent
     * 
     * @param product
     */
    private void sendFluentProductFeed(final TargetProductModel product) {
        if (!targetFeatureSwitchService.isFeatureEnabled(TargetProductImportConstants.FEATURE_FLUENT)) {
            return;
        }
        try {
            fluentProductUpsertService.upsertProduct(product);
        }
        catch (final Exception e) {
            LOG.error(MessageFormat.format(TargetProductImportConstants.LogMessages.ERR_PRODUCTIMPORT_BASE_PRODUCT,
                    product.getCode(), product.getName()), e);
        }
    }

    /**
     * Send variant data to fluent
     * 
     * @param variantProduct
     */
    private void sendFluentVariantFeed(final AbstractTargetVariantProductModel variantProduct) {
        if (!targetFeatureSwitchService.isFeatureEnabled(TargetProductImportConstants.FEATURE_FLUENT)) {
            return;
        }
        try {
            fluentSkuUpsertService.upsertSku(variantProduct);
        }
        catch (final Exception e) {
            LOG.error(MessageFormat.format(TargetProductImportConstants.LogMessages.ERR_PRODUCTIMPORT_VARIANT_PRODUCT,
                    variantProduct.getCode()), e);
        }
    }


    /**
     * returning false if the product type does not exist or it is blank. This should never happen as it is a drop down
     * on step
     * 
     * @param productType
     * @return boolean true if it is bulky1 or bulky2 or bulky3
     */
    private boolean isBulky(final String productType) {
        if (StringUtils.isNotBlank(productType)) {
            final ProductTypeModel typeByCode = productTypeService.getByCode(productType);
            return targetProductImportUtil.isBulky(typeByCode);
        }
        return false;
    }


    /**
     * 
     * @param code
     * @return Existing product if exists, else null
     */
    private ProductModel getExistingProduct(final String code) {
        ProductModel product = null;

        try {
            product = productService.getProductForCode(code.trim());
        }
        catch (final Exception e) {
            LOG.info(e.getMessage());
        }

        return product;
    }

    private TargetColourVariantProductModel getExistingColourVariant(final String baseProductCode,
            final String variantSwatchCode) {
        final TargetColourVariantProductModel variant = null;

        try {
            return productSearchService.getColourVariantByBaseProductAndSwatch(baseProductCode, variantSwatchCode);
        }
        catch (final ModelNotFoundException na) {
            LOG.info(MessageFormat.format("No colour variant with base product code {0} and swatch colour {1} found.",
                    baseProductCode, variantSwatchCode));
        }
        catch (final AmbiguousIdentifierException aie) {
            LOG.info(MessageFormat.format(
                    "More than one variant with base product code {0} and swatch colour {1} found.",
                    baseProductCode, variantSwatchCode));
        }
        catch (final Exception e) {
            LOG.info(e.getMessage());
        }

        return variant;
    }

    /**
     * 
     * @param dto
     * @return New TargetProductModel populated with basic information from dto
     * @throws InstantiationException
     */
    private TargetProductModel createNewTargetProduct(final IntegrationProductDto dto) throws InstantiationException {
        final TargetProductModel product = modelService.create(TargetProductModel.class);

        // minimum requirements: 
        // - code, catalogVersion, approvalStatus, brand, productType
        // - available for CNC, available for Lay-by, available for home-delivery

        product.setCode(dto.getProductCode());
        product.setCatalogVersion(catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION));
        product.setVariantType(variantService
                .getVariantTypeForCode(TargetProductImportConstants.TARGET_COLOUR_VARIANT_PRODUCT_TYPE));
        // base product will always be approved, the real approval status will be set at variant level
        product.setApprovalStatus(ArticleApprovalStatus.APPROVED);
        product.setBrand(getProductBrand(dto.getBrand()));
        product.setProductType(getProductType(dto));
        product.setClearanceProduct(targetProductImportUtil.getBooleanValue(dto.getClearance()));
        setPurchaseOptionValues(product, dto);
        populateDeliveryModes(product, dto);

        // in order to be able to get product features
        // product has to be attached to a category
        // so lets attach it right away
        setProductCategories(product, dto);

        if (dto.getGiftcardBrandId() != null) {
            createGiftCard(dto, product);
        }

        return product;
    }

    /**
     * 
     * @param product
     * @param dto
     * @param isNewProduct
     * @throws InstantiationException
     */
    private void populateProductDetails(final TargetProductModel product, final IntegrationProductDto dto,
            final boolean isNewProduct) throws InstantiationException {

        if (!isNewProduct) {
            // only try to set these values if this is NOT a new product
            // if it is a new product, these were set when the product was created
            product.setBrand(getProductBrand(dto.getBrand()));
            product.setProductType(getProductType(dto));
            setPurchaseOptionValues(product, dto);

            populateDeliveryModes(product, dto);

            // base product will always be approved, the real approval status will be set at variant level
            product.setApprovalStatus(ArticleApprovalStatus.APPROVED);
            setProductCategories(product, dto);
        }


        final TargetMerchDepartmentModel targetMerchDepartmentModel = targetProductDepartmentService
                .getDepartmentCategoryModel(catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                        TgtCoreConstants.Catalog.OFFLINE_VERSION), dto.getDepartment());
        targetProductDepartmentService.processMerchDeparment(
                product, targetMerchDepartmentModel);

        product.setMerchDepartment(targetMerchDepartmentModel);
        product.setName(dto.getName());

        final String description = dto.getDescription();
        product.setDescription(StringUtils.isNotBlank(description) ? description : StringUtils.EMPTY);

        final String materials = dto.getMaterials();
        product.setMaterials(StringUtils.isNotBlank(materials) ? materials : StringUtils.EMPTY);

        final String careInstructions = dto.getCareInstructions();
        product.setCareInstructions(StringUtils.isNotBlank(careInstructions) ? careInstructions : StringUtils.EMPTY);

        final String features = dto.getWebFeatures();
        product.setProductFeatures(StringUtils.isNotBlank(features) ? features : StringUtils.EMPTY);
        final String extraInfo = dto.getWebExtraInfo();
        product.setExtraInfo(StringUtils.isNotBlank(extraInfo) ? extraInfo : StringUtils.EMPTY);

        poulateFacets(product, dto);

        addProductYoutubeVideo(product, dto.getYoutube());

        setupProductFeatures(product, dto);
        product.setShowWhenOutOfStock(targetProductImportUtil.getBooleanValue(dto.getShowIfOutOfStock()));
        product.setClearanceProduct(targetProductImportUtil.getBooleanValue(dto.getClearance()));

        product.setShowStoreStock(targetProductImportUtil.getBooleanValue(dto.getShowInStoreStock()));
        product.setPreview(targetProductImportUtil.getBooleanValue(dto.getPreview()));

        final String originalCategory = dto.getOriginalCategory();
        if (originalCategory != null) {
            final CategoryModel originalCategoryModel = getCategoryForCode(dto.getOriginalCategory().trim());
            if (originalCategoryModel instanceof TargetProductCategoryModel) {
                product.setOriginalCategory((TargetProductCategoryModel)originalCategoryModel);
            }
        }

        if (!isNewProduct) {
            LOG.info(MessageFormat.format(TargetProductImportConstants.LogMessages.INFO_EXISTING_BASE_PRODUCT,
                    dto.getProductCode(), dto.getName()));
        }
    }

    /**
     * 
     * @param colourVariantProduct
     * @param dto
     */
    private void populatePreOrderAttributes(final TargetColourVariantProductModel colourVariantProduct,
            final IntegrationProductDto dto) {

        final Date preOrderStartDate = parseStringToDate(dto.getPreOrderStartDate());
        final Date preOrderEndDate = parseStringToDate(dto.getPreOrderEndDate());
        final Date preOrderEmbargoReleaseDate = parseStringToDate(dto.getPreOrderEmbargoReleaseDate());
        if (preOrderStartDate != null) {
            colourVariantProduct.setPreOrderStartDateTime(preOrderStartDate);
        }
        if (preOrderEndDate != null) {
            colourVariantProduct.setPreOrderEndDateTime(preOrderEndDate);
        }
        if (preOrderEmbargoReleaseDate != null) {
            final Date saleStartDateTime = checkEmbargoReleaseDate(preOrderEmbargoReleaseDate);
            colourVariantProduct.setNormalSaleStartDateTime(saleStartDateTime);
            setNormalSaleStartDateTimeOnOrder(colourVariantProduct.getCode(), saleStartDateTime);
        }
        else {
            LOG.warn("WARN-PRODUCTIMPORT: preOrderEmbargoReleaseDate is empty or invalid");
        }
    }

    private Date checkEmbargoReleaseDate(final Date preOrderEmbargoReleaseDate) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(preOrderEmbargoReleaseDate);

        final int hours = cal.get(Calendar.HOUR);
        final int minutes = cal.get(Calendar.MINUTE);
        if (hours == 0 && minutes == 0) {
            cal.set(Calendar.HOUR, hoursConstantEmbargoReleaseDate);
        }
        return cal.getTime();
    }

    /**
     * @param code
     * @param saleStartDateTime
     */
    protected void setNormalSaleStartDateTimeOnOrder(final String code, final Date saleStartDateTime) {
        final List<OrderModel> orders = targetOrderService.findPendingPreOrdersForProduct(code);
        if (CollectionUtils.isEmpty(orders)) {
            return;
        }
        for (final OrderModel order : orders) {
            order.setNormalSaleStartDateTime(saleStartDateTime);
        }
        modelService.saveAll(orders);
    }

    /**
     * 
     * @param dateString
     * @return date
     */
    private Date parseStringToDate(final String dateString) {
        Date dateTime = null;
        if (StringUtils.isNotEmpty(dateString)) {
            try {
                dateTime = new SimpleDateFormat(DATETIME_FORMAT).parse(dateString);
            }
            catch (final ParseException e) {
                LOG.info("Error converting date string to date");
            }
        }
        return dateTime;
    }

    /**
     * @param product
     * @param dto
     */
    private void poulateFacets(final TargetProductModel product, final IntegrationProductDto dto) {
        final String fit = dto.getFit();
        product.setFit(StringUtils.isNotBlank(fit) ? fit : StringUtils.EMPTY);

        final String material = dto.getMaterial();
        product.setMaterial(StringUtils.isNotBlank(material) ? material : StringUtils.EMPTY);

        final String trend = dto.getTrend();
        product.setTrend(StringUtils.isNotBlank(trend) ? trend : StringUtils.EMPTY);

        final String giftsFor = dto.getGiftsFor();
        product.setGiftsFor(StringUtils.isNotBlank(giftsFor) ? giftsFor : StringUtils.EMPTY);

        final String platform = dto.getPlatform();
        product.setPlatform(StringUtils.isNotBlank(platform) ? platform : StringUtils.EMPTY);

        final String genre = dto.getGenre();
        product.setGenre(StringUtils.isNotBlank(genre) ? genre : StringUtils.EMPTY);

        final String season = dto.getSeason();
        product.setSeason(StringUtils.isNotBlank(season) ? season : StringUtils.EMPTY);

        final String type = dto.getType();
        product.setType(StringUtils.isNotBlank(type) ? type : StringUtils.EMPTY);

        final String occassion = dto.getOccassion();
        product.setOccassion(StringUtils.isNotBlank(occassion) ? occassion : StringUtils.EMPTY);
    }


    private void populateDeliveryModes(final TargetProductModel product, final IntegrationProductDto dto) {
        final Set<DeliveryModeModel> deliveryModes = new HashSet<>();

        if (isProductTypeDigital(dto)) {
            final DeliveryModeModel digitalDeliveryModeModel = getDigitalDeliveryModeModel();
            deliveryModes.add(digitalDeliveryModeModel);
        }
        else if (targetProductImportUtil.isProductPhysicalGiftcard(dto)) {
            deliveryModes.add(getDeliveryModeModelForHomeDelivery());
        }
        else {

            if (BooleanUtils.isTrue(targetProductImportUtil.getBooleanValue(dto.getAvailableHomeDelivery()))) {
                deliveryModes.add(getDeliveryModeModelForHomeDelivery());
            }

            if (BooleanUtils.isTrue(targetProductImportUtil.getBooleanValue(dto.getAvailableCnc()))) {
                deliveryModes.add(getDeliveryModeModelForClickAndCollect());
            }

            if (BooleanUtils.isTrue(targetProductImportUtil.getBooleanValue(dto.getAvailableOnEbay()))) {
                deliveryModes.add(getDeliveryModeModelForEbayHomeDelivery());
                if (BooleanUtils
                        .isTrue(targetProductImportUtil.getBooleanValue(dto.getAvailableEbayExpressDelivery()))) {
                    deliveryModes.add(getDeliveryModeModelForEbayExpressDelivery());
                }
                if (BooleanUtils.isTrue(targetProductImportUtil.getBooleanValue(dto.getAvailableCnc()))) {
                    deliveryModes.add(getEbayClickAndCollectDeliveryCode());
                }
            }

            if (targetFeatureSwitchService.isFeatureEnabled(TargetProductImportConstants.FEATURE_EXPRESS_BY_FLAG)) {
                if (BooleanUtils.isTrue(targetProductImportUtil.getBooleanValue(dto.getAvailableExpressDelivery()))) {
                    deliveryModes.add(getDeliveryModeModelForExpressDelivery());
                }
            }
            else {
                if (BooleanUtils.isTrue(isAvailableForExpressDelivery(dto))) {
                    deliveryModes.add(getDeliveryModeModelForExpressDelivery());
                }
            }
        }

        product.setDeliveryModes(deliveryModes);
    }

    /**
     * @param dto
     * @return <code>true</code> if the product type is Digital <code>false</code> otherwise.
     * 
     */
    private boolean isProductTypeDigital(final IntegrationProductDto dto) {
        return TargetProductImportConstants.PRODUCT_TYPE_DIGITAL.equals(dto.getProductType());
    }

    private Boolean isAvailableForExpressDelivery(final IntegrationProductDto dto) {
        final CategoryModel departmentCategory = getCategoryForCode(dto.getDepartment().toString());

        final String productType = dto.getProductType();

        if (departmentCategory instanceof TargetMerchDepartmentModel && !isBulky(productType)) {
            final TargetMerchDepartmentModel merchDepartment = (TargetMerchDepartmentModel)departmentCategory;
            return merchDepartment.getExpressDeliveryEligible();
        }
        return Boolean.FALSE;
    }

    private DeliveryModeModel getDeliveryModeModel(final String deliveryModeCode) {
        return deliveryModeService.getDeliveryModeForCode(deliveryModeCode);
    }

    private DeliveryModeModel getDeliveryModeModelForHomeDelivery() {
        return getDeliveryModeModel(homeDeliveryCode);
    }

    private DeliveryModeModel getDeliveryModeModelForClickAndCollect() {
        return getDeliveryModeModel(clickAndCollectCode);
    }

    private DeliveryModeModel getDeliveryModeModelForExpressDelivery() {
        return getDeliveryModeModel(expressDeliveryCode);
    }

    private DeliveryModeModel getDeliveryModeModelForEbayExpressDelivery() {
        return getDeliveryModeModel(ebayExpressDeliveryCode);
    }

    private DeliveryModeModel getDeliveryModeModelForEbayHomeDelivery() {
        return getDeliveryModeModel(ebayHomeDeliveryCode);
    }

    private DeliveryModeModel getEbayClickAndCollectDeliveryCode() {
        return getDeliveryModeModel(ebayClickAndCollectDeliveryCode);
    }


    private DeliveryModeModel getDigitalDeliveryModeModel() {
        return getDeliveryModeModel(digitalGiftcardCode);
    }

    /**
     * @param product
     * @param dto
     */
    public void setupProductFeatures(final TargetProductModel product, final IntegrationProductDto dto) {
        final FeatureList features = classificationService.getFeatures(product);

        boolean hasUpdated = false;

        for (final Feature feature : features.getFeatures()) {
            final ClassificationAttributeModel attribute = feature.getClassAttributeAssignment()
                    .getClassificationAttribute();
            final String code = attribute.getCode();

            if (TargetProductImportConstants.FEATURE_LICENSE.equalsIgnoreCase(code)) {
                hasUpdated = true;
                setProductLicense(feature, dto.getLicense());
            }
            else if (TargetProductImportConstants.FEATURE_GENDER.equalsIgnoreCase(code)) {
                hasUpdated = true;
                setProductGender(feature, attribute, dto.getGenders());
            }
            else if (TargetProductImportConstants.FEATURE_AGE_RANGE.equalsIgnoreCase(code)) {
                hasUpdated = true;
                setProductAgeRange(feature, dto.getAgeFrom(), dto.getAgeTo());
            }
        }
        if (hasUpdated) {
            classificationService.replaceFeatures(product, features);
        }
    }

    /**
     * @param product
     * @param dto
     */
    private void setPurchaseOptionValues(final TargetProductModel product, final IntegrationProductDto dto) {
        final Set<PurchaseOptionModel> purchaseOptions = new HashSet<>();
        purchaseOptions.add(targetPurchaseOptionHelper.getCurrentPurchaseOptionModel());
        product.setPurchaseOptions(purchaseOptions);
    }

    /**
     * 
     * @param baseProduct
     * @param dto
     * @return TargetColourVariantProductModel populated with data from dto
     * @throws InstantiationException
     */
    private TargetColourVariantProductModel createTargetColourVariant(final TargetProductModel baseProduct,
            final IntegrationProductDto dto, final ProductMediaObjects mediaObjects,
            final IntegrationResponseDto response) throws InstantiationException {

        final boolean isStylegroup = dto.getIsStylegroup().booleanValue();
        final boolean isAssortment = dto.getIsAssortment().booleanValue();
        final ColourModel variantSwatch = getVariantSwatch(dto.getSwatch());
        final String colourVariantCode = getColourVariantCode(dto, variantSwatch);

        TargetColourVariantProductModel variant = null;

        if (targetFeatureSwitchService
                .isFeatureEnabled(TargetProductImportConstants.FEATURE_MOVE_BETWEEN_STYLEGROUPS)) {
            variant = (TargetColourVariantProductModel)getExistingProduct(colourVariantCode);
            if (isStylegroup && !isAssortment) {
                // if this product is part of a stylegroup
                // check if there is an existing colour variant with the same base product/swatch colour combination
                final TargetColourVariantProductModel existingVariant = getExistingColourVariant(dto.getProductCode(),
                        variantSwatch.getCode());
                if (existingVariant != null) {
                    if (variant != null && !variant.getCode().equals(existingVariant.getCode())
                            && !dto.getProductCode().equals(variant.getBaseProduct().getCode())) {
                        modelService.remove(variant);
                    }
                    variant = existingVariant;
                }
            }
        }
        else {
            if (isStylegroup && !isAssortment) {
                // if this product is part of a stylegroup
                // check if there is an existing colour variant with the same base product/swatch colour combination
                variant = getExistingColourVariant(dto.getProductCode(), variantSwatch.getCode());

            }
            else {
                variant = (TargetColourVariantProductModel)getExistingProduct(colourVariantCode);
            }
        }
        boolean isNewVariant = false;
        if (variant == null) {
            variant = modelService.create(TargetColourVariantProductModel.class);
            isNewVariant = true;

            variant.setVariantType(variantService
                    .getVariantTypeForCode(TargetProductImportConstants.TARGET_SIZE_VARIANT_PRODUCT_TYPE));
            variant.setCatalogVersion(catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                    TgtCoreConstants.Catalog.OFFLINE_VERSION));
            variant.setCode(colourVariantCode);
        }

        variant.setBaseProduct(baseProduct);
        variant.setName(StringUtils.isNotBlank(dto.getName()) ? dto.getName() : StringUtils.EMPTY);
        variant.setColour(getVariantColour(dto.getColour()));
        variant.setSwatch(variantSwatch);
        variant.setMaxOrderQuantity(dto.getMaxOrderQty() != null ? dto.getMaxOrderQty() : Integer.valueOf(99));
        variant.setDepartment(dto.getDepartment());
        variant.setApprovalStatus(getApprovalStatus(variant, dto.getApprovalStatus(), isNewVariant, variant.getCode(),
                response, variant));

        //update exclude payment flag
        updateExcludePaymentMethods(variant, dto.getExcludePaymentMethods());

        if (BooleanUtils.isFalse(dto.getIsSizeOnly())) {
            final String ean = dto.getEan();
            final String cleansedEan = BarcodeTools.getCleansedEan(ean);
            if (BarcodeTools.isValidEan(cleansedEan)) {
                variant.setEan(cleansedEan);
            }
            else {
                variant.setEan(StringUtils.EMPTY);
                final String validationMessage = getMessageforInvalidEan(dto.getProductCode(), dto.getName(),
                        ean);
                response.addMessage(validationMessage);
                LOG.warn("WARN-EAN-VALIDATION-FAILURE:" + validationMessage);
            }
        }

        setArticleStatus(variant, dto.getArticleStatus());
        if (mediaObjects != null) {
            setProductMedia(variant, mediaObjects);
        }
        //associate sizegroup to the color variant
        if (targetFeatureSwitchFacade.isSizeOrderingEnabled() && StringUtils.isNotEmpty(dto.getSizeGroup())) {
            associateSizeGroup(variant, dto);
        }
        updateVariantDateRange(dto, variant);

        updateGiftCardStyle(dto, variant);

        variant.setAssorted(targetProductImportUtil.getBooleanValue(dto.getAssorted()).booleanValue());

        //Tech Debt - Need to remove this method once OCR-18597
        excludeForAfterpayForSpecificProductTypeAndBrand(dto, variant);
        populatePreOrderAttributes(variant, dto);

        modelService.save(variant);

        final Boolean displayOnly = null != dto.getDisplayOnly() ? targetProductImportUtil
                .getBooleanValue(dto.getDisplayOnly()) : Boolean.FALSE;
        if (isColourVariantSellable(dto)) {
            variant.setDisplayOnly(displayOnly);
            if (StringUtils.isNotEmpty(dto.getPreOrderEndDate())) {
                variant.setMaxPreOrderQuantity(dto.getPreOrderOnlineQuantity());
            }
            else {
                variant.setMaxPreOrderQuantity(null);
            }
            modelService.save(variant);
            // create stock level only if:
            // - this is not size only product 
            //- this is not a display Only product
            // - if there are no size variations
            createOrCheckStockLevel(dto, variant, isNewVariant, displayOnly);
        }
        LOG.info(MessageFormat.format(
                isNewVariant ? TargetProductImportConstants.LogMessages.INFO_NEW_COLOUR_VARIATION
                        : TargetProductImportConstants.LogMessages.INFO_EXISTING_COLOUR_VARIATION,
                dto.getProductCode(), dto.getName(),
                dto.getVariantCode()));

        return variant;
    }

    /*
     * updateExcludePaymentMethods based on the request
     */
    public void updateExcludePaymentMethods(final TargetColourVariantProductModel product,
            final List<String> excludePaymentMethods) {

        boolean isExcludeForZipPayment = false;
        if (CollectionUtils.isNotEmpty(excludePaymentMethods)) {
            isExcludeForZipPayment = excludePaymentMethods.contains(ZIP_PAYMENT_CODE);
        }

        if (product.isExcludeForZipPayment() != isExcludeForZipPayment) {
            product.setExcludeForZipPayment(isExcludeForZipPayment);
        }
    }

    /**
     * @param dto
     * @param variant
     */
    private void excludeForAfterpayForSpecificProductTypeAndBrand(final IntegrationProductDto dto,
            final TargetColourVariantProductModel variant) {
        final BrandModel brandModel = variant.getBrand();
        final String brandCode = (null != brandModel) ? brandModel.getCode() : StringUtils.EMPTY;
        if (isProductTypeDigital(dto)
                || (TargetProductImportConstants.BRAND_CODE_APPLE.equalsIgnoreCase(brandCode))
                || targetProductImportUtil.isProductPhysicalGiftcard(dto)) {
            variant.setExcludeForAfterpay(true);
        }
    }

    /**
     * Method to check if colour variant is a sellable variant
     * 
     * @param dto
     * @return true - if colour variant is sellable
     */
    private boolean isColourVariantSellable(final IntegrationProductDto dto) {
        return !dto.getIsSizeOnly().booleanValue() && CollectionUtils.isEmpty(dto.getVariants());
    }

    /**
     * Set gift card style into color variant
     * 
     * @param dto
     * @param variant
     */
    private void updateGiftCardStyle(final IntegrationProductDto dto, final TargetColourVariantProductModel variant) {
        variant.setGiftCardStyle(dto.getGiftcardStyle());
    }

    protected String getMessageforInvalidEan(final String productCode, final String productName, final String ean) {
        return MessageFormat.format(TargetProductImportConstants.LogMessages.EAN_VALIDATION_FAILURE, productCode,
                productName, ean);
    }

    /**
     * 
     * @param colourVariant
     * @param dto
     * @param sizeVar
     * @return TargetSizeVariantProductModel populated with data from dto
     * @throws InstantiationException
     */
    private TargetSizeVariantProductModel createSizeVariant(final TargetColourVariantProductModel colourVariant,
            final IntegrationProductDto dto, final IntegrationVariantDto sizeVar, final IntegrationResponseDto response)
            throws InstantiationException {
        if (CollectionUtils.isNotEmpty(colourVariant.getEurope1Prices())) {
            logInvalidPrices(TargetProductImportConstants.LogMessages.INFO_ERR_SV_CREATE_UPDATE, getSizeVariantCode(
                    dto, sizeVar), colourVariant.getCode());
            return null;
        }
        TargetSizeVariantProductModel variant = (TargetSizeVariantProductModel)getExistingProduct(getSizeVariantCode(
                dto, sizeVar));
        boolean isNewVariant = false;
        final boolean isSizeOnly = dto.getIsSizeOnly().booleanValue();

        if (variant == null) {
            variant = modelService.create(TargetSizeVariantProductModel.class);
            isNewVariant = true;

            variant.setCatalogVersion(catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                    TgtCoreConstants.Catalog.OFFLINE_VERSION));
            variant.setCode(sizeVar != null ? sizeVar.getVariantCode() : dto.getVariantCode());
        }

        variant.setBaseProduct(colourVariant);

        //setup AvailableOnEbay flag on the variant
        if (StringUtils.isNotBlank(dto.getAvailableOnEbay())) {
            variant.setAvailableOnEbay(targetProductImportUtil.getBooleanValue(dto.getAvailableOnEbay()));
        }

        String ean = StringUtils.EMPTY;
        if (isSizeOnly) {
            variant.setName(dto.getName());
            ean = dto.getEan();
        }
        else {
            if (sizeVar == null) {
                variant.setName(dto.getName());
                ean = dto.getEan();
            }
            else {
                variant.setName(StringUtils.isNotBlank(sizeVar.getName()) ? sizeVar.getName() : dto.getName());
                ean = StringUtils.isNotBlank(sizeVar.getEan()) ? sizeVar.getEan() : dto.getEan();
            }
        }

        final String cleansedEan = BarcodeTools.getCleansedEan(ean);
        if (BarcodeTools.isValidEan(cleansedEan)) {
            variant.setEan(cleansedEan);
        }
        else {
            variant.setEan(StringUtils.EMPTY);
            final String validationMessage = getMessageforInvalidEan(variant.getCode(), dto.getName(),
                    ean);
            response.addMessage(validationMessage);
            LOG.warn("WARN-EAN-VALIDATION-FAILURE:" + validationMessage);
        }

        variant.setApprovalStatus(getApprovalStatus(variant, getSizeVariantStatus(dto, sizeVar), isNewVariant,
                variant.getCode(), response, null));

        if (dto.getSizeType() != null) {
            variant.setSizeType(dto.getSizeType());
            final SizeTypeModel sizeClassification = sizeTypeService.findSizeTypeForCode(dto.getSizeType());
            if (null != sizeClassification) {
                variant.setSizeClassification(sizeClassification);
            }
        }

        final String size = getVariantSize(dto, sizeVar);
        if (size != null) {
            variant.setSize(size);
        }

        if (dto.getDepartment() != null) {
            variant.setDepartment(dto.getDepartment());
        }

        updateVariantDateRange(dto, variant);

        final TargetProductDimensionsModel targetProductPkgDimModel;
        if (sizeVar != null) {
            targetProductPkgDimModel = populateProductPackageDimensions(null, sizeVar,
                    dto.getAvailableDigitalDelivery());
        }
        else {
            targetProductPkgDimModel = populateProductPackageDimensions(dto, null, dto.getAvailableDigitalDelivery());
        }
        if (null != targetProductPkgDimModel) {
            variant.setProductPackageDimensions(targetProductPkgDimModel);
        }

        populateMerchProductStatus(dto, sizeVar, variant);

        createGiftCardDenomination(variant, dto);
        //associate the product size with the size variant
        if (null != colourVariant.getSizeGroup() && targetFeatureSwitchFacade.isSizeOrderingEnabled()) {
            associateProductSize(variant, colourVariant.getSizeGroup(), size);

        }

        //display Only flag from the hierrachy color variant level,if the product is sizeonly,
        //or the display only flag not set at the sizevariant level,then the flag is populated from the colorvariant level

        Boolean displayOnly = null != dto.getDisplayOnly() ? targetProductImportUtil
                .getBooleanValue(dto.getDisplayOnly()) : Boolean.FALSE;

        //if itsnot a sizeonly and is just sizeonly prd with displayonly flag set at the size varaiant level in step
        if (BooleanUtils.isFalse(dto.getIsSizeOnly()) && null != sizeVar
                && StringUtils.isNotEmpty(sizeVar.getDisplayOnly())) {
            displayOnly = null != sizeVar.getDisplayOnly() ? targetProductImportUtil
                    .getBooleanValue(sizeVar.getDisplayOnly()) : Boolean.FALSE;
        }
        variant.setDisplayOnly(displayOnly);
        if (StringUtils.isNotEmpty(dto.getPreOrderEndDate())) {
            variant.setMaxPreOrderQuantity(dto.getMaxOrderQty());
        }
        else {
            variant.setMaxPreOrderQuantity(null);
        }
        modelService.save(variant);

        createOrCheckStockLevel(dto, variant, isNewVariant, displayOnly);

        LOG.info(MessageFormat.format(
                isNewVariant ? TargetProductImportConstants.LogMessages.INFO_NEW_SIZE_VARIATION
                        : TargetProductImportConstants.LogMessages.INFO_EXISTING_SIZE_VARIATION,
                dto.getProductCode(), dto.getName(),
                isSizeOnly ? dto.getSize() : sizeVar.getSize(),
                isSizeOnly ? dto.getVariantCode() : sizeVar.getVariantCode()));

        sendFluentVariantFeed(variant);

        return variant;
    }

    /**
     * @param dto
     * @param variant
     * @param isNewVariant
     * @param displayOnly
     */
    private void createOrCheckStockLevel(final IntegrationProductDto dto,
            final AbstractTargetVariantProductModel variant,
            final boolean isNewVariant, final Boolean displayOnly) {

        final StockLevelModel consolidatedWarehouseStock = getConsolidatedWarehouseStockLevelForVariant(variant);
        if (BooleanUtils.isFalse(displayOnly)) {
            final Collection<StockLevelModel> stockLevels = stockService.getAllStockLevels(variant);
            if (isNewVariant || CollectionUtils.isEmpty(stockLevels)) {
                //create stock level only if the prd is not display only and if it is a new prd
                //or existing prd display only with no stock levels and now changed to non display only
                createEmptyStockLevel(variant, dto);
            }
            else {
                if (null != consolidatedWarehouseStock) {
                    updateConsolidatedWarehouseStockLevel(consolidatedWarehouseStock, InStockStatus.NOTSPECIFIED);
                }
                checkStockLevelIsAssignedToCorrectWarehouse(variant, dto);
            }
            populateMaxPreOrderQuantity(dto, variant);
        }
        else {
            if (null != consolidatedWarehouseStock) {
                updateConsolidatedWarehouseStockLevel(consolidatedWarehouseStock, InStockStatus.FORCEOUTOFSTOCK);
            }
        }
    }


    private void updateConsolidatedWarehouseStockLevel(final StockLevelModel consolidatedWarehouseStockLevel,
            final InStockStatus stockStatus) {
        consolidatedWarehouseStockLevel.setInStockStatus(stockStatus);
        modelService.save(consolidatedWarehouseStockLevel);
        modelService.refresh(consolidatedWarehouseStockLevel);
    }


    private StockLevelModel getConsolidatedWarehouseStockLevelForVariant(final AbstractTargetVariantProductModel variant) {
        StockLevelModel consolidatedWarehouseStock = null;
        try {
            final WarehouseModel consolidatedWarehouse = warehouseService
                    .getWarehouseForCode(TgtCoreConstants.CONSOLIDATED_STORE_WAREHOUSE);
            consolidatedWarehouseStock = stockService.checkAndGetStockLevel(variant,
                    consolidatedWarehouse);
        }
        catch (final StockLevelNotFoundException ex) {
            LOG.info("Consolidated Warehouse StockLevel Not found for Variant:" + variant.getCode());
        }
        return consolidatedWarehouseStock;
    }

    /**
     * 
     * @param dto
     * @param variant
     */
    private void populateMaxPreOrderQuantity(final IntegrationProductDto dto,
            final AbstractTargetVariantProductModel variant) {
        final Integer onlineQuantity = dto.getPreOrderOnlineQuantity();
        try {
            final StockLevelModel stockLevelModel = stockService.checkAndGetStockLevel(variant,
                    warehouseService.getDefaultOnlineWarehouse());
            if (stockLevelModel != null && dto.getPreOrderEndDate() != null && onlineQuantity != null) {
                stockLevelModel.setMaxPreOrder(onlineQuantity.intValue());
                modelService.save(stockLevelModel);
            }
        }
        catch (final Exception e) {
            LOG.info(e.getMessage());
        }
    }



    /**
     * Method to check if stock level is assigned to the expected warehouse
     * 
     * @param variant
     * @param dto
     */
    protected void checkStockLevelIsAssignedToCorrectWarehouse(final AbstractTargetVariantProductModel variant,
            final IntegrationProductDto dto) {
        final Collection<StockLevelModel> stockLevels = stockService.getAllStockLevels(variant);

        String expectedWarehouseCode = null;


        if (CollectionUtils.isEmpty(stockLevels)) {
            return;
        }
        else if (isProductTypeDigital(dto) || targetProductImportUtil.isProductPhysicalGiftcard(dto)) {
            expectedWarehouseCode = targetFulfilmentWarehouseService.getGiftcardWarehouse().getCode();
        }
        else {
            expectedWarehouseCode = (warehouseInfoExists(dto.getWarehouses()))
                    ? dto.getWarehouses().get(0)
                    : warehouseService.getDefaultOnlineWarehouse().getCode();
        }

        final StockLevelModel stockLevel = stockLevels.iterator().next();
        final String actualWarehouseCode = stockLevel.getWarehouse().getCode();

        if (!StringUtils.equals(actualWarehouseCode, expectedWarehouseCode)) {
            logWarehouseMismatch(variant.getCode(), dto.getProductCode(), actualWarehouseCode,
                    expectedWarehouseCode);
        }
    }

    protected void logWarehouseMismatch(final String variantCode, final String baseProductCode,
            final String actualWarehouseCode, final String expectedWarehouseCode) {
        LOG.info(MessageFormat.format(TargetProductImportConstants.LogMessages.INFO_WAREHOUSE_MISMATCH,
                variantCode, baseProductCode, actualWarehouseCode, expectedWarehouseCode));
    }

    /**
     * set gift card denomination
     * 
     * @param variant
     * @param dto
     */
    private void createGiftCardDenomination(final TargetSizeVariantProductModel variant,
            final IntegrationProductDto dto) {
        variant.setDenomination(dto.getDenominaton());
        variant.setGiftCardProductId(dto.getGiftcardProductId());
    }

    /**
     * @param log
     * @param svCode
     * @param cvCode
     */
    protected void logInvalidPrices(final String log,
            final String svCode,
            final String cvCode) {
        LOG.info(MessageFormat.format(log, svCode, cvCode));
    }

    /**
     * 
     * @param brandName
     * @return BrandModel with specified name
     */
    private BrandModel getProductBrand(final String brandName) {
        return brandService.getBrandForFuzzyName(brandName, true);
    }

    /**
     * 
     * @param colour
     * @return ColourModel based on specified colour name
     */
    private ColourModel getVariantColour(final String colour) {
        String colourName = colour;
        boolean display = true;
        if (StringUtils.isBlank(colourName)) {
            colourName = TargetProductImportConstants.NO_COLOUR;
            display = false;
        }

        return colourService.getColourForFuzzyName(colourName, true, display);
    }

    /**
     * 
     * @param swatch
     * @return ColourModel based on specified swatch name
     */
    private ColourModel getVariantSwatch(final String swatch) {
        String swatchName = swatch;
        boolean display = true;
        if (StringUtils.isBlank(swatchName)) {
            swatchName = TargetProductImportConstants.NO_COLOUR;
            display = false;
        }

        return colourService.getColourForFuzzyName(swatchName, true, display);
    }

    /**
     * 
     * @param dto
     * @return ProductTypeModel
     */
    private ProductTypeModel getProductType(final IntegrationProductDto dto) {

        String productType = dto.getProductType();
        if (targetProductImportUtil.isProductPhysicalGiftcard(dto)) {
            productType = TargetProductImportConstants.PRODUCT_TYPE_PHYSICAL_GIFTCARD;
        }
        return productTypeService.getByCode(productType);
    }


    /**
     * 
     * @param product
     * @param status
     * @param isNew
     * @return ArticleApprovalStatus
     * @throws InstantiationException
     */
    private ArticleApprovalStatus getApprovalStatus(
            final ProductModel product,
            final String status,
            final boolean isNew,
            final String productCode,
            final IntegrationResponseDto response,
            final TargetColourVariantProductModel colourVar)
            throws InstantiationException {
        // if approvalStatus is 'NOT SOURCED' do not import product
        // to-do: what happens when it's Variant - check roll back rules      
        if (status.equalsIgnoreCase(TargetProductImportConstants.NOT_SOURCED)) {
            if (isNew) {
                throw new InstantiationException(
                        "'NOT SOURCED' product/variant status found, aborting product/variant import for: "
                                + product.getCode());
            }
        }

        if (productCode.contains(TargetProductImportConstants.VARIANT_CODE_SEPERATOR)) {
            // fake colour variant
            getFakeColourVariantArticleApprovalStatus(status, isNew, productCode, response, colourVar);
        }

        return getArticleApprovalStatus(status, productCode, response);
    }

    private ArticleApprovalStatus getFakeColourVariantArticleApprovalStatus(
            final String status,
            final boolean isNew,
            final String productCode,
            final IntegrationResponseDto response,
            final TargetColourVariantProductModel colourVar) {

        if (isNew) {
            // for new colour variant just use the value supplied
            return getArticleApprovalStatus(status, productCode, response);
        }
        else {
            // existing fake colour variant - must check all other size variants
            final Collection<VariantProductModel> variants = colourVar.getVariants();
            if (variants != null && CollectionUtils.isNotEmpty(variants)) {
                // if at least one size variant is approved, colour variant has to be approved as well
                for (final VariantProductModel var : variants) {
                    if (var.getApprovalStatus() == ArticleApprovalStatus.APPROVED) {
                        return ArticleApprovalStatus.APPROVED;
                    }
                }
                return ArticleApprovalStatus.UNAPPROVED;
            }
            else {
                // there are no size variants yet, just use the supplied value
                return getArticleApprovalStatus(status, productCode, response);
            }
        }
    }

    private ArticleApprovalStatus getArticleApprovalStatus(
            final String status,
            final String productCode,
            final IntegrationResponseDto response) {

        if (status.equalsIgnoreCase(TargetProductImportConstants.PRODUCT_ACTIVE)) {
            return ArticleApprovalStatus.APPROVED;
        }
        else if (status.equalsIgnoreCase(TargetProductImportConstants.PRODUCT_INACTIVE)) {
            return ArticleApprovalStatus.UNAPPROVED;
        }
        else {
            response.addMessage(productCode + " - ApprovalStatus was changed to CHECK status.");
            return ArticleApprovalStatus.CHECK;
        }
    }

    /**
     * Set product categories
     * 
     * @param product
     * @param dto
     */
    private void setProductCategories(final TargetProductModel product, final IntegrationProductDto dto) {

        final Collection<CategoryModel> categories = new ArrayList<>();

        final String primaryCategory = dto.getPrimaryCategory();
        if (primaryCategory != null) {
            //to-do: implement roll-back strategy
            final CategoryModel primaryCategoryModel = getCategoryForCode(dto.getPrimaryCategory().trim());
            if (primaryCategoryModel instanceof TargetProductCategoryModel) {
                categories.add(primaryCategoryModel);

                product.setPrimarySuperCategory((TargetProductCategoryModel)primaryCategoryModel);
            }
        }

        final List<String> secondaryCatList = dto.getSecondaryCategory();
        if (CollectionUtils.isNotEmpty(secondaryCatList)) {
            for (final String secondaryCategory : secondaryCatList) {
                if (secondaryCategory != null) {
                    //to-do: implement roll-back strategy
                    final CategoryModel secondaryCategoryModel = getCategoryForCode(secondaryCategory.trim());
                    if (secondaryCategoryModel != null) {
                        categories.add(secondaryCategoryModel);
                    }
                }
            }
        }
        if (!CollectionUtils.isEmpty(categories)) {
            product.setSupercategories(categories);
        }
    }

    /**
     * 
     * @param categoryCode
     * @return CategoryModel based on specified category code
     */
    private CategoryModel getCategoryForCode(final String categoryCode) {
        CategoryModel category = null;
        try {
            category = categoryService.getCategoryForCode(categoryCode);
        }
        catch (final Exception e) {
            LOG.info(e.getMessage());
        }
        return category;
    }

    /**
     * Add youtube video link to product videos collection
     * 
     * @param product
     * @param videoUrl
     */
    private void addProductYoutubeVideo(final TargetProductModel product, final String videoUrl) {
        final Collection<MediaModel> videos = new ArrayList<>();
        if (videoUrl != null) {
            final String videoID = YouTubeUrlUtils.getVideoId(videoUrl);
            final String embedUrl = YouTubeUrlUtils.buildYoutubeEmbeddedUrl(videoUrl);
            if (StringUtils.isNotBlank(videoID) && StringUtils.isNotBlank(embedUrl)) {
                final String code = TargetProductImportConstants.VIDEO + videoID;
                MediaModel video = null;
                try {
                    video = mediaService.getMedia(code);
                }
                catch (final UnknownIdentifierException ex) {
                    video = createNewMedia(code, embedUrl);
                }

                video.setOwner(product);
                videos.add(video);
                product.setVideos(videos);
            }
        }
        else {
            product.setVideos(videos);
        }
    }

    /**
     * 
     * @param code
     * @return video
     */
    protected MediaModel createNewMedia(final String code, final String url) {
        final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION);
        MediaModel video = modelService.create(MediaModel.class);
        video.setCode(code);
        video.setCatalogVersion(catalogVersionModel);
        video.setURL(url);
        try {
            modelService.save(video);
        }
        catch (final ModelSavingException mse) {
            // Hybris issue SUP-9353 - Duplicate Media are getting created
            // we will only re-try to create product 1 more time
            if (mse.getMessage().toUpperCase().contains(TargetProductImportConstants.UNIQUE)) {
                video = mediaService.getMedia(code);
            }
            else {
                throw mse;
            }
        }

        return video;
    }



    /**
     * 
     * @param dto
     * @param variantSwatch
     * @return Colour variant code, while accounting for size only products
     */
    private String getColourVariantCode(final IntegrationProductDto dto, final ColourModel variantSwatch) {
        // special handling is required for products that contain size variant only 
        // eg QUILTS as in that instance we do not have true color variant code available in STEP
        if (dto.getIsSizeOnly() == null) {
            return dto.getVariantCode();
        }
        else {
            if (dto.getIsSizeOnly().booleanValue()) {
                // if this is SIZE only product, we will use the ProductCode plus the swatch color code 
                // as the Hybris code for the made-up color variant
                //this VARIANT_CODE_SEPERATOR is used to determine if this variant is fake when determining the approval status in getApprovalStatus()
                return dto.getProductCode().substring(1) + TargetProductImportConstants.VARIANT_CODE_SEPERATOR
                        + variantSwatch.getCode();
            }
            else {
                return dto.getVariantCode();
            }
        }
    }

    /**
     * 
     * @param dto
     * @param sizeVar
     * @return Size variant code, while accounting for size only products
     */
    private String getSizeVariantCode(final IntegrationProductDto dto, final IntegrationVariantDto sizeVar) {
        if (sizeVar != null) {
            return sizeVar.getVariantCode();
        }
        else {
            return dto.getVariantCode();
        }
    }

    /**
     * 
     * @param dto
     * @param sizeVar
     * @return Variant approval value
     */
    private String getSizeVariantStatus(final IntegrationProductDto dto, final IntegrationVariantDto sizeVar) {
        if (sizeVar != null) {
            return sizeVar.getApprovalStatus();
        }
        else {
            return dto.getApprovalStatus();
        }
    }

    /**
     * Set product license as classification attribute of product
     * 
     * @param feature
     * @param license
     */
    private void setProductLicense(final Feature feature, final String license) {
        final List<FeatureValue> featureValues = new ArrayList<>();
        FeatureValue featureValue = feature.getValue();
        if (StringUtils.isNotBlank(license)) {
            if (featureValue == null) {
                featureValue = new FeatureValue(license);
            }
            featureValue.setValue(license);
            featureValues.add(featureValue);
        }
        feature.setValues(featureValues);
    }

    /**
     * Set genders as classification attributes of product
     * 
     * @param feature
     * @param attribute
     * @param genders
     */
    private void setProductGender(final Feature feature, final ClassificationAttributeModel attribute,
            final List<String> genders) {
        final List<FeatureValue> values = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(genders)) {
            final ClassificationSystemVersionModel systemVersion = attribute.getSystemVersion();
            for (final String gender : genders) {
                try {
                    final ClassificationAttributeValueModel attributeValue = classificationSystemService
                            .getAttributeValueForCode(systemVersion, gender.toLowerCase());
                    final FeatureValue value = new FeatureValue(attributeValue);
                    value.setDescription(gender);
                    values.add(value);
                }
                catch (final UnknownIdentifierException e) {
                    LOG.warn("Enable to find value for product gender attribute: " + gender.toLowerCase());
                }
            }
        }
        feature.setValues(values);
    }

    /**
     * Set age from and age to as a product feature
     * 
     * @param feature
     * @param ageFrom
     * @param ageTo
     */
    private void setProductAgeRange(final Feature feature, final Double ageFrom, final Double ageTo) {
        final List<FeatureValue> values = new ArrayList<>();

        if (ageFrom != null && ageTo != null) {
            if (ageFrom.doubleValue() >= 0 && ageTo.doubleValue() >= 0) {
                if (ageFrom.doubleValue() < ageTo.doubleValue()) {
                    final FeatureValue fromAge = new FeatureValue(ageFrom);
                    values.add(fromAge);
                    final FeatureValue toAge = new FeatureValue(ageTo);
                    values.add(toAge);
                }
                else {
                    LOG.warn(MessageFormat.format("From value {0} for age range cannot be larger than to value {1}.",
                            ageFrom, ageTo));
                }
            }
            else {
                LOG.warn("Both from and to values for age range should be positive");
            }
        }
        else if (ageFrom != null) {
            if (ageFrom.doubleValue() >= 0) {
                final FeatureValue fromAge = new FeatureValue(ageFrom);
                values.add(fromAge);
            }
            else {
                LOG.warn(MessageFormat.format("From value {0} for age range should be positve.", ageFrom));
            }
        }
        else if (ageTo != null) {
            if (ageTo.doubleValue() >= 0) {
                final FeatureValue fromAge = new FeatureValue(Double.valueOf(0d));
                values.add(fromAge);

                final FeatureValue toAge = new FeatureValue(ageTo);
                values.add(toAge);
            }
            else {
                LOG.warn(MessageFormat.format("To value {0} for age range should be positve.", ageTo));
            }
        }

        feature.setValues(values);
    }

    /**
     * 
     * @param dto
     * @param sizeVar
     * @return Correct size for selected variant
     */
    private String getVariantSize(final IntegrationProductDto dto, final IntegrationVariantDto sizeVar) {
        if (sizeVar != null) {
            if (sizeVar.getSize() != null) {
                return sizeVar.getSize();
            }
        }
        else {
            if (dto.getSize() != null) {
                return dto.getSize();
            }
        }

        return null;
    }

    /**
     * Set correct article status for variant
     * 
     * @param variant
     * @param articleStatus
     */
    private void setArticleStatus(final TargetColourVariantProductModel variant, final String articleStatus) {
        //set to defaults
        variant.setEssential(Boolean.FALSE);
        variant.setTargetExclusive(Boolean.FALSE);
        variant.setHotProduct(Boolean.FALSE);
        variant.setClearance(Boolean.FALSE);
        variant.setOnlineExclusive(Boolean.FALSE);

        if (StringUtils.isNotBlank(articleStatus)) {
            if (articleStatus.equalsIgnoreCase(TargetProductImportConstants.ARTICLE_STATUS_ESSENTIALS)) {
                variant.setEssential(Boolean.TRUE);
                return;
            }

            if (articleStatus.equalsIgnoreCase(TargetProductImportConstants.ARTICLE_STATUS_TARGETEXCLUSIVE)) {
                variant.setTargetExclusive(Boolean.TRUE);
                return;
            }

            if (articleStatus.equalsIgnoreCase(TargetProductImportConstants.ARTICLE_STATUS_HOTPRODUCT)) {
                variant.setHotProduct(Boolean.TRUE);
                return;
            }

            if (articleStatus.equalsIgnoreCase(TargetProductImportConstants.ARTICLE_STATUS_CLEARANCE)) {
                variant.setClearance(Boolean.TRUE);
                return;
            }

            if (articleStatus.equalsIgnoreCase(TargetProductImportConstants.ARTICLE_STATUS_ONLINEEXCLUSIVE)) {
                variant.setOnlineExclusive(Boolean.TRUE);
                return;
            }
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.productimport.ProductImportIntegrationFacade#generateProductCrossReference(au.com.target.tgtwsfacades.integration.dto.IntegrationProductReferenceDto)
     */
    @Override
    public IntegrationResponseDto generateProductCrossReference(final IntegrationProductReferenceDto product) {

        final IntegrationResponseDto response = new IntegrationResponseDto(null);
        response.setSuccessStatus(true);

        final String ownerCode = product.getCode();
        final IntegrationCrossReferenceProduct crossRef = product.getRefProducts();

        if (StringUtils.isBlank(ownerCode) || crossRef == null) {
            response.setSuccessStatus(false);
            response.addMessage("No owner product code or no cross-reference products specified.");
            return response;
        }

        final ProductModel owner = getExistingProduct(ownerCode);
        if (owner != null) {
            final Collection<ProductReferenceModel> refCollection = new ArrayList<>();

            for (final String refCrossProduct : crossRef.getRefProduct()) {

                if (StringUtils.isBlank(refCrossProduct)) {
                    continue;
                }

                final ProductModel refProduct = getExistingProduct(refCrossProduct);
                if (refProduct != null) {

                    final ProductReferenceModel reference = modelService
                            .create(ProductReferenceModel.class);

                    reference.setSource(owner);
                    reference.setTarget(refProduct);
                    reference.setReferenceType(ProductReferenceTypeEnum.UPSELLING);
                    reference.setActive(Boolean.TRUE);
                    reference.setPreselected(Boolean.FALSE);

                    refCollection.add(reference);

                }
                else {
                    response.setSuccessStatus(false);
                    final String msg = "Referenced product does not exist, product code: "
                            + refCrossProduct
                            + " for owner with product code: " + ownerCode;
                    LOG.warn("WARN-PRODUCTIMPORT-CROSSREF : " + msg);
                    response.addMessage(msg);
                }
            }

            if (CollectionUtils.isNotEmpty(refCollection)) {
                owner.setProductReferences(refCollection);
                modelService.save(owner);
            }
        }
        else {
            response.setSuccessStatus(false);
            final String msg = "Owner product does not exist, product code: " + ownerCode;
            LOG.warn("WARN-PRODUCTIMPORT-CROSSREF : " + msg);
            response.addMessage(msg);
        }

        return response;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.productimport.ProductImportIntegrationFacade#generateProductCrossReferences(au.com.target.tgtfacades.integration.dto.IntegrationProductReferencesDto)
     */
    @Override
    public IntegrationResponseDto generateProductCrossReferences(final IntegrationProductReferencesDto dto) {

        final IntegrationResponseDto response = new IntegrationResponseDto(null);
        response.setSuccessStatus(true);

        try {
            if (dto != null) {
                if (!CollectionUtils.isEmpty(dto.getStepProducts())) {
                    for (final IntegrationProductReferenceDto product : dto.getStepProducts()) {
                        final IntegrationResponseDto tmp = generateProductCrossReference(product);
                        if (!tmp.isSuccessStatus()) {
                            response.setSuccessStatus(false);
                            response.addMessages(tmp.getMessages());
                        }
                    }
                }
            }
        }
        catch (final Exception e) {
            LOG.error("Unable to create/update product associations.", e);
            response.setSuccessStatus(false);
            final String errMsg = e.getMessage();

            response.addMessage(errMsg == null ? "Runtime exception occured during product association." : errMsg);
        }

        return response;
    }



    /**
     * @return the associateMedia
     */
    public boolean isAssociateMedia() {
        return associateMedia;
    }

    /**
     * @param associateMedia
     *            the associateMedia to set
     */
    public void setAssociateMedia(final boolean associateMedia) {
        this.associateMedia = associateMedia;
    }

    private void createEmptyStockLevel(final ProductModel product, final IntegrationProductDto dto) {
        if (isProductTypeDigital(dto) || targetProductImportUtil.isProductPhysicalGiftcard(dto)) {
            createGiftcardStockLevel(product);
        }
        else {
            createEmptyStockLevelForWarehouse(product, dto);
        }
    }

    private void createGiftcardStockLevel(final ProductModel product) {
        final WarehouseModel warehouse = targetFulfilmentWarehouseService.getGiftcardWarehouse();
        if (warehouse != null) {
            // digital product stock is same as physical giftcard product stock and hence re-using
            stockService.updateActualStockLevel(product, warehouse, digitalProductStock, StringUtils.EMPTY);
        }
    }

    private void createEmptyStockLevelForWarehouse(final ProductModel product, final IntegrationProductDto dto) {
        WarehouseModel warehouse = null;
        final List<String> warehouses = dto.getWarehouses();
        if (warehouseInfoExists(warehouses)) {
            warehouse = warehouseService.getWarehouseForCode(warehouses.get(0));
        }
        else {
            warehouse = warehouseService.getDefaultOnlineWarehouse();
        }
        if (warehouse != null) {
            stockService.updateActualStockLevel(product, warehouse, 0, StringUtils.EMPTY);
        }
        //added for fastline falcon
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FASTLINE_FALCON)) {
            createConsolidatedAndReservationStockLevel(product);
        }

    }

    /**
     * 
     * @param product
     */
    private void createConsolidatedAndReservationStockLevel(final ProductModel product) {
        final WarehouseModel consolidatedWarehouse = warehouseService
                .getWarehouseForCode(TgtCoreConstants.CONSOLIDATED_STORE_WAREHOUSE);
        final WarehouseModel reservationWarehouse = warehouseService
                .getWarehouseForCode(TgtCoreConstants.STOCK_RESERVATION_WAREHOUSE);
        if (null != consolidatedWarehouse && null != reservationWarehouse) {
            LOG.info(":: Creating consolidated store warehouse and Stock reservation warehouse ::");
            stockService.updateActualStockLevel(product, consolidatedWarehouse, 0, StringUtils.EMPTY);
            createStockRervationStockLevel(product, reservationWarehouse);

        }

    }

    /**
     * @param product
     * @param reservationWarehouse
     */
    private void createStockRervationStockLevel(final ProductModel product, final WarehouseModel reservationWarehouse) {
        final StockLevelModel reservationStockLevelModel = modelService.create(StockLevelModel.class);
        reservationStockLevelModel.setProductCode(product.getCode());
        reservationStockLevelModel.setInStockStatus(InStockStatus.NOTSPECIFIED);
        reservationStockLevelModel.setTreatNegativeAsZero(false);
        reservationStockLevelModel.setWarehouse(reservationWarehouse);
        modelService.saveAll(reservationStockLevelModel);
        modelService.refresh(reservationStockLevelModel);
    }

    /**
     * Method to check if warehouse exist
     * 
     * @param warehouses
     * @return true - if warehouse exist
     */
    private boolean warehouseInfoExists(final List<String> warehouses) {
        return warehouses != null && CollectionUtils.isNotEmpty(warehouses);
    }

    /**
     * @param dto
     * @param variant
     */
    private void updateVariantDateRange(final IntegrationProductDto dto, final VariantProductModel variant) {
        final String onDateString = dto.getProductOnlineDate();
        final String offDateString = dto.getProductOfflineDate();

        Date fromDate = null;
        Date toDate = null;
        try {
            if (onDateString != null) {
                fromDate = new SimpleDateFormat(DATETIME_FORMAT).parse(onDateString);
            }
            if (offDateString != null) {
                toDate = new SimpleDateFormat(DATETIME_FORMAT).parse(offDateString);
            }
        }
        catch (final Exception e) {
            LOG.info("Error converting FROM or TO dates.");
        }

        if (null != fromDate) {
            variant.setOnlineDate(fromDate);
        }

        if (null != toDate) {
            variant.setOfflineDate(toDate);
        }
    }

    /**
     * Associate the sizegroup to the colourvariant
     * 
     * @param variant
     * @param productDto
     * @throws InstantiationException
     */
    protected void associateSizeGroup(final TargetColourVariantProductModel variant,
            final IntegrationProductDto productDto) throws InstantiationException {
        final String sizeGroup = productDto.getSizeGroup();
        final TargetSizeGroupModel sizeGroupModel = targetSizeOrderingService.getTargetSizeGroupByName(sizeGroup,
                productDto.getProductCode(), true);
        if (null == sizeGroupModel) {
            throw new InstantiationException(
                    "SizeGroup [" + sizeGroup + "] for the variant not found/created, aborting import for variant : "
                            + productDto.getProductCode());
        }
        variant.setSizeGroup(sizeGroupModel);
    }

    /**
     * Associate Target Product Size to sizevariant
     * 
     * @param sizeVariant
     * @param sizeGroup
     * @param size
     * @throws InstantiationException
     */
    protected void associateProductSize(final TargetSizeVariantProductModel sizeVariant,
            final TargetSizeGroupModel sizeGroup, final String size) throws InstantiationException {
        final TargetProductSizeModel productSize = targetSizeOrderingService.getTargetProductSize(
                size, sizeGroup, true, sizeVariant.getCode());
        if (null == productSize) {
            throw new InstantiationException(
                    "TargetProductSize for the size variant not found/created, aborting  import for variant: "
                            + sizeVariant.getCode());
        }
        sizeVariant.setProductSize(productSize);
    }

    @Required
    public void setHomeDeliveryCode(final String homeDeliveryCode) {
        this.homeDeliveryCode = homeDeliveryCode;
    }

    @Required
    public void setClickAndCollectCode(final String clickAndCollectCode) {
        this.clickAndCollectCode = clickAndCollectCode;
    }

    @Required
    public void setExpressDeliveryCode(final String expressDeliveryCode) {
        this.expressDeliveryCode = expressDeliveryCode;
    }

    @Required
    public void setEbayExpressDeliveryCode(final String ebayExpressDeliveryCode) {
        this.ebayExpressDeliveryCode = ebayExpressDeliveryCode;
    }

    /**
     * @param ebayHomeDeliveryCode
     *            the ebayHomeDeliveryCode to set
     */
    @Required
    public void setEbayHomeDeliveryCode(final String ebayHomeDeliveryCode) {
        this.ebayHomeDeliveryCode = ebayHomeDeliveryCode;
    }

    /**
     * @param productService
     *            the productService to set
     */
    @Required
    public void setProductService(final ProductService productService) {
        this.productService = productService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param categoryService
     *            the categoryService to set
     */
    @Required
    public void setCategoryService(final CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @param brandService
     *            the brandService to set
     */
    @Required
    public void setBrandService(final BrandService brandService) {
        this.brandService = brandService;
    }

    /**
     * @param colourService
     *            the colourService to set
     */
    @Required
    public void setColourService(final ColourService colourService) {
        this.colourService = colourService;
    }

    /**
     * @param classificationService
     *            the classificationService to set
     */
    @Required
    public void setClassificationService(final ClassificationService classificationService) {
        this.classificationService = classificationService;
    }

    /**
     * @param variantService
     *            the variantService to set
     */
    @Required
    public void setVariantService(final VariantsService variantService) {
        this.variantService = variantService;
    }

    /**
     * @param targetMediaImportService
     *            the targetMediaImportService to set
     */
    @Required
    public void setTargetMediaImportService(final TargetMediaImportService targetMediaImportService) {
        this.targetMediaImportService = targetMediaImportService;
    }

    /**
     * @param productSearchService
     *            the productSearchService to set
     */
    @Required
    public void setProductSearchService(final TargetProductSearchService productSearchService) {
        this.productSearchService = productSearchService;
    }

    /**
     * @param classificationSystemService
     *            the classificationSystemService to set
     */
    @Required
    public void setClassificationSystemService(final ClassificationSystemService classificationSystemService) {
        this.classificationSystemService = classificationSystemService;
    }

    /**
     * @param productTypeService
     *            the productTypeService to set
     */
    @Required
    public void setProductTypeService(final ProductTypeService productTypeService) {
        this.productTypeService = productTypeService;
    }

    /**
     * @param mediaService
     *            the mediaService to set
     */
    @Required
    public void setMediaService(final MediaService mediaService) {
        this.mediaService = mediaService;
    }

    /**
     * @param targetPurchaseOptionHelper
     *            the targetPurchaseOptionHelper to set
     */
    @Required
    public void setTargetPurchaseOptionHelper(final TargetPurchaseOptionHelper targetPurchaseOptionHelper) {
        this.targetPurchaseOptionHelper = targetPurchaseOptionHelper;
    }

    /**
     * @param stockService
     *            the stockService to set
     */
    @Required
    public void setStockService(final TargetStockService stockService) {
        this.stockService = stockService;
    }

    /**
     * @param warehouseService
     *            the warehouseService to set
     */
    @Required
    public void setWarehouseService(final TargetWarehouseService warehouseService) {
        this.warehouseService = warehouseService;
    }

    /**
     * @param deliveryModeService
     *            the deliveryModeService to set
     */
    @Required
    public void setDeliveryModeService(final TargetDeliveryService deliveryModeService) {
        this.deliveryModeService = deliveryModeService;
    }

    /**
     * @param targetProductDepartmentService
     *            the targetProductDepartmentService to set
     */
    @Required
    public void setTargetProductDepartmentService(final TargetProductDepartmentService targetProductDepartmentService) {
        this.targetProductDepartmentService = targetProductDepartmentService;
    }

    /**
     * @param sizeTypeService
     *            the sizeTypeService to set
     */
    @Required
    public void setSizeTypeService(final TargetSizeTypeService sizeTypeService) {
        this.sizeTypeService = sizeTypeService;
    }

    /**
     * @param targetProductDimensionsService
     *            the targetProductDimensionsService to set
     */
    @Required
    public void setTargetProductDimensionsService(final TargetProductDimensionsService targetProductDimensionsService) {
        this.targetProductDimensionsService = targetProductDimensionsService;
    }

    /**
     * @param targetProductService
     *            the targetProductService to set
     */
    @Required
    public void setTargetProductService(final TargetProductService targetProductService) {
        this.targetProductService = targetProductService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * @param digitalGiftcardCode
     *            the digitalGiftcardCode to set
     */
    @Required
    public void setDigitalGiftcardCode(final String digitalGiftcardCode) {
        this.digitalGiftcardCode = digitalGiftcardCode;
    }

    /**
     * @param targetFulfilmentWarehouseService
     *            the targetFulfilmentWarehouseService to set
     */
    @Required
    public void setTargetFulfilmentWarehouseService(
            final TargetFulfilmentWarehouseService targetFulfilmentWarehouseService) {
        this.targetFulfilmentWarehouseService = targetFulfilmentWarehouseService;
    }

    /**
     * @param digitalProductStock
     *            the digitalProductStock to set
     */
    @Required
    public void setDigitalProductStock(final int digitalProductStock) {
        this.digitalProductStock = digitalProductStock;
    }

    /**
     * @param giftCardService
     *            the giftCardService to set
     */
    @Required
    public void setGiftCardService(final GiftCardService giftCardService) {
        this.giftCardService = giftCardService;
    }

    /**
     * @param giftMaxOrderValue
     *            the giftMaxOrderValue to set
     */
    @Required
    public void setGiftMaxOrderValue(final int giftMaxOrderValue) {
        this.giftMaxOrderValue = giftMaxOrderValue;
    }

    /**
     * @param giftMaxOrderQuantity
     *            the giftMaxOrderQuantity to set
     */
    @Required
    public void setGiftMaxOrderQuantity(final int giftMaxOrderQuantity) {
        this.giftMaxOrderQuantity = giftMaxOrderQuantity;
    }

    /**
     * @param ebayClickAndCollectDeliveryCode
     *            the ebayClickAndCollectDeliveryCode to set
     */
    @Required
    public void setEbayClickAndCollectDeliveryCode(final String ebayClickAndCollectDeliveryCode) {
        this.ebayClickAndCollectDeliveryCode = ebayClickAndCollectDeliveryCode;
    }

    /**
     * @param validators
     *            the validators to set
     */
    @Required
    public void setValidators(final List<TargetProductImportValidator> validators) {
        this.validators = validators;
    }

    /**
     * @param targetProductImportUtil
     *            the targetProductImportUtil to set
     */
    @Required
    public void setTargetProductImportUtil(final TargetProductImportUtil targetProductImportUtil) {
        this.targetProductImportUtil = targetProductImportUtil;
    }

    /**
     * @param targetSizeOrderingService
     *            the targetSizeOrderingService to set
     */
    @Required
    public void setTargetSizeOrderingService(final TargetSizeOrderingService targetSizeOrderingService) {
        this.targetSizeOrderingService = targetSizeOrderingService;
    }

    /**
     * @param targetFeatureSwitchFacade
     *            the targetFeatureSwitchFacade to set
     */
    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }

    /**
     * @param fluentProductUpsertService
     *            the fluentProductUpsertService to set
     */
    @Required
    public void setFluentProductUpsertService(final FluentProductUpsertService fluentProductUpsertService) {
        this.fluentProductUpsertService = fluentProductUpsertService;
    }

    /**
     * @param fluentSkuUpsertService
     *            the fluentSkuUpsertService to set
     */
    @Required
    public void setFluentSkuUpsertService(final FluentSkuUpsertService fluentSkuUpsertService) {
        this.fluentSkuUpsertService = fluentSkuUpsertService;
    }

    /**
     * @param targetOrderService
     *            the targetOrderService to set
     */
    @Required
    public void setTargetOrderService(final TargetOrderService targetOrderService) {
        this.targetOrderService = targetOrderService;
    }

    /**
     * @param hoursConstantEmbargoReleaseDate
     *            the hoursConstantEmbargoReleaseDate to set
     */
    @Required
    public void setHoursConstantEmbargoReleaseDate(final int hoursConstantEmbargoReleaseDate) {
        this.hoursConstantEmbargoReleaseDate = hoursConstantEmbargoReleaseDate;
    }
}