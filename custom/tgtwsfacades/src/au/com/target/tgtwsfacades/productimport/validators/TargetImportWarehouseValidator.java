/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.validators;

import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;


/**
 * @author Pradeep
 *
 */
public class TargetImportWarehouseValidator implements TargetProductImportValidator {

    private TargetWarehouseService warehouseService;


    @Override
    public List<String> validate(final IntegrationProductDto product) {

        final List<String> messages = new ArrayList<>();


        if (product.getWarehouses() != null && CollectionUtils.isNotEmpty(product.getWarehouses())) {
            final List<String> warehouses = product.getWarehouses();
            if (warehouses.size() == 1) {
                try {
                    warehouseService.getWarehouseForCode(warehouses.get(0));
                }
                catch (final UnknownIdentifierException exception) {
                    messages.add(MessageFormat.format(
                            "PRODUCTIMPORT= Warehouse code - {0} not recognised for Product - {1}",
                            warehouses.get(0), product.getProductCode()));
                }
            }
            else {
                messages.add(MessageFormat.format(
                        "PRODUCTIMPORT= product - {0}, cannot have  more then one warehouse",
                        product.getProductCode()));
            }

        }
        return messages;
    }



    /**
     * @param warehouseService
     *            the warehouseService to set
     */
    @Required
    public void setWarehouseService(final TargetWarehouseService warehouseService) {
        this.warehouseService = warehouseService;
    }
}
