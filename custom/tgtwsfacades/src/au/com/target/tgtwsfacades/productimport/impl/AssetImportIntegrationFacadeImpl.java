/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.impl;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import au.com.target.tgtwsfacades.integration.dto.IntegrationAssetImportDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationMediaDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.productimport.AssetImportIntegrationFacade;
import au.com.target.tgtwsfacades.productimport.services.TargetMediaImportService;
import au.com.target.tgtwsfacades.productimport.utility.TargetProductImportUtil;




/**
 * @author rsamuel3
 * 
 */
public class AssetImportIntegrationFacadeImpl implements AssetImportIntegrationFacade {


    /**
     * 
     */
    protected static final String ERROR_MEDIAFORMAT = "ERR-ASSETIMPORT-MEDIAFORMATMISSING : The requested media "
            + "format %s is not available in hybris for the asset %s.";

    protected static final String ERROR_MEDIAFILE = "ERR-ASSETIMPORT-MEDIAFILEMISSING : The requested media "
            + "path %s is incorrect or the media is not available for the asset %s.";

    protected static final String ERROR_AMBIGOUS = "ERR-ASSETIMPORT-AMBIGOUSRESULTS : The requested %s "
            + "for the asset %s returned more than one result from hybris.";

    protected static final String INFO_MEDIAPATH = "INFO-ASSETIMPORT-MEDIAPATHMISSING : The requested media path "
            + "is empty for the asset %s. Hence deleting the asset";

    protected static final String ERROR_MEDIAIMAGE = "ERR-ASSETIMPORT-MEDIAIMAGEERROR : Errors reported when trying"
            + " to load media image %s for the asset %s. ";

    private static final Logger LOG = Logger.getLogger(AssetImportIntegrationFacadeImpl.class);
    public static final String IMG_WIDE = "IMG-WIDE";
    public static final String WIDE_FORMAT = "wide";


    private IntegrationResponseDto responses;

    @Autowired
    private TargetMediaImportService targetMediaImportService;

    @Autowired
    private ModelService modelService;

    @Autowired
    private TargetProductImportUtil targetProductImportUtil;


    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.productimport.AssetImportIntegrationFacade#importTargetAssets(au.com.target.tgtfacades.integration.dto.IntegrationAssetImportDto)
     */
    @Override
    public IntegrationResponseDto importTargetAssets(final IntegrationAssetImportDto integrationAssetsDto) {
        boolean success = true;
        final String assetId = integrationAssetsDto.getAssetId();
        final String assetName = integrationAssetsDto.getName();
        final List<String> errorMsgs = new ArrayList<>();

        if (integrationAssetsDto.getMedias() == null
                || CollectionUtils.isEmpty(integrationAssetsDto.getMedias().getMedia())) {
            final String errorMsg = String.format(
                    "ERR-ASSETIMPORT-NOMEDIA : No medias to import. Hence not processing the assets with id %s",
                    assetId);
            LOG.info(errorMsg);
            errorMsgs.add(errorMsg);
            success = false;
            responses = targetProductImportUtil.createResponse(success, errorMsgs, assetId);
            return responses;
        }

        MediaContainerModel mediacontainerModel = null;

        if (StringUtils.isNotBlank(assetId)) {
            mediacontainerModel = targetMediaImportService.getOrCreateMediaContainer(assetId);
        }

        final List<ItemModel> itemsToSave = new ArrayList<>();

        itemsToSave.add(mediacontainerModel);

        for (final IntegrationMediaDto mediaDTO : integrationAssetsDto.getMedias().getMedia()) {
            if (isValidMediaFormat(assetName, mediaDTO.getMediaFormat())) {
                final MediaModel mediaModel = convertDtoToModel(assetId, mediaDTO, errorMsgs);
                if (mediaModel == null) {
                    continue;
                }
                String folder = null;
                String filename = null;
                try {
                    LOG.info(String.format("INFO-ASSETIMPORT-IMAGEIMPORT : importing media with asset ID %s",
                            assetId));
                    final String mediaPath = mediaDTO.getMediaPath();
                    String imageURL = null;
                    if (StringUtils.isNotBlank(mediaPath)) {
                        final String[] fileDetails = getFileDetails(mediaPath);
                        folder = fileDetails[0];
                        filename = fileDetails[1];

                        if (StringUtils.isNotBlank(folder) && StringUtils.isNotBlank(filename)) {
                            final String mediaFile = folder + File.separator + filename;
                            final File actualFile = new File(targetProductImportUtil.getActualImageLocation(mediaFile));
                            if (actualFile.exists()) {
                                imageURL = targetProductImportUtil.getImageURL(mediaFile);
                                mediaModel.setURL(imageURL);
                            } else {
                                LOG.error("ERR-ASSETIMPORT-IMGNOTFOUND : The media not found in folder : "
                                        + actualFile.getAbsolutePath());
                                errorMsgs
                                        .add("ERR-ASSETIMPORT-IMGNOTFOUND : The media not found in folder : " + mediaFile);
                                success = false;
                                continue;
                            }
                        }
                    }

                    mediaModel.setMediaContainer(mediacontainerModel);
                    itemsToSave.add(mediaModel);

                    final String successmessage = String
                            .format(
                                    "INFO-ASSETIMPORT-SUCESS : The media of format %s for the asset %s has been successfully created in hybris",
                                    mediaDTO.getMediaFormat(),
                                    assetId);
                    LOG.info(successmessage);
                    errorMsgs.add(successmessage);
                } catch (final IOException e) {
                    final String file = folder + filename;
                    final StringBuilder errorMsg = new StringBuilder(String.format(ERROR_MEDIAIMAGE,
                            file, assetId));
                    errorMsg.append("Application threw an error with the message ").append(e.getMessage());
                    LOG.error(errorMsg.toString());
                    errorMsgs.add(errorMsg.toString());
                    success = false;
                    continue;
                }
            }
        }
        if (CollectionUtils.isNotEmpty(itemsToSave)) {
            modelService.saveAll(itemsToSave);
            //            for (final MediaImageData mediaImage : mediaMap.keySet()) {
            //                final MediaModel media = mediaMap.get(mediaImage);
            //                modelService.refresh(media);
            //                mediaService.setStreamForMedia(media, mediaImage.getImageData(), mediaImage.getFilename(),
            //                        mediaImage.getMimetype());
            //            }
        }
        else {
            success = false;
            final String errormessage = String
                    .format(
                            "INFO-ASSETIMPORT-ERROR : The media of with assetID %s was not saved since no images were found",
                            assetId);
            LOG.info(errormessage);
            errorMsgs.add(errormessage);
        }
        responses = targetProductImportUtil.createResponse(success, errorMsgs, assetId);
        return responses;
    }

    /**
     *
     * @param assetName
     * @param mediaFormat
     * @return boolean
     */
    private boolean isValidMediaFormat(final String assetName, final String mediaFormat) {
        if(!StringUtils.containsIgnoreCase(assetName, IMG_WIDE)
                && StringUtils.equalsIgnoreCase(mediaFormat, WIDE_FORMAT) ) {
            return false;
        } else if(StringUtils.containsIgnoreCase(assetName,IMG_WIDE)
                && !StringUtils.equalsIgnoreCase(mediaFormat,WIDE_FORMAT)){
            return false;
        }

        return true;
    }

    /**
     * @param media
     * @param errorMsgs
     * @return List of the MediaModel
     */
    private MediaModel convertDtoToModel(final String assetId, final IntegrationMediaDto media,
            final List<String> errorMsgs) {

        MediaFormatModel mediaformat = null;
        try {
            mediaformat = targetMediaImportService.getMediaFormatModel(media.getMediaFormat());
        }
        catch (final AmbiguousIdentifierException aie) {
            final String errorMsg = String.format(ERROR_AMBIGOUS, "Media Format",
                    assetId);
            LOG.error(errorMsg);
            errorMsgs.add(errorMsg);
            return null;
        }


        final String mediaCode = TargetProductImportUtil.FORWARD_SLASH + media.getMediaFormat()
                + TargetProductImportUtil.FORWARD_SLASH + assetId;

        MediaModel mediaModel = null;
        try {
            mediaModel = targetMediaImportService.getOrCreateMediaModel(mediaCode);
        }
        catch (final AmbiguousIdentifierException aie) {
            final String errorMsg = String.format(ERROR_AMBIGOUS, "Media Model",
                    assetId);
            LOG.error(errorMsg);
            errorMsgs.add(errorMsg);
            return null;
        }


        if (mediaformat == null && StringUtils.isNotBlank(media.getMediaFormat())
                && !media.getMediaFormat().equals("video")) {
            final String errorMsg = String.format(ERROR_MEDIAFORMAT, media.getMediaFormat(),
                    assetId);
            LOG.error(errorMsg);

            errorMsgs.add(errorMsg);
            return null;
        }
        else {
            mediaModel.setMediaFormat(mediaformat);
        }

        //catalog version
        //TODO fix up to get catalog version once
        mediaModel.setCatalogVersion(targetProductImportUtil.getProductCatalogStagedVersion());

        return mediaModel;
    }


    /**
     * @param mediaPath
     * @return String array 0 : folder 1: filename
     */
    private String[] getFileDetails(final String mediaPath) {
        final int index = mediaPath.lastIndexOf(TargetProductImportUtil.FORWARD_SLASH);
        if (index > 0) {
            final String filename = mediaPath.substring(index + 1);
            final String folder = mediaPath.substring(0, index);
            return new String[] { folder, filename };
        }
        else {
            return new String[] { mediaPath, mediaPath };
        }
    }



}
