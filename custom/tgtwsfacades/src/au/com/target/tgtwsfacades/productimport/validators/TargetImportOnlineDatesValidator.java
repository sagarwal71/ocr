/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.validators;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;


/**
 * @author rsamuel3
 *
 */
public class TargetImportOnlineDatesValidator implements TargetProductImportValidator {

    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.productimport.validators.TargetProductImportValidator#validate(au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto)
     */
    @Override
    public List<String> validate(final IntegrationProductDto dto) {
        final String variationCode = dto.getVariantCode();
        // on-line, off-line dates 
        final List<String> messages = new ArrayList<>();
        final String onDateString = dto.getProductOnlineDate();
        final String offDateString = dto.getProductOfflineDate();
        Date fromDate = null;
        Date toDate = null;

        if (onDateString != null) {
            try {
                fromDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(onDateString);
            }
            catch (final Exception e) {
                messages.add(MessageFormat.format(
                        "Product online date has incorrect format, aborting product/variant import for: {0}",
                        variationCode));
            }
        }

        if (offDateString != null) {
            try {
                toDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(offDateString);

            }
            catch (final Exception e) {
                messages.add(MessageFormat.format(
                        "Product offline date has incorrect format, aborting product/variant import for: {0}",
                        variationCode));
            }
        }

        if (fromDate != null && toDate != null) {
            if (!fromDate.before(toDate)) {
                messages.add(MessageFormat.format(
                        "Product offline date is before online date, aborting product/variant import for: {0}",
                        variationCode));
            }
        }
        return messages;
    }

}
