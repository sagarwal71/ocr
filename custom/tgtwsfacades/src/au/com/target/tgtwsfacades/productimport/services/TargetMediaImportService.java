/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.services;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;

import java.util.List;

import au.com.target.tgtwsfacades.productimport.data.ProductMediaObjects;



/**
 * @author rsamuel3
 * 
 */
public interface TargetMediaImportService {

    String HERO_MEDIA = "/hero/";
    String THUMB_MEDIA = "/thumb/";
    String LARGE_MEDIA = "/large/";
    String LIST_MEDIA = "/list/";
    String GRID_MEDIA = "/grid/";
    String SWATCH_MEDIA = "/swatch/";
    String FULL_MEDIA = "/full/";
    String WIDE_MEDIA = "/wide/";

    /**
     * deletes the media object from hybris
     *
     * @param code
     */
    void removeMedia(final String code);


    /**
     * gets either a new instance or ann already existing instance for the Media model
     *
     * @param qualifier
     *
     * @return MediaModel
     */
    MediaModel getOrCreateMediaModel(String qualifier);

    /**
     * @param qualifier
     * @param catalogVersion
     *
     * @return {@link MediaModel}
     */
    MediaModel getOrCreateMediaModel(String qualifier, CatalogVersionModel catalogVersion);

    /**
     * gets an already existing instance for the Media model
     *
     * @param qualifier
     *
     * @return MediaModel
     */
    MediaModel getMediaModel(String qualifier);

    /**
     * @param qualifier
     * @param catalogVersion
     *
     * @return {@link MediaModel}
     */
    MediaModel getMediaModel(String qualifier, CatalogVersionModel catalogVersion);

    /**
     * gets the existing instance of the MediaFormatModel or returns null Our implementation never creates a new
     * instance
     *
     * @param qualifier
     *
     * @return MediaFormatModel
     */
    MediaFormatModel getMediaFormatModel(String qualifier);

    /**
     * gets either a new instance or ann already existing instance for the Media folder
     *
     * @param qualifier
     * @param assetId
     *
     * @return MediaFolderModel
     */
    MediaFolderModel getMediaFolder(final String qualifier, final String assetId);


    //    /**
    //     * gets either a new instance or ann already existing instance for the Media container
    //     * 
    //     * 
    //     * @param qualifier
    //     * @param assetId
    //     * @return MediaContainerModel
    //     */
    //    MediaContainerModel getOrCreateMediaContainer(final String qualifier);

    /**
     * returns all the media objects for a product
     *
     * @param productCode
     * @param primaryQualifier
     * @param swatchImage
     * @param secondaryQualifier
     * @param wideQualifier
     *
     * @return ProductMediaObjects which have all the media required to associate with product
     */
    ProductMediaObjects getMediaObjects(String productCode, String primaryQualifier, String swatchImage,
                                        List<String> secondaryQualifier, List<String> wideQualifier);

    /**
     * @param qualifier
     *
     * @return Mediacontainer based on this qualifier and null if it cannot find it
     */
    MediaContainerModel getMediaContainer(String qualifier);

    /**
     * @param qualifier
     * @param catalogVersion
     *
     * @return {@link MediaContainerModel} based on this qualifier and null if it cannot find it
     */
    MediaContainerModel getMediaContainer(String qualifier, CatalogVersionModel catalogVersion);

    /**
     * @param assetId
     *
     * @return MediaContainerModel
     */
    MediaContainerModel getOrCreateMediaContainer(String assetId);

    /**
     * @param qualifier
     * @param catalogVersion
     *
     * @return {@link MediaContainerModel}
     */
    MediaContainerModel getOrCreateMediaContainer(String qualifier, CatalogVersionModel catalogVersion);
}

