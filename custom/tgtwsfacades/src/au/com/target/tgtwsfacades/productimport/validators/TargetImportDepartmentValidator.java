/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.validators;

import de.hybris.platform.catalog.CatalogVersionService;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.product.TargetProductDepartmentService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;


/**
 * @author rsamuel3
 *
 */
public class TargetImportDepartmentValidator implements TargetProductImportValidator {

    private TargetProductDepartmentService targetProductDepartmentService;

    private CatalogVersionService catalogVersionService;

    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.productimport.validators.TargetProductImportValidator#validate(au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto)
     */
    @Override
    public List<String> validate(final IntegrationProductDto dto) {

        final List<String> messages = new ArrayList<>();
        final String variationCode = dto.getVariantCode();
        final Integer department = dto.getDepartment();

        if (department == null) {
            messages.add(MessageFormat.format(
                    "Missing 'DEPARTMENT NUMBER' (Attr_308459), aborting product/variant import for: {0}",
                    variationCode));
            return messages;
        }

        if (null == targetProductDepartmentService
                .getDepartmentCategoryModel(
                        catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                                TgtCoreConstants.Catalog.OFFLINE_VERSION), department)) {
            messages.add(MessageFormat.format(
                    "Department {0} does not exists in hybris, aborting product/variant import for {1}",
                    department.toString(), variationCode));
        }
        return messages;
    }

    /**
     * @param targetProductDepartmentService
     *            the targetProductDepartmentService to set
     */
    @Required
    public void setTargetProductDepartmentService(final TargetProductDepartmentService targetProductDepartmentService) {
        this.targetProductDepartmentService = targetProductDepartmentService;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }


}
