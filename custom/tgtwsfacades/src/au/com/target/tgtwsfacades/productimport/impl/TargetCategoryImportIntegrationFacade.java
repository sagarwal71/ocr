/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.category.TargetCategoryService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtfluent.service.FluentCategoryUpsertService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationPrincipalDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationSupercategoryDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationTargetProductCategoryDto;
import au.com.target.tgtwsfacades.productimport.CategoryImportIntegrationFacade;
import au.com.target.tgtwsfacades.productimport.TargetProductImportConstants;


/**
 * @author rmcalave
 * 
 */
public class TargetCategoryImportIntegrationFacade implements CategoryImportIntegrationFacade {

    private static final Logger LOG = Logger.getLogger(TargetCategoryImportIntegrationFacade.class);

    private TargetCategoryService targetCategoryService;

    private ModelService modelService;

    private UserService userService;

    private CatalogVersionService catalogVersionService;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private FluentCategoryUpsertService fluentCategoryUpsertService;

    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.productimport.impl.CategoryImportIntegrationFacade#persistTargetCategory(au.com.target.tgtwsfacades.integration.dto.IntegrationTargetProductCategoryDto)
     */
    @Override
    public IntegrationResponseDto persistTargetCategory(
            final IntegrationTargetProductCategoryDto integrationTargetProductCategoryDto) {
        Validate.notNull(integrationTargetProductCategoryDto, "integrationTargetProductCategoryDto must not be null");

        final String categoryCode = integrationTargetProductCategoryDto.getCode();
        final String categoryName = integrationTargetProductCategoryDto.getName();

        final IntegrationResponseDto response = new IntegrationResponseDto(categoryCode);
        response.setSuccessStatus(true);

        if (StringUtils.isBlank(categoryCode) || StringUtils.isBlank(categoryName)) {
            response.setSuccessStatus(false);
            response.addMessage("One of required values is missing [Category Code, Category Name]");
            return response;
        }

        final CategoryModel category = getCategoryForCode(categoryCode, response);
        if (!response.isSuccessStatus()) {
            return response;
        }

        category.setName(categoryName);
        processAllowedPrincipals(category, integrationTargetProductCategoryDto);
        processSupercategories(category, integrationTargetProductCategoryDto, response);

        if (!response.isSuccessStatus()) {
            return response;
        }

        try {
            modelService.save(category);
            sendFluentCategoryFeed(category);

        }
        catch (final ModelSavingException ex) {
            LOG.error(ex);
            response.setSuccessStatus(false);
            response.addMessage("Unable to save changes to category " + categoryCode);
        }
        return response;
    }

    private CategoryModel getCategoryForCode(final String categoryCode, final IntegrationResponseDto response) {
        CategoryModel category = null;
        try {
            category = getTargetCategoryService().getCategoryForCode(categoryCode);

            if (LOG.isDebugEnabled()) {
                LOG.debug("Found existing category " + categoryCode + ", will be updated.");
            }
        }
        catch (final UnknownIdentifierException ex) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Category " + categoryCode + " does not exist and will be created.");
            }

            category = getModelService().create(TargetProductCategoryModel.class);
            category.setCatalogVersion(getCatalogVersionService().getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                    TgtCoreConstants.Catalog.OFFLINE_VERSION));
            category.setCode(categoryCode);
        }
        catch (final AmbiguousIdentifierException ex) {
            final String message = "Could not update category. More than one category with code " + categoryCode
                    + " already exists.";
            if (LOG.isDebugEnabled()) {
                LOG.debug(message);
            }

            response.setSuccessStatus(false);
            response.addMessage(message);
        }

        return category;
    }

    private void processAllowedPrincipals(final CategoryModel category,
            final IntegrationTargetProductCategoryDto integrationTargetProductCategoryDto) {
        final List<IntegrationPrincipalDto> incomingAllowedPrincipals = integrationTargetProductCategoryDto
                .getAllowedPrincipals();
        if (CollectionUtils.isEmpty(incomingAllowedPrincipals)) {
            return;
        }

        List<PrincipalModel> allowedPrincipals = category.getAllowedPrincipals();
        if (allowedPrincipals == null) {
            allowedPrincipals = new ArrayList<>();
        }
        else {
            allowedPrincipals = new ArrayList<PrincipalModel>(allowedPrincipals);
        }
        category.setAllowedPrincipals(allowedPrincipals);

        for (final IntegrationPrincipalDto integrationPrincipalDto : incomingAllowedPrincipals) {
            try {
                final UserGroupModel userGroup = getUserService().getUserGroupForUID(
                        integrationPrincipalDto.getUid());
                if (!allowedPrincipals.contains(userGroup)) {
                    allowedPrincipals.add(userGroup);
                }
            }
            catch (final UnknownIdentifierException ex) {
                LOG.error("Principal with uid " + integrationPrincipalDto.getUid()
                        + " does not exist and will not be added to the list of allowed principals for category "
                        + integrationTargetProductCategoryDto.getCode());
                continue;
            }

        }
    }

    private void processSupercategories(final CategoryModel category,
            final IntegrationTargetProductCategoryDto integrationTargetProductCategoryDto,
            final IntegrationResponseDto response) {
        final List<IntegrationSupercategoryDto> incomingSupercategories = integrationTargetProductCategoryDto
                .getSupercategories();
        final List<CategoryModel> existingSupercategories = category.getSupercategories();
        final List<CategoryModel> supercategories = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(existingSupercategories)) {
            for (final CategoryModel existingSupercategory : existingSupercategories) {
                if (existingSupercategory instanceof ClassificationClassModel) {
                    supercategories.add(existingSupercategory);
                }
            }
        }

        if (CollectionUtils.isEmpty(incomingSupercategories)) {
            final CategoryModel allProductsCategory = getTargetCategoryService().getCategoryForCode(
                    TgtCoreConstants.Category.ALL_PRODUCTS);
            supercategories.add(allProductsCategory);

        }
        else {
            for (final IntegrationSupercategoryDto integrationSupercategoryDto : incomingSupercategories) {
                try {
                    final CategoryModel supercategory = getTargetCategoryService().getCategoryForCode(
                            integrationSupercategoryDto.getCode());
                    supercategories.add(supercategory);
                }
                catch (final UnknownIdentifierException ex) {
                    response.setSuccessStatus(false);
                    response.addMessage("Category " + integrationSupercategoryDto.getCode()
                            + " defined as a supercategory of " + integrationTargetProductCategoryDto.getCode()
                            + " does not exist.");
                }
            }

            if (!response.isSuccessStatus()) {
                return;
            }
        }

        category.setSupercategories(supercategories);
    }


    private void sendFluentCategoryFeed(final CategoryModel category) {
        if (!targetFeatureSwitchService.isFeatureEnabled(TargetProductImportConstants.FEATURE_FLUENT)
                || !(category instanceof TargetProductCategoryModel)) {
            return;
        }
        try {
            fluentCategoryUpsertService.upsertCategory((TargetProductCategoryModel)category);
        }
        catch (final Exception e) {
            LOG.error(MessageFormat.format(
                    "ERR-CATEGORYIMPORT-CATEGORY_IMPORT: Category feed to fluent failed for category with category code={0}, name={0} ",
                    category.getCode(), category.getName()), e);
        }
    }

    /**
     * @return the targetCategoryService
     */
    protected TargetCategoryService getTargetCategoryService() {
        return targetCategoryService;
    }

    /**
     * @param targetCategoryService
     *            the targetCategoryService to set
     */
    @Required
    public void setTargetCategoryService(final TargetCategoryService targetCategoryService) {
        this.targetCategoryService = targetCategoryService;
    }

    /**
     * @return the modelService
     */
    protected ModelService getModelService() {
        return modelService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @return the userService
     */
    protected UserService getUserService() {
        return userService;
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @return the catalogVersionService
     */
    protected CatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * @param fluentCategoryUpsertService
     *            the fluentCategoryUpsertService to set
     */
    @Required
    public void setFluentCategoryUpsertService(final FluentCategoryUpsertService fluentCategoryUpsertService) {
        this.fluentCategoryUpsertService = fluentCategoryUpsertService;
    }
}
