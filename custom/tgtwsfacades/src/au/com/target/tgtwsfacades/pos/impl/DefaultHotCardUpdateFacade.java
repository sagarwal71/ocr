/**
 * 
 */
package au.com.target.tgtwsfacades.pos.impl;

import de.hybris.platform.servicelayer.exceptions.ModelSavingException;

import org.apache.log4j.Logger;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtsale.tmd.TargetTMDHotCardService;
import au.com.target.tgtwsfacades.pos.HotCardUpdateFacade;


/**
 * @author Olivier Lamy
 */
public class DefaultHotCardUpdateFacade implements HotCardUpdateFacade {

    private static final Logger LOG = Logger.getLogger(DefaultHotCardUpdateFacade.class);

    private TargetTMDHotCardService targetTMDHotCardService;

    /**
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     * @throws ModelSavingException
     * @see au.com.target.tgtwsfacades.pos.HotCardUpdateFacade#updateHotCardStatus(long, boolean)
     */
    @Override
    public void updateHotCardStatus(final long hotcardNumber, final boolean toAdd) throws ModelSavingException,
            TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        LOG.info("updateHotCardStatus(" + hotcardNumber + "," + toAdd + ")");

        // the service take care if the card already exist or not so do not verify that
        // boolean exists = targetTMDHotCardService.exists(hotcardNumber);
        if (toAdd) {
            targetTMDHotCardService.save(hotcardNumber);
        }
        else {
            targetTMDHotCardService.remove(hotcardNumber);
        }
    }

    public TargetTMDHotCardService getTargetTMDHotCardService() {
        return targetTMDHotCardService;
    }

    public void setTargetTMDHotCardService(final TargetTMDHotCardService targetTMDHotCardService) {
        this.targetTMDHotCardService = targetTMDHotCardService;
    }



}
