/**
 * 
 */
package au.com.target.tgtwsfacades.pos.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.util.StandardDateRange;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.price.TargetPreviousPermanentPriceService;
import au.com.target.tgtcore.product.TargetProductPriceService;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtcore.product.data.TargetPriceRowData;
import au.com.target.tgtfluent.service.FluentSkuUpsertService;
import au.com.target.tgtsale.integration.dto.IntegrationPosProductItemDto;
import au.com.target.tgtsale.integration.dto.IntegrationPosProductItemPriceDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.pos.PosPriceImportIntegrationFacade;


public class TargetPosPriceImportIntegrationFacade implements PosPriceImportIntegrationFacade {

    private static final String NO_CATALOGS = "ERR-PRICEUPDATE-CATMISSING : No catalogs were found";

    private static final Logger LOG = LoggerFactory.getLogger(TargetPosPriceImportIntegrationFacade.class);

    private TargetProductPriceService targetProductPriceService;

    private TargetPreviousPermanentPriceService targetPreviousPermanentPriceService;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private TargetProductService targetProductService;

    private CatalogVersionService catalogVersionService;

    private FluentSkuUpsertService fluentSkuUpsertService;

    @Override
    public IntegrationResponseDto importProductPriceFromPos(final List<IntegrationPosProductItemDto> dtos) {

        final List<CatalogVersionModel> catalogs = targetProductPriceService.getCatalogs();
        final IntegrationResponseDto response = new IntegrationResponseDto(null);
        response.setSuccessStatus(true);
        final List<String> productsNotOnSale = new ArrayList<>();

        final List<ProductModel> productsToUpdate = new ArrayList<ProductModel>(dtos.size());

        if (CollectionUtils.isNotEmpty(catalogs)) {

            if (CollectionUtils.isNotEmpty(dtos)) {
                for (final IntegrationPosProductItemDto dto : dtos) {
                    if (dto.isRemoveFromSale())
                    {
                        productsNotOnSale.add(dto.getItemCode());
                        //update the end date for previous permanent price if the product is removed from sale 
                        targetPreviousPermanentPriceService.updatePreviousPermanentPriceEndDate(dto.getItemCode());
                    }
                    else {
                        // Validate was price
                        final double wasPrice = getAdjustedWasPriceIfNecessary(dto.getCurrentSalePrice(),
                                dto.getPermanentPrice(),
                                dto.getItemCode());

                        try {
                            // get the product in each catalog version
                            final List<ProductModel> productToUpdate = targetProductPriceService
                                    .getProductModelForCatalogs(dto.getItemCode(), catalogs,
                                            dto.getCurrentSalePrice(), wasPrice,
                                            getFuturePrices(dto.getPrices(), dto.getItemCode()), dto.getGstRate(),
                                            dto.isPromoEvent());

                            if (CollectionUtils.isNotEmpty(productToUpdate)) {
                                productsToUpdate.addAll(productToUpdate);
                            }
                            //create or update the previous permanent price history table
                            targetPreviousPermanentPriceService.createOrUpdatePermanentPrice(dto.getItemCode(),
                                    Double.valueOf(dto.getPermanentPrice()), dto.isPromoEvent());
                        }
                        catch (final Exception e)
                        {
                            LOG.warn(
                                    "WARN-PRICEUPDATE-RETRIEVEPRODUCTS : Exception occured when updating Prices for {} : "
                                            + e.getMessage(),
                                    dto.getItemCode(), e);
                            response.setSuccessStatus(false);
                            response.addMessage("WARN-PRICEUPDATE-RETRIEVEPRODUCTS : Exception occured when updating Prices for "
                                    + dto.getItemCode() + " : " + e.getMessage());
                        }
                    }

                }
            }

            //if there are products to be removed from sale then update hybris
            if (CollectionUtils.isNotEmpty(productsNotOnSale)) {
                final List<String> errorMessages = targetProductPriceService.removeProductFromSale(productsNotOnSale,
                        catalogs);
                if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)) {
                    feedBannedProductsToFluent(productsNotOnSale);
                }
                if (CollectionUtils.isNotEmpty(errorMessages)) {
                    response.setSuccessStatus(false);
                    response.addMessages(errorMessages);
                }
            }

            //if there are products to be updated
            if (CollectionUtils.isNotEmpty(productsToUpdate)) {

                targetProductPriceService.updateProductPrices(productsToUpdate);
                updateOnlineFromDate(productsToUpdate);
            }

        }
        else {
            LOG.warn(NO_CATALOGS);
            response.setSuccessStatus(false);
            response.addMessage(NO_CATALOGS);
        }

        return response;
    }

    /**
     * 
     * @param productsNotOnSale
     */
    private void feedBannedProductsToFluent(final List<String> productsNotOnSale) {
        final CatalogVersionModel stagedCat = catalogVersionService.getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION);
        for (final String productNotOnSale : productsNotOnSale) {
            ProductModel productModel = null;
            try {
                productModel = targetProductService.getProductForCode(stagedCat, productNotOnSale);
            }
            catch (final UnknownIdentifierException e) {
                LOG.warn(
                        "UnknownIdentifierException for productCode:{0} and catalogVersion {1}", productNotOnSale,
                        stagedCat.getVersion(), e);
            }
            catch (final AmbiguousIdentifierException e) {
                LOG.error(
                        "AmbiguousIdentifierException for productCode:{0} and catalogVersion {1}", productNotOnSale,
                        stagedCat.getVersion(), e);
            }
            if (productModel instanceof AbstractTargetVariantProductModel) {
                fluentSkuUpsertService.upsertSku((AbstractTargetVariantProductModel)productModel);
            }
        }
    }

    /**
     * @param productsToUpdate
     */
    private void updateOnlineFromDate(final List<ProductModel> productsToUpdate) {
        for (final ProductModel product : productsToUpdate) {
            targetProductService.setOnlineDateIfApplicable(product);
        }
    }

    /**
     * Convert integration price dtos to core price dtos
     * 
     * @param priceDtos
     * @return list of TargetPriceRowData
     */
    private List<TargetPriceRowData> getFuturePrices(final List<IntegrationPosProductItemPriceDto> priceDtos,
            final String productCode) {
        final List<TargetPriceRowData> targetPrices = new ArrayList<>();
        for (final IntegrationPosProductItemPriceDto priceDto : priceDtos) {
            final TargetPriceRowData targetPrice = getTargetPriceData(priceDto, productCode);

            if (targetPrice != null) {
                targetPrices.add(targetPrice);
            }
            else {
                LOG.warn("Invalid price range received for product, price not added: " + productCode);
            }

        }

        return targetPrices;
    }


    /**
     * Convert an integration price dto to a core price dto
     * 
     * @param dto
     * @return TargetPriceRowData
     */
    private TargetPriceRowData getTargetPriceData(final IntegrationPosProductItemPriceDto dto, final String productCode) {
        final TargetPriceRowData targetPrice = new TargetPriceRowData();

        // Validate was price
        final double wasPrice = getAdjustedWasPriceIfNecessary(dto.getSellPrice(), dto.getPermPrice(), productCode);

        targetPrice.setWasPrice(wasPrice);
        targetPrice.setSellPrice(dto.getSellPrice());
        targetPrice.setPromoEvent(dto.isPromoEvent());

        // Check date range is valid
        if (dto.getStart() != null && dto.getEnd() != null && dto.getEnd().getTime() > dto.getStart().getTime()) {
            final StandardDateRange dateRange = new StandardDateRange(dto.getStart(), dto.getEnd());
            targetPrice.setDateRange(dateRange);
            return targetPrice;
        }

        return null;
    }

    /**
     * If the was price is not valid relative to the sell price, then make it same as sell price.
     * 
     * @param sellPrice
     * @param permPrice
     * @param productCode
     * @return double adjusted wasPrice
     */
    private double getAdjustedWasPriceIfNecessary(final double sellPrice, final double permPrice,
            final String productCode) {

        // If we don't have a valid was price, the item is not on promotion, so just use the current sell price
        double wasPrice = permPrice;
        if (!wasPriceIsValidForSellPrice(sellPrice, wasPrice)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Was price not valid, resetting to sell price. Product=" + productCode + ", " +
                        "Was price=" + wasPrice + ", Sell price=" + sellPrice);
            }
            wasPrice = sellPrice;
        }

        return wasPrice;
    }

    private boolean wasPriceIsValidForSellPrice(final double sellPrice, final double wasPrice) {
        return (wasPrice > sellPrice);
    }

    /**
     * @param targetProductPriceService
     *            the targetProductPriceService to set
     */
    public void setTargetProductPriceService(final TargetProductPriceService targetProductPriceService) {
        this.targetProductPriceService = targetProductPriceService;
    }

    /**
     * @param targetPreviousPermanentPriceService
     *            the targetPreviousPermanentPriceService to set
     */
    @Required
    public void setTargetPreviousPermanentPriceService(
            final TargetPreviousPermanentPriceService targetPreviousPermanentPriceService) {
        this.targetPreviousPermanentPriceService = targetPreviousPermanentPriceService;
    }

    /**
     * @param targetProductService
     *            the targetProductService to set
     */
    @Required
    public void setTargetProductService(final TargetProductService targetProductService) {
        this.targetProductService = targetProductService;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * @param fluentSkuUpsertService
     *            the fluentSkuUpsertService to set
     */
    @Required
    public void setFluentSkuUpsertService(final FluentSkuUpsertService fluentSkuUpsertService) {
        this.fluentSkuUpsertService = fluentSkuUpsertService;
    }
}
