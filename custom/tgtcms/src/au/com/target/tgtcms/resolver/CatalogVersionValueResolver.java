/**
 * 
 */
package au.com.target.tgtcms.resolver;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cockpit.wizards.generic.DefaultValueResolver;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;


/**
 * This Spring bean resolves default value that is CatalogVersion. The String representation must follow standard
 * notation: catalogId:versionName, e.g.: targetContentCatalog:Staged
 */
public class CatalogVersionValueResolver implements DefaultValueResolver {

    private static final Logger LOG = Logger.getLogger(CatalogVersionValueResolver.class);

    private CatalogVersionService catalogVersionService;

    /* (non-Javadoc)
     * @see de.hybris.platform.cockpit.wizards.generic.DefaultValueResolver#getValue(java.lang.String)
     */
    @Override
    public Object getValue(final String defaultValueRepresentation) {

        if (StringUtils.isEmpty(defaultValueRepresentation)) {
            return null;
        }

        final String[] values = defaultValueRepresentation.split(":");

        String catalogId = TgtCoreConstants.Catalog.CONTENT;
        String catalogVersionName = TgtCoreConstants.Catalog.OFFLINE_VERSION;

        if (values != null && values.length > 2) {
            catalogId = values[0].trim();
            catalogVersionName = values[1].trim();
        }

        CatalogVersionModel catalogVersion = null;
        try {
            catalogVersion = catalogVersionService.getCatalogVersion(catalogId, catalogVersionName);
        }
        catch (final Exception e) {
            LOG.warn("Can't find correct CatalogVersion", e);
        }

        return catalogVersion;
    }

    /**
     * @return the catalogVersionService
     */
    public CatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }



}
