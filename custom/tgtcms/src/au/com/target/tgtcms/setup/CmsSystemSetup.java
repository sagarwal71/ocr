/**
 * 
 */
package au.com.target.tgtcms.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;

import java.util.List;

import au.com.target.tgtcms.constants.TgtcmsConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 * 
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = TgtcmsConstants.EXTENSIONNAME)
public class CmsSystemSetup extends AbstractSystemSetup {

    /* (non-Javadoc)
     * @see de.hybris.platform.acceleratorservices.setup.AbstractSystemSetup#getInitializationOptions()
     */
    @Override
    public List<SystemSetupParameter> getInitializationOptions() {
        // YTODO Auto-generated method stub
        return null;
    }

    @SystemSetup(type = Type.ESSENTIAL, process = Process.INIT)
    public void initEssentialData(final SystemSetupContext context) {
        importImpexFile(context, "/tgtcms/import/cmsusergroups.impex");
        importImpexFile(context, "/tgtcms/import/cmsusers.impex");
    }

    @SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
    public void createEssentialData(final SystemSetupContext context)
    {
        importImpexFile(context, "/tgtcms/import/cmsusergroups.impex");
    }

}
