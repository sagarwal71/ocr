/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtcms.jalo;

import de.hybris.platform.core.Registry;
import de.hybris.platform.util.JspContext;

import java.util.Map;

import au.com.target.tgtcms.constants.TgtcmsConstants;


/**
 * This is the extension manager of the tgtcms extension.
 */
public class TgtcmsManager extends GeneratedTgtcmsManager
{

    /**
     * Get the valid instance of this manager.
     * 
     * @return the current instance of this manager
     */
    @SuppressWarnings("deprecation")
    public static final TgtcmsManager getInstance()
    {
        return (TgtcmsManager)Registry.getCurrentTenant().getJaloConnection().getExtensionManager()
                .getExtension(TgtcmsConstants.EXTENSIONNAME);
    }

    /**
     * Implement this method to create initial objects. This method will be called by system creator during
     * initialization and system update. Be sure that this method can be called repeatedly.
     * 
     * An example usage of this method is to create required cronjobs or modifying the type system (setting e.g some
     * default values)
     * 
     * @param params
     *            the parameters provided by user for creation of objects for the extension
     * @param jspc
     *            the jsp context; you can use it to write progress information to the jsp page during creation
     */
    @Override
    public void createEssentialData(final Map<String, String> params, final JspContext jspc)
    {
        // implement here code creating essential data
    }

    /**
     * Implement this method to create data that is used in your project. This method will be called during the system
     * initialization.
     * 
     * An example use is to import initial data like currencies or languages for your project from an csv file.
     * 
     * @param params
     *            the parameters provided by user for creation of objects for the extension
     * @param jspc
     *            the jsp context; you can use it to write progress information to the jsp page during creation
     */
    @Override
    public void createProjectData(final Map<String, String> params, final JspContext jspc)
    {
        // implement here code creating project data
    }
}
