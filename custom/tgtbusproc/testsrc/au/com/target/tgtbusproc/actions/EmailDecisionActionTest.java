/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author jhermoso
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class EmailDecisionActionTest {

    @InjectMocks
    private final EmailDecisionAction action = new EmailDecisionAction();

    @Mock
    private OrderProcessModel orderProcessModel;

    @Mock
    private OrderModel orderModel;

    @Before
    public void setUp() {
        given(orderProcessModel.getOrder()).willReturn(orderModel);
    }

    @Test
    public void testExecuteActionWhenPreorderFulfilmentReject() throws Exception {
        final Date date = mock(Date.class);
        given(orderModel.getNormalSaleStartDateTime()).willReturn(date);
        willReturn(Boolean.FALSE).given(orderModel).getContainsPreOrderItems();
        final String status = action.execute(orderProcessModel);
        assertThat(status).isEqualTo(EmailDecisionAction.Transition.PREORDER_FULFILMENT_REJECT.toString());
    }

    @Test
    public void testExecuteActionWhenSendPreorderReject() throws Exception {
        given(orderModel.getNormalSaleStartDateTime()).willReturn(null);
        willReturn(Boolean.TRUE).given(orderModel).getContainsPreOrderItems();
        final String status = action.execute(orderProcessModel);
        assertThat(status).isEqualTo(EmailDecisionAction.Transition.SEND_ORDER_REJECT.toString());
    }

    @Test
    public void testExecuteActionWhenSendNormalOrderReject() throws Exception {
        given(orderModel.getNormalSaleStartDateTime()).willReturn(null);
        willReturn(Boolean.FALSE).given(orderModel).getContainsPreOrderItems();
        final String status = action.execute(orderProcessModel);
        assertThat(status).isEqualTo(EmailDecisionAction.Transition.SEND_ORDER_REJECT.toString());
    }
}
