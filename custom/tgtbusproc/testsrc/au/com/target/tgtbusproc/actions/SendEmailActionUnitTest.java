/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.process.strategies.ProcessContextResolutionStrategy;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.email.BusinessProcessEmailStrategy;
import au.com.target.tgtbusproc.exceptions.BusinessProcessEmailException;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendEmailActionUnitTest {
    private static final String FRONTEND_TEMPLATE_NAME = "myTemplate";

    @Mock
    private OrderProcessModel process;

    @Mock
    private BusinessProcessEmailStrategy businessProcessEmailStrategy;

    @Mock
    private ProcessContextResolutionStrategy contextResolutionStrategy;

    @InjectMocks
    @Spy
    private final SendEmailAction sendEmailAction = new SendEmailAction();

    @Before
    public void setup() {
        sendEmailAction.setFrontendTemplateName(FRONTEND_TEMPLATE_NAME);
        Mockito.doReturn(businessProcessEmailStrategy).when(sendEmailAction).getBusinessProcessEmailStrategy();
    }

    @Test
    public void testSendEmailActionSuccess() throws RetryLaterException, Exception {

        final Transition trans = sendEmailAction.executeAction(process);
        Assert.assertEquals(Transition.OK, trans);
    }

    @Test
    public void testSendEmailActionFailure() throws RetryLaterException, Exception {

        Mockito.doThrow(new BusinessProcessEmailException("Failure")).when(businessProcessEmailStrategy)
                .sendEmail(process, FRONTEND_TEMPLATE_NAME);

        final Transition trans = sendEmailAction.executeAction(process);
        Assert.assertEquals(Transition.NOK, trans);
    }

}
