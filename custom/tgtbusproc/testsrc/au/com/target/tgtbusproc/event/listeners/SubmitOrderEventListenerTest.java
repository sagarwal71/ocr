/**
 * 
 */
package au.com.target.tgtbusproc.event.listeners;

import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.event.TargetSubmitOrderEvent;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SubmitOrderEventListenerTest {

    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;

    private final SubmitOrderEventListener submitOrderEventListener = new SubmitOrderEventListener() {
        @Override
        public TargetBusinessProcessService getTargetBusinessProcessService() {
            return targetBusinessProcessService;
        }
    };

    @Mock
    private TargetSubmitOrderEvent event;

    @Mock
    private OrderModel orderModel;

    @Before
    public void setup() {
        willReturn(orderModel).given(event).getOrder();
    }

    @Test
    public void testOnOrderEvent() {
        submitOrderEventListener.onEvent(event);
        verify(targetBusinessProcessService).startPlaceOrderProcess(orderModel, event.getPte());
    }

    @Test
    public void testOnPreOrderEvent() {
        willReturn(Boolean.TRUE).given(orderModel).getContainsPreOrderItems();

        submitOrderEventListener.onEvent(event);
        verify(targetBusinessProcessService).startPlacePreOrderProcess(orderModel, event.getPte());
    }

    @Test
    public void testOnFluentOrderEvent() {
        willReturn(Boolean.TRUE).given(event).isFluentOrder();

        submitOrderEventListener.onEvent(event);
        verify(targetBusinessProcessService).startPlaceFluentOrderProcess(orderModel, event.getPte());
    }

    /**
     * This method is used to verify business placeFluentPreOrderProcess being called when the order contains pre-order
     * item in it and fluent feature switch is ON. Non of other business process will executed when these two criteria
     * met.
     */
    @Test
    public void testOnEventFluentPreOrderProcessForFluentPreOrder() {
        willReturn(Boolean.TRUE).given(orderModel).getContainsPreOrderItems();
        willReturn(Boolean.TRUE).given(event).isFluentOrder();
        submitOrderEventListener.onEvent(event);
        verify(targetBusinessProcessService).startPlaceFluentPreOrderProcess(orderModel, event.getPte());
        verify(targetBusinessProcessService, never()).startPlacePreOrderProcess(orderModel, event.getPte());
        verify(targetBusinessProcessService, never()).startPlaceFluentOrderProcess(orderModel, event.getPte());
        verify(targetBusinessProcessService, never()).startPlaceOrderProcess(orderModel, event.getPte());
    }

    /**
     * This method is used to verify business placePreOrderProcess being called when the order contains pre-order item
     * in it and fluent feature switch is OFF. Non of other business process will executed when these two criteria met.
     */
    @Test
    public void testOnEventFluentPreOrderProcessForNonFluentPreOrder() {
        willReturn(Boolean.TRUE).given(orderModel).getContainsPreOrderItems();
        willReturn(Boolean.FALSE).given(event).isFluentOrder();
        submitOrderEventListener.onEvent(event);
        verify(targetBusinessProcessService).startPlacePreOrderProcess(orderModel, event.getPte());
        verify(targetBusinessProcessService, never()).startPlaceFluentPreOrderProcess(orderModel, event.getPte());
        verify(targetBusinessProcessService, never()).startPlaceFluentPreOrderProcess(orderModel, event.getPte());
        verify(targetBusinessProcessService, never()).startPlaceFluentPreOrderProcess(orderModel, event.getPte());
    }
}
