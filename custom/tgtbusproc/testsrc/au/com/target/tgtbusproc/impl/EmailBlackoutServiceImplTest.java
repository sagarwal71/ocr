package au.com.target.tgtbusproc.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.junit.Assert;
import org.junit.Test;


/**
 * Unit test for {@link EmailBlackoutServiceImpl}
 * 
 */
@UnitTest
public class EmailBlackoutServiceImplTest {

    private static final String TIME_ZONE = "Australia/Melbourne";

    private DateTime currentDate;

    private final EmailBlackoutServiceImpl emailBlackoutServiceImpl = new EmailBlackoutServiceImpl() {

        @Override
        protected DateTime getCurrentDate() {
            return currentDate;
        }

    };

    @Test
    public void testIsEmailBlackoutInvalidHours() {
        emailBlackoutServiceImpl.setEmailBlackoutStartTime("43:00");
        emailBlackoutServiceImpl.setEmailBlackoutEndTime("00:00");
        Assert.assertFalse(emailBlackoutServiceImpl.isEmailBlackout());

    }

    @Test
    public void testIsEmailBlackoutInvalidMins() {
        emailBlackoutServiceImpl.setEmailBlackoutStartTime("00:00");
        emailBlackoutServiceImpl.setEmailBlackoutEndTime("00:99");
        Assert.assertFalse(emailBlackoutServiceImpl.isEmailBlackout());

    }

    @Test
    public void testIsEmailBlackoutWhenCurrentDateIsNextDay() {
        setCurrentDate("00:10", 1);
        emailBlackoutServiceImpl.setEmailBlackoutStartTime("20:00");
        emailBlackoutServiceImpl.setEmailBlackoutEndTime("11:00");

        Assert.assertTrue(emailBlackoutServiceImpl.isEmailBlackout());
    }

    @Test
    public void testIsEmailBlackoutWhenCurrentDateIsTodayNotInBlackout() {
        setCurrentDate("17:55", 0);
        emailBlackoutServiceImpl.setEmailBlackoutStartTime("20:00");
        emailBlackoutServiceImpl.setEmailBlackoutEndTime("11:00");

        Assert.assertFalse(emailBlackoutServiceImpl.isEmailBlackout());
    }

    @Test
    public void testIsEmailBlackoutWhenCurrentDateIsTodayInBlackout() {
        setCurrentDate("21:00", 0);
        emailBlackoutServiceImpl.setEmailBlackoutStartTime("20:00");
        emailBlackoutServiceImpl.setEmailBlackoutEndTime("11:00");

        Assert.assertTrue(emailBlackoutServiceImpl.isEmailBlackout());
    }

    private void setCurrentDate(final String time, final int days) {
        final DateTime dateTime = new LocalDate()
                .toDateTime(DateTimeFormat.forPattern("HH:mm").parseDateTime(time)
                        .toMutableDateTime(DateTimeZone.forID(TIME_ZONE)));

        currentDate = dateTime.plusDays(days);
    }
}
