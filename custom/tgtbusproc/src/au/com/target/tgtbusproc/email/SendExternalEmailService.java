/**
 * 
 */
package au.com.target.tgtbusproc.email;

import au.com.target.tgtbusproc.email.data.TargetEmailContext;


/**
 * Service for sending an email represented a data object and template
 * 
 */
public interface SendExternalEmailService {

    /**
     * Send an email represented by the given context data and template. The implementor could use
     * SiteBaseUrlResolutionService, CustomerEmailResolutionService, OrderConverter to flatten the data.<br/>
     * See OrderNotificationEmailContext and superclass AbstractEmailContext.
     * 
     * @param context
     * @param template
     * @throws EmailServiceException
     */
    void sendEmail(TargetEmailContext context, String template) throws EmailServiceException;
}
