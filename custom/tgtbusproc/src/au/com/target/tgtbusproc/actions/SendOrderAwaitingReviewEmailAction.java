/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;


/**
 * @author bpottass
 *
 */
public class SendOrderAwaitingReviewEmailAction extends SendEmailAction {

    private static final Logger LOG = Logger.getLogger(SendOrderAwaitingReviewEmailAction.class);

    @Override
    public Transition executeAction(final BusinessProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process, "Process cannot be null");
        if (process instanceof OrderProcessModel) {
            final OrderModel order = ((OrderProcessModel)process).getOrder();
            if (order != null && order.getNormalSaleStartDateTime() != null
                    && Boolean.FALSE.equals(order.getContainsPreOrderItems())) {
                LOG.info("Order: " + order.getCode()
                        + " is a PreOrder order that is being released, and hence no orderAwaitingReviewEmail is send to Customer for the final payment");
                return Transition.OK;
            }
        }
        return super.executeAction(process);
    }

}
