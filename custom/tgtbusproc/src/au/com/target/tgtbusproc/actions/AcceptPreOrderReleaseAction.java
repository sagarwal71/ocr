/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import org.springframework.util.Assert;

import au.com.target.tgtbusproc.TargetBusinessProcessService;


/**
 * * Action that will create and trigger
 * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#FLUENT_PREORDER_RELEASE_ACCEPT_PROCESS}
 *
 */
public class AcceptPreOrderReleaseAction extends AbstractProceduralAction<OrderProcessModel> {

    /* (non-Javadoc)
     * @see de.hybris.platform.processengine.action.AbstractProceduralAction#executeAction(de.hybris.platform.processengine.model.BusinessProcessModel)
     */
    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process, "Pre-order release accept process cannot be null");
        final OrderModel orderModel = process.getOrder();
        Assert.notNull(orderModel, "Ordermodel cannot be null");
        getTargetBusinessProcessService().startFluentPreOrderReleaseAcceptProcess(orderModel);

    }

    /**
     * Lookup for TargetBusinessProcessService
     * 
     * @return TargetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

}
