package au.com.target.tgtbusproc.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.TargetBusinessProcessService;


/**
 */
public class TriggerOrderProcess extends AbstractProceduralAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(TriggerOrderProcess.class);

    private String businessProcess;

    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process, "OrderProcessModel can not be null");
        Assert.notNull(process.getOrder(), "Order can not be null");

        try {
            getTargetBusinessProcessService().startOrderProcess(process.getOrder(), businessProcess);
        }
        catch (final Exception e) {
            LOG.warn("Can not initiate consignment picked business process ", e);
        }
    }

    /**
     * Lookup for TargetBusinessProcessService
     * 
     * @return TargetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

    /**
     * @param businessProcess
     *            the businessProcess to set
     */
    @Required
    public void setBusinessProcess(final String businessProcess) {
        this.businessProcess = businessProcess;
    }

}
