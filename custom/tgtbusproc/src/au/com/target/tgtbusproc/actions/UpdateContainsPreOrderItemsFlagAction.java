/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import org.springframework.util.Assert;


/**
 * @author kbalasub
 *
 */
public class UpdateContainsPreOrderItemsFlagAction extends AbstractProceduralAction<OrderProcessModel> {

    private Boolean containsPreOrderItems;

    /* (non-Javadoc)
     * @see de.hybris.platform.processengine.action.AbstractProceduralAction#executeAction(de.hybris.platform.processengine.model.BusinessProcessModel)
     */
    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process, "Orderprocess cannot be null");
        final OrderModel order = process.getOrder();
        Assert.notNull(order, "Ordermodel cannot be null");
        order.setContainsPreOrderItems(containsPreOrderItems);
        final ModelService modelService = getModelService();
        modelService.save(order);
        modelService.refresh(order);
    }


    /**
     * @param containsPreOrderItems
     *            the containsPreOrderItems to set
     */
    public void setContainsPreOrderItems(final Boolean containsPreOrderItems) {
        this.containsPreOrderItems = containsPreOrderItems;
    }
}
