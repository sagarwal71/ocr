/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.constants.TgtbusprocConstants;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;


/**
 * Trigger consignment picked process (at end of a pick cancel process)
 * 
 */
public class TriggerOrderConsignmentPickedProcess extends AbstractProceduralAction<OrderProcessModel> {

    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process, "OrderProcessModel can not be null");
        Assert.notNull(process.getOrder(), "Order can not be null");

        final ConsignmentModel consignment = orderProcessParameterHelper.getConsignment(process);
        Assert.notNull(consignment, "Consignment can not be null");

        final Object consignmentUpdateData = orderProcessParameterHelper.getPickConsignmentUpdateData(process);
        Assert.notNull(consignmentUpdateData, "consignmentUpdateData can not be null");

        getTargetBusinessProcessService().startOrderConsignmentPickedProcess(process.getOrder(),
                consignment, consignmentUpdateData, TgtbusprocConstants.BusinessProcess.CONSIGNMENT_PICKED_PROCESS);
    }

    /**
     * Lookup for TargetBusinessProcessService
     * 
     * @return TargetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }

}