/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.EmailBlackoutService;


/**
 * @author Pradeep
 *
 */
public class CheckEmailBlackoutAction extends RetryActionWithSpringConfig<OrderProcessModel> {

    private EmailBlackoutService emailBlackoutService;

    public enum Transition {
        OK;

        public static Set<String> getStringValues() {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values()) {
                res.add(t.toString());
            }
            return res;
        }
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtbusproc.actions.RetryAction#executeInternal(de.hybris.platform.processengine.model.BusinessProcessModel)
     */
    @Override
    protected String executeInternal(final OrderProcessModel process) throws RetryLaterException, Exception {
        if (emailBlackoutService.isEmailBlackout()) {
            throw new RetryLaterException("Inside email blackout");
        }
        return Transition.OK.toString();
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtbusproc.actions.RetryAction#getTransitionsInternal()
     */
    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }



    /**
     * @param emailBlackoutService
     *            the emailBlackoutService to set
     */
    @Required
    public void setEmailBlackoutService(final EmailBlackoutService emailBlackoutService) {
        this.emailBlackoutService = emailBlackoutService;
    }


}
