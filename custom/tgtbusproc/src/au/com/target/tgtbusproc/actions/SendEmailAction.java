/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import de.hybris.platform.acceleratorservices.process.strategies.ProcessContextResolutionStrategy;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.aop.BusinessProcessLoggingAspect;
import au.com.target.tgtbusproc.email.BusinessProcessEmailStrategy;
import au.com.target.tgtbusproc.exceptions.BusinessProcessEmailException;
import au.com.target.tgtbusproc.model.UpdateCustomerEmailProcessModel;
import au.com.target.tgtutility.constants.TgtutilityConstants;


public class SendEmailAction extends AbstractSimpleDecisionAction<BusinessProcessModel> {

    private static final Logger LOG = Logger.getLogger(SendEmailAction.class);

    private String frontendTemplateName;
    
    private ProcessContextResolutionStrategy contextResolutionStrategy;

    @Override
    public Transition executeAction(final BusinessProcessModel process) throws RetryLaterException, Exception {

        Assert.notNull(process, "Process cannot be null");

        try {
            if (process instanceof UpdateCustomerEmailProcessModel) {
                ((UpdateCustomerEmailProcessModel)process).getCustomer().setUid(
                        ((UpdateCustomerEmailProcessModel)process).getOldEmailAddress());
            }
            contextResolutionStrategy.initializeContext(process);
            getBusinessProcessEmailStrategy().sendEmail(process, frontendTemplateName);
        }
        catch (final BusinessProcessEmailException ex) {

            String orderCode = null;

            if (process instanceof OrderProcessModel) {
                final OrderModel order = ((OrderProcessModel)process).getOrder();
                if (order != null) {
                    orderCode = order.getCode();
                }
            }

            final String errorMsg = String.format(BusinessProcessLoggingAspect.ERROR_MESSAGE_FORMAT, ex.getMessage(),
                    TgtutilityConstants.ErrorCode.WARN_EMAIL_SEND_FAIL, SendEmailAction.class.getSimpleName(),
                    process.getCode(),
                    orderCode);

            LOG.warn(errorMsg);

            return Transition.NOK;
        }

        catch (final IllegalArgumentException ie) {

            String orderCode = null;

            if (process instanceof OrderProcessModel) {
                final OrderModel order = ((OrderProcessModel)process).getOrder();
                if (order != null) {
                    orderCode = order.getCode();
                }
            }
            final String errorMsg = String.format(BusinessProcessLoggingAspect.ERROR_MESSAGE_FORMAT, ie.getMessage(),
                    TgtutilityConstants.ErrorCode.WARN_EMAIL_SEND_FAIL, SendEmailAction.class.getSimpleName(),
                    process.getCode(),
                    orderCode);

            LOG.warn(errorMsg);
            return Transition.NOK;
        }

        LOG.info("Successfully sent email message " + frontendTemplateName + " for process " + process.getCode());

        return Transition.OK;
    }
  
    @Required
    public void setContextResolutionStrategy(ProcessContextResolutionStrategy contextResolutionStrategy) {
      this.contextResolutionStrategy = contextResolutionStrategy;
    }
    
    /**
     * @param frontendTemplateName
     */
    @Required
    public void setFrontendTemplateName(final String frontendTemplateName)
    {
        this.frontendTemplateName = frontendTemplateName;
    }


    /**
     * Lookup for BusinessProcessEmailStrategy
     * 
     * @return BusinessProcessEmailStrategy
     */
    public BusinessProcessEmailStrategy getBusinessProcessEmailStrategy() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getBusinessProcessEmailStrategy().");
    }


}
