/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.CancelTypeEnum;


public class CancelTypeDecisionAction extends AbstractAction<OrderProcessModel> {
    private static final Logger LOG = Logger.getLogger(CancelTypeDecisionAction.class);
    private OrderProcessParameterHelper orderProcessParameterHelper;

    public enum Transition
    {
        SHORT_PICK, ZERO_PICK, COCKPIT_CANCEL, COCKPIT_PARTIAL_CANCEL, NOK;

        public static Set<String> getStringValues()
        {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values())
            {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    public String execute(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process, "process cannot be null");

        final CancelTypeEnum cancelType = orderProcessParameterHelper.getCancelType(process);
        Assert.notNull(cancelType,
                "No cancelType parameter found for this process context, process: " + process.getCode());

        switch (cancelType) {
            case SHORT_PICK:
                return Transition.SHORT_PICK.toString();
            case ZERO_PICK:
                return Transition.ZERO_PICK.toString();
            case COCKPIT_CANCEL:
                return Transition.COCKPIT_CANCEL.toString();
            case COCKPIT_PARTIAL_CANCEL:
                return Transition.COCKPIT_PARTIAL_CANCEL.toString();
            default:
                LOG.error("Unexpected cancelType found: " + cancelType.toString());
                return Transition.NOK.toString();
        }
    }

    @Override
    public Set<String> getTransitions() {
        return Transition.getStringValues();
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }
}
