package au.com.target.tgtbusproc.event.listeners;

//CHECKSTYLE:OFF
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.event.PasswordChangeEvent;


//CHECKSTYLE:ON

/**
 * Listens for {@link PasswordChangeEvent} and triggers
 * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#CUSTOMER_PASSWORD_CHANGE_PROCESS}
 */
public class PasswordChangeEventListener extends AbstractEventListener<PasswordChangeEvent> {

    @Override
    protected void onEvent(final PasswordChangeEvent event) {
        getTargetBusinessProcessService().startCustomerPasswordChangeProcess(event.getCustomer(), event.getSite());
    }

    public TargetBusinessProcessService getTargetBusinessProcessService()
    {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService.");
    }
}
