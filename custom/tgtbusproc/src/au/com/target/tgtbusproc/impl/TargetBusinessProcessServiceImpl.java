package au.com.target.tgtbusproc.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.model.process.ForgottenPasswordProcessModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontProcessModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.processengine.impl.DefaultBusinessProcessService;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.returns.model.OrderReturnRecordEntryModel;
import de.hybris.platform.site.BaseSiteService;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.constants.TgtbusprocConstants;
import au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess;
import au.com.target.tgtbusproc.model.UpdateCustomerEmailProcessModel;
import au.com.target.tgtbusproc.sms.data.SendSmsToStoreData;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.CancelTypeEnum;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntryList;
import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * Default implementation of {@link TargetBusinessProcessService}
 */
public class TargetBusinessProcessServiceImpl extends DefaultBusinessProcessService implements
        TargetBusinessProcessService {

    private static final Logger LOG = Logger.getLogger(TargetBusinessProcessServiceImpl.class);

    private static final String PROCESS_CODE_SEPERATOR = "-";
    private static final String LAST_PROCESS_CODE_KEY = "last-process-code";

    private BaseSiteService baseSiteService;

    private OrderProcessParameterHelper orderProcessParameterHelper;


    /**
     * {@inheritDoc}
     */
    @Override
    public OrderProcessModel startOrderProcess(final OrderModel orderModel,
            final String processDefinitionName) {

        Assert.notNull(orderModel, "Ordermodel cannot be null");
        Assert.notNull(processDefinitionName, "ProcessDefinitionName cannot be null");

        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        return startOrderProcess(orderModel, processCode, processDefinitionName);
    }



    /**
     * {@inheritDoc}
     */
    @Override
    public OrderProcessModel startOrderProcess(final OrderModel orderModel, final String processCode,
            final String processDefinitionName) {

        Assert.notNull(orderModel, "Ordermodel cannot be null");
        Assert.notNull(processCode, "ProcessCode cannot be null");
        Assert.notNull(processDefinitionName, "ProcessDefinitionName cannot be null");

        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);
        startProcess(orderProcess);
        return orderProcess;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderProcessModel startOrderProcessWithConsignment(final OrderModel orderModel,
            final ConsignmentModel consignment,
            final String processDefinitionName) {

        Assert.notNull(orderModel, "Ordermodel cannot be null");
        Assert.notNull(processDefinitionName, "ProcessDefinitionName cannot be null");
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);
        if (consignment != null) {
            orderProcessParameterHelper.setConsignment(orderProcess, consignment);
        }
        startProcess(orderProcess);

        return orderProcess;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderProcessModel startPlaceOrderProcess(final OrderModel orderModel,
            final PaymentTransactionEntryModel ptem) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.PLACE_ORDER_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();

        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);

        if (ptem != null) {
            orderProcessParameterHelper.setPaymentTransactionEntry(orderProcess, ptem);
        }

        startProcess(orderProcess);
        return orderProcess;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderProcessModel startPlacePreOrderProcess(final OrderModel orderModel,
            final PaymentTransactionEntryModel ptem) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.PLACE_PREORDER_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();

        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);

        if (ptem != null) {
            orderProcessParameterHelper.setPaymentTransactionEntry(orderProcess, ptem);
        }

        startProcess(orderProcess);
        return orderProcess;
    }

    @Override
    public OrderProcessModel startPlaceFluentOrderProcess(final OrderModel orderModel,
            final PaymentTransactionEntryModel ptem) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.PLACE_FLUENT_ORDER_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();

        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);

        if (ptem != null) {
            orderProcessParameterHelper.setPaymentTransactionEntry(orderProcess, ptem);
        }

        startProcess(orderProcess);
        return orderProcess;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderProcessModel startPlaceFluentPreOrderProcess(final OrderModel orderModel,
            final PaymentTransactionEntryModel ptem) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");
        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.PLACE_FLUENT_PREORDER_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);
        if (ptem != null) {
            orderProcessParameterHelper.setPaymentTransactionEntry(orderProcess, ptem);
        }
        startProcess(orderProcess);
        return orderProcess;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderProcessModel startOrderAcceptProcess(final OrderModel orderModel) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.ACCEPT_ORDER_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();

        return startOrderProcess(orderModel, processCode, processDefinitionName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderProcessModel startFluentOrderAcceptProcess(final OrderModel orderModel) {

        Assert.notNull(orderModel, "Ordermodel cannot be null");
        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.ACCEPT_FLUENT_ORDER_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        return startOrderProcess(orderModel, processCode, processDefinitionName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderProcessModel startFluentAcceptPreOrderProcess(final OrderModel orderModel) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");
        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.FLUENT_ACCEPT_PREORDER_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        return startOrderProcess(orderModel, processCode, processDefinitionName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderProcessModel startPreOrderAcceptProcess(final OrderModel orderModel) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.ACCEPT_PREORDER_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();

        return startOrderProcess(orderModel, processCode, processDefinitionName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderProcessModel startPaymentFailedProcess(final OrderModel orderModel, final String reason) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");
        Assert.notNull(reason, "Reason cannot be null");

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.PAYMENT_FAILED_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();

        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);
        orderProcessParameterHelper.setPaymentFailedReason(orderProcess, reason);
        startProcess(orderProcess);
        return orderProcess;
    }

    @Override
    public OrderProcessModel startOrderConsignmentProcess(final OrderModel orderModel,
            final ConsignmentModel consignment,
            final String processDefinitionName) {

        Assert.notNull(orderModel, "Ordermodel cannot be null");
        Assert.notNull(processDefinitionName, "ProcessDefinitionName cannot be null");
        Assert.notNull(consignment, "consignment cannot be null");

        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + consignment.getCode() + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);

        orderProcessParameterHelper.setConsignment(orderProcess, consignment);

        startProcess(orderProcess);
        return orderProcess;
    }

    @Override
    public OrderProcessModel startConsignmentRejectionProcess(final OrderModel orderModel,
            final ConsignmentModel consignment, final ConsignmentRejectReason rejectReason,
            final String processDefinitionName) {

        Assert.notNull(orderModel, "Ordermodel cannot be null");
        Assert.notNull(processDefinitionName, "ProcessDefinitionName cannot be null");
        Assert.notNull(consignment, "consignment cannot be null");

        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + consignment.getCode() + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);

        orderProcessParameterHelper.setConsignment(orderProcess, consignment);
        orderProcessParameterHelper.setConsignmentRejectReason(orderProcess, rejectReason);

        startProcess(orderProcess);
        return orderProcess;
    }

    @Override
    public OrderProcessModel startOrderConsignmentPickedProcess(final OrderModel orderModel,
            final ConsignmentModel consignment,
            final Object consignmentUpdateData, final String processDefinitionName) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");
        Assert.notNull(processDefinitionName, "ProcessDefinitionName cannot be null");
        Assert.notNull(consignment, "consignment cannot be null");
        Assert.notNull(consignmentUpdateData, "consignmentUpdateData cannot be null");

        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + consignment.getCode() + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);

        orderProcessParameterHelper.setConsignment(orderProcess, consignment);
        orderProcessParameterHelper.setPickConsignmentUpdateData(orderProcess, consignmentUpdateData);

        startProcess(orderProcess);
        return orderProcess;
    }

    @Override
    public OrderProcessModel startOrderPaymentProcess(final OrderModel orderModel,
            final PaymentTransactionEntryModel ptem,
            final String processDefinitionName) {

        Assert.notNull(orderModel, "Ordermodel cannot be null");
        Assert.notNull(processDefinitionName, "ProcessDefinitionName cannot be null");
        Assert.notNull(ptem, "ptem cannot be null");

        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + ptem.getCode() + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);

        orderProcessParameterHelper.setPaymentTransactionEntry(orderProcess, ptem);

        startProcess(orderProcess);
        return orderProcess;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderProcessModel startOrderCancelProcess(final OrderModel orderModel,
            final OrderCancelRecordEntryModel ocrem, final BigDecimal refundAmount, final CancelTypeEnum cancelType,
            final boolean doCancelExtract, final List<PaymentTransactionEntryModel> refundPaymentEntries,
            final BigDecimal refundAmountRemaining) {
        return genericStartOrderCancelProcess(orderModel, null, null, ocrem, refundAmount, cancelType, doCancelExtract,
                refundPaymentEntries, refundAmountRemaining);
    }

    @Override
    public OrderProcessModel startConsignmentPickedCancelProcess(final OrderModel orderModel,
            final ConsignmentModel consignmentModel, final Object consignmentUpdateData,
            final OrderCancelRecordEntryModel ocrem,
            final BigDecimal refundAmount, final CancelTypeEnum cancelType,
            final List<PaymentTransactionEntryModel> refundPaymentEntries, final BigDecimal refundAmountRemaining) {

        return genericStartOrderCancelProcess(orderModel, consignmentModel, consignmentUpdateData, ocrem, refundAmount,
                cancelType, true, refundPaymentEntries, refundAmountRemaining);
    }

    private OrderProcessModel genericStartOrderCancelProcess(final OrderModel orderModel,
            final ConsignmentModel consignmentModel, final Object consignmentUpdateData,
            final OrderCancelRecordEntryModel ocrem,
            final BigDecimal refundAmount, final CancelTypeEnum cancelType, final boolean doCancelExtract,
            final List<PaymentTransactionEntryModel> refundPaymentEntries, final BigDecimal refundAmountRemaining) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");
        Assert.notNull(ocrem, "ocrem cannot be null");

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.CANCEL_ORDER_PROCESS;

        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + ocrem.getCode() + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);

        if (consignmentModel != null) {
            orderProcessParameterHelper.setConsignment(orderProcess, consignmentModel);
        }

        if (consignmentUpdateData != null) {
            orderProcessParameterHelper.setPickConsignmentUpdateData(orderProcess, consignmentUpdateData);
        }

        orderProcessParameterHelper.setOrderCancelRequest(orderProcess, ocrem);
        orderProcessParameterHelper.setRefundAmount(orderProcess, refundAmount);
        orderProcessParameterHelper.setCancelType(orderProcess, cancelType);
        orderProcessParameterHelper.setDoCancelExtract(orderProcess, doCancelExtract);
        orderProcessParameterHelper.setRefundPaymentEntryList(orderProcess, refundPaymentEntries);
        orderProcessParameterHelper.setRefundAmountRemaining(orderProcess, refundAmountRemaining);

        startProcess(orderProcess);
        return orderProcess;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderProcessModel startOrderRejectProcess(final OrderModel orderModel,
            final OrderCancelRecordEntryModel ocrem) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");
        Assert.notNull(ocrem, "ocrem cannot be null");

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.REJECT_ORDER_PROCESS;

        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + ocrem.getCode() + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);

        orderProcessParameterHelper.setOrderCancelRequest(orderProcess, ocrem);

        startProcess(orderProcess);
        return orderProcess;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderProcessModel startOrderRefundProcess(final OrderModel orderModel,
            final OrderReturnRecordEntryModel orderReturnRecordEntry, final BigDecimal refundAmount,
            final List<PaymentTransactionEntryModel> refundPaymentEntries, final BigDecimal refundAmountRemaining) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");
        Assert.notNull(orderReturnRecordEntry, "ocrem cannot be null");

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.REFUND_ORDER_PROCESS;

        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + orderReturnRecordEntry.getCode() + PROCESS_CODE_SEPERATOR
                + System.currentTimeMillis();
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);

        orderProcessParameterHelper.setReturnRequest(orderProcess, orderReturnRecordEntry);
        orderProcessParameterHelper.setRefundAmount(orderProcess, refundAmount);
        orderProcessParameterHelper.setRefundPaymentEntryList(orderProcess, refundPaymentEntries);
        orderProcessParameterHelper.setRefundAmountRemaining(orderProcess, refundAmountRemaining);

        startProcess(orderProcess);
        return orderProcess;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoreFrontCustomerProcessModel startCustomerRegistrationProcess(final CustomerModel customerModel,
            final BaseSiteModel baseSiteModel) {
        Assert.notNull(customerModel, "CustomerModel cannot be null");
        Assert.notNull(baseSiteModel, "BaseSiteModel cannot be null");

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.CUSTOMER_REGISTRATION_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + customerModel.getUid()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();

        final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel = createStoreFrontCustomerProcess(
                customerModel, baseSiteModel, processCode, processDefinitionName);

        startProcess(storeFrontCustomerProcessModel);
        return storeFrontCustomerProcessModel;
    }

    @Override
    public StoreFrontCustomerProcessModel startCustomerPasswordChangeProcess(final CustomerModel customerModel,
            final BaseSiteModel baseSiteModel) {
        Assert.notNull(customerModel, "Customermodel cannot be null");
        Assert.notNull(baseSiteModel, "BaseSitemodel cannot be null");

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.CUSTOMER_PASSWORD_CHANGE_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + customerModel.getUid()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();

        final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel = createStoreFrontCustomerProcess(
                customerModel, baseSiteModel, processCode, processDefinitionName);

        startProcess(storeFrontCustomerProcessModel);
        return storeFrontCustomerProcessModel;
    }

    private StoreFrontCustomerProcessModel createStoreFrontCustomerProcess(final CustomerModel customerModel,
            final BaseSiteModel baseSiteModel, final String processCode, final String processDefinitionName) {

        final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel = createProcess(processCode,
                processDefinitionName);
        storeFrontCustomerProcessModel.setSite(baseSiteModel);
        storeFrontCustomerProcessModel.setCustomer(customerModel);
        getModelService().save(storeFrontCustomerProcessModel);
        return storeFrontCustomerProcessModel;
    }

    private StoreFrontProcessModel createStoreFrontProcess(final BaseSiteModel baseSiteModel, final String processCode,
            final String processDefinitionName) {

        final StoreFrontProcessModel storeFrontProcessModel = createProcess(processCode,
                processDefinitionName);
        storeFrontProcessModel.setSite(baseSiteModel);
        getModelService().save(storeFrontProcessModel);
        return storeFrontProcessModel;
    }

    @Override
    public ForgottenPasswordProcessModel startForgottenPasswordProcess(final CustomerModel customerModel,
            final BaseSiteModel baseSiteModel, final String token) {
        Assert.notNull(customerModel, "CustomerModel cannot be null");
        Assert.notNull(baseSiteModel, "BaseSiteModel cannot be null");
        Assert.notNull(token, "token cannot be null");

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.FORGOTTEN_PASSWORD_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + customerModel.getUid()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();

        final ForgottenPasswordProcessModel forgottenPasswordProcessModel = createProcess(processCode,
                processDefinitionName);

        forgottenPasswordProcessModel.setSite(baseSiteModel);
        forgottenPasswordProcessModel.setCustomer(customerModel);
        forgottenPasswordProcessModel.setToken(token);
        getModelService().save(forgottenPasswordProcessModel);
        startProcess(forgottenPasswordProcessModel);

        return forgottenPasswordProcessModel;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UpdateCustomerEmailProcessModel startCustomerUpdateEmailAddrProcess(final CustomerModel customerModel,
            final BaseSiteModel baseSiteModel, final String oldEmailAddress) {
        Assert.notNull(customerModel, "CustomerModel cannot be null");
        Assert.notNull(baseSiteModel, "BaseSiteModel cannot be null");

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.CUSTOMER_UPDATE_EMAIL_ADDR_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + customerModel.getUid()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final UpdateCustomerEmailProcessModel updateCustomerEmailProcessModel = createProcess(processCode,
                processDefinitionName);

        updateCustomerEmailProcessModel.setSite(baseSiteModel);
        updateCustomerEmailProcessModel.setCustomer(customerModel);
        updateCustomerEmailProcessModel.setOldEmailAddress(oldEmailAddress);
        getModelService().save(updateCustomerEmailProcessModel);

        startProcess(updateCustomerEmailProcessModel);

        return updateCustomerEmailProcessModel;
    }


    private OrderProcessModel createOrderProcess(final OrderModel orderModel, final String processCode,
            final String processDefinitionName) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");
        Assert.notNull(processCode, "ProcessCode cannot be null");
        Assert.notNull(processDefinitionName, "ProcessDefinitionName cannot be null");

        final OrderProcessModel orderProcess = createProcess(processCode, processDefinitionName);
        orderProcess.setOrder(orderModel);
        getModelService().save(orderProcess);
        return orderProcess;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderProcessModel startPOSAddressChangeProcess(final OrderModel orderModel) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.POS_ADDRESS_CHANGE_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();

        final OrderProcessModel posAddressChangeProcess = createOrderProcess(orderModel, processCode,
                processDefinitionName);

        startProcess(posAddressChangeProcess);
        return posAddressChangeProcess;
    }

    @Override
    public OrderProcessModel startResendTaxInvoiceProcess(final OrderModel orderModel, final String newEmailAddress) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.RESEND_TAX_INVOICE_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();

        final OrderProcessModel process = createOrderProcess(orderModel, processCode,
                processDefinitionName);


        if (StringUtils.isNotEmpty(newEmailAddress)
                && TargetValidationCommon.Email.PATTERN.matcher(newEmailAddress).matches()) {
            orderProcessParameterHelper.setNewEmailAddr(process, newEmailAddress);
        }

        startProcess(process);
        return process;
    }

    @Override
    public OrderProcessModel startResendTaxInvoiceProcess(final OrderModel orderModel) {
        return startResendTaxInvoiceProcess(orderModel, null);
    }

    /**
     * {@inheritDoc} This implementation stores only last child process code into context of {@code current} process.
     * 
     * @param child
     *            the spawned child process
     * @param current
     *            the process that is currently running, or a parent process
     */
    @Override
    public void registerChildProcess(final BusinessProcessModel child, final BusinessProcessModel current) {
        orderProcessParameterHelper.setProcessParameter(current, LAST_PROCESS_CODE_KEY, child.getCode());
        getModelService().refresh(current);
    }


    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtbusproc.TargetBusinessProcessService#resendConsignmentProcess(de.hybris.platform.core.model.order.OrderModel, de.hybris.platform.ordersplitting.model.ConsignmentModel)
     */
    @Override
    public OrderProcessModel resendConsignmentProcess(final ConsignmentModel consignment) {
        Assert.notNull(consignment, "Consignment can not be null");

        final OrderModel orderModel = (OrderModel)consignment.getOrder();

        LOG.info("Starting the process to resend the consignment to Warehouse for consignment : "
                + consignment.getCode());

        return startOrderConsignmentProcess(orderModel, consignment,
                TgtbusprocConstants.BusinessProcess.RESEND_CONSIGNMENT_TO_WAREHOUSE);
    }


    @Override
    public OrderProcessModel startSendClickAndCollectNotificationProcess(final OrderModel orderModel,
            final Object cncNotificationData, final boolean partnerUpdateRequired) {

        Assert.notNull(orderModel, "Ordermodel cannot be null");
        Assert.notNull(cncNotificationData, "cncNotificationData cannot be null");
        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.SEND_CLICK_AND_COLLECT_NOTIFICATION_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);
        orderProcessParameterHelper.setCncNotificationData(orderProcess, cncNotificationData);
        orderProcessParameterHelper.setPartnerUpdateRequired(orderProcess, partnerUpdateRequired);
        startProcess(orderProcess);
        return orderProcess;
    }

    @Override
    public BusinessProcessModel startCustomerSubscriptionProcess(final Object subscriptionDetails,
            final String processDefinitionName)
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Assert.notNull(subscriptionDetails, "subscriptionDetails DTO cannot be null");
        final Object email = PropertyUtils.getProperty(subscriptionDetails, "customerEmail");
        Assert.notNull(email, "email must not be null");
        Assert.state(email instanceof String, "email address in DTO is not a String");
        final String customerEmail = (String)email;
        Assert.state(StringUtils.isNotEmpty(customerEmail), "email cannot be empty");
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + customerEmail
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final BusinessProcessModel processModel = createProcess(processCode, processDefinitionName);
        orderProcessParameterHelper.setCustomerSubscriptionData(processModel, subscriptionDetails);
        getModelService().save(processModel);
        startProcess(processModel);
        return processModel;
    }

    @Override
    public BusinessProcessModel startSendSmsToStoreForOpenOrdersProcess(final SendSmsToStoreData sendSmsToStoreData) {

        Assert.notNull(sendSmsToStoreData, "sendSmsToStoreData cannot be null");
        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.SEND_SMS_TO_STORE_FOR_OPEN_ORDERS;
        final String processCode = processDefinitionName
                + PROCESS_CODE_SEPERATOR + sendSmsToStoreData.getMobileNumber()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final BusinessProcessModel process = createProcess(processCode,
                processDefinitionName);
        orderProcessParameterHelper.setSendSmsToStoreData(process,
                sendSmsToStoreData);
        startProcess(process);
        return process;
    }

    @Override
    public BusinessProcessModel startGiftCardReverseProcess(final Object giftcardReverseDetails,
            final String email, final String receipt,
            final PaymentTransactionEntryModel captureEntry) {
        Assert.notNull(giftcardReverseDetails, "giftcardReverseDetails DTO cannot be null");
        Assert.notNull(email, "email must not be null");
        Assert.state(StringUtils.isNotEmpty(email), "email cannot be empty");
        final String processCode = BusinessProcess.REVERSE_GIFTCARD_PROCESS + PROCESS_CODE_SEPERATOR + email
                + PROCESS_CODE_SEPERATOR + receipt;
        BusinessProcessModel processModel = getProcess(processCode);
        if (processModel == null) {
            processModel = createStoreFrontProcess(baseSiteService.getCurrentBaseSite(),
                    processCode,
                    BusinessProcess.REVERSE_GIFTCARD_PROCESS);
            orderProcessParameterHelper.setGiftCardReversalData(processModel,
                    giftcardReverseDetails);
            if (captureEntry != null) {
                orderProcessParameterHelper.setGiftCardReversalCaptureEntry(processModel,
                        captureEntry);
            }
            getModelService().save(processModel);
            startProcess(processModel);
        }
        return processModel;
    }

    @Override
    public OrderProcessModel startSendClickAndCollectPickedupNotificationProcess(final OrderModel orderModel,
            final boolean partnerUpdateRequired) {
        Assert.notNull(orderModel, "Order can't be null");
        final String processDefinitionName = BusinessProcess.SEND_CLICK_AND_COLLECT_PICKED_UP_NOTIFICATION_PROCESS;
        final StringBuilder processCode = new StringBuilder();
        processCode.append(processDefinitionName).append(PROCESS_CODE_SEPERATOR).append(orderModel.getCode())
                .append(PROCESS_CODE_SEPERATOR).append(System.currentTimeMillis());
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode.toString(),
                processDefinitionName);

        orderProcessParameterHelper.setPartnerUpdateRequired(orderProcess, partnerUpdateRequired);
        startProcess(orderProcess);
        return orderProcess;
    }

    @Override
    public OrderProcessModel startSendClickAndCollectReturnedToFloorNotificationProcess(final OrderModel orderModel) {
        Assert.notNull(orderModel, "Order can't be null");
        final String processDefinitionName = BusinessProcess.SEND_CLICK_AND_COLLECT_RETURNED_TO_FLOOR_NOTIFICATION_PROCESS;
        final StringBuilder processCode = new StringBuilder();
        processCode.append(processDefinitionName).append(PROCESS_CODE_SEPERATOR).append(orderModel.getCode())
                .append(PROCESS_CODE_SEPERATOR).append(System.currentTimeMillis());
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode.toString(),
                processDefinitionName);
        startProcess(orderProcess);
        return orderProcess;
    }

    @Override
    public OrderProcessModel startSendEmailWithBlackoutProcess(final OrderModel orderModel, final Object cncData) {

        Assert.notNull(orderModel, "Ordermodel cannot be null");
        Assert.notNull(cncData, "cncNotificationData cannot be null");

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.SEND_EMAIL_WITH_BLACKOUT_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);
        orderProcessParameterHelper.setCncNotificationData(orderProcess, cncData);
        startProcess(orderProcess);

        return orderProcess;
    }


    @Override
    public OrderProcessModel startOrderItemCancelProcess(final OrderModel orderModel,
            final TargetOrderCancelEntryList cancelEntryList) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");
        Assert.notNull(cancelEntryList, "TargetOrderCancelEntryList cannot be null");

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.CANCEL_ORDER_ITEMS_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);
        orderProcessParameterHelper.setTargetOrderCancelEntryList(orderProcess, cancelEntryList);
        startProcess(orderProcess);

        return orderProcess;
    }

    @Override
    public OrderProcessModel startFluentOrderItemCancelProcess(final OrderModel orderModel,
            final TargetOrderCancelEntryList cancelEntryList) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");
        Assert.notNull(cancelEntryList, "TargetOrderCancelEntryList cannot be null");

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.FLUENT_CANCEL_ORDER_ITEMS_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);
        orderProcessParameterHelper.setTargetOrderCancelEntryList(orderProcess, cancelEntryList);
        startProcess(orderProcess);

        return orderProcess;
    }

    @Override
    public OrderProcessModel startFluentOrderConsignmentPickedProcess(final OrderModel orderModel,
            final ConsignmentModel consignment,
            final String processDefinitionName) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");
        Assert.notNull(processDefinitionName, "ProcessDefinitionName cannot be null");
        Assert.notNull(consignment, "consignment cannot be null");
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + consignment.getCode() + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);
        orderProcessParameterHelper.setConsignment(orderProcess, consignment);
        startProcess(orderProcess);
        return orderProcess;

    }

    @Override
    public OrderProcessModel startFluentOrderConsignmentShippedProcess(final OrderModel orderModel,
            final TargetConsignmentModel consignment,
            final String processDefinitionName) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");
        Assert.notNull(processDefinitionName, "ProcessDefinitionName cannot be null");
        Assert.notNull(consignment, "consignment cannot be null");
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + consignment.getCode() + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);
        orderProcessParameterHelper.setConsignment(orderProcess, consignment);
        startProcess(orderProcess);
        return orderProcess;

    }

    @Override
    public OrderProcessModel startFluentOrderShippedProcess(final OrderModel orderModel,
            final String processDefinitionName) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");
        Assert.notNull(processDefinitionName, "ProcessDefinitionName cannot be null");
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);
        startProcess(orderProcess);
        return orderProcess;

    }

    /**
     * @return the baseSiteService
     */
    public BaseSiteService getBaseSiteService() {
        return baseSiteService;
    }

    /**
     * @param baseSiteService
     *            the baseSiteService to set
     */
    @Required
    public void setBaseSiteService(final BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }



    /* (non-Javadoc)
     * @see au.com.target.tgtbusproc.TargetBusinessProcessService#startFluentReleasePreOrderProcess(de.hybris.platform.core.model.order.OrderModel, java.lang.String)
     */
    @Override
    public OrderProcessModel startFluentReleasePreOrderProcess(final OrderModel orderModel) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");
        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.FLUENT_RELEASE_PREORDER_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);
        startProcess(orderProcess);
        return orderProcess;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtbusproc.TargetBusinessProcessService#startFluentPreOrderReleaseAcceptProcess(de.hybris.platform.core.model.order.OrderModel)
     */
    @Override
    public OrderProcessModel startFluentPreOrderReleaseAcceptProcess(final OrderModel orderModel) {
        Assert.notNull(orderModel, "Ordermodel cannot be null");
        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.FLUENT_PREORDER_RELEASE_ACCEPT_PROCESS;
        final String processCode = processDefinitionName + PROCESS_CODE_SEPERATOR + orderModel.getCode()
                + PROCESS_CODE_SEPERATOR + System.currentTimeMillis();
        final OrderProcessModel orderProcess = createOrderProcess(orderModel, processCode, processDefinitionName);
        startProcess(orderProcess);
        return orderProcess;
    }

}
