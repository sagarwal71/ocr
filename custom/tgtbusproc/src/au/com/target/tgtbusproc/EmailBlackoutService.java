package au.com.target.tgtbusproc;


/**
 * 
 * @author Trinadh N
 *
 *         Service to check blackout time
 */
public interface EmailBlackoutService {

    /**
     * checks current time against config values
     * 
     * @return true/false
     */
    boolean isEmailBlackout();
}
