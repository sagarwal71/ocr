package au.com.target.tgtbusproc;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.model.process.ForgottenPasswordProcessModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.returns.model.OrderReturnRecordEntryModel;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.List;

import au.com.target.tgtbusproc.model.UpdateCustomerEmailProcessModel;
import au.com.target.tgtbusproc.sms.data.SendSmsToStoreData;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.CancelTypeEnum;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntryList;


/**
 * Manage Target related business processes. Convenient wrapper around {@link BusinessProcessService} to create and
 * start business processes defined in {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess}
 */
public interface TargetBusinessProcessService extends BusinessProcessService {

    /**
     * Create and starts {@link OrderProcessModel} based on process definition and generates the process code internally
     * 
     * @param orderModel
     * @param processDefinitionName
     *            Name of process definition used to create process (same like in process xml file).
     * @return created process
     */
    OrderProcessModel startOrderProcess(final OrderModel orderModel, final String processDefinitionName);

    /**
     * Create and starts {@link OrderProcessModel} based on process definition and generates the process code internally
     * 
     * @param orderModel
     * @param processDefinitionName
     *            Name of process definition used to create process (same like in process xml file).
     * @return created process
     */
    OrderProcessModel startOrderProcessWithConsignment(final OrderModel orderModel, final ConsignmentModel consignment,
            final String processDefinitionName);

    /**
     * Create and starts {@link OrderProcessModel} based on process definition and processcode.
     * 
     * @param orderModel
     * @param processCode
     * @param processDefinitionName
     *            Name of process definition used to create process (same like in process xml file).
     * @return created process
     */
    OrderProcessModel startOrderProcess(final OrderModel orderModel, final String processCode,
            final String processDefinitionName);


    /**
     * Create and starts {@link OrderProcessModel} using the process definition
     * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#PLACE_ORDER_PROCESS} and the
     * optional PaymentTransactionEntryModel parameter.
     * 
     * @param orderModel
     * @param ptem
     * @return created process
     */
    OrderProcessModel startPlaceOrderProcess(OrderModel orderModel, final PaymentTransactionEntryModel ptem);

    /**
     * Create and starts {@link OrderProcessModel} using the process definition
     * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#PLACE_PREORDER_PROCESS} and the
     * optional PaymentTransactionEntryModel parameter.
     * 
     * @param orderModel
     * @param ptem
     * @return created process
     */
    OrderProcessModel startPlacePreOrderProcess(OrderModel orderModel, final PaymentTransactionEntryModel ptem);

    /**
     * Create and starts {@link OrderProcessModel} using the process definition
     * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#PLACE_FLUENT_ORDER_PROCESS} and the
     * optional PaymentTransactionEntryModel parameter.
     * 
     * @param orderModel
     * @param ptem
     * @return created process
     */
    OrderProcessModel startPlaceFluentOrderProcess(OrderModel orderModel, PaymentTransactionEntryModel ptem);

    /**
     * Create and starts {@link OrderProcessModel} using the process definition
     * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#PLACE_FLUENT_PREORDER_PROCESS} and
     * the optional PaymentTransactionEntryModel parameter.
     *
     * @param orderModel
     * @param ptem
     * @return created process
     */
    OrderProcessModel startPlaceFluentPreOrderProcess(OrderModel orderModel, PaymentTransactionEntryModel ptem);


    /**
     * Create and starts {@link OrderProcessModel} using the process definition
     * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#ACCEPT_ORDER_PROCESS}
     * 
     * @param orderModel
     * @return created process
     */
    OrderProcessModel startOrderAcceptProcess(OrderModel orderModel);

    /**
     * Create and starts {@link OrderProcessModel} using the process definition
     * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#ACCEPT_FLUENT_ORDER_PROCESS}
     * 
     * @param orderModel
     * @return created process
     */
    OrderProcessModel startFluentOrderAcceptProcess(OrderModel orderModel);

    /**
     * Create and starts {@link OrderProcessModel} using the process definition
     * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#FLUENT_ACCEPT_PREORDER_PROCESS}
     *
     * @param orderModel
     * @return created process
     */
    OrderProcessModel startFluentAcceptPreOrderProcess(OrderModel orderModel);

    /**
     * Create and starts {@link OrderProcessModel} using the process definition
     * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#ACCEPT_PREORDER_PROCESS}
     * 
     * @param orderModel
     * @return created process
     */
    OrderProcessModel startPreOrderAcceptProcess(OrderModel orderModel);

    /**
     * Create and starts {@link OrderProcessModel} given a ConsignmentModel parameter.
     *
     * @param orderModel
     * @param consignment
     * @param processDefinitionName
     * @return created process
     */
    OrderProcessModel startOrderConsignmentProcess(OrderModel orderModel, ConsignmentModel consignment,
            String processDefinitionName);



    /**
     * Create and starts {@link OrderProcessModel} to resend to warehouse given the ConsignmentModel parameter.
     * 
     * @param consignment
     * @return created process
     */
    OrderProcessModel resendConsignmentProcess(ConsignmentModel consignment);

    /**
     * Create and starts {@link OrderProcessModel} given a ConsignmentModel parameter.
     * 
     * @param orderModel
     * @param consignment
     * @param consignmentUpdateData
     * @param processDefinitionName
     * @return created process
     */
    OrderProcessModel startOrderConsignmentPickedProcess(OrderModel orderModel, ConsignmentModel consignment,
            Object consignmentUpdateData, String processDefinitionName);

    /**
     * Create and starts {@link OrderProcessModel} given a PaymentTransactionEntryModel parameter.
     * 
     * @param orderModel
     * @param ptem
     * @param processDefinitionName
     * @return created process
     */
    OrderProcessModel startOrderPaymentProcess(OrderModel orderModel, final PaymentTransactionEntryModel ptem,
            String processDefinitionName);

    /**
     * Create and starts {@link OrderProcessModel} using the process definition
     * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#CANCEL_ORDER_PROCESS} and the given
     * OrderCancelRecordEntry parameter.
     * 
     * @param orderModel
     * @param ocrem
     * @param refundAmount
     * @param doCancelExtract
     * @param refundAmountRemaining
     * @return created process
     */
    OrderProcessModel startOrderCancelProcess(OrderModel orderModel, final OrderCancelRecordEntryModel ocrem,
            final BigDecimal refundAmount, final CancelTypeEnum cancelType, final boolean doCancelExtract,
            List<PaymentTransactionEntryModel> refundPaymentEntries, BigDecimal refundAmountRemaining);

    /**
     * Create and starts {@link OrderProcessModel} given ConsignmentModel and consignmentUpdateData.
     * 
     * @param orderModel
     * @param consignmentModel
     * @param consignmentUpdateData
     * @param ocrem
     * @param refundAmount
     * @param cancelType
     * @param refundPaymentEntries
     * @param refundAmountRemaining
     * @return created process
     */
    OrderProcessModel startConsignmentPickedCancelProcess(OrderModel orderModel, ConsignmentModel consignmentModel,
            Object consignmentUpdateData, OrderCancelRecordEntryModel ocrem, BigDecimal refundAmount,
            CancelTypeEnum cancelType,
            List<PaymentTransactionEntryModel> refundPaymentEntries, BigDecimal refundAmountRemaining);

    /**
     * Create and starts {@link OrderProcessModel} using the process definition
     * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#REJECT_ORDER_PROCESS} and the given
     * OrderCancelRecordEntry parameter.
     * 
     * @param orderModel
     * @param ocrem
     * @return created process
     */
    OrderProcessModel startOrderRejectProcess(OrderModel orderModel, final OrderCancelRecordEntryModel ocrem);

    /**
     * Create and starts {@link OrderProcessModel} using the process definition
     * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#REFUND_ORDER_PROCESS} and the given
     * OrderReturnRecordEntryModel parameter.
     * 
     * @param order
     * @param orderReturnRecordEntry
     * @param refundAmount
     * @param refundPaymentEntries
     * @param refundAmountRemaining
     */
    OrderProcessModel startOrderRefundProcess(OrderModel order, OrderReturnRecordEntryModel orderReturnRecordEntry,
            final BigDecimal refundAmount, final List<PaymentTransactionEntryModel> refundPaymentEntries,
            BigDecimal refundAmountRemaining);

    /**
     * Create and starts {@link OrderProcessModel} using the process definition
     * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#PAYMENT_FAILED_PROCESS}
     * 
     * @param orderModel
     * @param reason
     * @return created process
     */
    OrderProcessModel startPaymentFailedProcess(OrderModel orderModel, String reason);

    /**
     * Create and starts {@link StoreFrontCustomerProcessModel} using the process definition
     * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#CUSTOMER_REGISTRATION_PROCESS}
     * 
     * @param customerModel
     * @param baseSiteModel
     * @return created process
     */
    StoreFrontCustomerProcessModel startCustomerRegistrationProcess(CustomerModel customerModel,
            BaseSiteModel baseSiteModel);

    /**
     * Create and starts {@link ForgottenPasswordProcessModel} using the process definition
     * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#FORGOTTEN_PASSWORD_PROCESS}
     * 
     * @param customerModel
     * @param baseSiteModel
     * @param token
     * @return created process
     */
    ForgottenPasswordProcessModel startForgottenPasswordProcess(CustomerModel customerModel,
            BaseSiteModel baseSiteModel, String token);

    /**
     * Create and starts {@link StoreFrontCustomerProcessModel} using the process definition
     * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#CUSTOMER_PASSWORD_CHANGE_PROCESS}
     * 
     * @param customerModel
     * @param baseSiteModel
     * @return created process
     */
    StoreFrontCustomerProcessModel startCustomerPasswordChangeProcess(CustomerModel customerModel,
            BaseSiteModel baseSiteModel);

    /**
     * Create and starts {@link StoreFrontCustomerProcessModel} using the process definition
     * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#CUSTOMER_UPDATE_EMAIL_ADDR_PROCESS}
     * 
     * @param customerModel
     * @param baseSiteModel
     * @param oldEmailAddress
     * @return created process
     */
    UpdateCustomerEmailProcessModel startCustomerUpdateEmailAddrProcess(CustomerModel customerModel,
            BaseSiteModel baseSiteModel, String oldEmailAddress);

    /**
     * Create and starts {@link OrderProcessModel} using the process definition
     * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#POS_ADDRESS_CHANGE_PROCESS}
     * 
     * @param orderModel
     * @return created process
     */
    OrderProcessModel startPOSAddressChangeProcess(OrderModel orderModel);

    /**
     * Registers a new {@code child} to a {@code current} process. Might be required for a parent processes to wait for
     * a child to finish (synchronous flow).
     * 
     * @param child
     *            the spawned child process
     * @param current
     *            the process that is currently running, or a parent process
     */
    void registerChildProcess(BusinessProcessModel child, BusinessProcessModel current);

    /**
     * start off resentTaxInvoiceProcess
     * 
     * @param orderModel
     * @return OrderProcessModel
     */
    OrderProcessModel startResendTaxInvoiceProcess(OrderModel orderModel);

    /**
     * start off resentTaxInvoiceProcess with a new Email Address
     * 
     * @param orderModel
     * @param newEmailAddress
     * @return OrderProcessModel
     */
    OrderProcessModel startResendTaxInvoiceProcess(OrderModel orderModel, String newEmailAddress);

    /**
     * start the cnc notification process with cnc data
     * 
     * @param orderModel
     * @param cncData
     * @param partnerUpdateRequired
     * @return OrderProcessModel
     */
    OrderProcessModel startSendClickAndCollectNotificationProcess(OrderModel orderModel, Object cncData,
            boolean partnerUpdateRequired);

    /**
     * start the customer subscription process with the subscription DTO
     * 
     * @param subscriptionDetails
     * @param processDefinitionName
     * @return BusinessProcessModel
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    BusinessProcessModel startCustomerSubscriptionProcess(Object subscriptionDetails, String processDefinitionName)
            throws IllegalAccessException,
            InvocationTargetException, NoSuchMethodException;

    /**
     * start the order consignment process in the case of a rejection of the consignment
     * 
     * @param orderModel
     * @param consignment
     * @param rejectReason
     * @param processDefinitionName
     * @return OrderProcess Model
     */
    OrderProcessModel startConsignmentRejectionProcess(OrderModel orderModel, ConsignmentModel consignment,
            ConsignmentRejectReason rejectReason, String processDefinitionName);

    /**
     * start the gift card reverse process with the gift card reverse DTO.
     * 
     * @param giftcardReverseDetails
     * @param email
     * @param receipt
     * @param captureEntry
     */
    BusinessProcessModel startGiftCardReverseProcess(Object giftcardReverseDetails, String email, String receipt,
            PaymentTransactionEntryModel captureEntry);

    /**
     * start the send sms to store process
     * 
     * @param sendSmsToStoreData
     * @return BusinessProcessModel
     */
    BusinessProcessModel startSendSmsToStoreForOpenOrdersProcess(
            SendSmsToStoreData sendSmsToStoreData);

    /**
     * Start send click and collect picked up notification process
     * 
     * @param orderModel
     * @param partnerUpdateRequired
     * @return OrderProcessModel
     */
    OrderProcessModel startSendClickAndCollectPickedupNotificationProcess(OrderModel orderModel,
            boolean partnerUpdateRequired);

    /**
     * Start send click and collect return to floor notification process
     * 
     * @param orderModel
     * @return OrderProcessModel
     */
    OrderProcessModel startSendClickAndCollectReturnedToFloorNotificationProcess(OrderModel orderModel);

    /**
     * Start send email with blackout process action
     * 
     * @param orderModel
     * @param cncData
     * @return OrderProcessModel
     */
    OrderProcessModel startSendEmailWithBlackoutProcess(OrderModel orderModel, Object cncData);

    /**
     * start Order Item Cancel Process
     * 
     * @param orderModel
     * @param cancelEntryList
     * @return OrderProcessModel
     */
    OrderProcessModel startOrderItemCancelProcess(OrderModel orderModel, TargetOrderCancelEntryList cancelEntryList);

    /**
     * Start Fluent Order Item Cancel Process
     * 
     * @param orderModel
     * @param cancelEntryList
     * @return OrderProcessModel
     */
    OrderProcessModel startFluentOrderItemCancelProcess(OrderModel orderModel,
            TargetOrderCancelEntryList cancelEntryList);

    /**
     * Create and starts {@link OrderProcessModel} given a ConsignmentModel parameter.
     * 
     * @param orderModel
     * @param consignment
     * @param processDefinitionName
     * @return created process
     */
    OrderProcessModel startFluentOrderConsignmentPickedProcess(OrderModel orderModel, ConsignmentModel consignment,
            String processDefinitionName);

    /**
     * Create and starts shipped {@link OrderProcessModel} given a ConsignmentModel parameter
     * 
     * @param order
     * @param consignment
     * @param processDefinitionName
     */
    OrderProcessModel startFluentOrderConsignmentShippedProcess(OrderModel order, TargetConsignmentModel consignment,
            String processDefinitionName);

    /**
     * start fluent order shipped {@link OrderProcessModel} process
     * 
     * @param orderModel
     * @param processDefinitionName
     */
    OrderProcessModel startFluentOrderShippedProcess(OrderModel orderModel, String processDefinitionName);

    /**
     * start fluent pre-order release {@link OrderProcessModel} process
     * 
     * @param orderModel
     */
    OrderProcessModel startFluentReleasePreOrderProcess(OrderModel orderModel);

    /**
     * start fluent pre-order release accept {@link OrderProcessModel} process
     * 
     * @param orderModel
     * @return orderProcessModel
     */
    OrderProcessModel startFluentPreOrderReleaseAcceptProcess(OrderModel orderModel);
}
