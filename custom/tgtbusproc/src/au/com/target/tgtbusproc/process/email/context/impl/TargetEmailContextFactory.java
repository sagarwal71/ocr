/**
 * 
 */
package au.com.target.tgtbusproc.process.email.context.impl;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.acceleratorservices.process.email.context.impl.DefaultEmailContextFactory;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import au.com.target.tgtbusproc.process.exception.TargetBeanNotFoundException;


/**
 * @author asingh78
 * 
 */
public class TargetEmailContextFactory extends DefaultEmailContextFactory {

    private static final Logger LOG = Logger.getLogger(TargetEmailContextFactory.class);

    /**
     * Create and initialise the email context, and populate the emailContextVariables defined in the bean definition.
     * 
     * This override exists solely because we need the emailContextVariables to be populated BEFORE the CMS slots are
     * rendered.
     */
    @Override
    public AbstractEmailContext create(final BusinessProcessModel businessProcessModel,
            final EmailPageModel emailPageModel,
            final RendererTemplateModel renderTemplate) throws TargetBeanNotFoundException
    {
        final AbstractEmailContext emailContext = resolveEmailContext(renderTemplate);
        emailContext.init(businessProcessModel, emailPageModel);
        parseVariablesIntoEmailContext(emailContext);

        renderCMSSlotsIntoEmailContext(emailContext, emailPageModel, businessProcessModel);

        final String languageIso = emailContext.getEmailLanguage() == null ? null : emailContext.getEmailLanguage()
                .getIsocode();
        //Render translated messages from the email message resource bundles into the email context.
        emailContext.setMessages(getEmailTemplateTranslationStrategy().translateMessagesForTemplate(renderTemplate,
                languageIso));

        return emailContext;
    }

    @Override
    protected <T extends AbstractEmailContext<BusinessProcessModel>> T resolveEmailContext(
            final RendererTemplateModel renderTemplate)
            throws TargetBeanNotFoundException
    {
        try
        {
            final Class<T> contextClass = (Class<T>)Class.forName(renderTemplate.getContextClass());
            Map<String, T> emailContexts = getApplicationContext().getBeansOfType(contextClass);
            if (MapUtils.isNotEmpty(emailContexts))
            {
                return emailContexts.entrySet().iterator().next().getValue();
            }
            else
            {
                emailContexts = getCoreApplicationContext().getBeansOfType(contextClass);
                if (MapUtils.isNotEmpty(emailContexts))
                {
                    return emailContexts.entrySet().iterator().next().getValue();
                }
                else {
                    throw new TargetBeanNotFoundException("Cannot find bean in application context for context class ["
                            + contextClass + "]");
                }
            }
        }
        catch (final ClassNotFoundException e)
        {
            LOG.error("failed to create email context", e);
            throw new TargetBeanNotFoundException("Cannot find email context class", e);
        }
    }

    /**
     * Gets the core application context.
     *
     * @return the core application context
     */
    private ApplicationContext getCoreApplicationContext() {
        return Registry.getCoreApplicationContext();
    }
}
