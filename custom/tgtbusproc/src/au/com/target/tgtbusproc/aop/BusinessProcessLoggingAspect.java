/**
 * 
 */
package au.com.target.tgtbusproc.aop;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;

import au.com.target.tgtbusproc.exceptions.BusinessProcessException;
import au.com.target.tgtutility.constants.TgtutilityConstants;


public class BusinessProcessLoggingAspect {
    public static final String LOG_FORMAT_BEGIN = "Begin Action: [ %s ], BusinessProcessCode: [ %s], OrderCode:[ %s ]";
    public static final String LOG_FORMAT_FINISH =
            "Finish Action: [ %s ], BusinessProcessCode: [ %s], OrderCode:[ %s ]";
    public static final String ERROR_MESSAGE_FORMAT =
            "ERROR Message: [ %s ], ERROR Code: [ %s ],  Action: [ %s ], BusinessProcessCode: [ %s], OrderCode:[ %s ]";

    private static final Logger LOG = Logger.getLogger(BusinessProcessLoggingAspect.class);

    /**
     * After any Business Process throw an exception, this method will log down the error message in specific format
     * 
     * @param joinPoint
     * @param error
     */
    public void afterThrowing(final JoinPoint joinPoint, final Throwable error) {
        String errorCode = "n/a";

        final LogInfo logInfo = getActionNameAndProcessCode(joinPoint);

        if (error instanceof BusinessProcessException) {
            errorCode = ((BusinessProcessException)error).getErrorCode();
        }
        else if (error instanceof RetryLaterException) {
            errorCode = TgtutilityConstants.ErrorCode.INFO_RETRY;
        }
        else {
            errorCode = TgtutilityConstants.ErrorCode.ERR_GENERIC;
        }

        LOG.info(String.format(ERROR_MESSAGE_FORMAT, error.getMessage(),
                errorCode, logInfo.action,
                logInfo.processCode, logInfo.orderCode));
    }

    /**
     * 
     * @param joinPoint
     */
    public void beforeAction(final JoinPoint joinPoint) {
        final LogInfo logInfo = getActionNameAndProcessCode(joinPoint);
        LOG.info(String.format(LOG_FORMAT_BEGIN, logInfo.action, logInfo.processCode, logInfo.orderCode));
    }

    /**
     * 
     * @param joinPoint
     */
    public void afterAction(final JoinPoint joinPoint) {
        final LogInfo logInfo = getActionNameAndProcessCode(joinPoint);
        LOG.info(String.format(LOG_FORMAT_FINISH, logInfo.action, logInfo.processCode, logInfo.orderCode));
    }

    private LogInfo getActionNameAndProcessCode(final JoinPoint joinPoint) {

        final LogInfo logInfo = new LogInfo();

        final Object target = joinPoint.getTarget();

        if (target != null) {
            logInfo.action = target.getClass().getSimpleName();
        }

        final Object[] args = joinPoint.getArgs();
        if (args != null && args.length > 0) {
            final Object arg1 = args[0];
            if (arg1 instanceof BusinessProcessModel) {
                final BusinessProcessModel process = (BusinessProcessModel)arg1;
                logInfo.processCode = process.getCode();

                if (process instanceof OrderProcessModel) {
                    final OrderProcessModel orderProcess = (OrderProcessModel)process;
                    logInfo.orderCode = orderProcess.getOrder() == null ? "n/a" : orderProcess.getOrder().getCode();
                }
            }
        }

        return logInfo;
    }

    private class LogInfo {

        private String action = "n/a";
        private String processCode = "n/a";
        private String orderCode = "n/a";
    }
}
