/**
 * 
 */
package au.com.target.tgtbusproc.sms.data;

import java.io.Serializable;


/**
 * @author Vivek
 *
 */
public class SendSmsToStoreData implements Serializable {

    private String mobileNumber;
    private String storeNumber;
    private int openOrders;

    /**
     * @return the mobileNumber
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * @param mobileNumber
     *            the mobileNumber to set
     */
    public void setMobileNumber(final String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * @return the storeNumber
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the openOrders
     */
    public int getOpenOrders() {
        return openOrders;
    }

    /**
     * @param openOrders
     *            the openOrders to set
     */
    public void setOpenOrders(final int openOrders) {
        this.openOrders = openOrders;
    }

}
