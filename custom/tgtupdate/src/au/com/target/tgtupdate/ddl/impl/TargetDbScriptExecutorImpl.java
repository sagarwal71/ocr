/**
 * 
 */
package au.com.target.tgtupdate.ddl.impl;

import de.hybris.bootstrap.ddl.tools.SqlScriptParser;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.jdbc.core.JdbcTemplate;

import au.com.target.tgtupdate.ddl.TargetDbScriptExecutor;


/**
 * @author htan3
 *
 */
public class TargetDbScriptExecutorImpl implements TargetDbScriptExecutor {

    private static final Logger LOG = Logger.getLogger(TargetDbScriptExecutorImpl.class);

    private JdbcTemplate jdbcTemplate;

    private SqlScriptParser sqlScriptParser = SqlScriptParser.getDefaultSqlScriptParser(";");

    /**
     * execute custom ddl script.
     * 
     * @param scriptPath
     * @return true if the script run successfully.
     */
    @Override
    public boolean executeDdl(final String scriptPath) {
        LOG.info("Executing custom DDL Statements in batch mode as part of tgtupdate, script path :" + scriptPath);
        boolean resultStatus = false;
        try {
            final Reader scriptReader = new InputStreamReader(this.getClass().getResourceAsStream(scriptPath));
            final Iterable statements = sqlScriptParser.parse(scriptReader);
            resultStatus = executeBatch(statements);
        }
        catch (final Exception e) {
            LOG.error("Failed to read resource from path:" + scriptPath);
        }
        return resultStatus;
    }

    /**
     * execute batch sql statements
     * 
     * @param statements
     * @return true or false
     */
    private boolean executeBatch(final Iterable<String> statements) {
        boolean status = true;
        final String[] statementArray = readBatch(statements);
        if (ArrayUtils.isEmpty(statementArray)) {
            LOG.error("no valid sql statements found.");
            status = false;
        }
        else {
            try {
                jdbcTemplate.batchUpdate(statementArray);
                LOG.info("Executed " + statementArray.length + " Statements");

            }
            catch (final Exception e) {
                LOG.error("problem executing sql", e);
                status = false;
            }
        }
        return status;
    }

    /**
     * read batch from statements iterator.
     * 
     * @param statements
     * @return statements in string array
     */
    private String[] readBatch(final Iterable<String> statements) {
        final List batch = new ArrayList();
        for (final String statement : statements) {
            batch.add(statement);
        }
        return (String[])batch.toArray(new String[batch.size()]);
    }

    /**
     * @param jdbcTemplate
     *            the jdbcTemplate to set
     */
    @Required
    public void setJdbcTemplate(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * @param sqlScriptParser
     *            the sqlScriptParser to set
     */
    public void setSqlScriptParser(final SqlScriptParser sqlScriptParser) {
        this.sqlScriptParser = sqlScriptParser;
    }
}
