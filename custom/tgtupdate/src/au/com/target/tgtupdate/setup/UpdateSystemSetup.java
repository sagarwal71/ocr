/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 * 
 */
package au.com.target.tgtupdate.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.validation.services.ValidationService;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtupdate.constants.TgtupdateConstants;
import au.com.target.tgtupdate.dao.TargetUpdatesDao;
import au.com.target.tgtupdate.ddl.TargetDbScriptExecutor;
import au.com.target.tgtupdate.model.TargetUpdatesModel;


/**
 * This class provides hooks into the system's initialization and update processes.
 * 
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = TgtupdateConstants.EXTENSIONNAME)
public class UpdateSystemSetup extends AbstractSystemSetup {

    @Resource
    private ModelService modelService;

    @Resource
    private TargetDbScriptExecutor targetDbScriptExecutor;

    @Resource
    private TargetUpdatesDao targetUpdatesDao;

    @Resource
    private ConfigurationService configurationService;

    @Resource
    private TargetSharedConfigService targetSharedConfigService;

    private String sharedConfigurationEntries;

    /**
     * This method will be called by system creator during initialisation and system update. Be sure that this method
     * can be called repeatedly.
     * 
     * @param context
     *            the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
    public void createEssentialData(final SystemSetupContext context) {
        createCustomIndexes();
        updateTargetSharedConfig();
    }


    protected void updateTargetSharedConfig() {
        if (StringUtils.isNotBlank(sharedConfigurationEntries)) {
            final List<String> sharedConfigs = Arrays.asList(StringUtils.split(sharedConfigurationEntries, ','));
            final Map sharedConfigEntries = new HashMap<String, String>();
            for (final String eachSharedConfig : sharedConfigs) {
                final String sharedConfigKey = configurationService.getConfiguration().getString(eachSharedConfig);
                if (StringUtils.isNotBlank(sharedConfigKey)) {
                    sharedConfigEntries.put(eachSharedConfig, sharedConfigKey);
                }
                else {
                    sharedConfigEntries.put(eachSharedConfig, null);
                }
            }
            targetSharedConfigService.setSharedConfigs(sharedConfigEntries);
        }


    }

    /**
     * This method will be called during the system update only.
     * 
     * @param context
     *            the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.PROJECT, process = Process.UPDATE)
    public void createProjectData(final SystemSetupContext context) {
        boolean isContentCatalogSyncRequired = false;
        final boolean isProductCatalogSyncRequired = false;
        final boolean isValidationEngineReloadRequired = false;

        importImpex(context, "OCR-18157 Use Cache Stock For Fulfillment",
                "/tgtupdate/featureswitch/feature-stocklookup-cache-fulfilment.impex");

        importImpex(context, "OCR-18561 create and set inventory adjustment using wm",
                "/tgtupdate/featureswitch/feature-inventoryadjustment-wm.impex");

        importImpex(context, "OCR-18576 create config for originalcategory batchlimit and subbatch limit",
                "/tgtupdate/sharedconfig/products-batchlimit.impex");

        importImpex(context, "OCR-18610 AfterPay on PDP pages",
                "/tgtupdate/featureswitch/feature-payment-afterpay.impex");

        importImpex(context, "OCR-18599 add afterpay payment mode",
                "/tgtupdate/payment/afterpay.impex");

        importImpex(context, "OCR-20664 add zip payment mode",
                "/tgtupdate/payment/zippay.impex");

        importImpex(context, "OCR-18602 add afterpay get configuration cronjob",
                "/tgtupdate/jobs/targetAfterpayConfigurationCronJob.impex");

        importImpex(context, "OCR-18670 add clearance category",
                "/tgtupdate/sharedconfig/products-clearancecategory.impex");

        importImpex(context, "OCR-17559 feature switch for end of life",
                "/tgtupdate/featureswitch/feature-product-end-of-life.impex");

        importImpex(context, "OCR-18670 original category job",
                "/tgtupdate/jobs/targetOriginalCategoryJob.impex");

        if (importImpex(context, "OCR-18603 Add afterpay modal",
                "/tgtupdate/content/afterpay-modal.impex")) {
            isContentCatalogSyncRequired = true;
        }

        importImpex(context, "OCR-18850 feature switch for rich snippet",
                "/tgtupdate/featureswitch/feature-ui-webPage-schema.impex");

        importImpex(context, "OCR-19039 display afterpayPaymentInfo in cscockpit",
                "/tgtcs/import/impex/projectdata_tgtcs_ui_components.impex");

        importImpex(context, "OCR-18819 feature switch for new megamenu",
                "/tgtupdate/featureswitch/feature-ui-new-megamenu.impex");

        if (importImpex(context, "OCR-18819 create shell navigation node for new megamenu.",
                "/tgtupdate/content/navigation-update.impex")) {
            isContentCatalogSyncRequired = true;
        }

        importImpex(context, "OCR-17506 feature switch display only tracking",
                "/tgtupdate/featureswitch/feature-ui-track-display-only.impex");

        importImpex(context, "OCR-17560 feature switch for product end of life cross sell",
                "/tgtupdate/featureswitch/feature_productendoflife_crosssell.impex");
        importImpex(context, "OCR-17560 shared config for product end of life cross sell items per page",
                "/tgtupdate/sharedconfig/productendoflife_crosssell_itemsperpage.impex");
        importImpex(context, "OCR-19110 feature switch for australia post delivery club",
                "/tgtupdate/featureswitch/feature-aus-post-delivery-club.impex");

        importImpex(context, "OMS-14 Ebay Click and Collect sales application config update",
                "/tgtupdate/salesapplicationconfig/update-ebay-sales-application-config.impex");

        importImpex(context,
                "OMS-14 Ebay Click and Collect target point of service eligibleForEbayClickAndCollect flag update",
                "/tgtupdate/targetpointofservice/update-eligibleForEbayClickAndCollect-flag.impex");

        importImpex(context,
                "OMS-18 Maximum threshold for items in cart shared config update",
                "/tgtupdate/sharedconfig/cart-entries-threshold-config.impex");

        importImpex(context,
                "OCR-19103 Shipster member default config for various delivery modes",
                "/tgtupdate/shipsterConfig/shipster-config.impex");

        importImpex(context, "LC18/LC19 Feature switches for Oracle LiveChat & Co-Browse",
                "/tgtupdate/featureswitch/feature-oracle-livechat.impex");

        importImpex(context, "OCR-19110 rename delivery club to shipster",
                "/tgtupdate/featureswitch/feature-rename-to-shipster.impex");

        importImpex(context, "OCR-19211 feature switch for FIS2 store stock",
                "/tgtupdate/featureswitch/feature_fis_store_stock_visibility.impex");

        importImpex(context, "OCR-19036 Shared config for issv listing clearance category",
                "/tgtupdate/sharedconfig/instoreStock-listing-clearance.impex");

        importImpex(context, "OMS-28 batch job to feed stores to fluent",
                "/tgtupdate/fluent/location-feed-job.impex");

        importImpex(context, "OCR-19367 new product type physical giftcards",
                "/tgtupdate/physicalgiftcard/physical-giftcard-config.impex");

        importImpex(context, "OMS-34 add fluent feature switch",
                "/tgtupdate/featureswitch/feature-fluent.impex");

        importImpex(context, "OCR-19430 changed the config for Incomm warehouse",
                "/tgtupdate/physicalgiftcard/incomm-warehouse-config.impex");

        importImpex(context, "OCR-19426 cronjob for extract consignments",
                "/tgtupdate/jobs/targetExtractUnshippedConsignmentsJob.impex");

        importImpex(context, "OCR-19381 physical giftcard basket size",
                "/tgtupdate/physicalgiftcard/physical-giftcard-sharedconfig.impex");

        importImpex(context, "OCR-19426 Changed the cronjob trigger ",
                "/tgtupdate/physicalgiftcard/physical-giftcard-cronjob-trigger.impex");

        importImpex(context, "OMS-80 job to check status of location batch updates to Fluent",
                "/tgtupdate/fluent/location-feed-status-check-job.impex");

        importImpex(context, "OMS-33 job to feed products in batch to fluent",
                "/tgtupdate/fluent/product-feed-job.impex");

        importImpex(context, "OMS-30 job to feed categories in batch to fluent",
                "/tgtupdate/fluent/category-feed-job.impex");

        importImpex(context, "OCR-19589 Defer non-critical CSS",
                "/tgtupdate/featureswitch/feature-defer-noncritical-css.impex");

        importImpex(context, "OMS-128 add fluent stock lookup feature switch",
                "/tgtupdate/featureswitch/feature-fluent-stock-lookup.impex");

        importImpex(context, "OCR-19739 Serve assets from new UI repository",
                "/tgtupdate/featureswitch/feature-ui-serve-new-assets.impex");

        if (importImpex(context, "OCR-19710 InStore Wifi terms and conditons",
                "/tgtupdate/content/termsandconditions-instorewifi.impex")) {
            isContentCatalogSyncRequired = true;
        }

        importImpex(context, "OCR-19795 React PDP Carousel",
                "/tgtupdate/featureswitch/feature-ui-react-pdp-carousel.impex");

        importImpex(context, "OCR-19803 Demoup Video on PDP",
                "/tgtupdate/featureswitch/feature-ui-demoup-pdp.impex");

        importImpex(context, "OMS-176 Cnc order extract for consignment",
                "/tgtupdate/featureswitch/feature-consignment-cnc-order-extract.impex");


        importImpex(context, "OCR-19870 Cnc order extract for consignment",
                "/tgtupdate/featureswitch/feature-ui-react-pdp-detail-panel.impex");

        importImpex(context, "OCR-19945 update preOrder flag for Fastline warehouse",
                "/tgtupdate/preorder/preOrderWarehouse.impex");

        importImpex(context, "NOCR break state boundaries in store selection and add distance restriction",
                "/tgtupdate/featureswitch/feature-fulfilment-store-with-distance-restriction.impex");

        importImpex(context,
                "NOCR set fulfilment store distance radius limit in shared config",
                "/tgtupdate/sharedconfig/store-fulfilment-selection-distance-limit.impex");

        importImpex(context, "OCR-19945 add max preOrder per order quantity allowed in basket",
                "/tgtupdate/preorder/preOrder-sharedConfig.impex");

        importImpex(context,
                "NOCR enable partial cancel button and allow the waved consignment to be cancelled in cscockpit",
                "/tgtupdate/featureswitch/feature-tgtcs-cancel-button.impex");

        importImpex(context, "OCR-19937 add preOrder deposit amount",
                "/tgtupdate/sharedconfig/preorder-deposit.impex");

        importImpex(context,
                "OMS-210 Endeca Partial Update to include all modified records instead of just out of stock",
                "/tgtupdate/featureswitch/feature-endeca-partial-update-all-modified-records.impex");

        if (importImpex(context, "OCR-19959 Add preorder deposit modal",
                "/tgtupdate/content/preorder-deposit-modal.impex")) {
            isContentCatalogSyncRequired = true;
        }

        importImpex(context, "OCR-20076 delivery modes PDP update attribute",
                "/tgtupdate/deliverymode/update-delivery-mode-available-for-preorder.impex");

        importImpex(context, "fastline-falcon-feature",
                "/tgtupdate/featureswitch/feature_fl_falcon.impex");

        importImpex(context, "fastlineFalcon-maxQtyFastlineFulfillPerDay",
                "/tgtupdate/sharedconfig/maximum-qty-fastline-fulfill-perday.impex");

        importImpex(context, "NOCR update the cancellation warehouse for consignment",
                "/tgtupdate/falcon/cancellationWarehouse.impex");

        importImpex(context,
                "NOCR create target task service to run the order process in order",
                "/tgtupdate/featureswitch/feature-task-service.impex");

        importImpex(context,
                "Full cancel button will be disabled for order having more than one consignments in cscockpit",
                "/tgtupdate/featureswitch/feature-tgtcs-full-cancel-button.impex");

        importImpex(context,
                "OCR-20304 update express delivery fee for preOrders",
                "/tgtupdate/preorder/preOrder-delivery-fee.impex");

        if (importImpex(context, "OCR-20258: Create content slot for scripts on SPC",
                "/tgtupdate/content/spc-create-script-slot.impex")) {
            isContentCatalogSyncRequired = true;
        }

        importImpex(context, "OCR-19829 Create feature switch for full width PDP",
                "/tgtupdate/featureswitch/feature-ui-full-width-pdp.impex");

        importImpex(context, "OMS-329 add apigateway usergroup and user",
                "/tgtupdate/fluent/apigatewaygroup.impex");

        importImpex(context,
                "OCR-20433 and OCR-20363 Feature switch for Disable/Enable Update CC button and cancel pre-order",
                "/tgtupdate/featureswitch/feature-preorder-update-card-button.impex");

        importImpex(context, "OCR-20433 Remove old auspost notification",
                "/tgtupdate/featureswitch/remove-old-auspost-notification.impex");

        importImpex(context, "OMS-347 Ensure Cancel is only available to authorised managers",
                "/tgtupdate/user-groups/cs-usergroup.impex");

        importImpex(context, "OCR-20550 Create the Cronjob to pick the parked preorders.",
                "/tgtupdate/fulfilment/preOrderFulfilment-job-triggers.impex");

        importImpex(context, "OMS-590 add fluentLocationRef to warehouseModel",
                "/tgtupdate/fluent/warehouse-fluentLocationRef.impex");
        importImpex(context, "NOCR include fastline into store routing logic",
                "/tgtupdate/featureswitch/feature_fl_falcon_fastline_in_store_splitting.impex");

        importImpex(context, "NOCR update onlinePos address for routing",
                "/tgtupdate/featureswitch/updateOnlinePosAddress.impex");

        importImpex(context,
                "NOCR create target task service add Failed process stalling Feature switch",
                "/tgtupdate/featureswitch/feature-task-service.impex");

        importImpex(context, "OMS-584 add parcelCount & statusDate to consignment in cscockpit",
                "/tgtcs/import/impex/projectdata_tgtcs_ui_components.impex");

        importImpex(context, "OCR-20702 updating delivery location",
                "/tgtupdate/featureswitch/feature-location-services.impex");

        importImpex(context,
                "OCR-20664 creating Zip payment feature switch",
                "/tgtupdate/featureswitch/feature-payment-zip.impex");

        if (importImpex(context, "OCR-20810 Add zip payment modal",
                "/tgtupdate/content/zippayment-modal.impex")) {
            isContentCatalogSyncRequired = true;
        }

        if (importImpex(context, "OCR-20776 Add zip-payment SPC modal (MoreInfo)",
                "/tgtupdate/content/zippayment-spc-modal.impex")) {
            isContentCatalogSyncRequired = true;
        }

        importImpex(context, "OCR-20810 creating Zip payment threshold config",
            "/tgtupdate/sharedconfig/zip-payment-threshold-config.impex");

        importImpex(context,
                "OCR-20922 Create/Update short lead time rows in Target shared config for HD, ED and Bulky delivery modes",
                "/tgtupdate/sharedconfig/short-lead-time.impex");

        importImpex(context,
                "OCR-20852 creating ui React Listing feature switch",
                "/tgtupdate/featureswitch/feature-ui-react-listing.impex");

        importImpex(context,
                "OCR-20938 Default Short Lead Time",
                "/tgtupdate/sharedconfig/short-lead-time-default.impex");

        importImpex(context, "OCR-20741 CS cockpit changes to show zip payment details",
            "/tgtcs/import/impex/projectdata_tgtcs_ui_components.impex");

        importImpex(context, "OCR-21046 add zip payment mode",
            "/tgtupdate/payment/zippay.impex");

        // add new impex updates before this line

        if (isContentCatalogSyncRequired) {
            executeCatalogSyncJob(context, "targetContentCatalog");
        }

        if (isProductCatalogSyncRequired) {
            executeCatalogSyncJob(context, "targetProductCatalog");
        }

        if (isValidationEngineReloadRequired) {
            final ValidationService validationService = Registry.getApplicationContext().getBean(
                    "validationService", ValidationService.class);
            validationService.reloadValidationEngine();
        }

        // do not add any new impex here, add it above the previous comment
    }

    /**
     * This method will first check if impexId already exists, if it doesn't exist then it will insert the impexId and
     * import the impex file and return a true. If impex is not imported then it will return false.
     * 
     * @param context
     * @param id
     * @param impexLocation
     * @return boolean, true - if impex is imported, false - if impex is not imported.
     */
    protected boolean importImpex(final SystemSetupContext context, final String id, final String impexLocation) {
        TargetUpdatesModel updatesModel = targetUpdatesDao.getTargetUpdatesModelById(id);
        if (updatesModel == null) {
            updatesModel = modelService.create(TargetUpdatesModel.class);
            updatesModel.setUpdateName(id);
            updatesModel.setDeploymentDate(new Date());
            importImpexFile(context, impexLocation);
            modelService.save(updatesModel);
            logInfo(context, "Created Target Update for update: '" + id + "' and imported impex from: '"
                    + impexLocation + "'");
            return true;
        }
        return false;
    }

    /**
     * Method to create custom indexes
     */
    private void createCustomIndexes() {
        targetDbScriptExecutor.executeDdl("/tgtupdate/db/custom-index.sql");
        targetDbScriptExecutor.executeDdl("/tgtupdate/db/custom-drop-indexes.sql");
    }

    /**
     * No initialization options required.
     */
    @Override
    public List<SystemSetupParameter> getInitializationOptions() {
        return null;
    }


    /**
     * @param sharedConfigurationEntries
     *            the sharedConfigurationEntries to set
     */
    public void setSharedConfigurationEntries(final String sharedConfigurationEntries) {
        this.sharedConfigurationEntries = sharedConfigurationEntries;
    }

}
