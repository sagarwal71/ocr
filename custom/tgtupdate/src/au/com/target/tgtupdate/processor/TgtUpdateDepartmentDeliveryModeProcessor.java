package au.com.target.tgtupdate.processor;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.product.TargetProductDeliveryModeService;
import au.com.target.tgtcore.product.TargetProductDepartmentService;


/**
 * Class will fetch all the Target products batch by batch and update the department based on its variants. It will also
 * update the Delivery mode.
 * 
 */
public class TgtUpdateDepartmentDeliveryModeProcessor {


    private static final Logger LOG = Logger.getLogger(TgtUpdateDepartmentDeliveryModeProcessor.class);

    private Integer defaultDepartment;

    private ModelService modelService;

    private TargetProductDepartmentService targetProductDepartmentService;

    private TargetProductDeliveryModeService targetProductDeliveryModeService;



    /**
     * This method will iterate over the list of products and its variants to find the department.
     * 
     * @param targetProductModelList
     */
    public void processDepartmentDeliveryModeUpdate(final List<TargetProductModel> targetProductModelList) {

        //To hold the values which needs to be updated at the end.
        final Collection<TargetProductModel> targetProductModelListToUpdate = new ArrayList<>();
        //Iterate over the retrieved set of products.
        for (final TargetProductModel targetProductModel : targetProductModelList) {

            //Get the variants from the targetProduct.
            final Collection<VariantProductModel> variantsCollection = targetProductModel.getVariants();

            boolean foundDepartment = false;
            Integer department = null;
            // Iterate over the variants
            for (final VariantProductModel variantProductModel : variantsCollection) {

                final Collection<VariantProductModel> childvariantCollection = variantProductModel.getVariants();
                //Checking whether the variant is of the type colour variant and it does not have any variants
                if (variantProductModel instanceof TargetColourVariantProductModel
                        && CollectionUtils.isEmpty(childvariantCollection)) {
                    department = ((TargetColourVariantProductModel)variantProductModel).getDepartment();
                    if (null != department && department.intValue() > 0) {
                        foundDepartment = true;
                        break;
                    }
                }
                //if the above condition does not satisfy it means it has child variants(size)
                // Iterate over the size variants in search of department if it finds even one break. 
                else {
                    for (final VariantProductModel variantProductModelChild : childvariantCollection) {
                        if (variantProductModelChild instanceof TargetSizeVariantProductModel) {
                            department = ((TargetSizeVariantProductModel)variantProductModelChild).getDepartment();
                            if (null != department && department.intValue() > 0) {
                                foundDepartment = true;
                                break;
                            }
                        }
                    }
                }
                //Can break if a department is found in any of the size or colour variant under a product.
                if (foundDepartment) {
                    break;
                }
            }

            if (variantsCollection.isEmpty()) {
                LOG.error("No Variants Found For TargetProduct - (pk,code) - (" + targetProductModel.getPk() + ","
                        + targetProductModel.getCode() + ")");
                department = defaultDepartment;
            }

            // get department to update
            final TargetMerchDepartmentModel targetMerchDepartmentModel = getAppropriateDepartmentToUpdate(
                    targetProductModel, department);

            //Call Update for updating the department.
            if (updateDepartmentAndDeliveryMode(targetProductModel, targetMerchDepartmentModel)) {
                targetProductModelListToUpdate.add(targetProductModel);
            }

        }
        //Saving to hybris
        if (CollectionUtils.isNotEmpty(targetProductModelListToUpdate)) {
            LOG.info("Products to update in first batch : " + targetProductModelListToUpdate.size());
            modelService.saveAll(targetProductModelListToUpdate);
        }
    }

    /**
     * This method will check the validity of the department found. If t is a valid department will return the
     * department model. If the department is not valid then it will return the default department configured as per the
     * defaultDepartment
     * 
     * @param targetProductModel
     * @param department
     * @return targetMerchDepartmentModel which is to be updated
     */
    private TargetMerchDepartmentModel getAppropriateDepartmentToUpdate(final TargetProductModel targetProductModel,
            final Integer department) {


        TargetMerchDepartmentModel targetMerchDepartmentModel = targetProductDepartmentService
                .getDepartmentCategoryModel(
                        targetProductModel.getCatalogVersion(), department);
        if (targetMerchDepartmentModel == null) {
            LOG.error(MessageFormat.format(
                    "DEPARTMENT-NOT-FOUND : Department {0} for the Product {1} ",
                    department,
                    targetProductModel.getCode()));
            LOG.info(MessageFormat.format("Assigning default Department 999 for product {0} ",
                    targetProductModel.getCode()));
            targetMerchDepartmentModel = targetProductDepartmentService
                    .getDepartmentCategoryModel(targetProductModel.getCatalogVersion(),
                            defaultDepartment);
        }
        return targetMerchDepartmentModel;

    }

    /**
     * Method checks whether a department or a delivery mode update is required. If the department to be updated is a
     * default then it will check whether home delivery is associated with the product. If it is associated will also
     * remove the same.
     * 
     * @param targetProductModel
     * @param targetMerchDepartmentModel
     * @return isUpdateRequired
     */
    private boolean updateDepartmentAndDeliveryMode(final TargetProductModel targetProductModel,
            final TargetMerchDepartmentModel targetMerchDepartmentModel) {
        final boolean isDepartmentUpdateRequired = targetProductDepartmentService.processMerchDeparment(
                targetProductModel,
                targetMerchDepartmentModel);

        final boolean isDeliveryModeUpdateRequired = targetProductDeliveryModeService
                .processExpressDeliveryModeUpdation(
                        targetProductModel,
                        targetMerchDepartmentModel);

        return (isDepartmentUpdateRequired || isDeliveryModeUpdateRequired);
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param targetProductDeliveryModeService
     *            the targetProductDeliveryModeService to set
     */
    @Required
    public void setTargetProductDeliveryModeService(
            final TargetProductDeliveryModeService targetProductDeliveryModeService) {
        this.targetProductDeliveryModeService = targetProductDeliveryModeService;
    }

    /**
     * @param targetProductDepartmentService
     *            the targetProductDepartmentService to set
     */
    @Required
    public void setTargetProductDepartmentService(final TargetProductDepartmentService targetProductDepartmentService) {
        this.targetProductDepartmentService = targetProductDepartmentService;
    }

    /**
     * @param defaultDepartment
     *            the defaultDepartment to set
     */
    @Required
    public void setDefaultDepartment(final Integer defaultDepartment) {
        this.defaultDepartment = defaultDepartment;
    }

}
