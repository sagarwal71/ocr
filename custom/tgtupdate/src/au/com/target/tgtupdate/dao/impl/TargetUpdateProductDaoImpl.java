/**
 * 
 */
package au.com.target.tgtupdate.dao.impl;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtupdate.dao.TargetUpdateProductDao;
import au.com.target.tgtupdate.model.TargetCheckedColourVariantToSetOnlineFromDateModel;


/**
 * @author ragarwa3
 *
 */
public class TargetUpdateProductDaoImpl extends DefaultGenericDao<TargetColourVariantProductModel> implements
        TargetUpdateProductDao {

    private int initializeOnlineFromDateBatchSize;

    public TargetUpdateProductDaoImpl(final String typecode) {
        super(typecode);
    }

    @Override
    public List<TargetColourVariantProductModel> getColourVariantsWithNullOnlineFromDate() {
        final StringBuilder query = new StringBuilder();
        query.append("SELECT TOP ").append(initializeOnlineFromDateBatchSize).append(" {tcvp:")
                .append(TargetColourVariantProductModel.PK).append("}");
        query.append(" FROM {").append(TargetColourVariantProductModel._TYPECODE).append(" as tcvp LEFT JOIN ")
                .append(TargetCheckedColourVariantToSetOnlineFromDateModel._TYPECODE).append(" as tcof ON {tcvp:")
                .append(TargetColourVariantProductModel.PK).append("} = {tcof:")
                .append(TargetCheckedColourVariantToSetOnlineFromDateModel.COLOURVARIANTPK).append("} JOIN ")
                .append(CatalogVersionModel._TYPECODE).append(" as cv ON {cv:").append(CatalogVersionModel.PK)
                .append("} = {tcvp:").append(TargetColourVariantProductModel.CATALOGVERSION).append("} JOIN ")
                .append(CatalogModel._TYPECODE).append(" as c ON {c:").append(CatalogModel.PK).append("} = {cv:")
                .append(CatalogVersionModel.CATALOG).append("}}");
        query.append(" WHERE {c:").append(CatalogModel.ID).append("} = '").append(TgtCoreConstants.Catalog.PRODUCTS)
                .append("' AND {cv:").append(CatalogVersionModel.VERSION).append("} = '")
                .append(TgtCoreConstants.Catalog.OFFLINE_VERSION).append("' AND {tcvp:")
                .append(TargetColourVariantProductModel.ONLINEDATE).append("} IS NULL AND {tcof:")
                .append(TargetCheckedColourVariantToSetOnlineFromDateModel.COLOURVARIANTPK)
                .append("} IS NULL AND {tcvp:").append(TargetColourVariantProductModel.APPROVALSTATUS)
                .append("} = ?approvalStatus");

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.addQueryParameter("approvalStatus", ArticleApprovalStatus.APPROVED);
        final SearchResult<TargetColourVariantProductModel> result = getFlexibleSearchService().search(searchQuery);
        return result.getResult();
    }

    /**
     * @param initializeOnlineFromDateBatchSize
     *            the initializeOnlineFromDateBatchSize to set
     */
    @Required
    public void setInitializeOnlineFromDateBatchSize(final int initializeOnlineFromDateBatchSize) {
        this.initializeOnlineFromDateBatchSize = initializeOnlineFromDateBatchSize;
    }

    @Override
    public List<TargetProductModel> getCncAndEbayAvailableProducts(final String includedDeliveryMode,
            final String excludedDeliveryMode, final int batchSize, final CatalogVersionModel catalogVersion) {
        final StringBuilder query = new StringBuilder();
        query.append("SELECT DISTINCT {bp:").append(TargetProductModel.PK).append("} FROM {")
                .append(TargetProductModel._TYPECODE).append(" AS bp }");
        query.append(" WHERE EXISTS ({{SELECT {pdr:source} FROM {ProductDeliveryModeRelation AS pdr");
        query.append(" JOIN ").append(DeliveryModeModel._TYPECODE).append(" AS dm ON {pdr:target}={dm:pk}} ");
        query.append(" WHERE  {bp:pk} ={pdr:source} AND {dm:").append(DeliveryModeModel.CODE)
                .append("} = ?includedDeliveryMode }})");
        query.append(" AND NOT EXISTS ({{SELECT {pdr:source} FROM {ProductDeliveryModeRelation AS pdr");
        query.append(" JOIN ").append(DeliveryModeModel._TYPECODE).append(" AS dm ON {pdr:target}={dm:pk}} ");
        query.append(" WHERE  {bp:pk} ={pdr:source} AND {dm:").append(DeliveryModeModel.CODE)
                .append("} = ?excludedDeliveryMode }})");
        query.append(" AND EXISTS ({{SELECT 1 FROM {").append(AbstractTargetVariantProductModel._TYPECODE)
                .append("  AS cvp ");
        query.append(" LEFT JOIN ").append(AbstractTargetVariantProductModel._TYPECODE)
                .append("  AS svp ON {svp:baseProduct}={cvp:pk}} ");
        query.append(" WHERE {cvp:baseProduct}={bp:pk} AND ({cvp:")
                .append(AbstractTargetVariantProductModel.AVAILABLEONEBAY).append("}=?availableOnEbay OR {svp:");
        query.append(AbstractTargetVariantProductModel.AVAILABLEONEBAY).append("}=?availableOnEbay )}}) ");
        query.append(" AND {bp:").append(TargetProductModel.CATALOGVERSION).append("} = ?catalogVersion ");

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("includedDeliveryMode", includedDeliveryMode);
        params.put("excludedDeliveryMode", excludedDeliveryMode);
        params.put("availableOnEbay", Boolean.TRUE);
        params.put("catalogVersion", catalogVersion);
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.addQueryParameters(params);
        searchQuery.setCount(batchSize);
        searchQuery.setResultClassList(Collections.singletonList(TargetProductModel.class));
        final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
        return searchResult.getResult();
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtupdate.dao.TargetUpdateProductDao#findSizeVariantsWhichDoesNotHaveProductSizeAssociation(java.util.Set, de.hybris.platform.catalog.model.CatalogVersionModel)
     */
    @Override
    public List<TargetSizeVariantProductModel> findSizeVariantsWhichDoesNotHaveProductSizeAssociation(
            final Set<String> codes,
            final CatalogVersionModel catalogVersion) {

        final StringBuilder query = new StringBuilder();
        query.append(" SELECT {").append(TargetSizeVariantProductModel.PK).append("} ");
        query.append(" from {").append(TargetSizeVariantProductModel._TYPECODE).append("} ");
        query.append(" where {").append(TargetSizeVariantProductModel.PRODUCTSIZE).append("} IS NULL ");
        query.append(" AND {").append(TargetSizeVariantProductModel.CATALOGVERSION).append("} = ?catalogVersion ");
        query.append(" AND {").append(TargetSizeVariantProductModel.CODE).append("} IN (");
        query.append(getCodesFormatted(codes));
        query.append(")");
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("catalogVersion", catalogVersion);
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.addQueryParameters(params);
        searchQuery.setResultClassList(Collections.singletonList(TargetSizeVariantProductModel.class));
        final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
        return searchResult.getResult();
    }

    private StringBuilder getCodesFormatted(final Set<String> codes) {
        final StringBuilder formattedCodes = new StringBuilder();
        int count = 1;
        for (final String code : codes) {
            formattedCodes.append("'").append(code).append("'");
            if (count < codes.size()) {
                formattedCodes.append(",");
            }
            count++;
        }
        return formattedCodes;
    }

}
