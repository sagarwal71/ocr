/**
 * 
 */
package au.com.target.tgtupdate.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.Collections;
import java.util.List;

import au.com.target.tgtupdate.dao.TargetUpdatesDao;
import au.com.target.tgtupdate.model.TargetUpdatesModel;


/**
 * @author pratik
 *
 */
public class TargetUpdatesDaoImpl extends DefaultGenericDao<TargetUpdatesModel> implements TargetUpdatesDao {

    public TargetUpdatesDaoImpl() {
        super(TargetUpdatesModel._TYPECODE);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtupdate.dao.TargetUpdatesDao#getTargetUpdateModelById(java.lang.String)
     */
    @Override
    public TargetUpdatesModel getTargetUpdatesModelById(final String id) {
        final List<TargetUpdatesModel> result = find(Collections.singletonMap(TargetUpdatesModel.UPDATENAME, id));
        return (result != null && result.size() > 0) ? result.get(0) : null;
    }
}
