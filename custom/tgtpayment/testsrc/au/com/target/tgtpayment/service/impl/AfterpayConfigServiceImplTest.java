/**
 * 
 */
package au.com.target.tgtpayment.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetGetPaymentConfigurationRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentPingRequest;
import au.com.target.tgtpayment.commands.result.TargetGetPaymentConfigurationResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentPingResult;
import au.com.target.tgtpayment.constants.TgtpaymentConstants;
import au.com.target.tgtpayment.dao.AfterpayConfigDao;
import au.com.target.tgtpayment.methods.TargetPaymentMethod;
import au.com.target.tgtpayment.model.AfterpayConfigModel;
import au.com.target.tgtpayment.service.AfterpayConfigService;
import au.com.target.tgtpayment.strategy.PaymentMethodStrategy;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AfterpayConfigServiceImplTest {

    @Mock
    private AfterpayConfigDao afterpayConfigDao;

    @Mock
    private AfterpayConfigModel afterpayConfigExpected;

    @Mock
    private PaymentMethodStrategy paymentMethodStrategy;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private final AfterpayConfigService afterpayConfigService = new AfterpayConfigServiceImpl();

    @Test
    public void testFetchAfterpayConfig() {
        given(afterpayConfigDao.getAfterpayConfig()).willReturn(afterpayConfigExpected);
        final AfterpayConfigModel afterpayConfig = afterpayConfigService.getAfterpayConfig();
        assertThat(afterpayConfig).isEqualTo(afterpayConfigExpected);
    }

    @Test
    public void testFetchAfterpayConfigNull() {
        final AfterpayConfigModel afterpayConfig = afterpayConfigService.getAfterpayConfig();
        assertThat(afterpayConfig).isNull();
    }

    @Test
    public void testCreateOrUpdateAfterpayConfigWithUpdateExistingModel() {
        final String description = "description";
        final String type = "type";
        final BigDecimal max = new BigDecimal("10.00").setScale(2, BigDecimal.ROUND_HALF_UP);
        final BigDecimal min = new BigDecimal("0.80").setScale(2, BigDecimal.ROUND_HALF_UP);
        final TargetPaymentMethod paymentMethod = mock(TargetPaymentMethod.class);
        given(paymentMethodStrategy.getPaymentMethod(TgtpaymentConstants.AFTERPAY_PAYMENT_TYPE)).willReturn(
                paymentMethod);
        final TargetGetPaymentConfigurationResult configuration = new TargetGetPaymentConfigurationResult();
        configuration.setDescription(description);
        configuration.setType(type);
        configuration.setMaximumAmount(max);
        configuration.setMinimumAmount(min);
        given(paymentMethod.getConfiguration(any(TargetGetPaymentConfigurationRequest.class)))
                .willReturn(configuration);
        final AfterpayConfigModel model = mock(AfterpayConfigModel.class);
        given(afterpayConfigDao.getAfterpayConfig()).willReturn(model);
        afterpayConfigService.createOrUpdateAfterpayConfig();
        verify(model).setDescription(description);
        verify(model).setPaymentType(type);
        verify(model).setMaximumAmount(max);
        verify(model).setMinimumAmount(min);
        verify(modelService).save(model);
        verify(modelService).refresh(model);
    }

    @Test(expected = AdapterException.class)
    public void testCreateOrUpdateAfterpayConfigWithAdapterException() {
        final TargetPaymentMethod paymentMethod = mock(TargetPaymentMethod.class);
        given(paymentMethodStrategy.getPaymentMethod(TgtpaymentConstants.AFTERPAY_PAYMENT_TYPE)).willReturn(
                paymentMethod);
        given(paymentMethod.getConfiguration(any(TargetGetPaymentConfigurationRequest.class)))
                .willThrow(new AdapterException());
        afterpayConfigService.createOrUpdateAfterpayConfig();
    }

    @Test(expected = ModelSavingException.class)
    public void testCreateOrUpdateAfterpayConfigModelSavingException() {
        final String description = "description";
        final String type = "type";
        final BigDecimal max = new BigDecimal("10.00").setScale(2, BigDecimal.ROUND_HALF_UP);
        final BigDecimal min = new BigDecimal("0.80").setScale(2, BigDecimal.ROUND_HALF_UP);
        final TargetPaymentMethod paymentMethod = mock(TargetPaymentMethod.class);
        given(paymentMethodStrategy.getPaymentMethod(TgtpaymentConstants.AFTERPAY_PAYMENT_TYPE)).willReturn(
                paymentMethod);
        final TargetGetPaymentConfigurationResult configuration = new TargetGetPaymentConfigurationResult();
        configuration.setDescription(description);
        configuration.setType(type);
        configuration.setMaximumAmount(max);
        configuration.setMinimumAmount(min);
        given(paymentMethod.getConfiguration(any(TargetGetPaymentConfigurationRequest.class)))
                .willReturn(configuration);
        final AfterpayConfigModel model = mock(AfterpayConfigModel.class);
        given(afterpayConfigDao.getAfterpayConfig()).willReturn(model);
        willThrow(new ModelSavingException("model saving exception")).given(modelService)
                .save(any(AfterpayConfigModel.class));
        afterpayConfigService.createOrUpdateAfterpayConfig();
    }


    @Test
    public void testCreateOrUpdateAfterpayConfigWithCreateNewModel() {
        final String description = "description";
        final String type = "type";
        final BigDecimal max = new BigDecimal("10.00").setScale(2, BigDecimal.ROUND_HALF_UP);
        final BigDecimal min = new BigDecimal("0.80").setScale(2, BigDecimal.ROUND_HALF_UP);
        final TargetPaymentMethod paymentMethod = mock(TargetPaymentMethod.class);
        given(paymentMethodStrategy.getPaymentMethod(TgtpaymentConstants.AFTERPAY_PAYMENT_TYPE)).willReturn(
                paymentMethod);
        final TargetGetPaymentConfigurationResult configuration = new TargetGetPaymentConfigurationResult();
        configuration.setDescription(description);
        configuration.setType(type);
        configuration.setMaximumAmount(max);
        configuration.setMinimumAmount(min);
        given(paymentMethod.getConfiguration(any(TargetGetPaymentConfigurationRequest.class)))
                .willReturn(configuration);
        final AfterpayConfigModel model = mock(AfterpayConfigModel.class);
        given(afterpayConfigDao.getAfterpayConfig()).willReturn(null);
        given(modelService.create(AfterpayConfigModel.class)).willReturn(model);
        afterpayConfigService.createOrUpdateAfterpayConfig();
        verify(model).setDescription(description);
        verify(model).setPaymentType(type);
        verify(model).setMaximumAmount(max);
        verify(model).setMinimumAmount(min);
        verify(modelService).save(model);
        verify(modelService).refresh(model);
    }

    @Test
    public void testPing() {
        final TargetPaymentMethod paymentMethod = mock(TargetPaymentMethod.class);
        given(paymentMethodStrategy.getPaymentMethod(TgtpaymentConstants.AFTERPAY_PAYMENT_TYPE)).willReturn(
                paymentMethod);
        final TargetPaymentPingResult result = new TargetPaymentPingResult();
        result.setSuccess(true);

        given(paymentMethod.ping(any(TargetPaymentPingRequest.class))).willReturn(result);
        final boolean output = afterpayConfigService.ping();
        assertThat(output).isTrue();
    }

    @Test
    public void testPingWhenResultNull() {
        final TargetPaymentMethod paymentMethod = mock(TargetPaymentMethod.class);
        given(paymentMethodStrategy.getPaymentMethod(TgtpaymentConstants.AFTERPAY_PAYMENT_TYPE)).willReturn(
                paymentMethod);

        given(paymentMethod.ping(any(TargetPaymentPingRequest.class))).willReturn(null);
        final boolean output = afterpayConfigService.ping();
        assertThat(output).isFalse();
    }
}
