/**
 * 
 */
package au.com.target.tgtpayment.util;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtpayment.constants.TgtpaymentConstants;
import au.com.target.tgtutility.util.TargetProductUtils;


/**
 * @author salexa10
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPaymentOrderHelperTest {

    @InjectMocks
    @Spy
    private final TargetPaymentOrderHelper targetPaymentOrderHelper = new TargetPaymentOrderHelper();

    @Mock
    private ProductModel product;

    @Mock
    private MediaModel mediaModel;

    @Mock
    private AbstractTargetVariantProductModel abstractTargetVariantProductModel;

    @Mock
    private TargetColourVariantProductModel targetColourVariantProductModel;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private Configuration configuration;

    @Before
    public void setup() throws IllegalArgumentException, IllegalAccessException {
        given(mediaModel.getURL()).willReturn("/medias/product/P1000_BLACK_thumb.jpg");
        product = abstractTargetVariantProductModel;
        given(product.getThumbnail()).willReturn(mediaModel);
        given(configuration.getString(TgtpaymentConstants.BASE_SECURE_URL_KEY))
                .willReturn(
                        "https://target.com.au");
        given(configurationService.getConfiguration()).willReturn(configuration);

        given(targetPaymentOrderHelper.getConfigurationService()).willReturn(configurationService);

        given(targetColourVariantProductModel.getThumbnail())
                .willReturn(mediaModel);
        given(TargetProductUtils.getColourVariantProduct(abstractTargetVariantProductModel))
                .willReturn(targetColourVariantProductModel);

    }

    @Test
    public void testGetProductThumbnailUrl() throws IllegalArgumentException, IllegalAccessException {
        assertThat(targetPaymentOrderHelper.getProductThumbnailUrl(product))
                .isEqualTo("https://target.com.au/medias/product/P1000_BLACK_thumb.jpg");
    }

    @Test
    public void testNullGetProductThumbnailUrl() throws IllegalArgumentException, IllegalAccessException {
        given(mediaModel.getURL()).willReturn("");
        given(configuration.getString(TgtpaymentConstants.BASE_SECURE_URL_KEY))
                .willReturn(
                        "");
        assertThat(targetPaymentOrderHelper.getProductThumbnailUrl(product))
                .isEqualTo("");
    }

    @Test
    public void testSecureBaseUrl() throws IllegalArgumentException, IllegalAccessException {
        assertThat(targetPaymentOrderHelper.getBaseSecureUrl())
                .isEqualTo("https://target.com.au");
    }

}