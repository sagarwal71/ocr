/**
 * 
 */
package au.com.target.tgtpayment.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PaymentTransactionEntryPrepareInterceptorTest {

    @Mock
    private PaymentTransactionEntryModel pte;
    @Mock
    private CurrencyModel currency;
    @Mock
    private InterceptorContext ctx;
    @InjectMocks
    private final PaymentTransactionEntryPrepareInterceptor interceptor = new PaymentTransactionEntryPrepareInterceptor();

    @Before
    public void setup() {
        BDDMockito.given(pte.getCurrency()).willReturn(currency);
        BDDMockito.given(currency.getDigits()).willReturn(Integer.valueOf(2));
    }

    @Test
    public void testRoundPTEAmount2DecimalPlaces() throws InterceptorException {
        BDDMockito.given(pte.getAmount()).willReturn(BigDecimal.valueOf(19.600));
        interceptor.onPrepare(pte, ctx);
        Mockito.verify(pte).setAmount(BigDecimal.valueOf(19.60).setScale(2));
    }

    @Test
    public void testRoundUpPTEAmount() throws InterceptorException {
        BDDMockito.given(pte.getAmount()).willReturn(BigDecimal.valueOf(19.696));
        interceptor.onPrepare(pte, ctx);
        Mockito.verify(pte).setAmount(BigDecimal.valueOf(19.70).setScale(2));
    }

    @Test
    public void testRoundDownPTEAmount() throws InterceptorException {
        BDDMockito.given(pte.getAmount()).willReturn(BigDecimal.valueOf(19.612));
        interceptor.onPrepare(pte, ctx);
        Mockito.verify(pte).setAmount(BigDecimal.valueOf(19.61).setScale(2));
    }
}
