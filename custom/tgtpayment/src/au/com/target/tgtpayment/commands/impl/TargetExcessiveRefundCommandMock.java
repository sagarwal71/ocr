/**
 * 
 */
package au.com.target.tgtpayment.commands.impl;

import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.util.Date;

import au.com.target.tgtpayment.commands.TargetExcessiveRefundCommand;
import au.com.target.tgtpayment.commands.request.TargetExcessiveRefundRequest;
import au.com.target.tgtpayment.commands.result.TargetExcessiveRefundResult;
import au.com.target.tgtpayment.constants.TgtpaymentConstants;


/**
 * @author jspence8
 * 
 */
public class TargetExcessiveRefundCommandMock implements TargetExcessiveRefundCommand {

    /* (non-Javadoc)
     * @see de.hybris.platform.payment.commands.Command#perform(java.lang.Object)
     */
    @Override
    public TargetExcessiveRefundResult perform(final TargetExcessiveRefundRequest request) {

        final TargetExcessiveRefundResult result = new TargetExcessiveRefundResult();

        result.setCurrency(request.getCurrency());
        result.setTotalAmount(request.getTotalAmount());
        result.setRequestTime(new Date());
        result.setRequestId(TgtpaymentConstants.MOCKID);
        result.setRequestToken(TgtpaymentConstants.MOCKTOKEN);
        result.setTransactionStatus(TransactionStatus.ACCEPTED);
        result.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL);
        result.setMerchantTransactionCode(request.getTransactionId());

        return result;
    }

}
