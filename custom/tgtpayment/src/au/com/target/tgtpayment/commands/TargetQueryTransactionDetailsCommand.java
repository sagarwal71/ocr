/**
 * 
 */
package au.com.target.tgtpayment.commands;

import de.hybris.platform.payment.commands.Command;

import au.com.target.tgtpayment.commands.request.TargetQueryTransactionDetailsRequest;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;


/**
 * Command for querying transaction details from payment provider
 */
public interface TargetQueryTransactionDetailsCommand extends
        Command<TargetQueryTransactionDetailsRequest, TargetQueryTransactionDetailsResult> {
    //TODO
}
