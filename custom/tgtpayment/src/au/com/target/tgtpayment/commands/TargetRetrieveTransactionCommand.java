package au.com.target.tgtpayment.commands;

import de.hybris.platform.payment.commands.Command;

import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;


/**
 * Command for retrieving a previous transaction from payment provider
 */
public interface TargetRetrieveTransactionCommand extends
        Command<TargetRetrieveTransactionRequest, TargetRetrieveTransactionResult> {
    //
}
