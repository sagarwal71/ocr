/**
 * 
 */
package au.com.target.tgtpayment.commands.request;

import java.math.BigDecimal;
import java.util.Currency;


/**
 * @author schen13
 * 
 */
public abstract class AbstractTargetStandaloneRefundRequest extends AbstractRequest {
    private BigDecimal totalAmount;
    private Currency currency;

    /**
     * @return the currency
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * @param currency
     *            the currency to set
     */
    public void setCurrency(final Currency currency) {
        this.currency = currency;
    }


    /**
     * @return the amount
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * @param totalAmount
     *            the amount to set
     */
    public void setTotalAmount(final BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }



}
