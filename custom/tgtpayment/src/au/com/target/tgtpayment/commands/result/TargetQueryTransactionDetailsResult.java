package au.com.target.tgtpayment.commands.result;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

//CHECKSTYLE:OFF
import au.com.target.tgtpayment.commands.TargetQueryTransactionDetailsCommand;


//CHECKSTYLE:ON

/**
 * 
 * Result from {@link TargetQueryTransactionDetailsCommand}
 * 
 */
public class TargetQueryTransactionDetailsResult {
    private boolean success;
    private boolean pending;
    private boolean cancel;
    private String trnResult;
    private String errorDescription;
    private String sessionKey;
    private String sessionId;
    private String sessionToken;
    private BigDecimal amount;
    private String custReference;
    private String custNumber;

    private List<TargetCardResult> cardResults;
    //Saved cards information
    private List<TargetCardResult> savedCards;

    /**
     * @return the sessionKey
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * @param sessionKey
     *            the sessionKey to set
     */
    public void setSessionKey(final String sessionKey) {
        this.sessionKey = sessionKey;
    }

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId
     *            the sessionId to set
     */
    public void setSessionId(final String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @return the sessionToken
     */
    public String getSessionToken() {
        return sessionToken;
    }

    /**
     * @param sessionToken
     *            the sessionToken to set
     */
    public void setSessionToken(final String sessionToken) {
        this.sessionToken = sessionToken;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success
     *            the success to set
     */
    public void setSuccess(final boolean success) {
        this.success = success;
    }

    /**
     * @return the errorDescription
     */
    public String getErrorDescription() {
        return errorDescription;
    }

    /**
     * @param errorDescription
     *            the errorDescription to set
     */
    public void setErrorDescription(final String errorDescription) {
        this.errorDescription = errorDescription;
    }

    /**
     * @return the trnResult
     */
    public String getTrnResult() {
        return trnResult;
    }

    /**
     * @param trnResult
     *            the trnResult to set
     */
    public void setTrnResult(final String trnResult) {
        this.trnResult = trnResult;
    }

    /**
     * @return the custReference
     */
    public String getCustReference() {
        return custReference;
    }

    /**
     * @param custReference
     *            the custReference to set
     */
    public void setCustReference(final String custReference) {
        this.custReference = custReference;
    }

    /**
     * @return the custNumber
     */
    public String getCustNumber() {
        return custNumber;
    }

    /**
     * @param custNumber
     *            the custNumber to set
     */
    public void setCustNumber(final String custNumber) {
        this.custNumber = custNumber;
    }

    /**
     * @return the cardResults
     */
    public List<TargetCardResult> getCardResults() {
        return cardResults;
    }

    /**
     * @param cardResults
     *            the cardResults to set
     */
    public void setCardResults(final List<TargetCardResult> cardResults) {
        this.cardResults = cardResults;
    }

    /**
     * Add a {@link TargetCardResult} to card results
     * 
     * @param cardResult
     */
    public void addCardResult(final TargetCardResult cardResult) {
        if (this.cardResults == null) {
            this.cardResults = new ArrayList<>();
        }
        cardResults.add(cardResult);
    }

    /**
     * @return the savedCards
     */
    public List<TargetCardResult> getSavedCards() {
        return savedCards;
    }

    /**
     * @param savedCards
     *            the savedCards to set
     */
    public void setSavedCards(final List<TargetCardResult> savedCards) {
        this.savedCards = savedCards;
    }

    /**
     * @return the pending
     */
    public boolean isPending() {
        return pending;
    }

    /**
     * @param pending
     *            the pending to set
     */
    public void setPending(final boolean pending) {
        this.pending = pending;
    }

    /**
     * @return the cancel
     */
    public boolean isCancel() {
        return cancel;
    }

    /**
     * @param cancel
     *            the cancel to set
     */
    public void setCancel(final boolean cancel) {
        this.cancel = cancel;
    }
}