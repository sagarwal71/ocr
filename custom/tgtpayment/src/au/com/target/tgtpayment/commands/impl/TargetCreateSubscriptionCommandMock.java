/**
 * 
 */
package au.com.target.tgtpayment.commands.impl;

import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;


public class TargetCreateSubscriptionCommandMock implements TargetCreateSubscriptionCommand {

    /* (non-Javadoc)
     * @see de.hybris.platform.payment.commands.Command#perform(java.lang.Object)
     */
    @Override
    public TargetCreateSubscriptionResult perform(final TargetCreateSubscriptionRequest request) {

        final TargetCreateSubscriptionResult result = new TargetCreateSubscriptionResult();

        result.setSubscriptionID("Mock subscription ID");
        result.setTransactionStatus(TransactionStatus.ACCEPTED);
        result.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL);
        result.setMerchantTransactionCode(request.getTransactionId());

        return result;
    }

}
