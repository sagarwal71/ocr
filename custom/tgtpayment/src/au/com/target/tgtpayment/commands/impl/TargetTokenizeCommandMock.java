package au.com.target.tgtpayment.commands.impl;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtpayment.commands.TargetTokenizeCommand;
import au.com.target.tgtpayment.commands.request.TargetTokenizeRequest;
import au.com.target.tgtpayment.commands.result.TargetTokenizeResult;


/**
 * Dummy implementation of {@link TargetTokenizeCommand} which will always returns a preconfigured token
 */
public class TargetTokenizeCommandMock implements TargetTokenizeCommand {

    private String dummyToken;

    @Override
    public TargetTokenizeResult perform(final TargetTokenizeRequest request) {
        final TargetTokenizeResult result = new TargetTokenizeResult();
        result.setSavedToken(dummyToken);
        return result;
    }

    @Required
    public void setDummyToken(final String dummyToken) {
        this.dummyToken = dummyToken;
    }

}
