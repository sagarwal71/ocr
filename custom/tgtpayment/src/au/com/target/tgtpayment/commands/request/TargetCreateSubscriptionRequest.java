package au.com.target.tgtpayment.commands.request;

//CHECKSTYLE:OFF
import java.util.List;

import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.dto.CreditCard;
import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;


//CHECKSTYLE:ON

/**
 * 
 * Request for {@link TargetCreateSubscriptionCommand}
 * 
 */
public class TargetCreateSubscriptionRequest extends AbstractRequest
{
    private Order order;
    private boolean billingAgreementRequired;
    private String cancelUrl;
    private String returnUrl;
    private String sessionId;
    private List<CreditCard> savedCreditCards;
    private IpgPaymentTemplateType ipgPaymentTemplateType;

    /**
     * @return the order
     */
    public Order getOrder() {
        return order;
    }

    /**
     * @param order
     *            the order to set
     */
    public void setOrder(final Order order) {
        this.order = order;
    }

    /**
     * @return the billingAgreementRequired
     */
    public boolean isBillingAgreementRequired() {
        return billingAgreementRequired;
    }

    /**
     * @param billingAgreementRequired
     *            the billingAgreementRequired to set
     */
    public void setBillingAgreementRequired(final boolean billingAgreementRequired) {
        this.billingAgreementRequired = billingAgreementRequired;
    }

    /**
     * @return the cancelUrl
     */
    public String getCancelUrl() {
        return cancelUrl;
    }

    /**
     * @param cancelUrl
     *            the cancelUrl to set
     */
    public void setCancelUrl(final String cancelUrl) {
        this.cancelUrl = cancelUrl;
    }

    /**
     * @return the returnUrl
     */
    public String getReturnUrl() {
        return returnUrl;
    }

    /**
     * @param returnUrl
     *            the returnUrl to set
     */
    public void setReturnUrl(final String returnUrl) {
        this.returnUrl = returnUrl;
    }

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId
     *            the sessionId to set
     */
    public void setSessionId(final String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @return the savedCreditCards
     */
    public List<CreditCard> getSavedCreditCards() {
        return savedCreditCards;
    }

    /**
     * @param savedCreditCards
     *            the savedCreditCards to set
     */
    public void setSavedCreditCards(final List<CreditCard> savedCreditCards) {
        this.savedCreditCards = savedCreditCards;
    }

    /**
     * @return the ipgPaymentTemplateType
     */
    public IpgPaymentTemplateType getIpgPaymentTemplateType() {
        return ipgPaymentTemplateType;
    }

    /**
     * @param ipgPaymentTemplateType
     *            the ipgPaymentTemplateType to set
     */
    public void setIpgPaymentTemplateType(final IpgPaymentTemplateType ipgPaymentTemplateType) {
        this.ipgPaymentTemplateType = ipgPaymentTemplateType;
    }

}