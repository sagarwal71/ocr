package au.com.target.tgtpayment.commands.result;

//CHECKSTYLE:OFF
import de.hybris.platform.payment.dto.BillingInfo;

import au.com.target.tgtpayment.commands.TargetCaptureCommand;


//CHECKSTYLE:ON

/**
 * 
 * Result for {@link TargetCaptureCommand}
 * 
 */
public class TargetCaptureResult extends TargetAbstractPaymentResult
{
    private BillingInfo billingInfo;

    /**
     * @return the billingInfo
     */
    public BillingInfo getBillingInfo() {
        return billingInfo;
    }

    /**
     * @param billingInfo
     *            the billingInfo to set
     */
    public void setBillingInfo(final BillingInfo billingInfo) {
        this.billingInfo = billingInfo;
    }

}
