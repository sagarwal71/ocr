/**
 * 
 */
package au.com.target.tgtpayment.exceptions;

import de.hybris.platform.payment.AdapterException;


public class InvalidTransactionRequestException extends AdapterException {

    public InvalidTransactionRequestException(final String message)
    {
        super(message);
    }

    public InvalidTransactionRequestException(final Exception exception)
    {
        super(exception);
    }

}
