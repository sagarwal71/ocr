package au.com.target.tgtpayment.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.model.PaymentsInProgressModel;



/**
 * Business service for handling {@link PaymentsInProgressModel}
 * 
 */
public interface PaymentsInProgressService {

    /**
     * 
     * @param abstractOrderModel
     * @param captureType
     *            eg for place order or layby payment
     * @return created PaymentsInProgressModel
     */
    PaymentsInProgressModel createInProgressPayment(AbstractOrderModel abstractOrderModel,
            PaymentTransactionEntryModel ptem,
            PaymentCaptureType captureType);

    /**
     * Check if the {@link AbstractOrderModel} has a corresponding {@link PaymentsInProgressModel}
     * 
     * @param abstractOrderModel
     * @return result
     */
    boolean hasInProgressPayment(AbstractOrderModel abstractOrderModel);

    /**
     * Removes the {@link PaymentsInProgressModel} corresponding to the {@link AbstractOrderModel} if present
     * 
     * @param abstractOrderModel
     * @return result
     */
    boolean removeInProgressPayment(AbstractOrderModel abstractOrderModel);

}
