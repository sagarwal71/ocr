package au.com.target.tgtpayment.service;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.math.BigDecimal;
import java.util.List;

import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.dto.GiftCardReversalData;
import au.com.target.tgtpayment.dto.HostedSessionTokenRequest;
import au.com.target.tgtpayment.dto.PaymentTransactionEntryData;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.exceptions.InvalidTransactionRequestException;
import au.com.target.tgtpayment.methods.TargetPaymentMethod;



public interface TargetPaymentService {

    /**
     * @param hostedSessionTokenRequest
     * @return HostedSessionToken
     */
    TargetCreateSubscriptionResult getHostedSessionToken(HostedSessionTokenRequest hostedSessionTokenRequest);

    /**
     * Saves the hosted session details in the payment gateway and returns a token. Typically used with card payments
     * 
     * @param sessionToken
     * @param paymentMode
     * @return savedToken
     */
    String tokenize(String sessionToken, PaymentModeModel paymentMode);


    /**
     * Capture the specified payment
     * 
     * @param orderModel
     * @param paymentInfo
     * @param amount
     * @param currency
     * @param paymentCaptureType
     * @return payment transaction entry
     */
    PaymentTransactionEntryModel capture(AbstractOrderModel orderModel, PaymentInfoModel paymentInfo,
            BigDecimal amount,
            CurrencyModel currency,
            PaymentCaptureType paymentCaptureType);

    /**
     * Capture payment
     * 
     * @param orderModel
     * @param paymentInfo
     * @param amount
     * @param currency
     * @param paymentCaptureType
     * @param paymentTransactionModel
     */
    void capture(AbstractOrderModel orderModel,
            PaymentInfoModel paymentInfo,
            BigDecimal amount,
            CurrencyModel currency,
            PaymentCaptureType paymentCaptureType,
            PaymentTransactionModel paymentTransactionModel);

    /**
     * if the original capture fails we need to re capture the transaction
     * 
     * @param paymentTransactionEntry
     */
    void retryCapture(PaymentTransactionEntryModel paymentTransactionEntry);

    /**
     * 
     * @param model
     * @param amount
     * @return payment transaction entry
     */
    List<PaymentTransactionEntryModel> refundFollowOn(PaymentTransactionModel model, BigDecimal amount);


    /**
     * refund the amount in PaymentTransactionEntryModel
     * 
     * @param model
     * @param amount
     * @return PaymentTransactionEntryModel
     */
    PaymentTransactionEntryModel refundFollowOn(PaymentTransactionEntryModel model, BigDecimal amount);

    /**
     * Try to retrieve and update the payment transaction entry from the gateway.
     * 
     * @param entry
     */
    void retrieveTransactionEntry(PaymentTransactionEntryModel entry)
            throws InvalidTransactionRequestException;

    /**
     * For standalone refund, capture a small amount on the new credit card details so that a larger amount can be
     * refunded.
     * 
     * @param orderModel
     * @param paymentInfo
     * @param captureOrderId
     *            transaction id to link standalone capture with refund
     * @return payment transaction entry for the small capture
     */
    PaymentTransactionEntryModel refundStandaloneCapture(AbstractOrderModel orderModel, PaymentInfoModel paymentInfo,
            String captureOrderId);

    /**
     * Standalone refund, depends on refundStandaloneCapture called previously.
     * 
     * @param orderModel
     * @param captureEntry
     * @param amount
     * @param captureOrderId
     * @return payment transaction entry for the refund
     */
    PaymentTransactionEntryModel refundStandaloneRefund(AbstractOrderModel orderModel,
            PaymentTransactionEntryModel captureEntry, BigDecimal amount, String captureOrderId);

    /**
     * perform ipg manual refund and save the entry
     * 
     * @param captureTransaction
     * @param amount
     * @param receiptNo
     * @return PaymentTransactionEntryModel
     */
    PaymentTransactionEntryModel refundIpgManualRefund(PaymentTransactionModel captureTransaction, BigDecimal amount,
            String receiptNo);

    /**
     * Creates a payment transaction entry to take payment for given order
     * 
     * @param orderModel
     * @return payment transaction entry
     */
    PaymentTransactionEntryModel createPaymentTransactionEntry(AbstractOrderModel orderModel);

    /**
     * update saved card information
     * 
     * @param orderModel
     * @param savedCards
     */
    void updateSavedCards(AbstractOrderModel orderModel, List<TargetCardResult> savedCards);

    /**
     * @param orderModel
     * @param result
     * @return PaymentTransactionModel
     */
    PaymentTransactionModel createTransactionWithQueryResult(AbstractOrderModel orderModel,
            TargetQueryTransactionDetailsResult result);

    /**
     * accepts payment transaction type, so that while updating credit card for preOrder, transction entry with type
     * DEFERRED can be created.
     * 
     * @param orderModel
     * @param result
     * @param transactionType
     * @return paymentTransactionModel transactionQueryResult
     */
    PaymentTransactionModel createTransactionWithQueryResult(final AbstractOrderModel orderModel,
            final TargetQueryTransactionDetailsResult result, final PaymentTransactionType transactionType);

    /**
     * Returns successful payments
     * 
     * @param order
     * @return successful payments
     */
    List<PaymentInfoModel> getSuccessfulPayments(OrderModel order);

    /**
     * Perform payment reversal - the difference between reversal and refund is that reversal causes payment to be
     * considered as it never happened
     * 
     * @param cartModel
     *            - payment info model
     * @param cards
     *            - list of cards to void payment to
     * @return is reversal succeeded
     */
    boolean reversePayment(CartModel cartModel, List<TargetCardResult> cards);

    /**
     * Perform payment reversal - the difference between reversal and refund is that reversal causes payment to be
     * considered as it never happened
     * 
     * @param reversalData
     *            - DTO holds all required information for gift reversal
     * @return is reversal succeeded
     */
    boolean reversePayment(GiftCardReversalData reversalData);

    /**
     * refund the latest transaction of a cart/order if it only contains capture entries.
     * 
     * @param abstractOrderModel
     */
    void refundLastCaptureTransaction(AbstractOrderModel abstractOrderModel);

    /**
     * Get refunded amount from refunded entries
     * 
     * @param refundedEntries
     * @return refundedAmount
     */
    BigDecimal findRefundedAmountAfterRefundFollowOn(List<PaymentTransactionEntryModel> refundedEntries);

    /**
     * query the failed gift card payment attempts from IPG
     * 
     * @param abstractOrderModel
     * @return list of failed payment details
     */
    List<PaymentTransactionEntryData> retrieveFailedGiftcardPayment(AbstractOrderModel abstractOrderModel);

    /**
     * Get gift card payment information
     * 
     * @param cartModel
     *            - cart
     * @return - list of gift cards associated with payment
     */
    List<TargetCardResult> getGiftCardPayments(CartModel cartModel);

    /**
     * Query transaction details. This is specific to IPG
     * 
     * @param paymentMethod
     * @return query transaction details result
     */
    TargetQueryTransactionDetailsResult queryTransactionDetails(TargetPaymentMethod paymentMethod,
            String ipgToken, String ipgSessionId);

    /**
     * Reverse gift card payments
     * 
     * @param cartModel
     * 
     * @return - has gift card reversal successful
     */
    boolean reverseGiftCardPayment(CartModel cartModel);

    /**
     * @param ipgToken
     * @param ipgSessionId
     * @return false if failed to reverse any gift card payment, otherwise true (including no gift card found)
     */
    boolean reverseGiftCardPayment(String ipgToken, String ipgSessionId);

    /**
     * @param paymentMethod
     * @param paymentInfoModel
     * @return the result of the query to get the transaction details
     */
    TargetQueryTransactionDetailsResult queryTransactionDetails(TargetPaymentMethod paymentMethod,
            PaymentInfoModel paymentInfoModel);

    /**
     * Method to create the request for session token generation.
     * 
     * @param cart
     * @param paymentMode
     * @param cancelUrl
     * @param returnUrl
     * @param pct
     * @param sessionId
     * @return sessionTokenRequest
     */
    HostedSessionTokenRequest createHostedSessionTokenRequest(AbstractOrderModel cart, PaymentModeModel paymentMode,
            String cancelUrl, String returnUrl, PaymentCaptureType pct,
            String sessionId, List<CreditCardPaymentInfoModel> savedCreditCards);

    /**
     * Create session token request for IPG card update.
     * 
     * @param order
     * @param paymentMode
     * @param cancelUrl
     * @param returnUrl
     * @param pct
     * @param sessionId
     * @return sessionTokenRequest
     */
    HostedSessionTokenRequest createUpdateCardHostedSessionTokenRequest(AbstractOrderModel order,
            PaymentModeModel paymentMode, String cancelUrl, String returnUrl, PaymentCaptureType pct, String sessionId);

    /**
     * Method to check whether the paymentProvider is available or not.
     * 
     * @param paymentProvider
     * @return boolean
     */
    boolean ping(String paymentProvider);

/**
     * Will check if balance payment captured for preorder - if yes - the update the entry with details if no - return
     * false, so that capture can be triggered next.
     * 
     * @param entry
     * @return - boolean.
     */
    boolean verifyIfPaymentCapturedAndUpdateEntry(final PaymentTransactionEntryModel entry);
}
