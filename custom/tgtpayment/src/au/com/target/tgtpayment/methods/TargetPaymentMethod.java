package au.com.target.tgtpayment.methods;

import de.hybris.platform.payment.commands.factory.CommandFactory;
import de.hybris.platform.payment.commands.request.SubscriptionDataRequest;
import de.hybris.platform.payment.methods.PaymentMethod;

import au.com.target.tgtpayment.commands.request.AbstractTargetStandaloneRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.request.TargetExcessiveRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetGetPaymentConfigurationRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentPingRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentVoidRequest;
import au.com.target.tgtpayment.commands.request.TargetQueryTransactionDetailsRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.request.TargetTokenizeRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.commands.result.TargetExcessiveRefundResult;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpayment.commands.result.TargetGetPaymentConfigurationResult;
import au.com.target.tgtpayment.commands.result.TargetGetSubscriptionResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentPingResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentVoidResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;
import au.com.target.tgtpayment.commands.result.TargetStandaloneRefundResult;
import au.com.target.tgtpayment.commands.result.TargetTokenizeResult;


/**
 * Generic Payment method
 */
public interface TargetPaymentMethod extends PaymentMethod {
    /**
     * 
     * @param captureRequest
     * @return captureResult
     */
    TargetCaptureResult capture(TargetCaptureRequest captureRequest);

    /**
     * 
     * @param tokenizeRequest
     * @return tokenizeResult
     */
    TargetTokenizeResult tokenize(TargetTokenizeRequest tokenizeRequest);

    /**
     * 
     * @param createSubscriptionRequest
     * @return createSubscriptionResult
     */
    TargetCreateSubscriptionResult createSubscription(TargetCreateSubscriptionRequest createSubscriptionRequest);

    /**
     * 
     * @param standaloneRefundRequest
     * @return standaloneRefundResult
     */
    TargetStandaloneRefundResult standaloneRefund(AbstractTargetStandaloneRefundRequest standaloneRefundRequest);

    /**
     * 
     * @param followOnRefundRequest
     * @return followOnRefundResult
     */
    TargetFollowOnRefundResult followOnRefund(TargetFollowOnRefundRequest followOnRefundRequest);

    /**
     * 
     * @param retrieveTransactionRequest
     * @return retrieveTransactionResult
     */
    TargetRetrieveTransactionResult retrieveTransaction(TargetRetrieveTransactionRequest retrieveTransactionRequest);

    /**
     * 
     * @param retrieveTransactionRequest
     * @return retrieveTransactionResult
     */
    TargetExcessiveRefundResult excessiveRefund(TargetExcessiveRefundRequest retrieveTransactionRequest);

    /**
     * 
     * @param commandFactory
     */
    void setCommandFactory(CommandFactory commandFactory);

    /**
     * Retrieve transaction details
     * 
     * @param targetQueryTransactionDetailsRequest
     * @return transaction details
     */
    TargetQueryTransactionDetailsResult queryTransactionDetails(
            TargetQueryTransactionDetailsRequest targetQueryTransactionDetailsRequest);

    /**
     * 
     * @return paymentProviderCode
     */
    String getPaymentProvider();

    /**
     * Void the payment (reversal)
     * 
     * @param voidRequest
     *            - request
     * @return payment void result
     */
    TargetPaymentVoidResult voidPayment(TargetPaymentVoidRequest voidRequest);

    /**
     * Get payment configuration
     * 
     * @return TargetGetPaymentConfigurationResult
     */
    TargetGetPaymentConfigurationResult getConfiguration(TargetGetPaymentConfigurationRequest request);

    /**
     * ping the payment provider to check the availability
     * 
     * @param request
     *            - TargetPaymentPingRequest
     * @return - boolean
     */
    TargetPaymentPingResult ping(TargetPaymentPingRequest request);

    /**
     * invoke get order api of client to verify the order details, before capture payment
     * 
     * @param request
     * @return {@link TargetGetSubscriptionResult}
     */
    TargetGetSubscriptionResult getSubscription(SubscriptionDataRequest request);
}