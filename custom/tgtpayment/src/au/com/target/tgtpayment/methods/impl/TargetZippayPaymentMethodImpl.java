/**
 * 
 */
package au.com.target.tgtpayment.methods.impl;

import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.GetSubscriptionDataCommand;
import de.hybris.platform.payment.commands.factory.CommandFactory;
import de.hybris.platform.payment.commands.factory.CommandNotSupportedException;
import de.hybris.platform.payment.commands.request.SubscriptionDataRequest;

import org.apache.log4j.Logger;

import au.com.target.tgtpayment.commands.TargetCaptureCommand;
import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.commands.TargetFollowOnRefundCommand;
import au.com.target.tgtpayment.commands.TargetPaymentPingCommand;
import au.com.target.tgtpayment.commands.request.AbstractTargetStandaloneRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.request.TargetExcessiveRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetGetPaymentConfigurationRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentPingRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentVoidRequest;
import au.com.target.tgtpayment.commands.request.TargetQueryTransactionDetailsRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.request.TargetTokenizeRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.commands.result.TargetExcessiveRefundResult;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpayment.commands.result.TargetGetPaymentConfigurationResult;
import au.com.target.tgtpayment.commands.result.TargetGetSubscriptionResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentPingResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentVoidResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;
import au.com.target.tgtpayment.commands.result.TargetStandaloneRefundResult;
import au.com.target.tgtpayment.commands.result.TargetTokenizeResult;
import au.com.target.tgtpayment.methods.TargetZippayPaymentMethod;


/**
 * @author salexa10
 *
 */
public class TargetZippayPaymentMethodImpl implements TargetZippayPaymentMethod {

    private static final Logger LOG = Logger.getLogger(TargetZippayPaymentMethodImpl.class);

    private CommandFactory commandFactory;

    @Override
    public TargetCaptureResult capture(final TargetCaptureRequest captureRequest) {
        try {
            final TargetCaptureCommand command = commandFactory.createCommand(TargetCaptureCommand.class);
            return command.perform(captureRequest);
        }
        catch (final CommandNotSupportedException ex) {
            LOG.error(ex);
            throw new AdapterException(ex);
        }
    }

    @Override
    public TargetTokenizeResult tokenize(final TargetTokenizeRequest tokenizeRequest) {
        // TODO: Not implemented yet
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public TargetCreateSubscriptionResult createSubscription(
            final TargetCreateSubscriptionRequest createSubscriptionRequest) {
        try {
            final TargetCreateSubscriptionCommand command = commandFactory
                    .createCommand(TargetCreateSubscriptionCommand.class);

            return command.perform(createSubscriptionRequest);
        }
        catch (final CommandNotSupportedException ex) {
            LOG.error(ex);
            throw new AdapterException(ex);
        }
    }

    @Override
    public TargetStandaloneRefundResult standaloneRefund(
            final AbstractTargetStandaloneRefundRequest standaloneRefundRequest) {
        // TODO: Not implemented yet
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public TargetFollowOnRefundResult followOnRefund(final TargetFollowOnRefundRequest followOnRefundRequest) {
        try {
            final TargetFollowOnRefundCommand command = commandFactory
                    .createCommand(TargetFollowOnRefundCommand.class);

            return command.perform(followOnRefundRequest);
        }
        catch (final CommandNotSupportedException ex) {
            LOG.error(ex);
            throw new AdapterException(ex);
        }
    }

    @Override
    public TargetRetrieveTransactionResult retrieveTransaction(
            final TargetRetrieveTransactionRequest retrieveTransactionRequest) {
        // TODO: Not implemented yet
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public TargetExcessiveRefundResult excessiveRefund(final TargetExcessiveRefundRequest retrieveTransactionRequest) {
        // TODO: Not implemented yet
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public void setCommandFactory(final CommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    @Override
    public String getPaymentProvider() {
        if (commandFactory == null) {
            return null;
        }
        return commandFactory.getPaymentProvider();
    }


    @Override
    public TargetQueryTransactionDetailsResult queryTransactionDetails(
            final TargetQueryTransactionDetailsRequest targetQueryTransactionDetailsRequest) {
        // TODO: Not implemented yet
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public TargetPaymentVoidResult voidPayment(final TargetPaymentVoidRequest voidRequest) {
        // TODO: Not implemented yet
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public TargetGetPaymentConfigurationResult getConfiguration(final TargetGetPaymentConfigurationRequest request) {
        // TODO: Not implemented yet
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public TargetPaymentPingResult ping(final TargetPaymentPingRequest request) {
        try {
            final TargetPaymentPingCommand command = commandFactory
                    .createCommand(TargetPaymentPingCommand.class);

            return command.perform(request);
        }
        catch (final CommandNotSupportedException ex) {
            LOG.error(ex);
            throw new AdapterException(ex);
        }
    }

    @Override
    public TargetGetSubscriptionResult getSubscription(final SubscriptionDataRequest request) {
        try {
            final GetSubscriptionDataCommand command = commandFactory
                    .createCommand(GetSubscriptionDataCommand.class);

            return (TargetGetSubscriptionResult)command.perform(request);
        }
        catch (final CommandNotSupportedException ex) {
            LOG.error(ex);
            throw new AdapterException(ex.getMessage(), ex);
        }
    }


}
