/**
 * 
 */
package au.com.target.tgtpayment.dto;

import java.util.List;



public class Order {
    private List<LineItem> lineItems;
    private Address shippingAddress;
    private AmountType itemSubTotalAmount;
    private AmountType orderTotalAmount;
    private AmountType shippingTotalAmount;
    private String orderId;
    private String userId;
    private Integer orderTotalCents;

    private String orderDescription;

    private Customer customer;
    private Boolean containPreOrderItems;

    private boolean pickup;
    private Address billingAddress;

    /**
     * @return the lineItems
     */
    public List<LineItem> getLineItems() {
        return lineItems;
    }

    /**
     * @param lineItems
     *            the lineItems to set
     */
    public void setLineItems(final List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

    /**
     * @return the shippingAddress
     */
    public Address getShippingAddress() {
        return shippingAddress;
    }

    /**
     * @param shippingAddress
     *            the shippingAddress to set
     */
    public void setShippingAddress(final Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    /**
     * @return the orderTotalAmount
     */
    public AmountType getOrderTotalAmount() {
        return orderTotalAmount;
    }

    /**
     * @param orderTotalAmount
     *            the orderTotalAmount to set
     */
    public void setOrderTotalAmount(final AmountType orderTotalAmount) {
        this.orderTotalAmount = orderTotalAmount;
    }

    /**
     * @return the shippingTotalAmount
     */
    public AmountType getShippingTotalAmount() {
        return shippingTotalAmount;
    }

    /**
     * @param shippingTotalAmount
     *            the shippingTotalAmount to set
     */
    public void setShippingTotalAmount(final AmountType shippingTotalAmount) {
        this.shippingTotalAmount = shippingTotalAmount;
    }

    /**
     * @return the orderDescription
     */
    public String getOrderDescription() {
        return orderDescription;
    }

    /**
     * @param orderDescription
     *            the orderDescription to set
     */
    public void setOrderDescription(final String orderDescription) {
        this.orderDescription = orderDescription;
    }

    /**
     * @return the itemSubTotalAmount
     */
    public AmountType getItemSubTotalAmount() {
        return itemSubTotalAmount;
    }

    /**
     * @param itemSubTotalAmount
     *            the itemSubTotalAmount to set
     */
    public void setItemSubTotalAmount(final AmountType itemSubTotalAmount) {
        this.itemSubTotalAmount = itemSubTotalAmount;
    }

    /**
     * @return the orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * @param orderId
     *            the orderId to set
     */
    public void setOrderId(final String orderId) {
        this.orderId = orderId;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     *            the userId to set
     */
    public void setUserId(final String userId) {
        this.userId = userId;
    }

    /**
     * @return the orderTotalCents
     */
    public Integer getOrderTotalCents() {
        return orderTotalCents;
    }

    /**
     * @param orderTotalCents
     *            the orderTotalCents to set
     */
    public void setOrderTotalCents(final Integer orderTotalCents) {
        this.orderTotalCents = orderTotalCents;
    }

    /**
     * @return the customer
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * @param customer
     *            the customer to set
     */
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }

    /**
     * @return the containPreOrderItems
     */
    public Boolean isContainPreOrderItems() {
        return containPreOrderItems;
    }

    /**
     * @param containPreOrderItems
     *            the containPreOrderItems to set
     */
    public void setContainPreOrderItems(final Boolean containPreOrderItems) {
        this.containPreOrderItems = containPreOrderItems;
    }

    /**
     * @return the pickup
     */
    public boolean isPickup() {
        return pickup;
    }

    /**
     * @param pickup
     *            the pickup to set
     */
    public void setPickup(final boolean pickup) {
        this.pickup = pickup;
    }

    /**
     * @return the billingAddress
     */
    public Address getBillingAddress() {
        return billingAddress;
    }

    /**
     * @param billingAddress
     *            the billingAddress to set
     */
    public void setBillingAddress(final Address billingAddress) {
        this.billingAddress = billingAddress;
    }


}