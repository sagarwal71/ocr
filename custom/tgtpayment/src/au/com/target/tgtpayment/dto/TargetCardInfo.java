package au.com.target.tgtpayment.dto;

import de.hybris.platform.payment.dto.CardInfo;


/**
 * @author asingh78
 * 
 */
public class TargetCardInfo extends CardInfo {

    private String paymentSessionId;

    /**
     * @return the paymentSessionId
     */
    public String getPaymentSessionId() {
        return paymentSessionId;
    }

    /**
     * @param paymentSessionId
     *            the paymentSessionId to set
     */
    public void setPaymentSessionId(final String paymentSessionId) {
        this.paymentSessionId = paymentSessionId;
    }

}
