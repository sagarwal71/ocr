package au.com.target.tgtpayment.dto;

import java.math.BigDecimal;
import java.util.Currency;


/**
 * @author schen13
 * 
 */
public class AmountType {

    private BigDecimal amount;
    private Currency currency;

    /**
     * Default constructor
     */
    public AmountType() {
        super();
    }

    /**
     * @param amount
     * @param currency
     */
    public AmountType(final BigDecimal amount, final Currency currency) {
        super();
        this.amount = amount;
        this.currency = currency;
    }


    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the currency
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * @param currency
     *            the currency to set
     */
    public void setCurrency(final Currency currency) {
        this.currency = currency;
    }


}
