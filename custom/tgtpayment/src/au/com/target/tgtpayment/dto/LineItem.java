/**
 * 
 */
package au.com.target.tgtpayment.dto;

public class LineItem {
    private int number;
    private String productCode;
    private String name;
    private String description;
    private long quantity;
    private AmountType price;
    private String imageUri;
    private String type;
    private boolean discount;

    /**
     * @return the number
     */
    public int getNumber() {
        return number;
    }

    /**
     * @param number
     *            the number to set
     */
    public void setNumber(final int number) {
        this.number = number;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return the quantity
     */
    public long getQuantity() {
        return quantity;
    }

    /**
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(final long quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the price
     */
    public AmountType getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(final AmountType price) {
        this.price = price;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the imageUri
     */
    public String getImageUri() {
        return imageUri;
    }

    /**
     * @param imageUri
     *            the imageUri to set
     */
    public void setImageUri(final String imageUri) {
        this.imageUri = imageUri;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * @return the isDiscount
     */
    public boolean isDiscount() {
        return discount;
    }

    /**
     * @param isDiscount
     *            the isDiscount to set
     */
    public void setDiscount(final boolean isDiscount) {
        this.discount = isDiscount;
    }



}
