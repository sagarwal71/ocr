/**
 * 
 */
package au.com.target.tgtpayment.util;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.IpgGiftCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtpayment.constants.TgtpaymentConstants;
import au.com.target.tgtpayment.dto.PaymentTransactionEntryData;
import au.com.target.tgtpayment.exceptions.PaymentException;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgtpayment.model.ZippayPaymentInfoModel;


public final class PaymentTransactionHelper {

    private static final Logger LOG = Logger.getLogger(PaymentTransactionHelper.class);

    private PaymentTransactionHelper() {
        // avoid construction
    }

    /**
     * Find the PaymentTransactionModel corresponding to the payment transaction entry <br/>
     * of type capture which has accepted status. For ipg we add new flag to check if the transaction is success or not,
     * in order to consider the existing Tns order if the Boolean IsCaptureSuccessful is null, it is the old tns order
     * and we go the old way. There should be only one transaction with IsCaptureSuccessful true.
     * 
     * @param orderModel
     * @return PaymentTransactionModel or null if not found
     */
    public static PaymentTransactionModel findCaptureTransaction(final OrderModel orderModel) {
        if (orderModel == null) {
            return null;
        }
        final List<PaymentTransactionModel> transactions = orderModel.getPaymentTransactions();
        if (orderModel.getPaymentInfo() instanceof IpgPaymentInfoModel) {
            for (final PaymentTransactionModel transaction : transactions) {
                if (BooleanUtils.isTrue(transaction.getIsCaptureSuccessful())) {
                    return transaction;
                }
            }
        }
        else {
            final PaymentTransactionEntryModel entryModel = findCaptureTransaction(transactions);
            return entryModel != null ? entryModel.getPaymentTransaction() : null;
        }
        return null;
    }

    /**
     * Find the capture payment transaction entry which has accepted status
     * 
     * @param paymentTransactions
     * @return the PaymentTransactionEntryModel found or null
     */
    public static PaymentTransactionEntryModel findCaptureTransaction(
            final List<PaymentTransactionModel> paymentTransactions) {
        return findCaptureTransactionInStates(paymentTransactions, ImmutableList.of(TransactionStatus.ACCEPTED));
    }


    /**
     * Find the PaymentTransactionEntryModel for the given STATE and PaymentTransactionModel's
     * 
     * @param paymentTransactions
     * @return the PaymentTransactionEntryModel found or null
     */
    public static PaymentTransactionEntryModel findTransactionEntryWithState(
            final List<PaymentTransactionModel> paymentTransactions,
            final PaymentTransactionType type) {

        if (CollectionUtils.isNotEmpty(paymentTransactions)) {
            for (final PaymentTransactionModel paymentTransaction : paymentTransactions) {
                final PaymentTransactionEntryModel txnEntryModel = filterPaymentTransactionEntry(type,
                        paymentTransaction);
                if (txnEntryModel != null) {
                    return txnEntryModel;
                }
            }
        }

        return null;
    }

    /**
     * @param type
     * @param paymentTransaction
     */
    private static PaymentTransactionEntryModel filterPaymentTransactionEntry(final PaymentTransactionType type,
            final PaymentTransactionModel paymentTransaction) {
        if (CollectionUtils.isNotEmpty(paymentTransaction.getEntries())) {
            for (final PaymentTransactionEntryModel paymentTransactionEntry : paymentTransaction.getEntries()) {
                if (type.equals(paymentTransactionEntry.getType())) {
                    return paymentTransactionEntry;
                }
            }
        }
        return null;
    }

    /**
     * Find the capture transaction entries with refundable amount in the transaction.
     * 
     * @param transaction
     * @return List<PaymentTransactionEntryData>
     */
    protected static List<PaymentTransactionEntryData> findCaptureTransactionEntriesWithRefundableAmount(
            final PaymentTransactionModel transaction) {
        final List<PaymentTransactionEntryData> transactionEntryDataList = new ArrayList<>();
        final List<PaymentTransactionEntryModel> entries = transaction.getEntries();
        if (CollectionUtils.isEmpty(entries)) {
            return transactionEntryDataList;
        }

        for (final PaymentTransactionEntryModel entry : entries) {
            if (entry.getType() == PaymentTransactionType.CAPTURE
                    && entry.getTransactionStatus().equals(TransactionStatus.ACCEPTED.name())) {
                BigDecimal capturedAmount = new BigDecimal(0);
                capturedAmount = capturedAmount.add(entry.getAmount());

                final BigDecimal refundedAmount = findRefundedAmountForCaptureTransactionEntry(entry);

                if (capturedAmount.compareTo(refundedAmount) > 0) {
                    transactionEntryDataList.add(new PaymentTransactionEntryData(entry, capturedAmount
                            .subtract(refundedAmount)));
                }
                else if (capturedAmount.compareTo(refundedAmount) < 0) {
                    LOG.error(
                            MessageFormat
                                    .format("Refund amount is bigger than capture amount in PaymentTransactionEntryModel={0},order={1}",
                                            entry.getPk(), transaction.getOrder() != null ? transaction.getOrder()
                                                    .getPk()
                                                    : StringUtils.EMPTY));
                }
            }
        }
        return transactionEntryDataList;
    }

    /**
     * Create the sorted capture transaction entries with refundable amount in the transaction.
     * 
     * @param transaction
     * @return sorted PaymentTransactionEntryData list
     */
    public static List<PaymentTransactionEntryData> createPaymentTransactionEntryData(
            final PaymentTransactionModel transaction, final BigDecimal amount) {
        if (transaction == null || amount == null || amount.compareTo(BigDecimal.ZERO) <= 0) {
            return null;
        }
        List<PaymentTransactionEntryData> capturedTransactionEntriesDataList = null;
        if (transaction.getInfo() instanceof IpgPaymentInfoModel) {
            capturedTransactionEntriesDataList = findCaptureTransactionEntriesWithRefundableAmount(transaction);
        }
        else {
            capturedTransactionEntriesDataList = findCaptureTransactionEntriesWithRefundableAmountForTns(transaction);
        }
        if (CollectionUtils.isEmpty(capturedTransactionEntriesDataList)) {
            return null;
        }
        final List<PaymentTransactionEntryData> sortedCapturedTransactionEntriesDataList = sortCaptureTransactionEntriesByPaymentInfo(
                capturedTransactionEntriesDataList);
        if (CollectionUtils.isEmpty(sortedCapturedTransactionEntriesDataList)) {
            return null;
        }
        final List<PaymentTransactionEntryData> paymentTransactionEntryDataForRefund = createPaymentTransactionEntryDataForRefund(
                sortedCapturedTransactionEntriesDataList, amount);
        return filterOutGiftcardPartialRefund(paymentTransactionEntryDataForRefund);
    }

    protected static List<PaymentTransactionEntryData> filterOutGiftcardPartialRefund(
            final List<PaymentTransactionEntryData> paymentTransactionEntryDataList) {
        final List<PaymentTransactionEntryData> filteredList = new ArrayList<>();
        if (paymentTransactionEntryDataList != null) {
            for (final PaymentTransactionEntryData entryData : paymentTransactionEntryDataList) {
                final PaymentTransactionEntryModel entryModel = entryData.getPaymentTransactionEntryModel();
                final BigDecimal refundAmount = entryData.getAmount();
                if (entryModel != null && entryModel.getIpgPaymentInfo() instanceof IpgGiftCardPaymentInfoModel) {
                    if (refundAmount != null && entryModel.getAmount() != null
                            && refundAmount.compareTo(entryModel.getAmount()) == 0) {
                        filteredList.add(entryData);
                    }
                }
                else {
                    filteredList.add(entryData);
                }
            }
        }
        return filteredList;
    }

    /**
     * check if the entry is valid for refund, current logic is type in (REFUND_FOLLOW_ON,REFUND_STANDALONE) status is
     * accept. for ipg if the entry is refunded by giftcard, we regard review as refunded by cs ticket.
     * 
     * @param refundEntry
     * @return boolean
     */
    private static boolean checkIfValidForRefund(final PaymentTransactionEntryModel refundEntry) {
        return (refundEntry.getType() == PaymentTransactionType.REFUND_FOLLOW_ON
                && refundEntry.getTransactionStatus().equals(TransactionStatus.ACCEPTED.name()))
                || (refundEntry.getType() == PaymentTransactionType.REFUND_STANDALONE
                        && refundEntry.getTransactionStatus().equals(TransactionStatus.ACCEPTED.name()));
    }

    /**
     * Find refunded amount for capture transaction entry
     * 
     * @param entry
     * @return refundedAmount
     */
    private static BigDecimal findRefundedAmountForCaptureTransactionEntry(final PaymentTransactionEntryModel entry) {
        BigDecimal refundedAmount = new BigDecimal(0);
        if (entry.getPaymentTransaction() != null
                && CollectionUtils.isNotEmpty(entry.getPaymentTransaction().getEntries())) {
            for (final PaymentTransactionEntryModel refundEntry : entry.getPaymentTransaction().getEntries()) {
                if (checkIfValidForRefund(refundEntry)) {
                    if (entry.getIpgPaymentInfo() != null && refundEntry.getIpgPaymentInfo() != null
                            && entry.getIpgPaymentInfo().getPk().equals(refundEntry.getIpgPaymentInfo().getPk())) {
                        refundedAmount = refundedAmount.add(refundEntry.getAmount());
                    }
                }
            }
        }
        return refundedAmount;
    }

    /**
     * Find the capture transaction entries with amount for tns
     * 
     * @param trans
     * @return List<PaymentTransactionEntryData>
     */
    protected static List<PaymentTransactionEntryData> findCaptureTransactionEntriesWithRefundableAmountForTns(
            final PaymentTransactionModel trans) {
        final List<PaymentTransactionEntryData> transactionEntryDataList = new ArrayList<>();
        final List<PaymentTransactionEntryModel> entries = trans.getEntries();
        if (CollectionUtils.isEmpty(entries)) {
            return transactionEntryDataList;
        }
        BigDecimal refundableAmount = new BigDecimal(0);
        PaymentTransactionEntryModel captureEntry = null;

        for (final PaymentTransactionEntryModel transEntry : trans.getEntries()) {
            if (transEntry.getType() == PaymentTransactionType.CAPTURE
                    && transEntry.getTransactionStatus().equals(TransactionStatus.ACCEPTED.name())) {
                refundableAmount = refundableAmount.add(transEntry.getAmount());
                captureEntry = transEntry;
            }

            if (checkIfValidForRefund(transEntry)) {
                refundableAmount = refundableAmount.subtract(transEntry.getAmount());
            }
        }
        if (captureEntry != null && refundableAmount.compareTo(BigDecimal.ZERO) > 0) {
            transactionEntryDataList.add(new PaymentTransactionEntryData(captureEntry, refundableAmount));
        }
        return transactionEntryDataList;

    }

    /**
     * Sort the PaymentTransactionEntryData list by IpgCreditCardPayment > IpgGiftCardPayment. Because we need to refund
     * to credit card firstly.
     * 
     * @param transactionEntryDataList
     * @return sortedList
     */
    protected static List<PaymentTransactionEntryData> sortCaptureTransactionEntriesByPaymentInfo(
            final List<PaymentTransactionEntryData> transactionEntryDataList) {
        if (CollectionUtils.isNotEmpty(transactionEntryDataList)) {
            final List<PaymentTransactionEntryData> sortedList = new ArrayList<PaymentTransactionEntryData>(
                    transactionEntryDataList);
            Collections.sort(sortedList, new Comparator<PaymentTransactionEntryData>() {

                @Override
                public int compare(final PaymentTransactionEntryData entryData1,
                        final PaymentTransactionEntryData entryData2) {
                    if (entryData1.getPaymentTransactionEntryModel() == null
                            || entryData2.getPaymentTransactionEntryModel() == null
                            || entryData1.getPaymentTransactionEntryModel().getIpgPaymentInfo() == null
                            || entryData2.getPaymentTransactionEntryModel().getIpgPaymentInfo() == null) {
                        return 0;
                    }
                    final PaymentInfoModel paymentInfoModel1 = entryData1.getPaymentTransactionEntryModel()
                            .getIpgPaymentInfo();
                    if (paymentInfoModel1 instanceof IpgGiftCardPaymentInfoModel) {
                        return 1;
                    }
                    else {
                        return -1;
                    }
                }

            });
            return sortedList;
        }
        return null;
    }

    /**
     * create the list of the PaymentTransactionEntryData for refund, hold the amount need to be refunded for each
     * entry.
     * 
     * @param refundablePaymentTransactionEntries
     *            -- it contains the paymentTransactionEntry and refundable amount.
     * @param amount
     *            -- total amount need to be refunded.
     * @return refundablePaymentTransactionEntries which contains the paymentTransactionEntry and the amount need to be
     *         refunded.
     */
    protected static List<PaymentTransactionEntryData> createPaymentTransactionEntryDataForRefund(
            final List<PaymentTransactionEntryData> refundablePaymentTransactionEntries, final BigDecimal amount) {
        final List<PaymentTransactionEntryData> entryDataList = new ArrayList<>();
        BigDecimal totalAmount = amount;
        if (CollectionUtils.isEmpty(refundablePaymentTransactionEntries) || totalAmount == null
                || totalAmount.compareTo(BigDecimal.ZERO) <= 0) {
            return entryDataList;
        }
        for (final PaymentTransactionEntryData entryData : refundablePaymentTransactionEntries) {
            final BigDecimal refundableAmount = entryData.getAmount();
            final PaymentTransactionEntryModel entryModel = entryData.getPaymentTransactionEntryModel();
            if (refundableAmount == null || entryModel == null) {
                continue;
            }
            if (totalAmount.compareTo(refundableAmount) >= 0) {
                totalAmount = totalAmount.subtract(refundableAmount);
                entryDataList.add(entryData);
                if (totalAmount.compareTo(BigDecimal.ZERO) == 0) {
                    break;
                }
            }
            else if (totalAmount.compareTo(refundableAmount) < 0) {
                entryDataList.add(new PaymentTransactionEntryData(entryModel, totalAmount));
                totalAmount = BigDecimal.ZERO;
                break;
            }
        }

        if (totalAmount.compareTo(BigDecimal.ZERO) != 0) {
            throw new PaymentException(
                    "Could not refund follow-on,because the total amount need to be refunded cannot be assigned to each transaction entry properly, the total amount may exceed the refundable amount");
        }
        return entryDataList;
    }


    /**
     * Check if the given payment transactions have a payment transaction entry of type capture in accepted or review
     * status
     * 
     * @param paymentTransactions
     * @return true or false
     */
    public static boolean hasCaptureTransactionInAcceptedOrReview(
            final List<PaymentTransactionModel> paymentTransactions) {

        return (null != findCaptureTransactionInAcceptedOrReview(paymentTransactions));
    }

    /**
     * Get the capture transaction entries for pre-order balance amount.
     * 
     * @param orderModel
     * @return paymentTransactionEntry
     */
    public static PaymentTransactionEntryModel findPreOrderBalanceAmountCaptureTransactionEntry(
            final OrderModel orderModel) {
        if (orderModel == null) {
            return null;
        }
        final List<PaymentTransactionModel> paymentTransactions = new ArrayList<>();
        for (final PaymentTransactionModel paymentTransaction : orderModel.getPaymentTransactions()) {
            if (!paymentTransaction.isIsPreOrderDepositCapture()
                    && BooleanUtils.isTrue(paymentTransaction.getIsCaptureSuccessful())) {
                paymentTransactions.add(paymentTransaction);
                break;
            }
        }
        return findCaptureTransactionInStates(paymentTransactions, ImmutableList.of(TransactionStatus.ACCEPTED));
    }


    /**
     * Find the capture payment transaction entry which has accepted or review status
     * 
     * @param paymentTransactions
     * @return PaymentTransactionEntryModel or null if not found
     */
    protected static PaymentTransactionEntryModel findCaptureTransactionInAcceptedOrReview(
            final List<PaymentTransactionModel> paymentTransactions) {
        final List<TransactionStatus> listStatus = new ArrayList<>();
        listStatus.add(TransactionStatus.ACCEPTED);
        listStatus.add(TransactionStatus.REVIEW);

        return findCaptureTransactionInStates(paymentTransactions, listStatus);
    }


    private static PaymentTransactionEntryModel findCaptureTransactionInStates(
            final List<PaymentTransactionModel> paymentTransactions,
            final List<TransactionStatus> listStatus) {

        if (CollectionUtils.isNotEmpty(paymentTransactions) && CollectionUtils.isNotEmpty(listStatus)) {
            for (final PaymentTransactionModel paymentTransaction : paymentTransactions) {
                if (CollectionUtils.isNotEmpty(paymentTransaction.getEntries())) {
                    for (final PaymentTransactionEntryModel paymentTransactionEntry : paymentTransaction.getEntries()) {
                        for (final TransactionStatus status : listStatus) {

                            if (PaymentTransactionType.CAPTURE.equals(paymentTransactionEntry.getType())
                                    && status.toString().equals(paymentTransactionEntry.getTransactionStatus())) {
                                return paymentTransactionEntry;
                            }
                        }
                    }
                }
            }
        }

        return null;
    }


    /**
     * Find the most recent transaction entry with ACCEPTED status
     * 
     * @param paymentTransactions
     * @return PaymentTransactionEntryModel or null if none found
     */
    public static PaymentTransactionEntryModel getLatestAcceptedTransactionEntry(
            final List<PaymentTransactionModel> paymentTransactions) {

        return getLastPayment(paymentTransactions, ImmutableList.of(PaymentTransactionType.AUTHORIZATION,
                PaymentTransactionType.CANCEL,
                PaymentTransactionType.CAPTURE,
                PaymentTransactionType.CREATE_SUBSCRIPTION,
                PaymentTransactionType.DELETE_SUBSCRIPTION,
                PaymentTransactionType.GET_SUBSCRIPTION_DATA,
                PaymentTransactionType.PARTIAL_CAPTURE,
                PaymentTransactionType.REFUND_FOLLOW_ON,
                PaymentTransactionType.REFUND_STANDALONE,
                PaymentTransactionType.UPDATE_SUBSCRIPTION), TransactionStatus.ACCEPTED);
    }


    /**
     * Find the most recent payment transaction type entry with status passed in the method.
     * 
     * @param paymentTransactions
     * @param transactionType
     * @param transactionStatus
     * @return PaymentTransactionEntryModel or null if none found
     */
    public static PaymentTransactionEntryModel getLastPayment(final List<PaymentTransactionModel> paymentTransactions,
            final List<PaymentTransactionType> transactionType, final TransactionStatus transactionStatus) {
        Assert.notNull(transactionType, "transaction type must not be null");
        if (paymentTransactions == null || paymentTransactions.isEmpty()) {
            return null;
        }

        PaymentTransactionEntryModel lastPayment = null;

        for (final PaymentTransactionModel paymentTransaction : paymentTransactions) {
            for (final PaymentTransactionEntryModel ptem : paymentTransaction.getEntries()) {
                if (transactionType.contains(ptem.getType())
                        && null != transactionStatus &&
                        transactionStatus.toString().equals(ptem.getTransactionStatus())) {
                    if (lastPayment == null || lastPayment.getTime() == null
                            || (ptem.getTime() != null && ptem.getTime().after(lastPayment.getTime()))) {
                        lastPayment = ptem;
                    }
                }
            }
        }

        return lastPayment;
    }

    /**
     * 
     * @param order
     * @return true if last transaction is pending (Review)
     */
    public static boolean isLastTransactionPending(final OrderModel order) {
        if (null == order) {
            return false;
        }
        final List<PaymentTransactionModel> paymentTransactionList = order.getPaymentTransactions();
        final int sizeOfPaymentTransactionList = paymentTransactionList.size();
        boolean inReview = false;
        if (sizeOfPaymentTransactionList <= 0) {
            return false;
        }
        final PaymentTransactionModel paymentTransactionModel = paymentTransactionList
                .get(sizeOfPaymentTransactionList - 1);
        if (CollectionUtils.isEmpty(paymentTransactionModel.getEntries())) {
            return false;
        }

        for (final PaymentTransactionEntryModel paymentTransactionEntryModel : paymentTransactionModel.getEntries()) {
            if (TransactionStatus.ACCEPTED.toString().equalsIgnoreCase(
                    paymentTransactionEntryModel.getTransactionStatus())) {
                return false;
            }
            if (TransactionStatus.REVIEW.toString().equalsIgnoreCase(
                    paymentTransactionEntryModel.getTransactionStatus())) {
                inReview = true;
            }
        }
        return inReview;
    }

    /**
     * Get the latest payment transaction for a cart
     * 
     * @param abstractOrderModel
     * @return latestTran
     */
    public static PaymentTransactionModel findLastTransaction(final AbstractOrderModel abstractOrderModel) {
        Assert.notNull(abstractOrderModel, "Cart/Order cannot be empty");
        PaymentTransactionModel latestTran = null;
        if (abstractOrderModel.getPaymentTransactions() != null) {
            for (final PaymentTransactionModel cur : abstractOrderModel.getPaymentTransactions()) {
                if (CollectionUtils.isNotEmpty(cur.getEntries())) {
                    if (latestTran == null
                            || latestTran.getCreationtime() == null
                            || (cur.getCreationtime() != null && cur.getCreationtime().after(
                                    latestTran.getCreationtime()))) {
                        latestTran = cur;
                    }
                }
            }
        }
        return latestTran;
    }

    /**
     * check if a given transaction only contains capture entries.
     * 
     * @param cur
     * @return true if it contains capture entry, false otherwise.
     */
    public static boolean isCaptureTransaction(final PaymentTransactionModel cur) {
        if (cur == null || CollectionUtils.isEmpty(cur.getEntries())) {
            return false;
        }
        for (final PaymentTransactionEntryModel entry : cur.getEntries()) {
            if (!PaymentTransactionType.CAPTURE.equals(entry.getType())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Extract expire month from card expire information sent by IPG
     * 
     * @param cardExpiry
     * @return expire month
     */
    public static String extractCardExpiryMonth(final String cardExpiry) {
        String expiryMonth = null;
        if (StringUtils.isNotEmpty(cardExpiry)) {
            final String[] arr = cardExpiry.split("/");
            if (arr != null && arr.length > 1) {
                expiryMonth = arr[0];
            }
        }
        return expiryMonth;
    }

    /**
     * Extract expire year from card expire information sent by IPG
     * 
     * @param cardExpiry
     * @return expire year
     */
    public static String extractCardExpiryYear(final String cardExpiry) {
        String expiryYear = null;
        if (StringUtils.isNotEmpty(cardExpiry)) {
            final String[] arr = cardExpiry.split("/");
            if (arr != null && arr.length > 1) {
                expiryYear = arr[1];
            }
        }
        return expiryYear;
    }

    /**
     * Method to capture card type.
     * 
     * @param paymentInfoModel
     */
    public static String capturPaymentType(final PaymentInfoModel paymentInfoModel) {

        if (paymentInfoModel instanceof IpgPaymentInfoModel) {
            return TgtpaymentConstants.CREDIT_GIFT_CARD;
        }
        else if (paymentInfoModel instanceof PaypalPaymentInfoModel) {
            return TgtpaymentConstants.PAY_PAL_PAYMENT_TYPE;
        }
        else if (paymentInfoModel instanceof AfterpayPaymentInfoModel) {
            return TgtpaymentConstants.AFTERPAY_PAYMENT_TYPE;
        }
        else if (paymentInfoModel instanceof ZippayPaymentInfoModel) {
            return TgtpaymentConstants.ZIPPAY_PAYMENT_TYPE;
        }
        else {
            return TgtpaymentConstants.IPG_PAYMENT_TYPE;
        }
    }

}