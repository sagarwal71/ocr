package au.com.target.tgtpayment.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.util.Assert;


/**
 * Utility class for math operations with prices that uses {@link BigDecimal}.
 */
public final class PriceCalculator {

    private static final int SCALE = 2;
    private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;

    /**
     * Hidden constructor for static utility class.
     */
    private PriceCalculator() {
        // intended hidden constructor
    }

    /**
     * Adds {@code addend} to {@code augend}.
     * 
     * @param addend
     *            the value to be added to initial value
     * @param augend
     *            the initial value
     * @return {@code augend} + {@code augend} as BigDecimal
     * @throws IllegalArgumentException
     *             if any of arguments is null
     */
    public static BigDecimal add(final Double addend, final Double augend) throws IllegalArgumentException {
        Assert.notNull(addend, "double value addend must not be null");
        Assert.notNull(augend, "double value augend must not be null");

        return BigDecimal.valueOf(addend.doubleValue())
                .add(BigDecimal.valueOf(augend.doubleValue()))
                .setScale(SCALE, ROUNDING_MODE);
    }

    /**
     * Subtracts {@code subtrahend} from {@code minuend}.
     * 
     * @param subtrahend
     *            the value to be subtracted from this initial value
     * @param minuend
     *            the initial value
     * @return {@code minuend} - {@code subtrahend} as BigDecimal
     * @throws IllegalArgumentException
     *             if any of arguments is null
     */
    public static BigDecimal subtract(final Double subtrahend, final Double minuend) throws IllegalArgumentException {
        Assert.notNull(subtrahend, "subtrahend must not be null");
        Assert.notNull(minuend, "minuend must not be null");

        return BigDecimal.valueOf(minuend.doubleValue())
                .subtract(BigDecimal.valueOf(subtrahend.doubleValue()))
                .setScale(SCALE, ROUNDING_MODE);
    }

    /**
     * Compares two decimal values.
     * 
     * @param arg1
     *            the first price
     * @param arg2
     *            the second price
     * @return {@code true} if both arguments are equal, {@code false} otherwise
     * @throws IllegalArgumentException
     *             if any of arguments is null
     */
    public static boolean equals(final Double arg1, final Double arg2) throws IllegalArgumentException {
        Assert.notNull(arg1, "arg1 must not be null");
        Assert.notNull(arg2, "arg2 must not be null");

        return BigDecimal.valueOf(arg1.doubleValue()).setScale(SCALE, ROUNDING_MODE)
                .compareTo(BigDecimal.valueOf(arg2.doubleValue()).setScale(SCALE, ROUNDING_MODE)) == 0;
    }

    /**
     * Just return the given Double amount as price formatted BigDecimal
     * 
     * @param amount
     * @return amount as BigDecimal
     */
    public static BigDecimal getAsPrice(final Double amount) {
        Assert.notNull(amount, "amount must not be null");

        return BigDecimal.valueOf(amount.doubleValue()).setScale(SCALE, ROUNDING_MODE);

    }

}
