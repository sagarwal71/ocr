/**
 * 
 */
package au.com.target.tgtpayment.util;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtpayment.constants.TgtpaymentConstants;
import au.com.target.tgtutility.util.TargetProductUtils;


/**
 * Helper for payment order.
 * 
 * @author salexa10
 *
 */
public class TargetPaymentOrderHelper {

    private String baseSecureUrl;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    /**
     * Retrieve the Thumbnail URL for the product
     * 
     * @param product
     * @return String
     */
    public String getProductThumbnailUrl(final ProductModel product) {
        String thumbnailUrl = StringUtils.EMPTY;
        if (product instanceof AbstractTargetVariantProductModel) {
            final TargetColourVariantProductModel colourVariantProduct = TargetProductUtils
                    .getColourVariantProduct((AbstractTargetVariantProductModel)product);
            if (null != colourVariantProduct && null != colourVariantProduct.getThumbnail()
                    && null != colourVariantProduct.getThumbnail().getURL()) {
                thumbnailUrl = getBaseSecureUrl()
                        + colourVariantProduct.getThumbnail().getURL();
            }
        }
        return thumbnailUrl;
    }

    /**
     * Retrieve the base Url
     * 
     * @return String
     */
    public String getBaseSecureUrl() {
        if (StringUtils.isEmpty(baseSecureUrl)) {
            baseSecureUrl = configurationService.getConfiguration().getString(
                    TgtpaymentConstants.BASE_SECURE_URL_KEY);
        }
        return baseSecureUrl;
    }

    /**
     * @return the configurationService
     */
    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    /**
     * @param configurationService
     *            the configurationService to set
     */
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }


}