package au.com.target.tgtpayment.strategy.impl;

import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.payment.commands.factory.CommandFactory;
import de.hybris.platform.payment.commands.factory.CommandFactoryRegistry;

import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtpayment.methods.TargetPaymentMethod;
import au.com.target.tgtpayment.strategy.PaymentMethodStrategy;


public class PaymentMethodStrategyImpl implements PaymentMethodStrategy {

    private CommandFactoryRegistry commandFactoryRegistry;

    private Map<String, TargetPaymentMethod> paymentMethodMapping;

    private Map<String, TargetPaymentMethod> paymentMode2MethodMapping;

    private Map<String, String> paymentProviderMapping;

    private Map<String, String> paymentMode2ProviderMapping;


    /* (non-Javadoc)
     * @see au.com.target.tgtpayment.strategy.PaymentMethodStrategy#getPaymentMethod(de.hybris.platform.core.model.order.payment.PaymentInfoModel, java.lang.String)
     */
    @Override
    public TargetPaymentMethod getPaymentMethod(final PaymentInfoModel paymentInfoModel, final String paymentProvider) {
        final CommandFactory commandFactory = commandFactoryRegistry.getFactory(paymentProvider);

        final TargetPaymentMethod paymentMethod = paymentMethodMapping.get(paymentInfoModel.getClass().getSimpleName());
        paymentMethod.setCommandFactory(commandFactory);

        return paymentMethod;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtpayment.strategy.PaymentMethodStrategy#getPaymentMethod(de.hybris.platform.core.model.order.payment.PaymentInfoModel)
     */
    @Override
    public TargetPaymentMethod getPaymentMethod(final PaymentInfoModel paymentInfoModel) {

        final CommandFactory commandFactory = commandFactoryRegistry.getFactory(paymentProviderMapping
                .get(paymentInfoModel.getClass().getSimpleName()));

        final TargetPaymentMethod paymentMethod = paymentMethodMapping.get(paymentInfoModel.getClass().getSimpleName());
        paymentMethod.setCommandFactory(commandFactory);

        return paymentMethod;
    }

    @Override
    public TargetPaymentMethod getPaymentMethod(final PaymentModeModel paymentModeModel) {
        final CommandFactory commandFactory = commandFactoryRegistry.getFactory(paymentMode2ProviderMapping
                .get(paymentModeModel.getCode()));

        final TargetPaymentMethod paymentMethod = paymentMode2MethodMapping.get(paymentModeModel.getCode());
        paymentMethod.setCommandFactory(commandFactory);

        return paymentMethod;
    }

    @Override
    public TargetPaymentMethod getPaymentMethod(final String provider) {
        final CommandFactory commandFactory = commandFactoryRegistry.getFactory(paymentMode2ProviderMapping
                .get(provider));
        final TargetPaymentMethod paymentMethod = paymentMode2MethodMapping.get(provider);
        paymentMethod.setCommandFactory(commandFactory);
        return paymentMethod;
    }

    /**
     * @param commandFactoryRegistry
     *            the commandFactoryRegistry to set
     */
    @Required
    public void setCommandFactoryRegistry(final CommandFactoryRegistry commandFactoryRegistry) {
        this.commandFactoryRegistry = commandFactoryRegistry;
    }

    /**
     * @param paymentMethodMapping
     *            the paymentMethodMapping to set
     */
    @Required
    public void setPaymentMethodMapping(final Map<String, TargetPaymentMethod> paymentMethodMapping) {
        this.paymentMethodMapping = paymentMethodMapping;
    }

    /**
     * @param paymentProviderMapping
     *            the paymentProviderMapping to set
     */
    @Required
    public void setPaymentProviderMapping(final Map<String, String> paymentProviderMapping) {
        this.paymentProviderMapping = paymentProviderMapping;
    }

    /**
     * @param paymentMode2MethodMapping
     *            the paymentMode2MethodMapping to set
     */
    @Required
    public void setPaymentMode2MethodMapping(final Map<String, TargetPaymentMethod> paymentMode2MethodMapping) {
        this.paymentMode2MethodMapping = paymentMode2MethodMapping;
    }

    /**
     * @param paymentMode2ProviderMapping
     *            the paymentMode2ProviderMapping to set
     */
    @Required
    public void setPaymentMode2ProviderMapping(final Map<String, String> paymentMode2ProviderMapping) {
        this.paymentMode2ProviderMapping = paymentMode2ProviderMapping;
    }

}
