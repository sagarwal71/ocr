/**
 * 
 */
package au.com.target.tgtpayment.resumer;

import au.com.target.tgtpayment.enums.PaymentCaptureType;


/**
 * Return the appropriate PaymentProcessResumer
 * 
 */
public interface PaymentProcessResumerFactory {

    /**
     * Find the right resumer for the given type.
     * 
     * @param paymentCaptureType
     * @return PaymentProcessResumer subclass or null if the right one cant be found
     */
    PaymentProcessResumer getPaymentProcessResumer(PaymentCaptureType paymentCaptureType);

}
