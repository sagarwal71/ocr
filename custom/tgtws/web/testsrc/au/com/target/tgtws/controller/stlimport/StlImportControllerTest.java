/**
 * 
 */
package au.com.target.tgtws.controller.stlimport;

import de.hybris.bootstrap.annotations.UnitTest;

import org.hamcrest.core.StringContains;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationShopTheLookDto;
import au.com.target.tgtwsfacades.stlimport.ShopTheLookImportIntegrationFacade;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StlImportControllerTest {

    @Mock
    private ShopTheLookImportIntegrationFacade shopTheLookImportIntegrationFacade;

    @InjectMocks
    private final StlImportController stlImportController = new StlImportController();

    @Test
    public void testPutStl() {
        final String code = "STL01";
        final IntegrationShopTheLookDto dto = new IntegrationShopTheLookDto();
        dto.setCode(code);
        final IntegrationResponseDto responseDto = new IntegrationResponseDto(code);
        responseDto.setSuccessStatus(true);
        responseDto.addMessage("Success");

        Mockito.when(shopTheLookImportIntegrationFacade.persistTargetShopTheLook(dto)).thenReturn(responseDto);

        final IntegrationResponseDto response = stlImportController.putStl(code, dto);
        Assert.assertTrue(response.isSuccessStatus());
        Assert.assertEquals(1, response.getMessages().size());
        Assert.assertEquals(code, response.getCode());
        Assert.assertEquals("Success", response.getMessages().get(0));
    }

    @Test
    public void testPutStlMismatchingCode() {
        final String code = "STL01";
        final IntegrationShopTheLookDto dto = new IntegrationShopTheLookDto();
        dto.setCode(code);
        final IntegrationResponseDto responseDto = new IntegrationResponseDto(code);
        responseDto.setSuccessStatus(true);
        responseDto.addMessage("Success");

        Mockito.when(shopTheLookImportIntegrationFacade.persistTargetShopTheLook(dto)).thenReturn(responseDto);

        final IntegrationResponseDto response = stlImportController.putStl("STL02", dto);
        Assert.assertFalse(response.isSuccessStatus());
        Assert.assertEquals(1, response.getMessages().size());
        Assert.assertEquals(code, response.getCode());
        Assert.assertThat(response.getMessages().get(0), new StringContains("code is different"));
    }
}
