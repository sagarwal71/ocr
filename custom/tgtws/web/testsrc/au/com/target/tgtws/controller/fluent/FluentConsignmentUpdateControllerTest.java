/**
 * 
 */
package au.com.target.tgtws.controller.fluent;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtfluent.data.Fulfilment;
import au.com.target.tgtfluent.exception.FluentFulfilmentException;
import au.com.target.tgtfluent.service.FluentFulfilmentService;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentConsignmentUpdateControllerTest {

    @Mock
    private FluentFulfilmentService fluentFulfilmentService;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private final FluentConsignmentUpdateController fluentFulfilmentUpdateController = new FluentConsignmentUpdateController();

    @Test
    public void testUpsertFulfilment()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, FluentFulfilmentException {
        final Fulfilment fulfilment = mock(Fulfilment.class);

        final ResponseEntity responseEntity = fluentFulfilmentUpdateController.upsertFulfilment("eventHorizon",
                fulfilment);
        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        verify(fluentFulfilmentService).upsertFulfilment(fulfilment);
    }

    @Test
    public void testUpsertFulfilmentBadRequest()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, FluentFulfilmentException {
        final Fulfilment fulfilment = mock(Fulfilment.class);
        willReturn("2048").given(fulfilment).getFulfilmentId();
        willThrow(new IllegalArgumentException()).given(fluentFulfilmentService).upsertFulfilment(fulfilment);

        final ResponseEntity responseEntity = fluentFulfilmentUpdateController.upsertFulfilment("eventHorizon",
                fulfilment);
        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void testUpsertFulfilmentFailedDependency()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, FluentFulfilmentException {
        final Fulfilment fulfilment = mock(Fulfilment.class);
        willReturn("2048").given(fulfilment).getFulfilmentId();
        willThrow(new FluentFulfilmentException("")).given(fluentFulfilmentService).upsertFulfilment(fulfilment);

        final ResponseEntity responseEntity = fluentFulfilmentUpdateController.upsertFulfilment("eventHorizon",
                fulfilment);
        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.FAILED_DEPENDENCY);
    }
}
