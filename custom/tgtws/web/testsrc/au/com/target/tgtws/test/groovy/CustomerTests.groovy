package au.com.target.tgtws.test.groovy

class CustomerTests extends GroovyTestCase {

    final firstName = "Sven"
    final lastName = "Haiges"
    final titleCode = "dr"
    final password = "test"
    final line1 = "Nymphenburger Str. 86 - Maillingerstraße"
    final town = "München"
    final countryIsoCode = "de"

    /**
     * helper method to register user
     * @return generated userId
     */
    def registerUser() {
        def con = TestUtil.getConnection("/customers", 'POST')
        con.doOutput = true
        def randomUID = System.currentTimeMillis()
        con.outputStream << "login=${randomUID}@sven.de&password=${password}&firstName=${firstName}&lastName=${lastName}&titleCode=${titleCode}"
        assert con.responseCode == HttpURLConnection.HTTP_CREATED : TestUtil.messageResponseCode(con.responseCode, HttpURLConnection.HTTP_CREATED)
        return "${randomUID}@sven.de"
    }

    /**
     * helper method to register user
     * @return generated userId
     */
    def createAddress(uid) {
        def con = TestUtil.getConnection("/customers/current/addresses", 'POST')
        TestUtil.acceptXML(con)
        TestUtil.basicAuth(con, "${uid}", "${password}")
        con.doOutput = true
        con.outputStream << "line1=${line1}&town=${town}&countryIsoCode=${countryIsoCode}"
        assert con.responseCode == HttpURLConnection.HTTP_OK : TestUtil.messageResponseCode(con.responseCode, HttpURLConnection.HTTP_OK)

        def response = new XmlSlurper().parseText(con.inputStream.text)
        assert response.line1 == "${line1}"
        assert response.town == "${town}"
        assert response.country.isocode == "${countryIsoCode}"

        return response.id
    }

    void testRegisterDuplicateUID() {
        def uid = registerUser()

        //try to register another user with same UID
        def con = TestUtil.getConnection("/customers", 'POST')
        TestUtil.acceptXML(con)
        con.doOutput = true
        con.outputStream << "login=${uid}&password=${password}&firstName=${firstName}&lastName=${lastName}&titleCode=${titleCode}"
        assert con.responseCode == HttpURLConnection.HTTP_BAD_REQUEST : TestUtil.messageResponseCode(con.responseCode, HttpURLConnection.HTTP_BAD_REQUEST)

        def response = new XmlSlurper().parseText(con.errorStream.text)
        assert response.'class' == 'DuplicateUidException'
        assert response.message == "${uid}"
    }

    void testGetCurrentUser() {
        def uid = registerUser()

        //check customer profile
        def con = TestUtil.getConnection("/customers/current", 'GET')
        TestUtil.acceptXML(con)
        TestUtil.basicAuth(con, "${uid}", "${password}")
        assert con.responseCode == HttpURLConnection.HTTP_OK : TestUtil.messageResponseCode(con.responseCode, HttpURLConnection.HTTP_OK)
        def response = new XmlSlurper().parseText(con.inputStream.text)
        assert response.uid == "${uid}"
        assert response.firstName == "${firstName}"
        assert response.lastName == "${lastName}"
        assert response.titleCode == "${titleCode}"
    }

    void testChangePassword() {
        def uid = registerUser()

        //change password
        def con = TestUtil.getConnection("/customers/current/password", 'POST')
        TestUtil.acceptXML(con)
        TestUtil.basicAuth(con, "${uid}", "${password}")
        con.doOutput = true
        con.outputStream << "old=${password}&new=newpassword"
        assert con.responseCode == HttpURLConnection.HTTP_ACCEPTED : TestUtil.messageResponseCode(con.responseCode, HttpURLConnection.HTTP_ACCEPTED)
    }

    void testAddressBook() {
        def uid = registerUser()
        def aid = createAddress(uid)

        //check if address is listed on address book
        def con = TestUtil.getConnection("/customers/current/addresses", 'GET')
        TestUtil.acceptXML(con)
        TestUtil.basicAuth(con, "${uid}", "${password}")
        assert con.responseCode == HttpURLConnection.HTTP_OK : TestUtil.messageResponseCode(con.responseCode, HttpURLConnection.HTTP_OK)
        def response = new XmlSlurper().parseText(con.inputStream.text)
        assert response.address[0].line1 == "${line1}"
        assert response.address[0].town == "${town}"
        assert response.address[0].country.isocode == "${countryIsoCode}"

        //add another address
        con = TestUtil.getConnection("/customers/current/addresses", 'POST')
        TestUtil.acceptJSON(con)
        TestUtil.basicAuth(con, "${uid}", "${password}")
        con.doOutput = true
        con.outputStream << "line1=Bayerstr. 10a&town=München&countryIsoCode=de&postcode=80335"
        assert con.responseCode == HttpURLConnection.HTTP_OK : TestUtil.messageResponseCode(con.responseCode, HttpURLConnection.HTTP_OK)

        //check if both addresses are listed on address book
        con = TestUtil.getConnection("/customers/current/addresses", 'GET')
        TestUtil.acceptXML(con)
        TestUtil.basicAuth(con, "${uid}", "${password}")
        assert con.responseCode == HttpURLConnection.HTTP_OK : TestUtil.messageResponseCode(con.responseCode, HttpURLConnection.HTTP_OK)
        response = new XmlSlurper().parseText(con.inputStream.text)
        assert response.address[0].line1 == "${line1}"
        assert response.address[0].town == "${town}"
        assert response.address[0].country.isocode == "${countryIsoCode}"
        assert response.address[1].line1 == "Bayerstr. 10a"
        assert response.address[1].town == "München"
        assert response.address[1].postalCode == "80335"
        assert response.address[1].country.isocode == "de"
    }

    void testDeleteAddress() {
        def uid = registerUser()
        def aid = createAddress(uid)

        //delete address
        def con = TestUtil.getConnection("/customers/current/addresses/" + aid, 'DELETE')
        TestUtil.acceptXML(con)
        TestUtil.basicAuth(con, "${uid}", "${password}")
        con.doOutput = true
        assert con.responseCode == HttpURLConnection.HTTP_OK : TestUtil.messageResponseCode(con.responseCode, HttpURLConnection.HTTP_OK)
    }

    void testEditAddress() {
        def uid = registerUser()
        def aid = createAddress(uid)

        //edit address
        def con = TestUtil.getConnection("/customers/current/addresses/" + aid, 'POST')
        TestUtil.acceptXML(con)
        TestUtil.basicAuth(con, "${uid}", "${password}")
        con.doOutput = true
        con.outputStream << "town=Montreal&postcode=80335"
        assert con.responseCode == HttpURLConnection.HTTP_OK : TestUtil.messageResponseCode(con.responseCode, HttpURLConnection.HTTP_OK)

        //check address book
        con = TestUtil.getConnection("/customers/current/addresses", 'GET')
        TestUtil.acceptXML(con)
        TestUtil.basicAuth(con, "${uid}", "${password}")
        assert con.responseCode == HttpURLConnection.HTTP_OK : TestUtil.messageResponseCode(con.responseCode, HttpURLConnection.HTTP_OK)
        def response = new XmlSlurper().parseText(con.inputStream.text)
        assert response.address[0].line1 == "${line1}"
        assert response.address[0].town == "Montreal"
        assert response.address[0].postalCode == "80335"
        assert response.address[0].country.isocode == "${countryIsoCode}"
    }

    void testSetDefaultAddress() {
        def uid = registerUser()
        def aid = createAddress(uid)

        //add another address
        def con = TestUtil.getConnection("/customers/current/addresses", 'POST')
        TestUtil.acceptXML(con)
        TestUtil.basicAuth(con, "${uid}", "${password}")
        con.doOutput = true
        con.outputStream << "line1=Bayerstr. 10a&town=München&countryIsoCode=de&postcode=80335"
        assert con.responseCode == HttpURLConnection.HTTP_OK : TestUtil.messageResponseCode(con.responseCode, HttpURLConnection.HTTP_OK)
        def response = new XmlSlurper().parseText(con.inputStream.text)
        def newAddressId = response.id

        //check if first address is set as default
        con = TestUtil.getConnection("/customers/current", 'GET')
        TestUtil.acceptXML(con)
        TestUtil.basicAuth(con, "${uid}", "${password}")
        assert con.responseCode == HttpURLConnection.HTTP_OK : TestUtil.messageResponseCode(con.responseCode, HttpURLConnection.HTTP_OK)
        response = new XmlSlurper().parseText(con.inputStream.text)
        assert response.uid == "${uid}"
        assert response.defaultAddress.line1 == "${line1}"
        assert response.defaultAddress.town == "${town}"
        assert response.defaultAddress.country.isocode == "${countryIsoCode}"

        //set second address as default
        con = TestUtil.getConnection("/customers/current/addresses/default/" + newAddressId, 'PUT')
        TestUtil.acceptXML(con)
        TestUtil.basicAuth(con, "${uid}", "${password}")
        con.doOutput = true
        assert con.responseCode == HttpURLConnection.HTTP_OK : TestUtil.messageResponseCode(con.responseCode, HttpURLConnection.HTTP_OK)

        //check if second address is set as customer's default
        con = TestUtil.getConnection("/customers/current", 'GET')
        TestUtil.acceptXML(con)
        TestUtil.basicAuth(con, "${uid}", "${password}")
        assert con.responseCode == HttpURLConnection.HTTP_OK : TestUtil.messageResponseCode(con.responseCode, HttpURLConnection.HTTP_OK)
        response = new XmlSlurper().parseText(con.inputStream.text)
        assert response.uid == "${uid}"
        assert response.defaultAddress.id == "${newAddressId}"
        assert response.defaultAddress.line1 == "Bayerstr. 10a"
        assert response.defaultAddress.town == "${town}"
        assert response.defaultAddress.country.isocode == "${countryIsoCode}"
    }
}