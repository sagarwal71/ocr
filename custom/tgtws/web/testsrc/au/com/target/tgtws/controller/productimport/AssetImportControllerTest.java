/**
 * 
 */
package au.com.target.tgtws.controller.productimport;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtwsfacades.integration.dto.IntegrationAssetImportDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.productimport.AssetImportIntegrationFacade;



/**
 * @author rsamuel3
 * 
 */
public class AssetImportControllerTest {
    @Mock
    private AssetImportIntegrationFacade assetImportIntegrationFacade;

    @InjectMocks
    private final AssetImportController controller = new AssetImportController();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testimportProductAssetsCodesdifferent() {
        final IntegrationAssetImportDto dto = new IntegrationAssetImportDto();
        final String payloadAssetId = "1234";
        dto.setAssetId(payloadAssetId);
        final String assertId = "3456";
        final IntegrationResponseDto response = controller.importProductAssets(assertId, dto);
        Assert.assertNotNull(response);
        Assert.assertFalse(response.isSuccessStatus());
        final List<String> messages = response.getMessages();
        Assert.assertTrue(CollectionUtils.isNotEmpty(messages));
        Assert.assertEquals(1, messages.size());
        Assert.assertEquals(String.format(AssetImportController.ERROR_CODE_MISMATCH, assertId, payloadAssetId),
                messages.get(0));
    }

    @Test
    public void testimportProductAssetsCodesValid() {
        final IntegrationAssetImportDto dto = new IntegrationAssetImportDto();
        final String assetId = "1234";
        dto.setAssetId(assetId);
        final IntegrationResponseDto responsedto = new IntegrationResponseDto(assetId);
        responsedto.setSuccessStatus(true);
        responsedto.addMessage("OK");
        Mockito.when(assetImportIntegrationFacade.importTargetAssets(dto)).thenReturn(responsedto);
        final IntegrationResponseDto response = controller.importProductAssets(assetId, dto);
        Assert.assertNotNull(response);
        Assert.assertTrue(response.isSuccessStatus());
        final List<String> messages = response.getMessages();
        Assert.assertTrue(CollectionUtils.isNotEmpty(messages));
        Assert.assertEquals(1, messages.size());
        Assert.assertEquals(responsedto, response);
    }
}
