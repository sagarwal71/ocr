/**
 * 
 */
package au.com.target.tgtws.controller.fluent;

import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtwsfacades.fluent.ProductUpdateFacade;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentStockUpdateControllerTest {

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private ProductUpdateFacade productUpdateFacade;

    @InjectMocks
    private final FluentStockUpdateController fluentStockUpdateController = new FluentStockUpdateController();

    @Test
    public void testSetOnlineDate() {
        fluentStockUpdateController.setOnlineDate();
        verify(productUpdateFacade, never()).setOnlineDateForApplicableProducts();
    }

    @Test
    public void testSetOnlineDateWithFluent() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);

        fluentStockUpdateController.setOnlineDate();
        verify(productUpdateFacade).setOnlineDateForApplicableProducts();
    }
}
