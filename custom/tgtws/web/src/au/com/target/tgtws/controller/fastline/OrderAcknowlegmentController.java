/**
 * 
 */
package au.com.target.tgtws.controller.fastline;

import java.text.MessageFormat;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtutility.logging.JaxbLogger;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.OrderAcknowlegment;
import au.com.target.tgtwsfacades.orderAck.OrderAckIntegrationFacade;


/**
 * @author rsamuel3
 * 
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/orderAckapi")
public class OrderAcknowlegmentController {

    private static final Logger LOG = Logger.getLogger(OrderAcknowlegmentController.class);

    @Autowired
    private OrderAckIntegrationFacade orderAckIntegrationFacade;

    /**
     * Updates all the order numbers to Orders acknowledged
     * 
     * @param dto
     * @return List of IntegrationResponses to the ones that have failed
     */
    @Secured("ROLE_ESBGROUP")
    @RequestMapping(value = "/targetorderAck", method = RequestMethod.PUT)
    @ResponseBody
    public List<IntegrationResponseDto> updateStockLevel(@RequestBody final OrderAcknowlegment dto) {

        JaxbLogger.logXml(LOG, dto);

        LOG.info(MessageFormat.format("INFO-FASTLINE-ORDERACK Updating the acknowledgement for {0} number of orders",
                Integer.valueOf(dto.getOrders().getOrders().size())));

        return orderAckIntegrationFacade.updateOrderAcknowledgements(dto.getOrders());
    }
}
