/**
 * 
 */
package au.com.target.tgtws.controller.productimport;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtwsfacades.integration.dto.IntegrationAssetImportDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.productimport.AssetImportIntegrationFacade;



/**
 * @author rsamuel3
 * 
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/assetimportapi")
public class AssetImportController {
    /**
     * 
     */
    protected static final String ERROR_CODE_MISMATCH = "ERR-ASSETIMPORT-CODEMISMATCH - Requested code %s is different to the one supplied with data payload: %s";
    private static final Logger LOG = Logger.getLogger(AssetImportController.class);

    @Autowired
    private AssetImportIntegrationFacade assetImportIntegrationFacade;

    @Secured("ROLE_ESBGROUP")
    @RequestMapping(value = "/assetimport/{assertCode}", method = RequestMethod.PUT)
    @ResponseBody
    public IntegrationResponseDto importProductAssets(@PathVariable final String assertCode,
            @RequestBody final IntegrationAssetImportDto dto) {
        final String codeInDto = dto.getAssetId();
        if (!codeInDto.equalsIgnoreCase(assertCode)) {
            final IntegrationResponseDto response = new IntegrationResponseDto(
                    codeInDto);
            response.setSuccessStatus(false);
            final String message = String.format(ERROR_CODE_MISMATCH, assertCode, codeInDto);
            LOG.error(message);
            response.addMessage(message);
            return response;
        }

        LOG.info("Persisting details of Asset with assetId of : " + assertCode);
        return assetImportIntegrationFacade.importTargetAssets(dto);
    }
}
