/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtws.controller;

//CHECKSTYLE:OFF
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoDatas;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import au.com.target.tgtws.conv.HttpRequestPaymentInfoPopulator;
import au.com.target.tgtws.dto.AddressDataList;
import au.com.target.tgtws.exceptions.InconsistentUseridException;
import au.com.target.tgtws.populator.HttpRequestAddressDataPopulator;
import au.com.target.tgtws.populator.HttpRequestCustomerDataPopulator;
import au.com.target.tgtws.populator.options.PaymentInfoOption;


//CHEKCSTYLE:ON

/**
 * Main Controller for CustomerFacade WebServices
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/customers")
public class CustomersController extends BaseController
{
    private static final Logger LOG = Logger.getLogger(CustomersController.class);

    @Autowired
    private CustomerFacade customerFacade;

    @Autowired
    private UserFacade userFacade;

    @Autowired
    private HttpRequestCustomerDataPopulator httpRequestCustomerDataPopulator;

    @Autowired
    private Populator<HttpServletRequest, AddressData> httpRequestAddressDataPopulator;

    /**
     * Client should pass customer's data as POST Body. Content-Type needs to be set to
     * application/x-www-form-urlencoded; charset=UTF-8 and sample body (urlencoded) is: old=1234&new=1111
     * 
     * @param login
     *            - login to be created
     * @param password
     *            - customer password
     * @param firstName
     *            - customer first name
     * @param lastName
     *            - customer last name
     * @param titleCode
     *            - customer's title
     * @throws DuplicateUidException
     *             in case the requested login already exists
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.CREATED, reason = "Customer created.")
    public void registerUser(@RequestParam final String login, @RequestParam final String password,
            @RequestParam(required = false) final String titleCode, @RequestParam final String firstName,
            @RequestParam final String lastName) throws DuplicateUidException
    {
        if (LOG.isDebugEnabled())
        {
            LOG.debug("registerUser: login=" + login);
        }
        final RegisterData registration = new RegisterData();
        registration.setFirstName(firstName);
        registration.setLastName(lastName);
        registration.setLogin(login);
        registration.setPassword(password);
        registration.setTitleCode(titleCode);
        customerFacade.register(registration);
    }

    /**
     * Update customer's default address
     * 
     * @param id
     *            - Address id to be set as default address
     * @throws DuplicateUidException
     */
    @Secured("ROLE_CUSTOMERGROUP")
    @RequestMapping(value = "/current/addresses/default/{id}", method = RequestMethod.PUT)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK, reason = "Default address updated.")
    public void updateDefaultAddress(@PathVariable final String id) throws DuplicateUidException
    {
        if (LOG.isDebugEnabled())
        {
            LOG.debug("updateDefaultAddress: id=" + id);
        }
        final AddressData address = userFacade.getAddressForCode(id);
        userFacade.setDefaultAddress(address);
    }

    /**
     * Update customer's profile
     * 
     * @param request
     *            - http request
     * @return updated profile
     * @throws DuplicateUidException
     */
    @Secured("ROLE_CUSTOMERGROUP")
    @RequestMapping(value = "/current/profile", method = RequestMethod.POST)
    @ResponseBody
    public CustomerData updateProfile(final HttpServletRequest request) throws DuplicateUidException
    {
        final CustomerData customer = customerFacade.getCurrentCustomer();
        if (LOG.isDebugEnabled())
        {
            LOG.debug("updateCustomer: userId=" + customer.getUid());
        }
        httpRequestCustomerDataPopulator.populate(request, customer);
        customerFacade.updateFullProfile(customer);
        return customer;
    }

    /**
     * Get all customer's addresses
     * 
     * @return List of customer addresses
     */
    @Secured("ROLE_CUSTOMERGROUP")
    @RequestMapping(value = "/current/addresses", method = RequestMethod.GET)
    @ResponseBody
    public AddressDataList getAddresses()
    {
        return new AddressDataList(userFacade.getAddressBook());
    }

    /**
     * Create new address for current customer
     * 
     * @param request
     * @return address created
     * @throws InconsistentUseridException
     * @throws DuplicateUidException
     */
    @Secured("ROLE_CUSTOMERGROUP")
    @RequestMapping(value = "/current/addresses", method = RequestMethod.POST)
    @ResponseBody
    public AddressData createAddress(final HttpServletRequest request) throws InconsistentUseridException,
            DuplicateUidException
    {
        final AddressData address = new AddressData();
        httpRequestAddressDataPopulator.populate(request, address);
        address.setShippingAddress(true);
        userFacade.addAddress(address);
        return address;
    }

    /**
     * Edit address from current customer
     * 
     * @param id
     *            - id of address to be edited
     * @return modified address
     */
    @Secured("ROLE_CUSTOMERGROUP")
    @RequestMapping(value = "/current/addresses/{id}", method = RequestMethod.POST)
    @ResponseBody
    public AddressData editAddress(@PathVariable final String id, final HttpServletRequest request)
    {
        if (LOG.isDebugEnabled())
        {
            LOG.debug("editAddress: id=" + id);
        }
        final AddressData address = userFacade.getAddressForCode(id);
        httpRequestAddressDataPopulator.populate(request, address);

        if (address.getId().equals(userFacade.getDefaultAddress().getId()))
        {
            address.setDefaultAddress(true);
        }
        userFacade.editAddress(address);
        return address;
    }

    /**
     * Remove address from current customer
     * 
     * @param id
     *            - id of address to be removed
     */
    @Secured("ROLE_CUSTOMERGROUP")
    @RequestMapping(value = "/current/addresses/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteAddress(@PathVariable final String id)
    {
        if (LOG.isDebugEnabled())
        {
            LOG.debug("deleteAddress: id=" + id);
        }
        final AddressData address = userFacade.getAddressForCode(id);
        userFacade.removeAddress(address);
    }

    /**
     * Get customer data
     * 
     * @return CustomerData object containing customer information
     * @throws InconsistentUseridException
     */
    @Secured("ROLE_CUSTOMERGROUP")
    @RequestMapping(value = "/current", method = RequestMethod.GET)
    @ResponseBody
    public CustomerData getCurrentCustomer() throws InconsistentUseridException
    {
        return customerFacade.getCurrentCustomer();
    }

    /**
     * Client should pass old and new password as POST Body. Content-Type needs to be set to
     * application/x-www-form-urlencoded; charset=UTF-8 and sample body (urlencoded) is: old=1234&new=1111
     * 
     * @param old
     *            - old password
     * @param newPassword
     *            - new password
     */
    @Secured("ROLE_CUSTOMERGROUP")
    @RequestMapping(value = "/current/password", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.ACCEPTED, reason = "Password changed.")
    public void changePassword(@RequestParam final String old, @RequestParam(value = "new") final String newPassword)
            throws InconsistentUseridException
    {
        customerFacade.changePassword(old, newPassword);
    }

    /**
     * Web service for getting current user's credit card payment infos.<br>
     * Sample call: http://localhost:9001/rest/v1/mysite/customers/paymentinfos?saved=true <br>
     * Method requires authentication and is restricted <code>HTTPS<code> channel.<br> 
     * Method type : <code>GET</code>.<br>
     * 
     * @param saved
     *            - <code>true</code> to retrieve only saved payment infos. <code>false</code> by default
     * 
     * @return List of {@link CCPaymentInfoData} as response body
     */
    @Secured("ROLE_CUSTOMERGROUP")
    @RequestMapping(value = "/current/paymentinfos", method = RequestMethod.GET)
    @ResponseBody
    public CCPaymentInfoDatas getPaymentInfos(
            @RequestParam(required = false, defaultValue = "false") final boolean saved)
    {
        if (LOG.isDebugEnabled())
        {
            LOG.debug("getPaymentInfos");
        }

        final CCPaymentInfoDatas ccPaymentInfoDatas = new CCPaymentInfoDatas();
        ccPaymentInfoDatas.setPaymentInfos(userFacade.getCCPaymentInfos(saved));
        return ccPaymentInfoDatas;
    }

    /**
     * Web service for getting current user's credit card payment info by id.<br>
     * Sample call: https://localhost:9002/rest/v1/mysite/customers/paymentinfos/123 <br>
     * Method requires authentication and is restricted <code>HTTPS<code> channel.<br> 
     * Method type : <code>GET</code>.<br>
     * 
     * 
     * @return {@link CCPaymentInfoData} as response body
     */
    @Secured("ROLE_CUSTOMERGROUP")
    @RequestMapping(value = "/current/paymentinfos/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CCPaymentInfoData getPaymentInfo(@PathVariable final String id)
    {
        if (LOG.isDebugEnabled())
        {
            LOG.debug("getPaymentInfo : id = " + id);
        }
        return userFacade.getCCPaymentInfoForCode(id);
    }

    /**
     * Web service for deleting current user's credit card payment info by id.<br>
     * Sample call: http://localhost:9001/rest/v1/mysite/customers/paymentinfos/123<br>
     * Method requires authentication and is restricted <code>HTTPS<code> channel.<br> 
     * Method type : <code>DELETE</code>.<br>
     * 
     */
    @Secured("ROLE_CUSTOMERGROUP")
    @RequestMapping(value = "/current/paymentinfos/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deletePaymentInfo(@PathVariable final String id)
    {
        if (LOG.isDebugEnabled())
        {
            LOG.debug("deletePaymentInfo: id = " + id);
        }
        userFacade.removeCCPaymentInfo(id);
    }

    /**
     * Web service for modifying billing address data for the specific payment info.<br>
     * Sample call: https://localhost:9002/rest/v1/mysite/paymentinfos/123/address<br>
     * Method requires authentication and is restricted <code>HTTPS<code> channel.<br> 
     * Method type : <code>POST</code>. Address data need to be sent as post body.<br>
     * Method uses {@link HttpRequestAddressDataPopulator} to populate address data from request parameters.
     * 
     * @param paymentInfoId
     * @param request
     * 
     */
    @RequestMapping(value = "/current/paymentinfos/{paymentInfoId}/address", method = RequestMethod.POST)
    @ResponseBody
    @Secured("ROLE_CUSTOMERGROUP")
    public void updatePaymentInfoAddress(@PathVariable final String paymentInfoId, final HttpServletRequest request)
    {
        if (LOG.isDebugEnabled())
        {
            LOG.debug("updatePaymentInfoAddress: id = " + paymentInfoId);
        }
        final CCPaymentInfoData paymentInfoData = getPaymentInfo(paymentInfoId);

        if (paymentInfoData != null)
        {
            final AddressData billingAddressData = paymentInfoData.getBillingAddress();
            httpRequestAddressDataPopulator.populate(request, billingAddressData);
            paymentInfoData.setBillingAddress(billingAddressData);
            userFacade.updateCCPaymentInfo(paymentInfoData);
        }
        else
        {
            throw new UnknownIdentifierException("Payment info [" + paymentInfoId + "] not found.");
        }
    }

    /**
     * Web service for modifying existing payment info.<br>
     * Sample call: https://localhost:9002/rest/v1/mysite/paymentinfos/123<br>
     * Method requires authentication and is restricted <code>HTTPS<code> channel.<br> 
     * Method type : <code>POST</code>. PaymentInfo data need to be sent as post body.<br>
     * Method uses {@link HttpRequestPaymentInfoPopulator} to populate payment info data from request parameters.
     * 
     * @param paymentInfoId
     * @param request
     * 
     */
    @RequestMapping(value = "/current/paymentinfos/{paymentInfoId}", method = RequestMethod.POST)
    @ResponseBody
    @Secured("ROLE_CUSTOMERGROUP")
    public void updatePaymentInfo(@PathVariable final String paymentInfoId, final HttpServletRequest request)
    {
        if (LOG.isDebugEnabled())
        {
            LOG.debug("updatePaymentInfo: id = " + paymentInfoId);
        }
        final CCPaymentInfoData paymentInfoData = getPaymentInfo(paymentInfoId);

        if (paymentInfoData != null)
        {
            getCCPaymentInfoPopulator().populate(request, paymentInfoData,
                    Collections.singletonList(PaymentInfoOption.BASIC));
            userFacade.updateCCPaymentInfo(paymentInfoData);
        }
        else
        {
            throw new UnknownIdentifierException("Payment info [" + paymentInfoId + "] not found.");
        }
    }

    public void setCustomerFacade(final CustomerFacade customerFacade)
    {
        this.customerFacade = customerFacade;
    }

    public void setUserFacade(final UserFacade userFacade)
    {
        this.userFacade = userFacade;
    }

}
