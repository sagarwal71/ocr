/**
 * 
 */
package au.com.target.tgtws.jackson;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;


/**
 * Setup config
 * 
 * @author rsamuel3
 * 
 */
public class JsonMapper extends ObjectMapper {

    /**
     * 
     */
    public JsonMapper() {
        super();
        this.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
        this.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        this.configure(SerializationFeature.WRAP_ROOT_VALUE, true);
    }

}
