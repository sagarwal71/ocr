/**
 * 
 */
package au.com.target.tgtverifyaddr.provider.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.ws.Holder;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.qas.ondemand_2011_03.Address;
import com.qas.ondemand_2011_03.AddressLineType;
import com.qas.ondemand_2011_03.EngineEnumType;
import com.qas.ondemand_2011_03.PicklistEntryType;
import com.qas.ondemand_2011_03.QAAddressType;
import com.qas.ondemand_2011_03.QAGetAddress;
import com.qas.ondemand_2011_03.QAPicklistType;
import com.qas.ondemand_2011_03.QAPortType;
import com.qas.ondemand_2011_03.QAQueryHeader;
import com.qas.ondemand_2011_03.QASearch;
import com.qas.ondemand_2011_03.QASearchResult;

import au.com.target.tgtverifyaddr.constants.TgtverifyaddrConstants;
import au.com.target.tgtverifyaddr.data.AddressData;
import au.com.target.tgtverifyaddr.data.Config;
import au.com.target.tgtverifyaddr.data.CountryEnum;
import au.com.target.tgtverifyaddr.data.FormattedAddressData;
import au.com.target.tgtverifyaddr.exception.InitializationFailedException;
import au.com.target.tgtverifyaddr.exception.RequestTimeOutException;
import au.com.target.tgtverifyaddr.exception.ServiceNotAvailableException;
import au.com.target.tgtverifyaddr.exception.TooManyMatchesFoundException;


/**
 * @author vmuthura
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class QASAddressVerificationClientTest {

    @InjectMocks
    private QASAddressVerificationClient qasAddressVerificationClient = new QASAddressVerificationClient();

    @Mock
    private Config config;

    @Mock
    private QAPortType qaPortType;

    @Mock
    private QAPicklistType qaPickList;

    /**
     * @throws InitializationFailedException
     * 
     */
    @Before
    public void init() throws InitializationFailedException {
        when(config.getWsdlUrl())
                .thenReturn("https//test.url");
        when(config.getqName()).thenReturn("http://www.qas.com/OnDemand-2011-03");
        when(config.getServiceName()).thenReturn("QASOnDemandIntermediary");
        when(config.getUserName()).thenReturn("ws_120_ext");
        when(config.getPassword()).thenReturn("qaspro2012");
        when(Boolean.valueOf(config.isFlatten())).thenReturn(Boolean.TRUE);
        when(config.getEngineType()).thenReturn("Singleline");
        when(config.getIntensity()).thenReturn("Close");
        when(config.getLayout()).thenReturn("QADefault");
        when(config.getFormattedLayout()).thenReturn("TargetAustralia");
        when(config.getLocalisation()).thenReturn("");
        when(config.getPromptset()).thenReturn("Default");
        when(Integer.valueOf(config.getThreshold())).thenReturn(Integer.valueOf(50));
        when(Integer.valueOf(config.getTimeout())).thenReturn(Integer.valueOf(500));
        when(config.getRequestTag()).thenReturn(null);
        when(Boolean.valueOf(config.isFormattedAddressInPicklist())).thenReturn(Boolean.TRUE);
        qasAddressVerificationClient = spy(qasAddressVerificationClient);

    }

    @Test
    public void testInit() throws InitializationFailedException {
        doReturn(qaPortType).when(qasAddressVerificationClient).initQAPortType();
        qasAddressVerificationClient.init();

        assertThat(qasAddressVerificationClient.getIntuitiveEngineType()).isNotNull();
        assertThat(qasAddressVerificationClient.getSinglelineEngineType()).isNotNull();
        assertThat(qasAddressVerificationClient.getQaPortType()).isNotNull();
        assertThat(qasAddressVerificationClient.getQaQueryHeader()).isNotNull();

        assertThat(qasAddressVerificationClient.getIntuitiveEngineType().getValue())
                .isEqualTo(EngineEnumType.INTUITIVE);
        assertThat(qasAddressVerificationClient.getSinglelineEngineType().getValue())
                .isEqualTo(EngineEnumType.SINGLELINE);
    }

    @Test(expected = InitializationFailedException.class)
    public void testInitWithIOException() throws InitializationFailedException {
        qasAddressVerificationClient.init();
    }

    @SuppressWarnings("boxing")
    @Test(expected = RequestTimeOutException.class)
    public void testSearchWithTimeout() throws InitializationFailedException, ServiceNotAvailableException,
            RequestTimeOutException, TooManyMatchesFoundException {
        final QASearchResult qaSearchResult = mock(QASearchResult.class);
        when(qaSearchResult.getQAPicklist()).thenReturn(qaPickList);
        when(qaPickList.isTimeout()).thenReturn(true);
        when(qaPortType.doSearch(any(QASearch.class), any(QAQueryHeader.class), any(Holder.class))).thenReturn(
                qaSearchResult);

        qasAddressVerificationClient.search("address", CountryEnum.AUSTRALIA.getCode());
    }

    @SuppressWarnings("boxing")
    @Test
    public void itShouldIgnoreResultWithEmptyMoniker() throws InitializationFailedException,
            ServiceNotAvailableException,
            RequestTimeOutException, TooManyMatchesFoundException {
        final QASearchResult qaSearchResult = mock(QASearchResult.class);
        when(qaSearchResult.getQAPicklist()).thenReturn(qaPickList);
        when(qaPickList.isTimeout()).thenReturn(false);
        final List<PicklistEntryType> entryTypeList = new ArrayList<>();
        final PicklistEntryType picklistEntryType1 = new PicklistEntryType();
        final PicklistEntryType picklistEntryType2 = new PicklistEntryType();
        picklistEntryType2.setMoniker("moniker1");
        picklistEntryType2.setPartialAddress("address");
        picklistEntryType2.setScore(BigInteger.valueOf(50l));
        entryTypeList.add(picklistEntryType1);
        entryTypeList.add(picklistEntryType2);
        when(qaPickList.getPicklistEntry()).thenReturn(entryTypeList);
        when(qaPortType.doSearch(any(QASearch.class), any(QAQueryHeader.class), any(Holder.class))).thenReturn(
                qaSearchResult);

        final List<AddressData> resultList = qasAddressVerificationClient.search("address",
                CountryEnum.AUSTRALIA.getCode());

        assertThat(entryTypeList).hasSize(2);
        assertThat(resultList).hasSize(1);
        assertThat(resultList.get(0).getMoniker()).isEqualTo("moniker1");

    }

    @Test(expected = ServiceNotAvailableException.class)
    public void itShouldInitWhenSearch() throws InitializationFailedException, ServiceNotAvailableException,
            RequestTimeOutException, TooManyMatchesFoundException {
        qasAddressVerificationClient.search("address", CountryEnum.AUSTRALIA.getCode());
    }

    /**
     * Test for Australia license
     * 
     * @throws ServiceNotAvailableException
     *             -
     * @throws InitializationFailedException
     *             -
     */
    @Ignore
    public void testIsSearchAvailableForLicensedCountry() throws ServiceNotAvailableException,
            InitializationFailedException {
        qasAddressVerificationClient.init();
        assertThat(qasAddressVerificationClient.isSearchAvailable(CountryEnum.AUSTRALIA.getCode())).isTrue();
    }

    /**
     * Test for unlicensed country
     * 
     * @throws ServiceNotAvailableException
     *             -
     * @throws InitializationFailedException
     *             -
     */
    @Ignore
    public void testIsSearchAvailableForNonLicensedCountry() throws ServiceNotAvailableException,
            InitializationFailedException {
        qasAddressVerificationClient.init();
        assertThat(qasAddressVerificationClient.isSearchAvailable("GER")).isFalse();
    }

    /**
     * Test method for {@link QASAddressVerificationClient#search(String, String)} .
     * 
     * @throws TooManyMatchesFoundException
     *             -
     * @throws RequestTimeOutException
     *             -
     * @throws ServiceNotAvailableException
     *             -
     * @throws InitializationFailedException
     *             -
     */
    @Test
    public void defaultSearchShouldUseSinglelineMode() throws ServiceNotAvailableException, RequestTimeOutException,
            TooManyMatchesFoundException, InitializationFailedException {
        doReturn(Collections.emptyList()).when(qasAddressVerificationClient).search("352 Plantation Road",
                CountryEnum.AUSTRALIA.getCode(), "Singleline");

        final List<AddressData> match = qasAddressVerificationClient.search("352 Plantation Road",
                CountryEnum.AUSTRALIA.getCode());

        assertThat(match).isEmpty();
    }

    @SuppressWarnings("boxing")
    @Test
    public void searchWithNullType() throws ServiceNotAvailableException,
            RequestTimeOutException,
            TooManyMatchesFoundException, InitializationFailedException {
        final QASearchResult qaSearchResult = mock(QASearchResult.class);
        when(qaSearchResult.getQAPicklist()).thenReturn(qaPickList);
        when(qaPickList.isTimeout()).thenReturn(false);
        when(qaPortType.doSearch(any(QASearch.class), any(QAQueryHeader.class), any(Holder.class))).thenReturn(
                qaSearchResult);

        final List<AddressData> match = qasAddressVerificationClient.search("352 Plantation Road",
                CountryEnum.AUSTRALIA.getCode(), null);

        verify(qaSearchResult).getQAPicklist();
        assertThat(match).isNotNull();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testSearchWithPassedInMode() throws ServiceNotAvailableException, RequestTimeOutException,
            TooManyMatchesFoundException, InitializationFailedException {
        final QASearchResult qaSearchResult = mock(QASearchResult.class);
        when(qaSearchResult.getQAPicklist()).thenReturn(qaPickList);
        when(qaPickList.isTimeout()).thenReturn(false);
        when(qaPortType.doSearch(any(QASearch.class), any(QAQueryHeader.class), any(Holder.class))).thenReturn(
                qaSearchResult);
        final List<AddressData> match = qasAddressVerificationClient.search("352 Plantation Road",
                CountryEnum.AUSTRALIA.getCode(), "Intuitive");

        verify(qaSearchResult).getQAPicklist();
        assertThat(match).isNotNull();
    }



    /**
     * @throws ServiceNotAvailableException
     *             - on null response
     * @throws RequestTimeOutException
     *             - on connection time out
     * @throws TooManyMatchesFoundException
     *             - on matches gt threshold
     * @throws InitializationFailedException
     *             - on init failure
     */
    @Test
    public void testFormatAddress() throws ServiceNotAvailableException, RequestTimeOutException,
            TooManyMatchesFoundException, InitializationFailedException {
        doReturn(qaPortType).when(qasAddressVerificationClient).initQAPortType();
        final AddressData addressData = new AddressData("moniker", null, null);
        final Address qaAddress = mock(Address.class);
        final QAAddressType addressType = mock(QAAddressType.class);
        final List<AddressLineType> list = new ArrayList<>();
        final AddressLineType addressLineType = new AddressLineType();
        addressLineType.setLabel(TgtverifyaddrConstants.ADDRESS_LINE_1);
        addressLineType.setLine("line1");
        list.add(addressLineType);
        when(qaAddress.getQAAddress()).thenReturn(addressType);
        when(addressType.getAddressLine()).thenReturn(list);
        when(qaPortType.doGetAddress(any(QAGetAddress.class), any(QAQueryHeader.class), any(Holder.class))).thenReturn(
                qaAddress);

        qasAddressVerificationClient.init();
        final FormattedAddressData formattedAddress = qasAddressVerificationClient.formatAddress(addressData);

        verify(addressType).getAddressLine();
        assertThat(formattedAddress.getAddressLine1()).isEqualTo("line1");

    }

    @Test
    public void testFormatAddressWithNoAddressLine1() throws ServiceNotAvailableException, RequestTimeOutException,
            TooManyMatchesFoundException, InitializationFailedException {
        doReturn(qaPortType).when(qasAddressVerificationClient).initQAPortType();
        final AddressData addressData = new AddressData("moniker", null, null);
        final Address qaAddress = mock(Address.class);
        final QAAddressType addressType = mock(QAAddressType.class);
        final List<AddressLineType> list = new ArrayList<>();
        final AddressLineType addressLineType = new AddressLineType();
        addressLineType.setLabel(TgtverifyaddrConstants.ADDRESS_LINE_2);
        addressLineType.setLine("line2");
        list.add(addressLineType);
        when(qaAddress.getQAAddress()).thenReturn(addressType);
        when(addressType.getAddressLine()).thenReturn(list);
        when(qaPortType.doGetAddress(any(QAGetAddress.class), any(QAQueryHeader.class), any(Holder.class))).thenReturn(
                qaAddress);

        qasAddressVerificationClient.init();
        final FormattedAddressData formattedAddress = qasAddressVerificationClient.formatAddress(addressData);

        verify(addressType).getAddressLine();
        assertThat(formattedAddress.getAddressLine1()).isEqualTo("line2");
    }

    @Test
    public void testFormatAddressWithNoAddressLine1Or2() throws ServiceNotAvailableException, RequestTimeOutException,
            TooManyMatchesFoundException, InitializationFailedException {
        doReturn(qaPortType).when(qasAddressVerificationClient).initQAPortType();
        final AddressData addressData = new AddressData("moniker", null, null);
        final Address qaAddress = mock(Address.class);
        final QAAddressType addressType = mock(QAAddressType.class);
        final List<AddressLineType> list = new ArrayList<>();
        when(qaAddress.getQAAddress()).thenReturn(addressType);
        when(addressType.getAddressLine()).thenReturn(list);
        when(qaPortType.doGetAddress(any(QAGetAddress.class), any(QAQueryHeader.class), any(Holder.class))).thenReturn(
                qaAddress);

        qasAddressVerificationClient.init();
        final FormattedAddressData formattedAddress = qasAddressVerificationClient.formatAddress(addressData);

        verify(addressType).getAddressLine();
        assertThat(formattedAddress.getAddressLine1()).isNull();
        assertThat(formattedAddress.getAddressLine2()).isNull();
    }
}
