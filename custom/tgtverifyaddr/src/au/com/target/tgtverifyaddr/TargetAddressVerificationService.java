/**
 * 
 */
package au.com.target.tgtverifyaddr;

import java.util.List;

import au.com.target.tgtverifyaddr.data.AddressData;
import au.com.target.tgtverifyaddr.data.CountryEnum;
import au.com.target.tgtverifyaddr.data.FormattedAddressData;
import au.com.target.tgtverifyaddr.exception.NoMatchFoundException;
import au.com.target.tgtverifyaddr.exception.RequestTimeOutException;
import au.com.target.tgtverifyaddr.exception.ServiceNotAvailableException;
import au.com.target.tgtverifyaddr.exception.TooManyMatchesFoundException;


/**
 * @author vmuthura
 * 
 */
public interface TargetAddressVerificationService {

    /**
     * Verify a given address is valid and exists. Returns the list of suggestions or the formatted address, if it's a
     * perfect match
     * 
     * @param address
     *            - Address to be verified
     * @param country
     *            - CountryEnum, where the address search has to constrained to
     * @return List of suggestions or the formatted address
     * @throws ServiceNotAvailableException
     *             if the service is not available
     * @throws NoMatchFoundException
     *             if no matches found
     * @throws RequestTimeOutException
     *             if the service does not respond with in the timeout configured
     * @throws TooManyMatchesFoundException
     *             if the results exceed the threshold configured
     */
    List<AddressData> verifyAddress(String address, CountryEnum country) throws ServiceNotAvailableException,
            RequestTimeOutException, NoMatchFoundException, TooManyMatchesFoundException;

    /**
     * Returns the list of suggestions or the formatted address, if it's a perfect match
     * 
     * @param address
     *            - Address to be verified
     * @param country
     *            - CountryEnum, where the address search has to constrained to
     * @return List of suggestions or the formatted address
     * @throws ServiceNotAvailableException
     *             if the service is not available
     * @throws NoMatchFoundException
     *             if no matches found
     * @throws RequestTimeOutException
     *             if the service does not respond with in the timeout configured
     * @throws TooManyMatchesFoundException
     *             if the results exceed the threshold configured
     */
    List<AddressData> searchAddress(String address, CountryEnum country) throws ServiceNotAvailableException,
            RequestTimeOutException, NoMatchFoundException, TooManyMatchesFoundException;

    /**
     * @param unformattedAddress
     *            - The unformatted address
     * @return formatted address as String
     * @throws ServiceNotAvailableException
     *             - on null response from QAS
     */
    FormattedAddressData formatAddress(AddressData unformattedAddress) throws ServiceNotAvailableException;

    /**
     * 
     * @param address
     * @return true if address is verified, false otherwise
     * @throws ServiceNotAvailableException
     */
    boolean isAddressVerified(String address) throws ServiceNotAvailableException;
}
