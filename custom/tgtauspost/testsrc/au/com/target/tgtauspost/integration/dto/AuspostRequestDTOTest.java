/**
 * 
 */
package au.com.target.tgtauspost.integration.dto;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.target.tgtfulfilment.dto.ConsignmentParcelDTO;


/**
 * Tests for AuspostRequestDTO.
 * 
 * @author jjayawa1
 *
 */
@UnitTest
public class AuspostRequestDTOTest {

    private final AuspostRequestDTO manifestRequest = new AuspostRequestDTO();
    private final StoreDTO storeDTO = new StoreDTO();
    private final ConsignmentDetailsDTO consignmentDetailsDTO1 = new ConsignmentDetailsDTO();

    /**
     * Sets up common data for request DTOs.
     */
    @Before
    public void setupCommonDTOValues() {
        final ParcelDetailsDTO parcelDetailsDTO1 = new ParcelDetailsDTO();
        final ParcelDetailsDTO parcelDetailsDTO2 = new ParcelDetailsDTO();

        final ConsignmentParcelDTO consignmentParcelDTO1 = new ConsignmentParcelDTO();
        final ConsignmentParcelDTO consignmentParcelDTO2 = new ConsignmentParcelDTO();

        consignmentParcelDTO1.setHeight("1.0");
        consignmentParcelDTO1.setLength("1.0");
        consignmentParcelDTO1.setWidth("1.0");
        consignmentParcelDTO1.setWeight("1.0");

        consignmentParcelDTO2.setHeight("2.0");
        consignmentParcelDTO2.setLength("2.0");
        consignmentParcelDTO2.setWidth("2.0");
        consignmentParcelDTO2.setWeight("2.0");

        parcelDetailsDTO1.setParcelDetails(consignmentParcelDTO1);
        parcelDetailsDTO2.setParcelDetails(consignmentParcelDTO2);

        final List<ParcelDetailsDTO> parcels1 = new ArrayList<>();
        parcels1.add(parcelDetailsDTO1);
        parcels1.add(parcelDetailsDTO2);

        consignmentDetailsDTO1.setTrackingId("JDQ1234561");
        consignmentDetailsDTO1.setDeliveryAddress(getDeliveryAddressData());
        consignmentDetailsDTO1.setParcels(parcels1);

        storeDTO.setName("TargetGeelong");
        storeDTO.setChargeToAccount("6616510");
        storeDTO.setMerchantLocationId("JDQ");
        storeDTO.setChargeCode("S1");
        storeDTO.setProductCode("60");
        storeDTO.setServiceCode("02");
        storeDTO.setAddress(getAddressData());
    }

    /**
     * Setup AuspostRequestDTO for AusPost transmit manifest.
     */
    public void setupManifestRequestDataForTransmitManifest() {
        setupCommonDTOValues();
        final TargetManifestDTO manifestDTO = new TargetManifestDTO();

        final ConsignmentDTO consignmentDTO1 = new ConsignmentDTO();
        final ConsignmentDTO consignmentDTO2 = new ConsignmentDTO();

        final ConsignmentDetailsDTO consignmentDetailsDTO2 = new ConsignmentDetailsDTO();

        final ParcelDetailsDTO parcelDetailsDTO3 = new ParcelDetailsDTO();
        final ParcelDetailsDTO parcelDetailsDTO4 = new ParcelDetailsDTO();

        final ConsignmentParcelDTO consignmentParcelDTO3 = new ConsignmentParcelDTO();
        final ConsignmentParcelDTO consignmentParcelDTO4 = new ConsignmentParcelDTO();

        consignmentParcelDTO3.setHeight("3.0");
        consignmentParcelDTO3.setLength("3.0");
        consignmentParcelDTO3.setWidth("3.0");
        consignmentParcelDTO3.setWeight("3.0");

        consignmentParcelDTO4.setHeight("4.0");
        consignmentParcelDTO4.setLength("4.0");
        consignmentParcelDTO4.setWidth("4.0");
        consignmentParcelDTO4.setWeight("4.0");

        parcelDetailsDTO3.setParcelDetails(consignmentParcelDTO3);
        parcelDetailsDTO4.setParcelDetails(consignmentParcelDTO4);

        final List<ParcelDetailsDTO> parcels2 = new ArrayList<>();
        parcels2.add(parcelDetailsDTO3);
        parcels2.add(parcelDetailsDTO4);

        consignmentDetailsDTO2.setTrackingId("JDQ1234561");
        consignmentDetailsDTO2.setDeliveryAddress(getDeliveryAddressData());
        consignmentDetailsDTO2.setParcels(parcels2);

        consignmentDTO1.setConsignment(consignmentDetailsDTO1);
        consignmentDTO2.setConsignment(consignmentDetailsDTO2);

        final List<ConsignmentDTO> consignments = new ArrayList<>();
        consignments.add(consignmentDTO1);
        consignments.add(consignmentDTO2);

        manifestDTO.setTransactionDateTime("20150601");
        manifestDTO.setManifestNumber("5126013001");
        manifestDTO.setConsignments(consignments);

        storeDTO.setUsername("eParcelDemoSoap");
        storeDTO.setManifest(manifestDTO);
        manifestRequest.setStore(storeDTO);
    }

    public void setupManifestRequestDataForPDFLabels() {
        setupCommonDTOValues();
        final LabelDTO label = new LabelDTO();
        label.setLayout("A4-1pp");
        label.setBranding("false");
        storeDTO.setLabel(label);
        storeDTO.setConsignment(consignmentDetailsDTO1);
        manifestRequest.setStore(storeDTO);
    }

    /**
     * Tests that the serialized AuspostRequestDTO is as expected for PDF labels.
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    @Test
    public void testManifestPDFLabelRequest() throws JsonGenerationException, JsonMappingException, IOException {
        setupManifestRequestDataForPDFLabels();
        final ObjectMapper mapper = new ObjectMapper();
        final String responseJSON = mapper.writeValueAsString(manifestRequest);
        final String expectedJSON = "{\"store\":{\"name\":\"TargetGeelong\",\"chargeToAccount\":"
                + "\"6616510\",\"merchantLocationId\":\"JDQ\",\"chargeCode\":\"S1\",\"productCode\":"
                + "\"60\",\"serviceCode\":\"02\",\"address\":{\"addressLine1\":\"1 Main Street\","
                + "\"addressLine2\":\"add line2\",\"suburb\":\"Melbourne\",\"state\":\"VIC\",\"postcode\":"
                + "\"3000\",\"country\":\"Australia\"},\"label\":{\"layout\":\"A4-1pp\",\"branding\":"
                + "\"false\"},\"consignment\":{\"trackingId\":\"JDQ1234561\",\"deliveryAddress\":"
                + "{\"addressLine1\":\"2 Station Road\",\"addressLine2\":\"add line2\",\"suburb\":"
                + "\"Sydney\",\"state\":\"NSW\",\"postcode\":\"2000\",\"country\":\"Australia\","
                + "\"name\":\"Camberwell\",\"companyName\":\"Target\"},\"parcels\":[{\"parcelDetails\":"
                + "{\"height\":\"1.0\",\"length\":\"1.0\",\"width\":\"1.0\",\"weight\":\"1.0\"}},{"
                + "\"parcelDetails\":{\"height\":\"2.0\",\"length\":\"2.0\",\"width\":\"2.0\",\"weight\":"
                + "\"2.0\"}}]}}}";
        Assert.assertEquals(expectedJSON, responseJSON);
    }

    /**
     * Tests that the serialized AuspostRequestDTO is as expected to transmit manifests.
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    @Test
    public void testManifestRequestForTransmitManifest() throws JsonGenerationException, JsonMappingException,
            IOException {
        setupManifestRequestDataForTransmitManifest();
        final ObjectMapper mapper = new ObjectMapper();
        final String responseJSON = mapper.writeValueAsString(manifestRequest);
        final String expectedJSON = "{\"store\":{\"name\":\"TargetGeelong\",\"chargeToAccount\":"
                + "\"6616510\",\"merchantLocationId\":\"JDQ\",\"chargeCode\":\"S1\",\"productCode\":"
                + "\"60\",\"serviceCode\":\"02\",\"username\":\"eParcelDemoSoap\",\"address\":{"
                + "\"addressLine1\":\"1 Main Street\",\"addressLine2\":\"add line2\",\"suburb\":"
                + "\"Melbourne\",\"state\":\"VIC\",\"postcode\":\"3000\",\"country\":\"Australia\"},"
                + "\"manifest\":{\"transactionDateTime\":\"20150601\",\"manifestNumber\":\"5126013001\","
                + "\"consignments\":[{\"consignment\":{\"trackingId\":\"JDQ1234561\",\"deliveryAddress\":{"
                + "\"addressLine1\":\"2 Station Road\",\"addressLine2\":\"add line2\",\"suburb\":\"Sydney\","
                + "\"state\":\"NSW\",\"postcode\":\"2000\",\"country\":\"Australia\",\"name\":\"Camberwell\","
                + "\"companyName\":\"Target\"},\"parcels\":[{\"parcelDetails\":{\"height\":\"1.0\",\"length"
                + "\":\"1.0\",\"width\":\"1.0\",\"weight\":\"1.0\"}},{\"parcelDetails\":{\"height\":\"2.0\",\"length\":\"2.0\","
                + "\"width\":\"2.0\",\"weight\":\"2.0\"}}]}},{\"consignment\":{\"trackingId\":\"JDQ1234561\","
                + "\"deliveryAddress\":{\"addressLine1\":\"2 Station Road\",\"addressLine2\":\"add line2\","
                + "\"suburb\":\"Sydney\",\"state\":\"NSW\",\"postcode\":\"2000\",\"country\":\"Australia\","
                + "\"name\":\"Camberwell\",\"companyName\":\"Target\"},\"parcels\":[{\"parcelDetails\":{"
                + "\"height\":\"3.0\",\"length\":\"3.0\",\"width\":\"3.0\",\"weight\":\"3.0\"}},{\"parcelDetails\":{"
                + "\"height\":\"4.0\",\"length\":\"4.0\",\"width\":\"4.0\",\"weight\":\"4.0\"}}]}}]}}}";
        Assert.assertEquals(expectedJSON, responseJSON);
    }

    /**
     * Get Address Data
     * 
     * @return AddressDTO
     */
    private AddressDTO getAddressData() {
        final AddressDTO addressDTO = new AddressDTO();
        addressDTO.setAddressLine1("1 Main Street");
        addressDTO.setAddressLine2("add line2");
        addressDTO.setSuburb("Melbourne");
        addressDTO.setState("VIC");
        addressDTO.setPostcode("3000");
        addressDTO.setCountry("Australia");
        return addressDTO;
    }

    /**
     * Get DeliveryAddressDTO
     * 
     * @return DeliveryAddressDTO
     */
    private DeliveryAddressDTO getDeliveryAddressData() {
        final DeliveryAddressDTO deliveryAddressDTO = new DeliveryAddressDTO();
        deliveryAddressDTO.setName("Camberwell");
        deliveryAddressDTO.setCompanyName("Target");
        deliveryAddressDTO.setAddressLine1("2 Station Road");
        deliveryAddressDTO.setAddressLine2("add line2");
        deliveryAddressDTO.setSuburb("Sydney");
        deliveryAddressDTO.setState("NSW");
        deliveryAddressDTO.setPostcode("2000");
        deliveryAddressDTO.setCountry("Australia");
        return deliveryAddressDTO;
    }
}
