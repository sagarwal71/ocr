/**
 * 
 */
package au.com.target.tgtauspost.interceptor;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtauspost.model.ShipsterConfigModel;


/**
 * @author gsing236
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ShipsterConfigValidateInterceptorTest {

    @InjectMocks
    private final ShipsterConfigValidateInterceptor interceptor = new ShipsterConfigValidateInterceptor();

    @Mock
    private ShipsterConfigModel model;

    @Test(expected = InterceptorException.class)
    public void testWhenMinOrderValueIsNull() throws Exception {

        given(model.getActive()).willReturn(Boolean.TRUE);
        given(model.getMinOrderTotalValue()).willReturn(null);
        given(model.getMaxShippingThresholdValue()).willReturn(Double.valueOf(20.0));
        interceptor.onValidate(model, null);
    }

    @Test(expected = InterceptorException.class)
    public void testWhenMaxShippingThresholdIsNull() throws Exception {
        given(model.getActive()).willReturn(Boolean.TRUE);
        given(model.getMaxShippingThresholdValue()).willReturn(null);
        given(model.getMinOrderTotalValue()).willReturn(Double.valueOf(25.0));
        interceptor.onValidate(model, null);
    }

    @Test
    public void testWhenIsActiveFalgIsFalse() throws Exception {
        given(model.getActive()).willReturn(Boolean.FALSE);
        interceptor.onValidate(model, null);
        verify(model, times(0)).getMaxShippingThresholdValue();
        verify(model, times(0)).getMinOrderTotalValue();
    }

    @Test
    public void testWhenModelIsNull() throws Exception {
        interceptor.onValidate(null, null);
        verify(model, times(0)).getMaxShippingThresholdValue();
        verify(model, times(0)).getMinOrderTotalValue();
        verify(model, times(0)).getActive();
    }
}
