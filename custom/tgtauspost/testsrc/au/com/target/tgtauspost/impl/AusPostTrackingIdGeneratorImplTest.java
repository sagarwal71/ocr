/**
 * 
 */
package au.com.target.tgtauspost.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.exceptions.TrackingIdGenerationException;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;


/**
 * @author smishra1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AusPostTrackingIdGeneratorImplTest {

    @Mock
    private KeyGenerator keyGenerator;

    @Mock
    private Object object;

    @Mock
    private StoreFulfilmentCapabilitiesModel model;

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON
    @InjectMocks
    private final AusPostTrackingIdGeneratorImpl trackingIdGenerator = new AusPostTrackingIdGeneratorImpl();

    @Before
    public void setUp() {
        Mockito.when(keyGenerator.generate()).thenReturn(object);
    }

    @Test
    public void testGenerateTrackingId() {
        Mockito.when(model.getMerchantLocationId()).thenReturn("TGTRB");
        Mockito.when(object.toString()).thenReturn("0000001");
        final String expectedTrackingId = "TGTRB0000001";
        final String resultTrackingId = trackingIdGenerator.generateTrackingIdForConsignment(model);
        Assert.assertNotNull(resultTrackingId);
        Assert.assertEquals(expectedTrackingId, resultTrackingId);
        Assert.assertEquals(12, resultTrackingId.length());
    }

    @Test
    public void testGenerateTrackingIdWhenKeyGeneratorReturnsNull() {
        expectedException.expect(TrackingIdGenerationException.class);
        expectedException.expectMessage("Error while generating tracking Id : Null code returned by key generator");
        Mockito.when(keyGenerator.generate()).thenReturn(null);
        trackingIdGenerator.generateTrackingIdForConsignment(model);
    }

    @Test
    public void testGenerateTrackingIdWhenMLIdIsNull() {
        expectedException.expect(TrackingIdGenerationException.class);
        expectedException.expectMessage("Error while generating tracking Id : No MerchantLocationId found");
        Mockito.when(model.getMerchantLocationId()).thenReturn(null);
        Mockito.when(keyGenerator.generate()).thenReturn("0000002");
        trackingIdGenerator.generateTrackingIdForConsignment(model);
    }

}