package au.com.target.tgtauspost.deliveryclub.client;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ShipsterResponseHelperTest {

    private final ShipsterResponseHelper shipsterResponseHelper = new ShipsterResponseHelper();

    @Mock
    private HttpHeaders mockHttpHeaders;

    @Test
    public void testGetTooManyRequestsLogMessage() {
        prepareHeaders();

        final String result = shipsterResponseHelper.getTooManyRequestsLogMessage(mockHttpHeaders);

        assertThat(result).isEqualTo(
                "Limits=[Day=[limit=100, remaining=200], Hour=[limit=300, remaining=400], Minute=[limit=500, remaining=600], Second=[limit=700, remaining=800]]");
    }

    private void prepareHeaders() {
        given(mockHttpHeaders.getFirst("X-RateLimit-Limit-day")).willReturn("100");
        given(mockHttpHeaders.getFirst("X-RateLimit-Remaining-day")).willReturn("200");
        given(mockHttpHeaders.getFirst("X-RateLimit-Limit-hour")).willReturn("300");
        given(mockHttpHeaders.getFirst("X-RateLimit-Remaining-hour")).willReturn("400");
        given(mockHttpHeaders.getFirst("X-RateLimit-Limit-minute")).willReturn("500");
        given(mockHttpHeaders.getFirst("X-RateLimit-Remaining-minute")).willReturn("600");
        given(mockHttpHeaders.getFirst("X-RateLimit-Limit-second")).willReturn("700");
        given(mockHttpHeaders.getFirst("X-RateLimit-Remaining-second")).willReturn("800");
    }

}
