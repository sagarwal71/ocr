/**
 * 
 */
package au.com.target.tgtauspost.manifest.converter.impl;

import org.springframework.util.Assert;

import au.com.target.tgtauspost.integration.dto.StoreDTO;
import au.com.target.tgtcore.converters.TargetConverter;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;


/**
 * @author jjayawa1
 *
 */
public class StoreDetailsConverter implements TargetConverter<TargetPointOfServiceModel, StoreDTO> {

    @Override
    public StoreDTO convert(final TargetPointOfServiceModel pos) {
        Assert.notNull(pos, "Target Point of Service cannot be null");
        Assert.notNull(pos.getFulfilmentCapability(), "StoreFulfilmentCapabilities cannot be null");

        final StoreDTO store = new StoreDTO();
        final StoreFulfilmentCapabilitiesModel storeFulfilmentCapabilities = pos.getFulfilmentCapability();
        store.setChargeCode(storeFulfilmentCapabilities.getChargeCode());

        if (storeFulfilmentCapabilities.getChargeAccountNumber() != null) {
            store.setChargeToAccount(storeFulfilmentCapabilities.getChargeAccountNumber().toString());
        }

        store.setMerchantLocationId(storeFulfilmentCapabilities.getMerchantLocationId());
        store.setName(pos.getName());
        if (storeFulfilmentCapabilities.getProductCode() != null) {
            store.setProductCode(storeFulfilmentCapabilities.getProductCode().toString());
        }

        if (storeFulfilmentCapabilities.getServiceCode() != null) {
            store.setServiceCode(storeFulfilmentCapabilities.getServiceCode().toString());
        }

        return store;
    }

}
