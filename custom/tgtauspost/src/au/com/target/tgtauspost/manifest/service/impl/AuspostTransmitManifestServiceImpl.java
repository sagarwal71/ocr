/**
 * 
 */
package au.com.target.tgtauspost.manifest.service.impl;

import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtauspost.integration.dto.AuspostRequestDTO;
import au.com.target.tgtauspost.manifest.client.TransmitManifestClient;
import au.com.target.tgtauspost.manifest.converter.impl.ManifestDetailsPopulator;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.integration.dto.ManifestResponseDTO;
import au.com.target.tgtfulfilment.manifest.service.impl.DefaultTransmitManifestServiceImpl;
import au.com.target.tgtfulfilment.model.TargetManifestModel;
import au.com.target.tgtfulfilment.service.TargetManifestService;


/**
 * AusPost specific implementation for TransmitManifestService
 * 
 * @author jjayawa1
 *
 */
public class AuspostTransmitManifestServiceImpl extends DefaultTransmitManifestServiceImpl {

    private TransmitManifestClient transmitManifestClient;

    private ManifestDetailsPopulator manifestDetailsPopulator;

    private ModelService modelService;

    private TargetManifestService targetManifestService;


    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.service.TransmitManifestService#transmitManifest(au.com.target.tgtcore.model.TargetPointOfServiceModel, au.com.target.tgtfulfilment.model.TargetManifestModel)
     */
    @Override
    public ManifestResponseDTO transmitManifest(final TargetPointOfServiceModel pos, final TargetManifestModel manifest) {

        Assert.notNull(pos, "pos cannot be null");
        Assert.notNull(manifest, "manifest cannot be null");

        final AuspostRequestDTO request = manifestDetailsPopulator.populateManifestDetails(pos, manifest);
        final ManifestResponseDTO response = transmitManifestClient.transmitManifest(request);
        if (!response.isSuccess()) {
            return response;
        }

        markManifestSent(manifest);

        targetManifestService.completeConsignmentsInManifest(manifest, pos.getStoreNumber());

        return response;
    }

    private void markManifestSent(final TargetManifestModel manifest) {

        manifest.setSent(true);
        manifest.setDateTransmitted(new Date());
        modelService.save(manifest);
        modelService.refresh(manifest);
    }

    /**
     * @param transmitManifestClient
     *            the transmitManifestClient to set
     */
    @Required
    public void setTransmitManifestClient(final TransmitManifestClient transmitManifestClient) {
        this.transmitManifestClient = transmitManifestClient;
    }

    /**
     * @param manifestDetailsPopulator
     *            the manifestDetailsPopulator to set
     */
    @Required
    public void setManifestDetailsPopulator(final ManifestDetailsPopulator manifestDetailsPopulator) {
        this.manifestDetailsPopulator = manifestDetailsPopulator;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    @Required
    public void setTargetManifestService(final TargetManifestService targetManifestService) {
        this.targetManifestService = targetManifestService;
    }

}
