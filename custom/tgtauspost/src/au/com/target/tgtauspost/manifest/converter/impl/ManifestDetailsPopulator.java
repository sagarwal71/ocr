/**
 * 
 */
package au.com.target.tgtauspost.manifest.converter.impl;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtauspost.integration.dto.AuspostRequestDTO;
import au.com.target.tgtauspost.integration.dto.StoreDTO;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.model.TargetManifestModel;


/**
 * @author jjayawa1
 *
 */
public class ManifestDetailsPopulator {

    private ManifestStoreDetailsConverter manifestStoreDetailsConverter;
    private AuspostAddressConverter auspostAddressConverter;
    private ManifestConverter manifestConverter;

    public AuspostRequestDTO populateManifestDetails(final TargetPointOfServiceModel pos,
            final TargetManifestModel manifest) {
        Assert.notNull(pos, "Point of Service cannot be null");
        Assert.notNull(manifest, "Manifest cannot be null");

        final AuspostRequestDTO request = new AuspostRequestDTO();

        final StoreDTO storeDTO = manifestStoreDetailsConverter.convert(pos);

        storeDTO.setAddress(auspostAddressConverter.convert(pos.getAddress()));

        storeDTO.setManifest(manifestConverter.convert(manifest));

        request.setStore(storeDTO);
        return request;
    }

    /**
     * @param manifestStoreDetailsConverter
     *            the manifestStoreDetailsConverter to set
     */
    @Required
    public void setManifestStoreDetailsConverter(final ManifestStoreDetailsConverter manifestStoreDetailsConverter) {
        this.manifestStoreDetailsConverter = manifestStoreDetailsConverter;
    }

    /**
     * @param auspostAddressConverter
     *            the auspostAddressConverter to set
     */
    @Required
    public void setAuspostAddressConverter(final AuspostAddressConverter auspostAddressConverter) {
        this.auspostAddressConverter = auspostAddressConverter;
    }

    /**
     * @param manifestConverter
     *            the manifestConverter to set
     */
    @Required
    public void setManifestConverter(final ManifestConverter manifestConverter) {
        this.manifestConverter = manifestConverter;
    }

}
