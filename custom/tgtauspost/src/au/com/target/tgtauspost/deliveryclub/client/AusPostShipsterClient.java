/**
 * 
 */
package au.com.target.tgtauspost.deliveryclub.client;

import au.com.target.tgtauspost.deliveryclub.dto.ShipsterVerifyEmailResponseDTO;
import au.com.target.tgtauspost.exception.AuspostShipsterClientException;
import au.com.target.tgtauspost.integration.dto.ShipsterConfirmOrderRequestDTO;
import au.com.target.tgtauspost.integration.dto.ShipsterConfirmOrderResponseDTO;


/**
 * @author gsing236
 *
 */
public interface AusPostShipsterClient {

    /**
     * verify if the email address is registered with AusPost for delivery club
     * 
     * @param email
     *            The customer email address
     * @return - boolean
     * @throws AuspostShipsterClientException
     */
    ShipsterVerifyEmailResponseDTO verify(String email) throws AuspostShipsterClientException;


    /**
     * Order confirmation sent to Aus-post once an order of a Shipster customer has been confirmed and the delivery mode
     * used is active for Shipster
     * 
     * @param request
     * @return ShipsterConfirmOrderResponseDTO
     * @throws AuspostShipsterClientException
     */
    ShipsterConfirmOrderResponseDTO confirmOrder(ShipsterConfirmOrderRequestDTO request)
            throws AuspostShipsterClientException;
}
