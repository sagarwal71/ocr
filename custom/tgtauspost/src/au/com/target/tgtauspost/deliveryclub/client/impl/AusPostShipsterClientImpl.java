/**
 * 
 */
package au.com.target.tgtauspost.deliveryclub.client.impl;

import java.net.URI;
import java.text.MessageFormat;

import au.com.target.tgtauspost.exception.AuspostHttpStatusCodeException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtauspost.deliveryclub.client.AusPostShipsterClient;
import au.com.target.tgtauspost.deliveryclub.client.ShipsterResponseHelper;
import au.com.target.tgtauspost.deliveryclub.dto.AbstractShipsterResponseDTO;
import au.com.target.tgtauspost.deliveryclub.dto.ShipsterVerifyEmailResponseDTO;
import au.com.target.tgtauspost.exception.AuspostShipsterClientException;
import au.com.target.tgtauspost.integration.dto.ShipsterConfirmOrderRequestDTO;
import au.com.target.tgtauspost.integration.dto.ShipsterConfirmOrderResponseDTO;
import au.com.target.tgtutility.logging.JsonLogger;


/**
 * @author gsing236
 *
 */
public class AusPostShipsterClientImpl implements AusPostShipsterClient {

    private static final Logger LOG = Logger.getLogger(AusPostShipsterClientImpl.class);

    private static final String LOG_FORMAT = "Shipster: action={0} message={1}";
    private static final String LOG_FORMAT_WITH_CART = "Shipster: action={0} cartId={1} message={2}";

    private static final String VERIFY_EMAIL = "verifyEmail";
    private static final String CONFIRM_ORDER = "confirmOrder";
    private static final String ERROR = "error";
    private static final String START_REST_CALL = "start";

    private RestTemplate deliveryClubRestTemplate;

    private String deliveryClubBaseUrl;

    private String verifyEmailUrl;

    private String confirmOrderUrl;

    private ShipsterResponseHelper shipsterResponseHelper;


    protected ResponseEntity<ShipsterVerifyEmailResponseDTO> verifyEmailRestTemplate(final String email) {

        return deliveryClubRestTemplate
                .getForEntity(URI.create(deliveryClubBaseUrl + verifyEmailUrl + email),
                        ShipsterVerifyEmailResponseDTO.class);
    }

    @Override
    public ShipsterVerifyEmailResponseDTO verify(final String email) throws AuspostShipsterClientException {

        log(VERIFY_EMAIL, START_REST_CALL, null);
        try {

            final ResponseEntity<ShipsterVerifyEmailResponseDTO> verifyEmailResponse = verifyEmailRestTemplate(email);

            if (!HttpStatus.Series.SUCCESSFUL.equals(verifyEmailResponse.getStatusCode().series())
                    && !HttpStatus.PAYMENT_REQUIRED.equals(verifyEmailResponse.getStatusCode())) {
                handleError(verifyEmailResponse, VERIFY_EMAIL);
            }
            
            if(verifyEmailResponse.getStatusCode().is4xxClientError() || verifyEmailResponse.getStatusCode().is5xxServerError()){
              throw new AuspostHttpStatusCodeException(verifyEmailResponse.getStatusCodeValue() + " " + verifyEmailResponse.getStatusCode().getReasonPhrase());
            }
            JsonLogger.logJson(LOG, verifyEmailResponse.getBody());
            return verifyEmailResponse.getBody();
        }
        catch (final Exception e) {
            log(Level.ERROR, VERIFY_EMAIL, ERROR, e);
            throw new AuspostShipsterClientException(e);
        }
    }

    protected ResponseEntity<ShipsterConfirmOrderResponseDTO> confirmOrderRestTemplate(
            final ShipsterConfirmOrderRequestDTO request) {
        return deliveryClubRestTemplate
                .postForEntity(
                        deliveryClubBaseUrl + confirmOrderUrl,
                        new HttpEntity<ShipsterConfirmOrderRequestDTO>(request),
                        ShipsterConfirmOrderResponseDTO.class);
    }


    @Override
    public ShipsterConfirmOrderResponseDTO confirmOrder(
            final ShipsterConfirmOrderRequestDTO request)
            throws AuspostShipsterClientException {
        log(CONFIRM_ORDER, request != null ? request.getOrderReference() : null, START_REST_CALL, null);
        try {
            final ResponseEntity<ShipsterConfirmOrderResponseDTO> shipsterConfirmOrderResponseEntity = confirmOrderRestTemplate(
                    request);
            ShipsterConfirmOrderResponseDTO shipsterConfirmOrderResponse = shipsterConfirmOrderResponseEntity
                    .getBody();
  
            if(shipsterConfirmOrderResponseEntity.getStatusCode().is5xxServerError()
                    || shipsterConfirmOrderResponseEntity.getStatusCode().is4xxClientError() ){

                LOG.log(Level.ERROR, MessageFormat.format(LOG_FORMAT_WITH_CART, CONFIRM_ORDER, request != null ? request.getOrderReference() : null, ERROR));
                JsonLogger.logJson(LOG, shipsterConfirmOrderResponseEntity.getBody());
                shipsterConfirmOrderResponse = new ShipsterConfirmOrderResponseDTO();
                shipsterConfirmOrderResponse.setSubmitted(false);
                shipsterConfirmOrderResponse.setRetry(true);
                return shipsterConfirmOrderResponse;
            }
            
            if (!HttpStatus.Series.SUCCESSFUL.equals(shipsterConfirmOrderResponseEntity.getStatusCode().series())
                    && handleError(shipsterConfirmOrderResponseEntity, CONFIRM_ORDER)) {
                shipsterConfirmOrderResponse.setRetry(true);
            }

            JsonLogger.logJson(LOG, shipsterConfirmOrderResponse);
            return shipsterConfirmOrderResponse;
        }
        catch (final RestClientException restClientException) {
            log(Level.ERROR, CONFIRM_ORDER, request != null ? request.getOrderReference() : null, ERROR,
                    restClientException);
            final ShipsterConfirmOrderResponseDTO shipsterConfirmOrderResponse = new ShipsterConfirmOrderResponseDTO();
            shipsterConfirmOrderResponse.setSubmitted(false);
            shipsterConfirmOrderResponse.setRetry(true);
            return shipsterConfirmOrderResponse;

        }
        catch (final Exception e) {
            log(Level.ERROR, CONFIRM_ORDER, request != null ? request.getOrderReference() : null, ERROR, e);
            throw new AuspostShipsterClientException(e);
        }
    }

    private boolean handleError(final ResponseEntity<? extends AbstractShipsterResponseDTO> response,
            final String action) {
        final boolean errorsPresent = response.getBody() != null
                && CollectionUtils.isNotEmpty(response.getBody().getErrors());

        boolean retryRequired = false;
        final StringBuilder errorMessageBuilder = new StringBuilder();
        if (!errorsPresent) {
            errorMessageBuilder.append("Unknown error from Shipster API");
        }
        else {
            errorMessageBuilder.append("Error from Shipster API");
        }

        final HttpStatus status = response.getStatusCode();
        errorMessageBuilder.append(", statusCode=").append(status.toString());


        if (HttpStatus.TOO_MANY_REQUESTS.equals(status)) {
            final HttpHeaders responseHeaders = response.getHeaders();
            errorMessageBuilder.append(", Rate limit exceeded. ")
                    .append(shipsterResponseHelper.getTooManyRequestsLogMessage(responseHeaders));
            retryRequired = true;
        }
        if (HttpStatus.SERVICE_UNAVAILABLE.equals(status)) {
            errorMessageBuilder.append(HttpStatus.SERVICE_UNAVAILABLE.getReasonPhrase());
            retryRequired = true;
        }
        log(Level.ERROR, action, errorMessageBuilder.toString(), null);
        if (errorsPresent) {
            JsonLogger.logJson(LOG, response.getBody().getErrors());
        }

        return retryRequired && CONFIRM_ORDER.equals(action);
    }

    private void log(final Level level, final String action, final String cartId, final String message,
            final Throwable t) {
        LOG.log(level, MessageFormat.format(LOG_FORMAT_WITH_CART, action, cartId, message), t);
    }

    private void log(final String action, final String cartId, final String message, final Throwable t) {
        log(Level.INFO, action, cartId, message, t);
    }

    private void log(final Level level, final String action, final String message,
            final Throwable t) {
        LOG.log(level, MessageFormat.format(LOG_FORMAT, action, message), t);
    }

    private void log(final String action, final String message, final Throwable t) {
        log(Level.INFO, action, message, t);
    }

    /**
     * @param deliveryClubRestTemplate
     *            the deliveryClubRestTemplate to set
     */
    @Required
    public void setDeliveryClubRestTemplate(final RestTemplate deliveryClubRestTemplate) {
        this.deliveryClubRestTemplate = deliveryClubRestTemplate;
    }

    /**
     * @param deliveryClubBaseUrl
     *            the deliveryClubBaseUrl to set
     */
    @Required
    public void setDeliveryClubBaseUrl(final String deliveryClubBaseUrl) {
        this.deliveryClubBaseUrl = deliveryClubBaseUrl;
    }

    /**
     * @param verifyEmailUrl
     *            the verifyEmailUrl to set
     */
    @Required
    public void setVerifyEmailUrl(final String verifyEmailUrl) {
        this.verifyEmailUrl = verifyEmailUrl;
    }

    /**
     * @param confirmOrderUrl
     *            the confirmOrderUrl to set
     */
    @Required
    public void setConfirmOrderUrl(final String confirmOrderUrl) {
        this.confirmOrderUrl = confirmOrderUrl;
    }

    /**
     * @param shipsterResponseHelper
     *            the shipsterResponseHelper to set
     */
    @Required
    public void setShipsterResponseHelper(final ShipsterResponseHelper shipsterResponseHelper) {
        this.shipsterResponseHelper = shipsterResponseHelper;
    }

}
