/**
 * 
 */
package au.com.target.tgtauspost.deliveryclub.dto;

import java.io.Serializable;
import java.util.List;


/**
 * @author rmcalave
 *
 */
public class AbstractShipsterResponseDTO implements Serializable {

    private List<ShipsterErrorResponseDTO> errors;

    private boolean retry;

    /**
     * @return the retry
     */
    public boolean isRetry() {
        return retry;
    }

    /**
     * @param retry
     *            the retry to set
     */
    public void setRetry(final boolean retry) {
        this.retry = retry;
    }

    /**
     * @return the errors
     */
    public List<ShipsterErrorResponseDTO> getErrors() {
        return errors;
    }

    /**
     * @param errors
     *            the errors to set
     */
    public void setErrors(final List<ShipsterErrorResponseDTO> errors) {
        this.errors = errors;
    }

}
