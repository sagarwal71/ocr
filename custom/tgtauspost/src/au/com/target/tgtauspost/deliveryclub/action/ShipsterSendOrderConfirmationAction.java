/**
 * 
 */
package au.com.target.tgtauspost.deliveryclub.action;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtauspost.deliveryclub.service.ShipsterClientService;
import au.com.target.tgtauspost.integration.dto.ShipsterConfirmOrderResponseDTO;
import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;
import au.com.target.tgtutility.constants.TgtutilityConstants;


/**
 * @author bpottass
 *
 */
public class ShipsterSendOrderConfirmationAction extends RetryActionWithSpringConfig<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(ShipsterSendOrderConfirmationAction.class);
    private ShipsterClientService shipsterClientService;

    public enum Transition {
        OK, RETRYEXCEEDED;

        public static Set<String> getStringValues() {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values()) {
                res.add(t.toString());
            }
            return res;
        }
    }

    /* (non-Javadoc)
    * @see au.com.target.tgtbusproc.actions.RetryAction#executeInternal(de.hybris.platform.processengine.model.BusinessProcessModel)
    */
    @Override
    protected String executeInternal(final OrderProcessModel process) throws RetryLaterException, Exception {
        final OrderModel order = process.getOrder();
        Assert.notNull(order, "Order cannot be null");
        return getTransition(shipsterClientService.confirmOrder(order), order);
    }

    /**
     * Gets the transition.
     * 
     * @param status
     *            the status
     * @param order
     *            the order
     * @return the transition
     * @throws RetryLaterException
     *             the retry later exception
     */
    protected String getTransition(final ShipsterConfirmOrderResponseDTO status, final OrderModel order)
            throws RetryLaterException {
        final Transition result = Transition.OK;
        if (status != null) {
            LOG.info(TgtutilityConstants.InfoCode.SHIPSTER_SEND_ORDER_CONFIRMATION_ACTION + " Shipster Transaction Id= "
                    + status.getTransactionId()
                    + " Order Id= " + order.getCode());
            if (!status.isSubmitted()) {
                LOG.warn(TgtutilityConstants.ErrorCode.ERR_SHIPSTER_SEND_ORDER_CONFIRMATION
                        + " Shipster Transaction Id= "
                        + status.getTransactionId()
                        + " Order Id= " + order.getCode());
                if (status.isRetry()) {
                    LOG.info(TgtutilityConstants.InfoCode.SHIPSTER_SEND_ORDER_CONFIRMATION_ACTION_RETRY
                            + " Shipster Transaction Id= "
                            + status.getTransactionId()
                            + " Order Id= " + order.getCode());
                    throw new RetryLaterException(
                            "Shipster send order confirmation failed, retrying for order: " + order.getCode());
                }
            }
        }
        return result.toString();
    }



    /* (non-Javadoc)
     * @see au.com.target.tgtbusproc.actions.RetryAction#getTransitionsInternal()
     */
    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }

    @Required
    public void setShipsterClientService(final ShipsterClientService shipsterClientService) {
        this.shipsterClientService = shipsterClientService;
    }

}
