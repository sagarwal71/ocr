/**
 * 
 */
package au.com.target.tgtauspost.integration.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * DTO to populate with details for manifest request.
 * 
 * @author jjayawa1
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LabelRequestDTO {
    private StoreDTO store;

    /**
     * @return the store
     */
    public StoreDTO getStore() {
        return store;
    }

    /**
     * @param store
     *            the store to set
     */
    public void setStore(final StoreDTO store) {
        this.store = store;
    }

}
