/**
 * 
 */
package au.com.target.tgtauspost.integration.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * DTO to populate consignment detail object.
 * 
 * @author jjayawa1
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConsignmentDTO {
    private ConsignmentDetailsDTO consignment;

    /**
     * @return the consignment
     */
    public ConsignmentDetailsDTO getConsignment() {
        return consignment;
    }

    /**
     * @param consignment
     *            the consignment to set
     */
    public void setConsignment(final ConsignmentDetailsDTO consignment) {
        this.consignment = consignment;
    }



}
