/**
 * 
 */
package au.com.target.tgtauspost.integration.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * DTO to populate consignment details.
 * 
 * @author jjayawa1
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConsignmentDetailsDTO {
    private String trackingId;
    private DeliveryAddressDTO deliveryAddress;
    private List<ParcelDetailsDTO> parcels;

    /**
     * @return the trackingId
     */
    public String getTrackingId() {
        return trackingId;
    }

    /**
     * @param trackingId
     *            the trackingId to set
     */
    public void setTrackingId(final String trackingId) {
        this.trackingId = trackingId;
    }

    /**
     * @return the deliveryAddress
     */
    public DeliveryAddressDTO getDeliveryAddress() {
        return deliveryAddress;
    }

    /**
     * @param deliveryAddress
     *            the deliveryAddress to set
     */
    public void setDeliveryAddress(final DeliveryAddressDTO deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    /**
     * @return the parcels
     */
    public List<ParcelDetailsDTO> getParcels() {
        return parcels;
    }

    /**
     * @param parcels
     *            the parcels to set
     */
    public void setParcels(final List<ParcelDetailsDTO> parcels) {
        this.parcels = parcels;
    }
}
