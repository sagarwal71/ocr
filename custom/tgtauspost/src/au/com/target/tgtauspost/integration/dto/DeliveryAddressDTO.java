/**
 * 
 */
package au.com.target.tgtauspost.integration.dto;

import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * DTO to hold delivery address details.
 * 
 * @author jjayawa1
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeliveryAddressDTO extends AddressDTO {
    private String name;
    private String companyName;
    private String phone;
    private String email;
    private String sendNotifications;

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     *            the phone to set
     */
    public void setPhone(final String phone) {
        this.phone = phone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * @return the sendNotifications
     */
    public String getSendNotifications() {
        return sendNotifications;
    }

    /**
     * @param sendNotifications
     *            the sendNotifications to set
     */
    public void setSendNotifications(final String sendNotifications) {
        this.sendNotifications = sendNotifications;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the companyName
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * @param companyName
     *            the companyName to set
     */
    public void setCompanyName(final String companyName) {
        this.companyName = companyName;
    }

}
