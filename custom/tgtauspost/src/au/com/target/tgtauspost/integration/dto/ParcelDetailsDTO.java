/**
 * 
 */
package au.com.target.tgtauspost.integration.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import au.com.target.tgtfulfilment.dto.ConsignmentParcelDTO;


/**
 * DTO to hold parcel details.
 * 
 * @author jjayawa1
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ParcelDetailsDTO {
    private ConsignmentParcelDTO parcelDetails;

    /**
     * @return the parcelDetails
     */
    public ConsignmentParcelDTO getParcelDetails() {
        return parcelDetails;
    }

    /**
     * @param parcelDetails
     *            the parcelDetails to set
     */
    public void setParcelDetails(final ConsignmentParcelDTO parcelDetails) {
        this.parcelDetails = parcelDetails;
    }

}
