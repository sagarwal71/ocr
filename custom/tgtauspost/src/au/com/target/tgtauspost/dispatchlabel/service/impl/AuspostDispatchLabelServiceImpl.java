/**
 * 
 */
package au.com.target.tgtauspost.dispatchlabel.service.impl;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtauspost.dispatchlabel.client.DispatchLabelClient;
import au.com.target.tgtauspost.integration.dto.AuspostRequestDTO;
import au.com.target.tgtauspost.manifest.converter.impl.LabelDetailsPopulator;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.dispatchlabel.service.DispatchLabelService;
import au.com.target.tgtfulfilment.integration.dto.LabelLayout;
import au.com.target.tgtfulfilment.integration.dto.LabelResponseDTO;


/**
 * @author sbryan6
 *
 */
public class AuspostDispatchLabelServiceImpl implements DispatchLabelService {

    private DispatchLabelClient dispatchLabelClient;
    private LabelDetailsPopulator labelDetailsPopulator;

    @Override
    public LabelResponseDTO getDispatchLabel(final TargetPointOfServiceModel pos,
            final TargetConsignmentModel consignment,
            final LabelLayout layout, final boolean branding) {
        final AuspostRequestDTO request = labelDetailsPopulator
                .populateLabelDetails(pos, consignment, layout, branding);
        final LabelResponseDTO response = dispatchLabelClient.retrieveDispatchLabel(request);
        return response;
    }

    /**
     * @param dispatchLabelClient
     *            the dispatchLabelClient to set
     */
    @Required
    public void setDispatchLabelClient(final DispatchLabelClient dispatchLabelClient) {
        this.dispatchLabelClient = dispatchLabelClient;
    }

    /**
     * @param labelDetailsPopulator
     *            the labelDetailsPopulator to set
     */
    @Required
    public void setLabelDetailsPopulator(final LabelDetailsPopulator labelDetailsPopulator) {
        this.labelDetailsPopulator = labelDetailsPopulator;
    }

}
