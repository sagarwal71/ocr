/**
 * 
 */
package au.com.target.tgtauspost.dispatchlabel.client;

import au.com.target.tgtauspost.integration.dto.AuspostRequestDTO;
import au.com.target.tgtfulfilment.integration.dto.LabelResponseDTO;


/**
 * Interface to define supported dispatch label operations.
 * 
 * @author sbryan6
 *
 */
public interface DispatchLabelClient {

    /**
     * Retrieve a dispatch label from carrier.
     * 
     * @param request
     * @return response
     */
    LabelResponseDTO retrieveDispatchLabel(AuspostRequestDTO request);
}
