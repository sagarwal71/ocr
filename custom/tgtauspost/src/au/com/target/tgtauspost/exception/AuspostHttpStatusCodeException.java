package au.com.target.tgtauspost.exception;

public class AuspostHttpStatusCodeException extends RuntimeException{
  public AuspostHttpStatusCodeException(final String message) {
    super(message);
  }
  
  public AuspostHttpStatusCodeException(final Throwable cause) {
    super(cause);
  }
  
  public AuspostHttpStatusCodeException(final String message, final Throwable cause) {
    super(message, cause);
  }
}
