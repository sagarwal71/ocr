/**
 * 
 */
package au.com.target.tgtpublicws.navigation;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtpublicws.navigation.dto.HomeScreenLayoutDTO;
import au.com.target.tgtpublicws.navigation.dto.HomeScreenTileDTO;
import au.com.target.tgtwebcore.enums.MobileHomeScreenImageLayoutEnum;


/**
 * @author rmcalave
 *
 */
public class HomeScreenLayoutBuilder {
    private static final String IMAGE_MIME_TYPE_PREFIX = "image";

    private Converter<CategoryModel, CategoryData> categoryConverter;
    private UrlResolver<CategoryData> categoryDataUrlResolver;

    public HomeScreenLayoutDTO buildHomeScreenLayout(final CMSNavigationNodeModel navigationNode) {
        if (navigationNode == null || !navigationNode.isVisible()) {
            return null;
        }

        final HomeScreenLayoutDTO layout = new HomeScreenLayoutDTO();

        final List<HomeScreenTileDTO> tiles = new ArrayList<>();
        for (final CMSNavigationNodeModel childNavNode : navigationNode.getChildren()) {
            final HomeScreenTileDTO tile = buildHomeScreenTile(childNavNode);
            if (tile != null) {
                tiles.add(buildHomeScreenTile(childNavNode));
            }
        }

        layout.setTiles(tiles);
        return layout;
    }

    private HomeScreenTileDTO buildHomeScreenTile(final CMSNavigationNodeModel navigationNode) {
        if (navigationNode == null || !navigationNode.isVisible()) {
            return null;
        }

        final HomeScreenTileDTO tile = new HomeScreenTileDTO();
        final MobileHomeScreenImageLayoutEnum layoutEnum = navigationNode.getLayout();
        if (layoutEnum != null) {
            tile.setLayout(layoutEnum.getCode());
        }

        boolean isLinkSet = false;
        boolean isImageSet = false;
        for (final CMSNavigationEntryModel entry : navigationNode.getEntries()) {
            final ItemModel entryItem = entry.getItem();

            if (!isLinkSet) {
                if (entryItem instanceof ContentPageModel) {
                    final ContentPageModel contentPage = (ContentPageModel)entryItem;
                    tile.setLinkUrl(contentPage.getLabel());

                    isLinkSet = true;
                }
                else if (entryItem instanceof CMSLinkComponentModel) {
                    final CMSLinkComponentModel link = (CMSLinkComponentModel)entryItem;

                    if (Boolean.FALSE.equals(link.getVisible())) {
                        continue;
                    }

                    tile.setLinkUrl(link.getUrl());
                    isLinkSet = true;
                }
                else if (entryItem instanceof CategoryModel) {
                    final CategoryModel category = (CategoryModel)entryItem;
                    final CategoryData categoryData = categoryConverter.convert(category);
                    tile.setLinkUrl(categoryDataUrlResolver.resolve(categoryData));

                    isLinkSet = true;
                }
            }

            if (!isImageSet) {
                if (entryItem instanceof MediaModel) {
                    final MediaModel media = (MediaModel)entryItem;

                    // Check if the media is an image type.
                    if (StringUtils.startsWith(media.getMime(), IMAGE_MIME_TYPE_PREFIX)) {
                        tile.setImageUrl(media.getDownloadURL());
                    }

                    isImageSet = true;
                }
            }

            if (isLinkSet && isImageSet) {
                break;
            }
        }

        return tile;
    }

    /**
     * @param categoryConverter
     *            the categoryConverter to set
     */
    @Required
    public void setCategoryConverter(final Converter<CategoryModel, CategoryData> categoryConverter) {
        this.categoryConverter = categoryConverter;
    }

    /**
     * @param categoryDataUrlResolver
     *            the categoryDataUrlResolver to set
     */
    @Required
    public void setCategoryDataUrlResolver(final UrlResolver<CategoryData> categoryDataUrlResolver) {
        this.categoryDataUrlResolver = categoryDataUrlResolver;
    }

}
