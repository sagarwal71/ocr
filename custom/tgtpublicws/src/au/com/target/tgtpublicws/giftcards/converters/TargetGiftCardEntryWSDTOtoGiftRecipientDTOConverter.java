/**
 * 
 */
package au.com.target.tgtpublicws.giftcards.converters;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.converters.TargetConverter;
import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;
import au.com.target.tgtpublicws.delivery.dto.order.TargetGiftCardOrderEntryWsDTO;


/**
 * Convert TargetGiftCardOrderEntryDTO to GiftRecipientDTO
 * 
 * @author pthoma20
 *
 */
public class TargetGiftCardEntryWSDTOtoGiftRecipientDTOConverter
        implements TargetConverter<TargetGiftCardOrderEntryWsDTO, GiftRecipientDTO> {

    @Override
    public GiftRecipientDTO convert(final TargetGiftCardOrderEntryWsDTO dto) {
        final GiftRecipientDTO giftRecipientDTO = new GiftRecipientDTO();

        if (StringUtils.isNotEmpty(dto.getFirstName())) {
            giftRecipientDTO.setFirstName(dto.getFirstName());
        }

        if (StringUtils.isNotEmpty(dto.getLastName())) {
            giftRecipientDTO.setLastName(dto.getLastName());
        }

        if (StringUtils.isNotEmpty(dto.getRecipientEmailAddress())) {
            giftRecipientDTO.setRecipientEmailAddress(dto.getRecipientEmailAddress());
        }

        if (StringUtils.isNotEmpty(dto.getMessageText())) {
            giftRecipientDTO.setMessageText(dto.getMessageText());
        }

        return giftRecipientDTO;
    }

}
