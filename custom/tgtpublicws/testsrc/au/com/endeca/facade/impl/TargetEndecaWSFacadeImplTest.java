/**
 * 
 */
package au.com.endeca.facade.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.converter.AggregatedEndecaRecordToTargetProductListerConverter;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.querybuilder.TargetDealProductsQueryBuilder;
import au.com.target.endeca.infront.querybuilder.TargetInstagramAndCatalogueQueryBuilder;
import au.com.target.endeca.infront.util.EndecaQueryConnecton;
import au.com.target.tgtfacades.product.data.TargetProductListerData;

import com.endeca.navigation.AggrERec;
import com.endeca.navigation.AggrERecList;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.HttpENEConnection;
import com.endeca.navigation.MAggrERecList;
import com.endeca.navigation.Navigation;
import com.endeca.navigation.UrlENEQuery;
import com.endeca.navigation.UrlENEQueryParseException;


/**
 * @author pthoma20
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TargetEndecaWSFacadeImplTest {

    @Mock
    private EndecaQueryConnecton endecaQueryConnection;

    @Mock
    private AggregatedEndecaRecordToTargetProductListerConverter<AggrERec, TargetProductListerData> aggrERecToProductConverter;

    @Mock
    private TargetInstagramAndCatalogueQueryBuilder targetInstagramAndCatalogueQueryBuilder;

    @Mock
    private TargetDealProductsQueryBuilder mockTargetDealProductsQueryBuilder;

    @InjectMocks
    private final TargetEndecaWSFacadeImpl targetEndecaWSFacadeImpl = new TargetEndecaWSFacadeImpl();

    @Test
    public void testGetProductsForInstagramAndCataloueWhenProductsAreReturned()
            throws ENEQueryException, TargetEndecaException {
        final List<String> productCodes = Mockito.mock(ArrayList.class);
        final UrlENEQuery mockQuery = Mockito.mock(UrlENEQuery.class);
        final ArgumentCaptor<EndecaSearchStateData> categoryCaptor = ArgumentCaptor
                .forClass(EndecaSearchStateData.class);
        BDDMockito
                .given(targetInstagramAndCatalogueQueryBuilder
                        .getQueryForFetchingAllProductsForInstagram(categoryCaptor.capture()))
                .willReturn(mockQuery);
        final HttpENEConnection mockHttpENEConnection = Mockito.mock(HttpENEConnection.class);

        final TargetProductListerData mockTargetProductListerData1 = Mockito.mock(TargetProductListerData.class);
        final TargetProductListerData mockTargetProductListerData2 = Mockito.mock(TargetProductListerData.class);
        final ENEQueryResults mockENEQueryResults = mockQueryResultsToReturn(mockTargetProductListerData1,
                mockTargetProductListerData2);
        BDDMockito.given(endecaQueryConnection.getConection()).willReturn(mockHttpENEConnection);
        BDDMockito.given(mockHttpENEConnection.query(mockQuery)).willReturn(mockENEQueryResults);

        final List<TargetProductListerData> targetProductListerDataList = targetEndecaWSFacadeImpl
                .getProductsForInstagramAndCatalogue(productCodes);
        Assert.assertEquals(categoryCaptor.getValue().getNpSearchState(), EndecaConstants.ENDECA_NP_1);
        Assert.assertEquals(categoryCaptor.getValue().getNpSearchState(), EndecaConstants.ENDECA_NP_1);

        Assertions.assertThat(targetProductListerDataList).contains(mockTargetProductListerData1,
                mockTargetProductListerData2);

    }

    @Test
    public void testGetProductsForInstagramAndCatalogueWhenNoProductsAreReturned()
            throws ENEQueryException, TargetEndecaException {
        final List<String> productCodes = Mockito.mock(ArrayList.class);
        final UrlENEQuery mockQuery = Mockito.mock(UrlENEQuery.class);
        final ArgumentCaptor<EndecaSearchStateData> categoryCaptor = ArgumentCaptor
                .forClass(EndecaSearchStateData.class);
        BDDMockito
                .given(targetInstagramAndCatalogueQueryBuilder
                        .getQueryForFetchingAllProductsForInstagram(categoryCaptor.capture()))
                .willReturn(mockQuery);

        final HttpENEConnection mockHttpENEConnection = Mockito.mock(HttpENEConnection.class);

        final ENEQueryResults mockENEQueryResults = mockQueryResultsToReturn(null,
                null);
        BDDMockito.given(endecaQueryConnection.getConection()).willReturn(mockHttpENEConnection);
        BDDMockito.given(mockHttpENEConnection.query(mockQuery)).willReturn(mockENEQueryResults);

        final List<TargetProductListerData> targetProductListerDataList = targetEndecaWSFacadeImpl
                .getProductsForInstagramAndCatalogue(productCodes);
        Assertions.assertThat(targetProductListerDataList).isEmpty();

    }

    @Test
    public void testGetProductsForDealCategoriesWithNullList() {
        final List<TargetProductListerData> result = targetEndecaWSFacadeImpl.getProductsForDealCategories(null);

        assertThat(result).isEmpty();
    }

    @Test
    public void testGetProductsForDealCategoriesWithEmptyList() {
        final List<String> dealCategoryDimIds = new ArrayList<>();
        final List<TargetProductListerData> result = targetEndecaWSFacadeImpl
                .getProductsForDealCategories(dealCategoryDimIds);

        assertThat(result).isEmpty();
    }

    @Test
    public void testGetProductsForDealCategories() throws Exception {
        final String dealCategoryDimId = "9389123709";

        final List<String> dealCategoryDimIds = new ArrayList<>();
        dealCategoryDimIds.add(dealCategoryDimId);

        final UrlENEQuery mockQuery = mock(UrlENEQuery.class);
        given(mockTargetDealProductsQueryBuilder.getQueryForFetchingAllProductsForDealCategory(dealCategoryDimId))
                .willReturn(mockQuery);

        final HttpENEConnection mockHttpEneConnection = mock(HttpENEConnection.class);
        given(endecaQueryConnection.getConection()).willReturn(mockHttpEneConnection);

        final TargetProductListerData product1 = mock(TargetProductListerData.class);
        final TargetProductListerData product2 = mock(TargetProductListerData.class);

        final ENEQueryResults mockQueryResults = mockQueryResultsToReturn(product1, product2);
        given(mockHttpEneConnection.query(mockQuery)).willReturn(mockQueryResults);

        final List<TargetProductListerData> result = targetEndecaWSFacadeImpl
                .getProductsForDealCategories(dealCategoryDimIds);

        assertThat(result).containsExactly(product1, product2);
    }

    @Test
    public void testGetProductsForDealCategoriesNoProductsReturned() throws Exception {
        final String dealCategoryDimId = "9389123709";

        final List<String> dealCategoryDimIds = new ArrayList<>();
        dealCategoryDimIds.add(dealCategoryDimId);

        final UrlENEQuery mockQuery = mock(UrlENEQuery.class);
        given(mockTargetDealProductsQueryBuilder.getQueryForFetchingAllProductsForDealCategory(dealCategoryDimId))
                .willReturn(mockQuery);

        final HttpENEConnection mockHttpEneConnection = mock(HttpENEConnection.class);
        given(endecaQueryConnection.getConection()).willReturn(mockHttpEneConnection);

        final ENEQueryResults mockQueryResults = mockQueryResultsToReturn(null, null);
        given(mockHttpEneConnection.query(mockQuery)).willReturn(mockQueryResults);

        final List<TargetProductListerData> result = targetEndecaWSFacadeImpl
                .getProductsForDealCategories(dealCategoryDimIds);

        assertThat(result).isEmpty();
    }

    @Test
    public void testGetProductsForDealCategoriesWhenBuilderThrowsException() throws Exception {
        final String dealCategoryDimId = "9389123709";

        final List<String> dealCategoryDimIds = new ArrayList<>();
        dealCategoryDimIds.add(dealCategoryDimId);

        final UrlENEQueryParseException mockUrlENEQueryParseException = mock(UrlENEQueryParseException.class);
        given(mockTargetDealProductsQueryBuilder.getQueryForFetchingAllProductsForDealCategory(dealCategoryDimId))
                .willThrow(mockUrlENEQueryParseException);

        final List<TargetProductListerData> result = targetEndecaWSFacadeImpl
                .getProductsForDealCategories(dealCategoryDimIds);

        assertThat(result).isEmpty();
    }

    private ENEQueryResults mockQueryResultsToReturn(final TargetProductListerData mockTargetProductListerData1,
            final TargetProductListerData mockTargetProductListerData2) {
        final ENEQueryResults mockQueryResults = Mockito.mock(ENEQueryResults.class);
        final Navigation mockNavigation = Mockito.mock(Navigation.class);
        BDDMockito.given(mockQueryResults.getNavigation()).willReturn(mockNavigation);

        final AggrERecList mockAggrERecList = new MAggrERecList();
        final AggrERec mockAggrERec1 = Mockito.mock(AggrERec.class);
        final AggrERec mockAggrERec2 = Mockito.mock(AggrERec.class);
        if (mockTargetProductListerData1 != null) {
            mockAggrERecList.add(mockAggrERec1);
        }
        if (mockTargetProductListerData2 != null) {
            mockAggrERecList.add(mockAggrERec2);
        }
        BDDMockito.given(aggrERecToProductConverter.convert(mockAggrERec1)).willReturn(mockTargetProductListerData1);
        BDDMockito.given(aggrERecToProductConverter.convert(mockAggrERec2)).willReturn(mockTargetProductListerData2);
        BDDMockito.given(mockNavigation.getAggrERecs()).willReturn(mockAggrERecList);
        return mockQueryResults;
    }
}
