/**
 * 
 */
package au.com.target.tgtpublicws.v2.helper;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.powermock.api.mockito.PowerMockito.mock;

import de.hybris.platform.webservicescommons.mapping.FieldSetLevelHelper;
import de.hybris.platform.webservicescommons.mapping.impl.DefaultDataMapper;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.configuration.BaseConfiguration;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Lists;

import au.com.endeca.facade.impl.TargetEndecaWSFacadeImpl;
import au.com.target.endeca.infront.cache.EndecaDimensionCacheService;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.tgtcore.deals.TargetDealService;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtpublicws.exceptions.UnknownResourceException;
import au.com.target.tgtpublicws.product.dto.TgtEndecaProductWsDTO;


/**
 * @author pthoma20
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class EndecaHelperTest {

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private TargetEndecaWSFacadeImpl targetEndecaWSFacade;

    @Mock
    private DefaultDataMapper dataMapper;

    @Mock
    private TargetDealService mockTargetDealService;

    @Mock
    private EndecaDimensionCacheService mockEndecaDimensionCacheService;

    @InjectMocks
    private final EndecaHelper endecaHelper = new EndecaHelper();

    @Test
    public void testGetProductsFromEndecaInBatchOfTwo() {

        final BaseConfiguration configuration = Mockito.mock(BaseConfiguration.class);
        BDDMockito.given(configurationService.getConfiguration()).willReturn(configuration);
        BDDMockito.given(configuration.getInteger("tgtpublicws.endeca.products.batch.size", Integer.valueOf(200)))
                .willReturn(Integer.valueOf(2));

        final List<String> productCodes = Arrays.asList("P2000", "P1000", "2300");
        final List<TgtEndecaProductWsDTO> mockedInputProducts = mockInputProducts(productCodes);


        final List<List<String>> productCodesBatchList = Lists.partition(productCodes, 2);
        final List<String> productCodeBatch1 = productCodesBatchList.get(0);
        final List<TargetProductListerData> mockedFacadeResponseForBatch1 = mockFacadeResponse(
                Arrays.asList("P2000", "P1000"));
        Mockito.when(targetEndecaWSFacade.getProductsForInstagramAndCatalogue(productCodeBatch1))
                .thenReturn(mockedFacadeResponseForBatch1);
        final List<String> productCodeBatch2 = productCodesBatchList.get(1);
        final List<TargetProductListerData> mockedFacadeResponseForBatch2 = mockFacadeResponse(Arrays.asList("2300"));
        Mockito.when(targetEndecaWSFacade.getProductsForInstagramAndCatalogue(productCodeBatch2))
                .thenReturn(mockedFacadeResponseForBatch2);


        final List<TgtEndecaProductWsDTO> resultedProducts = endecaHelper
                .getProductsForInstagramAndCatalogue(mockedInputProducts);
        Assertions.assertThat(resultedProducts).isNotEmpty();

        for (final TgtEndecaProductWsDTO resultedProduct : resultedProducts) {
            Assertions.assertThat(productCodes).contains(resultedProduct.getCode());
            Assertions.assertThat(resultedProduct.getCode()).isEqualTo(resultedProduct.getRequestedCode());
        }

    }


    @Test
    public void testGetProductsFromEndecaInASingleBatch() {

        final BaseConfiguration configuration = Mockito.mock(BaseConfiguration.class);
        BDDMockito.given(configurationService.getConfiguration()).willReturn(configuration);
        BDDMockito.given(configuration.getInteger("tgtpublicws.endeca.products.batch.size", Integer.valueOf(200)))
                .willReturn(Integer.valueOf(10));

        final List<String> productCodes = Arrays.asList("P2000", "P1000", "2300");
        final List<TgtEndecaProductWsDTO> mockedInputProducts = mockInputProducts(productCodes);


        final List<List<String>> productCodesBatchList = Lists.partition(productCodes, 10);
        final List<String> productCodeBatch1 = productCodesBatchList.get(0);
        final List<TargetProductListerData> mockedFacadeResponseForBatch1 = mockFacadeResponse(
                Arrays.asList("P2000", "P1000", "2300"));
        Mockito.when(targetEndecaWSFacade.getProductsForInstagramAndCatalogue(productCodeBatch1))
                .thenReturn(mockedFacadeResponseForBatch1);

        final List<TgtEndecaProductWsDTO> resultedProducts = endecaHelper
                .getProductsForInstagramAndCatalogue(mockedInputProducts);
        Assertions.assertThat(resultedProducts).isNotEmpty();

        for (final TgtEndecaProductWsDTO resultedProduct : resultedProducts) {
            Assertions.assertThat(productCodes).contains(resultedProduct.getCode());
            Assertions.assertThat(resultedProduct.getCode()).isEqualTo(resultedProduct.getRequestedCode());
        }

    }

    @Test(expected = UnknownResourceException.class)
    public void testGetProductsForDealWithNoDeal() {
        final String dealId = "1234";
        endecaHelper.getProductsForDeal(dealId);
    }

    @Test(expected = UnknownResourceException.class)
    public void testGetProductsForDealForIncorrectDealType() {
        final String dealId = "1234";

        final AbstractDealModel mockDeal = mock(AbstractDealModel.class);
        given(mockTargetDealService.getDealForId(dealId)).willReturn(mockDeal);

        endecaHelper.getProductsForDeal(dealId);
    }

    @Test
    public void testGetProductsForDeal() throws Exception {
        final String dealId = "1234";

        final String qualifierCategoryCode = "d1234q1cat1";
        final String rewardCategoryCode = "d1234r1cat1";

        final String qualifierCategoryDimensionId = "12345678";
        final String rewardCategoryDimensionId = "87654321";

        final TargetDealCategoryModel mockQualifierCategory = mock(TargetDealCategoryModel.class);
        given(mockQualifierCategory.getCode()).willReturn(qualifierCategoryCode);

        final List<TargetDealCategoryModel> qualifierCategories = new ArrayList<>();
        qualifierCategories.add(mockQualifierCategory);

        final DealQualifierModel mockQualifier = mock(DealQualifierModel.class);
        given(mockQualifier.getDealCategory()).willReturn(qualifierCategories);

        final List<DealQualifierModel> dealQualifiers = new ArrayList<>();
        dealQualifiers.add(mockQualifier);

        final TargetDealCategoryModel mockRewardCategory = mock(TargetDealCategoryModel.class);
        given(mockRewardCategory.getCode()).willReturn(rewardCategoryCode);

        final List<TargetDealCategoryModel> rewardCategories = new ArrayList<>();
        rewardCategories.add(mockRewardCategory);

        given(mockEndecaDimensionCacheService.getDimension(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_QUALIFIER_CATEGORY, qualifierCategoryCode))
                        .willReturn(qualifierCategoryDimensionId);
        given(mockEndecaDimensionCacheService.getDimension(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_REWARD_CATEGORY, rewardCategoryCode))
                        .willReturn(rewardCategoryDimensionId);

        final AbstractSimpleDealModel mockDeal = mock(AbstractSimpleDealModel.class);
        given(mockTargetDealService.getDealForId(dealId)).willReturn(mockDeal);
        given(mockDeal.getQualifierList()).willReturn(dealQualifiers);
        given(mockDeal.getRewardCategory()).willReturn(rewardCategories);

        final TargetProductListerData product1 = mock(TargetProductListerData.class);
        final TargetProductListerData product2 = mock(TargetProductListerData.class);

        final List<TargetProductListerData> endecaProducts = new ArrayList<>();
        Collections.addAll(endecaProducts, product1, product2);

        final ArgumentCaptor<List> dealCategoryDimIdsCaptor = ArgumentCaptor.forClass(List.class);
        given(targetEndecaWSFacade.getProductsForDealCategories(dealCategoryDimIdsCaptor.capture()))
                .willReturn(endecaProducts);

        final TgtEndecaProductWsDTO mockMappedEndecaProduct1 = mock(TgtEndecaProductWsDTO.class);
        final TgtEndecaProductWsDTO mockMappedEndecaProduct2 = mock(TgtEndecaProductWsDTO.class);

        given(dataMapper.map(product1, TgtEndecaProductWsDTO.class, FieldSetLevelHelper.DEFAULT_LEVEL))
                .willReturn(mockMappedEndecaProduct1);
        given(dataMapper.map(product2, TgtEndecaProductWsDTO.class, FieldSetLevelHelper.DEFAULT_LEVEL))
                .willReturn(mockMappedEndecaProduct2);

        final List<TgtEndecaProductWsDTO> result = endecaHelper.getProductsForDeal(dealId);

        assertThat(result).hasSize(2).containsExactly(mockMappedEndecaProduct1, mockMappedEndecaProduct2);

        final List<String> capturedDealCategoryDimIds = dealCategoryDimIdsCaptor.getValue();
        assertThat(capturedDealCategoryDimIds).hasSize(2).containsExactly(qualifierCategoryDimensionId,
                rewardCategoryDimensionId);
    }

    @Test
    public void testGetProductsForDealWithNoQualifiers() throws Exception {
        final String dealId = "1234";

        final String rewardCategoryCode = "d1234r1cat1";

        final String rewardCategoryDimensionId = "87654321";

        final TargetDealCategoryModel mockRewardCategory = mock(TargetDealCategoryModel.class);
        given(mockRewardCategory.getCode()).willReturn(rewardCategoryCode);

        final List<TargetDealCategoryModel> rewardCategories = new ArrayList<>();
        rewardCategories.add(mockRewardCategory);

        given(mockEndecaDimensionCacheService.getDimension(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_REWARD_CATEGORY, rewardCategoryCode))
                        .willReturn(rewardCategoryDimensionId);

        final AbstractSimpleDealModel mockDeal = mock(AbstractSimpleDealModel.class);
        given(mockTargetDealService.getDealForId(dealId)).willReturn(mockDeal);
        given(mockDeal.getRewardCategory()).willReturn(rewardCategories);

        final List<TgtEndecaProductWsDTO> result = endecaHelper.getProductsForDeal(dealId);

        assertThat(result).isNull();
    }

    @Test
    public void testGetProductsForDealWithNoRewards() throws Exception {
        final String dealId = "1234";

        final String qualifierCategoryCode = "d1234q1cat1";

        final String qualifierCategoryDimensionId = "12345678";

        final TargetDealCategoryModel mockQualifierCategory = mock(TargetDealCategoryModel.class);
        given(mockQualifierCategory.getCode()).willReturn(qualifierCategoryCode);

        final List<TargetDealCategoryModel> qualifierCategories = new ArrayList<>();
        qualifierCategories.add(mockQualifierCategory);

        final DealQualifierModel mockQualifier = mock(DealQualifierModel.class);
        given(mockQualifier.getDealCategory()).willReturn(qualifierCategories);

        final List<DealQualifierModel> dealQualifiers = new ArrayList<>();
        dealQualifiers.add(mockQualifier);

        given(mockEndecaDimensionCacheService.getDimension(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_QUALIFIER_CATEGORY, qualifierCategoryCode))
                        .willReturn(qualifierCategoryDimensionId);

        final AbstractSimpleDealModel mockDeal = mock(AbstractSimpleDealModel.class);
        given(mockTargetDealService.getDealForId(dealId)).willReturn(mockDeal);
        given(mockDeal.getQualifierList()).willReturn(dealQualifiers);

        final TargetProductListerData product1 = mock(TargetProductListerData.class);

        final List<TargetProductListerData> endecaProducts = new ArrayList<>();
        Collections.addAll(endecaProducts, product1);

        final ArgumentCaptor<List> dealCategoryDimIdsCaptor = ArgumentCaptor.forClass(List.class);
        given(targetEndecaWSFacade.getProductsForDealCategories(dealCategoryDimIdsCaptor.capture()))
                .willReturn(endecaProducts);

        final TgtEndecaProductWsDTO mockMappedEndecaProduct1 = mock(TgtEndecaProductWsDTO.class);

        given(dataMapper.map(product1, TgtEndecaProductWsDTO.class, FieldSetLevelHelper.DEFAULT_LEVEL))
                .willReturn(mockMappedEndecaProduct1);

        final List<TgtEndecaProductWsDTO> result = endecaHelper.getProductsForDeal(dealId);

        assertThat(result).hasSize(1).containsExactly(mockMappedEndecaProduct1);

        final List<String> capturedDealCategoryDimIds = dealCategoryDimIdsCaptor.getValue();
        assertThat(capturedDealCategoryDimIds).hasSize(1).containsExactly(qualifierCategoryDimensionId);
    }

    @Test
    public void testGetProductsForDealWithNoQualifiersOrRewards() throws Exception {
        final String dealId = "1234";

        final AbstractSimpleDealModel mockDeal = mock(AbstractSimpleDealModel.class);
        given(mockTargetDealService.getDealForId(dealId)).willReturn(mockDeal);

        final List<TgtEndecaProductWsDTO> result = endecaHelper.getProductsForDeal(dealId);

        assertThat(result).isNull();
        verifyZeroInteractions(targetEndecaWSFacade, dataMapper, mockEndecaDimensionCacheService);
    }

    @Test
    public void testGetProductsForDealWithNoProductsFromEndeca() throws Exception {
        final String dealId = "1234";

        final String qualifierCategoryCode = "d1234q1cat1";
        final String rewardCategoryCode = "d1234r1cat1";

        final String qualifierCategoryDimensionId = "12345678";
        final String rewardCategoryDimensionId = "87654321";

        final TargetDealCategoryModel mockQualifierCategory = mock(TargetDealCategoryModel.class);
        given(mockQualifierCategory.getCode()).willReturn(qualifierCategoryCode);

        final List<TargetDealCategoryModel> qualifierCategories = new ArrayList<>();
        qualifierCategories.add(mockQualifierCategory);

        final DealQualifierModel mockQualifier = mock(DealQualifierModel.class);
        given(mockQualifier.getDealCategory()).willReturn(qualifierCategories);

        final List<DealQualifierModel> dealQualifiers = new ArrayList<>();
        dealQualifiers.add(mockQualifier);

        final TargetDealCategoryModel mockRewardCategory = mock(TargetDealCategoryModel.class);
        given(mockRewardCategory.getCode()).willReturn(rewardCategoryCode);

        final List<TargetDealCategoryModel> rewardCategories = new ArrayList<>();
        rewardCategories.add(mockRewardCategory);

        given(mockEndecaDimensionCacheService.getDimension(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_QUALIFIER_CATEGORY, qualifierCategoryCode))
                        .willReturn(qualifierCategoryDimensionId);
        given(mockEndecaDimensionCacheService.getDimension(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_REWARD_CATEGORY, rewardCategoryCode))
                        .willReturn(rewardCategoryDimensionId);

        final AbstractSimpleDealModel mockDeal = mock(AbstractSimpleDealModel.class);
        given(mockTargetDealService.getDealForId(dealId)).willReturn(mockDeal);
        given(mockDeal.getQualifierList()).willReturn(dealQualifiers);
        given(mockDeal.getRewardCategory()).willReturn(rewardCategories);

        final ArgumentCaptor<List> dealCategoryDimIdsCaptor = ArgumentCaptor.forClass(List.class);
        given(targetEndecaWSFacade.getProductsForDealCategories(dealCategoryDimIdsCaptor.capture()))
                .willReturn(null);

        final List<TgtEndecaProductWsDTO> result = endecaHelper.getProductsForDeal(dealId);

        assertThat(result).isNull();

        final List<String> capturedDealCategoryDimIds = dealCategoryDimIdsCaptor.getValue();
        assertThat(capturedDealCategoryDimIds).hasSize(2).containsExactly(qualifierCategoryDimensionId,
                rewardCategoryDimensionId);

        verifyZeroInteractions(dataMapper);
    }

    private List<TargetProductListerData> mockFacadeResponse(final List<String> codes) {
        final List<TargetProductListerData> targetProductListFromFacadeForBatch = new ArrayList<>();

        for (final String code : codes) {
            final TargetProductListerData targetProductListerData = new TargetProductListerData();
            targetProductListerData.setCode(code);
            targetProductListFromFacadeForBatch.add(targetProductListerData);
            final TgtEndecaProductWsDTO targetEndecaProductWSDTO = mockMapperOutPut(code);
            Mockito.when(dataMapper.map(targetProductListerData, TgtEndecaProductWsDTO.class,
                    FieldSetLevelHelper.DEFAULT_LEVEL)).thenReturn(targetEndecaProductWSDTO);
        }
        return targetProductListFromFacadeForBatch;

    }

    private TgtEndecaProductWsDTO mockMapperOutPut(final String code) {
        final TgtEndecaProductWsDTO targetEndecaProductWSDTO = new TgtEndecaProductWsDTO();
        targetEndecaProductWSDTO.setCode(code);
        return targetEndecaProductWSDTO;
    }


    private List<TgtEndecaProductWsDTO> mockInputProducts(final List<String> codes) {
        final List<TgtEndecaProductWsDTO> targetProductList = new ArrayList<>();
        for (final String code : codes) {
            final TgtEndecaProductWsDTO targetEndecaProductWSDTO = new TgtEndecaProductWsDTO();
            targetEndecaProductWSDTO.setRequestedCode(code);
            targetProductList.add(targetEndecaProductWSDTO);
        }
        return targetProductList;
    }

}
