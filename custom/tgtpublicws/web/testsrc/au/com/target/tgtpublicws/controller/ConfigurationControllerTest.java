/**
 * 
 */
package au.com.target.tgtpublicws.controller;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.webservicescommons.mapping.DataMapper;
import de.hybris.platform.webservicescommons.mapping.FieldSetLevelHelper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.payment.AfterpayConfigFacade;
import au.com.target.tgtfacades.payment.data.AfterpayConfigData;
import au.com.target.tgtpublicws.exceptions.UnknownResourceException;
import au.com.target.tgtpublicws.tgtpayment.dto.AfterpayConfigurationWsDTO;
import au.com.target.tgtpublicws.v2.controller.ConfigurationController;
import au.com.target.tgtwebcore.constants.TgtwebcoreConstants;
import au.com.target.tgtwebcore.servicelayer.services.TargetCMSPageService;


/**
 * @author niannel1
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ConfigurationControllerTest {
    protected static final String DEFAULT_FIELD_SET = FieldSetLevelHelper.DEFAULT_LEVEL;

    @InjectMocks
    @Spy
    private final ConfigurationController configurationController = new ConfigurationController();

    @Mock
    private TargetFeatureSwitchFacade mockTargetFeatureSwitchFacade;

    @Mock
    private TargetCMSPageService mockCmsPageService;

    @Mock
    private AfterpayConfigFacade afterpayConfigFacade;

    @Mock
    private DataMapper dataMapper;


    @Test(expected = UnknownResourceException.class)
    public void testGetAfterpayConfigurationFeatureOff() {
        given(Boolean.valueOf(mockTargetFeatureSwitchFacade.isAfterpayEnabled())).willReturn(Boolean.FALSE);
        configurationController.getAfterpayConfiguration();
    }

    @Test
    public void testGetAfterpayConfigurationFeatureOn() throws CMSItemNotFoundException {
        final AfterpayConfigurationWsDTO afterpayWsDTO = mock(AfterpayConfigurationWsDTO.class);
        final AfterpayConfigData afterpayConfig = mock(AfterpayConfigData.class);
        final ContentPageModel cmsPage = mock(ContentPageModel.class);
        given(Boolean.valueOf(mockTargetFeatureSwitchFacade.isAfterpayEnabled())).willReturn(Boolean.TRUE);
        given(afterpayConfigFacade.getAfterpayConfig()).willReturn(afterpayConfig);
        given(dataMapper.map(afterpayConfig, AfterpayConfigurationWsDTO.class, DEFAULT_FIELD_SET))
                .willReturn(afterpayWsDTO);
        given(mockCmsPageService.getPageForLabelOrId(TgtwebcoreConstants.AFTERPAY_MODAL_ID)).willReturn(cmsPage);
        assertThat(configurationController.getAfterpayConfiguration()).isEqualTo(afterpayWsDTO);
    }

    @Test
    public void testGetAfterpayConfigurationPopulatesInfoLink() throws CMSItemNotFoundException {
        final AfterpayConfigurationWsDTO afterpayWsDTO = mock(AfterpayConfigurationWsDTO.class);
        final AfterpayConfigData afterpayConfig = mock(AfterpayConfigData.class);
        final ContentPageModel cmsPage = mock(ContentPageModel.class);
        given(Boolean.valueOf(mockTargetFeatureSwitchFacade.isAfterpayEnabled())).willReturn(Boolean.TRUE);
        given(afterpayConfigFacade.getAfterpayConfig()).willReturn(afterpayConfig);
        given(dataMapper.map(afterpayConfig, AfterpayConfigurationWsDTO.class, DEFAULT_FIELD_SET))
                .willReturn(afterpayWsDTO);
        given(cmsPage.getLabel()).willReturn("/modal/afterpay");
        given(cmsPage.getName()).willReturn("Afterpay");
        given(mockCmsPageService.getPageForLabelOrId(TgtwebcoreConstants.AFTERPAY_MODAL_ID)).willReturn(cmsPage);
        assert (configurationController.getAfterpayConfiguration().getHelp().getName()).equals("Afterpay");
        assert (configurationController.getAfterpayConfiguration().getHelp().getName()).equals("/modal/afterpay");
    }

    @Test
    public void testGetAfterpayConfigurationModalDoesntExist() throws CMSItemNotFoundException {
        final CMSItemNotFoundException ex = new CMSItemNotFoundException("");
        final AfterpayConfigurationWsDTO afterpayWsDTO = mock(AfterpayConfigurationWsDTO.class);
        final AfterpayConfigData afterpayConfig = mock(AfterpayConfigData.class);
        given(Boolean.valueOf(mockTargetFeatureSwitchFacade.isAfterpayEnabled())).willReturn(Boolean.TRUE);
        given(afterpayConfigFacade.getAfterpayConfig()).willReturn(afterpayConfig);
        given(dataMapper.map(afterpayConfig, AfterpayConfigurationWsDTO.class, DEFAULT_FIELD_SET))
                .willReturn(afterpayWsDTO);
        given(mockCmsPageService.getPageForLabelOrId(TgtwebcoreConstants.AFTERPAY_MODAL_ID)).willThrow(ex);
        assert (configurationController.getAfterpayConfiguration().getHelp().getName()).equals("");
        assert (configurationController.getAfterpayConfiguration().getHelp().getName()).equals("");
    }
}
