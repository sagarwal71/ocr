package au.com.target.tgtpublicws.mapping.converters;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.commercefacades.product.data.PriceData;

import org.junit.Test;


public class DisplayPriceConverterTest {

    private final DisplayPriceConverter displayPriceConverter = new DisplayPriceConverter();

    @Test
    public void testConvert() throws Exception {
        final PriceData priceData = new PriceData();
        priceData.setFormattedValue("$15.00");

        final String result = displayPriceConverter.convert(priceData, null, null);

        assertThat(result).isEqualTo("$15");
    }

}
