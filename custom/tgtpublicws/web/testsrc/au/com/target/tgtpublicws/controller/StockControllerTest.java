/**
 * 
 */
package au.com.target.tgtpublicws.controller;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.webservicescommons.mapping.DataMapper;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.stock.TargetStockLookUpFacade;
import au.com.target.tgtpublicws.stock.dto.StockDataListWsDTO;
import au.com.target.tgtpublicws.stock.dto.StockDataWsDTO;
import au.com.target.tgtpublicws.v2.controller.StockController;
import au.com.target.tgtwebcore.fluent.StockRequestDto;


/**
 * Mocking ProductsContorller for mobile devices.
 * 
 * @author bmcmuffin
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StockControllerTest {

    @InjectMocks
    @Spy
    private final StockController stockController = new StockController();

    @Mock
    private TargetStockLookUpFacade targetStockLookUpFacade;

    @Mock
    private DataMapper dataMapper;

    @Test
    public void testStockData() {
        final StockRequestDto request = new StockRequestDto();

        request.setVariantCode("P1000");

        final Map<String, Boolean> stockMap = new LinkedHashMap();

        stockMap.put("P1000_black_S", Boolean.TRUE);
        stockMap.put("P1000_black_M", Boolean.FALSE);
        stockMap.put("P1000_black_L", Boolean.TRUE);

        given(targetStockLookUpFacade.lookupStockOnline("P1000")).willReturn(stockMap);

        final StockDataListWsDTO resultData = stockController.getStockData(request);
        assertThat(resultData.getStockData().size()).isEqualTo(Integer.valueOf(3));
        final List<StockDataWsDTO> resultList = resultData.getStockData();
        assertThat(resultList).onProperty("variantCode").containsExactly("P1000_black_S", "P1000_black_M",
                "P1000_black_L");
        assertThat(resultList).onProperty("ats").containsExactly(Boolean.TRUE, Boolean.FALSE,
                Boolean.TRUE);
    }

    @Test
    public void testStockDataEmpty() {
        final StockRequestDto request = new StockRequestDto();

        request.setVariantCode("P1000");
        given(targetStockLookUpFacade.lookupStockOnline("P1000")).willReturn(null);

        final StockDataListWsDTO resultData = stockController.getStockData(request);
        assertThat(resultData.getStockData()).isEmpty();
    }
}
