/**
 * 
 */
package au.com.target.tgtpublicws.v2.controller;


import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.payment.AfterpayConfigFacade;
import au.com.target.tgtpublicws.exceptions.UnknownResourceException;
import au.com.target.tgtpublicws.navigation.dto.InfoLinkWsDTO;
import au.com.target.tgtpublicws.tgtpayment.dto.AfterpayConfigurationWsDTO;
import au.com.target.tgtwebcore.constants.TgtwebcoreConstants;
import au.com.target.tgtwebcore.servicelayer.services.TargetCMSPageService;


/**
 * @author niannel1
 *
 */
@Controller
@RequestMapping(value = "/{baseSiteId}/configuration")
public class ConfigurationController extends BaseController {
    private static final Logger LOG = Logger.getLogger(ConfigurationController.class);

    @Resource(name = "afterpayConfigFacade")
    private AfterpayConfigFacade afterpayConfigFacade;

    @Resource(name = "cmsPageService")
    private TargetCMSPageService cmsPageService;

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    /**
     * Method gets the current status of the afterpay configuration
     */
    @RequestMapping(value = "/afterpay", produces = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseBody
    public AfterpayConfigurationWsDTO getAfterpayConfiguration() {

        if (!targetFeatureSwitchFacade.isAfterpayEnabled()) {
            throw new UnknownResourceException("Afterpay Configuration Not Enabled");
        }

        final AfterpayConfigurationWsDTO dto = dataMapper.map(afterpayConfigFacade.getAfterpayConfig(),
                AfterpayConfigurationWsDTO.class, DEFAULT_FIELD_SET);

        populateAdditionalAfterpayProperties(dto);
        return dto;
    }

    private void populateAdditionalAfterpayProperties(final AfterpayConfigurationWsDTO target) {
        final InfoLinkWsDTO infoLink = new InfoLinkWsDTO();
        try {
            final ContentPageModel page = cmsPageService.getPageForLabelOrId(TgtwebcoreConstants.AFTERPAY_MODAL_ID);
            infoLink.setUrl(page.getLabel());
            infoLink.setName(page.getName());
        }
        catch (final CMSItemNotFoundException e) {
            LOG.error("Afterpay modal not found");
            infoLink.setUrl("");
            infoLink.setName("");
        }
        target.setHelp(infoLink);
    }
}
