package au.com.target.tgtpublicws.exceptions;

import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceException;


/**
 * 
 */
public class GiftCardValidationWSException extends WebserviceException {

    private static final String TYPE = "GiftCardValidationError";

    public GiftCardValidationWSException(final String message, final String reason,
            final Throwable cause) {
        super(message, reason, cause);
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public String getSubjectType() {
        return TYPE;
    }
}
