package au.com.target.tgtpublicws.exceptions;

import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceException;


/**
 * 
 */
public class ProductException extends WebserviceException
{
    public static final String NOT_FOUND = "notFound";
    private static final String TYPE = "ProductError";
    private static final String SUBJECT_TYPE = "product";

    public ProductException(final String message)
    {
        super(message);
    }

    public ProductException(final String message, final String reason)
    {
        super(message, reason);
    }

    public ProductException(final String message, final String reason, final Throwable cause)
    {
        super(message, reason, cause);
    }

    public ProductException(final String message, final String reason, final String subject)
    {
        super(message, reason, subject);
    }

    public ProductException(final String message, final String reason, final String subject, final Throwable cause)
    {
        super(message, reason, subject, cause);
    }

    @Override
    public String getType()
    {
        return TYPE;
    }

    @Override
    public String getSubjectType()
    {
        return SUBJECT_TYPE;
    }
}
