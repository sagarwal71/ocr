/**
 * 
 */
package au.com.target.tgtpublicws.mapping.converters;

import de.hybris.platform.webservicescommons.mapping.WsDTOMapping;

import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.metadata.Type;


/**
 * @author rmcalave
 *
 */
@WsDTOMapping
public class StockLevelMapConverter extends CustomConverter<Map<String, Boolean>, Boolean> {

    /* (non-Javadoc)
     * @see ma.glasnost.orika.Converter#convert(java.lang.Object, ma.glasnost.orika.metadata.Type)
     */
    @Override
    public Boolean convert(final Map<String, Boolean> source, final Type<? extends Boolean> destinationType,
            final MappingContext mappingContext) {
        if (CollectionUtils.isEmpty(source.values())) {
            return Boolean.FALSE;
        }

        return Boolean.valueOf(source.values().contains(Boolean.TRUE));
    }

}
