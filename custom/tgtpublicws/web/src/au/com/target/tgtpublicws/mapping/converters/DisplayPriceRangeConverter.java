/**
 * 
 */
package au.com.target.tgtpublicws.mapping.converters;

import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.webservicescommons.mapping.WsDTOMapping;

import au.com.target.tgtfacades.util.PriceFormatUtils;
import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.metadata.Type;


/**
 * @author pthoma20
 *
 */
@WsDTOMapping
public class DisplayPriceRangeConverter extends CustomConverter<PriceRangeData, String> {

    /* (non-Javadoc)
     * @see ma.glasnost.orika.Converter#convert(java.lang.Object, ma.glasnost.orika.metadata.Type)
     */
    @Override
    public String convert(final PriceRangeData source, final Type<? extends String> destinationType,
            final MappingContext mappingContext) {
        return PriceFormatUtils.formatPrice(source);
    }
}
