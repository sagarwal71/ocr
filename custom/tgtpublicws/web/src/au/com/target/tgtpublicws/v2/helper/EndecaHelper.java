/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 */
package au.com.target.tgtpublicws.v2.helper;

import de.hybris.platform.webservicescommons.mapping.FieldSetLevelHelper;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.endeca.navigation.ENEQueryException;
import com.google.common.collect.Lists;

import au.com.endeca.facade.impl.TargetEndecaWSFacadeImpl;
import au.com.target.endeca.infront.cache.EndecaDimensionCacheService;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.tgtcore.deals.TargetDealService;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtpublicws.exceptions.UnknownResourceException;
import au.com.target.tgtpublicws.product.dto.TgtEndecaProductWsDTO;


@Component
public class EndecaHelper extends AbstractHelper {

    private static final Logger LOG = Logger.getLogger(EndecaHelper.class);

    @Resource(name = "targetEndecaWSFacade")
    private TargetEndecaWSFacadeImpl targetEndecaWSFacade;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Resource(name = "targetDealService")
    private TargetDealService targetDealService;

    @Resource(name = "endecaDimensionCacheService")
    private EndecaDimensionCacheService endecaDimensionCacheService;

    /**
     * This is a helper class for finding the product codes in the request and making a call to get the data from
     * Endeca. After getting the products back, this method adds the requested code to the object.
     * 
     * @param endecaProductWsDTO
     * @return endecaProductWsDTO
     */
    public List<TgtEndecaProductWsDTO> getProductsForInstagramAndCatalogue(
            List<TgtEndecaProductWsDTO> endecaProductWsDTO) {
        final List<String> productCodes = extractProductCodes(endecaProductWsDTO);
        endecaProductWsDTO = new ArrayList<>();
        final int batchSize = getNumberOfProductsSentToEndecaAtOnce().intValue();
        final List<List<String>> productCodesBatchList = Lists.partition(productCodes, batchSize);
        final List<TargetProductListerData> targetEndecaProducts = new ArrayList<>();
        int count = 1;
        for (final List<String> productCodesList : productCodesBatchList) {
            LOG.info("TGT-ENDECA: going to Endeca For batch" + count);
            final List<TargetProductListerData> productsRetrieved = targetEndecaWSFacade
                    .getProductsForInstagramAndCatalogue(productCodesList);
            targetEndecaProducts.addAll(productsRetrieved);
            count++;
        }

        for (final TargetProductListerData productListerData : targetEndecaProducts) {
            final TgtEndecaProductWsDTO endecaProduct = dataMapper.map(productListerData, TgtEndecaProductWsDTO.class,
                    FieldSetLevelHelper.DEFAULT_LEVEL);
            endecaProduct.setRequestedCode(findRequestedCode(productListerData, productCodes));
            endecaProductWsDTO.add(endecaProduct);
        }
        return endecaProductWsDTO;
    }

    /**
     * Get a list of the products contained in the supplied deal. This is a single list of all the products in every
     * qualifier and reward category in the deal.
     * 
     * @param dealId
     *            The deal ID
     * @return a list of products
     */
    public List<TgtEndecaProductWsDTO> getProductsForDeal(final String dealId) {
        final AbstractSimpleDealModel simpleDeal = getDealForId(dealId);

        if (simpleDeal == null) {
            throw new UnknownResourceException("No deal found for code " + dealId);
        }

        final List<String> dealCategoryDimIds = new ArrayList<>();
        final List<DealQualifierModel> qualifiers = simpleDeal.getQualifierList();
        if (qualifiers.isEmpty()) {
            return null;
        }

        for (final DealQualifierModel qualifier : qualifiers) {
            final List<TargetDealCategoryModel> qualifierCategories = qualifier.getDealCategory();
            dealCategoryDimIds
                    .addAll(getDimIdsForDealCategories(qualifierCategories,
                            EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_QUALIFIER_CATEGORY));
        }

        final List<TargetDealCategoryModel> rewardCategories = simpleDeal.getRewardCategory();
        if (!rewardCategories.isEmpty()) {
            dealCategoryDimIds.addAll(getDimIdsForDealCategories(rewardCategories,
                    EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_REWARD_CATEGORY));
        }

        if (dealCategoryDimIds.isEmpty()) {
            return null;
        }

        final List<TargetProductListerData> targetEndecaProducts = targetEndecaWSFacade
                .getProductsForDealCategories(dealCategoryDimIds);

        if (CollectionUtils.isEmpty(targetEndecaProducts)) {
            return null;
        }

        final List<TgtEndecaProductWsDTO> endecaProductWsDTO = new ArrayList<>();
        for (final TargetProductListerData productListerData : targetEndecaProducts) {
            final TgtEndecaProductWsDTO endecaProduct = dataMapper.map(productListerData, TgtEndecaProductWsDTO.class,
                    FieldSetLevelHelper.DEFAULT_LEVEL);
            endecaProductWsDTO.add(endecaProduct);
        }
        return endecaProductWsDTO;
    }

    /**
     * This is to set the requested product code in the response so that the caller knows which data was requested for.
     * 
     * @param targetProduct
     * @param productCodes
     * @return requestCode
     */
    private String findRequestedCode(final TargetProductListerData targetProduct, final List<String> productCodes) {
        String requestCode = "";
        if (targetProduct.getCode() != null
                && productCodes.contains(targetProduct.getCode())) {
            requestCode = targetProduct.getCode();
        }
        else if (null != targetProduct.getTargetVariantProductListerData()) {
            for (final TargetVariantProductListerData targetVariantProductListerData : targetProduct
                    .getTargetVariantProductListerData()) {
                if (targetVariantProductListerData.getColourVariantCode() != null &&
                        productCodes.contains(targetVariantProductListerData.getColourVariantCode().toLowerCase())) {
                    requestCode = targetVariantProductListerData.getColourVariantCode();
                }
            }
        }
        return requestCode;
    }

    private List<String> extractProductCodes(final List<TgtEndecaProductWsDTO> endecaProductWsDTO) {
        final List<String> productCodes = new ArrayList<>();
        for (final TgtEndecaProductWsDTO product : endecaProductWsDTO) {
            productCodes.add(product.getRequestedCode());
        }
        return productCodes;
    }

    private Integer getNumberOfProductsSentToEndecaAtOnce() {
        return configurationService.getConfiguration()
                .getInteger("tgtpublicws.endeca.products.batch.size", Integer.valueOf(200));

    }

    private AbstractSimpleDealModel getDealForId(final String dealId) {
        AbstractDealModel deal = null;
        try {
            deal = targetDealService.getDealForId(dealId);
        }
        catch (final UnknownIdentifierException ex) {
            LOG.info("No deal found for dealId=" + dealId);
        }
        catch (final AmbiguousIdentifierException ex) {
            LOG.error("More than one deal found for dealId=" + dealId);
        }

        if (!(deal instanceof AbstractSimpleDealModel)) {
            LOG.info("Deal with dealId=" + dealId + " is not of type " + AbstractSimpleDealModel.class);
            return null;
        }

        return (AbstractSimpleDealModel)deal;
    }

    private List<String> getDimIdsForDealCategories(final List<TargetDealCategoryModel> dealCategories,
            final String dimensionName) {
        final List<String> dealCategoryDimIds = new ArrayList<>();

        for (final TargetDealCategoryModel dealCategory : dealCategories) {
            try {
                dealCategoryDimIds.add(endecaDimensionCacheService.getDimension(dimensionName, dealCategory.getCode()));
            }
            catch (final ENEQueryException e) {
                LOG.error("TGT-ENDECA-ERROR: Error while querying Endeca for dimension", e);
            }
            catch (final TargetEndecaException e) {
                LOG.error("TGT-ENDECA-ERROR: Error while getting connection to Endeca for dimension", e);
            }
        }

        return dealCategoryDimIds;
    }

}
