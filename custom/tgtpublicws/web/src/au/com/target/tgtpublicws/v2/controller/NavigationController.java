/**
 * 
 */
package au.com.target.tgtpublicws.v2.controller;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.servicelayer.services.CMSNavigationService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.navigation.NavigationMenuItem;
import au.com.target.tgtpublicws.navigation.HomeScreenLayoutBuilder;
import au.com.target.tgtpublicws.navigation.MobileMenuBuilder;
import au.com.target.tgtpublicws.navigation.dto.HomeScreenLayoutDTO;
import au.com.target.tgtpublicws.navigation.dto.HomeScreenLayoutWsDTO;
import au.com.target.tgtpublicws.navigation.dto.TargetNavigationWsDTO;


/**
 * @author rmcalave
 *
 */
@Controller
@RequestMapping(value = "/{baseSiteId}/navigation")
public class NavigationController extends BaseController {

    private static final Logger LOG = Logger.getLogger(NavigationController.class);
    private static final String CATEGORY_ROOT_NODE_ID = "TargetSiteCategoriesNavNode";
    private static final String HOME_SCREEN_ROOT_NODE_ID = "MobileAppHomeScreenNavNode";

    @Autowired
    private CMSNavigationService cmsNavigationService;

    @Autowired
    private MobileMenuBuilder mobileMenuBuilder;

    @Autowired
    private HomeScreenLayoutBuilder homeScreenLayoutBuilder;

    @Value("#{configurationService.configuration.getString('tgtstorefront.host.fe.fqdn')}")
    private String fqdn;

    /**
     * Returns the navigation structure of the site.
     * 
     * @return The root node containing the whole navigation structure.
     */
    @RequestMapping(method = RequestMethod.GET)
    @Cacheable(value = "navigationCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'navigation')")
    @ResponseBody
    public TargetNavigationWsDTO getNavigation() {
        CMSNavigationNodeModel navigationNode = null;
        try {
            navigationNode = cmsNavigationService.getNavigationNodeForId(CATEGORY_ROOT_NODE_ID);
        }
        catch (final CMSItemNotFoundException ex) {
            LOG.error(CATEGORY_ROOT_NODE_ID + " not found");
            return null;
        }

        final NavigationMenuItem topLevel = mobileMenuBuilder.createMenuItem(navigationNode);

        if (topLevel == null) {
            return new TargetNavigationWsDTO();
        }

        final TargetNavigationWsDTO dto = dataMapper.map(topLevel, TargetNavigationWsDTO.class, DEFAULT_FIELD_SET);

        if (dto != null) {
            dto.setFqdn(fqdn);
        }

        return dto;
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    @Cacheable(value = "navigationCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'home-screen')")
    @ResponseBody
    public HomeScreenLayoutWsDTO getHomeScreen() {
        CMSNavigationNodeModel navigationNode = null;
        try {
            navigationNode = cmsNavigationService.getNavigationNodeForId(HOME_SCREEN_ROOT_NODE_ID);
        }
        catch (final CMSItemNotFoundException ex) {
            LOG.error(CATEGORY_ROOT_NODE_ID + " not found");
            return null;
        }

        final HomeScreenLayoutDTO homeScreenLayout = homeScreenLayoutBuilder.buildHomeScreenLayout(navigationNode);

        if (homeScreenLayout == null) {
            return new HomeScreenLayoutWsDTO();
        }

        final HomeScreenLayoutWsDTO dto = dataMapper.map(homeScreenLayout, HomeScreenLayoutWsDTO.class,
                DEFAULT_FIELD_SET);

        if (dto != null) {
            dto.setFqdn(fqdn);
        }

        return dto;
    }
}
