/**
 * 
 */
package au.com.target.tgtpublicws.jaxb;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;


public interface JaxbContextFactory
{
    JAXBContext createJaxbContext(Class... classes) throws JAXBException;
}
