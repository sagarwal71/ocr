/**
 * 
 */
package au.com.target.tgtpublicws.mapping.converters;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.webservicescommons.mapping.WsDTOMapping;

import au.com.target.tgtfacades.util.PriceFormatUtils;
import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.metadata.Type;


/**
 * @author rmcalave
 *
 */
@WsDTOMapping
public class DisplayPriceConverter extends CustomConverter<PriceData, String> {

    /* (non-Javadoc)
     * @see ma.glasnost.orika.Converter#convert(java.lang.Object, ma.glasnost.orika.metadata.Type)
     */
    @Override
    public String convert(final PriceData source, final Type<? extends String> destinationType,
            final MappingContext mappingContext) {
        return PriceFormatUtils.formatPrice(source.getFormattedValue());
    }

}
