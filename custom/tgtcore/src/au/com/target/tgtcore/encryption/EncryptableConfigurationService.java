/**
 * 
 */
package au.com.target.tgtcore.encryption;

//CHECKSTYLE:OFF
import de.hybris.platform.core.Registry;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.config.impl.DefaultConfigurationService;
import de.hybris.platform.servicelayer.config.impl.HybrisConfiguration;
import de.hybris.platform.servicelayer.internal.service.AbstractService;
import de.hybris.platform.servicelayer.tenant.TenantService;

import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.commons.configuration.Configuration;
import org.jasypt.encryption.StringEncryptor;
import org.springframework.beans.factory.annotation.Required;


/**
 * A {@link ConfigurationService} implementation, that replaces the oob {@link DefaultConfigurationService}. This
 * returns a {@link Configuration}, that supports decrypting encrypted string values, using Jasypt.
 * 
 * @author vmuthura
 * 
 */
public class EncryptableConfigurationService extends AbstractService implements ConfigurationService {
    private EncryptableConfiguration cfg;
    private TenantService tenantService;
    private StringEncryptor encryptor;

    /**
     * Returned value is a commons {@link Configuration} instance and not a {@link HybrisConfiguration}.<br/>
     * Advantage: only most used getters are available; unused confusing methods are hidden.<br/>
     * Disadvantage: when using listeners result must be cast to {@link AbstractConfiguration}.
     */
    @Override
    public Configuration getConfiguration() {
        return this.cfg;
    }

    @Required
    public void setTenantService(final TenantService tenantService) {
        this.tenantService = tenantService;
    }

    /**
     * @param encryptor
     *            the encryptor to set
     */
    @Required
    public void setEncryptor(final StringEncryptor encryptor) {
        this.encryptor = encryptor;
    }

    @Override
    public void afterPropertiesSet() throws Exception //NOPMD
    {
        super.afterPropertiesSet();
        this.cfg = new EncryptableConfiguration(Registry.getTenantByID(tenantService.getCurrentTenantId()).getConfig(),
                encryptor);
    }
}
