/**
 * 
 */
package au.com.target.tgtcore.stockupdate;

/**
 * @author bhuang3
 * 
 */
public interface TargetInventoryService {

    /**
     * send the rest call to the web method to update the inventory quantity for ebay product.
     * 
     * @param productCode
     * @param quantity
     * @return return true if get the no error response from service and response productCode match the request
     *         productCode otherwise return false
     */
    boolean updateInventoryItemQuantity(final String productCode, final int quantity);

}
