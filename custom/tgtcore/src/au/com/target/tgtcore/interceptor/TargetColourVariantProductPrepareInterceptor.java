/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;


/**
 * @author pratik
 *
 */
public class TargetColourVariantProductPrepareInterceptor implements
        PrepareInterceptor<TargetColourVariantProductModel> {

    private int maxNewLowerPriceDisplayTime;

    /**
     * Prepare new lower price end date based on new lower price start date.
     */
    @Override
    public void onPrepare(final TargetColourVariantProductModel colourVariant, final InterceptorContext ctx)
            throws InterceptorException {
        if (colourVariant.getNewLowerPriceStartDate() != null
                && isNLPEndDateNullOrGreaterThanAllowed(colourVariant.getNewLowerPriceStartDate(),
                        colourVariant.getNewLowerPriceEndDate())) {
            colourVariant.setNewLowerPriceEndDate(DateUtils.addMonths(colourVariant.getNewLowerPriceStartDate(),
                    maxNewLowerPriceDisplayTime));
        }
    }

    /**
     * Method to check if end NLP End date is missing or is set to greater than 'maxNewLowerPriceDisplayTime' configured
     * in months from NLP start date.
     * 
     * @param newLowerPriceStartDate
     * @param newLowerPriceEndDate
     * @return boolean
     */
    private boolean isNLPEndDateNullOrGreaterThanAllowed(final Date newLowerPriceStartDate,
            final Date newLowerPriceEndDate) {
        return (newLowerPriceEndDate == null || (newLowerPriceEndDate.compareTo(DateUtils.addMonths(
                newLowerPriceStartDate, maxNewLowerPriceDisplayTime)) > 0));
    }

    /**
     * @param maxNewLowerPriceDisplayTime
     *            the maxNewLowerPriceDisplayTime to set
     */
    @Required
    public void setMaxNewLowerPriceDisplayTime(final int maxNewLowerPriceDisplayTime) {
        this.maxNewLowerPriceDisplayTime = maxNewLowerPriceDisplayTime;
    }
}
