package au.com.target.tgtcore.interceptor;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;


/**
 * {@link ValidateInterceptor} on {@link ProductModel} to ensure
 * 
 * 1) {@link ProductModel#MINORDERQUANTITY} is not less than or equal to Zero<br>
 * 2) {@link ProductModel#MAXORDERQUANTITY} is not less than or equal to Zero<br>
 * 3) {@link ProductModel#MINORDERQUANTITY} is less than {@link ProductModel#MAXORDERQUANTITY}
 */
public class ProductMinMaxOrderQtyValidator implements ValidateInterceptor {

    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof ProductModel)) {
            throw new IllegalArgumentException(model + " is not an instance of ProductModel");
        }
        final ProductModel productModel = (ProductModel)model;

        final Integer minOrderQty = productModel.getMinOrderQuantity();
        final Integer maxOrderQty = productModel.getMaxOrderQuantity();

        if (minOrderQty != null && minOrderQty.intValue() <= 0) {
            throw new InterceptorException("MinOrderQuantity cannot be less than or equals zero.");
        }

        if (maxOrderQty != null && maxOrderQty.intValue() <= 0) {
            throw new InterceptorException("MaxOrderQuantity cannot be less than or equals zero.");
        }

        if (minOrderQty != null && maxOrderQty != null && minOrderQty.intValue() > maxOrderQty.intValue()) {
            throw new InterceptorException("MinOrderQuantity cannot be greater than MaxOrderQuantity");
        }
    }

}
