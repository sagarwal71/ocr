/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.TargetSalesChannelZDMVRestrictionModel;


/**
 * The Class TargetSalesChannelZDMVRestrictionValidateInterceptor.
 * 
 * @author Nandini
 */
public class TargetSalesChannelZDMVRestrictionValidateInterceptor implements ValidateInterceptor {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof TargetSalesChannelZDMVRestrictionModel)
        {
            final TargetSalesChannelZDMVRestrictionModel restrictionModel = (TargetSalesChannelZDMVRestrictionModel)model;
            if (CollectionUtils.isEmpty(restrictionModel.getSalesApplication())) {
                throw new InterceptorException("Sales Channel must be provided.");
            }
        }
    }
}
