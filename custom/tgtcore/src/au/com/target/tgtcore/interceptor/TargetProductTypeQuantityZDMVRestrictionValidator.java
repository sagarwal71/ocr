/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetProductTypeQuantityZDMVRestrictionModel;



/**
 * The Class TargetProductTypeQuantityZDMVRestrictionValidator.
 * 
 * @author umesh
 * 
 *         Validator for TargetProductTypeQuantityZDMVRestriction.
 */
public class TargetProductTypeQuantityZDMVRestrictionValidator implements ValidateInterceptor {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */

    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {

        if (model instanceof TargetProductTypeQuantityZDMVRestrictionModel) {
            final TargetProductTypeQuantityZDMVRestrictionModel restrictionModel = (TargetProductTypeQuantityZDMVRestrictionModel)model;

            final Collection<ProductTypeModel> productTypes = restrictionModel.getProductType();
            if (CollectionUtils.isEmpty(productTypes)) {
                throw new InterceptorException(ErrorMessages.PRODUCT_TYPE_MINIMUM);
            }

            final Integer min = restrictionModel.getMinQuantity();
            final Integer max = restrictionModel.getMaxQuantity();
            if (min == null && max == null)
            {
                throw new InterceptorException(ErrorMessages.MIN_AND_MAX_QTY_NULL);
            }
            if ((min != null) && (max != null) &&
                    (min.compareTo(max) > 0)) {
                throw new InterceptorException(ErrorMessages.MIN_QTY_HIGH_THAN_MAX_QTY);
            }
        }
    }

    protected interface ErrorMessages {
        public String PRODUCT_TYPE_MINIMUM = "'Product Type' must contain at least 1 Product Type.";
        public String MIN_AND_MAX_QTY_NULL = "'Minimum Qty' and 'Maximum Qty' cannot both be null";
        public String MIN_QTY_HIGH_THAN_MAX_QTY = "'Minimum Quantity' cannot be greater than 'Maximum Quantity'.";
    }

}
