/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author rmcalave
 * 
 */
public class AbstractSimpleDealModelValidateInterceptor implements ValidateInterceptor {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof AbstractSimpleDealModel)) {
            return;
        }

        final AbstractSimpleDealModel abstractSimpleDeal = (AbstractSimpleDealModel)model;

        if (Boolean.TRUE.equals(abstractSimpleDeal.getEnabled())) {
            if (CollectionUtils.isEmpty(abstractSimpleDeal.getQualifierList())) {
                throw new InterceptorException(ErrorMessages.ONE_QUALIFIER_REQUIRED);
            }
        }
    }

    protected interface ErrorMessages {
        public static final String ONE_QUALIFIER_REQUIRED = "At least one qualifier must be assigned to this deal if it is enabled.";
    }
}
