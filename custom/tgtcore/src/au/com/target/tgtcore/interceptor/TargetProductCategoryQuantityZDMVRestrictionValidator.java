/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.TargetProductCategoryQuantityZDMVRestrictionModel;


/**
 * @author Rahul
 * 
 */
public class TargetProductCategoryQuantityZDMVRestrictionValidator implements ValidateInterceptor {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {

        if (model instanceof TargetProductCategoryQuantityZDMVRestrictionModel) {
            final TargetProductCategoryQuantityZDMVRestrictionModel restrictionModel = (TargetProductCategoryQuantityZDMVRestrictionModel)model;

            if (CollectionUtils.isEmpty(restrictionModel.getIncludedProducts())
                    && CollectionUtils.isEmpty(restrictionModel.getIncludedCategories())) {
                throw new InterceptorException("Either products or categories must be provided.");
            }
            if (restrictionModel.getQuantity() != null && restrictionModel.getQuantity().intValue() < 1)
            {
                throw new InterceptorException("'Quantity' must be greater than 0.");
            }
        }
    }
}
