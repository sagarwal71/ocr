/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;


public class TargetVariantProductValidateInterceptor implements ValidateInterceptor {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof AbstractTargetVariantProductModel) {
            final AbstractTargetVariantProductModel targetVariantProduct = (AbstractTargetVariantProductModel)model;
            final Integer departmentCode = targetVariantProduct.getDepartment();

            if (departmentCode == null) {
                return;
            }

            if (departmentCode.intValue() < 0) {
                throw new InterceptorException("Department code can't be negative value: " + departmentCode.intValue());
            }
            final Set<DeliveryModeModel> deliveryModes = targetVariantProduct.getDeliveryModes();
            if (CollectionUtils.isNotEmpty(deliveryModes)) {
                throw new InterceptorException("Variant product cannot have delivery modes: "
                        + targetVariantProduct.getCode());
            }
        }

    }

}
