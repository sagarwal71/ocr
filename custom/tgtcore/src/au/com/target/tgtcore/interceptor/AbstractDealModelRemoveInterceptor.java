package au.com.target.tgtcore.interceptor;

import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import de.hybris.platform.servicelayer.time.TimeService;

import java.util.Date;


/**
 * Remove interceptor which restricts deals having start date in the past
 */
public class AbstractDealModelRemoveInterceptor implements RemoveInterceptor {

    @Override
    public void onRemove(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof AbstractDealModel)) {
            return;
        }

        final AbstractDealModel dealModel = (AbstractDealModel)model;
        final Date now = getTimeService().getCurrentTime();
        final Date startDate = dealModel.getStartDate();
        if (startDate == null || startDate.before(now)) {
            throw new InterceptorException("Cannot remove deal " + dealModel.getCode()
                    + " as the start date is not set or in the past");
        }
    }

    public TimeService getTimeService() {
        throw new UnsupportedOperationException(
                "Please define a <lookup-bean> for getTimeService() in the spring configuration");
    }

}
