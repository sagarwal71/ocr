/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneModel;
import de.hybris.platform.order.ZoneDeliveryModeService;
import de.hybris.platform.order.exceptions.DeliveryModeInterceptorException;
import de.hybris.platform.order.strategies.deliveryzone.ZDMVConsistencyStrategy;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel;


/**
 * 
 * @author umesh
 * 
 */
public class RestrictableTargetZoneDeliveryModeValueValidator implements ValidateInterceptor {
    private ZoneDeliveryModeService zoneDeliveryModeService;
    private ZDMVConsistencyStrategy zdmvConsistencyStrategy;
    private TargetDeliveryService targetDeliveryService;

    private Integer maximumLengthOfPromotionSticker;

    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {

        if (model instanceof RestrictableTargetZoneDeliveryModeValueModel) {
            // 1. check if zones clash  (hybris logic)
            final RestrictableTargetZoneDeliveryModeValueModel zoneDeliveryModeValue = (RestrictableTargetZoneDeliveryModeValueModel)model;
            final ZoneModel zone = zoneDeliveryModeValue.getZone();
            final ZoneDeliveryModeModel zoneDeliveryMode = zoneDeliveryModeValue.getDeliveryMode();
            final Collection<ZoneModel> existingZones =
                    zoneDeliveryModeService.getZonesForZoneDeliveryMode(zoneDeliveryMode);
            final Set<ZoneModel> zones = new HashSet<ZoneModel>(existingZones);
            if (zones.add(zone)) {
                final Map<CountryModel, Set<ZoneModel>> ambiguous =
                        zdmvConsistencyStrategy.getAmbiguousCountriesForZones(zones);
                if (!ambiguous.isEmpty()) {
                    throw new DeliveryModeInterceptorException("Illegal value for [" + zoneDeliveryMode.getCode()
                            + "] with zone [" + zone.getCode() + "] - its countries [" + ambiguous.keySet()
                            + "] would be mapped to more than one zone");
                }
            }
            final List<AbstractTargetZoneDeliveryModeValueModel> values = targetDeliveryService
                    .getDeliveryValue(
                            zone,
                            zoneDeliveryModeValue.getCurrency(), zoneDeliveryModeValue.getMinimum(), zoneDeliveryMode);
            if (CollectionUtils.isEmpty(values)) {
                return;
            }
            final List<RestrictableTargetZoneDeliveryModeValueModel> resValues = getRestrictableDeliveryValue(values);
            for (final RestrictableTargetZoneDeliveryModeValueModel targetZoneDelValue : resValues) {
                if (targetZoneDelValue.getPk().equals(zoneDeliveryModeValue.getPk())) {
                    continue;
                }
                // 2. check if priority is same as existing zone value
                if (isConflicted(zoneDeliveryModeValue, targetZoneDelValue)) {
                    throw new InterceptorException(
                            "You can't save another RestrictableTargetZoneDeliveryModeValueModel "
                                    + zoneDeliveryModeValue
                                    + " having same priority as current one " + targetZoneDelValue);
                }
            }
            if (zoneDeliveryModeValue.getPromotionalStickerMessage() != null
                    && zoneDeliveryModeValue.getPromotionalStickerMessage().length() > maximumLengthOfPromotionSticker
                            .intValue()) {
                throw new InterceptorException("Promotional sticker message's length is greater than "
                        + maximumLengthOfPromotionSticker);
            }
        }
    }

    private boolean isConflicted(final RestrictableTargetZoneDeliveryModeValueModel newModel,
            final RestrictableTargetZoneDeliveryModeValueModel existingModel) {
        if (newModel.getPriority().equals(existingModel.getPriority())) {
            return true;
        }
        return false;
    }

    private List<RestrictableTargetZoneDeliveryModeValueModel> getRestrictableDeliveryValue(
            final List<AbstractTargetZoneDeliveryModeValueModel> values)
    {
        final List<RestrictableTargetZoneDeliveryModeValueModel> reslist = new ArrayList<>();
        for (final AbstractTargetZoneDeliveryModeValueModel abstractTargetZoneDeliveryModeValueModel : values) {
            if (abstractTargetZoneDeliveryModeValueModel instanceof RestrictableTargetZoneDeliveryModeValueModel)
            {
                reslist.add((RestrictableTargetZoneDeliveryModeValueModel)abstractTargetZoneDeliveryModeValueModel);
            }
        }
        return reslist;
    }

    @Required
    public void setZdmvConsistencyStrategy(final ZDMVConsistencyStrategy zdmvConsistencyStrategy) {
        this.zdmvConsistencyStrategy = zdmvConsistencyStrategy;

    }



    @Required
    public void setZoneDeliveryModeService(final ZoneDeliveryModeService zoneDeliveryModeService) {
        this.zoneDeliveryModeService = zoneDeliveryModeService;

    }

    /**
     * @param targetDeliveryService
     *            the targetDeliveryService to set
     */
    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        this.targetDeliveryService = targetDeliveryService;
    }

    /**
     * @param maximumLengthOfPromotionSticker
     *            the maximumLengthOfPromotionSticker to set
     */
    @Required
    public void setMaximumLengthOfPromotionSticker(final Integer maximumLengthOfPromotionSticker) {
        this.maximumLengthOfPromotionSticker = maximumLengthOfPromotionSticker;
    }
}
