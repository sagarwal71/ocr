/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.order.TargetCommerceCheckoutService;


public class FlybuysValidateInterceptor implements ValidateInterceptor {

    private TargetCommerceCheckoutService targetCommerceCheckoutService;

    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof CartModel) {
            final CartModel cartModel = (CartModel)model;
            final String flybuysCode = cartModel.getFlyBuysCode();
            if (StringUtils.isBlank(flybuysCode)) {
                return;
            }

            final boolean result = targetCommerceCheckoutService.isFlybuysNumberValid(cartModel);
            if (!result) {
                throw new InterceptorException("Invalid flybuys number " + flybuysCode);
            }
        }
    }

    @Required
    public void setTargetCommerceCheckoutService(final TargetCommerceCheckoutService targetCommerceCheckoutService) {
        this.targetCommerceCheckoutService = targetCommerceCheckoutService;
    }
}
