package au.com.target.tgtcore.interceptor;

import de.hybris.platform.promotions.model.QuantityBreakDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;


/**
 * @author dcwillia
 * 
 */
public class QuantityBreakDealInitDefaultsInterceptor extends AbstractDealInitDefaultsInterceptor {

    @Override
    public void onInitDefaults(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof QuantityBreakDealModel) {
            super.onInitDefaults(model, ctx);
        }
    }
}
