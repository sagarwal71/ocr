/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtwebcore.model.cms2.restrictions.CMSSalesChannelRestrictionModel;


/**
 * @author Nandini
 *
 */
public class CMSSalesChannelRestrictionInterceptor implements ValidateInterceptor<CMSSalesChannelRestrictionModel> {

    @Override
    public void onValidate(final CMSSalesChannelRestrictionModel model, final InterceptorContext ctx)
            throws InterceptorException {
        if (null != model) {
            final Collection<SalesApplication> salesApplications = model.getSalesApplications();
            if (CollectionUtils.isEmpty(salesApplications)) {
                throw new InterceptorException(
                        "Could not validate CMSSalesChannelRestriction. Atleast one Sales Channel needs to be set.");
            }
        }

    }

}
