/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InitDefaultsInterceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.voucher.model.OrderRestrictionModel;

import org.springframework.beans.factory.annotation.Required;


/**
 * For Voucher Order Restrictions, set some defaults.
 * 
 */
public class VoucherOrderRestrictionInitDefaultsInterceptor implements InitDefaultsInterceptor {

    private Boolean net;
    private Boolean valueOfGoodsOnly;


    @Override
    public void onInitDefaults(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof OrderRestrictionModel) {
            final OrderRestrictionModel restriction = (OrderRestrictionModel)model;
            restriction.setNet(net);
            restriction.setValueofgoodsonly(valueOfGoodsOnly);
        }
    }

    @Required
    public void setNet(final Boolean net) {
        this.net = net;
    }

    @Required
    public void setValueOfGoodsOnly(final Boolean valueOfGoodsOnly) {
        this.valueOfGoodsOnly = valueOfGoodsOnly;
    }


}
