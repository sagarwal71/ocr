/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.promotions.model.SpendAndSaveDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.enums.DealRewardTypeEnum;


/**
 * @author rmcalave
 * 
 */
public class SpendAndSaveDealModelValidateInterceptor implements ValidateInterceptor {

    private List<DealRewardTypeEnum> validDealTypes;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof SpendAndSaveDealModel)) {
            return;
        }

        final SpendAndSaveDealModel spendAndSaveDeal = (SpendAndSaveDealModel)model;

        if (!validDealTypes.contains(spendAndSaveDeal.getRewardType())) {
            throw new InterceptorException(String.format(ErrorMessages.INVALID_DEAL_REWARD_TYPE,
                    ArrayUtils.toString(validDealTypes.toArray())));
        }

        final List<DealQualifierModel> dealQualifiers = spendAndSaveDeal.getQualifierList();

        if (CollectionUtils.size(dealQualifiers) > 1) {
            throw new InterceptorException(ErrorMessages.TOO_MANY_QUALIFIERS);
        }

        if (CollectionUtils.isEmpty(dealQualifiers)) {
            if (Boolean.TRUE.equals(spendAndSaveDeal.getEnabled())) {
                throw new InterceptorException(ErrorMessages.NOT_ENOUGH_QUALIFIERS);
            }
        }
        else {
            final DealQualifierModel dealQualifier = dealQualifiers.get(0);
            if (dealQualifier.getMinAmt() == null || dealQualifier.getMinAmt().doubleValue() <= 0.0) {
                throw new InterceptorException(ErrorMessages.QUALIFIER_REQUIRES_AMOUNT);
            }
        }

    }

    protected interface ErrorMessages {
        String INVALID_DEAL_REWARD_TYPE = "'Reward Type' must be one of %s";
        String TOO_MANY_QUALIFIERS = "Deal cannot have more than 1 qualifier";
        String NOT_ENOUGH_QUALIFIERS = "Deal must have 1 qualifier if it is enabled.";
        String QUALIFIER_REQUIRES_AMOUNT = "'Minimum Amount' of the qualifier must be greater than 0.";
    }


    /**
     * @param validDealTypes
     *            the validDealTypes to set
     */
    @Required
    public void setValidDealTypes(final List<DealRewardTypeEnum> validDealTypes) {
        this.validDealTypes = validDealTypes;
    }
}
