package au.com.target.tgtcore.interceptor;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;


/**
 * Remove interceptor which removes the associated qualifier list and the reward categories
 */
public class AbstractSimpleDealModelRemoveInterceptor implements RemoveInterceptor {

    @Override
    public void onRemove(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof AbstractSimpleDealModel)) {
            return;
        }

        final AbstractSimpleDealModel dealModel = (AbstractSimpleDealModel)model;

        // remove the qualifier list including the categories and the reward categories
        final Collection<ItemModel> itemsToRemove = new ArrayList<>();

        for (final DealQualifierModel dealQualifierModel : dealModel.getQualifierList()) {
            itemsToRemove.addAll(dealQualifierModel.getDealCategory());
            itemsToRemove.add(dealQualifierModel);
        }

        itemsToRemove.addAll(dealModel.getRewardCategory());

        if (CollectionUtils.isNotEmpty(itemsToRemove)) {
            getModelService().removeAll(itemsToRemove);
        }
    }

    public ModelService getModelService() {
        throw new UnsupportedOperationException(
                "Please define a <lookup-method> for getModelService() in the spring configuration");
    }

}
