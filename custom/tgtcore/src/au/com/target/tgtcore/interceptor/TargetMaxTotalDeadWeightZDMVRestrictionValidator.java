/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.lang.ObjectUtils;

import au.com.target.tgtcore.model.TargetMaxTotalDeadWeightZDMVRestrictionModel;


/**
 * @author pratik
 *
 */
public class TargetMaxTotalDeadWeightZDMVRestrictionValidator implements
        ValidateInterceptor<TargetMaxTotalDeadWeightZDMVRestrictionModel> {

    /**
     * Method to validate Max total dead weight. The total dead weight should be a positive value.
     */
    @Override
    public void onValidate(final TargetMaxTotalDeadWeightZDMVRestrictionModel model, final InterceptorContext arg1)
            throws InterceptorException {
        if (ObjectUtils.compare(model.getMaxTotalDeadWeight(), Double.valueOf(0.0)) < 0) {
            throw new InterceptorException("Max total dead weight can't be negative.");
        }
    }
}
