/**
 * 
 */
package au.com.target.tgtcore.warehouse.dao;

import de.hybris.platform.ordersplitting.daos.WarehouseDao;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.List;


/**
 * @author dcwillia
 * 
 */
public interface TargetWarehouseDao extends WarehouseDao {


    /**
     * Retrieve a list of warehouses that associated with the provided store.
     * 
     * @param storeNumber
     *            The store number to search on
     * @return Warehouses associated with the store
     */
    List<WarehouseModel> getWarehouseForPointOfService(final Integer storeNumber);

    /**
     * Return all warehouses with the surfaceStockOnline flag set
     * 
     * @return list
     */
    List<WarehouseModel> getSurfaceStockOnlineWarehouses();

    /**
     * Retrieve a list of warehouses based on the value of externalWarehouse and surfaceOnlineStock
     * 
     * @param externalWarehouse
     * @return external warehouses
     */
    List<WarehouseModel> getExternalWarehouses(final boolean externalWarehouse);

    /**
     * list of warehouses which have capability of fulfilling pre-orders, i.e. preOrderWarehouse boolean set.
     * 
     * @return list
     */
    List<WarehouseModel> getPreOrderWarehouses();

    /**
     * Get the warehouse for given fluentLocationRef
     * 
     * @param fluentLocationRef
     * @return {@link WarehouseModel}
     */
    List<WarehouseModel> getWarehouseForFluentLocationRef(String fluentLocationRef);

}
