/**
 * 
 */
package au.com.target.tgtcore.warehouse.dao.impl;

import de.hybris.platform.ordersplitting.daos.impl.DefaultWarehouseDao;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.warehouse.dao.TargetWarehouseDao;


/**
 * @author dcwillia
 * 
 */
public class TargetWarehouseDaoImpl extends DefaultWarehouseDao implements TargetWarehouseDao {

    @Override
    public List<WarehouseModel> getWarehouseForPointOfService(final Integer storeNumber) {
        final StringBuilder query = new StringBuilder("select {w:").append(WarehouseModel.PK).append('}');
        query.append("from {").append(WarehouseModel._TYPECODE).append(" as w}, {")
                .append(TargetPointOfServiceModel._TYPECODE).append(" as p}, {PoS2WarehouseRel as r}");
        query.append("where {w:").append(WarehouseModel.PK).append("} = {r:target}");
        query.append("and {p:").append(TargetPointOfServiceModel.PK).append("} = {r:source}");
        query.append("and {p:").append(TargetPointOfServiceModel.STORENUMBER).append("} = ?storeNumber");

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("storeNumber", storeNumber);

        final SearchResult<WarehouseModel> result = getFlexibleSearchService().search(query.toString(), params);

        return result.getResult();
    }

    @Override
    public List<WarehouseModel> getSurfaceStockOnlineWarehouses() {
        return find(Collections.singletonMap(WarehouseModel.SURFACESTOCKONLINE, Boolean.TRUE));
    }

    @Override
    public List<WarehouseModel> getExternalWarehouses(final boolean externalWarehouse) {
        final StringBuilder query = new StringBuilder("SELECT {w:").append(WarehouseModel.PK).append('}');
        query.append(" FROM {").append(WarehouseModel._TYPECODE).append(" as w} ");
        query.append("WHERE {w:").append(WarehouseModel.EXTERNALWAREHOUSE).append("} = ?externalWarehouse");

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("externalWarehouse", Boolean.valueOf(externalWarehouse));

        final SearchResult<WarehouseModel> result = getFlexibleSearchService().search(query.toString(), params);

        return result.getResult();
    }

    @Override
    public List<WarehouseModel> getPreOrderWarehouses() {
        return find(Collections.singletonMap(WarehouseModel.PREORDERWAREHOUSE, Boolean.TRUE));
    }

    @Override
    public List<WarehouseModel> getWarehouseForFluentLocationRef(final String fluentLocationRef) {
        return this.find(Collections.singletonMap("fluentLocationRef", fluentLocationRef));
    }

}
