/**
 *
 */
package au.com.target.tgtcore.delivery.dao.impl;

import de.hybris.platform.commerceservices.delivery.dao.impl.DefaultCountryZoneDeliveryModeDao;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


/**
 * @author Pradeep
 *
 */
public class TargetCountryZoneDeliveryModeDao extends DefaultCountryZoneDeliveryModeDao {


    /**
     * overriding findDeliveryModesByCountryAndCurrency() of DefaultCountryZoneDeliveryModeDao method to remove warnings
     * 
     * @param deliveryCountry
     * @param currency
     * @param net
     * @return collection
     */
    public Collection<DeliveryModeModel> findDeliveryModes(final CountryModel deliveryCountry,
            final CurrencyModel currency,
            final Boolean net) {
        final Map params = new HashMap();
        params.put("deliveryCountry", deliveryCountry);
        params.put("currency", currency);
        params.put("net", net);
        params.put("active", Boolean.TRUE);

        return doSearch(getDeliveryModeQuery(), params, DeliveryModeModel.class);
    }

    protected String getDeliveryModeQuery() {
        final StringBuilder query = new StringBuilder("SELECT DISTINCT {zdm:")
                .append("pk").append("}");
        query.append(" FROM { ").append("ZoneDeliveryModeValue")
                .append(" AS val");
        query.append(" JOIN ").append("ZoneDeliveryMode").append(" AS zdm");
        query.append(" ON {val:").append("deliveryMode").append("}={zdm:")
                .append("pk").append('}');
        query.append(" JOIN ").append("ZoneCountryRelation").append(" AS z2c");
        query.append(" ON {val:").append("zone").append("}={z2c:")
                .append("source").append('}');
        query.append(" } WHERE {val:").append("currency").append("}=?currency");
        query.append(" AND {z2c:").append("target")
                .append("}=?deliveryCountry");
        query.append(" AND {zdm:").append("net").append("}=?net");
        query.append(" AND {zdm:").append("active").append("}=?active");
        return query.toString();
    }


}
