package au.com.target.tgtcore.product.dao.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ColourModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.product.dao.TargetProductSearchServiceDao;


public class TargetProductSearchServiceDaoImpl extends DefaultGenericDao<TargetSizeVariantProductModel> implements
        TargetProductSearchServiceDao
{

    private CatalogVersionService catalogService;
    private int productCount;

    /**
     * Defined for default constructor
     */
    public TargetProductSearchServiceDaoImpl() {
        super(TargetSizeVariantProductModel._TYPECODE);
    }

    /**
     * Get TargetSizeVariantProductModel List by baseProduct
     * 
     * @param baseProduct
     * @return TargetSizeVariantProductModel List
     */
    @Override
    public List<TargetSizeVariantProductModel> getTargetSizeVariantProductModelByBaseProduct(final String baseProduct) {

        final String queryString = "SELECT {"+TargetSizeVariantProductModel.PK+"} "
                + "FROM { " + TargetSizeVariantProductModel._TYPECODE + " }" + " WHERE {"+TargetSizeVariantProductModel.BASEPRODUCT+"} = ?baseProduct";

        final SearchResult<TargetSizeVariantProductModel> searchResult = getFlexibleSearchService().search(
                queryString, Collections.singletonMap("baseProduct", baseProduct));

        if (searchResult != null) {
            return searchResult.getResult();
        }
        else {
            return null;
        }

    }


    /**
     * Get getTargetSizeVariantProductModelByCode List by code
     * 
     * @param code
     * @return TargetSizeVariantProductModel List
     */
    @Override
    public List<TargetSizeVariantProductModel> getTargetSizeVariantProductModelByCode(final String code) {

        final String queryString = "SELECT {"+TargetSizeVariantProductModel.PK +"} "
                + "FROM { " + TargetSizeVariantProductModel._TYPECODE + " }" + " WHERE {"+ TargetSizeVariantProductModel.CODE + "} = ?code";

        final SearchResult<TargetSizeVariantProductModel> searchResult = getFlexibleSearchService().search(
                queryString, Collections.singletonMap("code", code));
        if (searchResult != null) {
            return searchResult.getResult();
        }
        else {
            return null;
        }

    }

    @Override
    public List<AbstractTargetVariantProductModel> getAllProductVariants(final String query, final int start) {
        final FlexibleSearchQuery search = new FlexibleSearchQuery(query);
        search.setStart(start);
        search.setCount(productCount);
        search.addQueryParameter("catalogVersion",
                catalogService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                        TgtCoreConstants.Catalog.ONLINE_VERSION));
        final SearchResult<AbstractTargetVariantProductModel> searchResult = getFlexibleSearchService().search(search);
        return searchResult.getResult();
    }

    @Override
    public TargetColourVariantProductModel getByBaseProductAndSwatch(
            final String baseProductCode, final String swatchCode, final CatalogVersionModel productCatalog) {
        final FlexibleSearchQuery query =
                new FlexibleSearchQuery("SELECT {p." + TargetColourVariantProductModel.PK + "}"
                        + "FROM {" + TargetColourVariantProductModel._TYPECODE + " AS p"
                        + "  JOIN " + ColourModel._TYPECODE + " AS c "
                        + "    ON {c." + ColourModel.PK + "} = {p." + TargetColourVariantProductModel.SWATCH + "}"
                        + "  JOIN " + ProductModel._TYPECODE + " AS b "
                        + "    ON {b." + ProductModel.PK + "} = {p." + TargetColourVariantProductModel.BASEPRODUCT
                        + "}"
                        + "}"
                        + "WHERE {c." + ColourModel.CODE + "} = ?swatchCode "
                        + "  AND {b." + ProductModel.CODE + "} = ?baseProductCode"
                        + "  AND {p." + TargetColourVariantProductModel.CATALOGVERSION + "} = ?catalogVersion");

        query.addQueryParameter("swatchCode", swatchCode);
        query.addQueryParameter("baseProductCode", baseProductCode);
        query.addQueryParameter("catalogVersion", productCatalog);
        return getFlexibleSearchService().searchUnique(query);
    }

    @Override
    public List<TargetProductModel> getAllTargetProductsWithNoDepartment(final CatalogVersionModel catalogVersion,
            final int start,
            final int count)
    {
        final StringBuilder query = new StringBuilder();
        query.append("SELECT {").append(TargetProductModel.PK).append("} ");
        query.append("FROM {").append(TargetProductModel._TYPECODE).append("} ");
        query.append("WHERE {").append(TargetProductModel.CATALOGVERSION).append("} = ?catalogVersion ");
        query.append(" AND {").append(TargetProductModel.MERCHDEPARTMENT).append("} IS NULL ");

        final FlexibleSearchQuery search = new FlexibleSearchQuery(query.toString());
        search.addQueryParameter("catalogVersion", catalogVersion);
        search.setCount(count);
        search.setStart(start);
        final SearchResult<TargetProductModel> searchResult = getFlexibleSearchService().search(search);
        return searchResult.getResult();
    }

    /**
     * @param catalogService
     *            the catalogService to set
     */
    @Required
    public void setCatalogService(final CatalogVersionService catalogService) {
        this.catalogService = catalogService;
    }

    /**
     * @param productCount
     *            the productCount to set
     */
    @Required
    public void setProductCount(final int productCount) {
        this.productCount = productCount;
    }
}
