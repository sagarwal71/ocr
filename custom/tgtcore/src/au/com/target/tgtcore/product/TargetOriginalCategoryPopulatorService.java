/**
 * 
 */
package au.com.target.tgtcore.product;

import au.com.target.tgtcore.product.exception.ProductOriginalCategoryPopulationException;


/**
 * It populates original category field of all the products based on certain conditions
 * 
 * @author nullah
 */
public interface TargetOriginalCategoryPopulatorService {

    /**
     * It reads all the products whose original category is NULL and approved status is approved/unapproved and in
     * Staged. It sets original category based on below mentioned rules 1)if it is not a clearance product then
     * originalCategory field is set as last category available 2)if it is a clearance product then originalCategory is
     * set as by taking category of product and comparing with all the top most categories and setting the found one
     * 
     * 
     * @throws ProductOriginalCategoryPopulationException
     */

    public void populateOriginalCategories() throws ProductOriginalCategoryPopulationException;

}