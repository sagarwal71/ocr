/**
 * TargetProductCountMerchDepRowData
 */
package au.com.target.tgtcore.product.data;

import au.com.target.tgtcore.model.TargetMerchDepartmentModel;


/**
 * @author bhuang3
 * 
 */
public class TargetProductCountMerchDepRowData {

    private TargetMerchDepartmentModel targetMerchDepartmentModel;

    private int productCount;

    /**
     * @return the targetMerchDepartmentModel
     */
    public TargetMerchDepartmentModel getTargetMerchDepartmentModel() {
        return targetMerchDepartmentModel;
    }

    /**
     * @param targetMerchDepartmentModel
     *            the targetMerchDepartmentModel to set
     */
    public void setTargetMerchDepartmentModel(final TargetMerchDepartmentModel targetMerchDepartmentModel) {
        this.targetMerchDepartmentModel = targetMerchDepartmentModel;
    }

    /**
     * @return the productCount
     */
    public int getProductCount() {
        return productCount;
    }

    /**
     * @param productCount
     *            the productCount to set
     */
    public void setProductCount(final int productCount) {
        this.productCount = productCount;
    }



}
