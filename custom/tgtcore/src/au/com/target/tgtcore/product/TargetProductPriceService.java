/**
 *
 */
package au.com.target.tgtcore.product;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPriceRowModel;
import au.com.target.tgtcore.product.data.TargetPriceRowData;


/**
 * Handles updating of product price details
 *
 */
public interface TargetProductPriceService {


    /**
     * Remove product from sale in all catalogs
     *
     * @param productsNotOnSale
     * @param catalogs
     * @return list of messages with status on the update
     */
    List<String> removeProductFromSale(final List<String> productsNotOnSale, List<CatalogVersionModel> catalogs);

    /**
     * Update product prices in all catalogs
     *
     * @param productsToUpdate
     * @return true if product was updated
     */
    boolean updateProductPrices(final List<ProductModel> productsToUpdate);


    /**
     * return a list of products for the given catalogs with the given prices, but only if they have changed.
     *
     * @param productCode
     * @param catalogs
     * @param currentPrice
     * @param wasPrice
     * @param futurePrices
     * @param gstRate
     * @param promoEvent
     * @return List of Products associated with each of the catalogs
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    List<ProductModel> getProductModelForCatalogs(final String productCode,
            final List<CatalogVersionModel> catalogs,
            final double currentPrice,
            final double wasPrice,
            final List<TargetPriceRowData> futurePrices,
            final int gstRate,
            final boolean promoEvent)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;


    /**
     * get a list of catalog's
     *
     * @return List containing online and staged catalog
     */
    List<CatalogVersionModel> getCatalogs();

    /**
     * Get the current price row from Staged. Returns null if product or price does not exist.
     *
     * @return TargetPriceRowModel
     */
    TargetPriceRowModel getCurrentTargetPriceRowModelFromStaged(final String productCode);
}
