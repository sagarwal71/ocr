/**
 * 
 */
package au.com.target.tgtcore.product.impl;

import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.order.ZoneDeliveryModeService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.TargetProductDeliveryModeService;


/**
 * @author pthoma20
 * 
 *         This class is used for updating the express delivery mode in the target product model which is passed as an
 *         argument along with the targetMerchDepartmentModel.
 * 
 */
public class TargetProductDeliveryModeServiceImpl implements TargetProductDeliveryModeService {


    private static final Logger LOG = Logger.getLogger(TargetProductDeliveryModeServiceImpl.class);

    private String expressDeliveryCode;

    private ZoneDeliveryModeService zoneDeliveryModeService;

    /**
     * Processes the Delivery Mode to check whether it needs an updation. If it needs, updates it in the target product
     * model.
     * 
     * @param targetProductModel
     * @param targetMerchDepModel
     * @return isUpdateRequired
     */
    @Override
    public boolean processExpressDeliveryModeUpdation(final TargetProductModel targetProductModel,
            final TargetMerchDepartmentModel targetMerchDepModel) {

        final Set<DeliveryModeModel> deliveryModeListModified = new HashSet();
        if (targetProductModel.getDeliveryModes() != null) {
            deliveryModeListModified.addAll(targetProductModel.getDeliveryModes());
        }

        final DeliveryModeModel existingExpressDeliveryMode = getExistingExpressDeliveryModel(
                deliveryModeListModified, expressDeliveryCode);

        final boolean isExpressDeliveryEligible = isProductEligibleForExpressDelivery(targetProductModel,
                targetMerchDepModel);

        boolean isUpdateRequired = false;

        if (null != existingExpressDeliveryMode && !isExpressDeliveryEligible) {
            removeDeliveryMode(targetProductModel, deliveryModeListModified, existingExpressDeliveryMode);
            isUpdateRequired = true;
        }
        else if (null == existingExpressDeliveryMode && isExpressDeliveryEligible) {
            try {
                final DeliveryModeModel expressDeliveryModel = zoneDeliveryModeService
                        .getDeliveryModeForCode(expressDeliveryCode);
                addDeliveryMode(targetProductModel, deliveryModeListModified, expressDeliveryModel);
                isUpdateRequired = true;
            }
            catch (final UnknownIdentifierException exep) {
                LOG.error("Delivery Mode not found for Code  " + expressDeliveryCode);
            }

        }
        return isUpdateRequired;

    }


    /**
     * Returns the express delivery model
     * 
     * @param deliveryModeList
     * @return deliveryModel
     */
    private DeliveryModeModel getExistingExpressDeliveryModel(final Set<DeliveryModeModel> deliveryModeList,
            final String deliveryModeCode) {
        if (null != deliveryModeList) {
            for (final DeliveryModeModel deliveryModel : deliveryModeList) {
                if (StringUtils.equalsIgnoreCase(deliveryModel.getCode(), deliveryModeCode)) {
                    return deliveryModel;
                }
            }
        }
        return null;

    }

    /**
     * This method takes the passed deliveryModeModel and does an operation add or remove based on identifier
     * 
     * @param targetProductModel
     * @param deliveryModeList
     * @param deliveryModeModel
     */
    private void addDeliveryMode(final TargetProductModel targetProductModel,
            final Set<DeliveryModeModel> deliveryModeList,
            final DeliveryModeModel deliveryModeModel) {
        Set<DeliveryModeModel> deliveryModeListModified = deliveryModeList;
        if (null != deliveryModeListModified) {
            deliveryModeListModified.add(deliveryModeModel);
        }
        else {
            deliveryModeListModified = new HashSet();
            deliveryModeListModified.add(deliveryModeModel);
        }

        targetProductModel.setDeliveryModes(deliveryModeListModified);
    }

    /**
     * This method takes the passed deliveryModeModel and does an operation add or remove based on identifier
     * 
     * @param targetProductModel
     * @param deliveryModeList
     * @param deliveryModeModel
     */
    private void removeDeliveryMode(final TargetProductModel targetProductModel,
            final Set<DeliveryModeModel> deliveryModeList,
            final DeliveryModeModel deliveryModeModel) {
        final Set<DeliveryModeModel> deliveryModeListModified = deliveryModeList;
        deliveryModeListModified.remove(deliveryModeModel);
        targetProductModel.setDeliveryModes(deliveryModeListModified);
    }

    /**
     * Method will check whether the product is eligible for delivery based on product type(non bulky) and on the
     * Eligible flag for its department.
     * 
     * @param targetProductModel
     * @param targetMerchDepModel
     * @return isExpressDeliveryEligible
     */
    private boolean isProductEligibleForExpressDelivery(final TargetProductModel targetProductModel,
            final TargetMerchDepartmentModel targetMerchDepModel) {

        final ProductTypeModel productTypeModel = targetProductModel.getProductType();
        return (null != targetMerchDepModel.getExpressDeliveryEligible()
                && targetMerchDepModel.getExpressDeliveryEligible().booleanValue()
                && null != productTypeModel && !productTypeModel.getBulky().booleanValue());
    }



    /**
     * @param zoneDeliveryModeService
     *            the zoneDeliveryModeService to set
     */
    @Required
    public void setZoneDeliveryModeService(final ZoneDeliveryModeService zoneDeliveryModeService) {
        this.zoneDeliveryModeService = zoneDeliveryModeService;
    }

    /**
     * @param expressDeliveryCode
     *            the expressDeliveryCode to set
     */
    @Required
    public void setExpressDeliveryCode(final String expressDeliveryCode) {
        this.expressDeliveryCode = expressDeliveryCode;
    }


}
