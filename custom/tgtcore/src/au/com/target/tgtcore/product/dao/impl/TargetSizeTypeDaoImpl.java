/**
 * 
 */
package au.com.target.tgtcore.product.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtcore.product.dao.TargetSizeTypeDao;
import au.com.target.tgtcore.util.TargetServicesUtil;



/**
 * The Class TargetSizeTypeDaoImpl.
 */
public class TargetSizeTypeDaoImpl implements TargetSizeTypeDao {

    /** The Constant LOG. */
    private static final Logger LOG = Logger.getLogger(TargetSizeTypeDaoImpl.class);

    /** The Constant QUERY_SIZETYPE_DEFAULT. */
    private static final String QUERY_SIZETYPE_DEFAULT = "SELECT {p:" + SizeTypeModel.PK + "}"
            + "FROM {" + SizeTypeModel._TYPECODE + " AS p} "
            + "WHERE {p:" + SizeTypeModel.ISDEFAULT + "}=?isDefault";

    /** The Constant QUERY_SIZETYPE_BY_CODE. */
    private static final String QUERY_SIZETYPE_BY_CODE = "SELECT {p:" + SizeTypeModel.PK + "}"
            + "FROM {" + SizeTypeModel._TYPECODE + " AS p} "
            + "WHERE {p:" + SizeTypeModel.CODE + "}=?code";

    /** The flexible search service. */
    private FlexibleSearchService flexibleSearchService;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.product.dao.TargetSizeTypeDao#getSizeTypeByCode(java.lang.String)
     */
    @Override
    public SizeTypeModel getSizeTypeByCode(final String code) {
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(QUERY_SIZETYPE_BY_CODE);
        searchQuery.addQueryParameter("code", code);

        final List<SizeTypeModel> models = flexibleSearchService.<SizeTypeModel> search(searchQuery).getResult();
        try {
            TargetServicesUtil.validateIfSingleResult(models, SizeTypeModel.class, SizeTypeModel.CODE,
                    code);
            return models.get(0);
        }
        catch (final TargetUnknownIdentifierException e) {
            LOG.error(e.getMessage(), e);
        }
        catch (final TargetAmbiguousIdentifierException e) {
            LOG.error(e.getMessage(), e);
        }

        return null;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.product.dao.TargetSizeTypeDao#getDefaultSizeType()
     */
    @Override
    public SizeTypeModel getDefaultSizeType() {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_SIZETYPE_DEFAULT);
        query.addQueryParameter("isDefault", Boolean.TRUE);
        final List<SizeTypeModel> defaultSize = flexibleSearchService.<SizeTypeModel> search(query).getResult();

        if (CollectionUtils.isEmpty(defaultSize)) {
            return null;
        }

        try {
            TargetServicesUtil.validateIfSingleResult(defaultSize, SizeTypeModel.class, SizeTypeModel.ISDEFAULT,
                    Boolean.TRUE.toString());
            return defaultSize.get(0);
        }
        catch (final TargetUnknownIdentifierException e) {
            LOG.error(e.getMessage(), e);
        }
        catch (final TargetAmbiguousIdentifierException e) {
            LOG.error(e.getMessage(), e);
        }

        return null;
    }

    /**
     * Sets the flexible search service.
     * 
     * @param flexibleSearchService
     *            the flexibleSearchService to set
     */
    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

}
