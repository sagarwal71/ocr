/**
 * 
 */
package au.com.target.tgtcore.product;

import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * @author pthoma20
 * 
 *         This class is used for updating the express delivery mode in the target product model which is passed as an
 *         argument along with the targetMerchDepartmentModel.
 * 
 */
public interface TargetProductDeliveryModeService {



    /**
     * Processes the Delivery Mode to check whether it needs an updation
     * 
     * @param targetProductModel
     * @param targetMerchDepModel
     * @return isUpdateRequired
     */
    public boolean processExpressDeliveryModeUpdation(final TargetProductModel targetProductModel,
            final TargetMerchDepartmentModel targetMerchDepModel);


}
