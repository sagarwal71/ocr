package au.com.target.tgtcore.product.dao.impl;

import au.com.target.tgtcore.genericsearch.TargetGenericSearchService;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.product.dao.ProductTypeDao;
import de.hybris.platform.core.GenericCondition;
import de.hybris.platform.core.GenericQuery;
import de.hybris.platform.core.GenericSearchField;
import org.springframework.beans.factory.annotation.Required;

/**
 * Default implementation for {@link ProductTypeDao}.
 */
public class TargetProductTypeDaoImpl implements ProductTypeDao {

    private TargetGenericSearchService searchService;

    @Override
    public ProductTypeModel getByCode(final String code) {
        final GenericQuery query = new GenericQuery(ProductTypeModel._TYPECODE);
        query.addCondition(GenericCondition.equals(new GenericSearchField(ProductTypeModel.CODE), code));
        return getSearchService().getSingleResult(query);
    }

    /**
     * Returns the internally used search service.
     *
     * @return the search service
     */
    public TargetGenericSearchService getSearchService() {
        return searchService;
    }

    /**
     * Sets the search service for internal use.
     *
     * @param searchService the search service to set
     */
    @Required
    public void setSearchService(final TargetGenericSearchService searchService) {
        this.searchService = searchService;
    }
}
