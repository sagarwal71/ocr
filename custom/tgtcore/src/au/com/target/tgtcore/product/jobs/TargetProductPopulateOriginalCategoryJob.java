/**
 * 
 */
package au.com.target.tgtcore.product.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.product.impl.TargetOriginalCategoryPopulatorServiceImpl;


public class TargetProductPopulateOriginalCategoryJob extends AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(TargetProductPopulateOriginalCategoryJob.class);

    private TargetOriginalCategoryPopulatorServiceImpl targetOriginalCategoryPopulatorService;

    /**
     * Job to update original category field for all products based on certain conditions.
     * 
     * It is one time task
     */
    @Override
    public PerformResult perform(final CronJobModel cronJob) {
        LOG.info("TGT-ORIG-CATEGORY-POPULATE: Starting Cronjob for populating OriginalCategory");

        try {
            targetOriginalCategoryPopulatorService.populateOriginalCategories();
        }
        catch (final Exception e) {
            LOG.error("TGT-ORIG-CATEGORY-POPULATE: Exception while executing the update Job", e);
            return new PerformResult(CronJobResult.ERROR, CronJobStatus.FINISHED);
        }

        LOG.info("TGT-ORIG-CATEGORY-POPULATE: Finished successfully: TargetProductPopulateOriginalCategoryJob");
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

    }

    /**
     * @param targetOriginalCategoryPopulatorService
     *            the targetOriginalCategoryPopulatorService to set
     */
    @Required
    public void setTargetOriginalCategoryPopulatorService(
            final TargetOriginalCategoryPopulatorServiceImpl targetOriginalCategoryPopulatorService) {
        this.targetOriginalCategoryPopulatorService = targetOriginalCategoryPopulatorService;
    }

}
