/**
 * TargetProductMerchDepartmentDaoImpl
 */
package au.com.target.tgtcore.product.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.dao.TargetProductMerchDepartmentDao;
import au.com.target.tgtcore.product.data.TargetProductCountMerchDepRowData;


/**
 * @author bhuang3
 * 
 */
public class TargetProductMerchDepartmentDaoImpl implements TargetProductMerchDepartmentDao {

    private FlexibleSearchService flexibleSearchService;

    @Override
    public List<TargetProductCountMerchDepRowData> findProductCountForEachMerchDep() {

        final StringBuilder query = new StringBuilder();
        query.append("SELECT {dep:" + TargetMerchDepartmentModel.PK + "}, COUNT({variantProducts:"
                + TargetColourVariantProductModel.PK
                + "})");
        query.append(" FROM {" + TargetColourVariantProductModel._TYPECODE + " AS variantProducts JOIN "
                + TargetProductModel._TYPECODE + " AS products ");
        query.append(" ON {variantProducts:"
                + TargetColourVariantProductModel.BASEPRODUCT + "}={products:" + TargetProductModel.PK + "}");
        query.append(" JOIN "
                + TargetMerchDepartmentModel._TYPECODE + " AS dep");
        query.append(" ON {products:" + TargetProductModel.MERCHDEPARTMENT + "} = {dep:"
                + TargetMerchDepartmentModel.PK + "} }");
        query.append(" GROUP BY {dep:" + TargetMerchDepartmentModel.PK + "}");
        query.append(" ORDER BY {dep:" + TargetMerchDepartmentModel.PK + "}");
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query.toString());
        fQuery.setResultClassList(Arrays.asList(TargetMerchDepartmentModel.class, Integer.class));
        final SearchResult<List<?>> searchResult = flexibleSearchService.search(fQuery);
        final List<List<?>> result = searchResult.getResult();

        final List<TargetProductCountMerchDepRowData> resultList = new ArrayList<>();
        for (final List<?> row : result) {
            final TargetProductCountMerchDepRowData rowData = new TargetProductCountMerchDepRowData();
            rowData.setTargetMerchDepartmentModel((TargetMerchDepartmentModel)row.get(0));
            rowData.setProductCount(((Integer)row.get(1)).intValue());
            resultList.add(rowData);
        }
        return resultList;
    }

    /**
     * @param flexibleSearchService
     *            the flexibleSearchService to set
     */
    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }


}
