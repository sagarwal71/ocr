/**
 * 
 */
package au.com.target.tgtcore.ordercancel.denialstrategies;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.ordercancel.OrderCancelDenialReason;
import de.hybris.platform.ordercancel.OrderCancelDenialStrategy;
import de.hybris.platform.ordercancel.impl.denialstrategies.AbstractCancelDenialStrategy;
import de.hybris.platform.ordercancel.model.OrderCancelConfigModel;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.voucher.service.TargetVoucherService;


/**
 * @author dcwillia
 * 
 */
public class FlybuysDiscCancelDenialStrategy extends AbstractCancelDenialStrategy implements
        OrderCancelDenialStrategy {
    private TargetVoucherService targetVoucherService;

    /* (non-Javadoc)
     * @see de.hybris.platform.ordercancel.OrderCancelDenialStrategy#getCancelDenialReason(de.hybris.platform.ordercancel.model.OrderCancelConfigModel, de.hybris.platform.core.model.order.OrderModel, de.hybris.platform.core.model.security.PrincipalModel, boolean, boolean)
     */
    @Override
    public OrderCancelDenialReason getCancelDenialReason(final OrderCancelConfigModel configuration,
            final OrderModel order, final PrincipalModel requestor, final boolean partialCancel,
            final boolean partialEntryCancel) {

        if (targetVoucherService.getFlybuysDiscountForOrder(order) != null) {
            return getReason();
        }
        return null;
    }

    /**
     * @param targetVoucherService
     *            the targetVoucherService to set
     */
    @Required
    public void setTargetVoucherService(final TargetVoucherService targetVoucherService) {
        this.targetVoucherService = targetVoucherService;
    }
}
