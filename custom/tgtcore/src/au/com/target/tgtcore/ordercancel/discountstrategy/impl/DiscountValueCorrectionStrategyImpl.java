/**
 * 
 */
package au.com.target.tgtcore.ordercancel.discountstrategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.promotions.model.AbstractPromotionActionModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TMDiscountProductPromotionModel;
import au.com.target.tgtcore.model.TMDiscountPromotionModel;
import au.com.target.tgtcore.ordercancel.discountstrategy.DiscountValueCorrectionStrategy;
import au.com.target.tgtcore.ordercancel.discountstrategy.PromotionResultCorrectionStrategy;


/**
 * @author rmcalave
 * 
 */
public class DiscountValueCorrectionStrategyImpl implements DiscountValueCorrectionStrategy {

    private PromotionResultCorrectionStrategy promotionResultCorrectionStrategy;

    /**
     * This process will always provide maximum benefit to the customer.
     * 
     * Where TMD has been applied, TMD discount values are not considered, removed or changed.
     * 
     * Where the number of discount values is greater than the item quantity on the order entry, the least beneficial
     * discount values are removed. Then, the apportioned discount value for all remaining discount values is
     * recalculated based on the new number of items.
     * 
     * @see au.com.target.tgtcore.ordercancel.discountstrategy.DiscountValueCorrectionStrategy#correctDiscountValues(de.hybris.platform.core.model.order.AbstractOrderEntryModel,
     *      long)
     */
    @Override
    public void correctDiscountValues(final AbstractOrderEntryModel orderEntry, final long previousQuantity) {
        if (CollectionUtils.isEmpty(orderEntry.getDiscountValues())) {
            return;
        }

        // Pull out TMD discount values and set them aside
        final Set<PromotionResultModel> promotionResults = orderEntry.getOrder().getAllPromotionResults();
        final List<DiscountValue> discountValues = new ArrayList<DiscountValue>(orderEntry.getDiscountValues());
        final List<DiscountValue> oldTmdDiscountValues = new ArrayList<>();
        final List<DiscountValue> newTmdDiscountValues = new ArrayList<>();

        final BigDecimal newTmdRate = pullOutTMDDiscountValues(promotionResults,
                discountValues,
                oldTmdDiscountValues,
                newTmdDiscountValues);


        // If there's more discount values than the quantity of items we're going to remove the excess.
        final int numberOfDiscountsToRemove = discountValues.size() - orderEntry.getQuantity().intValue();
        if (numberOfDiscountsToRemove > 0) {
            removeExcessDiscounts(orderEntry, numberOfDiscountsToRemove, discountValues);
        }


        // Case of old TMD discounts - should not be any other deals or new TMD discounts
        if (!oldTmdDiscountValues.isEmpty()) {

            final List<DiscountValue> newOldTmdDiscountValues = new ArrayList<>();
            if (orderEntry.getQuantity().intValue() > 0) {
                for (final DiscountValue tmdDiscountValue : oldTmdDiscountValues) {
                    promotionResultCorrectionStrategy.correctPromotionResultRecords(tmdDiscountValue.getCode(),
                            orderEntry, tmdDiscountValue.getValue(), orderEntry.getQuantity().intValue(), true);
                    newOldTmdDiscountValues.add(tmdDiscountValue); // Add the TMD discount values unchanged to the new discount list    
                }
            }
            else {
                for (final DiscountValue tmdDiscountValue : oldTmdDiscountValues) {
                    promotionResultCorrectionStrategy.correctPromotionResultRecords(tmdDiscountValue.getCode(),
                            orderEntry, 0, 0, true);
                }
            }

            orderEntry.setDiscountValues(newOldTmdDiscountValues);

            return;
        }

        // discountValues should now only have deal discount values
        final List<DiscountValue> newDealDiscountValues = updateDealDiscountValues(discountValues,
                orderEntry.getQuantity().intValue(),
                previousQuantity);


        final List<DiscountValue> allNewDiscountValues = new ArrayList<>();
        allNewDiscountValues.addAll(newDealDiscountValues);

        // Now do new TMD - need to apply rate to the new unit price after deals        
        if (CollectionUtils.isNotEmpty(newTmdDiscountValues) && newTmdRate != null) {
            // There should only be one newTmd per order entry
            final DiscountValue oldTmdDiscountValue = newTmdDiscountValues.get(0);
            if (orderEntry.getQuantity().intValue() > 0) {
                final double newUnitPriceAfterDeals = calculateUnitPriceAfterDeals(orderEntry, newDealDiscountValues);

                // There should only be one newTmd per order entry
                final DiscountValue newTmdDiscountValue = new DiscountValue(oldTmdDiscountValue.getCode(),
                        newUnitPriceAfterDeals * newTmdRate.doubleValue(),
                        oldTmdDiscountValue.isAbsolute(),
                        oldTmdDiscountValue.getCurrencyIsoCode());

                promotionResultCorrectionStrategy.correctPromotionResultRecords(newTmdDiscountValue.getCode(),
                        orderEntry,
                        newTmdDiscountValue.getValue(), orderEntry.getQuantity().intValue(), false);

                allNewDiscountValues.add(newTmdDiscountValue);
            }
            else {
                promotionResultCorrectionStrategy.correctPromotionResultRecords(oldTmdDiscountValue.getCode(),
                        orderEntry, 0, 0, false);
            }
        }

        orderEntry.setDiscountValues(allNewDiscountValues);
    }


    /**
     * Calculate the price PER UNIT after deals applied, given the deal discount values against the order entry
     * 
     * @param orderEntry
     * @param dealDiscountValues
     * @return unit deal price
     */
    protected double calculateUnitPriceAfterDeals(final AbstractOrderEntryModel orderEntry,
            final List<DiscountValue> dealDiscountValues) {

        if (orderEntry.getBasePrice() == null) {
            return 0;
        }

        double sumDiscountValues = 0;
        for (final DiscountValue dealDiscountValue : dealDiscountValues) {
            sumDiscountValues += dealDiscountValue.getValue();
        }

        return (orderEntry.getBasePrice().doubleValue() - sumDiscountValues);
    }

    /**
     * Pulls oldTMD and newTMD discount values from input list and populate them into separate lists.<br/>
     * Returns rate of new TMD if a newTMD promotion is applied for the order entry involved.
     * 
     * @param promotionResults
     * @param discountValues
     * @param oldTmdDiscountValues
     * @param newTmdDiscountValues
     * @return rate of new TMD applied if there is one on the order entry
     */
    protected BigDecimal pullOutTMDDiscountValues(final Set<PromotionResultModel> promotionResults,
            final List<DiscountValue> discountValues,
            final List<DiscountValue> oldTmdDiscountValues,
            final List<DiscountValue> newTmdDiscountValues) {

        BigDecimal newTmdRate = null;

        for (final PromotionResultModel promotionResult : promotionResults) {

            if (promotionResult.getPromotion() instanceof TMDiscountPromotionModel) {

                final Collection<AbstractPromotionActionModel> actions = promotionResult.getActions();
                for (final AbstractPromotionActionModel action : actions) {
                    for (final DiscountValue discountValue : discountValues) {
                        if (discountValue.getCode().equals(action.getGuid())) {
                            oldTmdDiscountValues.add(discountValue);
                        }
                    }
                    discountValues.removeAll(oldTmdDiscountValues);
                }
            }
            else if (promotionResult.getPromotion() instanceof TMDiscountProductPromotionModel) {

                final Collection<AbstractPromotionActionModel> actions = promotionResult.getActions();
                for (final AbstractPromotionActionModel action : actions) {
                    for (final DiscountValue discountValue : discountValues) {
                        if (discountValue.getCode().equals(action.getGuid())) {
                            newTmdDiscountValues.add(discountValue);
                            final TMDiscountProductPromotionModel promotion = (TMDiscountProductPromotionModel)promotionResult
                                    .getPromotion();
                            newTmdRate = BigDecimal.valueOf(promotion.getPercentageDiscount().doubleValue()).divide(
                                    BigDecimal.valueOf(100));
                        }
                    }
                    discountValues.removeAll(newTmdDiscountValues);
                }

            }
        }

        return newTmdRate;
    }


    /**
     * We need to sort the discount values by least discount to most discount before we remove some to make sure the
     * customer gets the most benefit.
     * 
     * @param orderEntry
     * @param numberOfDiscountsToRemove
     * @param discountValues
     */
    private void removeExcessDiscounts(final AbstractOrderEntryModel orderEntry,
            final int numberOfDiscountsToRemove,
            final List<DiscountValue> discountValues) {

        Collections.sort(discountValues, new Comparator<DiscountValue>() {

            /* (non-Javadoc)
             * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
             */
            @Override
            public int compare(final DiscountValue o1, final DiscountValue o2) {
                return Double.compare(o1.getValue(), o2.getValue());
            }

        });

        final List<DiscountValue> discountsToRemove = discountValues.subList(0, numberOfDiscountsToRemove);
        for (final DiscountValue discountToRemove : discountsToRemove) {
            promotionResultCorrectionStrategy.correctPromotionResultRecords(discountToRemove.getCode(), orderEntry,
                    0, 0, true);
        }

        discountsToRemove.clear();
    }


    /**
     * Discount value amounts are apportioned across the full quantity, whether they're all included in the deal or not,
     * so we need to recalculate the discount values based on the new quantity.
     * 
     * @param discountValues
     * @param newQuantity
     * @param previousQuantity
     * @return updated list of DiscountValue
     */
    protected List<DiscountValue> updateDealDiscountValues(final List<DiscountValue> discountValues,
            final int newQuantity, final long previousQuantity) {

        // discountValues should now only have deal discount values
        // Discount value amounts are apportioned across the full quantity, whether they're all included in the deal or not,
        // so we need to recalculate the discount values based on the new quantity.
        final List<DiscountValue> newDealDiscountValues = new ArrayList<>();
        for (final DiscountValue discountValue : discountValues) {
            double value = discountValue.getValue();

            value = (value * previousQuantity) / newQuantity;

            newDealDiscountValues.add(new DiscountValue(discountValue.getCode(), value, discountValue.isAbsolute(),
                    discountValue.getCurrencyIsoCode()));
        }

        return newDealDiscountValues;
    }


    /**
     * @param promotionResultCorrectionStrategy
     *            the promotionResultCorrectionStrategy to set
     */
    @Required
    public void setPromotionResultCorrectionStrategy(
            final PromotionResultCorrectionStrategy promotionResultCorrectionStrategy) {
        this.promotionResultCorrectionStrategy = promotionResultCorrectionStrategy;
    }
}
