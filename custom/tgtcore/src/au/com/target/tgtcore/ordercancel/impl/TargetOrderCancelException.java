package au.com.target.tgtcore.ordercancel.impl;

/**
 * Runtime exception related to order cancel.
 */
public class TargetOrderCancelException extends RuntimeException {

    /**
     * @param message
     * @param cause
     */
    public TargetOrderCancelException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     */
    public TargetOrderCancelException(final String message) {
        super(message);
    }

}
