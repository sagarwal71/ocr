/**
 * 
 */
package au.com.target.tgtcore.ordercancel.impl;

import de.hybris.platform.basecommerce.enums.OrderEntryStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeValueModel;
import de.hybris.platform.jalo.ConsistencyCheckException;
import de.hybris.platform.jalo.order.Order;
import de.hybris.platform.jalo.order.price.Discount;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRecordsHandler;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.OrderCancelResponse;
import de.hybris.platform.ordercancel.OrderCancelResponse.ResponseStatus;
import de.hybris.platform.ordercancel.OrderStatusChangeStrategy;
import de.hybris.platform.ordercancel.OrderUtils;
import de.hybris.platform.ordercancel.exceptions.OrderCancelRecordsHandlerException;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.tx.Transaction;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;
import de.hybris.platform.voucher.model.VoucherModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.TargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.order.TargetDiscountService;
import au.com.target.tgtcore.ordercancel.TargetCancelService;
import au.com.target.tgtcore.ordercancel.discountstrategy.DiscountValueCorrectionStrategy;
import au.com.target.tgtcore.stock.TargetStockService;


public class TargetCancelServiceImpl extends AbstractBusinessService implements TargetCancelService {

    private static final Logger LOG = Logger.getLogger(TargetCancelServiceImpl.class.getName());

    private OrderCancelRecordsHandler orderCancelRecordsHandler;
    private OrderStatusChangeStrategy completeCancelStatusChangeStrategy;
    private OrderStatusChangeStrategy partialCancelStatusChangeStrategy;
    private TargetStockService targetStockService;
    private WarehouseService warehouseService;

    private CalculationService calculationService;
    private SearchRestrictionService searchRestrictionService;
    private TargetDiscountService targetDiscountService;
    private TargetDeliveryService targetDeliveryService;

    private DiscountValueCorrectionStrategy discountValueCorrectionStrategy;

    private CommonI18NService commonI18NService;

    @Override
    public void updateOrderStatus(final OrderCancelRecordEntryModel cancelRequestRecordEntry, final OrderModel order) {
        if (!OrderUtils.hasLivingEntries(order)) {
            //Complete Cancel
            if (completeCancelStatusChangeStrategy != null) {
                completeCancelStatusChangeStrategy.changeOrderStatusAfterCancelOperation(cancelRequestRecordEntry,
                        true);
            }
        }
        else {
            //Partial Cancel
            if (partialCancelStatusChangeStrategy != null) {
                partialCancelStatusChangeStrategy.changeOrderStatusAfterCancelOperation(cancelRequestRecordEntry,
                        true);
            }
        }
    }

    @Override
    public boolean isExtractCancelToWarehouseRequired(final OrderModel order) {
        return false;
    }

    @Override
    public void modifyOrderAccordingToRequest(final OrderCancelRequest cancelRequest) throws OrderCancelException {
        modifyOrderAccordingToRequest(cancelRequest, cancelRequest.getOrder());
        updateConsignmentStatus(cancelRequest);
    }

    @Override
    public void modifyOrderAccordingToRequest(final OrderCancelRequest cancelRequest, final OrderModel order)
            throws OrderCancelException {
        for (final OrderCancelEntry oce : cancelRequest.getEntriesToCancel()) {
            final AbstractOrderEntryModel orderEntry = getOrderEntry(oce, order);
            final long previousQuantity = orderEntry.getQuantity().longValue();

            if (OrderEntryStatus.DEAD.equals(orderEntry.getQuantityStatus())) {
                continue;
            }

            if (oce.getCancelQuantity() <= previousQuantity) {
                for (final TaxValue tax : orderEntry.getTaxValues()) {
                    tax.unapply();
                }
                orderEntry.setQuantity(Long.valueOf(previousQuantity - oce.getCancelQuantity()));

                if (previousQuantity == oce.getCancelQuantity()) {
                    orderEntry.setQuantityStatus(OrderEntryStatus.DEAD);
                }

                discountValueCorrectionStrategy.correctDiscountValues(orderEntry, previousQuantity);

                orderEntry.setCalculated(Boolean.FALSE);
                getModelService().save(orderEntry);
            }
            else {
                throw new OrderCancelException(cancelRequest.getOrder().getCode(), "Trying to cancel "
                        + oce.getCancelQuantity()
                        + ", whereas orderEntry (" + orderEntry.getPk() + ") has quantity of " + previousQuantity);
            }
        }
    }

    protected WarehouseModel getOnlineWarehouse() {
        WarehouseModel warehouseModel = null;
        final List<WarehouseModel> defWarehouses = warehouseService.getDefWarehouse();
        if (CollectionUtils.isNotEmpty(defWarehouses)) {
            warehouseModel = defWarehouses.get(0);
        }
        else {
            LOG.warn("Default warehouse not found to release the allocated stock");
        }
        return warehouseModel;
    }

    protected AbstractOrderEntryModel getOrderEntry(final OrderCancelEntry oce, final AbstractOrderModel order) {
        final AbstractOrderEntryModel orderEntry = oce.getOrderEntry();
        AbstractOrderEntryModel ret = null;
        for (final AbstractOrderEntryModel original : order.getEntries()) {
            if (original.equals(orderEntry)) {
                ret = original;
                break;
            }
        }
        return ret;
    }

    @Override
    public void updateConsignmentStatus(final OrderCancelRequest cancelRequest) {
        // do nothing here. Impl has been placed in TargetFulfilmentCancelService
    }

    @Override
    public void releaseStock(final OrderCancelRequest cancelRequest) throws OrderCancelException {
        for (final OrderCancelEntry oce : cancelRequest.getEntriesToCancel()) {
            final AbstractOrderEntryModel orderEntry = oce.getOrderEntry();
            final ProductModel product = orderEntry.getProduct();
            final int amount = (int)oce.getCancelQuantity();
            targetStockService.release(product, amount, amount
                    + " stock has been released for " + product.getName());
        }
    }

    @Override
    public void releaseAllStockForOrder(final OrderModel order) {
        Assert.notNull(order, "Order model must not be null");

        for (final AbstractOrderEntryModel orderEntry : order.getEntries()) {
            final ProductModel product = orderEntry.getProduct();
            if (null == orderEntry.getQuantity()) {
                continue;
            }

            final int amount = (int)orderEntry.getQuantity().longValue();

            // Cancelled order entries will have qty=0, so don't release those
            if (amount > 0) {
                targetStockService.release(product, amount, amount
                        + " stock has been released for " + product.getName());
            }
        }
    }

    @Override
    public OrderCancelResponse makeInternalResponse(final OrderCancelRequest request, final boolean success,
            final String message) {
        final List<OrderCancelEntry> cancelEntries = new ArrayList<>();
        for (final OrderCancelEntry cancelEntry : request.getEntriesToCancel()) {
            if (cancelEntry.getCancelQuantity() > 0) {
                cancelEntries.add(cancelEntry);
            }
        }
        ResponseStatus status = null;
        if (request.isPartialCancel()) {
            status = success ? ResponseStatus.partial : ResponseStatus.error;
        }
        else {
            status = success ? ResponseStatus.full : ResponseStatus.error;
        }
        return new OrderCancelResponse(request.getOrder(), cancelEntries, status, message);
    }

    @Override
    public void recalculateOrder(final OrderModel order) {
        Assert.notNull(order, "Order cannot be null");

        try {
            searchRestrictionService.disableSearchRestrictions();

            // See https://wiki.hybris.com/display/forum/Promotion+transfer+from+cart+to+order
            // This is required for the promotionsService.updatePromotions to work within transactions
            final Transaction transaction = Transaction.current();
            transaction.enableDelayedStore(false);
            transaction.flushDelayedStore();

            order.setCalculated(Boolean.FALSE);
            calculationService.calculateTotals(order, false);
        }
        catch (final CalculationException e) {
            LOG.error(e);
            throw new TargetOrderCancelException("Cannot calculate order for refund", e);
        }
        finally {
            searchRestrictionService.enableSearchRestrictions();
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.ordercancel.TargetCancelService#recalculateOrderPreventingIncrease(de.hybris.platform.core.model.order.OrderModel, de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel)
     */
    @Override
    public void recalculateOrderPreventingIncrease(final OrderModel order,
            final OrderCancelRecordEntryModel cancelRequestRecordEntry) {
        recalculateOrderPreventingIncrease(order, false, 0d, cancelRequestRecordEntry);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.ordercancel.TargetCancelService#previewOrderPreventingIncrease(de.hybris.platform.core.model.order.OrderModel, double)
     */
    @Override
    public void previewOrderPreventingIncrease(final OrderModel order, final double deliveryCostRefund) {
        recalculateOrderPreventingIncrease(order, true, deliveryCostRefund, null);
    }

    /**
     * recalculate as a result of this refund, if the order value goes up (ie deal broken) then apply a discount to the
     * order to bring it back to the same price
     * 
     * @param order
     *            the order
     * @param preview
     *            the preview
     * @param previewDeliveryCostRefund
     *            the preview delivery cost refund
     * @param cancelRequestRecordEntry
     *            the cancel request record entry
     */
    @SuppressWarnings("deprecation")
    private void recalculateOrderPreventingIncrease(final OrderModel order, final boolean preview,
            final double previewDeliveryCostRefund, final OrderCancelRecordEntryModel cancelRequestRecordEntry) {
        final double previousTotalPrice = order.getTotalPrice().doubleValue();
        recalculateOrder(order);
        if (null != cancelRequestRecordEntry) {
            updateShippingAmountForCancel(order, cancelRequestRecordEntry);
            recalculateOrder(order);
        }
        double newTotalPrice = order.getTotalPrice().doubleValue();
        newTotalPrice -= previewDeliveryCostRefund;
        if (newTotalPrice > previousTotalPrice) {
            addNewGlobalDiscountToOrder(order, newTotalPrice - previousTotalPrice, preview);
            recalculateOrder(order);
        }
        else if (newTotalPrice < 0) {
            final Order orderSource = getModelService().getSource(order);
            final List<DiscountValue> discountValues = orderSource.getGlobalDiscountValues();
            if (CollectionUtils.isNotEmpty(discountValues)) {
                for (final DiscountValue discountValue : discountValues) {
                    final DiscountModel discountModel = targetDiscountService
                            .getDiscountForCode(discountValue.getCode());
                    if (!(discountModel instanceof VoucherModel)) {
                        final double oldDiscValue = discountValue.getAppliedValue();
                        final double reqDiscValue = oldDiscValue + newTotalPrice;
                        if (reqDiscValue >= 0d) {

                            // cleanup the old voucher
                            orderSource.removeGlobalDiscountValue(discountValue);
                            try {
                                orderSource.calculateTotals(true);
                            }
                            catch (final JaloPriceFactoryException e) {
                                LOG.error("Failed to add discount to order during a refund", e);
                            }

                            // create and apply the new discount
                            if (reqDiscValue > 0) {
                                addNewGlobalDiscountToOrder(order, reqDiscValue, preview);
                                recalculateOrder(order);
                            }
                            else {
                                getModelService().refresh(order);
                            }
                            break;
                        }
                    }
                }

            }

        }
    }

    @Override
    public boolean willCancellationIncreaseOrderValue(final OrderModel order) {
        final double previousTotalPrice = order.getTotalPrice().doubleValue();
        recalculateOrder(order);
        final double newTotalPrice = order.getTotalPrice().doubleValue();
        return newTotalPrice > previousTotalPrice;
    }

    @SuppressWarnings("deprecation")
    private void addNewGlobalDiscountToOrder(final OrderModel order, final double amt, final boolean preview) {
        final DiscountModel discountModel = targetDiscountService.createGlobalDiscountForOrder(order, amt, "Cancel"
                + order.getCode());
        final Discount discount = getModelService().getSource(discountModel);
        final Order orderSource = getModelService().getSource(order);
        orderSource.addGlobalDiscountValue(discount.getDiscountValue(orderSource));
        try {
            orderSource.calculateTotals(true);
            getModelService().refresh(order);
        }
        catch (final JaloPriceFactoryException e) {
            LOG.error("Failed to add discount to order during a refund", e);
        }
        finally {
            if (preview) {
                try {
                    discount.remove();
                }
                catch (final ConsistencyCheckException e) {
                    LOG.error("Failed to add cleanup discount for refund preview", e);
                }
            }
        }

    }

    @Override
    public void updateRecordEntry(final OrderCancelRequest orderCancelRequest)
            throws OrderCancelRecordsHandlerException {
        orderCancelRecordsHandler.updateRecordEntry(makeInternalResponse(orderCancelRequest, true, null));
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.ordercancel.TargetCancelService#updateShippingAmountForCancel(de.hybris.platform.core.model.order.OrderModel, de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel)
     */
    @Override
    public void updateShippingAmountForCancel(final OrderModel order,
            final OrderCancelRecordEntryModel cancelRequestRecordEntry) {
        getModelService().refresh(order);

        // if not all physical entries cancelled, than update the shipping cost as required
        if (!areAllPhysicalEntriesCancelled(order)) {
            final AbstractTargetZoneDeliveryModeValueModel zoneDeliveryModeValue = order.getZoneDeliveryModeValue();

            double valueOfGoods = 0.0d;
            valueOfGoods = order.getSubtotal().doubleValue()
                    + targetDiscountService.getTotalTMDiscount(order);
            valueOfGoods = commonI18NService.roundCurrency(valueOfGoods,
                    order.getCurrency().getDigits().intValue());

            // If it is TargetZDMV and threshold < orderSubTotal, then return same Delivery Mode.
            if (zoneDeliveryModeValue instanceof TargetZoneDeliveryModeValueModel
                    && Double.valueOf(valueOfGoods).compareTo(zoneDeliveryModeValue.getMinimum()) >= 0) {
                updateShippingAmountForTargetZDMVForPartialCancel(order, cancelRequestRecordEntry,
                        (TargetZoneDeliveryModeValueModel)zoneDeliveryModeValue);
            }
            else {
                // Now we need to evaluate which Delivery Mode Value will be applied
                final ZoneDeliveryModeValueModel newDeliveryMode = targetDeliveryService
                        .getZoneDeliveryModeValueForAbstractOrderAndMode(
                                (ZoneDeliveryModeModel)order.getDeliveryMode(), order);

                // For TargetZDMV, we need to find out number of bulky products in order and apply Delivery Cost accordingly
                if (newDeliveryMode instanceof TargetZoneDeliveryModeValueModel) {
                    // Setting newDeliveryMode in order
                    order.setZoneDeliveryModeValue((TargetZoneDeliveryModeValueModel)newDeliveryMode);
                    updateShippingAmountForTargetZDMVForPartialCancel(order, cancelRequestRecordEntry,
                            (TargetZoneDeliveryModeValueModel)newDeliveryMode);
                }
                // If newDeliveryMode is RestrictableZDMV
                else if (newDeliveryMode instanceof RestrictableTargetZoneDeliveryModeValueModel) {
                    updateShippingAmountForRestrictableTargetZDMVForPartialCancel(order, cancelRequestRecordEntry,
                            (RestrictableTargetZoneDeliveryModeValueModel)newDeliveryMode);
                }
            }
        }
        // otherwise remove full delivery cost
        else {
            cancelRequestRecordEntry.setRefundedShippingAmount(order.getDeliveryCost());
            order.setDeliveryCost(Double.valueOf(0.0d));
            getModelService().saveAll(order, cancelRequestRecordEntry);
        }
    }

    /**
     * Checks if is updated order digital only.
     *
     * @param abstractOrder
     *            the abstract order
     * @return true, if only digital entries are active
     */
    protected boolean areAllPhysicalEntriesCancelled(final AbstractOrderModel abstractOrder) {

        if (CollectionUtils.isEmpty(abstractOrder.getEntries())) {
            return true;
        }

        for (final AbstractOrderEntryModel entry : abstractOrder.getEntries()) {
            final TargetZoneDeliveryModeModel deliveryMode = (TargetZoneDeliveryModeModel)entry
                    .getDeliveryMode();

            if (null != entry.getQuantity() && entry.getQuantity().longValue() > 0l) {
                if (null == deliveryMode || BooleanUtils.isFalse(deliveryMode.getIsDigital())) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Update shipping amount for restrictable zone delivery mode for partial cancel.
     * 
     * @param order
     *            the order
     * @param cancelRequestRecordEntry
     *            the cancel request record entry
     * @param zoneDeliveryModeValue
     *            the zone delivery mode value
     */
    private void updateShippingAmountForRestrictableTargetZDMVForPartialCancel(final OrderModel order,
            final OrderCancelRecordEntryModel cancelRequestRecordEntry,
            final RestrictableTargetZoneDeliveryModeValueModel zoneDeliveryModeValue) {
        if (null != zoneDeliveryModeValue
                && zoneDeliveryModeValue.getValue().compareTo(order.getDeliveryCost()) < 0) {
            order.setZoneDeliveryModeValue(zoneDeliveryModeValue);
            double refundShippingAmount = order.getDeliveryCost().doubleValue()
                    - zoneDeliveryModeValue.getValue().doubleValue();
            refundShippingAmount = commonI18NService.roundCurrency(refundShippingAmount, order.getCurrency()
                    .getDigits().intValue());
            cancelRequestRecordEntry.setRefundedShippingAmount(Double.valueOf(refundShippingAmount));
            getModelService().saveAll(order, cancelRequestRecordEntry);
        }
    }

    /**
     * Update shipping amount for target zone delivery mode for partial cancel.
     * 
     * @param order
     *            the order
     * @param cancelRequestRecordEntry
     *            the cancel request record entry
     * @param zoneDeliveryModeValue
     *            the zone delivery mode value
     */
    private void updateShippingAmountForTargetZDMVForPartialCancel(final OrderModel order,
            final OrderCancelRecordEntryModel cancelRequestRecordEntry,
            final TargetZoneDeliveryModeValueModel zoneDeliveryModeValue) {
        int bulkyProducts = 0;

        // Calculate number of Bulky products in the order
        for (final AbstractOrderEntryModel orderEntry : order.getEntries()) {
            final AbstractTargetVariantProductModel productModel = (AbstractTargetVariantProductModel)orderEntry
                    .getProduct();
            if (null != productModel && productModel.getProductType().getBulky().booleanValue()) {
                bulkyProducts += orderEntry.getQuantity().intValue();
            }
            if (bulkyProducts > TgtCoreConstants.EXTENDED_BULKY_THRESHOLD) {
                break;
            }
        }

        // Extended Bulky Delivery Cost
        if (bulkyProducts > TgtCoreConstants.EXTENDED_BULKY_THRESHOLD) {
            applyNewShippingAmountAndRefundShipment(order, cancelRequestRecordEntry, zoneDeliveryModeValue
                    .getExtendedBulky());
        }
        // Bulky Delivery Cost
        else if (bulkyProducts > 0) {
            applyNewShippingAmountAndRefundShipment(order, cancelRequestRecordEntry, zoneDeliveryModeValue.getBulky());
        }
        // Normal Delivery Cost
        else {
            applyNewShippingAmountAndRefundShipment(order, cancelRequestRecordEntry, zoneDeliveryModeValue.getValue());
        }
    }

    /**
     * Apply new shipping amount and refund shippment.
     * 
     * @param order
     *            the order
     * @param cancelRequestRecordEntry
     *            the cancel request record entry
     * @param newShippingAmount
     *            the new shipping amount
     */
    private void applyNewShippingAmountAndRefundShipment(final OrderModel order,
            final OrderCancelRecordEntryModel cancelRequestRecordEntry, final Double newShippingAmount) {
        if (newShippingAmount.compareTo(order.getDeliveryCost()) < 0) {
            double refundShippingAmount = order.getDeliveryCost().doubleValue() - newShippingAmount.doubleValue();
            refundShippingAmount = commonI18NService.roundCurrency(refundShippingAmount, order.getCurrency()
                    .getDigits().intValue());
            cancelRequestRecordEntry.setRefundedShippingAmount(Double.valueOf(refundShippingAmount));
            order.setDeliveryCost(newShippingAmount);
            getModelService().saveAll(order, cancelRequestRecordEntry);
        }
    }

    /**
     * @param orderCancelRecordsHandler
     *            the orderCancelRecordsHandler to set
     */
    @Required
    public void setOrderCancelRecordsHandler(final OrderCancelRecordsHandler orderCancelRecordsHandler) {
        this.orderCancelRecordsHandler = orderCancelRecordsHandler;
    }

    /**
     * @param completeCancelStatusChangeStrategy
     *            the completeCancelStatusChangeStrategy to set
     */
    @Required
    public void setCompleteCancelStatusChangeStrategy(
            final OrderStatusChangeStrategy completeCancelStatusChangeStrategy) {
        this.completeCancelStatusChangeStrategy = completeCancelStatusChangeStrategy;
    }

    /**
     * @param partialCancelStatusChangeStrategy
     *            the partialCancelStatusChangeStrategy to set
     */
    public void setPartialCancelStatusChangeStrategy(
            final OrderStatusChangeStrategy partialCancelStatusChangeStrategy) {
        this.partialCancelStatusChangeStrategy = partialCancelStatusChangeStrategy;
    }

    /**
     * @param targetStockService
     *            the targetStockService to set
     */
    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }



    /**
     * @return the targetStockService
     */
    public TargetStockService getTargetStockService() {
        return targetStockService;
    }

    /**
     * @param warehouseService
     *            the warehouseService to set
     */
    @Required
    public void setWarehouseService(final WarehouseService warehouseService) {
        this.warehouseService = warehouseService;
    }

    /**
     * @param calculationService
     *            the calculationService to set
     */
    @Required
    public void setCalculationService(final CalculationService calculationService) {
        this.calculationService = calculationService;
    }

    /**
     * @param searchRestrictionService
     *            the searchRestrictionService to set
     */
    @Required
    public void setSearchRestrictionService(final SearchRestrictionService searchRestrictionService) {
        this.searchRestrictionService = searchRestrictionService;
    }

    /**
     * @param targetDiscountService
     *            the targetDiscountService to set
     */
    @Required
    public void setTargetDiscountService(final TargetDiscountService targetDiscountService) {
        this.targetDiscountService = targetDiscountService;
    }

    /**
     * @param discountValueCorrectionStrategy
     *            the discountValueCorrectionStrategy to set
     */
    @Required
    public void setDiscountValueCorrectionStrategy(
            final DiscountValueCorrectionStrategy discountValueCorrectionStrategy) {
        this.discountValueCorrectionStrategy = discountValueCorrectionStrategy;
    }

    /**
     * @param targetDeliveryService
     *            the targetDeliveryService to set
     */
    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        this.targetDeliveryService = targetDeliveryService;
    }

    /**
     * @param commonI18NService
     *            the commonI18NService to set
     */
    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

}
