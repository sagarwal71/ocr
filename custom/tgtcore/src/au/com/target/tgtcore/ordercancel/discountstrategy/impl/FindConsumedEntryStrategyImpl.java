/**
 * 
 */
package au.com.target.tgtcore.ordercancel.discountstrategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.model.TargetPromotionOrderEntryConsumedModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.ordercancel.discountstrategy.FindConsumedEntryStrategy;


/**
 * Since we are giving the customer the best refund, the strategy is:<br/>
 * a) Create a map with key being the adjusted unit price (AUP), sorted high to low<br/>
 * b) For each AUP, map to a list of consumedEntries, sorted by instance high to low<br/>
 * (the latter to make the invoice neater so we don't take qualifier from one instance and reward from another)
 * 
 */
public class FindConsumedEntryStrategyImpl implements FindConsumedEntryStrategy {

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.ordercancel.discountstrategy.FindConsumedEntryStrategy#findConsumedEntry(de.hybris.platform.promotions.model.PromotionResultModel, de.hybris.platform.core.model.order.AbstractOrderEntryModel)
     */
    @Override
    public PromotionOrderEntryConsumedModel findConsumedEntry(final PromotionResultModel dealResult,
            final AbstractOrderEntryModel orderEntry) {


        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = dealResult.getConsumedEntries();
        if (consumedEntries == null) {
            return null;
        }

        // There may be more than one consumed entry per order entry.<br/>
        // We could have qualifiers with higher AUP than rewards, we want to return one with highest AUP.<br/>
        // They could also be from different instances - if so we want to favour higher instance number to return.<br/>
        // There could be more than one with the same instance and AUP, in which case just return the first one.

        // Using a double for a map key seems dangerous, lets multiply by 100 and truncate to integer for safety

        final SortedMap<Integer, List<TargetPromotionOrderEntryConsumedModel>> aupMap =
                new TreeMap<Integer, List<TargetPromotionOrderEntryConsumedModel>>();

        for (final PromotionOrderEntryConsumedModel consumedEntry : consumedEntries) {
            if (consumedEntry instanceof TargetPromotionOrderEntryConsumedModel
                    && consumedEntry.getOrderEntry() != null
                    && consumedEntry.getOrderEntry().getPk().equals(orderEntry.getPk())) {

                final TargetPromotionOrderEntryConsumedModel targetConsumedEntry = (TargetPromotionOrderEntryConsumedModel)consumedEntry;

                final Integer priceKey = createPriceKey(targetConsumedEntry);
                if (priceKey != null) {

                    if (aupMap.containsKey(priceKey)) {

                        aupMap.get(priceKey).add(targetConsumedEntry);
                    }
                    else {

                        final List<TargetPromotionOrderEntryConsumedModel> consumedTargetEntries = new ArrayList<>();
                        consumedTargetEntries.add(targetConsumedEntry);
                        aupMap.put(priceKey, consumedTargetEntries);
                    }
                }
            }
        }

        // Get the last entry in the map, which will be the list for the highest price, then sort on instance desc
        if (aupMap.isEmpty()) {
            return null;
        }

        final List<TargetPromotionOrderEntryConsumedModel> consumedTargetEntriesForHighestPrice =
                aupMap.get(aupMap.lastKey());

        if (CollectionUtils.isEmpty(consumedTargetEntriesForHighestPrice)) {
            return null;
        }

        Collections.sort(consumedTargetEntriesForHighestPrice,
                new Comparator<TargetPromotionOrderEntryConsumedModel>() {

                    /* (non-Javadoc)
                     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
                     */
                    @Override
                    public int compare(final TargetPromotionOrderEntryConsumedModel o1,
                            final TargetPromotionOrderEntryConsumedModel o2) {
                        return o2.getInstance().compareTo(o1.getInstance());
                    }

                });

        return consumedTargetEntriesForHighestPrice.get(0);

    }


    /**
     * Create an Integer suitable for a map key from the adjusted unit price on the given entry
     * 
     * @param targetConsumedEntry
     * @return Integer
     */
    protected Integer createPriceKey(final TargetPromotionOrderEntryConsumedModel targetConsumedEntry) {

        if (targetConsumedEntry != null && targetConsumedEntry.getAdjustedUnitPrice() != null) {

            final double dAUP = targetConsumedEntry.getAdjustedUnitPrice().doubleValue();
            return Integer.valueOf((int)(dAUP * 100f));
        }

        return null;
    }

}
