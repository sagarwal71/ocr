package au.com.target.tgtcore.jalo;

import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.jalo.synchronization.CatalogVersionSyncCronJob;
import de.hybris.platform.catalog.jalo.synchronization.CatalogVersionSyncScheduleMedia;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.media.Media;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;




public class TargetCatalogVersionSyncCronJob extends CatalogVersionSyncCronJob
{

    private static final Logger LOG = Logger.getLogger(TargetCatalogVersionSyncCronJob.class);


    @SuppressWarnings("deprecation")
    @Override
    protected CatalogVersionSyncScheduleMedia createNewScheduleMedia()
    {
        final List<CatalogVersionSyncScheduleMedia> list = new ArrayList<CatalogVersionSyncScheduleMedia>(
                getScheduleMedias());

        final ItemAttributeMap attributes = new ItemAttributeMap();
        attributes.put(Media.CODE, "sync_schedule_" + System.nanoTime());

        attributes.put(CatalogVersionSyncScheduleMedia.CRONJOB, this);
        final CatalogVersionSyncScheduleMedia ret;

        ret = CatalogManager.getInstance().createCatalogVersionSyncScheduleMedia(attributes);
        try {
            //Using deprecated method because we are not using spring bean factory to create this class
            ret.setData(new byte[0]);
        }
        catch (final JaloBusinessException e) {

            LOG.error("JaloBusinessException", e);
        }

        list.add(ret);
        setScheduleMedias(list);
        return ret;
    }




}
