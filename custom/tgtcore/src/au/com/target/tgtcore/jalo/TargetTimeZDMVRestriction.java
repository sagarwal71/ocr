package au.com.target.tgtcore.jalo;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.jalo.product.Product;

import java.util.Date;


/**
 * The Class TargetTimeZDMVRestriction.
 */
@SuppressWarnings("deprecation")
public class TargetTimeZDMVRestriction extends GeneratedTargetTimeZDMVRestriction {

    /**
     * Evaluates to true if start date comes before the current date, and end date comes after current date in the
     * restriction otherwise return false
     * 
     * @param abstractOrder
     * @return boolean
     */
    @Override
    public boolean evaluate(final AbstractOrderModel abstractOrder) {
        if (abstractOrder instanceof CartModel) {
            return evaluate(new Date());
        }
        return evaluate(abstractOrder.getDate());
    }

    private boolean evaluate(final Date date) {
        final boolean start;
        final boolean end;
        final Date startDate = getActiveFrom();
        final Date endDate = getActiveUntil();
        if (startDate == null && endDate == null) {
            return true;
        }
        else {
            start = (startDate == null || startDate.before(date));
            end = (endDate == null || endDate.after(date));
            return start && end;
        }
    }

    @Override
    public boolean evaluate(final Product product) {
        return evaluate(new Date());
    }

}
