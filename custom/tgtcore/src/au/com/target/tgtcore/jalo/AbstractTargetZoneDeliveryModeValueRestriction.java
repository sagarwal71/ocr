package au.com.target.tgtcore.jalo;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.jalo.product.Product;




/**
 * The Class AbstractTargetZoneDeliveryModeValueRestriction.
 */
@SuppressWarnings("deprecation")
public abstract class AbstractTargetZoneDeliveryModeValueRestriction extends
        GeneratedAbstractTargetZoneDeliveryModeValueRestriction {

    /**
     * Evaluate for the order.
     * 
     * @param paramAbstractOrder
     *            the param abstract order
     * @return true, if successful
     */
    public abstract boolean evaluate(AbstractOrderModel paramAbstractOrder);

    /**
     * Evaluate for the product.
     * 
     * @param product
     *            the product
     * @return true, if successful
     */
    public boolean evaluate(final Product product) {
        return true;
    }
}