/**
 * 
 */
package au.com.target.tgtcore.jalo;

import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloSystemException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import de.hybris.platform.promotions.jalo.AbstractPromotionAction;
import de.hybris.platform.promotions.jalo.PromotionOrderEntryAdjustAction;
import de.hybris.platform.promotions.jalo.PromotionOrderEntryConsumed;
import de.hybris.platform.promotions.jalo.PromotionsManager;
import de.hybris.platform.promotions.jalo.TargetPromotionOrderEntryConsumed;

import java.util.HashMap;
import java.util.Map;

import au.com.target.tgtcore.constants.TgtCoreConstants;


/**
 * @author rsamuel3
 * 
 */
@SuppressWarnings("deprecation")
public class TgtPromotionManager extends PromotionsManager {

    /* (non-Javadoc)
     * @see de.hybris.platform.promotions.jalo.GeneratedPromotionsManager#createPromotionOrderEntryConsumed(de.hybris.platform.jalo.SessionContext, java.util.Map)
     */
    @Override
    public TargetPromotionOrderEntryConsumed createPromotionOrderEntryConsumed(final SessionContext ctx,
            final Map attributeValues)
    {
        try
        {
            final ComposedType type = getTenant().getJaloConnection().getTypeManager()
                    .getComposedType(TgtCoreConstants.TC.TARGETPROMOTIONORDERENTRYCONSUMED);
            return (TargetPromotionOrderEntryConsumed)type.newInstance(ctx, attributeValues);
        }
        catch (final JaloGenericCreationException e)
        {
            final Throwable cause = e.getCause();
            if (cause instanceof RuntimeException) {
                throw (RuntimeException)cause;
            }
            else {
                throw new JaloSystemException(cause, cause.getMessage(), e.getErrorCode()); //NOPMD pmd is wrong, we have the stack trace in the cause
            }
        }
        catch (final JaloBusinessException e)
        {
            throw new JaloSystemException(e, "error creating PromotionOrderEntryConsumed : " + e.getMessage(), 0);
        }
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.promotions.jalo.PromotionsManager#createPromotionOrderEntryAdjustAction(de.hybris.platform.jalo.SessionContext, de.hybris.platform.jalo.order.AbstractOrderEntry, long, double)
     */
    @Override
    public PromotionOrderEntryAdjustAction createPromotionOrderEntryAdjustAction(final SessionContext ctx,
            final AbstractOrderEntry entry, final long quantity, final double adjustment) {
        final Map parameters = new HashMap();
        parameters.put(AbstractPromotionAction.GUID, makeActionGUID());
        parameters.put(PromotionOrderEntryAdjustAction.AMOUNT, Double.valueOf(adjustment));
        parameters.put(PromotionOrderEntryAdjustAction.ORDERENTRYPRODUCT, entry.getProduct(ctx));
        parameters.put(PromotionOrderEntryAdjustAction.ORDERENTRYNUMBER, entry.getEntryNumber());
        parameters.put(PromotionOrderEntryAdjustAction.ORDERENTRYQUANTITY, Long.valueOf(quantity));
        return createPromotionOrderEntryAdjustAction(ctx, parameters);
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.promotions.jalo.PromotionsManager#createPromotionOrderEntryConsumed(de.hybris.platform.jalo.SessionContext, java.lang.String, de.hybris.platform.jalo.order.AbstractOrderEntry, long)
     */
    @Override
    public PromotionOrderEntryConsumed createPromotionOrderEntryConsumed(final SessionContext ctx, final String code,
            final AbstractOrderEntry orderEntry, final long quantity)
    {
        // get the current unit price from the orderEntry
        final double unitPrice = orderEntry.getBasePrice(ctx).doubleValue();

        final Map parameters = new HashMap();
        parameters.put(PromotionOrderEntryConsumed.CODE, code);
        parameters.put(PromotionOrderEntryConsumed.ORDERENTRY, orderEntry);
        parameters.put(PromotionOrderEntryConsumed.QUANTITY, Long.valueOf(quantity));

        // Default value for adjusted unit price is the real unit price
        parameters.put(PromotionOrderEntryConsumed.ADJUSTEDUNITPRICE, Double.valueOf(unitPrice));
        return createPromotionOrderEntryConsumed(ctx, parameters);
    }

    /**
     * Create a new {@link PromotionOrderEntryConsumed} instance.
     * 
     * @param ctx
     *            The session context
     * @param code
     *            The object's code
     * @param orderEntry
     *            The {@link AbstractOrderEntry} that is the base order entry for the object
     * @param quantity
     *            The quantity mapped through from the base order entry
     * @param adjustedUnitPrice
     *            The adjusted unit price
     * @return the new {@link PromotionOrderEntryConsumed}
     */
    @Override
    public PromotionOrderEntryConsumed createPromotionOrderEntryConsumed(final SessionContext ctx, final String code,
            final AbstractOrderEntry orderEntry, final long quantity, final double adjustedUnitPrice)
    {
        final Map parameters = new HashMap();
        parameters.put(PromotionOrderEntryConsumed.CODE, code);
        parameters.put(PromotionOrderEntryConsumed.ORDERENTRY, orderEntry);
        parameters.put(PromotionOrderEntryConsumed.QUANTITY, Long.valueOf(quantity));
        parameters.put(PromotionOrderEntryConsumed.ADJUSTEDUNITPRICE, Double.valueOf(adjustedUnitPrice));
        return createPromotionOrderEntryConsumed(ctx, parameters);
    }


    /**
     * all the attributes including deal type and instance set on the TargetPromotionOrderEntryConsumed
     * 
     * @param ctx
     * @param code
     * @param orderEntry
     * @param quantity
     * @param adjustedUnitPrice
     * @param type
     * @param instance
     * @return TargetOrderEntryConsumed
     */
    public PromotionOrderEntryConsumed createPromotionOrderEntryConsumed(final SessionContext ctx, final String code,
            final AbstractOrderEntry orderEntry, final long quantity, final double adjustedUnitPrice,
            final EnumerationValue type, final Integer instance) {
        final TargetPromotionOrderEntryConsumed orderEntryConsumed = (TargetPromotionOrderEntryConsumed)createPromotionOrderEntryConsumed(
                ctx, code, orderEntry,
                quantity, adjustedUnitPrice);
        orderEntryConsumed.setInstance(instance.intValue());
        orderEntryConsumed.setDealItemType(type);
        return orderEntryConsumed;
    }
}
