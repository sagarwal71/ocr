package au.com.target.tgtcore.jalo;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.model.TargetPostCodeGroupZDMVRestrictionModel;
import au.com.target.tgtcore.postcode.service.TargetPostCodeService;


public class TargetPostCodeGroupZDMVRestriction extends GeneratedTargetPostCodeGroupZDMVRestriction {
    private final ModelService modelService = (ModelService)Registry.getApplicationContext().getBean("modelService");

    private final SessionService sessionService = (SessionService)Registry.getApplicationContext()
            .getBean("sessionService");

    private final TargetPostCodeService targetPostCodeService = (TargetPostCodeService)Registry.getApplicationContext()
            .getBean("targetPostCodeService");

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.jalo.AbstractTargetZoneDeliveryModeValueRestriction#evaluate(de.hybris.platform.jalo.order.AbstractOrder)
     */
    @Override
    public boolean evaluate(final AbstractOrderModel abstractOrder) {
        final TargetPostCodeGroupZDMVRestrictionModel currentRestrictionModel = getRestrictionModel();
        final String postCode = getTargetPostCodeService().getPostalCodeFromCartOrSession(abstractOrder);
        if (StringUtils.isEmpty(postCode)) {
            throw new TargetNoPostCodeException("Post code is not available at the moment");
        }
        boolean result = false;
        if (CollectionUtils.isNotEmpty(currentRestrictionModel.getPostCodeGroup())) {
            result = getTargetPostCodeService().doesPostCodeBelongsToGroups(postCode,
                    currentRestrictionModel.getPostCodeGroup());

        }

        return result;
    }

    /**
     * @return TargetPostCodeGroupZDMVRestrictionModel
     */
    protected TargetPostCodeGroupZDMVRestrictionModel getRestrictionModel() {
        return getModelService().get(this);
    }

    /**
     * @return the modelService
     */
    protected ModelService getModelService() {
        return modelService;
    }

    /**
     * @return the sessionService
     */
    protected SessionService getSessionService() {
        return sessionService;
    }


    /**
     * @return the targetPostCodeService
     */
    public TargetPostCodeService getTargetPostCodeService() {
        return targetPostCodeService;
    }

}
