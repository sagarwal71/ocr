/**
 * 
 */
package au.com.target.tgtcore.jobs.util;

import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.jobs.exceptions.CronJobAbortException;



/**
 * The utility service class to have cronjob related check api's
 * 
 *
 */
public class CronJobUtilityService {

    private static final Logger LOG = Logger.getLogger(CronJobUtilityService.class);

    private static final String SYNC_CRONJOB = "StepSync_targetProductCatalog";

    private CronJobService cronJobService;

    private ModelService modelService;

    /**
     * This method Returns the job Model for the currently running sync job.
     * 
     * @return cronJobModel
     */
    public CronJobModel getRunningSyncJob() {
        final List<CronJobModel> cronJobsList = cronJobService.getRunningOrRestartedCronJobs();

        if (CollectionUtils.isNotEmpty(cronJobsList)) {
            for (final CronJobModel cronJobModel : cronJobsList) {
                if (StringUtils.equalsIgnoreCase(SYNC_CRONJOB, cronJobModel.getJob().getCode()) &&
                        isCronJobActive(cronJobModel)) {
                    return cronJobModel;
                }
            }
        }
        return null;
    }

    /**
     * This method is called when there is an active sync happening. This method will wait until the job has finished.
     * The frequency of checking whether the sync has been completed is set to 1 minute.
     * 
     * @param currentUpdateJob
     * @param runningSyncJobModel
     * @throws InterruptedException
     * @throws CronJobAbortException
     */
    public void waitForSyncCronJobToFinish(final CronJobModel currentUpdateJob,
            final CronJobModel runningSyncJobModel, final int syncJobStatusPollFrequency)
            throws InterruptedException, CronJobAbortException {
        while (isCronJobActive(runningSyncJobModel)) {
            LOG.info(MessageFormat.format("Sync Job {0} with code {1} has not finished --going to wait for 1 min ",
                    runningSyncJobModel.getJob().getCode(),
                    runningSyncJobModel.getCode()));
            Thread.sleep(syncJobStatusPollFrequency);
            //check in between whether the cron job was aborted while waiting for sync job to be finished
            checkIfCurrentJobIsAborted(currentUpdateJob);
        }
    }

    /**
     * This method checks whether the depart update job has been aborted or not. If it has been aborted then it throws
     * the custom CronJobAbortException
     * 
     * @param currentUpdateJob
     * @throws CronJobAbortException
     */
    public void checkIfCurrentJobIsAborted(final CronJobModel currentUpdateJob)
            throws CronJobAbortException {
        if (clearAbortRequestedIfNeeded(currentUpdateJob))
        {
            throw new CronJobAbortException(currentUpdateJob.getCode() + " Aborted");
        }
    }

    /**
     * Checks whether a particular cron job is active or not.
     * 
     * @param cronJobModel
     * @return true indicates job is active
     */
    private boolean isCronJobActive(final CronJobModel cronJobModel) {
        modelService.refresh(cronJobModel);
        return cronJobService.isRunning(cronJobModel);
    }

    private final boolean clearAbortRequestedIfNeeded(final CronJobModel myCronJob)
    {
        this.modelService.refresh(myCronJob);
        if (BooleanUtils.isTrue(myCronJob.getRequestAbort()))
        {
            myCronJob.setRequestAbort(null);
            this.modelService.save(myCronJob);
            return true;
        }
        return false;
    }

    /**
     * @param cronJobService
     *            the cronJobService to set
     */
    @Required
    public void setCronJobService(final CronJobService cronJobService) {
        this.cronJobService = cronJobService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }


}