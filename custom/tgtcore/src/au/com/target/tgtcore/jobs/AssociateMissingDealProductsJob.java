/**
 * 
 */
package au.com.target.tgtcore.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.deals.MissingDealProductInfoService;


/**
 * @author rmcalave
 * 
 */
public class AssociateMissingDealProductsJob extends AbstractJobPerformable<CronJobModel> {

    private MissingDealProductInfoService missingDealProductInfoService;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.CronJobModel)
     */
    @Override
    public PerformResult perform(final CronJobModel cronJob) {
        missingDealProductInfoService.assocateMissingProductsWithCategories();

        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    /**
     * @param missingDealProductInfoService
     *            the missingDealProductInfoService to set
     */
    @Required
    public void setMissingDealProductInfoService(final MissingDealProductInfoService missingDealProductInfoService) {
        this.missingDealProductInfoService = missingDealProductInfoService;
    }
}
