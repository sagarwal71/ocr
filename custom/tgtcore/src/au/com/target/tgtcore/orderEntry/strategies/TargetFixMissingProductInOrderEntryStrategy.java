/**
 * 
 */
package au.com.target.tgtcore.orderEntry.strategies;

/**
 * @author bhuang3
 *
 */
public interface TargetFixMissingProductInOrderEntryStrategy {

    /**
     * fix missing product in order entry
     */
    public void fixMissingProductInOrderEntry();
}
