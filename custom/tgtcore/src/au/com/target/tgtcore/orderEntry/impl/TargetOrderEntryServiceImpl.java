/**
 * 
 */
package au.com.target.tgtcore.orderEntry.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.orderEntry.TargetOrderEntryService;
import au.com.target.tgtcore.orderEntry.dao.TargetOrderEntryDao;


/**
 * @author bhuang3
 *
 */
public class TargetOrderEntryServiceImpl implements TargetOrderEntryService {

    private TargetOrderEntryDao targetOrderEntryDao;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.orderEntry.TargetOrderEntryService#findOrderEntryMissingProductModel()
     */
    @Override
    public List<OrderEntryModel> findOrderEntryMissingProductModel() {
        return getTargetOrderEntryDao().findOrderEntryMissingProductModel();
    }

    @Override
    public List<String> getAllProductCodeFromOrderEntries(final AbstractOrderModel orderModel) {
        final List<String> productList = new ArrayList<>();
        if (orderModel != null && CollectionUtils.isNotEmpty(orderModel.getEntries())) {
            for (final AbstractOrderEntryModel entryModel : orderModel.getEntries()) {
                productList.add(entryModel.getProduct().getCode());
            }
        }
        return productList;
    }

    @Override
    public AbstractOrderEntryModel getOrderEntryByProductCode(final AbstractOrderModel orderModel,
            final String productCode) {
        if (StringUtils.isNotEmpty(productCode) && orderModel != null
                && CollectionUtils.isNotEmpty(orderModel.getEntries())) {
            for (final AbstractOrderEntryModel entryModel : orderModel.getEntries()) {
                if (entryModel != null && entryModel.getProduct() != null
                        && productCode.equals(entryModel.getProduct().getCode())) {
                    return entryModel;
                }
            }
        }
        return null;
    }


    /**
     * @return the targetOrderEntryDao
     */
    protected TargetOrderEntryDao getTargetOrderEntryDao() {
        return targetOrderEntryDao;
    }

    /**
     * @param targetOrderEntryDao
     *            the targetOrderEntryDao to set
     */
    @Required
    public void setTargetOrderEntryDao(final TargetOrderEntryDao targetOrderEntryDao) {
        this.targetOrderEntryDao = targetOrderEntryDao;
    }


}
