package au.com.target.tgtcore.cart;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.order.CartService;

import au.com.target.tgtcore.model.TargetCustomerModel;


/**
 */
public interface TargetCartService extends CartService {

    /**
     * Set the delivery mode, calculating the delivery mode value
     * 
     * @param cart
     *            cart to set the delivery for
     * @param deliveryMode
     *            delivery mode to be used for this cart
     */
    void setDeliveryMode(final CartModel cart, final DeliveryModeModel deliveryMode);

    /**
     * Method to pre populate cart, with preferred delivery mode, delivery address and payment address from customer in
     * previous order, payment info, cnc details in the cart, flybuy number and payment provider.
     * 
     * @param cart
     * @param preferredDeliveryMode
     * @param customer
     * @param paymentInfo
     * @param previousOrder
     */
    void prePopulateCart(final CartModel cart, final TargetZoneDeliveryModeModel preferredDeliveryMode,
            final TargetCustomerModel customer, final CreditCardPaymentInfoModel paymentInfo,
            final OrderModel previousOrder);
}
