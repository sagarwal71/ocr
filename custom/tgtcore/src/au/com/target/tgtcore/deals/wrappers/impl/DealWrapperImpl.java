/**
 * 
 */
package au.com.target.tgtcore.deals.wrappers.impl;

import de.hybris.platform.core.Registry;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.promotions.jalo.AbstractDeal;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.promotions.result.PromotionEvaluationContext;
import de.hybris.platform.promotions.result.PromotionOrderView;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import org.springframework.util.Assert;

import au.com.target.tgtcore.deals.wrappers.DealWrapper;
import au.com.target.tgtcore.enums.DealRewardTypeEnum;


/**
 * Wrapper to support calculating deals. This exists to overcome the following problems:
 * <ul>
 * <li>Jalo layer does not utilise the model classes from the service layer in hybris, this makes getting custom
 * attributes a little more awkward and unsafe</li>
 * <li>We want to leverage the POS logic for applying deals, but it assumes the product has ready access of the deal it
 * is in.</li>
 * 
 */
public class DealWrapperImpl implements DealWrapper {
    private final SessionContext ctx;
    private final AbstractDealModel dealModel;
    private final PromotionEvaluationContext promoContext;
    private final PromotionOrderView orderView;

    private final ModelService modelService =
            (ModelService)Registry.getCoreApplicationContext().getBean("modelService");

    private DealType dealType = null;

    /**
     * Constructor.
     * 
     * @param ctx
     *            The session context
     * @param deal
     *            The deal that is being applied
     * @param promoContext
     *            The promotion result to be described
     * @param orderView
     *            View of the order containing only the allowed products
     */
    public DealWrapperImpl(final SessionContext ctx, final AbstractDeal deal,
            final PromotionEvaluationContext promoContext, final PromotionOrderView orderView) {
        Assert.notNull(ctx, "SessionContext must not be null");
        Assert.notNull(deal, "AbstractDeal must not be null");
        Assert.notNull(promoContext, "PromotionEvaluationContext must not be null");
        Assert.notNull(orderView, "PromotionOrderView must not be null");
        this.ctx = ctx;
        dealModel = modelService.toModelLayer(deal);
        this.promoContext = promoContext;
        this.orderView = orderView;
    }

    /**
     * @return the dealModel
     */
    @Override
    public AbstractDealModel getDealModel() {
        return dealModel;
    }

    /**
     * @return a list deal qualifier models
     */
    @Override
    public List<DealQualifierModel> getQualifierModelList() {
        if (dealModel instanceof AbstractSimpleDealModel) {
            return ((AbstractSimpleDealModel)dealModel).getQualifierList();
        }
        return null;
    }

    /**
     * @return the ctx
     */
    @Override
    public SessionContext getCtx() {
        return ctx;
    }

    /**
     * @return the promoContext
     */
    @Override
    public PromotionEvaluationContext getPromoContext() {
        return promoContext;
    }

    /**
     * @return the orderView
     */
    @Override
    public PromotionOrderView getOrderView() {
        return orderView;
    }

    /**
     * @return the type of the deal
     */
    @Override
    public DealType getDealType() {
        if (dealModel instanceof AbstractSimpleDealModel) {
            if (dealType == null) {
                final DealRewardTypeEnum localType = ((AbstractSimpleDealModel)dealModel).getRewardType();
                if (localType.equals(DealRewardTypeEnum.DOLLAROFFEACH)) {
                    dealType = DealType.DOLLAR_OFF_EACH;
                }
                else if (localType.equals(DealRewardTypeEnum.DOLLAROFFTOTAL)) {
                    dealType = DealType.DOLLAR_OFF_TOTAL;
                }
                else if (localType.equals(DealRewardTypeEnum.FIXEDDOLLAREACH)) {
                    dealType = DealType.FIXED_DOLLAR_EACH;
                }
                else if (localType.equals(DealRewardTypeEnum.FIXEDDOLLARTOTAL)) {
                    dealType = DealType.FIXED_DOLLAR_TOTAL;
                }
                else if (localType.equals(DealRewardTypeEnum.PERCENTOFFEACH)) {
                    dealType = DealType.PERCENT_OFF_EACH;
                }
            }
        }
        return dealType;
    }
}
