package au.com.target.tgtcore.deals.dao;

import de.hybris.platform.promotions.model.AbstractDealModel;
//CHECKSTYLE:OFF
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.List;


//CHECKSTYLE:ON

/**
 * DAO to manage {@link TargetDealDao}
 */
public interface TargetDealDao {

    /**
     * Find deal by dealId
     * 
     * @param dealId
     * @return AbstractDealModel
     * @throws UnknownIdentifierException
     *             if no deal is found for the given id
     * @throws AmbiguousIdentifierException
     *             if more than one deal is found
     */
    AbstractDealModel getDealForId(String dealId);


    /**
     * @return list of active deals
     */
    List<AbstractDealModel> getAllActiveDeals();


    /**
     * @return list of active deal codes
     */
    List<String> getAllActiveDealCodes();

}
