/**
 * 
 */
package au.com.target.tgtcore.integration.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author rsamuel3
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TargetIntegrationErrorDto implements Serializable {
    private String errorCode;
    private String errorMessage;
    private String errorFreeText;

    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage
     *            the errorMessage to set
     */
    public void setErrorMessage(final String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * @param errorCode
     *            the errorCode to set
     */
    public void setErrorCode(final String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * @return the errorFreeText
     */
    public String getErrorFreeText() {
        return errorFreeText;
    }

    /**
     * @param errorFreeText
     *            the errorFreeText to set
     */
    public void setErrorFreeText(final String errorFreeText) {
        this.errorFreeText = errorFreeText;
    }

    @Override
    public String toString() {
        final StringBuilder error = new StringBuilder();
        error.append("{errorCode=").append(errorCode).append(", errorMessage=").append(errorMessage)
                .append(", errorFreeText=").append(errorFreeText);
        return error.toString();
    }

}
