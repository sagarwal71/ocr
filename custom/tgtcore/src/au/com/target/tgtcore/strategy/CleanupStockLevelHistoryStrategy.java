/**
 * 
 */
package au.com.target.tgtcore.strategy;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.jobs.maintenance.MaintenanceCleanupStrategy;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.stock.model.StockLevelHistoryEntryModel;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;


/**
 * @author smishra1
 *
 *         The cleanup strategy removes entries in the stocklevel history table older than a give time defined in
 *         properties.
 */
public class CleanupStockLevelHistoryStrategy implements MaintenanceCleanupStrategy<ItemModel, CronJobModel> {
    private static final Logger LOG = Logger.getLogger(CleanupStockLevelHistoryStrategy.class);

    private Integer recordAge;
    private Integer batchSize;
    private ModelService modelService;

    @Override
    public FlexibleSearchQuery createFetchQuery(final CronJobModel cjm) {
        Assert.notNull(recordAge, "no create date time record found ");

        final String query = "SELECT TOP " + batchSize + " {PK} "
                + "FROM { " + StockLevelHistoryEntryModel._TYPECODE
                + "} WHERE {creationtime} < ?age";
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
        searchQuery.addQueryParameter("age", getAge());
        return searchQuery;
    }

    @Override
    public void process(final List<ItemModel> elements) {
        if (CollectionUtils.isNotEmpty(elements)) {
            try {
                modelService.removeAll(elements);
            }
            catch (final Exception e) {
                LOG.warn("can not remove the entries in the history table ", e);
            }
        }
    }

    /**
     * Calculates age of records to remove depends on created date time
     * 
     * @return {@link Date}
     */
    protected Date getAge() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -recordAge.intValue());
        return cal.getTime();
    }

    /**
     * @param batchSize
     *            the batchSize to set
     */
    @Required
    public void setBatchSize(final Integer batchSize) {
        this.batchSize = batchSize;
    }

    /**
     * @param recordAge
     *            the recordAge to set
     */
    @Required
    public void setRecordAge(final Integer recordAge) {
        this.recordAge = recordAge;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }
}