/**
 * 
 */
package au.com.target.tgtcore.ticket.service.impl;

import de.hybris.platform.comments.model.CommentTypeModel;
import de.hybris.platform.comments.model.ComponentModel;
import de.hybris.platform.comments.model.DomainModel;
import de.hybris.platform.comments.services.CommentService;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.ticket.enums.CsTicketCategory;
import de.hybris.platform.ticket.enums.CsTicketPriority;
import de.hybris.platform.ticket.enums.CsTicketState;
import de.hybris.platform.ticket.events.model.CsCustomerEventModel;
import de.hybris.platform.ticket.model.CsAgentGroupModel;
import de.hybris.platform.ticket.model.CsTicketModel;
import de.hybris.platform.ticket.service.TicketService;
import de.hybris.platform.ticket.service.impl.DefaultTicketBusinessService;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;


/**
 * 
 */
public class TargetTicketBusinessServiceImpl extends DefaultTicketBusinessService implements
        TargetTicketBusinessService {
    private static final String CS_AGENT = "csagent";
    private UserService userService;
    private CommentService commentService;
    private TicketService ticketService;


    @Override
    public String createCsAgentTicket(final String headline, final String subject, final String text,
            final OrderModel order) {
        return createCsAgentTicket(headline, subject, text, order, null);
    }


    /**
     * @param headline
     * @param subject
     * @param text
     * @param order
     * @return String
     */
    @Override
    public String createCsAgentTicket(final String headline, final String subject, final String text,
            final OrderModel order, final String group) {
        ServicesUtil.validateParameterNotNullStandardMessage("headline", headline);
        ServicesUtil.validateParameterNotNullStandardMessage("text", text);
        final Date now = new Date();
        final CsTicketModel ticket = new CsTicketModel();
        ticket.setCategory(CsTicketCategory.INCIDENT);
        ticket.setCreationtime(now);
        ticket.setHeadline(headline);
        ticket.setPriority(CsTicketPriority.HIGH);
        ticket.setState(CsTicketState.OPEN);
        final EmployeeModel csAgent = (EmployeeModel)userService.getUserForUID(CS_AGENT);
        ticket.setAssignedAgent(csAgent);
        SalesApplication salesApplication = null;
        if (order != null) {
            ticket.setOrder(order);
            ticket.setCustomer(order.getUser());
            salesApplication = order.getSalesApplication();
        }
        assignCSAgentGroupToTicket(salesApplication, ticket, group);
        final DomainModel domain = commentService.getDomainForCode("ticketSystemDomain");
        final ComponentModel component = commentService.getComponentForCode(domain, "ticketSystem");
        final CommentTypeModel commentType = commentService.getCommentTypeForCode(component, "customerNote");
        final CsCustomerEventModel event = (CsCustomerEventModel)getModelService()
                .create(commentType.getMetaType().getCode());
        event.setAuthor(csAgent);
        event.setComponent(component);
        event.setCommentType(commentType);
        event.setStartDateTime(now);
        event.setEndDateTime(now);
        event.setSubject(subject);
        event.setText(text);

        final CsTicketModel createdTicket = createTicketInternal(ticket, event);
        if (order != null) {
            getModelService().refresh(order);
        }
        return createdTicket == null ? null : createdTicket.getTicketID();
    }


    /**
     * @param salesApplication
     */
    protected void assignCSAgentGroupToTicket(final SalesApplication salesApplication, final CsTicketModel ticket,
            final String csAgentGroup) {

        final List<CsAgentGroupModel> agentGroups = ticketService.getAgentGroups();
        for (final CsAgentGroupModel csAgentGroupModel : agentGroups) {
            if (StringUtils.isNotEmpty(csAgentGroup)) {
                if (csAgentGroup.equals(csAgentGroupModel.getUid())) {
                    ticket.setAssignedGroup(csAgentGroupModel);
                    break;
                }
            }
            else if (SalesApplication.TRADEME.equals(salesApplication)
                    && csAgentGroupModel.getUid().equals(TgtCoreConstants.CsAgentGroup.TRADE_ME_CS_AGENT_GROUP)) {
                ticket.setAssignedGroup(csAgentGroupModel);
                break;
            }
            else if (SalesApplication.EBAY.equals(salesApplication)
                    && csAgentGroupModel.getUid().equals(TgtCoreConstants.CsAgentGroup.EBAY_CS_AGENT_GROUP)) {
                ticket.setAssignedGroup(csAgentGroupModel);
                break;
            }
        }

    }


    /**
     * @param userService
     *            the userService to set
     */
    @Override
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @param commentService
     *            the commentService to set
     */
    @Required
    public void setCommentService(final CommentService commentService) {
        this.commentService = commentService;
    }

    /**
     * @param ticketService
     *            the ticketService to set
     */
    @Required
    public void setTicketService(final TicketService ticketService) {
        this.ticketService = ticketService;
    }

}
