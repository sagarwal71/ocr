/**
 * 
 */
package au.com.target.tgtcore.cache;

import au.com.target.tgtutility.registry.RegistryFacade;
import de.hybris.platform.core.ItemDeployment;
import de.hybris.platform.core.Registry;
import de.hybris.platform.regioncache.CacheController;
import de.hybris.platform.regioncache.CacheStatistics;
import de.hybris.platform.regioncache.region.CacheRegion;

import java.io.IOException;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.target.tgtcore.cache.dto.TargetCacheDataDTO;

import javax.annotation.Resource;


/**
 * @author pthoma20
 *
 */
public class TargetGenerateCacheStats {

    private static final Logger LOG = Logger.getLogger(TargetGenerateCacheStats.class);


    @Autowired
    private CacheController controller;
    
    @Resource
    private RegistryFacade registryFacade;


    /**
     * This method looks up the cache stats and logs it to a cache stats file configured.
     * 
     * 
     */
    public void generateCacheStatsForEntityCache() {
        final Collection<CacheRegion> regions = controller.getRegions();
        for (final CacheRegion region : regions) {
            logRegionCacheStats(region);
            logTypesCacheStats(region);
        }
    }

    private void logRegionCacheStats(final CacheRegion region) {
        final TargetCacheDataDTO cacheDto = new TargetCacheDataDTO();
        cacheDto.setName(region.getName());
        cacheDto.setIdentifier("REGION");
        final CacheStatistics cacheStatistics = region.getCacheRegionStatistics();
        cacheDto.setEvictions(cacheStatistics.getEvictions());
        cacheDto.setFetches(cacheStatistics.getFetches());
        cacheDto.setHits(cacheStatistics.getHits());
        cacheDto.setInstanceCount(cacheStatistics.getInstanceCount());
        cacheDto.setInvalidations(cacheStatistics.getInvalidations());
        cacheDto.setMisses(cacheStatistics.getMisses());
        logDtoData(cacheDto);
    }


    protected void logDtoData(final TargetCacheDataDTO cacheDto) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            LOG.info(mapper.writeValueAsString(cacheDto));
        }
        catch (final IOException e) {
            LOG.error("invalid cache Data " + cacheDto.getName());
        }
    }

    private void logTypesCacheStats(final CacheRegion region) {
        final CacheStatistics cacheStatistics = region.getCacheRegionStatistics();
        final Collection<Object> types = cacheStatistics.getTypes();
        for (final Object type : types) {
            if (type == null) {
                continue;
            }
            final String typeCode = type.toString();

            final TargetCacheDataDTO cacheDto = new TargetCacheDataDTO();
            cacheDto.setIdentifier("TYPE");
            cacheDto.setName(getTypeNameForCode(typeCode));
            cacheDto.setCode(typeCode);
            cacheDto.setEvictions(cacheStatistics.getEvictions(typeCode));
            cacheDto.setFetches(cacheStatistics.getFetches(typeCode));
            cacheDto.setHits(cacheStatistics.getHits(typeCode));
            cacheDto.setInstanceCount(cacheStatistics.getInstanceCount(typeCode));
            cacheDto.setInvalidations(cacheStatistics.getInvalidations(typeCode));
            cacheDto.setMisses(cacheStatistics.getMisses(typeCode));
            logDtoData(cacheDto);
        }
    }

    private String getTypeNameForCode(final String typeCode) {
        ItemDeployment itemDeploymentType;
        try {
            itemDeploymentType = registryFacade.getItemDeploymentTypeByTypeCode(Integer.parseInt(typeCode));
        }
        catch (final NumberFormatException ne) {
            itemDeploymentType = registryFacade.getItemDeploymentTypeByTypeCode(typeCode);
        }
        if (itemDeploymentType != null) {
            final String name = itemDeploymentType.getName();
            final String formattedName = StringUtils.substring(name, StringUtils.lastIndexOf(name, ".") + 1);
            return StringUtils.substring(formattedName, StringUtils.lastIndexOf(formattedName, "_") + 1);

        }
        return null;
    }
}
