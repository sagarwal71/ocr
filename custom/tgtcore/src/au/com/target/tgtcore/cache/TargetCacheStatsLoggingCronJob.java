/**
 * 
 */
package au.com.target.tgtcore.cache;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.event.EventService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 * Cronjob class to update Target Product Size and Size group
 * 
 * @author pthoma20
 *
 */
public class TargetCacheStatsLoggingCronJob extends
        AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(TargetCacheStatsLoggingCronJob.class);

    private EventService eventService;

    /* 
     * The perform method in this job will publish an event to all the other nodes to 
     */
    @Override
    public PerformResult perform(final CronJobModel cronJob) {
        LOG.info("Starting Cronjob for Logging Cache Stats");
        try {
            eventService.publishEvent(new TargetCustomCacheStatsLogEvent());
        }
        catch (final Exception e) {
            LOG.error("Exception while executing the Logging Cache Stats Job", e);
            return new PerformResult(CronJobResult.ERROR, CronJobStatus.FINISHED);
        }
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    /**
     * @param eventService
     *            the eventService to set
     */
    @Required
    public void setEventService(final EventService eventService) {
        this.eventService = eventService;
    }
}
