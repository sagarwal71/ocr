/**
 * 
 */
package au.com.target.tgtcore.login;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.security.auth.AuthenticationService;


public interface TargetAuthenticationService extends AuthenticationService {

    /**
     * true if login attempts has reached the limit
     * 
     * @param customer
     * @return boolean
     */
    boolean checkAndIncrementLoginAttempts(CustomerModel customer);

    /**
     * true if login attempts has reached the limit
     * 
     * @param customer
     * @return boolean
     */
    boolean checkLoginAttempts(CustomerModel customer);

    /**
     * reset customers login attempts
     * 
     * @param customer
     */
    void resetLoginAttempts(CustomerModel customer);
}
