/**
 * 
 */
package au.com.target.tgtcore.config;

import java.util.Map;


/**
 * @author bhuang3
 * 
 */
public interface TargetSharedConfigService {

    /**
     * find the config by code, return null if not found
     * 
     * @param code
     * @return String
     */
    String getConfigByCode(String code);

    /**
     * creates new entries if not present or updates if key already present.
     * 
     * @param configEntries
     */
    void setSharedConfigs(Map<String, String> configEntries);

    int getInt(final String configCode, final int defaultValue);

    /**
     * get double from the shared config
     * 
     * @param configCode
     * @param defaultValue
     * @return double value
     */
    double getDouble(final String configCode, final double defaultValue);

}
