/**
 * 
 */
package au.com.target.tgtcore.config.daos;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetSharedConfigModel;


/**
 * @author bhuang3
 * 
 */
public interface TargetSharedConfigDao {

    /**
     * get the shared config model by code
     * 
     * @param code
     * @return TargetSharedConfigModel
     */
    TargetSharedConfigModel getSharedConfigByCode(String code) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException;
}
