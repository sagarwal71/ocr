/**
 * 
 */
package au.com.target.tgtcore.config.daos.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.config.daos.TargetSharedConfigDao;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetSharedConfigModel;
import au.com.target.tgtcore.util.TargetServicesUtil;


/**
 * @author bhuang3
 * 
 */
public class TargetSharedConfigDaoImpl extends DefaultGenericDao<TargetSharedConfigModel> implements
        TargetSharedConfigDao {


    /**
     * constructor
     */
    public TargetSharedConfigDaoImpl() {
        super(TargetSharedConfigModel._TYPECODE);
    }

    @Override
    public TargetSharedConfigModel getSharedConfigByCode(final String code) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        ServicesUtil.validateParameterNotNullStandardMessage("code", code);

        final Map<String, String> params = new HashMap<String, String>();
        params.put(TargetSharedConfigModel.CODE, code);
        final List<TargetSharedConfigModel> sharedConfigModel = find(params);

        TargetServicesUtil.validateIfSingleResult(sharedConfigModel, TargetSharedConfigModel.class,
                TargetSharedConfigModel.CODE, code);

        return sharedConfigModel.get(0);
    }

}
