package au.com.target.tgtcore.event;

//CHECKSTYLE:OFF
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.util.Date;

import au.com.target.tgtcore.jalo.TargetCatalogVersionSyncCronJob;


//CHECKSTYLE:ON

/**
 * Event representing the successful completion of {@link TargetCatalogVersionSyncCronJob}
 * 
 */
public class CatalogVersionSyncCompletionEvent extends AbstractEvent {

    private final Date finishTime;

    /**
     * @param finishTime
     */
    public CatalogVersionSyncCompletionEvent(final Date finishTime) {
        this.finishTime = finishTime;
    }


    /**
     * @return the finishTime
     */
    public Date getFinishTime() {
        return finishTime;
    }


}