/**
 * 
 */
package au.com.target.tgtcore.taxinvoice.dao;

import de.hybris.platform.commons.model.DocumentModel;

import au.com.target.tgtcore.model.TargetValuePackTypeModel;


public interface TaxInvoiceDao {

    /**
     * Check to see if product code matches a lead sku in TargetValuePackTypeModel
     * 
     * @param productCode
     * @return TargetValuePackTypeModel
     */
    TargetValuePackTypeModel findTargetValuePackTypeForProductCode(String productCode);

    /**
     * found InvoiceDocument for orderCode
     * 
     * @param orderCode
     * @return DocumentModel
     * @throws de.hybris.platform.servicelayer.exceptions.ModelNotFoundException
     *             if nothing was found
     */
    public DocumentModel findInvoiceDocumentForOrder(final String orderCode);
}
