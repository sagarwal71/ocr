package au.com.target.tgtcore.taxinvoice.dao.impl;

import de.hybris.platform.commons.model.DocumentModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import org.apache.log4j.Logger;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetValuePackTypeModel;
import au.com.target.tgtcore.taxinvoice.dao.TaxInvoiceDao;


/**
 * 
 */


public class TaxInvoiceDaoImpl implements TaxInvoiceDao {

    private static final Logger LOG = Logger.getLogger(TaxInvoiceDaoImpl.class);
    private static final String FIND_DOCUMENT_BY_CODE = "select {" + DocumentModel.PK + "} from {"
            + DocumentModel._TYPECODE + "} where {" + DocumentModel.CODE
            + "} = ?documentCode";

    private FlexibleSearchService flexibleSearchService;

    @Override
    public TargetValuePackTypeModel findTargetValuePackTypeForProductCode(final String productCode) {
        final TargetValuePackTypeModel example = new TargetValuePackTypeModel();
        example.setLeadSKU(productCode);
        TargetValuePackTypeModel valuePackType = null;
        try {
            valuePackType = flexibleSearchService.getModelByExample(example);
        }
        catch (final ModelNotFoundException e) {
            LOG.info("No value pack type model found for given example");
        }
        return valuePackType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DocumentModel findInvoiceDocumentForOrder(final String orderCode) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_DOCUMENT_BY_CODE);
        query.addQueryParameter("documentCode", TgtCoreConstants.TAX_INVOICE_PREFIX
                + TgtCoreConstants.Seperators.HYPHEN + orderCode);
        return flexibleSearchService.searchUnique(query);
    }

    /**
     * @param flexibleSearchService
     *            the flexibleSearchService to set
     */
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

}
