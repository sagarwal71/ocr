/**
 * 
 */
package au.com.target.tgtcore.taxinvoice.data;

import java.util.ArrayList;
import java.util.List;


/**
 * @author rmcalave
 * 
 */
public class TaxInvoiceItems {

    private List<TaxInvoiceDealSection> taxInvoiceDealSections;

    private List<TaxInvoiceLineItem> taxInvoiceLineItems;

    private double itemsSavings;

    private double vouchers;

    /**
     * @return the taxInvoiceDealSections
     */
    public List<TaxInvoiceDealSection> getTaxInvoiceDealSections() {
        return taxInvoiceDealSections;
    }

    /**
     * @param taxInvoiceDealSections
     *            the taxInvoiceDealSections to set
     */
    public void setTaxInvoiceDealSections(final List<TaxInvoiceDealSection> taxInvoiceDealSections) {
        this.taxInvoiceDealSections = taxInvoiceDealSections;
    }

    /**
     * @return the taxInvoiceLineItems
     */
    public List<TaxInvoiceLineItem> getTaxInvoiceLineItems() {
        return taxInvoiceLineItems;
    }

    /**
     * @param taxInvoiceLineItems
     *            the taxInvoiceLineItems to set
     */
    public void setTaxInvoiceLineItems(final List<TaxInvoiceLineItem> taxInvoiceLineItems) {
        this.taxInvoiceLineItems = taxInvoiceLineItems;
    }

    /**
     * adds the taxInvoiceLineItem to the list of LineItems
     * 
     * @param taxInvoiceLineItem
     */
    public void addTaxInvoiceLineItems(final TaxInvoiceLineItem taxInvoiceLineItem) {
        if (this.taxInvoiceLineItems == null) {
            this.taxInvoiceLineItems = new ArrayList<>();
        }
        this.taxInvoiceLineItems.add(taxInvoiceLineItem);
    }


    /**
     * adds the taxInvoiceDealSection to the list of DealSections
     * 
     * @param taxInvoiceDealSection
     */
    public void addTaxInvoiceDealSections(final TaxInvoiceDealSection taxInvoiceDealSection) {
        if (this.taxInvoiceDealSections == null) {
            this.taxInvoiceDealSections = new ArrayList<>();
        }
        this.taxInvoiceDealSections.add(taxInvoiceDealSection);
    }

    /**
     * @return the itemsSavings
     */
    public double getItemsSavings() {
        return itemsSavings;
    }

    /**
     * @param itemsSavings
     *            the itemsSavings to set
     */
    public void setItemsSavings(final double itemsSavings) {
        this.itemsSavings = itemsSavings;
    }

    /**
     * @return the vouchers
     */
    public double getVouchers() {
        return vouchers;
    }

    /**
     * @param vouchers
     *            the vouchers to set
     */
    public void setVouchers(final double vouchers) {
        this.vouchers = vouchers;
    }
}
