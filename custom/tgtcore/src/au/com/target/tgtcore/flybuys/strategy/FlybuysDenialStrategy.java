/**
 * 
 */
package au.com.target.tgtcore.flybuys.strategy;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;


/**
 * The Interface FlybuysDenialStrategy.
 * 
 * @author umesh
 */
public interface FlybuysDenialStrategy {

    /**
     * Checks if is flybuy discount allowed.
     * 
     * @param orderModel
     *            the order model
     * @return the flybuys discount denial dto
     */
    FlybuysDiscountDenialDto isFlybuysDiscountAllowed(AbstractOrderModel orderModel);

}
