/**
 * 
 */
package au.com.target.tgtcore.flybuys.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;
import au.com.target.tgtcore.flybuys.service.FlybuysRedeemConfigService;
import au.com.target.tgtcore.flybuys.strategy.AbstractFlybuysDenialStrategyConfig;
import au.com.target.tgtcore.model.FlybuysRedeemConfigModel;


/**
 * Strategy to deny flybuysDiscount if cart sub-total is less than configured minCartValue in FlybuysRedeemConfig.
 * 
 * @author umesh
 */
public class MinCartValueFlybuysDiscountDenialStrategy extends AbstractFlybuysDenialStrategyConfig {

    private static final String SUB_TOTAL = "subtotal";

    private FlybuysRedeemConfigService flybuysRedeemConfigService;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.strategy.FlybuysDenialStrategy#isFlybuysDiscountAllowed(de.hybris.platform.core.model.order.AbstractOrderModel)
     */
    @Override
    public FlybuysDiscountDenialDto isFlybuysDiscountAllowed(final AbstractOrderModel orderModel) {
        if (null != orderModel) {
            final FlybuysRedeemConfigModel redeemConfig = flybuysRedeemConfigService.getFlybuysRedeemConfig();
            if (null != redeemConfig)
            {
                final Double minCartValue = redeemConfig.getMinCartValue();
                final Double cartSubTotal = orderModel.getSubtotal();
                if (minCartValue != null && cartSubTotal != null && cartSubTotal.compareTo(minCartValue) < 0)
                {
                    return getDenyDto(SUB_TOTAL);
                }
            }
        }
        return null;
    }

    /**
     * @param flybuysRedeemConfigService
     *            the flybuysRedeemConfigService to set
     */
    @Required
    public void setFlybuysRedeemConfigService(final FlybuysRedeemConfigService flybuysRedeemConfigService) {
        this.flybuysRedeemConfigService = flybuysRedeemConfigService;
    }

}
