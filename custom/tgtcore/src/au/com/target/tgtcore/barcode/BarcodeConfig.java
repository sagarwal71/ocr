/**
 * 
 */
package au.com.target.tgtcore.barcode;

import org.springframework.beans.factory.annotation.Required;


public class BarcodeConfig {
    private String barcodeType;
    private String barcodeHeight;
    private String barcodeModuleWidth;
    private String barcodeDpi;

    /**
     * @return the barcodeType
     */
    public String getBarcodeType() {
        return barcodeType;
    }

    /**
     * @param barcodeType
     *            the barcodeType to set
     */
    @Required
    public void setBarcodeType(final String barcodeType) {
        this.barcodeType = barcodeType;
    }

    /**
     * @return the barcodeHeight
     */
    public String getBarcodeHeight() {
        return barcodeHeight;
    }

    /**
     * @param barcodeHeight
     *            the barcodeHeight to set
     */
    @Required
    public void setBarcodeHeight(final String barcodeHeight) {
        this.barcodeHeight = barcodeHeight;
    }

    /**
     * @return the barcodeModuleWidth
     */
    public String getBarcodeModuleWidth() {
        return barcodeModuleWidth;
    }

    /**
     * @param barcodeModuleWidth
     *            the barcodeModuleWidth to set
     */
    @Required
    public void setBarcodeModuleWidth(final String barcodeModuleWidth) {
        this.barcodeModuleWidth = barcodeModuleWidth;
    }

    /**
     * @return the barcodeDpi
     */
    public String getBarcodeDpi() {
        return barcodeDpi;
    }

    /**
     * @param barcodeDpi
     *            the barcodeDpi to set
     */
    @Required
    public void setBarcodeDpi(final String barcodeDpi) {
        this.barcodeDpi = barcodeDpi;
    }


}
