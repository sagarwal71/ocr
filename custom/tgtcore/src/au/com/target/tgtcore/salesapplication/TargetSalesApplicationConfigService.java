/**
 * 
 */
package au.com.target.tgtcore.salesapplication;

import de.hybris.platform.commerceservices.enums.SalesApplication;

import java.util.List;

import au.com.target.tgtcore.model.SalesApplicationConfigModel;


/**
 * Service for performing operations on SalesApplicationConfig.
 * 
 * @author ragarwa3
 *
 */
public interface TargetSalesApplicationConfigService {

    /**
     * Gets the config for sales application. Returns null if null sales application is provided or no config found for
     * given sales application.
     *
     * @param salesApp
     *            the sales app
     * @return the config for sales application
     */
    SalesApplicationConfigModel getConfigForSalesApplication(final SalesApplication salesApp);

    /**
     * Gets all sales application configs having partner channel marked as true.
     *
     * @return the all partner sales application configs
     */
    List<SalesApplicationConfigModel> getAllPartnerSalesApplicationConfigs();

    /**
     * Checks if given sales application is configured to skip fraud check.
     *
     * @param salesApp
     *            the sales app
     * @return true, if configured as skipFraudCheck otherwise false.
     */
    boolean isSkipFraudCheck(final SalesApplication salesApp);

    /**
     * Checks if given sales application is configured to deny refunds/returns.
     *
     * @param salesApp
     *            the sales app
     * @return true, if configured as denyRefunds otherwise false.
     */
    boolean isRefundDenied(final SalesApplication salesApp);

    /**
     * Checks if given sales application is configured to deny cancellation.
     * 
     * @param salesApp
     * @return true, if configured as denyCancellation otherwise false.
     */
    boolean isDenyCancellation(final SalesApplication salesApp);

    /**
     * Check if for given sales application tax calculation should be suppressed.
     * 
     * @param salesApp
     * @return true, if configured as suppressTaxCalculation otherwise false
     */
    boolean isSuppressTaxCalculation(final SalesApplication salesApp);

    /**
     * Check if for given sales application stock reservation should happen during accept order process.
     * 
     * @param salesApp
     * @return true, if configured as reserveStockOnAcceptOrder otherwise false
     */
    boolean isReserveStockOnAcceptOrder(final SalesApplication salesApp);

    /**
     * Check if for given sales application sending order confirmation email should be suppressed.
     * 
     * @param salesApp
     * @return true, if configured as suppressOrderConfMail otherwise false.
     */
    boolean isSuppressOrderConfMail(final SalesApplication salesApp);

    /**
     * Checks if given sales application is configured to suppress the tax invoice generation.
     *
     * @param salesApp
     *            the sales app
     * @return true, if configured as suppressTaxInvoice otherwise false.
     */
    boolean isSuppressTaxInvoice(final SalesApplication salesApp);

    /**
     * Checks if given sales application is configured to be partner channel.
     *
     * @param salesApp
     *            the sales app
     * @return true, if configured as partner channel otherwise false.
     */
    boolean isPartnerChannel(final SalesApplication salesApp);

}
