package au.com.target.tgtcore.media;

import de.hybris.platform.media.services.impl.DefaultMimeService;

import org.apache.commons.lang3.StringUtils;


/**
 * @author htan3
 *
 */
public class TargetMimeService extends DefaultMimeService {

    @Override
    public String getBestMime(final String fileName, final byte[] firstBytes,
            final String overrideMime) {
        if (!StringUtils.isEmpty(overrideMime) && getSupportedMimeTypes().contains(overrideMime)) {
            return overrideMime;
        }
        return super.getBestMime(fileName, firstBytes, overrideMime);
    }
}