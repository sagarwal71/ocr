/**
 * 
 */
package au.com.target.tgtcore.order.strategies.impl;

import de.hybris.platform.core.model.order.OrderModel;

import org.springframework.util.Assert;

import au.com.target.tgtcore.order.strategies.ReplaceOrderDenialStrategy;


/**
 * A denial strategy using order Status to decide whether an order replacement is possible
 * 
 */
public class OrderStatusReplaceOrderDenialStrategy implements ReplaceOrderDenialStrategy {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDenied(final OrderModel order) {
        Assert.notNull(order, "order model must not be null");
        return (!"COMPLETED".equals(order
                .getStatus().getCode()));
    }

}
