package au.com.target.tgtcore.order;

/**
 * Indicates an invalid order state when thrown.
 */
public class OrderStateException extends RuntimeException {

    /**
     * Creates new exception with given {@code message}.
     *
     * @param message the error message
     */
    public OrderStateException(final String message) {
        super(message);
    }

    /**
     * Creates new exception with given {@code message} and {@code cause}.
     *
     * @param message the error message
     * @param cause the error cause
     */
    public OrderStateException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Creates new exception with given {@code cause}.
     *
     * @param cause the error cause
     */
    public OrderStateException(final Throwable cause) {
        super(cause);
    }
}
