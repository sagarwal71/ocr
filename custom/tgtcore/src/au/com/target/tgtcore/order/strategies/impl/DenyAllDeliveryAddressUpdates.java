/**
 * 
 */
package au.com.target.tgtcore.order.strategies.impl;

import de.hybris.platform.core.model.order.OrderModel;

import au.com.target.tgtcore.order.strategies.UpdateAddressDenialStrategy;


/**
 * @author dcwillia
 * 
 */
public class DenyAllDeliveryAddressUpdates implements UpdateAddressDenialStrategy {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDenied(final OrderModel order) {
        return true;
    }
}
