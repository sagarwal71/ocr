/**
 * 
 */
package au.com.target.tgtcore.order.strategies.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.strategies.SubmitOrderStrategy;
import de.hybris.platform.servicelayer.event.EventService;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.event.TargetSubmitPartnerOrderEvent;


/**
 * 
 * @author jjayawa1
 * 
 */
public class TargetEventPublishingSubmitPartnerOrderStrategyImpl implements SubmitOrderStrategy {

    private EventService eventService;

    /*
     * (non-Javadoc)
     * @see de.hybris.platform.order.strategies.SubmitOrderStrategy#submitOrder(de.hybris.platform.core.model.order.OrderModel)
     */
    @Override
    public void submitOrder(final OrderModel order) {
        eventService.publishEvent(new TargetSubmitPartnerOrderEvent(order));
    }

    /**
     * @param eventService
     *            the eventService to set
     */
    @Required
    public void setEventService(final EventService eventService) {
        this.eventService = eventService;
    }

}
