package au.com.target.tgtcore.order;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.math.BigDecimal;


/**
 * Strategy for finding out the total payment made so far on an order
 * 
 */
public interface FindOrderTotalPaymentMadeStrategy {

    /**
     * Get amount paid so far from the order
     * 
     * @param order
     * @return Double
     */
    Double getAmountPaidSoFar(AbstractOrderModel order);

    /**
     * Please use getAmountPaidSoFar instead, this method is deprecated and will cause error for split payments.
     * 
     * Get the total amount captured so far from the cart/order
     */
    @Deprecated
    BigDecimal getAmountCapturedSoFar(AbstractOrderModel abstractOrderModel);

}
