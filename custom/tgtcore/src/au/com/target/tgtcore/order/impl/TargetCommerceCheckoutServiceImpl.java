package au.com.target.tgtcore.order.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeValueModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.promotions.jalo.PromotionsManager.AutoApplyMode;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.user.AddressService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.cart.TargetCartService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetInsufficientStockLevelException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.fluent.FluentStockService;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.model.StoreEmployeeModel;
import au.com.target.tgtcore.model.TMDiscountProductPromotionModel;
import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtcore.order.FluentOrderService;
import au.com.target.tgtcore.order.TargetCommerceCheckoutService;
import au.com.target.tgtcore.order.TargetDiscountService;
import au.com.target.tgtcore.order.UserCheckoutPreferencesService;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;
import au.com.target.tgtpayment.commands.result.TargetCardPaymentResult;
import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.exceptions.PaymentException;
import au.com.target.tgtpayment.service.PaymentsInProgressService;
import au.com.target.tgtpayment.service.TargetPaymentService;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.BarcodeTools;
import au.com.target.tgtutility.util.StringTools;


/**
 * Implementation of {@link TargetCommerceCheckoutService}
 * 
 */
public class TargetCommerceCheckoutServiceImpl extends DefaultCommerceCheckoutService implements
        TargetCommerceCheckoutService {

    private static final String STOCK_UPDATE_MESSAGE = "{0} : cart={1}, quantity={2} for product={3}";

    private static final Logger LOG = Logger.getLogger(TargetCommerceCheckoutServiceImpl.class);

    private static final String CLICK_AND_COLLECT = "click-and-collect";

    private static final String HOME_DELIVERY = "home-delivery";

    private static final String EXPRESS_DELIVERY = "express-delivery";

    private static final String DIGITAL_GIFT_CARD = "digital-gift-card";

    private TargetPointOfServiceService targetPointOfServiceService;

    private AddressService addressService;

    private TargetDiscountService targetDiscountService;

    private UserService userService;

    private TargetCartService targetCartService;

    private TargetStockService targetStockService;

    private TargetPaymentService targetPaymentService;

    private PaymentsInProgressService paymentsInProgressService;

    private TargetDeliveryService targetDeliveryService;

    private String flybuysPrefix;
    private int flybuysLen;

    private FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    private TargetVoucherService targetVoucherService;

    private CommerceCartService commerceCartService;

    private UserCheckoutPreferencesService userCheckoutPreferencesService;

    private TargetSalesApplicationConfigService salesApplicationConfigService;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private FluentOrderService fluentOrderService;

    private FluentStockService fluentStockService;

    @Override
    public boolean fillCart4ClickAndCollect(final CartModel cartModel, final int cncStoreNumber, final String title,
            final String firstName,
            final String lastName,
            final String phoneNumber,
            final DeliveryModeModel deliveryMode) {

        TitleModel titleModel = null;

        if (StringUtils.isNotBlank(title)) {
            titleModel = userService.getTitleForCode(title.trim());
        }

        this.fillCart4ClickAndCollectWithoutRecalculate(cartModel, Integer.valueOf(cncStoreNumber), titleModel,
                firstName, lastName,
                phoneNumber, deliveryMode);

        targetCartService.setDeliveryMode(cartModel, deliveryMode);
        getModelService().save(cartModel);

        final CommerceCartParameter cartParameter = new CommerceCartParameter();
        cartParameter.setCart(cartModel);
        commerceCartService.calculateCart(cartParameter);

        return true;
    }

    @Override
    public void fillCart4ClickAndCollectWithoutRecalculate(final CartModel cartModel, final Integer cncStoreNumber,
            final TitleModel titleModel,
            final String firstName, final String lastName, final String phoneNumber,
            final DeliveryModeModel deliveryMode) {

        Assert.notNull(cartModel, "cart model must not be null");

        try {
            final TargetPointOfServiceModel pointOfService = targetPointOfServiceService
                    .getPOSByStoreNumber(cncStoreNumber);

            final AddressModel pointOfServiceAddress = pointOfService.getAddress();

            final AddressModel clone = addressService.cloneAddressForOwner(pointOfServiceAddress, cartModel);

            clone.setTitle(titleModel);
            clone.setFirstname(firstName);
            clone.setLastname(lastName);
            clone.setPhone1(phoneNumber);

            cartModel.setDeliveryMode(deliveryMode);
            cartModel.setDeliveryAddress(clone);
            cartModel.setCncStoreNumber(cncStoreNumber);
        }
        catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
            LOG.error("Error occured while retriving store details for cart="
                    + cartModel.getCode() + " and the storeNumber=" + cncStoreNumber);
        }
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.order.impl.DefaultCommerceCheckoutService#placeOrder(de.hybris.platform.core.model.order.CartModel, de.hybris.platform.commerceservices.enums.SalesApplication)
     */
    @Override
    public CommerceOrderResult placeOrder(final CommerceCheckoutParameter checkoutParameter)
            throws InvalidCartException {

        validateParameterNotNull(checkoutParameter, "Checkout parameter cannot be null");
        final CartModel cartModel = checkoutParameter.getCart();
        validateParameterNotNull(cartModel, "Cart model cannot be null");
        final SalesApplication salesApplication = checkoutParameter.getSalesApplication();
        validateParameterNotNull(salesApplication, "Sales Application cannot be null");

        Assert.notNull(cartModel.getCalculated(), "Cart model calculated must not be null");
        Assert.isTrue(cartModel.getCalculated().booleanValue(), "Cart model must be calculated");

        final CustomerModel customer = (CustomerModel)cartModel.getUser();
        validateParameterNotNull(customer, "Customer model cannot be null");

        //Normalize order entry numbers to have them in sequential order
        normalizeEntryNumbers(cartModel);

        final OrderModel orderModel = getOrderService().createOrderFromCart(cartModel);
        if (orderModel != null) {
            // Store the current site and store on the order
            orderModel.setSite(getBaseSiteService().getCurrentBaseSite());
            orderModel.setStore(getBaseStoreService().getCurrentBaseStore());
            orderModel.setDate(new Date());
            if (salesApplication != null) {
                orderModel.setSalesApplication(salesApplication);
            }
            // clear the promotionResults that where cloned from cart PromotionService.transferPromotionsToOrder will copy them over bellow.
            orderModel.setAllPromotionResults(Collections.<PromotionResultModel> emptySet());
            getModelService().saveAll(customer, orderModel);

            final Integer cncStoreNumber = orderModel.getCncStoreNumber();
            if (cncStoreNumber != null) {
                LOG.info(MessageFormat.format("CNCORDER-STORENUMBER : Sending the order {0} to CNC store {1}",
                        orderModel.getCode(),
                        String.valueOf(cncStoreNumber.intValue())));
            }
            if (cartModel.getPaymentInfo() != null && cartModel.getPaymentInfo().getBillingAddress() != null) {
                final AddressModel clonedbillingAddress = getModelService().clone(
                        cartModel.getPaymentInfo().getBillingAddress());
                orderModel.setPaymentAddress(clonedbillingAddress);
                orderModel.getPaymentInfo().setBillingAddress(clonedbillingAddress);
                // I think this is a hybris bug to copy the same billing address instance to the paymentinfo
                // orderModel.getPaymentInfo().setBillingAddress(cartModel.getPaymentInfo().getBillingAddress());
                getModelService().save(orderModel.getPaymentInfo());
            }
            getModelService().save(orderModel);

            //skip promotions for partner orders
            if (!salesApplicationConfigService.isPartnerChannel(salesApplication)) {
                // Transfer promotions to the order
                getPromotionsService().transferPromotionsToOrder(cartModel, orderModel, false);

                // Properly apply any vouchers from the cart to the order - if successful then adds a voucher invalidation
                targetVoucherService.afterOrderCreation(orderModel, cartModel);

                // Calculate the order now that it has been copied
                try {
                    if (isOrderHavingNewTMDiscount(orderModel)) {
                        if (Boolean.FALSE.equals(orderModel.getCalculated())) {
                            orderModel.setCalculated(Boolean.TRUE);
                            getModelService().save(orderModel);
                        }
                        getPromotionsService().updatePromotions(getPromotionGroups(), orderModel,
                                true,
                                AutoApplyMode.APPLY_ALL,
                                AutoApplyMode.APPLY_ALL, orderModel.getDate());
                        getModelService().refresh(orderModel);
                        recalcDeliveryAfterPromos(orderModel);
                    }
                    getCalculationService().calculateTotals(orderModel, false);
                }
                catch (final CalculationException ex) {
                    LOG.error("Failed to calculate order [" + orderModel + "]", ex);
                }
            }
            else {
                try {
                    getCalculationService().recalculate(orderModel);
                }
                catch (final CalculationException e) {
                    LOG.error(MessageFormat.format(
                            TgtutilityConstants.ErrorCode.ERR_TGTEBAY
                                    + ": Failed to calculate partner order, partnerOrderNumber={0}, order={1}",
                            orderModel.getEBayOrderNumber(), orderModel.getCode()), e);
                }
            }

            if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)) {
                try {
                    orderModel.setFluentId(fluentOrderService.createOrder(orderModel));
                    getModelService().save(orderModel);

                }
                catch (final FluentOrderException ex) {
                    LOG.error(MessageFormat.format(TgtutilityConstants.ErrorCode.ERR_FLUENT_CREATE_ORDER
                            + ": Failed to create order in Fluent, order={0}, paymentStatus={1}",
                            orderModel.getCode(), orderModel.getPaymentStatus()), ex);
                }

            }
            getModelService().refresh(orderModel);
            getModelService().refresh(customer);
            getOrderService().submitOrder(orderModel);
        }

        final CommerceOrderResult orderResult = new CommerceOrderResult();
        orderResult.setOrder(orderModel);
        return orderResult;
    }

    /**
     * If order having tm discount.
     * 
     * @param orderModel
     *            the order model
     * @return true, if successful
     */
    private boolean isOrderHavingNewTMDiscount(final OrderModel orderModel) {
        for (final PromotionResultModel promotionResult : orderModel.getAllPromotionResults()) {
            if (promotionResult.getPromotion() instanceof TMDiscountProductPromotionModel) {
                return true;
            }
        }
        return false;
    }

    /**
     * Recalc delivery after promos.
     * 
     * @param orderModel
     *            the order model
     */
    private void recalcDeliveryAfterPromos(final OrderModel orderModel) {
        final DeliveryModeModel deliveryMode = orderModel.getDeliveryMode();
        if (deliveryMode instanceof TargetZoneDeliveryModeModel) {
            final ZoneDeliveryModeValueModel newValue = targetDeliveryService
                    .getZoneDeliveryModeValueForAbstractOrderAndMode(
                            (TargetZoneDeliveryModeModel)deliveryMode, orderModel);
            orderModel.setZoneDeliveryModeValue((AbstractTargetZoneDeliveryModeValueModel)newValue);
        }
    }

    /**
     * Gets the promotion groups.
     * 
     * @return the promotion groups
     */
    private Collection<PromotionGroupModel> getPromotionGroups() {
        final Collection<PromotionGroupModel> promotionGroupModels = new ArrayList<>();
        promotionGroupModels.add(getPromotionsService().getDefaultPromotionGroup());
        return promotionGroupModels;
    }

    @Override
    public boolean isFlybuysNumberValid(final CartModel cartModel) {
        Assert.notNull(cartModel, "cart model must not be null");
        final String flybuysNumber = cartModel.getFlyBuysCode();
        return isFlybuysNumberValid(flybuysNumber);
    }

    @Override
    public boolean setTMDCardNumber(final CartModel checkoutCartModel, final String tmdCardNumber) {
        Assert.notNull(checkoutCartModel, "checkout cart model must not be null");

        if (StringUtils.isNotBlank(tmdCardNumber) && targetDiscountService.isValidTMDCard(tmdCardNumber)) {
            checkoutCartModel.setTmdCardNumber(tmdCardNumber);
            checkoutCartModel.setCalculated(Boolean.FALSE);
            getModelService().save(checkoutCartModel);
            // Calculation will actually apply the applicable discounts/promotions
            final CommerceCartParameter cartParameter = new CommerceCartParameter();
            cartParameter.setCart(checkoutCartModel);
            return commerceCartService.calculateCart(cartParameter)
                    && targetDiscountService.isTMDiscountAppliedToOrder(cartParameter.getCart());
        }
        return false;
    }

    @Override
    public boolean isFlybuysNumberValid(final String flybuysNumber) {
        if (StringUtils.isBlank(flybuysNumber)) {
            return true;
        }
        if (StringUtils.isNumeric(flybuysNumber) && flybuysNumber.length() == flybuysLen
                && testFlybuysCheckDigit(flybuysNumber) && StringUtils.startsWith(flybuysNumber, flybuysPrefix)) {
            return true;
        }

        return false;
    }

    private boolean testFlybuysCheckDigit(final String flybuysNumber) {
        int len = flybuysNumber.length();

        final String temp = flybuysNumber.substring(0, --len);
        return (BarcodeTools.findCreditCardCheckDigit(temp) == flybuysNumber.charAt(len));
    }

    protected boolean checkFlybuysNumDiscount(final CartModel checkoutCartModel, final String flybuysNumber) {

        final FlybuysDiscountModel flybuysDiscountModel = targetVoucherService
                .getFlybuysDiscountForOrder(checkoutCartModel);
        if (!StringUtils.equals(flybuysNumber, checkoutCartModel.getFlyBuysCode())
                && flybuysDiscountModel != null) {

            return false;
        }

        return true;

    }


    @Override
    public boolean setFlybuysNumber(final CartModel checkoutCartModel, final String flybuysNumber) {
        Assert.notNull(checkoutCartModel, "CartModel cannot be null");

        if (!isFlybuysNumberValid(flybuysNumber)) {
            return false;
        }


        if (!checkFlybuysNumDiscount(checkoutCartModel, flybuysNumber)) {

            return false;
        }

        checkoutCartModel.setFlyBuysCode(flybuysNumber);
        getModelService().save(checkoutCartModel);
        return true;
    }

    @Override
    public boolean beforePlaceOrder(final CartModel cartModel,
            final PaymentTransactionModel paymentTransactionModel) throws InsufficientStockLevelException {
        Assert.notNull(cartModel, "CartModel cannot be null");
        Assert.notNull(cartModel.getPaymentInfo(), "PaymentInfo cannot be null");
        Assert.notNull(cartModel.getTotalPrice(), "Cart total cannot be null");

        try {
            reserveStockForCart(cartModel);
        }
        catch (final InsufficientStockLevelException | FluentOrderException e) {
            targetPaymentService.reverseGiftCardPayment(cartModel);
            throw e;
        }

        capturePayment(cartModel, paymentTransactionModel);

        final boolean captureResult = isCaptureOrderSuccessful(cartModel);
        if (paymentTransactionModel != null) {
            paymentTransactionModel.setIsCaptureSuccessful(Boolean.valueOf(captureResult));
            getModelService().save(paymentTransactionModel);
            getModelService().refresh(paymentTransactionModel);
        }
        if (!captureResult) {
            handlePaymentFailure(cartModel);
        }
        if (cartModel.getUser() instanceof TargetCustomerModel) {
            final TargetCustomerModel customer = (TargetCustomerModel)cartModel.getUser();
            if (CustomerType.GUEST.equals(customer.getType())) {
                setFirstNameAndLastNameFromDeliveryAddress(cartModel, customer);
            }
        }
        return captureResult;
    }

    /**
     * 
     * @param cartModel
     * @param customer
     */
    private void setFirstNameAndLastNameFromDeliveryAddress(final CartModel cartModel,
            final TargetCustomerModel customer) {
        final TargetAddressModel deliveryAddress = (TargetAddressModel)cartModel.getDeliveryAddress();
        if (deliveryAddress != null) {
            customer.setTitle(deliveryAddress.getTitle());
            final String firstname = StringTools.trimAllWhiteSpaces(deliveryAddress.getFirstname());
            final String lastname = StringTools.trimAllWhiteSpaces(deliveryAddress.getLastname());
            final String name = StringUtils.trimToEmpty(firstname) + ' ' + StringUtils.trimToEmpty(lastname);
            customer.setName(StringUtils.trimToNull(name));
            customer.setFirstname(firstname);
            customer.setLastname(lastname);

            try {
                getModelService().save(customer);
            }
            catch (final ModelSavingException mse) {
                LOG.warn("Unable to update customer profile", mse);
            }
        }
        else {
            LOG.warn("Unable to name guest user - no delivery address is set on cart " + cartModel.getCode());
        }
    }

    protected void capturePayment(final CartModel cartModel,
            final PaymentTransactionModel paymentTransactionModel) {

        final BigDecimal captureAmount = getAmountForPaymentCapture(cartModel);
        // Attempt the capture - this will create a PaymentsInProgress for the payment
        try {
            if (paymentTransactionModel != null) {
                targetPaymentService.capture(cartModel, cartModel.getPaymentInfo(), captureAmount,
                        cartModel.getCurrency(),
                        PaymentCaptureType.PLACEORDER, paymentTransactionModel);
            }
            else {
                targetPaymentService.capture(cartModel, cartModel.getPaymentInfo(), captureAmount,
                        cartModel.getCurrency(),
                        PaymentCaptureType.PLACEORDER);
            }

        }
        catch (final PaymentException pe) {
            handlePaymentFailure(cartModel);
            throw pe;
        }
    }

    @Override
    public boolean isCaptureOrderSuccessful(final CartModel cartModel) {
        getModelService().refresh(cartModel);
        final Double amountToBeCaptured;
        if (BooleanUtils.isTrue(cartModel.getContainsPreOrderItems())) {
            amountToBeCaptured = cartModel.getPreOrderDepositAmount();
        }
        else {
            amountToBeCaptured = cartModel.getTotalPrice();
        }

        final Double amountCaptured = findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(cartModel);
        return amountToBeCaptured.compareTo(amountCaptured) == 0;
    }

    @Override
    public void handlePaymentFailure(final CartModel cartModel) {
        Assert.notNull(cartModel, "CartModel cannot be null");
        targetPaymentService.refundLastCaptureTransaction(cartModel);
        if (paymentsInProgressService.removeInProgressPayment(cartModel)) {
            releaseStockForCart(cartModel);
        }
    }

    @Override
    public BigDecimal getAmountForPaymentCapture(final CartModel cartModel) {

        if (BooleanUtils.isTrue(cartModel.getContainsPreOrderItems())) {
            final Double preOrderDepositAmount = cartModel.getPreOrderDepositAmount();
            return preOrderDepositAmount != null ? BigDecimal.valueOf(preOrderDepositAmount.doubleValue()) : null;
        }

        final Double totalAmount = cartModel.getTotalPrice();
        if (totalAmount == null) {
            return null;
        }
        return BigDecimal.valueOf(totalAmount.doubleValue());
    }

    private void verifyProductStockQty(final Map<String, Integer> stockData, final ProductModel product,
            final CartModel cartModel, final Long amount)
            throws InsufficientStockLevelException {
        if (stockData != null) {
            final Integer productStockQty = stockData.get(product.getCode());
            if (productStockQty == null || productStockQty.intValue() < amount.intValue()) {
                throw new InsufficientStockLevelException("Product (" + product.getCode()
                        + ") does not have " + amount + " items in stock");
            }
        }
        else {
            targetStockService.reserve(
                    product,
                    amount.intValue(),
                    MessageFormat.format(STOCK_UPDATE_MESSAGE, "ReserveStock",
                            cartModel.getCode(), amount, product.getName()));
        }
    }

    /**
     * @param cartModel
     */
    protected void reserveStockForCart(final CartModel cartModel)
            throws InsufficientStockLevelException {
        final List<AbstractOrderEntryModel> orderEntryModelList = cartModel.getEntries();
        Map<String, Integer> stockData = null;

        AbstractOrderEntryModel currentCartEntry = null;
        try {
            if (CollectionUtils.isNotEmpty(orderEntryModelList)) {
                if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)) {
                    stockData = fluentStockService.lookupPlaceOrderAts(getSkuRefs(orderEntryModelList),
                            getFluentDeliveryMode(cartModel));
                }
                for (final AbstractOrderEntryModel cartEntry : orderEntryModelList) {
                    currentCartEntry = cartEntry;
                    final ProductModel product = cartEntry.getProduct();
                    final Long amount = cartEntry.getQuantity();
                    Assert.notNull(amount, "Entry Qty cannot be null");
                    verifyProductStockQty(stockData, product, cartModel, amount);
                }
            }
        }
        catch (final InsufficientStockLevelException e) {
            final TargetInsufficientStockLevelException stockLevelException = new TargetInsufficientStockLevelException(
                    e.getMessage(), currentCartEntry, e);
            throw stockLevelException;
        }
    }

    /**
     * Converts deliveryMode in cart to a deliveryMode defined in fluent
     * 
     * @param cartModel
     * @return fluent deliveryMode
     */
    protected String getFluentDeliveryMode(final CartModel cartModel) {
        if (cartModel.getDeliveryMode() == null) {
            return null;
        }
        if (BooleanUtils.isTrue(cartModel.getContainsPreOrderItems())) {
            return TgtCoreConstants.DELIVERY_TYPE.PO;
        }
        switch (cartModel.getDeliveryMode().getCode().toLowerCase()) {
            case CLICK_AND_COLLECT:
                return TgtCoreConstants.DELIVERY_TYPE.CC;
            case HOME_DELIVERY:
                return TgtCoreConstants.DELIVERY_TYPE.HD;
            case EXPRESS_DELIVERY:
                return TgtCoreConstants.DELIVERY_TYPE.ED;
            case DIGITAL_GIFT_CARD:
                return TgtCoreConstants.DELIVERY_TYPE.HD;
            default:
                LOG.warn("There is no matched fluent deliveryMode for " + cartModel.getDeliveryMode().getCode());
                return null;
        }
    }

    /**
     * Gets the collection of skuRefs from the cart's entries
     * 
     * @param orderEntryModelList
     * @return collection of skuRefs
     */
    protected Collection<String> getSkuRefs(final List<AbstractOrderEntryModel> orderEntryModelList) {
        final Collection<String> skuRefs = new ArrayList<>();
        for (final AbstractOrderEntryModel cartEntry : orderEntryModelList) {
            final ProductModel product = cartEntry.getProduct();
            skuRefs.add(product.getCode());
        }
        return skuRefs;
    }

    /**
     * @param cartModel
     */
    protected void releaseStockForCart(final CartModel cartModel) {
        final List<AbstractOrderEntryModel> orderEntryModelList = cartModel.getEntries();
        if (CollectionUtils.isNotEmpty(orderEntryModelList)) {
            for (final AbstractOrderEntryModel cartEntry : orderEntryModelList) {
                final ProductModel product = cartEntry.getProduct();
                final Long am = cartEntry.getQuantity();
                Assert.notNull(am, "Entry Qty cannot be null");
                final int amount = am.intValue();
                targetStockService.release(
                        product,
                        amount,
                        MessageFormat.format(STOCK_UPDATE_MESSAGE, "ReleaseStock",
                                cartModel.getCode(), am, product.getName()));
            }
        }
    }

    @Override
    public void afterPlaceOrder(final CartModel cartModel, final OrderModel orderModel) {
        Assert.notNull(cartModel, "CartModel cannot be null");
        Assert.notNull(orderModel, "OrderModel cannot be null");

        // if the order has been placed successfully then set the payment succeeded to true
        final PaymentInfoModel paymentInfo = cartModel.getPaymentInfo();
        final PaymentInfoModel orderPaymentInfo = orderModel.getPaymentInfo();
        paymentInfo.setIsPaymentSucceeded(Boolean.TRUE);
        orderPaymentInfo.setIsPaymentSucceeded(Boolean.TRUE);
        try {
            getModelService().saveAll(paymentInfo, orderPaymentInfo);
        }
        catch (final ModelSavingException mse) {
            LOG.warn("Cannot update payment info on cart", mse);
        }

        if (orderModel.getUser() instanceof TargetCustomerModel) {
            final TargetCustomerModel customer = (TargetCustomerModel)orderModel.getUser();
            if ((!CustomerType.GUEST.equals(customer.getType()))
                    && (StringUtils.isNotBlank(orderModel.getFlyBuysCode()))) {
                setFlyBuysCodeForCustomer(customer, orderModel.getFlyBuysCode());
            }
        }

        paymentsInProgressService.removeInProgressPayment(cartModel);
        handleCartAfterPlaceOrder(cartModel);
        getModelService().refresh(orderModel);
        userCheckoutPreferencesService.saveUserPreferences(orderModel);
    }

    /**
     * 
     * @param customer
     * @param flyBuysCode
     */
    private void setFlyBuysCodeForCustomer(final TargetCustomerModel customer, final String flyBuysCode) {
        customer.setFlyBuysCode(flyBuysCode);
        try {
            getModelService().save(customer);
        }
        catch (final ModelSavingException mse) {
            LOG.warn("Unable to update customer profile", mse);
        }
    }


    @Override
    public boolean setPaymentMode(final CartModel cartModel, final PaymentModeModel paymentMode) {
        validateParameterNotNull(cartModel, "Cart model cannot be null");
        validateParameterNotNull(paymentMode, "Payment mode model cannot be null");
        cartModel.setPaymentMode(paymentMode);
        getModelService().save(cartModel);
        final CommerceCartParameter cartParameter = new CommerceCartParameter();
        cartParameter.setCart(cartModel);
        commerceCartService.calculateCart(cartParameter);
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean removeDeliveryMode(final CommerceCheckoutParameter checkoutParameter) {
        final CartModel cartModel = checkoutParameter.getCart();
        if (cartModel != null) {
            cartModel.setZoneDeliveryModeValue(null);
        }
        checkoutParameter.setCart(cartModel);
        return super.removeDeliveryMode(checkoutParameter);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean setDeliveryMode(final CommerceCheckoutParameter checkoutParameter) {
        if (checkoutParameter.getDeliveryMode() != null && checkoutParameter.getCart() != null) {
            targetCartService.setDeliveryMode(checkoutParameter.getCart(), checkoutParameter.getDeliveryMode());
        }
        return super.setDeliveryMode(checkoutParameter);
    }

    @Override
    public boolean setKioskStoreNumber(final CartModel cartModel, final String kioskStoreNumber) {
        Validate.notNull(cartModel, "Cart model cannot be null");
        Validate.notEmpty(kioskStoreNumber, "Kiosk Store number cannot be null or empty");

        cartModel.setKioskStore(kioskStoreNumber);
        getModelService().save(cartModel);
        return true;
    }

    @Override
    public boolean setStoreAssisted(final CartModel cartModel, final StoreEmployeeModel storeEmployeeModel) {
        Validate.notNull(cartModel, "Cart model cannot be null");
        Validate.notNull(storeEmployeeModel, "Store Employee cannot be null");

        cartModel.setKioskStore(storeEmployeeModel.getStore().getStoreNumber().toString());
        cartModel.setAssistedBy(storeEmployeeModel);
        getModelService().save(cartModel);
        return true;
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtcore.order.TargetCommerceCheckoutService#removePaymentInfo(de.hybris.platform.core.model.order.CartModel)
     */
    @Override
    public void removePaymentInfo(final CartModel cartModel) {
        validateParameterNotNull(cartModel, "Cart model cannot be null");

        cartModel.setPaymentInfo(null);
        getModelService().save(cartModel);
        getModelService().refresh(cartModel);
    }

    /**
     * Method to Normalize order entry numbers to have them in sequential order
     * 
     * * @param cartModel
     */
    protected void normalizeEntryNumbers(final CartModel cartModel) {
        final List<AbstractOrderEntryModel> abstractOrderEntryModelList = cartModel.getEntries();
        if (CollectionUtils.isNotEmpty(abstractOrderEntryModelList)) {
            int counter = 0;
            final List<AbstractOrderEntryModel> retryList = new ArrayList<>();
            for (final AbstractOrderEntryModel abstractEntryModel : abstractOrderEntryModelList) {
                if (abstractEntryModel.getEntryNumber() == null
                        || abstractEntryModel.getEntryNumber().intValue() != counter) {
                    abstractEntryModel.setEntryNumber(Integer.valueOf(counter));
                    try {
                        getModelService().save(abstractEntryModel);
                    }
                    catch (final ModelSavingException exception) {
                        LOG.warn(exception.getMessage());
                        retryList.add(abstractEntryModel);
                    }
                }
                counter++;
            }
            if (CollectionUtils.isNotEmpty(retryList)) {
                getModelService().saveAll(retryList);
            }
        }
    }

    /**
     * @param checkoutCartModel
     */
    protected void handleCartAfterPlaceOrder(final CartModel checkoutCartModel) {
        targetCartService.removeSessionCart();
    }


    @Override
    public boolean shouldPaymentBeReversed(final CartModel cartModel, final List<TargetCardResult> cardResults) {
        boolean shouldReservePayment = false;

        if (isSplitPayment(cardResults) || hasGiftCardUsedInPayment(cardResults)) {
            if (!isOrderTotalMatchesIpgSubmittedTotal(cartModel, cardResults)) {
                shouldReservePayment = true;
            }
        }
        return shouldReservePayment;
    }

    /**
     * Checks whether payment was split across multiple cards
     * 
     * @param cardResults
     *            - card details used for payment
     * @return is split payment
     */
    protected boolean isSplitPayment(final List<TargetCardResult> cardResults) {
        boolean isSplitPayment = false;
        if (CollectionUtils.isNotEmpty(cardResults)) {
            isSplitPayment = cardResults.size() > 1;
        }
        return isSplitPayment;
    }

    /**
     * Checks whether at least one gift card was used in payment
     * 
     * @param cardResults
     *            - card details used for payment
     * @return has gift card been used
     */
    protected boolean hasGiftCardUsedInPayment(final List<TargetCardResult> cardResults) {
        boolean hasGiftCardUsed = false;
        if (CollectionUtils.isNotEmpty(cardResults)) {

            final List<TargetCardResult> giftCards = getGiftCards(cardResults);
            if (CollectionUtils.isNotEmpty(giftCards)) {
                hasGiftCardUsed = true;
            }
        }
        return hasGiftCardUsed;
    }

    /**
     * Checks whether order total matches with the amount paid from iFrame
     * 
     * @param cartModel
     *            - cart model
     * @param cardResults
     *            - card details used for payment
     * @return boolean
     */
    protected boolean isOrderTotalMatchesIpgSubmittedTotal(final CartModel cartModel,
            final List<TargetCardResult> cardResults) {

        final BigDecimal amountToBeCaptured = this.getAmountForPaymentCapture(cartModel);
        BigDecimal amountSubmitted = new BigDecimal("0");

        if (CollectionUtils.isNotEmpty(cardResults)) {
            for (final TargetCardResult targetCardResult : cardResults) {
                final TargetCardPaymentResult paymentResult = targetCardResult.getTargetCardPaymentResult();
                if (paymentResult != null && paymentResult.getAmount() != null) {
                    amountSubmitted = amountSubmitted.add(paymentResult.getAmount());
                }
            }
        }
        return amountToBeCaptured.compareTo(amountSubmitted) == 0 ? true : false;
    }

    @Override
    public List<TargetCardResult> getGiftCards(final List<TargetCardResult> cardResults) {
        final List<TargetCardResult> giftCards = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(cardResults)) {
            for (final TargetCardResult targetCardResult : cardResults) {
                if (targetCardResult.getCardType().equalsIgnoreCase(CreditCardType.GIFTCARD.getCode())) {
                    giftCards.add(targetCardResult);
                }
            }
        }
        return giftCards;
    }

    @Override
    public boolean removeFlybuysNumber(final CartModel checkoutCartModel) {
        Assert.notNull(checkoutCartModel, "CartModel cannot be null");
        checkoutCartModel.setFlyBuysCode(null);
        getModelService().save(checkoutCartModel);
        getModelService().refresh(checkoutCartModel);
        return true;
    }

    @Required
    public void setTargetPointOfServiceService(final TargetPointOfServiceService targetPointOfServiceService) {
        this.targetPointOfServiceService = targetPointOfServiceService;
    }

    @Required
    public void setAddressService(final AddressService addressService) {
        this.addressService = addressService;
    }

    @Required
    public void setTargetDiscountService(final TargetDiscountService targetDiscountService) {
        this.targetDiscountService = targetDiscountService;
    }

    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }

    @Required
    public void setTargetPaymentService(final TargetPaymentService targetPaymentService) {
        this.targetPaymentService = targetPaymentService;
    }

    @Required
    public void setTargetCartService(final TargetCartService targetCartService) {
        this.targetCartService = targetCartService;
    }

    @Required
    public void setTargetVoucherService(final TargetVoucherService targetVoucherService) {
        this.targetVoucherService = targetVoucherService;
    }

    @Required
    public void setPaymentsInProgressService(final PaymentsInProgressService paymentsInProgressService) {
        this.paymentsInProgressService = paymentsInProgressService;
    }

    /**
     * @param flybuysPrefix
     *            the flybuysPrefix to set
     */
    @Required
    public void setFlybuysPrefix(final String flybuysPrefix) {
        this.flybuysPrefix = flybuysPrefix;
    }

    /**
     * @param flybuysLen
     *            the flybuysLen to set
     */
    @Required
    public void setFlybuysLen(final int flybuysLen) {
        this.flybuysLen = flybuysLen;
    }

    /**
     * @param targetDeliveryService
     *            the targetDeliveryService to set
     */
    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        this.targetDeliveryService = targetDeliveryService;
    }

    /**
     * @param findOrderTotalPaymentMadeStrategy
     *            the findOrderTotalPaymentMadeStrategy to set
     */
    @Required
    public void setFindOrderTotalPaymentMadeStrategy(
            final FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy) {
        this.findOrderTotalPaymentMadeStrategy = findOrderTotalPaymentMadeStrategy;
    }

    /**
     * @param commerceCartService
     *            the commerceCartService to set
     */
    @Required
    public void setCommerceCartService(final CommerceCartService commerceCartService) {
        this.commerceCartService = commerceCartService;
    }

    /**
     * @param userCheckoutPreferencesService
     *            the userCheckoutPreferencesService to set
     */
    @Required
    public void setUserCheckoutPreferencesService(final UserCheckoutPreferencesService userCheckoutPreferencesService) {
        this.userCheckoutPreferencesService = userCheckoutPreferencesService;
    }

    /**
     * @param salesApplicationConfigService
     *            the salesApplicationConfigService to set
     */
    @Required
    public void setSalesApplicationConfigService(
            final TargetSalesApplicationConfigService salesApplicationConfigService) {
        this.salesApplicationConfigService = salesApplicationConfigService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * @param fluentOrderService
     *            the fluentOrderService to set
     */
    @Required
    public void setFluentOrderService(final FluentOrderService fluentOrderService) {
        this.fluentOrderService = fluentOrderService;
    }

    /**
     * @param fluentStockService
     *            the fluentStockService to set
     */
    @Required
    public void setFluentStockService(final FluentStockService fluentStockService) {
        this.fluentStockService = fluentStockService;
    }



}
