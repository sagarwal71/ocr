/**
 * 
 */
package au.com.target.tgtcore.order.impl;

import de.hybris.platform.basecommerce.enums.OrderModificationEntryStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordModel;

import org.springframework.util.Assert;

import au.com.target.tgtcore.order.FindOrderRefundedShippingStrategy;
import au.com.target.tgtcore.order.OrderStateException;


public class FindOrderRefundedShippingStrategyImpl implements FindOrderRefundedShippingStrategy {

    @Override
    public double getRefundedShippingAmount(final OrderModel order) {
        Assert.notNull(order, "order model must not be null");
        double result = 0;
        if (order.getModificationRecords() != null) {
            for (final OrderModificationRecordModel parentRecord : order.getModificationRecords()) {
                if (parentRecord.getModificationRecordEntries() != null) {
                    for (final OrderModificationRecordEntryModel record : parentRecord.getModificationRecordEntries()) {
                        if (record.getRefundedShippingAmount() != null
                                && record.getStatus() != OrderModificationEntryStatus.FAILED) {
                            result += record.getRefundedShippingAmount().doubleValue();
                        }
                    }
                }
            }
        }
        if (order.getInitialDeliveryCost() != null && result > order.getInitialDeliveryCost().doubleValue()) {
            throw new OrderStateException(String.format(
                    "Refunded shipping cost (%.2f) is more than actually paid (%.2f)",
                    Double.valueOf(result), order.getInitialDeliveryCost()));
        }
        return result;
    }


}
