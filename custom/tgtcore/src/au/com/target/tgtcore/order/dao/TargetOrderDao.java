/**
 * 
 */
package au.com.target.tgtcore.order.dao;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.Date;
import java.util.List;


public interface TargetOrderDao {

    /**
     * get order for order id
     * 
     * @param orderId
     * @return OrderModel
     */
    OrderModel findOrderModelForOrderId(String orderId);

    /**
     * get cart for order id
     * 
     * @param orderId
     * @return CartModel
     */
    CartModel findCartModelForOrderId(String orderId);

    /**
     * Method to find orders for given partner order id and sales channel.
     *
     * @param partnerOrderId
     * @param salesChannel
     * @return OrderModel
     */
    List<OrderModel> findOrdersForPartnerOrderIdAndSalesChannel(String partnerOrderId, String salesChannel);

    /**
     * Method to fetch latest order placed by the user
     * 
     * @param user
     * @return OrderModel
     */
    OrderModel findLatestOrderForUser(UserModel user);

    /**
     * @param fluentId
     * @return {@link OrderModel}
     */
    OrderModel findOrderByFluentId(String fluentId);


    /**
     * Method to retrieve all parked orders with release date prior to the specified normal sales start date or null.
     *
     * @param normalSalesStartDate
     * @return List<OrderModel>
     */
    List<OrderModel> findOrdersBeforeNormalSalesStartDate(Date normalSalesStartDate, OrderStatus status);

    /**
     * Retrieve all pending preOrders (PARKED, REVIEW and REVIEW_ON_HOLD) for a given product
     * 
     * @param productCode
     * @return list of {@link OrderModel}
     */
    List<OrderModel> findPendingPreOrdersForProduct(String productCode);

}
