package au.com.target.tgtcore.order.dao.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.internal.dao.SortParameters;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import com.google.common.base.Preconditions;

import au.com.target.tgtcore.order.dao.TargetOrderDao;


public class TargetOrderDaoImpl extends DefaultGenericDao<OrderModel> implements TargetOrderDao {

    private static final String FIND_ORDER_BY_CODE = "SELECT {" + OrderModel.PK + "} FROM {" + OrderModel._TYPECODE +
            "} WHERE {" + OrderModel.CODE + "}=?code and {" + OrderModel.ORIGINALVERSION + "} is null";

    private static final String FIND_CART_BY_CODE = "SELECT { " + CartModel.PK + "} " +
            "FROM {" + CartModel._TYPECODE + "} " + "WHERE { " + CartModel.CODE + "} = ?code";

    private static final String FIND_ORDER_BY_PARTNER_ORDER_ID_AND_SALES_CHANNEL = "SELECT {" + OrderModel.PK
            + "} FROM {" + OrderModel._TYPECODE + " AS or JOIN " + SalesApplication._TYPECODE + " AS sa ON {or."
            + OrderModel.SALESAPPLICATION + "} = {sa.pk}} WHERE {" + OrderModel.EBAYORDERNUMBER
            + "} = ?eBayOrderNumber AND {sa.code}=?salesChannel";

    private static final String FIND_ORDER_BY_FLUENTID = "SELECT {" + OrderModel.PK + "} FROM {" + OrderModel._TYPECODE
            + "} WHERE {" + OrderModel.FLUENTID + "} = ?" + OrderModel.FLUENTID + " AND {" + OrderModel.ORIGINALVERSION
            + "} IS NULL";

    private static final String FIND_ORDERS_BEFORE_NORMAL_SALES_DATE = "Select { " + OrderModel.PK + "} FROM { "
            + OrderModel._TYPECODE
            + "} WHERE {Order.STATUS} = ?orderStatus  and {" + OrderModel.NORMALSALESTARTDATETIME
            + "} <= ?normalSaleStartDateTime AND {" + OrderModel.CONTAINSPREORDERITEMS
            + "} = ?containsPreOrderItems AND {"
            + OrderModel.ORIGINALVERSION + "} IS NULL ORDER BY {" + OrderModel.CREATIONTIME
            + "} ASC";

    private static final String FIND_PENDING_PREORDERS_FOR_PRODUCT = "SELECT {o:" + OrderModel.PK + "} FROM {"
            + OrderEntryModel._TYPECODE + " AS oe JOIN " + OrderModel._TYPECODE + " AS o ON {oe:"
            + OrderEntryModel.ORDER + "} = {o:" + OrderModel.PK + "} JOIN " + ProductModel._TYPECODE + " AS p ON {oe:"
            + OrderEntryModel.PRODUCT + "} = {p:" + ProductModel.PK + "}} WHERE {p:" + ProductModel.CODE
            + "} = ?productCode AND {o:" + OrderModel.STATUS + "} IN (?orderStatuses) AND {o:"
            + OrderModel.CONTAINSPREORDERITEMS + "} = ?containsPreOrderItems AND {o:" + OrderModel.ORIGINALVERSION
            + "} IS NULL";

    public TargetOrderDaoImpl() {
        super(OrderModel._TYPECODE);
    }

    @Override
    public OrderModel findOrderModelForOrderId(final String orderId) {
        final SearchResult<OrderModel> searchResult = getFlexibleSearchService().search(FIND_ORDER_BY_CODE,
                Collections.singletonMap(OrderModel.CODE, orderId));
        final List<OrderModel> orders = searchResult.getResult();
        ServicesUtil.validateIfSingleResult(orders, OrderModel.class, OrderModel.CODE, orderId);
        return orders.get(0);

    }

    @Override
    public CartModel findCartModelForOrderId(final String orderId) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(orderId), "Code must be not empty");
        final List<CartModel> result = searchCarts(FIND_CART_BY_CODE,
                Collections.<String, Object> singletonMap("code", orderId));
        return CollectionUtils.isEmpty(result) ? null : result.get(0);
    }

    private List<CartModel> searchCarts(final String query, final Map<String, Object> params) {
        final SearchResult<CartModel> searchResult = getFlexibleSearchService().search(query, params);
        final List<CartModel> result = searchResult.getResult();
        return result == null ? Collections.EMPTY_LIST : result;
    }

    @Override
    public List<OrderModel> findOrdersForPartnerOrderIdAndSalesChannel(final String partnerOrderId,
            String salesChannel) {
        if (StringUtils.isEmpty(salesChannel)) {
            salesChannel = SalesApplication.EBAY.getCode();
        }

        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_ORDER_BY_PARTNER_ORDER_ID_AND_SALES_CHANNEL);
        query.addQueryParameter("eBayOrderNumber", partnerOrderId);
        query.addQueryParameter("salesChannel", salesChannel);

        final SearchResult<OrderModel> resultSet = getFlexibleSearchService().search(query);
        final List<OrderModel> orders = resultSet.getResult();

        return CollectionUtils.isEmpty(orders) ? null : orders;
    }

    @Override
    public OrderModel findLatestOrderForUser(final UserModel user) {
        Assert.notNull(user, "User model cannot be null");
        final List<OrderModel> orders = find(Collections.singletonMap(OrderModel.USER, user),
                SortParameters.singletonDescending(OrderModel.CREATIONTIME), 1);
        return CollectionUtils.isEmpty(orders) ? null : orders.get(0);
    }

    @Override
    public OrderModel findOrderByFluentId(final String fluentId) {
        final SearchResult<OrderModel> searchResult = getFlexibleSearchService().search(FIND_ORDER_BY_FLUENTID,
                Collections.singletonMap(OrderModel.FLUENTID, fluentId));
        final List<OrderModel> orders = searchResult.getResult();
        ServicesUtil.validateIfSingleResult(orders, OrderModel.class, OrderModel.FLUENTID, fluentId);
        return orders.get(0);
    }

    @Override
    public List<OrderModel> findOrdersBeforeNormalSalesStartDate(final Date normalSalesStartDate,
            final OrderStatus status) {

        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_ORDERS_BEFORE_NORMAL_SALES_DATE);
        query.addQueryParameter("orderStatus", status);
        query.addQueryParameter("normalSaleStartDateTime", normalSalesStartDate);
        query.addQueryParameter("containsPreOrderItems", Boolean.TRUE);

        final SearchResult<OrderModel> resultSet = getFlexibleSearchService().search(query);
        final List<OrderModel> orders = resultSet.getResult();

        return CollectionUtils.isEmpty(orders) ? null : orders;
    }

    @Override
    public List<OrderModel> findPendingPreOrdersForProduct(final String productCode) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_PENDING_PREORDERS_FOR_PRODUCT);
        query.addQueryParameter("productCode", productCode);
        query.addQueryParameter("orderStatuses",
                Arrays.asList(OrderStatus.PARKED, OrderStatus.REVIEW, OrderStatus.REVIEW_ON_HOLD));
        query.addQueryParameter("containsPreOrderItems", Boolean.TRUE);
        final SearchResult<OrderModel> result = getFlexibleSearchService().search(query);
        return result.getResult();
    }
}
