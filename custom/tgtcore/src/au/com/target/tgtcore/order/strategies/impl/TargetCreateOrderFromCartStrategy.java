package au.com.target.tgtcore.order.strategies.impl;

//CHECKSTYLE:OFF
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.strategies.impl.DefaultCreateOrderFromCartStrategy;

import org.springframework.util.Assert;


//CHECKSTYLE:ON

/**
 * Extended version to enable {@link OrderModel} to retain the same code as {@link CartModel}
 * 
 */
public class TargetCreateOrderFromCartStrategy extends DefaultCreateOrderFromCartStrategy {

    @Override
    protected String generateOrderCode(final CartModel cart) {
        Assert.notNull(cart, "Cartmodel cannot be null");
        return cart.getCode();
    }

}
