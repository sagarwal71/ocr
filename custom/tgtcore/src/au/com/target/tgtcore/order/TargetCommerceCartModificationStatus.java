/**
 * 
 */
package au.com.target.tgtcore.order;

import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;


public interface TargetCommerceCartModificationStatus extends CommerceCartModificationStatus {

    /**
     * Indicates that no modification has been made to a entry
     */
    String NO_MODIFICATION_OCCURRED = "noModificationOccurred";

    /**
     * Indicates that the product is out of stock now
     */
    String NO_STOCK = "noStock";

    /**
     * Indicates that the quantity is lower than the minimum order amount
     */
    String NO_MODFICATION_OCCURED_INSUFFICIENT_QUANTITY = "noModificationOccurredInsufficientQty";

    /**
     * Indicates that the product is in preview and cannot be sold.
     */
    String PREVIEW = "preview";

    /**
     * Indicates the cart contains the maximum entries allowed
     */
    String NO_MODIFICATION_OCCURED_CART_ENTRIES_THRESHOLD_REACHED = "noModificationOccurredCartEntriesThresholdReached";

    /**
     * Indicates the attempt to add preOrder and normal product
     */
    String NO_MIXED_BASKET_WITH_PRE_ORDERS = "noMixedBasketWithPreOrders";

    /**
     * Indicates that the stock lookup using fluent failed
     */
    String FLUENT_LOOKUP_FAILED = "fluentLookupFailed";

    /**
     * Indicates that the product is low stock for preOrder
     */
    String PREORDER_LOW_STOCK = "preOrder.lowStock";

    /**
     * Indicates that the product reached the items threshold configured
     */
    String ITEMS_THRESHOLD_REACHED = "itemsThresholdReached";
}
