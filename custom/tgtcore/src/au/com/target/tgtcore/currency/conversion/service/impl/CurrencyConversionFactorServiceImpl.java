/**
 * 
 */
package au.com.target.tgtcore.currency.conversion.service.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.currency.conversion.dao.CurrencyConversionFactorDao;
import au.com.target.tgtcore.currency.conversion.service.CurrencyConversionFactorService;
import au.com.target.tgtcore.model.CurrencyConversionFactorsModel;


/**
 * @author Nandini
 *
 */
public class CurrencyConversionFactorServiceImpl implements CurrencyConversionFactorService {

    private CurrencyConversionFactorDao currencyConversionFactorDao;

    private CommonI18NService commonI18NService;

    @Override
    public CurrencyConversionFactorsModel findLatestCurrencyFactor(final OrderModel order) {
        if (null != order) {
            final Date date = order.getDate();
            final CurrencyModel currencyModel = order.getCurrency();
            final CurrencyModel baseCurrencyModel = commonI18NService.getBaseCurrency();
            if (null != order.getDate() && !baseCurrencyModel.equals(currencyModel)) {
                return currencyConversionFactorDao.findLatestCurrencyFactor(date, currencyModel, baseCurrencyModel);
            }
        }
        return null;
    }


    @Override
    public double convertPriceToBaseCurrency(final double price, final OrderModel order) {
        double result = price;
        if (order != null && order.getCurrency() != null
                && !order.getCurrency().equals(commonI18NService.getBaseCurrency())
                && order.getCurrencyConversionFactor() != null) {
            final BigDecimal bp = BigDecimal.valueOf(price);
            final BigDecimal cf = BigDecimal.valueOf(order.getCurrencyConversionFactor().doubleValue());
            result = bp.multiply(cf).setScale(2, RoundingMode.HALF_UP).doubleValue();
        }
        return result;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.currency.conversion.service.CurrencyConversionFactorService#canConvertToBaseCurrency(de.hybris.platform.core.model.order.OrderModel)
     */
    @Override
    public boolean canConvertToBaseCurrency(final OrderModel order) {
        return (order != null && order.getCurrency() != null
                && (order.getCurrency().equals(commonI18NService.getBaseCurrency())
                        || order.getCurrencyConversionFactor() != null));
    }


    /**
     * @param currencyConversionFactorDao
     *            the currencyConversionFactorDao to set
     */
    @Required
    public void setCurrencyConversionFactorDao(final CurrencyConversionFactorDao currencyConversionFactorDao) {
        this.currencyConversionFactorDao = currencyConversionFactorDao;
    }

    /**
     * @param commonI18NService
     *            the commonI18NService to set
     */
    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }


}
