/**
 * 
 */
package au.com.target.tgtcore.currency.conversion.dao.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtcore.currency.conversion.dao.CurrencyConversionFactorDao;
import au.com.target.tgtcore.model.CurrencyConversionFactorsModel;


/**
 * @author Nandini
 *
 */
public class CurrencyConversionFactorDaoImpl extends DefaultGenericDao<CurrencyConversionFactorsModel> implements
        CurrencyConversionFactorDao {

    private static final Logger LOG = Logger.getLogger(CurrencyConversionFactorDaoImpl.class);

    private static final String FIND_CURRENECY_CONVRSION_BY_DATE = "SELECT {" + CurrencyConversionFactorsModel.PK
            + "} FROM {" + CurrencyConversionFactorsModel._TYPECODE
            + "} WHERE {" + CurrencyConversionFactorsModel.DATE + "} <  ?date AND {"
            + CurrencyConversionFactorsModel.SOURCECURRENCY + "} = ?source AND {"
            + CurrencyConversionFactorsModel.TARGETCURRENCY + "} = ?target ORDER BY {"
            + CurrencyConversionFactorsModel.DATE + "} DESC";

    public CurrencyConversionFactorDaoImpl() {
        super(CurrencyConversionFactorsModel._TYPECODE);
    }

    @Override
    public CurrencyConversionFactorsModel findLatestCurrencyFactor(final Date date, final CurrencyModel source,
            final CurrencyModel target) {
        final Map<String, Object> params = new HashMap<String, Object>(4, 1);
        params.put("date", date);
        params.put("source", source);
        params.put("target", target);

        final SearchResult<CurrencyConversionFactorsModel> searchResult =
                getFlexibleSearchService().search(FIND_CURRENECY_CONVRSION_BY_DATE, params);

        final List<CurrencyConversionFactorsModel> models = searchResult.getResult();
        if (CollectionUtils.isNotEmpty(models)) {
            return models.get(0);
        }
        LOG.error(MessageFormat.format(
                "There is no currency conversion factors model can be found for currency conversion from {0} to {1}",
                source.getIsocode(), target.getIsocode()));
        return null;
    }

}
