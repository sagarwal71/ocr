package au.com.target.tgtcore.util;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.promotions.jalo.PromotionOrderEntryConsumed;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.model.TargetPromotionOrderEntryConsumedModel;

import java.util.Collection;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;


/**
 * This class holds all the utility methods required to calculate deals in the order
 * 
 * @author rsamuel3
 * 
 */
public final class DealUtils {

    private static final float DEAL_FIRED_CERTAINTY = 1.0f;

    /**
     * never instantiated
     */
    private DealUtils() {
        //never instantiated
    }

    /**
     * sum up the discounts for each orderEntry
     * 
     * @param orderEntry
     * @return summation of all the discounts
     */
    public static double getTotalDiscounts(final AbstractOrderEntry orderEntry,
            final PromotionOrderEntryConsumed consumedEntry) {
        return getTotalDiscounts(orderEntry.getBasePrice().doubleValue(), consumedEntry.getAdjustedUnitPrice()
                .doubleValue());
    }

    private static double getTotalDiscounts(final double basePrice, final double adjustedUnitPrice) {
        return basePrice - adjustedUnitPrice;
    }

    /**
     * sum up the discounts for each orderEntry
     * 
     * @param orderEntry
     * @return summation of all the discounts
     */
    public static double getTotalDiscounts(final AbstractOrderEntryModel orderEntry,
            final PromotionOrderEntryConsumedModel consumedEntry) {
        return getTotalDiscounts(orderEntry.getBasePrice().doubleValue(), consumedEntry.getAdjustedUnitPrice()
                .doubleValue());
    }

    public static boolean isEntryPartOfDeal(final AbstractOrderEntryModel orderEntryModel) {
        final AbstractOrderModel orderModel = orderEntryModel.getOrder();
        final Set<PromotionResultModel> promoResults = orderModel.getAllPromotionResults();
        for (final PromotionResultModel promoResult : promoResults) {
            // only consider fired Target Deals
            if (promoResult.getPromotion() instanceof AbstractDealModel
                    && promoResult.getCertainty().floatValue() >= DEAL_FIRED_CERTAINTY) {
                for (final PromotionOrderEntryConsumedModel promotionOrderEntryConsumedModel : promoResult
                        .getConsumedEntries()) {
                    if (orderEntryModel.equals(promotionOrderEntryConsumedModel.getOrderEntry())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param instanceIndex
     * @param consumedEntries
     * @return Collection of consumed entries for that instance
     */
    public static Collection<PromotionOrderEntryConsumedModel> getConsumedEntriesForTheInstance(
            final int instanceIndex,
            final Collection<PromotionOrderEntryConsumedModel> consumedEntries) {
        return CollectionUtils.select(
                consumedEntries, new Predicate() {
                    @Override
                    public boolean evaluate(final Object arg0) {
                        if (arg0 instanceof TargetPromotionOrderEntryConsumedModel) {
                            final TargetPromotionOrderEntryConsumedModel consumedModel = (TargetPromotionOrderEntryConsumedModel)arg0;
                            return instanceIndex == consumedModel.getInstance().intValue();
                        }
                        return false;
                    }
                });
    }
}
