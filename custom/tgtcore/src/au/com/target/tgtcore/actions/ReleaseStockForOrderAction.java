/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.task.RetryLaterException;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.ordercancel.TargetCancelService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;


/**
 * ReleaseStockForOrderAction
 * 
 * @author jjayawa1
 *
 */
public class ReleaseStockForOrderAction extends AbstractProceduralAction<OrderProcessModel> {

    private TargetCancelService targetCancelService;
    private TargetFeatureSwitchService targetFeatureSwitchService;
    private TargetWarehouseService targetWarehouseService;
    private TargetStockService targetStockService;
    private OrderProcessParameterHelper orderProcessParameterHelper;

    /* (non-Javadoc)
     * @see de.hybris.platform.processengine.action.AbstractProceduralAction#executeAction(de.hybris.platform.processengine.model.BusinessProcessModel)
     */
    @Override
    public void executeAction(final OrderProcessModel orderProcessModel) throws RetryLaterException, Exception {

        //added for Falcon changes- when the consignment moved to shipped then relesae stock from rw and decrement from cw
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FASTLINE_FALCON)) {

            final ConsignmentModel consignment = orderProcessParameterHelper
                    .getConsignment(orderProcessModel);
            Assert.notNull(consignment, "consignment can not be null");
            final boolean isPPDStore = checkPickPackDispatchStore(consignment);
            transferStockForPPDStore(consignment, isPPDStore);
        }
        else {
            final OrderModel order = orderProcessModel.getOrder();
            targetCancelService.releaseAllStockForOrder(order);
        }
    }

    /**
     * @param consignment
     * @param isPPDStore
     */
    private void transferStockForPPDStore(final ConsignmentModel consignment, final boolean isPPDStore) {

        final WarehouseModel consolidatedWarehouse = targetWarehouseService
                .getWarehouseForCode(TgtCoreConstants.CONSOLIDATED_STORE_WAREHOUSE);
        final WarehouseModel reservationWarehouse = targetWarehouseService
                .getWarehouseForCode(TgtCoreConstants.STOCK_RESERVATION_WAREHOUSE);
        for (final ConsignmentEntryModel conEntryModel : consignment.getConsignmentEntries()) {
            if (conEntryModel.getShippedQuantity() != null && conEntryModel.getShippedQuantity().intValue() > 0) {
                targetStockService.release(conEntryModel.getOrderEntry().getProduct(), reservationWarehouse,
                        conEntryModel.getShippedQuantity().intValue(), "Release from RW");
                if (isPPDStore) {
                    targetStockService.adjustActualAmount(conEntryModel.getOrderEntry().getProduct(),
                            consolidatedWarehouse,
                            -conEntryModel.getShippedQuantity().intValue(), "Decrement from CW");
                }
            }

        }

    }

    /**
     * @param consignment
     * @return true or false
     */
    protected boolean checkPickPackDispatchStore(final ConsignmentModel consignment) {
        final PointOfServiceModel pointOfServiceModel = consignment.getWarehouse().getPointsOfService().iterator()
                .next();
        if (pointOfServiceModel instanceof TargetPointOfServiceModel) {
            final TargetPointOfServiceModel posModel = (TargetPointOfServiceModel)pointOfServiceModel;
            if (null != posModel.getFulfilmentCapability()) {
                return posModel.getFulfilmentCapability().getPickPackDispatchStore() != null
                        ? posModel.getFulfilmentCapability().getPickPackDispatchStore().booleanValue() : false;
            }
        }
        return false;
    }


    /**
     * @param targetCancelService
     *            the targetCancelService to set
     */
    @Required
    public void setTargetCancelService(final TargetCancelService targetCancelService) {
        this.targetCancelService = targetCancelService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */
    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }

    /**
     * @param targetStockService
     *            the targetStockService to set
     */
    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }


}
