/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;
import au.com.target.tgtbusproc.aop.BusinessProcessLoggingAspect;
import au.com.target.tgtbusproc.exceptions.BusinessProcessSmsException;
import au.com.target.tgtcore.sms.TargetSendSmsService;
import au.com.target.tgtcore.sms.dto.TargetSendSmsResponseDto;
import au.com.target.tgtcore.sms.enums.TargetSendSmsResponseType;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * @author knemalik
 * 
 */
public class SendClickAndCollectSmsNotificationAction extends RetryActionWithSpringConfig<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(SendClickAndCollectSmsNotificationAction.class);

    private TargetSendSmsService targetSendSmsService;

    private String frontendTemplateName;


    public enum Transition
    {
        OK, RETRYEXCEEDED, NOK;

        public static Set<String> getStringValues()
        {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values())
            {
                res.add(t.toString());
            }
            return res;
        }
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtbusproc.actions.RetryAction#executeInternal(de.hybris.platform.processengine.model.BusinessProcessModel)
     */
    @Override
    protected String executeInternal(final OrderProcessModel process) throws RetryLaterException, Exception {
        final OrderModel order = process.getOrder();
        final TargetSendSmsResponseDto targetSendSmsResponseDto;
        Transition result = Transition.OK;
        Assert.notNull(order, "Order cannot be null");
        if (StringUtils.isBlank(getMobileNumber(order))) {
            LOG.info("Mobile number not found,skipping CNC SMS notification: OrderCode=" + order.getCode());
            return result.toString();
        }
        try {
            targetSendSmsResponseDto = targetSendSmsService.sendSmsForCncNotification(
                    process, frontendTemplateName);
        }
        catch (final BusinessProcessSmsException e) {
            LOG.warn(String.format(BusinessProcessLoggingAspect.ERROR_MESSAGE_FORMAT,
                    e.getMessage(),
                    TgtutilityConstants.ErrorCode.WARN_SMS_SEND_FAIL,
                    SendClickAndCollectSmsNotificationAction.class.getSimpleName(),
                    process.getCode(), order.getCode()));
            result = Transition.NOK;
            return result.toString();
        }

        return getTransition(targetSendSmsResponseDto, order);

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtbusproc.actions.RetryAction#getTransitionsInternal()
     */
    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }


    /**
     * Gets the transition.
     * 
     * @param targetSendSmsResponseDto
     *            the targetSendSmsResponseDto
     * @param order
     *            the order
     * @return the transition
     * @throws RetryLaterException
     *             the retry later exception
     */
    protected String getTransition(final TargetSendSmsResponseDto targetSendSmsResponseDto, final OrderModel order)
            throws RetryLaterException {

        Transition result = Transition.NOK;
        if (targetSendSmsResponseDto != null)
        {

            if (TargetSendSmsResponseType.UNAVAILABLE.equals(targetSendSmsResponseDto.getResponseType()))
            {
                throw new RetryLaterException("Sms service is unavailable for order: " + order.getCode());

            }
            else if (TargetSendSmsResponseType.INVALID.equals(targetSendSmsResponseDto.getResponseType()))
            {
                result = Transition.NOK;


                LOG.warn(SplunkLogFormatter.formatMessage(
                        "Sms service has returned INVALID status for order: " + order.getCode(),
                        TgtutilityConstants.ErrorCode.WARN_SMS_SEND_FAIL));

            }
            else if (TargetSendSmsResponseType.OTHERS.equals(targetSendSmsResponseDto.getResponseType()))
            {
                result = Transition.NOK;
                LOG.warn(SplunkLogFormatter.formatMessage(
                        "Sms service has returned error for order: " + order.getCode(),
                        TgtutilityConstants.ErrorCode.WARN_SMS_SEND_FAIL));
            }
            else
            {
                result = Transition.OK;
            }

        }

        return result.toString();
    }

    /**
     * @param targetSendSmsService
     *            the targetSendSmsService to set
     */
    @Required
    public void setTargetSendSmsService(final TargetSendSmsService targetSendSmsService) {
        this.targetSendSmsService = targetSendSmsService;
    }

    /**
     * @param frontendTemplateName
     *            the frontendTemplateName to set
     */
    @Required
    public void setFrontendTemplateName(final String frontendTemplateName) {
        this.frontendTemplateName = frontendTemplateName;
    }


    private String getMobileNumber(final OrderModel orderModel) {
        return orderModel.getDeliveryAddress() != null ? orderModel.getDeliveryAddress().getPhone1() : null;

    }
}
