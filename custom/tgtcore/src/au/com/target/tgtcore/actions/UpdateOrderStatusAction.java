/**
 * 
 */
package au.com.target.tgtcore.actions;


import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;


public class UpdateOrderStatusAction extends AbstractProceduralAction<OrderProcessModel> {

    private OrderStatus orderStatus;
    private List<OrderStatus> onlyIfPreviousStatusList = null;

    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process, "OrderProcessModel must not be null");
        final OrderModel order = process.getOrder();
        Assert.notNull(order, "Order must not be null");
        if (CollectionUtils.isEmpty(onlyIfPreviousStatusList)
                || onlyIfPreviousStatusList.contains(order.getStatus())) {
            setOrderStatus(order, orderStatus);
        }
    }

    /**
     * @param orderStatus
     *            the orderStatus to set
     */
    @Required
    public void setOrderStatus(final OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * If the status is only to be set if the order has an existing status, set this. Defaults to null, so not setting
     * will result in the status always updating.
     * 
     * @param onlyIfPreviousStatusList
     *            the onlyIfPreviousStatusList to set, null for no previous status restriction
     */
    public void setOnlyIfPreviousStatusList(final List<OrderStatus> onlyIfPreviousStatusList) {
        this.onlyIfPreviousStatusList = onlyIfPreviousStatusList;
    }

}
