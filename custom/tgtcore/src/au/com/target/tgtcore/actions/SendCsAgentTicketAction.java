/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;


public class SendCsAgentTicketAction extends AbstractProceduralAction<BusinessProcessModel> {
    private static final Logger LOG = Logger.getLogger(SendCsAgentTicketAction.class);

    private TargetTicketBusinessService targetTicketBusinessService;
    private String headline;
    private String subject;
    private String text;
    private String group;

    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Override
    public void executeAction(final BusinessProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process, "BusinessProcessModel must not be null");
        OrderModel order = null;
        if (process instanceof OrderProcessModel) {
            order = ((OrderProcessModel)process).getOrder();
        }
        final StringBuilder ticketText = new StringBuilder();
        ticketText.append(generateTicketBodyText(process));
        if (order != null) {
            ticketText.append(" for order=").append(order.getCode());
        }
        targetTicketBusinessService.createCsAgentTicket(headline, subject, ticketText.toString(), order, group);

        final StringBuilder logMsg = new StringBuilder();
        logMsg.append("Successfully raised ticket for cs agent");
        if (order != null) {
            logMsg.append(" for order:").append(order.getCode());
        }
        LOG.info(logMsg);
    }

    /**
     * @param process
     */
    protected String generateTicketBodyText(final BusinessProcessModel process) {
        return text;
    }

    /**
     * @param headline
     *            the headline to set
     */
    @Required
    public void setHeadline(final String headline) {
        this.headline = headline;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject
     *            the subject to set
     */
    public void setSubject(final String subject) {
        this.subject = subject;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text
     *            the text to set
     */
    public void setText(final String text) {
        this.text = text;
    }

    /**
     * @param targetTicketBusinessService
     *            the targetTicketBusinessService to set
     */
    @Required
    public void setTargetTicketBusinessService(final TargetTicketBusinessService targetTicketBusinessService) {
        this.targetTicketBusinessService = targetTicketBusinessService;
    }

    /**
     * @return the orderProcessParameterHelper
     */
    public OrderProcessParameterHelper getOrderProcessParameterHelper() {
        return orderProcessParameterHelper;
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }

    /**
     * @param group
     *            the group to set
     */
    public void setGroup(final String group) {
        this.group = group;
    }
}
