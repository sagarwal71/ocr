/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;


/**
 * Class the calculate the total weight of consignment based on weights of the products in consignment entries.
 * 
 * @author ayushman
 *
 */
public class TotalConsignmentWeightAttributeHandler extends
        AbstractDynamicAttributeHandler<Double, TargetConsignmentModel> {

    @Override
    public Double get(final TargetConsignmentModel model) {

        // model is null or has no entries
        if (null == model || CollectionUtils.isEmpty(model.getConsignmentEntries())) {
            return null;
        }

        BigDecimal totalWeight = BigDecimal.ZERO;

        AbstractTargetVariantProductModel targetVariantProductModel = null;

        for (final ConsignmentEntryModel conEntryModel : model.getConsignmentEntries()) {

            final AbstractOrderEntryModel orderEntryModel = conEntryModel.getOrderEntry();

            // entry is null or product is null or qty is null or product is not a variant
            if (null == orderEntryModel || null == orderEntryModel.getProduct()
                    || null == orderEntryModel.getQuantity()
                    || !(orderEntryModel.getProduct() instanceof AbstractTargetVariantProductModel)) {
                return null;
            }

            targetVariantProductModel = (AbstractTargetVariantProductModel)orderEntryModel.getProduct();
            final TargetProductDimensionsModel productPackageDimensions = targetVariantProductModel
                    .getProductPackageDimensions();

            // if dimensions are null or weight is null or qty is null
            if (null == productPackageDimensions
                    || null == productPackageDimensions.getWeight()) {
                return null;
            }

            totalWeight = totalWeight.add(BigDecimal.valueOf(productPackageDimensions.getWeight().doubleValue()
                    * orderEntryModel.getQuantity().doubleValue()));
        }

        return Double.valueOf(totalWeight.setScale(2, RoundingMode.HALF_UP).doubleValue());
    }
}
