/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.springframework.util.Assert;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * Get the Product Type from the base product of this variant.
 */
public class VariantProductType implements
        DynamicAttributeHandler<ProductTypeModel, AbstractTargetVariantProductModel> {

    @Override
    public ProductTypeModel get(final AbstractTargetVariantProductModel model) {
        Assert.notNull(model, "AbstractTargetVariantProductModel must not be null");

        final ProductModel baseProduct = model.getBaseProduct();
        if (baseProduct instanceof AbstractTargetVariantProductModel) {
            // recursively call till we get to the ProductModel
            return ((AbstractTargetVariantProductModel)baseProduct).getProductType();
        }
        else if (baseProduct instanceof TargetProductModel) {
            return ((TargetProductModel)baseProduct).getProductType();
        }

        return null;
    }

    @Override
    public void set(final AbstractTargetVariantProductModel model, final ProductTypeModel value) {
        throw new UnsupportedOperationException();
    }

}
