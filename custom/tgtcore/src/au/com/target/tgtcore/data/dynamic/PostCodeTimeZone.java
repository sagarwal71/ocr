/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import au.com.target.tgtcore.model.PostCodeModel;


/**
 * @author rmcalave
 * 
 */
public class PostCodeTimeZone implements DynamicAttributeHandler<String, PostCodeModel> {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler#get(de.hybris.platform.servicelayer.model.AbstractItemModel)
     */
    @Override
    public String get(final PostCodeModel model) {
        if (model.getTimeZone() != null) {
            return model.getTimeZone().getID();
        }

        return null;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler#set(de.hybris.platform.servicelayer.model.AbstractItemModel, java.lang.Object)
     */
    @Override
    public void set(final PostCodeModel model, final String value) {
        throw new UnsupportedOperationException();
    }

}
