/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.springframework.util.Assert;

import au.com.target.tgtcore.model.ColourModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;


/**
 * Get the Name of the swatch from the colour object if it exists.
 */
public class SwatchName implements DynamicAttributeHandler<String, TargetColourVariantProductModel> {

    @Override
    public String get(final TargetColourVariantProductModel model) {
        Assert.notNull(model, "TargetColourVariantProductModel must not be null");

        final ColourModel colour = model.getSwatch();
        if (colour != null) {
            return colour.getName();
        }

        return null;
    }

    @Override
    public void set(final TargetColourVariantProductModel model, final String value) {
        throw new UnsupportedOperationException();
    }

}
