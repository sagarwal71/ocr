/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.springframework.util.Assert;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;


public class VariantPrimaryCategory implements
        DynamicAttributeHandler<TargetProductCategoryModel, AbstractTargetVariantProductModel> {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler#get(de.hybris.platform.servicelayer.model.AbstractItemModel)
     */
    @Override
    public TargetProductCategoryModel get(final AbstractTargetVariantProductModel model) {

        Assert.notNull(model, "AbstractTargetVariantProductModel must not be null");

        final ProductModel baseProduct = model.getBaseProduct();
        if (baseProduct instanceof AbstractTargetVariantProductModel) {
            // recursively call till we get to the ProductModel
            return ((AbstractTargetVariantProductModel)baseProduct).getPrimarySuperCategory();
        }
        else if (baseProduct instanceof TargetProductModel) {
            return ((TargetProductModel)baseProduct).getPrimarySuperCategory();
        }

        return null;

    }

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler#set(de.hybris.platform.servicelayer.model.AbstractItemModel, java.lang.Object)
     */
    @Override
    public void set(final AbstractTargetVariantProductModel model, final TargetProductCategoryModel value) {
        throw new UnsupportedOperationException("Primary Category can't be set");
    }

}
