/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.springframework.util.Assert;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * Get the showWehenOutOfStock flag from the base product of this variant.
 */
public class VariantShowWhenOutOfStock implements DynamicAttributeHandler<Boolean, AbstractTargetVariantProductModel> {

    @Override
    public Boolean get(final AbstractTargetVariantProductModel model) {
        Assert.notNull(model, "AbstractTargetVariantProductModel must not be null");

        final ProductModel baseProduct = model.getBaseProduct();
        Boolean retVal = null;
        if (baseProduct instanceof AbstractTargetVariantProductModel) {
            // recursively call till we get to the ProductModel
            retVal = ((AbstractTargetVariantProductModel)baseProduct).getShowWhenOutOfStock();
        }
        else if (baseProduct instanceof TargetProductModel) {
            retVal = ((TargetProductModel)baseProduct).getShowWhenOutOfStock();
        }
        if (retVal == null) {
            retVal = Boolean.FALSE;
        }

        return retVal;
    }

    @Override
    public void set(final AbstractTargetVariantProductModel model, final Boolean value) {
        throw new UnsupportedOperationException();
    }

}
