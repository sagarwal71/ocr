/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.springframework.util.Assert;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * @author rmcalave
 * 
 */
public class BulkyBoardVariantProduct implements DynamicAttributeHandler<Boolean, AbstractTargetVariantProductModel> {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler#get(de.hybris.platform.servicelayer.model.AbstractItemModel)
     */
    @Override
    public Boolean get(final AbstractTargetVariantProductModel model) {
        Assert.notNull(model, "AbstractTargetVariantProductModel must not be null");

        final ProductModel baseProduct = model.getBaseProduct();
        if (baseProduct instanceof AbstractTargetVariantProductModel) {
            // recursively call till we get to the ProductModel
            return ((AbstractTargetVariantProductModel)baseProduct).getBulkyBoardProduct();
        }
        else if (baseProduct instanceof TargetProductModel) {
            return ((TargetProductModel)baseProduct).getBulkyBoardProduct();
        }

        return null;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler#set(de.hybris.platform.servicelayer.model.AbstractItemModel, java.lang.Object)
     */
    @Override
    public void set(final AbstractTargetVariantProductModel model, final Boolean value) {
        throw new UnsupportedOperationException();
    }
}
