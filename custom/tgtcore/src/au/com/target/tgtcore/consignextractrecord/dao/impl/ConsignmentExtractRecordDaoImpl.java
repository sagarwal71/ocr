/**
 * 
 */
package au.com.target.tgtcore.consignextractrecord.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.consignextractrecord.dao.ConsignmentExtractRecordDao;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.model.ConsignmentExtractRecordModel;


/**
 * @author pratik
 *
 */
public class ConsignmentExtractRecordDaoImpl extends DefaultGenericDao<ConsignmentExtractRecordModel>
        implements ConsignmentExtractRecordDao {

    private static final Logger LOG = Logger.getLogger(ConsignmentExtractRecordDaoImpl.class);

    private ModelService modelService;

    public ConsignmentExtractRecordDaoImpl() {
        super(ConsignmentExtractRecordModel._TYPECODE);
    }

    @Override
    public ConsignmentExtractRecordModel getConsExtractRecordByConsignment(final TargetConsignmentModel consignment) {
        final List<ConsignmentExtractRecordModel> consExtractRecords = find(
                Collections.singletonMap(ConsignmentExtractRecordModel.CONSIGNMENT, consignment));
        return CollectionUtils.isNotEmpty(consExtractRecords) ? consExtractRecords.get(0) : null;
    }

    @Override
    public void saveConsignmentExtractRecord(final TargetConsignmentModel consignment, final boolean consignmentAsId) {
        if (getConsExtractRecordByConsignment(consignment) == null) {
            final ConsignmentExtractRecordModel consignmentExtractModel = modelService
                    .create(ConsignmentExtractRecordModel.class);
            consignmentExtractModel.setConsignment(consignment);
            consignmentExtractModel.setConsignmentAsId(Boolean.valueOf(consignmentAsId));
            modelService.save(consignmentExtractModel);
        }
        else {
            LOG.info("ConsignmentExtractRecord already exists for consignment, consignmentCode="
                    + consignment.getCode());
        }
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

}
