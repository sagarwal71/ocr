package au.com.target.tgtcore.storelocator.route.impl;

import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.storelocator.GPS;
import de.hybris.platform.storelocator.data.AddressData;
import de.hybris.platform.storelocator.route.impl.DefaultGeolocationDirectionsUrlBuilder;

import java.util.Map;

import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import au.com.target.tgtcore.storelocator.route.TargetGeolocationDirectionsUrlBuilder;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;


/**
 * Target extension to default geo-location url builder which adds {@code client ID} and {@code signature} parameters to
 * URL required by Google Maps API for Business.
 */
public class TargetGeolocationDirectionsUrlBuilderImpl extends DefaultGeolocationDirectionsUrlBuilder
        implements TargetGeolocationDirectionsUrlBuilder {

    private GoogleUrlSigner googleUrlSigner;


    @Override
    public String getWebServiceUrl(final String baseUrl, final GPS start, final GPS destination, final Map params) {
        ServicesUtil.validateParameterNotNull(baseUrl, "Base Url cannot be null");
        ServicesUtil.validateParameterNotNull(start, "START cannot be null");
        ServicesUtil.validateParameterNotNull(destination, "DESTINATION cannot be null");
        return addSignature(UriComponentsBuilder.fromHttpUrl(baseUrl)
                .path("/api/directions/")
                .path(getResponseType())
                .queryParam("origin", formatCoordinates(start))
                .queryParam("destination", formatCoordinates(destination))
                .queryParam("sensor", Boolean.valueOf(isSensor()))
                .queryParam("mode", getMode())
                .build().encode());
    }

    @Override
    public String getWebServiceUrl(final String baseUrl, final AddressData startAddress,
            final AddressData destinationAddress, final Map params) {
        ServicesUtil.validateParameterNotNull(baseUrl, "Base Url cannot be null");
        ServicesUtil.validateParameterNotNull(startAddress, "START cannot be null");
        ServicesUtil.validateParameterNotNull(destinationAddress, "DESTINATION cannot be null");
        return addSignature(UriComponentsBuilder.fromHttpUrl(baseUrl)
                .path("/api/directions/")
                .path(getResponseType())
                .queryParam("origin", addressData2String(startAddress))
                .queryParam("destination", addressData2String(destinationAddress))
                .queryParam("sensor", Boolean.valueOf(isSensor()))
                .queryParam("mode", getMode())
                .build().encode());
    }

    @Override
    public String getAddressWebServiceUrl(final String baseUrl, final AddressData address, final Map params) {
        ServicesUtil.validateParameterNotNull(baseUrl, "Base url cannot be null");
        ServicesUtil.validateParameterNotNull(address, "Address data cannot be null");
        return addSignature(UriComponentsBuilder.fromHttpUrl(baseUrl)
                .path(getResponseType())
                .queryParam("address", addressData2String(address))
                .queryParam("sensor", Boolean.valueOf(isSensor()))
                .build().encode());
    }

    @Override
    public String getDirectionUrl(final String baseUrl, final String start, final String destination) {
        ServicesUtil.validateParameterNotNull(baseUrl, "Base Url cannot be null");
        ServicesUtil.validateParameterNotNull(start, "START cannot be null");
        ServicesUtil.validateParameterNotNull(destination, "DESTINATION cannot be null");
        return UriComponentsBuilder.fromHttpUrl(baseUrl)
                .queryParam("saddr", formatString(start))
                .queryParam("daddr", formatString(destination))
                .build().toUriString();
    }

    /**
     * Add signature for business application.
     * 
     * @param uri
     *            the uri to add signature to
     * @return the string representing given {@code uri} with signature appended
     */
    protected String addSignature(final UriComponents uri) {
        return uri.getScheme() + "://" + uri.getHost()
                + getGoogleUrlSigner().sign(uri.getPath() + '?' + uri.getQuery());
    }

    /**
     * Converts given {@code addressData} into string according to Google API requirements.
     * 
     * @param addressData
     *            the address data object to be converted
     * @return string representation of address data
     */
    @Override
    protected String addressData2String(final AddressData addressData) {
        return Joiner.on(' ').skipNulls().join(
                Lists.newArrayList(
                        addressData.getBuilding(), addressData.getStreet(),
                        addressData.getCity(), addressData.getZip(),
                        addressData.getCountryCode()));
    }

    /**
     * Returns the Google URL signer.
     * 
     * @return the URL signer
     */
    public GoogleUrlSigner getGoogleUrlSigner() {
        return googleUrlSigner;
    }

    /**
     * Sets the Google URL signer.
     * 
     * @param googleUrlSigner
     *            the URL signer to use
     */
    public void setGoogleUrlSigner(final GoogleUrlSigner googleUrlSigner) {
        this.googleUrlSigner = googleUrlSigner;
    }

    private String formatString(final String start) {
        return start.replaceAll("\\s", "+");
    }
}
