/**
 * 
 */
package au.com.target.tgtcore.storelocator.pos.dao;

import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.PointOfServiceDao;

import java.util.List;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;


/**
 * DAO to retrieve TargetPointOfService
 * 
 */
public interface TargetPointOfServiceDao extends PointOfServiceDao {
    /**
     * Return a single TargetPointOfServiceModel identified by storeNumber.
     * 
     * @param storeNumber
     * @throws TargetUnknownIdentifierException
     *             if no result return
     * @throws TargetAmbiguousIdentifierException
     *             if more than one result return
     * @return TargetPointOfServiceModel
     */
    TargetPointOfServiceModel getPOSByStoreNumber(Integer storeNumber) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException;


    /**
     * Return a single RegionModel identified by region's abbreviation
     * 
     * @param abbreviation
     *            the abbreviation
     * 
     * @return RegionModel
     */
    RegionModel getRegionByAbbreviation(String abbreviation);

    /**
     * Gets the list of open stores.
     * 
     * @return the common stores
     */
    List<TargetPointOfServiceModel> getAllOpenPOS();

    /**
     * Gets the list of open stores which accepts CNC orders.
     * 
     * @return the all open cnc pos
     */
    List<TargetPointOfServiceModel> getAllOpenCncPOS();

    /**
     * @param baseStore
     * @return all POS of type Target and TargetCountry
     */
    List<TargetPointOfServiceModel> getAllPOSByTypes(final BaseStoreModel baseStore);

    /**
     * Gets the list of stores which are store fulfilment enabled.
     * 
     * @return the stores
     */
    List<TargetPointOfServiceModel> getAllFulfilmentEnabledPOS();


    /**
     * get the list of store that are enabled for fulfilment in store for a state
     * 
     * @param state
     * @return List of stores
     */
    List<TargetPointOfServiceModel> getAllFulfilmentEnabledPOSInState(String state);

    /**
     * get the quantity of products pending fulfilment at a point of service.
     * 
     * @param storeNumber
     * @param product
     * @return quantity
     */
    int getFulfilmentPendingQuantity(Integer storeNumber, ProductModel product);

    /**
     * get all stores for sending to fluent
     * 
     * @return {@link TargetPointOfServiceModel}
     */
    List<TargetPointOfServiceModel> getAllPosForFluent();


}
