/**
 * 
 */
package au.com.target.tgtcore.storelocator.data;

import de.hybris.platform.storelocator.data.MapLocationData;


/**
 * @author htan3
 *
 */
public class TargetMapLocationData extends MapLocationData {

    private String locality;

    private String postcode;

    private String state;

    /**
     * @return the locality
     */
    public String getLocality() {
        return locality;
    }

    /**
     * @param locality
     *            the locality to set
     */
    public void setLocality(final String locality) {
        this.locality = locality;
    }

    /**
     * @return the postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * @param postcode
     *            the postcode to set
     */
    public void setPostcode(final String postcode) {
        this.postcode = postcode;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(final String state) {
        this.state = state;
    }
}
