package au.com.target.tgtcore.storelocator;

import de.hybris.platform.storelocator.GPS;
import de.hybris.platform.storelocator.data.AddressData;
import de.hybris.platform.storelocator.data.MapLocationData;
import de.hybris.platform.storelocator.data.RouteData;
import de.hybris.platform.storelocator.exception.GeoLocatorException;
import de.hybris.platform.storelocator.exception.GeoServiceWrapperException;
import de.hybris.platform.storelocator.impl.DefaultGPS;
import de.hybris.platform.storelocator.impl.GoogleMapTools;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtcore.storelocator.data.TargetMapLocationData;
import au.com.target.tgtcore.storelocator.impl.TargetGPS;
import au.com.target.tgtcore.storelocator.route.TargetGeolocationDirectionsUrlBuilder;


/**
 * Target extension for {@link GoogleMapTools} that unifies approach with Google API URL builder.
 */
public class TargetGoogleMapTools extends GoogleMapTools {
    private static final Logger LOG = Logger.getLogger(TargetGoogleMapTools.class);
    private TargetGeolocationDirectionsUrlBuilder directionsUrlBuilder;
    private String baseUrl;
    private String directionBaseUrl;

    public String getDirectionUrl(final String source, final String destination) {
        return getDirectionsUrlBuilder().getDirectionUrl(directionBaseUrl, source, destination);
    }

    @Override
    public GPS geocodeAddress(final AddressData addressData) throws GeoServiceWrapperException {
        try {
            final String urlAddress = getDirectionsUrlBuilder().getAddressWebServiceUrl(
                    getBaseUrl(), addressData, null);
            final MapLocationData locationData = getResponse(urlAddress, getAddressLocationParser());
            final String latitude = locationData.getLatitude();
            final String longitude = locationData.getLongitude();
            if (StringUtils.isNotBlank(latitude) && StringUtils.isNotBlank(longitude)) {
                if (locationData instanceof TargetMapLocationData) {
                    final TargetMapLocationData targetLocationData = (TargetMapLocationData)locationData;
                    final TargetGPS targetGPS = new TargetGPS().create(Double.parseDouble(latitude),
                            Double.parseDouble(longitude));
                    targetGPS.setLocality(targetLocationData.getLocality());
                    targetGPS.setPostcode(targetLocationData.getPostcode());
                    targetGPS.setState(targetLocationData.getState());
                    return targetGPS;
                }
                else {
                    return new DefaultGPS().create(Double.parseDouble(latitude),
                            Double.parseDouble(longitude));
                }
            }
            throw new GeoServiceWrapperException(GeoServiceWrapperException.errorMessages.get(locationData.getCode()));

        }
        catch (final GeoLocatorException e) {
            throw new GeoServiceWrapperException(e);
        }
    }

    @Override
    protected RouteData getRouteData(final String urlAddress) {
        return getResponse(urlAddress, getRouteDataParser());
    }

    /**
     * Shamelessly copied from {@link GoogleMapTools#getRouteData(String)} to preserve platform logic. The only addition
     * here is to use
     * {@link RestTemplate#execute(java.net.URI, org.springframework.http.HttpMethod, org.springframework.web.client.RequestCallback, org.springframework.web.client.ResponseExtractor)}
     * with {@link URI} parameter instead of
     * {@link RestTemplate#execute(String, org.springframework.http.HttpMethod, org.springframework.web.client.RequestCallback, org.springframework.web.client.ResponseExtractor, java.util.Map)}
     * as when Spring attempts to substitute url parameters it may trim trailing {@code =} sign from Google generated
     * signature, that will obviously lead to HTTP 403 Forbidden.
     * 
     * Other solution here is to URL encode Google signature (not tested).
     * 
     * @param uri
     *            the uri to request
     * @param responseExtractor
     *            the response entity extractor
     * @param <T>
     *            the type of extracted entity
     * @return the instance of extracted entity
     * @throws GeoServiceWrapperException
     *             if HTTP error occurs
     */
    protected <T> T getResponse(final String uri, final ResponseExtractor<T> responseExtractor)
            throws GeoServiceWrapperException {
        final RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
            @Override
            public void handleError(final ClientHttpResponse response)
                    throws IOException {
                if (response.getStatusCode() != HttpStatus.OK) {
                    throw new GeoServiceWrapperException(
                            "Google maps unavailable. Status: " + response.getRawStatusCode());
                }
                else {
                    super.handleError(response);
                }
            }
        });
        try {
            LOG.info("INFO-GOOGLEAPI-LOOKUP : looking up google maps with uri=" + uri);
            return restTemplate.execute(new URI(uri), HttpMethod.GET, null, responseExtractor);
        }
        catch (final URISyntaxException e) {
            throw new GeoServiceWrapperException(e);
        }
    }

    /**
     * Returns the Google API URL builder.
     * 
     * @return the Google API URL builder
     */
    @Override
    public TargetGeolocationDirectionsUrlBuilder getDirectionsUrlBuilder() {
        return directionsUrlBuilder;
    }

    /**
     * Sets the Google API URL builder to use.
     * 
     * @param directionsUrlBuilder
     *            the Google API URL builder to use
     */
    public void setDirectionsUrlBuilder(final TargetGeolocationDirectionsUrlBuilder directionsUrlBuilder) {
        super.setDirectionsUrlBuilder(directionsUrlBuilder);
        this.directionsUrlBuilder = directionsUrlBuilder;
    }


    /**
     * Returns the Google API base URL.
     * 
     * @return the Google API base URL
     */
    @Override
    public String getBaseUrl() {
        return baseUrl;
    }

    /**
     * Sets the Google API base URL to use.
     * 
     * @param baseUrl
     *            the Google API base URL to use
     */
    @Override
    public void setBaseUrl(final String baseUrl) {
        super.setBaseUrl(baseUrl);
        this.baseUrl = baseUrl;
    }

    /**
     * @param directionBaseUrl
     *            the directionBaseUrl to set
     */
    @Required
    public void setDirectionBaseUrl(final String directionBaseUrl) {
        this.directionBaseUrl = directionBaseUrl;
    }
}
