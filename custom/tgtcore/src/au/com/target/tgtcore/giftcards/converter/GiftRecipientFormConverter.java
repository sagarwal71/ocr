/**
 * 
 */
package au.com.target.tgtcore.giftcards.converter;

import de.hybris.platform.servicelayer.model.ModelService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.converters.TargetConverter;
import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;
import au.com.target.tgtcore.model.GiftRecipientModel;


/**
 * Converts GiftRecipientDTO to a model.
 * 
 * @author jjayawa1
 *
 */
public class GiftRecipientFormConverter implements TargetConverter<GiftRecipientDTO, GiftRecipientModel>,
        GiftRecipientFormUpdater {

    private ModelService modelService;

    @Override
    public GiftRecipientModel convert(final GiftRecipientDTO giftRecipient) {
        final GiftRecipientModel giftRecipientModel = modelService.create(GiftRecipientModel.class);

        updateModelFromDto(giftRecipientModel, giftRecipient);
        modelService.save(giftRecipientModel);

        return giftRecipientModel;
    }


    @Override
    public void update(final GiftRecipientModel recipientModel, final GiftRecipientDTO recipientDto) {

        updateModelFromDto(recipientModel, recipientDto);
        modelService.save(recipientModel);
    }

    private void updateModelFromDto(final GiftRecipientModel giftRecipientModel, final GiftRecipientDTO giftRecipient) {

        if (StringUtils.isNotEmpty(giftRecipient.getRecipientEmailAddress())) {
            giftRecipientModel.setEmail(giftRecipient.getRecipientEmailAddress());
        }

        if (StringUtils.isNotEmpty(giftRecipient.getFirstName())) {
            giftRecipientModel.setFirstName(giftRecipient.getFirstName());
        }

        if (StringUtils.isNotEmpty(giftRecipient.getLastName())) {
            giftRecipientModel.setLastName(giftRecipient.getLastName());
        }

        if (StringUtils.isNotEmpty(giftRecipient.getMessageText())) {
            giftRecipientModel.setMessageText(giftRecipient.getMessageText());
        }

    }


    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }


}
