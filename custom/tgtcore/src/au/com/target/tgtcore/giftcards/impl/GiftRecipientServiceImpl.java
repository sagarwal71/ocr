/**
 * 
 */
package au.com.target.tgtcore.giftcards.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.giftcards.GiftRecipientService;
import au.com.target.tgtcore.giftcards.converter.GiftRecipientFormConverter;
import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;
import au.com.target.tgtcore.model.GiftRecipientModel;


/**
 * Service implementation for Gift Card operations.
 * 
 * @author jjayawa1
 *
 */
public class GiftRecipientServiceImpl implements GiftRecipientService {

    private ModelService modelService;
    private GiftRecipientFormConverter giftRecipientFormConverter;


    @Override
    public void addGiftRecipientToOrderEntry(final GiftRecipientDTO recipient,
            final AbstractOrderEntryModel orderEntry) {
        Assert.notNull(recipient, "GiftRecipient cannot be null");
        Assert.notNull(orderEntry, "OrderEntry cannot be null");

        final GiftRecipientModel giftRecipient = giftRecipientFormConverter.convert(recipient);
        giftRecipient.setOrderEntry(orderEntry);
        modelService.save(giftRecipient);
        modelService.refresh(giftRecipient);
        modelService.refresh(orderEntry);
    }

    /**
     * @return the modelService
     */
    public ModelService getModelService() {
        return modelService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param giftRecipientFormConverter
     *            the giftRecipientFormConverter to set
     */
    @Required
    public void setGiftRecipientFormConverter(final GiftRecipientFormConverter giftRecipientFormConverter) {
        this.giftRecipientFormConverter = giftRecipientFormConverter;
    }
}
