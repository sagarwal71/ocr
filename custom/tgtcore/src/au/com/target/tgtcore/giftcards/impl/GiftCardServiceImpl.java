/**
 * 
 */
package au.com.target.tgtcore.giftcards.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.giftcards.GiftCardService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.GiftCardModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.dao.GiftCardDao;
import au.com.target.tgtcore.product.exception.ProductNotFoundException;
import au.com.target.tgtcore.util.ProductUtil;
import au.com.target.tgtutility.util.TargetProductUtils;


/**
 * @author smishra1
 *
 */
public class GiftCardServiceImpl implements GiftCardService {

    private static final Logger LOG = Logger.getLogger(GiftCardServiceImpl.class);

    private GiftCardDao giftCardDao;
    private ProductService productService;

    /**
     * {@inheritDoc}
     */
    @Override
    public GiftCardModel getGiftCard(final String brandId) {
        ServicesUtil.validateParameterNotNullStandardMessage("Brand Id ", brandId);

        GiftCardModel giftCard = null;
        try {
            giftCard = giftCardDao.getGiftCardByBrandId(brandId);
        }
        catch (final TargetUnknownIdentifierException e) {
            LOG.warn("Error while getting gift card details for " + brandId, e);
        }
        catch (final TargetAmbiguousIdentifierException e) {
            LOG.warn("More than one giftCard found for the brandId : " + brandId, e);
        }
        return giftCard;
    }

    /**
     * {@inheritDoc}
     * 
     * @throws ProductNotFoundException
     */
    @Override
    public GiftCardModel getGiftCardForProduct(final String productCode) throws ProductNotFoundException {
        ServicesUtil.validateParameterNotNull(productCode, "Parameter productCode  must not be null");
        GiftCardModel giftCard = null;
        try {
            final ProductModel product = productService
                    .getProductForCode(productCode);
            giftCard = getGiftCardForProduct(product);
        }
        catch (final UnknownIdentifierException | AmbiguousIdentifierException ex) {
            throw new ProductNotFoundException("Error getting product for code: " + productCode, ex);
        }
        return giftCard;
    }

    /**
     * @param product
     * @return giftcard model if it exists and null otherwise
     */
    private GiftCardModel getGiftCardForProduct(final ProductModel product) {
        GiftCardModel giftCard = null;
        ProductModel baseProduct = product;
        if (product instanceof AbstractTargetVariantProductModel) {
            baseProduct = TargetProductUtils
                    .getBaseTargetProduct((AbstractTargetVariantProductModel)product);
        }
        if (baseProduct != null && baseProduct instanceof TargetProductModel) {
            giftCard = ((TargetProductModel)baseProduct).getGiftCard();
        }
        return giftCard;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.giftcards.GiftCardService#isProductAGiftCard(de.hybris.platform.core.model.product.ProductModel)
     */
    @Override
    public boolean isProductAGiftCard(final ProductModel product) {
        return getGiftCardForProduct(product) != null ? true : false;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.giftcards.GiftCardService#doesCartHaveAGiftCard(de.hybris.platform.core.model.order.CartModel)
     */
    @Override
    public boolean doesCartHaveAGiftCard(final CartModel cart) {
        if (cart == null) {
            return false;
        }
        final List<AbstractOrderEntryModel> cartEntries = cart.getEntries();
        if (CollectionUtils.isNotEmpty(cartEntries)) {
            for (final AbstractOrderEntryModel cartEntry : cartEntries) {
                if (isProductAGiftCard(cartEntry.getProduct())) {
                    return true;
                }
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.giftcards.GiftCardService#doesCartHaveGiftCardsOnly(de.hybris.platform.core.model.order.CartModel)
     */
    @Override
    public boolean doesCartHaveGiftCardsOnly(final CartModel cart) {
        final List<AbstractOrderEntryModel> cartEntries = cart.getEntries();
        if (CollectionUtils.isNotEmpty(cartEntries)) {
            for (final AbstractOrderEntryModel cartEntry : cartEntries) {
                if (!isProductADigitalGiftCard(cartEntry.getProduct())) {
                    return false;
                }
            }
        }
        else {
            // empty cart
            return false;
        }
        return true;
    }


    @Override
    public boolean isProductADigitalGiftCard(final ProductModel product) {
        final GiftCardModel giftCardForProduct = getGiftCardForProduct(product);

        return giftCardForProduct != null && ProductUtil.isProductTypeDigital(product);
    }

    /**
     * @param giftCardDao
     */
    @Required
    public void setGiftCardDao(final GiftCardDao giftCardDao) {
        this.giftCardDao = giftCardDao;
    }

    /**
     * @param productService
     *            the productService to set
     */
    @Required
    public void setProductService(final ProductService productService) {
        this.productService = productService;
    }

}
