/**
 *
 */
package au.com.target.tgtcore.storefinder.impl;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.commerceservices.storefinder.impl.DefaultStoreFinderService;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.GPS;
import de.hybris.platform.storelocator.data.AddressData;
import de.hybris.platform.storelocator.exception.GeoServiceWrapperException;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.storefinder.TargetStoreFinderService;
import au.com.target.tgtcore.storelocator.impl.TargetGPS;
import au.com.target.tgtcore.storelocator.pos.dao.TargetPointOfServiceDao;


/**
 * @author Pradeep
 *
 */
@SuppressWarnings("deprecation")
public class TargetStoreFinderServiceImpl<ITEM extends PointOfServiceDistanceData> extends
        DefaultStoreFinderService implements TargetStoreFinderService {
    private static final Logger LOG = Logger.getLogger(TargetStoreFinderServiceImpl.class);

    private TargetPointOfServiceDao targetPointOfServiceDao;

    @Override
    protected AddressData generateGeoAddressForSearchQuery(
            final BaseStoreModel baseStore, final String locationText) {
        String country = null;
        if (StringUtils.isNotBlank(locationText) && !locationText.contains(",")) {
            final Collection<CountryModel> deliveryCountries = baseStore.getDeliveryCountries();

            if (CollectionUtils.isNotEmpty(deliveryCountries)) {
                country = deliveryCountries.iterator().next().getName();
            }
        }

        final AddressData addressData = new AddressData();
        addressData.setCity(locationText);
        addressData.setCountryCode(country);

        if (LOG.isDebugEnabled()) {
            LOG.debug("Generated Geo Address Data: City[" + locationText
                    + "] - Country[" + country + "]");
        }

        return addressData;
    }


    @Override
    protected StoreFinderSearchPageData<ITEM> doSearch(final BaseStoreModel baseStore, final String locationText,
            final GeoPoint centerPoint, final PageableData pageableData, final Double maxRadiusKm)
    {
        final Collection posResults;

        if (maxRadiusKm != null)
        {
            posResults = getPointsOfServiceNear(centerPoint, maxRadiusKm.doubleValue(), baseStore);
        }
        else
        {
            posResults = targetPointOfServiceDao.getAllPOSByTypes(baseStore);
        }
        return doSearch(locationText, centerPoint, pageableData, posResults);
    }

    @Override
    public StoreFinderSearchPageData<ITEM> doSearchNearestClickAndCollectLocation(final String locationText,
            final GeoPoint centerPoint, final PageableData pageableData) {
        final Collection posResults = targetPointOfServiceDao.getAllOpenCncPOS();
        return doSearch(locationText, centerPoint, pageableData, posResults);
    }

    @Override
    public StoreFinderSearchPageData<ITEM> doSearch(final String locationText, final GeoPoint centerPoint,
            final PageableData pageableData,
            final Collection pointOfServiceModelList) {

        final int resultRangeStart = pageableData.getCurrentPage() * pageableData.getPageSize();
        final int resultRangeEnd = (pageableData.getCurrentPage() + 1) * pageableData.getPageSize();
        if (CollectionUtils.isNotEmpty(pointOfServiceModelList))
        {
            final List orderedResults = calculateDistances(centerPoint, pointOfServiceModelList);
            final PaginationData paginationData = createPagination(pageableData, pointOfServiceModelList.size());
            final List orderedResultsWindow = orderedResults.subList(
                    Math.min(orderedResults.size(), resultRangeStart),
                    Math.min(orderedResults.size(), resultRangeEnd));
            return createSearchResult(locationText, centerPoint, orderedResultsWindow, paginationData);
        }

        return createSearchResult(locationText, centerPoint, Collections.emptyList(),
                createPagination(pageableData, 0L));
    }


    @Override
    public StoreFinderSearchPageData<ITEM> doSearchByLocation(final BaseStoreModel baseStore,
            final String locationText,
            final PageableData pageableData,
            final Collection pointOfServiceModelList) {
        final GeoPoint geoPoint = new GeoPoint();
        if (StringUtils.isNotEmpty(locationText)) {
            try {
                final GPS resolvedPoint = getGeoWebServiceWrapper()
                        .geocodeAddress(generateGeoAddressForSearchQuery(baseStore, locationText));
                geoPoint.setLatitude(resolvedPoint.getDecimalLatitude());
                geoPoint.setLongitude(resolvedPoint.getDecimalLongitude());
                StoreFinderSearchPageData<ITEM> storeFinderSearchPageData = null;
                if (pointOfServiceModelList == null) {
                    storeFinderSearchPageData = doSearch(baseStore, locationText,
                            geoPoint,
                            pageableData, baseStore.getMaxRadiusForPoSSearch());
                }
                else {
                    storeFinderSearchPageData = doSearch(locationText, geoPoint, pageableData, pointOfServiceModelList);
                }
                if (resolvedPoint instanceof TargetGPS) {
                    final TargetGPS gps = (TargetGPS)resolvedPoint;
                    storeFinderSearchPageData.setLocationText(getLocationText(gps.getLocality(), gps.getPostcode(),
                            gps.getState(), locationText));
                }
                return storeFinderSearchPageData;
            }
            catch (final GeoServiceWrapperException localGeoServiceWrapperException) {
                LOG.info("Failed to resolve location for [" + locationText
                        + "]");
            }
        }
        return createSearchResult(locationText, geoPoint, Collections.emptyList(), createPaginationData());
    }

    @Override
    public StoreFinderSearchPageData<ITEM> doSearchClickAndCollectStoresByLocation(final BaseStoreModel baseStore,
            final String locationText,
            final PageableData pageableData) {
        final GeoPoint geoPoint = new GeoPoint();
        if (StringUtils.isNotEmpty(locationText)) {
            try {
                final GPS resolvedPoint = getGeoWebServiceWrapper()
                        .geocodeAddress(generateGeoAddressForSearchQuery(baseStore, locationText));
                if (resolvedPoint != null) {
                geoPoint.setLatitude(resolvedPoint.getDecimalLatitude());
                geoPoint.setLongitude(resolvedPoint.getDecimalLongitude());
                }
                final StoreFinderSearchPageData<ITEM> storeFinderSearchPageData = doSearchNearestClickAndCollectLocation(
                        locationText,
                        geoPoint, pageableData);
                if (resolvedPoint != null && resolvedPoint instanceof TargetGPS) {
                    final TargetGPS gps = (TargetGPS)resolvedPoint;
                    storeFinderSearchPageData.setLocationText(getLocationText(gps.getLocality(), gps.getPostcode(),
                            gps.getState(), locationText));
                }
                return storeFinderSearchPageData;
            }
            catch (final GeoServiceWrapperException localGeoServiceWrapperException) {
                LOG.info("Failed to resolve location for [" + locationText
                        + "]");
            }
        }
        return createSearchResult(locationText, geoPoint, Collections.emptyList(), createPaginationData());
    }

    @Override
    public StoreFinderSearchPageData<ITEM> doSearchByLocation(
            final BaseStoreModel baseStore, final String locationText,
            final PageableData pageableData) {
        return doSearchByLocation(baseStore, locationText, pageableData, null);
    }

    /**
     * 
     * @param locality
     * @param postcode
     * @param state
     * @param locationText
     * @return string as location text to be returned
     */
    private String getLocationText(final String locality, final String postcode, final String state,
            final String locationText) {
        final StringBuilder locationTextSB = new StringBuilder();
        if (StringUtils.isNotEmpty(locality)) {
            locationTextSB.append(locality);
        }
        if (StringUtils.isNotEmpty(postcode)) {
            if (!StringUtils.isEmpty(locationTextSB.toString())) {
                locationTextSB.append(" ");
            }
            locationTextSB.append(postcode);
        }
        if (StringUtils.isNotEmpty(state)) {
            if (!StringUtils.isEmpty(locationTextSB.toString())) {
                locationTextSB.append(", ");
            }
            locationTextSB.append(state);
        }
        if (StringUtils.isEmpty(locationTextSB.toString())) {
            return locationText;
        }
        return locationTextSB.toString();
    }


    /**
     * @param targetPointOfServiceDao
     *            the pointOfServiceDao to set
     */
    @Required
    public void setTargetPointOfServiceDao(final TargetPointOfServiceDao targetPointOfServiceDao) {
        this.targetPointOfServiceDao = targetPointOfServiceDao;
    }



}
