package au.com.target.tgtcore.category.daos.impl;

import de.hybris.platform.category.daos.impl.DefaultCategoryDao;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.GenericQuery;
import de.hybris.platform.genericsearch.GenericSearchService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;
import java.util.Collections;

import au.com.target.tgtcore.category.daos.TargetCategoryDao;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * Default implementation for {@link TargetCategoryDao}.
 */
public class TargetCategoryDaoImpl extends DefaultCategoryDao implements TargetCategoryDao {

    private GenericSearchService searchService;

    @Override
    public Collection<CategoryModel> findLeafCategoriesWithProducts() {
        final StringBuilder query = new StringBuilder();
        query.append("select distinct {p." + TargetProductModel.PRIMARYSUPERCATEGORY + "}");
        query.append(" from {" + TargetProductModel._TYPECODE + " as p join " + VariantProductModel._TYPECODE + " as v");
        query.append(" on {v." + VariantProductModel.BASEPRODUCT + "} = {p." + TargetProductModel.PK + "}");
        query.append(" and {p." + TargetProductModel.PRIMARYSUPERCATEGORY + "} IS NOT NULL }");

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.setResultClassList(Collections.singletonList(CategoryModel.class));

        final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);

        return searchResult.getResult();
    }

    @Override
    public Collection<CategoryModel> findAllCategories() {
        final GenericQuery query = new GenericQuery(TargetProductCategoryModel._TYPECODE);
        return searchService.<CategoryModel> search(query).getResult();
    }

    /**
     * Returns the service used internally to perform search.
     * 
     * @return the search service
     */
    public GenericSearchService getSearchService() {
        return searchService;
    }

    /**
     * Sets the search service.
     * 
     * @param searchService
     *            the service to set
     */
    public void setSearchService(final GenericSearchService searchService) {
        this.searchService = searchService;
    }
}
