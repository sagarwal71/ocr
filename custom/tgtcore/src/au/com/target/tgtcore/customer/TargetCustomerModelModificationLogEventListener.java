/**
 * 
 */
package au.com.target.tgtcore.customer;

import de.hybris.platform.jalo.JaloConnection;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;


/**
 * @author bpottass
 *
 */
public class TargetCustomerModelModificationLogEventListener
        extends AbstractEventListener<TargetCustomerModelModificationLogEvent> {


    @Override
    public void onEvent(final TargetCustomerModelModificationLogEvent event) {
        JaloSession.getCurrentSession().setUser(event.getUser());
        JaloConnection.getInstance().logItemModification(event.getItemPK(), event.getNewValueMap(),
                event.getPreviousValueMap(), true);

    }

}

