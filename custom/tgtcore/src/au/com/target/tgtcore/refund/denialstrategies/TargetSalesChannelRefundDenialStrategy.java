/**
 * 
 */
package au.com.target.tgtcore.refund.denialstrategies;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.strategy.ReturnableCheck;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;


/**
 * @author ayushman
 *
 */
public class TargetSalesChannelRefundDenialStrategy implements ReturnableCheck {

    private static final Logger LOG = Logger.getLogger(TargetSalesChannelRefundDenialStrategy.class);

    private TargetSalesApplicationConfigService salesApplicationConfigService;

    /**
     * Strategy to deny refund if order's sales application is configured to deny refunds.
     *
     * @param order
     *            the order
     * @param entry
     *            the entry
     * @param returnQuantity
     *            the return quantity
     * @return true, if not denied
     */
    @Override
    public boolean perform(final OrderModel order, final AbstractOrderEntryModel entry, final long returnQuantity) {

        Assert.notNull(order, "Order can't be null.");

        final SalesApplication salesApp = order.getSalesApplication();

        if (salesApplicationConfigService.isRefundDenied(salesApp)) {
            LOG.info("Refund is denied by the salesApplication=" + salesApp.toString() + ", order="
                    + order.getCode());
            return false;
        }

        return true;
    }

    /**
     * @param salesApplicationConfigService
     *            the salesApplicationConfigService to set
     */
    @Required
    public void setSalesApplicationConfigService(
            final TargetSalesApplicationConfigService salesApplicationConfigService) {
        this.salesApplicationConfigService = salesApplicationConfigService;
    }

}
