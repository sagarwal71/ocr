/**
 * 
 */
package au.com.target.tgtcore.refund.denialstrategies;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.strategy.ReturnableCheck;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.voucher.service.TargetVoucherService;


public class FlybuysDiscRefundDenialStrategy implements ReturnableCheck {
    private TargetVoucherService targetVoucherService;

    /* (non-Javadoc)
     * @see de.hybris.platform.returns.strategy.ReturnableCheck#perform(de.hybris.platform.core.model.order.OrderModel, de.hybris.platform.core.model.order.AbstractOrderEntryModel, long)
     */
    @Override
    public boolean perform(final OrderModel order, final AbstractOrderEntryModel entry, final long returnQuantity) {
        return null == targetVoucherService.getFlybuysDiscountForOrder(order);
    }

    /**
     * @param targetVoucherService
     *            the targetVoucherService to set
     */
    @Required
    public void setTargetVoucherService(final TargetVoucherService targetVoucherService) {
        this.targetVoucherService = targetVoucherService;
    }
}
