package de.hybris.platform.promotions.jalo;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.promotions.util.Helper;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import au.com.target.tgtcore.deals.wrappers.DealWrapper;
import au.com.target.tgtcore.deals.wrappers.ProductWrapper;
import au.com.target.tgtcore.enums.DealItemTypeEnum;


//*************************************************************************
// Spend and get
// Example of this type of deal:
//          spend $100.00 in manchester and get %5.00 off sheets
//          spend $50.00 on a DVD player and save $5.00 off 5 DVDs
//          spend $20.00 on shoes and get a pair of socks for $1.00

public class SpendAndGetDeal extends GeneratedSpendAndGetDeal {
    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }

    @Override
    protected List<PromotionResult> applyDeal(final DealWrapper wrappedDeal, final List<ProductWrapper> wrappedProducts) {
        long itemAmt;
        final long rewardAmt = Math.round(getRewardValueAsPrimitive() * 100);
        int numberofRewardsSofar = 0;

        // make sorted list of reward items
        final ProductWrapper[] rewardList = getArrayOfSortedRewardProducts(wrappedProducts, wrappedDeal);
        int get = getRewardMaxQtyAsPrimitive();
        if (rewardList.length == 0) {
            return buildCouldHaveFiredResultListForAllQualifiersButNoRewards(wrappedProducts, wrappedDeal, 1.0F, get);
        }

        // we know we have a deal, so lets apply the savings

        if (get == 0) {
            // setting get to the number of items means that all items
            // will be given as a reward on the first deal instance.
            get = rewardList.length;
        }

        int given = 0;
        final int instCnt = 1;
        for (int cnt = rewardList.length - 1; cnt >= 0 && given < get; cnt--) {
            final ProductWrapper thisItem = rewardList[cnt];
            itemAmt = rewardList[cnt].getUnitSellPrice();
            switch (wrappedDeal.getDealType()) {
                case DOLLAR_OFF_EACH:
                    if (itemAmt > rewardAmt) {
                        thisItem.setDealApplied(instCnt, DealItemTypeEnum.REWARD, rewardAmt);
                    }
                    else {
                        thisItem.setDealApplied(instCnt, DealItemTypeEnum.REWARD, itemAmt);
                    }
                    break;
                case FIXED_DOLLAR_EACH:
                    if (itemAmt > rewardAmt) {
                        thisItem.setDealApplied(instCnt, DealItemTypeEnum.REWARD, itemAmt - rewardAmt);
                    }
                    else {
                        thisItem.setDealApplied(instCnt, DealItemTypeEnum.REWARD, 0);
                    }
                    break;
                case PERCENT_OFF_EACH:
                    final long tmp = DealHelper.calcDiscTruncated(itemAmt, (int)rewardAmt);
                    thisItem.setDealApplied(1, DealItemTypeEnum.REWARD, tmp);
                    break;
                default:
                    // this space intentionally left blank
                    break;
            }
            given++;
            if (given > 0) {
                numberofRewardsSofar = given;
            }

        }


        // mark all the qualifying items (if they are not a reward)
        for (final ProductWrapper thisItem : wrappedProducts) {
            if ((thisItem.getQualifierModel() != null) && (!thisItem.isInDeal())) {
                thisItem.setDealApplied(1, DealItemTypeEnum.QUALIFIER, 0);
            }
        }

        // this can only fire once per transaction, so no need for could have fired messages
        return buildFiredResultListWithRewards(wrappedProducts, wrappedDeal, instCnt, numberofRewardsSofar, get);
    }

    @SuppressWarnings("deprecation")
    @Override
    public String getResultDescription(final SessionContext ctx, final PromotionResult promotionResult,
            final Locale locale) {
        final AbstractOrder order = promotionResult.getOrder(ctx);
        if (order != null) {
            final Currency orderCurrency = order.getCurrency(ctx);
            final List<DealQualifier> qualList = getQualifierList();
            final int maxRewardQty = getRewardMaxQtyAsPrimitive(ctx);
            if (promotionResult.getFired(ctx)) {

                if ((promotionResult instanceof TargetDealWithRewardResult)
                        && ((TargetDealWithRewardResult)promotionResult).getCouldhaveMoreRewards(ctx)) {
                    final int numberOfRewardsSofar = ((TargetDealWithRewardResult)promotionResult)
                            .getNumberofRewardsSofarAsPrimitive();
                    final Object[] args = { Integer.valueOf(numberOfRewardsSofar),
                            Integer.valueOf(maxRewardQty - numberOfRewardsSofar) };
                    return formatMessage(this.getMessageCouldHaveMoreRewards(ctx), args, locale);
                }

                else {
                    final double totalDiscount = promotionResult.getTotalDiscount(ctx);
                    final int rewardQty = getRewardMaxQtyAsPrimitive(ctx);
                    int rewardCnt = 0;
                    final Collection<PromotionOrderEntryConsumed> entries = promotionResult.getConsumedEntries(ctx);
                    for (final PromotionOrderEntryConsumed entry : entries) {
                        final TargetPromotionOrderEntryConsumed targetEntry = (TargetPromotionOrderEntryConsumed)entry;
                        if (au.com.target.tgtcore.constants.GeneratedTgtCoreConstants.Enumerations.DealItemTypeEnum.REWARD
                                .equals(targetEntry.getDealItemType(ctx).getCode())) {
                            rewardCnt++;
                        }
                    }

                    final Object[] args = {
                            Double.valueOf(totalDiscount),
                            Helper.formatCurrencyAmount(ctx, locale, orderCurrency, totalDiscount),
                            Integer.valueOf(rewardQty),
                            Integer.valueOf(rewardCnt),
                            Integer.valueOf(rewardQty - rewardCnt)
                    };
                    return formatMessage(this.getMessageFired(ctx), args, locale);
                }
            }
            else if (promotionResult.getCouldFire(ctx)) {

                if (promotionResult instanceof TargetDealWithRewardResult) {
                    if (((TargetDealWithRewardResult)promotionResult).getCouldhaveMoreRewards(ctx)) {
                        final int numberOfRewardsSofar = ((TargetDealWithRewardResult)promotionResult)
                                .getNumberofRewardsSofarAsPrimitive();
                        final Object[] args = { Integer.valueOf(numberOfRewardsSofar),
                                Integer.valueOf(maxRewardQty - numberOfRewardsSofar) };
                        return formatMessage(this.getMessageCouldHaveMoreRewards(ctx), args, locale);
                    }
                }
                double qualAmt = 0;
                for (final DealQualifier dealQualifier : qualList) {
                    qualAmt += dealQualifier.getMinAmtAsPrimitive();
                }

                final double foundAmt = qualAmt * promotionResult.getCertaintyAsPrimitive();
                final double shortAmt = qualAmt - foundAmt;


                final Object[] args = {
                        Double.valueOf(qualAmt),
                        Helper.formatCurrencyAmount(ctx, locale, orderCurrency, qualAmt),
                        Double.valueOf(foundAmt),
                        Helper.formatCurrencyAmount(ctx, locale, orderCurrency, foundAmt),
                        Double.valueOf(shortAmt),
                        Helper.formatCurrencyAmount(ctx, locale, orderCurrency, shortAmt)
                };
                return formatMessage(this.getMessageCouldHaveFired(ctx), args, locale);
            }
        }
        return "";

    }
}
