package de.hybris.platform.promotions.jalo;

import de.hybris.platform.core.Registry;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.enumeration.EnumerationManager;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.promotions.result.PromotionEvaluationContext;
import de.hybris.platform.promotions.result.PromotionOrderEntry;
import de.hybris.platform.promotions.result.PromotionOrderView;
import de.hybris.platform.promotions.util.Helper;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.deals.wrappers.DealWrapper;
import au.com.target.tgtcore.deals.wrappers.ProductWrapper;
import au.com.target.tgtcore.deals.wrappers.impl.DealWrapperImpl;
import au.com.target.tgtcore.deals.wrappers.impl.ProductWrapperImpl;
import au.com.target.tgtcore.enums.DealRewardConsumeOrderEnum;
import au.com.target.tgtcore.jalo.TgtCoreManager;


@SuppressWarnings("deprecation")
public abstract class AbstractDeal extends GeneratedAbstractDeal {

    private static final int AUD_DIGITS = 2;
    private ConfigurationService configurationService;
    private CommonI18NService commonI18NService;


    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }

    @Override
    public List<PromotionResult> evaluate(final SessionContext ctx, final PromotionEvaluationContext promoContext) {
        // Find the eligible products, and apply any restrictions
        final PromotionsManager.RestrictionSetResult restrictResult =
                findEligibleProductsInBasket(ctx, promoContext);

        List<PromotionResult> results = null;
        // If the restrictions did not reject this promotion, and there are still products allowed after the restrictions
        if (restrictResult.isAllowedToContinue() && !restrictResult.getAllowedProducts().isEmpty()) {
            // Create a view of the order containing only the allowed products
            final PromotionOrderView orderView =
                    promoContext.createView(ctx, this, restrictResult.getAllowedProducts());

            final List<PromotionOrderEntry> entries = orderView.getAllEntries(ctx);
            if (!CollectionUtils.isEmpty(entries)) {
                final DealWrapper wrappedDeal = new DealWrapperImpl(ctx, this, promoContext, orderView);
                final List<ProductWrapper> wrappedProducts = new ArrayList<ProductWrapper>(entries.size());
                for (final PromotionOrderEntry entry : entries) {
                    final long qty = entry.getQuantity(ctx);
                    for (long i = 0; i < qty; i++) {
                        wrappedProducts.add(new ProductWrapperImpl(entry, wrappedDeal));
                    }
                }

                results = preApplyCheck(wrappedDeal, wrappedProducts);

                if (results == null) {
                    results = applyDeal(wrappedDeal, wrappedProducts);
                }
            }
        }
        if (results == null) {
            results = new ArrayList<>();
        }
        return results;
    }

    /**
     * Opportunity to do some checks before we get in to applying the deal.
     * 
     * @param wrappedDeal
     *            the deal details wrapped in our class for convenience
     * @param wrappedProducts
     *            the product details wrapped in our class for convenience
     * @return results showing partial deal not being meet, null means need to go further and call applyDeal
     */
    protected abstract List<PromotionResult> preApplyCheck(final DealWrapper wrappedDeal,
            final List<ProductWrapper> wrappedProducts);

    /**
     * Do the work of applying this deal to this order.
     * 
     * @param wrappedDeal
     *            the deal details wrapped in our class for convenience
     * @param wrappedProducts
     *            the product details wrapped in our class for convenience
     * @return the list of promotion results that this promotions creates
     */
    protected abstract List<PromotionResult> applyDeal(final DealWrapper wrappedDeal,
            final List<ProductWrapper> wrappedProducts);

    /**
     * Deal has been met, now create the result list that hybris can deal with.
     * 
     * @param wrappedProducts
     *            the product details wrapped in our class for convenience
     * @param wrappedDeal
     *            the deal details wrapped in our class for convenience
     * @param instanceCount
     *            how many times this deal has been applied in this order
     * @return the list of promotion results that this promotions creates
     */

    protected List<PromotionResult> buildFiredResultList(final List<ProductWrapper> wrappedProducts,
            final DealWrapper wrappedDeal, final int instanceCount) {
        final List<PromotionResult> results = new ArrayList<>();
        final PromotionOrderView orderView = wrappedDeal.getOrderView();
        final SessionContext ctx = wrappedDeal.getCtx();
        final PromotionEvaluationContext promoContext = wrappedDeal.getPromoContext();
        final Currency currency = promoContext.getOrder().getCurrency();
        promoContext.startLoggingConsumed(this);
        final List<AbstractPromotionAction> actions = new ArrayList<>();
        createConsumedPromotionOrderEntries(wrappedProducts, orderView, currency, ctx, actions);
        final PromotionResult result = TgtCoreManager.getInstance().createTargetDealResult(ctx, this,
                promoContext.getOrder(), 1.0F, instanceCount);
        final List<PromotionOrderEntryConsumed> consumed = promoContext.finishLoggingAndGetConsumed(this, true);
        result.setConsumedEntries(ctx, consumed);
        result.setActions(ctx, actions);
        results.add(result);

        return results;
    }

    /**
     * Deal has been not been met but we have some items for this deal, now create the result list that hybris can deal
     * with.
     * 
     * @param wrappedProducts
     *            the product details wrapped in our class for convenience
     * @param wrappedDeal
     *            the deal details wrapped in our class for convenience
     * @param certainty
     *            how close we were to this deal being met, 0 not met at all, 1 fully met
     * @return the list of promotion results that this promotions creates
     */
    protected List<PromotionResult> buildCouldHaveFiredResultList(final List<ProductWrapper> wrappedProducts,
            final DealWrapper wrappedDeal, final float certainty) {
        final SessionContext ctx = wrappedDeal.getCtx();
        final PromotionEvaluationContext promoContext = wrappedDeal.getPromoContext();
        final List<PromotionResult> results = new ArrayList<>();
        promoContext.startLoggingConsumed(this);
        // our logic has broken products into individual items, no need for that now, and dealling with quantities should be more efficient
        consumePromotionOrderView(wrappedProducts, wrappedDeal, ctx);
        final PromotionResult result = PromotionsManager.getInstance().createPromotionResult(ctx, this,
                promoContext.getOrder(), certainty);
        result.setConsumedEntries(ctx, promoContext.finishLoggingAndGetConsumed(this, false));
        results.add(result);
        return results;
    }

    private double longCentsToDouble(final SessionContext ctx, final Currency currency, final long cents) {
        return Helper.roundCurrencyValue(ctx, currency, cents / 100.00).doubleValue();
    }

    /**
     * sort the items, least expensive to most expensive
     * 
     * @param wrappedProducts
     *            the product details wrapped in our class for convenience
     * @return array in the format that the POS logic can use
     */
    protected ProductWrapper[] getArrayOfSortedProducts(final List<ProductWrapper> wrappedProducts) {

        final ProductWrapper prodArray[] = wrappedProducts.toArray(new ProductWrapper[wrappedProducts.size()]);

        Arrays.sort(prodArray, new Comparator<ProductWrapper>() {
            @Override
            public int compare(final ProductWrapper pw0, final ProductWrapper pw1) {
                return (int)(pw0.getUnitSellPrice() - pw1.getUnitSellPrice());
            }
        });

        return prodArray;
    }

    /**
     * sort the items, highest expensive to least expensive
     * 
     * @param wrappedProducts
     *            the product details wrapped in our class for convenience
     * @return array in the format that the POS logic can use
     */
    protected ProductWrapper[] getArrayOfSortedProductsReverseOrder(final List<ProductWrapper> wrappedProducts) {

        final ProductWrapper prodArray[] = wrappedProducts.toArray(new ProductWrapper[wrappedProducts.size()]);

        Arrays.sort(prodArray, new Comparator<ProductWrapper>() {
            @Override
            public int compare(final ProductWrapper pw0, final ProductWrapper pw1) {
                return (int)(pw1.getUnitSellPrice() - pw0.getUnitSellPrice());
            }
        });

        return prodArray;
    }

    /**
     * extract the reward items and then sort them,based on the deal consumer config value
     * 
     * @param wrappedProducts
     *            the product details wrapped in our class for convenience
     * @return array in the format that the POS logic can use
     */
    protected ProductWrapper[] getArrayOfSortedRewardProducts(final List<ProductWrapper> wrappedProducts,
            final DealWrapper wrappedDeal) {
        final List<ProductWrapper> rewardProducts = new ArrayList<ProductWrapper>(wrappedProducts.size()); // set capacity to max size it could be to ensure we don't need to resize

        for (final ProductWrapper product : wrappedProducts) {
            if (product.isRewardProduct()) {
                rewardProducts.add(product);
            }
        }
        ProductWrapper prodArray[] = null;
        final DealRewardConsumeOrderEnum dealConfig = getDealConsumeOrderConfigValue(wrappedDeal);
        if (DealRewardConsumeOrderEnum.PRICELOWTOHIGH.equals(dealConfig)) {
            prodArray = getArrayOfSortedProductsReverseOrder(rewardProducts);
        }
        else {
            prodArray = getArrayOfSortedProducts(rewardProducts);
        }
        return prodArray;
    }

    /**
     * This method sort the products in lowtohigh/hightolow based on the deal consume order config value
     * 
     * @param wrappedProducts
     * @param wrappedDeal
     * @return prodArray
     */
    protected ProductWrapper[] getArrayOfSortedProducts(final List<ProductWrapper> wrappedProducts,
            final DealWrapper wrappedDeal)
    {
        ProductWrapper prodArray[] = null;
        final DealRewardConsumeOrderEnum dealConfig = getDealConsumeOrderConfigValue(wrappedDeal);
        if (DealRewardConsumeOrderEnum.PRICELOWTOHIGH.equals(dealConfig)) {
            prodArray = getArrayOfSortedProductsReverseOrder(wrappedProducts);
        }
        else {
            prodArray = getArrayOfSortedProducts(wrappedProducts);
        }
        return prodArray;
    }

    protected double round(final double value) {
        if (commonI18NService == null) {
            commonI18NService = (CommonI18NService)Registry.getGlobalApplicationContext().getBean("commonI18NService");
        }
        return commonI18NService.roundCurrency(value, AUD_DIGITS);
    }

    /**
     * This method return the order config value in which the deal has to be calculated Default/Pricelowtohigh
     * 
     * @param wrappedDeal
     * @return dealconfig
     */
    protected DealRewardConsumeOrderEnum getDealConsumeOrderConfigValue(final DealWrapper wrappedDeal) {

        if (configurationService == null) {
            configurationService = (ConfigurationService)Registry.getGlobalApplicationContext().getBean(
                    "configurationService");
        }
        final String dealRewardConsumeOrderString = configurationService.getConfiguration().getString(
                "deal.reward.consume.order");
        final DealRewardConsumeOrderEnum dealConfig = DealRewardConsumeOrderEnum.valueOf(dealRewardConsumeOrderString
                .toUpperCase());
        return dealConfig;
    }

    /**
     * 
     * @param wrappedProducts
     * @param orderView
     * @param currency
     * @param ctx
     * @param actions
     */
    protected void createConsumedPromotionOrderEntries(final List<ProductWrapper> wrappedProducts,
            final PromotionOrderView orderView, final Currency currency, final SessionContext ctx,
            final List<AbstractPromotionAction> actions) {
        final EnumerationManager enumManager = JaloSession.getCurrentSession().getEnumerationManager();
        for (final ProductWrapper product : wrappedProducts) {
            if (product.isInDeal()) {
                final TargetPromotionOrderEntryConsumed consumedEntry =
                        (TargetPromotionOrderEntryConsumed)orderView.consume(ctx, product.getProduct(), 1).get(0); // only consuming single unit so can only be one entry
                final double doubleAjustedUnitSellPrice =
                        longCentsToDouble(ctx, currency, product.getAdjustedUnitSellPrice());
                final double doubleMarkdown =
                        longCentsToDouble(ctx, currency, product.getDealMarkdown() * -1L);
                consumedEntry.setAdjustedUnitPrice(doubleAjustedUnitSellPrice);
                final EnumerationValue enumDealType =
                        enumManager.getEnumerationValue(TgtCoreConstants.TC.DEALITEMTYPEENUM, product.getType()
                                .getCode());
                consumedEntry.setDealItemType(enumDealType);
                consumedEntry.setInstance(product.getInstance());

                final PromotionOrderEntryAdjustAction adjustAction = PromotionsManager.getInstance()
                        .createPromotionOrderEntryAdjustAction(ctx, consumedEntry.getOrderEntry(ctx), 1L,
                                doubleMarkdown);
                actions.add(adjustAction);
            }
        }
    }

    /**
     * 
     * @param wrappedProducts
     * @param wrappedDeal
     * @param ctx
     */
    protected void consumePromotionOrderView(final List<ProductWrapper> wrappedProducts, final DealWrapper wrappedDeal,
            final SessionContext ctx) {
        long prdCnt = 0;
        Product curProduct = null;
        for (final ProductWrapper product : wrappedProducts) {
            if (!product.isInDeal()) {
                final Product thisProduct = product.getProduct();
                if (thisProduct.equals(curProduct)) {
                    prdCnt++;
                }
                else {
                    if (prdCnt > 0) {
                        wrappedDeal.getOrderView().consume(ctx, curProduct, prdCnt);
                    }
                    prdCnt = 1;
                    curProduct = thisProduct;
                }
            }
        }
        if (prdCnt > 0) {
            wrappedDeal.getOrderView().consume(ctx, curProduct, prdCnt);
        }
    }
}
