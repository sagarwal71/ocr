/**
 * 
 */
package de.hybris.platform.task.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;


/**
 * @author thomasadolfsson
 *
 */
public class TargetTaskServiceQueryProviderImpl implements TargetTaskServiceQueryProvider {

    private static final Logger LOG = LoggerFactory.getLogger(TargetTaskServiceQueryProviderImpl.class);


    //Get all Tasks and its corresponding processes along with order associations
    private static final String ALL_TASK_PROCESS_ORDER_DATA_QUERY = " WITH allTaskProcessData AS ( " +
            " SELECT   allTsk.pk as taskPK " +
            " , allTsk.createdTS as tskCreated " +
            " , allTsk.p_action as tskAction " +
            " , allTsk.p_runningonclusternode as isTaskRunning    " +
            " , allTsk.p_failed as isTaskFailed " +
            " , allTsk.p_expirationtimemillis as expirationtimemillis " +
            " , allTsk.p_executiontimemillis as execTimeInMillis " +
            " , allTsk.p_process as procPK " +
            " , procs.p_code as procCode " +
            " , procs.p_processdefinitionname as procDef " +
            " , procs.p_state as procState " +
            " , procs.createdTs as procCreated " +
            " , procs.p_order as ordPk " +
            " , p_nodeid as nodeid " +
            " , allTsk.hjmpTS as taskHjmpsTS " +
            " FROM    tasks allTsk " +
            " LEFT OUTER JOIN processes procs " +
            " ON procs.pk = allTsk.p_process " +
            " ) ";

    // Select the earlierst process, task combination for task associated to an order
    private static final String EARLIEST_PROCESS_TASK_ORDER_COMBINATION_QUERY = " ,OrderWithMinProcCreated AS ( " +
            " SELECT ordPk, MIN(procCreated) as procCreatedMin FROM allTaskProcessData " +
            " where  ordPk IS NOT NULL  " +
            " GROUP BY ordPk " +
            " ) ";

    // Select the earliest task task from the process selected above
    private static final String ORDER_WITH_EARLIEST_TASK_CREATED_QUERY = " ,OrderWithMinTaskCreated AS ( " +
            " SELECT allTaskProcessData.ordPk, MIN(tskCreated) as tskCreatedMin " +
            " FROM allTaskProcessData, OrderWithMinProcCreated " +
            " where allTaskProcessData.ordPk = OrderWithMinProcCreated.ordPk " +
            " and allTaskProcessData.procCreated = OrderWithMinProcCreated.procCreatedMin " +
            " and allTaskProcessData.isTaskRunning = :noNode " +
            " GROUP BY allTaskProcessData.ordPk " +
            " ) ";

    //Select the tasks & processes associated to Order to be executed " +
    private static final String TASK_ASSOC_ORDER_FILTERED_QUERY = " ,taskAssociatedToOrderFiltered AS ( " +
            " SELECT tskProcData.* " +
            " FROM allTaskProcessData tskProcData " +
            " INNER JOIN OrderWithMinTaskCreated   " +
            " ON tskProcData.ordPk = OrderWithMinTaskCreated.ordPk  " +
            " and tskProcData.tskCreated = OrderWithMinTaskCreated.tskCreatedMin " +
            " WHERE  ";

    //Ignore if any task for the order is in running state in another process
    private static final String FILTER_RUNNING_TASK_DIFFERENT_PROC_SAME_ORDER_QUERY = " NOT EXISTS ( " +
            " SELECT 1 FROM allTaskProcessData tskRunning " +
            " WHERE tskRunning.isTaskRunning!= :noNode  " +
            " AND tskRunning.ordPk= tskProcData.ordPk  " +
            " AND tskRunning.procCode!= tskProcData.procCode " +
            " ) ";
    // Ignore all processes for an order if there is any one process in failed state " +
    private static final String FILTER_PROCESS_HAVING_FAILED_PROCESS_ANOTHER_ORDER = "      AND NOT EXISTS ( " +
            " SELECT 1 FROM PROCESSES procs  " +
            " WHERE procs.p_order = tskProcData.ordPK " +
            " AND procs.p_state = :failedProcessState " +
            " ) ";
    //task combined is to combined the filtered tasks associated to the order and the ones which aren't
    private static final String COMBINE_FILTERED_TASKS_HAVING_ORDER_TO_WITHOUT_ORDER = " ) ,taskCombined AS ( " +
            " SELECT * FROM taskAssociatedToOrderFiltered " +
            " UNION  " +
            " SELECT * FROM allTaskProcessData " +
            " WHERE allTaskProcessData.ordPk IS NULL " +
            " ) " +
            // filters for time based and the failure
            " SELECT * from taskCombined " +
            " WHERE  taskCombined.isTaskFailed = :false  " +
            " AND ( " +
            " taskCombined.expirationtimemillis < :now  " +
            " OR ( " +
            " taskCombined.execTimeInMillis <= :now  " +
            " AND ( " +
            " taskCombined.nodeid = :nodeId  " +
            " OR taskCombined.nodeid IS NULL  " +
            " ) " +
            " AND taskCombined.isTaskRunning = :noNode  " +
            " AND NOT EXISTS ( " +
            " SELECT pk  " +
            " FROM taskconditions   " +
            " WHERE p_task=taskCombined.taskPK " +
            " AND ( p_fulfilled = :false OR p_fulfilled IS NULL )  " +
            " )  " +
            " ) " +
            " ) ORDER BY execTimeInMillis ASC";


    private TargetFeatureSwitchService targetFeatureSwitchService;


    @Override
    public String getTasksToExecuteQuery() {
        final boolean stallWhenOneTaskFailed = targetFeatureSwitchService
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.TASK_SERVICE_STALL_FAILED_PROCESS_SAME_ORDER);
        LOG.debug("Stall when one task failed enabled : {}", Boolean.valueOf(stallWhenOneTaskFailed));

        return getTaskQuery(stallWhenOneTaskFailed);
    }

    private String getTaskQuery(final boolean stallWhenOneTaskFailed) {
        final StringBuilder getTaskQuery = new StringBuilder();
        getTaskQuery.append(ALL_TASK_PROCESS_ORDER_DATA_QUERY).append(EARLIEST_PROCESS_TASK_ORDER_COMBINATION_QUERY)
                .append(ORDER_WITH_EARLIEST_TASK_CREATED_QUERY).append(TASK_ASSOC_ORDER_FILTERED_QUERY)
                .append(FILTER_RUNNING_TASK_DIFFERENT_PROC_SAME_ORDER_QUERY);
        if (stallWhenOneTaskFailed) {
            getTaskQuery.append(FILTER_PROCESS_HAVING_FAILED_PROCESS_ANOTHER_ORDER);
        }
        getTaskQuery.append(COMBINE_FILTERED_TASKS_HAVING_ORDER_TO_WITHOUT_ORDER);
        LOG.debug("query: {}", getTaskQuery.toString());
        return getTaskQuery.toString();
    }

    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }



}
