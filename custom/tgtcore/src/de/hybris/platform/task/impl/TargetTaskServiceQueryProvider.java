/**
 * 
 */
package de.hybris.platform.task.impl;

/**
 * @author thomasadolfsson
 *
 */
public interface TargetTaskServiceQueryProvider {

    String getTasksToExecuteQuery();
}
