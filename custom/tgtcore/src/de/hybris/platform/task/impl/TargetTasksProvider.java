package de.hybris.platform.task.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;


public class TargetTasksProvider implements TasksProvider {
    private static final Logger LOG = LoggerFactory.getLogger(TargetTasksProvider.class);

    private TasksProvider internalTasksProvider;
    private TasksProvider defaultTasksProvider;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Override
    public List<VersionPK> getTasksToSchedule(final RuntimeConfigHolder runtimeConfigHolder,
            final TaskEngineParameters taskEngineParameters, final int i) {

        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.TASK_SERVICE)) {
            LOG.debug("Running Target Task Provider");
            return internalTasksProvider.getTasksToSchedule(runtimeConfigHolder, taskEngineParameters, i);
        }
        else {
            LOG.debug("Running Default Task Provider");
            return defaultTasksProvider.getTasksToSchedule(runtimeConfigHolder, taskEngineParameters, i);
        }

    }

    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    @Required
    public void setInternalTasksProvider(final TasksProvider internalTasksProvider) {
        this.internalTasksProvider = internalTasksProvider;
    }

    @Required
    public void setDefaultTasksProvider(final TasksProvider defaultTasksProvider) {
        this.defaultTasksProvider = defaultTasksProvider;
    }
}
