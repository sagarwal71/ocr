package de.hybris.platform.task.impl;

import de.hybris.platform.core.PK;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.type.TypeService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;


/**
 * This has been refactored from the custom TargetTaskService
 */
public class TargetQueryTaskProvider implements TasksProvider {

    private static final Logger LOG = LoggerFactory.getLogger(TargetQueryTaskProvider.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    private TransactionTemplate transactionTemplate;

    private Long processStateFailedPk;

    private TypeService typeService;

    private TargetTaskServiceQueryProvider targetTaskServiceQueryProvider;


    @Override
    public List<VersionPK> getTasksToSchedule(final RuntimeConfigHolder runtimeConfigHolder,
            final TaskEngineParameters taskEngineParameters, final int i) {

        if (processStateFailedPk != null) {
            final PK pk = typeService
                    .getEnumerationValue(ProcessState.class.getSimpleName(), ProcessState.FAILED.toString()).getPk();
            processStateFailedPk = pk.getLong();
        }
        return queryNewTasks(taskEngineParameters);
    }


    private List<VersionPK> queryNewTasks(final TaskEngineParameters taskEngineParameters) {
        return transactionTemplate.execute(new TransactionCallback<List<VersionPK>>() {
            @Override
            public List<VersionPK> doInTransaction(final TransactionStatus status) {
                try {

                    final int clusterNodeID = taskEngineParameters.getClusterNodeID();

                    final Map<String, Object> argMap = new HashMap<String, Object>();
                    argMap.put("now", Long.valueOf(System.currentTimeMillis()));
                    argMap.put("false", Boolean.FALSE);
                    argMap.put("nodeId", Integer.valueOf(clusterNodeID));
                    argMap.put("noNode", Integer.valueOf(-1));
                    argMap.put("failedProcessState", processStateFailedPk);
                    final List<Map<String, Object>> rows = jdbcTemplate
                            .queryForList(
                                    targetTaskServiceQueryProvider.getTasksToExecuteQuery(),
                                    argMap);
                    final List<VersionPK> taskList = new ArrayList<>();
                    if (CollectionUtils.isNotEmpty(rows)) {
                        for (final Map<String, Object> row : rows) {
                            // taskPk is defined in task_query
                            taskList.add(
                                    new VersionPK(convertObjectToPK(row.get("taskPK")),
                                            toLong(row.get("taskHjmpsTS"))));

                        }
                    }
                    LOG.debug("Tasklist : {}", taskList);
                    return taskList;
                }
                catch (final DataAccessException dae) {
                    throw new SystemException(dae);
                }
            }
        });
    }


    private static long toLong(final Object o) {
        final String s = o.toString();
        final Long l = Long.valueOf(s);
        return l.longValue();
    }

    private static PK convertObjectToPK(final Object o) {
        return PK.fromLong(toLong(o));
    }

    @Required
    public void setJdbcTemplate(final NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Required
    public void setTransactionTemplate(final TransactionTemplate transactionTemplate) {
        this.transactionTemplate = transactionTemplate;
    }

    @Required
    public void setTypeService(final TypeService typeService) {
        this.typeService = typeService;
    }

    @Required
    public void setTargetTaskServiceQueryProvider(final TargetTaskServiceQueryProvider targetTaskServiceQueryProvider) {
        this.targetTaskServiceQueryProvider = targetTaskServiceQueryProvider;
    }
}
