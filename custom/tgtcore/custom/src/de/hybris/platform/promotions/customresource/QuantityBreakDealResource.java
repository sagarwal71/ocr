package de.hybris.platform.promotions.resource;

import de.hybris.platform.core.resource.link.LinkResource;
import de.hybris.platform.promotions.dto.QuantityBreakDealDTO;
import de.hybris.platform.promotions.model.BuyGetSameListDealModel;
import de.hybris.platform.promotions.model.QuantityBreakDealModel;
import de.hybris.platform.webservices.AbstractYResource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 * Custom resource class for type QuantityBreakDeal to invoke initdefaults interceptor
 */
public class QuantityBreakDealResource extends AbstractYResource<QuantityBreakDealModel>
{
	/**
	 * <i> constructor</i> - for generic creation.
	 */
	public QuantityBreakDealResource()
	{
		super("QuantityBreakDeal");
	}
	
	
	/**
	 * HTTP method for covering DELETE requests.
	 * @return {@link javax.ws.rs.core.Response}
	 */
	@DELETE
	public Response deleteQuantityBreakDeal()
	{
		return createDeleteResponse().build();
	}
	
	/**
	 *  getter for sub resource of type {@link LinkResource} for current root resource 
	 */
	@Path("/links/{link}")
	public AbstractYResource getLinkResource(@PathParam("link")  final String resourceKey)
	{
		final LinkResource  resource  = resourceCtx.getResource(LinkResource.class);
		resource.setResourceId(resourceKey );
		resource.setParentResource(this);
		passUniqueMember(resource);
		return resource;
	}
	
	/**
	 * HTTP method for covering GET requests.
	 * @return {@link javax.ws.rs.core.Response}
	 */
	@GET
	public Response getQuantityBreakDeal()
	{
		return createGetResponse().build();
	}
	
	/**
	 * Convenience method which just delegates to {@link #getResourceValue()}
	 */
	public QuantityBreakDealModel getQuantityBreakDealModel()
	{
		return super.getResourceValue();
	}
	
	/**
	 * HTTP method for covering PUT requests.
	 * @return {@link javax.ws.rs.core.Response}
	 */
	@PUT
	public Response putQuantityBreakDeal(final QuantityBreakDealDTO dto)
	{
		return createPutResponse(dto).build();
	}
	
	/**
	 * Gets the {@link QuantityBreakDealModel} resource which is addressed by current resource request.
	 * @see de.hybris.platform.webservices.AbstractYResource#readResource(String)
	 */
	@Override
	protected QuantityBreakDealModel readResource(final String resourceId) throws Exception
	{
		QuantityBreakDealModel model = new QuantityBreakDealModel();
		model.setCode(resourceId);
		return (QuantityBreakDealModel) readResourceInternal(model);
	}
	
	/**
	 * Convenience method which just delegates to {@link #setResourceValue(QuantityBreakDealModel)}
	 */
	public void setQuantityBreakDealModel(final QuantityBreakDealModel value)
	{
		super.setResourceValue(value);
	}
	
	/**
     * Overridden to invoke init defaults interceptor
     */
    @Override
    protected QuantityBreakDealModel createResource(@SuppressWarnings("unused") final Object reqEntity)
    {
        return getServiceLocator().getModelService().create(QuantityBreakDealModel.class);
    }
	
}
