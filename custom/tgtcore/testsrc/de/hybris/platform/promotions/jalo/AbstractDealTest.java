package de.hybris.platform.promotions.jalo;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.SessionContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.deals.wrappers.DealWrapper;
import au.com.target.tgtcore.deals.wrappers.ProductWrapper;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AbstractDealTest {
    private final AbstractDeal deal = new AbstractDeal() {

        @Override
        protected List<PromotionResult> preApplyCheck(final DealWrapper wrappedDeal,
                final List<ProductWrapper> wrappedProducts) {
            // do nothing
            return null;
        }

        @Override
        protected List<PromotionResult> applyDeal(final DealWrapper wrappedDeal,
                final List<ProductWrapper> wrappedProducts) {
            // do nothing
            return null;
        }

        @Override
        public String getResultDescription(final SessionContext ctx, final PromotionResult promotionResult,
                final Locale locale) {
            return "result description";
        }
    };

    @Test
    public void testGetArrayOfSortedProducts() {
        final List<ProductWrapper> list = new ArrayList<ProductWrapper>(3);

        final ProductWrapper p1 = Mockito.mock(ProductWrapper.class);
        BDDMockito.given(Long.valueOf(p1.getUnitSellPrice())).willReturn(Long.valueOf(200));
        list.add(p1);

        final ProductWrapper p2 = Mockito.mock(ProductWrapper.class);
        BDDMockito.given(Long.valueOf(p2.getUnitSellPrice())).willReturn(Long.valueOf(100));
        list.add(p2);

        final ProductWrapper p3 = Mockito.mock(ProductWrapper.class);
        BDDMockito.given(Long.valueOf(p3.getUnitSellPrice())).willReturn(Long.valueOf(300));
        list.add(p3);

        final ProductWrapper sorted[] = deal.getArrayOfSortedProducts(list);
        Assert.assertEquals(100, sorted[0].getUnitSellPrice());
        Assert.assertEquals(200, sorted[1].getUnitSellPrice());
        Assert.assertEquals(300, sorted[2].getUnitSellPrice());
    }

    @Test
    public void testGetArrayOfSortedProductsReverseOrder() {
        final List<ProductWrapper> list = new ArrayList<ProductWrapper>(3);

        final ProductWrapper p1 = Mockito.mock(ProductWrapper.class);
        BDDMockito.given(Long.valueOf(p1.getUnitSellPrice())).willReturn(Long.valueOf(200));
        list.add(p1);

        final ProductWrapper p2 = Mockito.mock(ProductWrapper.class);
        BDDMockito.given(Long.valueOf(p2.getUnitSellPrice())).willReturn(Long.valueOf(100));
        list.add(p2);

        final ProductWrapper p3 = Mockito.mock(ProductWrapper.class);
        BDDMockito.given(Long.valueOf(p3.getUnitSellPrice())).willReturn(Long.valueOf(300));
        list.add(p3);

        final ProductWrapper sorted[] = deal.getArrayOfSortedProductsReverseOrder(list);
        Assert.assertEquals(300, sorted[0].getUnitSellPrice());
        Assert.assertEquals(200, sorted[1].getUnitSellPrice());
        Assert.assertEquals(100, sorted[2].getUnitSellPrice());
    }
}
