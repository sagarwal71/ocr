package de.hybris.platform.promotions.jalo;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.BuyGetDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.promotions.result.PromotionEvaluationContext;
import de.hybris.platform.promotions.result.PromotionOrderView;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.deals.wrappers.DealWrapper;
import au.com.target.tgtcore.deals.wrappers.DealWrapper.DealType;
import au.com.target.tgtcore.deals.wrappers.ProductWrapper;
import au.com.target.tgtcore.enums.DealItemTypeEnum;
import au.com.target.tgtcore.enums.DealRewardConsumeOrderEnum;
import au.com.target.tgtcore.jalo.AbstractTargetVariantProduct;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("deprecation")
public class BuyGetDealTest {
    private class ProductWrapperTestImpl implements ProductWrapper {
        private boolean inDeal = false;

        private final AbstractTargetVariantProduct p;
        private final DealQualifierModel q;
        private final long unitPrice;
        private long markdown;
        private final boolean reward;
        private boolean rewardpartaillyConsumed = false;
        private int instance;

        public ProductWrapperTestImpl(final AbstractTargetVariantProduct p, final DealQualifierModel q,
                final long unitPrice, final boolean reward) {
            this.p = p;
            this.q = q;
            this.unitPrice = unitPrice;
            this.reward = reward;
        }

        @Override
        public DealQualifierModel getQualifierModel() {
            return q;
        }

        @Override
        public long getUnitSellPrice() {
            return unitPrice;
        }

        @Override
        public long getAdjustedUnitSellPrice() {
            return unitPrice - markdown;
        }

        @Override
        public void setDealApplied(final int inst, final DealItemTypeEnum dealType, final long saveAmt) {
            markdown = saveAmt;
            inDeal = true;
            instance = inst;
        }

        @Override
        public boolean isInDeal() {
            return inDeal;
        }

        @Override
        public long getDealMarkdown() {
            return markdown;
        }

        @Override
        public Product getProduct() {
            return p;
        }

        @Override
        public DealItemTypeEnum getType() {
            return null;
        }

        @Override
        public int getInstance() {
            return instance;
        }


        @Override
        public void clearDeal() {
            // these are not the droids you are looking for
        }

        @Override
        public boolean isRewardProduct() {
            return reward;
        }


        @Override
        public void setDealRewardsPartiallyConsumed() {
            rewardpartaillyConsumed = true;

        }


        @Override
        public boolean getDealRewardsPartiallyConsumed() {

            return rewardpartaillyConsumed;
        }


        @Override
        public void clearDealRewardsPartiallyConsumed() {
            rewardpartaillyConsumed = false;
        }
    }

    private class DealWrapperTestImpl implements DealWrapper {
        private DealType dealType;

        @Override
        public AbstractDealModel getDealModel() {
            return dealModel;
        }

        @Override
        public List<DealQualifierModel> getQualifierModelList() {
            return qualList;
        }

        @Override
        public SessionContext getCtx() {
            return null;
        }

        @Override
        public PromotionEvaluationContext getPromoContext() {
            return null;
        }

        @Override
        public PromotionOrderView getOrderView() {
            return null;
        }

        @Override
        public DealType getDealType() {
            return dealType;
        }

        public void setDealType(final DealType dealType) {
            this.dealType = dealType;
        }
    }


    @Mock
    private BuyGetDealModel dealModel;

    @Mock
    private DealQualifierModel q1;

    @Mock
    private AbstractTargetVariantProduct p1;
    @Mock
    private AbstractTargetVariantProduct p2;
    @Mock
    private AbstractTargetVariantProduct p3;
    @Mock
    private AbstractTargetVariantProduct p4;

    private boolean dealApplied = false;
    private final BuyGetDeal deal = new BuyGetDeal() {

        @Override
        public int getRewardMaxQtyAsPrimitive() {
            return dealModel.getRewardMaxQty().intValue();
        }

        @Override
        public double getRewardValueAsPrimitive() {
            return dealModel.getRewardValue().doubleValue();
        }

        @Override
        public double getRewardMinSaveAsPrimitive() {
            return dealModel.getRewardMinSave().doubleValue();
        }

        @Override
        protected java.util.List<PromotionResult> buildFiredResultList(
                final java.util.List<ProductWrapper> productWrapperList,
                final DealWrapper dealWrapper, final int instanceCount) {
            dealApplied = true;
            return null;
        }

        @Override
        protected List<PromotionResult> buildCouldHaveFiredResultListForAllQualifiersButNoRewards(
                final List<ProductWrapper> productWrapper,
                final DealWrapper dealWrapper, final float certainity, final int maxNumberofRewards) {

            return new ArrayList<>();

        }

        @Override
        protected List<PromotionResult> buildFiredResultListWithRewards(final List<ProductWrapper> productWrapper,
                final DealWrapper dealWrapper, final int instanceCount, final int numberofRewardsSofar,
                final int maxNUmberOfRewards) {
            dealApplied = true;
            return new ArrayList<>();

        }

        @Override
        protected DealRewardConsumeOrderEnum getDealConsumeOrderConfigValue(
                final DealWrapper dealWrapper)
        {
            return DealRewardConsumeOrderEnum.DEFAULT;
        }

        @Override
        protected java.util.List<PromotionResult> buildCouldHaveFiredResultList(
                final java.util.List<ProductWrapper> productWrapperList, final DealWrapper dealWrapper,
                final float certainty) {
            return new ArrayList<>();
        }

        @Override
        protected float calcQtyCertainty(final List<ProductWrapper> productWrapper) {
            return 0.0F;
        }
    };

    private final List<DealQualifierModel> qualList = new ArrayList<>();
    private final DealWrapper wrappedDeal = new DealWrapperTestImpl();
    private final List<ProductWrapper> wrappedProducts = new ArrayList<>();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        BDDMockito.given(dealModel.getQualifierList()).willReturn(qualList);
    }

    private long sumProductMarkdowns() {
        long retVal = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            retVal += wrappedProduct.getDealMarkdown();
        }
        return retVal;
    }

    private long sumTotalPrice() {
        long retVal = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            retVal += wrappedProduct.getAdjustedUnitSellPrice();
        }
        return retVal;
    }

    //**************************************************************************
    protected final int itemsWithValue(final int amt)
    {
        int ret = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            if (wrappedProduct.getAdjustedUnitSellPrice() == amt) {
                ret++;
            }
        }

        return ret;
    }


    //**************************************************************************
    protected final int itemsWithMarkdown(final int amt)
    {
        int ret = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            if (wrappedProduct.getDealMarkdown() == amt) {
                ret++;
            }
        }

        return ret;
    }

    //##################################################################################
    // Buy 2 items and get 10% off an unlimited number of items
    //##################################################################################

    @SuppressWarnings("boxing")
    private void setupDealBuy2Get10PctUnlimited() {
        BDDMockito.given(q1.getMinQty()).willReturn(Integer.valueOf(2));
        qualList.add(q1);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(10.00));
        ((DealWrapperTestImpl)wrappedDeal).setDealType(DealType.PERCENT_OFF_EACH);
    }

    @Test
    public void testApplyDealPctOffUnlimitedRewardOverlapingQualRewardNotEnoughItems() throws Exception {
        setupDealBuy2Get10PctUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);

        wrappedProducts.add(pw1);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
    }

    @Test
    public void testApplyDealPctOffUnlimitedRewardNonOverlapingQualRewardEnoughQualifingItemsButNotEnoughForAReward()
            throws Exception {
        setupDealBuy2Get10PctUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
    }

    @Test
    public void testApplyDealPctOffUnlimitedRewardNonOverlapingQualRewardEnoughItemsFor1RewardItem() throws Exception {
        setupDealBuy2Get10PctUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 1500, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(150, pw3.getDealMarkdown());
        Assert.assertEquals(4350, sumTotalPrice());
        Assert.assertEquals(150, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealPctOffUnlimitedRewardNonOverlapingQualRewardEnoughItemsForMultipleRewardItems()
            throws Exception {
        setupDealBuy2Get10PctUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 1500, true);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p2, null, 1500, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(150, pw3.getDealMarkdown());
        Assert.assertEquals(150, pw4.getDealMarkdown());
        Assert.assertEquals(5700, sumTotalPrice());
        Assert.assertEquals(300, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealPctOffUnlimitedRewardOverlapingQualRewardEnoughQualifingItemsButNotEnoughForAReward()
            throws Exception {
        setupDealBuy2Get10PctUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, true);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
    }

    @Test
    public void testApplyDealPctOffUnlimitedRewardOverlapingQualRewardEnoughItemsFor1RewardItem() throws Exception {
        setupDealBuy2Get10PctUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, true);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, true);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, q1, 1400, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(140, pw3.getDealMarkdown()); // cheapest item is always the reward
        Assert.assertEquals(4260, sumTotalPrice());
        Assert.assertEquals(140, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealPctOffUnlimitedRewardOverlapingQualRewardEnoughItemsForMultipleRewardItems()
            throws Exception {
        setupDealBuy2Get10PctUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, true);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, true);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, q1, 1400, true);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p2, q1, 1400, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(140, pw3.getDealMarkdown()); // cheapest items are always the reward
        Assert.assertEquals(140, pw4.getDealMarkdown());
        Assert.assertEquals(5520, sumTotalPrice());
        Assert.assertEquals(280, sumProductMarkdowns());
    }

    //##################################################################################
    // Buy 1 items and get 50% off 1 another item
    //##################################################################################

    @SuppressWarnings("boxing")
    private void setupDealBuy1Get50PctAnother() {
        BDDMockito.given(q1.getMinQty()).willReturn(Integer.valueOf(1));
        qualList.add(q1);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(50.00));
        BDDMockito.given(deal.getRewardMaxQtyAsPrimitive()).willReturn(Integer.valueOf(1));
        ((DealWrapperTestImpl)wrappedDeal).setDealType(DealType.PERCENT_OFF_EACH);
    }

    @Test
    public void testApplyDealPctOffOneRewardOverlapingQualRewardNotEnoughItems() throws Exception {
        setupDealBuy1Get50PctAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);

        wrappedProducts.add(pw1);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
    }

    @Test
    public void testApplyDealPctOffOneRewardNonOverlapingQualRewardEnoughQualifingItemsButNotEnoughForAReward()
            throws Exception {
        setupDealBuy1Get50PctAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
    }

    @Test
    public void testApplyDealPctOffOneRewardNonOverlapingQualRewardEnoughItemsFor1RewardItem() throws Exception {
        setupDealBuy1Get50PctAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 1500, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(750, pw3.getDealMarkdown());
        Assert.assertEquals(3750, sumTotalPrice());
        Assert.assertEquals(750, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealPctOffOneRewardNonOverlapingQualRewardEnoughItemsForMultipleRewardItemsButOnlyFiredOnce()
            throws Exception {
        setupDealBuy1Get50PctAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, null, 1500, true);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 1500, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(1, itemsWithMarkdown(750));
        Assert.assertEquals(2, itemsWithMarkdown(0));
        Assert.assertEquals(3750, sumTotalPrice());
        Assert.assertEquals(750, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealPctOffOneRewardNonOverlapingQualRewardEnoughItemsForMultipleRewardItemsForMultipleInstances()
            throws Exception {
        setupDealBuy1Get50PctAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p3, null, 1500, true);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p4, null, 1500, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(2, itemsWithMarkdown(750));
        Assert.assertEquals(2, itemsWithMarkdown(0));
        Assert.assertEquals(4500, sumTotalPrice());
        Assert.assertEquals(1500, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealPctOffOneRewardOverlapingQualRewardEnoughQualifingItemsButNotEnoughForAReward()
            throws Exception {
        setupDealBuy1Get50PctAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, true);

        wrappedProducts.add(pw1);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
    }

    @Test
    public void testApplyDealPctOffOneRewardOverlapingQualRewardEnoughItemsFor1RewardItem() throws Exception {
        setupDealBuy1Get50PctAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, true);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(1, itemsWithMarkdown(750));
        Assert.assertEquals(1, itemsWithMarkdown(0));
        Assert.assertEquals(2250, sumTotalPrice());
        Assert.assertEquals(750, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealPctOffOneRewardOverlapingQualRewardEnoughItemsForMultipleRewardItemsButOnlyFiredOnce()
            throws Exception {
        setupDealBuy1Get50PctAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, true);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, q1, 1400, true);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p3, q1, 1300, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        // most expensive is the first qualifier, the second most expensive becomes the reward
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(700, pw2.getDealMarkdown());
        Assert.assertEquals(0, pw3.getDealMarkdown());
        Assert.assertEquals(3500, sumTotalPrice());
        Assert.assertEquals(700, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealPctOffOneRewardOverlapingQualRewardEnoughItemsForMultipleInstances() throws Exception {
        setupDealBuy1Get50PctAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, true);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, q1, 1400, true);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p3, q1, 1300, true);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p4, q1, 1200, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        // most expensive is the first qualifier, the second most expensive becomes the reward (then we do it again with remaining items)
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(700, pw2.getDealMarkdown());
        Assert.assertEquals(00, pw3.getDealMarkdown());
        Assert.assertEquals(600, pw4.getDealMarkdown());
        Assert.assertEquals(4100, sumTotalPrice());
        Assert.assertEquals(1300, sumProductMarkdowns());
    }

    //##################################################################################
    // Buy 1 items and get $10.00 off 1 another item
    //##################################################################################

    @SuppressWarnings("boxing")
    private void setupDealBuy1Get10DolAnother() {
        BDDMockito.given(q1.getMinQty()).willReturn(Integer.valueOf(1));
        qualList.add(q1);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(10.00));
        BDDMockito.given(deal.getRewardMaxQtyAsPrimitive()).willReturn(Integer.valueOf(1));
        ((DealWrapperTestImpl)wrappedDeal).setDealType(DealType.DOLLAR_OFF_EACH);
    }

    @Test
    public void testApplyDealDolOffOneRewardOverlapingQualRewardNotEnoughItems() throws Exception {
        setupDealBuy1Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);

        wrappedProducts.add(pw1);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
    }

    @Test
    public void testApplyDealDolOffOneRewardNonOverlapingQualRewardEnoughQualifingItemsButNotEnoughForAReward()
            throws Exception {
        setupDealBuy1Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
    }

    @Test
    public void testApplyDealDolOffOneRewardNonOverlapingQualRewardEnoughItemsFor1RewardItem() throws Exception {
        setupDealBuy1Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 1500, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(1000, pw3.getDealMarkdown());
        Assert.assertEquals(3500, sumTotalPrice());
        Assert.assertEquals(1000, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealDolOffOneRewardNonOverlapingQualRewardEnoughItemsForMultipleRewardItemsButOnlyFiredOnce()
            throws Exception {
        setupDealBuy1Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, null, 1500, true);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 1500, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(1, itemsWithMarkdown(1000));
        Assert.assertEquals(2, itemsWithMarkdown(0));
        Assert.assertEquals(3500, sumTotalPrice());
        Assert.assertEquals(1000, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealDolOffOneRewardNonOverlapingQualRewardEnoughItemsForMultipleRewardItemsForMultipleInstances()
            throws Exception {
        setupDealBuy1Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p3, null, 1500, true);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p4, null, 1500, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(2, itemsWithMarkdown(1000));
        Assert.assertEquals(2, itemsWithMarkdown(0));
        Assert.assertEquals(4000, sumTotalPrice());
        Assert.assertEquals(2000, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealDolOffOneRewardOverlapingQualRewardEnoughQualifingItemsButNotEnoughForAReward()
            throws Exception {
        setupDealBuy1Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, true);

        wrappedProducts.add(pw1);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
    }

    @Test
    public void testApplyDealDolOffOneRewardOverlapingQualRewardEnoughItemsFor1RewardItem() throws Exception {
        setupDealBuy1Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, true);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(1, itemsWithMarkdown(1000));
        Assert.assertEquals(1, itemsWithMarkdown(0));
        Assert.assertEquals(2000, sumTotalPrice());
        Assert.assertEquals(1000, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealDolOffOneRewardOverlapingQualRewardEnoughItemsForMultipleRewardItemsButOnlyFiredOnce()
            throws Exception {
        setupDealBuy1Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, true);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, q1, 1400, true);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p3, q1, 1300, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        // most expensive is the first qualifier, the second most expensive becomes the reward
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(1000, pw2.getDealMarkdown());
        Assert.assertEquals(0, pw3.getDealMarkdown());
        Assert.assertEquals(3200, sumTotalPrice());
        Assert.assertEquals(1000, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealDolOffOneRewardOverlapingQualRewardEnoughItemsForMultipleInstances() throws Exception {
        setupDealBuy1Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, true);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, q1, 1400, true);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p3, q1, 1300, true);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p4, q1, 1200, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        // most expensive is the first qualifier, the second most expensive becomes the reward (then we do it again with remaining items)
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(1000, pw2.getDealMarkdown());
        Assert.assertEquals(00, pw3.getDealMarkdown());
        Assert.assertEquals(1000, pw4.getDealMarkdown());
        Assert.assertEquals(3400, sumTotalPrice());
        Assert.assertEquals(2000, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealDolOffOneRewardDontGoNegative()
            throws Exception {
        setupDealBuy1Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, null, 800, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(800, pw2.getDealMarkdown());
        Assert.assertEquals(1500, sumTotalPrice());
        Assert.assertEquals(800, sumProductMarkdowns());
    }

    //##################################################################################
    // Buy 2 items and get $1.00 off 1 unlimited items
    //##################################################################################

    @SuppressWarnings("boxing")
    private void setupDealBuy2Get1DolUnlimited() {
        BDDMockito.given(q1.getMinQty()).willReturn(Integer.valueOf(2));
        qualList.add(q1);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(1.00));
        ((DealWrapperTestImpl)wrappedDeal).setDealType(DealType.DOLLAR_OFF_EACH);
    }

    @Test
    public void testApplyDealDolOffUnlimitedRewardOverlapingQualRewardNotEnoughItems() throws Exception {
        setupDealBuy2Get1DolUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);

        wrappedProducts.add(pw1);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
    }

    @Test
    public void testApplyDealDolOffUnlimitedRewardNonOverlapingQualRewardEnoughQualifingItemsButNotEnoughForAReward()
            throws Exception {
        setupDealBuy2Get1DolUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
    }

    @Test
    public void testApplyDealDolOffUnlimitedRewardNonOverlapingQualRewardEnoughItemsFor1RewardItem() throws Exception {
        setupDealBuy2Get1DolUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 1500, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(100, pw3.getDealMarkdown());
        Assert.assertEquals(4400, sumTotalPrice());
        Assert.assertEquals(100, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealDolOffUnlimitedRewardNonOverlapingQualRewardEnoughItemsForMultipleRewardItems()
            throws Exception {
        setupDealBuy2Get1DolUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 1500, true);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p2, null, 1500, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(100, pw3.getDealMarkdown());
        Assert.assertEquals(100, pw4.getDealMarkdown());
        Assert.assertEquals(5800, sumTotalPrice());
        Assert.assertEquals(200, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealDolOffUnlimitedRewardOverlapingQualRewardEnoughQualifingItemsButNotEnoughForAReward()
            throws Exception {
        setupDealBuy2Get1DolUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, true);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
    }

    @Test
    public void testApplyDealDolOffUnlimitedRewardOverlapingQualRewardEnoughItemsFor1RewardItem() throws Exception {
        setupDealBuy2Get1DolUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, true);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, true);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, q1, 1400, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(100, pw3.getDealMarkdown()); // cheapest item is always the reward
        Assert.assertEquals(4300, sumTotalPrice());
        Assert.assertEquals(100, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealDolOffUnlimitedRewardOverlapingQualRewardEnoughItemsForMultipleRewardItems()
            throws Exception {
        setupDealBuy2Get1DolUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, true);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, true);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, q1, 1400, true);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p2, q1, 1400, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(100, pw3.getDealMarkdown()); // cheapest items are always the reward
        Assert.assertEquals(100, pw4.getDealMarkdown());
        Assert.assertEquals(5600, sumTotalPrice());
        Assert.assertEquals(200, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealDolOffUnlimitedRewardDontGoNegative()
            throws Exception {
        setupDealBuy2Get1DolUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 80, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(80, pw3.getDealMarkdown());
        Assert.assertEquals(3000, sumTotalPrice());
        Assert.assertEquals(80, sumProductMarkdowns());
    }

    //##################################################################################
    // Buy 1 item and get one for a $1.00
    //##################################################################################

    @SuppressWarnings("boxing")
    private void setupDealBuy1Get1For1Dol() {
        BDDMockito.given(q1.getMinQty()).willReturn(Integer.valueOf(1));
        qualList.add(q1);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(1.00));
        BDDMockito.given(deal.getRewardMaxQtyAsPrimitive()).willReturn(Integer.valueOf(1));
        ((DealWrapperTestImpl)wrappedDeal).setDealType(DealType.FIXED_DOLLAR_EACH);
    }

    @Test
    public void testApplyDealFixedDolOneRewardOverlapingQualRewardNotEnoughItems() throws Exception {
        setupDealBuy1Get1For1Dol();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);

        wrappedProducts.add(pw1);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
    }

    @Test
    public void testApplyDealFixedDolOneRewardNonOverlapingQualRewardEnoughQualifingItemsButNotEnoughForAReward()
            throws Exception {
        setupDealBuy1Get1For1Dol();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
    }

    @Test
    public void testApplyDealFixedDolOneRewardNonOverlapingQualRewardEnoughItemsFor1RewardItem() throws Exception {
        setupDealBuy1Get1For1Dol();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 1500, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(1400, pw3.getDealMarkdown());
        Assert.assertEquals(3100, sumTotalPrice());
        Assert.assertEquals(1400, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealFixedDolOneRewardNonOverlapingQualRewardEnoughItemsForMultipleInstances()
            throws Exception {
        setupDealBuy1Get1For1Dol();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 1500, true);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p2, null, 1500, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(1400, pw3.getDealMarkdown());
        Assert.assertEquals(1400, pw4.getDealMarkdown());
        Assert.assertEquals(3200, sumTotalPrice());
        Assert.assertEquals(2800, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealFixedDolOneRewardOverlapingQualRewardEnoughQualifingItemsButNotEnoughForAReward()
            throws Exception {
        setupDealBuy1Get1For1Dol();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, true);

        wrappedProducts.add(pw1);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
    }

    @Test
    public void testApplyDealFixedDolOneRewardOverlapingQualRewardEnoughItemsFor1RewardItem() throws Exception {
        setupDealBuy1Get1For1Dol();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, true);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1400, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(1300, pw2.getDealMarkdown()); // cheapest item is always the reward
        Assert.assertEquals(1600, sumTotalPrice());
        Assert.assertEquals(1300, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealFixedDolOneRewardOverlapingQualRewardEnoughItemsForMultipleRewardItems()
            throws Exception {
        setupDealBuy1Get1For1Dol();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, true);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, q1, 1400, true);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, q1, 1300, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(1300, pw2.getDealMarkdown()); // most expensive after the qualifier gets the reward
        Assert.assertEquals(0, pw3.getDealMarkdown());
        Assert.assertEquals(2900, sumTotalPrice());
        Assert.assertEquals(1300, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealFixedDolOneRewardDontGoNegative()
            throws Exception {
        setupDealBuy1Get1For1Dol();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, null, 80, true);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(1580, sumTotalPrice());
        Assert.assertEquals(0, sumProductMarkdowns());
    }

    @Test
    public void testClearDealPartiallyConsumed()
    {
        final ProductWrapper buyList[] = new ProductWrapper[2];
        final ProductWrapper getList[] = new ProductWrapper[1];
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        pw1.setDealApplied(1, DealItemTypeEnum.QUALIFIER, 100);
        pw1.setDealRewardsPartiallyConsumed();
        buyList[0] = pw1;
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, null, 80, false);
        pw2.setDealApplied(1, DealItemTypeEnum.QUALIFIER, 200);
        pw2.setDealRewardsPartiallyConsumed();
        buyList[1] = pw2;
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 80, true);
        pw3.setDealApplied(1, DealItemTypeEnum.REWARD, 200);
        pw3.setDealRewardsPartiallyConsumed();
        getList[0] = pw3;
        deal.clearDealPartiallyConsumed(buyList, getList, 1);
        Assert.assertFalse(buyList[0].getDealRewardsPartiallyConsumed());
        Assert.assertFalse(buyList[1].getDealRewardsPartiallyConsumed());
        Assert.assertFalse(getList[0].getDealRewardsPartiallyConsumed());

    }

    @Test
    public void testClearDealPartiallyConsumedWithDiffInstance()
    {
        final ProductWrapper buyList[] = new ProductWrapper[2];
        final ProductWrapper getList[] = new ProductWrapper[1];
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        pw1.setDealApplied(1, DealItemTypeEnum.QUALIFIER, 100);
        pw1.setDealRewardsPartiallyConsumed();
        buyList[0] = pw1;
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, null, 80, false);
        pw2.setDealApplied(1, DealItemTypeEnum.QUALIFIER, 200);
        pw2.setDealRewardsPartiallyConsumed();
        buyList[1] = pw2;
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 80, true);
        pw3.setDealApplied(1, DealItemTypeEnum.REWARD, 200);
        pw3.setDealRewardsPartiallyConsumed();
        getList[0] = pw3;
        deal.clearDealPartiallyConsumed(buyList, getList, 2);
        Assert.assertTrue(buyList[0].getDealRewardsPartiallyConsumed());
        Assert.assertTrue(buyList[1].getDealRewardsPartiallyConsumed());
        Assert.assertTrue(getList[0].getDealRewardsPartiallyConsumed());

    }

    @Test
    public void testCreatePartailRewardProductList()
    {

        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        pw1.setDealApplied(1, DealItemTypeEnum.QUALIFIER, 100);
        pw1.setDealRewardsPartiallyConsumed();
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, q1, 300, false);
        pw2.setDealApplied(1, DealItemTypeEnum.QUALIFIER, 200);
        pw2.setDealRewardsPartiallyConsumed();
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 80, true);
        pw3.setDealApplied(1, DealItemTypeEnum.REWARD, 200);
        pw3.setDealRewardsPartiallyConsumed();
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p2, q1, 400, true);
        pw4.setDealApplied(1, DealItemTypeEnum.QUALIFIER, 200);
        final ProductWrapper pw5 = new ProductWrapperTestImpl(p2, q1, 80, true);
        pw5.setDealApplied(1, DealItemTypeEnum.QUALIFIER, 200);

        final List<ProductWrapper> actualList = new ArrayList<>();
        actualList.add(pw1);
        actualList.add(pw2);
        actualList.add(pw3);
        actualList.add(pw4);
        actualList.add(pw5);
        final List<ProductWrapper> listWithoutDealRewardsPartaillyConsumed = new ArrayList<>();
        final List<ProductWrapper> dealRewardPartiallyConsumed = deal.createPartailRewardProductList(actualList,
                listWithoutDealRewardsPartaillyConsumed);
        Assert.assertEquals(dealRewardPartiallyConsumed.size(), 3);
        Assert.assertTrue(dealRewardPartiallyConsumed.get(0).getDealRewardsPartiallyConsumed());
        Assert.assertEquals(listWithoutDealRewardsPartaillyConsumed.size(), 2);
        Assert.assertFalse(listWithoutDealRewardsPartaillyConsumed.get(0).getDealRewardsPartiallyConsumed());

    }

    @Test
    public void testCreatePartailRewardProductListWithNoPartialConsumedFlag()
    {

        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        pw1.setDealApplied(1, DealItemTypeEnum.QUALIFIER, 100);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, q1, 300, false);
        pw2.setDealApplied(1, DealItemTypeEnum.QUALIFIER, 200);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 80, true);
        pw3.setDealApplied(1, DealItemTypeEnum.REWARD, 200);
        final List<ProductWrapper> actualList = new ArrayList<>();
        actualList.add(pw1);
        actualList.add(pw2);
        actualList.add(pw3);
        final List<ProductWrapper> listWithoutDealRewardsPartaillyConsumed = new ArrayList<>();
        final List<ProductWrapper> dealRewardPartiallyConsumed = deal.createPartailRewardProductList(actualList,
                listWithoutDealRewardsPartaillyConsumed);
        Assert.assertEquals(dealRewardPartiallyConsumed.size(), 0);
        Assert.assertEquals(listWithoutDealRewardsPartaillyConsumed.size(), 3);
        Assert.assertFalse(listWithoutDealRewardsPartaillyConsumed.get(0).getDealRewardsPartiallyConsumed());

    }
}
