package de.hybris.platform.promotions.jalo;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.promotions.model.SpendAndGetDealModel;
import de.hybris.platform.promotions.result.PromotionEvaluationContext;
import de.hybris.platform.promotions.result.PromotionOrderView;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.deals.wrappers.DealWrapper;
import au.com.target.tgtcore.deals.wrappers.DealWrapper.DealType;
import au.com.target.tgtcore.deals.wrappers.ProductWrapper;
import au.com.target.tgtcore.enums.DealItemTypeEnum;
import au.com.target.tgtcore.enums.DealRewardConsumeOrderEnum;
import au.com.target.tgtcore.jalo.AbstractTargetVariantProduct;


@SuppressWarnings("deprecation")
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SpendAndGetDealTest {
    private class ProductWrapperTestImpl implements ProductWrapper {
        private boolean inDeal = false;

        private final AbstractTargetVariantProduct p;
        private final DealQualifierModel q;
        private final long unitPrice;
        private long markdown;
        private final boolean reward;
        private boolean rewardpartaillyConsumed = false;

        public ProductWrapperTestImpl(final AbstractTargetVariantProduct p, final DealQualifierModel q,
                final long unitPrice, final boolean reward) {
            this.p = p;
            this.q = q;
            this.unitPrice = unitPrice;
            this.reward = reward;
        }

        @Override
        public DealQualifierModel getQualifierModel() {
            return q;
        }

        @Override
        public long getUnitSellPrice() {
            return unitPrice;
        }

        @Override
        public long getAdjustedUnitSellPrice() {
            return unitPrice - markdown;
        }

        @Override
        public void setDealApplied(final int inst, final DealItemTypeEnum dealType, final long saveAmt) {
            markdown = saveAmt;
            inDeal = true;
        }

        @Override
        public boolean isInDeal() {
            return inDeal;
        }

        @Override
        public long getDealMarkdown() {
            return markdown;
        }

        @Override
        public Product getProduct() {
            return p;
        }

        @Override
        public DealItemTypeEnum getType() {
            return null;
        }

        @Override
        public int getInstance() {
            return 0;
        }

        @Override
        public void clearDeal() {
            // these are not the droids you are looking for
        }

        @Override
        public boolean isRewardProduct() {
            return reward;
        }

        @Override
        public void setDealRewardsPartiallyConsumed() {
            rewardpartaillyConsumed = true;

        }


        @Override
        public boolean getDealRewardsPartiallyConsumed() {

            return rewardpartaillyConsumed;
        }


        @Override
        public void clearDealRewardsPartiallyConsumed() {
            rewardpartaillyConsumed = false;
        }
    }

    private class DealWrapperTestImpl implements DealWrapper {
        private DealType dealType;

        @Override
        public AbstractDealModel getDealModel() {
            return dealModel;
        }

        @Override
        public List<DealQualifierModel> getQualifierModelList() {
            return qualList;
        }

        @Override
        public SessionContext getCtx() {
            return null;
        }

        @Override
        public PromotionEvaluationContext getPromoContext() {
            return null;
        }

        @Override
        public PromotionOrderView getOrderView() {
            return null;
        }

        @Override
        public DealType getDealType() {
            return dealType;
        }

        public void setDealType(final DealType dealType) {
            this.dealType = dealType;
        }
    }


    @Mock
    private SpendAndGetDealModel dealModel;

    @Mock
    private DealQualifierModel q1;

    @Mock
    private AbstractTargetVariantProduct p1;
    @Mock
    private AbstractTargetVariantProduct p2;
    @Mock
    private AbstractTargetVariantProduct p3;
    @Mock
    private AbstractTargetVariantProduct p4;

    private boolean dealApplied = false;
    private final SpendAndGetDeal deal = new SpendAndGetDeal() {

        @Override
        public int getRewardMaxQtyAsPrimitive() {
            return dealModel.getRewardMaxQty().intValue();
        }

        @Override
        public double getRewardValueAsPrimitive() {
            return dealModel.getRewardValue().doubleValue();
        }

        @Override
        public double getRewardMinSaveAsPrimitive() {
            return dealModel.getRewardMinSave().doubleValue();
        }

        @Override
        protected java.util.List<PromotionResult> buildFiredResultList(
                final java.util.List<ProductWrapper> productWrapperList,
                final DealWrapper dealWrapper, final int instanceCount) {
            dealApplied = true;
            return null;
        }

        @Override
        protected DealRewardConsumeOrderEnum getDealConsumeOrderConfigValue(
                final DealWrapper dealWrapper) {

            return DealRewardConsumeOrderEnum.DEFAULT;
        }

        @Override
        protected java.util.List<PromotionResult> buildCouldHaveFiredResultList(
                final java.util.List<ProductWrapper> productWrapperList, final DealWrapper dealWrapper,
                final float certainty) {
            return new ArrayList<>();
        }

        @Override
        protected List<PromotionResult> buildCouldHaveFiredResultListForAllQualifiersButNoRewards(
                final List<ProductWrapper> wrappedProducts,
                final DealWrapper wrappedDeal, final float certainty, final int maxNUmberOfRewards) {

            return new ArrayList<>();

        }

        @Override
        protected List<PromotionResult> buildFiredResultListWithRewards(final List<ProductWrapper> wrappedProducts,
                final DealWrapper wrappedDeal, final int instanceCount, final int numberofRewardsSofar,
                final int maxNUmberOfRewards) {
            dealApplied = true;
            return new ArrayList<>();

        }

        @Override
        protected float calcQtyCertainty(final List<ProductWrapper> wrappedProducts) {
            return 0.0F;
        }


    };

    private final List<DealQualifierModel> qualList = new ArrayList<>();
    private final DealWrapper testWrappedDeal = new DealWrapperTestImpl();
    private final List<ProductWrapper> testWrappedProducts = new ArrayList<>();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        BDDMockito.given(dealModel.getQualifierList()).willReturn(qualList);
    }

    private long sumProductMarkdowns() {
        long retVal = 0;
        for (final ProductWrapper wrappedProduct : testWrappedProducts) {
            retVal += wrappedProduct.getDealMarkdown();
        }
        return retVal;
    }

    private long sumTotalPrice() {
        long retVal = 0;
        for (final ProductWrapper wrappedProduct : testWrappedProducts) {
            retVal += wrappedProduct.getAdjustedUnitSellPrice();
        }
        return retVal;
    }

    //**************************************************************************
    protected final int itemsWithValue(final int amt)
    {
        int ret = 0;
        for (final ProductWrapper wrappedProduct : testWrappedProducts) {
            if (wrappedProduct.getAdjustedUnitSellPrice() == amt) {
                ret++;
            }
        }

        return ret;
    }


    //**************************************************************************
    protected final int itemsWithMarkdown(final int amt)
    {
        int ret = 0;
        for (final ProductWrapper wrappedProduct : testWrappedProducts) {
            if (wrappedProduct.getDealMarkdown() == amt) {
                ret++;
            }
        }

        return ret;
    }

    //##################################################################################
    // Spend $20 and get 10% off an unlimited number of items
    //##################################################################################

    @SuppressWarnings("boxing")
    private void setupDealSpend20Get10PctUnlimited() {
        BDDMockito.given(q1.getMinAmt()).willReturn(Double.valueOf(20.00));
        qualList.add(q1);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(10.00));
        ((DealWrapperTestImpl)testWrappedDeal).setDealType(DealType.PERCENT_OFF_EACH);
    }

    @Test
    public void testApplyDealPctOffUnlimitedRewardEnoughQualifingItemsButNotEnoughForAReward()
            throws Exception {
        setupDealSpend20Get10PctUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);

        testWrappedProducts.add(pw1);
        testWrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(testWrappedDeal, testWrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
    }

    @Test
    public void testApplyDealPctOffUnlimitedRewardEnoughItemsFor1RewardItem() throws Exception {
        setupDealSpend20Get10PctUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 1500, true);

        testWrappedProducts.add(pw1);
        testWrappedProducts.add(pw2);
        testWrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(testWrappedDeal, testWrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(150, pw3.getDealMarkdown());
        Assert.assertEquals(4350, sumTotalPrice());
        Assert.assertEquals(150, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealPctOffUnlimitedRewardEnoughItemsForMultipleRewardItems()
            throws Exception {
        setupDealSpend20Get10PctUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 1500, true);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p2, null, 1500, true);

        testWrappedProducts.add(pw1);
        testWrappedProducts.add(pw2);
        testWrappedProducts.add(pw3);
        testWrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(testWrappedDeal, testWrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(150, pw3.getDealMarkdown());
        Assert.assertEquals(150, pw4.getDealMarkdown());
        Assert.assertEquals(5700, sumTotalPrice());
        Assert.assertEquals(300, sumProductMarkdowns());
    }

    //##################################################################################
    // Spend $20 and get 10% off 1 another item
    //##################################################################################

    @SuppressWarnings("boxing")
    private void setupDealSpend20Get10PctAnother() {
        BDDMockito.given(q1.getMinAmt()).willReturn(Double.valueOf(20.00));
        qualList.add(q1);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(10.00));
        BDDMockito.given(deal.getRewardMaxQtyAsPrimitive()).willReturn(Integer.valueOf(1));
        ((DealWrapperTestImpl)testWrappedDeal).setDealType(DealType.PERCENT_OFF_EACH);
    }

    @Test
    public void testApplyDealPctOffOneRewardEnoughQualifingItemsButNotEnoughForAReward()
            throws Exception {
        setupDealSpend20Get10PctAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);

        testWrappedProducts.add(pw1);
        testWrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(testWrappedDeal, testWrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
    }

    @Test
    public void testApplyDealPctOffOneRewardEnoughItemsFor1RewardItem() throws Exception {
        setupDealSpend20Get10PctAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 1500, true);

        testWrappedProducts.add(pw1);
        testWrappedProducts.add(pw2);
        testWrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(testWrappedDeal, testWrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(150, pw3.getDealMarkdown());
        Assert.assertEquals(4350, sumTotalPrice());
        Assert.assertEquals(150, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealPctOffOneRewardEnoughItemsForMultipleRewardItemsForMultipleRewardItems()
            throws Exception {
        setupDealSpend20Get10PctAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p3, null, 1500, true);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p4, null, 1500, true);

        testWrappedProducts.add(pw1);
        testWrappedProducts.add(pw2);
        testWrappedProducts.add(pw3);
        testWrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(testWrappedDeal, testWrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(1, itemsWithMarkdown(150));
        Assert.assertEquals(3, itemsWithMarkdown(0));
        Assert.assertEquals(5850, sumTotalPrice());
        Assert.assertEquals(150, sumProductMarkdowns());
    }

    //##################################################################################
    // Spend $20 and get $10.00 off 1 another item
    //##################################################################################

    @SuppressWarnings("boxing")
    private void setupDealSpend20Get10DolAnother() {
        BDDMockito.given(q1.getMinAmt()).willReturn(Double.valueOf(20.00));
        qualList.add(q1);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(10.00));
        BDDMockito.given(deal.getRewardMaxQtyAsPrimitive()).willReturn(Integer.valueOf(1));
        ((DealWrapperTestImpl)testWrappedDeal).setDealType(DealType.DOLLAR_OFF_EACH);
    }

    @Test
    public void testApplyDealDolOffOneEnoughQualifingItemsButNotEnoughForAReward()
            throws Exception {
        setupDealSpend20Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);

        testWrappedProducts.add(pw1);
        testWrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(testWrappedDeal, testWrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
    }

    @Test
    public void testApplyDealDolOffOneEnoughItemsFor1RewardItem() throws Exception {
        setupDealSpend20Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 1500, true);

        testWrappedProducts.add(pw1);
        testWrappedProducts.add(pw2);
        testWrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(testWrappedDeal, testWrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(1000, pw3.getDealMarkdown());
        Assert.assertEquals(3500, sumTotalPrice());
        Assert.assertEquals(1000, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealDolOffOneEnoughItemsForNoRewardItem() throws Exception {
        setupDealSpend20Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);

        testWrappedProducts.add(pw1);
        testWrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        final List<PromotionResult> results = deal.applyDeal(testWrappedDeal, testWrappedProducts);

        // get the results
        Assertions.assertThat(results).isNotNull().isEmpty();
    }

    @Test
    public void testApplyDealDolOffOneEnoughItemsForMultipleRewardItemsForMultipleRewardItems()
            throws Exception {
        setupDealSpend20Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p3, null, 1500, true);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p4, null, 1500, true);

        testWrappedProducts.add(pw1);
        testWrappedProducts.add(pw2);
        testWrappedProducts.add(pw3);
        testWrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(testWrappedDeal, testWrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(1, itemsWithMarkdown(1000));
        Assert.assertEquals(3, itemsWithMarkdown(0));
        Assert.assertEquals(5000, sumTotalPrice());
        Assert.assertEquals(1000, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealDolOffOneDontGoNegative()
            throws Exception {
        setupDealSpend20Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 2500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, null, 800, true);

        testWrappedProducts.add(pw1);
        testWrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(testWrappedDeal, testWrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(800, pw2.getDealMarkdown());
        Assert.assertEquals(2500, sumTotalPrice());
        Assert.assertEquals(800, sumProductMarkdowns());
    }

    //##################################################################################
    // Spend $20 items and get $10.00 off 1 unlimited items
    //##################################################################################

    @SuppressWarnings("boxing")
    private void setupDealBuy2Get1DolUnlimited() {
        BDDMockito.given(q1.getMinAmt()).willReturn(Double.valueOf(20.00));
        qualList.add(q1);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(10.00));
        ((DealWrapperTestImpl)testWrappedDeal).setDealType(DealType.DOLLAR_OFF_EACH);
    }

    @Test
    public void testApplyDealDolOffUnlimitedEnoughQualifingItemsButNotEnoughForAReward()
            throws Exception {
        setupDealBuy2Get1DolUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);

        testWrappedProducts.add(pw1);
        testWrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(testWrappedDeal, testWrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
    }

    @Test
    public void testApplyDealDolOffUnlimitedEnoughItemsFor1RewardItem() throws Exception {
        setupDealBuy2Get1DolUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 1500, true);

        testWrappedProducts.add(pw1);
        testWrappedProducts.add(pw2);
        testWrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(testWrappedDeal, testWrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(1, itemsWithMarkdown(1000));
        Assert.assertEquals(2, itemsWithMarkdown(0));
        Assert.assertEquals(3500, sumTotalPrice());
        Assert.assertEquals(1000, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealDolOffUnlimitedEnoughItemsForMultipleRewardItems()
            throws Exception {
        setupDealBuy2Get1DolUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 1500, true);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p2, null, 1500, true);

        testWrappedProducts.add(pw1);
        testWrappedProducts.add(pw2);
        testWrappedProducts.add(pw3);
        testWrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(testWrappedDeal, testWrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(2, itemsWithMarkdown(1000));
        Assert.assertEquals(2, itemsWithMarkdown(0));
        Assert.assertEquals(4000, sumTotalPrice());
        Assert.assertEquals(2000, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealDolOffUnlimitedDontGoNegative()
            throws Exception {
        setupDealBuy2Get1DolUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 80, true);

        testWrappedProducts.add(pw1);
        testWrappedProducts.add(pw2);
        testWrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(testWrappedDeal, testWrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(80, pw3.getDealMarkdown());
        Assert.assertEquals(3000, sumTotalPrice());
        Assert.assertEquals(80, sumProductMarkdowns());
    }

    //##################################################################################
    // Spend $20 and get one for a $10.00
    //##################################################################################

    @SuppressWarnings("boxing")
    private void setupDealSpend20Get1For10Dol() {
        BDDMockito.given(q1.getMinAmt()).willReturn(Double.valueOf(20.00));
        qualList.add(q1);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(10.00));
        BDDMockito.given(deal.getRewardMaxQtyAsPrimitive()).willReturn(Integer.valueOf(1));
        ((DealWrapperTestImpl)testWrappedDeal).setDealType(DealType.FIXED_DOLLAR_EACH);
    }

    @Test
    public void testApplyDealFixedDolOneEnoughQualifingItemsButNotEnoughForAReward()
            throws Exception {
        setupDealSpend20Get1For10Dol();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);

        testWrappedProducts.add(pw1);
        testWrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(testWrappedDeal, testWrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
    }

    @Test
    public void testApplyDealFixedDolOneEnoughItemsFor1RewardItem() throws Exception {
        setupDealSpend20Get1For10Dol();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 1500, true);

        testWrappedProducts.add(pw1);
        testWrappedProducts.add(pw2);
        testWrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(testWrappedDeal, testWrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(500, pw3.getDealMarkdown());
        Assert.assertEquals(4000, sumTotalPrice());
        Assert.assertEquals(500, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealFixedDolOneEnoughItemsForMultipleRewardItems()
            throws Exception {
        setupDealSpend20Get1For10Dol();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500, false);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, null, 1500, true);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p2, null, 1500, true);

        testWrappedProducts.add(pw1);
        testWrappedProducts.add(pw2);
        testWrappedProducts.add(pw3);
        testWrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(testWrappedDeal, testWrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(1, itemsWithMarkdown(500));
        Assert.assertEquals(3, itemsWithMarkdown(0));
        Assert.assertEquals(5500, sumTotalPrice());
        Assert.assertEquals(500, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealFixedDolOneRewardDontGoNegative()
            throws Exception {
        setupDealSpend20Get1For10Dol();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 2500, false);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, null, 80, true);

        testWrappedProducts.add(pw1);
        testWrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(testWrappedDeal, testWrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(2580, sumTotalPrice());
        Assert.assertEquals(0, sumProductMarkdowns());
    }
}
