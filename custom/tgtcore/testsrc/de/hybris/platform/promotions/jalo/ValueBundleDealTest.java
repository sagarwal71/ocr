package de.hybris.platform.promotions.jalo;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.promotions.model.QuantityBreakDealModel;
import de.hybris.platform.promotions.result.PromotionEvaluationContext;
import de.hybris.platform.promotions.result.PromotionOrderView;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.deals.wrappers.DealWrapper;
import au.com.target.tgtcore.deals.wrappers.ProductWrapper;
import au.com.target.tgtcore.enums.DealItemTypeEnum;
import au.com.target.tgtcore.jalo.AbstractTargetVariantProduct;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("deprecation")
public class ValueBundleDealTest {
    private class ProductWrapperTestImpl implements ProductWrapper {
        private boolean inDeal = false;

        private final AbstractTargetVariantProduct p;
        private final long unitPrice;
        private long markdown;
        private boolean rewardpartaillyConsumed = false;

        public ProductWrapperTestImpl(final AbstractTargetVariantProduct p, final long unitPrice) {
            this.p = p;
            this.unitPrice = unitPrice;
        }

        @Override
        public DealQualifierModel getQualifierModel() {
            return null;
        }

        @Override
        public long getUnitSellPrice() {
            return unitPrice;
        }

        @Override
        public long getAdjustedUnitSellPrice() {
            return unitPrice - markdown;
        }

        @Override
        public void setDealApplied(final int inst, final DealItemTypeEnum dealType, final long saveAmt) {
            markdown = saveAmt;
            inDeal = true;
        }

        @Override
        public boolean isInDeal() {
            return inDeal;
        }

        @Override
        public long getDealMarkdown() {
            return markdown;
        }

        @Override
        public Product getProduct() {
            return p;
        }

        @Override
        public DealItemTypeEnum getType() {
            return null;
        }

        @Override
        public int getInstance() {
            return 0;
        }

        @Override
        public void clearDeal() {
            // these are not the droids you are looking for
        }

        @Override
        public void setDealRewardsPartiallyConsumed() {
            rewardpartaillyConsumed = true;

        }

        @Override
        public boolean getDealRewardsPartiallyConsumed() {

            return rewardpartaillyConsumed;
        }


        @Override
        public void clearDealRewardsPartiallyConsumed() {
            rewardpartaillyConsumed = false;
        }

        /* (non-Javadoc)
         * @see au.com.target.tgtcore.deals.wrappers.ProductWrapper#isRewardProduct()
         */
        @Override
        public boolean isRewardProduct() {
            // YTODO Auto-generated method stub
            return false;
        }

    }

    private class DealWrapperTestImpl implements DealWrapper {
        @Override
        public AbstractDealModel getDealModel() {
            return dealModel;
        }

        @Override
        public List<DealQualifierModel> getQualifierModelList() {
            return qualList;
        }

        @Override
        public SessionContext getCtx() {
            return null;
        }

        @Override
        public PromotionEvaluationContext getPromoContext() {
            return null;
        }

        @Override
        public PromotionOrderView getOrderView() {
            return null;
        }

        @Override
        public DealType getDealType() {
            return null;
        }

    }


    @Mock
    private QuantityBreakDealModel dealModel;

    @Mock
    private DealQualifierModel q1;
    @Mock
    private DealQualifierModel q2;
    @Mock
    private DealQualifierModel q3;

    @Mock
    private AbstractTargetVariantProduct p1;
    @Mock
    private AbstractTargetVariantProduct p2;
    @Mock
    private AbstractTargetVariantProduct p3;

    private final List<DealBreakPoint> breakPoints = new ArrayList<>();

    private boolean dealApplied = false;
    private final QuantityBreakDeal deal = new QuantityBreakDeal() {

        @Override
        public List<DealBreakPoint> getBreakPoints() {
            return breakPoints;
        }

        @Override
        protected java.util.List<PromotionResult> buildFiredResultList(
                final java.util.List<ProductWrapper> productWrapperList,
                final DealWrapper dealWrapper, final int instanceCount) {
            dealApplied = true;
            return null;
        }

        @Override
        protected java.util.List<PromotionResult> buildCouldHaveFiredResultList(
                final java.util.List<ProductWrapper> productWrapperList, final DealWrapper dealWrapper,
                final float certainty) {
            return new ArrayList<>();
        }
    };

    private final List<DealQualifierModel> qualList = new ArrayList<>();
    private final DealWrapper wrappedDeal = new DealWrapperTestImpl();
    private final List<ProductWrapper> wrappedProducts = new ArrayList<>();

    @SuppressWarnings("boxing")
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        final DealBreakPoint bp1 = Mockito.mock(DealBreakPoint.class);
        BDDMockito.given(bp1.getQtyAsPrimitive()).willReturn(Integer.valueOf(2));
        BDDMockito.given(bp1.getUnitPriceAsPrimitive()).willReturn(Double.valueOf(14.00));
        breakPoints.add(bp1);

        final DealBreakPoint bp2 = Mockito.mock(DealBreakPoint.class);
        BDDMockito.given(bp2.getQtyAsPrimitive()).willReturn(Integer.valueOf(3));
        BDDMockito.given(bp2.getUnitPriceAsPrimitive()).willReturn(Double.valueOf(13.00));
        breakPoints.add(bp2);

        final DealBreakPoint bp3 = Mockito.mock(DealBreakPoint.class);
        BDDMockito.given(bp3.getQtyAsPrimitive()).willReturn(Integer.valueOf(4));
        BDDMockito.given(bp3.getUnitPriceAsPrimitive()).willReturn(Double.valueOf(12.00));
        breakPoints.add(bp3);

        final DealBreakPoint bp4 = Mockito.mock(DealBreakPoint.class);
        BDDMockito.given(bp4.getQtyAsPrimitive()).willReturn(Integer.valueOf(5));
        BDDMockito.given(bp4.getUnitPriceAsPrimitive()).willReturn(Double.valueOf(11.00));
        breakPoints.add(bp4);
    }


    private long sumProductMarkdowns() {
        long retVal = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            retVal += wrappedProduct.getDealMarkdown();
        }
        return retVal;
    }

    private long sumTotalPrice() {
        long retVal = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            retVal += wrappedProduct.getAdjustedUnitSellPrice();
        }
        return retVal;
    }

    //**************************************************************************
    protected final int itemsWithValue(final int amt)
    {
        int ret = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            if (wrappedProduct.getAdjustedUnitSellPrice() == amt) {
                ret++;
            }
        }

        return ret;
    }


    //**************************************************************************
    protected final int itemsWithMarkdown(final int amt)
    {
        int ret = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            if (wrappedProduct.getDealMarkdown() == amt) {
                ret++;
            }
        }

        return ret;
    }

    @Test
    public void testApplyDealNotEnoughItems() throws Exception {

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);

        wrappedProducts.add(pw1);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertFalse(dealApplied);
    }

    @Test
    public void testApplyDealEnoughItemsForFirstTeir() throws Exception {
        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, 1500);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(100, pw1.getDealMarkdown());
        Assert.assertEquals(100, pw2.getDealMarkdown());
        Assert.assertEquals(2800, sumTotalPrice());
        Assert.assertEquals(200, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealEnoughItemsForSubsequentTeir() throws Exception {
        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p1, 1500);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(200, pw1.getDealMarkdown());
        Assert.assertEquals(200, pw2.getDealMarkdown());
        Assert.assertEquals(200, pw3.getDealMarkdown());
        Assert.assertEquals(3900, sumTotalPrice());
        Assert.assertEquals(600, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealBasePriceLessThanDealPrice() throws Exception {
        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 800);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, 800);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p1, 800);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p1, 800);
        final ProductWrapper pw5 = new ProductWrapperTestImpl(p1, 800);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);
        wrappedProducts.add(pw5);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(0, pw3.getDealMarkdown());
        Assert.assertEquals(0, pw4.getDealMarkdown());
        Assert.assertEquals(0, pw5.getDealMarkdown());
        Assert.assertEquals(4000, sumTotalPrice());
        Assert.assertEquals(0, sumProductMarkdowns());
    }


    @Test
    public void testApplyDealBasePriceMixed() throws Exception {
        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, 800);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p1, 1100);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p1, 1200);
        final ProductWrapper pw5 = new ProductWrapperTestImpl(p1, 1450);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);
        wrappedProducts.add(pw5);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(400, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(0, pw3.getDealMarkdown());
        Assert.assertEquals(100, pw4.getDealMarkdown());
        Assert.assertEquals(350, pw5.getDealMarkdown());
        Assert.assertEquals(5200, sumTotalPrice());
    }

}
