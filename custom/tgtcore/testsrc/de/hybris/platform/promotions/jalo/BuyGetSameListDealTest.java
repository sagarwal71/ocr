package de.hybris.platform.promotions.jalo;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.BuyGetSameListDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.promotions.result.PromotionEvaluationContext;
import de.hybris.platform.promotions.result.PromotionOrderView;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.deals.wrappers.DealWrapper;
import au.com.target.tgtcore.deals.wrappers.DealWrapper.DealType;
import au.com.target.tgtcore.deals.wrappers.ProductWrapper;
import au.com.target.tgtcore.enums.DealItemTypeEnum;
import au.com.target.tgtcore.enums.DealRewardConsumeOrderEnum;
import au.com.target.tgtcore.jalo.AbstractTargetVariantProduct;



@UnitTest
@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("deprecation")
public class BuyGetSameListDealTest {
    private class ProductWrapperTestImpl implements ProductWrapper {
        private boolean inDeal = false;

        private final AbstractTargetVariantProduct p;
        private final long unitPrice;
        private long markdown;
        private boolean rewardpartaillyConsumed = false;

        public ProductWrapperTestImpl(final AbstractTargetVariantProduct p, final long unitPrice) {
            this.p = p;
            this.unitPrice = unitPrice;
        }

        @Override
        public DealQualifierModel getQualifierModel() {
            return q1;
        }

        @Override
        public long getUnitSellPrice() {
            return unitPrice;
        }

        @Override
        public long getAdjustedUnitSellPrice() {
            return unitPrice - markdown;
        }

        @Override
        public void setDealApplied(final int inst, final DealItemTypeEnum dealType, final long saveAmt) {
            markdown = saveAmt;
            inDeal = true;
        }

        @Override
        public boolean isInDeal() {
            return inDeal;
        }

        @Override
        public long getDealMarkdown() {
            return markdown;
        }

        @Override
        public Product getProduct() {
            return p;
        }

        @Override
        public DealItemTypeEnum getType() {
            return null;
        }

        @Override
        public int getInstance() {
            return 0;
        }

        @Override
        public void clearDeal() {
            // these are not the droids you are looking for
        }

        @Override
        public boolean isRewardProduct() {
            return true;
        }

        @Override
        public void setDealRewardsPartiallyConsumed() {
            rewardpartaillyConsumed = true;

        }

        @Override
        public boolean getDealRewardsPartiallyConsumed() {

            return rewardpartaillyConsumed;
        }


        @Override
        public void clearDealRewardsPartiallyConsumed() {
            rewardpartaillyConsumed = false;
        }

    }

    private class DealWrapperTestImpl implements DealWrapper {
        private DealType dealType;

        @Override
        public AbstractDealModel getDealModel() {
            return dealModel;
        }

        @Override
        public List<DealQualifierModel> getQualifierModelList() {
            return qualList;
        }

        @Override
        public SessionContext getCtx() {
            return null;
        }

        @Override
        public PromotionEvaluationContext getPromoContext() {
            return null;
        }

        @Override
        public PromotionOrderView getOrderView() {
            return null;
        }

        @Override
        public DealType getDealType() {
            return dealType;
        }

        public void setDealType(final DealType dealType) {
            this.dealType = dealType;
        }
    }


    @Mock
    private BuyGetSameListDealModel dealModel;

    @Mock
    private DealQualifierModel q1;

    @Mock
    private AbstractTargetVariantProduct p1;
    @Mock
    private AbstractTargetVariantProduct p2;
    @Mock
    private AbstractTargetVariantProduct p3;
    @Mock
    private AbstractTargetVariantProduct p4;

    private boolean dealApplied = false;

    private final BuyGetSameListDeal deal = new BuyGetSameListDeal() {

        @Override
        public int getRewardMaxQtyAsPrimitive() {
            return dealModel.getRewardMaxQty().intValue();
        }

        @Override
        public double getRewardValueAsPrimitive() {
            return dealModel.getRewardValue().doubleValue();
        }

        @Override
        public double getRewardMinSaveAsPrimitive() {
            return dealModel.getRewardMinSave().doubleValue();
        }

        @Override
        protected java.util.List<PromotionResult> buildFiredResultList(
                final java.util.List<ProductWrapper> productWrapperList,
                final DealWrapper dealWrapper, final int instanceCount) {
            dealApplied = true;
            return null;
        }

        @Override
        protected java.util.List<PromotionResult> buildCouldHaveFiredResultList(
                final java.util.List<ProductWrapper> productWrapperList, final DealWrapper dealWrapper,
                final float certainty) {
            return new ArrayList<>();
        }

        @Override
        protected float calcQtyCertainty(final List<ProductWrapper> p_wrappedProducts) {
            return 0.0f;
        }


        @Override
        protected DealRewardConsumeOrderEnum getDealConsumeOrderConfigValue(
                final DealWrapper dealWrapper)
        {
            return DealRewardConsumeOrderEnum.DEFAULT;
        }
    };
    private final BuyGetSameListDeal dealLowtoHigh = new BuyGetSameListDeal() {

        @Override
        public int getRewardMaxQtyAsPrimitive() {
            return dealModel.getRewardMaxQty().intValue();
        }

        @Override
        public double getRewardValueAsPrimitive() {
            return dealModel.getRewardValue().doubleValue();
        }

        @Override
        public double getRewardMinSaveAsPrimitive() {
            return dealModel.getRewardMinSave().doubleValue();
        }

        @Override
        protected java.util.List<PromotionResult> buildFiredResultList(
                final java.util.List<ProductWrapper> productWrapperList,
                final DealWrapper dealWrapper, final int instanceCount) {
            dealApplied = true;
            return null;
        }

        @Override
        protected java.util.List<PromotionResult> buildCouldHaveFiredResultList(
                final java.util.List<ProductWrapper> productWrapperList, final DealWrapper dealWrapper,
                final float certainty) {
            return new ArrayList<>();
        }

        @Override
        protected float calcQtyCertainty(final List<ProductWrapper> p_wrappedProducts) {
            return 0.0f;
        }


        @Override
        protected DealRewardConsumeOrderEnum getDealConsumeOrderConfigValue(
                final DealWrapper dealWrapper)
        {
            return DealRewardConsumeOrderEnum.PRICELOWTOHIGH;
        }
    };
    private final List<DealQualifierModel> qualList = new ArrayList<>();
    private final DealWrapper wrappedDeal = new DealWrapperTestImpl();
    private final List<ProductWrapper> wrappedProducts = new ArrayList<>();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        BDDMockito.given(dealModel.getQualifierList()).willReturn(qualList);
    }

    private long sumProductMarkdowns() {
        long retVal = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            retVal += wrappedProduct.getDealMarkdown();
        }
        return retVal;
    }

    private long sumTotalPrice() {
        long retVal = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            retVal += wrappedProduct.getAdjustedUnitSellPrice();
        }
        return retVal;
    }

    //**************************************************************************
    protected final int itemsWithValue(final int amt)
    {
        int ret = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            if (wrappedProduct.getAdjustedUnitSellPrice() == amt) {
                ret++;
            }
        }

        return ret;
    }


    //**************************************************************************
    protected final int itemsWithMarkdown(final int amt)
    {
        int ret = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            if (wrappedProduct.getDealMarkdown() == amt) {
                ret++;
            }
        }

        return ret;
    }

    //##################################################################################
    // Buy 2 items and get 10% off an unlimited number of items
    //##################################################################################

    @SuppressWarnings("boxing")
    private void setupDealBuy2Get10PctUnlimited() {
        BDDMockito.given(q1.getMinQty()).willReturn(Integer.valueOf(2));
        qualList.add(q1);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(10.00));
        ((DealWrapperTestImpl)wrappedDeal).setDealType(DealType.PERCENT_OFF_EACH);
    }

    @Test
    public void testApplyDealPctOffUnlimitedRewardEnoughQualifingItemsButNotEnoughForAReward() throws Exception {
        setupDealBuy2Get10PctUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, 1500);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
    }

    @Test
    public void testApplyDealPctOffUnlimitedRewardEnoughItemsFor1RewardItem() throws Exception {
        setupDealBuy2Get10PctUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, 1400);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(140, pw3.getDealMarkdown()); // cheapest item is always the reward
        Assert.assertEquals(4260, sumTotalPrice());
        Assert.assertEquals(140, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealPctOffUnlimitedRewardEnoughItemsForMultipleRewardItems()
            throws Exception {
        setupDealBuy2Get10PctUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, 1400);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p2, 1400);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(140, pw3.getDealMarkdown()); // cheapest items are always the reward
        Assert.assertEquals(140, pw4.getDealMarkdown());
        Assert.assertEquals(5520, sumTotalPrice());
        Assert.assertEquals(280, sumProductMarkdowns());
    }

    //##################################################################################
    // Buy 1 items and get 50% off 1 another item
    //##################################################################################

    @SuppressWarnings("boxing")
    private void setupDealBuy1Get50PctAnother() {
        BDDMockito.given(q1.getMinQty()).willReturn(Integer.valueOf(1));
        qualList.add(q1);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(50.00));
        BDDMockito.given(deal.getRewardMaxQtyAsPrimitive()).willReturn(Integer.valueOf(1));
        ((DealWrapperTestImpl)wrappedDeal).setDealType(DealType.PERCENT_OFF_EACH);
    }

    @Test
    public void testApplyDealPctOffOneRewardNotEnoughItems() throws Exception {
        setupDealBuy1Get50PctAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);

        wrappedProducts.add(pw1);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
    }

    @Test
    public void testApplyDealPctOffOneRewardEnoughQualifingItemsButNotEnoughForAReward()
            throws Exception {
        setupDealBuy1Get50PctAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);

        wrappedProducts.add(pw1);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
    }

    @Test
    public void testApplyDealPctOffOneRewardEnoughItemsFor1RewardItem() throws Exception {
        setupDealBuy1Get50PctAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, 1500);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(1, itemsWithMarkdown(750));
        Assert.assertEquals(1, itemsWithMarkdown(0));
        Assert.assertEquals(2250, sumTotalPrice());
        Assert.assertEquals(750, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealPctOffOneRewardEnoughItemsForMultipleRewardItemsButOnlyFiredOnce()
            throws Exception {
        setupDealBuy1Get50PctAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, 1400);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p3, 1300);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        // most expensive is the first qualifier, the second most expensive becomes the reward
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(700, pw2.getDealMarkdown());
        Assert.assertEquals(0, pw3.getDealMarkdown());
        Assert.assertEquals(3500, sumTotalPrice());
        Assert.assertEquals(700, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealPctOffOneRewardEnoughItemsForMultipleInstances() throws Exception {
        setupDealBuy1Get50PctAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, 1400);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p3, 1300);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p4, 1200);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        // most expensive is the first qualifier, the second most expensive becomes the reward (then we do it again with remaining items)
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(700, pw2.getDealMarkdown());
        Assert.assertEquals(00, pw3.getDealMarkdown());
        Assert.assertEquals(600, pw4.getDealMarkdown());
        Assert.assertEquals(4100, sumTotalPrice());
        Assert.assertEquals(1300, sumProductMarkdowns());
    }

    //##################################################################################
    // Buy 1 items and get $10.00 off 1 another item
    //##################################################################################

    @SuppressWarnings("boxing")
    private void setupDealBuy1Get10DolAnother() {
        BDDMockito.given(q1.getMinQty()).willReturn(Integer.valueOf(1));
        qualList.add(q1);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(10.00));
        BDDMockito.given(deal.getRewardMaxQtyAsPrimitive()).willReturn(Integer.valueOf(1));
        ((DealWrapperTestImpl)wrappedDeal).setDealType(DealType.DOLLAR_OFF_EACH);
    }

    @Test
    public void testApplyDealDolOffOneRewardNotEnoughItems() throws Exception {
        setupDealBuy1Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);

        wrappedProducts.add(pw1);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
    }

    @Test
    public void testApplyDealDolOffOneRewardEnoughQualifingItemsButNotEnoughForAReward()
            throws Exception {
        setupDealBuy1Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);

        wrappedProducts.add(pw1);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
    }

    @Test
    public void testApplyDealDolOffOneRewardEnoughItemsFor1RewardItem() throws Exception {
        setupDealBuy1Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, 1500);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(1, itemsWithMarkdown(1000));
        Assert.assertEquals(1, itemsWithMarkdown(0));
        Assert.assertEquals(2000, sumTotalPrice());
        Assert.assertEquals(1000, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealDolOffOneRewardEnoughItemsForMultipleRewardItemsButOnlyFiredOnce()
            throws Exception {
        setupDealBuy1Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, 1400);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p3, 1300);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        // most expensive is the first qualifier, the second most expensive becomes the reward
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(1000, pw2.getDealMarkdown());
        Assert.assertEquals(0, pw3.getDealMarkdown());
        Assert.assertEquals(3200, sumTotalPrice());
        Assert.assertEquals(1000, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealDolOffOneRewardEnoughItemsForMultipleInstances() throws Exception {
        setupDealBuy1Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, 1400);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p3, 1300);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p4, 1200);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        // most expensive is the first qualifier, the second most expensive becomes the reward (then we do it again with remaining items)
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(1000, pw2.getDealMarkdown());
        Assert.assertEquals(00, pw3.getDealMarkdown());
        Assert.assertEquals(1000, pw4.getDealMarkdown());
        Assert.assertEquals(3400, sumTotalPrice());
        Assert.assertEquals(2000, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealDolOffOneRewardDontGoNegative()
            throws Exception {
        setupDealBuy1Get10DolAnother();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 800);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, 800);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(1, itemsWithMarkdown(800));
        Assert.assertEquals(1, itemsWithMarkdown(0));
        Assert.assertEquals(800, sumTotalPrice());
        Assert.assertEquals(800, sumProductMarkdowns());
    }

    //##################################################################################
    // Buy 2 items and get $1.00 off 1 unlimited items
    //##################################################################################

    @SuppressWarnings("boxing")
    private void setupDealBuy2Get1DolUnlimited() {
        BDDMockito.given(q1.getMinQty()).willReturn(Integer.valueOf(2));
        qualList.add(q1);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(1.00));
        ((DealWrapperTestImpl)wrappedDeal).setDealType(DealType.DOLLAR_OFF_EACH);
    }

    @Test
    public void testApplyDealDolOffUnlimitedRewardNotEnoughItems() throws Exception {
        setupDealBuy2Get1DolUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);

        wrappedProducts.add(pw1);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
    }

    @Test
    public void testApplyDealDolOffUnlimitedRewardEnoughQualifingItemsButNotEnoughForAReward()
            throws Exception {
        setupDealBuy2Get1DolUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, 1500);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
    }

    @Test
    public void testApplyDealDolOffUnlimitedRewardEnoughItemsFor1RewardItem() throws Exception {
        setupDealBuy2Get1DolUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, 1400);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(100, pw3.getDealMarkdown()); // cheapest item is always the reward
        Assert.assertEquals(4300, sumTotalPrice());
        Assert.assertEquals(100, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealDolOffUnlimitedRewardEnoughItemsForMultipleRewardItems()
            throws Exception {
        setupDealBuy2Get1DolUnlimited();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, 1400);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p2, 1400);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(0, pw2.getDealMarkdown());
        Assert.assertEquals(100, pw3.getDealMarkdown()); // cheapest items are always the reward
        Assert.assertEquals(100, pw4.getDealMarkdown());
        Assert.assertEquals(5600, sumTotalPrice());
        Assert.assertEquals(200, sumProductMarkdowns());
    }

    //##################################################################################
    // Buy 1 item and get one for a $1.00
    //##################################################################################

    @SuppressWarnings("boxing")
    private void setupDealBuy1Get1For1Dol() {
        BDDMockito.given(q1.getMinQty()).willReturn(Integer.valueOf(1));
        qualList.add(q1);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(1.00));
        BDDMockito.given(deal.getRewardMaxQtyAsPrimitive()).willReturn(Integer.valueOf(1));
        ((DealWrapperTestImpl)wrappedDeal).setDealType(DealType.FIXED_DOLLAR_EACH);
    }

    @Test
    public void testApplyDealFixedDolOneRewardNotEnoughItems() throws Exception {
        setupDealBuy1Get1For1Dol();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);

        wrappedProducts.add(pw1);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
    }

    @Test
    public void testApplyDealFixedDolOneRewardEnoughQualifingItemsButNotEnoughForAReward()
            throws Exception {
        setupDealBuy1Get1For1Dol();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);

        wrappedProducts.add(pw1);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertFalse(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
    }

    @Test
    public void testApplyDealFixedDolOneRewardEnoughItemsFor1RewardItem() throws Exception {
        setupDealBuy1Get1For1Dol();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, 1400);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(1300, pw2.getDealMarkdown()); // cheapest item is always the reward
        Assert.assertEquals(1600, sumTotalPrice());
        Assert.assertEquals(1300, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealFixedDolOneRewardEnoughItemsForMultipleRewardItems()
            throws Exception {
        setupDealBuy1Get1For1Dol();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, 1400);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, 1300);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(0, pw1.getDealMarkdown());
        Assert.assertEquals(1300, pw2.getDealMarkdown()); // most expensive after the qualifier gets the reward
        Assert.assertEquals(0, pw3.getDealMarkdown());
        Assert.assertEquals(2900, sumTotalPrice());
        Assert.assertEquals(1300, sumProductMarkdowns());
    }


    @Test
    public void testCalculateDealsFromLowtoHighPercentOff()
    {
        final int get = 1;
        final int buy = 1;
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1000);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, 1200);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, 1400);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p2, 8000);
        final ProductWrapper prodArray[] = new ProductWrapper[4];
        prodArray[0] = pw1;
        prodArray[1] = pw2;
        prodArray[2] = pw3;
        prodArray[3] = pw4;
        ((DealWrapperTestImpl)wrappedDeal).setDealType(DealType.PERCENT_OFF_EACH);
        BDDMockito.given(dealModel.getRewardValue()).willReturn(new Double(50.00));
        final int dealCount = dealLowtoHigh.calculateDealsLowtoHigh(buy, get, prodArray, wrappedDeal);
        Assert.assertEquals(2, dealCount);
        Assert.assertEquals(pw1.getDealMarkdown(), 500);
        Assert.assertEquals(pw2.getDealMarkdown(), 600);
        Assert.assertEquals(pw4.getDealMarkdown(), 0);
        Assert.assertEquals(pw3.getDealMarkdown(), 0);


    }

    @Test
    public void testCalculateDealsFromLowtoHighDollarOff()
    {
        final int get = 1;
        final int buy = 1;
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1000);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, 1200);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, 1400);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p2, 8000);
        final ProductWrapper prodArray[] = new ProductWrapper[4];
        prodArray[0] = pw1;
        prodArray[1] = pw2;
        prodArray[2] = pw3;
        prodArray[3] = pw4;
        ((DealWrapperTestImpl)wrappedDeal).setDealType(DealType.DOLLAR_OFF_EACH);
        BDDMockito.given(dealModel.getRewardValue()).willReturn(new Double(5.00));
        final int dealCount = dealLowtoHigh.calculateDealsLowtoHigh(buy, get, prodArray, wrappedDeal);
        Assert.assertEquals(2, dealCount);

        Assert.assertEquals(pw1.getDealMarkdown(), 500);
        Assert.assertEquals(pw2.getDealMarkdown(), 500);


    }

    @Test
    public void testCalculateDealsFromLowtoHighWithTwoMinQty()
    {
        final int get = 1;
        final int buy = 2;
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 30000);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, 35000);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, 50000);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p2, 56000);
        final ProductWrapper prodArray[] = new ProductWrapper[4];
        prodArray[0] = pw1;
        prodArray[1] = pw2;
        prodArray[2] = pw3;
        prodArray[3] = pw4;
        ((DealWrapperTestImpl)wrappedDeal).setDealType(DealType.DOLLAR_OFF_EACH);
        BDDMockito.given(dealModel.getRewardValue()).willReturn(new Double(5.00));
        final int dealCount = dealLowtoHigh.calculateDealsLowtoHigh(buy, get, prodArray, wrappedDeal);
        Assert.assertEquals(1, dealCount);
        Assert.assertEquals(pw1.getDealMarkdown(), 500);
        Assert.assertEquals(pw1.getAdjustedUnitSellPrice(), 29500);
        Assert.assertEquals(pw2.getDealMarkdown(), 0);


    }

    @Test
    public void testCalculateDealsWithOneBuyandTwoGet()
    {
        final int get = 2;
        final int buy = 1;
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1000);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, 1200);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, 1400);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p2, 8000);
        final ProductWrapper prodArray[] = new ProductWrapper[4];
        prodArray[0] = pw1;
        prodArray[1] = pw2;
        prodArray[2] = pw3;
        prodArray[3] = pw4;
        ((DealWrapperTestImpl)wrappedDeal).setDealType(DealType.DOLLAR_OFF_EACH);
        BDDMockito.given(dealModel.getRewardValue()).willReturn(new Double(5.00));
        final int dealCount = dealLowtoHigh.calculateDealsLowtoHigh(buy, get, prodArray, wrappedDeal);
        Assert.assertEquals(1, dealCount);
        Assert.assertEquals(pw1.getDealMarkdown(), 500);
        Assert.assertEquals(pw2.getDealMarkdown(), 500);


    }

    @Test
    public void testCalculateDealsLowtoHigh()
    {

        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1000);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, 1200);
        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        BDDMockito.given(q1.getMinQty()).willReturn(Integer.valueOf(1));
        qualList.add(q1);
        final BuyGetSameListDeal spy = Mockito.spy(dealLowtoHigh);
        spy.calculateDeals(wrappedProducts, wrappedDeal);
        Mockito.verify(spy, Mockito.times(1)).calculateDealsLowtoHigh(Mockito.anyInt(), Mockito.anyInt(),
                Mockito.any(ProductWrapper[].class),
                Mockito.any(DealWrapper.class));

    }

    @Test
    public void testCalculateDealsDefault()
    {

        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, 1000);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, 1200);
        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        BDDMockito.given(q1.getMinQty()).willReturn(Integer.valueOf(1));
        qualList.add(q1);
        ((DealWrapperTestImpl)wrappedDeal).setDealType(DealType.DOLLAR_OFF_EACH);
        final BuyGetSameListDeal spy = Mockito.spy(deal);
        spy.calculateDeals(wrappedProducts, wrappedDeal);
        Mockito.verify(spy, Mockito.times(1)).calculateDealsDefault(Mockito.anyInt(), Mockito.anyInt(),
                Mockito.any(ProductWrapper[].class),
                Mockito.any(DealWrapper.class));

    }
}
