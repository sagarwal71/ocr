package de.hybris.platform.promotions.jalo;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.promotions.model.ValueBundleDealModel;
import de.hybris.platform.promotions.result.PromotionEvaluationContext;
import de.hybris.platform.promotions.result.PromotionOrderView;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.deals.wrappers.DealWrapper;
import au.com.target.tgtcore.deals.wrappers.ProductWrapper;
import au.com.target.tgtcore.enums.DealItemTypeEnum;
import au.com.target.tgtcore.jalo.AbstractTargetVariantProduct;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("deprecation")
public class QuantityBreakDealTest {
    private static final int FUZZY_LEWAY = 2;

    private class ProductWrapperTestImpl implements ProductWrapper {
        private boolean inDeal = false;

        private final AbstractTargetVariantProduct p;
        private final DealQualifierModel q;
        private final long unitPrice;
        private long markdown;
        private boolean rewardpartaillyConsumed = false;

        public ProductWrapperTestImpl(final AbstractTargetVariantProduct p, final DealQualifierModel q,
                final long unitPrice) {
            this.p = p;
            this.q = q;
            this.unitPrice = unitPrice;
        }

        @Override
        public DealQualifierModel getQualifierModel() {
            return q;
        }

        @Override
        public long getUnitSellPrice() {
            return unitPrice;
        }

        @Override
        public long getAdjustedUnitSellPrice() {
            return unitPrice - markdown;
        }

        @Override
        public void setDealApplied(final int inst, final DealItemTypeEnum dealType, final long saveAmt) {
            markdown = saveAmt;
            inDeal = true;
        }

        @Override
        public boolean isInDeal() {
            return inDeal;
        }

        @Override
        public long getDealMarkdown() {
            return markdown;
        }

        @Override
        public Product getProduct() {
            return p;
        }

        @Override
        public DealItemTypeEnum getType() {
            return null;
        }

        @Override
        public int getInstance() {
            return 0;
        }

        @Override
        public void clearDeal() {
            inDeal = false;
        }

        @Override
        public boolean isRewardProduct() {
            return false;
        }


        @Override
        public void setDealRewardsPartiallyConsumed() {
            rewardpartaillyConsumed = true;

        }


        @Override
        public boolean getDealRewardsPartiallyConsumed() {

            return rewardpartaillyConsumed;
        }


        @Override
        public void clearDealRewardsPartiallyConsumed() {
            rewardpartaillyConsumed = false;
        }
    }

    private class DealWrapperTestImpl implements DealWrapper {
        @Override
        public AbstractDealModel getDealModel() {
            return dealModel;
        }

        @Override
        public List<DealQualifierModel> getQualifierModelList() {
            return qualList;
        }

        @Override
        public SessionContext getCtx() {
            return null;
        }

        @Override
        public PromotionEvaluationContext getPromoContext() {
            return null;
        }

        @Override
        public PromotionOrderView getOrderView() {
            return null;
        }

        @Override
        public DealType getDealType() {
            return null;
        }

    }


    @Mock
    private ValueBundleDealModel dealModel;

    @Mock
    private DealQualifierModel q1;
    @Mock
    private DealQualifierModel q2;
    @Mock
    private DealQualifierModel q3;

    @Mock
    private AbstractTargetVariantProduct p1;
    @Mock
    private AbstractTargetVariantProduct p2;
    @Mock
    private AbstractTargetVariantProduct p3;

    private boolean dealApplied = false;
    private final ValueBundleDeal deal = new ValueBundleDeal() {

        @Override
        public int getRewardMaxQtyAsPrimitive() {
            return dealModel.getRewardMaxQty().intValue();
        }

        @Override
        public double getRewardValueAsPrimitive() {
            return dealModel.getRewardValue().doubleValue();
        }

        @Override
        public double getRewardMinSaveAsPrimitive() {
            return dealModel.getRewardMinSave().doubleValue();
        }

        @Override
        protected java.util.List<PromotionResult> buildFiredResultList(
                final java.util.List<ProductWrapper> productWrapperList,
                final DealWrapper dealWrapper, final int instanceCount) {
            dealApplied = true;
            return null;
        }
    };

    private final List<DealQualifierModel> qualList = new ArrayList<>();
    private final DealWrapper wrappedDeal = new DealWrapperTestImpl();
    private final List<ProductWrapper> wrappedProducts = new ArrayList<>();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        BDDMockito.given(dealModel.getQualifierList()).willReturn(qualList);
    }

    @SuppressWarnings("boxing")
    private void setupDeal3ItemsFor20Dollars() {
        // set up the deal, bundle for 20 dollars, buy p1, p2 and two p3
        BDDMockito.given(q1.getMinQty()).willReturn(Integer.valueOf(1));
        qualList.add(q1);

        BDDMockito.given(q2.getMinQty()).willReturn(Integer.valueOf(1));
        qualList.add(q2);

        BDDMockito.given(q3.getMinQty()).willReturn(Integer.valueOf(2));
        qualList.add(q3);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(20.00));
    }

    @SuppressWarnings("boxing")
    private void setupDeal3ItemsSave2Cents() {
        // set up the deal, bundle buy 3 items save 2 cents
        BDDMockito.given(q1.getMinQty()).willReturn(Integer.valueOf(3));
        qualList.add(q1);

        BDDMockito.given(deal.getRewardValueAsPrimitive()).willReturn(Double.valueOf(999999.99));
        BDDMockito.given(deal.getRewardMinSaveAsPrimitive()).willReturn(Double.valueOf(0.02));
    }

    //**************************************************************************
    // like assertEqual, but just needs to be within a couple of cents
    private void assertFuzzyEquals(final long expected, final long actual) throws Exception
    {
        if (Math.abs(expected - actual) > FUZZY_LEWAY) {
            Assert.assertEquals(expected, actual);
        }
    }

    private long sumProductMarkdowns() {
        long retVal = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            retVal += wrappedProduct.getDealMarkdown();
        }
        return retVal;
    }

    private long sumTotalPrice() {
        long retVal = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            retVal += wrappedProduct.getAdjustedUnitSellPrice();
        }
        return retVal;
    }

    //**************************************************************************
    protected final int itemsWithValue(final int amt)
    {
        int ret = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            if (wrappedProduct.getAdjustedUnitSellPrice() == amt) {
                ret++;
            }
        }

        return ret;
    }


    //**************************************************************************
    protected final int itemsWithMarkdown(final int amt)
    {
        int ret = 0;
        for (final ProductWrapper wrappedProduct : wrappedProducts) {
            if (wrappedProduct.getDealMarkdown() == amt) {
                ret++;
            }
        }

        return ret;
    }

    @Test
    public void testApplyDealNotEnoughItems() throws Exception {
        setupDeal3ItemsFor20Dollars();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, q2, 1300);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p3, q3, 900);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);
        Assert.assertFalse(dealApplied);
    }

    @Test
    public void testApplyDealEnoughItems() throws Exception {
        setupDeal3ItemsFor20Dollars();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, q2, 1300);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p3, q3, 900);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p3, q3, 900);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        assertFuzzyEquals(848, pw1.getDealMarkdown());
        assertFuzzyEquals(735, pw2.getDealMarkdown());
        assertFuzzyEquals(508, pw3.getDealMarkdown());
        assertFuzzyEquals(508, pw4.getDealMarkdown());
        Assert.assertEquals(2000, sumTotalPrice());
        Assert.assertEquals(2600, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealMoreThanEnoughItems() throws Exception {
        setupDeal3ItemsFor20Dollars();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, q2, 1300);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p2, q2, 1300);
        final ProductWrapper pw5 = new ProductWrapperTestImpl(p3, q3, 900);
        final ProductWrapper pw6 = new ProductWrapperTestImpl(p3, q3, 900);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);
        wrappedProducts.add(pw5);
        wrappedProducts.add(pw6);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(4800, sumTotalPrice());
        Assert.assertEquals(2600, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealMetTwice() throws Exception {
        setupDeal3ItemsFor20Dollars();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p1, q1, 1500);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, q2, 1300);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p2, q2, 1300);
        final ProductWrapper pw5 = new ProductWrapperTestImpl(p3, q3, 900);
        final ProductWrapper pw6 = new ProductWrapperTestImpl(p3, q3, 900);
        final ProductWrapper pw7 = new ProductWrapperTestImpl(p3, q3, 900);
        final ProductWrapper pw8 = new ProductWrapperTestImpl(p3, q3, 900);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);
        wrappedProducts.add(pw5);
        wrappedProducts.add(pw6);
        wrappedProducts.add(pw7);
        wrappedProducts.add(pw8);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        assertFuzzyEquals(848, pw1.getDealMarkdown());
        assertFuzzyEquals(848, pw2.getDealMarkdown());
        assertFuzzyEquals(735, pw3.getDealMarkdown());
        assertFuzzyEquals(735, pw4.getDealMarkdown());
        assertFuzzyEquals(508, pw5.getDealMarkdown());
        assertFuzzyEquals(508, pw6.getDealMarkdown());
        assertFuzzyEquals(508, pw7.getDealMarkdown());
        assertFuzzyEquals(508, pw8.getDealMarkdown());
        Assert.assertEquals(4000, sumTotalPrice());
        Assert.assertEquals(5200, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealHighestValueItemUsed() throws Exception {
        setupDeal3ItemsFor20Dollars();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 1500);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, q2, 1300);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p3, q3, 800);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p3, q3, 600);
        final ProductWrapper pw5 = new ProductWrapperTestImpl(p3, q3, 700);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);
        wrappedProducts.add(pw5);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        assertFuzzyEquals(802, pw1.getDealMarkdown());
        assertFuzzyEquals(695, pw2.getDealMarkdown());
        assertFuzzyEquals(428, pw3.getDealMarkdown());
        assertFuzzyEquals(0, pw4.getDealMarkdown());
        assertFuzzyEquals(375, pw5.getDealMarkdown());
        Assert.assertEquals(2600, sumTotalPrice());
        Assert.assertEquals(2300, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealNoSavingsToBeMade() throws Exception {
        setupDeal3ItemsFor20Dollars();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p1, q1, 100);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, q2, 200);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p3, q3, 300);
        final ProductWrapper pw4 = new ProductWrapperTestImpl(p3, q3, 400);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);
        wrappedProducts.add(pw4);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        assertFuzzyEquals(0, pw1.getDealMarkdown());
        assertFuzzyEquals(0, pw2.getDealMarkdown());
        assertFuzzyEquals(0, pw3.getDealMarkdown());
        assertFuzzyEquals(0, pw4.getDealMarkdown());
        Assert.assertEquals(1000, sumTotalPrice());
        Assert.assertEquals(0, sumProductMarkdowns());
    }

    @Test
    public void testApplyDealRoundingOfCents() throws Exception {
        setupDeal3ItemsSave2Cents();

        // set up the products
        final ProductWrapper pw1 = new ProductWrapperTestImpl(p2, q1, 900);
        final ProductWrapper pw2 = new ProductWrapperTestImpl(p2, q1, 900);
        final ProductWrapper pw3 = new ProductWrapperTestImpl(p2, q1, 900);

        wrappedProducts.add(pw1);
        wrappedProducts.add(pw2);
        wrappedProducts.add(pw3);

        // run the test
        dealApplied = false;
        deal.applyDeal(wrappedDeal, wrappedProducts);

        // get the results
        Assert.assertTrue(dealApplied);
        Assert.assertEquals(2, itemsWithValue(899));
        Assert.assertEquals(2, itemsWithMarkdown(1));
        Assert.assertEquals(1, itemsWithValue(900));
        Assert.assertEquals(1, itemsWithMarkdown(0));
        Assert.assertEquals(2698, sumTotalPrice());
        Assert.assertEquals(2, sumProductMarkdowns());
    }
}
