/**
 * 
 */
package de.hybris.platform.task.impl;

import static org.junit.Assert.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;


/**
 * @author thomasadolfsson This is based on TargetTaskServiceTest
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetTaskQueryProviderImplTest {

    private final TargetTaskServiceQueryProviderImpl targetTaskQueryProviderImpl = new TargetTaskServiceQueryProviderImpl();

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        targetTaskQueryProviderImpl.setTargetFeatureSwitchService(targetFeatureSwitchService);
    }

    @Test
    public void testGetTaskQueryWhenProcessNotToBeStalled() {

        Mockito.doReturn(Boolean.FALSE).when(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.TASK_SERVICE_STALL_FAILED_PROCESS_SAME_ORDER);

        final String getTaskQuery = targetTaskQueryProviderImpl.getTasksToExecuteQuery();

        assertThat(getTaskQuery, CoreMatchers.not(CoreMatchers.containsString("procs.p_state = :failedProcessState")));
    }

    @Test
    public void testGetTaskQueryWhenProcessToBeStalled() {

        Mockito.doReturn(Boolean.TRUE).when(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.TASK_SERVICE_STALL_FAILED_PROCESS_SAME_ORDER);

        final String getTaskQuery = targetTaskQueryProviderImpl.getTasksToExecuteQuery();

        assertThat(getTaskQuery, CoreMatchers.containsString("procs.p_state = :failedProcessState"));
    }

}
