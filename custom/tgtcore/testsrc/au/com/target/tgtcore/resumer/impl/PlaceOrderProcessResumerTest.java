/**
 * 
 */
package au.com.target.tgtcore.resumer.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.OrderService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.order.TargetCommerceCheckoutService;
import au.com.target.tgtpayment.service.PaymentsInProgressService;
import org.junit.Assert;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PlaceOrderProcessResumerTest {

    @Mock
    private TargetCommerceCheckoutService targetCommerceCheckoutService;

    @Mock
    private BaseSiteService baseSiteService;

    @Mock
    private BaseStoreService baseStoreService;

    @Mock
    private PromotionsService promotionService;

    @Mock
    private CalculationService calculationService;

    @Mock
    private FlexibleSearchService flexibleSearchService;

    @Mock
    private OrderService orderService;

    @Mock
    private ModelService modelService;

    @Mock
    private PaymentsInProgressService paymentsInProgressService;

    @Spy
    @InjectMocks
    private final PlaceOrderProcessResumer placeOrderProcessResumer = new PlaceOrderProcessResumer();

    @Mock
    private OrderModel order;

    @Mock
    private CartModel cart;

    @Mock
    private CustomerModel customer;

    @Mock
    private BaseSiteModel baseSite;

    @Mock
    private BaseStoreModel baseStore;

    @Mock
    private CommerceOrderResult orderResult;

    private CommerceCheckoutParameter checkoutParameter;

    @Before
    public void setup() {

        Mockito.when(baseSiteService.getBaseSiteForUID("target")).thenReturn(baseSite);
        Mockito.when(baseStoreService.getBaseStoreForUid("target")).thenReturn(baseStore);
    }

    @Test
    public void testResumeGivenNoExistingOrder() throws InvalidCartException {

        Mockito.doReturn(null).when(placeOrderProcessResumer).findExistingModel(cart);
        Mockito.doReturn(orderResult).when(targetCommerceCheckoutService)
                .placeOrder(Mockito.any(CommerceCheckoutParameter.class));
        Mockito.doReturn(order).when(orderResult).getOrder();

        placeOrderProcessResumer.resumeProcess(cart, null);
        Mockito.verify(targetCommerceCheckoutService).placeOrder(Mockito.any(CommerceCheckoutParameter.class));
        Mockito.verify(targetCommerceCheckoutService).afterPlaceOrder(cart, order);
        Mockito.verify(order).setSite(baseSite);
        Mockito.verify(order).setStore(baseStore);
    }

    @Test
    public void testResumeGivenExistingPlacedOrder() throws InvalidCartException {

        BDDMockito.given(order.getUser()).willReturn(customer);
        Mockito.doReturn(order).when(placeOrderProcessResumer).findExistingModel(cart);
        Mockito.when(order.getStatus()).thenReturn(OrderStatus.ON_VALIDATION);

        placeOrderProcessResumer.resumeProcess(cart, null);
        Mockito.verify(paymentsInProgressService).removeInProgressPayment(cart);
        Mockito.verifyZeroInteractions(targetCommerceCheckoutService);
        Mockito.verify(placeOrderProcessResumer, Mockito.never()).recoverFromPreviousOrder(order, cart);
    }

    @Test
    public void testResumeGivenExistingOrder() throws InvalidCartException {

        BDDMockito.given(order.getUser()).willReturn(customer);
        Mockito.doReturn(order).when(placeOrderProcessResumer).findExistingModel(cart);

        placeOrderProcessResumer.resumeProcess(cart, null);
        Mockito.verify(placeOrderProcessResumer).recoverFromPreviousOrder(order, cart);
    }

    @Test
    public void testResumeGivenPlacedOrderCartNotCalculated() throws InvalidCartException, CalculationException {

        BDDMockito.given(cart.getUser()).willReturn(customer);
        Mockito.when(cart.getCalculated()).thenReturn(Boolean.FALSE);
        Mockito.when(order.getStatus()).thenReturn(OrderStatus.ON_VALIDATION);
        Mockito.doReturn(order).when(placeOrderProcessResumer).findExistingModel(cart);

        placeOrderProcessResumer.resumeProcess(cart, null);
        Mockito.verify(calculationService).calculateTotals(cart, false);
    }

    @Test
    public void testResumeGivenPlacedOrderCartCalculated() throws InvalidCartException, CalculationException {

        BDDMockito.given(cart.getUser()).willReturn(customer);
        Mockito.when(cart.getCalculated()).thenReturn(Boolean.TRUE);
        Mockito.when(order.getStatus()).thenReturn(OrderStatus.ON_VALIDATION);
        Mockito.doReturn(order).when(placeOrderProcessResumer).findExistingModel(cart);

        placeOrderProcessResumer.resumeProcess(cart, null);
        Mockito.verifyZeroInteractions(calculationService);
    }

    @Test
    public void testCreateCheckoutParameterForCartKiosk() throws InvalidCartException {

        Mockito.doReturn("KioskStore").when(cart).getKioskStore();

        checkoutParameter = placeOrderProcessResumer
                .createCommerceCheckoutParameterForCart(cart);

        Assert.assertEquals(checkoutParameter.getSalesApplication(), SalesApplication.KIOSK);
    }

    @Test
    public void testCreateCheckoutParameterForCartEbay() throws InvalidCartException {

        Mockito.doReturn(Integer.valueOf(1101)).when(cart).getEBayOrderNumber();

        checkoutParameter = placeOrderProcessResumer
                .createCommerceCheckoutParameterForCart(cart);

        Assert.assertEquals(checkoutParameter.getSalesApplication(), SalesApplication.EBAY);
    }

    @Test
    public void testCreateCheckoutParameterForCartWeb() throws InvalidCartException {

        Mockito.doReturn(null).when(cart).getEBayOrderNumber();

        checkoutParameter = placeOrderProcessResumer
                .createCommerceCheckoutParameterForCart(cart);

        Assert.assertEquals(checkoutParameter.getSalesApplication(), SalesApplication.WEB);
    }
}
