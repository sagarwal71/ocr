package au.com.target.tgtcore.impex.jalo.translators;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.Item;

import java.util.TimeZone;

import org.junit.Test;


@UnitTest
public class TimeZoneValueTranslatorTest {

    private final TimeZoneValueTranslator timeZoneValueTranslator = new TimeZoneValueTranslator();

    @Test
    public void testExportValueWithNullObject() {
        final String result = timeZoneValueTranslator.exportValue(null);

        assertThat(result).isNull();
    }

    @Test
    public void testExportValueWithIncorrectObjectType() {
        final Object obj = mock(Object.class);
        final String result = timeZoneValueTranslator.exportValue(obj);

        assertThat(result).isNull();
    }

    @Test
    public void testExportValue() {
        final String timeZoneId = "Australia/Victoria";
        final TimeZone timeZone = TimeZone.getTimeZone(timeZoneId);

        final String result = timeZoneValueTranslator.exportValue(timeZone);

        assertThat(result).isEqualTo(timeZoneId);
    }

    @Test
    public void testImportValueWithNotAvailableTimeZoneId() {
        final Item item = mock(Item.class);
        final Object result = timeZoneValueTranslator.importValue("Moon", item);

        assertThat(timeZoneValueTranslator.wasUnresolved()).isTrue();
        assertThat(result).isNull();
    }

    @Test
    public void testImportValue() {
        final String timeZoneId = "Australia/Victoria";
        final Item item = mock(Item.class);

        final Object result = timeZoneValueTranslator.importValue(timeZoneId, item);

        assertThat(timeZoneValueTranslator.wasUnresolved()).isFalse();
        assertThat(result).isEqualTo(TimeZone.getTimeZone("Australia/Victoria"));
    }
}
