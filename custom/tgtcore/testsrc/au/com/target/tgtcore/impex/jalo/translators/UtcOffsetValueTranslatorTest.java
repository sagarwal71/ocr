package au.com.target.tgtcore.impex.jalo.translators;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.Item;

import java.util.Calendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author agarwalr
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UtcOffsetValueTranslatorTest {

    @Spy
    private final UtcOffsetValueTranslator translator = new UtcOffsetValueTranslator();

    @Test
    public void testImportValueNegativeOffset() {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(2015, 5, 1);

        doReturn(Long.valueOf(calendar.getTimeInMillis())).when(translator).getCurrentTimeMillis();
        final Item item = mock(Item.class);

        final Object result = translator.importValue("America/Paramaribo", item);

        assertThat(translator.wasUnresolved()).isFalse();
        assertThat(result).isEqualTo("-03:00");
    }

    @Test
    public void testImportValuePostiveOffset() {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(2015, 5, 1);

        doReturn(Long.valueOf(calendar.getTimeInMillis())).when(translator).getCurrentTimeMillis();
        final Item item = mock(Item.class);

        final Object result = translator.importValue("Australia/Victoria", item);

        assertThat(translator.wasUnresolved()).isFalse();
        assertThat(result).isEqualTo("+10:00");
    }

    @Test
    public void testImportValuePostiveOffsetDaylight() {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(2015, 11, 1);

        doReturn(Long.valueOf(calendar.getTimeInMillis())).when(translator).getCurrentTimeMillis();
        final Item item = mock(Item.class);

        final Object result = translator.importValue("Australia/Victoria", item);

        assertThat(translator.wasUnresolved()).isFalse();
        assertThat(result).isEqualTo("+10:00");
    }

    @Test
    public void testImportValuePostiveOffsetWithMinutes() {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(2015, 5, 1);

        doReturn(Long.valueOf(calendar.getTimeInMillis())).when(translator).getCurrentTimeMillis();
        final Item item = mock(Item.class);

        final Object result = translator.importValue("Australia/South", item);

        assertThat(translator.wasUnresolved()).isFalse();
        assertThat(result).isEqualTo("+09:30");
    }

    @Test
    public void testImportValueBrokenHill() {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(2015, 5, 1);

        doReturn(Long.valueOf(calendar.getTimeInMillis())).when(translator).getCurrentTimeMillis();
        final Item item = mock(Item.class);

        final Object result = translator.importValue("Australia/Broken_Hill", item);

        assertThat(translator.wasUnresolved()).isFalse();
        assertThat(result).isEqualTo("+09:30");
    }

    @Test
    public void testImportValueEucla() {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(2015, 5, 1);

        doReturn(Long.valueOf(calendar.getTimeInMillis())).when(translator).getCurrentTimeMillis();
        final Item item = mock(Item.class);

        final Object result = translator.importValue("Australia/Eucla", item);

        assertThat(translator.wasUnresolved()).isFalse();
        assertThat(result).isEqualTo("+08:45");
    }

    @Test
    public void testExportValue() {
        assertThat(translator.exportValue("+10:00")).isEqualTo("+10:00");
    }

}
