package au.com.target.tgtcore.postcode.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.PostCodeGroupModel;
import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgtcore.postcode.dao.TargetPostCodeDao;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPostCodeServiceImplTest {

    private static final String TEST_POST_CODE = "1234";

    @InjectMocks
    private final TargetPostCodeServiceImpl targetPostCodeServiceImpl = new TargetPostCodeServiceImpl();

    @Mock
    private TargetPostCodeDao mockTargetPostCodeDao;

    @Mock
    private PostCodeModel mockPostCodeModel;

    @Mock
    private SessionService sessionService;

    @Before
    public void setUp() {
        given(mockTargetPostCodeDao.findPostCodeByCode(TEST_POST_CODE)).willReturn(mockPostCodeModel);
    }

    @Test
    public void testGetPostCode() throws Exception {
        final PostCodeModel result = targetPostCodeServiceImpl.getPostCode(TEST_POST_CODE);
        assertThat(result).isEqualTo(mockPostCodeModel);
    }

    @Test
    public void testDoesPostCodeBelongsToGroupsNullPostCode() {
        assertThat(targetPostCodeServiceImpl.doesPostCodeBelongsToGroups(null, null)).isFalse();
    }

    @Test
    public void testDoesPostCodeBelongsToGroupsNullPostCodeGroups() {
        assertThat(targetPostCodeServiceImpl.doesPostCodeBelongsToGroups(TEST_POST_CODE, null)).isFalse();
    }

    @Test
    public void testDoesPostCodeBelongsToGroupsNullGroupsOfPostCode() {
        given(mockPostCodeModel.getPostCodeGroups()).willReturn(null);

        assertThat(targetPostCodeServiceImpl.doesPostCodeBelongsToGroups(TEST_POST_CODE, null)).isFalse();
    }

    @Test
    public void testDoesPostCodeBelongsToGroupsFalse() {
        final PostCodeGroupModel pcg1 = Mockito.mock(PostCodeGroupModel.class);
        final PostCodeGroupModel pcg2 = Mockito.mock(PostCodeGroupModel.class);
        final PostCodeGroupModel pcg3 = Mockito.mock(PostCodeGroupModel.class);

        given(mockPostCodeModel.getPostCodeGroups()).willReturn(Arrays.asList(pcg1, pcg2));

        assertThat(targetPostCodeServiceImpl.doesPostCodeBelongsToGroups(TEST_POST_CODE, Arrays.asList(pcg3)))
                .isFalse();
    }

    @Test
    public void testDoesPostCodeBelongsToGroupsTrue() {
        final PostCodeGroupModel pcg1 = Mockito.mock(PostCodeGroupModel.class);
        final PostCodeGroupModel pcg2 = Mockito.mock(PostCodeGroupModel.class);
        final PostCodeGroupModel pcg3 = Mockito.mock(PostCodeGroupModel.class);

        given(mockPostCodeModel.getPostCodeGroups()).willReturn(Arrays.asList(pcg1, pcg2));

        assertThat(targetPostCodeServiceImpl.doesPostCodeBelongsToGroups(TEST_POST_CODE, Arrays.asList(pcg2, pcg3)))
                .isTrue();
    }

    @Test
    public void testDoesPostCodeBelongsToGroupsUnknownPostCode() {
        final PostCodeGroupModel pcg1 = Mockito.mock(PostCodeGroupModel.class);
        final PostCodeGroupModel pcg2 = Mockito.mock(PostCodeGroupModel.class);
        final PostCodeGroupModel pcg3 = Mockito.mock(PostCodeGroupModel.class);

        given(mockPostCodeModel.getPostCodeGroups()).willReturn(Arrays.asList(pcg1, pcg2));
        given(mockTargetPostCodeDao.findPostCodeByCode(TEST_POST_CODE)).willReturn(null);
        assertThat(targetPostCodeServiceImpl.doesPostCodeBelongsToGroups(TEST_POST_CODE, Arrays.asList(pcg2, pcg3)))
                .isFalse();
    }

    @Test
    public void testGetPostCodeFromCart() {
        targetPostCodeServiceImpl.setSessionService(sessionService);
        final CartModel cart = Mockito.mock(CartModel.class);
        final AddressModel address = Mockito.mock(AddressModel.class);
        given(cart.getDeliveryAddress()).willReturn(address);
        given(address.getPostalcode()).willReturn("3001");
        Assert.assertEquals("3001", targetPostCodeServiceImpl.getPostalCodeFromCartOrSession(cart));
    }

    @Test
    public void testGetPostCodeFromOrder() {
        targetPostCodeServiceImpl.setSessionService(sessionService);
        final OrderModel order = Mockito.mock(OrderModel.class);
        final AddressModel address = Mockito.mock(AddressModel.class);
        given(order.getDeliveryAddress()).willReturn(address);
        given(address.getPostalcode()).willReturn("3001");
        Assert.assertEquals("3001", targetPostCodeServiceImpl.getPostalCodeFromCartOrSession(order));
    }

    @Test
    public void testGetPostCodeFromOrderWhenNoAddress() {
        targetPostCodeServiceImpl.setSessionService(sessionService);
        final OrderModel order = Mockito.mock(OrderModel.class);
        given(order.getDeliveryAddress()).willReturn(null);
        Assert.assertEquals(StringUtils.EMPTY, targetPostCodeServiceImpl.getPostalCodeFromCartOrSession(order));
    }

    @Test
    public void testGetPostCodeFromSession() {
        targetPostCodeServiceImpl.setSessionService(sessionService);
        final CartModel cart = Mockito.mock(CartModel.class);
        given(cart.getDeliveryAddress()).willReturn(null);
        given(sessionService.getAttribute(Mockito.anyString())).willReturn("3001");
        Assert.assertEquals("3001", targetPostCodeServiceImpl.getPostalCodeFromCartOrSession(cart));
    }

    @Test
    public void testGetPostCodeWhenEmpty() {
        targetPostCodeServiceImpl.setSessionService(sessionService);
        final CartModel cart = Mockito.mock(CartModel.class);
        given(cart.getDeliveryAddress()).willReturn(null);
        given(sessionService.getAttribute(Mockito.anyString())).willReturn(null);
        Assert.assertEquals(StringUtils.EMPTY, targetPostCodeServiceImpl.getPostalCodeFromCartOrSession(cart));
    }

    @Test
    public void testgetPostCodeGroupByPostcode() {
        final PostCodeGroupModel pcg1 = Mockito.mock(PostCodeGroupModel.class);
        final PostCodeGroupModel pcg2 = Mockito.mock(PostCodeGroupModel.class);
        final PostCodeGroupModel pcg3 = Mockito.mock(PostCodeGroupModel.class);
        final Collection<PostCodeGroupModel> groups = Arrays.asList(pcg1, pcg2, pcg3);

        given(mockPostCodeModel.getPostCodeGroups()).willReturn(groups);
        given(mockTargetPostCodeDao.findPostCodeByCode(TEST_POST_CODE)).willReturn(mockPostCodeModel);
        assertThat(targetPostCodeServiceImpl.getPostCodeGroupByPostcode(TEST_POST_CODE)).isEqualTo(groups);
    }

    @Test
    public void testgetPostCodeGroupByPostcodeWithEmptyPostcode() {
        assertThat(targetPostCodeServiceImpl.getPostCodeGroupByPostcode(StringUtils.EMPTY)).isNull();
    }

    @Test
    public void testgetPostCodeGroupByPostcodeWithNullPostcodeModel() {
        given(mockTargetPostCodeDao.findPostCodeByCode(TEST_POST_CODE)).willReturn(null);
        assertThat(targetPostCodeServiceImpl.getPostCodeGroupByPostcode(TEST_POST_CODE)).isNull();
    }
}
