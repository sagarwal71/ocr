package au.com.target.tgtcore.order.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.IpgGiftCardPaymentInfoModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFindOrderTotalPaymentMadeStrategyTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @InjectMocks
    private final TargetFindOrderTotalPaymentMadeStrategy strategy = new TargetFindOrderTotalPaymentMadeStrategy();

    @Mock
    private OrderModel order;

    @Mock
    private CartModel cart;

    @Mock
    private PaymentTransactionModel trans;

    @Mock
    private PaymentTransactionEntryModel entryCapture;

    @Mock
    private PaymentTransactionEntryModel entryCaptureReview;

    @Mock
    private PaymentTransactionEntryModel entryRefund;

    @Mock
    private PaymentTransactionEntryModel entryRefundRejected;

    private final List<PaymentTransactionEntryModel> listEntries = new ArrayList<>();

    @Before
    public void setup() {
        listEntries.clear();
        listEntries.add(entryCapture);
        listEntries.add(entryCaptureReview);
        listEntries.add(entryRefund);
        listEntries.add(entryRefundRejected);

        given(order.getPaymentTransactions()).willReturn(Collections.singletonList(trans));
        given(cart.getPaymentTransactions()).willReturn(Collections.singletonList(trans));
        given(trans.getEntries()).willReturn(listEntries);

        given(entryCapture.getAmount()).willReturn(new BigDecimal(8.34f));
        given(entryCapture.getType()).willReturn(PaymentTransactionType.CAPTURE);
        given(entryCapture.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.name());

        given(entryCaptureReview.getAmount()).willReturn(new BigDecimal(5.01f));
        given(entryCaptureReview.getType()).willReturn(PaymentTransactionType.CAPTURE);
        given(entryCaptureReview.getTransactionStatus()).willReturn(TransactionStatus.REVIEW.name());

        given(entryRefund.getAmount()).willReturn(new BigDecimal(2.13f));
        given(entryRefund.getType()).willReturn(PaymentTransactionType.REFUND_FOLLOW_ON);
        given(entryRefund.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.name());

        given(entryRefundRejected.getAmount()).willReturn(new BigDecimal(2.13f));
        given(entryRefundRejected.getType()).willReturn(PaymentTransactionType.REFUND_FOLLOW_ON);
        given(entryRefundRejected.getTransactionStatus()).willReturn(TransactionStatus.REJECTED.name());
    }


    @Test
    public void testGetAmountPaidSoFar() {
        final Double amount = strategy.getAmountPaidSoFar(order);
        Assert.assertEquals(new Double(6.21), amount);
    }

    @Test
    public void testGetAmountPaidSoFarForEmptyOrder() {
        listEntries.clear();
        final Double amount = strategy.getAmountPaidSoFar(order);
        Assert.assertEquals(new Double(0), amount);
    }

    @Test
    public void testGetAmountPaidSoFarForEmptyCart() {
        given(cart.getPaymentTransactions()).willReturn(null);
        final Double amount = strategy.getAmountPaidSoFar(cart);
        Assert.assertEquals(new Double(0), amount);
    }

    @Test
    public void testGetAmountPaidSoFarForEmptyTran() {
        given(trans.getEntries()).willReturn(null);
        final Double amount = strategy.getAmountPaidSoFar(cart);
        Assert.assertEquals(new Double(0), amount);
    }

    @Test
    public void testGetAmountPaidSoFarForCart() {
        final Double amount = strategy.getAmountPaidSoFar(cart);
        Assert.assertEquals(new Double(11.22), amount);
    }

    @Test
    public void testGetAmountPaidSoFarRefundStandalone() {
        given(entryRefund.getAmount()).willReturn(new BigDecimal(4.30f));
        given(entryRefund.getType()).willReturn(PaymentTransactionType.REFUND_STANDALONE);
        given(entryRefund.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.name());

        final Double amount = strategy.getAmountPaidSoFar(order);
        Assert.assertEquals(new Double(4.04), amount);
    }

    @Test
    public void testGetAmountPaidSoFarWithGiftcardPayment() {
        final BigDecimal amount1 = BigDecimal.valueOf(12.34d);
        final BigDecimal amount2 = BigDecimal.valueOf(56.78d);
        final BigDecimal amount3 = BigDecimal.valueOf(23.99d);
        final BigDecimal amount4 = BigDecimal.valueOf(63.00d);
        final BigDecimal amount5 = BigDecimal.valueOf(13.21d);
        final BigDecimal amount6 = BigDecimal.valueOf(3.48d);
        final BigDecimal amount7 = BigDecimal.valueOf(0.01d);
        final BigDecimal amount8 = BigDecimal.valueOf(11.1d);
        final BigDecimal amount9 = BigDecimal.valueOf(19.01d);

        final PaymentTransactionEntryModel paymentTransactionCapture1 = setUpPaymentTransactionEntry(amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.REJECTED);

        final PaymentTransactionEntryModel paymentTransactionCapture2 = setUpPaymentTransactionEntry(amount2,
                PaymentTransactionType.CAPTURE, TransactionStatus.REVIEW);

        final PaymentTransactionEntryModel paymentTransactionCapture3 = setUpPaymentTransactionEntry(amount3,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED);

        final PaymentTransactionEntryModel paymentTransactionCapture4 = setUpPaymentTransactionEntry(amount4,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED);

        final PaymentTransactionEntryModel paymentTransactionCapture5 = setUpPaymentTransactionEntry(amount5,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED);

        final PaymentTransactionEntryModel paymentTransactionRefund1 = setUpPaymentTransactionEntry(amount6,
                PaymentTransactionType.REFUND_STANDALONE, TransactionStatus.ACCEPTED);

        final PaymentTransactionEntryModel paymentTransactionRefund2 = setUpPaymentTransactionEntry(amount7,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.ACCEPTED);

        final PaymentTransactionEntryModel paymentTransactionRefund3 = setUpPaymentTransactionEntry(amount8,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.REVIEW);
        final IpgGiftCardPaymentInfoModel ipgGiftCardPaymentInfoModel = Mockito.mock(IpgGiftCardPaymentInfoModel.class);
        Mockito.when(paymentTransactionRefund3.getIpgPaymentInfo()).thenReturn(ipgGiftCardPaymentInfoModel);

        final PaymentTransactionEntryModel paymentTransactionRefund4 = setUpPaymentTransactionEntry(amount9,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.REVIEW);
        final PaymentTransactionModel paymentTransaction = Mockito.mock(PaymentTransactionModel.class);
        Mockito.when(paymentTransaction.getEntries()).thenReturn(
                Arrays.asList(paymentTransactionCapture1, paymentTransactionCapture2,
                        paymentTransactionCapture3,
                        paymentTransactionCapture4, paymentTransactionCapture5, paymentTransactionRefund1,
                        paymentTransactionRefund2, paymentTransactionRefund3, paymentTransactionRefund4));
        Mockito.when(order.getPaymentTransactions())
                .thenReturn(Arrays.asList(paymentTransaction));

        final Double result = strategy.getAmountPaidSoFar(order);

        Assert.assertEquals(amount3.add(amount4).add(amount5).subtract(amount6).subtract(amount7),
                new BigDecimal(result.doubleValue()).setScale(2, BigDecimal.ROUND_HALF_DOWN));
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testGetAmountCapturedSoFarNull() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("AbstractOrderModel cannot be null");

        strategy.getAmountCapturedSoFar(null);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testGetAmountCapturedSoFarNullTransactions() {

        Mockito.when(order.getPaymentTransactions()).thenReturn(null);

        final BigDecimal result = strategy.getAmountCapturedSoFar(order);

        Assert.assertEquals(BigDecimal.ZERO, result);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testGetAmountCapturedSoFarEmptyTransactions() {

        Mockito.when(order.getPaymentTransactions()).thenReturn(Collections.EMPTY_LIST);

        final BigDecimal result = strategy.getAmountCapturedSoFar(order);

        Assert.assertEquals(BigDecimal.ZERO, result);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testGetAmountCapturedSoFarNullTransactionEntries() {

        final PaymentTransactionModel paymentTransaction = Mockito.mock(PaymentTransactionModel.class);

        Mockito.when(paymentTransaction.getEntries()).thenReturn(null);
        Mockito.when(order.getPaymentTransactions()).thenReturn(Collections.singletonList(paymentTransaction));

        final BigDecimal result = strategy.getAmountCapturedSoFar(order);

        Assert.assertEquals(BigDecimal.ZERO, result);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testGetAmountCapturedSoFarEmptyTransactionEntries() {

        final PaymentTransactionModel paymentTransaction = Mockito.mock(PaymentTransactionModel.class);

        Mockito.when(paymentTransaction.getEntries()).thenReturn(Collections.EMPTY_LIST);
        Mockito.when(order.getPaymentTransactions()).thenReturn(Collections.singletonList(paymentTransaction));

        final BigDecimal result = strategy.getAmountCapturedSoFar(order);

        Assert.assertEquals(BigDecimal.ZERO, result);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testGetAmountCapturedSoFarNonCaptureTransactionEntries() {

        final PaymentTransactionModel paymentTransaction = setUpPaymentTransaction(null, null, null);
        Mockito.when(order.getPaymentTransactions()).thenReturn(Collections.singletonList(paymentTransaction));

        final BigDecimal result = strategy.getAmountCapturedSoFar(order);

        Assert.assertEquals(BigDecimal.ZERO, result);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testGetAmountCapturedSoFarCaptureTransactionEntriesWithNullStatus() {

        final PaymentTransactionModel paymentTransaction = setUpPaymentTransaction(null,
                PaymentTransactionType.CAPTURE, null);

        Mockito.when(order.getPaymentTransactions()).thenReturn(Collections.singletonList(paymentTransaction));

        final BigDecimal result = strategy.getAmountCapturedSoFar(order);

        Assert.assertEquals(BigDecimal.ZERO, result);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testGetAmountCapturedSoFarCaptureTransactionEntriesWithRejectedStatus() {

        final PaymentTransactionModel paymentTransaction = setUpPaymentTransaction(null,
                PaymentTransactionType.CAPTURE, TransactionStatus.REJECTED);
        Mockito.when(order.getPaymentTransactions()).thenReturn(Collections.singletonList(paymentTransaction));

        final BigDecimal result = strategy.getAmountCapturedSoFar(order);

        Assert.assertEquals(BigDecimal.ZERO, result);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testGetAmountCapturedSoFarCaptureTransactionEntriesWithErrorStatus() {

        final PaymentTransactionModel paymentTransaction = setUpPaymentTransaction(null,
                PaymentTransactionType.CAPTURE, TransactionStatus.ERROR);

        Mockito.when(order.getPaymentTransactions()).thenReturn(Collections.singletonList(paymentTransaction));

        final BigDecimal result = strategy.getAmountCapturedSoFar(order);

        Assert.assertEquals(BigDecimal.ZERO, result);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testGetAmountCapturedSoFarSingleAcceptedOnlyCaptureTransactionEntry() {

        final BigDecimal amount = BigDecimal.valueOf(13.33d);

        final PaymentTransactionModel paymentTransaction = setUpPaymentTransaction(amount,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED);
        Mockito.when(order.getPaymentTransactions()).thenReturn(Collections.singletonList(paymentTransaction));

        final BigDecimal result = strategy.getAmountCapturedSoFar(order);

        Assert.assertEquals(amount, result);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testGetAmountCapturedSoFarMultipleAcceptedOnlyCaptureTransactionEntries() {

        final BigDecimal amount1 = BigDecimal.valueOf(12.34d);
        final BigDecimal amount2 = BigDecimal.valueOf(56.78d);

        final PaymentTransactionModel paymentTransaction1 = setUpPaymentTransaction(amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED);

        final PaymentTransactionModel paymentTransaction2 = setUpPaymentTransaction(amount2,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED);

        Mockito.when(order.getPaymentTransactions())
                .thenReturn(Arrays.asList(paymentTransaction1, paymentTransaction2));

        final BigDecimal result = strategy.getAmountCapturedSoFar(order);

        Assert.assertEquals(amount1.add(amount2), result);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testGetAmountCapturedSoFarSingleReviewOnlyCaptureTransactionEntry() {

        final BigDecimal amount = BigDecimal.valueOf(13.33d);

        final PaymentTransactionModel paymentTransaction = setUpPaymentTransaction(amount,
                PaymentTransactionType.CAPTURE, TransactionStatus.REVIEW);
        Mockito.when(order.getPaymentTransactions()).thenReturn(Collections.singletonList(paymentTransaction));

        final BigDecimal result = strategy.getAmountCapturedSoFar(order);

        Assert.assertEquals(amount, result);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testGetAmountCapturedSoFarMultipleReviewOnlyCaptureTransactionEntries() {

        final BigDecimal amount1 = BigDecimal.valueOf(12.34d);
        final BigDecimal amount2 = BigDecimal.valueOf(56.78d);

        final PaymentTransactionModel paymentTransaction1 = setUpPaymentTransaction(amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.REVIEW);

        final PaymentTransactionModel paymentTransaction2 = setUpPaymentTransaction(amount2,
                PaymentTransactionType.CAPTURE, TransactionStatus.REVIEW);

        Mockito.when(order.getPaymentTransactions())
                .thenReturn(Arrays.asList(paymentTransaction1, paymentTransaction2));

        final BigDecimal result = strategy.getAmountCapturedSoFar(order);

        Assert.assertEquals(amount1.add(amount2), result);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testGetAmountCapturedSoFarCaptureTransactionEntriesMixedStatus() {

        final BigDecimal amount1 = BigDecimal.valueOf(12.34d);
        final BigDecimal amount2 = BigDecimal.valueOf(56.78d);
        final BigDecimal amount3 = BigDecimal.valueOf(23.48d);

        final PaymentTransactionModel paymentTransaction1 = setUpPaymentTransaction(amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.REJECTED);

        final PaymentTransactionModel paymentTransaction2 = setUpPaymentTransaction(amount2,
                PaymentTransactionType.CAPTURE, TransactionStatus.REVIEW);

        final PaymentTransactionModel paymentTransaction3 = setUpPaymentTransaction(amount3,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED);

        Mockito.when(order.getPaymentTransactions())
                .thenReturn(Arrays.asList(paymentTransaction1, paymentTransaction2, paymentTransaction3));

        final BigDecimal result = strategy.getAmountCapturedSoFar(order);

        Assert.assertEquals(amount2.add(amount3), result);
    }


    private PaymentTransactionModel setUpPaymentTransaction(final BigDecimal amount,
            final PaymentTransactionType transactionType, final TransactionStatus transactionStatus) {
        final PaymentTransactionModel paymentTransaction = Mockito.mock(PaymentTransactionModel.class);
        final PaymentTransactionEntryModel paymentTransactionEntry = Mockito.mock(PaymentTransactionEntryModel.class);

        Mockito.when(paymentTransactionEntry.getType()).thenReturn(transactionType);
        if (transactionStatus != null) {
            Mockito.when(paymentTransactionEntry.getTransactionStatus()).thenReturn(transactionStatus.toString());
        }
        Mockito.when(paymentTransactionEntry.getAmount()).thenReturn(amount);

        Mockito.when(paymentTransaction.getEntries()).thenReturn(Collections.singletonList(paymentTransactionEntry));

        return paymentTransaction;
    }

    private PaymentTransactionEntryModel setUpPaymentTransactionEntry(final BigDecimal amount,
            final PaymentTransactionType transactionType, final TransactionStatus transactionStatus) {
        final PaymentTransactionEntryModel paymentTransactionEntry = Mockito.mock(PaymentTransactionEntryModel.class);

        Mockito.when(paymentTransactionEntry.getType()).thenReturn(transactionType);
        if (transactionStatus != null) {
            Mockito.when(paymentTransactionEntry.getTransactionStatus()).thenReturn(transactionStatus.toString());
        }
        Mockito.when(paymentTransactionEntry.getAmount()).thenReturn(amount);

        return paymentTransactionEntry;
    }
}
