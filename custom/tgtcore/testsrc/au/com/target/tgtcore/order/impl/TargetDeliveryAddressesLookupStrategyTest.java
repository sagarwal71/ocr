/**
 * 
 */
package au.com.target.tgtcore.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import java.util.List;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetDeliveryAddressesLookupStrategyTest {

    @Mock
    private CustomerAccountService customerAccountService;

    @Mock
    private CheckoutCustomerStrategy checkoutCustomerStrategy;

    @Mock
    private AddressModel addressModel;

    @Mock
    private TargetZoneDeliveryModeModel deliveryMode;

    @InjectMocks
    private final TargetDeliveryAddressesLookupStrategy strategy = new TargetDeliveryAddressesLookupStrategy();

    @Test
    public void testGetDeliveryAddressesForOrderWithNullOrder() {
        final AbstractOrderModel abstractOrder = null;
        final boolean visibleAddressesOnly = true;
        final List<AddressModel> result = strategy.getDeliveryAddressesForOrder(abstractOrder, visibleAddressesOnly);
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void testGetDeliveryAddressesForOrderWithMismatchUserModel() {
        final AbstractOrderModel abstractOrder = Mockito.mock(AbstractOrderModel.class);
        final UserModel userModel = Mockito.mock(UserModel.class);
        final boolean visibleAddressesOnly = true;
        BDDMockito.given(abstractOrder.getUser()).willReturn(userModel);
        final List<AddressModel> result = strategy.getDeliveryAddressesForOrder(abstractOrder, visibleAddressesOnly);
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void testGetDeliveryAddressesForOrderWithVisibleAddressesOnly() {
        final AbstractOrderModel abstractOrder = Mockito.mock(AbstractOrderModel.class);
        final CustomerModel userModel = Mockito.mock(CustomerModel.class);
        final boolean visibleAddressesOnly = true;
        BDDMockito.given(abstractOrder.getUser()).willReturn(userModel);
        final List<AddressModel> result = strategy.getDeliveryAddressesForOrder(abstractOrder, visibleAddressesOnly);
        Mockito.verify(customerAccountService).getAddressBookDeliveryEntries(userModel);
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void testGetDeliveryAddressesForOrderWithVisibleAddressesOnlyFalse() {
        final AbstractOrderModel abstractOrder = Mockito.mock(AbstractOrderModel.class);
        final CustomerModel userModel = Mockito.mock(CustomerModel.class);
        final boolean visibleAddressesOnly = false;
        BDDMockito.given(abstractOrder.getUser()).willReturn(userModel);
        final List<AddressModel> result = strategy.getDeliveryAddressesForOrder(abstractOrder, visibleAddressesOnly);
        Mockito.verify(customerAccountService).getAllAddressEntries(userModel);
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void testGetDeliveryAddressesForOrderWithNoAnonymousCheckout() {
        final AbstractOrderModel abstractOrder = Mockito.mock(AbstractOrderModel.class);
        final CustomerModel userModel = Mockito.mock(CustomerModel.class);
        final boolean visibleAddressesOnly = true;
        BDDMockito.given(abstractOrder.getUser()).willReturn(userModel);
        BDDMockito.given(Boolean.valueOf(checkoutCustomerStrategy.isAnonymousCheckout())).willReturn(Boolean.FALSE);
        final List<AddressModel> result = strategy.getDeliveryAddressesForOrder(abstractOrder, visibleAddressesOnly);
        Mockito.verify(customerAccountService).getAddressBookDeliveryEntries(userModel);
        Mockito.verify(abstractOrder, Mockito.times(0)).getDeliveryAddress();
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void testGetDeliveryAddressesForOrderWithNullDeliveryAddress() {
        final AbstractOrderModel abstractOrder = Mockito.mock(AbstractOrderModel.class);
        final CustomerModel userModel = Mockito.mock(CustomerModel.class);
        final boolean visibleAddressesOnly = true;
        BDDMockito.given(abstractOrder.getUser()).willReturn(userModel);
        BDDMockito.given(Boolean.valueOf(checkoutCustomerStrategy.isAnonymousCheckout())).willReturn(Boolean.TRUE);
        BDDMockito.given(abstractOrder.getDeliveryAddress()).willReturn(null);
        final List<AddressModel> result = strategy.getDeliveryAddressesForOrder(abstractOrder, visibleAddressesOnly);
        Mockito.verify(customerAccountService).getAddressBookDeliveryEntries(userModel);
        Mockito.verify(abstractOrder, Mockito.times(1)).getDeliveryAddress();
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void testGetDeliveryAddressesForOrderWithCncDeliveryMode() {
        final AbstractOrderModel abstractOrder = Mockito.mock(AbstractOrderModel.class);
        final CustomerModel userModel = Mockito.mock(CustomerModel.class);
        final boolean visibleAddressesOnly = true;
        BDDMockito.given(abstractOrder.getUser()).willReturn(userModel);
        BDDMockito.given(Boolean.valueOf(checkoutCustomerStrategy.isAnonymousCheckout())).willReturn(Boolean.TRUE);
        BDDMockito.given(abstractOrder.getDeliveryAddress()).willReturn(addressModel);
        BDDMockito.given(abstractOrder.getDeliveryMode()).willReturn(deliveryMode);
        BDDMockito.given(deliveryMode.getIsDeliveryToStore()).willReturn(Boolean.TRUE);
        final List<AddressModel> result = strategy.getDeliveryAddressesForOrder(abstractOrder, visibleAddressesOnly);
        Mockito.verify(customerAccountService).getAddressBookDeliveryEntries(userModel);
        Mockito.verify(abstractOrder, Mockito.times(1)).getDeliveryAddress();
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void testGetDeliveryAddressesForOrderWithNoCncDeliveryMode() {
        final AbstractOrderModel abstractOrder = Mockito.mock(AbstractOrderModel.class);
        final CustomerModel userModel = Mockito.mock(CustomerModel.class);
        final boolean visibleAddressesOnly = true;
        BDDMockito.given(abstractOrder.getUser()).willReturn(userModel);
        BDDMockito.given(Boolean.valueOf(checkoutCustomerStrategy.isAnonymousCheckout())).willReturn(Boolean.TRUE);
        BDDMockito.given(abstractOrder.getDeliveryAddress()).willReturn(addressModel);
        BDDMockito.given(abstractOrder.getDeliveryMode()).willReturn(deliveryMode);
        BDDMockito.given(deliveryMode.getIsDeliveryToStore()).willReturn(Boolean.FALSE);
        final List<AddressModel> result = strategy.getDeliveryAddressesForOrder(abstractOrder, visibleAddressesOnly);
        Mockito.verify(customerAccountService).getAddressBookDeliveryEntries(userModel);
        Mockito.verify(abstractOrder, Mockito.times(2)).getDeliveryAddress();
        Assert.assertEquals(1, result.size());
    }
}
