package au.com.target.tgtcore.order.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;

import org.junit.Assert;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Unit test for {@link TargetCreateOrderFromCartStrategy}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCreateOrderFromCartStrategyTest {

    private final TargetCreateOrderFromCartStrategy targetCreateOrderFromCartStrategy = new TargetCreateOrderFromCartStrategy();

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Test
    public void testGenerateOrderCodeWithNullCart() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Cartmodel cannot be null");
        targetCreateOrderFromCartStrategy.generateOrderCode(null);
    }

    @Test
    public void testGenerateOrderCode() {
        final CartModel cart = Mockito.mock(CartModel.class);
        BDDMockito.given(cart.getCode()).willReturn("00001");

        final String result = targetCreateOrderFromCartStrategy.generateOrderCode(cart);
        Assert.assertEquals("00001", result);
    }

}
