/**
 * 
 */
package au.com.target.tgtcore.order.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.payment.dto.CardType;
import de.hybris.platform.servicelayer.type.TypeService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCommerceCardTypeServiceImplTest {

    @InjectMocks
    private final TargetCommerceCardTypeServiceImpl targetCommerceCardTypeService = new TargetCommerceCardTypeServiceImpl();

    @Mock
    private EnumerationService enumerationService;

    @Mock
    private TypeService typeService;

    @Mock
    private EnumerationValueModel amexEnumValue;
    @Mock
    private EnumerationValueModel visaEnumValue;
    @Mock
    private EnumerationValueModel masterEnumValue;

    private List<HybrisEnumValue> listCreditCardTypes;

    @Before
    public void setup() {
        listCreditCardTypes = setupCreditCardInfo();
        given(enumerationService.getEnumerationValues("CreditCardType")).willReturn(listCreditCardTypes);
        given(typeService.getEnumerationValue(CreditCardType.AMEX)).willReturn(amexEnumValue);
        given(typeService.getEnumerationValue(CreditCardType.VISA)).willReturn(visaEnumValue);
        given(typeService.getEnumerationValue(CreditCardType.MASTER)).willReturn(masterEnumValue);
        given(typeService.getEnumerationValue(CreditCardType.AMEX).getName()).willReturn("AMEX CARD");
        given(typeService.getEnumerationValue(CreditCardType.VISA).getName()).willReturn("VISA CARD");
        given(typeService.getEnumerationValue(CreditCardType.MASTER).getName()).willReturn("MASTER CARD");
    }

    @Test
    public void testGetCardTypes() {


        final Collection<CardType> cardTypesResult = targetCommerceCardTypeService.getCardTypes();

        Assert.assertEquals(3, cardTypesResult.size());
    }

    @Test
    public void testGetSpecificCard() {

        final CardType cardTypeResult = targetCommerceCardTypeService.getCardTypeForCode("visa");
        Assert.assertNotNull(cardTypeResult);
        Assert.assertEquals(cardTypeResult.getCode().getCode(), "visa");
    }

    private List<HybrisEnumValue> setupCreditCardInfo() {
        final List<HybrisEnumValue> creditCardTypeList = new ArrayList<>();

        creditCardTypeList.add(CreditCardType.AMEX);
        creditCardTypeList.add(CreditCardType.VISA);
        creditCardTypeList.add(CreditCardType.MASTER);

        return creditCardTypeList;
    }
}
