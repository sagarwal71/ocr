/**
 * 
 */
package au.com.target.tgtcore.order.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.util.TaxValue;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetTaxModel;
import au.com.target.tgtcore.tax.dao.TargetTaxDao;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFindFeeTaxValueStrategyTest {

    @Mock
    private TargetTaxDao targetTaxDao;

    @InjectMocks
    private final TargetFindFeeTaxValueStrategy strategy = new TargetFindFeeTaxValueStrategy();

    @Test
    public void testFindTaxValues()
    {
        final TargetTaxModel defaultTax = mock(TargetTaxModel.class);
        final CurrencyModel currency = mock(CurrencyModel.class);

        given(targetTaxDao.getDefaultTax()).willReturn(defaultTax);
        given(defaultTax.getCode()).willReturn("tax-code");
        given(defaultTax.getValue()).willReturn(new Double(10));
        given(defaultTax.getCurrency()).willReturn(currency);
        given(currency.getIsocode()).willReturn("AUD");

        final TaxValue taxValue = strategy.getTaxValueForFee();
        Assert.assertNotNull(taxValue);
        Assert.assertEquals("tax-code", taxValue.getCode());
        Assert.assertTrue(taxValue.getValue() == 10);
        Assert.assertEquals("AUD", taxValue.getCurrencyIsoCode());

    }
}
