/**
 * 
 */
package au.com.target.tgtcore.util;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.jalo.AbstractTargetVariantProduct;
import au.com.target.tgtcore.jalo.ProductType;
import au.com.target.tgtcore.jalo.TargetProduct;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * @author Nandini
 *
 */
@SuppressWarnings("deprecation")
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductUtilTest {

    @Mock
    private TargetProduct baseProduct;

    @Mock
    private AbstractTargetVariantProduct variantProduct;

    @Mock
    private ProductType productType;

    @Mock
    private TargetProductModel baseProductModel;

    @Mock
    private AbstractTargetVariantProductModel variantProductModel;

    @Mock
    private ProductTypeModel productTypeModel;

    @Test
    public void testGetProductTypeCodeNullProductType() {
        given(productType.getCode()).willReturn(null);
        given(variantProduct.getBaseProduct()).willReturn(baseProduct);
        given(baseProduct.getProductType()).willReturn(productType);
        assertThat(ProductUtil.getProductTypeCode(variantProduct)).isNull();
    }

    @Test
    public void testGetProductTypeCodeForBaseProduct() {
        given(productType.getCode()).willReturn(ProductType.BULKY);
        given(baseProduct.getProductType()).willReturn(productType);
        assertThat(ProductUtil.getProductTypeCode(baseProduct)).isEqualTo("bulky");
    }

    @Test
    public void testGetProductTypeCodeVariantProduct() {
        given(productType.getCode()).willReturn(ProductType.DIGITAL);
        given(variantProduct.getBaseProduct()).willReturn(baseProduct);
        given(baseProduct.getProductType()).willReturn(productType);
        assertThat(ProductUtil.getProductTypeCode(baseProduct)).isEqualTo("digital");
    }

    @Test
    public void testGetProductTypeCodeNullProductTypeModel() {
        given(productTypeModel.getCode()).willReturn(null);
        given(variantProductModel.getBaseProduct()).willReturn(baseProductModel);
        given(baseProductModel.getProductType()).willReturn(productTypeModel);
        assertThat(ProductUtil.getProductTypeCode(variantProduct)).isNull();
    }

    @Test
    public void testGetProductTypeCodeForBaseProductModel() {
        given(productTypeModel.getCode()).willReturn(ProductType.DIGITAL);
        given(baseProductModel.getProductType()).willReturn(productTypeModel);
        assertThat(ProductUtil.getProductTypeCode(baseProductModel)).isEqualTo("digital");
    }

    @Test
    public void testGetProductTypeCodeVariantProductModel() {
        given(productTypeModel.getCode()).willReturn(ProductType.BULKY);
        given(variantProductModel.getBaseProduct()).willReturn(baseProductModel);
        given(baseProductModel.getProductType()).willReturn(productTypeModel);
        assertThat(ProductUtil.getProductTypeCode(baseProductModel)).isEqualTo("bulky");
    }

    @Test
    public void testIsProductTypeDigital() {
        given(productTypeModel.getCode()).willReturn("digital");
        given(baseProductModel.getProductType()).willReturn(productTypeModel);
        assertThat(ProductUtil.isProductTypeDigital(baseProductModel)).isTrue();
    }

    @Test
    public void testIsProductTypePhysicalGiftCard() {
        given(productTypeModel.getCode()).willReturn("giftCard");
        given(baseProductModel.getProductType()).willReturn(productTypeModel);
        assertThat(ProductUtil.isProductTypePhysicalGiftcard(baseProductModel)).isTrue();
    }

}
