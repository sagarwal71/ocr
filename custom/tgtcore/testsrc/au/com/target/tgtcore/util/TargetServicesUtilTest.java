/**
 * 
 */
package au.com.target.tgtcore.util;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;

import org.junit.Test;


@UnitTest
public class TargetServicesUtilTest {

    @Test
    public void testExtractSafeCodeFromName() {
        final String test1 = "T   E S T1-1_(&*   &";
        final String result1 = TargetServicesUtil.extractSafeCodeFromName(test1, false);
        Assert.assertEquals("test", result1);

        final String result2 = TargetServicesUtil.extractSafeCodeFromName(test1, true);

        Assert.assertEquals("test11", result2);

    }
}
