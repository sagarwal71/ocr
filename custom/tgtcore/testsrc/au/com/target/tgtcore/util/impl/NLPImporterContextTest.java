/**
 * 
 */
package au.com.target.tgtcore.util.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.product.TargetProductService;


/**
 * @author ajit
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class NLPImporterContextTest {

    @InjectMocks
    private final NLPImporterContext nlpImportContext = new NLPImporterContext();

    @Mock
    private TargetProductService targetProductService;

    @Mock
    private CatalogVersionService catalogVersionService;

    @Test
    public void testPrepareNLPDataForColourVariantProduct() {
        final ProductModel product = new TargetColourVariantProductModel();
        final Map<Integer, String> line = new HashMap<Integer, String>();
        line.put(Integer.valueOf(1), "P0001");
        line.put(Integer.valueOf(2), "12-11-2015");
        line.put(Integer.valueOf(2), "12-02-2016");
        final CatalogVersionModel catalogVersion = Mockito.mock(CatalogVersionModel.class);
        Mockito.when(catalogVersionService.getCatalogVersion("targetProductCatalog", "Staged")).thenReturn(
                catalogVersion);
        Mockito.when(targetProductService.getProductForCode(catalogVersion, "P0001")).thenReturn(product);
        nlpImportContext.prepareNLPDates(line);
        Assert.assertNotNull(line);
        Assert.assertEquals(line.get(Integer.valueOf(11)), "P0001");
    }

    @Test
    public void testPrepareNLPDataForSizeVariantProduct() {
        final ProductModel colourVariantProduct = new TargetColourVariantProductModel();
        colourVariantProduct.setCode("CP0001");
        final TargetSizeVariantProductModel sizeVariantProduct = new TargetSizeVariantProductModel();
        sizeVariantProduct.setBaseProduct(colourVariantProduct);
        final Map<Integer, String> line = new HashMap<Integer, String>();
        line.put(Integer.valueOf(1), "P0001");
        line.put(Integer.valueOf(2), "12-11-2015");
        line.put(Integer.valueOf(2), "12-02-2016");
        final CatalogVersionModel catalogVersion = Mockito.mock(CatalogVersionModel.class);
        Mockito.when(catalogVersionService.getCatalogVersion("targetProductCatalog", "Staged")).thenReturn(
                catalogVersion);
        Mockito.when(targetProductService.getProductForCode(catalogVersion, "P0001")).thenReturn(sizeVariantProduct);
        nlpImportContext.prepareNLPDates(line);
        Assert.assertNotNull(line);
        Assert.assertEquals(line.get(Integer.valueOf(11)), "CP0001");
    }
}
