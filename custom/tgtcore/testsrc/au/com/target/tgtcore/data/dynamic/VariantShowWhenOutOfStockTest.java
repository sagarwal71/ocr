package au.com.target.tgtcore.data.dynamic;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;


@UnitTest
public class VariantShowWhenOutOfStockTest {

    private VariantShowWhenOutOfStock dynamicAttribute;

    @Before
    public void setUp() {
        dynamicAttribute = new VariantShowWhenOutOfStock();
    }

    /*
     *  test variant that does not have a base product
     */
    @Test
    public void testGetVariantBaseProductNull() throws Exception {
        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(null);

        final Boolean showWhenOutOfStock = dynamicAttribute.get(model);

        org.junit.Assert.assertNotNull(showWhenOutOfStock);
        org.junit.Assert.assertSame(Boolean.FALSE, showWhenOutOfStock);
    }

    /*
     *  test variant that has a product as the base, that base has null value for showWhenOutOfStock
     */
    @Test
    public void testGetVariantBaseProductShowIsNull() throws Exception {
        final TargetProductModel parent = Mockito.mock(TargetProductModel.class);
        BDDMockito.given(parent.getShowWhenOutOfStock()).willReturn(null);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final Boolean showWhenOutOfStock = dynamicAttribute.get(model);

        org.junit.Assert.assertNotNull(showWhenOutOfStock);
        org.junit.Assert.assertSame(Boolean.FALSE, showWhenOutOfStock);
    }

    /*
     *  test variant that has a product as the base, that base has true value for showWhenOutOfStock
     */
    @Test
    public void testGetVariantBaseProductShowIsTrue() throws Exception {
        final TargetProductModel parent = Mockito.mock(TargetProductModel.class);
        BDDMockito.given(parent.getShowWhenOutOfStock()).willReturn(Boolean.TRUE);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final Boolean showWhenOutOfStock = dynamicAttribute.get(model);

        org.junit.Assert.assertNotNull(showWhenOutOfStock);
        org.junit.Assert.assertSame(Boolean.TRUE, showWhenOutOfStock);
    }

    /*
     *  test variant that has a product as the base, that base has false value for showWhenOutOfStock
     */
    @Test
    public void testGetVariantBaseProductShowIsFalse() throws Exception {
        final TargetProductModel parent = Mockito.mock(TargetProductModel.class);
        BDDMockito.given(parent.getShowWhenOutOfStock()).willReturn(Boolean.FALSE);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final Boolean showWhenOutOfStock = dynamicAttribute.get(model);

        org.junit.Assert.assertNotNull(showWhenOutOfStock);
        org.junit.Assert.assertSame(Boolean.FALSE, showWhenOutOfStock);
    }

    /*
     *  test variant that has a another variant as the base, that base has null value for showWhenOutOfStock
     */
    @Test
    public void testGetVariantBaseVariantShowIsNull() throws Exception {
        final AbstractTargetVariantProductModel parent = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(parent.getShowWhenOutOfStock()).willReturn(null);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final Boolean showWhenOutOfStock = dynamicAttribute.get(model);

        org.junit.Assert.assertNotNull(showWhenOutOfStock);
        org.junit.Assert.assertSame(Boolean.FALSE, showWhenOutOfStock);
    }

    /*
     *  test variant that has a another variant as the base, that base has true value for showWhenOutOfStock
     */
    @Test
    public void testGetVariantBaseVariantShowIsTrue() throws Exception {
        final AbstractTargetVariantProductModel parent = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(parent.getShowWhenOutOfStock()).willReturn(Boolean.TRUE);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final Boolean showWhenOutOfStock = dynamicAttribute.get(model);

        org.junit.Assert.assertNotNull(showWhenOutOfStock);
        org.junit.Assert.assertSame(Boolean.TRUE, showWhenOutOfStock);
    }

    /*
     *  test variant that has a another variant as the base, that base has false value for showWhenOutOfStock
     */
    @Test
    public void testGetVariantBaseVariantShowIsFalse() throws Exception {
        final AbstractTargetVariantProductModel parent = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(parent.getShowWhenOutOfStock()).willReturn(Boolean.FALSE);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final Boolean showWhenOutOfStock = dynamicAttribute.get(model);

        org.junit.Assert.assertNotNull(showWhenOutOfStock);
        org.junit.Assert.assertSame(Boolean.FALSE, showWhenOutOfStock);
    }

    /*
     * It should never happen, but target variant where base product is not a target product
     */
    @Test
    public void testGetNonTargetParent() throws Exception {
        final ProductModel parent = Mockito.mock(ProductModel.class);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final Boolean showWhenOutOfStock = dynamicAttribute.get(model);

        org.junit.Assert.assertNotNull(showWhenOutOfStock);
        org.junit.Assert.assertSame(Boolean.FALSE, showWhenOutOfStock);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetNullModel() throws Exception {
        dynamicAttribute.get(null);
    }
}
