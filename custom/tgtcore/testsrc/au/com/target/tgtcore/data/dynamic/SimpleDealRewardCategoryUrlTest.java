package au.com.target.tgtcore.data.dynamic;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import au.com.target.tgtcore.model.TargetDealCategoryModel;


@UnitTest
public class SimpleDealRewardCategoryUrlTest {

    private final SimpleDealRewardCategoryUrl abstractSimpleDealRewardCategoryUrl = new SimpleDealRewardCategoryUrl();

    @Test
    public void testGetWithNullModel() throws Exception {
        final String result = abstractSimpleDealRewardCategoryUrl.get(null);

        assertThat(result).isNull();
    }

    @Test
    public void testGetWithNoRewardCategories() throws Exception {
        final List<TargetDealCategoryModel> rewardCategories = new ArrayList<>();

        final AbstractSimpleDealModel mockDealModel = mock(AbstractSimpleDealModel.class);
        given(mockDealModel.getRewardCategory()).willReturn(rewardCategories);

        final String result = abstractSimpleDealRewardCategoryUrl.get(mockDealModel);

        assertThat(result).isNull();
    }

    @Test
    public void testGetWithOneRewardCategory() throws Exception {
        final TargetDealCategoryModel mockDealCategory = mock(TargetDealCategoryModel.class);
        given(mockDealCategory.getCode()).willReturn("d1234cr1");

        final List<TargetDealCategoryModel> rewardCategories = new ArrayList<>();
        rewardCategories.add(mockDealCategory);

        final AbstractSimpleDealModel mockDealModel = mock(AbstractSimpleDealModel.class);
        given(mockDealModel.getRewardCategory()).willReturn(rewardCategories);

        final String result = abstractSimpleDealRewardCategoryUrl.get(mockDealModel);

        assertThat(result).isEqualTo("/d/d1234cr1");
    }

    @Test
    public void testGetWithTwoRewardCategories() throws Exception {
        final TargetDealCategoryModel mockDealCategory = mock(TargetDealCategoryModel.class);
        given(mockDealCategory.getCode()).willReturn("d1234cr1");

        final TargetDealCategoryModel mockDealCategory2 = mock(TargetDealCategoryModel.class);
        given(mockDealCategory2.getCode()).willReturn("d1234cr2");

        final List<TargetDealCategoryModel> rewardCategories = new ArrayList<>();
        rewardCategories.add(mockDealCategory);
        rewardCategories.add(mockDealCategory2);

        final AbstractSimpleDealModel mockDealModel = mock(AbstractSimpleDealModel.class);
        given(mockDealModel.getRewardCategory()).willReturn(rewardCategories);

        final String result = abstractSimpleDealRewardCategoryUrl.get(mockDealModel);

        assertThat(result).isEqualTo("/d/d1234cr1");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSet() throws Exception {
        abstractSimpleDealRewardCategoryUrl.set(null, null);
    }

}
