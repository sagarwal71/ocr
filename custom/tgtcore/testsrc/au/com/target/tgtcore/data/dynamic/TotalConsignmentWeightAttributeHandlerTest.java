/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtcore.model.TargetProductModel;



/**
 * @author ayushman
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TotalConsignmentWeightAttributeHandlerTest {

    @InjectMocks
    private final TotalConsignmentWeightAttributeHandler handler = new TotalConsignmentWeightAttributeHandler();

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private ConsignmentEntryModel consignemntEntries;

    @Test
    public void testNullConsignment() {
        final Double totalWeight = handler.get(null);
        Assert.assertNull(totalWeight);
    }

    @Test
    public void testNullConsignmentEntries() {
        BDDMockito.given(consignment.getConsignmentEntries()).willReturn(null);
        final Double totalWeight = handler.get(consignment);
        Assert.assertNull(totalWeight);
    }

    @Test
    public void testEmptyConsignmentEntries() {
        BDDMockito.given(consignment.getConsignmentEntries()).willReturn(new HashSet<ConsignmentEntryModel>());
        final Double totalWeight = handler.get(consignment);
        Assert.assertNull(totalWeight);
    }

    @Test
    public void testConsignmentEntryNullQty() {
        final OrderEntryModel orderEntry = new OrderEntryModel();
        orderEntry.setProduct(Mockito.mock(AbstractTargetVariantProductModel.class));
        orderEntry.setQuantity(null);
        final ConsignmentEntryModel conEntry = new ConsignmentEntryModel();
        conEntry.setOrderEntry(orderEntry);
        final HashSet<ConsignmentEntryModel> entries = new HashSet<>();
        entries.add(conEntry);

        BDDMockito.given(consignment.getConsignmentEntries()).willReturn(entries);

        final Double totalWeight = handler.get(consignment);
        Assert.assertNull(totalWeight);
    }

    @Test
    public void testConsignmentEntryNotVariant() {
        final OrderEntryModel orderEntry = new OrderEntryModel();
        orderEntry.setProduct(Mockito.mock(TargetProductModel.class));
        final ConsignmentEntryModel conEntry = new ConsignmentEntryModel();
        conEntry.setOrderEntry(orderEntry);
        final HashSet<ConsignmentEntryModel> entries = new HashSet<>();
        entries.add(conEntry);

        BDDMockito.given(consignment.getConsignmentEntries()).willReturn(entries);

        final Double totalWeight = handler.get(consignment);
        Assert.assertNull(totalWeight);
    }

    @Test
    public void testConsignentWithProductWeight() {
        final Set<ConsignmentEntryModel> conEntries = new HashSet<>();

        final ConsignmentEntryModel conEnt1 = Mockito.mock(ConsignmentEntryModel.class);
        final AbstractOrderEntryModel orderEnt1 = Mockito.mock(AbstractOrderEntryModel.class);
        final AbstractTargetVariantProductModel product1 = Mockito.mock(AbstractTargetVariantProductModel.class);
        final TargetProductDimensionsModel dimensions = new TargetProductDimensionsModel();
        dimensions.setWeight(Double.valueOf(9.328));
        BDDMockito.given(product1.getProductPackageDimensions()).willReturn(dimensions);
        BDDMockito.given(orderEnt1.getProduct()).willReturn(product1);
        BDDMockito.given(conEnt1.getOrderEntry()).willReturn(orderEnt1);
        BDDMockito.given(orderEnt1.getQuantity()).willReturn(Long.valueOf(2));
        conEntries.add(conEnt1);
        BDDMockito.given(consignment.getConsignmentEntries()).willReturn(conEntries);
        final Double weight = handler.get(consignment);
        Assert.assertEquals(Double.valueOf(18.66), weight);
        Assert.assertEquals("18.66", weight.toString());
    }

    @Test
    public void testConsignentWithMultipleProductsWithWeight() {
        final Set<ConsignmentEntryModel> conEntries = new HashSet<>();

        // first product
        final ConsignmentEntryModel conEnt1 = Mockito.mock(ConsignmentEntryModel.class);
        final AbstractOrderEntryModel orderEnt1 = Mockito.mock(AbstractOrderEntryModel.class);
        final AbstractTargetVariantProductModel product1 = Mockito.mock(AbstractTargetVariantProductModel.class);
        final TargetProductDimensionsModel dimensions = new TargetProductDimensionsModel();
        dimensions.setWeight(Double.valueOf(2.25));
        BDDMockito.given(product1.getProductPackageDimensions()).willReturn(dimensions);
        BDDMockito.given(orderEnt1.getProduct()).willReturn(product1);
        BDDMockito.given(conEnt1.getOrderEntry()).willReturn(orderEnt1);
        BDDMockito.given(orderEnt1.getQuantity()).willReturn(Long.valueOf(2));
        conEntries.add(conEnt1);

        // second product
        final ConsignmentEntryModel conEnt2 = Mockito.mock(ConsignmentEntryModel.class);
        final AbstractOrderEntryModel orderEnt2 = Mockito.mock(AbstractOrderEntryModel.class);
        final AbstractTargetVariantProductModel product2 = Mockito.mock(AbstractTargetVariantProductModel.class);
        final TargetProductDimensionsModel dimensions2 = new TargetProductDimensionsModel();
        dimensions2.setWeight(Double.valueOf(3.25));
        BDDMockito.given(product2.getProductPackageDimensions()).willReturn(dimensions2);
        BDDMockito.given(orderEnt2.getProduct()).willReturn(product2);
        BDDMockito.given(conEnt2.getOrderEntry()).willReturn(orderEnt2);
        BDDMockito.given(orderEnt2.getQuantity()).willReturn(Long.valueOf(2));
        conEntries.add(conEnt2);

        BDDMockito.given(consignment.getConsignmentEntries()).willReturn(conEntries);
        final Double totalWeight = handler.get(consignment);
        Assert.assertEquals(Double.valueOf(11d), totalWeight);
    }

    @Test
    public void testConsignentWithProductsWithoutWeight() {
        final Set<ConsignmentEntryModel> conEntries = new HashSet<>();

        // first product
        final ConsignmentEntryModel conEnt1 = Mockito.mock(ConsignmentEntryModel.class);
        final AbstractOrderEntryModel orderEnt1 = Mockito.mock(AbstractOrderEntryModel.class);
        final AbstractTargetVariantProductModel product1 = Mockito.mock(AbstractTargetVariantProductModel.class);
        final TargetProductDimensionsModel dimensions = new TargetProductDimensionsModel();
        dimensions.setWeight(Double.valueOf(20d));
        BDDMockito.given(product1.getProductPackageDimensions()).willReturn(dimensions);
        BDDMockito.given(orderEnt1.getProduct()).willReturn(product1);
        BDDMockito.given(conEnt1.getOrderEntry()).willReturn(orderEnt1);
        conEntries.add(conEnt1);

        // second product
        final ConsignmentEntryModel conEnt2 = Mockito.mock(ConsignmentEntryModel.class);
        final AbstractOrderEntryModel orderEnt2 = Mockito.mock(AbstractOrderEntryModel.class);
        final AbstractTargetVariantProductModel product2 = Mockito.mock(AbstractTargetVariantProductModel.class);
        final TargetProductDimensionsModel dimensions2 = new TargetProductDimensionsModel();
        BDDMockito.given(product2.getProductPackageDimensions()).willReturn(dimensions2);
        BDDMockito.given(orderEnt2.getProduct()).willReturn(product2);
        BDDMockito.given(conEnt2.getOrderEntry()).willReturn(orderEnt2);
        conEntries.add(conEnt2);

        BDDMockito.given(consignment.getConsignmentEntries()).willReturn(conEntries);
        final Double totalWeight = handler.get(consignment);
        Assert.assertNull(totalWeight);
    }
}
