/**
 * 
 */
package au.com.target.tgtcore.delivery.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


@UnitTest
public class TargetDeliveryModeHelperTest {

    /**
     * 
     */
    private static final String VARIANT_CODE = "ABC";

    /**
     * 
     */
    private static final String EXPRESS_DELIVERY = "express-delivery";

    /**
     * 
     */
    private static final String HOME_DELIVERY = "home-delivery";

    private static final String DIGITAL_DELIVERY = "digital";

    @Mock
    private TargetDeliveryService targetDeliveryService;

    @Spy
    private final TargetDeliveryModeHelperImpl targetDeliveryModeHelper = new TargetDeliveryModeHelperImpl();

    @Mock
    private TargetZoneDeliveryModeModel home;

    @Mock
    private TargetZoneDeliveryModeModel cnc;

    @Mock
    private TargetProductModel product;

    @Mock
    private AbstractTargetVariantProductModel variant;

    @Mock
    private AbstractOrderModel orderModel;

    @Mock
    private AbstractOrderEntryModel abstractOrderEntryModel;

    @Mock
    private AbstractTargetVariantProductModel abstractTargetProductModel;

    private Set<DeliveryModeModel> modes = new HashSet<>();

    @Before
    public void prepare() {
        MockitoAnnotations.initMocks(this);

        BDDMockito.given(home.getIsDeliveryToStore()).willReturn(Boolean.FALSE);
        BDDMockito.given(cnc.getIsDeliveryToStore()).willReturn(Boolean.TRUE);

        BDDMockito.given(targetDeliveryService.getDeliveryModeForCode("xxx")).willReturn(null);
        BDDMockito.given(targetDeliveryService.getDeliveryModeForCode(HOME_DELIVERY)).willReturn(home);
        BDDMockito.given(targetDeliveryService.getDeliveryModeForCode("click-and-collect")).willReturn(cnc);

        targetDeliveryModeHelper.setTargetDeliveryService(targetDeliveryService);

        // Initially the product is available for home delivery but not cnc
        modes.add(home);
        BDDMockito.given(targetDeliveryService.getVariantsDeliveryModesFromBaseProduct(product)).willReturn(modes);
        BDDMockito.given(product.getDeliveryModes()).willReturn(modes);
    }

    @Test
    public void testHasDigitalProductsContainsDigitalProducts() {
        Mockito.when(orderModel.getEntries()).thenReturn(Collections.singletonList(abstractOrderEntryModel));

        final AbstractTargetVariantProductModel abstractTargetVariantProductModel = createTargetVariantProductModel(VARIANT_CODE);
        final TargetZoneDeliveryModeModel deliveryModeDigital = createTargetZoneDeliveryModeModel(DIGITAL_DELIVERY, 1);
        final Set<DeliveryModeModel> targetZoneDeliveryModeModelList = new HashSet<>();
        Mockito.when(abstractOrderEntryModel.getProduct()).thenReturn(abstractTargetVariantProductModel);
        targetZoneDeliveryModeModelList.add(deliveryModeDigital);
        BDDMockito.given(targetDeliveryModeHelper.getDeliveryModesForProduct(abstractTargetVariantProductModel))
                .willReturn(targetZoneDeliveryModeModelList);
        Assert.assertTrue(targetDeliveryModeHelper.hasDigitalProducts(orderModel));
    }

    @Test
    public void testHasDigitalProductsContainsMixedProducts() {
        Mockito.when(orderModel.getEntries()).thenReturn(Collections.singletonList(abstractOrderEntryModel));

        final AbstractTargetVariantProductModel abstractTargetVariantProductModel = createTargetVariantProductModel(VARIANT_CODE);
        final TargetZoneDeliveryModeModel deliveryModeDigital = createTargetZoneDeliveryModeModel(DIGITAL_DELIVERY, 1);
        final TargetZoneDeliveryModeModel deliveryModeHome = createTargetZoneDeliveryModeModel(HOME_DELIVERY, 2);
        final TargetZoneDeliveryModeModel deliveryModeExpress = createTargetZoneDeliveryModeModel(EXPRESS_DELIVERY, 3);
        final Set<DeliveryModeModel> targetZoneDeliveryModeModelList = new HashSet<>();
        Mockito.when(abstractOrderEntryModel.getProduct()).thenReturn(abstractTargetVariantProductModel);
        targetZoneDeliveryModeModelList.add(deliveryModeDigital);
        targetZoneDeliveryModeModelList.add(deliveryModeHome);
        targetZoneDeliveryModeModelList.add(deliveryModeExpress);
        BDDMockito.given(targetDeliveryModeHelper.getDeliveryModesForProduct(abstractTargetVariantProductModel))
                .willReturn(targetZoneDeliveryModeModelList);
        Assert.assertTrue(targetDeliveryModeHelper.hasDigitalProducts(orderModel));
    }

    @Test
    public void testHasDigitalProductsContainsPhysicalProducts() {
        Mockito.when(orderModel.getEntries()).thenReturn(Collections.singletonList(abstractOrderEntryModel));

        final AbstractTargetVariantProductModel abstractTargetVariantProductModel = createTargetVariantProductModel(VARIANT_CODE);
        final TargetZoneDeliveryModeModel deliveryModeHome = createTargetZoneDeliveryModeModel(HOME_DELIVERY, 1);
        final TargetZoneDeliveryModeModel deliveryModeExpress = createTargetZoneDeliveryModeModel(EXPRESS_DELIVERY, 2);
        final Set<DeliveryModeModel> targetZoneDeliveryModeModelList = new HashSet<>();
        Mockito.when(abstractOrderEntryModel.getProduct()).thenReturn(abstractTargetVariantProductModel);
        targetZoneDeliveryModeModelList.add(deliveryModeHome);
        targetZoneDeliveryModeModelList.add(deliveryModeExpress);
        BDDMockito.given(targetDeliveryModeHelper.getDeliveryModesForProduct(abstractTargetVariantProductModel))
                .willReturn(targetZoneDeliveryModeModelList);
        Assert.assertFalse(targetDeliveryModeHelper.hasDigitalProducts(orderModel));
    }

    @Test
    public void testHasDigitalProductsContainsNullProducts() {
        Mockito.when(orderModel.getEntries()).thenReturn(null);

        Assert.assertFalse(targetDeliveryModeHelper.hasDigitalProducts(orderModel));
    }

    @Test
    public void testGetDeliveryModeUnknownCode() {

        final DeliveryModeModel mode = targetDeliveryModeHelper.getDeliveryModeModel("xxx");
        Assert.assertNull(mode);
    }

    @Test
    public void testProductForClickAndCollectGeneric() {

        final boolean available = targetDeliveryModeHelper.isProductAvailableForDeliveryMode(product, cnc);
        Assert.assertFalse(available);
    }

    @Test
    public void testProductForHomeDeliveryGeneric() {

        final boolean available = targetDeliveryModeHelper.isProductAvailableForDeliveryMode(product, home);

        Assert.assertTrue(available);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testNullProductForDeliveryGeneric() {
        targetDeliveryModeHelper.isProductAvailableForDeliveryMode(null, cnc);
        Assert.fail();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testProductForNullDeliveryGeneric() {
        targetDeliveryModeHelper.isProductAvailableForDeliveryMode(product, null);
        Assert.fail();
    }


    @Test
    public void testUpdateModesForHomeDeliveryTrue() {

        final Set<DeliveryModeModel> newModes = targetDeliveryModeHelper.getNewModes(product, home, true);

        // Home delivery was already an option so no change
        Assert.assertTrue(newModes.size() == 1);
        Assert.assertTrue(newModes.contains(home));
    }

    @Test
    public void testUpdateModesForHomeDeliveryFalse() {

        final Set<DeliveryModeModel> newModes = targetDeliveryModeHelper.getNewModes(product, home, false);

        // Home delivery was already an option so it should be removed
        Assert.assertTrue(newModes.isEmpty());
    }

    @Test
    public void testUpdateModesForClickAndCollectTrue() {

        final Set<DeliveryModeModel> newModes = targetDeliveryModeHelper.getNewModes(product, cnc, true);

        // Should add cnc to the set which already contains home
        Assert.assertTrue(newModes.size() == 2);
        Assert.assertTrue(newModes.contains(home) && newModes.contains(cnc));
    }

    @Test
    public void testUpdateModesForClickAndCollectFalse() {

        final Set<DeliveryModeModel> newModes = targetDeliveryModeHelper.getNewModes(product, cnc, false);

        // Shouldn't add cnc to the set which already contains home
        Assert.assertTrue(newModes.size() == 1);
        Assert.assertTrue(newModes.contains(home));
    }

    @Test
    public void testIsDeliveryModeStoreDelivery() {
        final boolean result = targetDeliveryModeHelper.isDeliveryModeStoreDelivery(cnc);
        Assert.assertTrue(result);
    }

    @Test
    public void testIsDeliveryModeStoreDeliveryNotCNC() {
        final boolean result = targetDeliveryModeHelper.isDeliveryModeStoreDelivery(home);
        Assert.assertFalse(result);
    }

    @Test
    public void testIsDeliveryModeApplicableForOrder() {
        final Map<String, DeliveryModeModel> deliveryModeModelListToSetup = new HashMap<String, DeliveryModeModel>();
        deliveryModeModelListToSetup.put(HOME_DELIVERY, createTargetZoneDeliveryModeModel(HOME_DELIVERY, 1));
        deliveryModeModelListToSetup.put(EXPRESS_DELIVERY, createTargetZoneDeliveryModeModel(EXPRESS_DELIVERY, 22));

        final CartModel cartModel = populateDummyCartData(deliveryModeModelListToSetup);

        final boolean result = targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                deliveryModeModelListToSetup.get(HOME_DELIVERY));
        Assert.assertTrue(result);
    }

    @Test
    public void testIsDeliveryModeApplicableForOrderWithDigitalDeliveryInCart() {
        List<DeliveryModeModel> deliveryModeModelListToSetup = new ArrayList<>();
        final TargetZoneDeliveryModeModel homeDelivery = createTargetZoneDeliveryModeModel(HOME_DELIVERY, 1);
        deliveryModeModelListToSetup.add(homeDelivery);
        deliveryModeModelListToSetup.add(createTargetZoneDeliveryModeModel(EXPRESS_DELIVERY, 22));

        final CartModel cartModel = new CartModel();
        populateDummyCartEntryInCart(cartModel, "ABC", deliveryModeModelListToSetup);
        deliveryModeModelListToSetup = new ArrayList<>();
        deliveryModeModelListToSetup.add(createTargetZoneDeliveryModeModel(DIGITAL_DELIVERY, 1));
        populateDummyCartEntryInCart(cartModel, "ABC2", deliveryModeModelListToSetup);

        final boolean result = targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                homeDelivery);
        Assert.assertTrue(result);
    }

    @Test
    public void testIsDeliveryModeApplicableForOrderWithOnlyDigitalDeliveryInCart() {
        final TargetZoneDeliveryModeModel homeDelivery = createTargetZoneDeliveryModeModel(HOME_DELIVERY, 1);

        final List<DeliveryModeModel> deliveryModeModelListToSetup = new ArrayList<>();

        final CartModel cartModel = new CartModel();
        deliveryModeModelListToSetup.add(createTargetZoneDeliveryModeModel(DIGITAL_DELIVERY, 1));
        populateDummyCartEntryInCart(cartModel, "ABC2", deliveryModeModelListToSetup);

        final boolean result = targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                homeDelivery);
        Assert.assertFalse(result);
    }

    @Test
    public void testIsDeliveryModeApplicableForOrderNegative() {
        final Map<String, DeliveryModeModel> deliveryModeModelListToSetup = new HashMap<String, DeliveryModeModel>();
        deliveryModeModelListToSetup.put(HOME_DELIVERY, createTargetZoneDeliveryModeModel(HOME_DELIVERY, 1));
        deliveryModeModelListToSetup.put(EXPRESS_DELIVERY, createTargetZoneDeliveryModeModel(EXPRESS_DELIVERY, 2));

        final DeliveryModeModel deliveryModeModelToVerify = createTargetZoneDeliveryModeModel(EXPRESS_DELIVERY, 2);
        final CartModel cartModel = populateDummyCartData(deliveryModeModelListToSetup);
        final boolean result = targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                deliveryModeModelToVerify);

        Assert.assertFalse(result);
    }

    @Test
    public void testGetDeliveryModesSortedForProduct() {
        final AbstractTargetVariantProductModel abstractTargetVariantProductModel = createTargetVariantProductModel(VARIANT_CODE);
        final TargetZoneDeliveryModeModel deliveryModeHD = createTargetZoneDeliveryModeModel(HOME_DELIVERY, 2);
        final TargetZoneDeliveryModeModel deliveryModeExpress = createTargetZoneDeliveryModeModel(EXPRESS_DELIVERY, 1);
        final Set<DeliveryModeModel> targetZoneDeliveryModeModelList = new HashSet<>();
        targetZoneDeliveryModeModelList.add(deliveryModeHD);
        targetZoneDeliveryModeModelList.add(deliveryModeExpress);
        BDDMockito.given(targetDeliveryModeHelper.getDeliveryModesForProduct(abstractTargetVariantProductModel))
                .willReturn(targetZoneDeliveryModeModelList);
        final List<DeliveryModeModel> results = targetDeliveryModeHelper
                .getDeliveryModesSortedForProduct(abstractTargetVariantProductModel);
        Assert.assertTrue(CollectionUtils.isNotEmpty(results));
        Assert.assertTrue(results.size() == 2);
        final DeliveryModeModel deliveryModel1 = results.get(0);
        Assert.assertTrue(deliveryModel1.getCode().equals(EXPRESS_DELIVERY));
        final DeliveryModeModel deliveryModel2 = results.get(1);
        Assert.assertTrue(deliveryModel2.getCode().equals(HOME_DELIVERY));
    }

    @Test
    public void testGetDeliveryModesSortedForProductWithMixedDeliveryModeTypes() {
        final AbstractTargetVariantProductModel abstractTargetVariantProductModel = createTargetVariantProductModel(VARIANT_CODE);
        final DeliveryModeModel deliveryModeHD = createDeliveryModeModel(HOME_DELIVERY);
        final TargetZoneDeliveryModeModel deliveryModeExpress = createTargetZoneDeliveryModeModel(EXPRESS_DELIVERY, 3);

        final Set<DeliveryModeModel> targetZoneDeliveryModeModelList = new HashSet<>();
        targetZoneDeliveryModeModelList.add(deliveryModeHD);
        targetZoneDeliveryModeModelList.add(deliveryModeExpress);
        BDDMockito.given(targetDeliveryModeHelper.getDeliveryModesForProduct(abstractTargetVariantProductModel))
                .willReturn(targetZoneDeliveryModeModelList);
        final List<DeliveryModeModel> results = targetDeliveryModeHelper
                .getDeliveryModesSortedForProduct(abstractTargetVariantProductModel);
        Assertions.assertThat(results).isNotNull().isNotEmpty().hasSize(2);
        Assertions.assertThat(results).onProperty("code").containsOnly(EXPRESS_DELIVERY, HOME_DELIVERY);
    }



    @Test
    public void testGetDeliveryModesSortedForProductWithJustDeliveryModeModelType() {
        final AbstractTargetVariantProductModel abstractTargetVariantProductModel = createTargetVariantProductModel(VARIANT_CODE);
        final DeliveryModeModel deliveryModeHD = createDeliveryModeModel(EXPRESS_DELIVERY);
        final DeliveryModeModel deliveryModeExpress = new DeliveryModeModel();
        deliveryModeExpress.setCode(HOME_DELIVERY);

        final Set<DeliveryModeModel> targetZoneDeliveryModeModelList = new HashSet<>();
        targetZoneDeliveryModeModelList.add(deliveryModeHD);
        targetZoneDeliveryModeModelList.add(deliveryModeExpress);
        BDDMockito.given(targetDeliveryModeHelper.getDeliveryModesForProduct(abstractTargetVariantProductModel))
                .willReturn(targetZoneDeliveryModeModelList);
        final List<DeliveryModeModel> results = targetDeliveryModeHelper
                .getDeliveryModesSortedForProduct(abstractTargetVariantProductModel);
        Assertions.assertThat(results).isNotNull().isNotEmpty().hasSize(2);
        Assertions.assertThat(results).onProperty("code").containsOnly(EXPRESS_DELIVERY, HOME_DELIVERY);
    }



    @Test
    public void testGetDeliveryModesSortedForProductEmptyList() {
        final AbstractTargetVariantProductModel abstractTargetVariantProductModel = createTargetVariantProductModel(VARIANT_CODE);
        final Set<DeliveryModeModel> targetZoneDeliveryModeModelList = new HashSet<>();
        BDDMockito.given(targetDeliveryModeHelper.getDeliveryModesForProduct(abstractTargetVariantProductModel))
                .willReturn(targetZoneDeliveryModeModelList);
        final List<DeliveryModeModel> results = targetDeliveryModeHelper
                .getDeliveryModesSortedForProduct(abstractTargetVariantProductModel);
        Assert.assertTrue(CollectionUtils.isEmpty(results));
    }

    @Test
    public void testGetDeliveryModesSortedForProductNullList() {
        final AbstractTargetVariantProductModel abstractTargetVariantProductModel = createTargetVariantProductModel(VARIANT_CODE);
        BDDMockito.given(targetDeliveryModeHelper.getDeliveryModesForProduct(abstractTargetVariantProductModel))
                .willReturn(null);
        final List<DeliveryModeModel> results = targetDeliveryModeHelper
                .getDeliveryModesSortedForProduct(abstractTargetVariantProductModel);
        Assert.assertTrue(CollectionUtils.isEmpty(results));
    }

    @Test
    public void testActiveDeliveryModesForProductWithColourVariant() {
        final TargetZoneDeliveryModeModel deliveryModeHD = createTargetZoneDeliveryModeModel(EXPRESS_DELIVERY, 1);

        final List<TargetZoneDeliveryModeModel> targetZoneDeliveryModeModelList = new ArrayList<>();
        targetZoneDeliveryModeModelList.add(deliveryModeHD);
        final AbstractTargetVariantProductModel abstractTargetVariantProductModel = createTargetColourVariantProductModel("colour");
        BDDMockito.given(
                targetDeliveryService.getAllApplicableDeliveryModesForProduct(Mockito.any(ProductModel.class)))
                .willReturn(targetZoneDeliveryModeModelList);

        final Collection<TargetZoneDeliveryModeModel> results = targetDeliveryModeHelper
                .getAllCurrentActiveDeliveryModesForProduct(abstractTargetVariantProductModel);

        Assertions.assertThat(results).isNotNull().isNotEmpty().hasSize(1);
    }

    @Test
    public void testActiveDeliveryModesForProductWithSizeVariant() {
        final TargetZoneDeliveryModeModel deliveryModeHD = createTargetZoneDeliveryModeModel(EXPRESS_DELIVERY, 1);

        final List<TargetZoneDeliveryModeModel> targetZoneDeliveryModeModelList = new ArrayList<>();
        targetZoneDeliveryModeModelList.add(deliveryModeHD);
        final AbstractTargetVariantProductModel abstractTargetVariantProductModel = createTargetSizeVariantProductModel("colour");
        BDDMockito.given(
                targetDeliveryService.getAllApplicableDeliveryModesForProduct(Mockito.any(ProductModel.class)))
                .willReturn(targetZoneDeliveryModeModelList);

        final Collection<TargetZoneDeliveryModeModel> results = targetDeliveryModeHelper
                .getAllCurrentActiveDeliveryModesForProduct(abstractTargetVariantProductModel);

        Assertions.assertThat(results).isNotNull().isNotEmpty().hasSize(1);
    }

    /**
     * Creates and Returns a dummy cart Model Containing 2 products
     * 
     * @return cartModel
     */
    private CartModel populateDummyCartData(final Map<String, DeliveryModeModel> deliveryModeModelListToSetup) {
        final CartModel cartModel = new CartModel();
        final List<AbstractOrderEntryModel> abstractOrderEntryModelList = new ArrayList<>();
        AbstractOrderEntryModel abstractOrderEntry = new AbstractOrderEntryModel();
        AbstractTargetVariantProductModel abstractTargetVariantProductModel = new AbstractTargetVariantProductModel();
        abstractTargetVariantProductModel.setCode(VARIANT_CODE);

        Set<DeliveryModeModel> targetZoneDeliveryModeModelList = new HashSet<>();
        targetZoneDeliveryModeModelList.add(deliveryModeModelListToSetup.get(HOME_DELIVERY));
        targetZoneDeliveryModeModelList.add(deliveryModeModelListToSetup.get(EXPRESS_DELIVERY));
        BDDMockito.given(targetDeliveryModeHelper.getDeliveryModesForProduct(abstractTargetVariantProductModel))
                .willReturn(targetZoneDeliveryModeModelList);

        BDDMockito.doReturn(Boolean.TRUE).when(
                targetDeliveryModeHelper).isProductAvailableForDeliveryMode(abstractTargetVariantProductModel,
                deliveryModeModelListToSetup.get(HOME_DELIVERY)
                );

        BDDMockito.doReturn(Boolean.TRUE).when(
                targetDeliveryModeHelper).isProductAvailableForDeliveryMode(abstractTargetVariantProductModel,
                deliveryModeModelListToSetup.get(EXPRESS_DELIVERY)
                );

        abstractOrderEntry.setProduct(abstractTargetVariantProductModel);

        abstractOrderEntryModelList.add(abstractOrderEntry);

        abstractOrderEntry = new AbstractOrderEntryModel();
        abstractTargetVariantProductModel = new AbstractTargetVariantProductModel();
        abstractTargetVariantProductModel.setCode("ABC2");

        targetZoneDeliveryModeModelList = new HashSet<>();
        targetZoneDeliveryModeModelList.add(createTargetZoneDeliveryModeModel(HOME_DELIVERY, 1));
        BDDMockito.doReturn(Boolean.TRUE).when(
                targetDeliveryModeHelper).isProductAvailableForDeliveryMode(abstractTargetVariantProductModel,
                deliveryModeModelListToSetup.get(HOME_DELIVERY)
                );

        BDDMockito.doReturn(Boolean.TRUE).when(
                targetDeliveryModeHelper).isProductAvailableForDeliveryMode(abstractTargetVariantProductModel,
                deliveryModeModelListToSetup.get(EXPRESS_DELIVERY)
                );
        BDDMockito.given(targetDeliveryModeHelper.getDeliveryModesForProduct(abstractTargetVariantProductModel))
                .willReturn(targetZoneDeliveryModeModelList);

        abstractOrderEntry.setProduct(abstractTargetVariantProductModel);
        abstractOrderEntryModelList.add(abstractOrderEntry);

        cartModel.setEntries(abstractOrderEntryModelList);
        return cartModel;
    }

    private CartModel populateDummyCartEntryInCart(final CartModel cartModel, final String productCode,
            final List<DeliveryModeModel> deliveryModeModelListToSetup) {

        List<AbstractOrderEntryModel> abstractOrderEntryModelList = cartModel.getEntries();
        if (abstractOrderEntryModelList == null) {
            abstractOrderEntryModelList = new ArrayList<>();
        }
        final AbstractOrderEntryModel abstractOrderEntry = new AbstractOrderEntryModel();
        final AbstractTargetVariantProductModel abstractTargetVariantProductModel = new AbstractTargetVariantProductModel();
        abstractTargetVariantProductModel.setCode(productCode);

        final Set<DeliveryModeModel> targetZoneDeliveryModeModelList = new HashSet<>();
        if (CollectionUtils.isNotEmpty(deliveryModeModelListToSetup)) {
            for (final DeliveryModeModel deliveryMode : deliveryModeModelListToSetup) {
                targetZoneDeliveryModeModelList.add(deliveryMode);
                BDDMockito.doReturn(Boolean.TRUE).when(
                        targetDeliveryModeHelper)
                        .isProductAvailableForDeliveryMode(abstractTargetVariantProductModel, deliveryMode);
            }
        }
        BDDMockito.given(targetDeliveryModeHelper.getDeliveryModesForProduct(abstractTargetVariantProductModel))
                .willReturn(targetZoneDeliveryModeModelList);


        abstractOrderEntry.setProduct(abstractTargetVariantProductModel);

        abstractOrderEntryModelList.add(abstractOrderEntry);

        cartModel.setEntries(abstractOrderEntryModelList);
        return cartModel;
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsDeliveryModeDigitalNullDeliveryMode() {
        targetDeliveryModeHelper.isDeliveryModeDigital(null);
    }

    @Test
    public void testIsDeliveryModeDigitalNonDigitalDeliveryMode() {
        final DeliveryModeModel deliveryModeHD = createDeliveryModeModel(HOME_DELIVERY);
        Assert.assertFalse(targetDeliveryModeHelper.isDeliveryModeDigital(deliveryModeHD));
    }

    @Test
    public void testIsDeliveryModeDigitalDigitalDeliveryMode() {
        final DeliveryModeModel deliveryModeDigital = createDigitalTargetZoneDeliveryModeModel(DIGITAL_DELIVERY, 1);
        Assert.assertTrue(targetDeliveryModeHelper.isDeliveryModeDigital(deliveryModeDigital));
    }


    @Test
    public void testIsDeliveryModeDigitalDeliveryModeModel() {
        final DeliveryModeModel deliveryModeHD = new DeliveryModeModel();
        Assert.assertFalse(targetDeliveryModeHelper.isDeliveryModeDigital(deliveryModeHD));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testHasDeliveryModeDigitalProductNull() {
        targetDeliveryModeHelper.getDigitalDeliveryMode(null);
    }

    @Test
    public void testHasDeliveryModeDigitalEmptyDeliveryModes() {
        final AbstractTargetVariantProductModel cvproduct = createTargetColourVariantProductModel("123445");
        final Set<DeliveryModeModel> targetZoneDeliveryModeModelList = new HashSet<>();
        cvproduct.setDeliveryModes(targetZoneDeliveryModeModelList);
        Assert.assertNull(targetDeliveryModeHelper.getDigitalDeliveryMode(cvproduct));
    }

    @Test
    public void testHasDeliveryModeDigitalNullDeliveryModes() {
        final AbstractTargetVariantProductModel cvproduct = createTargetColourVariantProductModel("123445");
        Assert.assertNull(targetDeliveryModeHelper.getDigitalDeliveryMode(cvproduct));
    }


    @Test
    public void testHasDeliveryModeDigitalNonDigitalDeliveryModes() {
        final DeliveryModeModel deliveryModeHD = createDeliveryModeModel(HOME_DELIVERY);
        final TargetZoneDeliveryModeModel deliveryModeExpress = createTargetZoneDeliveryModeModel(EXPRESS_DELIVERY, 3);
        modes.add(deliveryModeHD);
        modes.add(deliveryModeExpress);
        Assert.assertNull(targetDeliveryModeHelper.getDigitalDeliveryMode(product));
    }

    @Test
    public void testHasDeliveryModeDigitalDeliveryModes() {
        final DeliveryModeModel deliveryModeHD = createDeliveryModeModel(HOME_DELIVERY);
        final TargetZoneDeliveryModeModel deliveryModeExpress = createTargetZoneDeliveryModeModel(EXPRESS_DELIVERY, 3);
        final TargetZoneDeliveryModeModel deliveryModeDigital = createDigitalTargetZoneDeliveryModeModel(
                DIGITAL_DELIVERY, 3);
        modes.add(deliveryModeHD);
        modes.add(deliveryModeExpress);
        modes.add(deliveryModeDigital);
        final DeliveryModeModel digitalDeliveryMode = targetDeliveryModeHelper.getDigitalDeliveryMode(product);
        Assert.assertNotNull(digitalDeliveryMode);
    }

    @Test
    public void testDoesProductHavePhysicalDeliveryMode() {
        final DeliveryModeModel deliveryModeHD = createDeliveryModeModel(HOME_DELIVERY);
        final TargetZoneDeliveryModeModel deliveryModeExpress = createTargetZoneDeliveryModeModel(EXPRESS_DELIVERY, 3);
        final TargetZoneDeliveryModeModel deliveryModeDigital = createDigitalTargetZoneDeliveryModeModel(
                DIGITAL_DELIVERY, 3);
        modes.add(deliveryModeHD);
        modes.add(deliveryModeExpress);
        modes.add(deliveryModeDigital);
        Mockito.when(targetDeliveryService.getVariantsDeliveryModesFromBaseProduct(variant)).thenReturn(modes);
        final boolean hasPysicalDeliveryMode = targetDeliveryModeHelper.doesProductHavePhysicalDeliveryMode(variant);
        Assert.assertTrue(hasPysicalDeliveryMode);
    }

    @Test
    public void testDoesProductHavePhysicalDeliveryModeHasOnlyDigital() {
        modes = new HashSet<>();
        final TargetZoneDeliveryModeModel deliveryModeDigital = createDigitalTargetZoneDeliveryModeModel(
                DIGITAL_DELIVERY, 3);
        modes.add(deliveryModeDigital);
        Mockito.when(targetDeliveryService.getVariantsDeliveryModesFromBaseProduct(variant)).thenReturn(modes);
        final boolean hasPysicalDeliveryMode = targetDeliveryModeHelper.doesProductHavePhysicalDeliveryMode(variant);
        Assert.assertFalse(hasPysicalDeliveryMode);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDoesProductHaveOnlyDigitalDeliveryModeWithProductNull() {
        targetDeliveryModeHelper.hasOnlyDigitalDeliveryMode(null);
    }

    @Test
    public void testDoesProductHaveOnlyDigitalDeliveryModeWithNonDigitalInProduct() {
        final DeliveryModeModel deliveryModeHD = createDeliveryModeModel(HOME_DELIVERY);
        final TargetZoneDeliveryModeModel deliveryModeExpress = createTargetZoneDeliveryModeModel(EXPRESS_DELIVERY, 3);
        final TargetZoneDeliveryModeModel deliveryModeDigital = createDigitalTargetZoneDeliveryModeModel(
                DIGITAL_DELIVERY, 3);
        modes.add(deliveryModeHD);
        modes.add(deliveryModeExpress);
        modes.add(deliveryModeDigital);
        Mockito.when(targetDeliveryService.getVariantsDeliveryModesFromBaseProduct(variant)).thenReturn(modes);
        final boolean hasPysicalDeliveryMode = targetDeliveryModeHelper.hasOnlyDigitalDeliveryMode(variant);
        Assert.assertFalse(hasPysicalDeliveryMode);
    }

    @Test
    public void testDoesProductHaveOnlyDigital() {
        modes = new HashSet<>();
        final TargetZoneDeliveryModeModel deliveryModeDigital = createDigitalTargetZoneDeliveryModeModel(
                DIGITAL_DELIVERY, 3);
        modes.add(deliveryModeDigital);
        Mockito.when(targetDeliveryService.getVariantsDeliveryModesFromBaseProduct(variant)).thenReturn(modes);
        final boolean hasPysicalDeliveryMode = targetDeliveryModeHelper.hasOnlyDigitalDeliveryMode(variant);
        Assert.assertTrue(hasPysicalDeliveryMode);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDoesProductHavePhysicalDeliveryModeNullProduct() {
        targetDeliveryModeHelper.doesProductHavePhysicalDeliveryMode(null);
    }

    /**
     * @return AbstractTargetVariantProductModel
     */
    public AbstractTargetVariantProductModel createTargetVariantProductModel(final String code) {
        final AbstractTargetVariantProductModel abstractTargetVariantProductModel = new AbstractTargetVariantProductModel();
        abstractTargetVariantProductModel.setCode(code);
        return abstractTargetVariantProductModel;
    }

    public AbstractTargetVariantProductModel createTargetColourVariantProductModel(final String code) {
        final AbstractTargetVariantProductModel colourVariantModel = new TargetColourVariantProductModel();
        colourVariantModel.setCode(code);
        final ProductModel baseProduct = new ProductModel();
        baseProduct.setCode("base");
        colourVariantModel.setBaseProduct(baseProduct);
        return colourVariantModel;
    }

    public AbstractTargetVariantProductModel createTargetSizeVariantProductModel(final String code) {
        final AbstractTargetVariantProductModel sizeVariantModel = new TargetSizeVariantProductModel();
        sizeVariantModel.setCode(code);
        final AbstractTargetVariantProductModel colourVariant = createTargetColourVariantProductModel("colour");
        sizeVariantModel.setBaseProduct(colourVariant);
        return sizeVariantModel;
    }

    private TargetZoneDeliveryModeModel createDigitalTargetZoneDeliveryModeModel(final String code, final int order) {
        final TargetZoneDeliveryModeModel deliveryModeModelPdt = createTargetZoneDeliveryModeModel(code, order);
        deliveryModeModelPdt.setIsDigital(Boolean.TRUE);
        return deliveryModeModelPdt;
    }


    /**
     * Creates and Returns dummy DeliveryMode for the code
     * 
     * @param code
     * @return deliveryModeModelPdt
     */
    private TargetZoneDeliveryModeModel createTargetZoneDeliveryModeModel(final String code, final int order) {
        final TargetZoneDeliveryModeModel deliveryModeModelPdt = new TargetZoneDeliveryModeModel();
        deliveryModeModelPdt.setCode(code);
        deliveryModeModelPdt.setDisplayOrder(Integer.valueOf(order));
        if (DIGITAL_DELIVERY.equals(code)) {
            deliveryModeModelPdt.setIsDigital(Boolean.TRUE);
        }
        return deliveryModeModelPdt;
    }

    /**
     * @param code
     * @return DeliveryModeModel
     */
    public DeliveryModeModel createDeliveryModeModel(final String code) {
        final DeliveryModeModel deliveryModeHD = new DeliveryModeModel();
        deliveryModeHD.setCode(code);
        return deliveryModeHD;
    }
}