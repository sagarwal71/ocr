package au.com.target.tgtcore.giftcard;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.giftcards.impl.GiftCardServiceImpl;
import au.com.target.tgtcore.model.GiftCardModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.product.dao.GiftCardDao;
import au.com.target.tgtcore.product.exception.ProductNotFoundException;


/**
 * @author smishra1
 *
 */
@UnitTest
public class GiftCardServiceImplTest {

    @Mock
    private GiftCardDao giftCardDao;

    @Mock
    private ModelService modelService;

    @Mock
    private ProductService productService;

    @InjectMocks
    private final GiftCardServiceImpl giftCardService = new GiftCardServiceImpl();

    @Mock
    private CartModel cart;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        final GiftCardModel giftCardModel = new GiftCardModel();
        given(modelService.create(GiftCardModel.class)).willReturn(giftCardModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetGiftCardForNullName() {
        giftCardService.getGiftCard(null);
    }

    @Test
    public void testGetGiftCardForValidBrandid() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final GiftCardModel giftCard = new GiftCardModel();
        giftCard.setBrandName("DummyGiftCard");
        giftCard.setBrandId("DummyId");
        given(giftCardDao.getGiftCardByBrandId("DummyId")).willReturn(giftCard);
        final GiftCardModel result = giftCardService.getGiftCard("DummyId");

        assertThat(result).isNotNull();
        assertThat(giftCard.getBrandName()).isEqualTo(result.getBrandName());
    }

    @Test
    public void testGiftCardForNotValidBrandId() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final GiftCardModel giftCard = new GiftCardModel();
        giftCard.setBrandName("DummyGiftCard");
        giftCard.setBrandId("DummyId");
        given(giftCardDao.getGiftCardByBrandId("DummyId")).willThrow(
                new TargetUnknownIdentifierException("Error while getting gift card details for NotValidBrandId"));
        given(modelService.create(GiftCardModel.class)).willReturn(giftCard);
        final GiftCardModel result = giftCardService.getGiftCard("NotValidBrandId");

        assertThat(result).isNull();
    }

    @Test
    public void testForMoreThanOneRecordForGivenBrandId() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final GiftCardModel giftCard = new GiftCardModel();
        giftCard.setBrandName("DummyGiftCard");
        giftCard.setBrandId("DummyId");

        given(giftCardDao.getGiftCardByBrandId("DummyId")).willThrow(
                new TargetAmbiguousIdentifierException(
                        "More than one giftCard found for the brandId : brandIdWithMultipleRecords"));
        final GiftCardModel result = giftCardService.getGiftCard("brandIdWithMultipleRecords");

        assertThat(result).isNull();
    }

    @Test
    public void testGetGiftCardForProductForABaseProductCode() throws ProductNotFoundException {
        final String productCode = "PGC1000";
        final GiftCardModel giftCard = new GiftCardModel();
        final TargetProductModel productModel = new TargetProductModel();
        productModel.setGiftCard(giftCard);
        given(productService.getProductForCode(productCode)).willReturn(productModel);
        final GiftCardModel result = giftCardService.getGiftCardForProduct(productCode);
        assertThat(result).isNotNull();
    }

    @Test
    public void testGetGiftCardForProductForAColourVariantProductCode() throws ProductNotFoundException {
        final String productCode = "PGC1000_iTunes";
        final GiftCardModel giftCard = new GiftCardModel();
        giftCard.setBrandId("appletest1234");
        final TargetColourVariantProductModel colourVariantModel = new TargetColourVariantProductModel();
        final TargetProductModel productModel = new TargetProductModel();
        productModel.setGiftCard(giftCard);
        colourVariantModel.setBaseProduct(productModel);
        given(productService.getProductForCode(productCode)).willReturn(colourVariantModel);
        final GiftCardModel result = giftCardService.getGiftCardForProduct(productCode);
        assertThat(result).isNotNull();
        assertThat("appletest1234").isEqualTo(result.getBrandId());
    }

    @Test
    public void testGetGiftCardForProductForASizeVariantProductCode() throws ProductNotFoundException {
        final String productCode = "PGC1000_iTunes_50";
        final TargetProductModel productModel = createMockProduct(true);
        final VariantProductModel sizeVariantModel = new TargetSizeVariantProductModel();
        final TargetColourVariantProductModel colourVariantModel = new TargetColourVariantProductModel();
        sizeVariantModel.setBaseProduct(colourVariantModel);
        colourVariantModel.setBaseProduct(productModel);
        given(productService.getProductForCode(productCode)).willReturn(sizeVariantModel);
        final GiftCardModel result = giftCardService.getGiftCardForProduct(productCode);
        assertThat(result).isNotNull();
        assertThat("appletest1234").isEqualTo(result.getBrandId());
    }

    @Test(expected = ProductNotFoundException.class)
    public void testGetGiftCardForProductWhenUnknownIdentifierExceptionThrown() throws ProductNotFoundException {
        given(productService.getProductForCode("productCode"))
                .willThrow(new UnknownIdentifierException("UnknownIdentifierException"));
        giftCardService.getGiftCardForProduct("productCode");
    }

    @Test(expected = ProductNotFoundException.class)
    public void testGetGiftCardForProductWhenAmbiguousIdentifierException() throws ProductNotFoundException {
        given(productService.getProductForCode("productCode"))
                .willThrow(new AmbiguousIdentifierException("AmbiguousIdentifierException"));
        giftCardService.getGiftCardForProduct("productCode");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetGiftCardForProductWhenProductCodeIsNull() throws ProductNotFoundException {
        giftCardService.getGiftCardForProduct(null);
    }

    @Test
    public void testIfProductIsGiftCardSizeVariant() {

        final TargetProductModel productModel = createMockProduct(true);
        final VariantProductModel sizeVariantModel = new TargetSizeVariantProductModel();
        final TargetColourVariantProductModel colourVariantModel = new TargetColourVariantProductModel();
        sizeVariantModel.setBaseProduct(colourVariantModel);
        colourVariantModel.setBaseProduct(productModel);

        assertThat(giftCardService.isProductAGiftCard(sizeVariantModel)).isTrue();
    }

    @Test
    public void testIfProductIsGiftCardColourVariant() {

        final GiftCardModel giftCard = new GiftCardModel();
        giftCard.setBrandId("appletest1234");
        final TargetColourVariantProductModel colourVariantModel = new TargetColourVariantProductModel();
        final TargetProductModel productModel = new TargetProductModel();
        productModel.setGiftCard(giftCard);
        colourVariantModel.setBaseProduct(productModel);

        assertThat(giftCardService.isProductAGiftCard(colourVariantModel)).isTrue();
    }

    @Test
    public void testIfProductIsGiftCardBaseProduct() {

        final GiftCardModel giftCard = new GiftCardModel();
        giftCard.setBrandId("appletest1234");
        final TargetProductModel productModel = new TargetProductModel();
        productModel.setGiftCard(giftCard);

        assertThat(giftCardService.isProductAGiftCard(productModel)).isTrue();
    }


    @Test
    public void testIfProductIsGiftCardSizeVariantNoGiftCard() {

        final VariantProductModel sizeVariantModel = new TargetSizeVariantProductModel();
        final TargetColourVariantProductModel colourVariantModel = new TargetColourVariantProductModel();
        sizeVariantModel.setBaseProduct(colourVariantModel);
        final TargetProductModel productModel = new TargetProductModel();
        colourVariantModel.setBaseProduct(productModel);

        assertThat(giftCardService.isProductAGiftCard(sizeVariantModel)).isFalse();
    }

    @Test
    public void testIfProductIsGiftCardColourVariantNoGiftCard() {

        final TargetColourVariantProductModel colourVariantModel = new TargetColourVariantProductModel();
        final TargetProductModel productModel = new TargetProductModel();
        colourVariantModel.setBaseProduct(productModel);

        assertThat(giftCardService.isProductAGiftCard(colourVariantModel)).isFalse();
    }

    @Test
    public void testIfProductIsGiftCardBaseProductNoGiftCard() {

        final TargetProductModel productModel = new TargetProductModel();

        assertThat(giftCardService.isProductAGiftCard(productModel)).isFalse();
    }

    @Test
    public void testDoesCartHaveAGiftCardWhenCartDoesntContainEntries() {
        BDDMockito.when(cart.getEntries()).thenReturn(Collections.EMPTY_LIST);
        assertThat(giftCardService.doesCartHaveAGiftCard(cart)).isFalse();
    }

    @Test
    public void testDoesCartHaveAGiftCardWhenCartEntriesAreNull() {
        BDDMockito.when(cart.getEntries()).thenReturn(null);
        assertThat(giftCardService.doesCartHaveAGiftCard(cart)).isFalse();
    }

    @Test
    public void testDoesCartHaveAGiftCardWhenCartContainsGiftCardProduct() {

        final TargetProductModel productModel = createMockProduct(true);
        mockCartEntries(productModel, null);

        assertThat(giftCardService.doesCartHaveAGiftCard(cart)).isTrue();
    }

    @Test
    public void testDoesCartHaveAGiftCardWhenCartDoesntContainGiftCard() {

        final TargetProductModel productModel = createMockProduct(false);
        mockCartEntries(productModel, null);

        assertThat(giftCardService.doesCartHaveAGiftCard(cart)).isFalse();
    }

    @Test
    public void testDoesCartHaveAGiftCardWhenCartIsEmpty() {

        assertThat(giftCardService.doesCartHaveAGiftCard(null)).isFalse();
    }

    @Test
    public void testDoesCartHaveAGiftCardWhenCartContainsMixedProducts() {

        final TargetProductModel entryOneProduct = createMockProduct(false);
        final TargetProductModel entryTwoProduct = createMockProduct(true);
        mockCartEntries(entryOneProduct, entryTwoProduct);

        assertThat(giftCardService.doesCartHaveAGiftCard(cart)).isTrue();
    }

    @Test
    public void testDoesCartHaveAGiftCardWhenCartContainsOnlyNormalProducts() {

        final TargetProductModel entryOneProduct = createMockProduct(false);
        final TargetProductModel entryTwoProduct = createMockProduct(false);
        mockCartEntries(entryOneProduct, entryTwoProduct);

        assertThat(giftCardService.doesCartHaveAGiftCard(cart)).isFalse();
    }

    @Test
    public void testDoesCartHaveGiftCardsOnlyWhenCartDoesntContainEntries() {
        BDDMockito.when(cart.getEntries()).thenReturn(Collections.EMPTY_LIST);
        assertThat(giftCardService.doesCartHaveGiftCardsOnly(cart)).isFalse();
    }

    @Test
    public void testDoesCartHaveGiftCardsOnlyWhenCartEntriesAreNull() {
        BDDMockito.when(cart.getEntries()).thenReturn(null);
        assertThat(giftCardService.doesCartHaveGiftCardsOnly(cart)).isFalse();
    }

    @Test
    public void testDoesCartHaveDigitalGiftCardsOnlyWhenCartHasOneGiftCard() {
        final TargetProductModel entryOneProduct = createMockProduct(true);
        final ProductTypeModel productType = new ProductTypeModel();
        productType.setCode("digital");
        entryOneProduct.setProductType(productType);
        mockCartEntries(entryOneProduct, null);

        assertThat(giftCardService.doesCartHaveGiftCardsOnly(cart)).isTrue();
    }

    @Test
    public void testDoesCartHaveGiftCardsOnlyWhenCartHasTwoGiftCards() {
        final TargetProductModel entryOneProduct = createMockProduct(true);
        final TargetProductModel entryTwoProduct = createMockProduct(true);
        final ProductTypeModel productType = new ProductTypeModel();
        productType.setCode("digital");
        entryOneProduct.setProductType(productType);
        entryTwoProduct.setProductType(productType);

        mockCartEntries(entryOneProduct, entryTwoProduct);

        assertThat(giftCardService.doesCartHaveGiftCardsOnly(cart)).isTrue();
    }

    @Test
    public void testDoesCartHaveGiftCardsOnlyWhenCartHasNonGiftCard() {
        final TargetProductModel entryOneProduct = createMockProduct(false);
        mockCartEntries(entryOneProduct, null);

        assertThat(giftCardService.doesCartHaveGiftCardsOnly(cart)).isFalse();
    }

    @Test
    public void testDoesCartHaveGiftCardsOnlyWhenCartHasMixedProducts() {
        final TargetProductModel entryOneProduct = createMockProduct(false);
        final TargetProductModel entryTwoProduct = createMockProduct(true);
        final ProductTypeModel productType = new ProductTypeModel();
        productType.setCode("digital");
        entryTwoProduct.setProductType(productType);
        mockCartEntries(entryOneProduct, entryTwoProduct);

        assertThat(giftCardService.doesCartHaveGiftCardsOnly(cart)).isFalse();
    }

    /**
     * Method to mock cart entries
     * 
     * @param entryOneProduct
     * @param entryTwoProduct
     */
    private void mockCartEntries(final TargetProductModel entryOneProduct, final TargetProductModel entryTwoProduct) {
        final List<AbstractOrderEntryModel> cartEntries = new ArrayList<>();
        if (entryOneProduct != null) {
            final AbstractOrderEntryModel cartEntryOne = Mockito.mock(AbstractOrderEntryModel.class);
            cartEntries.add(cartEntryOne);
            BDDMockito.when(cartEntryOne.getProduct()).thenReturn(entryOneProduct);
        }
        if (entryTwoProduct != null) {
            final AbstractOrderEntryModel cartEntryTwo = Mockito.mock(AbstractOrderEntryModel.class);
            cartEntries.add(cartEntryTwo);
            BDDMockito.when(cartEntryTwo.getProduct()).thenReturn(entryTwoProduct);
        }
        BDDMockito.when(cart.getEntries()).thenReturn(cartEntries);
    }

    /**
     * Method to create a mock base product either gift card or non gift card
     * 
     * @return TargetProductModel
     */
    private TargetProductModel createMockProduct(final boolean requireGiftCardProduct) {
        if (requireGiftCardProduct) {
            final GiftCardModel giftCard = new GiftCardModel();
            giftCard.setBrandId("appletest1234");
            final TargetProductModel productModel = new TargetProductModel();
            productModel.setGiftCard(giftCard);
            return productModel;
        }
        return new TargetProductModel();
    }
}
