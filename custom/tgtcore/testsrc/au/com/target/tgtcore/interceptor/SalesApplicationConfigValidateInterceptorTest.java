/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.model.SalesApplicationConfigModel;


/**
 * Test class for SalesApplicationConfigValidateInterceptor
 * 
 * @author Trinadh N
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SalesApplicationConfigValidateInterceptorTest {

    @InjectMocks
    private final SalesApplicationConfigValidateInterceptor salesApplicationConfigValidateInterceptor = new SalesApplicationConfigValidateInterceptor();

    @Mock
    private InterceptorContext ctx;

    @Mock
    private TargetDeliveryService targetDeliveryService;

    @Mock
    private TargetZoneDeliveryModeModel eBayTestDelivery1;

    @Mock
    private TargetZoneDeliveryModeModel eBayTestDelivery2;

    @Mock
    private TargetZoneDeliveryModeModel tradeMeDelivery;

    @Mock
    private TargetZoneDeliveryModeModel expressDelivey;

    @Mock
    private SalesApplicationConfigModel salesApplicationConfigModel;


    private Collection<TargetZoneDeliveryModeModel> allCurrentlyActiveDeliveryModesforEbay;

    private Collection<TargetZoneDeliveryModeModel> allCurrentlyActiveDeliveryModesforTradeMe;

    private Set<TargetZoneDeliveryModeModel> skipPartnerInventoryUpdateForDelModesEbay;

    private Set<TargetZoneDeliveryModeModel> skipPartnerInventoryUpdateForDelModesWeb;

    private Set<TargetZoneDeliveryModeModel> skipPartnerInventoryUpdateForDelModesTradeMe;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        allCurrentlyActiveDeliveryModesforEbay = new HashSet<>();
        allCurrentlyActiveDeliveryModesforEbay.add(eBayTestDelivery1);
        allCurrentlyActiveDeliveryModesforEbay.add(eBayTestDelivery2);

        allCurrentlyActiveDeliveryModesforTradeMe = new HashSet<>();
        allCurrentlyActiveDeliveryModesforTradeMe.add(tradeMeDelivery);

        skipPartnerInventoryUpdateForDelModesEbay = new HashSet<>();
        skipPartnerInventoryUpdateForDelModesEbay.add(eBayTestDelivery1);

        skipPartnerInventoryUpdateForDelModesTradeMe = new HashSet<>();
        skipPartnerInventoryUpdateForDelModesTradeMe.add(tradeMeDelivery);

        skipPartnerInventoryUpdateForDelModesWeb = new HashSet<>();
        skipPartnerInventoryUpdateForDelModesWeb.add(expressDelivey);

        Mockito.when(targetDeliveryService.getAllCurrentlyActiveDeliveryModes(SalesApplication.EBAY))
                .thenReturn(allCurrentlyActiveDeliveryModesforEbay);

        Mockito.when(targetDeliveryService.getAllCurrentlyActiveDeliveryModes(SalesApplication.TRADEME))
                .thenReturn(allCurrentlyActiveDeliveryModesforTradeMe);

        Mockito.when(targetDeliveryService.getAllCurrentlyActiveDeliveryModes(SalesApplication.WEB))
                .thenReturn(null);
    }


    @Test(expected = InterceptorException.class)
    public void testOnAddingInvalidDelModes() throws InterceptorException {

        BDDMockito.given(salesApplicationConfigModel.getSalesApplication())
                .willReturn(SalesApplication.EBAY);

        BDDMockito.given(salesApplicationConfigModel.getSkipPartnerInventoryUpdateForDelModes())
                .willReturn(skipPartnerInventoryUpdateForDelModesTradeMe);

        salesApplicationConfigValidateInterceptor.onValidate(salesApplicationConfigModel, ctx);
    }


    @Test
    public void testOnAddingValidDelModes() {
        try {

            BDDMockito.given(salesApplicationConfigModel.getSalesApplication())
                    .willReturn(SalesApplication.EBAY);

            BDDMockito.given(salesApplicationConfigModel.getSkipPartnerInventoryUpdateForDelModes())
                    .willReturn(skipPartnerInventoryUpdateForDelModesEbay);

            salesApplicationConfigValidateInterceptor.onValidate(salesApplicationConfigModel, ctx);
            Assert.assertTrue("InterceptorException is not thrown", true);
        }
        catch (final InterceptorException ie) {
            Assert.assertFalse("InterceptorException is thrown", true);
        }
    }

    @Test
    public void testOnNotAddingAnyDelModes() {
        try {

            BDDMockito.given(salesApplicationConfigModel.getSalesApplication())
                    .willReturn(SalesApplication.EBAY);

            BDDMockito.given(salesApplicationConfigModel.getSkipPartnerInventoryUpdateForDelModes())
                    .willReturn(null);

            salesApplicationConfigValidateInterceptor.onValidate(salesApplicationConfigModel, ctx);
            Assert.assertTrue("InterceptorException is not thrown", true);
        }
        catch (final InterceptorException ie) {
            Assert.assertFalse("InterceptorException is thrown", true);
        }
    }

    @Test
    public void testOnAddingDelModesWhenNoAvailableDelModes() {
        try {

            BDDMockito.given(salesApplicationConfigModel.getSalesApplication())
                    .willReturn(SalesApplication.WEB);

            BDDMockito.given(salesApplicationConfigModel.getSkipPartnerInventoryUpdateForDelModes())
                    .willReturn(skipPartnerInventoryUpdateForDelModesWeb);

            salesApplicationConfigValidateInterceptor.onValidate(salesApplicationConfigModel, ctx);
            Assert.assertFalse("InterceptorException is not thrown", true);
        }
        catch (final InterceptorException ie) {
            Assert.assertTrue("InterceptorException is thrown", true);
        }
    }
}
