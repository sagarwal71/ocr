/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneModel;
import de.hybris.platform.order.ZoneDeliveryModeService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.model.TargetZoneDeliveryModeValueModel;


/**
 * Test for {@link TargetZoneDeliveryModeValueValidator}.
 */
public class TargetZoneDeliveryModeValueValidatorTest extends ServicelayerTransactionalTest {
    private static final Date NOW = new Date();
    private static final Date YESTERDAY = DateUtils.addDays(NOW, -1);
    private static final Date TOMORROW = DateUtils.addDays(NOW, 1);

    @Resource
    private TargetZoneDeliveryModeValueValidator targetZoneDeliveryModeValueValidator;

    @Resource
    private ModelService modelService;

    @Resource
    private ZoneDeliveryModeService zoneDeliveryModeService;

    @Resource
    private CommonI18NService commonI18NService;

    private ZoneDeliveryModeModel dhlZoneDeliveryMode;
    private ZoneModel deZone;
    private CurrencyModel eurCurrency;

    /**
     * Creates the core data, and necessary data for delivery modes.
     */
    @Before
    public void setUp() throws Exception {
        createCoreData();
        importCsv("/tgtcore/test/testDeliveryMode.impex", "windows-1252");
        dhlZoneDeliveryMode = (ZoneDeliveryModeModel)zoneDeliveryModeService.getDeliveryModeForCode("dhl");
        deZone = zoneDeliveryModeService.getZoneForCode("de");
        eurCurrency = commonI18NService.getCurrency("EUR");
    }

    @Test(expected = InterceptorException.class)
    public void testZoneDeliveryModeValueValidatorDuplicate() throws Exception {
        final Double min = Double.valueOf(0);
        final Double value = Double.valueOf(5);
        final ZoneModel duplicateZone = zoneDeliveryModeService.getZoneForCode("duplicate_zone");

        final TargetZoneDeliveryModeValueModel zoneDeliveryModeValue = testZoneDeliveryModeValueValidator(eurCurrency,
                min, value, duplicateZone, dhlZoneDeliveryMode, null, null);
        targetZoneDeliveryModeValueValidator.onValidate(zoneDeliveryModeValue, null);
    }

    @Test(expected = InterceptorException.class)
    public void testZoneDeliveryModeValueValidatorExistingMinimum() throws Exception {
        final Double min = Double.valueOf(0);
        final Double value = Double.valueOf(5);

        final TargetZoneDeliveryModeValueModel zoneDeliveryModeValue =
                testZoneDeliveryModeValueValidator(eurCurrency, min, value, deZone, dhlZoneDeliveryMode, null, null);
        targetZoneDeliveryModeValueValidator.onValidate(zoneDeliveryModeValue, null);
    }

    @Test
    public void testZoneDeliveryModeValueValidatorOK() throws Exception {
        Double min = Double.valueOf(0);
        final Double value = Double.valueOf(5);

        min = Double.valueOf(10);
        final TargetZoneDeliveryModeValueModel zoneDeliveryModeValue =
                testZoneDeliveryModeValueValidator(eurCurrency, min, value, deZone, dhlZoneDeliveryMode, null, null);
        targetZoneDeliveryModeValueValidator.onValidate(zoneDeliveryModeValue, null);
    }

    @Test(expected = InterceptorException.class)
    public void testZoneDeliveryModeValueValidatorOverlapingDatesToday() throws Exception {
        final TargetZoneDeliveryModeValueModel targetValue = (TargetZoneDeliveryModeValueModel)
                zoneDeliveryModeService.getDeliveryValue(deZone, eurCurrency, Double.valueOf(0), dhlZoneDeliveryMode);
        final Double min = Double.valueOf(0);
        final Double value = Double.valueOf(5);

        targetValue.setValidTo(TOMORROW);
        modelService.save(targetValue);
        final TargetZoneDeliveryModeValueModel zoneDeliveryModeValue =
                testZoneDeliveryModeValueValidator(eurCurrency, min, value, deZone, dhlZoneDeliveryMode,
                        YESTERDAY, null);
        targetZoneDeliveryModeValueValidator.onValidate(zoneDeliveryModeValue, null);

    }

    @Test
    public void testZoneDeliveryModeValueValidatorNonOverlapingDatesEitherSideOfToday() throws Exception {
        final TargetZoneDeliveryModeValueModel targetValue = (TargetZoneDeliveryModeValueModel)
                zoneDeliveryModeService.getDeliveryValue(deZone, eurCurrency, Double.valueOf(0), dhlZoneDeliveryMode);
        final Double min = Double.valueOf(0);
        final Double value = Double.valueOf(5);

        targetValue.setValidTo(YESTERDAY);
        modelService.save(targetValue);
        final TargetZoneDeliveryModeValueModel zoneDeliveryModeValue =
                testZoneDeliveryModeValueValidator(eurCurrency, min, value, deZone, dhlZoneDeliveryMode,
                        TOMORROW, null);
        targetZoneDeliveryModeValueValidator.onValidate(zoneDeliveryModeValue, null);

    }

    @Test
    public void testZoneDeliveryModeValueValidatorNonOverlapingDatesBackToBackFuture() throws Exception {
        final TargetZoneDeliveryModeValueModel targetValue = (TargetZoneDeliveryModeValueModel)
                zoneDeliveryModeService.getDeliveryValue(deZone, eurCurrency, Double.valueOf(0), dhlZoneDeliveryMode);
        final Double min = Double.valueOf(0);
        final Double value = Double.valueOf(5);

        targetValue.setValidTo(TOMORROW);
        modelService.save(targetValue);
        final TargetZoneDeliveryModeValueModel zoneDeliveryModeValue =
                testZoneDeliveryModeValueValidator(eurCurrency, min, value, deZone, dhlZoneDeliveryMode, TOMORROW, null);
        targetZoneDeliveryModeValueValidator.onValidate(zoneDeliveryModeValue, null);

    }

    @Test
    public void testZoneDeliveryModeValueValidatorNonOverlapingDatesEitherSideOfToday2() throws Exception {
        final TargetZoneDeliveryModeValueModel targetValue = (TargetZoneDeliveryModeValueModel)
                zoneDeliveryModeService.getDeliveryValue(deZone, eurCurrency, Double.valueOf(0), dhlZoneDeliveryMode);
        final Double min = Double.valueOf(0);
        final Double value = Double.valueOf(5);

        targetValue.setValidFrom(TOMORROW);
        modelService.save(targetValue);
        final TargetZoneDeliveryModeValueModel zoneDeliveryModeValue =
                testZoneDeliveryModeValueValidator(eurCurrency, min, value, deZone, dhlZoneDeliveryMode, null,
                        YESTERDAY);
        targetZoneDeliveryModeValueValidator.onValidate(zoneDeliveryModeValue, null);

    }

    @Test(expected = InterceptorException.class)
    public void testZoneDeliveryModeValueValidatorOverlapingDatesTodayValidInBoth() throws Exception {
        final TargetZoneDeliveryModeValueModel targetValue = (TargetZoneDeliveryModeValueModel)
                zoneDeliveryModeService.getDeliveryValue(deZone, eurCurrency, Double.valueOf(0), dhlZoneDeliveryMode);
        final Double min = Double.valueOf(0);
        final Double value = Double.valueOf(5);

        targetValue.setValidFrom(YESTERDAY);
        modelService.save(targetValue);
        final TargetZoneDeliveryModeValueModel zoneDeliveryModeValue =
                testZoneDeliveryModeValueValidator(eurCurrency, min, value, deZone, dhlZoneDeliveryMode, null, TOMORROW);
        targetZoneDeliveryModeValueValidator.onValidate(zoneDeliveryModeValue, null);

    }

    @Test
    public void testZoneDeliveryModeValueValidatorNonOverlapingDates() throws Exception {
        final TargetZoneDeliveryModeValueModel targetValue = (TargetZoneDeliveryModeValueModel)
                zoneDeliveryModeService.getDeliveryValue(deZone, eurCurrency, Double.valueOf(0), dhlZoneDeliveryMode);
        final Double min = Double.valueOf(0);
        final Double value = Double.valueOf(5);

        targetValue.setValidFrom(TOMORROW);
        modelService.save(targetValue);
        final TargetZoneDeliveryModeValueModel zoneDeliveryModeValue =
                testZoneDeliveryModeValueValidator(eurCurrency, min, value, deZone, dhlZoneDeliveryMode, null, TOMORROW);
        targetZoneDeliveryModeValueValidator.onValidate(zoneDeliveryModeValue, null);
    }

    private TargetZoneDeliveryModeValueModel testZoneDeliveryModeValueValidator(final CurrencyModel currency,
            final Double min, final Double value, final ZoneModel zone, final ZoneDeliveryModeModel zoneDeliveryMode,
            final Date validFrom, final Date validTo) {
        final TargetZoneDeliveryModeValueModel zoneDeliveryModeValue =
                modelService.create(TargetZoneDeliveryModeValueModel.class);
        zoneDeliveryModeValue.setCurrency(currency);
        zoneDeliveryModeValue.setMinimum(min);
        zoneDeliveryModeValue.setValue(value);
        zoneDeliveryModeValue.setZone(zone);
        zoneDeliveryModeValue.setDeliveryMode(zoneDeliveryMode);
        zoneDeliveryModeValue.setValidFrom(validFrom);
        zoneDeliveryModeValue.setValidTo(validTo);
        return zoneDeliveryModeValue;
    }
}
