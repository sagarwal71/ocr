/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.TargetTimeZDMVRestrictionModel;



/**
 * @author Nandini
 * 
 */
@UnitTest
public class TargetTimeValidateInterceptorTest {

    private TargetTimeValidateInterceptor targetTimeValidateInterceptor;

    @Mock
    private TargetTimeZDMVRestrictionModel targetTimeZDMVRestrictionModel;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        targetTimeValidateInterceptor = new TargetTimeValidateInterceptor();
    }

    @Test(expected = InterceptorException.class)
    public void testValidateEmptyDate() throws InterceptorException {
        given(targetTimeZDMVRestrictionModel.getActiveFrom()).willReturn(null);
        given(targetTimeZDMVRestrictionModel.getActiveUntil()).willReturn(null);
        targetTimeValidateInterceptor.onValidate(targetTimeZDMVRestrictionModel, null);
        Assert.fail();
    }

    @Test
    public void testValidateStartDate() throws InterceptorException {
        given(targetTimeZDMVRestrictionModel.getActiveFrom()).willReturn(new Date());
        given(targetTimeZDMVRestrictionModel.getActiveUntil()).willReturn(null);
        targetTimeValidateInterceptor.onValidate(targetTimeZDMVRestrictionModel, null);
    }

    @Test
    public void testValidateEndDate() throws InterceptorException {
        given(targetTimeZDMVRestrictionModel.getActiveFrom()).willReturn(null);
        given(targetTimeZDMVRestrictionModel.getActiveUntil()).willReturn(new Date());
        targetTimeValidateInterceptor.onValidate(targetTimeZDMVRestrictionModel, null);
    }

    @Test
    public void testValidateBothDates() throws InterceptorException {
        given(targetTimeZDMVRestrictionModel.getActiveFrom()).willReturn(new Date());
        given(targetTimeZDMVRestrictionModel.getActiveUntil()).willReturn(new Date());
        targetTimeValidateInterceptor.onValidate(targetTimeZDMVRestrictionModel, null);
    }

}
