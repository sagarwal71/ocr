package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetDealCategoryModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AbstractSimpleDealModelRemoveInterceptorTest {

    //CHECKSTYLE:OFF

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @Spy
    private final AbstractSimpleDealModelRemoveInterceptor dealModelRemoveInterceptor = new AbstractSimpleDealModelRemoveInterceptor();

    @Mock
    private ModelService modelService;

    @Mock
    private InterceptorContext interceptorContext;

    @Mock
    private AbstractSimpleDealModel dealModel;

    @Before
    public void setUp() {
        BDDMockito.willReturn(modelService).given(dealModelRemoveInterceptor).getModelService();
    }

    @Test
    public void testOnRemoveForNullMode() throws InterceptorException {
        dealModelRemoveInterceptor.onRemove(null, interceptorContext);
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testOnRemoveForOtherModel() throws InterceptorException {
        dealModelRemoveInterceptor.onRemove(new Object(), interceptorContext);
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testOnRemoveWithNoQualifierListAndRewardCategory() throws InterceptorException {

        BDDMockito.given(dealModel.getRewardCategory()).willReturn(Collections.EMPTY_LIST);
        BDDMockito.given(dealModel.getQualifierList()).willReturn(Collections.EMPTY_LIST);

        dealModelRemoveInterceptor.onRemove(dealModel, interceptorContext);
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testOnRemoveWithRewardCategoryNoQualifierList() throws InterceptorException {

        final TargetDealCategoryModel rewardCategory = Mockito.mock(TargetDealCategoryModel.class);

        BDDMockito.given(dealModel.getRewardCategory()).willReturn(Collections.singletonList(rewardCategory));
        BDDMockito.given(dealModel.getQualifierList()).willReturn(Collections.EMPTY_LIST);

        final ArgumentCaptor<Collection> argumentCaptor = ArgumentCaptor.forClass(Collection.class);

        dealModelRemoveInterceptor.onRemove(dealModel, interceptorContext);
        Mockito.verify(modelService).removeAll(argumentCaptor.capture());

        Assert.assertEquals(1, argumentCaptor.getValue().size());
        Assert.assertTrue(argumentCaptor.getValue().contains(rewardCategory));
    }

    @Test
    public void testOnRemoveWithQualifierListNoRewardCategory() throws InterceptorException {

        final TargetDealCategoryModel dealCategory = Mockito.mock(TargetDealCategoryModel.class);
        final DealQualifierModel dealQualifier = Mockito.mock(DealQualifierModel.class);

        BDDMockito.given(dealQualifier.getDealCategory()).willReturn(Collections.singletonList(dealCategory));
        BDDMockito.given(dealModel.getRewardCategory()).willReturn(Collections.EMPTY_LIST);
        BDDMockito.given(dealModel.getQualifierList()).willReturn(Collections.singletonList(dealQualifier));

        final ArgumentCaptor<Collection> argumentCaptor = ArgumentCaptor.forClass(Collection.class);

        dealModelRemoveInterceptor.onRemove(dealModel, interceptorContext);
        Mockito.verify(modelService).removeAll(argumentCaptor.capture());

        Assert.assertEquals(2, argumentCaptor.getValue().size());
        Assert.assertTrue(argumentCaptor.getValue().contains(dealCategory));
        Assert.assertTrue(argumentCaptor.getValue().contains(dealQualifier));
    }

    @Test
    public void testOnRemoveWithQualifierListAndRewardCategory() throws InterceptorException {

        final TargetDealCategoryModel dealCategory = Mockito.mock(TargetDealCategoryModel.class);
        final DealQualifierModel dealQualifier = Mockito.mock(DealQualifierModel.class);

        BDDMockito.given(dealQualifier.getDealCategory()).willReturn(Collections.singletonList(dealCategory));
        BDDMockito.given(dealModel.getQualifierList()).willReturn(Collections.singletonList(dealQualifier));

        final TargetDealCategoryModel rewardCategory = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(dealModel.getRewardCategory()).willReturn(Collections.singletonList(rewardCategory));

        final ArgumentCaptor<Collection> argumentCaptor = ArgumentCaptor.forClass(Collection.class);

        dealModelRemoveInterceptor.onRemove(dealModel, interceptorContext);
        Mockito.verify(modelService).removeAll(argumentCaptor.capture());

        Assert.assertEquals(3, argumentCaptor.getValue().size());
        Assert.assertTrue(argumentCaptor.getValue().contains(dealCategory));
        Assert.assertTrue(argumentCaptor.getValue().contains(rewardCategory));
        Assert.assertTrue(argumentCaptor.getValue().contains(dealQualifier));
    }

    @Test
    public void testOnRemoveWithMultipleDealAndRewardCategories() throws InterceptorException {

        final TargetDealCategoryModel dealCategory1 = Mockito.mock(TargetDealCategoryModel.class);
        final TargetDealCategoryModel dealCategory2 = Mockito.mock(TargetDealCategoryModel.class);
        final DealQualifierModel dealQualifier = Mockito.mock(DealQualifierModel.class);

        BDDMockito.given(dealQualifier.getDealCategory()).willReturn(Arrays.asList(dealCategory1, dealCategory2));
        BDDMockito.given(dealModel.getQualifierList()).willReturn(Collections.singletonList(dealQualifier));

        final TargetDealCategoryModel rewardCategory1 = Mockito.mock(TargetDealCategoryModel.class);
        final TargetDealCategoryModel rewardCategory2 = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(dealModel.getRewardCategory()).willReturn(Arrays.asList(rewardCategory1, rewardCategory2));

        final ArgumentCaptor<Collection> argumentCaptor = ArgumentCaptor.forClass(Collection.class);

        dealModelRemoveInterceptor.onRemove(dealModel, interceptorContext);
        Mockito.verify(modelService).removeAll(argumentCaptor.capture());

        Assert.assertEquals(5, argumentCaptor.getValue().size());
        Assert.assertTrue(argumentCaptor.getValue().contains(dealCategory1));
        Assert.assertTrue(argumentCaptor.getValue().contains(rewardCategory1));
        Assert.assertTrue(argumentCaptor.getValue().contains(dealCategory2));
        Assert.assertTrue(argumentCaptor.getValue().contains(rewardCategory2));
        Assert.assertTrue(argumentCaptor.getValue().contains(dealQualifier));
    }

    @Test
    public void testOnRemoveWithMultipleQualifierList() throws InterceptorException {

        final TargetDealCategoryModel dealCategory1 = Mockito.mock(TargetDealCategoryModel.class);
        final TargetDealCategoryModel dealCategory2 = Mockito.mock(TargetDealCategoryModel.class);
        final DealQualifierModel dealQualifier1 = Mockito.mock(DealQualifierModel.class);
        final DealQualifierModel dealQualifier2 = Mockito.mock(DealQualifierModel.class);

        BDDMockito.given(dealQualifier1.getDealCategory()).willReturn(Collections.singletonList(dealCategory1));
        BDDMockito.given(dealQualifier2.getDealCategory()).willReturn(Collections.singletonList(dealCategory2));
        BDDMockito.given(dealModel.getQualifierList()).willReturn(Arrays.asList(dealQualifier1, dealQualifier2));

        final TargetDealCategoryModel rewardCategory1 = Mockito.mock(TargetDealCategoryModel.class);
        final TargetDealCategoryModel rewardCategory2 = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(dealModel.getRewardCategory()).willReturn(Arrays.asList(rewardCategory1, rewardCategory2));

        final ArgumentCaptor<Collection> argumentCaptor = ArgumentCaptor.forClass(Collection.class);

        dealModelRemoveInterceptor.onRemove(dealModel, interceptorContext);
        Mockito.verify(modelService).removeAll(argumentCaptor.capture());

        Assert.assertEquals(6, argumentCaptor.getValue().size());
        Assert.assertTrue(argumentCaptor.getValue().contains(dealCategory1));
        Assert.assertTrue(argumentCaptor.getValue().contains(rewardCategory1));
        Assert.assertTrue(argumentCaptor.getValue().contains(dealCategory2));
        Assert.assertTrue(argumentCaptor.getValue().contains(rewardCategory2));
        Assert.assertTrue(argumentCaptor.getValue().contains(dealQualifier1));
        Assert.assertTrue(argumentCaptor.getValue().contains(dealQualifier2));
    }
}
