package au.com.target.tgtcore.interceptor;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;


@UnitTest
public class TargetColourVariantProductValidateInterceptorTest {

    @Mock
    private InterceptorContext mockInterceptorContext;

    @InjectMocks
    private final TargetColourVariantProductValidateInterceptor targetColourVariantProductValidateInterceptor = new TargetColourVariantProductValidateInterceptor();

    @Test
    public void testOnValidateWithModel() throws InterceptorException {
        // This test just ensures that no errors occur when a null model is passed in
        targetColourVariantProductValidateInterceptor.onValidate(null, mockInterceptorContext);
    }

    @Test
    public void testOnValidateWithIncorrectModelType() throws InterceptorException {
        // This test just ensures that no errors occur when the wrong model type is passed in
        final VariantProductModel mockVariant = mock(VariantProductModel.class);

        targetColourVariantProductValidateInterceptor.onValidate(mockVariant, mockInterceptorContext);
    }

    @Test
    public void testOnvalidateWithNonBulkyBoardProduct() throws InterceptorException {
        final TargetColourVariantProductModel mockTargetColourVariantProduct = mock(
                TargetColourVariantProductModel.class);
        given(mockTargetColourVariantProduct.getBulkyBoardProduct()).willReturn(Boolean.FALSE);

        targetColourVariantProductValidateInterceptor
                .onValidate(mockTargetColourVariantProduct, mockInterceptorContext);
    }

    @Test
    public void testOnvalidateWithBulkyBoardProductWithNoVariants() throws InterceptorException {
        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);
        given(mockTargetProduct.getVariants()).willReturn(null);

        final TargetColourVariantProductModel mockTargetColourVariantProduct = mock(
                TargetColourVariantProductModel.class);
        given(mockTargetColourVariantProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);
        given(mockTargetColourVariantProduct.getBaseProduct()).willReturn(mockTargetProduct);

        targetColourVariantProductValidateInterceptor
                .onValidate(mockTargetColourVariantProduct, mockInterceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testOnvalidateWithBulkyBoardProductWithVariantNotSelf() throws InterceptorException {
        final TargetColourVariantProductModel mockTargetColourVariantProductFromBaseProduct = mock(
                TargetColourVariantProductModel.class);

        final Collection<VariantProductModel> baseProductVariants = new ArrayList<>();
        baseProductVariants.add(mockTargetColourVariantProductFromBaseProduct);

        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);
        given(mockTargetProduct.getVariants()).willReturn(baseProductVariants);

        final TargetColourVariantProductModel mockTargetColourVariantProduct = mock(
                TargetColourVariantProductModel.class);
        given(mockTargetColourVariantProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);
        given(mockTargetColourVariantProduct.getBaseProduct()).willReturn(mockTargetProduct);

        targetColourVariantProductValidateInterceptor
                .onValidate(mockTargetColourVariantProduct, mockInterceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testOnvalidateWithBulkyBoardProductWithTooManyVariants() throws InterceptorException {
        final TargetColourVariantProductModel mockTargetColourVariantProductFromBaseProduct1 = mock(
                TargetColourVariantProductModel.class);
        final TargetColourVariantProductModel mockTargetColourVariantProductFromBaseProduct2 = mock(
                TargetColourVariantProductModel.class);

        final Collection<VariantProductModel> baseProductVariants = new ArrayList<>();
        baseProductVariants.add(mockTargetColourVariantProductFromBaseProduct1);
        baseProductVariants.add(mockTargetColourVariantProductFromBaseProduct2);

        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);
        given(mockTargetProduct.getVariants()).willReturn(baseProductVariants);

        final TargetColourVariantProductModel mockTargetColourVariantProduct = mock(
                TargetColourVariantProductModel.class);
        given(mockTargetColourVariantProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);
        given(mockTargetColourVariantProduct.getBaseProduct()).willReturn(mockTargetProduct);

        targetColourVariantProductValidateInterceptor
                .onValidate(mockTargetColourVariantProduct, mockInterceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testOnvalidateWithBulkyBoardProductWithTooManyVariantsIncludingSelf() throws InterceptorException {
        final TargetColourVariantProductModel mockTargetColourVariantProduct = mock(
                TargetColourVariantProductModel.class);
        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);

        given(mockTargetColourVariantProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);
        given(mockTargetColourVariantProduct.getBaseProduct()).willReturn(mockTargetProduct);

        final TargetColourVariantProductModel mockTargetColourVariantProductFromBaseProduct1 = mock(
                TargetColourVariantProductModel.class);

        final Collection<VariantProductModel> baseProductVariants = new ArrayList<>();
        baseProductVariants.add(mockTargetColourVariantProductFromBaseProduct1);
        baseProductVariants.add(mockTargetColourVariantProduct);

        given(mockTargetProduct.getVariants()).willReturn(baseProductVariants);

        targetColourVariantProductValidateInterceptor
                .onValidate(mockTargetColourVariantProduct, mockInterceptorContext);
    }

    @Test
    public void testOnvalidateWithBulkyBoardProductWithVariantIsSelf() throws InterceptorException {
        final TargetColourVariantProductModel mockTargetColourVariantProduct = mock(
                TargetColourVariantProductModel.class);
        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);

        given(mockTargetColourVariantProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);
        given(mockTargetColourVariantProduct.getBaseProduct()).willReturn(mockTargetProduct);

        final Collection<VariantProductModel> baseProductVariants = new ArrayList<>();
        baseProductVariants.add(mockTargetColourVariantProduct);

        given(mockTargetProduct.getVariants()).willReturn(baseProductVariants);

        targetColourVariantProductValidateInterceptor
                .onValidate(mockTargetColourVariantProduct, mockInterceptorContext);
    }

    @Test
    public void testNewLowerPriceWhenStartDateEndDateNull() throws InterceptorException {
        final TargetColourVariantProductModel colourVariantProduct = mock(TargetColourVariantProductModel.class);
        given(colourVariantProduct.getBulkyBoardProduct()).willReturn(Boolean.FALSE);
        given(colourVariantProduct.getNewLowerPriceEndDate()).willReturn(null);
        given(colourVariantProduct.getNewLowerPriceStartDate()).willReturn(null);
        targetColourVariantProductValidateInterceptor.onValidate(colourVariantProduct, mockInterceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testNewLowerPriceWhenOnlyStartDateNull() throws InterceptorException {
        final TargetColourVariantProductModel colourVariantProduct = mock(TargetColourVariantProductModel.class);
        given(colourVariantProduct.getBulkyBoardProduct()).willReturn(Boolean.FALSE);
        given(colourVariantProduct.getNewLowerPriceEndDate()).willReturn(new Date());
        given(colourVariantProduct.getNewLowerPriceStartDate()).willReturn(null);
        targetColourVariantProductValidateInterceptor.onValidate(colourVariantProduct, mockInterceptorContext);
    }

    @Test
    public void testNewLowerPriceWhenOnlyStartDateLTEndDate() throws InterceptorException {
        final TargetColourVariantProductModel colourVariantProduct = mock(TargetColourVariantProductModel.class);
        given(colourVariantProduct.getBulkyBoardProduct()).willReturn(Boolean.FALSE);
        given(colourVariantProduct.getNewLowerPriceEndDate()).willReturn(DateUtils.addHours(new Date(), 5));
        given(colourVariantProduct.getNewLowerPriceStartDate()).willReturn(DateUtils.addHours(new Date(), -4));
        targetColourVariantProductValidateInterceptor.onValidate(colourVariantProduct, mockInterceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testNewLowerPriceWhenOnlyStartDateGTEndDate() throws InterceptorException {
        final TargetColourVariantProductModel colourVariantProduct = mock(TargetColourVariantProductModel.class);
        given(colourVariantProduct.getBulkyBoardProduct()).willReturn(Boolean.FALSE);
        given(colourVariantProduct.getNewLowerPriceEndDate()).willReturn(DateUtils.addHours(new Date(), -5));
        given(colourVariantProduct.getNewLowerPriceStartDate()).willReturn(DateUtils.addHours(new Date(), 4));
        targetColourVariantProductValidateInterceptor.onValidate(colourVariantProduct, mockInterceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testNewLowerPriceWhenOnlyStartDateEQEndDate() throws InterceptorException {
        final TargetColourVariantProductModel colourVariantProduct = mock(TargetColourVariantProductModel.class);
        given(colourVariantProduct.getBulkyBoardProduct()).willReturn(Boolean.FALSE);
        given(colourVariantProduct.getNewLowerPriceEndDate()).willReturn(DateUtils.addHours(new Date(), 4));
        given(colourVariantProduct.getNewLowerPriceStartDate()).willReturn(DateUtils.addHours(new Date(), 4));
        targetColourVariantProductValidateInterceptor.onValidate(colourVariantProduct, mockInterceptorContext);
    }
}
