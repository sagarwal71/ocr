package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.Collections;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.constants.TgtCoreConstants.Catalog;
import au.com.target.tgtcore.interceptor.TargetDealCategoryModelValidateInterceptor.ErrorMessages;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import org.junit.Assert;


@UnitTest
public class TargetDealCategoryModelValidateInterceptorTest {

    private static final String CUSTOMERGROUP_UID = "customergroup";

    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";

    private CatalogVersionModel catalogVersion = null;

    private final TargetDealCategoryModelValidateInterceptor targetDealCategoryModelValidateInterceptor = new TargetDealCategoryModelValidateInterceptor();

    @Before
    public void setUp() {
        catalogVersion = new CatalogVersionModel();
        catalogVersion.setVersion(Catalog.OFFLINE_VERSION);
        targetDealCategoryModelValidateInterceptor.setCustomerGroupUid(CUSTOMERGROUP_UID);
    }

    @Test
    public void testOnValidateWithNullModel() throws InterceptorException {
        targetDealCategoryModelValidateInterceptor.onValidate(null, null);
    }

    @Test
    public void testOnValidateWithWrongModelType() throws InterceptorException {
        final ProductPromotionModel mockProductPromotionModel = Mockito.mock(ProductPromotionModel.class);

        targetDealCategoryModelValidateInterceptor.onValidate(mockProductPromotionModel, null);

        Mockito.verifyZeroInteractions(mockProductPromotionModel);
    }

    @Test
    public void testOnValidateWithSupercategories() throws InterceptorException {
        final TargetDealCategoryModel mockTargetDealCategory = Mockito.mock(TargetDealCategoryModel.class);

        final List<CategoryModel> supercategories = Collections.singletonList(Mockito.mock(CategoryModel.class));
        BDDMockito.given(mockTargetDealCategory.getSupercategories()).willReturn(supercategories);
        BDDMockito.given(mockTargetDealCategory.getCatalogVersion()).willReturn(catalogVersion);

        try {
            targetDealCategoryModelValidateInterceptor.onValidate(mockTargetDealCategory, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).startsWith(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.NO_SUPERCATEGORIES);
        }
    }

    @Test
    public void testOnValidateWithSubordinateCategories() throws InterceptorException {
        final TargetDealCategoryModel mockTargetDealCategory = Mockito.mock(TargetDealCategoryModel.class);

        final List<CategoryModel> categories = Collections.singletonList(Mockito.mock(CategoryModel.class));
        BDDMockito.given(mockTargetDealCategory.getCategories()).willReturn(categories);
        BDDMockito.given(mockTargetDealCategory.getCatalogVersion()).willReturn(catalogVersion);

        try {
            targetDealCategoryModelValidateInterceptor.onValidate(mockTargetDealCategory, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).startsWith(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.NO_SUBORDINATE_CATEGORIES);
        }
    }

    @Test
    public void testOnValidateWithNoAllowedPrincipals() throws InterceptorException {
        final TargetDealCategoryModel mockTargetDealCategory = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(mockTargetDealCategory.getCatalogVersion()).willReturn(catalogVersion);

        try {
            targetDealCategoryModelValidateInterceptor.onValidate(mockTargetDealCategory, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).startsWith(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.CUSTOMER_GROUP_REQUIRED);
        }
    }

    @Test
    public void testOnValidateWithoutCustomerGroup() throws InterceptorException {
        final TargetDealCategoryModel mockTargetDealCategory = Mockito.mock(TargetDealCategoryModel.class);

        final PrincipalModel mockUserGroup = Mockito.mock(PrincipalModel.class);
        BDDMockito.given(mockUserGroup.getUid()).willReturn("admingroup");

        final List<PrincipalModel> allowedPrincipals = Collections.singletonList(mockUserGroup);
        BDDMockito.given(mockTargetDealCategory.getAllowedPrincipals()).willReturn(allowedPrincipals);
        BDDMockito.given(mockTargetDealCategory.getCatalogVersion()).willReturn(catalogVersion);

        try {
            targetDealCategoryModelValidateInterceptor.onValidate(mockTargetDealCategory, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).startsWith(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.CUSTOMER_GROUP_REQUIRED);
        }
    }

    @Test
    public void testOnValidate() throws InterceptorException {
        final TargetDealCategoryModel mockTargetDealCategory = Mockito.mock(TargetDealCategoryModel.class);

        final PrincipalModel mockUserGroup = Mockito.mock(PrincipalModel.class);
        BDDMockito.given(mockUserGroup.getUid()).willReturn(CUSTOMERGROUP_UID);

        final List<PrincipalModel> allowedPrincipals = Collections.singletonList(mockUserGroup);
        BDDMockito.given(mockTargetDealCategory.getAllowedPrincipals()).willReturn(allowedPrincipals);
        BDDMockito.given(mockTargetDealCategory.getCatalogVersion()).willReturn(catalogVersion);

        targetDealCategoryModelValidateInterceptor.onValidate(mockTargetDealCategory, null);
    }

    @Test
    public void testOnValidateForNonStagedCatalogVersion() throws InterceptorException {
        final TargetDealCategoryModel mockTargetDealCategory = Mockito.mock(TargetDealCategoryModel.class);

        final PrincipalModel mockUserGroup = Mockito.mock(PrincipalModel.class);
        BDDMockito.given(mockUserGroup.getUid()).willReturn(CUSTOMERGROUP_UID);

        final List<PrincipalModel> allowedPrincipals = Collections.singletonList(mockUserGroup);
        BDDMockito.given(mockTargetDealCategory.getAllowedPrincipals()).willReturn(allowedPrincipals);
        catalogVersion = new CatalogVersionModel();
        catalogVersion.setVersion(Catalog.ONLINE_VERSION);
        BDDMockito.given(mockTargetDealCategory.getCatalogVersion()).willReturn(catalogVersion);
        targetDealCategoryModelValidateInterceptor.onValidate(mockTargetDealCategory, null);

        BDDMockito.verify(mockTargetDealCategory, BDDMockito.times(0)).getSupercategories();
    }

    @Test
    public void testOnValidateForNullCatalogVersion() throws InterceptorException {
        final TargetDealCategoryModel mockTargetDealCategory = Mockito.mock(TargetDealCategoryModel.class);

        final PrincipalModel mockUserGroup = Mockito.mock(PrincipalModel.class);
        BDDMockito.given(mockUserGroup.getUid()).willReturn(CUSTOMERGROUP_UID);

        final List<PrincipalModel> allowedPrincipals = Collections.singletonList(mockUserGroup);
        BDDMockito.given(mockTargetDealCategory.getAllowedPrincipals()).willReturn(allowedPrincipals);
        catalogVersion = new CatalogVersionModel();
        catalogVersion.setVersion(Catalog.ONLINE_VERSION);
        BDDMockito.given(mockTargetDealCategory.getCatalogVersion()).willReturn(null);
        targetDealCategoryModelValidateInterceptor.onValidate(mockTargetDealCategory, null);

        BDDMockito.verify(mockTargetDealCategory, BDDMockito.times(0)).getSupercategories();
    }
}
