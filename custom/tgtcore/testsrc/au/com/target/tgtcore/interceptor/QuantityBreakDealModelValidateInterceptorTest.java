package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.promotions.model.DealBreakPointModel;
import de.hybris.platform.promotions.model.QuantityBreakDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.interceptor.QuantityBreakDealModelValidateInterceptor.ErrorMessages;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;


@UnitTest
public class QuantityBreakDealModelValidateInterceptorTest {

    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";

    private final QuantityBreakDealModelValidateInterceptor quantityBreakDealModelValidateInterceptor = new QuantityBreakDealModelValidateInterceptor();

    @Test
    public void testOnValidateWithNullModel() throws InterceptorException {
        quantityBreakDealModelValidateInterceptor.onValidate(null, null);
    }

    @Test
    public void testOnValidateWithWrongModelType() throws InterceptorException {
        final DealBreakPointModel mockModel = Mockito.mock(DealBreakPointModel.class);

        quantityBreakDealModelValidateInterceptor.onValidate(mockModel, null);

        Mockito.verifyZeroInteractions(mockModel);
    }

    @Test
    public void testOnValidateWithNoCategories() {
        final QuantityBreakDealModel mockQuantityBreakDeal = Mockito.mock(QuantityBreakDealModel.class);

        try {
            quantityBreakDealModelValidateInterceptor.onValidate(mockQuantityBreakDeal, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.CAT_RANGE);
        }
    }

    @Test
    public void testOnValidateWithNonDealCategory() {
        final TargetProductCategoryModel mockCategory = Mockito.mock(TargetProductCategoryModel.class);

        final Collection<CategoryModel> categories = new ArrayList<>();
        categories.add(mockCategory);

        final QuantityBreakDealModel mockQuantityBreakDeal = Mockito.mock(QuantityBreakDealModel.class);
        BDDMockito.given(mockQuantityBreakDeal.getCategories()).willReturn(categories);

        try {
            quantityBreakDealModelValidateInterceptor.onValidate(mockQuantityBreakDeal, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.CAT_NOT_DEAL_CATEGORY);
        }
    }

    @Test
    public void testOnValidateWithTooManyStagedCategories() {
        final CatalogVersionModel mockCatalogVersion = Mockito.mock(CatalogVersionModel.class);
        BDDMockito.given(mockCatalogVersion.getActive()).willReturn(Boolean.FALSE);

        final TargetDealCategoryModel mockCategory1 = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(mockCategory1.getCatalogVersion()).willReturn(mockCatalogVersion);

        final TargetDealCategoryModel mockCategory2 = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(mockCategory2.getCatalogVersion()).willReturn(mockCatalogVersion);

        final Collection<CategoryModel> categories = new ArrayList<>();
        categories.add(mockCategory1);
        categories.add(mockCategory2);

        final QuantityBreakDealModel mockQuantityBreakDeal = Mockito.mock(QuantityBreakDealModel.class);
        BDDMockito.given(mockQuantityBreakDeal.getCategories()).willReturn(categories);

        try {
            quantityBreakDealModelValidateInterceptor.onValidate(mockQuantityBreakDeal, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.CAT_TOO_MANY_STAGED);
        }
    }

    @Test
    public void testOnValidateWithDealEnabledNoBreakpoints() {
        final CatalogVersionModel mockCatalogVersion = Mockito.mock(CatalogVersionModel.class);
        BDDMockito.given(mockCatalogVersion.getActive()).willReturn(Boolean.FALSE);

        final TargetDealCategoryModel mockCategory1 = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(mockCategory1.getCatalogVersion()).willReturn(mockCatalogVersion);

        final Collection<CategoryModel> categories = new ArrayList<>();
        categories.add(mockCategory1);

        final QuantityBreakDealModel mockQuantityBreakDeal = Mockito.mock(QuantityBreakDealModel.class);
        BDDMockito.given(mockQuantityBreakDeal.getCategories()).willReturn(categories);
        BDDMockito.given(mockQuantityBreakDeal.getEnabled()).willReturn(Boolean.TRUE);

        try {
            quantityBreakDealModelValidateInterceptor.onValidate(mockQuantityBreakDeal, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(EXCEPTION_MESSAGE_PREFIX + ErrorMessages.NO_BREAKPOINTS);
        }
    }

    @Test
    public void testOnValidateWithDealDisabledNoBreakpoints() throws InterceptorException {
        final CatalogVersionModel mockCatalogVersion = Mockito.mock(CatalogVersionModel.class);
        BDDMockito.given(mockCatalogVersion.getActive()).willReturn(Boolean.FALSE);

        final TargetDealCategoryModel mockCategory1 = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(mockCategory1.getCatalogVersion()).willReturn(mockCatalogVersion);

        final Collection<CategoryModel> categories = new ArrayList<>();
        categories.add(mockCategory1);

        final QuantityBreakDealModel mockQuantityBreakDeal = Mockito.mock(QuantityBreakDealModel.class);
        BDDMockito.given(mockQuantityBreakDeal.getCategories()).willReturn(categories);
        BDDMockito.given(mockQuantityBreakDeal.getEnabled()).willReturn(Boolean.FALSE);

        quantityBreakDealModelValidateInterceptor.onValidate(mockQuantityBreakDeal, null);
    }

    @Test
    public void testOnValidateWithDealEnabledOneBreakpoint() throws InterceptorException {
        final CatalogVersionModel mockCatalogVersion = Mockito.mock(CatalogVersionModel.class);
        BDDMockito.given(mockCatalogVersion.getActive()).willReturn(Boolean.FALSE);

        final TargetDealCategoryModel mockCategory1 = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(mockCategory1.getCatalogVersion()).willReturn(mockCatalogVersion);

        final Collection<CategoryModel> categories = new ArrayList<>();
        categories.add(mockCategory1);

        final DealBreakPointModel mockDealBreakPoint = Mockito.mock(DealBreakPointModel.class);

        final QuantityBreakDealModel mockQuantityBreakDeal = Mockito.mock(QuantityBreakDealModel.class);
        BDDMockito.given(mockQuantityBreakDeal.getCategories()).willReturn(categories);
        BDDMockito.given(mockQuantityBreakDeal.getEnabled()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockQuantityBreakDeal.getBreakPoints()).willReturn(
                Collections.singletonList(mockDealBreakPoint));

        quantityBreakDealModelValidateInterceptor.onValidate(mockQuantityBreakDeal, null);
    }

    @Test
    public void testOnValidateWithDealEnabledTwoBreakpoints() throws InterceptorException {
        final CatalogVersionModel mockCatalogVersion = Mockito.mock(CatalogVersionModel.class);
        BDDMockito.given(mockCatalogVersion.getActive()).willReturn(Boolean.FALSE);

        final TargetDealCategoryModel mockCategory1 = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(mockCategory1.getCatalogVersion()).willReturn(mockCatalogVersion);

        final Collection<CategoryModel> categories = new ArrayList<>();
        categories.add(mockCategory1);

        final DealBreakPointModel mockDealBreakPoint1 = Mockito.mock(DealBreakPointModel.class);
        final DealBreakPointModel mockDealBreakPoint2 = Mockito.mock(DealBreakPointModel.class);

        final List<DealBreakPointModel> dealBreakPoints = new ArrayList<>();
        dealBreakPoints.add(mockDealBreakPoint1);
        dealBreakPoints.add(mockDealBreakPoint2);

        final QuantityBreakDealModel mockQuantityBreakDeal = Mockito.mock(QuantityBreakDealModel.class);
        BDDMockito.given(mockQuantityBreakDeal.getCategories()).willReturn(categories);
        BDDMockito.given(mockQuantityBreakDeal.getEnabled()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockQuantityBreakDeal.getBreakPoints()).willReturn(dealBreakPoints);

        quantityBreakDealModelValidateInterceptor.onValidate(mockQuantityBreakDeal, null);
    }

    @Test
    public void testOnValidate() throws InterceptorException {
        final CatalogVersionModel mockStagedCatalogVersion = Mockito.mock(CatalogVersionModel.class);
        BDDMockito.given(mockStagedCatalogVersion.getActive()).willReturn(Boolean.FALSE);

        final CatalogVersionModel mockOnlineCatalogVersion = Mockito.mock(CatalogVersionModel.class);
        BDDMockito.given(mockStagedCatalogVersion.getActive()).willReturn(Boolean.TRUE);

        final TargetDealCategoryModel mockStagedCategory = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(mockStagedCategory.getCatalogVersion()).willReturn(mockStagedCatalogVersion);

        final TargetDealCategoryModel mockOnlineCategory = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(mockOnlineCategory.getCatalogVersion()).willReturn(mockOnlineCatalogVersion);

        final Collection<CategoryModel> categories = new ArrayList<>();
        categories.add(mockStagedCategory);
        categories.add(mockOnlineCategory);

        final DealBreakPointModel mockDealBreakPoint1 = Mockito.mock(DealBreakPointModel.class);
        final DealBreakPointModel mockDealBreakPoint2 = Mockito.mock(DealBreakPointModel.class);

        final List<DealBreakPointModel> dealBreakPoints = new ArrayList<>();
        dealBreakPoints.add(mockDealBreakPoint1);
        dealBreakPoints.add(mockDealBreakPoint2);

        final QuantityBreakDealModel mockQuantityBreakDeal = Mockito.mock(QuantityBreakDealModel.class);
        BDDMockito.given(mockQuantityBreakDeal.getCategories()).willReturn(categories);
        BDDMockito.given(mockQuantityBreakDeal.getEnabled()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockQuantityBreakDeal.getBreakPoints()).willReturn(dealBreakPoints);

        quantityBreakDealModelValidateInterceptor.onValidate(mockQuantityBreakDeal, null);
    }
}
