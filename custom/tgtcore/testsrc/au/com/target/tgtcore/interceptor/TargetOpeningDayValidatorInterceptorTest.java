/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.storelocator.model.OpeningDayModel;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.model.TargetOpeningDayModel;


/**
 * Test for {@link TargetOpeningDayValidateInterceptor}.
 */
@UnitTest
public class TargetOpeningDayValidatorInterceptorTest {
    private TargetOpeningDayValidateInterceptor interceptor;
    private InterceptorContext ctx;

    private final Date todayOpen = DateUtils.setHours(new Date(), 9);
    private final Date yesterdayOpen = DateUtils.addDays(todayOpen, -1);
    private final Date tomorrowOpen = DateUtils.addDays(todayOpen, 1);
    private final Date todayClose = DateUtils.setHours(todayOpen, 17);
    private final Date yesterdayClose = DateUtils.addDays(todayClose, -1);
    private final Date tomorrowClose = DateUtils.addDays(todayClose, 1);
    private final String openingDayId = "7004-23423424";


    @Before
    public void setup() {
        interceptor = new TargetOpeningDayValidateInterceptor();
        ctx = Mockito.mock(InterceptorContext.class);
    }

    @Test
    public void testZoneDeliveryModeValueValidatorAllGood() throws Exception {

        final TargetOpeningDayModel testModel = Mockito.mock(TargetOpeningDayModel.class);
        final TargetOpeningDayModel otherModel = Mockito.mock(TargetOpeningDayModel.class);

        final OpeningScheduleModel schedule = Mockito.mock(OpeningScheduleModel.class);
        final Collection<OpeningDayModel> openingDays = new ArrayList<OpeningDayModel>(2);
        openingDays.add(testModel);
        openingDays.add(otherModel);
        BDDMockito.given(schedule.getOpeningDays()).willReturn(openingDays);
        BDDMockito.given(testModel.getOpeningSchedule()).willReturn(schedule);


        BDDMockito.given(testModel.getOpeningTime()).willReturn(todayOpen);
        BDDMockito.given(testModel.getClosingTime()).willReturn(todayClose);
        BDDMockito.given(testModel.getTargetOpeningDayId()).willReturn(openingDayId);
        BDDMockito.given(otherModel.getOpeningTime()).willReturn(tomorrowOpen);
        BDDMockito.given(otherModel.getClosingTime()).willReturn(tomorrowClose);
        interceptor.onValidate(testModel, ctx);


        BDDMockito.given(testModel.getOpeningTime()).willReturn(todayOpen);
        BDDMockito.given(testModel.getClosingTime()).willReturn(todayClose);
        BDDMockito.given(testModel.getTargetOpeningDayId()).willReturn(openingDayId);
        BDDMockito.given(otherModel.getOpeningTime()).willReturn(yesterdayOpen);
        BDDMockito.given(otherModel.getClosingTime()).willReturn(yesterdayClose);
        interceptor.onValidate(testModel, ctx);
    }


    @Test(expected = InterceptorException.class)
    public void testZoneDeliveryModeValueValidatorOverlapingDates() throws Exception {

        final TargetOpeningDayModel testModel = Mockito.mock(TargetOpeningDayModel.class);
        final TargetOpeningDayModel otherModel = Mockito.mock(TargetOpeningDayModel.class);

        final OpeningScheduleModel schedule = Mockito.mock(OpeningScheduleModel.class);
        final Collection<OpeningDayModel> openingDays = new ArrayList<OpeningDayModel>(2);
        openingDays.add(testModel);
        openingDays.add(otherModel);
        BDDMockito.given(schedule.getOpeningDays()).willReturn(openingDays);
        BDDMockito.given(testModel.getOpeningSchedule()).willReturn(schedule);


        BDDMockito.given(testModel.getOpeningTime()).willReturn(todayOpen);
        BDDMockito.given(testModel.getClosingTime()).willReturn(todayClose);
        BDDMockito.given(testModel.getTargetOpeningDayId()).willReturn(openingDayId);
        BDDMockito.given(otherModel.getOpeningTime()).willReturn(todayOpen);
        BDDMockito.given(otherModel.getClosingTime()).willReturn(todayClose);
        interceptor.onValidate(testModel, ctx);
    }


    @Test(expected = InterceptorException.class)
    public void testZoneDeliveryModeValueValidatorNotSameDay() throws Exception {
        final TargetOpeningDayModel testModel = Mockito.mock(TargetOpeningDayModel.class);
        BDDMockito.given(testModel.getOpeningTime()).willReturn(todayOpen);
        BDDMockito.given(testModel.getClosingTime()).willReturn(tomorrowClose);
        BDDMockito.given(testModel.getTargetOpeningDayId()).willReturn(openingDayId);
        interceptor.onValidate(testModel, ctx);
    }

    @Test(expected = InterceptorException.class)
    public void testZoneDeliveryModeValueValidatorForEmptyOpeningDayId() throws Exception {
        final TargetOpeningDayModel testModel = Mockito.mock(TargetOpeningDayModel.class);
        BDDMockito.given(testModel.getOpeningTime()).willReturn(todayOpen);
        BDDMockito.given(testModel.getClosingTime()).willReturn(tomorrowClose);
        BDDMockito.given(testModel.getTargetOpeningDayId()).willReturn(StringUtils.EMPTY);
        interceptor.onValidate(testModel, ctx);
    }


    @Test(expected = InterceptorException.class)
    public void testZoneDeliveryModeValueValidatorNulls() throws Exception {
        final TargetOpeningDayModel testModel = Mockito.mock(TargetOpeningDayModel.class);
        interceptor.onValidate(testModel, ctx);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testZoneDeliveryModeValueWrongModel() throws Exception {
        final OrderModel testModel = Mockito.mock(OrderModel.class);
        interceptor.onValidate(testModel, ctx);
    }
}
