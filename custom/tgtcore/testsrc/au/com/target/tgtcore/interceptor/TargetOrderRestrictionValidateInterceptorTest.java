package au.com.target.tgtcore.interceptor;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.voucher.model.OrderRestrictionModel;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetOrderRestrictionModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetOrderRestrictionValidateInterceptorTest {

    // CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    // CHECKSTYLE:ON

    @Mock
    private CatalogVersionModel mockStagedCatalogVersion;

    @Mock
    private CatalogVersionModel mockOnlineCatalogVersion;

    private final TargetOrderRestrictionValidateInterceptor targetOrderRestrictionValidateInterceptor = new TargetOrderRestrictionValidateInterceptor();

    @Before
    public void setUp() {
        given(mockStagedCatalogVersion.getActive()).willReturn(Boolean.FALSE);
        given(mockOnlineCatalogVersion.getActive()).willReturn(Boolean.TRUE);
    }

    @Test
    public void testOnValidateWithWrongType() throws Exception {
        final OrderRestrictionModel mockOrderRestriction = mock(OrderRestrictionModel.class);

        targetOrderRestrictionValidateInterceptor.onValidate(mockOrderRestriction, null);

        verifyZeroInteractions(mockOrderRestriction);
    }

    @Test
    public void testOnValidateWithNoProductsOrCategories() throws Exception {
        final TargetOrderRestrictionModel mockTargetOrderRestriction = mock(TargetOrderRestrictionModel.class);

        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage(TargetOrderRestrictionValidateInterceptor.Messages.NO_PRODUCTS);
        targetOrderRestrictionValidateInterceptor.onValidate(mockTargetOrderRestriction, null);
    }

    @Test
    public void testOnValidateWithStagedIncludedProduct() throws Exception {
        final ProductModel mockProduct = mock(ProductModel.class);
        given(mockProduct.getCatalogVersion()).willReturn(mockStagedCatalogVersion);

        final List<ProductModel> includedProducts = new ArrayList<>();
        includedProducts.add(mockProduct);

        final TargetOrderRestrictionModel mockTargetOrderRestriction = mock(TargetOrderRestrictionModel.class);
        given(mockTargetOrderRestriction.getIncludedProducts()).willReturn(includedProducts);

        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage(MessageFormat.format(
                TargetOrderRestrictionValidateInterceptor.Messages.STAGED_ITEMS,
                TargetOrderRestrictionValidateInterceptor.Messages.INCLUDED_PRODUCTS));
        targetOrderRestrictionValidateInterceptor.onValidate(mockTargetOrderRestriction, null);
    }

    @Test
    public void testOnValidateWithStagedIncludedCategory() throws Exception {
        final CategoryModel mockCategory = mock(CategoryModel.class);
        given(mockCategory.getCatalogVersion()).willReturn(mockStagedCatalogVersion);

        final List<CategoryModel> includedCategories = new ArrayList<>();
        includedCategories.add(mockCategory);

        final TargetOrderRestrictionModel mockTargetOrderRestriction = mock(TargetOrderRestrictionModel.class);
        given(mockTargetOrderRestriction.getIncludedCategories()).willReturn(includedCategories);

        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage(MessageFormat.format(
                TargetOrderRestrictionValidateInterceptor.Messages.STAGED_ITEMS,
                TargetOrderRestrictionValidateInterceptor.Messages.INCLUDED_CATEGORIES));
        targetOrderRestrictionValidateInterceptor.onValidate(mockTargetOrderRestriction, null);
    }

    @Test
    public void testOnValidateWithStagedExcludedProduct() throws Exception {
        final ProductModel mockProduct = mock(ProductModel.class);
        given(mockProduct.getCatalogVersion()).willReturn(mockStagedCatalogVersion);

        final List<ProductModel> excludedProducts = new ArrayList<>();
        excludedProducts.add(mockProduct);

        final TargetOrderRestrictionModel mockTargetOrderRestriction = mock(TargetOrderRestrictionModel.class);
        given(mockTargetOrderRestriction.getExcludedProducts()).willReturn(excludedProducts);

        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage(MessageFormat.format(
                TargetOrderRestrictionValidateInterceptor.Messages.STAGED_ITEMS,
                TargetOrderRestrictionValidateInterceptor.Messages.EXCLUDED_PRODUCTS));
        targetOrderRestrictionValidateInterceptor.onValidate(mockTargetOrderRestriction, null);
    }

    @Test
    public void testOnValidateWithStagedExcludedCategory() throws Exception {
        final CategoryModel mockCategory = mock(CategoryModel.class);
        given(mockCategory.getCatalogVersion()).willReturn(mockStagedCatalogVersion);

        final List<CategoryModel> excludedCategories = new ArrayList<>();
        excludedCategories.add(mockCategory);

        final TargetOrderRestrictionModel mockTargetOrderRestriction = mock(TargetOrderRestrictionModel.class);
        given(mockTargetOrderRestriction.getExcludedCategories()).willReturn(excludedCategories);

        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage(MessageFormat.format(
                TargetOrderRestrictionValidateInterceptor.Messages.STAGED_ITEMS,
                TargetOrderRestrictionValidateInterceptor.Messages.EXCLUDED_CATEGORIES));
        targetOrderRestrictionValidateInterceptor.onValidate(mockTargetOrderRestriction, null);
    }

    @Test
    public void testOnValidateWithSameCategoryIncludedAndExcluded() throws Exception {
        final CategoryModel mockCategory = mock(CategoryModel.class);
        given(mockCategory.getCatalogVersion()).willReturn(mockOnlineCatalogVersion);

        final List<CategoryModel> includedCategories = new ArrayList<>();
        includedCategories.add(mockCategory);

        final List<CategoryModel> excludedCategories = new ArrayList<>();
        excludedCategories.add(mockCategory);

        final TargetOrderRestrictionModel mockTargetOrderRestriction = mock(TargetOrderRestrictionModel.class);
        given(mockTargetOrderRestriction.getIncludedCategories()).willReturn(includedCategories);
        given(mockTargetOrderRestriction.getExcludedCategories()).willReturn(excludedCategories);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage(TargetOrderRestrictionValidateInterceptor.Messages.CATEGORY_INCLUDED_AND_EXCLUDED);
        targetOrderRestrictionValidateInterceptor.onValidate(mockTargetOrderRestriction, null);
    }

    @Test
    public void testOnValidateWithSameProductIncludedAndExcluded() throws Exception {
        final ProductModel mockProduct = mock(ProductModel.class);
        given(mockProduct.getCatalogVersion()).willReturn(mockOnlineCatalogVersion);

        final List<ProductModel> includedProducts = new ArrayList<>();
        includedProducts.add(mockProduct);

        final List<ProductModel> excludedProducts = new ArrayList<>();
        excludedProducts.add(mockProduct);

        final TargetOrderRestrictionModel mockTargetOrderRestriction = mock(TargetOrderRestrictionModel.class);
        given(mockTargetOrderRestriction.getIncludedProducts()).willReturn(includedProducts);
        given(mockTargetOrderRestriction.getExcludedProducts()).willReturn(excludedProducts);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage(TargetOrderRestrictionValidateInterceptor.Messages.PRODUCT_INCLUDED_AND_EXCLUDED);
        targetOrderRestrictionValidateInterceptor.onValidate(mockTargetOrderRestriction, null);
    }

    @Test
    public void testOnValidateWithIncludedProductInExcludedCategory() throws Exception {
        final CategoryModel mockCategory = mock(CategoryModel.class);
        given(mockCategory.getCatalogVersion()).willReturn(mockOnlineCatalogVersion);

        final List<CategoryModel> excludedCategories = new ArrayList<>();
        excludedCategories.add(mockCategory);

        final ProductModel mockProduct = mock(ProductModel.class);
        given(mockProduct.getCatalogVersion()).willReturn(mockOnlineCatalogVersion);
        given(mockProduct.getSupercategories()).willReturn(excludedCategories);

        final List<ProductModel> includedProducts = new ArrayList<>();
        includedProducts.add(mockProduct);

        final TargetOrderRestrictionModel mockTargetOrderRestriction = mock(TargetOrderRestrictionModel.class);
        given(mockTargetOrderRestriction.getIncludedProducts()).willReturn(includedProducts);
        given(mockTargetOrderRestriction.getExcludedCategories()).willReturn(excludedCategories);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage(TargetOrderRestrictionValidateInterceptor.Messages.PRODUCT_EXCLUDED_BY_CATEGORY);
        targetOrderRestrictionValidateInterceptor.onValidate(mockTargetOrderRestriction, null);
    }

    @Test
    public void testOnValidateWithIncludedProducts() throws Exception {
        final ProductModel mockProduct1 = mock(ProductModel.class);
        given(mockProduct1.getCatalogVersion()).willReturn(mockOnlineCatalogVersion);

        final ProductModel mockProduct2 = mock(ProductModel.class);
        given(mockProduct2.getCatalogVersion()).willReturn(mockOnlineCatalogVersion);

        final List<ProductModel> includedProducts = new ArrayList<>();
        includedProducts.add(mockProduct1);
        includedProducts.add(mockProduct2);

        final TargetOrderRestrictionModel mockTargetOrderRestriction = mock(TargetOrderRestrictionModel.class);
        given(mockTargetOrderRestriction.getIncludedProducts()).willReturn(includedProducts);

        targetOrderRestrictionValidateInterceptor.onValidate(mockTargetOrderRestriction, null);
    }

    @Test
    public void testOnValidateWithIncludedCategories() throws Exception {
        final CategoryModel mockCategory1 = mock(CategoryModel.class);
        given(mockCategory1.getCatalogVersion()).willReturn(mockOnlineCatalogVersion);

        final CategoryModel mockCategory2 = mock(CategoryModel.class);
        given(mockCategory2.getCatalogVersion()).willReturn(mockOnlineCatalogVersion);

        final List<CategoryModel> includedCategories = new ArrayList<>();
        includedCategories.add(mockCategory1);
        includedCategories.add(mockCategory2);

        final TargetOrderRestrictionModel mockTargetOrderRestriction = mock(TargetOrderRestrictionModel.class);
        given(mockTargetOrderRestriction.getIncludedCategories()).willReturn(includedCategories);

        targetOrderRestrictionValidateInterceptor.onValidate(mockTargetOrderRestriction, null);
    }

    @Test
    public void testOnValidateWithExcludedProducts() throws Exception {
        final ProductModel mockProduct1 = mock(ProductModel.class);
        given(mockProduct1.getCatalogVersion()).willReturn(mockOnlineCatalogVersion);

        final ProductModel mockProduct2 = mock(ProductModel.class);
        given(mockProduct2.getCatalogVersion()).willReturn(mockOnlineCatalogVersion);

        final List<ProductModel> excludedProducts = new ArrayList<>();
        excludedProducts.add(mockProduct1);
        excludedProducts.add(mockProduct2);

        final TargetOrderRestrictionModel mockTargetOrderRestriction = mock(TargetOrderRestrictionModel.class);
        given(mockTargetOrderRestriction.getExcludedProducts()).willReturn(excludedProducts);

        targetOrderRestrictionValidateInterceptor.onValidate(mockTargetOrderRestriction, null);
    }

    @Test
    public void testOnValidateWithExcludedCategories() throws Exception {
        final CategoryModel mockCategory1 = mock(CategoryModel.class);
        given(mockCategory1.getCatalogVersion()).willReturn(mockOnlineCatalogVersion);

        final CategoryModel mockCategory2 = mock(CategoryModel.class);
        given(mockCategory2.getCatalogVersion()).willReturn(mockOnlineCatalogVersion);

        final List<CategoryModel> excludedCategories = new ArrayList<>();
        excludedCategories.add(mockCategory1);
        excludedCategories.add(mockCategory2);

        final TargetOrderRestrictionModel mockTargetOrderRestriction = mock(TargetOrderRestrictionModel.class);
        given(mockTargetOrderRestriction.getExcludedCategories()).willReturn(excludedCategories);

        targetOrderRestrictionValidateInterceptor.onValidate(mockTargetOrderRestriction, null);
    }

    @Test
    public void testOnValidateWithIncludedAndExcludedCategories() throws Exception {
        final CategoryModel mockIncludedCategory = mock(CategoryModel.class);
        given(mockIncludedCategory.getCatalogVersion()).willReturn(mockOnlineCatalogVersion);

        final List<CategoryModel> includedCategories = new ArrayList<>();
        includedCategories.add(mockIncludedCategory);

        final CategoryModel mockExcludedCategory = mock(CategoryModel.class);
        given(mockExcludedCategory.getCatalogVersion()).willReturn(mockOnlineCatalogVersion);

        final List<CategoryModel> excludedCategories = new ArrayList<>();
        excludedCategories.add(mockExcludedCategory);

        final TargetOrderRestrictionModel mockTargetOrderRestriction = mock(TargetOrderRestrictionModel.class);
        given(mockTargetOrderRestriction.getIncludedCategories()).willReturn(includedCategories);
        given(mockTargetOrderRestriction.getExcludedCategories()).willReturn(excludedCategories);

        targetOrderRestrictionValidateInterceptor.onValidate(mockTargetOrderRestriction, null);
    }

    @Test
    public void testOnValidateWithIncludedAndExcludedProducts() throws Exception {
        final ProductModel mockIncludedProduct = mock(ProductModel.class);
        given(mockIncludedProduct.getCatalogVersion()).willReturn(mockOnlineCatalogVersion);

        final List<ProductModel> includedProducts = new ArrayList<>();
        includedProducts.add(mockIncludedProduct);

        final ProductModel mockExcludedProduct = mock(ProductModel.class);
        given(mockExcludedProduct.getCatalogVersion()).willReturn(mockOnlineCatalogVersion);

        final List<ProductModel> excludedProducts = new ArrayList<>();
        excludedProducts.add(mockExcludedProduct);

        final TargetOrderRestrictionModel mockTargetOrderRestriction = mock(TargetOrderRestrictionModel.class);
        given(mockTargetOrderRestriction.getIncludedProducts()).willReturn(includedProducts);
        given(mockTargetOrderRestriction.getExcludedProducts()).willReturn(excludedProducts);

        targetOrderRestrictionValidateInterceptor.onValidate(mockTargetOrderRestriction, null);
    }
}
