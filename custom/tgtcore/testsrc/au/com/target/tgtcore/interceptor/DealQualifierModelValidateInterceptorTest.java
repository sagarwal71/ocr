package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.ArrayList;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.interceptor.DealQualifierModelValidateInterceptor.ErrorMessages;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import org.junit.Assert;


@UnitTest
public class DealQualifierModelValidateInterceptorTest {

    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";

    private final DealQualifierModelValidateInterceptor dealQualifierModelValidateInterceptor = new DealQualifierModelValidateInterceptor();

    @Test
    public void testOnValidateWithNullModel() throws InterceptorException {
        dealQualifierModelValidateInterceptor.onValidate(null, null);
    }

    @Test
    public void testOnValidateWithWrongModelType() throws InterceptorException {
        final ProductPromotionModel mockProductPromotionModel = Mockito.mock(ProductPromotionModel.class);

        dealQualifierModelValidateInterceptor.onValidate(mockProductPromotionModel, null);

        Mockito.verifyZeroInteractions(mockProductPromotionModel);
    }

    @Test
    public void testOnValidateWithNoMinQtyAndNoMinAmt() throws InterceptorException {
        final DealQualifierModel mockDealQualifierModel = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifierModel.getMinQty()).willReturn(null);
        BDDMockito.given(mockDealQualifierModel.getMinAmt()).willReturn(null);

        try {
            dealQualifierModelValidateInterceptor.onValidate(mockDealQualifierModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.MIN_QTY_MIN_AMT_REQUIRED);
        }

    }

    @Test
    public void testOnValidateWithMinQtyTooLow() throws InterceptorException {
        final DealQualifierModel mockDealQualifierModel = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifierModel.getMinQty()).willReturn(Integer.valueOf(-1));
        BDDMockito.given(mockDealQualifierModel.getMinAmt()).willReturn(null);

        try {
            dealQualifierModelValidateInterceptor.onValidate(mockDealQualifierModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(EXCEPTION_MESSAGE_PREFIX + ErrorMessages.MIN_QTY_TOO_LOW);
        }
    }

    @Test
    public void testOnValidateWithMinAmtTooLow() throws InterceptorException {
        final DealQualifierModel mockDealQualifierModel = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifierModel.getMinQty()).willReturn(null);
        BDDMockito.given(mockDealQualifierModel.getMinAmt()).willReturn(Double.valueOf(-0.1));

        try {
            dealQualifierModelValidateInterceptor.onValidate(mockDealQualifierModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(EXCEPTION_MESSAGE_PREFIX + ErrorMessages.MIN_AMT_TOO_LOW);
        }
    }

    @Test
    public void testOnValidateWithMinAmtQtyZero() throws InterceptorException {
        final DealQualifierModel mockDealQualifierModel = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifierModel.getMinQty()).willReturn(null);
        BDDMockito.given(mockDealQualifierModel.getMinAmt()).willReturn(Double.valueOf(0.0));
        BDDMockito.given(mockDealQualifierModel.getMinQty()).willReturn(Integer.valueOf(0));

        try {
            dealQualifierModelValidateInterceptor.onValidate(mockDealQualifierModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.MIN_AMT_AND_QTY_ZERO);
        }
    }

    @Test
    public void testOnValidateWithMinAmtQtyNull() throws InterceptorException {
        final DealQualifierModel mockDealQualifierModel = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifierModel.getMinQty()).willReturn(null);

        try {
            dealQualifierModelValidateInterceptor.onValidate(mockDealQualifierModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.MIN_AMT_AND_QTY_ZERO);
        }
    }

    @Test
    public void testOnValidateWithNoDealCategories() throws InterceptorException {
        final DealQualifierModel mockDealQualifierModel = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifierModel.getMinQty()).willReturn(Integer.valueOf(1));
        BDDMockito.given(mockDealQualifierModel.getMinAmt()).willReturn(Double.valueOf(0.1));

        try {
            dealQualifierModelValidateInterceptor.onValidate(mockDealQualifierModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(EXCEPTION_MESSAGE_PREFIX + ErrorMessages.DEAL_CAT_MINIMUM);
        }
    }

    @Test
    public void testOnValidateWithTooManyStagedDealCategories() throws InterceptorException {
        final DealQualifierModel mockDealQualifierModel = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifierModel.getMinQty()).willReturn(null);
        BDDMockito.given(mockDealQualifierModel.getMinAmt()).willReturn(Double.valueOf(0.1));

        final CatalogVersionModel mockCatalogVersion = BDDMockito.mock(CatalogVersionModel.class);
        BDDMockito.given(mockCatalogVersion.getActive()).willReturn(Boolean.FALSE);

        final List<TargetDealCategoryModel> dealCategories = new ArrayList<>();
        final TargetDealCategoryModel mockTargetDealCategoryModel1 = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(mockTargetDealCategoryModel1.getCatalogVersion()).willReturn(mockCatalogVersion);
        dealCategories.add(mockTargetDealCategoryModel1);
        final TargetDealCategoryModel mockTargetDealCategoryModel2 = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(mockTargetDealCategoryModel2.getCatalogVersion()).willReturn(mockCatalogVersion);
        dealCategories.add(mockTargetDealCategoryModel2);
        BDDMockito.given(mockDealQualifierModel.getDealCategory()).willReturn(dealCategories);

        try {
            dealQualifierModelValidateInterceptor.onValidate(mockDealQualifierModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.DEAL_CAT_TOO_MANY_STAGED);
        }
    }

    @Test
    public void testOnValidate() throws InterceptorException {
        final DealQualifierModel mockDealQualifierModel = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifierModel.getMinQty()).willReturn(Integer.valueOf(1));
        BDDMockito.given(mockDealQualifierModel.getMinAmt()).willReturn(null);

        final CatalogVersionModel mockCatalogVersionStaged = BDDMockito.mock(CatalogVersionModel.class);
        BDDMockito.given(mockCatalogVersionStaged.getActive()).willReturn(Boolean.FALSE);

        final CatalogVersionModel mockCatalogVersionOnline = BDDMockito.mock(CatalogVersionModel.class);
        BDDMockito.given(mockCatalogVersionStaged.getActive()).willReturn(Boolean.TRUE);

        final List<TargetDealCategoryModel> dealCategories = new ArrayList<>();
        final TargetDealCategoryModel mockTargetDealCategoryModel1 = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(mockTargetDealCategoryModel1.getCatalogVersion()).willReturn(mockCatalogVersionStaged);
        dealCategories.add(mockTargetDealCategoryModel1);
        final TargetDealCategoryModel mockTargetDealCategoryModel2 = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(mockTargetDealCategoryModel2.getCatalogVersion()).willReturn(mockCatalogVersionOnline);
        dealCategories.add(mockTargetDealCategoryModel2);
        BDDMockito.given(mockDealQualifierModel.getDealCategory()).willReturn(dealCategories);

        dealQualifierModelValidateInterceptor.onValidate(mockDealQualifierModel, null);
    }
}
