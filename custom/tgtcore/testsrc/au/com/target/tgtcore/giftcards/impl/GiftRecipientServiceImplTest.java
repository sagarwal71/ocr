package au.com.target.tgtcore.giftcards.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.giftcards.converter.GiftRecipientFormConverter;
import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;
import au.com.target.tgtcore.model.GiftRecipientModel;


/**
 * Unit tests for GiftRecipientServiceImpl.
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GiftRecipientServiceImplTest {

    @Mock
    private ModelService modelService;

    @Mock
    private GiftRecipientFormConverter giftRecipientFormConverter;

    @Mock
    private GiftRecipientDTO recipientDto;

    @InjectMocks
    private final GiftRecipientServiceImpl giftRecipientService = new GiftRecipientServiceImpl();

    @Test(expected = IllegalArgumentException.class)
    public void testAddGiftRecipientToOrderEntryWithNullRecipient() {
        final CartEntryModel cartEntryModel = Mockito.mock(CartEntryModel.class);
        giftRecipientService.addGiftRecipientToOrderEntry(null, cartEntryModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddGiftRecipientToOrderEntryWithNullOrderEntry() {
        giftRecipientService.addGiftRecipientToOrderEntry(recipientDto, null);
    }

    @Test
    public void testAddGiftRecipient() {
        final GiftRecipientModel giftRecipientModel = new GiftRecipientModel();
        final OrderEntryModel orderEntryModel = new OrderEntryModel();
        Mockito.when(giftRecipientFormConverter.convert(recipientDto)).thenReturn(giftRecipientModel);
        giftRecipientService.addGiftRecipientToOrderEntry(recipientDto, orderEntryModel);
        Assert.assertNotNull(giftRecipientModel.getOrderEntry());
    }

}
