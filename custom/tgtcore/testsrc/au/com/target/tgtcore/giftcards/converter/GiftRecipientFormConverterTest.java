package au.com.target.tgtcore.giftcards.converter;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;
import au.com.target.tgtcore.model.GiftRecipientModel;


/**
 * Tests for GiftRecipientConverter
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GiftRecipientFormConverterTest {

    @Mock
    private ModelService modelService;

    @InjectMocks
    private final GiftRecipientFormConverter converter = new GiftRecipientFormConverter();

    @Before
    public void setupTest() {
        BDDMockito.given(modelService.create(GiftRecipientModel.class)).willReturn(new GiftRecipientModel());
    }

    @Test
    public void testDTOWithNoValue() {
        final GiftRecipientDTO dto = new GiftRecipientDTO();
        final GiftRecipientModel model = converter.convert(dto);
        Assert.assertNotNull(model);
        Assert.assertNull(model.getFirstName());
        Assert.assertNull(model.getLastName());
        Assert.assertNull(model.getMessageText());
        Assert.assertNull(model.getEmail());
    }

    @Test
    public void testConvertWithValues() {
        final GiftRecipientDTO dto = createTestDto();
        final GiftRecipientModel model = converter.convert(dto);
        Assert.assertNotNull(model);
        Assert.assertEquals("firstName", model.getFirstName());
        Assert.assertEquals("lastName", model.getLastName());
        Assert.assertEquals("Message Text", model.getMessageText());
        Assert.assertEquals("firstName@email.com", model.getEmail());
    }

    @Test
    public void testUpdateWithValues() {
        final GiftRecipientDTO dto = createTestDto();
        final GiftRecipientModel model = new GiftRecipientModel();
        converter.update(model, dto);
        Assert.assertEquals("firstName", model.getFirstName());
        Assert.assertEquals("lastName", model.getLastName());
        Assert.assertEquals("Message Text", model.getMessageText());
        Assert.assertEquals("firstName@email.com", model.getEmail());
    }


    private GiftRecipientDTO createTestDto() {

        final GiftRecipientDTO dto = new GiftRecipientDTO();
        dto.setFirstName("firstName");
        dto.setLastName("lastName");
        dto.setMessageText("Message Text");
        dto.setRecipientEmailAddress("firstName@email.com");
        return dto;
    }

}
