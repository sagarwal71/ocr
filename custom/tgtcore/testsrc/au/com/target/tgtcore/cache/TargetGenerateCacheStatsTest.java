/**
 * 
 */
package au.com.target.tgtcore.cache;


import static org.mockito.BDDMockito.given;

import au.com.target.tgtutility.registry.RegistryFacade;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.ItemDeployment;
import de.hybris.platform.core.Registry;
import de.hybris.platform.persistence.property.PersistenceManager;
import de.hybris.platform.regioncache.CacheController;
import de.hybris.platform.regioncache.CacheStatistics;
import de.hybris.platform.regioncache.region.CacheRegion;
import de.hybris.platform.servicelayer.tenant.MockTenant;
import de.hybris.platform.util.typesystem.YItemDeploymentWrapper;

import java.util.ArrayList;
import java.util.Collection;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.cache.dto.TargetCacheDataDTO;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetGenerateCacheStatsTest {

    @Mock
    private CacheController controller;

    @Mock
    private MockTenant currentTenant;
    
    @Mock
    private RegistryFacade registryFacade;

    @InjectMocks
    private final TargetGenerateCacheStatsTestable targetGenerateCacheStats = new TargetGenerateCacheStatsTestable();

    private class TargetGenerateCacheStatsTestable extends TargetGenerateCacheStats {
        private int totalNumberOfObjectsLogged = 0;

        public int getTotalNumberOfObjectsLogged() {
            return totalNumberOfObjectsLogged;
        }

        @Override
        protected void logDtoData(final TargetCacheDataDTO cacheDto) {
            super.logDtoData(cacheDto);
            totalNumberOfObjectsLogged++;
        }
    }

    @Test
    public void testGetFormattedCacheStatistics() {
       
        final Collection<CacheRegion> cacheRegions = new ArrayList<>();
        final CacheRegion cacheRegion1 = Mockito.mock(CacheRegion.class);
        final CacheRegion cacheRegion2 = Mockito.mock(CacheRegion.class);
        cacheRegions.add(cacheRegion1);
        cacheRegions.add(cacheRegion2);

        given(controller.getRegions()).willReturn(cacheRegions);

        given(cacheRegion1.getName()).willReturn("region1");
        given(cacheRegion2.getName()).willReturn("region2");
        final CacheStatistics regionCacheStatistics1 = Mockito.mock(CacheStatistics.class);
        given(cacheRegion1.getCacheRegionStatistics()).willReturn(regionCacheStatistics1);
        given(Long.valueOf(regionCacheStatistics1.getEvictions())).willReturn(Long.valueOf(100));
        given(Long.valueOf(regionCacheStatistics1.getInvalidations())).willReturn(Long.valueOf(200));
        given(Long.valueOf(regionCacheStatistics1.getMisses())).willReturn(Long.valueOf(300));
        given(Long.valueOf(regionCacheStatistics1.getInstanceCount())).willReturn(Long.valueOf(400));
        given(Long.valueOf(regionCacheStatistics1.getHits())).willReturn(Long.valueOf(200));
        given(Long.valueOf(regionCacheStatistics1.getFetches())).willReturn(Long.valueOf(300));

        final Collection<Object> region1types = new ArrayList<>();
        final String region1type1 = "11";
        final String region1type2 = "12";
        region1types.add(region1type1);
        region1types.add(region1type2);
        given(regionCacheStatistics1.getTypes()).willReturn(region1types);

        final ItemDeployment itemDeploymentRegion1Type1 = Mockito.mock(YItemDeploymentWrapper.class);
        given(registryFacade.getItemDeploymentTypeByTypeCode(11)).willReturn(itemDeploymentRegion1Type1);
        given(itemDeploymentRegion1Type1.getName()).willReturn("region1type1");

        final ItemDeployment itemDeploymentRegion1Type2 = Mockito.mock(YItemDeploymentWrapper.class);
        given(registryFacade.getItemDeploymentTypeByTypeCode(12)).willReturn(itemDeploymentRegion1Type2);
        given(itemDeploymentRegion1Type2.getName()).willReturn("region1type2");


        given(Long.valueOf(regionCacheStatistics1.getEvictions("11"))).willReturn(Long.valueOf(100));
        given(Long.valueOf(regionCacheStatistics1.getInvalidations("11"))).willReturn(Long.valueOf(200));
        given(Long.valueOf(regionCacheStatistics1.getMisses("11"))).willReturn(Long.valueOf(300));
        given(Long.valueOf(regionCacheStatistics1.getInstanceCount("11"))).willReturn(Long.valueOf(400));
        given(Long.valueOf(regionCacheStatistics1.getHits("11"))).willReturn(Long.valueOf(200));
        given(Long.valueOf(regionCacheStatistics1.getFetches("11"))).willReturn(Long.valueOf(300));

        given(Long.valueOf(regionCacheStatistics1.getEvictions("12"))).willReturn(Long.valueOf(200));
        given(Long.valueOf(regionCacheStatistics1.getInvalidations("12"))).willReturn(Long.valueOf(300));
        given(Long.valueOf(regionCacheStatistics1.getMisses("12"))).willReturn(Long.valueOf(400));
        given(Long.valueOf(regionCacheStatistics1.getInstanceCount("12"))).willReturn(Long.valueOf(500));
        given(Long.valueOf(regionCacheStatistics1.getHits("12"))).willReturn(Long.valueOf(600));
        given(Long.valueOf(regionCacheStatistics1.getFetches("12"))).willReturn(Long.valueOf(700));


        final CacheStatistics regionCacheStatistics2 = Mockito.mock(CacheStatistics.class);
        given(cacheRegion2.getCacheRegionStatistics()).willReturn(regionCacheStatistics2);
        given(Long.valueOf(regionCacheStatistics2.getEvictions())).willReturn(Long.valueOf(100));
        given(Long.valueOf(regionCacheStatistics2.getInvalidations())).willReturn(Long.valueOf(200));
        given(Long.valueOf(regionCacheStatistics2.getMisses())).willReturn(Long.valueOf(300));
        given(Long.valueOf(regionCacheStatistics2.getInstanceCount())).willReturn(Long.valueOf(400));
        given(Long.valueOf(regionCacheStatistics2.getHits())).willReturn(Long.valueOf(200));
        given(Long.valueOf(regionCacheStatistics2.getFetches())).willReturn(Long.valueOf(300));

        final Collection<Object> region2types = new ArrayList<>();
        final String region2type1 = "21";
        final String region2type2 = "22";
        region2types.add(region2type1);
        region2types.add(region2type2);
        given(regionCacheStatistics2.getTypes()).willReturn(region2types);

        final ItemDeployment itemDeploymentRegion2Type1 = Mockito.mock(YItemDeploymentWrapper.class);
        given(registryFacade.getItemDeploymentTypeByTypeCode(11)).willReturn(itemDeploymentRegion2Type1);
        given(itemDeploymentRegion2Type1.getName()).willReturn("region2type1");

        final ItemDeployment itemDeploymentRegion2Type2 = Mockito.mock(YItemDeploymentWrapper.class);
        given(registryFacade.getItemDeploymentTypeByTypeCode(12)).willReturn(itemDeploymentRegion2Type2);
        given(itemDeploymentRegion2Type2.getName()).willReturn("region2type2");


        given(Long.valueOf(regionCacheStatistics2.getEvictions("21"))).willReturn(Long.valueOf(100));
        given(Long.valueOf(regionCacheStatistics2.getInvalidations("21"))).willReturn(Long.valueOf(200));
        given(Long.valueOf(regionCacheStatistics2.getMisses("21"))).willReturn(Long.valueOf(300));
        given(Long.valueOf(regionCacheStatistics2.getInstanceCount("21"))).willReturn(Long.valueOf(400));
        given(Long.valueOf(regionCacheStatistics2.getHits("21"))).willReturn(Long.valueOf(200));
        given(Long.valueOf(regionCacheStatistics2.getFetches("21"))).willReturn(Long.valueOf(300));

        given(Long.valueOf(regionCacheStatistics2.getEvictions("22"))).willReturn(Long.valueOf(2));
        given(Long.valueOf(regionCacheStatistics2.getInvalidations("22"))).willReturn(Long.valueOf(44));
        given(Long.valueOf(regionCacheStatistics2.getMisses("22"))).willReturn(Long.valueOf(33));
        given(Long.valueOf(regionCacheStatistics2.getInstanceCount("22"))).willReturn(Long.valueOf(44));
        given(Long.valueOf(regionCacheStatistics2.getHits("22"))).willReturn(Long.valueOf(55));
        given(Long.valueOf(regionCacheStatistics2.getFetches("22"))).willReturn(Long.valueOf(66));


        targetGenerateCacheStats.generateCacheStatsForEntityCache();
        Assertions.assertThat((targetGenerateCacheStats.getTotalNumberOfObjectsLogged())).isEqualTo(6);

    }

}
