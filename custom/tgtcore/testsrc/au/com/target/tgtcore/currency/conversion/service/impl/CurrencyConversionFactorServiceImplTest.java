/**
 * 
 */
package au.com.target.tgtcore.currency.conversion.service.impl;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.Boolean.valueOf;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.currency.conversion.dao.CurrencyConversionFactorDao;


/**
 * @author Nandini
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CurrencyConversionFactorServiceImplTest {

    @InjectMocks
    private final CurrencyConversionFactorServiceImpl conversionFactor = new CurrencyConversionFactorServiceImpl();

    @Mock
    private CurrencyConversionFactorDao currencyConversionFactorDao;

    @Mock
    private OrderModel order;

    @Mock
    private CurrencyModel currency;

    @Mock
    private CurrencyModel orderCurrency;

    @Mock
    private CommonI18NService commonI18NService;


    @Test
    public void testFindLatestCurrenyFactorForNullOrder() {
        conversionFactor.findLatestCurrencyFactor(null);
        Mockito.verifyZeroInteractions(currencyConversionFactorDao);
    }

    @Test
    public void testFindLatestCurrenyFactorForNullDate() {
        conversionFactor.findLatestCurrencyFactor(null);
        Mockito.verifyZeroInteractions(currencyConversionFactorDao);
    }

    @Test
    public void testFindLatestCurrenyFactor() {
        final Date date = new Date();
        BDDMockito.given(order.getDate()).willReturn(date);
        BDDMockito.given(order.getCurrency()).willReturn(orderCurrency);
        BDDMockito.given(currency.getBase()).willReturn(Boolean.TRUE);
        BDDMockito.when(commonI18NService.getBaseCurrency()).thenReturn(currency);
        conversionFactor.findLatestCurrencyFactor(order);
        verify(currencyConversionFactorDao).findLatestCurrencyFactor(date, orderCurrency, currency);
    }

    @Test
    public void testConvertPriceToBaseCurrency() {
        final double price = 99999.99d;
        final double factor = 0.934271d;
        BDDMockito.given(order.getCurrency()).willReturn(orderCurrency);
        BDDMockito.when(order.getCurrencyConversionFactor()).thenReturn(Double.valueOf(factor));
        BDDMockito.when(commonI18NService.getBaseCurrency()).thenReturn(currency);
        final double result = conversionFactor.convertPriceToBaseCurrency(price, order);
        Assert.assertEquals(Double.valueOf(93427.09d), Double.valueOf(result));
    }

    @Test
    public void testConvertPriceToBaseCurrencyWithZeroPrice() {
        final double price = 0d;
        final double factor = 0.934271d;
        BDDMockito.given(order.getCurrency()).willReturn(orderCurrency);
        BDDMockito.when(order.getCurrencyConversionFactor()).thenReturn(Double.valueOf(factor));
        BDDMockito.when(commonI18NService.getBaseCurrency()).thenReturn(currency);
        final double result = conversionFactor.convertPriceToBaseCurrency(price, order);
        Assert.assertEquals(Double.valueOf(0d), Double.valueOf(result));
    }

    @Test
    public void testConvertPriceToBaseCurrencyWithOrderCurrencyEqualsBaseCurrency() {
        final double price = 21.59d;
        BDDMockito.given(order.getCurrency()).willReturn(currency);
        BDDMockito.when(order.getCurrencyConversionFactor()).thenReturn(null);
        BDDMockito.when(commonI18NService.getBaseCurrency()).thenReturn(currency);
        final double result = conversionFactor.convertPriceToBaseCurrency(price, order);
        Assert.assertEquals(Double.valueOf(21.59d), Double.valueOf(result));
    }

    @Test
    public void testConvertPriceToBaseCurrencyWithNullOrder() {
        final double price = 21.59d;
        final double result = conversionFactor.convertPriceToBaseCurrency(price, null);
        Assert.assertEquals(Double.valueOf(21.59d), Double.valueOf(result));
    }

    @Test
    public void testConvertPriceToBaseCurrencyWithNegativePrice() {
        final double price = -99999.99d;
        final double factor = 0.934271d;
        BDDMockito.given(order.getCurrency()).willReturn(orderCurrency);
        BDDMockito.when(order.getCurrencyConversionFactor()).thenReturn(Double.valueOf(factor));
        BDDMockito.when(commonI18NService.getBaseCurrency()).thenReturn(currency);
        final double result = conversionFactor.convertPriceToBaseCurrency(price, order);
        Assert.assertEquals(Double.valueOf(-93427.09d), Double.valueOf(result));
    }

    @Test
    public void testConvertPriceToBaseCurrencyWithZeroConversion() {
        final double price = -99999.99d;
        final double factor = 0.0d;
        BDDMockito.given(order.getCurrency()).willReturn(orderCurrency);
        BDDMockito.when(order.getCurrencyConversionFactor()).thenReturn(Double.valueOf(factor));
        BDDMockito.when(commonI18NService.getBaseCurrency()).thenReturn(currency);
        final double result = conversionFactor.convertPriceToBaseCurrency(price, order);
        Assert.assertEquals(Double.valueOf(0.0d), Double.valueOf(result));
    }

    @Test
    public void testCanConvertToBaseCurrencyNullOrder() {
        final boolean result = conversionFactor.canConvertToBaseCurrency(null);
        Assert.assertEquals(FALSE, valueOf(result));
    }

    @Test
    public void testCanConvertToBaseCurrencyConversionFactorNotBaseCurrency() {
        final double factor = 0.934271d;
        BDDMockito.given(order.getCurrency()).willReturn(orderCurrency);
        BDDMockito.when(order.getCurrencyConversionFactor()).thenReturn(Double.valueOf(factor));
        BDDMockito.when(commonI18NService.getBaseCurrency()).thenReturn(currency);
        final boolean result = conversionFactor.canConvertToBaseCurrency(order);
        Assert.assertEquals(TRUE, valueOf(result));
    }

    @Test
    public void testCanConvertToBaseCurrencyBaseCurrencyNoConversionFactor() {
        BDDMockito.given(order.getCurrency()).willReturn(currency);
        BDDMockito.when(order.getCurrencyConversionFactor()).thenReturn(null);
        BDDMockito.when(commonI18NService.getBaseCurrency()).thenReturn(currency);
        final boolean result = conversionFactor.canConvertToBaseCurrency(order);
        Assert.assertEquals(TRUE, valueOf(result));
    }

    @Test
    public void testCanConvertToBaseCurrencyNoConversionFactorNotBaseCurrency() {
        BDDMockito.given(order.getCurrency()).willReturn(orderCurrency);
        BDDMockito.when(order.getCurrencyConversionFactor()).thenReturn(null);
        BDDMockito.when(commonI18NService.getBaseCurrency()).thenReturn(currency);
        final boolean result = conversionFactor.canConvertToBaseCurrency(order);
        Assert.assertEquals(FALSE, valueOf(result));
    }

    @Test
    public void testCanConvertToBaseCurrencyConversionFactorNoBaseCurrency() {
        final double factor = 0.934271d;
        BDDMockito.given(order.getCurrency()).willReturn(orderCurrency);
        BDDMockito.when(order.getCurrencyConversionFactor()).thenReturn(Double.valueOf(factor));
        BDDMockito.when(commonI18NService.getBaseCurrency()).thenReturn(null);
        final boolean result = conversionFactor.canConvertToBaseCurrency(order);
        Assert.assertEquals(TRUE, valueOf(result));
    }
}
