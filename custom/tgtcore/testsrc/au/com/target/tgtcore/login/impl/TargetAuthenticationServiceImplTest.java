/**
 * 
 */
package au.com.target.tgtcore.login.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetCustomerModel;


@UnitTest
@SuppressWarnings("boxing")
@RunWith(MockitoJUnitRunner.class)
public class TargetAuthenticationServiceImplTest {

    @Mock
    private TargetCustomerModel customer;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private final TargetAuthenticationServiceImpl targetAuthenticationServiceImpl = new TargetAuthenticationServiceImpl();

    @Before
    public void setup() {
        targetAuthenticationServiceImpl.setFailedLoginAttemptsAllowed(5);
    }

    @Test
    public void testGetFailedLoginAttemptsTrue() {
        BDDMockito.given(customer.getFailedLoginAttempts()).willReturn(5);
        final Boolean failed = targetAuthenticationServiceImpl.checkAndIncrementLoginAttempts(customer);
        Assert.assertEquals(Boolean.TRUE, failed);
    }

    @Test
    public void testGetFailedLoginAttemptsFalse() {
        BDDMockito.given(customer.getFailedLoginAttempts()).willReturn(2);
        final Boolean failed = targetAuthenticationServiceImpl.checkAndIncrementLoginAttempts(customer);
        Mockito.verify(modelService).save(customer);
        Assert.assertEquals(Boolean.FALSE, failed);
    }

    @Test
    public void testResetLoginAttempts() {
        customer.setFailedLoginAttempts(2);
        targetAuthenticationServiceImpl.checkAndIncrementLoginAttempts(customer);
        Mockito.verify(modelService).save(customer);
        Assert.assertEquals(0, customer.getFailedLoginAttempts().intValue());
    }
}
