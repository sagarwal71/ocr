package au.com.target.tgtcore.taxinvoice.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commons.jalo.CommonsManager;
import de.hybris.platform.commons.jalo.Document;
import de.hybris.platform.commons.jalo.Format;
import de.hybris.platform.commons.model.DocumentModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.order.Order;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.document.dao.TargetDocumentDao;
import au.com.target.tgtcore.enums.DocumentType;
import au.com.target.tgtcore.taxinvoice.TaxInvoiceException;
import au.com.target.tgtcore.taxinvoice.dao.TaxInvoiceDao;
import au.com.target.tgtcore.taxinvoice.data.TaxInvoiceLineItem;


/**
 * Unit Test for {@link TaxInvoiceServiceImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TaxInvoiceServiceImplTest {

    protected class MyTaxInvoiceServiceImpl extends TaxInvoiceServiceImpl {

        @Override
        protected CommonsManager getCommonsManagerInstance() {
            return commonsManager;
        }
    }

    private static final String PROMOTION_CODE = "12345";

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException exception = ExpectedException.none();
    //CHECKSTYLE:ON

    @Mock
    private ModelService modelService;

    @Mock
    private CommonsManager commonsManager;

    @Mock
    private OrderModel order;

    @Mock
    private AbstractOrderEntryModel entry;

    @Mock
    private TaxInvoiceLineItem taxInvoiceLineItem;

    @Mock
    private ProductModel product;

    @Mock
    private TaxInvoiceLineItem taxInvoiceLineItem2;

    @Mock
    private TaxInvoiceDao taxInvoiceDao;

    @Mock
    private TypeService typeService;

    @Mock
    private TargetDocumentDao targetDocumentDao;

    @InjectMocks
    @Spy
    private final MyTaxInvoiceServiceImpl taxInvoiceService = new MyTaxInvoiceServiceImpl();

    @Before
    public void setUp() {
        taxInvoiceService.setTaxInvoiceTemplateCode("TaxInvoiceTemplate");

        final List<AbstractOrderEntryModel> list = new ArrayList<>();
        list.add(entry);
        BDDMockito.given(order.getEntries()).willReturn(list);
    }

    @Test
    public void testWithNullOrder() throws TaxInvoiceException {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Order cannot be null");
        taxInvoiceService.generateTaxInvoiceForOrder(null);
    }

    @Test
    public void testWithOrderNoAttachedFormats() throws TaxInvoiceException {

        exception.expect(TaxInvoiceException.class);
        exception.expectMessage("TaxInvoice template not found for order:");

        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final Order orderItem = Mockito.mock(Order.class);

        BDDMockito.given(modelService.toPersistenceLayer(orderModel)).willReturn(orderItem);

        taxInvoiceService.generateTaxInvoiceForOrder(orderModel);
    }

    @Test
    public void testWithOrderNonMatchingTaxInvoiceFormat() throws TaxInvoiceException {

        exception.expect(TaxInvoiceException.class);
        exception.expectMessage("TaxInvoice template not found for order:");

        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final Order orderItem = Mockito.mock(Order.class);
        final Format nonTaxInvoiceFormat = Mockito.mock(Format.class);

        BDDMockito.given(modelService.toPersistenceLayer(orderModel)).willReturn(orderItem);
        BDDMockito.given(nonTaxInvoiceFormat.getCode()).willReturn("dummy");
        BDDMockito.given(commonsManager.getFormatsForItem(orderItem)).willReturn(
                Collections.singletonList(nonTaxInvoiceFormat));

        taxInvoiceService.generateTaxInvoiceForOrder(orderModel);
    }

    @Test
    public void testExceptionWithValidOrder() throws TaxInvoiceException, JaloBusinessException {

        exception.expect(TaxInvoiceException.class);
        exception.expectMessage("Some Exception happened");

        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final Order orderItem = Mockito.mock(Order.class);
        final Format taxInvoiceFormat = Mockito.mock(Format.class);

        BDDMockito.given(modelService.toPersistenceLayer(orderModel)).willReturn(orderItem);
        BDDMockito.given(taxInvoiceFormat.getCode()).willReturn("TaxInvoiceTemplate");
        BDDMockito.given(commonsManager.getFormatsForItem(orderItem)).willReturn(
                Collections.singletonList(taxInvoiceFormat));
        BDDMockito.given(taxInvoiceFormat.format(orderItem)).willThrow(
                new JaloBusinessException("Some Exception happened"));

        taxInvoiceService.generateTaxInvoiceForOrder(orderModel);

        Mockito.verify(taxInvoiceFormat).format(orderItem);
    }

    @Test
    public void testWithValidOrder() throws TaxInvoiceException, JaloBusinessException {

        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final Order orderItem = Mockito.mock(Order.class);
        final Format taxInvoiceFormat = Mockito.mock(Format.class);
        final DocumentModel documentModel = Mockito.mock(DocumentModel.class);
        final Document documentItem = Mockito.mock(Document.class);

        BDDMockito.given(orderModel.getCode()).willReturn(PROMOTION_CODE);
        BDDMockito.given(modelService.toPersistenceLayer(orderModel)).willReturn(orderItem);
        BDDMockito.given(modelService.toModelLayer(documentItem)).willReturn(documentModel);
        BDDMockito.given(taxInvoiceFormat.getCode()).willReturn("TaxInvoiceTemplate");
        BDDMockito.given(commonsManager.getFormatsForItem(orderItem)).willReturn(
                Collections.singletonList(taxInvoiceFormat));
        BDDMockito.given(taxInvoiceFormat.format(orderItem)).willReturn(documentItem);

        final DocumentModel result = taxInvoiceService.generateTaxInvoiceForOrder(orderModel);

        Assert.assertNotNull(result);
        Mockito.verify(taxInvoiceFormat).format(orderItem);
        Mockito.verify(documentModel).setCode("INV-12345");
        Mockito.verify(documentModel).setRealFileName("INV-12345.pdf");
        Mockito.verify(documentModel).setDocumentType(DocumentType.INVOICE);
        Mockito.verify(modelService).save(documentModel);
    }


}
