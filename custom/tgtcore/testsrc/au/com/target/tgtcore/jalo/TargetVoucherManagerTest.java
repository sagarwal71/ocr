/**
 * 
 */
package au.com.target.tgtcore.jalo;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.Tenant;
import de.hybris.platform.jalo.order.Cart;
import de.hybris.platform.jalo.order.Order;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.voucher.jalo.Voucher;
import de.hybris.platform.voucher.jalo.VoucherInvalidation;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.voucher.service.TargetVoucherService;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetVoucherManagerTest {

    @Mock
    private TargetVoucherService targetVoucherService;

    @InjectMocks
    private final TargetVoucherManager targetVoucherManger = new TargetVoucherManager() {
        @Override
        public void setTenant(final Tenant tenant) {
            //Do nothing
        }

        @Override
        public Voucher getVoucher(final String voucherCode) {
            return mockVoucher;
        }
    };


    private Cart mockCart;

    private Order mockOrder;

    private Voucher mockVoucher;

    @Before
    public void setup() {
        mockVoucher = Mockito.mock(Voucher.class);
        mockCart = Mockito.mock(Cart.class);
        mockOrder = Mockito.mock(Order.class);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testRedeemInvalidForCartVoucher() throws JaloPriceFactoryException {
        final String voucherCode = "ONLINEDISABLED";
        BDDMockito.given(targetVoucherService.isVoucherEnabledOnline(voucherCode)).willReturn(false);
        Assertions.assertThat(targetVoucherManger.redeemVoucher(voucherCode, mockCart)).isFalse();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testRedeemOnlineEnabledVoucherForCartWithRedeemVoucherFalse() throws JaloPriceFactoryException {
        final String voucherCode = "ONLINEENABLEDREDEEMINVALIDVOUCHER";
        BDDMockito.given(targetVoucherService.isVoucherEnabledOnline(voucherCode)).willReturn(true);
        BDDMockito.given(mockVoucher.redeem(voucherCode, mockCart)).willReturn(false);
        Assertions.assertThat(targetVoucherManger.redeemVoucher(voucherCode, mockCart)).isFalse();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testRedeemOnlineEnabledVoucherForCartWithRedeemVoucherTrue() throws JaloPriceFactoryException {
        final String voucherCode = "ONLINEENABLEDREDEEMVOUCHER";
        BDDMockito.given(targetVoucherService.isVoucherEnabledOnline(voucherCode)).willReturn(true);
        BDDMockito.given(mockVoucher.redeem(voucherCode, mockCart)).willReturn(true);
        Assertions.assertThat(targetVoucherManger.redeemVoucher(voucherCode, mockCart)).isTrue();
    }


    @SuppressWarnings("boxing")
    @Test
    public void testRedeemInvalidForOrderVoucher() throws JaloPriceFactoryException {
        final String voucherCode = "ONLINEDISABLED";
        BDDMockito.given(targetVoucherService.isVoucherEnabledOnline(voucherCode)).willReturn(false);
        Assertions.assertThat(targetVoucherManger.redeemVoucher(voucherCode, mockOrder)).isNull();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testRedeemOnlineEnabledVoucherForOrderWithRedeemVoucher() throws JaloPriceFactoryException {
        final String voucherCode = "VALIDREDEEMVOUCHER";
        BDDMockito.given(targetVoucherService.isVoucherEnabledOnline(voucherCode)).willReturn(true);
        final VoucherInvalidation voucherInvalidationMock = Mockito.mock(VoucherInvalidation.class);
        BDDMockito.given(mockVoucher.redeem(voucherCode, mockOrder)).willReturn(voucherInvalidationMock);
        Assertions.assertThat(targetVoucherManger.redeemVoucher(voucherCode, mockOrder)).isEqualTo(
                voucherInvalidationMock);
    }
}
