/**
 * 
 */
package au.com.target.tgtcore.jalo;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * @author pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("deprecation")
public class TargetMaxVolumeZDMVRestrictionTest {

    @Mock
    private TargetMaxVolumeZDMVRestriction targetMaxVolumeZDMVRestriction;

    @Mock
    private ModelService modelService;

    @Mock
    private Product product;

    @Mock
    private TargetColourVariantProductModel productModel;

    @Mock
    private TargetProductDimensionsModel targetProductDimensions;

    @Mock
    private CartModel cart;

    private final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        given(Boolean.valueOf(targetMaxVolumeZDMVRestriction.evaluate(product))).willCallRealMethod();
        given(Boolean.valueOf(targetMaxVolumeZDMVRestriction.evaluate(cart))).willCallRealMethod();
        given(targetMaxVolumeZDMVRestriction.getModelService()).willReturn(modelService);
        given(Double.valueOf(targetMaxVolumeZDMVRestriction.getMaxVolumeAsPrimitive()))
                .willReturn(Double.valueOf(1000.0));
        given(modelService.get(product)).willReturn(productModel);
        given(productModel.getProductPackageDimensions()).willReturn(targetProductDimensions);
        given(targetProductDimensions.getLength()).willReturn(Double.valueOf(10.0));
        given(targetProductDimensions.getWidth()).willReturn(Double.valueOf(10.0));
        given(targetProductDimensions.getHeight()).willReturn(Double.valueOf(10.0));
    }

    @Test
    public void testEvaluateProductWhenDimensionsAreValidAndVolumeIsWithinLimit() {
        assertThat(targetMaxVolumeZDMVRestriction.evaluate(product)).isTrue();
    }

    @Test
    public void testEvaluateProductWhenDimensionsAreValidAndVolumeIsAboveLimit() {
        given(targetProductDimensions.getLength()).willReturn(Double.valueOf(10.01));
        assertThat(targetMaxVolumeZDMVRestriction.evaluate(product)).isFalse();
    }

    @Test
    public void testEvaluateProductWhenAllDimensionAreZero() {
        given(targetProductDimensions.getLength()).willReturn(Double.valueOf(0));
        given(targetProductDimensions.getWidth()).willReturn(Double.valueOf(0));
        given(targetProductDimensions.getHeight()).willReturn(Double.valueOf(0));
        assertThat(targetMaxVolumeZDMVRestriction.evaluate(product)).isTrue();
    }

    @Test
    public void testEvaluateProductWhenDimensionIsMissing() {
        given(productModel.getProductPackageDimensions()).willReturn(null);
        assertThat(targetMaxVolumeZDMVRestriction.evaluate(product)).isFalse();
    }

    @Test
    public void testEvaluateProductWhenDimensionHasMissingLength() {
        given(targetProductDimensions.getLength()).willReturn(null);
        assertThat(targetMaxVolumeZDMVRestriction.evaluate(product)).isFalse();
    }

    @Test
    public void testEvaluateProductWhenDimensionHasMissingWidth() {
        given(targetProductDimensions.getWidth()).willReturn(null);
        assertThat(targetMaxVolumeZDMVRestriction.evaluate(product)).isFalse();
    }

    @Test
    public void testEvaluateProductWhenDimensionHasMissingHeight() {
        given(targetProductDimensions.getHeight()).willReturn(null);
        assertThat(targetMaxVolumeZDMVRestriction.evaluate(product)).isFalse();
    }

    @Test
    public void testEvaluateProductWhenProductsLengthIsNegative() {
        given(targetProductDimensions.getLength()).willReturn(Double.valueOf(-.01));
        assertThat(targetMaxVolumeZDMVRestriction.evaluate(product)).isFalse();
    }

    @Test
    public void testEvaluateProductWithVariantsWithValidDimensionsAndVolumeWithinLimits() {
        final Collection<VariantProductModel> variants = new HashSet<>();
        final TargetSizeVariantProductModel firstVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetSizeVariantProductModel secondVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetProductDimensionsModel firstVariantsDimensions = mock(TargetProductDimensionsModel.class);
        final TargetProductDimensionsModel secondDimension = mock(TargetProductDimensionsModel.class);
        given(firstVariantProductModel.getProductPackageDimensions()).willReturn(firstVariantsDimensions);
        given(firstVariantsDimensions.getHeight()).willReturn(Double.valueOf(10.0));
        given(firstVariantsDimensions.getWidth()).willReturn(Double.valueOf(10.0));
        given(firstVariantsDimensions.getLength()).willReturn(Double.valueOf(10.0));

        given(secondVariantProductModel.getProductPackageDimensions()).willReturn(secondDimension);
        given(secondDimension.getHeight()).willReturn(Double.valueOf(10.0));
        given(secondDimension.getWidth()).willReturn(Double.valueOf(10.0));
        given(secondDimension.getLength()).willReturn(Double.valueOf(10.0));

        variants.add(firstVariantProductModel);
        variants.add(secondVariantProductModel);
        given(productModel.getVariants()).willReturn(variants);
        assertThat(targetMaxVolumeZDMVRestriction.evaluate(product)).isTrue();
    }

    @Test
    public void testEvaluateProductWithVariantsWhenOneOfTheVariantHasDimensionsMissing() {
        final Collection<VariantProductModel> variants = new HashSet<>();
        final TargetSizeVariantProductModel firstVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetSizeVariantProductModel secondVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetProductDimensionsModel firstVariantsDimensions = mock(TargetProductDimensionsModel.class);
        given(firstVariantProductModel.getProductPackageDimensions()).willReturn(firstVariantsDimensions);
        given(firstVariantsDimensions.getHeight()).willReturn(Double.valueOf(10.0));
        given(firstVariantsDimensions.getWidth()).willReturn(Double.valueOf(10.0));
        given(firstVariantsDimensions.getLength()).willReturn(Double.valueOf(10.0));

        given(secondVariantProductModel.getProductPackageDimensions()).willReturn(null);
        variants.add(firstVariantProductModel);
        variants.add(secondVariantProductModel);
        given(productModel.getVariants()).willReturn(variants);
        assertThat(targetMaxVolumeZDMVRestriction.evaluate(product)).isFalse();
    }

    @Test
    public void testEvaluateProductWithVariantsWhenOneOfTheVariantHasLengthMissing() {
        final Collection<VariantProductModel> variants = new HashSet<>();
        final TargetSizeVariantProductModel firstVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetSizeVariantProductModel secondVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetProductDimensionsModel firstVariantsDimensions = mock(TargetProductDimensionsModel.class);
        final TargetProductDimensionsModel secondDimension = mock(TargetProductDimensionsModel.class);
        given(firstVariantProductModel.getProductPackageDimensions()).willReturn(firstVariantsDimensions);
        given(firstVariantsDimensions.getHeight()).willReturn(Double.valueOf(10.0));
        given(firstVariantsDimensions.getWidth()).willReturn(Double.valueOf(10.0));
        given(firstVariantsDimensions.getLength()).willReturn(Double.valueOf(10.0));

        given(secondVariantProductModel.getProductPackageDimensions()).willReturn(secondDimension);
        given(secondDimension.getHeight()).willReturn(Double.valueOf(10.0));
        given(secondDimension.getWidth()).willReturn(Double.valueOf(10.0));
        given(secondDimension.getLength()).willReturn(null);

        variants.add(firstVariantProductModel);
        variants.add(secondVariantProductModel);
        given(productModel.getVariants()).willReturn(variants);
        assertThat(targetMaxVolumeZDMVRestriction.evaluate(product)).isFalse();
    }

    @Test
    public void testEvaluateProductWithVariantsWhenOneOfTheVariantHasVolumeOverTheLimit() {
        final Collection<VariantProductModel> variants = new HashSet<>();
        final TargetSizeVariantProductModel firstVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetSizeVariantProductModel secondVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetProductDimensionsModel firstVariantsDimensions = mock(TargetProductDimensionsModel.class);
        final TargetProductDimensionsModel secondDimension = mock(TargetProductDimensionsModel.class);
        given(firstVariantProductModel.getProductPackageDimensions()).willReturn(firstVariantsDimensions);
        given(firstVariantsDimensions.getHeight()).willReturn(Double.valueOf(10.0));
        given(firstVariantsDimensions.getWidth()).willReturn(Double.valueOf(10.0));
        given(firstVariantsDimensions.getLength()).willReturn(Double.valueOf(10.0));

        given(secondVariantProductModel.getProductPackageDimensions()).willReturn(secondDimension);
        given(secondDimension.getHeight()).willReturn(Double.valueOf(10.0));
        given(secondDimension.getWidth()).willReturn(Double.valueOf(100.0));
        given(secondDimension.getLength()).willReturn(Double.valueOf(10.0));

        variants.add(firstVariantProductModel);
        variants.add(secondVariantProductModel);
        given(productModel.getVariants()).willReturn(variants);
        assertThat(targetMaxVolumeZDMVRestriction.evaluate(product)).isFalse();
    }

    @Test
    public void testEvaluateProductWithVariantsWhenOneOfTheVariantHasZeroDimensionsWithValidVolume() {
        final Collection<VariantProductModel> variants = new HashSet<>();
        final TargetSizeVariantProductModel firstVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetSizeVariantProductModel secondVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetProductDimensionsModel firstVariantsDimensions = mock(TargetProductDimensionsModel.class);
        final TargetProductDimensionsModel secondDimension = mock(TargetProductDimensionsModel.class);
        given(firstVariantProductModel.getProductPackageDimensions()).willReturn(firstVariantsDimensions);
        given(firstVariantsDimensions.getHeight()).willReturn(Double.valueOf(10.0));
        given(firstVariantsDimensions.getWidth()).willReturn(Double.valueOf(10.0));
        given(firstVariantsDimensions.getLength()).willReturn(Double.valueOf(10.0));

        given(secondVariantProductModel.getProductPackageDimensions()).willReturn(secondDimension);
        given(secondDimension.getHeight()).willReturn(Double.valueOf(0));
        given(secondDimension.getWidth()).willReturn(Double.valueOf(0));
        given(secondDimension.getLength()).willReturn(Double.valueOf(0));

        variants.add(firstVariantProductModel);
        variants.add(secondVariantProductModel);
        given(productModel.getVariants()).willReturn(variants);
        assertThat(targetMaxVolumeZDMVRestriction.evaluate(product)).isTrue();
    }

    @Test
    public void testEvaluateCartContainingProductsWithValidDimensionsAndWithinLimit() {
        setUpEntries(Double.valueOf(6.0));
        assertThat(targetMaxVolumeZDMVRestriction.evaluate(cart)).isTrue();
    }

    @Test
    public void testEvaluateCartContainingProductsWithValidDimensionsAndOverLimit() {
        setUpEntries(Double.valueOf(6.01));
        assertThat(targetMaxVolumeZDMVRestriction.evaluate(cart)).isFalse();
    }

    @Test
    public void testEvaluateCartContainingProductsWithNegativeDimensions() {
        setUpEntries(Double.valueOf(-10.0));
        assertThat(targetMaxVolumeZDMVRestriction.evaluate(cart)).isFalse();
    }

    @Test
    public void testEvaluateCartContainingProductsWithoutDimensions() {
        setUpEntries(null);
        assertThat(targetMaxVolumeZDMVRestriction.evaluate(cart)).isFalse();
    }

    @Test
    public void testEvaluateCartWithNoEntries() {
        given(cart.getEntries()).willReturn(null);
        assertThat(targetMaxVolumeZDMVRestriction.evaluate(cart)).isFalse();
    }

    @Test
    public void testEvaluateCartWithNullCart() {
        final CartModel testCart = null;
        assertThat(targetMaxVolumeZDMVRestriction.evaluate(testCart)).isFalse();
    }

    /**
     * Method to set up cart entries. This method sets valid dimensions for all the products and 1 dimension value is
     * set as the parameter value passed.
     * 
     * @param value
     *            This dimension length is set to 2nd product's length, if null then that product itself has no
     *            dimension set to it.
     */
    private void setUpEntries(final Double value) {
        final AbstractOrderEntryModel firstEntry = mock(OrderEntryModel.class);
        final AbstractOrderEntryModel secondEntry = mock(OrderEntryModel.class);
        final TargetSizeVariantProductModel firstProduct = mock(TargetSizeVariantProductModel.class);
        final TargetProductDimensionsModel firstDimension = mock(TargetProductDimensionsModel.class);
        final TargetSizeVariantProductModel secondProduct = mock(TargetSizeVariantProductModel.class);
        final TargetProductDimensionsModel secondDimension = mock(TargetProductDimensionsModel.class);
        orderEntries.add(firstEntry);
        orderEntries.add(secondEntry);
        given(cart.getEntries()).willReturn(orderEntries);

        given(modelService.get(any(Product.class))).willReturn(firstProduct, secondProduct);
        given(firstEntry.getQuantity()).willReturn(Long.valueOf(2));
        given(firstProduct.getProductPackageDimensions()).willReturn(firstDimension);
        given(firstDimension.getHeight()).willReturn(Double.valueOf(10));
        given(firstDimension.getWidth()).willReturn(Double.valueOf(10));
        given(firstDimension.getLength()).willReturn(Double.valueOf(2));

        given(secondEntry.getQuantity()).willReturn(Long.valueOf(1));
        if (value != null) {
            given(secondProduct.getProductPackageDimensions()).willReturn(secondDimension);
            given(secondDimension.getHeight()).willReturn(Double.valueOf(10));
            given(secondDimension.getWidth()).willReturn(Double.valueOf(10));
            given(secondDimension.getLength()).willReturn(value);
        }
        given(firstEntry.getProduct()).willReturn(firstProduct);
        given(secondEntry.getProduct()).willReturn(secondProduct);
    }
}
