/**
 * 
 */
package au.com.target.tgtcore.dbformatter.formatter;


import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.dbformatter.TargetDBSpecificQueryFormatter;


/**
 * @author pthoma20
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetHsqlDbQueryFormatterTest {

    @InjectMocks
    private final TargetHsqlDbQueryFormatter targetHsqlDbQueryFormatter = new TargetHsqlDbQueryFormatter();

    @Test
    public void testFormatDateDiffDays() {
        Assert.assertEquals(" DATEDIFF('dd',date1,date2) ",
                targetHsqlDbQueryFormatter.formatDateDiff(TargetDBSpecificQueryFormatter.DAYS, "date1", "date2"));
    }

    @Test
    public void testFormatDateDiffHours() {
        Assert.assertEquals(" DATEDIFF('hh',date1,date2) ",
                targetHsqlDbQueryFormatter.formatDateDiff(TargetDBSpecificQueryFormatter.HOURS, "date1", "date2"));
    }

    @Test
    public void testFormatDateDiffMinutes() {
        Assert.assertEquals(" DATEDIFF('mi',date1,date2) ",
                targetHsqlDbQueryFormatter.formatDateDiff(TargetDBSpecificQueryFormatter.MINUTES, "date1", "date2"));
    }

    @Test
    public void testFormatDateDiffSeconds() {
        Assert.assertEquals(" DATEDIFF('ss',date1,date2) ",
                targetHsqlDbQueryFormatter.formatDateDiff(TargetDBSpecificQueryFormatter.SECONDS, "date1", "date2"));
    }

}
