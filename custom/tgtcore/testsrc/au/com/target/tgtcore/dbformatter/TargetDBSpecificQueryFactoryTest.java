/**
 * 
 */
package au.com.target.tgtcore.dbformatter;


import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.dbformatter.formatter.TargetHsqlDbQueryFormatter;
import au.com.target.tgtcore.dbformatter.formatter.TargetMySqlQueryFormatter;
import au.com.target.tgtcore.dbformatter.formatter.TargetSqlServerQueryFormatter;
import org.junit.Assert;


/**
 * @author pthoma20
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetDBSpecificQueryFactoryTest {

    @InjectMocks
    private final TargetDBSpecificQueryFactory targetDBSpecificQueryFactory = new TargetDBSpecificQueryFactory();

    @Mock
    private TargetHsqlDbQueryFormatter targetHsqlDbQueryFormatter;

    @Mock
    private TargetSqlServerQueryFormatter targetSqlServerQueryFormatter;

    @Mock
    private TargetMySqlQueryFormatter targetMySqlQueryFormatter;

    @Test
    public void testGetDBSpecificQueryFormatter() {

        targetDBSpecificQueryFactory.setTargetHsqlDbQueryFormatter(targetHsqlDbQueryFormatter);
        targetDBSpecificQueryFactory.setTargetMySqlQueryFormatter(targetMySqlQueryFormatter);
        targetDBSpecificQueryFactory.setTargetSqlServerQueryFormatter(targetSqlServerQueryFormatter);


        targetDBSpecificQueryFactory.setDbIdentifier("org.hsqldb.jdbcDriver");
        Assert.assertTrue((targetDBSpecificQueryFactory
                .getDBSpecificQueryFormatter() instanceof TargetHsqlDbQueryFormatter));

        targetDBSpecificQueryFactory.setDbIdentifier("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        Assert.assertTrue(
                targetDBSpecificQueryFactory.getDBSpecificQueryFormatter() instanceof TargetSqlServerQueryFormatter);

        targetDBSpecificQueryFactory.setDbIdentifier("com.mysql.jdbc.Driver");
        Assert.assertTrue(
                targetDBSpecificQueryFactory.getDBSpecificQueryFormatter() instanceof TargetMySqlQueryFormatter);
    }
}
