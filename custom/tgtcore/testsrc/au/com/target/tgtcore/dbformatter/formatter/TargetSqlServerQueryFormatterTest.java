/**
 * 
 */
package au.com.target.tgtcore.dbformatter.formatter;


import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.dbformatter.TargetDBSpecificQueryFormatter;


/**
 * @author pthoma20
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetSqlServerQueryFormatterTest {

    @InjectMocks
    private final TargetSqlServerQueryFormatter targetSqlServerQueryFormatter = new TargetSqlServerQueryFormatter();

    @Test
    public void testFormatDateDiffDays() {
        Assert.assertEquals(" DATEDIFF(day,date1,date2) ",
                targetSqlServerQueryFormatter.formatDateDiff(TargetDBSpecificQueryFormatter.DAYS, "date1", "date2"));
    }

    @Test
    public void testFormatDateDiffHours() {
        Assert.assertEquals(" DATEDIFF(hour,date1,date2) ",
                targetSqlServerQueryFormatter.formatDateDiff(TargetDBSpecificQueryFormatter.HOURS, "date1", "date2"));
    }

    @Test
    public void testFormatDateDiffMinutes() {
        Assert.assertEquals(" DATEDIFF(minute,date1,date2) ",
                targetSqlServerQueryFormatter.formatDateDiff(TargetDBSpecificQueryFormatter.MINUTES, "date1", "date2"));
    }

    @Test
    public void testFormatDateDiffSeconds() {
        Assert.assertEquals(" DATEDIFF(second,date1,date2) ",
                targetSqlServerQueryFormatter.formatDateDiff(TargetDBSpecificQueryFormatter.SECONDS, "date1", "date2"));
    }

}
