/**
 * 
 */
package au.com.target.tgtcore.dbformatter.formatter;


import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.dbformatter.TargetDBSpecificQueryFormatter;


/**
 * @author pthoma20
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetMySqlQueryFormatterTest {

    @InjectMocks
    private final TargetMySqlQueryFormatter targetMySqlQueryFormatter = new TargetMySqlQueryFormatter();

    @Test
    public void testFormatDateDiffDays() {
        Assert.assertEquals(" TIMESTAMPDIFF(DAY,date1,date2) ",
                targetMySqlQueryFormatter.formatDateDiff(TargetDBSpecificQueryFormatter.DAYS, "date1", "date2"));
    }

    @Test
    public void testFormatDateDiffHours() {
        Assert.assertEquals(" TIMESTAMPDIFF(HOUR,date1,date2) ",
                targetMySqlQueryFormatter.formatDateDiff(TargetDBSpecificQueryFormatter.HOURS, "date1", "date2"));
    }

    @Test
    public void testFormatDateDiffMinutes() {
        Assert.assertEquals(" TIMESTAMPDIFF(MINUTE,date1,date2) ",
                targetMySqlQueryFormatter.formatDateDiff(TargetDBSpecificQueryFormatter.MINUTES, "date1", "date2"));
    }

    @Test
    public void testFormatDateDiffSeconds() {
        Assert.assertEquals(" TIMESTAMPDIFF(SECOND,date1,date2) ",
                targetMySqlQueryFormatter.formatDateDiff(TargetDBSpecificQueryFormatter.SECONDS, "date1", "date2"));
    }

}
