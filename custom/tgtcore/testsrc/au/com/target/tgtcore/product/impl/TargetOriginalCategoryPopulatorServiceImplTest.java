package au.com.target.tgtcore.product.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stub;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.category.TargetCategoryService;
import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.dao.TargetProductDao;
import au.com.target.tgtcore.product.exception.ProductOriginalCategoryPopulationException;



@RunWith(MockitoJUnitRunner.class)
public class TargetOriginalCategoryPopulatorServiceImplTest {
    @Mock
    private CatalogVersionService mockCatalogVersionService;

    @Mock
    private TargetCategoryService mockTargetCategoryService;

    @Mock
    private TargetProductDao mockTargetProductDao;

    @Mock
    private TargetSharedConfigService mockTargetSharedConfigService;

    @Mock
    private ModelService mockModelService;

    @InjectMocks
    private final TargetOriginalCategoryPopulatorServiceImpl targetOriginalCategoryPopulatorServiceImpl = new TargetOriginalCategoryPopulatorServiceImpl();

    @Mock
    private CatalogVersionModel mockStagedProductCatalog;

    private Map<String, TargetProductCategoryModel> categoryLookup;

    private final int defaultSubBatchLimit = 100;

    private final int defaultBatchLimit = 10000;

    private final Integer subBatchLimit = Integer.valueOf(1);

    private final Integer batchLimit = Integer.valueOf(1);

    @Before
    public void setup() {
        categoryLookup = new HashMap<>();
        createCategoryStructure();
        given(mockCatalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION)).willReturn(mockStagedProductCatalog);

        given(mockTargetCategoryService.getCategoryForCode(mockStagedProductCatalog,
                TgtCoreConstants.Category.ALL_PRODUCTS))
                        .willReturn(categoryLookup.get(TgtCoreConstants.Category.ALL_PRODUCTS));

        given(mockTargetCategoryService.getCategoryForCode(mockStagedProductCatalog,
                "W676714")).willReturn(categoryLookup.get("W676714"));


        willReturn(batchLimit).given(mockTargetSharedConfigService)
                .getInt("products.originalCategory.populate.batchlimit", defaultBatchLimit);

        willReturn(subBatchLimit).given(mockTargetSharedConfigService)
                .getInt("products.originalCategory.populate.sub.batchlimit", defaultSubBatchLimit);

        willReturn("W676714").given(mockTargetSharedConfigService)
                .getConfigByCode("products.clearanceCategoryCode");

    }


    @Test(expected = ProductOriginalCategoryPopulationException.class)
    public void testPopulateOriginalCategoriesForEmptyClearanceCode() throws Exception {
        willReturn(null).given(mockTargetSharedConfigService)
                .getConfigByCode("products.clearanceCategoryCode");

        targetOriginalCategoryPopulatorServiceImpl.populateOriginalCategories();
    }

    @Test
    public void testPopulateOriginalCategoriesForClearanceProductWithThirdLevelMatch() throws Exception {
        willReturn(Integer.valueOf(5)).given(mockTargetSharedConfigService)
                .getInt("products.originalCategory.populate.batchlimit", defaultBatchLimit);

        final TargetProductModel mockProduct1 = mock(TargetProductModel.class);
        final TargetProductModel mockProduct2 = mock(TargetProductModel.class);
        final TargetProductModel mockProduct3 = mock(TargetProductModel.class);
        final TargetProductModel mockProduct4 = mock(TargetProductModel.class);
        final TargetProductModel mockProduct5 = mock(TargetProductModel.class);

        given(mockProduct1.getPrimarySuperCategory()).willReturn(categoryLookup.get("W678727"));
        given(mockProduct2.getPrimarySuperCategory()).willReturn(categoryLookup.get("W678727"));
        given(mockProduct3.getPrimarySuperCategory()).willReturn(categoryLookup.get("W678727"));
        given(mockProduct4.getPrimarySuperCategory()).willReturn(categoryLookup.get("W678727"));
        given(mockProduct5.getPrimarySuperCategory()).willReturn(categoryLookup.get("W678727"));

        final List<TargetProductModel> products1 = new ArrayList<>(Arrays.asList(mockProduct1));
        final List<TargetProductModel> products2 = new ArrayList<>(Arrays.asList(mockProduct2));
        final List<TargetProductModel> products3 = new ArrayList<>(Arrays.asList(mockProduct3));
        final List<TargetProductModel> products4 = new ArrayList<>(Arrays.asList(mockProduct4));
        final List<TargetProductModel> products5 = new ArrayList<>(Arrays.asList(mockProduct5));

        given(mockTargetProductDao.findProductsByOriginalCategoryNull(eq(mockStagedProductCatalog),
                eq(subBatchLimit.intValue()), eq(Collections.EMPTY_LIST)))
                        .willReturn(products1, products2, products3, products4, products5);

        targetOriginalCategoryPopulatorServiceImpl.populateOriginalCategories();

        verify(mockProduct1).setOriginalCategory(categoryLookup.get("W95136"));
        verify(mockProduct2).setOriginalCategory(categoryLookup.get("W95136"));
        verify(mockProduct3).setOriginalCategory(categoryLookup.get("W95136"));
        verify(mockProduct4).setOriginalCategory(categoryLookup.get("W95136"));
        verify(mockProduct5).setOriginalCategory(categoryLookup.get("W95136"));


        final ArgumentCaptor<List> modelSaveCaptor = ArgumentCaptor.forClass(List.class);
        verify(mockModelService, times(5)).saveAll(modelSaveCaptor.capture());

        final List<List> savedProducts = modelSaveCaptor.getAllValues();
        assertThat(savedProducts.get(0)).containsExactly(mockProduct1);
        assertThat(savedProducts.get(1)).containsExactly(mockProduct2);
        assertThat(savedProducts.get(2)).containsExactly(mockProduct3);
        assertThat(savedProducts.get(3)).containsExactly(mockProduct4);
        assertThat(savedProducts.get(4)).containsExactly(mockProduct5);

    }

    @Test
    public void testPopulateOriginalCategoriesForMultiplePages() throws Exception {
        willReturn(Integer.valueOf(7)).given(mockTargetSharedConfigService)
                .getInt("products.originalCategory.populate.batchlimit", defaultBatchLimit);
        willReturn(Integer.valueOf(2)).given(mockTargetSharedConfigService)
                .getInt("products.originalCategory.populate.sub.batchlimit", defaultSubBatchLimit);

        final TargetProductModel mockProduct1 = mock(TargetProductModel.class);
        final TargetProductModel mockProduct2 = mock(TargetProductModel.class);
        final TargetProductModel mockProduct3 = mock(TargetProductModel.class);
        final TargetProductModel mockProduct4 = mock(TargetProductModel.class);
        final TargetProductModel mockProduct5 = mock(TargetProductModel.class);

        given(mockProduct1.getPrimarySuperCategory()).willReturn(categoryLookup.get("W678727"));
        given(mockProduct2.getPrimarySuperCategory()).willReturn(categoryLookup.get("W678727"));
        given(mockProduct3.getPrimarySuperCategory()).willReturn(categoryLookup.get("W678727"));
        given(mockProduct4.getPrimarySuperCategory()).willReturn(categoryLookup.get("W678727"));
        given(mockProduct5.getPrimarySuperCategory()).willReturn(categoryLookup.get("W678727"));

        final List<TargetProductModel> products1 = new ArrayList<>(Arrays.asList(mockProduct1, mockProduct2));
        final List<TargetProductModel> products2 = new ArrayList<>(Arrays.asList(mockProduct3, mockProduct4));
        final List<TargetProductModel> products3 = new ArrayList<>(Arrays.asList(mockProduct5));


        given(mockTargetProductDao.findProductsByOriginalCategoryNull(eq(mockStagedProductCatalog),
                eq(2), eq(Collections.EMPTY_LIST)))
                        .willReturn(products1, products2, products3);

        targetOriginalCategoryPopulatorServiceImpl.populateOriginalCategories();

        verify(mockProduct1).setOriginalCategory(categoryLookup.get("W95136"));
        verify(mockProduct2).setOriginalCategory(categoryLookup.get("W95136"));
        verify(mockProduct3).setOriginalCategory(categoryLookup.get("W95136"));
        verify(mockProduct4).setOriginalCategory(categoryLookup.get("W95136"));
        verify(mockProduct5).setOriginalCategory(categoryLookup.get("W95136"));


        final ArgumentCaptor<List> modelSaveCaptor = ArgumentCaptor.forClass(List.class);
        verify(mockModelService, times(3)).saveAll(modelSaveCaptor.capture());

        final List<List> savedProducts = modelSaveCaptor.getAllValues();
        assertThat(savedProducts.get(0)).containsExactly(mockProduct1, mockProduct2);
        assertThat(savedProducts.get(1)).containsExactly(mockProduct3, mockProduct4);
        assertThat(savedProducts.get(2)).containsExactly(mockProduct5);

    }

    @Test
    public void testPopulateOriginalCategoriesForClearanceProductWithFirstLevelMatch() throws Exception {
        final TargetProductModel mockProduct = mock(TargetProductModel.class);
        given(mockProduct.getPrimarySuperCategory()).willReturn(categoryLookup.get("W678050"));

        final List<TargetProductModel> products = new ArrayList<>();
        products.add(mockProduct);

        given(mockTargetProductDao.findProductsByOriginalCategoryNull(eq(mockStagedProductCatalog),
                eq(subBatchLimit.intValue()), eq(Collections.EMPTY_LIST)))
                        .willReturn(products);

        targetOriginalCategoryPopulatorServiceImpl.populateOriginalCategories();

        verify(mockProduct, times(1)).setOriginalCategory(categoryLookup.get("W93742"));

        final ArgumentCaptor<List> modelSaveCaptor = ArgumentCaptor.forClass(List.class);
        verify(mockModelService, times(1)).saveAll(modelSaveCaptor.capture());

        final List savedProducts = modelSaveCaptor.getValue();
        assertThat(savedProducts).containsExactly(mockProduct);
    }

    @Test
    public void testPopulateOriginalCategoriesForNormalProduct() throws Exception {
        final TargetProductModel mockProduct = mock(TargetProductModel.class);
        given(mockProduct.getPrimarySuperCategory()).willReturn(categoryLookup.get("W917427"));

        final List<TargetProductModel> products = new ArrayList<>();
        products.add(mockProduct);

        given(mockTargetProductDao.findProductsByOriginalCategoryNull(eq(mockStagedProductCatalog),
                eq(subBatchLimit.intValue()), eq(Collections.EMPTY_LIST)))
                        .willReturn(products);

        targetOriginalCategoryPopulatorServiceImpl.populateOriginalCategories();

        verify(mockProduct).setOriginalCategory(categoryLookup.get("W917427"));

        final ArgumentCaptor<List> modelSaveCaptor = ArgumentCaptor.forClass(List.class);
        verify(mockModelService).saveAll(modelSaveCaptor.capture());

        final List savedProducts = modelSaveCaptor.getValue();
        assertThat(savedProducts).containsExactly(mockProduct);
    }

    @Test
    public void testPopulateOriginalCategoriesForNoProducts() throws Exception {
        final List<TargetProductModel> products = new ArrayList<>();

        given(mockTargetProductDao.findProductsByOriginalCategoryNull(eq(mockStagedProductCatalog),
                eq(subBatchLimit.intValue()), eq(Collections.EMPTY_LIST)))
                        .willReturn(products);

        targetOriginalCategoryPopulatorServiceImpl.populateOriginalCategories();

        verifyZeroInteractions(mockModelService);
    }

    @Test
    public void testPopulateOriginalCategoriesForNoPrimaryCategory() throws Exception {
        final TargetProductModel mockProduct = mock(TargetProductModel.class);
        given(mockProduct.getPrimarySuperCategory()).willReturn(null);

        final List<TargetProductModel> products = new ArrayList<>();
        products.add(mockProduct);

        targetOriginalCategoryPopulatorServiceImpl.populateOriginalCategories();

        verifyZeroInteractions(mockModelService);
    }

    @Test
    public void testPopulateOriginalCategoriesForSuperCategoryThatItCannotfind() throws Exception {
        willReturn(Integer.valueOf(10)).given(mockTargetSharedConfigService)
                .getInt("products.originalCategory.populate.batchlimit", defaultBatchLimit);

        final TargetProductModel mockProduct = mock(TargetProductModel.class);
        given(mockProduct.getCode()).willReturn("P50005");
        given(mockProduct.getPrimarySuperCategory()).willReturn(categoryLookup.get("W545454"));


        final List<TargetProductModel> products = new ArrayList<>();
        products.add(mockProduct);

        given(mockTargetProductDao.findProductsByOriginalCategoryNull(eq(mockStagedProductCatalog),
                eq(subBatchLimit.intValue()), eq(Collections.EMPTY_LIST)))
                        .willReturn(products);

        given(mockTargetProductDao.findProductsByOriginalCategoryNull(eq(mockStagedProductCatalog),
                eq(subBatchLimit.intValue()), eq(Arrays.asList("P50005"))))
                        .willReturn(Collections.EMPTY_LIST);

        targetOriginalCategoryPopulatorServiceImpl.populateOriginalCategories();

        verify(mockTargetProductDao, times(2)).findProductsByOriginalCategoryNull(eq(mockStagedProductCatalog),
                eq(subBatchLimit.intValue()), eq(Arrays.asList("P50005")));

        verifyNoMoreInteractions(mockTargetProductDao);

        verifyZeroInteractions(mockModelService);
    }

    @Test
    public void testPopulateOriginalCategoriesForNoSuperCategory() throws Exception {
        willReturn(Integer.valueOf(1)).given(mockTargetSharedConfigService)
                .getInt("products.originalCategory.populate.batchlimit", defaultBatchLimit);

        final TargetProductModel mockProduct = mock(TargetProductModel.class);
        given(mockProduct.getPrimarySuperCategory()).willReturn(categoryLookup.get("AP01"));

        final List<TargetProductModel> products = new ArrayList<>();
        products.add(mockProduct);

        given(mockTargetProductDao.findProductsByOriginalCategoryNull(eq(mockStagedProductCatalog),
                eq(subBatchLimit.intValue()), eq(Collections.EMPTY_LIST)))
                        .willReturn(products);

        targetOriginalCategoryPopulatorServiceImpl.populateOriginalCategories();


        final ArgumentCaptor<List> modelSaveCaptor = ArgumentCaptor.forClass(List.class);
        verify(mockModelService, times(1)).saveAll(modelSaveCaptor.capture());

        final List savedProducts = modelSaveCaptor.getValue();
        assertThat(savedProducts).containsExactly(mockProduct);
    }


    @Test
    public void testPopulateOriginalCategoriesSaveError() throws Exception {
        final TargetProductModel mockProduct = mock(TargetProductModel.class);
        given(mockProduct.getPrimarySuperCategory()).willReturn(categoryLookup.get("W917427"));

        final List<TargetProductModel> products = new ArrayList<>();
        products.add(mockProduct);

        given(mockTargetProductDao.findProductsByOriginalCategoryNull(eq(mockStagedProductCatalog),
                eq(subBatchLimit.intValue()), eq(Collections.EMPTY_LIST)))
                        .willReturn(products);

        doThrow(new ModelSavingException("testing")).when(mockModelService).saveAll(products);

        targetOriginalCategoryPopulatorServiceImpl.populateOriginalCategories();

        final ArgumentCaptor<List> modelSaveCaptor = ArgumentCaptor.forClass(List.class);
        verify(mockModelService, times(1)).saveAll(modelSaveCaptor.capture());
        final ArgumentCaptor individualModelSaveCaptor = ArgumentCaptor.forClass(TargetProductModel.class);
        verify(mockModelService, times(1)).save(individualModelSaveCaptor.capture());
        assertThat(individualModelSaveCaptor.getValue()).isEqualTo(mockProduct);


    }

    @Test
    public void testPopulateOriginalCategoriesForClearanceProductWithNoMatch() throws Exception {
        final TargetProductModel mockProduct = mock(TargetProductModel.class);
        given(mockProduct.getPrimarySuperCategory()).willReturn(categoryLookup.get("W698034"));

        final List<TargetProductModel> products = new ArrayList<>();
        products.add(mockProduct);

        given(mockTargetProductDao.findProductsByOriginalCategoryNull(eq(mockStagedProductCatalog),
                eq(subBatchLimit.intValue()), eq(Collections.EMPTY_LIST)))
                        .willReturn(products);

        targetOriginalCategoryPopulatorServiceImpl.populateOriginalCategories();

        verifyZeroInteractions(mockModelService);
    }

    private void createCategoryStructure() {
        final List<TestCategory> categories = new ArrayList<>();

        categories.add(new TestCategory("AP01", "All Products", null, "W93743", "W676714", "W93742"));

        categories.add(new TestCategory("W676714", "Clearance", "AP01", "W678024", "W545454"));
        categories.add(new TestCategory("W678024", "Women", "W676714", "W678727"));
        categories.add(new TestCategory("W698034", "Special Women", "W676714", "W678727"));
        categories.add(new TestCategory("W678727", "Tops", "W678024", (String[])null));

        categories.add(new TestCategory("W678022", "Kids", "W676714", "W678050"));
        categories.add(new TestCategory("W678050", "Tops + T-shirts", "W678022", (String[])null));

        categories.add(new TestCategory("W93743", "Women", "AP01", "W95136"));
        categories.add(new TestCategory("W95136", "Tops", "W93743", "W917427"));
        categories.add(new TestCategory("W917427", "Knitwear", "W95136", (String[])null));

        categories.add(new TestCategory("W93742", "Kids", "AP01", "W103459"));
        categories.add(new TestCategory("W103459", "Girls 1-7", "W93742", "W104539"));
        categories.add(new TestCategory("W104539", "Tops + Tees", "W103459", (String[])null));

        categories.add(new TestCategory("W94147", "Boys 1-7", "W93742", "W94171"));
        categories.add(new TestCategory("W94171", "Tops + Tees", "W94147", (String[])null));
        categories.add(new TestCategory("W545454", "Last Chance", "W676714", (String[])null));

        for (final TestCategory testCategory : categories) {
            final TargetProductCategoryModel mockTargetProductCategory = mock(TargetProductCategoryModel.class);
            given(mockTargetProductCategory.getCode()).willReturn(testCategory.getCode());
            given(mockTargetProductCategory.getName()).willReturn(testCategory.getName());

            categoryLookup.put(testCategory.getCode(), mockTargetProductCategory);
        }

        for (final TestCategory testCategory : categories) {
            final TargetProductCategoryModel mockTargetProductCategory = categoryLookup.get(testCategory.getCode());

            final List<CategoryModel> subCategories = new ArrayList<>();
            if (testCategory.getSubCategoryCodes() != null) {
                for (final String subCategoryCode : testCategory.getSubCategoryCodes()) {
                    subCategories.add(categoryLookup.get(subCategoryCode));
                }
            }

            given(mockTargetProductCategory.getCategories()).willReturn(subCategories);

            final List<CategoryModel> superCategories = new ArrayList<>();
            superCategories.add(categoryLookup.get(testCategory.getSuperCategoryCode()));

            given(mockTargetProductCategory.getSupercategories()).willReturn(superCategories);

            final String toString = String.format("code=%s, name=%s", mockTargetProductCategory.getCode(),
                    mockTargetProductCategory.getName());
            stub(mockTargetProductCategory.toString()).toReturn(toString);
        }
    }

    private class TestCategory {
        private final String code;

        private final String name;

        private final String superCategoryCode;

        private final String[] subCategoryCodes;

        /**
         * @param code
         * @param name
         * @param superCategoryCode
         */
        public TestCategory(final String code, final String name, final String superCategoryCode,
                final String... subCategoryCodes) {
            super();
            this.code = code;
            this.name = name;
            this.superCategoryCode = superCategoryCode;
            this.subCategoryCodes = subCategoryCodes;
        }

        /**
         * @return the code
         */
        public String getCode() {
            return code;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @return the superCategoryCode
         */
        public String getSuperCategoryCode() {
            return superCategoryCode;
        }

        /**
         * @return the subCategoryCodes
         */
        public String[] getSubCategoryCodes() {
            return subCategoryCodes;
        }

    }

}