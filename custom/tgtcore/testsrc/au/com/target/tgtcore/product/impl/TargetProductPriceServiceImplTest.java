/**
 * 
 */
package au.com.target.tgtcore.product.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.europe1.enums.ProductTaxGroup;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.util.StandardDateRange;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPriceRowModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.data.TargetPriceRowData;
import au.com.target.tgtcore.tax.TargetTaxGroupService;


@UnitTest
public class TargetProductPriceServiceImplTest {

    @Mock
    private CatalogVersionService catalogVersionService;

    @Mock
    private ProductService productService;

    @Mock
    private UnitService unitService;

    @Mock
    private FlexibleSearchService flexibleSearchService;

    @Mock
    private ModelService modelService;

    @Mock
    private PriceUpdateRequiredStrategy priceUpdateRequiredStrategy;

    @Mock
    private ProductTaxGroupUpdateRequiredStrategy taxGroupUpdateRequiredStrategy;

    @Mock
    private TargetTaxGroupService targetTaxGroupService;


    @InjectMocks
    private final TargetProductPriceServiceImpl targetProductPriceService = new TargetProductPriceServiceImpl();

    @Mock
    private CatalogVersionModel catalog;

    @Mock
    private ProductModel product1;

    @Mock
    private ProductModel product2;

    @Mock
    private ProductTaxGroup ptg;

    @Mock
    private UnitModel unit;

    @Mock
    private CurrencyModel currency;

    private List<CatalogVersionModel> catalogs;


    @SuppressWarnings("boxing")
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        given(catalogVersionService.getCatalogVersion(any(String.class), any(String.class))).willReturn(catalog);
        given(productService.getProductForCode(catalog, "prod1")).willReturn(product1);
        given(productService.getProductForCode(catalog, "prod2")).willReturn(product2);
        given(productService.getProductForCode(catalog, "prodnull")).willReturn(null);
        given(productService.getProductForCode(catalog, "produnknown")).willThrow(
                new UnknownIdentifierException("Test"));
        given(productService.getProductForCode(catalog, "prodambiguous")).willThrow(
                new AmbiguousIdentifierException("Test"));

        given(product1.getApprovalStatus()).willReturn(ArticleApprovalStatus.CHECK);
        given(product2.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);

        given(product1.getEurope1Prices()).willReturn(Collections.EMPTY_SET);

        // Always assume update is required for prices and tax
        given(priceUpdateRequiredStrategy.isPriceUpdateRequired(anyCollectionOf(PriceRowModel.class), anyDouble(),
                anyDouble(), Matchers.anyBoolean(), anyListOf(TargetPriceRowData.class))).willReturn(true);

        given(taxGroupUpdateRequiredStrategy.isTaxGroupUpdateRequired(any(ProductModel.class), anyInt())).willReturn(
                true);

        given(targetTaxGroupService.getProductTaxGroupWithCreate(anyInt(), any(CatalogVersionModel.class))).willReturn(
                ptg);

        given(unitService.getUnitForCode(any(String.class))).willReturn(unit);

        given(flexibleSearchService.getModelsByExample(any(CurrencyModel.class))).willReturn(
                Collections.singletonList(currency));
        given(currency.getIsocode()).willReturn("AUD");

        catalogs = new ArrayList<>();
        catalogs.add(catalog);
        given(modelService.create(TargetPriceRowModel.class)).willReturn(new TargetPriceRowModel(),
                new TargetPriceRowModel());

    }

    @Test
    public void testRemoveProductFromSaleProductNull()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException
    {
        final List<String> products = new ArrayList<>();
        products.add("prodnull");
        targetProductPriceService.removeProductFromSale(products, catalogs);
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testRemoveProductFromSaleProductUnknown()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException
    {
        final List<String> products = new ArrayList<>();
        products.add("produnknown");
        targetProductPriceService.removeProductFromSale(products, catalogs);
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testRemoveProductFromSaleProductAmbiguous()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException
    {
        final List<String> products = new ArrayList<>();
        products.add("prodambiguous");
        targetProductPriceService.removeProductFromSale(products, catalogs);
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testRemoveProductFromSaleProductExistsInCheck()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException
    {
        final List<String> products = new ArrayList<>();
        products.add("prod1");
        targetProductPriceService.removeProductFromSale(products, catalogs);
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testRemoveProductFromSaleProductExistsInApproved()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException
    {
        // The approved product should get updated with status=check (once in each catalog)
        final List<String> products = new ArrayList<>();
        products.add("prod2");
        targetProductPriceService.removeProductFromSale(products, catalogs);
        Mockito.verify(product2, Mockito.times(1)).setApprovalStatus(ArticleApprovalStatus.CHECK);
        Mockito.verify(modelService, Mockito.times(1)).saveAll(Mockito.anyCollection());
    }

    @Test
    public void testUpdatePricesProductNull()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException
    {
        final List<String> products = new ArrayList<>();
        products.add("prodnull");
        targetProductPriceService.updateProductPrices(null);
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testUpdatePricesProductListEmpty()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException
    {
        targetProductPriceService.updateProductPrices(new ArrayList<ProductModel>());
        Mockito.verifyZeroInteractions(modelService);
    }


    @Test
    public void testCreateNewPricesNoFutures()
    {
        final Collection<PriceRowModel> prices = targetProductPriceService.createNewPricesForProduct(product1, catalog,
                1, 2, true, null);
        Assert.assertNotNull(prices);
        Assert.assertEquals(1, prices.size());

        // Should just get one row for the current price, with no range
        // and that it is saved
        final TargetPriceRowModel row1 = (TargetPriceRowModel)prices.iterator().next();
        Assert.assertNotNull(row1);
        Assert.assertEquals(Double.valueOf(1), row1.getPrice());
        Assert.assertEquals(Double.valueOf(2), row1.getWasPrice());
        Assert.assertEquals("AUD", row1.getCurrency().getIsocode());
        Assert.assertEquals(catalog, row1.getCatalogVersion());
        Assert.assertEquals(product1, row1.getProduct());
        Assert.assertEquals(unit, row1.getUnit());
        Assert.assertFalse(row1.getNet().booleanValue());
        Assert.assertNull(row1.getDateRange());
        Assert.assertTrue(row1.getPromoEvent().booleanValue());
        Mockito.verify(modelService).save(any(TargetPriceRowModel.class));
    }

    @Test
    public void testCreateNewPricesNoFuturesWasSameAsSell()
    {
        final Collection<PriceRowModel> prices = targetProductPriceService.createNewPricesForProduct(product1, catalog,
                2, 2, false, null);
        Assert.assertNotNull(prices);
        Assert.assertEquals(1, prices.size());

        // Should just get one row for the current price, with no range
        // and that it is saved
        final TargetPriceRowModel row1 = (TargetPriceRowModel)prices.iterator().next();
        Assert.assertNotNull(row1);
        Assert.assertEquals(Double.valueOf(2), row1.getPrice());
        Assert.assertNull(row1.getWasPrice());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCreateNewPricesWithFuture()
    {
        final StandardDateRange dateRange = mock(StandardDateRange.class);
        final TargetPriceRowData future = new TargetPriceRowData();
        future.setSellPrice(3);
        future.setWasPrice(4);
        future.setDateRange(dateRange);
        future.setPromoEvent(true);

        final TargetPriceRowModel mockRow1 = mock(TargetPriceRowModel.class);
        final TargetPriceRowModel mockRow2 = mock(TargetPriceRowModel.class);
        given(modelService.create(TargetPriceRowModel.class)).willReturn(mockRow1, mockRow2);
        final Collection<PriceRowModel> prices = targetProductPriceService.createNewPricesForProduct(product1, catalog,
                1, 2, true, Collections.singletonList(future));
        Assert.assertNotNull(prices);
        Assert.assertEquals(2, prices.size());

        // Should just get one row for the current price, with no range
        // and that it is saved
        verify(mockRow1).setPrice(Double.valueOf(1));
        verify(mockRow1).setWasPrice(Double.valueOf(2));
        verify(mockRow1).setPromoEvent(true);
        verify(mockRow1, never()).setDateRange(dateRange);

        // Check the second row is the future price
        verify(mockRow2).setPrice(Double.valueOf(3));
        verify(mockRow2).setWasPrice(Double.valueOf(4));
        verify(mockRow2).setPromoEvent(true);
        verify(mockRow2).setDateRange(dateRange);

        // Both rows saved
        Mockito.verify(modelService, Mockito.times(2)).save(any(TargetPriceRowModel.class));
    }

    @Test
    public void testGetCurrentTargetPriceRowModelFromStaged()
    {
        final TargetPriceRowModel expect = new TargetPriceRowModel();
        final List<PriceRowModel> priceList = new ArrayList<>();
        priceList.add(expect);
        given(product1.getEurope1Prices()).willReturn(priceList);
        final TargetPriceRowModel result = targetProductPriceService.getCurrentTargetPriceRowModelFromStaged("prod1");
        Assert.assertEquals(expect, result);

    }

    @Test
    public void testGetProductModelForCatalogsWhenProductDoesHaveVariantApprovedProd()
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final TargetProductPriceServiceImpl newTargetProductPriceService = Mockito.spy(targetProductPriceService);

        final CatalogVersionModel catalogVersion = new CatalogVersionModel();
        final ProductModel productModel = new ProductModel();
        final VariantProductModel variants = new VariantProductModel();
        final PriceRowModel priceRowModel = new PriceRowModel();
        final List<PriceRowModel> currentPrice = Arrays.asList(priceRowModel);
        productModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
        productModel.setVariants(Arrays.asList(variants));
        productModel.setEurope1Prices(currentPrice);

        final StandardDateRange dateRange = setUpStandardDateRange();

        final List<TargetPriceRowData> list = setUpTargetPriceData(dateRange);

        Mockito.when(productService.getProductForCode(catalogVersion, "CP7001")).thenReturn(productModel);
        Mockito.when(Boolean.valueOf(priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrice, 1, 2, false,
                null))).thenReturn(Boolean.TRUE);
        Mockito.when(Boolean.valueOf(taxGroupUpdateRequiredStrategy.isTaxGroupUpdateRequired(productModel, 10)))
                .thenReturn(Boolean.TRUE);

        final List<ProductModel> productsToUpdate = newTargetProductPriceService.getProductModelForCatalogs(
                "CP7001", Arrays.asList(catalogVersion), 5, 5, list, 10, false);
        Assert.assertEquals(0, productsToUpdate.size());
    }

    @Test
    public void testGetProductModelForCatalogsWhenProductDoesHaveVariantUnapprovedProd()
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final TargetProductPriceServiceImpl newTargetProductPriceService = Mockito.spy(targetProductPriceService);

        final CatalogVersionModel catalogVersion = new CatalogVersionModel();
        final ProductModel productModel = new ProductModel();
        final PriceRowModel priceRowModel = new PriceRowModel();
        final List<PriceRowModel> currentPrice = Arrays.asList(priceRowModel);
        productModel.setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);
        productModel.setEurope1Prices(currentPrice);

        final StandardDateRange dateRange = setUpStandardDateRange();

        final List<TargetPriceRowData> list = setUpTargetPriceData(dateRange);

        Mockito.when(productService.getProductForCode(catalogVersion, "CP7001")).thenReturn(productModel);
        Mockito.when(Boolean.valueOf(priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrice, 1, 2, false,
                null))).thenReturn(Boolean.TRUE);
        Mockito.when(Boolean.valueOf(taxGroupUpdateRequiredStrategy.isTaxGroupUpdateRequired(productModel, 10)))
                .thenReturn(Boolean.TRUE);

        final List<ProductModel> productsToUpdate = newTargetProductPriceService.getProductModelForCatalogs(
                "CP7001", Arrays.asList(catalogVersion), 5, 5, list, 10, false);
        Assert.assertEquals(1, productsToUpdate.size());
    }

    @Test
    public void testGetProductModelForCatalogsWhenProductDoesnotHaveVariant() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final TargetProductPriceServiceImpl newTargetProductPriceService = Mockito.spy(targetProductPriceService);

        final CatalogVersionModel catalogVersion = new CatalogVersionModel();
        final ProductModel productModel = new ProductModel();
        final PriceRowModel priceRowModel = new PriceRowModel();
        final List<PriceRowModel> currentPrice = Arrays.asList(priceRowModel);
        productModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
        productModel.setEurope1Prices(currentPrice);

        final StandardDateRange dateRange = setUpStandardDateRange();

        final List<TargetPriceRowData> list = setUpTargetPriceData(dateRange);

        Mockito.when(productService.getProductForCode(catalogVersion, "CP7001")).thenReturn(productModel);
        Mockito.when(Boolean.valueOf(priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrice, 1, 2, false,
                null))).thenReturn(Boolean.TRUE);
        Mockito.when(Boolean.valueOf(taxGroupUpdateRequiredStrategy.isTaxGroupUpdateRequired(productModel, 10)))
                .thenReturn(Boolean.TRUE);

        final List<ProductModel> productsToUpdate = newTargetProductPriceService.getProductModelForCatalogs(
                "CP7001", Arrays.asList(catalogVersion), 5, 5, list, 10, false);
        Assert.assertEquals(1, productsToUpdate.size());
    }

    @Test
    public void testGetProductModelForCatalogsWhenBaseProductPriceUpdate() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final TargetProductPriceServiceImpl newTargetProductPriceService = Mockito.spy(targetProductPriceService);

        final CatalogVersionModel catalogVersion = new CatalogVersionModel();
        final TargetProductModel productModel = new TargetProductModel();
        final PriceRowModel priceRowModel = new PriceRowModel();
        final List<PriceRowModel> currentPrice = Arrays.asList(priceRowModel);
        productModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
        productModel.setEurope1Prices(currentPrice);

        final StandardDateRange dateRange = setUpStandardDateRange();

        final List<TargetPriceRowData> list = setUpTargetPriceData(dateRange);

        Mockito.when(productService.getProductForCode(catalogVersion, "CP7001")).thenReturn(productModel);
        Mockito.when(Boolean.valueOf(priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrice, 1, 2, false,
                null))).thenReturn(Boolean.TRUE);
        Mockito.when(Boolean.valueOf(taxGroupUpdateRequiredStrategy.isTaxGroupUpdateRequired(productModel, 10)))
                .thenReturn(Boolean.TRUE);

        final List<ProductModel> productsToUpdate = newTargetProductPriceService.getProductModelForCatalogs(
                "CP7001", Arrays.asList(catalogVersion), 5, 5, list, 10, false);
        Assert.assertEquals(0, productsToUpdate.size());
    }

    /**
     * @return StandardDateRange
     */
    private StandardDateRange setUpStandardDateRange() {
        final Calendar cal = Calendar.getInstance();
        final Date now = cal.getTime();
        cal.add(Calendar.WEEK_OF_YEAR, 1);
        final Date nextWeek = cal.getTime();

        final StandardDateRange dateRange = new StandardDateRange(now, nextWeek);
        return dateRange;
    }

    /**
     * @param dateRange
     * @return List<TargetPriceRowData>
     */
    public List<TargetPriceRowData> setUpTargetPriceData(final StandardDateRange dateRange) {
        final TargetPriceRowData row1 = new TargetPriceRowData();
        row1.setSellPrice(4);
        row1.setWasPrice(4.5);
        row1.setDateRange(dateRange);
        final TargetPriceRowData row2 = new TargetPriceRowData();
        row2.setSellPrice(3);
        row2.setWasPrice(3.5);
        row2.setDateRange(dateRange);
        final List<TargetPriceRowData> list = new ArrayList<>();
        list.add(row1);
        list.add(row2);
        return list;
    }
}
