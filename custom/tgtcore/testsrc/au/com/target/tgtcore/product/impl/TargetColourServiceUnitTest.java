/**
 * 
 */
package au.com.target.tgtcore.product.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.ColourModel;
import au.com.target.tgtcore.product.dao.ColourDao;


@UnitTest
public class TargetColourServiceUnitTest {
    @Mock
    private ColourDao targetColourDao;

    @Mock
    private ModelService modelService;

    private TargetColourServiceImpl targetColourService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        targetColourService = new TargetColourServiceImpl();
        targetColourService.setColourDao(targetColourDao);
        targetColourService.setModelService(modelService);
        final ColourModel colourModel = new ColourModel();
        BDDMockito.given(modelService.create(ColourModel.class)).willReturn(colourModel);
    }

    @Test
    public void testGetTargetColourByName() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final String colourName = "red";
        final ColourModel red = new ColourModel();
        red.setName(colourName);
        BDDMockito.given(targetColourDao.getColourByCode(colourName)).willReturn(red);
        final ColourModel returnModel = targetColourService.getColourForFuzzyName(colourName, false, true);

        Assert.assertEquals(colourName, returnModel.getName());
    }

    @Test
    public void testCreateTargetColourByName() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final String colourName = "red";
        final ColourModel red = new ColourModel();
        red.setName(colourName);

        BDDMockito.given(targetColourDao.getColourByCode(colourName)).willThrow(
                new TargetUnknownIdentifierException("Can't find target colour model"));
        BDDMockito.given(modelService.create(ColourModel.class)).willReturn(red);
        final ColourModel returnModel = targetColourService.getColourForFuzzyName(colourName, true, true);
        Assert.assertEquals(colourName, returnModel.getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetTargetColourByNameUnknownException() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        targetColourService.getColourForFuzzyName(null, false, true);
    }

    @Test
    public void testCodeRequiresChange() throws Exception {
        final ColourModel colour = targetColourService.createColour("BLUE", true);
        Assert.assertNotNull(colour);
        Assert.assertEquals("BLUE", colour.getName());
        Assert.assertEquals("blue", colour.getCode());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateColourNull() throws Exception {
        targetColourService.getColourForFuzzyName(null, true, true);
    }

    @Test
    public void testCreateColourWithSpace() throws Exception {
        final ColourModel colour = targetColourService.createColour(" Dark Blue ", true);
        Assert.assertNotNull(colour);
        Assert.assertEquals(" Dark Blue ", colour.getName());
        Assert.assertEquals("darkblue", colour.getCode());
    }

    @Test
    public void testCreateColourFancyChars() throws Exception {
        final ColourModel colour = targetColourService.createColour("'Blu�'-123", true);
        Assert.assertNotNull(colour);
        Assert.assertEquals("'Blu�'-123", colour.getName());
        Assert.assertEquals("blu", colour.getCode());
    }

    @Test
    public void testCreateColourSpaceAtStartAfterFancyChar() throws Exception {
        final ColourModel colour = targetColourService.createColour(" ' Blue - ", true);
        Assert.assertNotNull(colour);
        Assert.assertEquals(" ' Blue - ", colour.getName());
        Assert.assertEquals("blue", colour.getCode());
    }
}
