/**
 * 
 */
package au.com.target.tgtcore.featureswitch.service.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.consignextractrecord.dao.ConsignmentExtractRecordDao;
import au.com.target.tgtcore.featureswitch.dao.TargetFeatureSwitchDao;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetFeatureSwitchModel;
import org.junit.Assert;


/**
 * @author pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFeatureSwitchServiceImplTest {

    @InjectMocks
    private final TargetFeatureSwitchServiceImpl targetFeatureSwitchService = new TargetFeatureSwitchServiceImpl();

    @Mock
    private TargetFeatureSwitchDao targetFeatureSwitchDao;

    @Mock
    private TargetFeatureSwitchModel featureSwitch;

    @Mock
    private ModelService modelService;

    @Mock
    private ConsignmentExtractRecordDao consignmentExtractRecordDao;

    @Test
    public void testWhenFeatureIsEnabled() {
        when(targetFeatureSwitchDao.getFeatureByName("PAYPAL")).thenReturn(featureSwitch);
        when(featureSwitch.getEnabled()).thenReturn(Boolean.FALSE);
        Assert.assertFalse(targetFeatureSwitchService.isFeatureEnabled("PAYPAL"));
    }

    @Test
    public void testWhenFeatureSwitchDoesntExist() {
        when(targetFeatureSwitchDao.getFeatureByName("TNS")).thenReturn(null);
        Assert.assertFalse(targetFeatureSwitchService.isFeatureEnabled("TNS"));
    }

    @Test
    public void testGetAllEnabledFeatures() {
        targetFeatureSwitchService.getEnabledFeatures(false);
        verify(targetFeatureSwitchDao).getEnabledFeatures(false);
    }

    /**
     * Method to test UI enabled features.
     */
    @Test
    public void testGetEnabledFeaturesForUI() {
        targetFeatureSwitchService.getEnabledFeatures(true);
        verify(targetFeatureSwitchDao).getEnabledFeatures(true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSaveConsignmentExtractRecordWhenConsignmentIsNull() {
        targetFeatureSwitchService.saveConsignmentExtractRecord(null, false);
    }

    @Test
    public void testSaveConsignmentExtractRecordWhenConsignmentIsNotNull() {
        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        targetFeatureSwitchService.saveConsignmentExtractRecord(consignment, false);

        verify(consignmentExtractRecordDao).saveConsignmentExtractRecord(consignment, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetConsExtractRecordByConsignmentWhenConsignmentIsNull() {
        targetFeatureSwitchService.getConsExtractRecordByConsignment(null);
    }

    @Test
    public void testGetConsExtractRecordByConsignmentWhenConsignmentIsNotNull() {
        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        targetFeatureSwitchService.getConsExtractRecordByConsignment(consignment);

        verify(consignmentExtractRecordDao).getConsExtractRecordByConsignment(consignment);
    }

}
