/**
 * 
 */
package au.com.target.tgtcore.flybuys.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.flybuys.client.FlybuysClient;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysAuthenticateResponseDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysConsumeResponseDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysRedeemTierDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysRefundResponseDto;
import au.com.target.tgtcore.flybuys.service.FlybuysRedeemConfigService;
import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.model.FlybuysRedeemConfigModel;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;


/**
 * Unit test for ({@link FlybuysDiscountServiceImpl}
 * 
 * @author Rahul
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FlybuysDiscountServiceImplTest {

    private static final String STORE_NUMBER = "5599";
    private static final String TRANSACTION_ID_PREFIX = "trans-id-";

    @InjectMocks
    private final FlybuysDiscountServiceImpl flybuysDiscountService = new FlybuysDiscountServiceImpl() {

        @Override
        protected String getTransactionIdTimestamp(final String orderNumber, final DateTime now) {
            return TRANSACTION_ID_PREFIX + orderNumber;
        }

        @Override
        protected String getTimestamp(final DateTime now) {
            return TRANSACTION_ID_PREFIX;
        }
    };

    @Mock
    private FlybuysDiscountServiceImpl mockFlybuysDiscountService;

    @Mock
    private FlybuysDiscountModel flybuysDiscountModel;

    @Mock
    private FlybuysRedeemConfigModel flybuysRedeemConfigModel;

    @Mock
    private FlybuysClient flybuysClient;

    @Mock
    private FlybuysRedeemConfigService flybuysRedeemConfigService;

    @Mock
    private FlybuysAuthenticateResponseDto fbAuthenticateResponseDto;

    @Mock
    private FlybuysConsumeResponseDto fbConsumeResponseDto;

    @Mock
    private FlybuysRefundResponseDto flybuysRefundResponseDto;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetVoucherService voucherService;

    @Mock
    private OrderModel orderModel;

    @Mock
    private OrderModificationRecordEntryModel orderModRecEntryModel;

    private final Collection<DiscountModel> appliedVouchers = new ArrayList<>();

    @Before
    public void setup() {
        flybuysDiscountService.setStoreNumber(STORE_NUMBER);
        given(flybuysRedeemConfigService.getFlybuysRedeemConfig()).willReturn(flybuysRedeemConfigModel);
        given(mockFlybuysDiscountService.refundFlybuysPoints(orderModel)).willCallRealMethod();
    }

    @Test
    public void testAuthenticateFlybuysWithoutConfigModel() {
        final String flybuy = "82972778169675";
        final Date dob = new Date();
        final String postCode = "3220";
        when(flybuysClient.flybuysAuthenticate(flybuy, dob, postCode)).thenReturn(fbAuthenticateResponseDto);
        given(flybuysRedeemConfigService.getFlybuysRedeemConfig()).willReturn(null);
        final FlybuysAuthenticateResponseDto responseFlybuysAuthenticateResponseDto = flybuysDiscountService
                .authenticateFlybuys(flybuy, dob, postCode);
        assertThat(responseFlybuysAuthenticateResponseDto).isNotNull();
        assertThat(responseFlybuysAuthenticateResponseDto).isSameAs(fbAuthenticateResponseDto);
    }

    @Test
    public void testAuthenticateFlybuysWithoutTiers() {
        final String flybuy = "82972778169675";
        final Date dob = new Date();
        final String postCode = "3220";
        when(flybuysClient.flybuysAuthenticate(flybuy, dob, postCode)).thenReturn(fbAuthenticateResponseDto);
        when(fbAuthenticateResponseDto.getRedeemTiers()).thenReturn(null);
        final FlybuysAuthenticateResponseDto responseFlybuysAuthenticateResponseDto = flybuysDiscountService
                .authenticateFlybuys(flybuy, dob, postCode);
        assertThat(responseFlybuysAuthenticateResponseDto).isNotNull();
        assertThat(responseFlybuysAuthenticateResponseDto).isSameAs(fbAuthenticateResponseDto);
    }

    @Test
    public void testAuthenticateFlybuys() {
        final String flybuy = "82972778169675";
        final Date dob = new Date();
        final String postCode = "3220";
        when(flybuysClient.flybuysAuthenticate(flybuy, dob, postCode)).thenReturn(fbAuthenticateResponseDto);
        final FlybuysAuthenticateResponseDto responseFlybuysAuthenticateResponseDto = flybuysDiscountService
                .authenticateFlybuys(flybuy, dob, postCode);
        assertThat(responseFlybuysAuthenticateResponseDto).isNotNull();
        assertThat(responseFlybuysAuthenticateResponseDto).isSameAs(fbAuthenticateResponseDto);
    }

    @Test
    public void testAuthenticateFlybuysNullDate() {
        final String flybuy = "82972368169675";
        final Date dob = null;
        final String postCode = "3000";
        when(flybuysClient.flybuysAuthenticate(flybuy, dob, postCode)).thenReturn(fbAuthenticateResponseDto);
        final FlybuysAuthenticateResponseDto responseFlybuysAuthenticateResponseDto = flybuysDiscountService
                .authenticateFlybuys(flybuy, dob, postCode);
        assertThat(responseFlybuysAuthenticateResponseDto).isNull();
    }

    @Test
    public void testAuthenticateFlybuysNullFlybuysNumber() {
        final String flybuy = "";
        final Date dob = new Date();
        final String postCode = "3008";
        when(flybuysClient.flybuysAuthenticate(flybuy, dob, postCode)).thenReturn(fbAuthenticateResponseDto);
        final FlybuysAuthenticateResponseDto responseFlybuysAuthenticateResponseDto = flybuysDiscountService
                .authenticateFlybuys(flybuy, dob, postCode);
        assertThat(responseFlybuysAuthenticateResponseDto).isNull();
    }

    @Test
    public void testAuthenticateFlybuysNullPostcode() {
        final String flybuy = "82972368169";
        final Date dob = new Date();
        final String postCode = "";
        when(flybuysClient.flybuysAuthenticate(flybuy, dob, postCode)).thenReturn(fbAuthenticateResponseDto);
        final FlybuysAuthenticateResponseDto responseFlybuysAuthenticateResponseDto = flybuysDiscountService
                .authenticateFlybuys(flybuy, dob, postCode);
        assertThat(responseFlybuysAuthenticateResponseDto).isNull();
    }

    @Test
    public void testAuthenticateFlybuysFilteredByConfig() {
        final String flybuy = "82972778169675";
        final Date dob = new Date();
        final String postCode = "3220";

        final FlybuysRedeemTierDto flybuysRedeemTier1 = new FlybuysRedeemTierDto();
        flybuysRedeemTier1.setDollarAmt(BigDecimal.valueOf(10));
        final FlybuysRedeemTierDto flybuysRedeemTier2 = new FlybuysRedeemTierDto();
        flybuysRedeemTier2.setDollarAmt(BigDecimal.valueOf(20));
        final FlybuysRedeemTierDto flybuysRedeemTier3 = new FlybuysRedeemTierDto();
        flybuysRedeemTier3.setDollarAmt(BigDecimal.valueOf(30));

        final List<FlybuysRedeemTierDto> tiersAvailableDto = new ArrayList<>();
        tiersAvailableDto.add(flybuysRedeemTier1);
        tiersAvailableDto.add(flybuysRedeemTier2);
        tiersAvailableDto.add(flybuysRedeemTier3);

        final FlybuysAuthenticateResponseDto flybuysAuthenticateResponseDto = new FlybuysAuthenticateResponseDto();
        flybuysAuthenticateResponseDto.setRedeemTiers(tiersAvailableDto);

        when(flybuysClient.flybuysAuthenticate(flybuy, dob, postCode)).thenReturn(flybuysAuthenticateResponseDto);
        when(flybuysRedeemConfigModel.getMinRedeemable()).thenReturn(Double.valueOf(20d));
        when(flybuysRedeemConfigModel.getMaxRedeemable()).thenReturn(Double.valueOf(50d));
        final FlybuysAuthenticateResponseDto responseFlybuysAuthenticateResponseDto = flybuysDiscountService
                .authenticateFlybuys(flybuy, dob, postCode);
        assertThat(responseFlybuysAuthenticateResponseDto).isNotNull();
        assertThat(responseFlybuysAuthenticateResponseDto.getRedeemTiers().size()).isEqualTo(2);
    }

    @Test
    public void testConsumeFlybuysPointsWithFlybuysNull() {
        assertThat(flybuysDiscountService
                .consumeFlybuysPoints(orderModel)).isNull();
    }

    @Test
    public void testConsumeFlybuysPoints() {
        final String flybuysCardNumber = "82972778169675";
        final String securityToken = "1234";
        final Integer points = Integer.valueOf(1000);
        final String redeemCode = "4673";
        final BigDecimal dollars = new BigDecimal("100");
        final String orderNumber = "12345678";
        given(voucherService.getFlybuysDiscountForOrder(orderModel)).willReturn(flybuysDiscountModel);
        given(flybuysDiscountModel.getCode()).willReturn(redeemCode);
        given(flybuysDiscountModel.getSecurityToken()).willReturn(securityToken);
        given(flybuysDiscountModel.getFlybuysCardNumber()).willReturn(flybuysCardNumber);
        given(flybuysDiscountModel.getValue()).willReturn(Double.valueOf(100d));
        given(flybuysDiscountModel.getRedeemCode()).willReturn(redeemCode);
        given(fbConsumeResponseDto.getConfimationCode()).willReturn("ConfCode");
        given(orderModel.getCode()).willReturn(orderNumber);
        given(orderModel.getFlybuysPointsConsumed()).willReturn(points);
        when(
                flybuysClient.consumeFlybuysPoints(eq(flybuysCardNumber), eq(securityToken), eq(STORE_NUMBER),
                        eq(points),
                        any(BigDecimal.class), eq(redeemCode), eq(TRANSACTION_ID_PREFIX + orderNumber)))
                .thenReturn(
                        fbConsumeResponseDto);

        final FlybuysConsumeResponseDto consumeResponse = flybuysDiscountService
                .consumeFlybuysPoints(orderModel);

        assertThat(consumeResponse).isNotNull();
        assertThat(consumeResponse).isSameAs(fbConsumeResponseDto);
        verify(flybuysDiscountModel).setConfirmationCode("ConfCode");
        verify(modelService).save(flybuysDiscountModel);

        final ArgumentCaptor<BigDecimal> dollarsCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        verify(flybuysClient).consumeFlybuysPoints(eq(flybuysCardNumber), eq(securityToken), eq(STORE_NUMBER),
                eq(points),
                dollarsCaptor.capture(),
                eq(redeemCode), eq(TRANSACTION_ID_PREFIX + orderNumber));

        assertThat(dollarsCaptor.getValue()).isEqualByComparingTo(dollars);
    }

    @Test
    public void testConsumeFlybuysPointsResponseDto() {
        final String flybuysCardNumber = "82972778169675";
        final String storeNumber = "5599";
        final String securityToken = "1234";
        final Integer points = Integer.valueOf(1000);
        final String redeemCode = "4673";
        final BigDecimal dollars = new BigDecimal("100");
        final String orderNumber = "12345678";
        given(voucherService.getFlybuysDiscountForOrder(orderModel)).willReturn(flybuysDiscountModel);
        given(flybuysDiscountModel.getCode()).willReturn(redeemCode);
        given(flybuysDiscountModel.getSecurityToken()).willReturn(securityToken);
        given(flybuysDiscountModel.getFlybuysCardNumber()).willReturn(storeNumber);
        given(flybuysDiscountModel.getValue()).willReturn(Double.valueOf(100d));
        given(fbConsumeResponseDto.getConfimationCode()).willReturn("ConfCode");
        when(flybuysClient.consumeFlybuysPoints(flybuysCardNumber, securityToken, storeNumber, points, dollars,
                redeemCode, TRANSACTION_ID_PREFIX + orderNumber)).thenReturn(
                null);

        final FlybuysConsumeResponseDto consumeResponse = flybuysDiscountService
                .consumeFlybuysPoints(orderModel);

        assertThat(consumeResponse).isNull();
        verify(flybuysDiscountModel, Mockito.never()).setConfirmationCode("ConfCode");

        verify(modelService, Mockito.never()).save(flybuysDiscountModel);
    }

    @Test
    public void testConsumeFlybuysPointsNullStoreNumber() {
        final String flybuysCardNumber = "82972778169675";
        final String storeNumber = "";
        final String securityToken = "1234";
        final Integer points = Integer.valueOf(1000);
        final String redeemCode = "4673";
        final BigDecimal dollars = new BigDecimal("2000");
        final String orderNumber = "12345678";
        given(voucherService.getFlybuysDiscountForOrder(orderModel)).willReturn(flybuysDiscountModel);
        when(flybuysClient.consumeFlybuysPoints(flybuysCardNumber, securityToken, storeNumber, points, dollars,
                redeemCode, TRANSACTION_ID_PREFIX + orderNumber)).thenReturn(
                fbConsumeResponseDto);
        final FlybuysConsumeResponseDto consumeResponse = flybuysDiscountService
                .consumeFlybuysPoints(orderModel);
        assertThat(consumeResponse).isNull();
    }

    @Test
    public void testConsumeFlybuysPointsNullSecurityToken() {
        final String flybuysCardNumber = "82972778169675";
        final String storeNumber = "5599";
        final String securityToken = "";
        final Integer points = Integer.valueOf(1000);
        final String redeemCode = "4673";
        final BigDecimal dollars = new BigDecimal("1000");
        final String orderNumber = "12345678";
        given(voucherService.getFlybuysDiscountForOrder(orderModel)).willReturn(flybuysDiscountModel);
        when(flybuysClient.consumeFlybuysPoints(flybuysCardNumber, securityToken, storeNumber, points, dollars,
                redeemCode, TRANSACTION_ID_PREFIX + orderNumber)).thenReturn(
                fbConsumeResponseDto);
        final FlybuysConsumeResponseDto consumeResponse = flybuysDiscountService
                .consumeFlybuysPoints(orderModel);
        assertThat(consumeResponse).isNull();
    }

    @Test
    public void testConsumeFlybuysPointsNullRedeemCode() {
        final String flybuysCardNumber = "82972778169675";
        final String storeNumber = "5599";
        final String securityToken = "2345";
        final Integer points = Integer.valueOf(1000);
        final String redeemCode = "";
        final BigDecimal dollars = new BigDecimal("1000");
        final String orderNumber = "12345678";
        given(voucherService.getFlybuysDiscountForOrder(orderModel)).willReturn(flybuysDiscountModel);
        when(flybuysClient.consumeFlybuysPoints(flybuysCardNumber, securityToken, storeNumber, points, dollars,
                redeemCode, TRANSACTION_ID_PREFIX + orderNumber)).thenReturn(
                fbConsumeResponseDto);
        final FlybuysConsumeResponseDto consumeResponse = flybuysDiscountService
                .consumeFlybuysPoints(orderModel);
        assertThat(consumeResponse).isNull();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRefundFlybuysPointsWithOrderNull() {
        flybuysDiscountService.refundFlybuysPoints(null);
    }

    @Test
    public void testRefundFlybuysPointsWithOrderCancelRecordNull() {
        given(orderModel.getModificationRecords()).willReturn(null);
        assertThat(flybuysDiscountService.refundFlybuysPoints(orderModel)).isNull();
    }

    @Test
    public void testRefundFlybuysPointsWithFlybuysDiscountNull() {
        given(orderModel.getModificationRecords()).willReturn(
                getOrderModificationRecordModelList());
        given(voucherService.getFlybuysDiscountForOrder(orderModel)).willReturn(null);
        assertThat(flybuysDiscountService.refundFlybuysPoints(orderModel)).isNull();
    }

    @Test
    public void testRefundFlybuysPointsWithSecurityTokenNull() {
        appliedVouchers.add(flybuysDiscountModel);
        given(orderModel.getModificationRecords()).willReturn(
                getOrderModificationRecordModelList());
        given(voucherService.getFlybuysDiscountForOrder(orderModel)).willReturn(flybuysDiscountModel);
        assertThat(flybuysDiscountService.refundFlybuysPoints(orderModel)).isNull();
    }

    @Test
    public void testRefundFlybuysPointsWithStoreNumberNull() {
        appliedVouchers.add(flybuysDiscountModel);
        given(orderModel.getModificationRecords()).willReturn(
                getOrderModificationRecordModelList());
        given(voucherService.getFlybuysDiscountForOrder(orderModel)).willReturn(flybuysDiscountModel);
        given(flybuysDiscountModel.getSecurityToken()).willReturn("10j39s34psk9384fjmcks");
        flybuysDiscountService.setStoreNumber(null);

        assertThat(flybuysDiscountService.refundFlybuysPoints(orderModel)).isNull();
    }

    @Test
    public void testRefundFlybuysPointsWithFlybuysRefundAmountNull() {
        given(voucherService.getFlybuysDiscountForOrder(orderModel)).willReturn(flybuysDiscountModel);
        given(flybuysDiscountModel.getSecurityToken()).willReturn("10j39s34psk9384fjmcks");
        given(orderModel.getModificationRecords()).willReturn(
                getOrderModificationRecordModelListWithFlybuysRefundAmount(null));

        assertThat(flybuysDiscountService.refundFlybuysPoints(orderModel)).isNull();
    }

    @Test
    public void testRefundFlybuysPointsWithFlybuysRefundAmountZERO() {
        given(voucherService.getFlybuysDiscountForOrder(orderModel)).willReturn(flybuysDiscountModel);
        given(flybuysDiscountModel.getSecurityToken()).willReturn("10j39s34psk9384fjmcks");
        given(orderModel.getModificationRecords()).willReturn(
                getOrderModificationRecordModelListWithFlybuysRefundAmount(Double.valueOf(0d)));

        assertThat(flybuysDiscountService.refundFlybuysPoints(orderModel)).isNull();
    }

    @Test
    public void testRefundFlybuysPointsWithFlybuysRefundPointsNull() {
        given(voucherService.getFlybuysDiscountForOrder(orderModel)).willReturn(flybuysDiscountModel);
        given(flybuysDiscountModel.getSecurityToken()).willReturn("10j39s34psk9384fjmcks");
        given(orderModel.getModificationRecords()).willReturn(
                getOrderModificationRecordModelListWithFlybuysRefundPoints(null));

        assertThat(flybuysDiscountService.refundFlybuysPoints(orderModel)).isNull();
    }

    @Test
    public void testRefundFlybuysPointsWithFlybuysRefundPointsZERO() {
        given(voucherService.getFlybuysDiscountForOrder(orderModel)).willReturn(flybuysDiscountModel);
        given(flybuysDiscountModel.getSecurityToken()).willReturn("10j39s34psk9384fjmcks");
        given(orderModel.getModificationRecords()).willReturn(
                getOrderModificationRecordModelListWithFlybuysRefundPoints(Integer.valueOf(0)));

        assertThat(flybuysDiscountService.refundFlybuysPoints(orderModel)).isNull();
    }

    @Test
    public void testRefundFlybuysPointsWithFlybuysRefundResponseDtoNull() {
        given(voucherService.getFlybuysDiscountForOrder(orderModel)).willReturn(flybuysDiscountModel);
        given(flybuysDiscountModel.getSecurityToken()).willReturn("10j39s34psk9384fjmcks");
        given(orderModel.getModificationRecords()).willReturn(
                getOrderModificationRecordModelList());

        assertThat(flybuysDiscountService.refundFlybuysPoints(orderModel)).isNull();
    }

    @Test
    public void testRefundFlybuysPoints() {
        final String flybuysCardNumber = "82972778169675";
        final String securityToken = "1234";
        final String orderNumber = "12345678";
        given(voucherService.getFlybuysDiscountForOrder(orderModel)).willReturn(flybuysDiscountModel);
        given(flybuysDiscountModel.getSecurityToken()).willReturn(securityToken);
        given(flybuysDiscountModel.getFlybuysCardNumber()).willReturn(flybuysCardNumber);
        given(orderModel.getModificationRecords()).willReturn(
                getOrderModificationRecordModelList());
        given(flybuysDiscountModel.getValue()).willReturn(Double.valueOf(100d));
        given(flybuysRefundResponseDto.getConfimationCode()).willReturn("ConfCode");
        given(orderModel.getCode()).willReturn(orderNumber);

        appliedVouchers.add(flybuysDiscountModel);

        given(
                flybuysClient.refundFlybuysPoints(eq(flybuysCardNumber),
                        eq(securityToken), eq(STORE_NUMBER), any(Integer.class), any(BigDecimal.class),
                        eq(TRANSACTION_ID_PREFIX),
                        eq(TRANSACTION_ID_PREFIX + orderNumber))).willReturn(flybuysRefundResponseDto);

        final FlybuysRefundResponseDto flybuysRefundResponse = flybuysDiscountService.refundFlybuysPoints(
                orderModel);

        assertThat(flybuysRefundResponse).isNotNull();
        assertThat(flybuysRefundResponse).isSameAs(flybuysRefundResponseDto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetFlybuysPointsToRefundWithFlybuysNull() {
        flybuysDiscountService.getFlybuysPointsToRefund(null, BigDecimal.valueOf(3));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetFlybuysPointsToRefundWithFBRefundAmountNull() {
        flybuysDiscountService.getFlybuysPointsToRefund(flybuysDiscountModel, null);
    }

    @Test
    public void testGetFlybuysPointsToRefundWithTotalFBDiscountValueNull() {
        when(flybuysDiscountModel.getValue()).thenReturn(null);

        Assert.assertEquals(
                flybuysDiscountService.getFlybuysPointsToRefund(flybuysDiscountModel, BigDecimal.valueOf(2)),
                BigDecimal.ZERO);
    }

    @Test
    public void testGetFlybuysPointsToRefundWithTotalFBPointsRedeemedNull() {
        when(flybuysDiscountModel.getValue()).thenReturn(Double.valueOf(10));
        when(flybuysDiscountModel.getPointsConsumed()).thenReturn(null);

        Assert.assertEquals(
                flybuysDiscountService.getFlybuysPointsToRefund(flybuysDiscountModel, BigDecimal.valueOf(2)),
                BigDecimal.ZERO);
    }

    @Test
    public void testGetFlybuysPointsToRefundWithFBRefundAmountZero() {
        Assert.assertEquals(flybuysDiscountService.getFlybuysPointsToRefund(flybuysDiscountModel, BigDecimal.ZERO),
                BigDecimal.ZERO);
    }

    @Test
    public void testGetFlybuysPointsToRefundWithTotalFBDiscountValueZero() {
        when(flybuysDiscountModel.getValue()).thenReturn(Double.valueOf(0));
        when(flybuysDiscountModel.getPointsConsumed()).thenReturn(Integer.valueOf(2000));

        Assert.assertEquals(
                flybuysDiscountService.getFlybuysPointsToRefund(flybuysDiscountModel, BigDecimal.valueOf(2)),
                BigDecimal.ZERO);
    }

    @Test
    public void testGetFlybuysPointsToRefundWithTotalFBPointsRedeemedZero() {
        when(flybuysDiscountModel.getValue()).thenReturn(Double.valueOf(10));
        when(flybuysDiscountModel.getPointsConsumed()).thenReturn(Integer.valueOf(0));

        Assert.assertEquals(
                flybuysDiscountService.getFlybuysPointsToRefund(flybuysDiscountModel, BigDecimal.valueOf(2)),
                BigDecimal.ZERO);
    }

    @Test
    public void testGetFlybuysPointsToRefundWithFBRefundAmountGreaterThanTotalFBDiscountValue() {
        when(flybuysDiscountModel.getValue()).thenReturn(Double.valueOf(10));
        when(flybuysDiscountModel.getPointsConsumed()).thenReturn(Integer.valueOf(2000));

        Assert.assertEquals(
                flybuysDiscountService.getFlybuysPointsToRefund(flybuysDiscountModel, BigDecimal.valueOf(20)),
                BigDecimal.ZERO);
    }

    @Test
    public void testGetFlybuysPointsToRefund() {
        when(flybuysDiscountModel.getValue()).thenReturn(Double.valueOf(30));
        when(flybuysDiscountModel.getPointsConsumed()).thenReturn(Integer.valueOf(6000));

        Assert.assertEquals(
                flybuysDiscountService.getFlybuysPointsToRefund(flybuysDiscountModel, BigDecimal.valueOf(10))
                        .setScale(0),
                BigDecimal.valueOf(2000).setScale(0));
    }

    private Set<OrderModificationRecordModel> getOrderModificationRecordModelListWithFlybuysRefundAmount(
            final Double refundamount) {

        final Set<OrderModificationRecordModel> set = new HashSet<>();
        final OrderModificationRecordModel orderCancelRecordModel = new OrderModificationRecordModel();
        orderCancelRecordModel.setIdentifier("testModification1");
        orderCancelRecordModel.setCreationtime(DateUtils.addDays(DateTime.now().toDate(), 0));

        final List<OrderModificationRecordEntryModel> orderModificationRecordEntryModelList = new ArrayList<>();
        orderModificationRecordEntryModelList.add(getOrderModificationRecordEntryModel("testEntry1", refundamount,
                Integer.valueOf(500), DateUtils.addDays(DateTime.now().toDate(), -2)));
        orderCancelRecordModel.setModificationRecordEntries(orderModificationRecordEntryModelList);
        set.add(orderCancelRecordModel);
        return set;
    }

    private Set<OrderModificationRecordModel> getOrderModificationRecordModelListWithFlybuysRefundPoints(
            final Integer refundPoints) {

        final Set<OrderModificationRecordModel> set = new HashSet<>();
        final OrderModificationRecordModel orderCancelRecordModel = new OrderModificationRecordModel();
        orderCancelRecordModel.setIdentifier("testModification1");
        orderCancelRecordModel.setCreationtime(DateUtils.addDays(DateTime.now().toDate(), 0));

        final List<OrderModificationRecordEntryModel> orderModificationRecordEntryModelList = new ArrayList<>();
        orderModificationRecordEntryModelList.add(getOrderModificationRecordEntryModel("testEntry1",
                Double.valueOf(5d), refundPoints, DateUtils.addDays(DateTime.now().toDate(), -2)));
        orderCancelRecordModel.setModificationRecordEntries(orderModificationRecordEntryModelList);
        set.add(orderCancelRecordModel);
        return set;
    }

    private Set<OrderModificationRecordModel> getOrderModificationRecordModelList() {

        final Set<OrderModificationRecordModel> set = new HashSet<>();
        final OrderModificationRecordModel model1 = new OrderModificationRecordModel();
        model1.setIdentifier("testModification1");
        model1.setCreationtime(DateUtils.addDays(DateTime.now().toDate(), -2));
        model1.setModificationRecordEntries(getOrderModificationRecordEntryModelList());
        set.add(model1);

        final OrderModificationRecordModel model2 = new OrderModificationRecordModel();
        model2.setIdentifier("testModification2");
        model2.setCreationtime(DateUtils.addDays(DateTime.now().toDate(), -1));
        model2.setModificationRecordEntries(getOrderModificationRecordEntryModelList());
        set.add(model2);

        final OrderModificationRecordModel model3 = new OrderModificationRecordModel();
        model3.setIdentifier("testModification3");
        model3.setCreationtime(DateUtils.addDays(DateTime.now().toDate(), 0));
        model3.setModificationRecordEntries(getOrderModificationRecordEntryModelList());
        set.add(model3);

        return set;
    }

    private OrderModificationRecordEntryModel getOrderModificationRecordEntryModel(final String code,
            final Double flybuysRefundAmount, final Integer flybuysRefundPoints, final Date creationDate) {
        final OrderModificationRecordEntryModel model1 = new OrderModificationRecordEntryModel();
        model1.setCode(code);
        model1.setRefundedFlybuysAmount(flybuysRefundAmount);
        model1.setRefundedFlybuysPoints(flybuysRefundPoints);
        model1.setCreationtime(creationDate);
        return model1;
    }

    private List<OrderModificationRecordEntryModel> getOrderModificationRecordEntryModelList() {

        final List<OrderModificationRecordEntryModel> entryList = new ArrayList<>();

        entryList.add(getOrderModificationRecordEntryModel("testEntry1", Double.valueOf(5d), Integer.valueOf(500),
                DateUtils.addDays(DateTime.now().toDate(), -2)));

        entryList.add(getOrderModificationRecordEntryModel("testEntry2", Double.valueOf(2d), Integer.valueOf(200),
                DateUtils.addDays(DateTime.now().toDate(), -1)));

        entryList.add(getOrderModificationRecordEntryModel("testEntry3", Double.valueOf(3d), Integer.valueOf(300),
                DateUtils.addDays(DateTime.now().toDate(), 0)));
        return entryList;
    }

    @Test
    public void testSetFlybuysRefundDetailsInModificationEntryWithFlybuys() {
        when(voucherService.getFlybuysDiscountForOrder(orderModel)).thenReturn(flybuysDiscountModel);
        final Double orderTotalBeforeVoucherUpdation = Double.valueOf(-10d);
        flybuysDiscountService.setFlybuysRefundDetailsInModificationEntry(orderModRecEntryModel, orderModel,
                orderTotalBeforeVoucherUpdation);
        verify(orderModRecEntryModel).setRefundedFlybuysAmount(Double.valueOf(10d));
        verify(orderModRecEntryModel).setRefundedFlybuysPoints(Integer.valueOf(0));
    }


    @Test
    public void testSetFlybuysRefundDetailsInModificationEntryWithoutFlybuys() {
        when(voucherService.getFlybuysDiscountForOrder(orderModel)).thenReturn(null);
        final Double orderTotalBeforeVoucherUpdation = Double.valueOf(-10d);
        flybuysDiscountService.setFlybuysRefundDetailsInModificationEntry(orderModRecEntryModel, orderModel,
                orderTotalBeforeVoucherUpdation);
        verify(orderModRecEntryModel, Mockito.never()).setRefundedFlybuysAmount(Double.valueOf(10d));
        verify(orderModRecEntryModel, Mockito.never()).setRefundedFlybuysPoints(Integer.valueOf(0));
    }


    @Test
    public void testSetFlybuysRefundDetailsInModificationEntryWithPositiveTotalPrice() {
        when(voucherService.getFlybuysDiscountForOrder(orderModel)).thenReturn(flybuysDiscountModel);
        final Double orderTotalBeforeVoucherUpdation = Double.valueOf(10d);
        flybuysDiscountService.setFlybuysRefundDetailsInModificationEntry(orderModRecEntryModel, orderModel,
                orderTotalBeforeVoucherUpdation);
        verify(orderModRecEntryModel, Mockito.never()).setRefundedFlybuysAmount(Double.valueOf(10d));
        verify(orderModRecEntryModel, Mockito.never()).setRefundedFlybuysPoints(Integer.valueOf(0));
    }
}