/**
 * 
 */
package au.com.target.tgtcore.flybuys.strategy.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.voucher.model.VoucherModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.FlybuysDiscountModel;


/**
 * @author dcwillia
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFlybuysVoucherConverterStategyImplTest {
    private final TargetFlybuysVoucherConverterStategyImpl stategy = new TargetFlybuysVoucherConverterStategyImpl();

    @Test
    public void testGetVoucherDescriptionNonFlybuys() {
        final VoucherModel voucher = mock(VoucherModel.class);
        final String voucherCode = "1234";

        final String descr = stategy.getVoucherDescription(voucher, voucherCode);

        assertEquals(voucherCode, descr);
    }

    @Test
    public void testGetVoucherDescriptionFlybuys() {
        final VoucherModel voucher = mock(FlybuysDiscountModel.class);
        final String voucherCode = "1234";

        final String descr = stategy.getVoucherDescription(voucher, voucherCode);

        assertEquals(TargetFlybuysVoucherConverterStategyImpl.FLYBUYS_LINE_NAME, descr);
    }

    @Test
    public void testGetVoucherDescriptionNullVoucher() {
        final VoucherModel voucher = null;
        final String voucherCode = "1234";

        final String descr = stategy.getVoucherDescription(voucher, voucherCode);

        assertEquals(voucherCode, descr);
    }

    @Test
    public void testGetVoucherDescriptionNullVoucherCodeFlybuys() {
        final VoucherModel voucher = mock(FlybuysDiscountModel.class);
        final String voucherCode = null;

        final String descr = stategy.getVoucherDescription(voucher, voucherCode);

        assertEquals(TargetFlybuysVoucherConverterStategyImpl.FLYBUYS_LINE_NAME, descr);
    }

    @Test
    public void testGetVoucherDescriptionNullVoucherCodeNonFlybuys() {
        final VoucherModel voucher = mock(VoucherModel.class);
        final String voucherCode = null;

        final String descr = stategy.getVoucherDescription(voucher, voucherCode);

        assertEquals(null, descr);
    }

    @Test
    public void testGetVoucherDescriptionAllNulls() {
        final VoucherModel voucher = null;
        final String voucherCode = null;

        final String descr = stategy.getVoucherDescription(voucher, voucherCode);

        assertEquals(voucherCode, descr);
    }
}
