/**
 * 
 */
package au.com.target.tgtcore.refund.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.orderhistory.OrderHistoryService;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.returns.OrderReturnRecordsHandlerException;
import de.hybris.platform.returns.model.OrderReturnRecordEntryModel;
import de.hybris.platform.returns.model.OrderReturnRecordModel;
import de.hybris.platform.returns.model.RefundEntryModel;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.returns.strategy.ReturnableCheck;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.flybuys.service.FlybuysDiscountService;
import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtcore.order.TargetFindDeliveryCostStrategy;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.ordercancel.TargetCancelService;
import au.com.target.tgtcore.ordercancel.TargetRefundPaymentService;
import au.com.target.tgtcore.ordercancel.data.IpgNewRefundInfoDTO;
import au.com.target.tgtcore.ordercancel.data.RefundInfoDTO;
import au.com.target.tgtcore.ordercancel.discountstrategy.VoucherCorrectionStrategy;
import au.com.target.tgtcore.orderreturn.TargetOrderReturnRecordHandler;
import au.com.target.tgtcore.refund.denialstrategies.TargetPaymentTypeRefundDenailStrategy;
import au.com.target.tgtcore.refund.denialstrategies.TargetSalesChannelRefundDenialStrategy;


/**
 * Test suite for {@link TargetRefundServiceImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetRefundServiceImplTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @Mock
    private TargetCancelService targetCancelService;

    @Mock
    private TargetRefundPaymentService targetRefundPaymentService;

    @Mock
    private TargetBusinessProcessService businessProcessService;

    @Mock
    private TargetOrderReturnRecordHandler modificationHandler;

    @Mock
    private FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    @Mock
    private TargetOrderService orderService;

    @Mock
    private OrderModel order;

    @Mock
    private ModelService modelService;

    @Mock
    private PlatformTransactionManager transactionManager;

    @Mock
    private ReturnRequestModel request;

    @Mock
    private RefundEntryModel refundEntry;

    @Mock
    private CreditCardPaymentInfoModel paymentInfo;

    @Mock
    private OrderHistoryService orderHistoryService;

    @Mock
    private PromotionsService promotionsService;

    @Mock
    private VoucherCorrectionStrategy mockVoucherCorrectionStrategy;

    @Mock
    private FlybuysDiscountService flybuysDiscountService;

    @Mock
    private TargetFindDeliveryCostStrategy targetFindDeliveryCostStrategy;

    @Mock
    private TargetPaymentTypeRefundDenailStrategy targetPaymentTypeRefundDenialStrategy;

    @Mock
    private TargetSalesChannelRefundDenialStrategy targetSalesChannelRefundDenialStrategy;

    private ConsignmentModel consignmentModel;

    private Set<ConsignmentModel> consignments;

    private AbstractOrderEntryModel orderEntry;

    @InjectMocks
    private final TargetRefundServiceImpl targetRefundServiceImpl = new TargetRefundServiceImpl();


    /**
     * Initializes test case before run.
     */
    @Before
    public void setUp() {
        when(request.getReturnEntries()).thenReturn(ImmutableList.<ReturnEntryModel> of(refundEntry));

        targetRefundServiceImpl.setTransactionTemplate(new TransactionTemplate(transactionManager));

        Mockito.when(paymentInfo.getSubscriptionId()).thenReturn("123");
        consignmentModel = new ConsignmentModel();
        consignments = new HashSet<>();
        consignments.add(consignmentModel);
    }

    /**
     * Verifies that refund request must contain an order and entries to refund.
     */
    @Test
    public void testApplyExceptions() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Order can't be null");

        targetRefundServiceImpl.apply(request, null);

        when(request.getReturnEntries()).thenReturn(ImmutableList.<ReturnEntryModel> of());
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("refunds list can't be empty");

        targetRefundServiceImpl.apply(request, order);
    }

    @Test
    public void testApplyIsPreviewOrder() {
        final OrderModel originalVersion = Mockito.mock(OrderModel.class);
        BDDMockito.given(order.getOriginalVersion()).willReturn(originalVersion);

        targetRefundServiceImpl.apply(request, order);

        Mockito.verifyZeroInteractions(modificationHandler);
        Mockito.verifyZeroInteractions(findOrderTotalPaymentMadeStrategy);
        Mockito.verifyZeroInteractions(targetRefundPaymentService);
        Mockito.verifyZeroInteractions(businessProcessService);
        Mockito.verify(targetCancelService).previewOrderPreventingIncrease(order, 0d);
    }

    @Test
    public void testApply() throws OrderReturnRecordsHandlerException {
        final double paidSoFar = 156d;
        final double newTotal = 19d;
        final BigDecimal refundAmount = new BigDecimal(paidSoFar - newTotal).setScale(2);

        final List<PaymentTransactionModel> paymentTransactions = new ArrayList<>();
        final PaymentTransactionModel paymentTransaction = new PaymentTransactionModel();
        final List<PaymentTransactionEntryModel> entries = new ArrayList<>();
        final PaymentTransactionEntryModel entry = new PaymentTransactionEntryModel();

        entry.setType(PaymentTransactionType.CAPTURE);
        entry.setTransactionStatus(TransactionStatus.ACCEPTED.toString());
        entry.setPaymentTransaction(paymentTransaction);
        entries.add(entry);

        paymentTransaction.setEntries(entries);

        paymentTransactions.add(paymentTransaction);

        final OrderReturnRecordEntryModel orderReturnRecordEntryModel = mock(OrderReturnRecordEntryModel.class);
        final OrderReturnRecordModel orderReturnRecordModel = mock(OrderReturnRecordModel.class);
        when(orderReturnRecordEntryModel.getOwner()).thenReturn(orderReturnRecordModel);
        when(order.getCode()).thenReturn("000");
        when(order.getPaymentTransactions()).thenReturn(paymentTransactions);
        when(order.getTotalPrice()).thenReturn(Double.valueOf(newTotal));
        when(modificationHandler.createRefundEntry(order, request,
                "Refund request for order: " + order.getCode())).thenReturn(orderReturnRecordEntryModel);
        when(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(order)).thenReturn(Double.valueOf(paidSoFar));

        final RefundInfoDTO refundInfo = new RefundInfoDTO();
        given(targetRefundPaymentService.createRefundedInfo(anyList(), any(BigDecimal.class))).willReturn(refundInfo);

        targetRefundServiceImpl.apply(request, order);

        verify(targetCancelService).recalculateOrderPreventingIncrease(order, null);
        verify(targetRefundPaymentService).performFollowOnRefund(order, refundAmount);
        verify(orderReturnRecordModel).setInProgress(false);
        verify(modelService).saveAll(orderReturnRecordModel, orderReturnRecordEntryModel);
        verify(modelService).refresh(orderReturnRecordEntryModel);
        verify(mockVoucherCorrectionStrategy).correctAppliedVouchers(order);
    }

    /**
     * Verifies that RefundService processes refund for arbitrary card
     */
    @Test
    public void testApplyWithPaymentInfo() throws OrderReturnRecordsHandlerException {
        final double paidSoFar = 156d;
        final double newTotal = 19d;

        final List<PaymentTransactionModel> paymentTransactions = new ArrayList<>();
        final PaymentTransactionModel paymentTransaction = new PaymentTransactionModel();
        final List<PaymentTransactionEntryModel> entries = new ArrayList<>();
        final PaymentTransactionEntryModel entry = new PaymentTransactionEntryModel();

        entry.setType(PaymentTransactionType.CAPTURE);
        entry.setTransactionStatus(TransactionStatus.ACCEPTED.toString());
        entry.setPaymentTransaction(paymentTransaction);
        entries.add(entry);

        paymentTransaction.setEntries(entries);

        paymentTransactions.add(paymentTransaction);

        final OrderReturnRecordEntryModel orderReturnRecordEntryModel = mock(OrderReturnRecordEntryModel.class);
        final OrderReturnRecordModel orderReturnRecordModel = mock(OrderReturnRecordModel.class);
        when(orderReturnRecordEntryModel.getOwner()).thenReturn(orderReturnRecordModel);
        when(order.getCode()).thenReturn("000");
        when(order.getPaymentTransactions()).thenReturn(paymentTransactions);
        when(order.getTotalPrice()).thenReturn(Double.valueOf(newTotal));
        when(modificationHandler.createRefundEntry(order, request,
                "Refund request for order: " + order.getCode())).thenReturn(orderReturnRecordEntryModel);
        when(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(order)).thenReturn(Double.valueOf(paidSoFar));

        final RefundInfoDTO refundInfo = new RefundInfoDTO();
        given(targetRefundPaymentService.createRefundedInfo(anyList(), any(BigDecimal.class))).willReturn(refundInfo);

        targetRefundServiceImpl.apply(request, order, paymentInfo, null);

        verify(targetCancelService).recalculateOrderPreventingIncrease(order, null);
        verify(targetRefundPaymentService).performStandaloneRefund(order,
                new BigDecimal(paidSoFar - newTotal).setScale(2),
                paymentInfo);
        verify(orderReturnRecordModel).setInProgress(false);
        verify(modelService).save(paymentInfo);
        verify(modelService).saveAll(orderReturnRecordModel, orderReturnRecordEntryModel);
        verify(modelService).refresh(orderReturnRecordEntryModel);
        verify(mockVoucherCorrectionStrategy).correctAppliedVouchers(order);
    }

    @Test
    public void testApplyWithIpgManualRefund() throws OrderReturnRecordsHandlerException {
        final double paidSoFar = 156d;
        final double newTotal = 19d;

        final List<PaymentTransactionModel> paymentTransactions = new ArrayList<>();
        final PaymentTransactionModel paymentTransaction = new PaymentTransactionModel();
        final List<PaymentTransactionEntryModel> entries = new ArrayList<>();
        final PaymentTransactionEntryModel entry = new PaymentTransactionEntryModel();

        entry.setType(PaymentTransactionType.CAPTURE);
        entry.setTransactionStatus(TransactionStatus.ACCEPTED.toString());
        entry.setPaymentTransaction(paymentTransaction);
        entries.add(entry);

        paymentTransaction.setEntries(entries);

        paymentTransactions.add(paymentTransaction);

        final OrderReturnRecordEntryModel orderReturnRecordEntryModel = mock(OrderReturnRecordEntryModel.class);
        final OrderReturnRecordModel orderReturnRecordModel = mock(OrderReturnRecordModel.class);
        when(orderReturnRecordEntryModel.getOwner()).thenReturn(orderReturnRecordModel);
        when(order.getCode()).thenReturn("000");
        when(order.getPaymentTransactions()).thenReturn(paymentTransactions);
        when(order.getTotalPrice()).thenReturn(Double.valueOf(newTotal));
        when(modificationHandler.createRefundEntry(order, request,
                "Refund request for order: " + order.getCode())).thenReturn(orderReturnRecordEntryModel);
        when(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(order)).thenReturn(Double.valueOf(paidSoFar));

        final RefundInfoDTO refundInfo = new RefundInfoDTO();
        given(targetRefundPaymentService.createRefundedInfo(anyList(), any(BigDecimal.class))).willReturn(refundInfo);

        final IpgNewRefundInfoDTO ipgNewRefundInfoDTO = new IpgNewRefundInfoDTO();
        targetRefundServiceImpl.apply(request, order, null, ipgNewRefundInfoDTO);

        verify(targetRefundPaymentService).performIpgManualRefund(order,
                new BigDecimal(paidSoFar - newTotal).setScale(2),
                ipgNewRefundInfoDTO);
    }

    @Test
    public void testApplyWithRefundFollowOn() throws OrderReturnRecordsHandlerException {
        final double paidSoFar = 156d;
        final double newTotal = 19d;

        final List<PaymentTransactionModel> paymentTransactions = new ArrayList<>();
        final PaymentTransactionModel paymentTransaction = new PaymentTransactionModel();
        final List<PaymentTransactionEntryModel> entries = new ArrayList<>();
        final PaymentTransactionEntryModel entry = new PaymentTransactionEntryModel();

        entry.setType(PaymentTransactionType.CAPTURE);
        entry.setTransactionStatus(TransactionStatus.ACCEPTED.toString());
        entry.setPaymentTransaction(paymentTransaction);
        entries.add(entry);

        paymentTransaction.setEntries(entries);

        paymentTransactions.add(paymentTransaction);

        final OrderReturnRecordEntryModel orderReturnRecordEntryModel = mock(OrderReturnRecordEntryModel.class);
        final OrderReturnRecordModel orderReturnRecordModel = mock(OrderReturnRecordModel.class);
        when(orderReturnRecordEntryModel.getOwner()).thenReturn(orderReturnRecordModel);
        when(order.getCode()).thenReturn("000");
        when(order.getPaymentTransactions()).thenReturn(paymentTransactions);
        when(order.getTotalPrice()).thenReturn(Double.valueOf(newTotal));
        when(modificationHandler.createRefundEntry(order, request,
                "Refund request for order: " + order.getCode())).thenReturn(orderReturnRecordEntryModel);
        when(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(order)).thenReturn(Double.valueOf(paidSoFar));

        final RefundInfoDTO refundInfo = new RefundInfoDTO();
        given(targetRefundPaymentService.createRefundedInfo(anyList(), any(BigDecimal.class))).willReturn(refundInfo);

        targetRefundServiceImpl.apply(request, order, null, null);

        verify(targetRefundPaymentService).performFollowOnRefund(order,
                new BigDecimal(paidSoFar - newTotal).setScale(2));
    }

    /**
     * Verifies that order modification records are copied from origin to preview.
     */
    @Test
    public void testCreateOrderPreview() {
        final OrderModel preview = mock(OrderModel.class);

        when(order.getModificationRecords()).thenReturn(ImmutableSet.<OrderModificationRecordModel> of());
        when(orderHistoryService.createHistorySnapshot(order)).thenReturn(preview);

        targetRefundServiceImpl.createRefundOrderPreview(order);

        verify(preview).setModificationRecords(order.getModificationRecords());
    }

    @Test
    public void testIsDeliveryCostRefundableWhenDeliveryFeeExistsAndIsRefundable() {
        final TargetRefundServiceImpl spyTargetRefundServiceSpy = Mockito.spy(targetRefundServiceImpl);
        mockIsRefundable(spyTargetRefundServiceSpy, true);
        when(Double.valueOf(targetFindDeliveryCostStrategy.deliveryCostOutstandingForOrder(order)))
                .thenReturn(Double.valueOf(10.0));
        Assert.assertTrue(spyTargetRefundServiceSpy.isDeliveryCostRefundable(order));
    }

    @Test
    public void testIsDeliveryCostRefundableForNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Order can't be null");
        targetRefundServiceImpl.isDeliveryCostRefundable(null);
    }

    @Test
    public void testIsDeliveryCostRefundableWhenDeliveryIsFreeAndIsRefundable() {
        final TargetRefundServiceImpl spyTargetRefundServiceSpy = Mockito.spy(targetRefundServiceImpl);
        mockIsRefundable(spyTargetRefundServiceSpy, true);
        when(Double.valueOf(targetFindDeliveryCostStrategy.deliveryCostOutstandingForOrder(order)))
                .thenReturn(Double.valueOf(0.0));
        Assert.assertFalse(spyTargetRefundServiceSpy.isDeliveryCostRefundable(order));
    }

    @Test
    public void testIsDeliveryCostRefundableWhenDeliveryIsNotFreeAndIsNotRefundable() {
        final TargetRefundServiceImpl spyTargetRefundServiceSpy = Mockito.spy(targetRefundServiceImpl);
        mockIsRefundable(spyTargetRefundServiceSpy, false);
        when(Double.valueOf(targetFindDeliveryCostStrategy.deliveryCostOutstandingForOrder(order)))
                .thenReturn(Double.valueOf(10.0));
        Assert.assertFalse(spyTargetRefundServiceSpy.isDeliveryCostRefundable(order));
    }

    /**
     * Method to mock method isRefundable
     * 
     * @param spyTargetRefundServiceSpy
     */
    private void mockIsRefundable(final TargetRefundServiceImpl spyTargetRefundServiceSpy, final boolean isRefundable) {
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        final AbstractOrderEntryModel entry = Mockito.mock(AbstractOrderEntryModel.class);
        orderEntries.add(entry);
        when(Boolean.valueOf(spyTargetRefundServiceSpy.isRefundable(order, entry, 0)))
                .thenReturn(Boolean.valueOf(isRefundable));
        when(order.getEntries()).thenReturn(orderEntries);
    }

    @Test
    public void testCreateOrderPreviewWithModifications() {
        final OrderModificationRecordModel record = new OrderModificationRecordModel();
        record.setIdentifier("record");

        final OrderModificationRecordEntryModel recordEntry1 = new OrderModificationRecordEntryModel();
        recordEntry1.setCode("recordEntry1");
        final OrderModificationRecordEntryModel recordEntry2 = new OrderModificationRecordEntryModel();
        recordEntry2.setCode("recordEntry2");

        record.setModificationRecordEntries(Arrays.asList(recordEntry1, recordEntry2));

        final OrderModel preview = new OrderModel();
        preview.setVersionID("1");

        OrderModificationRecordModel clonedRecord = new OrderModificationRecordModel();
        final OrderModificationRecordEntryModel clonedRecordEntry1 = new OrderModificationRecordEntryModel();
        final OrderModificationRecordEntryModel clonedRecordEntry2 = new OrderModificationRecordEntryModel();

        when(orderHistoryService.createHistorySnapshot(order)).thenReturn(preview);
        when(order.getModificationRecords()).thenReturn(
                (Sets.newHashSet(Arrays.asList(record))));
        when(modelService.clone(record)).thenReturn(clonedRecord);
        when(modelService.clone(recordEntry1)).thenReturn(clonedRecordEntry1);
        when(modelService.clone(recordEntry2)).thenReturn(clonedRecordEntry2);

        targetRefundServiceImpl.createRefundOrderPreview(order);

        final Set<OrderModificationRecordModel> records = preview.getModificationRecords();
        Assert.assertEquals(1, records.size());

        clonedRecord = records.iterator().next();
        Assert.assertEquals(record.getIdentifier() + "_1_1", clonedRecord.getIdentifier());
        Assert.assertEquals(2, clonedRecord.getModificationRecordEntries().size());

        for (int index = 1; index <= clonedRecord.getModificationRecordEntries().size(); index++) {
            if (index == 1) {
                Assert.assertEquals(recordEntry1.getCode() + "_1_1", clonedRecordEntry1.getCode());
            }
            else {
                Assert.assertEquals(recordEntry2.getCode() + "_1_2", clonedRecordEntry2.getCode());
            }
        }
    }

    @Test
    public void isRefundableForInValidSalesChannel() {
        final List<ReturnableCheck> refundableChecks = new ArrayList<>();
        when(Boolean.valueOf(targetSalesChannelRefundDenialStrategy.perform(order, orderEntry, 0))).thenReturn(
                Boolean.FALSE);
        when(Boolean.valueOf(targetPaymentTypeRefundDenialStrategy.perform(order, orderEntry, 0))).thenReturn(
                Boolean.TRUE);

        refundableChecks.add(targetPaymentTypeRefundDenialStrategy);
        refundableChecks.add(targetSalesChannelRefundDenialStrategy);
        targetRefundServiceImpl.setRefundableChecks(refundableChecks);
        Assert.assertFalse(targetRefundServiceImpl.isRefundable(order, orderEntry, 0));

    }

    @Test
    public void isRefundableForInValidPaymentMethod() {
        final List<ReturnableCheck> refundableChecks = new ArrayList<>();
        when(Boolean.valueOf(targetSalesChannelRefundDenialStrategy.perform(order, orderEntry, 0))).thenReturn(
                Boolean.TRUE);
        when(Boolean.valueOf(targetPaymentTypeRefundDenialStrategy.perform(order, orderEntry, 0))).thenReturn(
                Boolean.FALSE);

        refundableChecks.add(targetPaymentTypeRefundDenialStrategy);
        refundableChecks.add(targetSalesChannelRefundDenialStrategy);
        targetRefundServiceImpl.setRefundableChecks(refundableChecks);
        Assert.assertFalse(targetRefundServiceImpl.isRefundable(order, orderEntry, 0));
    }

    @Test
    public void isRefundableForValidPaymentMethodAndSalesChannel() {
        final List<ReturnableCheck> refundableChecks = new ArrayList<>();
        when(Boolean.valueOf(targetSalesChannelRefundDenialStrategy.perform(order, orderEntry, 0))).thenReturn(
                Boolean.TRUE);
        when(Boolean.valueOf(targetPaymentTypeRefundDenialStrategy.perform(order, orderEntry, 0))).thenReturn(
                Boolean.TRUE);

        refundableChecks.add(targetPaymentTypeRefundDenialStrategy);
        refundableChecks.add(targetSalesChannelRefundDenialStrategy);
        targetRefundServiceImpl.setRefundableChecks(refundableChecks);
        Assert.assertTrue(targetRefundServiceImpl.isRefundable(order, orderEntry, 0));
    }
}
