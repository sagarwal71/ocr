/**
 * 
 */
package au.com.target.tgtcore.warehouse.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.stock.TargetStockLevelDao;
import au.com.target.tgtcore.warehouse.dao.TargetWarehouseDao;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;


/**
 * @author dcwillia
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetWarehouseServiceTest {

    @InjectMocks
    @Spy
    private final TargetWarehouseService targetWarehouseService = new TargetWarehouseServiceImpl();

    @Mock
    private TargetWarehouseDao targetWarehouseDao;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetStockLevelDao targetStockLevelDao;

    @Mock
    private WarehouseModel warehouseModel;

    @Test(expected = IllegalArgumentException.class)
    public void testGetWarehouseForPointOfServiceWithNullPoS() {
        targetWarehouseService.getWarehouseForPointOfService(null);
    }

    @Test
    public void testGetWarehouseForPointOfServiceNoResults() {
        final Integer storeNumber = Integer.valueOf(5678);

        final TargetPointOfServiceModel mockTargetPoS = mock(TargetPointOfServiceModel.class);
        given(mockTargetPoS.getStoreNumber()).willReturn(storeNumber);

        final List<WarehouseModel> warehouses = new ArrayList<>();
        given(targetWarehouseDao.getWarehouseForPointOfService(storeNumber)).willReturn(warehouses);

        final WarehouseModel result = targetWarehouseService.getWarehouseForPointOfService(mockTargetPoS);
        assertThat(result).isNull();
    }

    @Test
    public void testGetWarehouseForPointOfService() {
        final Integer storeNumber = Integer.valueOf(5678);

        final TargetPointOfServiceModel mockTargetPoS = mock(TargetPointOfServiceModel.class);
        given(mockTargetPoS.getStoreNumber()).willReturn(storeNumber);

        final WarehouseModel mockWarehouse = mock(WarehouseModel.class);
        final List<WarehouseModel> warehouses = new ArrayList<>();
        warehouses.add(mockWarehouse);

        given(targetWarehouseDao.getWarehouseForPointOfService(storeNumber)).willReturn(warehouses);

        final WarehouseModel result = targetWarehouseService.getWarehouseForPointOfService(mockTargetPoS);
        assertThat(result).isEqualTo(mockWarehouse);
    }

    @Test
    public void testGetOnlineWarehouseWhenThereIsAWarehouse() {
        given(targetWarehouseService.getDefWarehouse())
                .willReturn(Collections.singletonList(warehouseModel));
        final WarehouseModel warehouse = targetWarehouseService.getDefaultOnlineWarehouse();
        assertThat(warehouse).isNotNull();
        assertThat(warehouse).isSameAs(warehouseModel);
    }

    @Test
    public void testGetOnlineWarehouseWhenThereIsNoWarehouse() {
        given(targetWarehouseService.getDefWarehouse()).willReturn(null);
        final WarehouseModel warehouse = targetWarehouseService.getDefaultOnlineWarehouse();
        verify(targetWarehouseService, atLeastOnce()).getDefWarehouse();
        assertThat(warehouse).isNull();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsStockAdjustmentRequiredNullWarehouse() {
        targetWarehouseService.isStockAdjustmentRequired(null);
    }

    @Test
    public void testIsStockAdjustmentRequiredTrue() {
        willReturn(Boolean.TRUE).given(warehouseModel).isNeedStockAdjustment();
        final boolean result = targetWarehouseService.isStockAdjustmentRequired(warehouseModel);
        assertThat(result).isTrue();
    }

    @Test
    public void testIsStockAdjustmentRequiredFalse() {
        willReturn(Boolean.FALSE).given(warehouseModel).isNeedStockAdjustment();
        final boolean result = targetWarehouseService.isStockAdjustmentRequired(warehouseModel);
        assertThat(result).isFalse();
    }

    @Test
    public void getWarehouseForFluentLocationRef()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final WarehouseModel warehouse = mock(WarehouseModel.class);
        willReturn(Arrays.asList(warehouse)).given(targetWarehouseDao)
                .getWarehouseForFluentLocationRef("fluentLocationRef");
        assertThat(targetWarehouseService.getWarehouseForFluentLocationRef("fluentLocationRef")).isEqualTo(warehouse);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getWarehouseForFluentLocationRefNull()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        targetWarehouseService.getWarehouseForFluentLocationRef(null);
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void getWarehouseForFluentLocationRefUnkownId()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        willReturn(Collections.emptyList()).given(targetWarehouseDao)
                .getWarehouseForFluentLocationRef("fluentLocationRef");
        targetWarehouseService.getWarehouseForFluentLocationRef("fluentLocationRef");
    }

    @Test(expected = TargetAmbiguousIdentifierException.class)
    public void getWarehouseForFluentLocationRefAmbiguousId()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final WarehouseModel warehouse1 = mock(WarehouseModel.class);
        final WarehouseModel warehouse2 = mock(WarehouseModel.class);
        willReturn(Arrays.asList(warehouse1, warehouse2)).given(targetWarehouseDao)
                .getWarehouseForFluentLocationRef("fluentLocationRef");
        targetWarehouseService.getWarehouseForFluentLocationRef("fluentLocationRef");
    }
}
