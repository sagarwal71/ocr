/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.flybuys.dto.response.FlybuysRefundResponseDto;
import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;
import au.com.target.tgtcore.flybuys.service.FlybuysDiscountService;


/**
 * The Class FlybuysRefundActionTest.
 * 
 * @author umesh
 */
@UnitTest
public class FlybuysRefundActionTest {

    @InjectMocks
    private final FlybuysRefundAction flybuysRefundAction = new FlybuysRefundAction();

    @Mock
    private FlybuysDiscountService flybuysDiscountService;

    @Mock
    private OrderProcessModel process;

    @Mock
    private FlybuysRefundResponseDto responseDto;

    @Mock
    private OrderModel order;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        BDDMockito.given(process.getOrder()).willReturn(order);
        BDDMockito.given(flybuysDiscountService.refundFlybuysPoints(order)).willReturn(responseDto);
    }

    @Test(expected = NullPointerException.class)
    public void testExecuteInternalWithNoProcessFound() throws RetryLaterException, Exception {
        flybuysRefundAction.executeInternal(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteInternalWithNoOrderFound() throws RetryLaterException, Exception {
        BDDMockito.given(process.getOrder()).willReturn(null);
        flybuysRefundAction.executeInternal(process);
    }

    @Test
    public void testExecuteInternalWithFlybuysRefundResponseDtoAsNull() throws RetryLaterException, Exception {
        BDDMockito.given(flybuysDiscountService.refundFlybuysPoints(order)).willReturn(null);
        Assert.assertEquals("OK", flybuysRefundAction.executeInternal(process));
    }

    @Test
    public void testExecuteInternalWithResponseNotNullAndResponseStatusAsSUCCESS() throws RetryLaterException,
            Exception {
        BDDMockito.given(responseDto.getResponse()).willReturn(FlybuysResponseType.SUCCESS);
        Assert.assertEquals("OK", flybuysRefundAction.executeInternal(process));
    }

    @Test(expected = RetryLaterException.class)
    public void testExecuteInternalWithResponseNotNullAndResponseStatusAsINVALID() throws RetryLaterException,
            Exception {
        BDDMockito.given(responseDto.getResponse()).willReturn(FlybuysResponseType.INVALID);
        flybuysRefundAction.executeInternal(process);
    }

    @Test(expected = RetryLaterException.class)
    public void testExecuteInternalWithResponseNotNullAndResponseStatusAsUNAVAILABLE() throws RetryLaterException,
            Exception {
        BDDMockito.given(responseDto.getResponse()).willReturn(FlybuysResponseType.UNAVAILABLE);
        flybuysRefundAction.executeInternal(process);
    }

    @Test(expected = RetryLaterException.class)
    public void testExecuteInternalWithResponseNotNullAndResponseStatusAsERROR()
            throws RetryLaterException,
            Exception {
        BDDMockito.given(responseDto.getResponse()).willReturn(FlybuysResponseType.FLYBUYS_OTHER_ERROR);
        flybuysRefundAction.executeInternal(process);
    }
}
