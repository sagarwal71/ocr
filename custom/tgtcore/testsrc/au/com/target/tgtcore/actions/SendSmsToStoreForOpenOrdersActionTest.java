/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.sms.TargetSendSmsService;
import au.com.target.tgtcore.sms.dto.TargetSendSmsResponseDto;
import au.com.target.tgtcore.sms.enums.TargetSendSmsResponseType;


/**
 * @author Vivek
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendSmsToStoreForOpenOrdersActionTest {

    @InjectMocks
    private final SendSmsToStoreForOpenOrdersAction storeForOpenOrdersAction = new SendSmsToStoreForOpenOrdersAction();

    @Mock
    private TargetSendSmsService targetSendSmsService;

    @Test
    public void testSmsToStoreExecuteInternalWithOkStatus() throws RetryLaterException, Exception {

        final BusinessProcessModel process = Mockito.mock(BusinessProcessModel.class);
        final TargetSendSmsResponseDto targetSendSmsResponseDto = new TargetSendSmsResponseDto();
        targetSendSmsResponseDto.setResponseType(TargetSendSmsResponseType.SUCCESS);

        BDDMockito.given(
                targetSendSmsService.sendSmsToStoreForOpenOrders(process)).willReturn(
                targetSendSmsResponseDto);

        final String result = storeForOpenOrdersAction.executeInternal(process);

        Assert.assertEquals("OK", result);
    }

    @Test
    public void testSmsToStoreExecuteInternalWithInvalidStatus() throws RetryLaterException, Exception {

        final BusinessProcessModel process = Mockito.mock(BusinessProcessModel.class);
        final TargetSendSmsResponseDto targetSendSmsResponseDto = new TargetSendSmsResponseDto();
        targetSendSmsResponseDto.setResponseType(TargetSendSmsResponseType.INVALID);

        BDDMockito.given(
                targetSendSmsService.sendSmsToStoreForOpenOrders(process)).willReturn(
                targetSendSmsResponseDto);

        final String result = storeForOpenOrdersAction.executeInternal(process);

        Assert.assertEquals("NOK", result);
    }

    @Test
    public void testSmsToStoreExecuteInternalWithOthersStatus() throws RetryLaterException, Exception {

        final BusinessProcessModel process = Mockito.mock(BusinessProcessModel.class);
        final TargetSendSmsResponseDto targetSendSmsResponseDto = new TargetSendSmsResponseDto();
        targetSendSmsResponseDto.setResponseType(TargetSendSmsResponseType.OTHERS);

        BDDMockito.given(
                targetSendSmsService.sendSmsToStoreForOpenOrders(process)).willReturn(
                targetSendSmsResponseDto);

        final String result = storeForOpenOrdersAction.executeInternal(process);

        Assert.assertEquals("NOK", result);
    }

    @Test(expected = RetryLaterException.class)
    public void testSmsToStoreExecuteInternalWithUnavailableStatus() throws RetryLaterException, Exception {

        final BusinessProcessModel process = Mockito.mock(BusinessProcessModel.class);
        final TargetSendSmsResponseDto targetSendSmsResponseDto = new TargetSendSmsResponseDto();
        targetSendSmsResponseDto.setResponseType(TargetSendSmsResponseType.UNAVAILABLE);

        BDDMockito.given(
                targetSendSmsService.sendSmsToStoreForOpenOrders(process)).willReturn(
                targetSendSmsResponseDto);

        storeForOpenOrdersAction.executeInternal(process);
    }

}
