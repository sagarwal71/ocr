package au.com.target.tgtcore.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.actions.DeliveryModeDecisionAction.Transition;
import au.com.target.tgtcore.model.TargetConsignmentModel;


/**
 * Unit test for {@link DeliveryModeDecisionAction}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DeliveryModeDecisionActionTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @InjectMocks
    private final DeliveryModeDecisionAction deliveryModeDecisionAction = new DeliveryModeDecisionAction();

    @Mock
    private OrderProcessModel orderProcessModel;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private TargetConsignmentModel con;

    @Mock
    private TargetZoneDeliveryModeModel targetDeliveryMode;


    @Test
    public void testWithNullConsignment() throws RetryLaterException, Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Consignment cannot be null");
        deliveryModeDecisionAction.execute(orderProcessModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithNullDeliveryMode() throws RetryLaterException, Exception {

        BDDMockito.given(orderProcessParameterHelper.getConsignment(orderProcessModel)).willReturn(con);

        deliveryModeDecisionAction.execute(orderProcessModel);
    }

    @Test
    public void testWithNonTargetDeliveryMode() throws RetryLaterException, Exception {

        final DeliveryModeModel deliveryMode = Mockito.mock(DeliveryModeModel.class);
        BDDMockito.given(orderProcessParameterHelper.getConsignment(orderProcessModel)).willReturn(con);
        BDDMockito.given(con.getDeliveryMode()).willReturn(deliveryMode);

        final String result = deliveryModeDecisionAction.execute(orderProcessModel);

        Assert.assertEquals(Transition.SHIP.name(), result);
    }

    @Test
    public void testWithTargetStoreDeliveryMode() throws RetryLaterException, Exception {

        BDDMockito.given(orderProcessParameterHelper.getConsignment(orderProcessModel)).willReturn(con);
        BDDMockito.given(con.getDeliveryMode()).willReturn(targetDeliveryMode);
        BDDMockito.given(targetDeliveryMode.getIsDeliveryToStore()).willReturn(Boolean.TRUE);

        final String result = deliveryModeDecisionAction.execute(orderProcessModel);

        Assert.assertEquals(Transition.PICK_UP.name(), result);
    }

    @Test
    public void testWithTargetNonStoreDeliveryMode() throws RetryLaterException, Exception {

        BDDMockito.given(orderProcessParameterHelper.getConsignment(orderProcessModel)).willReturn(con);
        BDDMockito.given(con.getDeliveryMode()).willReturn(targetDeliveryMode);
        BDDMockito.given(targetDeliveryMode.getIsDeliveryToStore()).willReturn(Boolean.FALSE);

        final String result = deliveryModeDecisionAction.execute(orderProcessModel);

        Assert.assertEquals(Transition.SHIP.name(), result);
    }

    @Test
    public void testWithTargetDeliveryModeNullStoreDeliveryFlag() throws RetryLaterException, Exception {

        BDDMockito.given(orderProcessParameterHelper.getConsignment(orderProcessModel)).willReturn(con);
        BDDMockito.given(con.getDeliveryMode()).willReturn(targetDeliveryMode);
        BDDMockito.given(targetDeliveryMode.getIsDeliveryToStore()).willReturn(null);

        final String result = deliveryModeDecisionAction.execute(orderProcessModel);

        Assert.assertEquals(Transition.SHIP.name(), result);
    }
}
