/**
 * 
 */

package au.com.target.tgtcore.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import org.junit.Assert;


/**
 * @author pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StartSendEmailWithBlackoutProcessActionTest {

    @InjectMocks
    private final StartSendEmailWithBlackoutProcessAction action = new StartSendEmailWithBlackoutProcessAction() {

        @Override
        public TargetBusinessProcessService getTargetBusinessProcessService() {
            return targetBusinessProcessService;
        }
    };

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private OrderProcessModel orderProcess;

    @Mock
    private OrderModel orderModel;

    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private Object cncData;

    @Before
    public void setUp() {
        Mockito.when(orderProcess.getOrder()).thenReturn(orderModel);
        Mockito.when(orderProcessParameterHelper.getCncNotificationData(orderProcess)).thenReturn(cncData);
    }

    @Test
    public void testExecuteActionWhenFeatureSwitchIsEnabled() throws RetryLaterException, Exception {
        Mockito.when(Boolean.valueOf(targetFeatureSwitchService.sendEmailWithBlackout())).thenReturn(Boolean.TRUE);
        final String transition = action.execute(orderProcess);

        Assert.assertEquals("OK", transition);
        Mockito.verify(targetBusinessProcessService).startSendEmailWithBlackoutProcess(orderModel, cncData);
    }

    @Test
    public void testExecuteActionWhenFeatureSwitchIsDisabled() throws RetryLaterException, Exception {
        Mockito.when(Boolean.valueOf(targetFeatureSwitchService.sendEmailWithBlackout())).thenReturn(Boolean.FALSE);
        final String transition = action.execute(orderProcess);

        Assert.assertEquals("NOK", transition);
        Mockito.verifyZeroInteractions(targetBusinessProcessService);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWhenOrderIsNull() throws RetryLaterException, Exception {
        Mockito.when(orderProcess.getOrder()).thenReturn(null);
        action.execute(orderProcess);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWhenOrderProcessIsNull() throws RetryLaterException, Exception {
        action.execute(null);
    }

}
