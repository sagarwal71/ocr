package au.com.target.tgtcore.ordercancel.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.ordercancel.data.IpgNewRefundInfoDTO;
import au.com.target.tgtcore.ordercancel.data.PaymentTransactionDTO;
import au.com.target.tgtcore.ordercancel.data.PaymentTransactionEntryDTO;
import au.com.target.tgtcore.ordercancel.data.RefundInfoDTO;
import au.com.target.tgtpayment.exceptions.PaymentException;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.service.TargetPaymentService;


/**
 * Unit test for {@link TargetRefundPaymentServiceImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetRefundPaymentServiceImplTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @InjectMocks
    @Spy
    private final TargetRefundPaymentServiceImpl targetRefundPaymentService = new TargetRefundPaymentServiceImpl();

    @Mock
    private TargetPaymentService targetPaymentService;

    @Mock
    private KeyGenerator vpcCaptureCodeGenerator;

    @Mock
    private OrderModel order;

    @Mock
    private PaymentTransactionEntryModel refundTransactionEntryModel;

    @Mock
    private PaymentTransactionEntryDTO refundTransactionEntryDTO;

    @Mock
    private PaymentTransactionModel standaloneTransactionModel;

    @Mock
    private PaymentTransactionEntryModel standaloneCaptureTransactionEntryModel;

    @Mock
    private PaymentTransactionEntryModel standaloneRefundTransactionEntryModel;

    @Mock
    private PaymentInfoModel newPayment;

    @Mock
    private PaymentTransactionDTO standaloneTransactionDTO;

    @Mock
    private PaymentTransactionEntryDTO standaloneCaptureTransactionEntryDTO;

    @Mock
    private PaymentTransactionEntryDTO standaloneRefundTransactionEntryDTO;

    @Mock
    private ModelService modelService;

    @Before
    public void setup() {
        targetRefundPaymentService.setTargetPaymentService(targetPaymentService);
        targetRefundPaymentService.setTargetPaymentService(targetPaymentService);

        targetRefundPaymentService.setVpcCaptureCodeGenerator(vpcCaptureCodeGenerator);
        targetRefundPaymentService.setVpcCaptureCodeGenerator(vpcCaptureCodeGenerator);


    }

    @Test
    public void testFollowOnRefundWithNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Order cannot be null");
        targetRefundPaymentService.performFollowOnRefund(null, BigDecimal.ZERO);
    }

    @Test
    public void testFollowOnRefundWithNoCapture() {
        expectedException.expect(TargetOrderCancelException.class);
        expectedException.expectMessage("Cannot find a successful capture transaction against the order:");

        targetRefundPaymentService.performFollowOnRefund(order, BigDecimal.ZERO);
    }

    @Test
    public void testRefundFollowOnSuccess() {
        final PaymentTransactionEntryModel refundEntry1 = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionEntryModel refundEntry2 = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel captureTransaction = setupCaptureTransaction();
        final List<PaymentTransactionEntryModel> refundList = new ArrayList<PaymentTransactionEntryModel>(
                Arrays.asList(refundEntry1, refundEntry2));
        given(targetPaymentService.refundFollowOn(captureTransaction, BigDecimal.TEN)).willReturn(refundList);

        final List<PaymentTransactionEntryModel> pteDataList = targetRefundPaymentService.performFollowOnRefund(order,
                BigDecimal.TEN);

        assertThat(pteDataList).containsExactly(refundEntry1, refundEntry2);
    }

    @Test
    public void testConvertPaymentTransactionEntries() {
        final PaymentTransactionEntryModel refundEntry1 = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionEntryModel refundEntry2 = mock(PaymentTransactionEntryModel.class);
        final List<PaymentTransactionEntryModel> refundList = new ArrayList<PaymentTransactionEntryModel>(
                Arrays.asList(refundEntry1, refundEntry2));

        final PaymentTransactionEntryDTO entryDto1 = mock(PaymentTransactionEntryDTO.class);
        final PaymentTransactionEntryDTO entryDto2 = mock(PaymentTransactionEntryDTO.class);

        willReturn(entryDto1).given(targetRefundPaymentService).convertPaymentTransactionEntry(refundEntry1);
        willReturn(entryDto2).given(targetRefundPaymentService).convertPaymentTransactionEntry(refundEntry2);

        final List<PaymentTransactionEntryDTO> result = targetRefundPaymentService
                .convertPaymentTransactionEntries(refundList);

        assertThat(result).containsOnly(entryDto1, entryDto2);
    }

    private PaymentTransactionModel setupCaptureTransaction() {
        final PaymentTransactionEntryModel paymentTransactionEntry = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        given(paymentTransaction.getIsCaptureSuccessful()).willReturn(null);
        given(paymentTransaction.getEntries())
                .willReturn(Collections.singletonList(paymentTransactionEntry));
        given(paymentTransactionEntry.getType()).willReturn(PaymentTransactionType.CAPTURE);
        given(paymentTransactionEntry.getTransactionStatus()).willReturn(
                TransactionStatus.ACCEPTED.toString());
        given(order.getPaymentTransactions()).willReturn(Collections.singletonList(paymentTransaction));
        given(paymentTransactionEntry.getPaymentTransaction()).willReturn(paymentTransaction);
        return paymentTransaction;
    }


    @Test
    public void testStandaloneRefundNullPaymentTransaction() {
        expectedException.expect(PaymentException.class);

        setupRefundStandalone();

        given(standaloneCaptureTransactionEntryModel.getPaymentTransaction()).willReturn(null);

        targetRefundPaymentService.performStandaloneRefund(order, BigDecimal.TEN, newPayment);
    }


    @Test
    public void testStandaloneRefundFailedCapture() {

        setupRefundStandalone();

        given(standaloneCaptureTransactionEntryModel.getTransactionStatus()).willReturn(
                TransactionStatus.REVIEW.toString());

        final PaymentTransactionDTO ptData = targetRefundPaymentService.performStandaloneRefund(order, BigDecimal.TEN,
                newPayment);

        verify(standaloneTransactionDTO).getEntries();

        assertThat(ptData).isEqualTo(standaloneTransactionDTO);
    }

    @Test
    public void testStandaloneRefundAcceptedCapture() {

        setupRefundStandalone();

        given(standaloneCaptureTransactionEntryModel.getTransactionStatus()).willReturn(
                TransactionStatus.ACCEPTED.toString());

        final PaymentTransactionDTO ptData = targetRefundPaymentService.performStandaloneRefund(order, BigDecimal.TEN,
                newPayment);

        verify(standaloneTransactionDTO, times(2)).getEntries();

        assertThat(ptData).isEqualTo(standaloneTransactionDTO);
    }

    private void setupRefundStandalone() {

        final String captureOrderId = "123";

        given(vpcCaptureCodeGenerator.generate()).willReturn(captureOrderId);

        // Payment service responses
        given(targetPaymentService.refundStandaloneCapture(order, newPayment, captureOrderId)).willReturn(
                standaloneCaptureTransactionEntryModel);
        given(
                targetPaymentService.refundStandaloneRefund(order, standaloneCaptureTransactionEntryModel,
                        BigDecimal.TEN, captureOrderId))
                                .willReturn(
                                        standaloneRefundTransactionEntryModel);

        // Conversions
        willReturn(standaloneTransactionDTO).given(targetRefundPaymentService)
                .convertPaymentTransaction(standaloneTransactionModel);
        willReturn(standaloneCaptureTransactionEntryDTO).given(targetRefundPaymentService)
                .convertPaymentTransactionEntry(standaloneCaptureTransactionEntryModel);
        willReturn(standaloneRefundTransactionEntryDTO).given(targetRefundPaymentService)
                .convertPaymentTransactionEntry(standaloneRefundTransactionEntryModel);

        // Entry to payment transaction references
        given(standaloneCaptureTransactionEntryModel.getPaymentTransaction()).willReturn(
                standaloneTransactionModel);
        given(standaloneRefundTransactionEntryModel.getPaymentTransaction()).willReturn(
                standaloneTransactionModel);



    }

    private PaymentTransactionEntryDTO setUpPaymentTransactionEntryDTO(final BigDecimal amount,
            final PaymentTransactionType transactionType, final TransactionStatus transactionStatus) {
        final PaymentTransactionEntryDTO paymentTransactionEntryDTO = mock(PaymentTransactionEntryDTO.class);
        given(paymentTransactionEntryDTO.getType()).willReturn(transactionType);
        given(paymentTransactionEntryDTO.getTransactionStatus()).willReturn(transactionStatus.toString());
        given(paymentTransactionEntryDTO.getAmount()).willReturn(amount);
        return paymentTransactionEntryDTO;
    }


    @Test
    public void testFindRefundedAmountAfterRefundFollowOn() {
        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);
        final BigDecimal amount2 = BigDecimal.valueOf(11.11d).setScale(2);
        final BigDecimal amount3 = BigDecimal.valueOf(41.00d).setScale(2);
        final BigDecimal amount4 = BigDecimal.valueOf(99.99d).setScale(2);
        final BigDecimal amount5 = BigDecimal.valueOf(17.97d).setScale(2);

        final PaymentTransactionEntryDTO paymentTransactionRefund1 = setUpPaymentTransactionEntryDTO(amount1,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.ACCEPTED);
        final PaymentTransactionEntryDTO paymentTransactionRefund2 = setUpPaymentTransactionEntryDTO(
                amount2, PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.ACCEPTED);
        final PaymentTransactionEntryDTO paymentTransactionRefund3 = setUpPaymentTransactionEntryDTO(
                amount3, PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.ACCEPTED);
        final PaymentTransactionEntryDTO paymentTransactionRefund4 = setUpPaymentTransactionEntryDTO(
                amount4, PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.REJECTED);

        final PaymentTransactionEntryDTO paymentTransactionRefund5 = setUpPaymentTransactionEntryDTO(
                amount5, PaymentTransactionType.REFUND_STANDALONE, TransactionStatus.ACCEPTED);

        final List<PaymentTransactionEntryDTO> refundedEntryDTOs = new ArrayList<PaymentTransactionEntryDTO>(
                Arrays.asList(paymentTransactionRefund1, paymentTransactionRefund2, paymentTransactionRefund3,
                        paymentTransactionRefund4, paymentTransactionRefund5));
        final BigDecimal resultAmount = targetRefundPaymentService.findRefundedAmountAfterRefund(refundedEntryDTOs);
        assertThat(resultAmount).isEqualTo(amount1.add(amount2).add(amount3).add(amount5));
    }

    @Test
    public void testCreateRefundedInfoWithFullRefundSuccess() {
        final BigDecimal refundedAmount = BigDecimal.valueOf(12.34d).setScale(2);
        final BigDecimal totalRefundAmount = BigDecimal.valueOf(12.34d).setScale(2);
        final PaymentTransactionEntryDTO paymentTransactionRefund1 = setUpPaymentTransactionEntryDTO(refundedAmount,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.ACCEPTED);

        final List<PaymentTransactionEntryDTO> refundedEntryDTOs = new ArrayList<PaymentTransactionEntryDTO>(
                Arrays.asList(paymentTransactionRefund1));
        final RefundInfoDTO result = targetRefundPaymentService.createRefundedInfo(refundedEntryDTOs,
                totalRefundAmount);
        assertThat(result.getMessage()).isNull();
    }

    @Test
    public void testCreateRefundedInfoWithFullRefundFailure() {
        final BigDecimal totalRefundAmount = BigDecimal.valueOf(12.34d).setScale(2);
        final List<PaymentTransactionEntryDTO> refundedEntryDTOs = new ArrayList<>();
        final RefundInfoDTO result = targetRefundPaymentService.createRefundedInfo(refundedEntryDTOs,
                totalRefundAmount);
        assertThat(result.getMessage())
                .isEqualTo("System failed to refund automatically, refund amount=12.34 need to be refunded manually");
    }

    @Test
    public void testCreateRefundedInfoWithPartialRefundFailure() {
        final BigDecimal refundedAmount = BigDecimal.valueOf(11.30d).setScale(2);
        final BigDecimal totalRefundAmount = BigDecimal.valueOf(12.34d).setScale(2);
        final PaymentTransactionEntryDTO paymentTransactionRefund1 = setUpPaymentTransactionEntryDTO(refundedAmount,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.ACCEPTED);

        final List<PaymentTransactionEntryDTO> refundedEntryDTOs = new ArrayList<PaymentTransactionEntryDTO>(
                Arrays.asList(paymentTransactionRefund1));
        final RefundInfoDTO result = targetRefundPaymentService.createRefundedInfo(refundedEntryDTOs,
                totalRefundAmount);
        assertThat(result.getMessage()).isEqualTo(MessageFormat.format(
                "Total amount need to be refunded={0}. System has already succeeded to refund amount={1} automatically, another amount={2} need to be refunded manually",
                totalRefundAmount, refundedAmount, totalRefundAmount.subtract(refundedAmount)));
    }

    @Test
    public void testCreateFollowOnRefundPaymentTransactionEntries() {
        final PaymentTransactionModel transaction = mock(PaymentTransactionModel.class);
        given(transaction.getIsCaptureSuccessful()).willReturn(Boolean.TRUE);
        final List<PaymentTransactionModel> paymentTransactionList = ImmutableList.of(transaction);
        final OrderModel orderModel = mock(OrderModel.class);
        given(orderModel.getPaymentTransactions()).willReturn(paymentTransactionList);
        final IpgPaymentInfoModel ipgPaymentInfoModel = mock(IpgPaymentInfoModel.class);
        final CurrencyModel currency = mock(CurrencyModel.class);
        given(orderModel.getPaymentInfo()).willReturn(ipgPaymentInfoModel);
        given(orderModel.getCurrency()).willReturn(currency);

        final PaymentTransactionEntryDTO dto1 = mock(PaymentTransactionEntryDTO.class);
        final PaymentTransactionEntryDTO dto2 = mock(PaymentTransactionEntryDTO.class);
        final List<PaymentTransactionEntryDTO> dataList = new ArrayList<PaymentTransactionEntryDTO>(Arrays.asList(dto1,
                dto2));
        willDoNothing().given(targetRefundPaymentService).createPaymentTransactionEntry(
                Mockito.any(PaymentTransactionEntryDTO.class),
                Mockito.any(PaymentTransactionModel.class), Mockito.any(CurrencyModel.class));
        targetRefundPaymentService.createFollowOnRefundPaymentTransactionEntries(orderModel, dataList);
        verify(targetRefundPaymentService).createPaymentTransactionEntry(dto1, transaction, currency);
        verify(targetRefundPaymentService).createPaymentTransactionEntry(dto2, transaction, currency);
    }

    @Test
    public void testPerformIpgManualRefund() {

        final BigDecimal refundAmount = BigDecimal.valueOf(52.14d).setScale(2);
        final String receiptNo = "123456";
        final IpgNewRefundInfoDTO refundInfoDTO = new IpgNewRefundInfoDTO();
        refundInfoDTO.setAmount(Double.valueOf(refundAmount.doubleValue()));
        refundInfoDTO.setReceiptNo(receiptNo);
        final PaymentTransactionModel captureTransaction = mock(PaymentTransactionModel.class);
        final OrderModel orderModel = mock(OrderModel.class);
        final PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);
        willReturn(captureTransaction).given(targetRefundPaymentService).findCaptureTransaction(orderModel);
        given(targetPaymentService.refundIpgManualRefund(captureTransaction, refundAmount, receiptNo))
                .willReturn(entry);
        final List<PaymentTransactionEntryModel> transactionEntryList = targetRefundPaymentService
                .performIpgManualRefund(orderModel,
                        refundAmount, refundInfoDTO);
        assertThat(transactionEntryList.get(0)).isEqualTo(entry);
    }

    @Test
    public void testPerformIpgManualRefundWithNullRefundInfoDTO() {

        final BigDecimal refundAmount = BigDecimal.valueOf(52.14d).setScale(2);
        final String receiptNo = "123456";
        final IpgNewRefundInfoDTO refundInfoDTO = null;
        final PaymentTransactionModel captureTransaction = mock(PaymentTransactionModel.class);
        final OrderModel orderModel = mock(OrderModel.class);
        final PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);
        willReturn(captureTransaction).given(targetRefundPaymentService).findCaptureTransaction(orderModel);
        given(targetPaymentService.refundIpgManualRefund(captureTransaction, refundAmount, receiptNo))
                .willReturn(entry);
        final List<PaymentTransactionEntryModel> transactionEntryDTOList = targetRefundPaymentService
                .performIpgManualRefund(orderModel,
                        refundAmount, refundInfoDTO);
        assertThat(transactionEntryDTOList.size()).isEqualTo(0);
    }

    @Test
    public void testPerformIpgManualRefundWithMismatchRefundAmount() {

        final BigDecimal refundAmount = BigDecimal.valueOf(52.14d).setScale(2);
        final String receiptNo = "123456";
        final IpgNewRefundInfoDTO refundInfoDTO = new IpgNewRefundInfoDTO();
        refundInfoDTO.setAmount(Double.valueOf(51.14d));
        refundInfoDTO.setReceiptNo(receiptNo);
        final PaymentTransactionModel captureTransaction = mock(PaymentTransactionModel.class);
        final OrderModel orderModel = mock(OrderModel.class);
        final PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionEntryDTO entryDto = mock(PaymentTransactionEntryDTO.class);
        willReturn(captureTransaction).given(targetRefundPaymentService).findCaptureTransaction(orderModel);
        given(targetPaymentService.refundIpgManualRefund(captureTransaction, refundAmount, receiptNo))
                .willReturn(entry);
        willReturn(entryDto).given(targetRefundPaymentService).convertPaymentTransactionEntry(entry);
        try {
            targetRefundPaymentService.performIpgManualRefund(orderModel, refundAmount, refundInfoDTO);
            Assert.assertFalse("expected paymentException", true);
        }
        catch (final PaymentException e) {
            Assert.assertTrue("expected paymentException", true);
        }
    }

    @Test
    public void testPerformIpgManualRefundForAfterpay() {
        final BigDecimal refundAmount = BigDecimal.valueOf(52.14d).setScale(2);
        final String receiptNo = "123456";
        final IpgNewRefundInfoDTO refundInfoDTO = new IpgNewRefundInfoDTO();
        refundInfoDTO.setAmount(Double.valueOf(refundAmount.doubleValue()));
        refundInfoDTO.setReceiptNo(receiptNo);
        final PaymentTransactionModel captureTransaction = mock(PaymentTransactionModel.class);
        final AfterpayPaymentInfoModel afterpayPaymentInfoModel = mock(AfterpayPaymentInfoModel.class);
        given(captureTransaction.getInfo()).willReturn(afterpayPaymentInfoModel);
        final OrderModel orderModel = mock(OrderModel.class);
        final PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);
        willReturn(captureTransaction).given(targetRefundPaymentService).findCaptureTransaction(orderModel);
        given(targetPaymentService.refundIpgManualRefund(captureTransaction, refundAmount, receiptNo))
                .willReturn(entry);

        final List<PaymentTransactionEntryModel> transactionEntryList = targetRefundPaymentService
                .performIpgManualRefund(orderModel, refundAmount, refundInfoDTO);
        assertThat(transactionEntryList.get(0)).isEqualTo(entry);
        verify(afterpayPaymentInfoModel).setAfterpayRefundId(receiptNo);
        verify(modelService).save(afterpayPaymentInfoModel);
    }
}
