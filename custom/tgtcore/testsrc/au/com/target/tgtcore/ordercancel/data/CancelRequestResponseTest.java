/**
 * 
 */
package au.com.target.tgtcore.ordercancel.data;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.dto.TransactionStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import org.junit.Assert;


/**
 * @author bhuang3
 *
 */
@UnitTest
public class CancelRequestResponseTest {

    @Test
    public void testGetRefundFailureMessageWithAcceptedTransactionEntry() {
        final PaymentTransactionEntryDTO dto1 = new PaymentTransactionEntryDTO();
        dto1.setTransactionStatus(TransactionStatus.ACCEPTED.toString());
        final PaymentTransactionEntryDTO dto2 = new PaymentTransactionEntryDTO();
        dto2.setTransactionStatus(TransactionStatus.REJECTED.toString());
        final List<PaymentTransactionEntryDTO> followOnRefundPaymentDataList = new ArrayList<PaymentTransactionEntryDTO>(
                Arrays.asList(dto1, dto2));
        final CancelRequestResponse response = new CancelRequestResponse();
        response.setFollowOnRefundPaymentDataList(followOnRefundPaymentDataList);
        final String message = response.getRefundFailureMessage();
        Assert.assertNull(message);
    }

    @Test
    public void testGetRefundFailureMessageWithoutAcceptedTransactionEntry() {
        final PaymentTransactionEntryDTO dto1 = new PaymentTransactionEntryDTO();
        dto1.setTransactionStatus(TransactionStatus.REJECTED.toString());
        final PaymentTransactionEntryDTO dto2 = new PaymentTransactionEntryDTO();
        dto2.setTransactionStatus(TransactionStatus.REJECTED.toString());
        final List<PaymentTransactionEntryDTO> followOnRefundPaymentDataList = new ArrayList<PaymentTransactionEntryDTO>(
                Arrays.asList(dto1, dto2));
        final CancelRequestResponse response = new CancelRequestResponse();
        response.setFollowOnRefundPaymentDataList(followOnRefundPaymentDataList);
        final String message = response.getRefundFailureMessage();
        Assert.assertNotNull(message);
    }
}
