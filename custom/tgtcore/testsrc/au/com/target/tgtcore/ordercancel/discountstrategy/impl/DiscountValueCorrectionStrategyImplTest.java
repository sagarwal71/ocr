package au.com.target.tgtcore.ordercancel.discountstrategy.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.AbstractPromotionActionModel;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.fest.assertions.Delta;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TMDiscountProductPromotionModel;
import au.com.target.tgtcore.model.TMDiscountPromotionModel;
import au.com.target.tgtcore.ordercancel.discountstrategy.PromotionResultCorrectionStrategy;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DiscountValueCorrectionStrategyImplTest {

    private static final String TMD_ACTION_GUID = "TMDiscountGUID";
    private static final String DEAL_ACTION_GUID = "DealGUID";
    private static final BigDecimal NEW_TMD_PERCENT = BigDecimal.valueOf(15.0);

    @InjectMocks
    private final DiscountValueCorrectionStrategyImpl discountValueCorrectionStrategy = new DiscountValueCorrectionStrategyImpl();

    @Mock
    private PromotionResultCorrectionStrategy mockPromotionResultCorrectionStrategy;

    @Mock
    private OrderEntryModel mockOrderEntry;

    @Mock
    private OrderModel mockOrder;

    @Captor
    private ArgumentCaptor<List<DiscountValue>> discountValuesCaptor;

    @Before
    public void setUp() {
        given(mockOrderEntry.getOrder()).willReturn(mockOrder);
    }

    @Test
    public void testUnitPriceAfterDealsNullBasePrice() {

        final DiscountValue discountValue = createDiscountValue(DEAL_ACTION_GUID, 1.0);
        final List<DiscountValue> discountValues = createDiscountValueList(discountValue);
        given(mockOrderEntry.getBasePrice()).willReturn(null);

        final double unitPrice = discountValueCorrectionStrategy.calculateUnitPriceAfterDeals(mockOrderEntry,
                discountValues);

        assertThat(unitPrice).isEqualTo(0);
    }

    @Test
    public void testUnitPriceAfterDealsNoDiscountValues() {

        given(mockOrderEntry.getBasePrice()).willReturn(Double.valueOf(10.0));

        final double unitPrice = discountValueCorrectionStrategy.calculateUnitPriceAfterDeals(mockOrderEntry,
                Collections.EMPTY_LIST);

        assertThat(unitPrice).isEqualTo(10.0);
    }

    @Test
    public void testUnitPriceAfterDealsWithDiscountValues() {

        final DiscountValue discountValue = createDiscountValue(DEAL_ACTION_GUID, 1.0);
        final List<DiscountValue> discountValues = createDiscountValueList(discountValue);
        given(mockOrderEntry.getBasePrice()).willReturn(Double.valueOf(10.0));

        final double unitPrice = discountValueCorrectionStrategy.calculateUnitPriceAfterDeals(mockOrderEntry,
                discountValues);

        assertThat(unitPrice).isEqualTo(9.0);
    }

    @Test
    public void testUpdateDealDiscountValuesEmptyList() {

        final List<DiscountValue> discountValues = new ArrayList<>();
        final List<DiscountValue> updatedDiscountValues = discountValueCorrectionStrategy.updateDealDiscountValues(
                discountValues, 3, 6);
        assertThat(updatedDiscountValues).isEmpty();
    }

    @Test
    public void testUpdateDealDiscountValues() {

        final DiscountValue discountValue = createDiscountValue(DEAL_ACTION_GUID, 2.5);
        final List<DiscountValue> discountValues = createDiscountValueList(discountValue);
        final List<DiscountValue> updatedDiscountValues = discountValueCorrectionStrategy.updateDealDiscountValues(
                discountValues, 3, 6);
        assertThat(updatedDiscountValues).isNotEmpty();
        assertThat(updatedDiscountValues).hasSize(1);
        final DiscountValue newValue = updatedDiscountValues.get(0);
        assertThat(newValue.getValue()).isEqualTo(2.5 * 6 / 3);
        assertThat(newValue.getCode()).isEqualTo(discountValue.getCode());
        assertThat(newValue.getCurrencyIsoCode()).isEqualTo(discountValue.getCurrencyIsoCode());
    }

    @Test
    public void testPullOutTMDDiscountValuesWithOldTmd() {

        // Given old tmd results 
        final PromotionResultModel oldTmdResult1 = setupOldTMDPromotionResult("oldTmd1");
        final PromotionResultModel oldTmdResult2 = setupOldTMDPromotionResult("oldTmd2");

        final DiscountValue discountValue1 = createDiscountValue("oldTmd1", 1.0);
        final DiscountValue discountValue2 = createDiscountValue("oldTmd2", 1.0);
        final List<DiscountValue> discountValues = createDiscountValueList(discountValue1, discountValue2);

        // When call pullOutTMDDiscountValues 
        final List<DiscountValue> oldTmdDiscountValues = new ArrayList<>();
        final List<DiscountValue> newTmdDiscountValues = new ArrayList<>();
        final BigDecimal rate = discountValueCorrectionStrategy.pullOutTMDDiscountValues(
                createPromotionResultsSet(oldTmdResult1, oldTmdResult2),
                discountValues, oldTmdDiscountValues, newTmdDiscountValues);

        // Expect all discount values to be pulled into oldTmdDiscountValues
        assertThat(rate).isNull();
        assertThat(discountValues).isEmpty();
        assertThat(newTmdDiscountValues).isEmpty();
        assertThat(oldTmdDiscountValues).isNotEmpty();
        assertThat(oldTmdDiscountValues).hasSize(2);
        assertThat(oldTmdDiscountValues).contains(discountValue1);
        assertThat(oldTmdDiscountValues).contains(discountValue2);
    }

    @Test
    public void testPullOutTMDDiscountValuesWithNewTmd() {

        // Given new tmd results 
        final PromotionResultModel newTmdResult1 = setupNewTMDPromotionResult("newTmd1");
        final PromotionResultModel newTmdResult2 = setupNewTMDPromotionResult("newTmd2");

        final DiscountValue discountValue1 = createDiscountValue("newTmd1", 1.0);
        final DiscountValue discountValue2 = createDiscountValue("newTmd2", 1.0);
        final List<DiscountValue> discountValues = createDiscountValueList(discountValue1, discountValue2);

        // When call pullOutTMDDiscountValues 
        final List<DiscountValue> oldTmdDiscountValues = new ArrayList<>();
        final List<DiscountValue> newTmdDiscountValues = new ArrayList<>();
        final BigDecimal rate = discountValueCorrectionStrategy.pullOutTMDDiscountValues(
                createPromotionResultsSet(newTmdResult1, newTmdResult2),
                discountValues, oldTmdDiscountValues, newTmdDiscountValues);

        // Expect all discount values to be pulled into newTmdDiscountValues, and rate returned
        assertThat(rate).isNotNull();
        assertThat(rate.doubleValue()).isEqualTo(NEW_TMD_PERCENT.doubleValue() / 100);
        assertThat(discountValues).isEmpty();
        assertThat(oldTmdDiscountValues).isEmpty();
        assertThat(newTmdDiscountValues).isNotEmpty();
        assertThat(newTmdDiscountValues).hasSize(2);
        assertThat(newTmdDiscountValues).contains(discountValue1);
        assertThat(newTmdDiscountValues).contains(discountValue2);

    }

    @Test
    public void testPullOutTMDDiscountValuesWithDeals() {

        // Given deal results 
        final PromotionResultModel result1 = setupDealPromotionResult("deal1");
        final PromotionResultModel result2 = setupDealPromotionResult("deal2");

        final DiscountValue discountValue1 = createDiscountValue("deal1", 1.0);
        final DiscountValue discountValue2 = createDiscountValue("deal2", 1.0);
        final List<DiscountValue> discountValues = createDiscountValueList(discountValue1, discountValue2);

        // When call pullOutTMDDiscountValues 
        final List<DiscountValue> oldTmdDiscountValues = new ArrayList<>();
        final List<DiscountValue> newTmdDiscountValues = new ArrayList<>();
        final BigDecimal rate = discountValueCorrectionStrategy.pullOutTMDDiscountValues(
                createPromotionResultsSet(result1, result2),
                discountValues, oldTmdDiscountValues, newTmdDiscountValues);

        // Expect no discount values to be pulled 
        assertThat(rate).isNull();
        assertThat(oldTmdDiscountValues).isEmpty();
        assertThat(newTmdDiscountValues).isEmpty();
        assertThat(discountValues).isNotEmpty();
        assertThat(discountValues).hasSize(2);
        assertThat(discountValues).contains(discountValue1);
        assertThat(discountValues).contains(discountValue2);
    }

    @Test
    public void testPullOutTMDDiscountValuesWithDealsAndNewTmd() {

        // Given deal and tmd results 
        final PromotionResultModel result1 = setupDealPromotionResult("deal1");
        final PromotionResultModel newTmdResult2 = setupNewTMDPromotionResult("newTmd2");

        final DiscountValue discountValue1 = createDiscountValue("deal1", 1.0);
        final DiscountValue discountValue2 = createDiscountValue("newTmd2", 1.0);
        final List<DiscountValue> discountValues = createDiscountValueList(discountValue1, discountValue2);

        // When call pullOutTMDDiscountValues 
        final List<DiscountValue> oldTmdDiscountValues = new ArrayList<>();
        final List<DiscountValue> newTmdDiscountValues = new ArrayList<>();
        final BigDecimal rate = discountValueCorrectionStrategy.pullOutTMDDiscountValues(
                createPromotionResultsSet(result1, newTmdResult2),
                discountValues, oldTmdDiscountValues, newTmdDiscountValues);

        // Expect no discount values to be pulled 
        assertThat(rate).isNotNull();
        assertThat(rate.doubleValue()).isEqualTo(NEW_TMD_PERCENT.doubleValue() / 100);
        assertThat(oldTmdDiscountValues).isEmpty();
        assertThat(newTmdDiscountValues).isNotEmpty();
        assertThat(discountValues).isNotEmpty();
        assertThat(discountValues).hasSize(1);
        assertThat(newTmdDiscountValues).hasSize(1);
        assertThat(discountValues).contains(discountValue1);
        assertThat(newTmdDiscountValues).contains(discountValue2);
    }

    @Test
    public void testCorrectDiscountValuesWithNoDiscountValues() {
        discountValueCorrectionStrategy.correctDiscountValues(mockOrderEntry, 7);

        verify(mockOrderEntry).getDiscountValues();
        verifyNoMoreInteractions(mockOrderEntry);
    }

    @Test
    public void testCorrectDiscountValuesDealsOnlyWithLessDiscountValuesThanQuantity() {

        // Given one deal result and single discount value 
        final double discountAmount = 2.0 / 3.0;
        setPromotionResultsInOrder(setupDealPromotionResult(DEAL_ACTION_GUID));
        final DiscountValue discountValue = createDiscountValue(DEAL_ACTION_GUID, discountAmount);
        final List<DiscountValue> discountValues = createDiscountValueList(discountValue);

        // And new qty 2
        given(mockOrderEntry.getDiscountValues()).willReturn(discountValues);
        given(mockOrderEntry.getQuantity()).willReturn(Long.valueOf(2));

        // When call strategy with prev qty 3
        discountValueCorrectionStrategy.correctDiscountValues(mockOrderEntry, 3);

        // Then a discount value remains but with updated value
        verify(mockOrderEntry).setDiscountValues(discountValuesCaptor.capture());

        final List<DiscountValue> newDiscountValues = discountValuesCaptor.getValue();
        assertThat(newDiscountValues).hasSize(1);

        final DiscountValue newDiscountValue = newDiscountValues.get(0);
        assertThat(newDiscountValue.getCode()).isEqualTo(discountValue.getCode());
        assertThat(newDiscountValue.getCurrencyIsoCode()).isEqualTo(discountValue.getCurrencyIsoCode());

        assertThat(newDiscountValue.getValue()).isEqualTo((discountAmount * 3) / 2);

        verifyZeroInteractions(mockPromotionResultCorrectionStrategy);
    }

    @Test
    public void testCorrectDiscountValuesDealsOnlyWithEqualDiscountValuesAndQuantity() {

        // Note the actions don't matter in the case of deals only since they are only used for filtering out TMD

        // Given deal result and two discount values
        setPromotionResultsInOrder(setupDealPromotionResult(DEAL_ACTION_GUID));

        final double discountAmount = (2.0 * 2.0) / 3.0;
        final DiscountValue discountValue1 = createDiscountValue("12345", discountAmount);
        final DiscountValue discountValue2 = createDiscountValue("12346", discountAmount);
        final List<DiscountValue> discountValues = createDiscountValueList(discountValue1, discountValue2);

        // And new qty is 2
        given(mockOrderEntry.getDiscountValues()).willReturn(discountValues);
        given(mockOrderEntry.getQuantity()).willReturn(Long.valueOf(2));

        // When call strategy with prev qty 3
        discountValueCorrectionStrategy.correctDiscountValues(mockOrderEntry, 3);

        // Then expect both entries to have values updated
        verify(mockOrderEntry).setDiscountValues(discountValuesCaptor.capture());

        final List<DiscountValue> newDiscountValues = discountValuesCaptor.getValue();
        assertThat(newDiscountValues).hasSize(2);

        final DiscountValue newDiscountValue1 = newDiscountValues.get(0);
        assertThat(newDiscountValue1.getCode()).isEqualTo(discountValue1.getCode());
        assertThat(newDiscountValue1.getCurrencyIsoCode()).isEqualTo(discountValue1.getCurrencyIsoCode());

        assertThat(newDiscountValue1.getValue()).isEqualTo((discountAmount * 3) / 2);


        final DiscountValue newDiscountValue2 = newDiscountValues.get(1);
        assertThat(newDiscountValue2.getCode()).isEqualTo(discountValue2.getCode());
        assertThat(newDiscountValue2.getCurrencyIsoCode()).isEqualTo(discountValue2.getCurrencyIsoCode());

        assertThat(newDiscountValue2.getValue()).isEqualTo((discountAmount * 3) / 2);

        verifyZeroInteractions(mockPromotionResultCorrectionStrategy);
    }

    @Test
    public void testCorrectDiscountValuesDealsOnlyWithMoreDiscountValuesThanQuantity() {

        // Given deal result and three discount values
        setPromotionResultsInOrder(setupDealPromotionResult(DEAL_ACTION_GUID));

        final double discountAmount = 2.0;
        final DiscountValue discountValue1 = createDiscountValue("12345", discountAmount);
        final DiscountValue discountValue2 = createDiscountValue("12346", discountAmount);
        final DiscountValue discountValue3 = createDiscountValue("12347", discountAmount);
        final List<DiscountValue> discountValues = createDiscountValueList(discountValue1,
                discountValue2, discountValue3);

        // And new qty 2
        given(mockOrderEntry.getDiscountValues()).willReturn(discountValues);
        given(mockOrderEntry.getQuantity()).willReturn(Long.valueOf(2));

        // When orig qty was 3
        discountValueCorrectionStrategy.correctDiscountValues(mockOrderEntry, 3);

        // Expect one dv and one promo result to be removed
        verify(mockOrderEntry).setDiscountValues(discountValuesCaptor.capture());

        final List<DiscountValue> newDiscountValues = discountValuesCaptor.getValue();
        assertThat(newDiscountValues).hasSize(2);

        final DiscountValue newDiscountValue1 = newDiscountValues.get(0);
        assertThat(newDiscountValue1.getCode()).isEqualTo(discountValue2.getCode());
        assertThat(newDiscountValue1.getCurrencyIsoCode()).isEqualTo(discountValue2.getCurrencyIsoCode());

        assertThat(newDiscountValue1.getValue()).isEqualTo((discountAmount * 3) / 2);


        final DiscountValue newDiscountValue2 = newDiscountValues.get(1);
        assertThat(newDiscountValue2.getCode()).isEqualTo(discountValue3.getCode());
        assertThat(newDiscountValue2.getCurrencyIsoCode()).isEqualTo(discountValue3.getCurrencyIsoCode());

        assertThat(newDiscountValue2.getValue()).isEqualTo((discountAmount * 3) / 2);

        verify(mockPromotionResultCorrectionStrategy).correctPromotionResultRecords(discountValue1.getCode(),
                mockOrderEntry, 0, 0, true);
        verifyNoMoreInteractions(mockPromotionResultCorrectionStrategy);
    }

    @Test
    public void testCorrectDiscountValuesWithMoreDiscountValuesThanQuantityAndDifferentDiscountAmounts() {

        // Given deal result and three discount values
        setPromotionResultsInOrder(setupDealPromotionResult(DEAL_ACTION_GUID));

        final DiscountValue discountValue1 = createDiscountValue("12345", 4.0 / 6.0);
        final DiscountValue discountValue2 = createDiscountValue("12346", 3.0 / 6.0);
        final DiscountValue discountValue3 = createDiscountValue("12347", 8.0 / 6.0);
        final DiscountValue discountValue4 = createDiscountValue("12348", 1.0 / 6.0);
        final DiscountValue discountValue5 = createDiscountValue("12349", 9.0 / 6.0);
        final DiscountValue discountValue6 = createDiscountValue("12350", 2.0 / 6.0);
        final List<DiscountValue> discountValues = createDiscountValueList(discountValue1,
                discountValue2, discountValue3, discountValue4, discountValue5, discountValue6);

        // And new qty is 3      
        given(mockOrderEntry.getDiscountValues()).willReturn(discountValues);
        given(mockOrderEntry.getQuantity()).willReturn(Long.valueOf(3));

        // When prev qty was 6
        discountValueCorrectionStrategy.correctDiscountValues(mockOrderEntry, 6);

        // Then expect 3 left and 3 promo results get removed
        verify(mockOrderEntry).setDiscountValues(discountValuesCaptor.capture());

        final List<DiscountValue> newDiscountValues = discountValuesCaptor.getValue();
        assertThat(newDiscountValues).hasSize(3);

        final DiscountValue newDiscountValue1 = newDiscountValues.get(0);
        assertThat(newDiscountValue1.getCode()).isEqualTo(discountValue1.getCode());
        assertThat(newDiscountValue1.getCurrencyIsoCode()).isEqualTo(discountValue1.getCurrencyIsoCode());
        assertThat(newDiscountValue1.getValue()).isEqualTo((discountValue1.getValue() * 6) / 3);

        final DiscountValue newDiscountValue2 = newDiscountValues.get(1);
        assertThat(newDiscountValue2.getCode()).isEqualTo(discountValue3.getCode());
        assertThat(newDiscountValue2.getCurrencyIsoCode()).isEqualTo(discountValue3.getCurrencyIsoCode());
        assertThat(newDiscountValue2.getValue()).isEqualTo((discountValue3.getValue() * 6) / 3);

        final DiscountValue newDiscountValue3 = newDiscountValues.get(2);
        assertThat(newDiscountValue3.getCode()).isEqualTo(discountValue5.getCode());
        assertThat(newDiscountValue3.getCurrencyIsoCode()).isEqualTo(discountValue5.getCurrencyIsoCode());
        assertThat(newDiscountValue3.getValue()).isEqualTo((discountValue5.getValue() * 6) / 3);

        verify(mockPromotionResultCorrectionStrategy).correctPromotionResultRecords(discountValue2.getCode(),
                mockOrderEntry, 0, 0, true);
        verify(mockPromotionResultCorrectionStrategy).correctPromotionResultRecords(discountValue4.getCode(),
                mockOrderEntry, 0, 0, true);
        verify(mockPromotionResultCorrectionStrategy).correctPromotionResultRecords(discountValue6.getCode(),
                mockOrderEntry, 0, 0, true);
        verifyNoMoreInteractions(mockPromotionResultCorrectionStrategy);
    }

    @Test
    public void testCorrectDiscountValuesWithMoreDiscountValuesThanQuantityAndDifferentDiscountAmountsIncludingZero() {

        // Given deal result and three discount values
        setPromotionResultsInOrder(setupDealPromotionResult(DEAL_ACTION_GUID));

        final DiscountValue discountValue1 = createDiscountValue("12345", 4.0 / 6.0);
        final DiscountValue discountValue2 = createDiscountValue("12346", 3.0 / 6.0);
        final DiscountValue discountValue3 = createDiscountValue("12347", 8.0 / 6.0);
        final DiscountValue discountValue4 = createDiscountValue("12348", 1.0 / 6.0);
        final DiscountValue discountValue5 = createDiscountValue("12349", 0.0 / 6.0);
        final DiscountValue discountValue6 = createDiscountValue("12350", 2.0 / 6.0);
        final List<DiscountValue> discountValues = createDiscountValueList(discountValue1,
                discountValue2, discountValue3, discountValue4, discountValue5, discountValue6);

        // And new qty is 3      
        given(mockOrderEntry.getDiscountValues()).willReturn(discountValues);
        given(mockOrderEntry.getQuantity()).willReturn(Long.valueOf(3));

        discountValueCorrectionStrategy.correctDiscountValues(mockOrderEntry, 6);

        verify(mockOrderEntry).setDiscountValues(discountValuesCaptor.capture());

        final List<DiscountValue> newDiscountValues = discountValuesCaptor.getValue();
        assertThat(newDiscountValues).hasSize(3);

        final DiscountValue newDiscountValue1 = newDiscountValues.get(0);
        assertThat(newDiscountValue1.getCode()).isEqualTo(discountValue2.getCode());
        assertThat(newDiscountValue1.getCurrencyIsoCode()).isEqualTo(discountValue2.getCurrencyIsoCode());

        assertThat(newDiscountValue1.getValue()).isEqualTo((discountValue2.getValue() * 6) / 3);


        final DiscountValue newDiscountValue2 = newDiscountValues.get(1);
        assertThat(newDiscountValue2.getCode()).isEqualTo(discountValue1.getCode());
        assertThat(newDiscountValue2.getCurrencyIsoCode()).isEqualTo(discountValue1.getCurrencyIsoCode());

        assertThat(newDiscountValue2.getValue()).isEqualTo((discountValue1.getValue() * 6) / 3);


        final DiscountValue newDiscountValue3 = newDiscountValues.get(2);
        assertThat(newDiscountValue3.getCode()).isEqualTo(discountValue3.getCode());
        assertThat(newDiscountValue3.getCurrencyIsoCode()).isEqualTo(discountValue3.getCurrencyIsoCode());

        assertThat(newDiscountValue3.getValue()).isEqualTo((discountValue3.getValue() * 6) / 3);

        verify(mockPromotionResultCorrectionStrategy).correctPromotionResultRecords(discountValue4.getCode(),
                mockOrderEntry, 0, 0, true);
        verify(mockPromotionResultCorrectionStrategy).correctPromotionResultRecords(discountValue5.getCode(),
                mockOrderEntry, 0, 0, true);
        verify(mockPromotionResultCorrectionStrategy).correctPromotionResultRecords(discountValue6.getCode(),
                mockOrderEntry, 0, 0, true);
        verifyNoMoreInteractions(mockPromotionResultCorrectionStrategy);
    }

    @Test
    public void testCorrectDiscountValuesWithOldTMDOnlyReducedQty() {

        // Given one result and discount value for old TMD promotion
        setPromotionResultsInOrder(setupOldTMDPromotionResult(TMD_ACTION_GUID));
        final DiscountValue discountValue1 = createDiscountValue(TMD_ACTION_GUID, 5.0);
        final List<DiscountValue> discountValues = createDiscountValueList(discountValue1);

        // And new qty of 3
        given(mockOrderEntry.getDiscountValues()).willReturn(discountValues);
        given(mockOrderEntry.getQuantity()).willReturn(Long.valueOf(3));

        // When call the strategy with old qty 6
        discountValueCorrectionStrategy.correctDiscountValues(mockOrderEntry, 6);

        // Then the resulting discount value does not change  
        verify(mockOrderEntry).setDiscountValues(discountValuesCaptor.capture());

        final List<DiscountValue> newDiscountValues = discountValuesCaptor.getValue();
        assertThat(newDiscountValues).hasSize(1).containsOnly(discountValue1);

        // And the PromotionResultCorrectionStrategy is called to update the quantity
        verify(mockPromotionResultCorrectionStrategy).correctPromotionResultRecords(discountValue1.getCode(),
                mockOrderEntry, discountValue1.getValue(), 3, true);
        verifyNoMoreInteractions(mockPromotionResultCorrectionStrategy);
    }

    @Test
    public void testCorrectDiscountValuesWithOldTMDOnlyZeroQty() {

        // Given one result and discount value for old TMD promotion
        setPromotionResultsInOrder(setupOldTMDPromotionResult(TMD_ACTION_GUID));
        final DiscountValue discountValue1 = createDiscountValue(TMD_ACTION_GUID, 5.0);
        final List<DiscountValue> discountValues = createDiscountValueList(discountValue1);

        // And new qty 0
        given(mockOrderEntry.getDiscountValues()).willReturn(discountValues);
        given(mockOrderEntry.getQuantity()).willReturn(Long.valueOf(0));

        // When call the strategy with old qty 6
        discountValueCorrectionStrategy.correctDiscountValues(mockOrderEntry, 6);

        // Then discount value is removed and promo result is removed
        verify(mockOrderEntry).setDiscountValues(discountValuesCaptor.capture());

        final List<DiscountValue> newDiscountValues = discountValuesCaptor.getValue();
        assertThat(newDiscountValues).isEmpty();

        verify(mockPromotionResultCorrectionStrategy).correctPromotionResultRecords(discountValue1.getCode(),
                mockOrderEntry, 0, 0, true);
        verifyNoMoreInteractions(mockPromotionResultCorrectionStrategy);
    }


    @Test
    public void testCorrectDiscountValuesWithDealOnlyReducedQtyAndOldTmdOnOtherEntry() {

        // Given order has results for old tmd and a deal 
        final PromotionResultModel oldTmdPromoResult = setupOldTMDPromotionResult(TMD_ACTION_GUID);
        final PromotionResultModel dealPromoResult = setupDealPromotionResult(DEAL_ACTION_GUID);
        setPromotionResultsInOrder(oldTmdPromoResult, dealPromoResult);

        // Given one deal discount value - TMD one is in a different order entry
        final DiscountValue discountValue1 = createDiscountValue(DEAL_ACTION_GUID, 4.0 / 6.0);
        final List<DiscountValue> discountValues = createDiscountValueList(discountValue1);

        // And new qty 3
        given(mockOrderEntry.getDiscountValues()).willReturn(discountValues);
        given(mockOrderEntry.getQuantity()).willReturn(Long.valueOf(3));

        // When call the strategy with old qty 6
        discountValueCorrectionStrategy.correctDiscountValues(mockOrderEntry, 6);

        // Then the resulting discount value is a deal one and has the value adjusted 
        verify(mockOrderEntry).setDiscountValues(discountValuesCaptor.capture());

        final List<DiscountValue> newDiscountValues = discountValuesCaptor.getValue();
        assertThat(newDiscountValues).hasSize(1);

        final DiscountValue newDiscountValue = newDiscountValues.get(0);
        assertThat(newDiscountValue.getCode()).isEqualTo(discountValue1.getCode());
        assertThat(newDiscountValue.getCurrencyIsoCode()).isEqualTo(discountValue1.getCurrencyIsoCode());
        assertThat(newDiscountValue.getValue()).isEqualTo((discountValue1.getValue() * 6) / 3);

        verifyZeroInteractions(mockPromotionResultCorrectionStrategy);
    }

    @Test
    public void testCorrectDiscountValuesWithNewTMDOnlyReducedQty() {

        final Double basePrice = Double.valueOf(10.0);

        // Given one result and discount value for new TMD
        setPromotionResultsInOrder(setupNewTMDPromotionResult(TMD_ACTION_GUID));
        final DiscountValue discountValue1 = createDiscountValue(TMD_ACTION_GUID,
                basePrice.doubleValue() * NEW_TMD_PERCENT.doubleValue() / 100.0);
        final List<DiscountValue> discountValues = createDiscountValueList(discountValue1);

        // And new qty of 3, base price 10.0
        given(mockOrderEntry.getDiscountValues()).willReturn(discountValues);
        given(mockOrderEntry.getQuantity()).willReturn(Long.valueOf(3));
        given(mockOrderEntry.getBasePrice()).willReturn(basePrice);

        // When call the strategy with old qty 6
        discountValueCorrectionStrategy.correctDiscountValues(mockOrderEntry, 6);

        // Then the resulting discount value is same as the old one  
        verify(mockOrderEntry).setDiscountValues(discountValuesCaptor.capture());

        final List<DiscountValue> newDiscountValues = discountValuesCaptor.getValue();
        assertThat(newDiscountValues).hasSize(1);
        final DiscountValue newDiscountValue = newDiscountValues.get(0);
        assertThat(newDiscountValue.getCode()).isEqualTo(discountValue1.getCode());
        assertThat(newDiscountValue.getCurrencyIsoCode()).isEqualTo(discountValue1.getCurrencyIsoCode());
        assertThat(newDiscountValue.getValue()).isEqualTo(discountValue1.getValue());

        // And the PromotionResultCorrectionStrategy is called to update the quantity
        verify(mockPromotionResultCorrectionStrategy).correctPromotionResultRecords(discountValue1.getCode(),
                mockOrderEntry, discountValue1.getValue(), 3, false);
        verifyNoMoreInteractions(mockPromotionResultCorrectionStrategy);
    }

    @Test
    public void testCorrectDiscountValuesWithNewTMDOnlyZeroQty() {

        final Double basePrice = Double.valueOf(10.0);

        // Given one result and discount value for new TMD
        setPromotionResultsInOrder(setupNewTMDPromotionResult(TMD_ACTION_GUID));
        final DiscountValue discountValue1 = createDiscountValue(TMD_ACTION_GUID,
                basePrice.doubleValue() * NEW_TMD_PERCENT.doubleValue() / 100.0);
        final List<DiscountValue> discountValues = createDiscountValueList(discountValue1);

        // And new qty of 0
        given(mockOrderEntry.getDiscountValues()).willReturn(discountValues);
        given(mockOrderEntry.getQuantity()).willReturn(Long.valueOf(0));
        given(mockOrderEntry.getBasePrice()).willReturn(basePrice);

        // When call the strategy with old qty 6
        discountValueCorrectionStrategy.correctDiscountValues(mockOrderEntry, 6);

        // Then discount value is removed and promo result is removed
        verify(mockOrderEntry).setDiscountValues(discountValuesCaptor.capture());

        final List<DiscountValue> newDiscountValues = discountValuesCaptor.getValue();
        assertThat(newDiscountValues).isEmpty();

        verify(mockPromotionResultCorrectionStrategy).correctPromotionResultRecords(discountValue1.getCode(),
                mockOrderEntry, 0, 0, false);
        verifyNoMoreInteractions(mockPromotionResultCorrectionStrategy);
    }

    @Test
    public void testCorrectDiscountValuesWithDealAndOldNewTmdReduceQty() {

        final Double basePrice = Double.valueOf(10.0);

        // Given order has results for new tmd and a deal 
        final PromotionResultModel newTmdPromoResult = setupNewTMDPromotionResult(TMD_ACTION_GUID);
        final PromotionResultModel dealPromoResult = setupDealPromotionResult(DEAL_ACTION_GUID);
        setPromotionResultsInOrder(newTmdPromoResult, dealPromoResult);

        // Given discount values for both
        final DiscountValue discountValue1 = createDiscountValue(DEAL_ACTION_GUID, 4.0 / 6.0);
        final DiscountValue discountValue2 = createDiscountValue(TMD_ACTION_GUID,
                basePrice.doubleValue() * NEW_TMD_PERCENT.doubleValue() / 100.0);
        final List<DiscountValue> discountValues = createDiscountValueList(discountValue1, discountValue2);

        // And new qty 3
        given(mockOrderEntry.getDiscountValues()).willReturn(discountValues);
        given(mockOrderEntry.getQuantity()).willReturn(Long.valueOf(3));
        given(mockOrderEntry.getBasePrice()).willReturn(basePrice);

        // When call the strategy with old qty 6
        discountValueCorrectionStrategy.correctDiscountValues(mockOrderEntry, 6);

        // Then the resulting discount values have the value adjusted 
        verify(mockOrderEntry).setDiscountValues(discountValuesCaptor.capture());

        final List<DiscountValue> newDiscountValues = discountValuesCaptor.getValue();
        assertThat(newDiscountValues).hasSize(2);

        final DiscountValue newDiscountValue1 = newDiscountValues.get(0);
        assertThat(newDiscountValue1.getCode()).isEqualTo(discountValue1.getCode());
        assertThat(newDiscountValue1.getCurrencyIsoCode()).isEqualTo(discountValue1.getCurrencyIsoCode());
        assertThat(newDiscountValue1.getValue()).isEqualTo((discountValue1.getValue() * 6) / 3);

        // New value is recalculated by applying percent rate to unit price after deal discount removed
        // Need to include a delta to cope with rounding errors
        final double newTmdDiscountValue = (basePrice.doubleValue() - newDiscountValue1.getValue())
                * NEW_TMD_PERCENT.doubleValue() / 100.0;
        final DiscountValue newDiscountValue2 = newDiscountValues.get(1);
        assertThat(newDiscountValue2.getCode()).isEqualTo(discountValue2.getCode());
        assertThat(newDiscountValue2.getCurrencyIsoCode()).isEqualTo(discountValue2.getCurrencyIsoCode());
        assertThat(newDiscountValue2.getValue()).isEqualTo(newTmdDiscountValue, Delta.delta(0.001));

        // And the promotion action for TMD is updated
        verify(mockPromotionResultCorrectionStrategy).correctPromotionResultRecords(newDiscountValue2.getCode(),
                mockOrderEntry, newDiscountValue2.getValue(), 3, false);
        verifyZeroInteractions(mockPromotionResultCorrectionStrategy);
    }


    private PromotionResultModel setupOldTMDPromotionResult(final String actionGuid) {

        return setupPromotionResult(actionGuid, mock(TMDiscountPromotionModel.class));
    }

    private PromotionResultModel setupNewTMDPromotionResult(final String actionGuid) {

        final TMDiscountProductPromotionModel mockNewTmdPromotion = mock(TMDiscountProductPromotionModel.class);
        given(mockNewTmdPromotion.getPercentageDiscount()).willReturn(Double.valueOf(NEW_TMD_PERCENT.doubleValue()));
        return setupPromotionResult(actionGuid, mockNewTmdPromotion);
    }

    private PromotionResultModel setupDealPromotionResult(final String actionGuid) {

        return setupPromotionResult(actionGuid, mock(AbstractDealModel.class));
    }

    private PromotionResultModel setupPromotionResult(final String actionGuid, final AbstractPromotionModel promotion) {

        final AbstractPromotionActionModel mockTMDPromotionAction = mock(AbstractPromotionActionModel.class);
        given(mockTMDPromotionAction.getGuid()).willReturn(actionGuid);

        final PromotionResultModel promotionResult = mock(PromotionResultModel.class);
        given(promotionResult.getPromotion()).willReturn(promotion);
        given(promotionResult.getActions()).willReturn(Collections.singleton(mockTMDPromotionAction));
        return promotionResult;
    }

    private void setPromotionResultsInOrder(final PromotionResultModel... promotionResults) {

        given(mockOrder.getAllPromotionResults()).willReturn(createPromotionResultsSet(promotionResults));
    }

    private Set<PromotionResultModel> createPromotionResultsSet(final PromotionResultModel... promotionResults) {

        final Set<PromotionResultModel> setPromotionResults = new HashSet<>();
        for (final PromotionResultModel result : promotionResults) {
            setPromotionResults.add(result);
        }
        return setPromotionResults;
    }

    private DiscountValue createDiscountValue(final String guid, final double value) {

        final DiscountValue discountValue = new DiscountValue(guid, value, true, "AUD");
        return discountValue;
    }

    private List<DiscountValue> createDiscountValueList(final DiscountValue... dvs) {

        final List<DiscountValue> discountValues = new ArrayList<>();
        for (final DiscountValue dv : dvs) {
            discountValues.add(dv);
        }
        return discountValues;
    }



}
