/**
 * 
 */
package au.com.target.tgtcore.attributes.dynamic;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetProductDimensionsModel;


/**
 * @author pthoma20
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductDimensionHmcLabelProviderTest {

    @Mock
    private TargetProductDimensionsModel model;

    @InjectMocks
    @Spy
    private final TargetProductDimensionHmcLabelProvider provider = new TargetProductDimensionHmcLabelProvider();

    @Test
    public void testTargetProductDimensionHmcLabel() {

        BDDMockito.given(model.getDimensionCode()).willReturn("PKG_112233");
        BDDMockito.given(model.getHeight()).willReturn(Double.valueOf(1.2));
        BDDMockito.given(model.getWeight()).willReturn(Double.valueOf(2.2));
        BDDMockito.given(model.getLength()).willReturn(Double.valueOf(4.2));
        BDDMockito.given(model.getWidth()).willReturn(Double.valueOf(5.0));
        Assert.assertEquals("DimensionCode:PKG_112233; Weight:2.2; Height:1.2; Length:4.2; Width:5.0",
                provider.get(model));

    }

    @Test
    public void testTargetProductDimensionHmcLabelWithAttributesNulls() {

        BDDMockito.given(model.getDimensionCode()).willReturn("PKG_112233");
        BDDMockito.given(model.getHeight()).willReturn(null);
        BDDMockito.given(model.getWeight()).willReturn(null);
        BDDMockito.given(model.getLength()).willReturn(null);
        BDDMockito.given(model.getWidth()).willReturn(null);
        Assert.assertEquals("DimensionCode:PKG_112233",
                provider.get(model));

    }

    @Test
    /** This test case will ideally not happen as the dimensioncode cannot be null.
     * But in case it comes as null then it is to return the label code as empty String
     */
    public void testTargetProductDimensionHmcLabelWithNulls() {

        BDDMockito.given(model.getDimensionCode()).willReturn(null);
        BDDMockito.given(model.getHeight()).willReturn(null);
        BDDMockito.given(model.getWeight()).willReturn(null);
        BDDMockito.given(model.getLength()).willReturn(null);
        BDDMockito.given(model.getWidth()).willReturn(null);
        Assert.assertEquals("",
                provider.get(model));

    }


    @Test
    public void testTargetProductDimensionHmcLabelWithPartialNulls() {

        BDDMockito.given(model.getDimensionCode()).willReturn("PKG_112233");
        BDDMockito.given(model.getHeight()).willReturn(Double.valueOf(2.2));
        BDDMockito.given(model.getWeight()).willReturn(null);
        BDDMockito.given(model.getLength()).willReturn(null);
        BDDMockito.given(model.getWidth()).willReturn(Double.valueOf(3.2));
        Assert.assertEquals("DimensionCode:PKG_112233; Height:2.2; Width:3.2",
                provider.get(model));

    }

}
