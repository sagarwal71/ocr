/**
 * 
 */
package au.com.target.tgtcore.sizegroup.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetProductSizeModel;
import au.com.target.tgtcore.model.TargetSizeGroupModel;
import au.com.target.tgtcore.sizeorder.dao.TargetProductSizeDao;
import au.com.target.tgtcore.sizeorder.dao.TargetSizeGroupDao;


/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetSizeOrderingServiceImplTest {

    @Mock
    private TargetSizeGroupDao targetSizeGroupDao;

    @Mock
    private TargetSizeGroupModel groupModel;

    @Mock
    private TargetProductSizeDao targetProductSizeDao;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetProductSizeModel prdSize;

    @InjectMocks
    private final TargetSizeOrderingServiceImpl service = new TargetSizeOrderingServiceImpl();

    @Before
    public void setup() {
        Mockito.when(modelService.create(TargetSizeGroupModel.class)).thenReturn(groupModel);
        Mockito.when(modelService.create(TargetProductSizeModel.class)).thenReturn(prdSize);
        Mockito.when(targetSizeGroupDao.getMaxSizeGroupPosition()).thenReturn(Integer.valueOf(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetTargetSizeGroupByNameNull() {
        service.getTargetSizeGroupByName(null, "P1000", false);
    }

    @Test
    public void testGetTargetSizeGroupByName() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        service.getTargetSizeGroupByName("womens", "P1000", false);
        Mockito.verify(targetSizeGroupDao).findSizeGroupbyCode("womens");
    }



    @Test
    public void testGetTargetSizeGroupByNameWithExistingAndCreate() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        Mockito.when(targetSizeGroupDao.findSizeGroupbyCode("womens")).thenReturn(groupModel);
        service.getTargetSizeGroupByName("womens", "P1000", true);
        Mockito.verify(targetSizeGroupDao, Mockito.times(1)).findSizeGroupbyCode("womens");
    }

    @Test
    public void testGetTargetSizeGroupByNameWithNonExistingAndCreate() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        Mockito.when(targetSizeGroupDao.findSizeGroupbyCode("womens"))
                .thenThrow(new TargetUnknownIdentifierException("test"));
        service.getTargetSizeGroupByName("womens", "P1000", true);
        Mockito.verify(targetSizeGroupDao, Mockito.times(1)).findSizeGroupbyCode("womens");
        Mockito.verify(targetSizeGroupDao).getMaxSizeGroupPosition();
        Mockito.verify(modelService).save(Mockito.any(TargetSizeGroupModel.class));
    }


    @Test
    public void testGetTargetSizeGroupByNameWithCreate() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetSizeGroupModel sizeGrp = new TargetSizeGroupModel();
        Mockito.when(modelService.create(TargetSizeGroupModel.class)).thenReturn(sizeGrp);
        Mockito.when(targetSizeGroupDao.findSizeGroupbyCode("mensbottoms"))
                .thenThrow(new TargetUnknownIdentifierException("test"));
        service.getTargetSizeGroupByName("mens bottoms", "P1000", true);
        final ArgumentCaptor<TargetSizeGroupModel> sizegroup = ArgumentCaptor
                .forClass(TargetSizeGroupModel.class);
        Mockito.verify(modelService).save(sizegroup.capture());
        Mockito.verify(modelService).refresh(sizegroup.capture());
        Mockito.verify(targetSizeGroupDao).findSizeGroupbyCode("mensbottoms");
        Assert.assertEquals(sizegroup.getValue().getPosition(), Integer.valueOf(2));
        Assert.assertEquals(sizegroup.getValue().getCode(), "mensbottoms");
        Assert.assertEquals(sizegroup.getValue().getName(), "mens bottoms");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetTargetProductSizeWithhNullGrp() {
        service.getTargetProductSize("5", null, false, "CP2012");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetTargetProductSizeWithhNullSize() {
        service.getTargetProductSize(null, groupModel, false, "CP2012");
    }

    @Test
    public void testGetTargetProductSizeWithoutCreateIfMissing() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        service.getTargetProductSize("xs", groupModel, false, "CP2012");
        Mockito.verify(targetProductSizeDao).findTargetProductSizeBySizeGroup("xs", groupModel);
    }

    @Test
    public void testGetTargetProductSizeWithCreateIfMissing() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        Mockito.when(targetProductSizeDao.findTargetProductSizeBySizeGroup("xs", groupModel))
                .thenThrow(new TargetUnknownIdentifierException("test"));
        service.getTargetProductSize("xs", groupModel, true, "CP2012");
        Mockito.verify(targetProductSizeDao).getMaxProductSizePosition(groupModel);
        Mockito.verify(targetProductSizeDao).findTargetProductSizeBySizeGroup("xs", groupModel);
        Mockito.verify(modelService).save(Mockito.any(TargetProductSizeModel.class));
    }


    @Test
    public void testGetTargetProductSizeWithCreateIfMissing1() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final TargetProductSizeModel productSize = new TargetProductSizeModel();

        Mockito.when(modelService.create(TargetProductSizeModel.class)).thenReturn(productSize);
        Mockito.when(targetProductSizeDao.findTargetProductSizeBySizeGroup("xs", groupModel))
                .thenThrow(new TargetUnknownIdentifierException("test"));
        Mockito.when(groupModel.getCode()).thenReturn("womens");
        Mockito.when(groupModel.getName()).thenReturn("women s");
        Mockito.when(groupModel.getPosition()).thenReturn(Integer.valueOf(1));
        final ArgumentCaptor<TargetProductSizeModel> productSizeCaptor = ArgumentCaptor
                .forClass(TargetProductSizeModel.class);
        Mockito.when(targetProductSizeDao.getMaxProductSizePosition(groupModel)).thenReturn(Integer.valueOf(1));
        service.getTargetProductSize("xs", groupModel, true, "CP2012");
        Mockito.verify(targetProductSizeDao).getMaxProductSizePosition(groupModel);
        Mockito.verify(targetProductSizeDao).findTargetProductSizeBySizeGroup("xs", groupModel);
        Mockito.verify(modelService).save(productSizeCaptor.capture());
        Mockito.verify(modelService).refresh(productSizeCaptor.capture());

        Assert.assertEquals(productSizeCaptor.getValue().getPosition(), Integer.valueOf(2));
        Assert.assertEquals(productSizeCaptor.getValue().getSize(), "xs");
        Assert.assertEquals(productSizeCaptor.getValue().getSizeGroup(), groupModel);
        Assert.assertEquals(productSizeCaptor.getValue().getSizeGroup().getName(), "women s");
        Assert.assertEquals(productSizeCaptor.getValue().getSizeGroup().getCode(), "womens");
        Assert.assertEquals(productSizeCaptor.getValue().getSizeGroup().getPosition(), Integer.valueOf(1));


    }
}
