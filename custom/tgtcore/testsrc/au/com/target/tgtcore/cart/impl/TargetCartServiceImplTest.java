/**
 * 
 */
package au.com.target.tgtcore.cart.impl;

import static org.mockito.Mockito.times;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.AddressService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collection;
import java.util.Iterator;

import org.junit.Assert;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.SavedCncStoreDetailsModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtpayment.enums.PaymentProviderTypeEnum;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCartServiceImplTest {

    @Mock
    private TargetDeliveryService targetDeliveryService;

    @Mock
    private CartModel cartModel;

    @Mock
    private InterceptorContext ctx;

    @Mock
    private AbstractTargetZoneDeliveryModeValueModel targetZoneDeliveryModeValueModel;

    @Mock
    private TargetZoneDeliveryModeModel targetZoneDeliveryModeModel;

    @Mock
    private TargetZoneDeliveryModeModel targetZoneDeliveryModeModel2;

    @InjectMocks
    private final TargetCartServiceImpl targetCartService = new TargetCartServiceImpl();

    @Mock
    private AddressModel shippingAddress;

    @Mock
    private AddressModel paymentAddress;

    @Mock
    private CreditCardPaymentInfoModel paymentInfo;

    @Mock
    private TargetZoneDeliveryModeModel preferredDeliveryMode;

    @Mock
    private TargetCustomerModel customer;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetPointOfServiceService targetPointOfServiceService;

    @Mock
    private TargetPointOfServiceModel targetPointOfService;

    @Mock
    private PaymentModeService paymentModeService;

    @Mock
    private PaymentModeModel paymentMode;

    @Mock
    private AddressService addressService;

    @Mock
    private OrderModel previousOrder;

    @Mock
    private Collection<SavedCncStoreDetailsModel> cncDetails;

    @Mock
    private SavedCncStoreDetailsModel cncDetail;

    @Mock
    private Iterator<SavedCncStoreDetailsModel> cncDetailsIterator;

    @Mock
    private UserService userService;

    @Mock
    private TitleModel title;

    @Mock
    private AddressModel clonedAddressModel;

    @Mock
    private SessionService sessionService;



    @Before
    public void setUp() {
        Mockito.when(customer.getDefaultPaymentAddress()).thenReturn(paymentAddress);
        Mockito.when(customer.getDefaultShipmentAddress()).thenReturn(shippingAddress);
        Mockito.when(customer.getFlyBuysCode()).thenReturn("123");
        Mockito.when(paymentModeService.getPaymentModeForCode(PaymentProviderTypeEnum.CREDITCARD.getCode()))
                .thenReturn(paymentMode);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullModel() throws InterceptorException {
        cartModel = null;
        targetZoneDeliveryModeModel = null;
        targetCartService.setDeliveryMode(cartModel, targetZoneDeliveryModeModel);
    }

    @Test
    public void testNullOrderTargetZoneDeliveryModeValue() throws InterceptorException {
        BDDMockito.given(cartModel.getZoneDeliveryModeValue()).willReturn(null);
        BDDMockito.given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        BDDMockito.given(targetCartService.getZoneDeliveryModeValueFromDeliveryMode(cartModel)).willReturn(
                targetZoneDeliveryModeValueModel);
        targetCartService.setDeliveryMode(cartModel, targetZoneDeliveryModeModel);
        Mockito.verify(cartModel).setZoneDeliveryModeValue(targetZoneDeliveryModeValueModel);
    }

    @Test
    public void testExistingOrderTargetZoneDeliveryModeValue() throws InterceptorException {
        BDDMockito.given(cartModel.getZoneDeliveryModeValue()).willReturn(targetZoneDeliveryModeValueModel);
        BDDMockito.given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        BDDMockito.given(targetZoneDeliveryModeValueModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        BDDMockito.given(targetCartService.getZoneDeliveryModeValueFromDeliveryMode(cartModel)).willReturn(
                targetZoneDeliveryModeValueModel);
        targetCartService.setDeliveryMode(cartModel, targetZoneDeliveryModeModel2);
        Mockito.verify(cartModel).setZoneDeliveryModeValue(targetZoneDeliveryModeValueModel);
    }

    @Test
    public void testTargetZoneDeliveryModeValueWhenZDMVNull() throws InterceptorException {
        BDDMockito.given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        BDDMockito.given(targetZoneDeliveryModeValueModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        BDDMockito.given(targetCartService.getZoneDeliveryModeValueFromDeliveryMode(cartModel)).willThrow(
                new TargetNoPostCodeException("No postcode found"));
        targetCartService.setDeliveryMode(cartModel, targetZoneDeliveryModeModel2);
        Mockito.verify(cartModel).setZoneDeliveryModeValue(null);
        Assert.assertEquals(null, cartModel.getZoneDeliveryModeValue());
        Assert.assertEquals(cartModel.getDeliveryMode(), targetZoneDeliveryModeModel);
    }

    @Test
    public void testSetCncStoreNumberDeliveryTypeCNC() throws InterceptorException {
        BDDMockito.given(cartModel.getCncStoreNumber()).willReturn(null);
        BDDMockito.given(cartModel.getZoneDeliveryModeValue()).willReturn(null);
        BDDMockito.given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        BDDMockito.given(targetCartService.getZoneDeliveryModeValueFromDeliveryMode(cartModel)).willReturn(
                targetZoneDeliveryModeValueModel);
        BDDMockito.given(targetZoneDeliveryModeModel.getIsDeliveryToStore()).willReturn(Boolean.TRUE);

        targetCartService.setDeliveryMode(cartModel, targetZoneDeliveryModeModel);
        Mockito.verify(cartModel).setZoneDeliveryModeValue(targetZoneDeliveryModeValueModel);
        Mockito.verify(cartModel).setCalculated(Boolean.FALSE);
        Mockito.verify(cartModel, Mockito.never()).setCncStoreNumber(null);
        Mockito.verify(cartModel).setDeliveryAddress(null);
    }

    @Test
    public void testSetCncStoreNumberDeliveryTypeHD() throws InterceptorException {
        BDDMockito.given(cartModel.getCncStoreNumber()).willReturn(Integer.valueOf(5001));
        BDDMockito.given(cartModel.getZoneDeliveryModeValue()).willReturn(null);
        BDDMockito.given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        BDDMockito.given(targetCartService.getZoneDeliveryModeValueFromDeliveryMode(cartModel)).willReturn(
                targetZoneDeliveryModeValueModel);
        BDDMockito.given(targetZoneDeliveryModeModel.getIsDeliveryToStore()).willReturn(Boolean.FALSE);

        targetCartService.setDeliveryMode(cartModel, targetZoneDeliveryModeModel);
        Mockito.verify(cartModel).setZoneDeliveryModeValue(targetZoneDeliveryModeValueModel);
        Mockito.verify(cartModel).setCalculated(Boolean.FALSE);
        Mockito.verify(cartModel).setCncStoreNumber(null);
        Mockito.verify(cartModel).setDeliveryAddress(null);
    }

    @Test
    public void testSetCncStoreNumberDeliveryTypeNull() throws InterceptorException {
        BDDMockito.given(cartModel.getCncStoreNumber()).willReturn(null);
        BDDMockito.given(cartModel.getZoneDeliveryModeValue()).willReturn(null);
        BDDMockito.given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        BDDMockito.given(targetCartService.getZoneDeliveryModeValueFromDeliveryMode(cartModel)).willReturn(
                targetZoneDeliveryModeValueModel);
        BDDMockito.given(targetZoneDeliveryModeModel.getIsDeliveryToStore()).willReturn(null);

        targetCartService.setDeliveryMode(cartModel, targetZoneDeliveryModeModel);
        Mockito.verify(cartModel).setZoneDeliveryModeValue(targetZoneDeliveryModeValueModel);
        Mockito.verify(cartModel).setCalculated(Boolean.FALSE);
        Mockito.verify(cartModel).setCncStoreNumber(null);
        Mockito.verify(cartModel, Mockito.never()).setDeliveryAddress(null);
    }

    @Test
    public void testSetDeliveryModeWithDeliveryModeModelNoStoreNumber() throws InterceptorException {
        BDDMockito.given(cartModel.getCncStoreNumber()).willReturn(null);
        BDDMockito.given(cartModel.getZoneDeliveryModeValue()).willReturn(null);
        BDDMockito.given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        BDDMockito.given(targetCartService.getZoneDeliveryModeValueFromDeliveryMode(cartModel)).willReturn(
                targetZoneDeliveryModeValueModel);

        final DeliveryModeModel mockDeliveryMode = Mockito.mock(DeliveryModeModel.class);

        targetCartService.setDeliveryMode(cartModel, mockDeliveryMode);
        Mockito.verify(cartModel).setZoneDeliveryModeValue(targetZoneDeliveryModeValueModel);
        Mockito.verify(cartModel).setCalculated(Boolean.FALSE);
        Mockito.verify(cartModel).setCncStoreNumber(null);
        Mockito.verify(cartModel, Mockito.never()).setDeliveryAddress(null);
    }

    @Test
    public void testSetDeliveryModeWithDeliveryModeModelWithStoreNumber() throws InterceptorException {
        BDDMockito.given(cartModel.getCncStoreNumber()).willReturn(Integer.valueOf(5001));
        BDDMockito.given(cartModel.getZoneDeliveryModeValue()).willReturn(null);
        BDDMockito.given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        BDDMockito.given(targetCartService.getZoneDeliveryModeValueFromDeliveryMode(cartModel)).willReturn(
                targetZoneDeliveryModeValueModel);

        final DeliveryModeModel mockDeliveryMode = Mockito.mock(DeliveryModeModel.class);

        targetCartService.setDeliveryMode(cartModel, mockDeliveryMode);
        Mockito.verify(cartModel).setZoneDeliveryModeValue(targetZoneDeliveryModeValueModel);
        Mockito.verify(cartModel).setCalculated(Boolean.FALSE);
        Mockito.verify(cartModel).setCncStoreNumber(null);
        Mockito.verify(cartModel).setDeliveryAddress(null);
    }

    @Test
    public void testSetDeliveryModeDeliveryTypeCNCWithStoreNumber() throws InterceptorException {
        BDDMockito.given(cartModel.getCncStoreNumber()).willReturn(Integer.valueOf(5001));
        BDDMockito.given(cartModel.getZoneDeliveryModeValue()).willReturn(null);
        BDDMockito.given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        BDDMockito.given(targetCartService.getZoneDeliveryModeValueFromDeliveryMode(cartModel)).willReturn(
                targetZoneDeliveryModeValueModel);
        BDDMockito.given(targetZoneDeliveryModeModel.getIsDeliveryToStore()).willReturn(Boolean.TRUE);

        targetCartService.setDeliveryMode(cartModel, targetZoneDeliveryModeModel);
        Mockito.verify(cartModel).setZoneDeliveryModeValue(targetZoneDeliveryModeValueModel);
        Mockito.verify(cartModel).setCalculated(Boolean.FALSE);
        Mockito.verify(cartModel, Mockito.never()).setCncStoreNumber(null);
        Mockito.verify(cartModel, Mockito.never()).setDeliveryAddress(null);
    }

    /**
     * Method to test for a non cnc order.
     */
    @Test
    public void testPrePopulateForNonCnCOrder() {
        targetCartService.prePopulateCart(cartModel, preferredDeliveryMode, customer, paymentInfo, previousOrder);
        Mockito.when(preferredDeliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.FALSE);
        Mockito.verify(cartModel).setPaymentInfo(paymentInfo);
        Mockito.verify(cartModel).setDeliveryMode(preferredDeliveryMode);
        Mockito.verify(cartModel).setPaymentAddress(paymentAddress);
        Mockito.verify(cartModel).setFlyBuysCode("123");
        Mockito.verify(cartModel, times(0)).setPaymentMode(paymentMode);
        Mockito.verify(cartModel).setDeliveryAddress(shippingAddress);
        Mockito.verify(modelService).save(cartModel);
        Mockito.verify(sessionService, Mockito.never()).setAttribute(Mockito.anyString(),
                Mockito.anyString());
    }

    /**
     * Method to test for a cnc order.
     * 
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    @Test
    public void testPrePopulateForCnCOrder() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final Integer storeCnCNumber = Integer.valueOf(100);
        Mockito.when(preferredDeliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);
        Mockito.when(customer.getSavedCncStoreDetails()).thenReturn(cncDetails);
        Mockito.when(cncDetails.iterator()).thenReturn(cncDetailsIterator);
        Mockito.when(customer.getSavedCncStoreDetails()).thenReturn(cncDetails);
        Mockito.when(userService.getTitleForCode("mr")).thenReturn(title);
        Mockito.when(cncDetailsIterator.next()).thenReturn(cncDetail);
        Mockito.when(addressService.cloneAddressForOwner(targetPointOfService.getAddress(), cartModel)).thenReturn(
                shippingAddress);
        Mockito.when(shippingAddress.getPostalcode()).thenReturn("3000");
        Mockito.when(cncDetail.getStore()).thenReturn(targetPointOfService);
        Mockito.when(targetPointOfService.getStoreNumber()).thenReturn(storeCnCNumber);
        Mockito.when(cncDetail.getTitle()).thenReturn("mr");
        Mockito.when(cartModel.getDeliveryAddress()).thenReturn(shippingAddress);
        Mockito.when(
                Boolean.valueOf(targetPointOfServiceService.isSupportAllProductTypesInCart(targetPointOfService,
                        cartModel))).thenReturn(Boolean.valueOf(true));
        targetCartService.prePopulateCart(cartModel, preferredDeliveryMode, customer, paymentInfo, previousOrder);

        Mockito.verify(cartModel).setPaymentInfo(paymentInfo);
        Mockito.verify(cartModel).setDeliveryMode(preferredDeliveryMode);
        Mockito.verify(cartModel).setPaymentAddress(paymentAddress);
        Mockito.verify(cartModel).setFlyBuysCode("123");
        Mockito.verify(cartModel, times(0)).setPaymentMode(paymentMode);
        Mockito.verify(cartModel).setCncStoreNumber(targetPointOfService.getStoreNumber());
        Mockito.verify(shippingAddress).setTitle(title);
        Mockito.verify(shippingAddress).setFirstname(cncDetail.getFirstName());
        Mockito.verify(shippingAddress).setLastname(cncDetail.getLastName());
        Mockito.verify(shippingAddress).setPhone1(cncDetail.getPhoneNumber());
        Mockito.verify(cartModel).setDeliveryAddress(shippingAddress);
        Mockito.verify(modelService).save(cartModel);
        Mockito.verify(sessionService).setAttribute(TgtCoreConstants.SESSION_POSTCODE, "3000");
    }

    @Test
    public void testPrePopulateForCnCOrderWithUnsupportedProductTypes() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final Integer storeCnCNumber = Integer.valueOf(100);
        Mockito.when(preferredDeliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);
        Mockito.when(customer.getSavedCncStoreDetails()).thenReturn(cncDetails);
        Mockito.when(cncDetails.iterator()).thenReturn(cncDetailsIterator);
        Mockito.when(customer.getSavedCncStoreDetails()).thenReturn(cncDetails);
        Mockito.when(userService.getTitleForCode("mr")).thenReturn(title);
        Mockito.when(cncDetailsIterator.next()).thenReturn(cncDetail);
        Mockito.when(addressService.cloneAddressForOwner(targetPointOfService.getAddress(), cartModel)).thenReturn(
                shippingAddress);
        Mockito.when(shippingAddress.getPostalcode()).thenReturn("3000");
        Mockito.when(cncDetail.getStore()).thenReturn(targetPointOfService);
        Mockito.when(targetPointOfService.getStoreNumber()).thenReturn(storeCnCNumber);
        Mockito.when(cncDetail.getTitle()).thenReturn("mr");
        Mockito.when(cartModel.getDeliveryAddress()).thenReturn(shippingAddress);
        Mockito.when(
                Boolean.valueOf(targetPointOfServiceService.isSupportAllProductTypesInCart(targetPointOfService,
                        cartModel))).thenReturn(Boolean.valueOf(false));
        targetCartService.prePopulateCart(cartModel, preferredDeliveryMode, customer, paymentInfo, previousOrder);

        Mockito.verify(cartModel).setPaymentInfo(paymentInfo);
        Mockito.verify(cartModel).setDeliveryMode(preferredDeliveryMode);
        Mockito.verify(cartModel).setPaymentAddress(paymentAddress);
        Mockito.verify(cartModel).setFlyBuysCode("123");
        Mockito.verify(cartModel, times(0)).setPaymentMode(paymentMode);
        Mockito.verify(cartModel, Mockito.times(0)).setCncStoreNumber(targetPointOfService.getStoreNumber());
        Mockito.verify(shippingAddress, Mockito.times(0)).setTitle(title);
        Mockito.verify(shippingAddress, Mockito.times(0)).setFirstname(cncDetail.getFirstName());
        Mockito.verify(shippingAddress, Mockito.times(0)).setLastname(cncDetail.getLastName());
        Mockito.verify(shippingAddress, Mockito.times(0)).setPhone1(cncDetail.getPhoneNumber());
        Mockito.verify(cartModel, Mockito.times(0)).setDeliveryAddress(shippingAddress);
        Mockito.verify(modelService).save(cartModel);
        Mockito.verify(sessionService).setAttribute(TgtCoreConstants.SESSION_POSTCODE, "3000");
    }

    /**
     * Method to test for a cnc order picking up details from previous order.
     * 
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    @Test
    public void testPrePopulateForCnCOrderUsingPreviousOrder() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final Integer storeCnCNumber = Integer.valueOf(100);
        Mockito.when(preferredDeliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);
        Mockito.when(customer.getSavedCncStoreDetails()).thenReturn(CollectionUtils.EMPTY_COLLECTION);
        Mockito.when(targetPointOfServiceService.getPOSByStoreNumber(storeCnCNumber)).thenReturn(
                targetPointOfService);
        Mockito.when(addressService.cloneAddressForOwner(targetPointOfService.getAddress(), cartModel)).thenReturn(
                clonedAddressModel);
        Mockito.when(previousOrder.getCncStoreNumber()).thenReturn(Integer.valueOf(100));
        Mockito.when(previousOrder.getDeliveryAddress()).thenReturn(shippingAddress);
        Mockito.when(shippingAddress.getPostalcode()).thenReturn("3000");
        Mockito.when(cartModel.getDeliveryAddress()).thenReturn(shippingAddress);
        Mockito.when(
                Boolean.valueOf(targetPointOfServiceService.isSupportAllProductTypesInCart(targetPointOfService,
                        cartModel))).thenReturn(Boolean.valueOf(true));
        targetCartService.prePopulateCart(cartModel, preferredDeliveryMode, customer, paymentInfo, previousOrder);

        Mockito.verify(cartModel).setPaymentInfo(paymentInfo);
        Mockito.verify(cartModel).setDeliveryMode(preferredDeliveryMode);
        Mockito.verify(cartModel).setPaymentAddress(paymentAddress);
        Mockito.verify(cartModel).setFlyBuysCode("123");
        Mockito.verify(cartModel, times(0)).setPaymentMode(paymentMode);
        Mockito.verify(cartModel).setCncStoreNumber(targetPointOfService.getStoreNumber());
        Mockito.verify(clonedAddressModel).setTitle(shippingAddress.getTitle());
        Mockito.verify(clonedAddressModel).setFirstname(shippingAddress.getFirstname());
        Mockito.verify(clonedAddressModel).setLastname(shippingAddress.getLastname());
        Mockito.verify(clonedAddressModel).setPhone1(shippingAddress.getPhone1());
        Mockito.verify(cartModel).setDeliveryAddress(clonedAddressModel);
        Mockito.verify(modelService).save(cartModel);
        Mockito.verify(sessionService).setAttribute(TgtCoreConstants.SESSION_POSTCODE, "3000");
    }

    @Test
    public void testPrePopulateForCnCOrderUsingPreviousOrderWithUnsupportedProductTypes()
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final Integer storeCnCNumber = Integer.valueOf(100);
        Mockito.when(preferredDeliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);
        Mockito.when(customer.getSavedCncStoreDetails()).thenReturn(CollectionUtils.EMPTY_COLLECTION);
        Mockito.when(targetPointOfServiceService.getPOSByStoreNumber(storeCnCNumber)).thenReturn(
                targetPointOfService);
        Mockito.when(addressService.cloneAddressForOwner(targetPointOfService.getAddress(), cartModel)).thenReturn(
                clonedAddressModel);
        Mockito.when(previousOrder.getCncStoreNumber()).thenReturn(Integer.valueOf(100));
        Mockito.when(previousOrder.getDeliveryAddress()).thenReturn(shippingAddress);
        Mockito.when(shippingAddress.getPostalcode()).thenReturn("3000");
        Mockito.when(cartModel.getDeliveryAddress()).thenReturn(shippingAddress);
        Mockito.when(
                Boolean.valueOf(targetPointOfServiceService.isSupportAllProductTypesInCart(targetPointOfService,
                        cartModel))).thenReturn(Boolean.valueOf(false));
        targetCartService.prePopulateCart(cartModel, preferredDeliveryMode, customer, paymentInfo, previousOrder);

        Mockito.verify(cartModel).setPaymentInfo(paymentInfo);
        Mockito.verify(cartModel).setDeliveryMode(preferredDeliveryMode);
        Mockito.verify(cartModel).setPaymentAddress(paymentAddress);
        Mockito.verify(cartModel).setFlyBuysCode("123");
        Mockito.verify(cartModel, times(0)).setPaymentMode(paymentMode);
        Mockito.verify(cartModel, Mockito.times(0)).setCncStoreNumber(targetPointOfService.getStoreNumber());
        Mockito.verify(clonedAddressModel, Mockito.times(0)).setTitle(shippingAddress.getTitle());
        Mockito.verify(clonedAddressModel, Mockito.times(0)).setFirstname(shippingAddress.getFirstname());
        Mockito.verify(clonedAddressModel, Mockito.times(0)).setLastname(shippingAddress.getLastname());
        Mockito.verify(clonedAddressModel, Mockito.times(0)).setPhone1(shippingAddress.getPhone1());
        Mockito.verify(cartModel, Mockito.times(0)).setDeliveryAddress(clonedAddressModel);
        Mockito.verify(modelService).save(cartModel);
        Mockito.verify(sessionService).setAttribute(TgtCoreConstants.SESSION_POSTCODE, "3000");
    }

    @Test
    public void testPrePopulateForNonCnCOrderFromPreviousOrder() {
        Mockito.when(previousOrder.getDeliveryAddress()).thenReturn(shippingAddress);
        Mockito.when(shippingAddress.getPostalcode()).thenReturn("3000");
        Mockito.when(customer.getDefaultPaymentAddress()).thenReturn(null);
        Mockito.when(customer.getDefaultShipmentAddress()).thenReturn(null);
        targetCartService.prePopulateCart(cartModel, preferredDeliveryMode, customer, paymentInfo, previousOrder);
        Mockito.verify(cartModel).setPaymentInfo(paymentInfo);
        Mockito.verify(cartModel).setDeliveryMode(preferredDeliveryMode);
        Mockito.verify(cartModel).setPaymentAddress(previousOrder.getPaymentAddress());
        Mockito.verify(cartModel).setFlyBuysCode("123");
        Mockito.verify(cartModel, times(0)).setPaymentMode(paymentMode);
        Mockito.verify(cartModel).setDeliveryAddress(null);
        Mockito.verify(modelService).save(cartModel);
        Mockito.verify(sessionService, times(0)).setAttribute(TgtCoreConstants.SESSION_POSTCODE, "3000");
    }
}