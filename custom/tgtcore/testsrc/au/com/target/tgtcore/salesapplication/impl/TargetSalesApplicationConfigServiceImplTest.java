package au.com.target.tgtcore.salesapplication.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import org.apache.commons.collections.ListUtils;
import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.SalesApplicationConfigModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetSalesApplicationConfigServiceImplTest {

    private static final SalesApplication EBAY_SALES_APP = SalesApplication.EBAY;

    @InjectMocks
    private final TargetSalesApplicationConfigServiceImpl service = new TargetSalesApplicationConfigServiceImpl();

    @Mock
    private GenericDao<SalesApplicationConfigModel> salesApplicationConfigDao;

    @Mock
    private SalesApplicationConfigModel salesAppConfig;

    private final Map<String, SalesApplication> params = Collections
            .singletonMap(SalesApplicationConfigModel.SALESAPPLICATION, EBAY_SALES_APP);

    @Test
    public void testGetConfigForSalesApplicationNull() {
        Assert.assertNull(service.getConfigForSalesApplication(null));
    }

    @Test
    public void testGetConfigForSalesApplicationNotFound() {
        Mockito.doReturn(null).when(salesApplicationConfigDao).find(params);

        Assert.assertNull(service.getConfigForSalesApplication(EBAY_SALES_APP));
    }

    @Test
    public void testGetConfigForSalesApplication() {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);

        Assert.assertEquals(salesAppConfig, service.getConfigForSalesApplication(EBAY_SALES_APP));
    }

    @Test
    public void testGetAllPartnerSalesApplicationConfigsEmpty() {
        Mockito.doReturn(null).when(salesApplicationConfigDao).find();

        Assert.assertSame(ListUtils.EMPTY_LIST, service.getAllPartnerSalesApplicationConfigs());
    }

    @Test
    public void testGetAllPartnerSalesApplicationConfigs() {
        final SalesApplicationConfigModel nonPartnerConfig = new SalesApplicationConfigModel();
        nonPartnerConfig.setIsPartnerChannel(false);
        final SalesApplicationConfigModel partnerConfig = new SalesApplicationConfigModel();
        partnerConfig.setIsPartnerChannel(true);

        Mockito.doReturn(Arrays.asList(nonPartnerConfig, partnerConfig)).when(salesApplicationConfigDao).find();

        Assert.assertNotNull(service.getAllPartnerSalesApplicationConfigs());
        Assert.assertEquals(1, service.getAllPartnerSalesApplicationConfigs().size());
    }

    @Test
    public void testIsSkipFraudCheckNull() {
        Mockito.doReturn(null).when(salesApplicationConfigDao).find(params);

        Assert.assertFalse(service.isSkipFraudCheck(EBAY_SALES_APP));
    }

    @Test
    public void testIsSkipFraudCheckFalse() {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.FALSE).when(salesAppConfig).isSkipFraudCheck();

        Assert.assertFalse(service.isSkipFraudCheck(EBAY_SALES_APP));
    }

    @Test
    public void testIsSkipFraudCheckTrue() {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.TRUE).when(salesAppConfig).isSkipFraudCheck();

        Assert.assertTrue(service.isSkipFraudCheck(EBAY_SALES_APP));
    }

    @Test
    public void testIsDenyCancellationCheckFalse() {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.FALSE).when(salesAppConfig).isDenyCancellation();

        Assertions.assertThat(service.isDenyCancellation(EBAY_SALES_APP)).isFalse();
    }

    @Test
    public void testIsDenyCancellationCheckTrue() {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.TRUE).when(salesAppConfig).isDenyCancellation();

        Assertions.assertThat(service.isDenyCancellation(EBAY_SALES_APP)).isTrue();
    }

    @Test
    public void testIsSuppressTaxCalculationTrue() {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.TRUE).when(salesAppConfig).isSuppressTaxCalculation();

        Assert.assertTrue(service.isSuppressTaxCalculation(EBAY_SALES_APP));
    }

    @Test
    public void testIsSuppressTaxCalculationFalse() {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.FALSE).when(salesAppConfig).isSuppressTaxCalculation();

        Assert.assertFalse(service.isSuppressTaxCalculation(EBAY_SALES_APP));
    }

    @Test
    public void testIsSuppressTaxCalculationConfigMissing() {
        Mockito.doReturn(null).when(salesApplicationConfigDao).find(params);

        Assert.assertFalse(service.isSuppressTaxCalculation(EBAY_SALES_APP));
    }

    @Test
    public void testIsReserveStockOnAcceptOrderNull() throws Exception {
        Mockito.doReturn(null).when(salesApplicationConfigDao).find(params);

        Assert.assertFalse(service.isReserveStockOnAcceptOrder(EBAY_SALES_APP));
    }

    @Test
    public void testIsReserveStockOnAcceptOrderFalse() throws Exception {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.FALSE).when(salesAppConfig).isReserveStockOnAcceptOrder();

        Assert.assertFalse(service.isSuppressTaxCalculation(EBAY_SALES_APP));
    }

    @Test
    public void testIsReserveStockOnAcceptOrderTrue() throws Exception {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.TRUE).when(salesAppConfig).isReserveStockOnAcceptOrder();

        Assert.assertFalse(service.isSuppressTaxCalculation(EBAY_SALES_APP));
    }

    @Test
    public void testIsSuppressOrderConfMailTrue() {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.TRUE).when(salesAppConfig).isSuppressOrderConfMail();

        Assert.assertTrue(service.isSuppressOrderConfMail(EBAY_SALES_APP));
    }

    @Test
    public void testIsSuppressOrderConfMailFalse() {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.FALSE).when(salesAppConfig).isSuppressOrderConfMail();

        Assert.assertFalse(service.isSuppressOrderConfMail(EBAY_SALES_APP));
    }

    @Test
    public void testIsSuppressOrderConfMailMissing() {
        Mockito.doReturn(null).when(salesApplicationConfigDao).find(params);

        Assert.assertFalse(service.isSuppressOrderConfMail(EBAY_SALES_APP));

    }

    @Test
    public void testIsSuppressTaxInvoiceNull() {
        Mockito.doReturn(null).when(salesApplicationConfigDao).find(params);

        Assertions.assertThat(service.isSuppressTaxInvoice(EBAY_SALES_APP)).isFalse();
    }

    @Test
    public void testIsSuppressTaxInvoiceFalse() {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.FALSE).when(salesAppConfig).isSuppressTaxInvoice();

        Assertions.assertThat(service.isSuppressTaxInvoice(EBAY_SALES_APP)).isFalse();
    }

    @Test
    public void testIsSuppressTaxInvoiceTrue() {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.TRUE).when(salesAppConfig).isSuppressTaxInvoice();

        Assertions.assertThat(service.isSuppressTaxInvoice(EBAY_SALES_APP)).isTrue();
    }

    @Test
    public void testIsPartnerChannelNull() {
        Mockito.doReturn(null).when(salesApplicationConfigDao).find(params);
        Assertions.assertThat(service.isPartnerChannel(EBAY_SALES_APP)).isFalse();
    }

    @Test
    public void testIsPartnerChannelFalse() {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.FALSE).when(salesAppConfig).isIsPartnerChannel();
        Assertions.assertThat(service.isPartnerChannel(EBAY_SALES_APP)).isFalse();
    }

    @Test
    public void testIsPartnerChannelTrue() {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.TRUE).when(salesAppConfig).isIsPartnerChannel();
        Assertions.assertThat(service.isPartnerChannel(EBAY_SALES_APP)).isTrue();
    }

}
