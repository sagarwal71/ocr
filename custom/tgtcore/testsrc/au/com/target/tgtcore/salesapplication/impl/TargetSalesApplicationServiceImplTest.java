/**
 * 
 */
package au.com.target.tgtcore.salesapplication.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.servicelayer.session.MockSessionService;
import de.hybris.platform.servicelayer.session.SessionService;

import org.junit.Test;

import org.junit.Assert;


/**
 * Unit tests for TargetSalesApplicationService
 * 
 * @author jjayawa1
 * 
 */
@UnitTest
public class TargetSalesApplicationServiceImplTest {

    protected static final String SELECTED_SALES_APPLICATION = "Selected-Sales-Application";

    private final TargetSalesApplicationServiceImpl targetSalesApplicationService = new TargetSalesApplicationServiceImpl();

    @Test
    public void testSetCurrentSalesApplication() {
        final SalesApplication channel = SalesApplication.KIOSK;
        final SessionService sessionService = new MockSessionService();
        targetSalesApplicationService.setSessionService(sessionService);
        targetSalesApplicationService.setCurrentSalesApplication(SalesApplication.KIOSK);
        final SalesApplication detectedChannel = targetSalesApplicationService.getSessionService()
                .getAttribute(SELECTED_SALES_APPLICATION);
        Assert.assertEquals(channel, detectedChannel);
    }

    @Test
    public void testGetCurrentSalesApplication() {
        final SalesApplication channel = SalesApplication.KIOSK;
        final SessionService sessionService = new MockSessionService();
        sessionService.setAttribute(SELECTED_SALES_APPLICATION, channel);
        targetSalesApplicationService.setSessionService(sessionService);
        Assert.assertEquals(channel, targetSalesApplicationService.getCurrentSalesApplication());
    }
}
