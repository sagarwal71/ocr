/**
 * 
 */
package au.com.target.tgtcore.orderEntry.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.orderEntry.TargetOrderEntryService;
import au.com.target.tgtcore.product.TargetProductService;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFixMissingProductInOrderEntryStrategyTest {

    @Mock
    private TargetOrderEntryService targetOrderEntryService;
    @Mock
    private TargetProductService targetProductServiceImpl;
    @Mock
    private CatalogVersionService catalogVersionService;
    @Mock
    private ModelService modelService;
    @InjectMocks
    private final TargetFixMissingProductInOrderEntryStrategyImpl targetFixMissingProductInOrderEntryStrategy = new TargetFixMissingProductInOrderEntryStrategyImpl();

    @Test
    public void testFixMissingProductInOrderEntryWithEmptyList() {
        final List<OrderEntryModel> orderEntryMissingProduct = new ArrayList<>();
        BDDMockito.given(targetOrderEntryService.findOrderEntryMissingProductModel()).willReturn(
                orderEntryMissingProduct);
        targetFixMissingProductInOrderEntryStrategy.fixMissingProductInOrderEntry();
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testFixMissingProductInOrderEntry() {
        final OrderModel order = new OrderModel();
        order.setCode("testOrder");
        final List<OrderEntryModel> orderEntryMissingProduct = new ArrayList<>();
        final OrderEntryModel orderEntry1 = new OrderEntryModel();
        orderEntry1.setOrder(order);
        orderEntry1.setInfo("product \"12345678\" with name \"Swimwear product red M\"");
        orderEntryMissingProduct.add(orderEntry1);

        final ProductModel productModel = new ProductModel();
        productModel.setCode("12345678");
        BDDMockito
                .given(targetProductServiceImpl.getProductForCode(Mockito.any(CatalogVersionModel.class),
                        Mockito.anyString())).willReturn(productModel);

        BDDMockito.given(targetOrderEntryService.findOrderEntryMissingProductModel()).willReturn(
                orderEntryMissingProduct);
        BDDMockito.doNothing().when(modelService).save(productModel);

        targetFixMissingProductInOrderEntryStrategy.fixMissingProductInOrderEntry();
        Assert.assertEquals(productModel, orderEntry1.getProduct());
    }

    @Test
    public void testFixMissingProductInOrderEntryWithUnexpectedOrderInfoPattern() {
        final OrderModel order = new OrderModel();
        order.setCode("testOrder");
        final List<OrderEntryModel> orderEntryMissingProduct = new ArrayList<>();
        final OrderEntryModel orderEntry1 = new OrderEntryModel();
        orderEntry1.setOrder(order);
        orderEntry1.setInfo("product 12345678 with name Swimwear product red M");
        orderEntryMissingProduct.add(orderEntry1);

        final ProductModel productModel = new ProductModel();
        productModel.setCode("12345678");
        BDDMockito
                .given(targetProductServiceImpl.getProductForCode(Mockito.any(CatalogVersionModel.class),
                        Mockito.anyString())).willReturn(productModel);

        BDDMockito.given(targetOrderEntryService.findOrderEntryMissingProductModel()).willReturn(
                orderEntryMissingProduct);
        BDDMockito.doNothing().when(modelService).save(productModel);

        targetFixMissingProductInOrderEntryStrategy.fixMissingProductInOrderEntry();
        Mockito.verifyZeroInteractions(targetProductServiceImpl);
        Assert.assertEquals(null, orderEntry1.getProduct());
    }

    @Test
    public void testFixMissingProductInOrderEntryWithQuoteInProductName() {
        final OrderModel order = new OrderModel();
        order.setCode("testOrder");
        final List<OrderEntryModel> orderEntryMissingProduct = new ArrayList<>();
        final OrderEntryModel orderEntry1 = new OrderEntryModel();
        orderEntry1.setOrder(order);
        orderEntry1.setInfo("product \"12345678\" with name \"Swimwear \"test value in the quote\"product red M\"");
        orderEntryMissingProduct.add(orderEntry1);

        final ProductModel productModel = new ProductModel();
        productModel.setCode("12345678");
        BDDMockito
                .given(targetProductServiceImpl.getProductForCode(Mockito.any(CatalogVersionModel.class),
                        Mockito.anyString())).willReturn(productModel);

        BDDMockito.given(targetOrderEntryService.findOrderEntryMissingProductModel()).willReturn(
                orderEntryMissingProduct);
        BDDMockito.doNothing().when(modelService).save(productModel);

        targetFixMissingProductInOrderEntryStrategy.fixMissingProductInOrderEntry();
        targetFixMissingProductInOrderEntryStrategy.fixMissingProductInOrderEntry();
        Assert.assertEquals(productModel, orderEntry1.getProduct());
    }

    @Test
    public void testFixMissingProductInOrderEntryWithUnknowIdentifierException() {
        final OrderModel order = new OrderModel();
        order.setCode("testOrder");
        final List<OrderEntryModel> orderEntryMissingProduct = new ArrayList<>();
        final OrderEntryModel orderEntry1 = new OrderEntryModel();
        orderEntry1.setOrder(order);
        orderEntry1.setInfo("product \"P4036_red_M\" with name \"Swimwear product red M\"");
        orderEntryMissingProduct.add(orderEntry1);

        final ProductModel productModel = new ProductModel();
        productModel.setCode("P4036_red_M");
        BDDMockito
                .given(targetProductServiceImpl.getProductForCode(Mockito.any(CatalogVersionModel.class),
                        Mockito.anyString())).willThrow(new UnknownIdentifierException("unknowIdentifierTest"));

        BDDMockito.given(targetOrderEntryService.findOrderEntryMissingProductModel()).willReturn(
                orderEntryMissingProduct);

        targetFixMissingProductInOrderEntryStrategy.fixMissingProductInOrderEntry();
        Mockito.verifyZeroInteractions(modelService);
        Assert.assertEquals(null, orderEntry1.getProduct());
    }

    @Test
    public void testFixMissingProductInOrderEntryWithUnknowAmbiguousIdentifierException() {
        final OrderModel order = new OrderModel();
        order.setCode("testOrder");
        final List<OrderEntryModel> orderEntryMissingProduct = new ArrayList<>();
        final OrderEntryModel orderEntry1 = new OrderEntryModel();
        orderEntry1.setOrder(order);
        orderEntry1.setInfo("product \"P4036_red_M\" with name \"Swimwear product red M\"");
        orderEntryMissingProduct.add(orderEntry1);

        final ProductModel productModel = new ProductModel();
        productModel.setCode("P4036_red_M");
        BDDMockito
                .given(targetProductServiceImpl.getProductForCode(Mockito.any(CatalogVersionModel.class),
                        Mockito.anyString())).willThrow(
                        new AmbiguousIdentifierException("AmbiguousIdentifierExceptionTest"));

        BDDMockito.given(targetOrderEntryService.findOrderEntryMissingProductModel()).willReturn(
                orderEntryMissingProduct);

        targetFixMissingProductInOrderEntryStrategy.fixMissingProductInOrderEntry();
        Mockito.verifyZeroInteractions(modelService);
        Assert.assertEquals(null, orderEntry1.getProduct());
    }
}
