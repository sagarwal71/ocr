/**
 * 
 */
package au.com.target.tgtofcws.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.util.Assert;

import au.com.target.tgtofcws.interceptor.BeforeControllerInterceptor;


/**
 * @author smudumba
 *
 */

public class BaseController {



    /**
     * Common method for request to retrieve store number.
     * 
     * @param request
     *            http request
     * @return store number
     */
    public Integer getStoreNumber(final HttpServletRequest request) {
        final Object store = request.getAttribute(BeforeControllerInterceptor.EMPLOYEE_STORE);
        Integer storeNumber = null;
        //this should never be null, as login controller will block the request
        Assert.notNull(store, "Store Number is null");
        if (store instanceof Integer) {
            storeNumber = (Integer)store;
        }

        return storeNumber;


    }

}
