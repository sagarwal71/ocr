package au.com.target.tgtfacades.marketing.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;

import au.com.target.tgtfacades.marketing.data.SocialMediaProductsData;
import au.com.target.tgtmarketing.model.SocialMediaProductsModel;


@UnitTest
public class TargetSocialMediaProductsPopulatorTest {

    private final TargetSocialMediaProductsPopulator populator = new TargetSocialMediaProductsPopulator();

    @Test
    public void testPopulate() throws Exception {
        final String url = "https://instagram.com/p/9Dm36yGK1O/";
        final String productList = "12345678,34567890 , 33333333 ,P55555555, P99999999_nocolour";

        final SocialMediaProductsModel mockSocialMediaProducts = mock(SocialMediaProductsModel.class);
        given(mockSocialMediaProducts.getUrl()).willReturn(url);
        given(mockSocialMediaProducts.getProductList()).willReturn(productList);

        final SocialMediaProductsData result = new SocialMediaProductsData();
        populator.populate(mockSocialMediaProducts, result);

        assertThat(result.getUrl());
        assertThat(result.getProductCodes()).containsExactly("12345678", "34567890", "33333333", "P55555555",
                "P99999999_nocolour");
    }

}
