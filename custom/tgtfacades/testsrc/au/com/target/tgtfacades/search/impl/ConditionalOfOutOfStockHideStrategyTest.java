package au.com.target.tgtfacades.search.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.StockData;

import org.junit.Assert;

import org.junit.Test;

import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.search.TargetProductHideStrategy;


@UnitTest
public class ConditionalOfOutOfStockHideStrategyTest {
    private final TargetProductHideStrategy stategy = new ConditionalOutOfStockHideStrategy();
    private final StockData stockData = new StockData();

    @Test
    public void testHideThisProductWhenOutOfStockAndShowWhenOutOfStock() {
        final TargetProductData productData = new TargetProductData();
        stockData.setStockLevelStatus(StockLevelStatus.OUTOFSTOCK);
        productData.setStock(stockData);
        productData.setShowWhenOutOfStock(true);

        Assert.assertFalse(stategy.hideThisProduct(productData));
    }

    @Test
    public void testHideThisProductWhenInStockAndShowWhenOutOfStock() {
        final TargetProductData productData = new TargetProductData();
        stockData.setStockLevelStatus(StockLevelStatus.INSTOCK);
        productData.setStock(stockData);
        productData.setShowWhenOutOfStock(true);

        Assert.assertFalse(stategy.hideThisProduct(productData));
    }

    @Test
    public void testHideThisProductWhenOutOfStockAndNotShowWhenOutOfStock() {
        final TargetProductData productData = new TargetProductData();
        stockData.setStockLevelStatus(StockLevelStatus.OUTOFSTOCK);
        productData.setStock(stockData);
        productData.setShowWhenOutOfStock(false);

        Assert.assertTrue(stategy.hideThisProduct(productData));
    }

    @Test
    public void testHideThisProductWhenInStockAndNotShowWhenOutOfStock() {
        final TargetProductData productData = new TargetProductData();
        stockData.setStockLevelStatus(StockLevelStatus.INSTOCK);
        productData.setStock(stockData);
        productData.setShowWhenOutOfStock(false);

        Assert.assertFalse(stategy.hideThisProduct(productData));
    }

    @Test
    public void testHideThisProductVariantWhenOutOfStockAndShowWhenOutOfStock() {
        final TargetVariantProductListerData productData = new TargetVariantProductListerData();
        productData.setInStock(false);
        productData.setShowWhenOutOfStock(true);

        Assert.assertFalse(stategy.hideThisProduct(productData));
    }

    @Test
    public void testHideThisProductVariantWhenInStockAndShowWhenOutOfStock() {
        final TargetVariantProductListerData productData = new TargetVariantProductListerData();
        productData.setInStock(true);
        productData.setShowWhenOutOfStock(true);

        Assert.assertFalse(stategy.hideThisProduct(productData));
    }

    @Test
    public void testHideThisProductVariantWhenOutOfStockAndNotShowWhenOutOfStock() {
        final TargetVariantProductListerData productData = new TargetVariantProductListerData();
        productData.setInStock(false);
        productData.setShowWhenOutOfStock(false);

        Assert.assertTrue(stategy.hideThisProduct(productData));
    }

    @Test
    public void testHideThisProductVariantWhenInStockAndNotShowWhenOutOfStock() {
        final TargetVariantProductListerData productData = new TargetVariantProductListerData();
        productData.setInStock(true);
        productData.setShowWhenOutOfStock(false);

        Assert.assertFalse(stategy.hideThisProduct(productData));
    }



}
