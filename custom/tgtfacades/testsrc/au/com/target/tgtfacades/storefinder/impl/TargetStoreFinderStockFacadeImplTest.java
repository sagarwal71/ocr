/**
 * 
 */
package au.com.target.tgtfacades.storefinder.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemResponseDto;
import au.com.target.tgtcore.storefinder.TargetStoreFinderService;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtfacades.config.TargetSharedConfigFacade;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.response.data.Error;
import au.com.target.tgtfacades.response.data.LocationResponseData;
import au.com.target.tgtfacades.response.data.ProductStockSearchResultResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.storefinder.converters.TargetProductStoreStockConverter;
import au.com.target.tgtfacades.storefinder.data.StoreStockVisiblityHolder;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceStockData;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.service.FluentStockLookupService;
import au.com.target.tgtsale.stock.service.TargetStoreStockService;


/**
 * @author htan3
 *
 */
@SuppressWarnings("deprecation")
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetStoreFinderStockFacadeImplTest {

    private static final String POSTAL_CODE = "3148";

    private static final String SANITISED_LOCATION = "Chadstone";

    private static final String HD = "hd";

    private static final String CNC = "cnc";

    private static final String STATE_ACT = "act";

    private static final String STATE_NSW = "nsw";

    private static final String STATE_NT = "nt";

    private static final String STATE_QLD = "qld";

    private static final String STATE_SA = "sa";

    private static final String STATE_TAS = "tas";

    private static final String STATE_VIC = "vic";

    private static final String STATE_WA = "wa";

    private static final String HD_SHORT_LEAD_TIME = "HD Short Lead Time";

    private static final String ED_SHORT_LEAD_TIME = "ED Short Lead Time";

    private static final String BULKY1_SHORT_LEAD_TIME = "Bulky1 Short Lead Time";

    private static final String BULKY2_SHORT_LEAD_TIME = "Bulky2 Short Lead Time";

    @InjectMocks
    @Spy
    private final TargetStoreFinderStockFacadeImpl targetStoreFinderStockFacadeImpl = new TargetStoreFinderStockFacadeImpl();

    @Mock
    private BaseStoreService baseStoreService;

    @Mock
    private TargetStoreFinderService<PointOfServiceDistanceData, StoreFinderSearchPageData<PointOfServiceDistanceData>> storeFinderService;

    @Mock
    private ProductService productService;

    @Mock
    private TargetStoreStockService targetStoreStockService;

    @Mock
    private TargetProductStoreStockConverter storeStockConverter;

    @Mock
    private PageableData pageableData;

    @Mock
    private StoreFinderSearchPageData<PointOfServiceDistanceData> searchPageData;

    @Mock
    private PointOfServiceDistanceData posData;

    @Mock
    private BaseStoreModel baseStore;

    @Mock
    private TargetPointOfServiceModel pointOfServiceModel;

    @Mock
    private TargetPointOfServiceModel pointOfServiceModel1;

    @Mock
    private TargetPointOfServiceModel pointOfServiceModel2;

    @Mock
    private GeoPoint geoPoint;

    @Mock
    private TargetPointOfServiceStockData mockStoreStockData;

    @Mock
    private TargetSharedConfigFacade mockTargetSharedConfigFacade;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Mock
    private TargetPointOfServiceService mockTargetPointOfServiceService;

    @Mock
    private FluentStockLookupService fluentStockLookupService;

    @Mock
    private TargetProductModel mockProduct;

    @Captor
    private ArgumentCaptor<GeoPoint> geoPointCaptor;

    private final String location = "3030";

    private final String productCode = "P1001_Red_S";

    private final Double latitude = Double.valueOf(-38.1245357);

    private final Double longitude = Double.valueOf(144.3385314);

    private final String[] stockStatusLevels = { "no", "low", "high" };

    @Before
    public void setUp() {
        given(baseStoreService.getCurrentBaseStore()).willReturn(baseStore);
        given(storeFinderService.doSearchByLocation(baseStore, location, pageableData)).willReturn(searchPageData);
        given(storeFinderService.positionSearch(eq(baseStore), geoPointCaptor.capture(), eq(pageableData)))
                .willReturn(searchPageData);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isInStoreStockVisibilityEnabled();

        given(productService.getProductForCode(productCode)).willReturn(mockProduct);

        targetStoreFinderStockFacadeImpl.setStockStatusLevel(stockStatusLevels);
    }

    @Test
    public void testSearchStockByLocation() {
        setPosData();
        given(searchPageData.getLocationText()).willReturn("Geelong 3220, VIC");
        given(storeStockConverter.convert(any(StoreStockVisiblityHolder.class))).willReturn(mockStoreStockData);

        final StockVisibilityItemLookupResponseDto stockResult = new StockVisibilityItemLookupResponseDto();
        final StockVisibilityItemResponseDto stockVisibilityItemResponseDto = mock(
                StockVisibilityItemResponseDto.class);
        stockResult.setItems(ImmutableList.of(stockVisibilityItemResponseDto));
        given(targetStoreStockService.lookupStockForItemsInStores(any(List.class), any(List.class))).willReturn(
                stockResult);
        final ArgumentCaptor<StoreStockVisiblityHolder> storeStockVisiblityHolderCaptor = ArgumentCaptor
                .forClass(StoreStockVisiblityHolder.class);
        final Response response = targetStoreFinderStockFacadeImpl.doSearchProductStock(productCode, location, null,
                null, null, pageableData, true);
        verify(storeFinderService).doSearchByLocation(baseStore, location, pageableData);
        verify(targetStoreStockService).lookupStockForItemsInStores(any(List.class), any(List.class));
        verify(storeStockConverter).convert(storeStockVisiblityHolderCaptor.capture());
        final List<StoreStockVisiblityHolder> holders = storeStockVisiblityHolderCaptor.getAllValues();
        assertThat(holders).hasSize(1);
        assertThat(holders.get(0).getLocationText()).isEqualTo("Geelong 3220, VIC");
        assertThat(response.isSuccess()).isTrue();
        assertThat(((ProductStockSearchResultResponseData)response.getData()).getAvailabilityTime()).isNotEmpty();
        assertThat(((ProductStockSearchResultResponseData)response.getData()).getSelectedLocation()).isEqualTo(
                "Geelong 3220, VIC");
    }

    @Test
    public void testSearchStockByPosition() {
        setPosData();
        given(storeStockConverter.convert(any(StoreStockVisiblityHolder.class))).willReturn(mockStoreStockData);
        final ArgumentCaptor<StoreStockVisiblityHolder> storeStockVisiblityHolderCaptor = ArgumentCaptor
                .forClass(StoreStockVisiblityHolder.class);
        final StockVisibilityItemLookupResponseDto stockResult = new StockVisibilityItemLookupResponseDto();
        final StockVisibilityItemResponseDto stockVisibilityItemResponseDto = mock(
                StockVisibilityItemResponseDto.class);
        stockResult.setItems(ImmutableList.of(stockVisibilityItemResponseDto));
        given(targetStoreStockService.lookupStockForItemsInStores(any(List.class), any(List.class))).willReturn(
                stockResult);

        final Response response = targetStoreFinderStockFacadeImpl.doSearchProductStock(productCode, null,
                latitude,
                longitude, null, pageableData, true);

        assertThat(geoPointCaptor.getValue().getLatitude()).isEqualTo(latitude);
        assertThat(geoPointCaptor.getValue().getLongitude()).isEqualTo(longitude);

        verify(targetStoreStockService).lookupStockForItemsInStores(any(List.class), any(List.class));
        verifyZeroInteractions(targetStoreStockService);
        verify(storeStockConverter).convert(storeStockVisiblityHolderCaptor.capture());
        final List<StoreStockVisiblityHolder> holders = storeStockVisiblityHolderCaptor.getAllValues();
        assertThat(holders).hasSize(1);
        assertThat(holders.get(0).getLocationText()).isEqualTo("Current+Location");
        assertThat(response.isSuccess()).isTrue();
    }

    @Test
    public void testSearchStockByPositionUsingFluent() throws FluentClientException {
        setPosData();
        willReturn(Integer.valueOf(7001)).given(pointOfServiceModel).getStoreNumber();
        given(storeStockConverter.convert(any(StoreStockVisiblityHolder.class))).willReturn(mockStoreStockData);
        final ArgumentCaptor<StoreStockVisiblityHolder> storeStockVisiblityHolderCaptor = ArgumentCaptor
                .forClass(StoreStockVisiblityHolder.class);
        final StockVisibilityItemLookupResponseDto stockResult = new StockVisibilityItemLookupResponseDto();
        final StockVisibilityItemResponseDto stockVisibilityItemResponseDto = mock(
                StockVisibilityItemResponseDto.class);
        stockResult.setItems(ImmutableList.of(stockVisibilityItemResponseDto));
        given(targetStoreStockService.lookupStockForItemsInStores(any(List.class), any(List.class))).willReturn(
                stockResult);
        given(fluentStockLookupService.lookupStock(Arrays.asList(productCode), Arrays.asList("7001")))
                .willReturn(stockResult);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isFluentEnabled();

        final Response response = targetStoreFinderStockFacadeImpl.doSearchProductStock(productCode, null,
                latitude, longitude, null, pageableData, true);
        assertThat(geoPointCaptor.getValue().getLatitude()).isEqualTo(latitude);
        assertThat(geoPointCaptor.getValue().getLongitude()).isEqualTo(longitude);
        verifyZeroInteractions(targetStoreStockService);
        verify(storeStockConverter).convert(storeStockVisiblityHolderCaptor.capture());
        final List<StoreStockVisiblityHolder> holders = storeStockVisiblityHolderCaptor.getAllValues();
        assertThat(holders).hasSize(1);
        assertThat(holders.get(0).getLocationText()).isEqualTo("Current+Location");
        assertThat(response.isSuccess()).isTrue();
    }

    @Test
    public void testSearchStockByPositionUsingFluentWithException() throws FluentClientException {
        setPosData();
        willReturn(Integer.valueOf(7001)).given(pointOfServiceModel).getStoreNumber();
        given(storeStockConverter.convert(any(StoreStockVisiblityHolder.class))).willReturn(mockStoreStockData);
        final ArgumentCaptor<StoreStockVisiblityHolder> storeStockVisiblityHolderCaptor = ArgumentCaptor
                .forClass(StoreStockVisiblityHolder.class);
        final StockVisibilityItemLookupResponseDto stockResult = new StockVisibilityItemLookupResponseDto();
        final StockVisibilityItemResponseDto stockVisibilityItemResponseDto = mock(
                StockVisibilityItemResponseDto.class);
        stockResult.setItems(ImmutableList.of(stockVisibilityItemResponseDto));
        given(targetStoreStockService.lookupStockForItemsInStores(any(List.class), any(List.class))).willReturn(
                stockResult);
        given(fluentStockLookupService.lookupStock(Arrays.asList(productCode), Collections.<String> emptyList(),
                Arrays.asList("7001"))).willThrow(new FluentClientException(new au.com.target.tgtfluent.data.Error()));
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isFluentEnabled();

        final Response response = targetStoreFinderStockFacadeImpl.doSearchProductStock(productCode, null,
                latitude, longitude, null, pageableData, true);

        assertThat(geoPointCaptor.getValue().getLatitude()).isEqualTo(latitude);
        assertThat(geoPointCaptor.getValue().getLongitude()).isEqualTo(longitude);

        verifyZeroInteractions(targetStoreStockService);
        verifyZeroInteractions(storeStockConverter);
        final List<StoreStockVisiblityHolder> holders = storeStockVisiblityHolderCaptor.getAllValues();
        assertThat(holders).hasSize(0);
        assertThat(response.isSuccess()).isFalse();
    }

    @Test
    public void testSearchStockWithInvalidInput() {
        setPosData();
        final Response response = targetStoreFinderStockFacadeImpl.doSearchProductStock(productCode, null, null, null,
                null, pageableData, true);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_CHECK_STOCK_LOCATION_NOTFOUND");
    }

    @Test
    public void testSearchStockWithServiceFailure() {
        setPosData();
        given(storeStockConverter.convert(any(StoreStockVisiblityHolder.class))).willReturn(mockStoreStockData);

        final StockVisibilityItemLookupResponseDto stockResult = new StockVisibilityItemLookupResponseDto();
        given(targetStoreStockService.lookupStockForItemsInStores(any(List.class), any(List.class))).willReturn(
                stockResult);

        final Response response = targetStoreFinderStockFacadeImpl.doSearchProductStock(productCode, null,
                latitude,
                longitude, null, pageableData, true);

        assertThat(geoPointCaptor.getValue().getLatitude()).isEqualTo(latitude);
        assertThat(geoPointCaptor.getValue().getLongitude()).isEqualTo(longitude);

        verify(targetStoreStockService).lookupStockForItemsInStores(any(List.class), any(List.class));
        verifyZeroInteractions(targetStoreStockService);
        verifyZeroInteractions(storeStockConverter);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_CHECK_STOCK_SERVICE_FAILURE");
    }

    @Test
    public void testSearchStockWithServiceError() {
        setPosData();
        given(storeStockConverter.convert(any(StoreStockVisiblityHolder.class))).willReturn(mockStoreStockData);
        given(targetStoreStockService.lookupStockForItemsInStores(any(List.class), any(List.class))).willReturn(null);

        final Response response = targetStoreFinderStockFacadeImpl.doSearchProductStock(productCode, null,
                latitude,
                longitude, null, pageableData, true);

        assertThat(geoPointCaptor.getValue().getLatitude()).isEqualTo(latitude);
        assertThat(geoPointCaptor.getValue().getLongitude()).isEqualTo(longitude);

        verify(targetStoreStockService).lookupStockForItemsInStores(any(List.class), any(List.class));
        verifyZeroInteractions(targetStoreStockService);
        verifyZeroInteractions(storeStockConverter);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_CHECK_STOCK_SERVICE_ERROR");
    }

    @Test
    public void testSearchStockWithServiceDisabled() {
        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isInStoreStockVisibilityEnabled();
        setPosData();
        given(storeStockConverter.convert(any(StoreStockVisiblityHolder.class))).willReturn(mockStoreStockData);
        given(targetStoreStockService.lookupStockForItemsInStores(any(List.class), any(List.class))).willReturn(null);

        final Response response = targetStoreFinderStockFacadeImpl.doSearchProductStock(productCode, null,
                Double.valueOf(10d),
                Double.valueOf(10d), null, pageableData, true);
        verify(storeFinderService, times(0)).positionSearch(baseStore, geoPoint, pageableData);
        verify(targetStoreStockService, times(0)).lookupStockForItemsInStores(any(List.class), any(List.class));
        verifyZeroInteractions(targetStoreStockService);
        verifyZeroInteractions(storeStockConverter);
        assertThat(response.isSuccess()).isFalse();
        final Error error = response.getData().getError();
        assertThat(error.getCode()).isEqualTo("ERR_CHECK_STOCK_SERVICE_ERROR");
        assertThat(error.getMessage()).isEqualTo("Sorry, the store stock service is disabled.");
    }

    @Test
    public void testDoSearchProductStockByLocationNoProduct() {
        final PointOfServiceDistanceData storeData = new PointOfServiceDistanceData();
        storeData.setPointOfService(pointOfServiceModel);
        given(searchPageData.getResults()).willReturn(ImmutableList.of(storeData));
        given(searchPageData.getLocationText()).willReturn("Geelong 3220, VIC");

        final ArgumentCaptor<StoreStockVisiblityHolder> storeStockVisiblityHolderCaptor = ArgumentCaptor
                .forClass(StoreStockVisiblityHolder.class);
        given(storeStockConverter.convert(storeStockVisiblityHolderCaptor.capture())).willReturn(mockStoreStockData);


        final Response response = targetStoreFinderStockFacadeImpl.doSearchProductStock(null, location, null,
                null, null, pageableData, true);


        verify(storeFinderService).doSearchByLocation(baseStore, location, pageableData);

        assertThat(response.isSuccess()).isTrue();

        final List<StoreStockVisiblityHolder> holders = storeStockVisiblityHolderCaptor.getAllValues();

        assertThat(holders).hasSize(1);
        assertThat(holders.get(0).getLocationText()).isEqualTo("Geelong 3220, VIC");
        assertThat(holders.get(0).getPointOfServiceDistanceData()).isEqualTo(storeData);

        assertThat(response.getData()).isInstanceOf(ProductStockSearchResultResponseData.class);
        final ProductStockSearchResultResponseData data = (ProductStockSearchResultResponseData)response.getData();

        assertThat(data.getAvailabilityTime()).isNotEmpty();
        assertThat(data.getSelectedLocation()).isEqualTo("Geelong 3220, VIC");
        assertThat(data.getError()).isNull();
        assertThat(data.isStockAvailable()).isFalse();
        assertThat(data.isStockDataIncluded()).isFalse();

        assertThat(data.getStores()).hasSize(1);
        assertThat(data.getStores().get(0)).isEqualTo(mockStoreStockData);
    }

    @Test
    public void testDoSearchProductStockByCoordinatesNoProduct() {
        final PointOfServiceDistanceData storeData = new PointOfServiceDistanceData();
        storeData.setPointOfService(pointOfServiceModel);
        given(searchPageData.getResults()).willReturn(ImmutableList.of(storeData));
        given(searchPageData.getLocationText()).willReturn("Geelong 3220, VIC");

        final ArgumentCaptor<StoreStockVisiblityHolder> storeStockVisiblityHolderCaptor = ArgumentCaptor
                .forClass(StoreStockVisiblityHolder.class);
        given(storeStockConverter.convert(storeStockVisiblityHolderCaptor.capture())).willReturn(mockStoreStockData);


        final Response response = targetStoreFinderStockFacadeImpl.doSearchProductStock(null, null,
                latitude, longitude, null, pageableData, true);


        assertThat(response.isSuccess()).isTrue();

        final List<StoreStockVisiblityHolder> holders = storeStockVisiblityHolderCaptor.getAllValues();

        assertThat(holders).hasSize(1);
        assertThat(holders.get(0).getLocationText()).isEqualTo("Geelong 3220, VIC");
        assertThat(holders.get(0).getPointOfServiceDistanceData()).isEqualTo(storeData);

        assertThat(response.getData()).isInstanceOf(ProductStockSearchResultResponseData.class);
        final ProductStockSearchResultResponseData data = (ProductStockSearchResultResponseData)response.getData();

        assertThat(data.getAvailabilityTime()).isNotEmpty();
        assertThat(data.getSelectedLocation()).isEqualTo("Geelong 3220, VIC");
        assertThat(data.getError()).isNull();
        assertThat(data.isStockAvailable()).isFalse();
        assertThat(data.isStockDataIncluded()).isFalse();

        assertThat(data.getStores()).hasSize(1);
        assertThat(data.getStores().get(0)).isEqualTo(mockStoreStockData);

        assertThat(geoPointCaptor.getValue().getLatitude()).isEqualTo(latitude.doubleValue());
        assertThat(geoPointCaptor.getValue().getLongitude()).isEqualTo(longitude.doubleValue());
    }

    @Test
    public void testDoSearchProductStockByStoreNumberNoProduct()
            throws TargetAmbiguousIdentifierException, TargetUnknownIdentifierException {
        final Double posLatitude = Double.valueOf(-38.1503551);
        final Double posLongitude = Double.valueOf(144.3593311);

        final PointOfServiceDistanceData storeData = new PointOfServiceDistanceData();
        storeData.setPointOfService(pointOfServiceModel);
        given(searchPageData.getResults()).willReturn(ImmutableList.of(storeData));
        given(searchPageData.getLocationText()).willReturn("Geelong 3220, VIC");

        final ArgumentCaptor<StoreStockVisiblityHolder> storeStockVisiblityHolderCaptor = ArgumentCaptor
                .forClass(StoreStockVisiblityHolder.class);
        given(storeStockConverter.convert(storeStockVisiblityHolderCaptor.capture())).willReturn(mockStoreStockData);

        final TargetPointOfServiceModel mockPos = mock(TargetPointOfServiceModel.class);
        given(mockPos.getLatitude()).willReturn(posLatitude);
        given(mockPos.getLongitude()).willReturn(posLongitude);

        given(mockTargetPointOfServiceService.getPOSByStoreNumber(Integer.valueOf(5100))).willReturn(mockPos);

        final Response response = targetStoreFinderStockFacadeImpl.doSearchProductStock(null, null,
                null, null, Integer.valueOf(5100), pageableData, true);


        assertThat(response.isSuccess()).isTrue();

        final List<StoreStockVisiblityHolder> holders = storeStockVisiblityHolderCaptor.getAllValues();

        assertThat(holders).hasSize(1);
        assertThat(holders.get(0).getLocationText()).isEqualTo("Geelong 3220, VIC");
        assertThat(holders.get(0).getPointOfServiceDistanceData()).isEqualTo(storeData);

        assertThat(response.getData()).isInstanceOf(ProductStockSearchResultResponseData.class);
        final ProductStockSearchResultResponseData data = (ProductStockSearchResultResponseData)response.getData();

        assertThat(data.getAvailabilityTime()).isNotEmpty();
        assertThat(data.getSelectedLocation()).isEqualTo("Geelong 3220, VIC");
        assertThat(data.getError()).isNull();
        assertThat(data.isStockAvailable()).isFalse();
        assertThat(data.isStockDataIncluded()).isFalse();

        assertThat(data.getStores()).hasSize(1);
        assertThat(data.getStores().get(0)).isEqualTo(mockStoreStockData);

        assertThat(geoPointCaptor.getValue().getLatitude()).isEqualTo(posLatitude.doubleValue());
        assertThat(geoPointCaptor.getValue().getLongitude()).isEqualTo(posLongitude.doubleValue());
    }

    @Test
    public void testDoSearchProductStockByInvalidStoreNumber()
            throws TargetAmbiguousIdentifierException, TargetUnknownIdentifierException {
        setPosData();
        given(searchPageData.getLocationText()).willReturn("Geelong 3220, VIC");

        final ArgumentCaptor<StoreStockVisiblityHolder> storeStockVisiblityHolderCaptor = ArgumentCaptor
                .forClass(StoreStockVisiblityHolder.class);
        given(storeStockConverter.convert(storeStockVisiblityHolderCaptor.capture())).willReturn(mockStoreStockData);

        given(mockTargetPointOfServiceService.getPOSByStoreNumber(Integer.valueOf(5001)))
                .willThrow(new TargetUnknownIdentifierException("Exception!"));


        final Response response = targetStoreFinderStockFacadeImpl.doSearchProductStock(null, null,
                null, null, Integer.valueOf(5001), pageableData, true);


        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_CHECK_STOCK_LOCATION_NOTFOUND");
    }

    @Test
    public void testDoSearchProductStockByStoreNumber()
            throws TargetAmbiguousIdentifierException, TargetUnknownIdentifierException {
        final Double posLatitude = Double.valueOf(-38.1503551);
        final Double posLongitude = Double.valueOf(144.3593311);

        final Integer storeNumber = Integer.valueOf(5100);
        final Integer storeNumber2 = Integer.valueOf(5001);

        final TargetPointOfServiceModel mockPos = mock(TargetPointOfServiceModel.class);
        given(mockPos.getLatitude()).willReturn(posLatitude);
        given(mockPos.getLongitude()).willReturn(posLongitude);
        given(mockPos.getStoreNumber()).willReturn(storeNumber);

        given(mockTargetPointOfServiceService.getPOSByStoreNumber(storeNumber)).willReturn(mockPos);

        final PointOfServiceDistanceData storeData = new PointOfServiceDistanceData();
        storeData.setPointOfService(mockPos);

        final TargetPointOfServiceModel mockPos2 = mock(TargetPointOfServiceModel.class);
        given(mockPos2.getStoreNumber()).willReturn(storeNumber2);

        final PointOfServiceDistanceData storeData2 = new PointOfServiceDistanceData();
        storeData2.setPointOfService(mockPos2);

        final List<PointOfServiceDistanceData> stores = new ArrayList<>();
        stores.add(storeData);
        stores.add(storeData2);

        given(searchPageData.getResults()).willReturn(stores);
        given(searchPageData.getLocationText()).willReturn("Geelong 3220, VIC");

        final TargetPointOfServiceStockData storeStockDataNo = new TargetPointOfServiceStockData();
        storeStockDataNo.setStockLevel("no");

        final TargetPointOfServiceStockData storeStockDataHigh = new TargetPointOfServiceStockData();
        storeStockDataHigh.setStockLevel("high");


        final ArgumentCaptor<StoreStockVisiblityHolder> storeStockVisiblityHolderCaptor = ArgumentCaptor
                .forClass(StoreStockVisiblityHolder.class);
        given(storeStockConverter.convert(storeStockVisiblityHolderCaptor.capture())).willReturn(storeStockDataNo,
                storeStockDataHigh);

        final StockVisibilityItemResponseDto stockVisResponse = new StockVisibilityItemResponseDto();

        final StockVisibilityItemLookupResponseDto stockResult = new StockVisibilityItemLookupResponseDto();
        stockResult.setItems(Collections.singletonList(stockVisResponse));

        final ArgumentCaptor<List> storeNumbersCaptor = ArgumentCaptor.forClass(List.class);
        final ArgumentCaptor<List> itemcodesCaptor = ArgumentCaptor.forClass(List.class);
        given(targetStoreStockService.lookupStockForItemsInStores(storeNumbersCaptor.capture(),
                itemcodesCaptor.capture())).willReturn(stockResult);

        final Response response = targetStoreFinderStockFacadeImpl.doSearchProductStock(productCode, null,
                null, null, storeNumber, pageableData, true);


        assertThat(response.isSuccess()).isTrue();

        final List<StoreStockVisiblityHolder> holders = storeStockVisiblityHolderCaptor.getAllValues();

        assertThat(holders).hasSize(2);
        // These assertions are due to the same holder being reused, so the last value set will be the one for all values
        assertThat(holders).onProperty("locationText").containsExactly("Geelong 3220, VIC", "Geelong 3220, VIC");
        assertThat(holders).onProperty("pointOfServiceDistanceData").containsExactly(storeData2, storeData2);

        assertThat(response.getData()).isInstanceOf(ProductStockSearchResultResponseData.class);
        final ProductStockSearchResultResponseData data = (ProductStockSearchResultResponseData)response.getData();

        assertThat(data.getAvailabilityTime()).isNotEmpty();
        assertThat(data.getSelectedLocation()).isEqualTo("Geelong 3220, VIC");
        assertThat(data.getError()).isNull();
        assertThat(data.isStockAvailable()).isTrue();
        assertThat(data.isStockDataIncluded()).isTrue();

        // When not sorted, the top store has stock despite that store being lower on the list by distance
        assertThat(data.getStores()).hasSize(2);
        assertThat(data.getStores().get(0)).isEqualTo(storeStockDataHigh);
        assertThat(data.getStores().get(1)).isEqualTo(storeStockDataNo);

        assertThat(geoPointCaptor.getValue().getLatitude()).isEqualTo(posLatitude.doubleValue());
        assertThat(geoPointCaptor.getValue().getLongitude()).isEqualTo(posLongitude.doubleValue());

        final List<String> capturedStoreNumbers = storeNumbersCaptor.getValue();
        assertThat(capturedStoreNumbers).hasSize(2);
        assertThat(capturedStoreNumbers.get(0)).isEqualTo(storeNumber.toString());
        assertThat(capturedStoreNumbers.get(1)).isEqualTo(storeNumber2.toString());

        final List<String> capturedItemcodes = itemcodesCaptor.getValue();
        assertThat(capturedItemcodes).hasSize(1);
        assertThat(capturedItemcodes.get(0)).isEqualTo(productCode);
    }

    @Test
    public void testDoSearchProductStockByStoreNumberNoReordering()
            throws TargetAmbiguousIdentifierException, TargetUnknownIdentifierException {
        final Double posLatitude = Double.valueOf(-38.1503551);
        final Double posLongitude = Double.valueOf(144.3593311);

        final Integer storeNumber = Integer.valueOf(5100);
        final Integer storeNumber2 = Integer.valueOf(5001);

        final TargetPointOfServiceModel mockPos = mock(TargetPointOfServiceModel.class);
        given(mockPos.getLatitude()).willReturn(posLatitude);
        given(mockPos.getLongitude()).willReturn(posLongitude);
        given(mockPos.getStoreNumber()).willReturn(storeNumber);

        given(mockTargetPointOfServiceService.getPOSByStoreNumber(storeNumber)).willReturn(mockPos);

        final PointOfServiceDistanceData storeData = new PointOfServiceDistanceData();
        storeData.setPointOfService(mockPos);

        final TargetPointOfServiceModel mockPos2 = mock(TargetPointOfServiceModel.class);
        given(mockPos2.getStoreNumber()).willReturn(storeNumber2);

        final PointOfServiceDistanceData storeData2 = new PointOfServiceDistanceData();
        storeData2.setPointOfService(mockPos2);

        final List<PointOfServiceDistanceData> stores = new ArrayList<>();
        stores.add(storeData);
        stores.add(storeData2);

        given(searchPageData.getResults()).willReturn(stores);
        given(searchPageData.getLocationText()).willReturn("Geelong 3220, VIC");

        final TargetPointOfServiceStockData storeStockDataNo = new TargetPointOfServiceStockData();
        storeStockDataNo.setStockLevel("no");

        final TargetPointOfServiceStockData storeStockDataHigh = new TargetPointOfServiceStockData();
        storeStockDataHigh.setStockLevel("high");


        final ArgumentCaptor<StoreStockVisiblityHolder> storeStockVisiblityHolderCaptor = ArgumentCaptor
                .forClass(StoreStockVisiblityHolder.class);
        given(storeStockConverter.convert(storeStockVisiblityHolderCaptor.capture())).willReturn(storeStockDataNo,
                storeStockDataHigh);

        final StockVisibilityItemResponseDto stockVisResponse = new StockVisibilityItemResponseDto();

        final StockVisibilityItemLookupResponseDto stockResult = new StockVisibilityItemLookupResponseDto();
        stockResult.setItems(Collections.singletonList(stockVisResponse));

        final ArgumentCaptor<List> storeNumbersCaptor = ArgumentCaptor.forClass(List.class);
        final ArgumentCaptor<List> itemcodesCaptor = ArgumentCaptor.forClass(List.class);
        given(targetStoreStockService.lookupStockForItemsInStores(storeNumbersCaptor.capture(),
                itemcodesCaptor.capture())).willReturn(stockResult);

        final Response response = targetStoreFinderStockFacadeImpl.doSearchProductStock(productCode, null,
                null, null, storeNumber, pageableData, false);


        assertThat(response.isSuccess()).isTrue();

        final List<StoreStockVisiblityHolder> holders = storeStockVisiblityHolderCaptor.getAllValues();

        assertThat(holders).hasSize(2);

        // These assertions are due to the same holder being reused, so the last value set will be the one for all values
        assertThat(holders).onProperty("locationText").containsExactly("Geelong 3220, VIC", "Geelong 3220, VIC");
        assertThat(holders).onProperty("pointOfServiceDistanceData").containsExactly(storeData2, storeData2);

        assertThat(response.getData()).isInstanceOf(ProductStockSearchResultResponseData.class);
        final ProductStockSearchResultResponseData data = (ProductStockSearchResultResponseData)response.getData();

        assertThat(data.getAvailabilityTime()).isNotEmpty();
        assertThat(data.getSelectedLocation()).isEqualTo("Geelong 3220, VIC");
        assertThat(data.getError()).isNull();
        assertThat(data.isStockAvailable()).isTrue();
        assertThat(data.isStockDataIncluded()).isTrue();

        // When not sorted, the top store has no stock
        assertThat(data.getStores()).hasSize(2);
        assertThat(data.getStores().get(0)).isEqualTo(storeStockDataNo);
        assertThat(data.getStores().get(1)).isEqualTo(storeStockDataHigh);

        assertThat(geoPointCaptor.getValue().getLatitude()).isEqualTo(posLatitude.doubleValue());
        assertThat(geoPointCaptor.getValue().getLongitude()).isEqualTo(posLongitude.doubleValue());

        final List<String> capturedStoreNumbers = storeNumbersCaptor.getValue();
        assertThat(capturedStoreNumbers).hasSize(2);
        assertThat(capturedStoreNumbers.get(0)).isEqualTo(storeNumber.toString());
        assertThat(capturedStoreNumbers.get(1)).isEqualTo(storeNumber2.toString());

        final List<String> capturedItemcodes = itemcodesCaptor.getValue();
        assertThat(capturedItemcodes).hasSize(1);
        assertThat(capturedItemcodes.get(0)).isEqualTo(productCode);
    }

    /**
     * Method to verify nearest cnc store delivery location. Here the method accepting location parameter and store
     * location is used to fetch nearby store details available for cnc.
     */
    @Test
    public void testNearestClickAndCollectStoreLocationWithLocationParameter() {
        given(storeFinderService.doSearchClickAndCollectStoresByLocation(baseStore, SANITISED_LOCATION, pageableData))
                .willReturn(searchPageData);
        setPosData();
        final TargetPointOfServiceStockData targetPointOfServiceStockData = pointOfServiceStoreData();
        given(storeStockConverter.convert(any(StoreStockVisiblityHolder.class)))
                .willReturn(targetPointOfServiceStockData);
        final Response response = targetStoreFinderStockFacadeImpl.nearestStoreLocation(CNC,
                SANITISED_LOCATION, null, pageableData);
        assertThat(response.isSuccess()).isTrue();
        final LocationResponseData locationResponseData = (LocationResponseData)response.getData();
        assertThat(locationResponseData.getClickAndCollectPos()).isNotNull();
        assertThat(locationResponseData.getClickAndCollectPos().getName()).isNotEmpty().isEqualTo(SANITISED_LOCATION);
        assertThat(locationResponseData.getClickAndCollectPos().getAddress().getPostalCode()).isNotEmpty()
                .isEqualTo(POSTAL_CODE);
        assertThat(locationResponseData.getClickAndCollectPos().getAddress()).isNotNull()
                .isEqualTo(targetPointOfServiceStockData.getAddress());
    }

    /**
     * Method to verify nearest cnc store delivery location. Here the method accepting store number parameter and is
     * used to fetch nearby store details available for cnc.
     *
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    @Test
    public void testNearestClickAndCollectStoreLocationWithStoreNumberParameter()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final Integer storeNumber = Integer.valueOf(7000);
        final TargetPointOfServiceModel mockPos = mock(TargetPointOfServiceModel.class);
        given(mockPos.getStoreNumber()).willReturn(storeNumber);
        given(mockPos.getLongitude()).willReturn(longitude);
        given(mockPos.getLatitude()).willReturn(latitude);
        given(mockTargetPointOfServiceService.getPOSByStoreNumber(storeNumber)).willReturn(mockPos);
        setPosData();
        final TargetPointOfServiceStockData targetPointOfServiceStockData = pointOfServiceStoreData();
        given(storeStockConverter.convert(any(StoreStockVisiblityHolder.class)))
                .willReturn(targetPointOfServiceStockData);
        final StoreFinderSearchPageData<PointOfServiceDistanceData> searchPageResultData = new StoreFinderSearchPageData();
        final List<PointOfServiceDistanceData> posDataList = new ArrayList<>();
        final PointOfServiceDistanceData storeData = new PointOfServiceDistanceData();
        posDataList.add(storeData);
        searchPageResultData.setResults(posDataList);
        given(storeFinderService.doSearchNearestClickAndCollectLocation((String)isNull(),
                any(GeoPoint.class), eq(pageableData))).willReturn(searchPageResultData);
        final Response response = targetStoreFinderStockFacadeImpl.nearestStoreLocation(CNC, null,
                Integer.valueOf(7000), pageableData);
        assertThat(response.isSuccess()).isTrue();
        final LocationResponseData locationResponseData = (LocationResponseData)response.getData();
        assertThat(locationResponseData.getClickAndCollectPos()).isNotNull();
        assertThat(locationResponseData.getClickAndCollectPos().getName()).isNotEmpty().isEqualTo(SANITISED_LOCATION);
        assertThat(locationResponseData.getClickAndCollectPos().getAddress().getPostalCode()).isNotEmpty()
                .isEqualTo(POSTAL_CODE);
        assertThat(locationResponseData.getClickAndCollectPos().getAddress()).isNotNull()
                .isEqualTo(targetPointOfServiceStockData.getAddress());
    }

    /**
     * This method is to verify the if store number and store location input is not provided for cnc delivery type.
     * Basically this method will throw an exception.
     */
    @Test
    public void testSearchCncStoreLocationWithEmptyLocationDetails() {
        final Response response = targetStoreFinderStockFacadeImpl.nearestStoreLocation(CNC, null, null,
                pageableData);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode())
                .isEqualTo("STORE_LOCATION_NOTFOUND_OR_FOUND_MORE_THAN_ONE_LOCATION");
    }

    /**
     * Method to verify nearest Home delivery location. Here the method accepting location parameter and store location
     * is used to fetch nearby store details available for home delivery.
     */
    @Test
    public void testNearestHomeDeliveryStoreLocationWithLocationParameter() {
        given(storeFinderService.doSearchByLocation(baseStore, SANITISED_LOCATION, pageableData))
                .willReturn(searchPageData);
        setPosData();
        final TargetPointOfServiceStockData targetPointOfServiceStockData = pointOfServiceStoreData();
        given(storeStockConverter.convert(any(StoreStockVisiblityHolder.class)))
                .willReturn(targetPointOfServiceStockData);
        final Response response = targetStoreFinderStockFacadeImpl.nearestStoreLocation(HD,
                SANITISED_LOCATION, null, pageableData);
        assertThat(response.isSuccess()).isTrue();
        final LocationResponseData locationResponseData = (LocationResponseData)response.getData();
        assertThat(locationResponseData.getHomeDeliveryPos()).isNotNull();
        assertThat(locationResponseData.getHomeDeliveryPos().getSuburb()).isNotEmpty()
                .isEqualTo(SANITISED_LOCATION);
        assertThat(locationResponseData.getHomeDeliveryPos().getPostalCode()).isNotEmpty()
                .isEqualTo(POSTAL_CODE);
    }

    /**
     * Method to verify nearest home delivery location. Here the method accepting store number parameter and is
     * used to fetch nearby store details available for home delivery.
     *
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    @Test
    public void testNearestHomeDeliveryStoreLocationWithStoreNumberParameter()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final Integer storeNumber = Integer.valueOf(7000);
        final TargetPointOfServiceModel mockPos = mock(TargetPointOfServiceModel.class);
        given(mockPos.getStoreNumber()).willReturn(storeNumber);
        given(mockPos.getLongitude()).willReturn(longitude);
        given(mockPos.getLatitude()).willReturn(latitude);
        given(mockTargetPointOfServiceService.getPOSByStoreNumber(storeNumber)).willReturn(mockPos);
        setPosData();
        final TargetPointOfServiceStockData targetPointOfServiceStockData = pointOfServiceStoreData();
        given(storeStockConverter.convert(any(StoreStockVisiblityHolder.class)))
                .willReturn(targetPointOfServiceStockData);
        final StoreFinderSearchPageData<PointOfServiceDistanceData> searchPageResultData = new StoreFinderSearchPageData();
        final List<PointOfServiceDistanceData> posDataList = new ArrayList<>();
        final PointOfServiceDistanceData storeData = new PointOfServiceDistanceData();
        posDataList.add(storeData);
        searchPageResultData.setResults(posDataList);
        given(storeFinderService.positionSearch(eq(baseStore),
                any(GeoPoint.class), eq(pageableData))).willReturn(searchPageResultData);
        final Response response = targetStoreFinderStockFacadeImpl.nearestStoreLocation(HD, null,
                Integer.valueOf(7000), pageableData);
        assertThat(response.isSuccess()).isTrue();
        final LocationResponseData locationResponseData = (LocationResponseData)response.getData();
        assertThat(locationResponseData.getHomeDeliveryPos()).isNotNull();
        assertThat(locationResponseData.getHomeDeliveryPos().getSuburb()).isNotEmpty()
                .isEqualTo(SANITISED_LOCATION);
        assertThat(locationResponseData.getHomeDeliveryPos().getPostalCode()).isNotEmpty()
                .isEqualTo(POSTAL_CODE);
    }

    /**
     * This method is to verify the if store number and store location input is not provided for home delivery type.
     * Basically this method will throw an exception.
     */
    @Test
    public void testNearestHomeDeliveryStoreLocationWithEmptyLocationDetails() {
        setPosData();
        final Response response = targetStoreFinderStockFacadeImpl.nearestStoreLocation(HD, null, null,
                pageableData);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode())
                .isEqualTo("STORE_LOCATION_NOTFOUND_OR_FOUND_MORE_THAN_ONE_LOCATION");
    }

    /**
     * Method to verify short lead times for the state ACT(Australian Capital Territory). Short Lead Times will be
     * captured for HD, ED, Bulky1 and Bulky2 delivery modes.
     */
    @Test
    public void testNearestStoreLocationWithHDShortLeadTimeForStateACT() {
        mockLeadTimeForState(STATE_ACT);
        final Response response = targetStoreFinderStockFacadeImpl.nearestStoreLocation(HD,
                SANITISED_LOCATION, null, pageableData);
        verifyLeadTimeForState(response);
    }

    /**
     * Method to verify short lead times for the state NSW(New South Wales). Short Lead Times will be captured for HD,
     * ED, Bulky1 and Bulky2 delivery modes.
     */
    @Test
    public void testNearestStoreLocationWithHDShortLeadTimeForStateNSW() {
        mockLeadTimeForState(STATE_NSW);
        final Response response = targetStoreFinderStockFacadeImpl.nearestStoreLocation(HD,
                SANITISED_LOCATION, null, pageableData);
        verifyLeadTimeForState(response);
    }

    /**
     * Method to verify short lead times for the state in NT(Northern Territory). Short Lead Times will be captured for
     * HD, ED, Bulky1 and Bulky2 delivery modes.
     */
    @Test
    public void testNearestStoreLocationWithHDShortLeadTimeForStateNT() {
        mockLeadTimeForState(STATE_NT);
        final Response response = targetStoreFinderStockFacadeImpl.nearestStoreLocation(HD,
                SANITISED_LOCATION, null, pageableData);
        verifyLeadTimeForState(response);
    }

    /**
     * Method to verify short lead times for the state QLD(Queensland). Short Lead Times will be captured for HD, ED,
     * Bulky1 and Bulky2 delivery modes.
     */
    @Test
    public void testNearestStoreLocationWithHDShortLeadTimeForStateQLD() {
        mockLeadTimeForState(STATE_QLD);
        final Response response = targetStoreFinderStockFacadeImpl.nearestStoreLocation(HD,
                SANITISED_LOCATION, null, pageableData);
        verifyLeadTimeForState(response);
    }

    /**
     * Method to verify short lead times for the state SA(South Australia). Short Lead Times will be captured for HD,
     * ED, Bulky1 and Bulky2 delivery modes.
     */
    @Test
    public void testNearestStoreLocationWithHDShortLeadTimeForStateSA() {
        mockLeadTimeForState(STATE_SA);
        final Response response = targetStoreFinderStockFacadeImpl.nearestStoreLocation(HD,
                SANITISED_LOCATION, null, pageableData);
        verifyLeadTimeForState(response);
    }

    /**
     * Method to verify short lead times for the state TAS(Tasmania). Short Lead Times will be captured for HD, ED,
     * Bulky1 and Bulky2 delivery modes.
     */
    @Test
    public void testNearestStoreLocationWithHDShortLeadTimeForStateTAS() {
        mockLeadTimeForState(STATE_TAS);
        final Response response = targetStoreFinderStockFacadeImpl.nearestStoreLocation(HD,
                SANITISED_LOCATION, null, pageableData);
        verifyLeadTimeForState(response);
    }

    /**
     * Method to verify short lead times for the state VIC(Victoria). Short Lead Times will be captured for HD, ED,
     * Bulky1 and Bulky2 delivery modes.
     */
    @Test
    public void testNearestStoreLocationWithHDShortLeadTimeForStateVIC() {
        mockLeadTimeForState(STATE_VIC);
        final Response response = targetStoreFinderStockFacadeImpl.nearestStoreLocation(HD,
                SANITISED_LOCATION, null, pageableData);
        verifyLeadTimeForState(response);
    }

    /**
     * Method to verify short lead times for the state WA(Western Australia). Short Lead Times will be captured for HD,
     * ED, Bulky1 and Bulky2 delivery modes.
     */
    @Test
    public void testNearestStoreLocationWithHDShortLeadTimeForStateWA() {
        mockLeadTimeForState(STATE_WA);
        final Response response = targetStoreFinderStockFacadeImpl.nearestStoreLocation(HD,
                SANITISED_LOCATION, null, pageableData);
        verifyLeadTimeForState(response);
    }

    /**
     * Method to verify short lead times for the state other than("ACT", "NSW", "NT", "QLD", "SA", "TAS", "VIC", "WA").
     * Short Lead Times will will not captured here in this case for HD, ED, Bulky1 and Bulky2 delivery modes.
     */
    @Test
    public void testNearestStoreLocationWithHDShortLeadTimeForDifferentState() {
        setPosData();
        given(storeFinderService.doSearchByLocation(baseStore, SANITISED_LOCATION, pageableData))
        .willReturn(searchPageData);
        final TargetPointOfServiceStockData targetPointOfServiceStockData = pointOfServiceStoreData();
        given(storeStockConverter.convert(any(StoreStockVisiblityHolder.class)))
                .willReturn(targetPointOfServiceStockData);
        final TargetAddressData targetAddressData = mock(TargetAddressData.class);
        given(targetAddressData.getState()).willReturn("INVALID_STATE");
        final Response response = targetStoreFinderStockFacadeImpl.nearestStoreLocation(HD,
                SANITISED_LOCATION, null, pageableData);
        assertThat(response.isSuccess()).isTrue();
        final LocationResponseData locationResponseData = (LocationResponseData)response.getData();
        assertThat(locationResponseData.getHomeDeliveryPos().getHdShortLeadTime()).as(HD_SHORT_LEAD_TIME).isEqualTo(0);
        assertThat(locationResponseData.getHomeDeliveryPos().getEdShortLeadTime()).as(HD_SHORT_LEAD_TIME).isEqualTo(0);
        assertThat(locationResponseData.getHomeDeliveryPos().getBulky1ShortLeadTime()).as(HD_SHORT_LEAD_TIME)
                .isEqualTo(0);
        assertThat(locationResponseData.getHomeDeliveryPos().getBulky1ShortLeadTime()).as(HD_SHORT_LEAD_TIME)
                .isEqualTo(0);
    }

    /**
     * Setting store data for for evaluation on retrieval of store details.
     *
     * @return TargetPointOfServiceStockData
     */
    private TargetPointOfServiceStockData pointOfServiceStoreData() {
        final TargetPointOfServiceStockData targetPointOfServiceStockData = new TargetPointOfServiceStockData();
        targetPointOfServiceStockData.setName(SANITISED_LOCATION);
        final AddressData address = new AddressData();
        address.setPhone("(03) 5821 9266");
        final CountryData countryData = new CountryData();
        countryData.setName("Australia");
        countryData.setIsocode("AU");
        address.setCountry(countryData);
        address.setTown(SANITISED_LOCATION);
        address.setPostalCode(POSTAL_CODE);
        targetPointOfServiceStockData.setAddress(address);
        return targetPointOfServiceStockData;
    }

    /**
     * Method to set store data for response object
     */
    private void setPosData() {
        final PointOfServiceDistanceData storeData = new PointOfServiceDistanceData();
        storeData.setPointOfService(pointOfServiceModel);
        given(searchPageData.getResults()).willReturn(ImmutableList.of(storeData));
    }

    /**
     * Method to mock short lead time for the delivery modes HD, ED, Bulky1 and Bulky2 delivery modes.
     *
     * @param state
     */
    private void mockLeadTimeForState(final String state) {
        given(storeFinderService.doSearchByLocation(baseStore, SANITISED_LOCATION, pageableData))
                .willReturn(searchPageData);
        setPosData();
        final TargetPointOfServiceStockData targetPointOfServiceStockData = mock(TargetPointOfServiceStockData.class);
        final TargetAddressData targetAddressData = mock(TargetAddressData.class);
        given(targetAddressData.getState()).willReturn(state);
        willReturn(Integer.valueOf(3)).given(mockTargetSharedConfigFacade).getInt(
                TgtFacadesConstants.ShortLeadTime.LEADTIME_DEFAULT_CONFIG_CODE,
                TgtFacadesConstants.ShortLeadTime.DEFAULT_SHORT_LEAD_TIME_VALUE);
        willReturn(Integer.valueOf(0)).given(mockTargetSharedConfigFacade).getInt(
                TgtFacadesConstants.ShortLeadTime.LEADTIME_HD_SHORT.concat(state),
                TgtFacadesConstants.ShortLeadTime.DEFAULT_SHORT_LEAD_TIME_VALUE);
        willReturn(Integer.valueOf(10)).given(mockTargetSharedConfigFacade).getInt(
                TgtFacadesConstants.ShortLeadTime.LEADTIME_ED_SHORT.concat(state),
                TgtFacadesConstants.ShortLeadTime.DEFAULT_SHORT_LEAD_TIME_VALUE);
        willReturn(Integer.valueOf(20)).given(mockTargetSharedConfigFacade).getInt(
                TgtFacadesConstants.ShortLeadTime.LEADTIME_BULKY1_SHORT.concat(state),
                TgtFacadesConstants.ShortLeadTime.DEFAULT_SHORT_LEAD_TIME_VALUE);
        willReturn(Integer.valueOf(3)).given(mockTargetSharedConfigFacade).getInt(
                TgtFacadesConstants.ShortLeadTime.LEADTIME_BULKY2_SHORT.concat(state),
                TgtFacadesConstants.ShortLeadTime.DEFAULT_SHORT_LEAD_TIME_VALUE);
        given(targetPointOfServiceStockData.getTargetAddressData()).willReturn(targetAddressData);
        given(storeStockConverter.convert(any(StoreStockVisiblityHolder.class)))
                .willReturn(targetPointOfServiceStockData);
    }

    /**
     * Verify the short lead time for various states across Australia.
     *
     * @param response
     */
    private void verifyLeadTimeForState(final Response response) {
        assertThat(response.isSuccess()).isTrue();
        final LocationResponseData locationResponseData = (LocationResponseData)response.getData();
        assertThat(locationResponseData.getHomeDeliveryPos().getHdShortLeadTime()).isNotNull().as(HD_SHORT_LEAD_TIME)
                .isEqualTo(0);
        assertThat(locationResponseData.getHomeDeliveryPos().getEdShortLeadTime()).isNotNull().as(ED_SHORT_LEAD_TIME)
                .isEqualTo(10);
        assertThat(locationResponseData.getHomeDeliveryPos().getBulky1ShortLeadTime()).isNotNull()
                .as(BULKY1_SHORT_LEAD_TIME).isEqualTo(20);
        assertThat(locationResponseData.getHomeDeliveryPos().getBulky2ShortLeadTime()).isNotNull()
                .as(BULKY2_SHORT_LEAD_TIME).isEqualTo(3);
    }

}
