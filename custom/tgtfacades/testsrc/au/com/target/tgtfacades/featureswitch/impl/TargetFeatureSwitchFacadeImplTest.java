/**
 * 
 */
package au.com.target.tgtfacades.featureswitch.impl;




import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetFeatureSwitchModel;
import au.com.target.tgtfacades.featureswitch.data.FeatureSwitchData;


/**
 * @author pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFeatureSwitchFacadeImplTest {

    @InjectMocks
    private final TargetFeatureSwitchFacadeImpl targetFeatureSwitchFacade = new TargetFeatureSwitchFacadeImpl();

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private TargetFeatureSwitchModel featureSwitch;

    @Mock
    private List<TargetFeatureSwitchModel> mockTargetFeatureSwitchModel;


    @Test
    public void testWhenFeatureIsEnabled() {
        when(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled("PAYPAL")))
                .thenReturn(Boolean.TRUE);
        assertThat(targetFeatureSwitchFacade.isFeatureEnabled("PAYPAL")).isTrue();
    }

    @Test
    public void testWhenFeatureIsNotEnabled() {
        when(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled("PAYPAL")))
                .thenReturn(Boolean.FALSE);
        assertThat(targetFeatureSwitchFacade.isFeatureEnabled("PAYPAL")).isFalse();
    }

    @Test
    public void testGetFeatureEnabledMap() {
        final TargetFeatureSwitchModel feature = createFeatureModel("attrName", Boolean.TRUE, null);
        final List<TargetFeatureSwitchModel> enabledFeatureList = new ArrayList<>();
        enabledFeatureList.add(feature);
        when(targetFeatureSwitchService.getEnabledFeatures(true))
                .thenReturn(enabledFeatureList);

        final Map<String, Boolean> enabledFeatureMap = targetFeatureSwitchFacade
                .getEnabledFeatureMap(true);

        assertThat(enabledFeatureMap).hasSize(1);
        assertThat(enabledFeatureMap.containsKey("attrName")).isTrue();
        assertThat(enabledFeatureMap.get("attrName")).isTrue();
    }

    /**
     * This method will test UI enabled features to be viewed in PDP source page. Here few UI feature switches are ON.
     * And will evaluate on those switches.
     */
    @Test
    public void testGetEnabledFeatureMapWithUIEnabledWithSwitches() {

        final boolean uiEnabledFeature = true;

        final TargetFeatureSwitchModel feature = createFeatureModel("attrName", Boolean.TRUE, null);

        final List<TargetFeatureSwitchModel> targetFeatureSwitchModelList = new ArrayList<>();
        targetFeatureSwitchModelList.add(feature);

        given(targetFeatureSwitchService.getEnabledFeatures(uiEnabledFeature)).willReturn(targetFeatureSwitchModelList);

        final Map<String, Boolean> uiEnabledFeatureMap = targetFeatureSwitchFacade
                .getEnabledFeatureMap(true);

        assertThat(uiEnabledFeatureMap).hasSize(1);
        assertThat(uiEnabledFeatureMap.containsKey("attrName")).isTrue();
        assertThat(uiEnabledFeatureMap.get("attrName")).isTrue();

    }

    /**
     * This method will test UI enabled features to be viewed in PDP source page. Here few UI feature switches are OFF.
     * And will evaluate on to empty switches.
     */
    @Test
    public void testGetEnabledFeatureMapWithUIEnabledWithoutSwitches() {


        final List<TargetFeatureSwitchModel> targetFeatureSwitchModelList = new ArrayList<>();

        when(targetFeatureSwitchService.getEnabledFeatures(false))
                .thenReturn(targetFeatureSwitchModelList);

        final Map<String, Boolean> uiEnabledFeatureMap = targetFeatureSwitchFacade
                .getEnabledFeatureMap(false);

        assertThat(uiEnabledFeatureMap).isEmpty();
    }

    @Test
    public void testGetAllFeatures() {

        final List<TargetFeatureSwitchModel> featuresList = new ArrayList<>();
        final Date featureSwitchModifiedDate = new Date();
        final SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss dd-mm-yyyy");
        final String featureSwitchModFormattedDate = dateFormat.format(featureSwitchModifiedDate);
        featuresList.add(createFeatureModel("attr1", Boolean.TRUE, featureSwitchModifiedDate));
        featuresList.add(createFeatureModel("attr2", Boolean.FALSE, featureSwitchModifiedDate));
        when(targetFeatureSwitchService.getAllFeatures()).thenReturn(featuresList);

        final List<String> featuresListExpected = Arrays.asList("attr1", "attr2");
        final List<FeatureSwitchData> retrievedFeaturesList = targetFeatureSwitchFacade.getAllFeatures();
        assertThat(featuresList).hasSize(2);
        for (final FeatureSwitchData featureSwitchData : retrievedFeaturesList) {
            assertThat(featuresListExpected.contains(featureSwitchData.getAttrName()));
            assertThat(featureSwitchData.getFormattedModifiedDate())
                    .isEqualTo(featureSwitchModFormattedDate);

        }
    }

    private TargetFeatureSwitchModel createFeatureModel(final String attrName,
            final Boolean enabled, final Date date) {
        final TargetFeatureSwitchModel feature = new TargetFeatureSwitchModel();
        feature.setAttrName(attrName);
        feature.setEnabled(enabled);
        feature.setModifiedtime(date);
        return feature;
    }

    @Test
    public void testExpeditedCheckoutFeature() {
        assertThat(targetFeatureSwitchFacade.isExpeditedCheckoutEnabled()).isEqualTo(false);
    }

    @Test
    public void isIpgEnabled() {
        assertThat(targetFeatureSwitchFacade.isIpgEnabled()).isEqualTo(false);
    }

    @Test
    public void isPaypalEnabled() {
        assertThat(targetFeatureSwitchFacade.isPaypalEnabled()).isEqualTo(false);
    }

    @Test
    public void isGiftcardEnabled() {
        assertThat(targetFeatureSwitchFacade.isGiftCardPaymentEnabled()).isEqualTo(false);
    }

    @Test
    public void testIsStockVisibilityOnlyForKiosks() {
        targetFeatureSwitchFacade.isStockVisibilityOnlyForKiosks();
        verify(targetFeatureSwitchService).isFeatureEnabled("kioskInStoreStockVisibility");
    }

    @Test
    public void testIsSpcLoginAndCheckout() {
        targetFeatureSwitchFacade.isSpcLoginAndCheckout();
        verify(targetFeatureSwitchService).isFeatureEnabled("spc.login.and.checkout");
    }

    @Test
    public void testIsAfterpayEnabled() {
        targetFeatureSwitchFacade.isAfterpayEnabled();
        verify(targetFeatureSwitchService).isFeatureEnabled("payment.afterpay");
    }

    @Test
    public void testIsShipsterEnabled() {
        targetFeatureSwitchFacade.isShipsterEnabled();
        verify(targetFeatureSwitchService).isFeatureEnabled("shipster.available");
    }

    @Test
    public void testIsFindInStoreStockVisibilityEnabled() {
        targetFeatureSwitchFacade.isFindInStoreStockVisibilityEnabled();
        verify(targetFeatureSwitchService).isFeatureEnabled("fis2.store.stock.visibility");
    }

    @Test
    public void testIsFluentStockLookupEnabled() {
        targetFeatureSwitchFacade.isFluentStockLookupEnabled();
        verify(targetFeatureSwitchService).isFeatureEnabled("fluentStockLookup");
    }

    @Test
    public void testIsPreOrderUpdateCardEnabled() {
        targetFeatureSwitchFacade.isPreOrderUpdateCardEnabled();
        verify(targetFeatureSwitchService).isFeatureEnabled("preorder.update.card");
    }

    @Test
    public void testIsReactListingEnabled() {
        targetFeatureSwitchFacade.isReactListingEnabled();
        verify(targetFeatureSwitchService).isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_LISTING);
    }
}
