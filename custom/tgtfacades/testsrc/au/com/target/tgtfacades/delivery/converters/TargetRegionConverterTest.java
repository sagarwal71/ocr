/**
 * 
 */
package au.com.target.tgtfacades.delivery.converters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.RegionModel;

import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtfacades.delivery.data.TargetRegionData;
import org.junit.Assert;


/**
 * @author rmcalave
 * 
 */
@UnitTest
public class TargetRegionConverterTest {
    private final TargetRegionConverter targetRegionConverter = new TargetRegionConverter();



    @Test
    public void testPopulate() {
        final String shortState = "VIC";
        final String longState = "Victoria";

        final RegionModel mockRegion = Mockito.mock(RegionModel.class);
        BDDMockito.given(mockRegion.getAbbreviation()).willReturn(shortState);
        BDDMockito.given(mockRegion.getName()).willReturn(longState);

        final TargetRegionData regionData = new TargetRegionData();

        targetRegionConverter.populate(mockRegion, regionData);

        Assert.assertEquals(shortState, regionData.getAbbreviation());
        Assert.assertEquals(longState, regionData.getName());
    }
}
