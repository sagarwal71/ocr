/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;
import au.com.target.tgtfacades.user.data.TargetAddressData;


/**
 * @author htan3
 *
 */
@UnitTest
public class CartDetailDeliveryAddressPopulatorTest {

    private final CartDetailDeliveryAddressPopulator populator = new CartDetailDeliveryAddressPopulator();

    private TargetCartData source;

    private CartDetailResponseData target;

    @Before
    public void setUp() {
        source = new TargetCartData();
        target = new CartDetailResponseData();
    }

    @Test
    public void testPopulateWithTargetAddressData() {
        final TargetAddressData targetAddressData = mock(TargetAddressData.class);
        source.setDeliveryAddress(targetAddressData);
        populator.populate(source, target);
        Assertions.assertThat(target.getDeliveryAddress()).isEqualTo(targetAddressData);
    }

    @Test
    public void testPopulateWithAddressData() {
        final AddressData targetAddressData = mock(AddressData.class);
        source.setDeliveryAddress(targetAddressData);
        populator.populate(source, target);
        Assertions.assertThat(target.getDeliveryAddress()).isNull();
    }

    @Test
    public void testPopulateWithTargetAddressDataWithCNCStoreNumber() {
        final AddressData targetAddressData = mock(AddressData.class);
        source.setDeliveryAddress(targetAddressData);
        source.setCncStoreNumber(Integer.valueOf(7321));
        populator.populate(source, target);
        Assertions.assertThat(target.getDeliveryAddress()).isNull();
    }
}
