/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;


/**
 * Test cases for CartDetailSummaryPopulator
 * 
 * @author jjayawa1
 *
 */
@UnitTest
public class CartDetailSummaryPopulatorTest {

    private final CartDetailSummaryPopulator populator = new CartDetailSummaryPopulator();
    private TargetCartData source;
    private CartDetailResponseData target;

    @Before
    public void setUp() {
        source = new TargetCartData();
        target = new CartDetailResponseData();
    }

    @Test
    public void populateTotal() {
        final PriceData total = new PriceData();
        total.setValue(new BigDecimal(15.00));
        source.setTotalPrice(total);
        populator.populate(source, target);
        assertThat(target.getTotal()).isNotNull();
        assertThat(target.getTotal().getValue()).isEqualTo(new BigDecimal(15.00));
    }

    @Test
    public void populateSubTotal() {
        final PriceData subtotal = new PriceData();
        subtotal.setValue(new BigDecimal(10.00));
        source.setSubTotal(subtotal);
        populator.populate(source, target);
        assertThat(target.getSubtotal()).isNotNull();
        assertThat(target.getSubtotal().getValue()).isEqualTo(new BigDecimal(10.00));
    }

    @Test
    public void populateDeliveryFee() {
        final PriceData deliveryFee = new PriceData();
        deliveryFee.setValue(new BigDecimal(5.00));
        source.setDeliveryCost(deliveryFee);
        populator.populate(source, target);
        assertThat(target.getDeliveryFee()).isNotNull();
        assertThat(target.getDeliveryFee().getValue()).isEqualTo(new BigDecimal(5.00));
    }

    @Test
    public void populateEntries() {
        final List<OrderEntryData> entries = mock(List.class);
        source.setEntries(entries);
        populator.populate(source, target);
        assertThat(target.getEntries()).isEqualTo(entries);
    }

    @Test
    public void populateGst() {
        final PriceData totalTax = new PriceData();
        totalTax.setValue(new BigDecimal(5.00));
        source.setTotalTax(totalTax);
        populator.populate(source, target);
        assertThat(target.getGst()).isNotNull();
        assertThat(target.getGst().getValue()).isEqualTo(new BigDecimal(5.00));
    }

    @Test
    public void populateOrderDiscounts() {
        final PriceData orderDiscounts = new PriceData();
        orderDiscounts.setValue(new BigDecimal(15.00));
        source.setOrderDiscounts(orderDiscounts);
        populator.populate(source, target);
        assertThat(target.getOrderDiscounts()).isNotNull();
        assertThat(target.getOrderDiscounts().getValue()).isEqualTo(new BigDecimal(15.00));
    }

    /**
     * Method to verify pre-order deposit amount for the summary of pre-order product in the cart. Here cart contians
     * pre-order item and initial deposit amount is configured for the product.
     */
    @Test
    public void populatePreOrderDepositAmount() {
        final PriceData preOrderDepositAmount = new PriceData();
        preOrderDepositAmount.setValue(BigDecimal.valueOf(10.0));
        source.setPreOrderDepositAmount(preOrderDepositAmount);
        populator.populate(source, target);
        assertThat(target.getPreOrderDepositAmount().getValue()).isNotNull().isEqualTo(BigDecimal.valueOf(10.0));
    }

    /**
     * Method to verify pre-order deposit amount for the summary of pre-order product in the cart. Here cart contains
     * pre-order item. In this case initial deposit amount not configured and hence this case will always return null.
     */
    @Test
    public void populatePreOrderDepositAmountIfNotConfigured() {
        populator.populate(source, target);
        assertThat(target.getPreOrderDepositAmount()).isNull();
    }
}