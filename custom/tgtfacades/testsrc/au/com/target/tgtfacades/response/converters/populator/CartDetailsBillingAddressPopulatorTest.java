package au.com.target.tgtfacades.response.converters.populator;

import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;


@UnitTest
public class CartDetailsBillingAddressPopulatorTest {
    private final CartDetailBillingAddressPopulator populator = new CartDetailBillingAddressPopulator();

    private TargetCartData source;

    private CartDetailResponseData target;

    @Before
    public void setUp() {
        source = new TargetCartData();
        target = new CartDetailResponseData();
    }

    @Test
    public void testPopulateWithAddressData() {
        final AddressData addressData = mock(AddressData.class);
        source.setBillingAddress(addressData);
        populator.populate(source, target);
        Assertions.assertThat(target.getBillingAddress()).isEqualTo(addressData);
    }

    @Test
    public void testPopulateWithNullAddressData() {
        source.setBillingAddress(null);
        populator.populate(source, target);
        Assertions.assertThat(target.getBillingAddress()).isNull();
    }

}
