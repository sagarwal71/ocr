/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CartDetailClickAndCollectDetailPopulatorTest {

    @InjectMocks
    private final CartDetailClickAndCollectDetailPopulator populator = new CartDetailClickAndCollectDetailPopulator();

    @Mock
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Test
    public void testPopulateCncStore() {
        final TargetCartData source = new TargetCartData();
        final Integer cncStoreNumber = Integer.valueOf(1);
        final TargetPointOfServiceData pos = mock(TargetPointOfServiceData.class);
        given(targetStoreLocatorFacade.getPointOfService(cncStoreNumber)).willReturn(pos);
        source.setCncStoreNumber(cncStoreNumber);
        final CartDetailResponseData target = new CartDetailResponseData();
        populator.populate(source, target);

        Assertions.assertThat(target.getStore()).isEqualTo(pos);
    }
}
