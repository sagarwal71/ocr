/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.tgtfacades.response.data.ProductSearchResponseData;
import au.com.target.tgtfacades.search.data.TargetProductCategorySearchPageData;
import au.com.target.tgtfacades.sort.TargetSortData;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductSearchSortDataPopulatorTest {

    private final ProductSearchSortDataPopulator populator = new ProductSearchSortDataPopulator();

    @Test
    public void testPopulate() {

        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> source = new TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData>();
        final ProductSearchResponseData target = new ProductSearchResponseData();
        final TargetSortData targetSortData1 = new TargetSortData();
        final String targetSortData1Name = "targetSortData1Name";
        final boolean targetSortData1Selected = false;
        final String targetSortData1Url = "targetSortData1Url";
        final String targetSortData1WsUrl = "targetSortData1WsUrl";
        targetSortData1.setCode(targetSortData1Name);
        targetSortData1.setName(targetSortData1Name);
        targetSortData1.setSelected(targetSortData1Selected);
        targetSortData1.setUrl(targetSortData1Url);
        targetSortData1.setWsUrl(targetSortData1WsUrl);
        final List<TargetSortData> targetSortDataList = new ArrayList<>(Arrays.asList(targetSortData1));
        source.setTargetSortData(targetSortDataList);
        populator.populate(source, target);
        assertThat(target.getSortDataList()).isNotEmpty();
        assertThat(target.getSortDataList().get(0).getName()).isEqualTo(targetSortData1Name);
        assertThat(target.getSortDataList().get(0).isSelected()).isEqualTo(targetSortData1Selected);
        assertThat(target.getSortDataList().get(0).getUrl()).isEqualTo(targetSortData1Url);
        assertThat(target.getSortDataList().get(0).getQueryUrl()).isEqualTo(targetSortData1WsUrl);
    }
}
