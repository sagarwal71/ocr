/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.tgtfacades.response.data.ProductSearchResponseData;
import au.com.target.tgtfacades.search.data.TargetProductCategorySearchPageData;


/**
 * @author bhuang3
 *
 */
@SuppressWarnings("deprecation")
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductSearchCommonDataPopulatorTest {

    private final ProductSearchCommonDataPopulator populator = new ProductSearchCommonDataPopulator();

    @Test
    public void testPopulate() {
        final long totalNumberOfResults = 30;
        final int pageSize = 20;
        final int numberOfPages = 2;
        final int currentPage = 0;
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> source = new TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData>();
        final PaginationData pagination = new PaginationData();
        pagination.setTotalNumberOfResults(totalNumberOfResults);
        pagination.setCurrentPage(currentPage);
        pagination.setNumberOfPages(numberOfPages);
        pagination.setPageSize(pageSize);
        source.setPagination(pagination);
        final String url = "testUrl";
        final String wsUrl = "testWsUrlNrpp=";
        final String replaceString = "%7BrecordsPerPage%7D";
        final String clearAll = "clearAll";
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        searchStateData.setUrl(url);
        searchStateData.setWsUrl(wsUrl + replaceString);
        source.setCurrentQuery(searchStateData);
        final String keywordRedirectUrl = "testKeywordRedirectUrl";
        source.setKeywordRedirectUrl(keywordRedirectUrl);
        source.setClearAllUrl(clearAll);
        final ProductSearchResponseData target = new ProductSearchResponseData();
        populator.populate(source, target);
        assertThat(target.getCommonData()).isNotNull();
        assertThat(target.getCommonData().getTotalNumberOfResults()).isEqualTo(totalNumberOfResults);
        assertThat(target.getCommonData().getPageSize()).isEqualTo(pageSize);
        assertThat(target.getCommonData().getTotalPages()).isEqualTo(numberOfPages);
        assertThat(target.getCommonData().getCurrentPage()).isEqualTo(currentPage);
        assertThat(target.getCommonData().getCurrentPageUrl()).isEqualTo(url);
        assertThat(target.getCommonData().getCurrentPageWsUrl()).isEqualTo(wsUrl + 20);
        assertThat(target.getCommonData().getClearAllUrl()).isEqualTo(clearAll);
    }
}
