/**
 * 
 */
package au.com.target.tgtfacades.order.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.util.TargetPriceHelper;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetExtendedCartPopulatorTest {

    @Mock
    private PriceDataFactory priceDataFactory;

    @Mock
    private Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter;

    @Mock
    private TargetPriceHelper mockPriceHelper;

    @InjectMocks
    @Spy
    private final TargetExtendedCartPopulator targetExtendedCartPopulator = new TargetExtendedCartPopulator();

    @Mock
    private AbstractOrderModel source;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
        targetExtendedCartPopulator.setOrderEntryConverter(orderEntryConverter);
    }

    @Test
    public void testAddTotals() {
        final CurrencyModel currency = mock(CurrencyModel.class);
        final PriceData totalPrice = mock(PriceData.class);
        given(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(10.0), currency)).willReturn(totalPrice);
        final PriceData totalTax = mock(PriceData.class);
        given(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(0.9), currency)).willReturn(totalTax);
        final PriceData subtotal = mock(PriceData.class);
        given(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(9.1), currency)).willReturn(subtotal);
        final PriceData installmentPrice = mock(PriceData.class);
        given(mockPriceHelper.calculateAfterpayInstallmentPrice(totalPrice))
                .willReturn(installmentPrice);
        given(source.getCurrency()).willReturn(currency);
        given(source.getTotalPrice()).willReturn(Double.valueOf(10.0));
        given(source.getTotalTax()).willReturn(Double.valueOf(0.9));
        given(source.getSubtotal()).willReturn(Double.valueOf(9.1));
        final AbstractOrderData prototype = new AbstractOrderData();

        targetExtendedCartPopulator.addTotals(source, prototype);
        assertThat(prototype.getTotalPrice()).isEqualTo(totalPrice);
        assertThat(prototype.getTotalTax()).isEqualTo(totalTax);
        assertThat(prototype.getSubTotal()).isEqualTo(subtotal);
        assertThat(prototype.getInstallmentPrice()).isEqualTo(installmentPrice);
    }

    @Test
    public void testExcludeForPaymentPartners() {
        final List<AbstractOrderEntryModel> sourceEntries = new ArrayList<>();
        final AbstractOrderEntryModel sourceEntry1 = mock(AbstractOrderEntryModel.class);
        sourceEntries.add(sourceEntry1);
        final AbstractOrderEntryModel sourceEntry2 = mock(AbstractOrderEntryModel.class);
        sourceEntries.add(sourceEntry2);
        given(source.getEntries()).willReturn(sourceEntries);

        final OrderEntryData entry1 = mock(OrderEntryData.class);
        final TargetProductData product1 = mock(TargetProductData.class);
        willReturn(Boolean.TRUE).given(product1).isExcludeForAfterpay();
        given(entry1.getProduct()).willReturn(product1);
        given(orderEntryConverter.convert(sourceEntry1)).willReturn(entry1);
        final OrderEntryData entry2 = mock(OrderEntryData.class);
        final TargetProductData product2 = mock(TargetProductData.class);
        willReturn(Boolean.TRUE).given(product2).isExcludeForZipPayment();
        given(entry2.getProduct()).willReturn(product2);
        given(orderEntryConverter.convert(sourceEntry2)).willReturn(entry2);
  
        final List<OrderEntryData> entries = new ArrayList<OrderEntryData>();
        entries.add(entry1);
        entries.add(entry2);
        final AbstractOrderData prototype = mock(AbstractOrderData.class);
        given(prototype.getEntries()).willReturn(entries);
  
        targetExtendedCartPopulator.addEntries(source, prototype);
        verify(prototype).setExcludeForAfterpay(true);
        verify(prototype).setExcludeForZipPayment(true);
    }

    @Test
    public void testExcludePaymentPartnersForProductData() {
        final List<AbstractOrderEntryModel> sourceEntries = new ArrayList<>();
        final AbstractOrderEntryModel sourceEntry1 = mock(AbstractOrderEntryModel.class);
        sourceEntries.add(sourceEntry1);
        final AbstractOrderEntryModel sourceEntry2 = mock(AbstractOrderEntryModel.class);
        sourceEntries.add(sourceEntry2);
        given(source.getEntries()).willReturn(sourceEntries);

        final OrderEntryData entry1 = mock(OrderEntryData.class);
        final ProductData product1 = mock(ProductData.class);
        given(entry1.getProduct()).willReturn(product1);
        given(orderEntryConverter.convert(sourceEntry1)).willReturn(entry1);
        final OrderEntryData entry2 = mock(OrderEntryData.class);
        final ProductData product2 = mock(ProductData.class);
        given(entry2.getProduct()).willReturn(product2);
        given(orderEntryConverter.convert(sourceEntry2)).willReturn(entry2);
        final AbstractOrderData prototype = new AbstractOrderData();

        targetExtendedCartPopulator.addEntries(source, prototype);
        assertThat(prototype.isExcludeForAfterpay()).isFalse();
        assertThat(prototype.isExcludeForZipPayment()).isFalse();
    }

    @Test
    public void testNotExcludeForPaymentPartners() {
        final List<AbstractOrderEntryModel> sourceEntries = new ArrayList<>();
        final AbstractOrderEntryModel sourceEntry1 = mock(AbstractOrderEntryModel.class);
        sourceEntries.add(sourceEntry1);
        final AbstractOrderEntryModel sourceEntry2 = mock(AbstractOrderEntryModel.class);
        sourceEntries.add(sourceEntry2);
        given(source.getEntries()).willReturn(sourceEntries);

        final OrderEntryData entry1 = mock(OrderEntryData.class);
        final TargetProductData product1 = mock(TargetProductData.class);
        given(entry1.getProduct()).willReturn(product1);
        given(orderEntryConverter.convert(sourceEntry1)).willReturn(entry1);
        final OrderEntryData entry2 = mock(OrderEntryData.class);
        final TargetProductData product2 = mock(TargetProductData.class);
        given(entry2.getProduct()).willReturn(product2);
        given(orderEntryConverter.convert(sourceEntry2)).willReturn(entry2);
        final AbstractOrderData prototype = new AbstractOrderData();

        targetExtendedCartPopulator.addEntries(source, prototype);
        assertThat(prototype.isExcludeForAfterpay()).isFalse();
        assertThat(prototype.isExcludeForZipPayment()).isFalse();
    }
}
