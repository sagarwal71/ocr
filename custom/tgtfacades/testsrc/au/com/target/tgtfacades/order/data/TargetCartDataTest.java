/**
 * 
 */
package au.com.target.tgtfacades.order.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;


@UnitTest
public class TargetCartDataTest {

    private final TargetCartData cartData = new TargetCartData();

    @Test
    public void testGetPaymentsInfo() {
        final List<CCPaymentInfoData> paymentsInfo = cartData.getPaymentsInfo();

        assertTrue(CollectionUtils.isEmpty(paymentsInfo));
    }

    @Test
    public void testGetPaymentsInfoWithOnePaymentInfo() {
        final CCPaymentInfoData ccPaymentInfoData = new CCPaymentInfoData();
        cartData.setPaymentInfo(ccPaymentInfoData);

        final List<CCPaymentInfoData> paymentsInfo = cartData.getPaymentsInfo();

        assertTrue(CollectionUtils.isNotEmpty(paymentsInfo));
        assertEquals(1, paymentsInfo.size());
        assertEquals(ccPaymentInfoData, paymentsInfo.get(0));
    }
}