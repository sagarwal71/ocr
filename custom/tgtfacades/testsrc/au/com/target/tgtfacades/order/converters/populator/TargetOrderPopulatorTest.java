package au.com.target.tgtfacades.order.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.PromotionResultData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.PrincipalData;
import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.IpgGiftCardPaymentInfoModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordModel;
import de.hybris.platform.ordercancel.model.OrderEntryCancelRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.jalo.ProductPromotion;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.returns.model.OrderEntryReturnRecordEntryModel;
import de.hybris.platform.returns.model.OrderReturnRecordEntryModel;
import de.hybris.platform.returns.model.OrderReturnRecordModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.voucher.VoucherService;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.CollectionUtils;

import au.com.target.tgtcore.barcode.TargetBarCodeService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.formatter.InvoiceFormatterHelper;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.affiliate.converters.AffiliateOrderDataConverter;
import au.com.target.tgtfacades.order.converters.AbstractOrderHelper;
import au.com.target.tgtfacades.order.converters.OrderEntryModificationRecordEntryConverter;
import au.com.target.tgtfacades.order.converters.TargetFlybuysDiscountConverter;
import au.com.target.tgtfacades.order.data.OrderModificationData;
import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.order.enums.OrderModificationStatusEnum;
import au.com.target.tgtlayby.data.PaymentDueData;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.order.TargetLaybyPaymentDueService;
import au.com.target.tgtlayby.order.strategies.OrderOutstandingStrategy;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;
import au.com.target.tgtpayment.model.ZippayPaymentInfoModel;


/**
 * @author Benoit VanalderWeireldt
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetOrderPopulatorTest {

    @InjectMocks
    @Spy
    private final TargetOrderPopulator targetOrderPopulator = new TargetOrderPopulator();

    @Mock
    private PriceDataFactory priceDataFactory;

    @Mock
    private PromotionsService promotionsService;

    @Mock
    private DeliveryService deliveryService;

    @Mock
    private CommonI18NService commonI18NService;

    @Mock
    private AbstractOrderHelper abstractOrderHelper;

    @Mock
    private CurrencyModel currencyModel;

    @Mock
    private OrderModel orderModel;

    @Mock
    private OrderModel replacementOrderModel;

    @Mock
    private OrderEntryReturnRecordEntryModel replacementOrder;

    @Mock
    private OrderEntryCancelRecordEntryModel cancelledOrder;

    @Mock
    private OrderEntryModel orderEntryModel;

    @Mock
    private DeliveryModeModel deliveryModeModel;

    @Mock
    private PurchaseOptionConfigModel purchaseOptionConfigModel;

    @Mock
    private CustomerModel customerModel;

    @Mock
    private TargetLaybyPaymentDueService targetLaybyPaymentDueService;

    @Mock
    private PaymentDueData initialPayment;

    @Mock
    private PaymentDueData finalPayment;

    @Mock
    private PaymentDueData intermediatePayment;

    @Mock
    private Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter;

    @Mock
    private Converter<DeliveryModeModel, DeliveryModeData> deliveryModeConverter;

    @Mock
    private OrderEntryModificationRecordEntryConverter orderEntryModificationRecordEntryConverter;

    @Mock
    private Converter<PinPadPaymentInfoModel, TargetCCPaymentInfoData> targetPinPadPaymentInfoConverter;

    @Mock
    private Converter<IpgPaymentInfoModel, TargetCCPaymentInfoData> targetIpgPaymentInfoConverter;

    @Mock
    private Converter<AfterpayPaymentInfoModel, TargetCCPaymentInfoData> targetAfterpayPaymentInfoConverter;

    @Mock
    private Converter<ZippayPaymentInfoModel, TargetCCPaymentInfoData> targetZipPaymentInfoConverter;

    @Mock
    private TargetBarCodeService targetBarCodeService;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private OrderOutstandingStrategy orderOutstandingStrategy;

    @Mock
    private Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> creditCardPaymentInfoConverter;

    @Mock
    private Converter<IpgGiftCardPaymentInfoModel, TargetCCPaymentInfoData> targetIpgGiftCardPaymentInfoConverter;

    @Mock
    private ModelService modelService;

    @Mock
    private Converter<PromotionResultModel, PromotionResultData> promotionResultPopulator;

    @Mock
    private AbstractPopulatingConverter<PrincipalModel, PrincipalData> principalConverter;

    @Mock
    private TargetFlybuysDiscountConverter targetFlybuysDiscountConverter;

    @Mock
    private AffiliateOrderDataConverter affiliateOrderDataConverter;

    @Mock
    private VoucherService voucherService;

    @Mock
    private DeliveryModeData deliveryModeData;

    @Mock
    private CustomerData customerData;

    @Mock
    private InvoiceFormatterHelper invoiceFormatterHelper;

    private final TargetOrderData targetOrderData = new TargetOrderData();

    @Before
    public void initData() {
        targetOrderPopulator.setTargetFlybuysDiscountConverter(targetFlybuysDiscountConverter);
        given(orderModel.getCurrency()).willReturn(currencyModel);
        given(orderModel.getPurchaseOptionConfig()).willReturn(purchaseOptionConfigModel);
        given(purchaseOptionConfigModel.getCode()).willReturn("HomeDelivery");
        given(orderModel.getDeliveryMode()).willReturn(deliveryModeModel);
        given(orderModel.getUser()).willReturn(customerModel);
        given(orderModel.getCode()).willReturn("00000000");
        given(targetLaybyPaymentDueService.getInitialPaymentDue(orderModel)).willReturn(initialPayment);
        given(targetLaybyPaymentDueService.getFinalPaymentDue(orderModel)).willReturn(finalPayment);
        given(deliveryModeConverter.convert(deliveryModeModel)).willReturn(deliveryModeData);
        given(principalConverter.convert(customerModel)).willReturn(customerData);
        willDoNothing().given(targetFlybuysDiscountConverter).convertFlybuysDiscount(orderModel, targetOrderData);
        willReturn("").given(targetOrderPopulator).encodeOrderId(any(String.class));
    }

    @Test
    public void testConverter() {
        final String tmd = "tmdValue";
        final Integer cncStorenumber = Integer.valueOf(20);
        given(orderModel.getTmdCardNumber()).willReturn(tmd);
        given(orderModel.getCncStoreNumber()).willReturn(cncStorenumber);

        targetOrderPopulator.populate(orderModel, targetOrderData);

        assertThat(cncStorenumber).isEqualTo(targetOrderData.getCncStoreNumber());
        assertThat(tmd).isEqualTo(targetOrderData.getTmdNumber());
    }

    @Test
    public void testConverterOtherValue() {
        final String tmd = "2222/725BIGDATA";
        final Integer cncStorenumber = Integer.valueOf(-20);
        given(orderModel.getTmdCardNumber()).willReturn(tmd);
        given(orderModel.getCncStoreNumber()).willReturn(cncStorenumber);

        targetOrderPopulator.populate(orderModel, targetOrderData);

        assertThat(cncStorenumber).isEqualTo(targetOrderData.getCncStoreNumber());
        assertThat(tmd).isEqualTo(targetOrderData.getTmdNumber());
    }

    @Test
    public void testConverterLaybyOrder() throws ParseException {
        given(Boolean.valueOf(abstractOrderHelper.isLayBy(Mockito.any(AbstractOrderModel.class)))).willReturn(
                Boolean.TRUE);
        final Set<PaymentDueData> paymentDues = new HashSet<>();
        paymentDues.add(initialPayment);
        paymentDues.add(intermediatePayment);
        paymentDues.add(finalPayment);
        willReturn(paymentDues).given(targetLaybyPaymentDueService).getAllPaymentDuesForAbstractOrder(orderModel);
        given(orderModel.getCncStoreNumber()).willReturn(null);
        final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        final Date finalDate = sdf.parse("20/07/2013 00:00:00");

        willReturn(finalDate).given(finalPayment).getDueDate();
        given(Double.valueOf(initialPayment.getAmount())).willReturn(Double.valueOf(20.0d));

        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        final List<PaymentTransactionEntryModel> transactions = new ArrayList<>();
        PaymentTransactionEntryModel paymentTransactionEntryModel;
        for (int i = 0; i < 5; i++) {
            paymentTransactionEntryModel = new PaymentTransactionEntryModel();
            paymentTransactionEntryModel.setAmount(new BigDecimal(i * 10));
            paymentTransactionEntryModel.setCreationtime(new Date(System.currentTimeMillis()));
            paymentTransactionEntryModel.setReceiptNo("receipt" + i);
            paymentTransactionEntryModel.setPaymentTransaction(paymentTransactionModel);
            paymentTransactionEntryModel.setTransactionStatus(i == 4 ? TransactionStatus.REJECTED.toString()
                    : TransactionStatus.ACCEPTED.toString());
            transactions.add(paymentTransactionEntryModel);
        }

        given(paymentTransactionModel.getEntries()).willReturn(transactions);
        given(orderModel.getPaymentTransactions())
                .willReturn(Collections.singletonList(paymentTransactionModel));

        targetOrderPopulator.populate(orderModel, targetOrderData);

        assertThat(targetOrderData.getLaybyTransactions().size() == 1).isTrue();
        assertThat(targetOrderData.getLaybyTransactions().get(0).getStatus())
                .isEqualTo(TransactionStatus.ACCEPTED.toString());
        assertThat(targetOrderData.getLaybyTransactions().get(0).getReceiptNumber().startsWith("receipt")).isTrue();
        assertThat(targetOrderData.getLaybyTransactions().get(0).getPaymentDate()
                .before(new Date(System.currentTimeMillis()))).isTrue();
    }

    @Test
    public void testCancelledProducts() {
        final Date modificationDate = new Date(System.currentTimeMillis());

        final Set<OrderModificationRecordModel> modifications = new HashSet<>();
        given(orderModel.getCncStoreNumber()).willReturn(null);
        final OrderCancelRecordModel orderCancelRecordModel = mock(OrderCancelRecordModel.class);
        final Collection<OrderModificationRecordEntryModel> cancelledEntries = new LinkedList<>();
        final Collection<OrderEntryModificationRecordEntryModel> cancelledRecordEntries = new LinkedList<>();

        final OrderCancelRecordEntryModel orderCancelRecordEntryModel = mock(OrderCancelRecordEntryModel.class);

        final OrderEntryCancelRecordEntryModel orderEntryCancelRecordEntryModel = mock(
                OrderEntryCancelRecordEntryModel.class);
        given(orderEntryCancelRecordEntryModel.getOrderEntry()).willReturn(orderEntryModel);
        given(orderEntryCancelRecordEntryModel.getCreationtime()).willReturn(modificationDate);
        given(orderEntryCancelRecordEntryModel.getCancelledQuantity()).willReturn(Integer.valueOf(5));
        cancelledRecordEntries.add(orderEntryCancelRecordEntryModel);
        given(orderCancelRecordEntryModel.getOrderEntriesModificationEntries()).willReturn(
                cancelledRecordEntries);
        cancelledEntries.add(orderCancelRecordEntryModel);
        given(orderCancelRecordModel.getModificationRecordEntries()).willReturn(cancelledEntries);
        modifications.add(orderCancelRecordModel);

        given(orderModel.getModificationRecords()).willReturn(modifications);

        final OrderModificationData orderModificationData = new OrderModificationData();
        given(orderEntryModificationRecordEntryConverter.convert(orderEntryCancelRecordEntryModel))
                .willReturn(orderModificationData);

        targetOrderPopulator.populate(orderModel, targetOrderData);

        assertThat(targetOrderData.getCancelledProducts().size()).isEqualTo(1);
        assertThat(targetOrderData.getCancelledProducts().get(0)).isEqualTo(orderModificationData);
        assertThat(targetOrderData.getCancelledProducts().get(0).getStatus())
                .isEqualTo(OrderModificationStatusEnum.Cancelled);
    }

    @Test
    public void testReturnProducts() {
        final Date modificationDate = mock(Date.class);
        given(orderEntryModel.getOrder()).willReturn(replacementOrderModel);
        given(replacementOrderModel.getCode()).willReturn("replacement order");

        final Set<OrderModificationRecordModel> modifications = new HashSet<>();

        final OrderReturnRecordModel orderReturnRecordModel = mock(OrderReturnRecordModel.class);
        final Collection<OrderModificationRecordEntryModel> returnEntries = new LinkedList<>();
        final Collection<OrderEntryModificationRecordEntryModel> returnRecordEntries = new LinkedList<>();

        final OrderReturnRecordEntryModel orderCancelRecordEntryModel = BDDMockito
                .mock(OrderReturnRecordEntryModel.class);

        final OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntryModel = BDDMockito
                .mock(OrderEntryReturnRecordEntryModel.class);
        given(orderEntryReturnRecordEntryModel.getOrderEntry()).willReturn(orderEntryModel);
        given(orderEntryReturnRecordEntryModel.getCreationtime()).willReturn(modificationDate);
        given(orderEntryReturnRecordEntryModel.getReturnedQuantity()).willReturn(Long.valueOf(5));
        given(orderEntryReturnRecordEntryModel.getOrderEntry()).willReturn(orderEntryModel);
        returnRecordEntries.add(orderEntryReturnRecordEntryModel);
        given(orderCancelRecordEntryModel.getOrderEntriesModificationEntries()).willReturn(
                returnRecordEntries);
        returnEntries.add(orderCancelRecordEntryModel);
        given(orderReturnRecordModel.getModificationRecordEntries()).willReturn(returnEntries);
        modifications.add(orderReturnRecordModel);

        given(orderModel.getModificationRecords()).willReturn(modifications);

        final OrderModificationData orderModificationData = new OrderModificationData();
        given(orderEntryModificationRecordEntryConverter.convert(orderEntryReturnRecordEntryModel))
                .willReturn(orderModificationData);

        targetOrderPopulator.populate(orderModel, targetOrderData);

        assertThat(targetOrderData.getReturnAndRefundProducts().size()).isEqualTo(1);
        assertThat(targetOrderData.getReturnAndRefundProducts().get(0)).isEqualTo(orderModificationData);
        assertThat(targetOrderData.getReturnAndRefundProducts().get(0)
                .getStatus()).isEqualTo(OrderModificationStatusEnum.Refunded);
    }

    @Test
    public void testAddPinPadPaymentInformation() {
        final CartData cartData = new CartData();
        final PinPadPaymentInfoModel pinPadPaymentInfoModel = mock(PinPadPaymentInfoModel.class);
        targetOrderPopulator.setTargetPinPadPaymentInfoConverter(targetPinPadPaymentInfoConverter);
        final TargetCCPaymentInfoData targetCCPaymentInfoData = mock(TargetCCPaymentInfoData.class);
        given(orderModel.getPaymentInfo()).willReturn(pinPadPaymentInfoModel);
        given(targetPinPadPaymentInfoConverter.convert(pinPadPaymentInfoModel)).willReturn(targetCCPaymentInfoData);
        targetOrderPopulator.addPaymentInformation(orderModel, cartData);

        assertThat(cartData.getPaymentInfo()).isEqualTo(targetCCPaymentInfoData);
    }

    @Test
    public void testAddAfterpayPaymentInformation() {
        final CartData cartData = new CartData();
        final AfterpayPaymentInfoModel afterpayPaymentInfoModel = mock(AfterpayPaymentInfoModel.class);
        targetOrderPopulator.setTargetPinPadPaymentInfoConverter(targetPinPadPaymentInfoConverter);
        final TargetCCPaymentInfoData targetCCPaymentInfoData = mock(TargetCCPaymentInfoData.class);
        given(orderModel.getPaymentInfo()).willReturn(afterpayPaymentInfoModel);
        given(targetAfterpayPaymentInfoConverter.convert(afterpayPaymentInfoModel)).willReturn(targetCCPaymentInfoData);
        targetOrderPopulator.addPaymentInformation(orderModel, cartData);

        assertThat(cartData.getPaymentInfo()).isEqualTo(targetCCPaymentInfoData);
    }

    @Test
    public void testAddZipPaymentInformation() {
        final CartData cartData = new CartData();
        final ZippayPaymentInfoModel zippayPaymentInfoModel = mock(ZippayPaymentInfoModel.class);
        targetOrderPopulator.setTargetZipPaymentInfoConverter(targetZipPaymentInfoConverter);
        final TargetCCPaymentInfoData targetCCPaymentInfoData = mock(TargetCCPaymentInfoData.class);
        given(orderModel.getPaymentInfo()).willReturn(zippayPaymentInfoModel);
        given(targetZipPaymentInfoConverter.convert(zippayPaymentInfoModel)).willReturn(targetCCPaymentInfoData);
        targetOrderPopulator.addPaymentInformation(orderModel, cartData);

        assertThat(cartData.getPaymentInfo()).isEqualTo(targetCCPaymentInfoData);
    }

    @Test
    public void testAppliedVouchers() {
        final TargetOrderData target = new TargetOrderData();
        given(voucherService.getAppliedVoucherCodes(orderModel)).willReturn(
                CollectionUtils.arrayToList(new String[] { "CODE123", "CODE345" }));
        targetOrderPopulator.setAppliedVoucherCodes(orderModel, target);
        assertThat(target.getAppliedVoucherCodes().get(0)).isEqualTo("CODE123");
        assertThat(target.getAppliedVoucherCodes().get(1)).isEqualTo("CODE345");
    }

    @Test
    public void testAppliedVoucher() {
        final TargetOrderData target = new TargetOrderData();
        given(voucherService.getAppliedVoucherCodes(orderModel)).willReturn(
                CollectionUtils.arrayToList(new String[] { "CODE123" }));
        targetOrderPopulator.setAppliedVoucherCodes(orderModel, target);
        assertThat(target.getAppliedVoucherCodes().get(0)).isEqualTo("CODE123");
    }

    @Test
    public void testNoAppliedVoucher() {
        final TargetOrderData target = new TargetOrderData();
        given(voucherService.getAppliedVoucherCodes(orderModel)).willReturn(null);
        targetOrderPopulator.setAppliedVoucherCodes(orderModel, target);
        assertThat(target.getAppliedVoucherCodes()).isNull();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testAddPromotionsTMD() {
        final SessionContext sessioncontext = mock(SessionContext.class);
        final AbstractOrder abstractOrder = mock(AbstractOrder.class);
        final PromotionResult promotionResult = mock(PromotionResult.class);
        final PromotionOrderResults promotionOrderResults = new PromotionOrderResults(sessioncontext, abstractOrder,
                Collections.singletonList(promotionResult), 2.1);
        final DiscountValue discountValue = mock(DiscountValue.class);
        final AbstractOrderEntryModel abstractOrderEntryModel = mock(AbstractOrderEntryModel.class);
        final PromotionResultModel promotionResultModel = mock(PromotionResultModel.class);
        final List<PromotionResultModel> promotionResultModelList = new ArrayList<>();
        promotionResultModelList.add(promotionResultModel);
        final AbstractPromotionModel abstractPromotionModel = mock(AbstractPromotionModel.class);
        final PromotionOrderEntryConsumedModel promotionOrderEntryConsumedModel = mock(
                PromotionOrderEntryConsumedModel.class);
        final PromotionResultData promotionResultData = mock(PromotionResultData.class);
        final ProductPromotion promotion = mock(ProductPromotion.class);
        final JaloSession jaloSession = mock(JaloSession.class);

        targetOrderPopulator.setModelService(modelService);
        targetOrderPopulator.setPromotionResultConverter(promotionResultPopulator);

        given(orderModel.getCurrency()).willReturn(currencyModel);
        given(abstractOrderEntryModel.getDiscountValues()).willReturn(Collections.singletonList(discountValue));
        given(orderModel.getEntries()).willReturn(Collections.singletonList(abstractOrderEntryModel));
        given(promotionsService.getPromotionResults(orderModel)).willReturn(promotionOrderResults);
        given(orderModel.getGlobalDiscountValues()).willReturn(Collections.singletonList(discountValue));
        given(Double.valueOf(discountValue.getAppliedValue())).willReturn(Double.valueOf(2.3));
        given(modelService.getAll(Mockito.anyCollection(), Mockito.anyCollection())).willReturn(
                promotionResultModelList);
        given(promotionResultModel.getPromotion()).willReturn(abstractPromotionModel);
        given(promotionResultModel.getConsumedEntries()).willReturn(
                Collections.singletonList(promotionOrderEntryConsumedModel));
        given(promotionResultPopulator.convert(promotionResultModel)).willReturn(promotionResultData);
        given(promotionResult.getSession()).willReturn(jaloSession);
        given(promotionResult.getFired()).willReturn(true);
        given(promotionResult.isApplied()).willReturn(true);
        given(promotionResult.getPromotion()).willReturn(promotion);
        given(modelService.get(promotionResult)).willReturn(promotionResultModel);

        final TargetOrderData orderData = new TargetOrderData();
        targetOrderPopulator.addPromotions(orderModel, orderData);
        assertThat(orderData.getAppliedProductPromotions().iterator().next()).isEqualTo(promotionResultData);
    }

    @Test
    public void testPopulateSinglePaymentInfoForIpgCreditCard() {
        final TargetOrderData orderData = new TargetOrderData();
        final IpgPaymentInfoModel ipgPaymentInfo = mock(IpgPaymentInfoModel.class);
        given(orderModel.getPaymentInfo()).willReturn(ipgPaymentInfo);
        final List<PaymentTransactionModel> list = new ArrayList<>();
        final List<PaymentTransactionEntryModel> entryList = new ArrayList<>();
        final PaymentTransactionModel tran = mock(PaymentTransactionModel.class);
        final PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);
        given(entry.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.name());
        given(entry.getType()).willReturn(PaymentTransactionType.CAPTURE);
        final CreditCardPaymentInfoModel actualPaymentInfo = mock(CreditCardPaymentInfoModel.class);
        given(entry.getIpgPaymentInfo()).willReturn(actualPaymentInfo);
        given(tran.getEntries()).willReturn(entryList);
        given(tran.getIsCaptureSuccessful()).willReturn(Boolean.TRUE);
        entryList.add(entry);
        list.add(tran);
        given(orderModel.getPaymentTransactions()).willReturn(list);
        given(orderModel.getCncStoreNumber()).willReturn(null);
        final CCPaymentInfoData paymentInfoData = mock(CCPaymentInfoData.class);
        given(creditCardPaymentInfoConverter.convert(actualPaymentInfo)).willReturn(paymentInfoData);
        final TargetCCPaymentInfoData ipgPaymentInfoData = mock(TargetCCPaymentInfoData.class);
        given(targetIpgPaymentInfoConverter.convert(ipgPaymentInfo)).willReturn(ipgPaymentInfoData);
        final AddressData addressData = mock(AddressData.class);
        given(ipgPaymentInfoData.getBillingAddress()).willReturn(addressData);

        targetOrderPopulator.populate(orderModel, orderData);

        verify(creditCardPaymentInfoConverter).convert(actualPaymentInfo);
        verify(paymentInfoData).setBillingAddress(addressData);
        assertThat(orderData.getPaymentsInfo()).isNotNull();
        assertThat(orderData.getPaymentsInfo().isEmpty()).isFalse();
        assertThat(orderData.getPaymentsInfo().get(0)).isEqualTo(paymentInfoData);
    }

    @Test
    public void testPopulateSinglePaymentInfoForIpgCreditCardWithNoIpgPaymentInfoYet() {
        final TargetOrderData orderData = new TargetOrderData();
        final IpgPaymentInfoModel ipgPaymentInfo = mock(IpgPaymentInfoModel.class);
        given(orderModel.getPaymentInfo()).willReturn(ipgPaymentInfo);
        final List<PaymentTransactionModel> list = new ArrayList<>();
        final List<PaymentTransactionEntryModel> entryList = new ArrayList<>();
        final PaymentTransactionModel tran = mock(PaymentTransactionModel.class);
        final PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);
        given(entry.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.name());
        given(entry.getType()).willReturn(PaymentTransactionType.CAPTURE);
        final CreditCardPaymentInfoModel actualPaymentInfo = mock(CreditCardPaymentInfoModel.class);
        given(entry.getIpgPaymentInfo()).willReturn(actualPaymentInfo);
        given(tran.getEntries()).willReturn(entryList);
        given(tran.getIsCaptureSuccessful()).willReturn(Boolean.TRUE);
        given(entry.getPaymentTransaction()).willReturn(tran);
        entryList.add(entry);
        list.add(tran);
        given(orderModel.getPaymentTransactions()).willReturn(list);
        given(orderModel.getCncStoreNumber()).willReturn(null);
        final CCPaymentInfoData paymentInfoData = mock(CCPaymentInfoData.class);
        given(creditCardPaymentInfoConverter.convert(actualPaymentInfo)).willReturn(paymentInfoData);
        given(targetIpgPaymentInfoConverter.convert(ipgPaymentInfo)).willReturn(null);

        targetOrderPopulator.populate(orderModel, orderData);

        verify(creditCardPaymentInfoConverter).convert(actualPaymentInfo);
        verify(paymentInfoData, times(0)).setBillingAddress(any(AddressData.class));
        assertThat(orderData.getPaymentsInfo()).isNotNull();
        assertThat(orderData.getPaymentsInfo().isEmpty()).isFalse();
        assertThat(orderData.getPaymentsInfo().get(0)).isEqualTo(paymentInfoData);
    }

    @Test
    public void testPopulateFailedPaymentInfoForIpgCreditCard() {
        final TargetOrderData orderData = new TargetOrderData();
        final IpgPaymentInfoModel ipgPaymentInfo = mock(IpgPaymentInfoModel.class);
        given(orderModel.getPaymentInfo()).willReturn(ipgPaymentInfo);
        final List<PaymentTransactionModel> list = new ArrayList<>();
        final List<PaymentTransactionEntryModel> entryList = new ArrayList<>();
        final PaymentTransactionModel tran = mock(PaymentTransactionModel.class);
        final PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);
        given(entry.getTransactionStatus()).willReturn(TransactionStatus.REJECTED.name());
        final CreditCardPaymentInfoModel actualPaymentInfo = mock(CreditCardPaymentInfoModel.class);
        given(entry.getIpgPaymentInfo()).willReturn(actualPaymentInfo);
        given(tran.getEntries()).willReturn(entryList);
        entryList.add(entry);
        list.add(tran);
        given(orderModel.getPaymentTransactions()).willReturn(list);
        given(orderModel.getCncStoreNumber()).willReturn(null);
        final TargetCCPaymentInfoData paymentInfoData = mock(TargetCCPaymentInfoData.class);
        given(targetIpgPaymentInfoConverter.convert(ipgPaymentInfo)).willReturn(paymentInfoData);

        targetOrderPopulator.populate(orderModel, orderData);

        verifyZeroInteractions(creditCardPaymentInfoConverter);
        assertThat(orderData.getPaymentInfo()).isEqualTo(paymentInfoData);
    }

    @Test
    public void testSetTransactionPaymentInfoForNonIpgPayment() {
        final TargetOrderData orderData = new TargetOrderData();
        final List<PaymentTransactionModel> paymentTransactions = new ArrayList<>();
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        paymentTransactions.add(paymentTransaction);
        given(orderModel.getPaymentTransactions()).willReturn(paymentTransactions);

        final List<PaymentTransactionEntryModel> paymentTransactionEntries = new ArrayList<>();
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        paymentTransactionEntries.add(paymentTransactionEntryModel);
        given(paymentTransaction.getEntries()).willReturn(paymentTransactionEntries);
        given(paymentTransaction.getIsCaptureSuccessful()).willReturn(Boolean.TRUE);
        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransaction);
        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(null);

        final TransactionStatus status = TransactionStatus.ACCEPTED;
        final PaymentTransactionType type = PaymentTransactionType.CAPTURE;

        final CCPaymentInfoData paymentInfoData = mock(CCPaymentInfoData.class);

        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(status.toString());
        given(paymentTransactionEntryModel.getType()).willReturn(type);

        orderData.setPaymentInfo(paymentInfoData);

        targetOrderPopulator.setTransactionPaymentInfo(orderModel, orderData);

        assertThat(orderData.getPaymentsInfo()).isNotNull();
        assertThat(orderData.getPaymentsInfo().isEmpty()).isFalse();
        assertThat(orderData.getPaymentsInfo().size()).isEqualTo(1);
        assertThat(orderData.getPaymentsInfo().get(0)).isEqualTo(paymentInfoData);
    }

    @Test
    public void testSetTransactionPaymentInfoForIpgCreditCardPayment() {
        final TargetOrderData orderData = new TargetOrderData();
        final List<PaymentTransactionModel> paymentTransactions = new ArrayList<>();
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        paymentTransactions.add(paymentTransaction);
        given(orderModel.getPaymentTransactions()).willReturn(paymentTransactions);

        final List<PaymentTransactionEntryModel> paymentTransactionEntries = new ArrayList<>();
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        paymentTransactionEntries.add(paymentTransactionEntryModel);
        given(paymentTransaction.getEntries()).willReturn(paymentTransactionEntries);
        given(paymentTransaction.getIsCaptureSuccessful()).willReturn(Boolean.TRUE);
        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransaction);

        final CreditCardPaymentInfoModel creditCardPaymentInfoModel = mock(CreditCardPaymentInfoModel.class);
        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(creditCardPaymentInfoModel);

        final BigDecimal amount = new BigDecimal("10.99");
        final String receiptNumber = "123456789";
        final PriceData priceData = new PriceData();
        priceData.setValue(amount);

        given(paymentTransactionEntryModel.getAmount()).willReturn(amount);
        given(paymentTransactionEntryModel.getReceiptNo()).willReturn(receiptNumber);

        final TransactionStatus status = TransactionStatus.ACCEPTED;
        final PaymentTransactionType type = PaymentTransactionType.CAPTURE;

        final TargetCCPaymentInfoData paymentInfoData = new TargetCCPaymentInfoData();

        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(status.toString());
        given(paymentTransactionEntryModel.getType()).willReturn(type);

        given(creditCardPaymentInfoConverter.convert(creditCardPaymentInfoModel)).willReturn(paymentInfoData);
        given(priceDataFactory.create(PriceDataType.BUY, amount, orderModel.getCurrency().getIsocode())).willReturn(
                priceData);

        targetOrderPopulator.setTransactionPaymentInfo(orderModel, orderData);

        assertThat(orderData.getPaymentsInfo()).isNotNull();
        assertThat(orderData.getPaymentsInfo().isEmpty()).isFalse();
        assertThat(orderData.getPaymentsInfo().size()).isEqualTo(1);
        assertThat(orderData.getPaymentsInfo().get(0)).isEqualTo(paymentInfoData);
        assertThat(orderData.getPaymentsInfo().get(0) instanceof TargetCCPaymentInfoData).isTrue();
        assertThat(((TargetCCPaymentInfoData)orderData.getPaymentsInfo().get(0)).getAmount().getValue())
                .isEqualTo(amount);
        assertThat(((TargetCCPaymentInfoData)orderData.getPaymentsInfo().get(0)).getReceiptNumber())
                .isEqualTo(receiptNumber);
    }

    @Test
    public void testSetTransactionPaymentInfoForIpgGiftCardPayment() {
        final TargetOrderData orderData = new TargetOrderData();
        final List<PaymentTransactionModel> paymentTransactions = new ArrayList<>();
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        paymentTransactions.add(paymentTransaction);
        given(orderModel.getPaymentTransactions()).willReturn(paymentTransactions);

        final List<PaymentTransactionEntryModel> paymentTransactionEntries = new ArrayList<>();
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        paymentTransactionEntries.add(paymentTransactionEntryModel);
        given(paymentTransaction.getEntries()).willReturn(paymentTransactionEntries);
        given(paymentTransaction.getIsCaptureSuccessful()).willReturn(Boolean.TRUE);
        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransaction);

        final IpgGiftCardPaymentInfoModel giftCardPaymentInfoModel = mock(IpgGiftCardPaymentInfoModel.class);
        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(giftCardPaymentInfoModel);

        final BigDecimal amount = new BigDecimal("10.99");
        final String receiptNumber = "123456789";
        final PriceData priceData = new PriceData();
        priceData.setValue(amount);

        given(paymentTransactionEntryModel.getAmount()).willReturn(amount);
        given(paymentTransactionEntryModel.getReceiptNo()).willReturn(receiptNumber);

        final TransactionStatus status = TransactionStatus.ACCEPTED;
        final PaymentTransactionType type = PaymentTransactionType.CAPTURE;

        final TargetCCPaymentInfoData paymentInfoData = new TargetCCPaymentInfoData();

        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(status.toString());
        given(paymentTransactionEntryModel.getType()).willReturn(type);

        given(targetIpgGiftCardPaymentInfoConverter.convert(giftCardPaymentInfoModel)).willReturn(paymentInfoData);
        given(priceDataFactory.create(PriceDataType.BUY, amount, orderModel.getCurrency().getIsocode())).willReturn(
                priceData);

        targetOrderPopulator.setTransactionPaymentInfo(orderModel, orderData);

        assertThat(orderData.getPaymentsInfo()).isNotNull();
        assertThat(orderData.getPaymentsInfo().isEmpty()).isFalse();
        assertThat(orderData.getPaymentsInfo().size()).isEqualTo(1);
        assertThat(orderData.getPaymentsInfo().get(0)).isEqualTo(paymentInfoData);
        assertThat(orderData.getPaymentsInfo().get(0) instanceof TargetCCPaymentInfoData).isTrue();
        assertThat(((TargetCCPaymentInfoData)orderData.getPaymentsInfo().get(0)).getAmount().getValue())
                .isEqualTo(amount);
        assertThat(((TargetCCPaymentInfoData)orderData.getPaymentsInfo().get(0)).getReceiptNumber())
                .isEqualTo(receiptNumber);
    }

    @SuppressWarnings("static-access")
    @Test
    public void testSetTransactionPaymentInfoForIpgGiftCardAndCreditCardPayment() {
        final TargetOrderData orderData = new TargetOrderData();
        final List<PaymentTransactionModel> paymentTransactions = new ArrayList<>();
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        paymentTransactions.add(paymentTransaction);
        given(orderModel.getPaymentTransactions()).willReturn(paymentTransactions);

        final List<PaymentTransactionEntryModel> paymentTransactionEntries = new ArrayList<>();
        final PaymentTransactionEntryModel paymentTransactionEntryModel1 = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionEntryModel paymentTransactionEntryModel2 = mock(PaymentTransactionEntryModel.class);
        paymentTransactionEntries.add(paymentTransactionEntryModel1);
        paymentTransactionEntries.add(paymentTransactionEntryModel2);
        given(paymentTransaction.getEntries()).willReturn(paymentTransactionEntries);
        given(paymentTransaction.getIsCaptureSuccessful()).willReturn(Boolean.TRUE);
        given(paymentTransactionEntryModel1.getPaymentTransaction()).willReturn(paymentTransaction);
        given(paymentTransactionEntryModel2.getPaymentTransaction()).willReturn(paymentTransaction);

        final CreditCardPaymentInfoModel creditCardPaymentInfoModel = mock(CreditCardPaymentInfoModel.class);
        given(paymentTransactionEntryModel1.getIpgPaymentInfo()).willReturn(creditCardPaymentInfoModel);
        final IpgGiftCardPaymentInfoModel giftCardPaymentInfoModel = mock(IpgGiftCardPaymentInfoModel.class);
        given(paymentTransactionEntryModel2.getIpgPaymentInfo()).willReturn(giftCardPaymentInfoModel);

        final BigDecimal amount1 = new BigDecimal("10.99");
        final String receiptNumber1 = "123456789";
        final PriceData priceData1 = new PriceData();
        priceData1.setValue(amount1);

        final BigDecimal amount2 = new BigDecimal("15.00");
        final String receiptNumber2 = "789456135";
        final PriceData priceData2 = new PriceData();
        priceData2.setValue(amount2);

        given(paymentTransactionEntryModel1.getAmount()).willReturn(amount1);
        given(paymentTransactionEntryModel1.getReceiptNo()).willReturn(receiptNumber1);

        given(paymentTransactionEntryModel2.getAmount()).willReturn(amount2);
        given(paymentTransactionEntryModel2.getReceiptNo()).willReturn(receiptNumber2);

        final TransactionStatus status = TransactionStatus.ACCEPTED;
        final PaymentTransactionType type = PaymentTransactionType.CAPTURE;

        final TargetCCPaymentInfoData paymentInfoData1 = new TargetCCPaymentInfoData();
        final TargetCCPaymentInfoData paymentInfoData2 = new TargetCCPaymentInfoData();

        given(paymentTransactionEntryModel1.getTransactionStatus()).willReturn(status.toString());
        given(paymentTransactionEntryModel2.getTransactionStatus()).willReturn(status.toString());
        given(paymentTransactionEntryModel1.getType()).willReturn(type);
        given(paymentTransactionEntryModel2.getType()).willReturn(type);

        given(creditCardPaymentInfoConverter.convert(creditCardPaymentInfoModel)).willReturn(paymentInfoData1);
        given(targetIpgGiftCardPaymentInfoConverter.convert(giftCardPaymentInfoModel)).willReturn(paymentInfoData2);

        given(priceDataFactory.create(PriceDataType.BUY, amount1, orderModel.getCurrency().getIsocode())).willReturn(
                priceData1);
        given(priceDataFactory.create(PriceDataType.BUY, amount2, orderModel.getCurrency().getIsocode())).willReturn(
                priceData2);

        targetOrderPopulator.setTransactionPaymentInfo(orderModel, orderData);

        assertThat(orderData.getPaymentsInfo()).isNotNull();
        assertThat(orderData.getPaymentsInfo().isEmpty()).isFalse();
        assertThat(orderData.getPaymentsInfo().size()).isEqualTo(2);
        assertThat(orderData.getPaymentsInfo().get(0)).isEqualTo(paymentInfoData1);
        assertThat(orderData.getPaymentsInfo().get(1)).isEqualTo(paymentInfoData2);
        assertThat(orderData.getPaymentsInfo().get(0) instanceof TargetCCPaymentInfoData).isTrue();
        assertThat(((TargetCCPaymentInfoData)orderData.getPaymentsInfo().get(0)).getAmount().getValue())
                .isEqualTo(amount1);
        assertThat(((TargetCCPaymentInfoData)orderData.getPaymentsInfo().get(0)).getReceiptNumber())
                .isEqualTo(receiptNumber1);
        assertThat(((TargetCCPaymentInfoData)orderData.getPaymentsInfo().get(1)).getAmount().getValue())
                .isEqualTo(amount2);
        assertThat(((TargetCCPaymentInfoData)orderData.getPaymentsInfo().get(1)).getReceiptNumber())
                .isEqualTo(receiptNumber2);

    }

    @Test
    public void testPopulateMaskedFlybuysNumber() {
        given(orderModel.getFlyBuysCode()).willReturn("flybuysCode");
        given(invoiceFormatterHelper.getMaskedFlybuysNumber("flybuysCode")).willReturn("maskedFlybuysNumber");
        targetOrderPopulator.populate(orderModel, targetOrderData);
        assertThat(targetOrderData.getMaskedFlybuysNumber()).isEqualTo("maskedFlybuysNumber");
    }


    /**
     * This test method will verify the initial deposit and outstanding amount. 1. Here the product is a pre-order
     * product 2. The order should set and evaluate to pre-order deposit amount.
     */
    @Test
    public void testPopulateWithPreOrderDepositAmout() {
        final ArgumentCaptor<BigDecimal> bigDecimalCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        final BigDecimal totalPriceAmount = new BigDecimal("100.0");
        final PriceData totalPrice = new PriceData();
        totalPrice.setValue(totalPriceAmount);
        final BigDecimal preOrderDepositAmount = new BigDecimal("10.0");
        final PriceData preOrderDepositAmountPriceData = new PriceData();
        preOrderDepositAmountPriceData.setValue(preOrderDepositAmount);
        given(orderModel.getContainsPreOrderItems()).willReturn(Boolean.TRUE);
        given(orderModel.getPreOrderDepositAmount()).willReturn(Double.valueOf(10.0d));
        given(orderModel.getTotalPrice()).willReturn(Double.valueOf(100.0d));
        given(priceDataFactory.create(eq(PriceDataType.BUY), bigDecimalCaptor.capture(),
                eq(currencyModel)))
                .willReturn(totalPrice);
        given(abstractOrderHelper.createPrice(eq(orderModel), bigDecimalCaptor.capture()))
                .willReturn(preOrderDepositAmountPriceData);
        createPreOrderPaymentDetails(false);
        targetOrderPopulator.populate(orderModel, targetOrderData);
        assertThat(targetOrderData.isContainsPreOrderItems()).isTrue();
        assertThat(targetOrderData.getPreOrderDepositAmount().getValue()).isNotNull()
                .isEqualTo(BigDecimal.valueOf(10.0));
        //Outstanding amount evaluation
        assertThat(bigDecimalCaptor.getValue()).isNotNull().isEqualTo(BigDecimal.valueOf(90.0));
    }

    /**
     * This test method will verify initial deposit amount. 1. Here the product is a pre-order product and 2. The order
     * does not have pre-order deposit amount.
     */
    @Test
    public void testPopulateWithoutPreOrderDepositAmout() {
        final ArgumentCaptor<BigDecimal> bigDecimalCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        final BigDecimal totalPriceAmount = new BigDecimal("100.0");
        final PriceData totalPrice = new PriceData();
        totalPrice.setValue(totalPriceAmount);
        given(orderModel.getContainsPreOrderItems()).willReturn(Boolean.TRUE);
        given(orderModel.getTotalPrice()).willReturn(Double.valueOf(100.0d));
        targetOrderData.setTotalPrice(totalPrice);
        given(priceDataFactory.create(eq(PriceDataType.BUY), bigDecimalCaptor.capture(),
                eq(currencyModel)))
                .willReturn(totalPrice);
        given(abstractOrderHelper.createPrice(eq(orderModel), bigDecimalCaptor.capture()))
                .willReturn(new PriceData());
        createPreOrderPaymentDetails(false);
        targetOrderPopulator.populate(orderModel, targetOrderData);
        assertThat(targetOrderData.getPreOrderDepositAmount().getValue()).isNull();
        //Outstanding amount evaluation
        assertThat(bigDecimalCaptor.getValue()).isNotNull().isEqualTo(BigDecimal.valueOf(100.0));
    }

    /**
     *
     * This method will verify setting the values for pre-order deposit amount and outstanding amount for normal
     * product. Here in this case order data will never set both pre-order deposit amount as well as pre-order
     * outstanding amount attributes.
     */
    @Test
    public void testPopulateForNormalProduct() {
        given(orderModel.getContainsPreOrderItems()).willReturn(Boolean.FALSE);
        targetOrderPopulator.populate(orderModel, targetOrderData);
        assertThat(targetOrderData.getPreOrderDepositAmount()).isNull();
    }

    @Test
    public void testCheckMultipleConsimentsWithMultConsiment() {
        final AbstractOrderEntryModel abstractOrderEntryModel = mock(AbstractOrderEntryModel.class);
        given(orderModel.getEntries()).willReturn(Collections.singletonList(abstractOrderEntryModel));
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(2));
        given(deliveryModeModel.getCode()).willReturn("home-delivery");
        targetOrderPopulator.populate(orderModel, targetOrderData);
        assertThat(targetOrderData.isMultipleConsignments()).isTrue();
    }

    @Test
    public void testCheckMultipleConsimentsWithOneConsignment() {
        final AbstractOrderEntryModel abstractOrderEntryModel = mock(AbstractOrderEntryModel.class);
        given(orderModel.getEntries()).willReturn(Collections.singletonList(abstractOrderEntryModel));
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(1));
        given(deliveryModeModel.getCode()).willReturn("home-delivery");
        targetOrderPopulator.populate(orderModel, targetOrderData);
        assertThat(targetOrderData.isMultipleConsignments()).isFalse();
    }

    @Test
    public void testCheckMultipleConsimentsWithGiftCard() {
        final AbstractOrderEntryModel abstractOrderEntryModel = mock(AbstractOrderEntryModel.class);
        given(orderModel.getEntries()).willReturn(Collections.singletonList(abstractOrderEntryModel));
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(2));
        given(deliveryModeModel.getCode()).willReturn("home-delivery");
        final TargetProductModel productModel = mock(TargetProductModel.class);
        final ProductTypeModel productTypeModel = mock(ProductTypeModel.class);
        given(abstractOrderEntryModel.getProduct()).willReturn(productModel);
        given(productModel.getProductType()).willReturn(productTypeModel);
        given(productTypeModel.getCode()).willReturn(TgtCoreConstants.DIGITAL);
        targetOrderPopulator.populate(orderModel, targetOrderData);
        assertThat(targetOrderData.isMultipleConsignments()).isFalse();
    }

    @Test
    public void testCheckMultipleConsimentsWithNullDeliveryModel() {
        final AbstractOrderEntryModel abstractOrderEntryModel = mock(AbstractOrderEntryModel.class);
        given(orderModel.getDeliveryMode()).willReturn(null);
        given(orderModel.getEntries()).willReturn(Collections.singletonList(abstractOrderEntryModel));
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(1));
        given(deliveryModeModel.getCode()).willReturn("home-delivery");
        targetOrderPopulator.populate(orderModel, targetOrderData);
        assertThat(targetOrderData.isMultipleConsignments()).isFalse();
    }

    @Test
    public void testOutstandingAmountPaymentInfoWhenNoDeferredEntryExists() {
        final String tmd = "tmdValue";
        final Integer cncStorenumber = Integer.valueOf(20);
        given(orderModel.getTmdCardNumber()).willReturn(tmd);
        given(orderModel.getCncStoreNumber()).willReturn(cncStorenumber);
        given(orderModel.getPreOrderDepositAmount()).willReturn(Double.valueOf(10.0d));
        given(orderModel.getTotalPrice()).willReturn(Double.valueOf(100.0d));
        willReturn(Boolean.TRUE).given(orderModel).getContainsPreOrderItems();

        createPreOrderPaymentDetails(false);

        targetOrderPopulator.populate(orderModel, targetOrderData);

        assertThat(targetOrderData.getOutstandingAmountPaymentsInfo().get(0).getCardNumber()).isEqualTo("CapturedCard");
        assertThat(targetOrderData.getOutstandingAmountPaymentsInfo().get(0).getExpiryMonth())
                .isEqualTo("CapturedExpiryMonth");
        assertThat(targetOrderData.getOutstandingAmountPaymentsInfo().get(0).getExpiryYear())
                .isEqualTo("CapturedExpiryYear");
    }

    @Test
    public void testOutstandingAmountPaymentInfoWhenDeferredEntryExists() {
        final String tmd = "tmdValue";
        final Integer cncStorenumber = Integer.valueOf(20);
        given(orderModel.getTmdCardNumber()).willReturn(tmd);
        given(orderModel.getCncStoreNumber()).willReturn(cncStorenumber);
        given(orderModel.getPreOrderDepositAmount()).willReturn(Double.valueOf(10.0d));
        given(orderModel.getTotalPrice()).willReturn(Double.valueOf(100.0d));
        willReturn(Boolean.TRUE).given(orderModel).getContainsPreOrderItems();

        createPreOrderPaymentDetails(true);

        targetOrderPopulator.populate(orderModel, targetOrderData);

        assertThat(targetOrderData.getOutstandingAmountPaymentsInfo().get(0).getCardNumber()).isEqualTo("DeferredCard");
        assertThat(targetOrderData.getOutstandingAmountPaymentsInfo().get(0).getExpiryMonth())
                .isEqualTo("DeferredExpiryMonth");
        assertThat(targetOrderData.getOutstandingAmountPaymentsInfo().get(0).getExpiryYear())
                .isEqualTo("DeferredExpiryYear");
    }

    /**
     * 
     */
    private void createPreOrderPaymentDetails(final boolean createDeferredEntry) {
        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        final List<PaymentTransactionEntryModel> transactions = new ArrayList<>();
        final CreditCardPaymentInfoModel actualPaymentInfo = mock(CreditCardPaymentInfoModel.class);
        PaymentTransactionEntryModel paymentTransactionEntryModel;
        final CCPaymentInfoData ccPaymentInfoData = mock(CCPaymentInfoData.class);
        for (int i = 0; i < 5; i++) {
            paymentTransactionEntryModel = new PaymentTransactionEntryModel();
            paymentTransactionEntryModel.setAmount(new BigDecimal(i * 10));
            paymentTransactionEntryModel.setCreationtime(new Date(System.currentTimeMillis()));
            paymentTransactionEntryModel.setReceiptNo("receipt" + i);
            paymentTransactionEntryModel.setPaymentTransaction(paymentTransactionModel);
            paymentTransactionEntryModel.setTransactionStatus(i == 4 ? TransactionStatus.REJECTED.toString()
                    : TransactionStatus.ACCEPTED.toString());
            if (createDeferredEntry) {
                if (i == 0) {
                    paymentTransactionEntryModel.setType(PaymentTransactionType.CAPTURE);
                    given(ccPaymentInfoData.getCardNumber()).willReturn("CapturedCard");
                    given(ccPaymentInfoData.getExpiryYear()).willReturn("CapturedExpiryYear");
                    given(ccPaymentInfoData.getExpiryMonth()).willReturn("CapturedExpiryMonth");
                }
                else if (i == 1) {
                    paymentTransactionEntryModel.setType(PaymentTransactionType.DEFERRED);
                    given(ccPaymentInfoData.getCardNumber()).willReturn("DeferredCard");
                    given(ccPaymentInfoData.getExpiryYear()).willReturn("DeferredExpiryYear");
                    given(ccPaymentInfoData.getExpiryMonth()).willReturn("DeferredExpiryMonth");

                }
                else {
                    paymentTransactionEntryModel.setType(PaymentTransactionType.CANCEL);
                }
            }
            else {
                if (i == 1) {
                    paymentTransactionEntryModel.setType(PaymentTransactionType.CAPTURE);
                    given(ccPaymentInfoData.getCardNumber()).willReturn("CapturedCard");
                    given(ccPaymentInfoData.getExpiryYear()).willReturn("CapturedExpiryYear");
                    given(ccPaymentInfoData.getExpiryMonth()).willReturn("CapturedExpiryMonth");
                }
                else {
                    paymentTransactionEntryModel.setType(PaymentTransactionType.CANCEL);
                }
            }
            paymentTransactionEntryModel.setIpgPaymentInfo(actualPaymentInfo);
            transactions.add(paymentTransactionEntryModel);
        }

        given(paymentTransactionModel.getEntries()).willReturn(transactions);
        willReturn(Boolean.TRUE).given(paymentTransactionModel).getIsCaptureSuccessful();
        given(orderModel.getPaymentTransactions())
                .willReturn(Collections.singletonList(paymentTransactionModel));
        given(orderModel.getPaymentInfo()).willReturn(new IpgPaymentInfoModel());
        given(creditCardPaymentInfoConverter.convert(actualPaymentInfo)).willReturn(ccPaymentInfoData);
    }

}