/**
 *
 */
package au.com.target.tgtfacades.order.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.data.BaseOptionData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.product.data.VariantOptionQualifierData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.voucher.VoucherModelService;
import de.hybris.platform.voucher.jalo.util.VoucherValue;
import de.hybris.platform.voucher.model.PromotionVoucherModel;
import de.hybris.platform.voucher.model.SerialVoucherModel;
import de.hybris.platform.voucher.model.VoucherModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.StringUtils;
import org.fest.assertions.Assertions;
import org.fest.util.Collections;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.customer.TargetCustomerAccountService;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.giftcards.GiftCardService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.StoreEmployeeModel;
import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtcore.order.TargetCommerceCartModificationStatus;
import au.com.target.tgtcore.order.TargetCommerceCheckoutService;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.postcode.service.TargetPostCodeService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.order.converters.AbstractOrderHelper;
import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;
import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtfacades.user.converters.populator.TargetAddressReversePopulator;
import au.com.target.tgtfacades.user.data.IpgPaymentInfoData;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtfluent.data.StockDatum;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.service.FluentStockLookupService;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.order.TargetLaybyCommerceCheckoutService;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.dto.HostedSessionTokenRequest;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;
import au.com.target.tgtpayment.model.ZippayPaymentInfoModel;
import au.com.target.tgtpayment.service.TargetPaymentService;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


/**
 * @author asingh78
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCheckoutFacadeImplTest {

    private static final String DELIVERY_MODE_CODE = "home-delivery";

    @InjectMocks
    @Spy
    private final TargetCheckoutFacadeImpl targetCheckoutFacade = new TargetCheckoutFacadeImpl();

    @Mock
    private TargetLaybyCommerceCheckoutService mockTargetLaybyCommerceCheckoutService;

    @Mock(name = "cartConverter")
    private Converter<CartModel, CartData> mockCartConverter;

    @Mock(name = "miniCartConverter")
    private Converter<CartModel, CartData> mockMiniCartConverter;

    @Mock
    private TargetPurchaseOptionHelper mockTargetPurchaseOptionHelper;

    @Mock
    private SessionService mockSessionService;

    @Mock(name = "targetCommerceCheckoutService")
    private TargetCommerceCheckoutService mockTargetCommerceCheckoutService;

    @Mock
    private PaymentModeService mockPaymentModeService;

    @Mock
    private TargetPaymentService mockTargetPaymentService;

    @Mock
    private TargetCustomerAccountService mockTargetCustomerAccountService;

    @Mock
    private AbstractOrderHelper mockAbstractOrderHelper;

    @Mock
    private ModelService mockModelService;

    @Mock
    private TargetVoucherService mockTargetVoucherService;

    @Mock
    private VoucherModelService mockVoucherModelService;

    @Mock
    private PriceDataFactory mockPriceDataFactory;

    @Mock
    private CommerceCartService commerceCartService;

    @Mock
    private TargetAddressReversePopulator targetAddressReversePopulator;

    @Mock
    private UserService userService;

    @Mock
    private TargetDeliveryService mockTargetDeliveryService;

    @Mock
    private CurrencyModel currency;

    @Mock
    private DeliveryModeModel cncDeliveryMode;

    @Mock
    private DeliveryModeModel homeDeliveryMode;

    @Mock
    private DeliveryModeModel digitalDeliveryMode;

    @Mock
    private CartModel cartModel;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Mock
    private TargetZoneDeliveryModeModel targetZoneDeliveryMode;

    @Mock
    private Converter<AddressModel, AddressData> addressConverter;

    @Mock
    private CalculationService calculationService;

    @Mock
    private TargetOrderService targetOrderService;

    @Mock
    private PaymentModeModel mockPaymentModeModel;

    @Mock
    private GiftCardService giftCardService;

    @Mock
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Mock
    private TargetPostCodeService mockTargetPostCodeService;

    @Mock
    private CustomerModel customerModel;

    @Mock
    private ProductFacade productFacade;

    @Mock
    private PurchaseOptionModel purchaseOptionModel;

    @Mock
    private FluentStockLookupService fluentStockLookupService;

    @Mock
    private TargetFeatureSwitchFacade mockTargetFeatureSwitchFacade;

    @Mock
    private CheckoutCustomerStrategy checkoutCustomerStrategy;

    @Mock
    private CartFacade cartFacade;

    @Mock
    private TargetStockService targetStockService;

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private CartService cartService;

    @Mock
    private TargetUserFacade targetUserFacade;
    
    @Mock
    private TargetProductModel mockProduct1;

    @Mock
    private TargetProductModel mockProduct2;

    @Mock
    private AbstractTargetVariantProductModel mockVariantProduct1;

    @Mock
    private AbstractTargetVariantProductModel mockVariantProduct2;

    @Mock
    private ProductTypeModel mockProductType1;

    @Mock
    private ProductTypeModel mockProductType2;

    @Mock
    private AbstractOrderEntryModel mockOrderEntry1;

    @Mock
    private AbstractOrderEntryModel mockOrderEntry2;

    @Mock
    private TargetCreateSubscriptionResult targetCreateSubscriptionResult;

    @Before
    public void prepare() {
        MockitoAnnotations.initMocks(this);
        given(targetCheckoutFacade.getCart()).willReturn(cartModel);
        given(userService.getCurrentUser()).willReturn(customerModel);
        given(cartModel.getPurchaseOption()).willReturn(purchaseOptionModel);
    }


    @Test
    public void hasCheckoutCartTest() {
        final String purchaseOption = "purchaseOption";
        given(mockSessionService.getAttribute(TgtFacadesConstants.ACTIVE_PURCHASE_OPTION)).willReturn(
                purchaseOption);
        willReturn(purchaseOptionModel).given(mockTargetPurchaseOptionHelper)
                .getPurchaseOptionModel(purchaseOption);
        willReturn(Boolean.TRUE).given(mockTargetLaybyCommerceCheckoutService)
                .hasCheckoutCart(purchaseOptionModel);
        willReturn(purchaseOption).given(mockSessionService)
                .getAttribute(TgtFacadesConstants.ACTIVE_PURCHASE_OPTION);
        final boolean hasCart = targetCheckoutFacade.hasCheckoutCart();
        assertThat(hasCart).isTrue();

    }

    @Test
    public void hasCheckoutPurchaceOptionNullCartTest() {
        final String purchaseOption = "purchaseOption";
        given(mockSessionService.getAttribute(TgtFacadesConstants.ACTIVE_PURCHASE_OPTION)).willReturn(
                purchaseOption);
        willReturn(null).given(mockTargetPurchaseOptionHelper)
                .getPurchaseOptionModel(purchaseOption);
        willReturn(Boolean.TRUE).given(mockTargetLaybyCommerceCheckoutService)
                .hasCheckoutCart(purchaseOptionModel);
        final boolean hasCart = targetCheckoutFacade.hasCheckoutCart();
        assertThat(hasCart).isFalse();

    }

    @Test
    public void testGetCheckoutCartWithNoCart() {
        final String purchaseOption = "purchaseOption";
        willReturn(purchaseOptionModel).given(mockTargetPurchaseOptionHelper)
                .getPurchaseOptionModel(purchaseOption);
        willReturn(Boolean.FALSE).given(mockTargetLaybyCommerceCheckoutService)
                .hasCheckoutCart(purchaseOptionModel);
        targetCheckoutFacade.getCheckoutCart();
        verifyZeroInteractions(mockCartConverter);
    }

    @Test
    public void testGetCheckoutCartWithNullPuchaseOption() {
        final String purchaseOption = "purchaseOption";
        willReturn(null).given(mockTargetPurchaseOptionHelper)
                .getPurchaseOptionModel(purchaseOption);
        willReturn(Boolean.FALSE).given(mockTargetLaybyCommerceCheckoutService)
                .hasCheckoutCart(purchaseOptionModel);
        targetCheckoutFacade.getCheckoutCart();
        verifyZeroInteractions(mockCartConverter);
    }

    @Test
    public void testSetFlybuysNumberWithNoCart() {
        final String flybuysNumber = "6008943218842115";
        given(targetCheckoutFacade.getCart()).willReturn(null);
        final boolean result = targetCheckoutFacade.setFlybuysNumber(flybuysNumber);

        assertThat(result).isFalse();

        verifyZeroInteractions(mockTargetCommerceCheckoutService);
    }

    @Test
    public void testSetFlybuysNumberWithNullFlybuysNumber() {
        setUpGetCartToReturnCheckoutCart(cartModel);
        willReturn(Boolean.TRUE).given(mockTargetCommerceCheckoutService).setFlybuysNumber(cartModel, null);
        final boolean result = targetCheckoutFacade.setFlybuysNumber(null);
        assertThat(result).isTrue();
        verify(mockTargetCommerceCheckoutService).setFlybuysNumber(cartModel, null);
    }

    @Test
    public void testSetFlybuysNumberWithBlankFlybuysNumber() {
        setUpGetCartToReturnCheckoutCart(cartModel);

        final String flybuysNumber = "";

        willReturn(Boolean.TRUE).given(mockTargetCommerceCheckoutService).setFlybuysNumber(cartModel, flybuysNumber);

        final boolean result = targetCheckoutFacade.setFlybuysNumber(flybuysNumber);

        assertThat(result).isTrue();

        verify(mockTargetCommerceCheckoutService).setFlybuysNumber(cartModel, flybuysNumber);
    }

    @Test
    public void testSetFlybuysNumber() {

        setUpGetCartToReturnCheckoutCart(cartModel);

        final String flybuysNumber = "6008943218842115";

        willReturn(Boolean.TRUE).given(mockTargetCommerceCheckoutService).setFlybuysNumber(cartModel, flybuysNumber);

        final boolean result = targetCheckoutFacade.setFlybuysNumber(flybuysNumber);

        assertThat(result).isTrue();

        verify(mockTargetCommerceCheckoutService).setFlybuysNumber(cartModel, flybuysNumber);
    }

    @Test
    public void testSetFlybuysNumberWithFailure() {
        setUpGetCartToReturnCheckoutCart(cartModel);

        final String flybuysNumber = "6008943218842115";

        willReturn(Boolean.FALSE).given(mockTargetCommerceCheckoutService).setFlybuysNumber(cartModel, flybuysNumber);

        final boolean result = targetCheckoutFacade.setFlybuysNumber(flybuysNumber);

        assertThat(result).isFalse();

        verify(mockTargetCommerceCheckoutService).setFlybuysNumber(cartModel, flybuysNumber);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetPayPalSessionTokenWithNullReturnUrl() {
        final String returnUrl = null;
        final String cancelUrl = "http://cancel";


        targetCheckoutFacade.getPayPalSessionToken(returnUrl, cancelUrl);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetPayPalSessionTokenWithNullCancelUrl() {
        final String returnUrl = "http://return";
        final String cancelUrl = null;

        targetCheckoutFacade.getPayPalSessionToken(returnUrl, cancelUrl);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetPayPalSessionTokenWithBlankReturnUrl() {
        final String returnUrl = "";
        final String cancelUrl = "http://cancel";

        targetCheckoutFacade.getPayPalSessionToken(returnUrl, cancelUrl);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetPayPalSessionTokenWithBlankCancelUrl() {
        final String returnUrl = "http://return";
        final String cancelUrl = "";

        targetCheckoutFacade.getPayPalSessionToken(returnUrl, cancelUrl);
    }

    @Test
    public void testGetPayPalSessionTokenForBuyNow() {
        final String payPalSessionToken = "PAYPAL_TOKEN";

        final String cancelUrl = "http://cancel";
        final String returnUrl = "http://return";

        final PaymentModeModel mockPaymentMode = mock(PaymentModeModel.class);
        given(mockPaymentModeService.getPaymentModeForCode(TgtFacadesConstants.PAYPAL)).willReturn(
                mockPaymentMode);

        final Double totalPrice = Double.valueOf(10.0d);
        given(cartModel.getTotalPrice()).willReturn(totalPrice);
        setUpGetCartToReturnCheckoutCart(cartModel);
        final HostedSessionTokenRequest hostedSessionTokenRequest = mock(HostedSessionTokenRequest.class);
        willReturn(Boolean.FALSE).given(mockAbstractOrderHelper).isLayBy(cartModel);
        given(mockTargetPaymentService.createHostedSessionTokenRequest(cartModel, mockPaymentMode, cancelUrl,
                returnUrl, PaymentCaptureType.PLACEORDER, null, ListUtils.EMPTY_LIST))
                        .willReturn(hostedSessionTokenRequest);


        given(mockTargetPaymentService.getHostedSessionToken(hostedSessionTokenRequest))
                .willReturn(targetCreateSubscriptionResult);
        given(targetCreateSubscriptionResult.getRequestToken())
                .willReturn(payPalSessionToken);

        final TargetCreateSubscriptionResult targetCreateSubscriptionResult = targetCheckoutFacade
                .getPayPalSessionToken(returnUrl, cancelUrl);

        assertThat(targetCreateSubscriptionResult.getRequestToken()).isEqualTo(payPalSessionToken);

    }

    @Test
    public void testGetAfterpaySessionToken() {
        final String afterpaySessionToken = "AFTERPAY_TOKEN";


        final PaymentModeModel mockPaymentMode = mock(PaymentModeModel.class);
        given(mockPaymentModeService.getPaymentModeForCode(TgtFacadesConstants.AFTERPAY)).willReturn(mockPaymentMode);

        final Double totalPrice = Double.valueOf(10.0d);

        given(cartModel.getTotalPrice()).willReturn(totalPrice);
        setUpGetCartToReturnCheckoutCart(cartModel);
        final ArgumentCaptor<HostedSessionTokenRequest> captor = ArgumentCaptor
                .forClass(HostedSessionTokenRequest.class);
        given(mockTargetPaymentService.getHostedSessionToken(captor.capture()))
                .willReturn(targetCreateSubscriptionResult);
        given(targetCreateSubscriptionResult.getRequestToken())
                .willReturn(afterpaySessionToken);

        final TargetCreateSubscriptionResult targetCreateSubscriptionResult = targetCheckoutFacade
                .getAfterpaySessionToken("http://return",
                        "http://cancel");

        assertThat(targetCreateSubscriptionResult.getRequestToken()).isEqualTo(afterpaySessionToken);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetAfterpaySessionTokenWithNoReturnUrl() {
        targetCheckoutFacade.getAfterpaySessionToken(null, "http://cancel");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetAfterpaySessionTokenWithNoCancelUrl() {
        targetCheckoutFacade.getAfterpaySessionToken("http://return", null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreatePayPalPaymentInfoWithNullToken() {
        targetCheckoutFacade.createPayPalPaymentInfo(null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreatePayPalPaymentInfoWithNullPayerId() {
        final String token = "PAYPAL_TOKEN";

        targetCheckoutFacade.createPayPalPaymentInfo(token, null);
    }

    @Test
    public void testCreatePayPalPaymentInfoWithNoCart() {
        final String token = "PAYPAL_TOKEN";
        final String payerId = "PAYPAL_PAYER_ID";

        given(cartService.getSessionCart()).willReturn(null);
        targetCheckoutFacade.createPayPalPaymentInfo(token, payerId);

        verifyZeroInteractions(mockTargetCustomerAccountService, mockTargetCommerceCheckoutService);
    }

    @Test
    public void testCreatePayPalPaymentInfo() {
        final String token = "PAYPAL_TOKEN";
        final String payerId = "PAYPAL_PAYER_ID";

        final CustomerModel mockCustomer = mock(CustomerModel.class);

        given(cartModel.getUser()).willReturn(mockCustomer);
        setUpGetCartToReturnCheckoutCart(cartModel);

        final PaypalPaymentInfoModel mockPaypalPaymentInfo = mock(PaypalPaymentInfoModel.class);
        given(mockTargetCustomerAccountService.createPaypalPaymentInfo(mockCustomer, payerId, token))
                .willReturn(mockPaypalPaymentInfo);

        targetCheckoutFacade.createPayPalPaymentInfo(token, payerId);

        verify(mockTargetCommerceCheckoutService).setPaymentInfo(Mockito.any(CommerceCheckoutParameter.class));
    }

    private void setUpGetCartToReturnCheckoutCart(final CartModel cartModel) {
        final String purchaseOption = "buynow";

        given(mockSessionService.getAttribute(TgtFacadesConstants.ACTIVE_PURCHASE_OPTION)).willReturn(
                purchaseOption);

        final PurchaseOptionModel mockPurchaseOptionModel = mock(PurchaseOptionModel.class);
        given(mockTargetPurchaseOptionHelper.getPurchaseOptionModel(purchaseOption)).willReturn(
                mockPurchaseOptionModel);

        given(cartService.getSessionCart()).willReturn(cartModel);
    }

    private void setUpGiftCardMixedBasket() {
        willReturn(mockProductType1).given(mockProduct1).getProductType();
        willReturn(mockProduct1).given(mockVariantProduct1).getBaseProduct();
        willReturn(mockVariantProduct1).given(mockOrderEntry1).getProduct();
        willReturn(Long.valueOf(2L)).given(mockOrderEntry1).getQuantity();
        willReturn(mockProduct2).given(mockVariantProduct2)
                .getBaseProduct();
        willReturn(mockProductType2).given(mockProduct2).getProductType();
        willReturn(mockVariantProduct2).given(mockOrderEntry2).getProduct();
        willReturn(Long.valueOf(10L)).given(mockOrderEntry2).getQuantity();
        willReturn(cartModel).given(targetCheckoutFacade).getCart();
    }

    @Test
    public void testGetFlyBuysNumberWithFlybuysNumberInCart() {
        final String flybuysNumber = "6008943218842115";
        willReturn(flybuysNumber).given(cartModel).getFlyBuysCode();
        targetCheckoutFacade.applyUsersFlybuysNumberToCart();
        verify(targetCheckoutFacade, never()).setFlybuysNumber(Mockito.anyString());
    }

    @Test
    public void testGetFlyBuysNumberWithoutFlybuysNumberInUser() {
        final TargetCustomerModel user = mock(TargetCustomerModel.class);
        willReturn(user).given(cartModel).getUser();
        targetCheckoutFacade.applyUsersFlybuysNumberToCart();
        verify(targetCheckoutFacade, never()).setFlybuysNumber(Mockito.anyString());
    }

    @Test
    public void testGetFlyBuysNumberWithFlybuysNumberInUser() {
        final String flybuysNumber = "6008943218842115";
        final TargetCustomerModel user = mock(TargetCustomerModel.class);
        willReturn(user).given(cartModel).getUser();
        willReturn(flybuysNumber).given(user).getFlyBuysCode();
        targetCheckoutFacade.applyUsersFlybuysNumberToCart();
        verify(targetCheckoutFacade).setFlybuysNumber(flybuysNumber);
    }

    @Test
    public void testGetFlyBuysNumberWithNoFlybuysNumberInCartAndHavePaymentInfo() {
        final String flybuysNumber = "6008943218842115";
        final TargetCustomerModel user = mock(TargetCustomerModel.class);
        willReturn(user).given(cartModel).getUser();

        given(user.getFlyBuysCode()).willReturn(flybuysNumber);
        willReturn(Boolean.TRUE).given(mockTargetCommerceCheckoutService).setFlybuysNumber(cartModel, flybuysNumber);
        targetCheckoutFacade.applyUsersFlybuysNumberToCart();
        verify(mockTargetCommerceCheckoutService).setFlybuysNumber(cartModel, flybuysNumber);
    }

    @Test
    public void testApplyVoucherWithNull() throws JaloPriceFactoryException {
        assertThat(targetCheckoutFacade.applyVoucher(null)).isFalse();
        verify(cartModel, never()).setCalculated(Boolean.FALSE);
    }

    @Test
    public void testApplyVoucherWhereTheVoucherDoesntExist() throws JaloPriceFactoryException {
        final String voucher = "XYZ-123";
        assertThat(targetCheckoutFacade.applyVoucher(voucher)).isFalse();
        verify(cartModel, never()).setCalculated(Boolean.FALSE);
    }

    @Test
    public void testApplyVoucherThatIsRedeemable() throws JaloPriceFactoryException {
        final String voucher = "XYZ-123";
        final VoucherModel mockVoucherModel = mock(VoucherModel.class);
        willReturn(mockVoucherModel).given(mockTargetVoucherService).getVoucher(voucher);
        willReturn(Boolean.TRUE).given(mockVoucherModelService).isApplicable(mockVoucherModel, cartModel);
        willReturn(Boolean.TRUE).given(mockTargetVoucherService).redeemVoucher(voucher, cartModel);

        assertThat(targetCheckoutFacade.applyVoucher(voucher)).isTrue();
        verify(mockTargetVoucherService).redeemVoucher(voucher, cartModel);
        verify(cartModel).setCalculated(Boolean.FALSE);
    }

    @Test
    public void testApplyVoucherThatIsNotRedeemable() throws JaloPriceFactoryException {
        final String voucher = "XYZ-567";
        final VoucherModel mockVoucherModel = mock(VoucherModel.class);
        willReturn(mockVoucherModel).given(mockTargetVoucherService).getVoucher(voucher);
        willReturn(Boolean.TRUE).given(mockVoucherModelService).isApplicable(mockVoucherModel, cartModel);
        willReturn(Boolean.FALSE).given(mockTargetVoucherService).redeemVoucher(voucher, cartModel);

        assertThat(targetCheckoutFacade.applyVoucher(voucher)).isFalse();
        verify(mockTargetVoucherService).redeemVoucher(voucher, cartModel);
        verify(cartModel).setCalculated(Boolean.FALSE);
    }

    @Test
    public void testApplyVoucherThatIsRedeemableButNotApplicable() throws JaloPriceFactoryException {
        final String voucher = "XYZ-123";
        final VoucherModel mockVoucherModel = mock(VoucherModel.class);
        willReturn(mockVoucherModel).given(mockTargetVoucherService).getVoucher(voucher);
        willReturn(Boolean.FALSE).given(mockVoucherModelService).isApplicable(mockVoucherModel, cartModel);
        willReturn(Boolean.TRUE).given(mockTargetVoucherService).redeemVoucher(voucher, cartModel);

        assertThat(targetCheckoutFacade.applyVoucher(voucher)).isFalse();
        verify(mockTargetVoucherService, never()).redeemVoucher(voucher, cartModel);
        verify(cartModel, never()).setCalculated(Boolean.FALSE);
    }

    @Test
    public void testGetFirstAppliedPromotionalVoucherFromList() {
        final String voucher = "XYZ-123";
        final DiscountModel mockDiscountModel = mock(PromotionVoucherModel.class);
        final Collection<String> mockVouchersCollection = new ArrayList<>();
        final Collection<DiscountModel> mockDiscountModelsCollection = new ArrayList<>();
        mockVouchersCollection.add(voucher);
        mockDiscountModelsCollection.add(mockDiscountModel);

        given(mockTargetVoucherService.getAppliedVoucherCodes(cartModel)).willReturn(
                mockVouchersCollection);
        given(mockTargetVoucherService.getAppliedVouchers(cartModel)).willReturn(
                mockDiscountModelsCollection);
        assertThat(targetCheckoutFacade.getFirstAppliedVoucher()).isEqualTo(voucher);
    }

    @Test
    public void testGetFirstAppliedSerialVoucherFromList() {
        final String voucher = "XYZ-123";
        final DiscountModel mockDiscountModel = mock(SerialVoucherModel.class);
        final Collection<String> mockVouchersCollection = new ArrayList<>();
        final Collection<DiscountModel> mockDiscountModelsCollection = new ArrayList<>();
        mockVouchersCollection.add(voucher);
        mockDiscountModelsCollection.add(mockDiscountModel);
        given(mockTargetVoucherService.getAppliedVoucherCodes(cartModel)).willReturn(
                mockVouchersCollection);
        given(mockTargetVoucherService.getAppliedVouchers(cartModel)).willReturn(
                mockDiscountModelsCollection);
        assertThat(targetCheckoutFacade.getFirstAppliedVoucher()).isEqualTo(voucher);
    }

    @Test
    public void testGetFirstAppliedVoucherFromListWithFlybuys() {
        final String voucher = "XYZ-123";
        final DiscountModel mockDiscountModel = mock(FlybuysDiscountModel.class);
        final Collection<String> mockVouchersCollection = new ArrayList<>();
        final Collection<DiscountModel> mockDiscountModelsCollection = new ArrayList<>();
        mockVouchersCollection.add(voucher);
        mockDiscountModelsCollection.add(mockDiscountModel);
        given(mockTargetVoucherService.getAppliedVoucherCodes(cartModel)).willReturn(
                mockVouchersCollection);
        given(mockTargetVoucherService.getAppliedVouchers(cartModel)).willReturn(
                mockDiscountModelsCollection);
        assertThat(targetCheckoutFacade.getFirstAppliedVoucher()).isNull();
    }

    @Test
    public void testGetFirstAppliedVoucherFromEmptyList() {
        given(mockTargetVoucherService.getAppliedVoucherCodes(cartModel)).willReturn(
                CollectionUtils.EMPTY_COLLECTION);
        given(mockTargetVoucherService.getAppliedVouchers(cartModel)).willReturn(
                CollectionUtils.EMPTY_COLLECTION);
        assertThat(targetCheckoutFacade.getFirstAppliedVoucher()).isNull();
    }

    @Test
    public void testGetFirstAppliedVoucherFromInaccurateLists() {
        final DiscountModel mockDiscountModel = mock(DiscountModel.class);
        final Collection<DiscountModel> mockDiscountModelsCollection = new ArrayList<>();
        mockDiscountModelsCollection.add(mockDiscountModel);
        given(mockTargetVoucherService.getAppliedVoucherCodes(cartModel)).willReturn(
                CollectionUtils.EMPTY_COLLECTION);
        given(mockTargetVoucherService.getAppliedVouchers(cartModel)).willReturn(
                mockDiscountModelsCollection);
        assertThat(targetCheckoutFacade.getFirstAppliedVoucher()).isNull();
    }

    @Test
    public void testRemoveVoucher() throws JaloPriceFactoryException {
        final String voucher = "XYZ-123";
        targetCheckoutFacade.removeVoucher(voucher);
        verify(mockTargetVoucherService).releaseVoucher(voucher, cartModel);
        verify(cartModel).setCalculated(Boolean.FALSE);
    }

    @Test
    public void testRemoveVoucherNull() throws JaloPriceFactoryException {
        targetCheckoutFacade.removeVoucher(null);
        verify(mockTargetVoucherService, never()).releaseVoucher(null, cartModel);
        verify(cartModel, never()).setCalculated(Boolean.FALSE);
    }

    @Test
    public void testGetAppliedVoucherPrice() {
        final String currencyIso = "AU";
        final String voucher = "XYZ-123";
        final CurrencyModel mockCurrency = mock(CurrencyModel.class);
        given(mockCurrency.getIsocode()).willReturn(currencyIso);
        given(cartModel.getCurrency()).willReturn(mockCurrency);
        final VoucherModel mockVoucherModel = mock(VoucherModel.class);
        willReturn(mockVoucherModel).given(mockTargetVoucherService).getVoucher(voucher);

        final VoucherValue mockVoucherValue = mock(VoucherValue.class);
        willReturn(mockVoucherValue).given(mockVoucherModelService)
                .getAppliedValue(mockVoucherModel, cartModel);
        willReturn(Double.valueOf(9.99)).given(mockVoucherValue).getValue();

        final PriceData voucherPriceData = new PriceData();
        given(mockPriceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(mockVoucherValue.getValue()),
                currencyIso)).willReturn(voucherPriceData);

        assertThat(targetCheckoutFacade.getAppliedVoucherPrice(voucher)).isEqualTo(voucherPriceData);
    }

    @Test
    public void testGetAppliedVoucherPriceNullValue() {
        final String voucher = "XYZ-123";
        final VoucherModel mockVoucherModel = mock(VoucherModel.class);
        willReturn(mockVoucherModel).given(mockTargetVoucherService).getVoucher(voucher);
        assertThat(targetCheckoutFacade.getAppliedVoucherPrice(voucher)).isNull();
    }

    @Test
    public void testGetAppliedVoucherPriceNullVoucher() {
        final String voucher = "XYZ-123";
        assertThat(targetCheckoutFacade.getAppliedVoucherPrice(voucher)).isNull();
    }

    @Test
    public void testCreatePinPadPaymentInfo() {
        final TargetAddressData targetAddressData = mock(TargetAddressData.class);
        final TargetAddressModel targetAddressModel = mock(TargetAddressModel.class);
        given(mockModelService.create(TargetAddressModel.class)).willReturn(targetAddressModel);

        final CustomerModel mockCustomer = mock(CustomerModel.class);
        given(cartModel.getUser()).willReturn(mockCustomer);
        setUpGetCartToReturnCheckoutCart(cartModel);

        final PinPadPaymentInfoModel mockPinPadPaymentInfoModel = mock(PinPadPaymentInfoModel.class);
        given(mockTargetCustomerAccountService.createPinPadPaymentInfo(mockCustomer, targetAddressModel))
                .willReturn(mockPinPadPaymentInfoModel);

        targetCheckoutFacade.createPinPadPaymentInfo(targetAddressData);

        verify(mockTargetCommerceCheckoutService).setPaymentInfo(Mockito.any(CommerceCheckoutParameter.class));
    }

    @Test
    public void testSetKioskStoreNumberNullCart() {
        given(targetCheckoutFacade.getCart()).willReturn(null);
        final boolean result = targetCheckoutFacade.setKioskStoreNumber("5001");
        assertThat(result).isFalse();
        verifyZeroInteractions(mockTargetCommerceCheckoutService);
    }

    @Test
    public void testSetKioskStoreNumber() {
        setUpGetCartToReturnCheckoutCart(cartModel);

        final String kioskStoreNumber = "5001";
        willReturn(Boolean.TRUE).given(mockTargetCommerceCheckoutService).setKioskStoreNumber(cartModel,
                kioskStoreNumber);

        final boolean result = targetCheckoutFacade.setKioskStoreNumber(kioskStoreNumber);

        assertThat(result).isTrue();

        verify(mockTargetCommerceCheckoutService).setKioskStoreNumber(cartModel, kioskStoreNumber);
    }

    @Test
    public void testSetStoreAssistedNullCart() {
        given(targetCheckoutFacade.getCart()).willReturn(null);
        final boolean result = targetCheckoutFacade.setStoreAssisted("fred");

        assertThat(result).isFalse();

        verifyZeroInteractions(mockTargetCommerceCheckoutService);
    }

    @Test
    public void testSetStoreAssisted() {
        setUpGetCartToReturnCheckoutCart(cartModel);

        willReturn(Boolean.TRUE).given(mockTargetCommerceCheckoutService).setStoreAssisted(
                (CartModel)Mockito.any(), (StoreEmployeeModel)Mockito.any());

        final boolean result = targetCheckoutFacade.setStoreAssisted("fred");

        assertThat(result).isTrue();

        verify(mockTargetCommerceCheckoutService).setStoreAssisted((CartModel)Mockito.any(),
                (StoreEmployeeModel)Mockito.any());
    }

    private TargetZoneDeliveryModeValueModel mockDeliveryValue() {
        final TargetZoneDeliveryModeValueModel value = mock(TargetZoneDeliveryModeValueModel.class);
        given(value.getCurrency()).willReturn(currency);
        return value;
    }

    private RestrictableTargetZoneDeliveryModeValueModel mockDeliveryValueRestriction() {
        final RestrictableTargetZoneDeliveryModeValueModel value = Mockito
                .mock(RestrictableTargetZoneDeliveryModeValueModel.class);
        given(value.getCurrency()).willReturn(currency);
        return value;
    }

    @Test
    public void testGetFirstBulkyPriceMultipleValues() {
        final ProductModel mockProduct = mock(ProductModel.class);
        final TargetZoneDeliveryModeModel targetDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        final List<AbstractTargetZoneDeliveryModeValueModel> valueList = new ArrayList<>();

        final TargetZoneDeliveryModeValueModel value1 = mockDeliveryValue();
        given(value1.getBulky()).willReturn(Double.valueOf(2.00));
        given(value1.getPriority()).willReturn(Integer.valueOf(10));
        valueList.add(value1);

        final TargetZoneDeliveryModeValueModel value2 = mockDeliveryValue();
        given(value2.getBulky()).willReturn(Double.valueOf(1.00));
        given(value2.getPriority()).willReturn(Integer.valueOf(5));
        valueList.add(value2);

        final PriceData price1 = mock(PriceData.class);
        given(mockPriceDataFactory.create(Mockito.eq(PriceDataType.BUY),
                Mockito.eq(BigDecimal.valueOf(2.00)), Mockito.anyString())).willReturn(price1);
        final PriceData price2 = mock(PriceData.class);
        given(mockPriceDataFactory.create(Mockito.eq(PriceDataType.BUY),
                Mockito.eq(BigDecimal.valueOf(1.00)), Mockito.anyString())).willReturn(price2);
        setUpGetCartToReturnCheckoutCart(cartModel);
        given(
                mockTargetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(targetDeliveryModeModel,
                        mockProduct))
                                .willReturn(valueList);

        final PriceData priceData = targetCheckoutFacade.getFirstPrice(targetDeliveryModeModel, mockProduct);

        assertThat(priceData).isNotNull();
        assertThat(priceData).isSameAs(price1);
    }

    @Test
    public void testGetFirstBulkyPriceMultipleValuesForRestriction() {
        final ProductModel mockProduct = mock(ProductModel.class);
        final TargetZoneDeliveryModeModel targetDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        final List<AbstractTargetZoneDeliveryModeValueModel> valueList = new ArrayList<>();

        final RestrictableTargetZoneDeliveryModeValueModel value1 = mockDeliveryValueRestriction();
        given(value1.getValue()).willReturn(Double.valueOf(2.00));
        given(value1.getPriority()).willReturn(Integer.valueOf(10));
        valueList.add(value1);

        final RestrictableTargetZoneDeliveryModeValueModel value2 = mockDeliveryValueRestriction();
        given(value2.getValue()).willReturn(Double.valueOf(1.00));
        given(value2.getPriority()).willReturn(Integer.valueOf(5));
        valueList.add(value2);

        final PriceData price1 = mock(PriceData.class);
        given(mockPriceDataFactory.create(Mockito.eq(PriceDataType.BUY),
                Mockito.eq(BigDecimal.valueOf(2.00)), Mockito.anyString())).willReturn(price1);
        final PriceData price2 = mock(PriceData.class);
        given(mockPriceDataFactory.create(Mockito.eq(PriceDataType.BUY),
                Mockito.eq(BigDecimal.valueOf(1.00)), Mockito.anyString())).willReturn(price2);
        setUpGetCartToReturnCheckoutCart(cartModel);
        given(mockTargetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(targetDeliveryModeModel,
                mockProduct)).willReturn(valueList);

        final PriceData priceData = targetCheckoutFacade.getFirstPrice(targetDeliveryModeModel, mockProduct);

        assertThat(priceData).isNotNull();
        assertThat(priceData).isSameAs(price1);
    }

    @Test
    public void testGetFirstBulkyPriceOnlyOneValue() {
        final ProductModel mockProduct = mock(ProductModel.class);
        final TargetZoneDeliveryModeModel targetDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        final List<AbstractTargetZoneDeliveryModeValueModel> valueList = new ArrayList<>();

        final TargetZoneDeliveryModeValueModel value1 = mockDeliveryValue();
        given(value1.getBulky()).willReturn(Double.valueOf(1.00));
        valueList.add(value1);

        final PriceData price1 = mock(PriceData.class);
        given(mockPriceDataFactory.create(Mockito.eq(PriceDataType.BUY),
                Mockito.eq(BigDecimal.valueOf(1.00)), Mockito.anyString())).willReturn(price1);
        setUpGetCartToReturnCheckoutCart(cartModel);
        given(mockTargetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(targetDeliveryModeModel,
                mockProduct)).willReturn(valueList);

        final PriceData priceData = targetCheckoutFacade.getFirstPrice(targetDeliveryModeModel, mockProduct);

        assertThat(priceData).isNotNull();
        assertThat(priceData).isSameAs(price1);
    }

    @Test
    public void testGetFirstBulkyPriceOnlyOneValueRestriction() {
        final ProductModel mockProduct = mock(ProductModel.class);
        final TargetZoneDeliveryModeModel targetDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        final List<AbstractTargetZoneDeliveryModeValueModel> valueList = new ArrayList<>();

        final RestrictableTargetZoneDeliveryModeValueModel value1 = mockDeliveryValueRestriction();
        given(value1.getValue()).willReturn(Double.valueOf(1.00));
        valueList.add(value1);

        final PriceData price1 = mock(PriceData.class);
        given(mockPriceDataFactory.create(Mockito.eq(PriceDataType.BUY),
                Mockito.eq(BigDecimal.valueOf(1.00)), Mockito.anyString())).willReturn(price1);
        setUpGetCartToReturnCheckoutCart(cartModel);
        given(mockTargetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(targetDeliveryModeModel,
                mockProduct)).willReturn(valueList);

        final PriceData priceData = targetCheckoutFacade.getFirstPrice(targetDeliveryModeModel, mockProduct);

        assertThat(priceData).isNotNull();
        assertThat(priceData).isSameAs(price1);
    }

    @Test
    public void testGetFirstBulkyPriceEmptyCurrentValues() {
        final TargetZoneDeliveryModeModel targetDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        final List<AbstractTargetZoneDeliveryModeValueModel> valueList = new ArrayList<>();
        setUpGetCartToReturnCheckoutCart(cartModel);
        given(mockTargetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(targetDeliveryModeModel,
                cartModel)).willReturn(valueList);

        final ProductModel mockProduct = mock(ProductModel.class);

        final PriceData priceData = targetCheckoutFacade.getFirstPrice(targetDeliveryModeModel, mockProduct);

        assertThat(priceData).isNull();
    }

    @Test
    public void testGetFirstBulkyPriceNullCurrentValues() {
        final TargetZoneDeliveryModeModel targetDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);

        final ProductModel mockProduct = mock(ProductModel.class);

        final PriceData priceData = targetCheckoutFacade.getFirstPrice(targetDeliveryModeModel, mockProduct);

        assertThat(priceData).isNull();
    }

    @Test
    public void testGetLastThresholdPriceMultipleValues() {
        final ProductModel mockProduct = mock(ProductModel.class);
        final TargetZoneDeliveryModeModel targetDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        final List<AbstractTargetZoneDeliveryModeValueModel> valueList = new ArrayList<>();

        final TargetZoneDeliveryModeValueModel value1 = mockDeliveryValue();
        given(value1.getMinimum()).willReturn(Double.valueOf(1.00));
        valueList.add(value1);

        final TargetZoneDeliveryModeValueModel value2 = mockDeliveryValue();
        given(value2.getMinimum()).willReturn(Double.valueOf(2.00));
        valueList.add(value2);

        final PriceData price1 = mock(PriceData.class);
        given(mockPriceDataFactory.create(Mockito.eq(PriceDataType.BUY),
                Mockito.eq(BigDecimal.valueOf(1.00)), Mockito.anyString())).willReturn(price1);
        final PriceData price2 = mock(PriceData.class);
        given(mockPriceDataFactory.create(Mockito.eq(PriceDataType.BUY),
                Mockito.eq(BigDecimal.valueOf(2.00)), Mockito.anyString())).willReturn(price2);

        setUpGetCartToReturnCheckoutCart(cartModel);
        given(mockTargetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(targetDeliveryModeModel,
                mockProduct)).willReturn(valueList);

        final PriceData priceData = targetCheckoutFacade.getLastThreshold(targetDeliveryModeModel, mockProduct);

        assertThat(priceData).isNotNull();
        assertThat(priceData).isSameAs(price1);
    }

    @Test
    public void testGetLastThresholdPriceOnlyOneValue() {
        final ProductModel mockProduct = mock(ProductModel.class);
        final TargetZoneDeliveryModeModel targetDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        final List<AbstractTargetZoneDeliveryModeValueModel> valueList = new ArrayList<>();

        final AbstractTargetZoneDeliveryModeValueModel value1 = mockDeliveryValue();
        given(value1.getMinimum()).willReturn(Double.valueOf(1.00));
        valueList.add(value1);

        final PriceData price1 = mock(PriceData.class);
        given(mockPriceDataFactory.create(Mockito.eq(PriceDataType.BUY),
                Mockito.eq(BigDecimal.valueOf(1.00)), Mockito.anyString())).willReturn(price1);

        setUpGetCartToReturnCheckoutCart(cartModel);
        given(mockTargetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(targetDeliveryModeModel,
                mockProduct)).willReturn(valueList);

        final PriceData priceData = targetCheckoutFacade.getLastThreshold(targetDeliveryModeModel, mockProduct);

        assertThat(priceData).isNotNull();
        assertThat(priceData).isSameAs(price1);
    }

    @Test
    public void testGetLastThresholdPriceEmptyCurrentValues() {
        final TargetZoneDeliveryModeModel targetDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        final List<AbstractTargetZoneDeliveryModeValueModel> valueList = new ArrayList<>();

        setUpGetCartToReturnCheckoutCart(cartModel);
        given(mockTargetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(targetDeliveryModeModel,
                cartModel)).willReturn(valueList);

        final ProductModel mockProduct = mock(ProductModel.class);

        final PriceData priceData = targetCheckoutFacade.getLastThreshold(targetDeliveryModeModel, mockProduct);

        assertThat(priceData).isNull();
    }

    @Test
    public void testGetLastThresholdPriceNullCurrentValues() {
        final TargetZoneDeliveryModeModel targetDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);

        final ProductModel mockProduct = mock(ProductModel.class);

        final PriceData priceData = targetCheckoutFacade.getLastThreshold(targetDeliveryModeModel, mockProduct);

        assertThat(priceData).isNull();
    }


    @Test(expected = IllegalArgumentException.class)
    public void testHasDeliveryOptionConflictsIllegalArugments() {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        given(cartModel.getEntries()).willReturn(entries);
        targetCheckoutFacade.hasDeliveryOptionConflicts(cartModel, cncDeliveryMode);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testHasDeliveryOptionConflictsIllegalArugments2() {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final AbstractOrderEntryModel entry = mock(AbstractOrderEntryModel.class);
        entries.add(entry);
        given(cartModel.getEntries()).willReturn(entries);
        targetCheckoutFacade.hasDeliveryOptionConflicts(cartModel, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testHasDeliveryOptionConflictsIllegalArugments3() {
        targetCheckoutFacade.hasDeliveryOptionConflicts(null, homeDeliveryMode);
    }

    @Test
    public void testHasDeliveryOptionConflictsHappyPath() {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final AbstractOrderEntryModel e1 = mock(AbstractOrderEntryModel.class);
        final ProductModel tp1 = mock(ProductModel.class);
        given(tp1.getDeliveryModes()).willReturn(Collections.set(cncDeliveryMode, homeDeliveryMode));
        given(e1.getProduct()).willReturn(tp1);

        final AbstractOrderEntryModel e2 = mock(AbstractOrderEntryModel.class);
        final ProductModel tp2 = mock(ProductModel.class);
        given(tp2.getDeliveryModes()).willReturn(Collections.set(cncDeliveryMode, homeDeliveryMode));
        given(e2.getProduct()).willReturn(tp2);

        entries.add(e1);
        entries.add(e2);

        given(cartModel.getEntries()).willReturn(entries);
        given(tp1.getDeliveryModes()).willReturn(Collections.set(cncDeliveryMode, homeDeliveryMode));
        given(tp2.getDeliveryModes()).willReturn(Collections.set(cncDeliveryMode, homeDeliveryMode));
        final boolean isConfilcted1 = targetCheckoutFacade.hasDeliveryOptionConflicts(cartModel, homeDeliveryMode);
        assertThat(isConfilcted1).isFalse();

        given(tp1.getDeliveryModes()).willReturn(Collections.set(cncDeliveryMode, homeDeliveryMode));
        given(tp2.getDeliveryModes()).willReturn(Collections.set(cncDeliveryMode, homeDeliveryMode));
        final boolean isConfilcted2 = targetCheckoutFacade.hasDeliveryOptionConflicts(cartModel, cncDeliveryMode);
        assertThat(isConfilcted2).isFalse();
    }

    @Test
    public void testHasDeliveryOptionConflictsNoHomeDel() {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final AbstractOrderEntryModel e1 = mock(AbstractOrderEntryModel.class);
        final ProductModel tp1 = mock(ProductModel.class);
        given(e1.getProduct()).willReturn(tp1);

        final AbstractOrderEntryModel e2 = mock(AbstractOrderEntryModel.class);
        final ProductModel tp2 = mock(ProductModel.class);
        given(e2.getProduct()).willReturn(tp2);

        entries.add(e1);
        entries.add(e2);

        given(cartModel.getEntries()).willReturn(entries);

        given(tp1.getDeliveryModes()).willReturn(Collections.set(cncDeliveryMode));
        given(tp2.getDeliveryModes()).willReturn(Collections.set(cncDeliveryMode, homeDeliveryMode));
        willReturn(Boolean.FALSE).given(targetDeliveryModeHelper).hasOnlyDigitalDeliveryMode(Mockito
                .any(AbstractTargetVariantProductModel.class));
        boolean isConfilcted = targetCheckoutFacade.hasDeliveryOptionConflicts(cartModel, homeDeliveryMode);
        assertThat(isConfilcted).isTrue();

        given(tp1.getDeliveryModes()).willReturn(Collections.set(cncDeliveryMode, homeDeliveryMode));
        given(tp2.getDeliveryModes()).willReturn(Collections.set(cncDeliveryMode));
        isConfilcted = targetCheckoutFacade.hasDeliveryOptionConflicts(cartModel, homeDeliveryMode);
        assertThat(isConfilcted).isTrue();
    }

    @Test
    public void testHasDeliveryOptionConflictsNoCnc() {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final AbstractOrderEntryModel e1 = mock(AbstractOrderEntryModel.class);
        final ProductModel tp1 = mock(ProductModel.class);
        given(tp1.getDeliveryModes()).willReturn(Collections.set(cncDeliveryMode, homeDeliveryMode));
        given(e1.getProduct()).willReturn(tp1);

        final AbstractOrderEntryModel e2 = mock(AbstractOrderEntryModel.class);
        final ProductModel tp2 = mock(ProductModel.class);
        given(tp2.getDeliveryModes()).willReturn(Collections.set(cncDeliveryMode, homeDeliveryMode));
        given(e2.getProduct()).willReturn(tp2);

        entries.add(e1);
        entries.add(e2);

        given(cartModel.getEntries()).willReturn(entries);

        given(tp1.getDeliveryModes()).willReturn(Collections.set(homeDeliveryMode));
        given(tp2.getDeliveryModes()).willReturn(Collections.set(cncDeliveryMode, homeDeliveryMode));
        willReturn(Boolean.FALSE).given(targetDeliveryModeHelper).hasOnlyDigitalDeliveryMode(Mockito
                .any(AbstractTargetVariantProductModel.class));
        boolean isConfilcted = targetCheckoutFacade.hasDeliveryOptionConflicts(cartModel, cncDeliveryMode);
        assertThat(isConfilcted).isTrue();

        given(tp1.getDeliveryModes()).willReturn(Collections.set(cncDeliveryMode, homeDeliveryMode));
        given(tp2.getDeliveryModes()).willReturn(Collections.set(homeDeliveryMode));
        isConfilcted = targetCheckoutFacade.hasDeliveryOptionConflicts(cartModel, cncDeliveryMode);
        assertThat(isConfilcted).isTrue();
    }

    @Test
    public void testHasDeliveryOptionNoConflictsWithOnePhysicalAndOneDigital() {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final AbstractOrderEntryModel e1 = mock(AbstractOrderEntryModel.class);
        final ProductModel tp1 = mock(ProductModel.class);
        given(tp1.getDeliveryModes()).willReturn(Collections.set(cncDeliveryMode, homeDeliveryMode));
        given(e1.getProduct()).willReturn(tp1);

        final AbstractOrderEntryModel e2 = mock(AbstractOrderEntryModel.class);
        final ProductModel tp2 = mock(ProductModel.class);
        given(tp2.getDeliveryModes()).willReturn(Collections.set(digitalDeliveryMode));
        given(e2.getProduct()).willReturn(tp2);

        entries.add(e1);
        entries.add(e2);

        given(cartModel.getEntries()).willReturn(entries);

        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper).hasOnlyDigitalDeliveryMode(tp2);
        willReturn(Boolean.FALSE).given(targetDeliveryModeHelper).hasOnlyDigitalDeliveryMode(tp1);
        final boolean isConfilcted = targetCheckoutFacade.hasDeliveryOptionConflicts(cartModel, cncDeliveryMode);
        assertThat(isConfilcted).isFalse();

    }

    @Test
    public void testHasDeliveryOptionConflictsWithPhysicalAndOneDigital() {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final ProductModel p1 = mock(ProductModel.class);
        final AbstractOrderEntryModel e1 = mock(AbstractOrderEntryModel.class);
        final AbstractTargetVariantProductModel tp1 = mock(AbstractTargetVariantProductModel.class);
        given(tp1.getDeliveryModes()).willReturn(Collections.set(homeDeliveryMode));
        given(e1.getProduct()).willReturn(tp1);

        final AbstractOrderEntryModel e2 = mock(AbstractOrderEntryModel.class);
        final AbstractTargetVariantProductModel tp2 = mock(AbstractTargetVariantProductModel.class);
        given(tp2.getDeliveryModes()).willReturn(Collections.set(digitalDeliveryMode));
        given(e2.getProduct()).willReturn(tp2);

        final AbstractOrderEntryModel e3 = mock(AbstractOrderEntryModel.class);
        final AbstractTargetVariantProductModel tp3 = mock(AbstractTargetVariantProductModel.class);
        given(tp3.getDeliveryModes()).willReturn(Collections.set(cncDeliveryMode));
        given(e3.getProduct()).willReturn(tp3);

        given(tp1.getBaseProduct()).willReturn(p1);
        given(tp2.getBaseProduct()).willReturn(p1);
        given(tp3.getBaseProduct()).willReturn(p1);

        entries.add(e1);
        entries.add(e2);
        entries.add(e3);

        given(cartModel.getEntries()).willReturn(entries);

        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper).doesProductHavePhysicalDeliveryMode(tp2);
        willReturn(Boolean.FALSE).given(targetDeliveryModeHelper).doesProductHavePhysicalDeliveryMode(tp1);
        willReturn(Boolean.FALSE).given(targetDeliveryModeHelper).doesProductHavePhysicalDeliveryMode(tp3);
        final boolean isConfilcted = targetCheckoutFacade.hasDeliveryOptionConflicts(cartModel, cncDeliveryMode);
        assertThat(isConfilcted).isTrue();

    }

    @Test
    public void testGetSupportedDeliveryAddressesNull() {
        willReturn(null).given(targetCheckoutFacade).getSupportedDeliveryAddresses(true);
        targetCheckoutFacade.getSupportedDeliveryAddresses("deliveryModeCode", true);

        verifyZeroInteractions(mockTargetDeliveryService);
    }

    @Test
    public void testGetSupportedDeliveryAddressesEmpty() {
        willReturn(java.util.Collections.EMPTY_LIST).given(targetCheckoutFacade).getSupportedDeliveryAddresses(true);
        targetCheckoutFacade.getSupportedDeliveryAddresses("deliveryModeCode", true);

        verifyZeroInteractions(mockTargetDeliveryService);
    }

    @Test
    public void testGetSupportedDeliveryAddressesNotTargetAddresses() {
        final AddressData addressData = mock(AddressData.class);
        willReturn(Collections.list(addressData)).given(targetCheckoutFacade).getSupportedDeliveryAddresses(true);
        targetCheckoutFacade.getSupportedDeliveryAddresses("deliveryModeCode", true);
        verify(mockTargetDeliveryService, never()).isDeliveryModePostCodeCombinationValid(
                Mockito.any(DeliveryModeModel.class), Mockito.anyString(), Mockito.any(CartModel.class));
    }

    @Test
    public void testGetSupportedDeliveryAddresses() {
        final String expressZonePostCode = "3000";
        final String deliveryModeCode = "deliveryModeCode";

        final DeliveryModeModel deliveryMode = mock(DeliveryModeModel.class);

        final TargetAddressData expressZoneAddress = mock(TargetAddressData.class);
        given(expressZoneAddress.getPostalCode()).willReturn(expressZonePostCode);

        final TargetAddressData nonExpressZoneAddress = mock(TargetAddressData.class);
        given(nonExpressZoneAddress.getPostalCode()).willReturn("3215");

        given(mockTargetDeliveryService.getDeliveryModeForCode(deliveryModeCode)).willReturn(deliveryMode);

        setUpGetCartToReturnCheckoutCart(cartModel);
        willReturn(Boolean.TRUE).given(mockTargetDeliveryService).isDeliveryModePostCodeCombinationValid(deliveryMode,
                expressZonePostCode, cartModel);

        willReturn(Collections.list(expressZoneAddress, nonExpressZoneAddress)).given(targetCheckoutFacade)
                .getSupportedDeliveryAddresses(true);

        final List<AddressData> result = targetCheckoutFacade.getSupportedDeliveryAddresses("deliveryModeCode", true);

        verify(mockTargetDeliveryService).getDeliveryModeForCode(deliveryModeCode);
        verify(expressZoneAddress).setValidForDeliveryMode(Boolean.TRUE);
        verify(nonExpressZoneAddress).setValidForDeliveryMode(Boolean.FALSE);

        assertThat(result).isNotNull();
        assertThat(result.size() == 2).isTrue();
    }

    @Test
    public void getSavedAddressesWithNoGivenDeliveryModeNoApplicableDeliveryModes() {
        final TargetCartData cartData = mock(TargetCartData.class);
        willReturn(cartData).given(targetCheckoutFacade).getCheckoutCart();
        willReturn(null).given(targetCheckoutFacade).getDeliveryModes();

        final String expressZonePostCode = "3000";
        final String deliveryModeCode = "deliveryModeCode";

        final DeliveryModeModel deliveryMode = mock(DeliveryModeModel.class);

        final TargetAddressData expressZoneAddress = mock(TargetAddressData.class);
        given(expressZoneAddress.getPostalCode()).willReturn(expressZonePostCode);

        final TargetAddressData nonExpressZoneAddress = mock(TargetAddressData.class);
        given(nonExpressZoneAddress.getPostalCode()).willReturn("3215");
        given(mockTargetDeliveryService.getDeliveryModeForCode(deliveryModeCode)).willReturn(deliveryMode);
        setUpGetCartToReturnCheckoutCart(cartModel);
        willReturn(Boolean.TRUE).given(mockTargetDeliveryService).isDeliveryModePostCodeCombinationValid(deliveryMode,
                expressZonePostCode, cartModel);

        willReturn(Collections.list(expressZoneAddress, nonExpressZoneAddress)).given(targetCheckoutFacade)
                .getSupportedDeliveryAddresses(true);

        final List<AddressData> result = targetCheckoutFacade.getSupportedDeliveryAddresses(null, true);
        verify(expressZoneAddress, times(0)).setValidForDeliveryMode(any(Boolean.class));
        verify(nonExpressZoneAddress, times(0)).setValidForDeliveryMode(any(Boolean.class));

        assertThat(result).isNotNull();
        assertThat(result.size() == 2).isTrue();
    }

    @Test
    public void getSavedAddressesWithNoGivenDeliveryModeNoPhisicalDeliveryModes() {
        final TargetCartData cartData = mock(TargetCartData.class);
        final TargetZoneDeliveryModeData egiftCard = mock(TargetZoneDeliveryModeData.class);
        willReturn(cartData).given(targetCheckoutFacade).getCheckoutCart();
        willReturn(Boolean.TRUE).given(egiftCard).isDigitalDelivery();
        willReturn(ImmutableList.of(egiftCard)).given(targetCheckoutFacade).getDeliveryModes();

        final String expressZonePostCode = "3000";
        final String deliveryModeCode = "deliveryModeCode";
        final DeliveryModeModel deliveryMode = mock(DeliveryModeModel.class);
        final TargetAddressData expressZoneAddress = mock(TargetAddressData.class);
        given(expressZoneAddress.getPostalCode()).willReturn(expressZonePostCode);
        final TargetAddressData nonExpressZoneAddress = mock(TargetAddressData.class);
        given(nonExpressZoneAddress.getPostalCode()).willReturn("3215");
        given(mockTargetDeliveryService.getDeliveryModeForCode(deliveryModeCode)).willReturn(deliveryMode);
        setUpGetCartToReturnCheckoutCart(cartModel);
        willReturn(Boolean.TRUE).given(mockTargetDeliveryService).isDeliveryModePostCodeCombinationValid(deliveryMode,
                expressZonePostCode, cartModel);
        willReturn(Collections.list(expressZoneAddress, nonExpressZoneAddress)).given(targetCheckoutFacade)
                .getSupportedDeliveryAddresses(true);

        final List<AddressData> result = targetCheckoutFacade.getSupportedDeliveryAddresses(null, true);

        verify(expressZoneAddress, times(0)).setValidForDeliveryMode(any(Boolean.class));
        verify(nonExpressZoneAddress, times(0)).setValidForDeliveryMode(any(Boolean.class));
        assertThat(result).isNotNull();
        assertThat(result.size() == 2).isTrue();
    }

    @Test
    public void getSavedAddressesWithNoGivenDeliveryModeCncDeliveryModes() {
        final TargetCartData cartData = mock(TargetCartData.class);
        final TargetZoneDeliveryModeData cnc = mock(TargetZoneDeliveryModeData.class);
        willReturn(cartData).given(targetCheckoutFacade).getCheckoutCart();
        willReturn(Boolean.TRUE).given(cnc).isDeliveryToStore();
        willReturn(ImmutableList.of(cnc)).given(targetCheckoutFacade).getDeliveryModes();

        final String expressZonePostCode = "3000";
        final String deliveryModeCode = "deliveryModeCode";
        final DeliveryModeModel deliveryMode = mock(DeliveryModeModel.class);
        final TargetAddressData expressZoneAddress = mock(TargetAddressData.class);
        given(expressZoneAddress.getPostalCode()).willReturn(expressZonePostCode);
        final TargetAddressData nonExpressZoneAddress = mock(TargetAddressData.class);
        given(nonExpressZoneAddress.getPostalCode()).willReturn("3215");
        given(mockTargetDeliveryService.getDeliveryModeForCode(deliveryModeCode)).willReturn(deliveryMode);
        setUpGetCartToReturnCheckoutCart(cartModel);
        willReturn(Boolean.TRUE).given(mockTargetDeliveryService).isDeliveryModePostCodeCombinationValid(deliveryMode,
                expressZonePostCode, cartModel);
        willReturn(Collections.list(expressZoneAddress, nonExpressZoneAddress)).given(targetCheckoutFacade)
                .getSupportedDeliveryAddresses(true);

        final List<AddressData> result = targetCheckoutFacade.getSupportedDeliveryAddresses(null, true);

        verify(expressZoneAddress, times(0)).setValidForDeliveryMode(any(Boolean.class));
        verify(nonExpressZoneAddress, times(0)).setValidForDeliveryMode(any(Boolean.class));
        assertThat(result).isNotNull();
        assertThat(result.size() == 2).isTrue();
    }

    @Test
    public void getSavedAddressesWithNoGivenDeliveryMode() {
        final TargetCartData cartData = mock(TargetCartData.class);
        final TargetZoneDeliveryModeData hd = mock(TargetZoneDeliveryModeData.class);
        final TargetZoneDeliveryModeData ex = mock(TargetZoneDeliveryModeData.class);
        willReturn(cartData).given(targetCheckoutFacade).getCheckoutCart();
        willReturn(Boolean.FALSE).given(hd).isDeliveryToStore();
        willReturn(Boolean.FALSE).given(ex).isDeliveryToStore();
        willReturn(ImmutableList.of(hd, ex)).given(targetCheckoutFacade).getDeliveryModes();

        final String nonExpressZonePostCode = "3125";
        final String expressZonePostCode = "3000";
        final String homedeliveryModeCode = "home-delivery";
        final String exdeliveryModeCode = "express-delivery";
        final DeliveryModeModel exDeliveryMode = mock(DeliveryModeModel.class);
        final DeliveryModeModel homedeliveryMode = mock(DeliveryModeModel.class);
        final TargetAddressData expressZoneAddress = new TargetAddressData();
        expressZoneAddress.setPostalCode(expressZonePostCode);
        final TargetAddressData nonExpressZoneAddress = new TargetAddressData();
        nonExpressZoneAddress.setPostalCode(nonExpressZonePostCode);
        given(mockTargetDeliveryService.getDeliveryModeForCode(exdeliveryModeCode)).willReturn(exDeliveryMode);
        given(mockTargetDeliveryService.getDeliveryModeForCode(homedeliveryModeCode)).willReturn(homedeliveryMode);
        given(hd.getCode()).willReturn(homedeliveryModeCode);
        given(ex.getCode()).willReturn(exdeliveryModeCode);
        given(exDeliveryMode.getCode()).willReturn(exdeliveryModeCode);
        given(homedeliveryMode.getCode()).willReturn(homedeliveryModeCode);
        setUpGetCartToReturnCheckoutCart(cartModel);
        willReturn(Boolean.FALSE).given(mockTargetDeliveryService).isDeliveryModePostCodeCombinationValid(
                exDeliveryMode, nonExpressZonePostCode, cartModel);
        willReturn(Boolean.TRUE).given(mockTargetDeliveryService).isDeliveryModePostCodeCombinationValid(
                homedeliveryMode, nonExpressZonePostCode, cartModel);
        willReturn(Boolean.TRUE).given(mockTargetDeliveryService).isDeliveryModePostCodeCombinationValid(
                exDeliveryMode,
                expressZonePostCode, cartModel);
        willReturn(Boolean.TRUE).given(mockTargetDeliveryService).isDeliveryModePostCodeCombinationValid(
                homedeliveryMode, expressZonePostCode, cartModel);
        willReturn(Collections.list(expressZoneAddress, nonExpressZoneAddress)).given(targetCheckoutFacade)
                .getSupportedDeliveryAddresses(true);

        final List<AddressData> result = targetCheckoutFacade.getSupportedDeliveryAddresses(null, true);

        assertThat(expressZoneAddress.getValidDeliveryModeIds()).contains(homedeliveryModeCode,
                exdeliveryModeCode);
        assertThat(nonExpressZoneAddress.getValidDeliveryModeIds()).contains(homedeliveryModeCode);
        assertThat(result).isNotNull();
        assertThat(result.size() == 2).isTrue();
    }

    @Test
    public void getSavedAddressesWithNoGivenDeliveryModeAndNoPostcode() {
        final TargetCartData cartData = mock(TargetCartData.class);
        final TargetZoneDeliveryModeData hd = mock(TargetZoneDeliveryModeData.class);
        final TargetZoneDeliveryModeData ex = mock(TargetZoneDeliveryModeData.class);
        willReturn(cartData).given(targetCheckoutFacade).getCheckoutCart();
        willReturn(Boolean.FALSE).given(hd).isDeliveryToStore();
        willReturn(Boolean.FALSE).given(ex).isDeliveryToStore();
        willReturn(ImmutableList.of(hd, ex)).given(targetCheckoutFacade).getDeliveryModes();

        final String exdeliveryModeCode = "express-delivery";
        final DeliveryModeModel exDeliveryMode = mock(DeliveryModeModel.class);
        final TargetAddressData expressZoneAddress = new TargetAddressData();
        given(mockTargetDeliveryService.getDeliveryModeForCode(exdeliveryModeCode)).willReturn(exDeliveryMode);
        given(ex.getCode()).willReturn(exdeliveryModeCode);
        given(exDeliveryMode.getCode()).willReturn(exdeliveryModeCode);
        setUpGetCartToReturnCheckoutCart(cartModel);
        final TargetNoPostCodeException targetNoPostCodeException = mock(TargetNoPostCodeException.class);
        willThrow(targetNoPostCodeException).given(mockTargetDeliveryService).isDeliveryModePostCodeCombinationValid(
                exDeliveryMode,
                (String)null, cartModel);
        willReturn(Collections.list(expressZoneAddress)).given(targetCheckoutFacade)
                .getSupportedDeliveryAddresses(true);

        targetCheckoutFacade.getSupportedDeliveryAddresses(null, true);

        assertThat(expressZoneAddress.getValidDeliveryModeIds()).contains(exdeliveryModeCode);
    }

    @Test
    public void testGetSupportedDeliveryAddressesNoPostCodeFound() {
        final String expressZonePostCode = "3000";
        final String deliveryModeCode = "deliveryModeCode";

        final DeliveryModeModel deliveryMode = mock(DeliveryModeModel.class);
        final AddressData addressData = new TargetAddressData();
        addressData.setId("1234");
        given(targetUserFacade.getDefaultAddress()).willReturn(addressData);
        final List<AddressData> supportedDeliveryAddresses = Arrays.asList(addressData);

        final TargetAddressData expressZoneAddress = mock(TargetAddressData.class);
        given(expressZoneAddress.getPostalCode()).willReturn(expressZonePostCode);

        final TargetAddressData nonExpressZoneAddress = mock(TargetAddressData.class);
        given(nonExpressZoneAddress.getPostalCode()).willReturn("3215");

        given(mockTargetDeliveryService.getDeliveryModeForCode(deliveryModeCode)).willReturn(deliveryMode);
        setUpGetCartToReturnCheckoutCart(cartModel);
        willReturn(Boolean.FALSE).given(mockTargetDeliveryService).isDeliveryModePostCodeCombinationValid(deliveryMode,
                expressZonePostCode, cartModel);
        given(targetCheckoutFacade.getSupportedDeliveryAddresses(true)).willReturn(supportedDeliveryAddresses);
        final List<AddressData> returnedAddresses = targetCheckoutFacade
                .getSupportedDeliveryAddresses("deliveryModeCode", true);
        assertThat(CollectionUtils.isNotEmpty(returnedAddresses)).isTrue();
        assertThat(returnedAddresses.size()).isEqualTo(1);
        assertThat(((TargetAddressData)returnedAddresses.get(0)).getValidForDeliveryMode().booleanValue()).isFalse();
    }

    @Test
    public void testGetExistingTmSessionID() {
        cartModel = new CartModel();
        cartModel.setCode("012345");
        cartModel.setThreatMatrixSessionID("existing");
        setUpGetCartToReturnCheckoutCart(cartModel);
        final String result = targetCheckoutFacade.getThreatMatrixSessionID();
        assertThat(result).isEqualTo("existing");
        verifyZeroInteractions(mockModelService);
    }

    @Test
    public void testGetTmSessionID() {
        cartModel = new CartModel();
        cartModel.setCode("012345");
        setUpGetCartToReturnCheckoutCart(cartModel);
        final String result = targetCheckoutFacade.getThreatMatrixSessionID();
        verify(mockModelService).save(cartModel);
        assertThat(result).isNotNull();
        assertThat(result).isNotSameAs(cartModel.getCode());
    }

    @Test
    public void testGetTmSessionIDWithEmptyCart() {
        cartModel = new CartModel();
        cartModel.setCode("");
        setUpGetCartToReturnCheckoutCart(cartModel);
        final String result = targetCheckoutFacade.getThreatMatrixSessionID();
        assertThat(result).isNull();
    }

    @Test
    public void testGetTmSessionIDNullCartModel() {
        cartModel = null;
        setUpGetCartToReturnCheckoutCart(cartModel);
        final String result = targetCheckoutFacade.getThreatMatrixSessionID();
        assertThat(result).isNull();
    }

    @Test
    public void testGetTmSessionIDNullCartModelCode() {
        given(cartModel.getCode()).willReturn(null);
        setUpGetCartToReturnCheckoutCart(cartModel);
        final String result = targetCheckoutFacade.getThreatMatrixSessionID();
        assertThat(result).isNull();
    }

    @Test
    public void testSetThreatMatrixSessionIdForNullCart() {
        targetCheckoutFacade.setThreatMatrixSessionID(null);
        verifyZeroInteractions(mockModelService);
    }

    @Test
    public void testIsFlybuysDiscountAlreadyAppliedWithNoCart() {
        assertThat(targetCheckoutFacade.isFlybuysDiscountAlreadyApplied("12345")).isFalse();
    }

    @Test
    public void testIsFlybuysDiscountAlreadyAppliedWithNewFlybuys() {
        setUpGetCartToReturnCheckoutCart(cartModel);
        given(cartModel.getFlyBuysCode()).willReturn(StringUtils.EMPTY);

        assertThat(targetCheckoutFacade.isFlybuysDiscountAlreadyApplied("12345")).isFalse();
    }

    @Test
    public void testIsFlybuysDiscountAlreadyAppliedWithSameFlybuys() {
        final String flybuysnumber = "12345";
        setUpGetCartToReturnCheckoutCart(cartModel);
        given(cartModel.getFlyBuysCode()).willReturn(flybuysnumber);
        final FlybuysDiscountModel flybuysDiscount = mock(FlybuysDiscountModel.class);
        given(mockTargetVoucherService.getFlybuysDiscountForOrder(cartModel)).willReturn(flybuysDiscount);

        assertThat(targetCheckoutFacade.isFlybuysDiscountAlreadyApplied(flybuysnumber)).isFalse();
    }

    @Test
    public void testIsFlybuysDiscountAlreadyAppliedWithSameFlybuysButNotAsADiscount() {
        final String flybuysnumber = "12345";
        setUpGetCartToReturnCheckoutCart(cartModel);
        given(cartModel.getFlyBuysCode()).willReturn(flybuysnumber);

        assertThat(targetCheckoutFacade.isFlybuysDiscountAlreadyApplied(flybuysnumber)).isFalse();
    }

    @Test
    public void testIsFlybuysDiscountAlreadyAppliedWithDifferentFlybuys() {
        setUpGetCartToReturnCheckoutCart(cartModel);
        given(cartModel.getFlyBuysCode()).willReturn("54321");
        final FlybuysDiscountModel flybuysDiscount = mock(FlybuysDiscountModel.class);
        given(mockTargetVoucherService.getFlybuysDiscountForOrder(cartModel)).willReturn(flybuysDiscount);

        assertThat(targetCheckoutFacade.isFlybuysDiscountAlreadyApplied("12345")).isTrue();
    }

    @Test
    public void testIsFlybuysDiscountAlreadyAppliedWithDifferentFlybuysButNotAsADiscount() {
        setUpGetCartToReturnCheckoutCart(cartModel);
        given(cartModel.getFlyBuysCode()).willReturn("54321");

        assertThat(targetCheckoutFacade.isFlybuysDiscountAlreadyApplied("12345")).isFalse();
    }

    @Test
    public void testRemovePaymentInfo() {
        targetCheckoutFacade.removePaymentInfo();
        verify(mockTargetCommerceCheckoutService).removePaymentInfo(cartModel);
    }

    @Test
    public void testRemoveSelectedCncStore() {
        given(cartModel.getCncStoreNumber()).willReturn(Integer.valueOf(1));
        targetCheckoutFacade.removeSelectedCncStore();

        verify(cartModel).setCncStoreNumber(null);
        verify(cartModel).setDeliveryAddress(null);
        verify(mockModelService).save(cartModel);
        verify(mockModelService).refresh(cartModel);
    }

    @Test
    public void testRemoveSelectedCncStoreFromNull() {
        given(cartService.getSessionCart()).willReturn(null);
        targetCheckoutFacade.removeSelectedCncStore();
        verifyZeroInteractions(mockModelService);
    }

    @Test
    public void testRemoveSelectedCncStoreForCartWithoutSelection() {
        given(cartModel.getCncStoreNumber()).willReturn(null);
        targetCheckoutFacade.removeSelectedCncStore();
        verifyZeroInteractions(mockModelService);
    }

    @Test
    public void testUpdateDeliveryModeWhenSelectedDeliveryModeExists() {
        given(targetDeliveryModeHelper.getDeliveryModeModel(DELIVERY_MODE_CODE)).willReturn(targetZoneDeliveryMode);
        targetCheckoutFacade.updateDeliveryMode(DELIVERY_MODE_CODE);
        verify(targetCheckoutFacade).setDeliveryMode(DELIVERY_MODE_CODE);
        verify(targetCheckoutFacade).removeDeliveryAddressFromCart();
    }

    @Test
    public void testUpdateDeliveryModeWhenSelectedDeliveryModeIsEmpty() {
        targetCheckoutFacade.updateDeliveryMode(StringUtils.EMPTY);
        verify(targetCheckoutFacade, never()).setDeliveryMode(StringUtils.EMPTY);
        verify(targetCheckoutFacade, never()).removeDeliveryAddressFromCart();
    }

    @Test
    public void testCreateIpgPaymentInfoToCart() {
        final TargetAddressModel targetAddressModel = mock(TargetAddressModel.class);
        given(mockModelService.create(TargetAddressModel.class)).willReturn(targetAddressModel);
        final IpgPaymentInfoModel ipgPaymentInfo = mock(IpgPaymentInfoModel.class);
        given(
                mockTargetCustomerAccountService.createIpgPaymentInfo(any(CustomerModel.class),
                        any(TargetAddressModel.class), any(IpgPaymentTemplateType.class))).willReturn(
                                ipgPaymentInfo);
        final TargetAddressData targetBillingAddressData = mock(TargetAddressData.class);

        targetCheckoutFacade.createIpgPaymentInfo(targetBillingAddressData, IpgPaymentTemplateType.CREDITCARDSINGLE);

        verify(targetAddressReversePopulator).populate(targetBillingAddressData, targetAddressModel);
    }

    @Test
    public void testCreateIpgPaymentInfoWithoutBillingAddressToCart() {
        given(cartModel.getUser()).willReturn(customerModel);

        final IpgPaymentInfoModel ipgPaymentInfo = mock(IpgPaymentInfoModel.class);
        given(
                mockTargetCustomerAccountService.createIpgPaymentInfo(any(CustomerModel.class),
                        any(TargetAddressModel.class), any(IpgPaymentTemplateType.class))).willReturn(
                                ipgPaymentInfo);

        targetCheckoutFacade.createIpgPaymentInfo(null, IpgPaymentTemplateType.CREDITCARDSINGLE);
        verify(mockTargetCustomerAccountService).createIpgPaymentInfo(customerModel, null,
                IpgPaymentTemplateType.CREDITCARDSINGLE);
        verifyZeroInteractions(targetAddressReversePopulator);
    }

    @Test(expected = AdapterException.class)
    public void shouldThrowExceptionWhenCreateIPGPaymentInfoIfIpgIsNotAvaiable() {
        final TargetAddressModel targetAddressModel = mock(TargetAddressModel.class);
        final TargetAddressData targetBillingAddressData = mock(TargetAddressData.class);

        given(mockModelService.create(TargetAddressModel.class)).willReturn(targetAddressModel);
        willThrow(new AdapterException()).given(mockTargetCustomerAccountService).createIpgPaymentInfo(
                any(CustomerModel.class),
                any(TargetAddressModel.class), any(IpgPaymentTemplateType.class));

        targetCheckoutFacade.createIpgPaymentInfo(targetBillingAddressData, IpgPaymentTemplateType.CREDITCARDSINGLE);
    }

    @Test
    public void testCreateIpgPaymentInfoToEmptyCart() {
        given(cartService.getSessionCart()).willReturn(null);
        final TargetAddressModel targetAddressModel = mock(TargetAddressModel.class);
        given(mockModelService.create(TargetAddressModel.class)).willReturn(targetAddressModel);
        final TargetAddressData targetBillingAddressData = mock(TargetAddressData.class);

        targetCheckoutFacade.createIpgPaymentInfo(targetBillingAddressData, IpgPaymentTemplateType.CREDITCARDSINGLE);

        verify(targetAddressReversePopulator, times(0)).populate(targetBillingAddressData, targetAddressModel);
        verify(targetAddressModel, times(0)).setOwner(cartModel);
        verify(cartModel, times(0)).setPaymentAddress(targetAddressModel);
    }

    @Test
    public void testGetPaymentInfoDataWithWrongPaymentMode() {
        final PaymentModeModel paymentMode = mock(PaymentModeModel.class);
        given(paymentMode.getCode()).willReturn("badmode");
        given(cartModel.getPaymentMode()).willReturn(paymentMode);
        final AddressModel paymentAddress = mock(AddressModel.class);
        given(cartModel.getPaymentAddress()).willReturn(paymentAddress);
        final AddressData addressData = mock(AddressData.class);
        given(addressConverter.convert(paymentAddress)).willReturn(addressData);

        final CCPaymentInfoData paymentInfoData = targetCheckoutFacade.getPaymentDetails();

        assertThat(paymentInfoData).isNull();
    }

    @Test
    public void testRemoveDeliveryInfoWhenCartExists() throws CalculationException {
        final CartModel mockedCart = mock(CartModel.class);
        given(targetCheckoutFacade.getCart()).willReturn(mockedCart);
        willDoNothing().given(calculationService).calculateTotals(mockedCart, true);
        targetCheckoutFacade.removeDeliveryInfo();
        verify(mockSessionService).removeAttribute(TgtCoreConstants.SESSION_POSTCODE);
        verify(mockedCart).setDeliveryMode(null);
        verify(mockedCart).setDeliveryAddress(null);
        verify(mockedCart).setCncStoreNumber(null);
        verify(mockedCart).setZoneDeliveryModeValue(null);
        verify(mockModelService).save(mockedCart);
        verify(mockModelService).refresh(mockedCart);
        verify(calculationService).calculateTotals(mockedCart, true);
    }

    @Test(expected = IllegalStateException.class)
    public void testRemoveDeliveryInfoWhenUnableToRecalculateCart() throws CalculationException {
        willThrow(new CalculationException("Message")).given(calculationService).calculateTotals(cartModel, true);
        targetCheckoutFacade.removeDeliveryInfo();
    }

    @Test
    public void testRemoveDeliveryInfoWhenCartDoesntExist() {
        given(targetCheckoutFacade.getCart()).willReturn(null);
        targetCheckoutFacade.removeDeliveryInfo();
        verifyZeroInteractions(mockSessionService);
        verifyZeroInteractions(mockModelService);
    }

    @Test
    public void testGetIpgSessionWithNullCart() {
        final PaymentModeModel paymentMode = mock(PaymentModeModel.class);
        given(mockPaymentModeService.getPaymentModeForCode(TgtFacadesConstants.IPG)).willReturn(paymentMode);
        given(targetCheckoutFacade.getCart()).willReturn(null);

        final TargetCreateSubscriptionResult targetCreateSubscriptionResult = targetCheckoutFacade
                .getIpgSessionToken("returnUrl");
        assertThat(targetCreateSubscriptionResult).isNull();
    }

    @Test
    public void testUpdateIpgPaymentInfo() {
        final CartModel mockedCart = mock(CartModel.class);
        final IpgPaymentInfoModel ipgPaymentInfoModel = mock(IpgPaymentInfoModel.class);
        given(targetCheckoutFacade.getCart()).willReturn(mockedCart);
        given(mockedCart.getPaymentInfo()).willReturn(ipgPaymentInfoModel);
        given(mockedCart.getCode()).willReturn("cart1");
        given(mockedCart.getUniqueKeyForPayment()).willReturn("1");
        assertThat(targetCheckoutFacade.updateIpgPaymentInfoWithToken("ipgToken")).isTrue();

        verify(ipgPaymentInfoModel).setToken("ipgToken");
        verify(ipgPaymentInfoModel).setSessionId("1");
        verify(mockModelService).save(ipgPaymentInfoModel);
    }

    @Test
    public void testUpdateIpgTokenForPaypalPaymentInfo() {
        final CartModel mockedCart = mock(CartModel.class);
        final PaypalPaymentInfoModel paymentInfoModel = mock(PaypalPaymentInfoModel.class);
        given(targetCheckoutFacade.getCart()).willReturn(mockedCart);
        given(mockedCart.getPaymentInfo()).willReturn(paymentInfoModel);
        given(mockedCart.getCode()).willReturn("cart1");
        given(mockedCart.getUniqueKeyForPayment()).willReturn("1");
        assertThat(targetCheckoutFacade.updateIpgPaymentInfoWithToken("ipgToken")).isFalse();
    }

    @Test
    public void testCreateUniqueKeyNotCalledWhenUniqueKeyAvailableInCart() {
        final String uniqueKey = "123456879";
        final PaymentModeModel mockPaymentMode = mock(PaymentModeModel.class);
        given(mockPaymentModeService.getPaymentModeForCode(TgtFacadesConstants.IPG)).willReturn(
                mockPaymentMode);

        final Double totalPrice = Double.valueOf(10.0d);

        final CartModel mockCart = mock(CartModel.class);
        given(mockCart.getTotalPrice()).willReturn(totalPrice);
        given(mockCart.getUniqueKeyForPayment()).willReturn(uniqueKey);
        setUpGetCartToReturnCheckoutCart(mockCart);

        targetCheckoutFacade.getIpgSessionToken("returnUrl");
        verify(targetOrderService, Mockito.times(0)).createUniqueKeyForPaymentInitiation(
                mockCart);
    }

    @Test
    public void testCreateUniqueKeyCalledWhenUniqueKeyInCartIsNull() {
        final PaymentModeModel mockPaymentMode = mock(PaymentModeModel.class);
        given(mockPaymentModeService.getPaymentModeForCode(TgtFacadesConstants.IPG)).willReturn(
                mockPaymentMode);

        final Double totalPrice = Double.valueOf(10.0d);


        given(cartModel.getTotalPrice()).willReturn(totalPrice);
        given(cartModel.getUniqueKeyForPayment()).willReturn(null);
        setUpGetCartToReturnCheckoutCart(cartModel);

        targetCheckoutFacade.getIpgSessionToken("returnUrl");
        verify(targetOrderService).createUniqueKeyForPaymentInitiation(cartModel);
    }

    @Test
    public void testCreateUniqueKeyCalledWhenUniqueKeyInCartIsEmpty() {
        final PaymentModeModel mockPaymentMode = mock(PaymentModeModel.class);
        given(mockPaymentModeService.getPaymentModeForCode(TgtFacadesConstants.IPG)).willReturn(
                mockPaymentMode);

        final Double totalPrice = Double.valueOf(10.0d);

        given(cartModel.getTotalPrice()).willReturn(totalPrice);
        given(cartModel.getUniqueKeyForPayment()).willReturn("");
        setUpGetCartToReturnCheckoutCart(cartModel);
        targetCheckoutFacade.getIpgSessionToken("returnUrl");
        verify(targetOrderService).createUniqueKeyForPaymentInitiation(cartModel);
    }

    @Test
    public void testGetIpgSession() {
        final String ipgSessionToken = "IpgSessionToken";
        final String sessionId = "123456";
        final PaymentModeModel mockPaymentMode = mock(PaymentModeModel.class);
        given(mockPaymentModeService.getPaymentModeForCode(TgtFacadesConstants.IPG)).willReturn(
                mockPaymentMode);

        final Double totalPrice = Double.valueOf(10.0d);

        given(cartModel.getTotalPrice()).willReturn(totalPrice);

        setUpGetCartToReturnCheckoutCart(cartModel);
        final HostedSessionTokenRequest hostedSessionTokenRequest = mock(HostedSessionTokenRequest.class);

        given(targetOrderService.createUniqueKeyForPaymentInitiation(cartModel)).willReturn(
                sessionId);
        given(mockTargetPaymentService.createHostedSessionTokenRequest(cartModel, mockPaymentMode, "returnUrl",
                "returnUrl", PaymentCaptureType.PLACEORDER, sessionId, ListUtils.EMPTY_LIST))
                        .willReturn(hostedSessionTokenRequest);

        given(mockTargetPaymentService.getHostedSessionToken(hostedSessionTokenRequest))
                .willReturn(targetCreateSubscriptionResult);
        given(targetCreateSubscriptionResult.getRequestToken())
                .willReturn(ipgSessionToken);

        final TargetCreateSubscriptionResult targetCreateSubscriptionResult = targetCheckoutFacade
                .getIpgSessionToken("returnUrl");

        assertThat(targetCreateSubscriptionResult.getRequestToken()).isEqualTo(ipgSessionToken);
    }

    @Test
    public void testDeliveryFeeSameAfterAddressChange() {
        given(cartModel.getDeliveryCost()).willReturn(Double.valueOf(9));
        assertThat(targetCheckoutFacade.isDeliveryFeeVariesAfterAddressChange(BigDecimal.valueOf(9))).isFalse();
    }

    @Test
    public void testIsDeliveryFeeDifferentAfterChangeAddress() {
        given(cartModel.getDeliveryCost()).willReturn(Double.valueOf(10));
        assertThat(targetCheckoutFacade.isDeliveryFeeVariesAfterAddressChange(BigDecimal.valueOf(9))).isTrue();
    }

    @Test
    public void testIsDeliveryFeeWhenDeliveryCostNull() {
        final CartModel mockedCart = mock(CartModel.class);
        given(targetCheckoutFacade.getCart()).willReturn(mockedCart);
        given(mockedCart.getDeliveryCost()).willReturn(null);
        assertThat(targetCheckoutFacade.isDeliveryFeeVariesAfterAddressChange(BigDecimal.valueOf(9))).isFalse();
    }

    @Test
    public void testRemoveDeliveryAddressFromCart() {
        targetCheckoutFacade.removeDeliveryAddressFromCart();
        verify(mockSessionService, Mockito.times(0)).getAttribute(Mockito.anyString());
    }

    @Test
    public void testIsGiftCardPaymentAllowedForCartWhenCartContainsGiftCard() {
        willReturn(Boolean.TRUE).given(cartFacade).hasSessionCart();
        willReturn(Boolean.TRUE).given(giftCardService).doesCartHaveAGiftCard(cartModel);
        assertThat(targetCheckoutFacade.isGiftCardPaymentAllowedForCart()).isFalse();
    }

    @Test
    public void testIsGiftCardPaymentAllowedForCartWhenCartDoesntContainsGiftCard() {
        willReturn(Boolean.FALSE).given(giftCardService).doesCartHaveAGiftCard(cartModel);
        assertThat(targetCheckoutFacade.isGiftCardPaymentAllowedForCart()).isTrue();
    }

    @Test
    public void testIsGiftCardInCartWhenCartContainsGiftCard() {
        willReturn(Boolean.TRUE).given(cartFacade).hasSessionCart();
        given(cartService.getSessionCart()).willReturn(cartModel);
        willReturn(Boolean.TRUE).given(giftCardService).doesCartHaveAGiftCard(cartModel);
        assertThat(targetCheckoutFacade.isGiftCardInCart()).isTrue();
    }

    @Test
    public void testIsGiftCardInCartWhenEmptyCart() {
        willReturn(Boolean.FALSE).given(cartFacade).hasSessionCart();
        willReturn(Boolean.FALSE).given(giftCardService).doesCartHaveAGiftCard(cartModel);
        assertThat(targetCheckoutFacade.isGiftCardInCart()).isFalse();
    }

    @Test
    public void testIsGiftCardInCartWhenCartDoesntContainsGiftCard() {
        willReturn(Boolean.TRUE).given(cartFacade).hasSessionCart();
        willReturn(Boolean.FALSE).given(giftCardService).doesCartHaveAGiftCard(cartModel);
        assertThat(targetCheckoutFacade.isGiftCardInCart()).isFalse();
    }

    @Test
    public void testDoesCartContainGiftCardOnlyWhenCartContainsGiftCardOnly() {
        willReturn(Boolean.TRUE).given(giftCardService).doesCartHaveGiftCardsOnly(cartModel);
        assertThat(targetCheckoutFacade.doesCartHaveGiftCardOnly()).isTrue();
    }

    @Test
    public void testDoesCartContainGiftCardOnlyWhenCartContainsMixedProducts() {
        willReturn(Boolean.FALSE).given(giftCardService).doesCartHaveGiftCardsOnly(cartModel);
        assertThat(targetCheckoutFacade.doesCartHaveGiftCardOnly()).isFalse();
    }

    @Test
    public void testIsPaymentModeGiftCardWhenPaymentModeIsNull() {
        given(cartModel.getPaymentMode()).willReturn(null);
        assertThat(targetCheckoutFacade.isPaymentModeGiftCard()).isFalse();
    }

    @Test
    public void testIsPaymentModeGiftCardWhenPaymentModeIsCreditCard() {
        given(cartModel.getPaymentMode()).willReturn(mockPaymentModeModel);
        given(mockPaymentModeModel.getCode()).willReturn(TgtFacadesConstants.CREDIT_CARD);
        assertThat(targetCheckoutFacade.isPaymentModeGiftCard()).isFalse();
    }

    @Test
    public void testIsPaymentModeGiftCardWhenPaymentModeIsGiftCard() {
        given(cartModel.getPaymentMode()).willReturn(mockPaymentModeModel);
        given(mockPaymentModeModel.getCode()).willReturn(TgtFacadesConstants.GIFT_CARD);
        assertThat(targetCheckoutFacade.isPaymentModeGiftCard()).isTrue();
    }

    public void testGetIpgSessionTokenFromCartWhenCartDataIsNull() {
        final String token = targetCheckoutFacade.getIpgSessionTokenFromCart(null);
        assertThat(token).isNull();
    }

    @Test
    public void testGetIpgSessionTokenFromCartWhenPaymentInfoIsNull() {
        final CartData cartData = mock(CartData.class);
        given(cartData.getPaymentInfo()).willReturn(null);
        final String token = targetCheckoutFacade.getIpgSessionTokenFromCart(cartData);
        assertThat(token).isNull();
    }

    @Test
    public void testGetIpgSessionTokenFromCartWhenPaymentInfoIsNotTypeCCPaymentInfoData() {
        final CartData cartData = mock(CartData.class);
        final CCPaymentInfoData ccPaymentInfoData = mock(CCPaymentInfoData.class);
        given(cartData.getPaymentInfo()).willReturn(ccPaymentInfoData);

        final String token = targetCheckoutFacade.getIpgSessionTokenFromCart(cartData);
        assertThat(token).isNull();
    }

    @Test
    public void testGetIpgSessionTokenFromCartWhenIpgPaymentInfoDataIsNull() {
        final CartData cartData = mock(CartData.class);
        final TargetCCPaymentInfoData targetCcPaymentInfoData = mock(TargetCCPaymentInfoData.class);
        given(cartData.getPaymentInfo()).willReturn(targetCcPaymentInfoData);
        given(targetCcPaymentInfoData.getIpgPaymentInfoData()).willReturn(null);

        final String token = targetCheckoutFacade.getIpgSessionTokenFromCart(cartData);
        assertThat(token).isNull();
    }

    @Test
    public void testGetIpgSessionTokenFromCartWhenIpgPaymentInfoDataDoesNotHaveIpgToken() {
        final CartData cartData = mock(CartData.class);
        final TargetCCPaymentInfoData targetCcPaymentInfoData = mock(TargetCCPaymentInfoData.class);
        given(cartData.getPaymentInfo()).willReturn(targetCcPaymentInfoData);
        final IpgPaymentInfoData ipgPaymentInfoData = mock(IpgPaymentInfoData.class);
        given(targetCcPaymentInfoData.getIpgPaymentInfoData()).willReturn(ipgPaymentInfoData);
        given(ipgPaymentInfoData.getToken()).willReturn(null);

        final String token = targetCheckoutFacade.getIpgSessionTokenFromCart(cartData);
        assertThat(token).isNull();
    }

    @Test
    public void testGetIpgSessionTokenFromCartWhenIpgPaymentInfoDataHasEmptyIpgToken() {
        final CartData cartData = mock(CartData.class);
        final TargetCCPaymentInfoData targetCcPaymentInfoData = mock(TargetCCPaymentInfoData.class);
        given(cartData.getPaymentInfo()).willReturn(targetCcPaymentInfoData);
        final IpgPaymentInfoData ipgPaymentInfoData = mock(IpgPaymentInfoData.class);
        given(targetCcPaymentInfoData.getIpgPaymentInfoData()).willReturn(ipgPaymentInfoData);
        given(ipgPaymentInfoData.getToken()).willReturn("");

        final String token = targetCheckoutFacade.getIpgSessionTokenFromCart(cartData);
        assertThat(token).isNotNull();
        assertThat(StringUtils.isEmpty(token)).isTrue();
    }

    @Test
    public void testGetIpgSessionTokenFromCartWhenIpgPaymentInfoDataHasIpgToken() {
        final String token = "123456789";
        final CartData cartData = mock(CartData.class);
        final TargetCCPaymentInfoData targetCcPaymentInfoData = mock(TargetCCPaymentInfoData.class);
        given(cartData.getPaymentInfo()).willReturn(targetCcPaymentInfoData);
        final IpgPaymentInfoData ipgPaymentInfoData = mock(IpgPaymentInfoData.class);
        given(targetCcPaymentInfoData.getIpgPaymentInfoData()).willReturn(ipgPaymentInfoData);
        given(ipgPaymentInfoData.getToken()).willReturn(token);

        final String returnedToken = targetCheckoutFacade.getIpgSessionTokenFromCart(cartData);
        assertThat(returnedToken).isNotNull();
        assertThat(returnedToken).isEqualTo(token);
    }

    @Test
    public void isSupportedDeliveryModeForNullCart() {
        given(targetCheckoutFacade.getCart()).willReturn(null);
        assertThat(targetCheckoutFacade.isSupportedDeliveryMode("delMode")).isFalse();
    }

    @Test
    public void isSupportedDeliveryModeForUnknownDeliveryMode() {
        given(mockTargetDeliveryService.getDeliveryModeForCode("delMode")).willReturn(null);
        assertThat(targetCheckoutFacade.isSupportedDeliveryMode("delMode")).isFalse();
    }

    @Test
    public void isSupportedDeliveryModeForNotSupportedDeliveryMode() {
        final TargetZoneDeliveryModeModel deliveryMode = mock(TargetZoneDeliveryModeModel.class);
        given(mockTargetDeliveryService.getDeliveryModeForCode("delMode")).willReturn(deliveryMode);
        willReturn(Boolean.FALSE).given(targetDeliveryModeHelper).isDeliveryModeApplicableForOrder(cartModel,
                deliveryMode);
        assertThat(targetCheckoutFacade.isSupportedDeliveryMode("delMode")).isFalse();
    }

    @Test
    public void isSupportedDeliveryModeForSupportedDeliveryMode() {
        final TargetZoneDeliveryModeModel deliveryMode = mock(TargetZoneDeliveryModeModel.class);
        given(mockTargetDeliveryService.getDeliveryModeForCode("delMode")).willReturn(deliveryMode);
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper).isDeliveryModeApplicableForOrder(cartModel,
                deliveryMode);
        assertThat(targetCheckoutFacade.isSupportedDeliveryMode("delMode")).isTrue();
    }

    @Test
    public void testSetCncStoreWithEmptyCart() {
        final Integer storeNumber = Integer.valueOf(1);
        final TargetPointOfServiceData posData = mock(TargetPointOfServiceData.class);
        given(targetStoreLocatorFacade.getPointOfService(storeNumber)).willReturn(posData);
        given(cartService.getSessionCart()).willReturn(null);
        assertThat(targetCheckoutFacade.setClickAndCollectStore(1)).isNotNull();
        verify(cartModel, times(0)).setCncStoreNumber(storeNumber);
        verifyZeroInteractions(posData);
    }

    @Test
    public void testSetCncStoreWithNonExistingIdentifier() {
        final Integer storeNumber = Integer.valueOf(1);
        final TargetPointOfServiceData posData = mock(TargetPointOfServiceData.class);
        given(targetStoreLocatorFacade.getPointOfService(storeNumber)).willReturn(null);

        assertThat(targetCheckoutFacade.setClickAndCollectStore(1)).isNull();
        verify(cartModel, times(0)).setCncStoreNumber(storeNumber);
        verifyZeroInteractions(posData);
    }

    @Test
    public void testSetCncStore() {
        final Integer storeNumber = Integer.valueOf(1);
        final TargetPointOfServiceData posData = mock(TargetPointOfServiceData.class);
        final Set<String> cartProductTypeSet = mock(Set.class);
        given(targetStoreLocatorFacade.getPointOfService(storeNumber)).willReturn(posData);
        given(posData.getAvailableForPickup()).willReturn(Boolean.TRUE);
        given(mockAbstractOrderHelper.getProductTypeSet(cartModel)).willReturn(cartProductTypeSet);

        assertThat(targetCheckoutFacade.setClickAndCollectStore(1)).isNotNull();
        verify(mockAbstractOrderHelper).getProductTypeSet(cartModel);
        verify(mockAbstractOrderHelper).updateStoreWithPickupAvailability(cartProductTypeSet, posData);
        verify(cartModel).setCncStoreNumber(storeNumber);
        verify(mockModelService).save(cartModel);
    }

    @Test
    public void testSetCncStoreThatNotAvailableForPickup() {
        final Integer storeNumber = Integer.valueOf(1);
        final TargetPointOfServiceData posData = mock(TargetPointOfServiceData.class);
        final Set<String> cartProductTypeSet = mock(Set.class);
        given(targetStoreLocatorFacade.getPointOfService(storeNumber)).willReturn(posData);
        given(posData.getAvailableForPickup()).willReturn(Boolean.FALSE);
        given(mockAbstractOrderHelper.getProductTypeSet(cartModel)).willReturn(cartProductTypeSet);

        assertThat(targetCheckoutFacade.setClickAndCollectStore(1)).isNotNull();
        verify(mockAbstractOrderHelper).getProductTypeSet(cartModel);
        verify(mockAbstractOrderHelper).updateStoreWithPickupAvailability(cartProductTypeSet, posData);
        verify(cartModel, times(0)).setCncStoreNumber(storeNumber);
        verifyZeroInteractions(mockModelService);
    }

    @Test
    public void testGetDeliveryModesReturnsNullWhenCartDeliveryModesIsNull() {
        given(mockAbstractOrderHelper.getPhysicalDeliveryModes(cartModel)).willReturn(null);
        final List<TargetZoneDeliveryModeData> deliveryModes = targetCheckoutFacade.getDeliveryModes();
        assertThat(deliveryModes).isNull();
        verify(targetCheckoutFacade, never()).overlayFluentStockDataOnDeliveryModes(Mockito.anyList());
    }

    @Test
    public void testGetDeliveryModesReturnsNullWhenCartDeliveryModesIsEmpty() {
        final TargetCartData cartData = mock(TargetCartData.class);
        given(cartData.getDeliveryModes()).willReturn(new ArrayList());

        final List<TargetZoneDeliveryModeData> deliveryModes = targetCheckoutFacade.getDeliveryModes();
        assertThat(deliveryModes).isNotNull();
        assertThat(deliveryModes).isEmpty();
        verify(targetCheckoutFacade, never()).overlayFluentStockDataOnDeliveryModes(Mockito.anyList());
    }

    @Test
    public void testGetDeliveryModesReturnsDeliveryModeListWhenCartHasDeliveryModes() {
        final TargetZoneDeliveryModeData deliveryModeData = mock(TargetZoneDeliveryModeData.class);
        given(mockAbstractOrderHelper.getPhysicalDeliveryModes(cartModel)).willReturn(
                ImmutableList.of(deliveryModeData));

        final List<TargetZoneDeliveryModeData> deliveryModes = targetCheckoutFacade.getDeliveryModes();
        assertThat(deliveryModes).isNotEmpty();
        verify(targetCheckoutFacade, never()).overlayFluentStockDataOnDeliveryModes(Mockito.anyList());
    }

    @Test
    public void testGetDeliveryModesWithFluentStockLookup() {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isFluentEnabled();
        final TargetZoneDeliveryModeData deliveryModeData = mock(TargetZoneDeliveryModeData.class);
        given(mockAbstractOrderHelper.getPhysicalDeliveryModes(cartModel)).willReturn(
                ImmutableList.of(deliveryModeData));
        willDoNothing().given(targetCheckoutFacade).overlayFluentStockDataOnDeliveryModes(Mockito.anyList());

        final List<TargetZoneDeliveryModeData> deliveryModes = targetCheckoutFacade.getDeliveryModes();
        assertThat(deliveryModes).isNotEmpty();
        verify(targetCheckoutFacade).overlayFluentStockDataOnDeliveryModes(ImmutableList.of(deliveryModeData));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getDeliveryModeWithRangeInfoWhenInputNull() {
        targetCheckoutFacade.getDeliveryModeWithRangeInfoForCart(null);
    }

    @Test
    public void getDeliveryModeWithRangeInfoWhenNoCart() {
        final TargetZoneDeliveryModeData deliveryModeData = new TargetZoneDeliveryModeData();
        assertThat(targetCheckoutFacade.getDeliveryModeWithRangeInfoForCart(deliveryModeData).getFeeRange())
                .isNull();
    }

    @Test
    public void getDeliveryModeWithRangeInfoWhenDeliveryDataHasFee() {
        final TargetZoneDeliveryModeData deliveryModeData = new TargetZoneDeliveryModeData();
        deliveryModeData.setDeliveryCost(new PriceData());
        given(mockTargetPostCodeService.getPostalCodeFromCartOrSession(cartModel)).willReturn("3000");
        assertThat(targetCheckoutFacade.getDeliveryModeWithRangeInfoForCart(deliveryModeData).getFeeRange())
                .isFalse();
    }

    @Test
    public void getDeliveryModeWithRangeInfoWhenDeliveryDataHasNoFee() {
        final TargetZoneDeliveryModeData deliveryModeData = new TargetZoneDeliveryModeData();
        given(mockTargetPostCodeService.getPostalCodeFromCartOrSession(cartModel)).willReturn("3000");
        assertThat(targetCheckoutFacade.getDeliveryModeWithRangeInfoForCart(deliveryModeData).getFeeRange())
                .isFalse();
    }

    @Test
    public void getDeliveryModeWithRangeInfoWhenPostCodeSet() {
        final TargetZoneDeliveryModeData deliveryModeData = new TargetZoneDeliveryModeData();
        given(mockTargetPostCodeService.getPostalCodeFromCartOrSession(cartModel)).willReturn("3000");
        assertThat(targetCheckoutFacade.getDeliveryModeWithRangeInfoForCart(deliveryModeData).getFeeRange())
                .isFalse();
    }

    @Test
    public void getDeliveryModeWithRangeInfoWhenDeliveryCodeInDataIsNull() {
        final TargetZoneDeliveryModeData deliveryModeData = new TargetZoneDeliveryModeData();
        given(mockTargetPostCodeService.getPostalCodeFromCartOrSession(cartModel)).willReturn("");
        assertThat(targetCheckoutFacade.getDeliveryModeWithRangeInfoForCart(deliveryModeData).getFeeRange())
                .isNull();
    }

    @Test
    public void getDeliveryModeWithRangeInfoWhenDeliveryCodeInDataValuesEmpty() {
        final TargetZoneDeliveryModeData deliveryModeData = new TargetZoneDeliveryModeData();
        given(mockTargetPostCodeService.getPostalCodeFromCartOrSession(cartModel)).willReturn("");
        given(mockTargetDeliveryService.getDeliveryModeForCode("deliveryMode")).willReturn(null);
        deliveryModeData.setCode("deliveryMode");
        assertThat(targetCheckoutFacade.getDeliveryModeWithRangeInfoForCart(deliveryModeData).getFeeRange())
                .isNull();
    }

    @Test
    public void getDeliveryModeWithRangeInfoWhenDeliveryValuesEmpty() {
        final TargetZoneDeliveryModeData deliveryModeData = new TargetZoneDeliveryModeData();
        final TargetZoneDeliveryModeModel mockDeliveryMode = mock(TargetZoneDeliveryModeModel.class);
        final ZoneDeliveryModeModel zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        deliveryModeData.setCode("deliveryMode");
        given(mockTargetPostCodeService.getPostalCodeFromCartOrSession(cartModel)).willReturn("");
        given(mockTargetDeliveryService.getDeliveryModeForCode(deliveryModeData.getCode()))
                .willReturn(zoneDeliveryModeModel);
        given(mockTargetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(mockDeliveryMode, cartModel,
                true)).willReturn(null);
        assertThat(targetCheckoutFacade.getDeliveryModeWithRangeInfoForCart(deliveryModeData).getFeeRange())
                .isNull();
    }

    @Test
    public void getDeliveryModeWithRangeInfoWhenThereIsMoreThanOneDifferentDeliveryValue() {
        final TargetZoneDeliveryModeData deliveryModeData = new TargetZoneDeliveryModeData();
        final TargetZoneDeliveryModeModel mockDeliveryMode = mock(TargetZoneDeliveryModeModel.class);
        final CurrencyModel mockCurrency = mock(CurrencyModel.class);
        final List<AbstractTargetZoneDeliveryModeValueModel> deliveryModeValues = new ArrayList<>();
        final AbstractTargetZoneDeliveryModeValueModel deliveryValue1 = new AbstractTargetZoneDeliveryModeValueModel();
        final AbstractTargetZoneDeliveryModeValueModel deliveryValue2 = new AbstractTargetZoneDeliveryModeValueModel();
        deliveryValue1.setValue(Double.valueOf(0.1));
        deliveryValue1.setLowestPossibleValue(Double.valueOf(0.01));
        deliveryValue2.setValue(Double.valueOf(0.2));
        deliveryValue1.setCurrency(mockCurrency);
        deliveryValue2.setCurrency(mockCurrency);
        deliveryModeValues.add(deliveryValue1);
        deliveryModeValues.add(deliveryValue2);

        given(mockTargetPostCodeService.getPostalCodeFromCartOrSession(cartModel)).willReturn("");
        given(mockTargetDeliveryService.getDeliveryModeForCode("deliveryMode")).willReturn(mockDeliveryMode);
        given(
                mockTargetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(mockDeliveryMode,
                        cartModel, true)).willReturn(deliveryModeValues);
        given(mockPriceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(0.01), mockCurrency)).willReturn(
                new PriceData());
        deliveryModeData.setCode("deliveryMode");
        assertThat(targetCheckoutFacade.getDeliveryModeWithRangeInfoForCart(deliveryModeData).getFeeRange())
                .isTrue();
        Assertions
                .assertThat(
                        targetCheckoutFacade.getDeliveryModeWithRangeInfoForCart(deliveryModeData).getDeliveryCost())
                .isNotNull();
    }

    @Test
    public void testAddEntryIntoAdjustedCartForNotSizeVariantProductOutOfStock()
            throws CommerceCartModificationException {
        final String productName = "productName";
        final CommerceCartModification commerceCartModification = mock(CommerceCartModification.class);
        final TargetProductData targetProductData = mock(TargetProductData.class);
        willReturn(Long.valueOf(0)).given(commerceCartModification).getQuantity();
        given(targetProductData.getName()).willReturn(productName);
        given(commerceCartModification.getProduct()).willReturn(mock(ProductModel.class));
        given(productFacade.getProductForCodeAndOptions(any(String.class), any(ArrayList.class))).willReturn(
                targetProductData);
        given(commerceCartModification.getStatusCode()).willReturn(
                TargetCommerceCartModificationStatus.NO_STOCK);
        given(mockTargetLaybyCommerceCheckoutService.performSOHOnCart(cartModel)).willReturn(
                ImmutableList.of(commerceCartModification));
        final AdjustedCartEntriesData adjustedCartEntriesData = targetCheckoutFacade.adjustCart();

        assertThat(adjustedCartEntriesData.getOutOfStockItems().size()).isEqualTo(1);
        assertThat(adjustedCartEntriesData.getOutOfStockItems().get(0).getProduct().getName()).isEqualTo(productName);
        assertThat(adjustedCartEntriesData.getOutOfStockItems().get(0).getQuantity()).isEqualTo(0);
        assertThat(adjustedCartEntriesData.getOutOfStockItems().get(0).getProduct().getSize()).isNull();
        assertThat(adjustedCartEntriesData.getAdjustedItems()).isEmpty();
    }

    @Test
    public void testAddEntryIntoAdjustedCartForSizeVariantProductOutOfStock() throws CommerceCartModificationException {
        final String productName = "productName";
        final CommerceCartModification commerceCartModification = mock(CommerceCartModification.class);
        final TargetProductData targetProductData = mock(TargetProductData.class);
        willReturn(Long.valueOf(0)).given(commerceCartModification).getQuantity();
        given(targetProductData.getName()).willReturn(productName);

        final String variantName = "size";
        final String variantValue = "M";

        final VariantOptionQualifierData variantOptionQualifierData = new VariantOptionQualifierData();
        variantOptionQualifierData.setName(variantName);
        variantOptionQualifierData.setValue(variantValue);

        final Collection variantOptionQualifiers = new ArrayList();
        variantOptionQualifiers.add(variantOptionQualifierData);

        final VariantOptionData variantOptionData = new VariantOptionData();
        variantOptionData.setVariantOptionQualifiers(variantOptionQualifiers);

        final BaseOptionData baseOptionData = new BaseOptionData();
        baseOptionData.setSelected(variantOptionData);

        final List<BaseOptionData> baseOptions = new ArrayList<>();
        baseOptions.add(baseOptionData);

        given(targetProductData.getBaseOptions()).willReturn(baseOptions);
        given(commerceCartModification.getStatusCode()).willReturn(
                TargetCommerceCartModificationStatus.NO_STOCK);
        given(commerceCartModification.getProduct()).willReturn(mock(ProductModel.class));
        given(productFacade.getProductForCodeAndOptions(any(String.class), any(ArrayList.class))).willReturn(
                targetProductData);
        given(mockTargetLaybyCommerceCheckoutService.performSOHOnCart(cartModel)).willReturn(
                ImmutableList.of(commerceCartModification));
        final AdjustedCartEntriesData adjustedCartEntriesData = targetCheckoutFacade.adjustCart();

        assertThat(adjustedCartEntriesData.getOutOfStockItems().size()).isEqualTo(1);
        assertThat(adjustedCartEntriesData.getOutOfStockItems().get(0).getProduct().getName()).isEqualTo(productName);
        assertThat(adjustedCartEntriesData.getOutOfStockItems().get(0).getQuantity()).isEqualTo(0);
        assertThat(adjustedCartEntriesData.getOutOfStockItems().get(0).getProduct().getBaseOptions().get(0)
                .getSelected().getVariantOptionQualifiers().iterator().next().getName()).isEqualTo(variantName);
        assertThat(adjustedCartEntriesData.getOutOfStockItems().get(0).getProduct().getBaseOptions().get(0)
                .getSelected().getVariantOptionQualifiers().iterator().next().getValue()).isEqualTo(variantValue);
        assertThat(adjustedCartEntriesData.getAdjustedItems()).isEmpty();
    }

    @Test
    public void testAddEntryIntoAdjustedCartForNotSizeVariantProductAdjustedQuantity()
            throws CommerceCartModificationException {
        final Long outOfStockQty = Long.valueOf("0");
        final String productName = "productName";

        final CommerceCartModification commerceCartModification = mock(CommerceCartModification.class);
        willReturn(outOfStockQty).given(commerceCartModification).getQuantity();
        given(commerceCartModification.getStatusCode()).willReturn(
                TargetCommerceCartModificationStatus.NO_STOCK);
        final TargetProductData targetProductData = mock(TargetProductData.class);
        given(targetProductData.getName()).willReturn(productName);

        given(commerceCartModification.getProduct()).willReturn(mock(ProductModel.class));
        given(productFacade.getProductForCodeAndOptions(any(String.class), any(ArrayList.class))).willReturn(
                targetProductData);
        given(mockTargetLaybyCommerceCheckoutService.performSOHOnCart(cartModel)).willReturn(
                ImmutableList.of(commerceCartModification));
        final AdjustedCartEntriesData adjustedCartEntriesData = targetCheckoutFacade.adjustCart();

        assertThat(adjustedCartEntriesData.getOutOfStockItems().size()).isEqualTo(1);
        assertThat(adjustedCartEntriesData.getOutOfStockItems().get(0).getProduct().getName()).isEqualTo(productName);
        assertThat(adjustedCartEntriesData.getOutOfStockItems().get(0).getQuantity()).isEqualTo(0);
        assertThat(adjustedCartEntriesData.getAdjustedItems()).isEmpty();
    }

    @Test
    public void testAddEntryIntoAdjustedCartForSizeVariantProductAdjustedQuantity()
            throws CommerceCartModificationException {
        final Long outOfStockQty = Long.valueOf("1");
        final String productName = "productName";

        final CommerceCartModification commerceCartModification = mock(CommerceCartModification.class);
        willReturn(outOfStockQty).given(commerceCartModification).getQuantity();
        given(commerceCartModification.getStatusCode()).willReturn(
                TargetCommerceCartModificationStatus.NO_MODFICATION_OCCURED_INSUFFICIENT_QUANTITY);
        final TargetProductData targetProductData = mock(TargetProductData.class);
        given(targetProductData.getName()).willReturn(productName);
        final String variantName = "size";
        final String variantValue = "M";
        final VariantOptionQualifierData variantOptionQualifierData = new VariantOptionQualifierData();
        variantOptionQualifierData.setName(variantName);
        variantOptionQualifierData.setValue(variantValue);
        final Collection variantOptionQualifiers = new ArrayList();
        variantOptionQualifiers.add(variantOptionQualifierData);
        final VariantOptionData variantOptionData = new VariantOptionData();
        variantOptionData.setVariantOptionQualifiers(variantOptionQualifiers);
        final BaseOptionData baseOptionData = new BaseOptionData();
        baseOptionData.setSelected(variantOptionData);
        final List<BaseOptionData> baseOptions = new ArrayList<>();
        baseOptions.add(baseOptionData);

        given(targetProductData.getBaseOptions()).willReturn(baseOptions);
        given(commerceCartModification.getProduct()).willReturn(mock(ProductModel.class));
        given(productFacade.getProductForCodeAndOptions(any(String.class), any(ArrayList.class))).willReturn(
                targetProductData);
        given(mockTargetLaybyCommerceCheckoutService.performSOHOnCart(cartModel)).willReturn(
                ImmutableList.of(commerceCartModification));
        final AdjustedCartEntriesData adjustedCartEntriesData = targetCheckoutFacade.adjustCart();

        assertThat(adjustedCartEntriesData.getAdjustedItems().size()).isEqualTo(1);
        assertThat(adjustedCartEntriesData.getAdjustedItems().get(0).getProduct().getName()).isEqualTo(productName);
        assertThat(adjustedCartEntriesData.getAdjustedItems().get(0).getQuantity()).isEqualTo(1);
        assertThat(adjustedCartEntriesData.getAdjustedItems().get(0).getProduct().getBaseOptions().get(0).getSelected()
                .getVariantOptionQualifiers().iterator().next().getName()).isEqualTo(variantName);
        assertThat(adjustedCartEntriesData.getAdjustedItems().get(0).getProduct().getBaseOptions().get(0).getSelected()
                .getVariantOptionQualifiers().iterator().next().getValue()).isEqualTo(variantValue);
        assertThat(adjustedCartEntriesData.getOutOfStockItems()).isEmpty();
    }

    /**
     * HasIncompleteDeliveryDetailsGoodHomeDelivery
     */
    @Test
    public void testHasIncompleteDeliveryDetailsGoodDelivery() {
        given(cartModel.getDeliveryMode()).willReturn(homeDeliveryMode);
        final TargetAddressModel addressModel = mock(TargetAddressModel.class);
        given(cartModel.getDeliveryAddress()).willReturn(addressModel);
        given(cartModel.getCncStoreNumber()).willReturn(null);
        assertThat(targetCheckoutFacade.hasIncompleteDeliveryDetails()).isFalse();
    }

    /**
     * HasIncompleteDeliveryDetailsGoodHomeDelivery
     */
    @Test
    public void testHasIncompleteDeliveryDetailsGoodDigital() {
        given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryMode);
        willReturn(Boolean.TRUE).given(targetZoneDeliveryMode).getIsDigital();
        willReturn(Boolean.FALSE)
                .given(targetCheckoutFacade).hasDeliveryOptionConflicts(cartModel, targetZoneDeliveryMode);
        assertThat(targetCheckoutFacade.hasIncompleteDeliveryDetails()).isFalse();
    }

    /**
     * HasIncompleteDeliveryDetailsStoreDeliveryWithNoStoreNumber
     */
    @Test
    public void testHasIncompleteDeliveryDetailsStoreDeliveryWithNoStoreNumber() {
        given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryMode);
        willReturn(Boolean.TRUE).given(targetZoneDeliveryMode).getIsDeliveryToStore();
        willReturn(Boolean.FALSE)
                .given(targetCheckoutFacade).hasDeliveryOptionConflicts(cartModel, targetZoneDeliveryMode);
        final TargetAddressModel addressModel = mock(TargetAddressModel.class);
        given(cartModel.getDeliveryAddress()).willReturn(addressModel);
        given(cartModel.getCncStoreNumber()).willReturn(null);

        assertThat(targetCheckoutFacade.hasIncompleteDeliveryDetails()).isTrue();
    }

    /**
     * HasIncompleteDeliveryDetailsStoreDeliveryWithNoStoreNumber
     */
    @Test
    public void testHasIncompleteDeliveryDetailsStoreDeliveryWithStoreNumber() {
        given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryMode);
        willReturn(Boolean.TRUE).given(targetZoneDeliveryMode).getIsDeliveryToStore();
        willReturn(Boolean.FALSE)
                .given(targetCheckoutFacade).hasDeliveryOptionConflicts(cartModel, targetZoneDeliveryMode);
        final TargetAddressModel addressModel = mock(TargetAddressModel.class);
        given(cartModel.getDeliveryAddress()).willReturn(addressModel);
        given(cartModel.getCncStoreNumber()).willReturn(Integer.valueOf(100));

        assertThat(targetCheckoutFacade.hasIncompleteDeliveryDetails()).isFalse();
    }

    /**
     * testHasIncompleteDeliveryDetailsHomeDeliveryWithStoreNumber
     */
    @Test
    public void testHasIncompleteDeliveryDetailsGoodHomeDelivery() {
        given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryMode);
        willReturn(Boolean.FALSE).given(targetZoneDeliveryMode).getIsDeliveryToStore();
        willReturn(Boolean.FALSE)
                .given(targetCheckoutFacade).hasDeliveryOptionConflicts(cartModel, targetZoneDeliveryMode);
        final TargetAddressModel addressModel = mock(TargetAddressModel.class);
        given(cartModel.getDeliveryAddress()).willReturn(addressModel);
        given(cartModel.getCncStoreNumber()).willReturn(null);

        assertThat(targetCheckoutFacade.hasIncompleteDeliveryDetails()).isFalse();
    }

    /**
     * testHasIncompleteDeliveryDetailsHomeDeliveryWithStoreNumber
     */
    @Test
    public void testHasIncompleteDeliveryDetailsWithDeliveryModelConflicts() {
        given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryMode);
        willReturn(Boolean.TRUE)
                .given(targetCheckoutFacade).hasDeliveryOptionConflicts(cartModel, targetZoneDeliveryMode);
        assertThat(targetCheckoutFacade.hasIncompleteDeliveryDetails()).isTrue();
    }

    /**
     * testHasIncompleteDeliveryDetailsNoDeliveryMode
     */
    @Test
    public void testHasIncompleteDeliveryDetailsNoDeliveryMode() {
        given(cartModel.getDeliveryMode()).willReturn(null);
        assertThat(targetCheckoutFacade.hasIncompleteDeliveryDetails()).isTrue();
    }

    /**
     * testHasIncompleteDeliveryDetailsNoDeliveryMode
     */
    @Test
    public void testHasIncompleteDeliveryDetailsNoCart() {
        given(cartService.getSessionCart()).willReturn(null);
        assertThat(targetCheckoutFacade.hasIncompleteDeliveryDetails()).isTrue();
    }

    /**
     * testHasIncompleteDeliveryDetailsNoAddress
     */
    @Test
    public void testHasIncompleteDeliveryDetailsNoAddress() {
        given(cartService.getSessionCart()).willReturn(cartModel);
        given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryMode);
        willReturn(Boolean.FALSE)
                .given(targetCheckoutFacade).hasDeliveryOptionConflicts(cartModel, targetZoneDeliveryMode);
        given(cartModel.getDeliveryAddress()).willReturn(null);
        given(cartModel.getCncStoreNumber()).willReturn(Integer.valueOf(100));
        assertThat(targetCheckoutFacade.hasIncompleteDeliveryDetails()).isTrue();
    }

    @Test
    public void testHasIncompleteBillingAddressForCncCart() {
        given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryMode);
        willReturn(Boolean.TRUE)
                .given(targetZoneDeliveryMode).getIsDeliveryToStore();
        given(cartModel.getPaymentAddress()).willReturn(null);
        given(cartModel.getCncStoreNumber()).willReturn(Integer.valueOf(100));
        assertThat(targetCheckoutFacade.hasIncompleteBillingAddress()).isTrue();
    }

    @Test
    public void testHascompleteBillingAddressForCncCart() {
        given(cartService.getSessionCart()).willReturn(cartModel);
        given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryMode);
        willReturn(Boolean.TRUE)
                .given(targetZoneDeliveryMode).getIsDeliveryToStore();
        final AddressModel addressModel = mock(AddressModel.class);
        given(cartModel.getPaymentAddress()).willReturn(addressModel);
        given(cartModel.getCncStoreNumber()).willReturn(Integer.valueOf(100));
        assertThat(targetCheckoutFacade.hasIncompleteBillingAddress()).isFalse();
    }

    @Test
    public void testHasIncompleteBillingAddressForGiftCardOnlyOrder() {
        given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryMode);
        willReturn(Boolean.TRUE)
                .given(targetZoneDeliveryMode).getIsDigital();
        given(cartModel.getPaymentAddress()).willReturn(null);
        assertThat(targetCheckoutFacade.hasIncompleteBillingAddress()).isTrue();
    }

    @Test
    public void testHasIncompleteBillingAddressForNormalCart() {
        given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryMode);
        given(cartModel.getPaymentAddress()).willReturn(null);
        assertThat(targetCheckoutFacade.hasIncompleteBillingAddress()).isFalse();
    }

    @Test
    public void isPaymentModePaypalWhenCartNull() {
        given(cartService.getSessionCart()).willReturn(null);
        assertThat(targetCheckoutFacade.isPaymentModePaypal()).isFalse();
    }

    @Test
    public void isPaymentModePaypalWhenNoPaymentMode() {
        given(cartModel.getPaymentMode()).willReturn(null);
        assertThat(targetCheckoutFacade.isPaymentModePaypal()).isFalse();
    }

    @Test
    public void isPaymentModePaypalWhenPaymentModeNotPaypal() {
        final PaymentModeModel paymentMode = mock(PaymentModeModel.class);
        given(cartModel.getPaymentMode()).willReturn(paymentMode);
        given(paymentMode.getCode()).willReturn(TgtFacadesConstants.CREDIT_CARD);
        assertThat(targetCheckoutFacade.isPaymentModePaypal()).isFalse();
    }

    @Test
    public void isPaymentModePaypalSuccess() {
        final PaymentModeModel paymentMode = mock(PaymentModeModel.class);
        given(cartModel.getPaymentMode()).willReturn(paymentMode);
        given(paymentMode.getCode()).willReturn(TgtFacadesConstants.PAYPAL);
        assertThat(targetCheckoutFacade.isPaymentModePaypal()).isTrue();
    }

    @Test
    public void testCreateBillingAddress() {
        final TargetAddressModel addressModel = mock(TargetAddressModel.class);
        final PK pk = PK.fromLong(123);
        given(addressModel.getPk()).willReturn(pk);
        given(mockModelService.create(TargetAddressModel.class)).willReturn(addressModel);
        final TargetAddressData addressData = mock(TargetAddressData.class);
        targetCheckoutFacade.createBillingAddress(addressData);

        verify(targetAddressReversePopulator).populate(addressData, addressModel);
        verify(addressModel, times(1)).setOwner(cartModel);
        verify(mockModelService).save(addressModel);

        verify(cartModel, times(1)).setPaymentAddress(addressModel);
        verify(mockModelService).save(cartModel);
        verify(addressData, times(1)).setId(pk.toString());
    }

    @Test
    public void testIsGuestCheckoutAllowedNotAllowed() {
        willReturn(Boolean.FALSE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.CHECKOUT_ALLOW_GUEST);

        final boolean result = targetCheckoutFacade.isGuestCheckoutAllowed();

        assertThat(result).isFalse();
    }

    @Test
    public void testIsGuestCheckoutAllowed() {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.CHECKOUT_ALLOW_GUEST);

        final boolean result = targetCheckoutFacade.isGuestCheckoutAllowed();

        assertThat(result).isTrue();
    }

    @Test
    public void testRemoveFlybuys() {
        final boolean result = targetCheckoutFacade.removeFlybuysNumber();
        assertThat(result).isFalse();
        verify(mockSessionService).removeAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE);
        verify(mockTargetCommerceCheckoutService).removeFlybuysNumber(cartModel);
    }

    @Test
    public void testRemoveFlybuysWithNullCart() {
        willReturn(null).given(targetCheckoutFacade).getCart();
        final boolean result = targetCheckoutFacade.removeFlybuysNumber();
        assertThat(result).isFalse();
        verifyZeroInteractions(mockTargetCommerceCheckoutService);
    }

    @Test
    public void testSetBillingAddressWithNoCheckoutCart() {
        setUpGetCartToReturnCheckoutCart(null);
        assertThat(targetCheckoutFacade.setBillingAddress("123")).isFalse();
        verifyZeroInteractions(mockModelService);
        verifyZeroInteractions(mockTargetCustomerAccountService);
    }

    @Test
    public void testSetBillingAddressWithCheckoutCartWithNoUser() {
        setUpGetCartToReturnCheckoutCart(cartModel);
        given(cartModel.getUser()).willReturn(null);
        assertThat(targetCheckoutFacade.setBillingAddress("123")).isFalse();
        verifyZeroInteractions(mockModelService);
        verifyZeroInteractions(mockTargetCustomerAccountService);
    }

    @Test
    public void testSetInvalidBillingAddress() {
        setUpGetCartToReturnCheckoutCart(cartModel);
        given(cartModel.getUser()).willReturn(customerModel);
        given(mockTargetCustomerAccountService.getAddressForCode(customerModel, "123")).willReturn(null);
        targetCheckoutFacade.setBillingAddress("123");
        verify(cartModel, times(0)).setPaymentAddress(any(AddressModel.class));
        verifyZeroInteractions(mockModelService);
    }

    @Test
    public void testSetBillingAddress() {
        setUpGetCartToReturnCheckoutCart(cartModel);
        final AddressModel addressModel = mock(AddressModel.class);
        given(cartModel.getUser()).willReturn(customerModel);
        given(mockTargetCustomerAccountService.getAddressForCode(customerModel, "123")).willReturn(addressModel);
        targetCheckoutFacade.setBillingAddress("123");
        verify(cartModel).setPaymentAddress(addressModel);
        verify(mockModelService).save(cartModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateAfterpayPaymentInfoWithNullToken() {
        targetCheckoutFacade.createAfterpayPaymentInfo(null);
    }

    @Test
    public void testCreateAfterpayPaymentInfoWithNoCart() {
        final String token = "AFTERPAY_TOKEN";
        willReturn(null).given(targetCheckoutFacade).getCart();
        given(cartService.getSessionCart()).willReturn(null);
        targetCheckoutFacade.createAfterpayPaymentInfo(token);

        verifyZeroInteractions(mockTargetCustomerAccountService, mockTargetCommerceCheckoutService);
    }

    @Test
    public void testCreateAfterpayPaymentInfo() {
        final String token = "AFTERPAY_TOKEN";
        final CustomerModel mockCustomer = mock(CustomerModel.class);
        given(cartModel.getUser()).willReturn(mockCustomer);
        setUpGetCartToReturnCheckoutCart(cartModel);

        final AfterpayPaymentInfoModel mockAfterpayPaymentInfo = Mockito.mock(AfterpayPaymentInfoModel.class);
        given(mockTargetCustomerAccountService.createAfterpayPaymentInfo(mockCustomer, token))
                .willReturn(mockAfterpayPaymentInfo);

        final ArgumentCaptor<CommerceCheckoutParameter> captor = ArgumentCaptor
                .forClass(CommerceCheckoutParameter.class);
        targetCheckoutFacade.createAfterpayPaymentInfo(token);

        verify(mockTargetCustomerAccountService).createAfterpayPaymentInfo(mockCustomer, token);
        verify(mockTargetCommerceCheckoutService).setPaymentInfo(captor.capture());
        final CommerceCheckoutParameter capturedValue = captor.getValue();
        assertThat(capturedValue.getPaymentInfo()).isEqualTo(mockAfterpayPaymentInfo);
        assertThat(capturedValue.getCart()).isEqualTo(cartModel);

    }

    @Test
    public void testIsPaymentModeAfterpay() {
        final PaymentModeModel paymentMode = Mockito.mock(PaymentModeModel.class);
        given(cartModel.getPaymentMode()).willReturn(paymentMode);
        given(paymentMode.getCode()).willReturn(TgtFacadesConstants.AFTERPAY);
        final boolean paymentModeAfterpay = targetCheckoutFacade.isPaymentModeAfterpay();
        assertThat(paymentModeAfterpay).isTrue();
    }

    @Test
    public void testIsPaymentModeAfterpayWhenCartIsNull() {
        final PaymentModeModel paymentMode = Mockito.mock(PaymentModeModel.class);
        given(cartService.getSessionCart()).willReturn(null);
        given(cartModel.getPaymentMode()).willReturn(paymentMode);
        given(paymentMode.getCode()).willReturn(TgtFacadesConstants.AFTERPAY);
        final boolean paymentModeAfterpay = targetCheckoutFacade.isPaymentModeAfterpay();
        assertThat(paymentModeAfterpay).isFalse();
    }

    @Test
    public void testIsPaymentModeAfterpayWhenWhenPaymentModeIsCreditCard() {
        final PaymentModeModel paymentMode = Mockito.mock(PaymentModeModel.class);
        given(cartModel.getPaymentMode()).willReturn(paymentMode);
        given(paymentMode.getCode()).willReturn(TgtFacadesConstants.CREDIT_CARD);
        final boolean paymentModeAfterpay = targetCheckoutFacade.isPaymentModeAfterpay();
        assertThat(paymentModeAfterpay).isFalse();
    }

    @Test
    public void testOverlayFluentStockDataOnDeliveryModes() throws FluentClientException {
        ProductModel product = mock(ProductModel.class);
        willReturn("P1001").given(product).getCode();
        final AbstractOrderEntryModel orderEntry1 = mock(AbstractOrderEntryModel.class);
        willReturn(product).given(orderEntry1).getProduct();
        willReturn(Long.valueOf(2L)).given(orderEntry1).getQuantity();
        product = mock(ProductModel.class);
        willReturn("P1002").given(product).getCode();
        final AbstractOrderEntryModel orderEntry2 = mock(AbstractOrderEntryModel.class);
        willReturn(product).given(orderEntry2).getProduct();
        willReturn(Long.valueOf(10L)).given(orderEntry2).getQuantity();
        willReturn(Arrays.asList(orderEntry1, orderEntry2)).given(cartModel).getEntries();

        final StockDatum stockDatum1 = mock(StockDatum.class);
        willReturn("P1001").given(stockDatum1).getVariantCode();
        willReturn(Integer.valueOf(1)).given(stockDatum1).getAtsCcQty();
        willReturn(Integer.valueOf(2)).given(stockDatum1).getAtsHdQty();
        willReturn(Integer.valueOf(2)).given(stockDatum1).getAtsEdQty();
        final StockDatum stockDatum2 = mock(StockDatum.class);
        willReturn("P1002").given(stockDatum2).getVariantCode();
        willReturn(Integer.valueOf(10)).given(stockDatum2).getAtsCcQty();
        willReturn(Integer.valueOf(10)).given(stockDatum2).getAtsHdQty();
        willReturn(Integer.valueOf(9)).given(stockDatum2).getAtsEdQty();
        willReturn(Arrays.asList(stockDatum1, stockDatum2)).given(fluentStockLookupService).lookupStock(
                Collections.set("P1001", "P1002"),
                Arrays.asList(TgtCoreConstants.DELIVERY_TYPE.CC, TgtCoreConstants.DELIVERY_TYPE.HD,
                        TgtCoreConstants.DELIVERY_TYPE.ED, TgtCoreConstants.DELIVERY_TYPE.PO),
                null);
        final TargetZoneDeliveryModeData ccData = mock(TargetZoneDeliveryModeData.class);
        willReturn("click-and-collect").given(ccData).getCode();
        final TargetZoneDeliveryModeData hdData = mock(TargetZoneDeliveryModeData.class);
        willReturn("home-delivery").given(hdData).getCode();
        final TargetZoneDeliveryModeData edData = mock(TargetZoneDeliveryModeData.class);
        willReturn("express-delivery").given(edData).getCode();
        targetCheckoutFacade.overlayFluentStockDataOnDeliveryModes(Arrays.asList(ccData, hdData, edData));
        verify(ccData).setAvailable(false);
        verify(hdData, never()).setAvailable(false);
        verify(edData).setAvailable(false);
    }

    @Test(expected = FluentOrderException.class)
    public void testOverlayFluentStockDataOnDeliveryModesFail() throws FluentClientException {
        ProductModel product = mock(ProductModel.class);
        willReturn("P1001").given(product).getCode();
        final AbstractOrderEntryModel orderEntry1 = mock(AbstractOrderEntryModel.class);
        willReturn(product).given(orderEntry1).getProduct();
        product = mock(ProductModel.class);
        willReturn("P1002").given(product).getCode();
        final AbstractOrderEntryModel orderEntry2 = mock(AbstractOrderEntryModel.class);
        willReturn(product).given(orderEntry2).getProduct();
        willReturn(Arrays.asList(orderEntry1, orderEntry2)).given(cartModel).getEntries();

        willThrow(new FluentClientException(new au.com.target.tgtfluent.data.Error())).given(fluentStockLookupService)
                .lookupStock(
                        Collections.set("P1001", "P1002"),
                        Arrays.asList(TgtCoreConstants.DELIVERY_TYPE.CC, TgtCoreConstants.DELIVERY_TYPE.HD,
                                TgtCoreConstants.DELIVERY_TYPE.ED, TgtCoreConstants.DELIVERY_TYPE.PO),
                        null);
        final TargetZoneDeliveryModeData ccData = mock(TargetZoneDeliveryModeData.class);
        willReturn("click-and-collect").given(ccData).getCode();
        final TargetZoneDeliveryModeData hdData = mock(TargetZoneDeliveryModeData.class);
        willReturn("home-delivery").given(hdData).getCode();
        final TargetZoneDeliveryModeData edData = mock(TargetZoneDeliveryModeData.class);
        willReturn("express-delivery").given(edData).getCode();
        targetCheckoutFacade.overlayFluentStockDataOnDeliveryModes(Arrays.asList(ccData, hdData, edData));
    }

    @Test
    public void testIsCartHavingPreOrderItemForPreOrderProduct() {
        willReturn(Boolean.TRUE).given(cartFacade).hasSessionCart();
        given(cartService.getSessionCart()).willReturn(cartModel);
        willReturn(Boolean.TRUE).given(cartModel).getContainsPreOrderItems();
        assertThat(targetCheckoutFacade.isCartHavingPreOrderItem()).isTrue();
    }

    @Test
    public void testIsCartHavingPreOrderItemForNormalProduct() {
        willReturn(Boolean.FALSE).given(cartModel).getContainsPreOrderItems();
        assertThat(targetCheckoutFacade.isCartHavingPreOrderItem()).isFalse();
    }

    @Test
    public void testIsCartHavingPreOrderItemWhenNoSessionCart() {
        willReturn(Boolean.FALSE).given(cartFacade).hasSessionCart();
        assertThat(targetCheckoutFacade.isCartHavingPreOrderItem()).isFalse();
        verify(cartService, never()).getSessionCart();
        verify(cartModel, never()).getContainsPreOrderItems();
    }

    @Test
    public void testCheckFastlineStockOnExpressDeliveryModes() throws FluentClientException {
        final AbstractTargetVariantProductModel product1 = mock(AbstractTargetVariantProductModel.class);
        final TargetProductModel productbase1 = mock(TargetProductModel.class);
        final ProductTypeModel productType1 = mock(ProductTypeModel.class);
        willReturn("normal").given(productType1).getCode();
        willReturn(productType1).given(productbase1).getProductType();
        willReturn(productbase1).given(product1).getBaseProduct();
        willReturn("P1001").given(product1).getCode();
        final AbstractOrderEntryModel orderEntry1 = mock(AbstractOrderEntryModel.class);
        willReturn(product1).given(orderEntry1).getProduct();
        willReturn(Long.valueOf(2L)).given(orderEntry1).getQuantity();

        final AbstractTargetVariantProductModel product2 = mock(AbstractTargetVariantProductModel.class);
        final TargetProductModel productbase2 = mock(TargetProductModel.class);
        willReturn(productbase2).given(product2).getBaseProduct();
        final ProductTypeModel productType2 = mock(ProductTypeModel.class);
        willReturn("normal").given(productType2).getCode();
        willReturn(productType1).given(productbase2).getProductType();
        willReturn("P1002").given(product2).getCode();
        final AbstractOrderEntryModel orderEntry2 = mock(AbstractOrderEntryModel.class);
        willReturn(product2).given(orderEntry2).getProduct();
        willReturn(Long.valueOf(10L)).given(orderEntry2).getQuantity();
        willReturn(Arrays.asList(orderEntry1, orderEntry2)).given(cartModel).getEntries();

        final WarehouseModel warehouse = mock(WarehouseModel.class);
        willReturn(warehouse).given(targetWarehouseService).getDefaultOnlineWarehouse();
        willReturn(Integer.valueOf(5)).given(targetStockService).getStockLevelAmount(product1, warehouse);
        willReturn(Integer.valueOf(5)).given(targetStockService).getStockLevelAmount(product2, warehouse);

        final TargetZoneDeliveryModeData ccData = mock(TargetZoneDeliveryModeData.class);
        willReturn("click-and-collect").given(ccData).getCode();
        final TargetZoneDeliveryModeData hdData = mock(TargetZoneDeliveryModeData.class);
        willReturn("home-delivery").given(hdData).getCode();
        final TargetZoneDeliveryModeData edData = mock(TargetZoneDeliveryModeData.class);
        willReturn("express-delivery").given(edData).getCode();
        targetCheckoutFacade.checkFastlineStockOnExpressDeliveryModes(Arrays.asList(ccData, hdData, edData));
        verify(ccData, never()).setAvailable(Mockito.anyBoolean());
        verify(hdData, never()).setAvailable(Mockito.anyBoolean());
        verify(edData).setAvailable(false);
    }

    @Test
    public void testCheckFastlineStockOnExpressDeliveryModesWithDigitalProduct() throws FluentClientException {
        final AbstractTargetVariantProductModel product1 = mock(AbstractTargetVariantProductModel.class);
        final TargetProductModel productbase1 = mock(TargetProductModel.class);
        final ProductTypeModel productType1 = mock(ProductTypeModel.class);
        willReturn("normal").given(productType1).getCode();
        willReturn(productType1).given(productbase1).getProductType();
        willReturn(productbase1).given(product1).getBaseProduct();
        willReturn("P1001").given(product1).getCode();
        final AbstractOrderEntryModel orderEntry1 = mock(AbstractOrderEntryModel.class);
        willReturn(product1).given(orderEntry1).getProduct();
        willReturn(Long.valueOf(2L)).given(orderEntry1).getQuantity();

        final AbstractTargetVariantProductModel product2 = mock(AbstractTargetVariantProductModel.class);
        final TargetProductModel productbase2 = mock(TargetProductModel.class);
        willReturn(productbase2).given(product2).getBaseProduct();
        final ProductTypeModel productType2 = mock(ProductTypeModel.class);
        willReturn("digital").given(productType2).getCode();
        willReturn(productType2).given(productbase2).getProductType();
        willReturn("P1002").given(product2).getCode();
        final AbstractOrderEntryModel orderEntry2 = mock(AbstractOrderEntryModel.class);
        willReturn(product2).given(orderEntry2).getProduct();
        willReturn(Long.valueOf(10L)).given(orderEntry2).getQuantity();
        willReturn(Arrays.asList(orderEntry1, orderEntry2)).given(cartModel).getEntries();

        final WarehouseModel warehouse = mock(WarehouseModel.class);
        willReturn(warehouse).given(targetWarehouseService).getDefaultOnlineWarehouse();
        willReturn(Integer.valueOf(5)).given(targetStockService).getStockLevelAmount(product1, warehouse);
        willReturn(Integer.valueOf(5)).given(targetStockService).getStockLevelAmount(product2, warehouse);

        final TargetZoneDeliveryModeData ccData = mock(TargetZoneDeliveryModeData.class);
        willReturn("click-and-collect").given(ccData).getCode();
        final TargetZoneDeliveryModeData hdData = mock(TargetZoneDeliveryModeData.class);
        willReturn("home-delivery").given(hdData).getCode();
        final TargetZoneDeliveryModeData edData = mock(TargetZoneDeliveryModeData.class);
        willReturn("express-delivery").given(edData).getCode();
        targetCheckoutFacade.checkFastlineStockOnExpressDeliveryModes(Arrays.asList(ccData, hdData, edData));
        verify(ccData, never()).setAvailable(Mockito.anyBoolean());
        verify(hdData, never()).setAvailable(Mockito.anyBoolean());
        verify(edData, never()).setAvailable(Mockito.anyBoolean());
    }

    /*
     * Method will verify the availability of delivery modes if atsPoQty value is supplied for the pre-order product in the 
     * basket for checkout.
     */
    @Test
    public void testoverlayFluentStockDataOnDeliveryModesForPreOrderWithAtsPoQty() throws FluentClientException {
        final ProductModel product = mock(ProductModel.class);
        willReturn("V1111_preOrder_1").given(product).getCode();
        final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
        willReturn(product).given(orderEntry).getProduct();
        willReturn(Long.valueOf(2L)).given(orderEntry).getQuantity();
        willReturn(Arrays.asList(orderEntry)).given(cartModel).getEntries();
        final StockDatum stockDatum = mock(StockDatum.class);
        willReturn("V1111_preOrder_1").given(stockDatum).getVariantCode();
        willReturn(Integer.valueOf(0)).given(stockDatum).getAtsCcQty();
        willReturn(Integer.valueOf(0)).given(stockDatum).getAtsHdQty();
        willReturn(Integer.valueOf(0)).given(stockDatum).getAtsEdQty();
        willReturn(Integer.valueOf(2)).given(stockDatum).getAtsPoQty();
        willReturn(Arrays.asList(stockDatum)).given(fluentStockLookupService).lookupStock(
                Collections.set("V1111_preOrder_1"),
                Arrays.asList(TgtCoreConstants.DELIVERY_TYPE.CC, TgtCoreConstants.DELIVERY_TYPE.HD,
                        TgtCoreConstants.DELIVERY_TYPE.ED, TgtCoreConstants.DELIVERY_TYPE.PO),
                null);
        final TargetZoneDeliveryModeData ccData = mock(TargetZoneDeliveryModeData.class);
        willReturn("click-and-collect").given(ccData).getCode();
        final TargetZoneDeliveryModeData hdData = mock(TargetZoneDeliveryModeData.class);
        willReturn("home-delivery").given(hdData).getCode();
        final TargetZoneDeliveryModeData edData = mock(TargetZoneDeliveryModeData.class);
        willReturn("express-delivery").given(edData).getCode();
        targetCheckoutFacade.overlayFluentStockDataOnDeliveryModes(Arrays.asList(ccData, hdData, edData));
        verify(ccData).setAvailable(false);
        verify(hdData).setAvailable(false);
        verify(edData, never()).setAvailable(false);
    }

    /*
     * Method will verify the availability of delivery modes if atsEdQty value is supplied for the pre-order product in the 
     * basket for checkout.
     */
    @Test
    public void testoverlayFluentStockDataOnDeliveryModesForPreOrderWithAtsEdQty() throws FluentClientException {
        final ProductModel product = mock(ProductModel.class);
        willReturn("V1111_preOrder_1").given(product).getCode();
        final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
        willReturn(product).given(orderEntry).getProduct();
        willReturn(Long.valueOf(2L)).given(orderEntry).getQuantity();
        willReturn(Arrays.asList(orderEntry)).given(cartModel).getEntries();
        final StockDatum stockDatum = mock(StockDatum.class);
        willReturn("V1111_preOrder_1").given(stockDatum).getVariantCode();
        willReturn(Integer.valueOf(0)).given(stockDatum).getAtsCcQty();
        willReturn(Integer.valueOf(0)).given(stockDatum).getAtsHdQty();
        willReturn(Integer.valueOf(10)).given(stockDatum).getAtsEdQty();
        willReturn(Integer.valueOf(0)).given(stockDatum).getAtsPoQty();
        willReturn(Arrays.asList(stockDatum)).given(fluentStockLookupService).lookupStock(
                Collections.set("V1111_preOrder_1"),
                Arrays.asList(TgtCoreConstants.DELIVERY_TYPE.CC, TgtCoreConstants.DELIVERY_TYPE.HD,
                        TgtCoreConstants.DELIVERY_TYPE.ED, TgtCoreConstants.DELIVERY_TYPE.PO),
                null);
        final TargetZoneDeliveryModeData ccData = mock(TargetZoneDeliveryModeData.class);
        willReturn("click-and-collect").given(ccData).getCode();
        final TargetZoneDeliveryModeData hdData = mock(TargetZoneDeliveryModeData.class);
        willReturn("home-delivery").given(hdData).getCode();
        final TargetZoneDeliveryModeData edData = mock(TargetZoneDeliveryModeData.class);
        willReturn("express-delivery").given(edData).getCode();
        targetCheckoutFacade.overlayFluentStockDataOnDeliveryModes(Arrays.asList(ccData, hdData, edData));
        verify(ccData).setAvailable(false);
        verify(hdData).setAvailable(false);
        verify(edData, never()).setAvailable(false);
    }

    /*
     * Method will verify the availability of delivery modes if atsPoQty value is not supplied for the pre-order product in the 
     * basket for checkout.
     */
    @Test
    public void testoverlayFluentStockDataOnDeliveryModesForPreOrderWithoutAtsPoQty() throws FluentClientException {
        final ProductModel product = mock(ProductModel.class);
        willReturn("V1111_preOrder_1").given(product).getCode();
        final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
        willReturn(product).given(orderEntry).getProduct();
        willReturn(Long.valueOf(2L)).given(orderEntry).getQuantity();
        willReturn(Arrays.asList(orderEntry)).given(cartModel).getEntries();
        final StockDatum stockDatum = mock(StockDatum.class);
        willReturn("V1111_preOrder_1").given(stockDatum).getVariantCode();
        willReturn(Integer.valueOf(0)).given(stockDatum).getAtsCcQty();
        willReturn(Integer.valueOf(0)).given(stockDatum).getAtsHdQty();
        willReturn(Integer.valueOf(0)).given(stockDatum).getAtsEdQty();
        willReturn(Integer.valueOf(0)).given(stockDatum).getAtsPoQty();
        willReturn(Arrays.asList(stockDatum)).given(fluentStockLookupService).lookupStock(
                Collections.set("V1111_preOrder_1"),
                Arrays.asList(TgtCoreConstants.DELIVERY_TYPE.CC, TgtCoreConstants.DELIVERY_TYPE.HD,
                        TgtCoreConstants.DELIVERY_TYPE.ED, TgtCoreConstants.DELIVERY_TYPE.PO),
                null);
        final TargetZoneDeliveryModeData ccData = mock(TargetZoneDeliveryModeData.class);
        willReturn("click-and-collect").given(ccData).getCode();
        final TargetZoneDeliveryModeData hdData = mock(TargetZoneDeliveryModeData.class);
        willReturn("home-delivery").given(hdData).getCode();
        final TargetZoneDeliveryModeData edData = mock(TargetZoneDeliveryModeData.class);
        willReturn("express-delivery").given(edData).getCode();
        targetCheckoutFacade.overlayFluentStockDataOnDeliveryModes(Arrays.asList(ccData, hdData, edData));
        verify(ccData).setAvailable(false);
        verify(hdData).setAvailable(false);
        verify(edData).setAvailable(false);
    }

    /**
     * This method will verify if the cart contains mixed of normal as well as digital products. It will always evaluate
     * to True if it contains both.
     */
    @Test
    public void testIsGiftCardMixedBasketWithGiftCardWithNormalAndeGiftCardInCart() {
        setUpGiftCardMixedBasket();
        willReturn("normal").given(mockProductType1).getCode();
        willReturn("P1001").given(mockVariantProduct1).getCode();
        willReturn("digital").given(mockProductType2).getCode();
        willReturn("PGC1003_skydive").given(mockVariantProduct2).getCode();
        willReturn(Arrays.asList(mockOrderEntry1, mockOrderEntry2))
                .given(cartModel).getEntries();
        final boolean isGiftCardMixedBasket = targetCheckoutFacade
                .isGiftCardMixedBasket();
        assertThat(isGiftCardMixedBasket).isTrue();
    }

    /**
     * This method will verify if the cart contains mixed of normal as well as physical gift card products. It will
     * always evaluate to True if it contains both.
     */
    @Test
    public void testIsGiftCardMixedBasketWithNormalAndPhysicalGiftCardInCart() {
        setUpGiftCardMixedBasket();
        willReturn("normal").given(mockProductType1).getCode();
        willReturn("P1001").given(mockVariantProduct1).getCode();
        willReturn("giftCard").given(mockProductType2).getCode();
        willReturn("PGC1003_adventure_100").given(mockVariantProduct2).getCode();
        willReturn(Arrays.asList(mockOrderEntry1, mockOrderEntry2))
                .given(cartModel).getEntries();
        final boolean isGiftCardMixedBasket = targetCheckoutFacade
                .isGiftCardMixedBasket();
        assertThat(isGiftCardMixedBasket).isTrue();
    }

    /**
     * This method will verify if the cart contains mixed of normal products. It will always evaluate to False if it
     * contains normal products.
     */
    @Test
    public void testIsGiftCardMixedBasketWithGiftCardWithNormalItemsInCart() {
        setUpGiftCardMixedBasket();
        willReturn("normal").given(mockProductType1).getCode();
        willReturn("P1001").given(mockVariantProduct1).getCode();
        willReturn("normal").given(mockProductType2).getCode();
        willReturn("P1002").given(mockVariantProduct2).getCode();
        willReturn(Arrays.asList(mockOrderEntry1, mockOrderEntry2))
                .given(cartModel).getEntries();
        final boolean isGiftCardMixedBasket = targetCheckoutFacade
                .isGiftCardMixedBasket();
        assertThat(isGiftCardMixedBasket).isFalse();
    }

    /**
     * This method will verify if the cart contains digital products. It will always evaluate to False if it contains
     * normal products.
     */
    @Test
    public void testIsGiftCardMixedBasketWithGiftCardWithOnlyGiftCardItemsInCart() {
        setUpGiftCardMixedBasket();
        willReturn("digital").given(mockProductType1).getCode();
        willReturn(mockProduct1).given(mockVariantProduct1).getBaseProduct();
        willReturn("PGC1003_skydive").given(mockVariantProduct1).getCode();
        willReturn("digital").given(mockProductType2).getCode();
        willReturn("PGC1004_ribbon").given(mockVariantProduct2).getCode();
        willReturn(Arrays.asList(mockOrderEntry1, mockOrderEntry2))
                .given(cartModel).getEntries();
        final boolean isGiftCardMixedBasket = targetCheckoutFacade
                .isGiftCardMixedBasket();
        assertThat(isGiftCardMixedBasket).isFalse();
    }

    /**
     * This method will verify if the cart contains digital and physical gift card products. It will always evaluate to
     * False if it contains normal products.
     */
    @Test
    public void testIsGiftCardMixedBasketWithGiftCardWithDigitalAndPhysicalGiftCardItemsInCart() {
        setUpGiftCardMixedBasket();
        willReturn("digital").given(mockProductType1).getCode();
        willReturn("PGC1003_skydive").given(mockVariantProduct1).getCode();
        willReturn("giftCard").given(mockProductType2).getCode();
        willReturn("PGC1004_ribbon").given(mockVariantProduct2).getCode();
        willReturn(Arrays.asList(mockOrderEntry1, mockOrderEntry2))
                .given(cartModel).getEntries();
        final boolean isGiftCardMixedBasket = targetCheckoutFacade
                .isGiftCardMixedBasket();
        assertThat(isGiftCardMixedBasket).isFalse();
    }

    /**
     * This method will verify if the cart contains only one digital product. It will always evaluate to False if it
     * contain only one digital product.
     */
    @Test
    public void testIsGiftCardMixedBasketWithOnlyOneGiftCardItemsInCart() {
        willReturn("digital").given(mockProductType1).getCode();
        willReturn(mockProductType1).given(mockProduct1).getProductType();
        willReturn(mockProduct1).given(mockVariantProduct1).getBaseProduct();
        willReturn("PGC1003_skydive").given(mockVariantProduct1).getCode();
        willReturn(mockVariantProduct1).given(mockOrderEntry1).getProduct();
        willReturn(Arrays.asList(mockOrderEntry1)).given(cartModel).getEntries();
        willReturn(cartModel).given(targetCheckoutFacade).getCart();
        final boolean isGiftCardMixedBasket = targetCheckoutFacade
                .isGiftCardMixedBasket();
        assertThat(isGiftCardMixedBasket).isFalse();
    }

    /**
     * This method will verify if the cart contains only one normal product. It will always evaluate to False if it
     * contain only one normal product.
     */
    @Test
    public void testIsGiftCardMixedBasketWithOnlyOneNormalItemInCart() {
        willReturn("normal").given(mockProductType1).getCode();
        willReturn(mockProductType1).given(mockProduct1).getProductType();
        willReturn(mockProduct1).given(mockVariantProduct1).getBaseProduct();
        willReturn("P1001").given(mockVariantProduct1).getCode();
        willReturn(mockVariantProduct1).given(mockOrderEntry1).getProduct();
        willReturn(Arrays.asList(mockOrderEntry1)).given(cartModel).getEntries();
        willReturn(cartModel).given(targetCheckoutFacade).getCart();
        final boolean isGiftCardMixedBasket = targetCheckoutFacade
                .isGiftCardMixedBasket();
        assertThat(isGiftCardMixedBasket).isFalse();
    }

    @Test
    public void testPaymentModeEquals(){
        PaymentModeModel paymentMode = mock(PaymentModeModel.class);
        willReturn(cartModel).given(targetCheckoutFacade).getCart();
        given(cartModel.getPaymentMode()).willReturn(paymentMode);
        given(paymentMode.getCode()).willReturn(TgtFacadesConstants.ZIPPAY);
        assertThat(targetCheckoutFacade.paymentModeEquals(TgtFacadesConstants.ZIPPAY)).isTrue();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateZipPaymentInfoWithNullToken() {
        targetCheckoutFacade.createZipPaymentInfo(null);
    }

    @Test
    public void testCreateZipPaymentInfoNoCart() {
        final String token = "testToken";
        given(targetCheckoutFacade.getCart()).willReturn(null);
        targetCheckoutFacade.createZipPaymentInfo(token);
        verifyZeroInteractions(mockTargetCustomerAccountService, mockTargetCommerceCheckoutService);
    }

    @Test
    public void testCreateZipPaymentInfo() {
        final String token = "testToken";

        final CustomerModel mockCustomer = mock(CustomerModel.class);
        given(cartModel.getUser()).willReturn(mockCustomer);

        setUpGetCartToReturnCheckoutCart(cartModel);

        final ZippayPaymentInfoModel mockPaymentInfo = Mockito.mock(ZippayPaymentInfoModel.class);
        given(mockTargetCustomerAccountService.createZipPaymentInfo(mockCustomer, token))
                .willReturn(mockPaymentInfo);

        final ArgumentCaptor<CommerceCheckoutParameter> captor = ArgumentCaptor
                .forClass(CommerceCheckoutParameter.class);
        targetCheckoutFacade.createZipPaymentInfo(token);

        verify(mockTargetCustomerAccountService).createZipPaymentInfo(mockCustomer, token);
        verify(mockTargetCommerceCheckoutService).setPaymentInfo(captor.capture());
        final CommerceCheckoutParameter capturedValue = captor.getValue();
        assertThat(capturedValue.getPaymentInfo()).isEqualTo(mockPaymentInfo);
        assertThat(capturedValue.getCart()).isEqualTo(cartModel);

    }
}