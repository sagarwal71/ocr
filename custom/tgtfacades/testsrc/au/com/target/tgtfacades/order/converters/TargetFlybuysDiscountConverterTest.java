/**
 * 
 */
package au.com.target.tgtfacades.order.converters;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.voucher.VoucherService;
import de.hybris.platform.voucher.jalo.util.VoucherValue;
import de.hybris.platform.voucher.model.VoucherModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.order.data.TargetOrderData;


/**
 * @author knemalik
 * 
 */
@UnitTest
public class TargetFlybuysDiscountConverterTest {

    private final TargetCartData targetCartData = new TargetCartData();

    private final CartData cartData = new CartData();

    private final DiscountModel flybuysDiscountModel = Mockito.mock(FlybuysDiscountModel.class);

    @Mock
    private OrderModel mockOrder;

    @Mock
    private VoucherService voucherService;

    @Mock
    private VoucherModel voucherModel1;

    @Mock
    private VoucherModel voucherModel2;

    @Mock
    private DiscountModel discountModel;

    @Mock
    private PriceDataFactory priceDataFactory;

    @Mock
    private AbstractOrderModel orderModel;

    @Mock
    private CartModel cartModel;

    @Mock
    private CurrencyModel currencyModel;

    @Mock
    private TargetOrderData targetOrderData;

    @InjectMocks
    private final TargetFlybuysDiscountConverter targetFlybuysDiscountConverter = new TargetFlybuysDiscountConverter();

    @Mock
    private PriceData priceData;

    @Before
    public void initMocks() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testConvertFlybuysDiscountFlybuysDiscount() {

        final String dummyFlybuysNumber = "1783";
        final Double dummyValue = Double.valueOf(10f);
        final Integer pointsConsumed = Integer.valueOf(120);
        final String currencyIso = "AU";

        when(voucherService.getAppliedVouchers(orderModel)).thenReturn(Collections.singletonList(flybuysDiscountModel));

        when(((FlybuysDiscountModel)flybuysDiscountModel).getFlybuysCardNumber())
                .thenReturn(dummyFlybuysNumber);

        when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(dummyValue.doubleValue()), currencyIso))
                .thenReturn(priceData);

        when(((FlybuysDiscountModel)flybuysDiscountModel).getValue()).thenReturn(dummyValue);

        when(orderModel.getFlybuysPointsConsumed()).thenReturn(pointsConsumed);

        when(flybuysDiscountModel.getCurrency()).thenReturn(currencyModel);

        when(currencyModel.getIsocode()).thenReturn(currencyIso);

        targetFlybuysDiscountConverter.convertFlybuysDiscount(orderModel, targetCartData);


        assertThat(targetCartData.getFlybuysDiscountData()).isNotNull();

        assertThat(targetCartData.getFlybuysDiscountData().getFlybuysCardNumber()).isEqualTo(dummyFlybuysNumber);

        assertThat(targetCartData.getFlybuysDiscountData().getPointsConsumed()).isEqualTo(pointsConsumed);

        assertThat(targetCartData.getFlybuysDiscountData().getValue()).isEqualTo(priceData);
    }

    @Test
    public void testConvertFlybuysDiscountNullDiscount() {

        final String dummyFlybuysNumber = "2365";
        final Double dummyValue = Double.valueOf(240f);
        final String currencyIso = "AU";
        final VoucherValue mockVoucherValue = Mockito.mock(VoucherValue.class);

        when(voucherService.getAppliedVouchers(orderModel)).thenReturn(Collections.EMPTY_LIST);

        when(((FlybuysDiscountModel)flybuysDiscountModel).getFlybuysCardNumber())
                .thenReturn(dummyFlybuysNumber);

        when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(mockVoucherValue.getValue()), currencyIso))
                .thenReturn(priceData);

        when(((FlybuysDiscountModel)flybuysDiscountModel).getValue()).thenReturn(dummyValue);

        when(flybuysDiscountModel.getCurrency()).thenReturn(currencyModel);

        when(currencyModel.getIsocode()).thenReturn(currencyIso);

        targetFlybuysDiscountConverter.convertFlybuysDiscount(orderModel, targetCartData);

        assertThat(targetCartData.getFlybuysDiscountData()).isNull();
    }

    @Test
    public void testConvertFlybuysDiscountNonFlybuysDiscount() {

        final Collection<DiscountModel> vouchersList = new ArrayList<>();
        vouchersList.add(voucherModel1);
        vouchersList.add(voucherModel2);

        final String dummyFlybuysNumber = "98876";
        final Double dummyValue = Double.valueOf(11f);
        final String currencyIso = "AU";
        final VoucherValue mockVoucherValue = Mockito.mock(VoucherValue.class);

        when(voucherService.getAppliedVouchers(orderModel)).thenReturn(vouchersList);

        when(((FlybuysDiscountModel)flybuysDiscountModel).getFlybuysCardNumber())
                .thenReturn(dummyFlybuysNumber);

        when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(mockVoucherValue.getValue()), currencyIso))
                .thenReturn(priceData);

        when(((FlybuysDiscountModel)flybuysDiscountModel).getValue()).thenReturn(dummyValue);

        when(flybuysDiscountModel.getCurrency()).thenReturn(currencyModel);

        when(currencyModel.getIsocode()).thenReturn(currencyIso);

        targetFlybuysDiscountConverter.convertFlybuysDiscount(orderModel, targetCartData);

        assertThat(targetCartData.getFlybuysDiscountData()).isNull();
    }

    @Test
    public void testConvertFlybuysDiscountNonTargetOrderData() {

        final String dummyFlybuysNumber = "98654";
        final Double dummyValue = Double.valueOf(1789f);
        final String currencyIso = "AU";
        final VoucherValue mockVoucherValue = Mockito.mock(VoucherValue.class);
        when(voucherService.getAppliedVouchers(orderModel)).thenReturn(Collections.singletonList(flybuysDiscountModel));

        when(((FlybuysDiscountModel)flybuysDiscountModel).getFlybuysCardNumber())
                .thenReturn(dummyFlybuysNumber);

        when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(mockVoucherValue.getValue()), currencyIso))
                .thenReturn(priceData);

        when(((FlybuysDiscountModel)flybuysDiscountModel).getValue()).thenReturn(dummyValue);

        when(flybuysDiscountModel.getCurrency()).thenReturn(currencyModel);

        when(currencyModel.getIsocode()).thenReturn(currencyIso);

        targetFlybuysDiscountConverter.convertFlybuysDiscount(orderModel, cartData);

        assertThat(targetCartData.getFlybuysDiscountData()).isNull();
    }
}
