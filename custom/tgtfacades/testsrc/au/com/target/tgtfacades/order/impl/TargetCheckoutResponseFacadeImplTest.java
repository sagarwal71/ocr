/**
 * 
 */
package au.com.target.tgtfacades.order.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.converters.ConfigurablePopulator;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.DeliveryModeService;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.fest.assertions.Fail;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtauspost.deliveryclub.service.ShipsterClientService;
import au.com.target.tgtauspost.exception.AuspostShipsterClientException;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;
import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.delivery.TargetDeliveryFacade;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade;
import au.com.target.tgtfacades.flybuys.data.FlybuysRequestStatusData;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.TargetOrderFacade;
import au.com.target.tgtfacades.order.TargetPlaceOrderFacade;
import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;
import au.com.target.tgtfacades.order.data.ClickAndCollectDeliveryDetailData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.payment.AfterpayConfigFacade;
import au.com.target.tgtfacades.payment.data.AfterpayConfigData;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.BillingCountryResponseData;
import au.com.target.tgtfacades.response.data.CartDetailOption;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;
import au.com.target.tgtfacades.response.data.CheckoutOptionsResponseData;
import au.com.target.tgtfacades.response.data.CreateAddressResponseData;
import au.com.target.tgtfacades.response.data.OrderDetailGaData;
import au.com.target.tgtfacades.response.data.OrderDetailResponseData;
import au.com.target.tgtfacades.response.data.PaymentMethodData;
import au.com.target.tgtfacades.response.data.PaymentResponseData;
import au.com.target.tgtfacades.response.data.RegisterUserResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.response.data.ShipsterEmailVerificationResponseData;
import au.com.target.tgtfacades.response.data.StoreSearchResultResponseData;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtfacades.user.data.TargetRegisterData;
import au.com.target.tgtfacades.voucher.TargetVoucherFacade;
import au.com.target.tgtfacades.voucher.data.TargetFlybuysLoginData;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtpayment.commands.result.TargetCardPaymentResult;
import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.constants.TgtpaymentConstants;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.service.TargetPaymentService;
import au.com.target.tgtverifyaddr.TargetAddressVerificationService;
import au.com.target.tgtverifyaddr.data.CountryEnum;
import au.com.target.tgtverifyaddr.exception.NoMatchFoundException;
import au.com.target.tgtverifyaddr.exception.RequestTimeOutException;
import au.com.target.tgtverifyaddr.exception.ServiceNotAvailableException;
import au.com.target.tgtverifyaddr.exception.TooManyMatchesFoundException;


/**
 * Test cases for TargetCheckoutResponseFacadeImpl
 * 
 * @author htan3
 *
 */
@SuppressWarnings("deprecation")
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCheckoutResponseFacadeImplTest {

    private static final Integer CNC_STORE_NUMBER = new Integer(7001);

    private static final String REASON = "Items in your basket prevent this payment option.";

    @Mock
    private TargetCheckoutFacade targetCheckoutFacade;

    @Mock
    private TargetCartData cartData;

    @Mock
    private AddressData addressData;

    @Mock
    private CartModel cart;

    @Mock
    private DeliveryModeService deliveryModeService;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Mock
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Mock
    private PageableData pageableData;

    @Mock
    private FlexibleSearchService flexibleSearchService;

    @Mock
    private ConfigurablePopulator<TargetCartData, CartDetailResponseData, CartDetailOption> cartDetailPopulator;

    @Mock
    private TargetVoucherFacade targetVoucherFacade;

    @Mock
    private TargetUserFacade targetUserFacade;

    @Mock
    private TargetAddressVerificationService targetAddressVerificationService;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Mock
    private TargetPlaceOrderFacade targetPlaceOrderFacade;

    @Mock
    private Converter<AddressModel, AddressData> addressConverter;

    @Mock
    private TargetDeliveryFacade targetDeliveryFacade;

    @Mock
    private DeliveryModeModel deliveryModeModel;

    @Mock
    private FlybuysDiscountFacade flybuysDiscountFacade;

    @Mock
    private TargetOrderFacade targetOrderFacade;

    @Mock
    private Populator<TargetOrderData, OrderDetailGaData> orderDetailGaPopulator;

    @Mock
    private TargetCustomerFacade targetCustomerFacade;

    @Mock
    private TargetCommerceCartService targetCommerceCartService;

    @Mock
    private ShipsterClientService shipsterClientService;

    @Spy
    @InjectMocks
    private final TargetCheckoutResponseFacadeImpl facade = new TargetCheckoutResponseFacadeImpl();

    @Mock
    private AfterpayConfigFacade afterpayConfigFacade;

    @Mock
    private CartService cartService;

    @Mock
    private ModelService modelService;

    @Mock
    private CalculationService calculationService;

    @Mock
    private TargetPaymentService targetPaymentService;

    @Mock
    private TargetCreateSubscriptionResult targetCreateSubscriptionResult;


    private interface ClickAndCollectDeliveryMode {
        String DESCRIPTION = "Click and Collect";
        String NAME = "Click + Collect";
        String SHORT_DESCRIPTION = "Free for orders over $40. Not available to all stores.";
        String LONG_DESCRIPTION = "Free for orders over $40. Orders under $40 will incur a $5 fee.";
        boolean IS_DELIVERY_TO_STORE = true;
        String DISCLAIMER = "Not available on all products or to all stores.";
        String CODE = "click-and-collect";

        interface DeliveryCost {
            BigDecimal VALUE = BigDecimal.valueOf(5);
            String FORMATTED_VALUE = "$5.00";
        }
    }

    interface HomeDeliveryMode {
        String DESCRIPTION = "Standard Australia Post Delivery";
        String NAME = "Home Delivery";
        String SHORT_DESCRIPTION = "Free for orders over $75 (excluding large items).";
        String LONG_DESCRIPTION = "Free for orders over $75. Orders under $75 will incur a $9 delivery fee.";
        boolean IS_DELIVERY_TO_STORE = false;
        String DISCLAIMER = StringUtils.EMPTY;
        String CODE = "home-delivery";

        interface DeliveryCost {
            BigDecimal VALUE = BigDecimal.valueOf(9);
            String FORMATTED_VALUE = "$9.00";
        }
    }

    interface ExpressDeliveryMode {
        String DESCRIPTION = "Express Delivery";
        String NAME = "Express Delivery";
        String SHORT_DESCRIPTION = "For apparel only orders.  Not available to all locations.";
        String LONG_DESCRIPTION = "Express Delivery - $12.95. Orders placed by 12pm will be delivered on the next business day.";
        boolean IS_DELIVERY_TO_STORE = false;
        String DISCLAIMER = StringUtils.EMPTY;
        String CODE = "express-delivery";

        interface DeliveryCost {
            BigDecimal VALUE = BigDecimal.valueOf(12.95);
            String FORMATTED_VALUE = "$12.95";
        }
    }

    @Before
    public void setUp() {
        given(addressData.getPostalCode()).willReturn("2145");
        given(targetCheckoutFacade.getCart()).willReturn(cart);
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(cartData);
        given(cart.getDeliveryMode()).willReturn(deliveryModeModel);
        given(deliveryModeModel.getCode()).willReturn("home-delivery");
        final AfterpayConfigData afterpayConfigData = new AfterpayConfigData();
        final PriceData minimumAmount = new PriceData();
        minimumAmount.setFormattedValue("$50.00");
        minimumAmount.setValue(new BigDecimal("50"));
        final PriceData maximumAmount = new PriceData();
        maximumAmount.setFormattedValue("$1,000.00");
        maximumAmount.setValue(new BigDecimal("1000"));
        afterpayConfigData.setMaximumAmount(maximumAmount);
        afterpayConfigData.setMinimumAmount(minimumAmount);
        given(afterpayConfigFacade.getAfterpayConfig()).willReturn(afterpayConfigData);

    }

    @Test
    public void getDeliveryModesWithoutCart() {
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(null);
        final Response response = facade.getApplicableDeliveryModes();
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        final CheckoutOptionsResponseData data = (CheckoutOptionsResponseData)response.getData();
        assertThat(data.getDeliveryModes()).isNotNull();
        assertThat(CollectionUtils.isEmpty(data.getDeliveryModes())).isTrue();
    }

    @Test
    public void getDeliveryModesForCartWithDeliveryModes() {
        final TargetZoneDeliveryModeData deliveryModeData = mock(TargetZoneDeliveryModeData.class);
        given(targetCheckoutFacade.getDeliveryModes()).willReturn(ImmutableList.of(deliveryModeData));
        final Response response = facade.getApplicableDeliveryModes();
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        final CheckoutOptionsResponseData data = (CheckoutOptionsResponseData)response.getData();
        assertThat(data.getDeliveryModes()).containsExactly(deliveryModeData);
        verify(targetCheckoutFacade).getDeliveryModeWithRangeInfoForCart(deliveryModeData);
    }

    @Test
    public void getDeliveryModesWhenNoDeliveryModesConfigured() {
        given(targetCheckoutFacade.getDeliveryModes()).willReturn(null);
        final Response response = facade.getApplicableDeliveryModes();
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        final CheckoutOptionsResponseData data = (CheckoutOptionsResponseData)response.getData();
        assertThat(data.getDeliveryModes()).isNotNull();
        assertThat(CollectionUtils.isEmpty(data.getDeliveryModes())).isTrue();
    }

    @Test
    public void getDeliveryModesWhenFluentException() {
        willThrow(new FluentOrderException()).given(targetCheckoutFacade).getDeliveryModes();
        final Response response = facade.getApplicableDeliveryModes();
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_DELIVERY_MODES_FLUENT_EXCEPTION");
    }

    @Test
    public void setClickAndCollectAsDeliveryMode() {
        final TargetZoneDeliveryModeData deliveryModeData = getClickAndCollectDeliveryMode();
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).isSupportedDeliveryMode(deliveryModeData.getCode());
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).setDeliveryMode(deliveryModeData.getCode());
        given(cartData.getDeliveryMode()).willReturn(deliveryModeData);

        final List<TargetZoneDeliveryModeData> applicableDeliveryModes = getApplicableDeliveryModes();
        given(cartData.getDeliveryModes()).willReturn(applicableDeliveryModes);
        given(cartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        final Response response = facade.setDeliveryMode(deliveryModeData.getCode());
        final CartDetailResponseData responseData = (CartDetailResponseData)response.getData();
        assertThat(response.isSuccess()).isTrue();
        verify(cartDetailPopulator).populate(cartData, responseData, ImmutableList.of(CartDetailOption.DELIVERY_MODE));
    }

    @Test
    public void setHomeDeliveryAsDeliveryMode() {
        final TargetZoneDeliveryModeData deliveryModeData = getHomeDeliveryMode();
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).isSupportedDeliveryMode(deliveryModeData.getCode());
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).setDeliveryMode(deliveryModeData.getCode());
        given(cartData.getDeliveryMode()).willReturn(deliveryModeData);
        given(cartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        final List<TargetZoneDeliveryModeData> applicableDeliveryModes = getApplicableDeliveryModes();
        given(cartData.getDeliveryModes()).willReturn(applicableDeliveryModes);

        final Response response = facade.setDeliveryMode(deliveryModeData.getCode());
        final CartDetailResponseData responseData = (CartDetailResponseData)response.getData();
        assertThat(response.isSuccess()).isTrue();
        verify(cartDetailPopulator).populate(cartData, responseData, ImmutableList.of(CartDetailOption.DELIVERY_MODE));
    }

    @Test
    public void setMultipleConsiments() {
        final TargetZoneDeliveryModeData deliveryModeData = getExpressDeliveryMode();
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).isSupportedDeliveryMode(deliveryModeData.getCode());
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).setDeliveryMode(deliveryModeData.getCode());
        given(cartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        given(cartData.getDeliveryMode()).willReturn(deliveryModeData);

        final List<TargetZoneDeliveryModeData> applicableDeliveryModes = getApplicableDeliveryModes();
        given(cartData.getDeliveryModes()).willReturn(applicableDeliveryModes);

        final Response response = facade.setDeliveryMode(deliveryModeData.getCode());
        final CartDetailResponseData responseData = (CartDetailResponseData)response.getData();
        assertThat(response.isSuccess()).isTrue();
        verify(cartDetailPopulator).populate(cartData, responseData, ImmutableList.of(CartDetailOption.DELIVERY_MODE));
    }

    @Test
    public void setExpressDeliveryAsDeliveryMode() {
        final TargetZoneDeliveryModeData deliveryModeData = getExpressDeliveryMode();
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).isSupportedDeliveryMode(deliveryModeData.getCode());
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).setDeliveryMode(deliveryModeData.getCode());
        given(cartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        given(cartData.getDeliveryMode()).willReturn(deliveryModeData);

        final List<TargetZoneDeliveryModeData> applicableDeliveryModes = getApplicableDeliveryModes();
        given(cartData.getDeliveryModes()).willReturn(applicableDeliveryModes);

        final Response response = facade.setDeliveryMode(deliveryModeData.getCode());
        final CartDetailResponseData responseData = (CartDetailResponseData)response.getData();
        assertThat(response.isSuccess()).isTrue();
        verify(cartDetailPopulator).populate(cartData, responseData, ImmutableList.of(CartDetailOption.DELIVERY_MODE));
    }

    @Test
    public void setDeliveryModesForNotApplicableDeliveryMode() {
        willReturn(Boolean.FALSE).given(targetCheckoutFacade).setDeliveryMode("invalidDelMode");
        final Response response = facade.setDeliveryMode("invalidDelMode");
        final BaseResponseData baseResponseData = response.getData();
        final au.com.target.tgtfacades.response.data.Error error = baseResponseData.getError();
        assertThat(response.isSuccess()).isFalse();
        assertThat(error.getCode()).isEqualTo("ERR_DELIVERY_MODE_UNAVAILABLE");
    }

    @Test
    public void setDeliveryModesForValidDeliveryModeAndAnInvalidSavedShippingAddress() {
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).setDeliveryMode("validDeliveryMode");
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).isSupportedDeliveryMode("validDeliveryMode");

        final CartModel cartModel = mock(CartModel.class);
        given(targetCheckoutFacade.getCart()).willReturn(cartModel);
        final AddressModel addressModel = mock(AddressModel.class);
        given(cartModel.getDeliveryAddress()).willReturn(addressModel);
        given(addressModel.getPostalcode()).willReturn("4916");

        final DeliveryModeModel deliveryMode = mock(DeliveryModeModel.class);
        given(cartModel.getDeliveryMode()).willReturn(deliveryMode);
        given(deliveryMode.getCode()).willReturn("validDeliveryMode");

        willReturn(Boolean.FALSE).given(targetDeliveryFacade)
                .isDeliveryModePostCodeCombinationValid("validDeliveryMode", "4916");

        final TargetCartData mockCartData = mock(TargetCartData.class);
        given(mockCartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(mockCartData);

        final Response response = facade.setDeliveryMode("validDeliveryMode");
        assertThat(response.isSuccess()).isTrue();
        verify(targetCheckoutFacade).removeDeliveryAddress();
    }

    @Test
    public void setDeliveryModesForEmptyDeliveryMode() {
        final Response response = facade.setDeliveryMode("");
        final BaseResponseData baseResponseData = response.getData();
        final au.com.target.tgtfacades.response.data.Error error = baseResponseData.getError();
        assertThat(response.isSuccess()).isFalse();
        assertThat(error.getCode()).isEqualTo("ERR_DELIVERY_MODE_UNAVAILABLE");
    }

    @Test
    public void testSearchStores() {
        final StoreFinderSearchPageData<PointOfServiceData> result = mock(StoreFinderSearchPageData.class);
        final List<PointOfServiceData> stores = mock(List.class);
        given(targetStoreLocatorFacade.searchStoresWithCncAvailability("test", cart,
                pageableData)).willReturn(result);
        given(result.getResults()).willReturn(stores);

        final Response response = facade.searchCncStores("test", pageableData);
        final StoreSearchResultResponseData data = (StoreSearchResultResponseData)response.getData();

        verify(targetStoreLocatorFacade).searchStoresWithCncAvailability("test", cart,
                pageableData);

        assertThat(response.isSuccess()).isEqualTo(true);
        assertThat(data.getStores()).isEqualTo(stores);
    }

    @Test
    public void testSearchStoresWithNoResult() {
        given(targetStoreLocatorFacade.searchStoresWithCncAvailability(anyString(), any(CartModel.class),
                any(PageableData.class))).willReturn(null);
        final Response response = facade.searchCncStores("test", pageableData);
        final StoreSearchResultResponseData data = (StoreSearchResultResponseData)response.getData();

        verify(targetStoreLocatorFacade).searchStoresWithCncAvailability("test", cart,
                pageableData);

        assertThat(response.isSuccess()).isEqualTo(true);
        assertThat(data.getStores()).isNull();
    }

    @Test
    public void testSetCncStore() {
        final Integer storeNumber = Integer.valueOf(1);
        final TargetPointOfServiceData posData = mock(TargetPointOfServiceData.class);
        given(targetCheckoutFacade.setClickAndCollectStore(storeNumber.intValue())).willReturn(posData);
        given(posData.getAvailableForPickup()).willReturn(Boolean.TRUE);
        final Response response = facade.setCncStore(storeNumber);
        final StoreSearchResultResponseData data = (StoreSearchResultResponseData)response.getData();
        assertThat(response.isSuccess()).isTrue();
        assertThat(data.getStores()).isNotNull();
    }

    @Test
    public void testSetCncStoreWhenStoreIsNotAvailableForPickup() {
        final Integer storeNumber = Integer.valueOf(1);
        final TargetPointOfServiceData posData = mock(TargetPointOfServiceData.class);
        given(targetCheckoutFacade.setClickAndCollectStore(storeNumber.intValue())).willReturn(posData);
        given(posData.getAvailableForPickup()).willReturn(Boolean.FALSE);
        final Response response = facade.setCncStore(storeNumber);
        final BaseResponseData data = response.getData();
        assertThat(response.isSuccess()).isFalse();
        assertThat(data.getError()).isNotNull();
        assertThat(data.getError().getCode()).isEqualTo("ERR_STORE_UNAVAILABLE");
    }

    @Test
    public void testSetCncStoreWhenStoreIsNotFound() {
        final Integer storeNumber = Integer.valueOf(1);
        given(targetCheckoutFacade.setClickAndCollectStore(storeNumber.intValue())).willReturn(null);
        final Response response = facade.setCncStore(storeNumber);
        final BaseResponseData data = response.getData();
        assertThat(response.isSuccess()).isFalse();
        assertThat(data.getError()).isNotNull();
        assertThat(data.getError().getCode()).isEqualTo("ERR_STORE_UNAVAILABLE");
    }

    @Test
    public void verifyGetCartSummary() {
        final TargetCartData mockCartData = mock(TargetCartData.class);
        given(mockCartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(mockCartData);
        final Response response = facade.getCartSummary();
        assertThat(response).isNotNull();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData()).isInstanceOfAny(CartDetailResponseData.class);
        final CartDetailResponseData cartDetailResponseData = (CartDetailResponseData)response.getData();
        verify(cartDetailPopulator).populate(mockCartData, cartDetailResponseData,
                ImmutableList.of(CartDetailOption.CART_SUMMARY));
    }

    @Test
    public void testGetCartDetailWhenCartIsNull() {
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(null);

        final Response response = facade.getCartDetail();
        assertThat(response).isNotNull();
        assertThat(response.getData()).isNull();
    }

    @Test
    public void testGetCartDetailsWithNoStoreSelected() {
        final TargetCartData mockCartData = mock(TargetCartData.class);
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(mockCartData);
        given(mockCartData.getCncStoreNumber()).willReturn(null);
        given(mockCartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        final Response response = facade.getCartDetail();
        assertThat(response).isNotNull();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData()).isInstanceOfAny(CartDetailResponseData.class);
        assertThat(((CartDetailResponseData)response.getData()).getStore()).isNull();
    }

    @Test
    public void testGetCartDetailsWithSelectedStoreNotAvailable() {
        final TargetCartData mockCartData = mock(TargetCartData.class);
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(mockCartData);
        given(mockCartData.getCncStoreNumber()).willReturn(new Integer(7000));
        given(targetStoreLocatorFacade.getPointOfService(new Integer(7000))).willReturn(null);
        given(mockCartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));

        final Response response = facade.getCartDetail();
        assertThat(response).isNotNull();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData()).isInstanceOfAny(CartDetailResponseData.class);
        assertThat(((CartDetailResponseData)response.getData()).getStore()).isNull();
    }

    @Test
    public void testGetCartDetailsWhenDeliveryDetailsNotPopulated() {
        final TargetCartData mockCartData = mock(TargetCartData.class);
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(mockCartData);
        given(mockCartData.getDeliveryAddress()).willReturn(null);
        given(mockCartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));

        final Response response = facade.getCartDetail();
        assertThat(response).isNotNull();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData()).isInstanceOfAny(CartDetailResponseData.class);
        assertThat(((CartDetailResponseData)response.getData()).getContact()).isNull();
    }

    @Test
    public void testGetCartDetailsWithNullDeliveryMode() {
        final TargetCartData mockCartData = mock(TargetCartData.class);
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(mockCartData);
        given(mockCartData.getDeliveryMode()).willReturn(null);
        given(mockCartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        final Response response = facade.getCartDetail();
        assertThat(response).isNotNull();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData()).isInstanceOfAny(CartDetailResponseData.class);
    }

    @Test
    public void testGetCartDetail() {
        final TargetCartData mockCartData = mock(TargetCartData.class);
        given(mockCartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(mockCartData);
        final Response response = facade.getCartDetail();
        assertThat(response).isNotNull();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData()).isInstanceOfAny(CartDetailResponseData.class);
        final CartDetailResponseData cartDetailResponseData = (CartDetailResponseData)response.getData();
        verify(cartDetailPopulator).populate(mockCartData, cartDetailResponseData,
                ImmutableList.of(CartDetailOption.ALL));
    }

    @Test
    public void testSearchDeliveryAddressesWithNoDeliveryMode() {
        final Response response = facade.getDeliveryAddresses();
        verify(targetCheckoutFacade).getSupportedDeliveryAddresses(null, true);
        assertThat(response.isSuccess()).isTrue();
    }

    @Test
    public void setCncPickupDetailsWhenCNCDeliveryModeIsNull() {
        given(deliveryModeService.getDeliveryModeForCode("click-and-collect")).willReturn(null);
        final ClickAndCollectDeliveryDetailData cncData = new ClickAndCollectDeliveryDetailData();
        cncData.setStoreNumber(CNC_STORE_NUMBER);
        final Response response = facade.setCncPickupDetails(cncData);
        assertThat(response.isSuccess()).isFalse();
    }

    @Test
    public void setCncPickupDetailsWhenDeliveryAddressIsNull() {
        final TargetCartData checkoutCartData = new TargetCartData();
        final TargetZoneDeliveryModeModel deliveryMode = mock(TargetZoneDeliveryModeModel.class);
        given(deliveryModeService.getDeliveryModeForCode("click-and-collect")).willReturn(deliveryMode);
        given(Boolean.valueOf(targetCheckoutFacade.setClickAndCollectDeliveryDetails(Mockito
                .any(ClickAndCollectDeliveryDetailData.class)))).willReturn(Boolean.TRUE);
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(checkoutCartData);
        final ClickAndCollectDeliveryDetailData cncData = new ClickAndCollectDeliveryDetailData();
        cncData.setStoreNumber(CNC_STORE_NUMBER);
        final Response response = facade.setCncPickupDetails(cncData);
        assertThat(response.isSuccess()).isFalse();
    }

    @Test
    public void setCncPickupDetailsWhenCannotSetCNCDetails() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetZoneDeliveryModeModel deliveryMode = mock(TargetZoneDeliveryModeModel.class);
        given(deliveryModeService.getDeliveryModeForCode("click-and-collect")).willReturn(deliveryMode);
        willReturn(Boolean.FALSE).given(targetCheckoutFacade)
                .setClickAndCollectDeliveryDetails(Mockito.any(ClickAndCollectDeliveryDetailData.class));
        final ClickAndCollectDeliveryDetailData cncData = new ClickAndCollectDeliveryDetailData();
        cncData.setStoreNumber(CNC_STORE_NUMBER);
        final Response response = facade.setCncPickupDetails(cncData);
        assertThat(response.isSuccess()).isFalse();
    }

    @Test
    public void setCncPickupDetailsWhenAllInformationPresent() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetCartData checkoutCartData = new TargetCartData();
        checkoutCartData.setDeliveryAddress(new TargetAddressData());
        checkoutCartData.setEntries(Collections.singletonList(new OrderEntryData()));
        final TargetZoneDeliveryModeModel deliveryMode = mock(TargetZoneDeliveryModeModel.class);
        given(deliveryModeService.getDeliveryModeForCode("click-and-collect")).willReturn(deliveryMode);
        given(targetStoreLocatorFacade.getPointOfService(CNC_STORE_NUMBER)).willReturn(new TargetPointOfServiceData());
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(checkoutCartData);
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).setClickAndCollectDeliveryDetails(Mockito
                .any(ClickAndCollectDeliveryDetailData.class));
        final ClickAndCollectDeliveryDetailData cncData = new ClickAndCollectDeliveryDetailData();
        cncData.setStoreNumber(CNC_STORE_NUMBER);
        checkoutCartData.setCncStoreNumber(CNC_STORE_NUMBER);
        final Response response = facade.setCncPickupDetails(cncData);
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isNotNull();
        final CartDetailResponseData cartDetailResponseData = (CartDetailResponseData)response.getData();
        verify(cartDetailPopulator).populate(checkoutCartData, cartDetailResponseData,
                ImmutableList.of(CartDetailOption.CNC));
    }

    @Test
    public void testFailedToApplyTmd() {
        final String tmdNumber = "wrong number";
        willReturn(Boolean.FALSE).given(targetCheckoutFacade).setTeamMemberDiscountCardNumber(tmdNumber);
        final Response response = facade.applyTmd(tmdNumber);
        verify(flybuysDiscountFacade, Mockito.times(0)).reassessFlybuysDiscountOnCheckoutCart();
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getMessage()).isEqualTo(
                "We don't recognise that team member card number. Please check and re-submit.");
    }

    @Test
    public void testApplyTmd() {
        final String tmdNumber = "2771166666666";
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).setTeamMemberDiscountCardNumber(tmdNumber);
        willReturn(Collections.singletonList(new OrderEntryData())).given(cartData).getEntries();
        final Response response = facade.applyTmd(tmdNumber);
        verify(flybuysDiscountFacade).reassessFlybuysDiscountOnCheckoutCart();
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(CartDetailResponseData.class);
        final CartDetailResponseData cartDetailResponseData = (CartDetailResponseData)response.getData();
        verify(cartDetailPopulator).populate(cartData, cartDetailResponseData, ImmutableList.of(CartDetailOption.TMD));

    }

    @Test
    public void applyVoucherWhenVoucherCodeNotExist() {
        final String voucherCode = "A1234";
        willReturn(Boolean.FALSE).given(targetVoucherFacade).doesExist(voucherCode);
        final Response response = facade.applyVoucher(voucherCode);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_VOUCHER_DOES_NOT_EXIST");
        assertThat(response.getData().getError().getMessage()).isEqualTo("Promo Code unknown or expired");
    }

    @Test
    public void applyVoucherWhenThereIsVoucherInCart() {
        final String voucherCode = "A1234";
        willReturn(Boolean.TRUE).given(targetVoucherFacade).doesExist(voucherCode);
        willReturn(Boolean.FALSE).given(targetVoucherFacade).isExpired(voucherCode);
        given(targetCheckoutFacade.getFirstAppliedVoucher()).willReturn(voucherCode);
        final Response response = facade.applyVoucher(voucherCode);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_VOUCHER_CONDITIONS_NOT_MET");
        assertThat(response.getData().getError().getMessage()).isEqualTo("Promo Code conditions not met");
    }

    @Test
    public void applyVoucherWhenVoucherCodeExistAndVoucherExpired() {
        final String voucherCode = "A1234";
        willReturn(Boolean.TRUE).given(targetVoucherFacade).doesExist(voucherCode);
        willReturn(Boolean.TRUE).given(targetVoucherFacade).isExpired(voucherCode);
        final Response response = facade.applyVoucher(voucherCode);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_VOUCHER_DOES_NOT_EXIST");
        assertThat(response.getData().getError().getMessage()).isEqualTo("Promo Code unknown or expired");
    }

    @Test
    public void applyVoucherWhenVoucherCodeExistVoucherNotExpiredAndNotApplicable() {
        final String voucherCode = "A1234";
        willReturn(Boolean.TRUE).given(targetVoucherFacade).doesExist(voucherCode);
        willReturn(Boolean.FALSE).given(targetVoucherFacade).isExpired(voucherCode);

        final Response response = facade.applyVoucher(voucherCode);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_VOUCHER_CONDITIONS_NOT_MET");
        assertThat(response.getData().getError().getMessage()).isEqualTo("Promo Code conditions not met");
    }

    @Test
    public void applyVoucherPasses() {
        final String voucherCode = "A1234";
        final CartModel cartModel = mock(CartModel.class);
        willReturn(Boolean.TRUE).given(targetVoucherFacade).doesExist(voucherCode);
        willReturn(Boolean.FALSE).given(targetVoucherFacade).isExpired(voucherCode);
        given(targetCheckoutFacade.getCart()).willReturn(cartModel);
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).applyVoucher(voucherCode);
        given(targetCheckoutFacade.getCart()).willReturn(cartModel);
        given(cartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        final Response response = facade.applyVoucher(voucherCode);
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isNotNull();
        final CartDetailResponseData cartDetailResponseData = (CartDetailResponseData)response.getData();
        verify(cartDetailPopulator).populate(cartData, cartDetailResponseData,
                ImmutableList.of(CartDetailOption.VOUCHER));
    }

    @Test
    public void applyVoucherFails() {
        final String voucherCode = "A1234";
        final CartModel cartModel = mock(CartModel.class);
        willReturn(Boolean.TRUE).given(targetVoucherFacade).doesExist(voucherCode);
        willReturn(Boolean.FALSE).given(targetVoucherFacade).isExpired(voucherCode);
        given(targetCheckoutFacade.getCart()).willReturn(
                cartModel);
        willReturn(Boolean.FALSE).given(targetCheckoutFacade).applyVoucher(voucherCode);
        final Response response = facade.applyVoucher(voucherCode);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_VOUCHER_CONDITIONS_NOT_MET");
        assertThat(response.getData().getError().getMessage()).isEqualTo(
                "Promo Code conditions not met");
    }

    @Test
    public void selectEmptyAddress() {
        final Response response = facade.selectDeliveryAddress("");
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_VALIDATION");
        assertThat(response.getData().getError().getMessage()).isEqualTo(
                "Invalid post data");
    }

    @Test
    public void selectNonExistAddress() {
        given(targetCheckoutFacade.getDeliveryAddressForCode("wrongid")).willReturn(null);
        final Response response = facade.selectDeliveryAddress("wrongid");
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_VALIDATION");
        assertThat(response.getData().getError().getMessage()).isEqualTo(
                "Invalid post data");
    }

    @Test
    public void selectAddressFails() {
        given(targetCheckoutFacade.getDeliveryAddressForCode("addressId")).willReturn(addressData);
        willReturn(Boolean.TRUE).given(targetDeliveryFacade).isDeliveryModePostCodeCombinationValid("home-delivery",
                "2145");
        willReturn(Boolean.FALSE).given(targetCheckoutFacade).setDeliveryAddress(addressData);
        final Response response = facade.selectDeliveryAddress("addressId");
        verify(targetCheckoutFacade).setDeliveryAddress(addressData);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_VALIDATION");
        assertThat(response.getData().getError().getMessage()).isEqualTo(
                "Invalid post data");
    }

    @Test
    public void selectAddress() {
        given(targetCheckoutFacade.getDeliveryAddressForCode("addressId")).willReturn(addressData);
        willReturn(Boolean.TRUE).given(targetDeliveryFacade).isDeliveryModePostCodeCombinationValid("home-delivery",
                "2145");
        given(cartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).setDeliveryAddress(addressData);
        final Response response = facade.selectDeliveryAddress("addressId");
        verify(targetCheckoutFacade).setDeliveryAddress(addressData);
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isNotNull();
        final CartDetailResponseData cartDetailResponseData = (CartDetailResponseData)response.getData();
        verify(cartDetailPopulator).populate(cartData, cartDetailResponseData,
                ImmutableList.of(CartDetailOption.DELIVERY_ADDRESS));
    }

    @Test
    public void selectAddressWithInvalidPostCodeAndDeliveryModeCombination() {
        given(targetCheckoutFacade.getDeliveryAddressForCode("addressId")).willReturn(addressData);
        willReturn(Boolean.FALSE).given(targetDeliveryFacade).isDeliveryModePostCodeCombinationValid("home-delivery",
                "2145");
        final Response response = facade.selectDeliveryAddress("addressId");
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_ADDRESS_UNAVAILABLE");
        assertThat(response.getData().getError().getMessage()).isEqualTo(
                "Provided address is not valid for the selected delivery mode");
    }

    @Test
    public void selectAddressWithDeliveryModeNull() {
        given(cart.getDeliveryMode()).willReturn(null);
        given(targetCheckoutFacade.getDeliveryAddressForCode("addressId")).willReturn(addressData);
        willReturn(Boolean.FALSE).given(targetDeliveryFacade).isDeliveryModePostCodeCombinationValid("home-delivery",
                "2145");
        final Response response = facade.selectDeliveryAddress("addressId");
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_ADDRESS_UNAVAILABLE");
        assertThat(response.getData().getError().getMessage()).isEqualTo(
                "Provided address is not valid for the selected delivery mode");
    }

    @Test
    public void selectAddressDeliveryFeeChanged() {
        final CartModel checkoutCart = mock(CartModel.class);
        final Double deliveryCost = new Double(10.0);
        given(targetCheckoutFacade.getDeliveryAddressForCode("addressId")).willReturn(addressData);
        given(targetCheckoutFacade.getCart()).willReturn(checkoutCart);
        given(checkoutCart.getDeliveryMode()).willReturn(deliveryModeModel);
        given(cartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        willReturn(Boolean.FALSE).given(targetCheckoutFacade).isDeliveryFeeVariesAfterAddressChange(BigDecimal
                .valueOf(deliveryCost.doubleValue()));
        given(checkoutCart.getDeliveryCost()).willReturn(deliveryCost);
        willReturn(Boolean.TRUE).given(targetDeliveryFacade).isDeliveryModePostCodeCombinationValid("home-delivery",
                "2145");
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).setDeliveryAddress(addressData);
        final Response response = facade.selectDeliveryAddress("addressId");
        verify(targetCheckoutFacade).setDeliveryAddress(addressData);
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isNotNull();
        final CartDetailResponseData cartDetailResponseData = (CartDetailResponseData)response.getData();
        verify(cartDetailPopulator).populate(cartData, cartDetailResponseData,
                ImmutableList.of(CartDetailOption.DELIVERY_ADDRESS));
        assertThat(cartDetailResponseData.getDeliveryFeeChanged()).isFalse();
    }

    @Test
    public void selectAddressDeliveryFeeNotChanged() {
        final CartModel checkoutCart = mock(CartModel.class);
        final Double deliveryCost = new Double(10.0);
        given(targetCheckoutFacade.getDeliveryAddressForCode("addressId")).willReturn(addressData);
        given(targetCheckoutFacade.getCart()).willReturn(checkoutCart);
        given(checkoutCart.getDeliveryMode()).willReturn(deliveryModeModel);
        willReturn(Boolean.TRUE).given(targetDeliveryFacade).isDeliveryModePostCodeCombinationValid("home-delivery",
                "2145");
        given(cartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).isDeliveryFeeVariesAfterAddressChange(BigDecimal
                .valueOf(deliveryCost.doubleValue()));
        given(checkoutCart.getDeliveryCost()).willReturn(deliveryCost);

        willReturn(Boolean.TRUE).given(targetCheckoutFacade).setDeliveryAddress(addressData);

        final Response response = facade.selectDeliveryAddress("addressId");
        verify(targetCheckoutFacade).setDeliveryAddress(addressData);
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isNotNull();
        final CartDetailResponseData cartDetailResponseData = (CartDetailResponseData)response.getData();
        verify(cartDetailPopulator).populate(cartData, cartDetailResponseData,
                ImmutableList.of(CartDetailOption.DELIVERY_ADDRESS));
        assertThat(cartDetailResponseData.getDeliveryFeeChanged()).isTrue();
    }

    @Test
    public void createAddressSuccessful() {
        final TargetAddressData newAddressData = mock(TargetAddressData.class);
        given(newAddressData.getId()).willReturn("123");
        final Response response = facade.createAddress(newAddressData);
        verify(targetUserFacade).addAddress(newAddressData);
        verify(targetCheckoutFacade).getDeliveryAddressForCode("123");
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData()).isInstanceOf(CreateAddressResponseData.class);
    }

    @Test
    public void createAddressUnsuccessful() {
        final TargetAddressData newAddressData = mock(TargetAddressData.class);
        given(newAddressData.getId()).willReturn(null);
        final Response response = facade.createAddress(newAddressData);
        verify(targetUserFacade).addAddress(newAddressData);
        verifyZeroInteractions(targetCheckoutFacade);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_VALIDATION");
        assertThat(response.getData().getError().getMessage()).isEqualTo(
                "Invalid post data");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createAddressWithNullAddress() {
        facade.createAddress(null);
    }

    @Test
    public void getApplicablePaymentMethodsWhenAllActive() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isPaypalEnabled();
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isIpgEnabled();
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isGiftCardPaymentEnabled();
        final Response response = facade.getApplicablePaymentMethods();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        assertThat(paymentMethods).hasSize(3);
        final PaymentMethodData giftcard = getPaymentMethodData(TgtFacadesConstants.GIFT_CARD, paymentMethods);
        final PaymentMethodData ipg = getPaymentMethodData(TgtFacadesConstants.IPG, paymentMethods);
        final PaymentMethodData paypal = getPaymentMethodData(TgtFacadesConstants.PAYPAL, paymentMethods);
        assertThat(giftcard).isNotNull();
        assertThat(giftcard.isAvailable()).isTrue();
        assertThat(giftcard.getReason()).isNull();
        assertThat(ipg).isNotNull();
        assertThat(ipg.isAvailable()).isTrue();
        assertThat(paypal).isNotNull();
        assertThat(paypal.isAvailable()).isTrue();
    }

    @Test
    public void getApplicablePaymentMethodsWhenAllActiveButGiftCardInCart() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isPaypalEnabled();
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isIpgEnabled();
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isGiftCardPaymentEnabled();
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).isGiftCardInCart();
        final Response response = facade.getApplicablePaymentMethods();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        assertThat(paymentMethods).hasSize(3);
        final PaymentMethodData giftcard = getPaymentMethodData(TgtFacadesConstants.GIFT_CARD, paymentMethods);
        final PaymentMethodData ipg = getPaymentMethodData(TgtFacadesConstants.IPG, paymentMethods);
        final PaymentMethodData paypal = getPaymentMethodData(TgtFacadesConstants.PAYPAL, paymentMethods);
        assertThat(giftcard).isNotNull();
        assertThat(giftcard.isAvailable()).isFalse();
        assertThat(giftcard.getReason()).isEqualTo(REASON);
        assertThat(ipg).isNotNull();
        assertThat(ipg.isAvailable()).isTrue();
        assertThat(paypal).isNotNull();
        assertThat(paypal.isAvailable()).isTrue();
    }

    @Test
    public void getApplicablePaymentMethodsWhenPayPalDisabledIPGAndGiftCardActive() {
        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isPaypalEnabled();
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isIpgEnabled();
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isGiftCardPaymentEnabled();
        final Response response = facade.getApplicablePaymentMethods();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        assertThat(paymentMethods).hasSize(2);
        final PaymentMethodData giftcard = getPaymentMethodData(TgtFacadesConstants.GIFT_CARD, paymentMethods);
        final PaymentMethodData ipg = getPaymentMethodData(TgtFacadesConstants.IPG, paymentMethods);
        assertThat(giftcard).isNotNull();
        assertThat(giftcard.isAvailable()).isTrue();
        assertThat(giftcard.getReason()).isNull();
        assertThat(ipg).isNotNull();
        assertThat(ipg.isAvailable()).isTrue();
    }

    @Test
    public void getApplicablePaymentMethodsWhenPaypalActiveIPGDisabledGiftCardDisabled() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isPaypalEnabled();
        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isIpgEnabled();
        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isGiftCardPaymentEnabled();
        final Response response = facade.getApplicablePaymentMethods();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        assertThat(paymentMethods).hasSize(1);
        final PaymentMethodData paypal = getPaymentMethodData(TgtFacadesConstants.PAYPAL, paymentMethods);
        assertThat(paypal).isNotNull();
        assertThat(paypal.isAvailable()).isTrue();
        assertThat(paypal.getReason()).isEqualTo(
                "Credit and gift card payments currently unavailable. We recommend PayPal.");
    }

    @Test
    public void getApplicablePaymentMethodsWhenPaypalActiveIPGDisabledGiftCardActive() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isPaypalEnabled();
        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isIpgEnabled();
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isGiftCardPaymentEnabled();
        final Response response = facade.getApplicablePaymentMethods();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        assertThat(paymentMethods).hasSize(1);
        final PaymentMethodData paypal = getPaymentMethodData(TgtFacadesConstants.PAYPAL, paymentMethods);
        assertThat(paypal).isNotNull();
        assertThat(paypal.isAvailable()).isTrue();
        assertThat(paypal.getReason()).isEqualTo(
                "Credit and gift card payments currently unavailable. We recommend PayPal.");
    }

    @Test
    public void getApplicablePaymentMethodsWhenPaypalActiveIPGActiveAndGiftCardInActive() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isPaypalEnabled();
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isIpgEnabled();
        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isGiftCardPaymentEnabled();
        final Response response = facade.getApplicablePaymentMethods();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        assertThat(paymentMethods).hasSize(2);
        final PaymentMethodData ipg = getPaymentMethodData(TgtFacadesConstants.IPG, paymentMethods);
        final PaymentMethodData paypal = getPaymentMethodData(TgtFacadesConstants.PAYPAL, paymentMethods);
        assertThat(ipg).isNotNull();
        assertThat(ipg.isAvailable()).isTrue();
        assertThat(paypal).isNotNull();
        assertThat(paypal.isAvailable()).isTrue();
    }

    @Test
    public void getApplicablePaymentMethodsWhenAfterpayActiveAndCartWithinThreshold() throws Exception {
        final Double cartTotal = Double.valueOf(100d);
        given(targetCheckoutFacade.getCart()).willReturn(cart);
        given(cart.getTotalPrice()).willReturn(cartTotal);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isAfterpayEnabled();
        willReturn(Boolean.TRUE).given(targetCommerceCartService).isCartWithinAfterpayThresholds(any(BigDecimal.class));
        willReturn(Boolean.TRUE).given(afterpayConfigFacade).ping();
        given(Boolean.valueOf(targetCommerceCartService.isExcludedForAfterpay(cart))).willReturn(Boolean.FALSE);

        final Response response = facade.getApplicablePaymentMethods();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        assertThat(paymentMethods).hasSize(1);
        final PaymentMethodData afterpay = getPaymentMethodData(TgtFacadesConstants.AFTERPAY, paymentMethods);
        final ArgumentCaptor<BigDecimal> dollarsCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        verify(targetCommerceCartService).isCartWithinAfterpayThresholds(dollarsCaptor.capture());
        assertThat(dollarsCaptor.getValue()).isEqualTo(BigDecimal.valueOf(cartTotal.doubleValue()));
        assertThat(afterpay).isNotNull();
        assertThat(afterpay.isAvailable()).isTrue();
    }

    @Test
    public void getApplicablePaymentMethodsWhenAfterpayActiveAndCartOutsideThreshold() throws Exception {
        final Double cartTotal = Double.valueOf(20d);
        given(targetCheckoutFacade.getCart()).willReturn(cart);
        given(cart.getTotalPrice()).willReturn(cartTotal);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isAfterpayEnabled();
        willReturn(Boolean.FALSE).given(targetCommerceCartService)
                .isCartWithinAfterpayThresholds(any(BigDecimal.class));
        willReturn(Boolean.TRUE).given(afterpayConfigFacade).ping();
        given(Boolean.valueOf(targetCommerceCartService.isExcludedForAfterpay(cart))).willReturn(Boolean.FALSE);

        final Response response = facade.getApplicablePaymentMethods();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        assertThat(paymentMethods).hasSize(1);
        final PaymentMethodData afterpay = getPaymentMethodData(TgtFacadesConstants.AFTERPAY, paymentMethods);

        assertThat(afterpay).isNotNull();
        assertThat(afterpay.isAvailable()).isFalse();
        assertThat(afterpay.getReason()).isEqualTo("Available on orders $50 to $1,000");
        final ArgumentCaptor<BigDecimal> dollarsCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        verify(targetCommerceCartService).isCartWithinAfterpayThresholds(dollarsCaptor.capture());
        assertThat(dollarsCaptor.getValue()).isEqualTo(BigDecimal.valueOf(cartTotal.doubleValue()));

    }

    @Test
    public void getApplicablePaymentMethodsWhenAfterpayInactive() throws Exception {
        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isAfterpayEnabled();
        final Response response = facade.getApplicablePaymentMethods();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        assertThat(paymentMethods).hasSize(0);
        final PaymentMethodData afterpay = getPaymentMethodData(TgtFacadesConstants.AFTERPAY, paymentMethods);

        assertThat(afterpay).isNull();
    }

    @Test
    public void getApplicablePaymentMethodsWhenAfterpayActiveAndPingSuccess() throws Exception {
        final Double cartTotal = Double.valueOf(100d);
        given(targetCheckoutFacade.getCart()).willReturn(cart);
        given(cart.getTotalPrice()).willReturn(cartTotal);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isAfterpayEnabled();
        willReturn(Boolean.TRUE).given(targetCommerceCartService).isCartWithinAfterpayThresholds(any(BigDecimal.class));
        willReturn(Boolean.TRUE).given(afterpayConfigFacade).ping();
        given(Boolean.valueOf(targetCommerceCartService.isExcludedForAfterpay(cart))).willReturn(Boolean.FALSE);
        final Response response = facade.getApplicablePaymentMethods();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        assertThat(paymentMethods).hasSize(1);
        final PaymentMethodData afterpay = getPaymentMethodData(TgtFacadesConstants.AFTERPAY, paymentMethods);

        assertThat(afterpay).isNotNull();
        assertThat(afterpay.isAvailable()).isTrue();
    }

    @Test
    public void getApplicablePaymentMethodsWhenAfterpayActiveAndPingFails() throws Exception {
        final Double cartTotal = Double.valueOf(100d);
        given(targetCheckoutFacade.getCart()).willReturn(cart);
        given(cart.getTotalPrice()).willReturn(cartTotal);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isAfterpayEnabled();
        willReturn(Boolean.TRUE).given(targetCommerceCartService).isCartWithinAfterpayThresholds(any(BigDecimal.class));
        willReturn(Boolean.FALSE).given(afterpayConfigFacade).ping();
        given(Boolean.valueOf(targetCommerceCartService.isExcludedForAfterpay(cart))).willReturn(Boolean.FALSE);

        final Response response = facade.getApplicablePaymentMethods();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        assertThat(paymentMethods).hasSize(1);
        final PaymentMethodData afterpay = getPaymentMethodData(TgtFacadesConstants.AFTERPAY, paymentMethods);

        assertThat(afterpay).isNotNull();
        assertThat(afterpay.isAvailable()).isFalse();
        assertThat(afterpay.isRetryRequired()).isTrue();
    }

    @Test
    public void getApplicablePaymentMethodsWhenAfterpayActiveAndCartContainsExcludedItem() throws Exception {
        final Double cartTotal = Double.valueOf(20d);
        given(targetCheckoutFacade.getCart()).willReturn(cart);
        given(cart.getTotalPrice()).willReturn(cartTotal);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isAfterpayEnabled();
        willReturn(Boolean.TRUE).given(targetCommerceCartService)
                .isCartWithinAfterpayThresholds(any(BigDecimal.class));
        willReturn(Boolean.TRUE).given(afterpayConfigFacade).ping();
        given(Boolean.valueOf(targetCommerceCartService.isExcludedForAfterpay(cart))).willReturn(Boolean.TRUE);


        final Response response = facade.getApplicablePaymentMethods();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        assertThat(paymentMethods).hasSize(1);
        final PaymentMethodData afterpay = getPaymentMethodData(TgtFacadesConstants.AFTERPAY, paymentMethods);

        assertThat(afterpay).isNotNull();
        assertThat(afterpay.isAvailable()).isFalse();
        assertThat(afterpay.getReason()).isEqualTo("Items in your basket prevent this payment option.");
    }

    @Test
    public void getApplicablePaymentMethodsWhenAfterpayActiveAndConfigMissing() throws Exception {
        final Double cartTotal = Double.valueOf(100d);
        given(targetCheckoutFacade.getCart()).willReturn(cart);
        given(cart.getTotalPrice()).willReturn(cartTotal);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isAfterpayEnabled();
        willThrow(new IllegalArgumentException()).given(targetCommerceCartService)
                .isCartWithinAfterpayThresholds(any(BigDecimal.class));
        willReturn(Boolean.TRUE).given(afterpayConfigFacade).ping();
        given(Boolean.valueOf(targetCommerceCartService.isExcludedForAfterpay(cart))).willReturn(Boolean.FALSE);

        final Response response = facade.getApplicablePaymentMethods();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        assertThat(paymentMethods).hasSize(1);
        final PaymentMethodData afterpay = getPaymentMethodData(TgtFacadesConstants.AFTERPAY, paymentMethods);

        assertThat(afterpay).isNotNull();
        assertThat(afterpay.isAvailable()).isFalse();
        assertThat(afterpay.isRetryRequired()).isFalse();
    }

    @Test
    public void getApplicablePaymentMethodsWhenAllInactive() {
        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isPaypalEnabled();
        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isIpgEnabled();
        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isGiftCardPaymentEnabled();
        final Response response = facade.getApplicablePaymentMethods();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        assertThat(paymentMethods).hasSize(0);
    }

    @Test
    public void searchAddressWithQASUnavaible() throws ServiceNotAvailableException, RequestTimeOutException,
            NoMatchFoundException, TooManyMatchesFoundException {
        final String keyword = "111 bourke st";
        final ServiceNotAvailableException exception = new ServiceNotAvailableException("ServiceNotAvailableException");
        given(targetAddressVerificationService.searchAddress(keyword, CountryEnum.AUSTRALIA)).willThrow(
                exception);
        final Response response = facade.searchAddress(keyword, 10);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_QAS_UNAVAILABLE");
        assertThat(response.getData().getError().getMessage()).isEqualTo("ServiceNotAvailableException");
    }

    @Test
    public void searchAddressWithTimeout() throws ServiceNotAvailableException, RequestTimeOutException,
            NoMatchFoundException, TooManyMatchesFoundException {
        final String keyword = "111 bourke st";
        final RequestTimeOutException exception = new RequestTimeOutException("RequestTimeOutException");
        given(targetAddressVerificationService.searchAddress(keyword, CountryEnum.AUSTRALIA)).willThrow(
                exception);
        final Response response = facade.searchAddress(keyword, 10);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_QAS_UNAVAILABLE");
        assertThat(response.getData().getError().getMessage()).isEqualTo("RequestTimeOutException");
    }

    @Test
    public void searchAddressWithTooManyResult() throws ServiceNotAvailableException, RequestTimeOutException,
            NoMatchFoundException, TooManyMatchesFoundException {
        final String keyword = "111 bourke st";
        final TooManyMatchesFoundException exception = new TooManyMatchesFoundException("TooManyMatchesFoundException");
        given(targetAddressVerificationService.searchAddress(keyword, CountryEnum.AUSTRALIA)).willThrow(
                exception);
        final Response response = facade.searchAddress(keyword, 10);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_TOOMANY_MATCHES");
        assertThat(response.getData().getError().getMessage()).isEqualTo("TooManyMatchesFoundException");
    }

    @Test
    public void searchAddressWithSublistResult() throws ServiceNotAvailableException, RequestTimeOutException,
            NoMatchFoundException, TooManyMatchesFoundException {
        final String keyword = "111 bourke st";
        final List<au.com.target.tgtverifyaddr.data.AddressData> suggestions = new ArrayList<au.com.target.tgtverifyaddr.data.AddressData>();
        final au.com.target.tgtverifyaddr.data.AddressData firstMatch = new au.com.target.tgtverifyaddr.data.AddressData(
                "1", "1", "1");
        suggestions.add(firstMatch);
        suggestions.add(new au.com.target.tgtverifyaddr.data.AddressData("2", "2", "2"));
        given(targetAddressVerificationService.searchAddress(keyword, CountryEnum.AUSTRALIA)).willReturn(suggestions);
        final Response response = facade.searchAddress(keyword, 1);
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        assertThat(response.isSuccess()).isTrue();
        assertThat(responseData.getAddressSuggestions()).hasSize(1);
        assertThat(responseData.getAddressSuggestions().get(0)).isEqualTo(firstMatch);
    }

    @Test
    public void searchAddressWith1Result() throws ServiceNotAvailableException, RequestTimeOutException,
            NoMatchFoundException, TooManyMatchesFoundException {
        final String keyword = "111 bourke st";
        final List<au.com.target.tgtverifyaddr.data.AddressData> suggestions = new ArrayList<au.com.target.tgtverifyaddr.data.AddressData>();
        final au.com.target.tgtverifyaddr.data.AddressData firstMatch = new au.com.target.tgtverifyaddr.data.AddressData(
                "1", "1", "1");
        suggestions.add(firstMatch);
        given(targetAddressVerificationService.searchAddress(keyword, CountryEnum.AUSTRALIA)).willReturn(suggestions);
        final Response response = facade.searchAddress(keyword, 10);
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        assertThat(response.isSuccess()).isTrue();
        assertThat(responseData.getAddressSuggestions()).hasSize(1);
        assertThat(responseData.getAddressSuggestions().get(0)).isEqualTo(firstMatch);
    }

    @Test
    public void searchAddressWithNoResult() throws ServiceNotAvailableException, RequestTimeOutException,
            NoMatchFoundException, TooManyMatchesFoundException {
        final String keyword = "111 bourke st";
        final NoMatchFoundException exception = new NoMatchFoundException("NoMatchFoundException");
        given(targetAddressVerificationService.searchAddress(keyword, CountryEnum.AUSTRALIA)).willThrow(
                exception);
        final Response response = facade.searchAddress(keyword, 10);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_NO_MATCH");
        assertThat(response.getData().getError().getMessage()).isEqualTo("NoMatchFoundException");
    }

    @Test
    public void removeExistingPayment() {
        facade.removeExistingPayment();
        verify(targetPlaceOrderFacade).reverseGiftCardPayment();
        verify(targetCheckoutFacade).removePaymentInfo();
    }

    @Test
    public void setIpgPaymentWithInvalidMode() {
        final Response response = facade.setIpgPaymentMode("cc", "return");
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_PAYMENTMODE_NONEXIST");
        assertThat(response.getData().getError().getMessage()).isEqualTo("invalid payment mode");
    }

    @Test
    public void setIpgPaymentWithInvalidCart() {
        given(targetCheckoutFacade.getCart()).willReturn(null);
        final Response response = facade.setIpgPaymentMode("ipg", "return");
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_CART_INVALID");
        assertThat(response.getData().getError().getMessage()).isEqualTo(
                "cart data is not complete to process to payment");
    }

    @Test
    public void setIpgPaymentSuccessfully() {
        final String ipgToken = "token";
        final String sessionId = "sessionId";
        final String paymentMode = "giftcard";
        final String iframeUrl = "iframeUrl";
        final String returnUrl = "return";
        given(targetCreateSubscriptionResult.getRequestToken()).willReturn(ipgToken);
        given(targetCheckoutFacade.getIpgSessionToken(returnUrl)).willReturn(targetCreateSubscriptionResult);
        given(cart.getUniqueKeyForPayment()).willReturn(sessionId);
        given(targetCheckoutFacade.createIpgUrl(sessionId, ipgToken)).willReturn(iframeUrl);
        final AddressModel billingAddressModel = mock(AddressModel.class);
        given(cart.getPaymentAddress()).willReturn(billingAddressModel);
        final TargetAddressData billingAddressData = mock(TargetAddressData.class);
        given(addressConverter.convert(billingAddressModel)).willReturn(billingAddressData);

        final Response response = facade.setIpgPaymentMode(paymentMode, returnUrl);

        verify(targetCheckoutFacade).setPaymentMode(paymentMode);
        verify(targetCheckoutFacade).createIpgPaymentInfo(billingAddressData, IpgPaymentTemplateType.GIFTCARDMULTIPLE);
        verify(targetCheckoutFacade).updateIpgPaymentInfoWithToken(ipgToken);
        assertThat(response.isSuccess()).isTrue();
        final PaymentResponseData responseData = (PaymentResponseData)response.getData();
        assertThat(responseData.getIframeUrl()).isEqualTo(iframeUrl);
        assertThat(responseData.getIpgToken()).isEqualTo(ipgToken);
        assertThat(responseData.getSessionId()).isEqualTo(sessionId);
        assertThat(responseData.getPaymentMethod()).isEqualTo(paymentMode);
    }

    @Test
    public void applyFlybuysWhenFlybuysDiscountAlreadyApplied() {
        final String flybuysNumber = "6008943218616910";
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).isFlybuysDiscountAlreadyApplied(flybuysNumber);
        final Response response = facade.applyFlybuys(flybuysNumber);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        verify(targetCheckoutFacade, Mockito.times(0)).setFlybuysNumber(flybuysNumber);
    }

    @Test
    public void applyFlybuysWhenNoFlybuysDiscountWhenNumberBlank() {
        final String flybuysNumber = "";
        willReturn(Boolean.FALSE).given(targetCheckoutFacade).isFlybuysDiscountAlreadyApplied(flybuysNumber);
        final Response response = facade.applyFlybuys(flybuysNumber);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        verify(targetCheckoutFacade, Mockito.times(0)).setFlybuysNumber(flybuysNumber);
    }

    @Test
    public void applyFlybuysWhenSetFlybuysNumberFails() {
        final String flybuysNumber = "6008943218616910";
        willReturn(Boolean.FALSE).given(targetCheckoutFacade).isFlybuysDiscountAlreadyApplied(flybuysNumber);
        willReturn(Boolean.FALSE).given(targetCheckoutFacade).setFlybuysNumber(flybuysNumber);
        final Response response = facade.applyFlybuys(flybuysNumber);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        verify(targetCheckoutFacade, Mockito.times(1)).setFlybuysNumber(flybuysNumber);
    }

    @Test
    public void applyFlybuysWhenSetFlybuysNumberPass() {
        final String flybuysNumber = "6008943218616910";
        willReturn(Boolean.FALSE).given(targetCheckoutFacade).isFlybuysDiscountAlreadyApplied(flybuysNumber);
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).setFlybuysNumber(flybuysNumber);
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(cartData);
        given(cartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        final Response response = facade.applyFlybuys(flybuysNumber);
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData().getError()).isNull();
        assertThat(response.getData()).isNotNull();
    }

    @Test
    public void setPaypalPaymentModeWhenPayPalReturnUrlIsNull() {
        given(targetCreateSubscriptionResult.getRequestToken()).willReturn("");
        given(targetCheckoutFacade.getPayPalSessionToken(null, "cancelURL")).willReturn(targetCreateSubscriptionResult);
        final Response response = facade.setPaypalPaymentMode(null, "cancelURL");
        assertThat(response.isSuccess()).isEqualTo(false);
        final au.com.target.tgtfacades.response.data.Error error = response.getData().getError();
        assertThat(error.getCode()).isEqualTo("ERR_CART_INVALID");
        assertThat(error.getMessage()).isEqualTo("Cart data is not complete to process to payment");
    }

    @Test
    public void setPaypalPaymentModeWhenPayPalCancelUrlIsNull() {
        given(targetCreateSubscriptionResult.getRequestToken()).willReturn("");
        given(targetCheckoutFacade.getPayPalSessionToken("returnURL", null)).willReturn(targetCreateSubscriptionResult);
        final Response response = facade.setPaypalPaymentMode("returnURL", null);
        assertThat(response.isSuccess()).isEqualTo(false);
        final au.com.target.tgtfacades.response.data.Error error = response.getData().getError();
        assertThat(error.getCode()).isEqualTo("ERR_CART_INVALID");
        assertThat(error.getMessage()).isEqualTo("Cart data is not complete to process to payment");
    }

    @Test
    public void setPaypalPaymentModeWhenAllParamsNull() {
        given(targetCreateSubscriptionResult.getRequestToken()).willReturn("");
        given(targetCheckoutFacade.getPayPalSessionToken(null, null)).willReturn(targetCreateSubscriptionResult);
        final Response response = facade.setPaypalPaymentMode(null, null);
        assertThat(response.isSuccess()).isEqualTo(false);
    }

    @Test
    public void setPaypalPaymentModeWhenSessionTokenNull() {
        given(targetCreateSubscriptionResult.getRequestToken()).willReturn("");
        given(targetCheckoutFacade.getPayPalSessionToken("returnURL", "cancelUrl"))
                .willReturn(targetCreateSubscriptionResult);

        final Response response = facade.setPaypalPaymentMode("returnURL", "cancelUrl");
        assertThat(response.isSuccess()).isEqualTo(false);
        final au.com.target.tgtfacades.response.data.Error error = response.getData().getError();
        assertThat(error.getCode()).isEqualTo("ERR_CART_INVALID");
        assertThat(error.getMessage()).isEqualTo("Cart data is not complete to process to payment");
    }

    @Test
    public void setPaypalPaymentModeWhenSessionTokenEmpty() {
        given(targetCreateSubscriptionResult.getRequestToken())
                .willReturn("");
        given(targetCheckoutFacade.getPayPalSessionToken("returnURL", "cancelUrl"))
                .willReturn(targetCreateSubscriptionResult);
        final Response response = facade.setPaypalPaymentMode("returnURL", "cancelUrl");
        assertThat(response.isSuccess()).isEqualTo(false);
        final au.com.target.tgtfacades.response.data.Error error = response.getData().getError();
        assertThat(error.getCode()).isEqualTo("ERR_CART_INVALID");
        assertThat(error.getMessage()).isEqualTo("Cart data is not complete to process to payment");
    }

    @Test
    public void setPaypalPaymentModeWhenAdapterException() {
        given(targetCheckoutFacade.getPayPalSessionToken("returnURL", "cancelUrl")).willThrow(new AdapterException());
        final Response response = facade.setPaypalPaymentMode("returnURL", "cancelUrl");
        assertThat(response.isSuccess()).isEqualTo(false);
        final au.com.target.tgtfacades.response.data.Error error = response.getData().getError();
        assertThat(error.getCode()).isEqualTo("ERR_PAYPAL_UNAVAILABLE");
        assertThat(error.getMessage()).isEqualTo("PayPal is currently unavailable");
    }

    @Test
    public void setPaypalPaymentModeWhenSuccess() {
        given(targetCreateSubscriptionResult.getRequestToken())
                .willReturn("EC-7E42147767118710X");
        given(targetCheckoutFacade.getPayPalSessionToken("returnURL", "cancelUrl"))
                .willReturn(targetCreateSubscriptionResult);
        final Response response = facade.setPaypalPaymentMode("returnURL", "cancelUrl");
        assertThat(response.isSuccess()).isEqualTo(true);
        final PaymentResponseData responseData = (PaymentResponseData)response.getData();
        assertThat(responseData.getPaypalSessionToken()).isEqualTo("EC-7E42147767118710X");
    }

    @Test
    public void setAfterpayPaymentModeWhenAfterpayReturnUrlIsNull() {
        given(targetCheckoutFacade.getAfterpaySessionToken(null, "cancelUrl"))
                .willReturn(null);
        final Response response = facade.setAfterpayPaymentMode(null, "cancelURL");
        assertThat(response.isSuccess()).isEqualTo(false);
        final au.com.target.tgtfacades.response.data.Error error = response.getData().getError();
        assertThat(error.getCode()).isEqualTo("ERR_CART_INVALID");
        assertThat(error.getMessage()).isEqualTo("Cart data is not complete to process to payment");
    }

    @Test
    public void setAfterpayPaymentModeWhenAfterpayCancelUrlIsNull() {
        given(targetCreateSubscriptionResult.getRequestToken()).willReturn("");
        given(targetCheckoutFacade.getAfterpaySessionToken("returnURL", null))
                .willReturn(targetCreateSubscriptionResult);
        final Response response = facade.setAfterpayPaymentMode("returnURL", null);
        assertThat(response.isSuccess()).isEqualTo(false);
        final au.com.target.tgtfacades.response.data.Error error = response.getData().getError();
        assertThat(error.getCode()).isEqualTo("ERR_CART_INVALID");
        assertThat(error.getMessage()).isEqualTo("Cart data is not complete to process to payment");
    }

    @Test
    public void setAfterpayPaymentModeWhenAllParamsNull() {
        given(targetCreateSubscriptionResult.getRequestToken()).willReturn("");
        given(targetCheckoutFacade.getAfterpaySessionToken(null, null))
                .willReturn(targetCreateSubscriptionResult);
        final Response response = facade.setAfterpayPaymentMode(null, null);
        assertThat(response.isSuccess()).isEqualTo(false);
    }

    @Test
    public void setAfterpayPaymentModeWhenSessionTokenNull() {
        given(targetCreateSubscriptionResult.getRequestToken()).willReturn("");
        given(targetCheckoutFacade.getAfterpaySessionToken("returnURL", "cancelUrl"))
                .willReturn(targetCreateSubscriptionResult);
        final Response response = facade.setAfterpayPaymentMode("returnURL", "cancelUrl");
        assertThat(response.isSuccess()).isEqualTo(false);
        final au.com.target.tgtfacades.response.data.Error error = response.getData().getError();
        assertThat(error.getCode()).isEqualTo("ERR_CART_INVALID");
        assertThat(error.getMessage()).isEqualTo("Cart data is not complete to process to payment");
    }

    @Test
    public void setAfterpayPaymentModeWhenSessionTokenEmpty() {
        given(targetCreateSubscriptionResult.getRequestToken()).willReturn("");
        given(targetCheckoutFacade.getAfterpaySessionToken("returnURL", "cancelUrl"))
                .willReturn(targetCreateSubscriptionResult);
        final Response response = facade.setAfterpayPaymentMode("returnURL", "cancelUrl");
        assertThat(response.isSuccess()).isEqualTo(false);
        final au.com.target.tgtfacades.response.data.Error error = response.getData().getError();
        assertThat(error.getCode()).isEqualTo("ERR_CART_INVALID");
        assertThat(error.getMessage()).isEqualTo("Cart data is not complete to process to payment");
    }

    @Test
    public void setAfterpayPaymentModeWhenAdapterException() {
        given(targetCheckoutFacade.getAfterpaySessionToken("returnURL", "cancelUrl")).willThrow(new AdapterException());
        final Response response = facade.setAfterpayPaymentMode("returnURL", "cancelUrl");
        assertThat(response.isSuccess()).isEqualTo(false);
        final au.com.target.tgtfacades.response.data.Error error = response.getData().getError();
        assertThat(error.getCode()).isEqualTo("ERR_AFTERPAY_UNAVAILABLE");
        assertThat(error.getMessage()).isEqualTo("Afterpay is currently unavailable");
    }

    @Test
    public void setAfterpayPaymentModeWhenSuccess() {
        given(targetCreateSubscriptionResult.getRequestToken())
                .willReturn("q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu");
        given(targetCheckoutFacade.getAfterpaySessionToken("returnURL", "cancelUrl"))
                .willReturn(targetCreateSubscriptionResult);
        final Response response = facade.setAfterpayPaymentMode("returnURL", "cancelUrl");
        assertThat(response.isSuccess()).isEqualTo(true);
        final PaymentResponseData responseData = (PaymentResponseData)response.getData();
        assertThat(responseData.getAfterpaySessionToken())
                .isEqualTo("q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu");
    }

    @Test
    public void removeVoucherWhenNoAppliedVoucher() {
        given(targetCheckoutFacade.getFirstAppliedVoucher()).willReturn(null);
        final Response response = facade.removeVoucher();
        assertThat(response.isSuccess()).isFalse();
        verify(targetCheckoutFacade, times(0)).removeVoucher(null);
    }

    @Test
    public void removeVoucherWhenVoucherIsApplied() {
        final String voucherCode = "1234A";
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(cartData);
        given(cartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        given(targetCheckoutFacade.getFirstAppliedVoucher()).willReturn(voucherCode);
        final Response response = facade.removeVoucher();
        assertThat(response.isSuccess()).isTrue();
        verify(targetCheckoutFacade, times(1)).removeVoucher(voucherCode);
    }

    @Test
    public void getBillingCountries() {
        final List<CountryData> countries = new ArrayList<>();
        final CountryData australia = new CountryData();
        australia.setName("Australia");
        australia.setIsocode("AU");
        final CountryData newZealand = new CountryData();
        newZealand.setName("New Zealand");
        newZealand.setIsocode("NZ");
        countries.add(australia);
        countries.add(newZealand);

        given(targetCheckoutFacade.getBillingCountries()).willReturn(countries);
        final Response response = facade.getBillingCountries();

        assertThat(response.isSuccess());
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData() instanceof BillingCountryResponseData).isTrue();
        final BillingCountryResponseData billingCountryResponseData = (BillingCountryResponseData)response.getData();
        assertThat(billingCountryResponseData).isNotNull();
        assertThat(billingCountryResponseData.getCountries()).isNotNull();
        assertThat(billingCountryResponseData.getCountries()).isNotEmpty();

        for (final CountryData countryData : billingCountryResponseData.getCountries()) {
            switch (countryData.getIsocode()) {
                case "AU":
                    assertThat(countryData.getName().equals("Australia"));
                    break;
                case "NZ":
                    assertThat(countryData.getName().equals("New Zealand"));
                    break;
                default:
                    Fail.fail();
            }
        }

    }

    @Test
    public void testCreateBillingAddressWhenAddressNotCreatedProperly() {
        final TargetAddressData newAddressData = mock(TargetAddressData.class);
        willDoNothing().given(targetCheckoutFacade).createBillingAddress(newAddressData);
        given(newAddressData.getId()).willReturn(null);

        final Response response = facade.createBillingAddress(newAddressData);
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_VALIDATION");
        assertThat(response.getData().getError().getMessage()).isEqualTo("Invalid post data");
    }

    @Test
    public void testCreateBillingAddressWhenAddressCreatedProperly() {
        final TargetAddressData newAddressData = mock(TargetAddressData.class);
        willDoNothing().given(targetCheckoutFacade).createBillingAddress(newAddressData);
        given(newAddressData.getId()).willReturn("123465");

        final Response response = facade.createBillingAddress(newAddressData);
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData().getError()).isNull();
        assertThat(response.getData() instanceof CartDetailResponseData).isTrue();
        assertThat(((CartDetailResponseData)response.getData()).getBillingAddress()).isNotNull();
        assertThat(((CartDetailResponseData)response.getData()).getBillingAddress()).isEqualTo(newAddressData);
    }

    @Test
    public void testRemoveFlybuysWhenRemoveFlybuysFailed() {
        willReturn(Boolean.FALSE).given(targetCheckoutFacade).removeFlybuysNumber();
        final Response response = facade.removeFlybuys();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_CART_INVALID");
        assertThat(response.getData().getError().getMessage()).isEqualTo("Invalid cart data");
    }

    @Test
    public void testRemoveFlybuys() {
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).removeFlybuysNumber();
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(cartData);
        given(cartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        final Response response = facade.removeFlybuys();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData().getError()).isNull();
        assertThat(response.getData()).isNotNull();
    }

    @Test
    public void testSetBillingAddress() {
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(cartData);
        given(cartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).setBillingAddress("addressId");
        final Response response = facade.selectBillingAddress("addressId");
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData().getError()).isNull();
        assertThat(response.getData()).isNotNull();
    }

    @Test
    public void testGetIpgPaymentStatusWithNoGiftcardPayment() {
        given(targetCheckoutFacade.getCart()).willReturn(cart);
        given(targetPlaceOrderFacade.getGiftCardPayments(cart)).willReturn(Collections.EMPTY_LIST);
        final Response response = facade.getIpgPaymentStatus();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData().getError()).isNull();
        final PaymentResponseData responseData = (PaymentResponseData)response.getData();
        assertThat(responseData.getHasPartialPayment()).isFalse();
    }

    @Test
    public void testGetIpgPaymentStatusWithNoPayment() {
        given(targetCheckoutFacade.getCart()).willReturn(cart);
        given(targetPlaceOrderFacade.getGiftCardPayments(cart)).willReturn(null);
        final Response response = facade.getIpgPaymentStatus();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData().getError()).isNull();
        final PaymentResponseData responseData = (PaymentResponseData)response.getData();
        assertThat(responseData.getHasPartialPayment()).isFalse();
    }

    @Test
    public void testGetIpgPaymentStatusWithFailedGiftCardPayment() {
        final TargetCardResult giftcardPayment = mock(TargetCardResult.class);
        final TargetCardPaymentResult paymentResult = mock(TargetCardPaymentResult.class);
        given(giftcardPayment.getTargetCardPaymentResult()).willReturn(paymentResult);
        willReturn(Boolean.FALSE).given(paymentResult).isPaymentSuccessful();
        given(targetCheckoutFacade.getCart()).willReturn(cart);
        given(targetPlaceOrderFacade.getGiftCardPayments(cart)).willReturn(ImmutableList.of(giftcardPayment));
        final Response response = facade.getIpgPaymentStatus();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData().getError()).isNull();
        final PaymentResponseData responseData = (PaymentResponseData)response.getData();
        assertThat(responseData.getHasPartialPayment()).isFalse();
    }

    @Test
    public void testGetIpgPaymentStatusWithSuccessfulGiftCardPayment() {
        final TargetCardResult giftcardPayment = mock(TargetCardResult.class);
        final TargetCardPaymentResult paymentResult = mock(TargetCardPaymentResult.class);
        given(giftcardPayment.getTargetCardPaymentResult()).willReturn(paymentResult);
        willReturn(Boolean.TRUE).given(paymentResult).isPaymentSuccessful();
        given(targetCheckoutFacade.getCart()).willReturn(cart);
        given(targetPlaceOrderFacade.getGiftCardPayments(cart)).willReturn(ImmutableList.of(giftcardPayment));
        final Response response = facade.getIpgPaymentStatus();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData().getError()).isNull();
        final PaymentResponseData responseData = (PaymentResponseData)response.getData();
        assertThat(responseData.getHasPartialPayment()).isTrue();
    }

    @Test
    public void testGetIpgPaymentStatusWithCart() {
        given(targetCheckoutFacade.getCart()).willReturn(null);
        final Response response = facade.getIpgPaymentStatus();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData().getError()).isNull();
        final PaymentResponseData responseData = (PaymentResponseData)response.getData();
        assertThat(responseData.getHasPartialPayment()).isFalse();
    }

    @Test
    public void testLoginFlybuysPassed() {
        final TargetFlybuysLoginData flybuysLoginData = mock(TargetFlybuysLoginData.class);
        final FlybuysRequestStatusData statusData = mock(FlybuysRequestStatusData.class);
        given(statusData.getResponse()).willReturn(FlybuysResponseType.SUCCESS);
        given(flybuysDiscountFacade.getFlybuysRedemptionData(Mockito.anyString(), Mockito.any(Date.class),
                Mockito.anyString())).willReturn(statusData);
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(cartData);
        given(cartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        final Response response = facade.loginFlybuys(flybuysLoginData);
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData().getError()).isNull();
    }

    @Test
    public void testLoginFlybuysFailed() {
        final TargetFlybuysLoginData flybuysLoginData = mock(TargetFlybuysLoginData.class);
        final FlybuysRequestStatusData statusData = mock(FlybuysRequestStatusData.class);
        given(statusData.getResponse()).willReturn(FlybuysResponseType.INVALID);
        given(flybuysDiscountFacade.getFlybuysRedemptionData(Mockito.anyString(), Mockito.any(Date.class),
                Mockito.anyString())).willReturn(statusData);
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(cartData);
        given(cartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        final Response response = facade.loginFlybuys(flybuysLoginData);
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_FLYBUYS_INVALID");
    }

    @Test
    public void testRedeemFlybuysWithRemovePassed() {
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(cartData);
        given(cartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        willReturn(Boolean.TRUE).given(flybuysDiscountFacade).removeFlybuysDiscountFromCheckoutCart();
        final String redeemCode = "remove";
        final Response response = facade.redeemFlybuys(redeemCode);
        verify(flybuysDiscountFacade, Mockito.times(0)).applyFlybuysDiscountToCheckoutCart(redeemCode);
        verify(flybuysDiscountFacade).removeFlybuysDiscountFromCheckoutCart();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData().getError()).isNull();
    }

    @Test
    public void testRedeemFlybuysWithRemoveFailed() {
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(cartData);
        given(cartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        willReturn(Boolean.FALSE).given(flybuysDiscountFacade).removeFlybuysDiscountFromCheckoutCart();
        final String redeemCode = "remove";
        final Response response = facade.redeemFlybuys(redeemCode);
        verify(flybuysDiscountFacade, Mockito.times(0)).applyFlybuysDiscountToCheckoutCart(redeemCode);
        verify(flybuysDiscountFacade).removeFlybuysDiscountFromCheckoutCart();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_FLYBUYS_REDEMPTION");
    }

    @Test
    public void testRedeemFlybuysPassed() {
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(cartData);
        given(cartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        final String redeemCode = "1234";
        willReturn(Boolean.TRUE).given(flybuysDiscountFacade).applyFlybuysDiscountToCheckoutCart(redeemCode);
        final Response response = facade.redeemFlybuys(redeemCode);
        verify(flybuysDiscountFacade).applyFlybuysDiscountToCheckoutCart(redeemCode);
        verify(flybuysDiscountFacade, Mockito.times(0)).removeFlybuysDiscountFromCheckoutCart();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData().getError()).isNull();
    }

    @Test
    public void testRedeemFlybuysWithFailed() {
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(cartData);
        given(cartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        final String redeemCode = "1234";
        willReturn(Boolean.FALSE).given(flybuysDiscountFacade).applyFlybuysDiscountToCheckoutCart(redeemCode);
        final Response response = facade.redeemFlybuys(redeemCode);
        verify(flybuysDiscountFacade).applyFlybuysDiscountToCheckoutCart(redeemCode);
        verify(flybuysDiscountFacade, Mockito.times(0)).removeFlybuysDiscountFromCheckoutCart();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_FLYBUYS_REDEMPTION");
    }

    @Test
    public void testShowFlybuysRedemptionOption() {
        given(targetCheckoutFacade.getCheckoutCart()).willReturn(cartData);
        given(cartData.getEntries()).willReturn(Collections.singletonList(new OrderEntryData()));
        final Response response = facade.showFlybuysRedemptionOption();
        final CartDetailResponseData responseData = (CartDetailResponseData)response.getData();
        verify(cartDetailPopulator).populate(cartData, responseData, ImmutableList.of(CartDetailOption.FLYBUYS));
    }

    @Test
    public void testGetOrderDetail() {
        final AdjustedCartEntriesData sohUpdates = mock(AdjustedCartEntriesData.class);
        final TargetOrderData targetOrderData = mock(TargetOrderData.class);
        given(targetOrderFacade.getOrderDetailsForCode("1234")).willReturn(targetOrderData);
        final ArgumentCaptor<OrderDetailGaData> orderDetailGaDataCaptor = ArgumentCaptor
                .forClass(OrderDetailGaData.class);
        final ArgumentCaptor<TargetOrderData> targetOrderDataCaptor = ArgumentCaptor
                .forClass(TargetOrderData.class);
        given(targetOrderData.getEmail()).willReturn("customer@company.com");
        given(targetOrderData.getStatus()).willReturn(OrderStatus.CREATED);

        final Response response = facade.getOrderDetail("1234", sohUpdates);

        assertThat(response).isNotNull();
        assertThat(response.getData()).isInstanceOf(OrderDetailResponseData.class);
        verify(orderDetailGaPopulator).populate(targetOrderDataCaptor.capture(), orderDetailGaDataCaptor.capture());
        assertThat(targetOrderDataCaptor.getValue()).isEqualTo(targetOrderData);
        assertThat(((OrderDetailResponseData)response.getData()).getOrderDetailGaData())
                .isEqualTo(orderDetailGaDataCaptor.getValue());
        assertThat(((OrderDetailResponseData)response.getData()).getSohUpdates()).isEqualTo(sohUpdates);
        assertThat(((OrderDetailResponseData)response.getData()).getOrderDetails().getStatus())
                .isEqualTo(OrderStatus.CREATED.getCode());
        assertThat(((OrderDetailResponseData)response.getData()).getOrderDetails().getEmail())
                .isEqualTo("customer@company.com");
        assertThat(((OrderDetailResponseData)response.getData()).getOrderDetails().getTotalPrice())
                .isEqualTo(targetOrderData.getTotalPrice());
    }

    @Test
    public void testRegister() throws DuplicateUidException {
        final TargetOrderData targetOrderData = mock(TargetOrderData.class);
        given(targetOrderFacade.getOrderDetailsForCode("1234")).willReturn(targetOrderData);
        given(targetOrderData.getEmail()).willReturn("customer@company.com");
        given(targetOrderData.getDeliveryAddress()).willReturn(addressData);
        given(addressData.getFirstName()).willReturn("first");
        given(addressData.getLastName()).willReturn("last");
        given(addressData.getTitle()).willReturn("title");
        given(addressData.getTitleCode()).willReturn("tCode");
        given(addressData.getPhone()).willReturn("phone");
        final ArgumentCaptor<TargetRegisterData> registerDataCaptor = ArgumentCaptor.forClass(TargetRegisterData.class);

        final Response response = facade.registerCustomer("1234", "password");
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(RegisterUserResponseData.class);
        assertThat(((RegisterUserResponseData)response.getData()).getEmail()).isEqualTo("customer@company.com");
        verify(targetCustomerFacade).register(registerDataCaptor.capture());
        final TargetRegisterData registerData = registerDataCaptor.getValue();
        assertThat(registerData.getFirstName()).isEqualTo("first");
        assertThat(registerData.getLastName()).isEqualTo("last");
        assertThat(registerData.getTitleCode()).isEqualTo("tCode");
        assertThat(registerData.getMobileNumber()).isEqualTo("phone");
        assertThat(registerData.getPassword()).isEqualTo("password");
    }

    @Test
    public void testRegisterWithBlacklistedPassword() throws DuplicateUidException {
        final TargetOrderData targetOrderData = mock(TargetOrderData.class);
        given(targetOrderFacade.getOrderDetailsForCode("1234")).willReturn(targetOrderData);
        willReturn(Boolean.TRUE).given(targetCustomerFacade).isPasswordBlacklisted("password");

        final Response response = facade.registerCustomer("1234", "password");
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode())
                .isEqualTo(TgtFacadesConstants.WebServiceError.ERR_LOGIN_REGISTER_WEAK_PASSWORD);
        assertThat(response.getData().getError().getMessage()).isEqualTo("Supplied password is too weak");
    }

    @Test
    public void testRegisterWithDuplicateId() throws DuplicateUidException {
        final TargetOrderData targetOrderData = mock(TargetOrderData.class);
        given(targetOrderFacade.getOrderDetailsForCode("1234")).willReturn(targetOrderData);
        given(targetOrderData.getEmail()).willReturn("customer@company.com");
        given(targetOrderData.getDeliveryAddress()).willReturn(addressData);
        given(addressData.getFirstName()).willReturn("first");
        given(addressData.getLastName()).willReturn("last");
        given(addressData.getTitle()).willReturn("title");
        given(addressData.getTitleCode()).willReturn("tCode");
        given(addressData.getPhone()).willReturn("phone");
        willThrow(new DuplicateUidException()).given(targetCustomerFacade)
                .register(Mockito.any(TargetRegisterData.class));

        final Response response = facade.registerCustomer("1234", "password");
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode())
                .isEqualTo(TgtFacadesConstants.WebServiceError.ERR_LOGIN_REGISTER_USER_EXISTS);
        assertThat(response.getData().getError().getMessage()).isEqualTo("A user with this ID already exists.");
    }

    @Test
    public void testCheckoutProblemNoCart() {
        given(targetCheckoutFacade.getCart()).willReturn(null);
        final Response response = facade.checkoutProblem();

        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("THANKYOU_CHECK_PAYMENT_RECEIVED");
        verify(targetCustomerFacade).invalidateUserSession();
    }

    @Test
    public void testCheckoutProblemCartWithZeroTotal() {
        given(cart.getTotalPrice()).willReturn(Double.valueOf(0D));
        final Response response = facade.checkoutProblem();

        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("THANKYOU_CHECK_PAYMENT_RECEIVED");
        verify(targetCustomerFacade).invalidateUserSession();
    }

    @Test
    public void testCheckoutProblemCartWithPaidValueMatchesCartTotal() {
        given(cart.getTotalPrice()).willReturn(Double.valueOf(10D));
        given(targetPlaceOrderFacade.getAmountPaidSoFar(cart)).willReturn(Double.valueOf(10D));
        final Response response = facade.checkoutProblem();

        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("THANKYOU_CHECK_PAYMENT_RECEIVED");
        verify(targetCustomerFacade).invalidateUserSession();
    }

    @Test
    public void testCheckoutProblemCartWithPaidValueNotMatchesCartTotal() {
        given(cart.getTotalPrice()).willReturn(Double.valueOf(10D));
        given(targetPlaceOrderFacade.getAmountPaidSoFar(cart)).willReturn(Double.valueOf(9D));
        final Response response = facade.checkoutProblem();

        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("THANKYOU_CHECK_PAYMENT_ERROR");
        verify(targetCustomerFacade).invalidateUserSession();
    }

    private List<TargetZoneDeliveryModeData> getApplicableDeliveryModes() {
        final List<TargetZoneDeliveryModeData> deliveryModes = new ArrayList<>();
        deliveryModes.add(getClickAndCollectDeliveryMode());
        deliveryModes.add(getExpressDeliveryMode());
        deliveryModes.add(getHomeDeliveryMode());

        return deliveryModes;
    }

    private TargetZoneDeliveryModeData getClickAndCollectDeliveryMode() {
        final TargetZoneDeliveryModeData deliveryMode = new TargetZoneDeliveryModeData();
        deliveryMode.setDescription(ClickAndCollectDeliveryMode.DESCRIPTION);
        deliveryMode.setName(ClickAndCollectDeliveryMode.NAME);
        deliveryMode.setShortDescription(ClickAndCollectDeliveryMode.SHORT_DESCRIPTION);
        deliveryMode.setLongDescription(ClickAndCollectDeliveryMode.LONG_DESCRIPTION);
        deliveryMode.setDeliveryToStore(ClickAndCollectDeliveryMode.IS_DELIVERY_TO_STORE);
        deliveryMode.setDisclaimer(ClickAndCollectDeliveryMode.DISCLAIMER);
        deliveryMode.setCode(ClickAndCollectDeliveryMode.CODE);
        final PriceData deliveryCost = new PriceData();
        deliveryCost.setValue(ClickAndCollectDeliveryMode.DeliveryCost.VALUE);
        deliveryCost.setFormattedValue(ClickAndCollectDeliveryMode.DeliveryCost.FORMATTED_VALUE);
        deliveryMode.setDeliveryCost(deliveryCost);

        return deliveryMode;
    }

    private TargetZoneDeliveryModeData getHomeDeliveryMode() {
        final TargetZoneDeliveryModeData deliveryMode = new TargetZoneDeliveryModeData();
        deliveryMode.setDescription(HomeDeliveryMode.DESCRIPTION);
        deliveryMode.setName(HomeDeliveryMode.NAME);
        deliveryMode.setShortDescription(HomeDeliveryMode.SHORT_DESCRIPTION);
        deliveryMode.setLongDescription(HomeDeliveryMode.LONG_DESCRIPTION);
        deliveryMode.setDeliveryToStore(HomeDeliveryMode.IS_DELIVERY_TO_STORE);
        deliveryMode.setDisclaimer(HomeDeliveryMode.DISCLAIMER);
        deliveryMode.setCode(HomeDeliveryMode.CODE);
        final PriceData deliveryCost = new PriceData();
        deliveryCost.setValue(HomeDeliveryMode.DeliveryCost.VALUE);
        deliveryCost.setFormattedValue(HomeDeliveryMode.DeliveryCost.FORMATTED_VALUE);
        deliveryMode.setDeliveryCost(deliveryCost);

        return deliveryMode;
    }

    private TargetZoneDeliveryModeData getExpressDeliveryMode() {
        final TargetZoneDeliveryModeData deliveryMode = new TargetZoneDeliveryModeData();
        deliveryMode.setDescription(ExpressDeliveryMode.DESCRIPTION);
        deliveryMode.setName(ExpressDeliveryMode.NAME);
        deliveryMode.setShortDescription(ExpressDeliveryMode.SHORT_DESCRIPTION);
        deliveryMode.setLongDescription(ExpressDeliveryMode.LONG_DESCRIPTION);
        deliveryMode.setDeliveryToStore(ExpressDeliveryMode.IS_DELIVERY_TO_STORE);
        deliveryMode.setDisclaimer(ExpressDeliveryMode.DISCLAIMER);
        deliveryMode.setCode(ExpressDeliveryMode.CODE);
        final PriceData deliveryCost = new PriceData();
        deliveryCost.setValue(ExpressDeliveryMode.DeliveryCost.VALUE);
        deliveryCost.setFormattedValue(ExpressDeliveryMode.DeliveryCost.FORMATTED_VALUE);
        deliveryMode.setDeliveryCost(deliveryCost);

        return deliveryMode;
    }

    private PaymentMethodData getPaymentMethodData(final String paymentMethod,
            final List<PaymentMethodData> paymentMethods) {
        for (final PaymentMethodData paymentMode : paymentMethods) {
            if (paymentMode.getName().equals(paymentMethod)) {
                return paymentMode;
            }
        }
        return null;
    }

    @Test
    public void testVerifyDeliveryClubSuccessResponse() throws Exception {
        final String email = "any.email";
        given(targetCustomerFacade.getCartUserEmail()).willReturn(email);
        given(cartService.getSessionCart()).willReturn(cart);
        final DeliveryModeModel deliveryModeMock = mock(DeliveryModeModel.class);
        given(cart.getDeliveryMode()).willReturn(deliveryModeMock);
        willReturn(Boolean.TRUE).given(shipsterClientService).verifyEmail(email);
        final Response response = facade.verifyShipsterStatus();
        assertThat(response.isSuccess()).isTrue();
        final ShipsterEmailVerificationResponseData data = (ShipsterEmailVerificationResponseData)response
                .getData();
        assertThat(data.isDeliveryClubMember()).isTrue();
        verify(cart).setAusPostClubMember(Boolean.FALSE);
        verify(cart).setAusPostDeliveryClubFreeDelivery(Boolean.FALSE);
        verify(cart).setAusPostClubMember(Boolean.TRUE);
        verify(calculationService).calculateTotals(cart, true);

    }

    @Test
    public void testVerifyDeliveryClubFailureResponse() throws Exception {
        final String email = "any.email";
        given(targetCustomerFacade.getCartUserEmail()).willReturn(email);
        given(cartService.getSessionCart()).willReturn(cart);
        given(cart.getDeliveryMode()).willReturn(null);
        willThrow(new AuspostShipsterClientException("testException")).given(shipsterClientService)
                .verifyEmail(email);
        final Response response = facade.verifyShipsterStatus();
        assertThat(response.isSuccess()).isFalse();
        final BaseResponseData data = response.getData();
        assertThat(data.getError().getCode()).isEqualTo("UNABLE_TO_VERIFY");
        verify(cart).setAusPostClubMember(Boolean.FALSE);
        verify(cart).setAusPostDeliveryClubFreeDelivery(Boolean.FALSE);
        verify(cart).setAusPostClubMember(Boolean.FALSE);
        verifyZeroInteractions(calculationService);
    }


    /**
     * Method to verify All payment options availability if the cart contains pre-order item.
     */
    @Test
    public void getApplicablePaymentMethodsAndVerifyAllPaymentMethodAvailability() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isIpgEnabled();
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isGiftCardPaymentEnabled();
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isPaypalEnabled();
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isAfterpayEnabled();
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).isCartHavingPreOrderItem();

        final Response response = facade.getApplicablePaymentMethods();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();

        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        final PaymentMethodData paypal = getPaymentMethodData(TgtFacadesConstants.PAYPAL, paymentMethods);
        final PaymentMethodData ipg = getPaymentMethodData(TgtFacadesConstants.IPG, paymentMethods);
        final PaymentMethodData giftCard = getPaymentMethodData(TgtFacadesConstants.GIFT_CARD, paymentMethods);
        final PaymentMethodData afterpay = getPaymentMethodData(TgtFacadesConstants.AFTERPAY, paymentMethods);

        assertThat(paypal).isNotNull();
        assertThat(paypal.isAvailable()).isFalse();
        assertThat(paypal.getReason()).isEqualTo(REASON);

        assertThat(ipg).isNotNull();
        assertThat(ipg.isAvailable()).isTrue();

        assertThat(giftCard).isNotNull();
        assertThat(giftCard.isAvailable()).isFalse();
        assertThat(giftCard.getReason()).isEqualTo(REASON);


        assertThat(afterpay).isNotNull();
        assertThat(afterpay.isAvailable()).isFalse();
        assertThat(afterpay.getReason()).isEqualTo("Available on orders $50 to $1,000");

    }

    @Test
    public void testApplyVoucherWhenPreOrderProductInCart() {
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).isCartHavingPreOrderItem();
        final Response response = facade.applyVoucher("Any Voucher");
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_VOUCHER_NOT_AVAILABLE_FOR_PREORDER");
        assertThat(response.getData().getError().getMessage()).isEqualTo("Promo Code not available for pre-orders.");
    }

    @Test
    public void testRedeemFlybuysWhenPreOrderProductInCart() {
        willReturn(Boolean.TRUE).given(targetCheckoutFacade).isCartHavingPreOrderItem();
        final Response response = facade.redeemFlybuys("any redeem code");
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isNotNull();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode())
                .isEqualTo("ERR_FLYBUYS_REDEMPTION_NOT_AVAILABLE_FOR_PREORDER");
        assertThat(response.getData().getError().getMessage())
                .isEqualTo("Flybuys redemption not available for pre-orders.");
    }

    @Test
    public void getApplicablePaymentMethodsWhenZipInactive() throws Exception {
        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isZipEnabled();
        final Response response = facade.getApplicablePaymentMethods();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        assertThat(paymentMethods).hasSize(0);
        final PaymentMethodData zip = getPaymentMethodData(TgtFacadesConstants.ZIP, paymentMethods);
        assertThat(zip).isNull();
    }

    @Test
    public void getApplicablePaymentMethodsWhenzipActive() throws Exception {
        final Double cartTotal = Double.valueOf(100d);
        given(targetCheckoutFacade.getCart()).willReturn(cart);
        given(cart.getTotalPrice()).willReturn(cartTotal);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isZipEnabled();
        given(Boolean.valueOf(targetCommerceCartService.isExcludedForZip(cart))).willReturn(Boolean.FALSE);
        willReturn(Boolean.TRUE).given(targetPaymentService).ping(TgtpaymentConstants.ZIPPAY_PAYMENT_TYPE);
        final Response response = facade.getApplicablePaymentMethods();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        assertThat(paymentMethods).hasSize(1);
        final PaymentMethodData zip = getPaymentMethodData(TgtFacadesConstants.ZIP, paymentMethods);
        assertThat(zip).isNotNull();
        assertThat(zip.isAvailable()).isTrue();
    }

    @Test
    public void getApplicablePaymentMethodsWhenzipActivePingFailure() throws Exception {
        final Double cartTotal = Double.valueOf(100d);
        given(targetCheckoutFacade.getCart()).willReturn(cart);
        given(cart.getTotalPrice()).willReturn(cartTotal);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isZipEnabled();
        given(Boolean.valueOf(targetCommerceCartService.isExcludedForZip(cart))).willReturn(Boolean.FALSE);
        willReturn(Boolean.FALSE).given(targetPaymentService).ping(TgtpaymentConstants.ZIPPAY_PAYMENT_TYPE);
        final Response response = facade.getApplicablePaymentMethods();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        assertThat(paymentMethods).hasSize(1);
        final PaymentMethodData zip = getPaymentMethodData(TgtFacadesConstants.ZIP, paymentMethods);
        assertThat(zip).isNotNull();
        assertThat(zip.isAvailable()).isFalse();
        assertThat(zip.isRetryRequired()).isTrue();
    }

    @Test
    public void getApplicablePaymentMethodsWhenzipActiveAndCartContainsExcludedItem() throws Exception {
        final Double cartTotal = Double.valueOf(20d);
        given(targetCheckoutFacade.getCart()).willReturn(cart);
        given(cart.getTotalPrice()).willReturn(cartTotal);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isZipEnabled();
        given(Boolean.valueOf(targetCommerceCartService.isExcludedForZip(cart))).willReturn(Boolean.TRUE);
        final Response response = facade.getApplicablePaymentMethods();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        assertThat(paymentMethods).hasSize(1);
        final PaymentMethodData zip = getPaymentMethodData(TgtFacadesConstants.ZIP, paymentMethods);
        assertThat(zip).isNotNull();
        assertThat(zip.isAvailable()).isFalse();
        assertThat(zip.getReason()).isEqualTo("Items in your basket prevent this payment option.");
    }

}