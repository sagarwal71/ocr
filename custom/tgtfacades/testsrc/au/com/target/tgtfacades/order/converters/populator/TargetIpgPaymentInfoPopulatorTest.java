/**
 * 
 */
package au.com.target.tgtfacades.order.converters.populator;

import static java.lang.Boolean.TRUE;
import static java.lang.Boolean.valueOf;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CardTypeData;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.user.data.IpgPaymentInfoData;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;


/**
 * Unit tests for TargetIpgPaymentInfoPopulator
 * 
 * @author jjayawa1
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetIpgPaymentInfoPopulatorTest {
    @Mock
    private Converter<CreditCardType, CardTypeData> mockCardTypeConverter;

    @Mock
    private Converter<AddressModel, TargetAddressData> addressConverter;

    @InjectMocks
    private final TargetIpgPaymentInfoPopulator populator = new TargetIpgPaymentInfoPopulator();

    @Test
    public void testPopulate() {
        final IpgPaymentInfoModel ipgPaymentInfoModel = Mockito.mock(IpgPaymentInfoModel.class);
        final PK pk = PK.parse("123");

        final AddressModel billingAddressModel = Mockito.mock(AddressModel.class);
        Mockito.when(billingAddressModel.getLine1()).thenReturn("12, Thompson Road");
        Mockito.when(billingAddressModel.getTown()).thenReturn("North Geelong");

        final TargetAddressData billingAddressData = Mockito.mock(TargetAddressData.class);
        Mockito.when(billingAddressData.getLine1()).thenReturn("12, Thompson Road");
        Mockito.when(billingAddressData.getTown()).thenReturn("North Geelong");

        Mockito.when(addressConverter.convert(billingAddressModel)).thenReturn(billingAddressData);

        Mockito.when(ipgPaymentInfoModel.getPk()).thenReturn(pk);
        Mockito.when(ipgPaymentInfoModel.getToken()).thenReturn("token");
        Mockito.when(ipgPaymentInfoModel.getBillingAddress()).thenReturn(billingAddressModel);
        Mockito.when(ipgPaymentInfoModel.getIpgPaymentTemplateType()).thenReturn(
                IpgPaymentTemplateType.CREDITCARDSINGLE);
        populator.setAddressConverter(addressConverter);

        final CardTypeData cardTypeData = new CardTypeData();
        cardTypeData.setCode("master");
        given(mockCardTypeConverter.convert(CreditCardType.MASTER)).willReturn(cardTypeData);

        final TargetCCPaymentInfoData paymentInfoData = new TargetCCPaymentInfoData();
        populator.populate(ipgPaymentInfoModel, paymentInfoData);
        Assert.assertNotNull(paymentInfoData);

        final IpgPaymentInfoData ipgInfoData = paymentInfoData.getIpgPaymentInfoData();
        Assert.assertNotNull(ipgInfoData);

        Assert.assertEquals(TRUE, valueOf(paymentInfoData.isIpgPaymentInfo()));

        Assert.assertNotNull(paymentInfoData.getBillingAddress());
        Assert.assertEquals("12, Thompson Road", paymentInfoData.getBillingAddress().getLine1());
        Assert.assertEquals("North Geelong", paymentInfoData.getBillingAddress().getTown());

        Assert.assertEquals(IpgPaymentTemplateType.CREDITCARDSINGLE, ipgInfoData.getIpgPaymentTemplateType());
        Assert.assertEquals("token", ipgInfoData.getToken());
        Assert.assertTrue(paymentInfoData.isIpgPaymentInfo());

    }
}