/**
 *
 */
package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.order.CartService;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import org.junit.Assert;


/**
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetZoneDeliveryModePopulatorTest {
    @InjectMocks
    private final TargetZoneDeliveryModePopulator populator = new TargetZoneDeliveryModePopulator();

    @Mock
    private TargetDeliveryService targetDeliveryService;
    @Mock
    private CartModel cartModel;
    @Mock
    private CartService cartService;

    @Test
    public void testConvertStoreDelivery() {
        final TargetZoneDeliveryModeModel source = Mockito.mock(TargetZoneDeliveryModeModel.class);
        BDDMockito.given(source.getIsDeliveryToStore()).willReturn(Boolean.TRUE);

        final List<AbstractTargetZoneDeliveryModeValueModel> valueModelList = null;
        BDDMockito.given(cartService.getSessionCart()).willReturn(cartModel);
        BDDMockito.given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(source, cartModel))
                .willReturn(valueModelList);
        final TargetZoneDeliveryModeData target = new TargetZoneDeliveryModeData();
        populator.populate(source, target);

        Assert.assertTrue(target.isDeliveryToStore());
    }

    @Test
    public void testConvertNotStoreDelivery() {
        final TargetZoneDeliveryModeModel source = Mockito.mock(TargetZoneDeliveryModeModel.class);
        BDDMockito.given(source.getIsDeliveryToStore()).willReturn(Boolean.FALSE);

        final List<AbstractTargetZoneDeliveryModeValueModel> valueModelList = null;
        BDDMockito.given(cartService.getSessionCart()).willReturn(cartModel);
        BDDMockito.given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(source, cartModel))
                .willReturn(valueModelList);
        final TargetZoneDeliveryModeData target = new TargetZoneDeliveryModeData();
        populator.populate(source, target);

        Assert.assertFalse(target.isDeliveryToStore());
    }

    @Test
    public void testConvertNullStoreDelivery() {
        final TargetZoneDeliveryModeModel source = Mockito.mock(TargetZoneDeliveryModeModel.class);

        final TargetZoneDeliveryModeData target = new TargetZoneDeliveryModeData();
        final List<AbstractTargetZoneDeliveryModeValueModel> valueModelList = null;
        BDDMockito.given(cartService.getSessionCart()).willReturn(cartModel);
        BDDMockito.given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(source, cartModel))
                .willReturn(valueModelList);

        populator.populate(source, target);

        Assert.assertFalse(target.isDeliveryToStore());
    }

    @Test
    public void testConvertNotTargetSource() {
        final TargetZoneDeliveryModeModel source = Mockito.mock(TargetZoneDeliveryModeModel.class);

        final List<AbstractTargetZoneDeliveryModeValueModel> valueModelList = null;
        BDDMockito.given(cartService.getSessionCart()).willReturn(cartModel);
        BDDMockito.given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(null, cartModel))
                .willReturn(valueModelList);
        final TargetZoneDeliveryModeData target = new TargetZoneDeliveryModeData();
        populator.populate(source, target);

        Assert.assertFalse(target.isDeliveryToStore());
    }

}
