/**
 *
 */
package au.com.target.tgtfacades.order.converters.populator;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CardTypeData;
import de.hybris.platform.commerceservices.util.ConverterFactory;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtpayment.model.PaypalHerePaymentInfoModel;


/**
 * Unit tests for TargetPinPadPaymentInfoConverter
 *
 * @author jjayawa1
 *
 */
@SuppressWarnings("deprecation")
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPayPalHerePaymentInfoPopulatorTest {
    @Mock
    private Converter<CreditCardType, CardTypeData> mockCardTypeConverter;

    @Mock
    private Converter<AddressModel, TargetAddressData> addressConverter;

    @InjectMocks
    private final TargetPayPalHerePaymentInfoPopulator populator = new TargetPayPalHerePaymentInfoPopulator();

    private AbstractPopulatingConverter<PaypalHerePaymentInfoModel, TargetCCPaymentInfoData> converter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        populator.setAddressConverter(addressConverter);

        converter = new ConverterFactory<PaypalHerePaymentInfoModel, TargetCCPaymentInfoData, TargetPayPalHerePaymentInfoPopulator>()
                .create(TargetCCPaymentInfoData.class, populator);
    }

    @Test
    public void testPopulate() {
        final PaypalHerePaymentInfoModel payPalHerePaymentInfoModel = Mockito.mock(PaypalHerePaymentInfoModel.class);
        final PK pk = PK.parse("123");

        final AddressModel billingAddressModel = Mockito.mock(AddressModel.class);
        Mockito.when(billingAddressModel.getLine1()).thenReturn("12, Thompson Road");
        Mockito.when(billingAddressModel.getTown()).thenReturn("North Geelong");

        final TargetAddressData billingAddressData = Mockito.mock(TargetAddressData.class);
        Mockito.when(billingAddressData.getLine1()).thenReturn("12, Thompson Road");
        Mockito.when(billingAddressData.getTown()).thenReturn("North Geelong");

        Mockito.when(addressConverter.convert(billingAddressModel)).thenReturn(billingAddressData);

        Mockito.when(payPalHerePaymentInfoModel.getPk()).thenReturn(pk);
        Mockito.when(payPalHerePaymentInfoModel.getBillingAddress()).thenReturn(billingAddressModel);
        populator.setAddressConverter(addressConverter);

        final CardTypeData cardTypeData = new CardTypeData();
        cardTypeData.setCode("master");
        given(mockCardTypeConverter.convert(CreditCardType.MASTER)).willReturn(cardTypeData);


        final TargetCCPaymentInfoData paymentInfoData = converter.convert(payPalHerePaymentInfoModel);
        Assert.assertNotNull(paymentInfoData);

        Assert.assertNull(paymentInfoData.getPinPadPaymentInfoData());
        Assert.assertNull(paymentInfoData.getPaypalInfoData());

        Assert.assertFalse(paymentInfoData.isPinPadPaymentInfo());

        Assert.assertNotNull(paymentInfoData.getBillingAddress());
        Assert.assertEquals("12, Thompson Road", paymentInfoData.getBillingAddress().getLine1());
        Assert.assertEquals("North Geelong", paymentInfoData.getBillingAddress().getTown());
    }
}