/**
 * 
 */
package au.com.target.tgtfacades.order.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.enums.UiExperienceLevel;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.request.SubscriptionDataRequest;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.customer.TargetCustomerAccountService;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtfacades.cart.TargetOrderErrorHandlerFacade;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.order.data.TargetPlaceOrderRequest;
import au.com.target.tgtfacades.order.data.TargetPlaceOrderResult;
import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderAfterpayPaymentInfoData;
import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderIPGPaymentInfoData;
import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderPayPalPaymentInfoData;
import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderZipPaymentInfoData;
import au.com.target.tgtfacades.order.enums.TargetPlaceOrderResultEnum;
import au.com.target.tgtfraud.order.ClientDetails;
import au.com.target.tgtfraud.order.TargetFraudCommerceCheckoutService;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpayment.commands.result.TargetGetSubscriptionResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.dto.LineItem;
import au.com.target.tgtpayment.exceptions.BadFundingMethodException;
import au.com.target.tgtpayment.methods.TargetAfterpayPaymentMethod;
import au.com.target.tgtpayment.methods.TargetIpgPaymentMethod;
import au.com.target.tgtpayment.methods.TargetPaymentMethod;
import au.com.target.tgtpayment.methods.impl.TargetPayPalPaymentMethodImpl;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.service.impl.TargetPaymentServiceImpl;
import au.com.target.tgtpayment.strategy.PaymentMethodStrategy;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


/**
 * @author rmcalave
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPlaceOrderFacadeImplTest {

    @InjectMocks
    @Spy
    private final TargetPlaceOrderFacadeImpl targetPlaceOrderFacadeImpl = new TargetPlaceOrderFacadeImpl();

    @Mock
    private TargetFraudCommerceCheckoutService mockTargetFraudCommerceCheckoutService;

    @Mock
    private TargetCommerceCartService targetCommerceCartService;

    @Mock
    private ModelService mockModelService;

    @Mock
    private TargetCheckoutFacade mockCheckoutFacade;

    @Mock
    private UserService mockUserService;

    @Mock
    private Converter<OrderModel, OrderData> mockOrderConverter;

    @Mock
    private CartService mockCartService;

    @Mock
    private UiExperienceService mockUiExperienceService;

    @Mock
    private CommerceOrderResult mockOrderResult;

    @Mock
    private CommerceCheckoutParameter mockCheckoutParameter;

    @Mock
    private TargetSalesApplicationService targetSalesApplicationService;

    @Mock
    private PaymentMethodStrategy paymentMethodStrategy;

    @Mock
    private TargetPaymentServiceImpl targetPaymentService;

    @Mock
    private TargetCustomerAccountService targetCustomerAccountService;

    @Mock
    private TargetCustomerModel customerModel;

    @Mock
    private CartModel cartModel;

    @Mock
    private PaymentInfoModel paymentInfoModel;

    @Mock
    private IpgPaymentInfoModel ipgPaymentInfoModel;

    @Mock
    private AfterpayPaymentInfoModel afterpayPaymentInfoModel;
    @Mock
    private SubscriptionDataRequest subscriptionDataRequest;

    @Mock
    private TargetPaymentMethod paymentMethod;

    @Mock
    private TargetIpgPaymentMethod ipgPaymentMethod;

    @Mock
    private TargetAfterpayPaymentMethod afterpayPaymentMethod;

    @Mock
    private TargetQueryTransactionDetailsResult queryResult;

    @Mock
    private FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    @Mock
    private PaymentTransactionModel paymentTransactionModel;

    @Mock
    private AddressModel addressModel;

    @Mock
    private PurchaseOptionModel purchaseOptionModel;

    @Mock
    private TargetCartData cartData;

    @Mock
    private TargetOrderErrorHandlerFacade targetOrderErrorHandlerFacade;

    @Mock
    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;

    @Mock
    private FlybuysDiscountFacade flybuysDiscountFacade;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Captor
    private ArgumentCaptor<CommerceCheckoutParameter> commerceCheckoutParameterCaptor;

    @Spy
    private final TargetPlaceOrderFacadeImpl targetPlaceOrderFacadeImplSpy = new TargetPlaceOrderFacadeImpl();

    @Before
    public void setUp() throws InvalidCartException {
        given(cartModel.getPaymentInfo()).willReturn(paymentInfoModel);
        given(cartModel.getUser()).willReturn(customerModel);
        given(cartModel.getPk()).willReturn(PK.BIG_PK);
        given(paymentMethodStrategy.getPaymentMethod(paymentInfoModel)).willReturn(paymentMethod);
        given(paymentMethodStrategy.getPaymentMethod(ipgPaymentInfoModel)).willReturn(ipgPaymentMethod);
        doReturn(Boolean.TRUE).when(queryResult).isSuccess();
        given(mockCheckoutFacade.getCart()).willReturn(cartModel);
        given(mockUserService.getCurrentUser()).willReturn(customerModel);
        given(targetPaymentService.createTransactionWithQueryResult(cartModel, queryResult)).willReturn(
                paymentTransactionModel);
        given(
                mockTargetFraudCommerceCheckoutService.placeOrder(commerceCheckoutParameterCaptor.capture()))
                        .willReturn(mockOrderResult);
    }

    @Test
    public void testBeforePlaceOrderWithIpgNoUser() throws InsufficientStockLevelException {
        when(cartModel.getPaymentInfo()).thenReturn(ipgPaymentInfoModel);
        given(cartModel.getUser()).willReturn(null);
        doReturn(Boolean.TRUE).when(queryResult).isSuccess();

        targetPlaceOrderFacadeImpl.beforePlaceOrder(cartModel);

        verifyZeroInteractions(targetCustomerAccountService);
    }

    @Test
    public void testBeforePlaceOrder() throws InsufficientStockLevelException {
        doReturn(Boolean.TRUE).when(mockTargetFraudCommerceCheckoutService).beforePlaceOrder(null,
                null);

        final TargetPlaceOrderResultEnum result = targetPlaceOrderFacadeImpl.beforePlaceOrder(null);

        assertThat(result).isNull();
    }

    @Test
    public void testBeforePlaceOrderWithFalseResult() throws InsufficientStockLevelException {
        doReturn(Boolean.FALSE).when(mockTargetFraudCommerceCheckoutService).beforePlaceOrder(cartModel,
                paymentTransactionModel);

        final TargetPlaceOrderResultEnum result = targetPlaceOrderFacadeImpl.beforePlaceOrder(null);

        assertThat(result).isEqualTo(TargetPlaceOrderResultEnum.PAYMENT_FAILURE);
    }

    @Test
    public void testBeforePlaceOrderWithInsufficientStock() throws InsufficientStockLevelException {
        doThrow(new InsufficientStockLevelException("Insufficient stock")).when(mockTargetFraudCommerceCheckoutService)
                .beforePlaceOrder(null, null);

        final TargetPlaceOrderResultEnum result = targetPlaceOrderFacadeImpl.beforePlaceOrder(null);

        verifyZeroInteractions(mockModelService);
        assertThat(result).isEqualTo(TargetPlaceOrderResultEnum.INSUFFICIENT_STOCK);
    }

    @Test
    public void testPlaceOrderModelWithNoExperienceLevel() throws InvalidCartException {
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);

        targetPlaceOrderFacadeImpl.placeOrder(cartModel);

        final CommerceCheckoutParameter capturedCommerceCheckoutParameter = commerceCheckoutParameterCaptor.getValue();
        assertThat(capturedCommerceCheckoutParameter.getSalesApplication()).isEqualTo(SalesApplication.WEB);
        assertThat(capturedCommerceCheckoutParameter.getCart()).isEqualTo(cartModel);
    }

    @Test
    public void testPlaceOrderModelWithDesktopExperienceLevel() throws InvalidCartException {
        given(mockUiExperienceService.getUiExperienceLevel()).willReturn(UiExperienceLevel.DESKTOP);
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);

        targetPlaceOrderFacadeImpl.placeOrder(cartModel);

        final CommerceCheckoutParameter capturedCommerceCheckoutParameter = commerceCheckoutParameterCaptor.getValue();
        assertThat(capturedCommerceCheckoutParameter.getSalesApplication()).isEqualTo(SalesApplication.WEB);
        assertThat(capturedCommerceCheckoutParameter.getCart()).isEqualTo(cartModel);
    }

    @Test
    public void testPlaceOrderModelWithMobileExperienceLevel() throws InvalidCartException {
        given(mockUiExperienceService.getUiExperienceLevel()).willReturn(UiExperienceLevel.MOBILE);
        when(targetSalesApplicationService.getCurrentSalesApplication()).thenReturn(SalesApplication.WEBMOBILE);

        targetPlaceOrderFacadeImpl.placeOrder(cartModel);

        final CommerceCheckoutParameter capturedCommerceCheckoutParameter = commerceCheckoutParameterCaptor.getValue();
        assertThat(capturedCommerceCheckoutParameter.getSalesApplication()).isEqualTo(SalesApplication.WEBMOBILE);
        assertThat(capturedCommerceCheckoutParameter.getCart()).isEqualTo(cartModel);
    }

    @Test
    public void testPlaceOrderWithNullOrder() {
        given(mockCheckoutFacade.getCart()).willReturn(null);
        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.placeOrder();

        assertThat(result).isNotNull();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.INVALID_CART);
        assertThat(result.getAbstractOrderModel()).isNull();
    }

    @Test
    public void testPlaceOrderWithUserMismatch() {
        final TargetCustomerModel mockCurrentUser = mock(TargetCustomerModel.class);
        final TargetCustomerModel mockOrderUser = mock(TargetCustomerModel.class);

        given(cartModel.getUser()).willReturn(mockOrderUser);

        given(mockCheckoutFacade.getCart()).willReturn(cartModel);
        given(mockUserService.getCurrentUser()).willReturn(mockCurrentUser);

        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.placeOrder();

        assertThat(result).isNotNull();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.USER_MISMATCH);
        assertThat(result.getAbstractOrderModel()).isNotNull();
    }

    @Test
    public void testPlaceOrderWithPaymentFailure() throws InsufficientStockLevelException {
        doReturn(Boolean.FALSE).when(mockTargetFraudCommerceCheckoutService).beforePlaceOrder(cartModel,
                paymentTransactionModel);

        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.placeOrder();

        assertThat(result).isNotNull();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.PAYMENT_FAILURE);
        assertThat(result.getAbstractOrderModel()).isNotNull();
    }

    @Test
    public void testPlaceOrderWithInsufficientStock() throws InsufficientStockLevelException {
        given(paymentMethodStrategy.getPaymentMethod(paymentInfoModel)).willReturn(ipgPaymentMethod);
        doThrow(new InsufficientStockLevelException("Insufficient stock")).when(mockTargetFraudCommerceCheckoutService)
                .beforePlaceOrder(cartModel, paymentTransactionModel);
        when(cartModel.getPaymentInfo()).thenReturn(ipgPaymentInfoModel);
        given(targetPaymentService.queryTransactionDetails(ipgPaymentMethod, ipgPaymentInfoModel))
                .willReturn(queryResult);

        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.placeOrder();
        verify(mockModelService).remove(paymentTransactionModel);

        assertThat(result).isNotNull();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.INSUFFICIENT_STOCK);
        assertThat(result.getAbstractOrderModel()).isNotNull();
    }

    @Test
    public void testPlaceOrderWithInvalidCart() throws InsufficientStockLevelException, InvalidCartException {
        final TargetPayPalPaymentMethodImpl paypalPaymentMethod = mock(TargetPayPalPaymentMethodImpl.class);
        given(paymentMethodStrategy.getPaymentMethod(paymentInfoModel)).willReturn(paypalPaymentMethod);
        doReturn(Boolean.TRUE).when(mockTargetFraudCommerceCheckoutService).beforePlaceOrder(cartModel,
                null);

        doReturn(Boolean.TRUE).when(mockTargetFraudCommerceCheckoutService).isCaptureOrderSuccessful(cartModel);
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        given(mockTargetFraudCommerceCheckoutService.placeOrder(commerceCheckoutParameterCaptor.capture()))
                .willThrow(new InvalidCartException("Invalid cart"));

        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.placeOrder();

        assertThat(result).isNotNull();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.INVALID_CART);
        assertThat(result.getAbstractOrderModel()).isNull();

        final CommerceCheckoutParameter capturedCommerceCheckoutParameter = commerceCheckoutParameterCaptor.getValue();
        assertThat(capturedCommerceCheckoutParameter.getSalesApplication()).isEqualTo(SalesApplication.WEB);
        assertThat(capturedCommerceCheckoutParameter.getCart()).isEqualTo(cartModel);
    }

    @Test
    public void testPlaceOrderWithNoReturnedOrder() throws InsufficientStockLevelException, InvalidCartException {
        doReturn(Boolean.TRUE).when(mockTargetFraudCommerceCheckoutService).beforePlaceOrder(cartModel,
                null);
        doReturn(Boolean.TRUE).when(mockTargetFraudCommerceCheckoutService).isCaptureOrderSuccessful(cartModel);

        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.placeOrder();

        assertThat(result).isNotNull();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.UNKNOWN);
        assertThat(result.getAbstractOrderModel()).isNull();
    }

    @Test
    public void testPlaceOrderAsAuthenticatedUser() throws InsufficientStockLevelException, InvalidCartException {
        doReturn(Boolean.TRUE).when(mockTargetFraudCommerceCheckoutService).beforePlaceOrder(cartModel,
                null);
        doReturn(Boolean.TRUE).when(mockTargetFraudCommerceCheckoutService).isCaptureOrderSuccessful(cartModel);
        final OrderModel mockOrder = mock(OrderModel.class);
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        given(mockOrderResult.getOrder()).willReturn(mockOrder);

        final OrderData mockOrderData = mock(OrderData.class);
        given(mockOrderConverter.convert(mockOrder)).willReturn(mockOrderData);

        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.placeOrder();

        assertThat(result).isNotNull();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.SUCCESS);
        assertThat(result.getAbstractOrderModel()).isEqualTo(mockOrder);

        verify(targetPlaceOrderFacadeImpl).afterPlaceOrder(mockOrder);
    }

    @Test
    public void testPlaceOrderAsGuest() throws InsufficientStockLevelException, InvalidCartException {
        given(customerModel.getType()).willReturn(CustomerType.GUEST);
        doReturn(Boolean.TRUE).when(mockTargetFraudCommerceCheckoutService).beforePlaceOrder(cartModel,
                null);
        doReturn(Boolean.TRUE).when(mockTargetFraudCommerceCheckoutService).isCaptureOrderSuccessful(cartModel);

        final OrderModel mockOrder = mock(OrderModel.class);
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        given(mockOrderResult.getOrder()).willReturn(mockOrder);

        final OrderData mockOrderData = mock(OrderData.class);
        given(mockOrderConverter.convert(mockOrder)).willReturn(mockOrderData);
        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.placeOrder();

        assertThat(result).isNotNull();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.SUCCESS);
        assertThat(result.getAbstractOrderModel()).isEqualTo(mockOrder);

        verify(targetPlaceOrderFacadeImpl).afterPlaceOrder(mockOrder);
    }

    @Test
    public void testAfterPlaceOrder() {
        final OrderModel mockOrderModel = mock(OrderModel.class);
        given(mockCheckoutFacade.getCart()).willReturn(cartModel);

        targetPlaceOrderFacadeImpl.afterPlaceOrder(mockOrderModel);

        verify(mockTargetFraudCommerceCheckoutService).afterPlaceOrder(cartModel, mockOrderModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testsetClientDetailsForFraudDetectionWithNullIPAddress() {
        targetPlaceOrderFacadeImpl.setClientDetailsForFraudDetection(null, "cookies");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testsetClientDetailsForFraudDetectionWithNullCookies() {
        targetPlaceOrderFacadeImpl.setClientDetailsForFraudDetection("ipAddress", null);
    }

    @Test
    public void testsetClientDetailsForFraudDetectionWithNoCheckoutCart() {
        given(mockCheckoutFacade.getCart()).willReturn(null);

        targetPlaceOrderFacadeImpl.setClientDetailsForFraudDetection("ipAddress", "cookies");

        verifyZeroInteractions(mockTargetFraudCommerceCheckoutService);
    }

    @Test
    public void testsetClientDetailsForFraudDetection() {
        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCheckoutFacade.getCart()).willReturn(mockCartModel);

        targetPlaceOrderFacadeImpl.setClientDetailsForFraudDetection("ipAddress", "cookies");

        final ArgumentCaptor<ClientDetails> argumentCaptor = ArgumentCaptor.forClass(ClientDetails.class);
        verify(mockTargetFraudCommerceCheckoutService).populateClientDetailsForFraudDetection(
                eq(mockCartModel), argumentCaptor.capture());

        final ClientDetails capturedClientDetails = argumentCaptor.getValue();
        assertThat(capturedClientDetails.getIpAddress()).isEqualTo("ipAddress");
        assertThat(capturedClientDetails.getBrowserCookies()).isEqualTo("cookies");
    }

    @Test
    public void testPrepareForPlaceOrderNullCart() {
        given(mockCheckoutFacade.getCart()).willReturn(null);

        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.prepareForPlaceOrder();

        assertThat(result).isNotNull();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.INVALID_CART);
        assertThat(result.getAbstractOrderModel()).isNull();
    }

    @Test
    public void testPrepareForPlaceOrderUserMisMatch() {
        final TargetCustomerModel customer1 = mock(TargetCustomerModel.class);
        final TargetCustomerModel customer2 = mock(TargetCustomerModel.class);

        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getUser()).willReturn(customer1);

        given(mockCheckoutFacade.getCart()).willReturn(mockCartModel);
        given(mockUserService.getCurrentUser()).willReturn(customer2);

        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.prepareForPlaceOrder();

        assertThat(result).isNotNull();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.USER_MISMATCH);
        assertThat(result.getAbstractOrderModel()).isNotNull();
    }

    @Test
    public void testPrepareForPlaceOrderInsufficientStock() throws InsufficientStockLevelException {
        final TargetCustomerModel customer = mock(TargetCustomerModel.class);

        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getUser()).willReturn(customer);

        given(mockCheckoutFacade.getCart()).willReturn(mockCartModel);
        given(mockUserService.getCurrentUser()).willReturn(customer);

        doThrow(new InsufficientStockLevelException("Insufficient stock")).when(mockTargetFraudCommerceCheckoutService)
                .beforePlaceOrder(mockCartModel,
                        null);

        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.prepareForPlaceOrder();

        assertThat(result).isNotNull();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.INSUFFICIENT_STOCK);
        assertThat(result.getAbstractOrderModel()).isNotNull();
    }

    @Test
    public void testPrepareForPlaceOrderPaymentFailure() throws InsufficientStockLevelException {
        final TargetCustomerModel customer = mock(TargetCustomerModel.class);

        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getUser()).willReturn(customer);

        given(mockCheckoutFacade.getCart()).willReturn(mockCartModel);
        given(mockUserService.getCurrentUser()).willReturn(customer);

        doReturn(Boolean.FALSE).when(mockTargetFraudCommerceCheckoutService).beforePlaceOrder(cartModel,
                paymentTransactionModel);

        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.prepareForPlaceOrder();

        assertThat(result).isNotNull();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.PAYMENT_FAILURE);
        assertThat(result.getAbstractOrderModel()).isNotNull();
    }

    @Test
    public void testPrepareForPlaceOrder() throws InsufficientStockLevelException {
        doReturn(Boolean.TRUE).when(mockTargetFraudCommerceCheckoutService).beforePlaceOrder(cartModel,
                null);

        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.prepareForPlaceOrder();
        verify(findOrderTotalPaymentMadeStrategy).getAmountPaidSoFar(cartModel);

        assertThat(result).isNotNull();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.PREPARE_SUCCESS);
    }

    @Test
    public void testFinishPlaceOrderNullCart() {
        given(mockCheckoutFacade.getCart()).willReturn(null);

        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.finishPlaceOrder();

        assertThat(result).isNotNull();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.INVALID_CART);
        assertThat(result.getAbstractOrderModel()).isNull();
    }

    @Test
    public void testFinishPlaceOrderUserMisMatch() {
        final TargetCustomerModel customer1 = mock(TargetCustomerModel.class);
        final TargetCustomerModel customer2 = mock(TargetCustomerModel.class);

        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getUser()).willReturn(customer1);

        given(mockCheckoutFacade.getCart()).willReturn(mockCartModel);
        given(mockUserService.getCurrentUser()).willReturn(customer2);

        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.finishPlaceOrder();

        assertThat(result).isNotNull();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.USER_MISMATCH);
        assertThat(result.getAbstractOrderModel()).isNotNull();
    }

    @Test
    public void testFinishPlaceOrderPaymentFailure() throws InsufficientStockLevelException {
        final TargetCustomerModel customer = mock(TargetCustomerModel.class);

        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getUser()).willReturn(customer);

        given(mockCheckoutFacade.getCart()).willReturn(mockCartModel);
        given(mockUserService.getCurrentUser()).willReturn(customer);

        doReturn(Boolean.FALSE).when(mockTargetFraudCommerceCheckoutService).isCaptureOrderSuccessful(mockCartModel);

        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.finishPlaceOrder();

        assertThat(result).isNotNull();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.PAYMENT_FAILURE);
        assertThat(result.getAbstractOrderModel()).isNotNull();
    }

    @Test
    public void testFinishPlaceOrderInvalidCart() throws InsufficientStockLevelException, InvalidCartException {
        final TargetCustomerModel customer = mock(TargetCustomerModel.class);

        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getUser()).willReturn(customer);

        given(mockCheckoutFacade.getCart()).willReturn(mockCartModel);
        given(mockUserService.getCurrentUser()).willReturn(customer);

        doReturn(Boolean.TRUE).when(mockTargetFraudCommerceCheckoutService).isCaptureOrderSuccessful(mockCartModel);

        given(mockTargetFraudCommerceCheckoutService.placeOrder(commerceCheckoutParameterCaptor.capture()))
                .willThrow(new InvalidCartException("Invalid Cart"));

        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);

        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.finishPlaceOrder();

        assertThat(result).isNotNull();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.INVALID_CART);
        assertThat(result.getAbstractOrderModel()).isNull();

        final CommerceCheckoutParameter capturedCommerceCheckoutParameter = commerceCheckoutParameterCaptor.getValue();
        assertThat(capturedCommerceCheckoutParameter.getSalesApplication()).isEqualTo(SalesApplication.WEB);
        assertThat(capturedCommerceCheckoutParameter.getCart()).isEqualTo(mockCartModel);
    }

    @Test
    public void testFinishPlaceOrder() throws InsufficientStockLevelException, InvalidCartException {
        final TargetCustomerModel customer = mock(TargetCustomerModel.class);

        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getUser()).willReturn(customer);

        given(mockCheckoutFacade.getCart()).willReturn(mockCartModel);
        given(mockUserService.getCurrentUser()).willReturn(customer);

        doReturn(Boolean.TRUE).when(mockTargetFraudCommerceCheckoutService).isCaptureOrderSuccessful(mockCartModel);

        final OrderModel mockOrder = mock(OrderModel.class);
        given(mockOrderResult.getOrder()).willReturn(mockOrder);

        final OrderData mockOrderData = mock(OrderData.class);
        given(mockOrderConverter.convert(mockOrder)).willReturn(mockOrderData);

        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.finishPlaceOrder();

        assertThat(result).isNotNull();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.SUCCESS);
        assertThat(result.getAbstractOrderModel()).isEqualTo(mockOrder);

        verify(mockOrderConverter).convert(mockOrder);
    }

    @Test
    public void testBeforePlaceOrderWithNullIpgPaymentMethod() throws InsufficientStockLevelException {
        given(paymentMethodStrategy.getPaymentMethod(paymentInfoModel)).willReturn(null);

        doReturn(Boolean.TRUE).when(mockTargetFraudCommerceCheckoutService).beforePlaceOrder(cartModel,
                null);

        final TargetPlaceOrderResultEnum result = targetPlaceOrderFacadeImpl.beforePlaceOrder(cartModel);

        assertThat(result).isNull();
    }

    @Test
    public void testBeforePlaceOrderWithNonIpgPaymentMethod() throws InsufficientStockLevelException {
        doReturn(Boolean.TRUE).when(mockTargetFraudCommerceCheckoutService).beforePlaceOrder(cartModel,
                null);

        final TargetPlaceOrderResultEnum result = targetPlaceOrderFacadeImpl.beforePlaceOrder(cartModel);
        assertThat(result).isNull();
    }

    @Test
    public void testBeforePlaceOrderWithIpgPaymentMethodWithNullQueryTransaction()
            throws InsufficientStockLevelException {
        given(cartModel.getPaymentInfo()).willReturn(ipgPaymentInfoModel);
        doReturn(Boolean.TRUE).when(mockTargetFraudCommerceCheckoutService).beforePlaceOrder(cartModel,
                paymentTransactionModel);
        doReturn(Boolean.FALSE).when(queryResult).isSuccess();

        final TargetPlaceOrderResultEnum result = targetPlaceOrderFacadeImpl.beforePlaceOrder(cartModel);

        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(TargetPlaceOrderResultEnum.PAYMENT_FAILURE);
    }

    @Test
    public void testBeforePlaceOrderWithIpgPaymentMethodWithFailedQueryTransaction()
            throws InsufficientStockLevelException {
        given(paymentMethodStrategy.getPaymentMethod(paymentInfoModel)).willReturn(ipgPaymentMethod);
        doReturn(Boolean.TRUE).when(mockTargetFraudCommerceCheckoutService).beforePlaceOrder(cartModel,
                paymentTransactionModel);
        doReturn(Boolean.FALSE).when(queryResult).isSuccess();
        doReturn(queryResult).when(targetPaymentService)
                .queryTransactionDetails(ipgPaymentMethod, paymentInfoModel);

        final TargetPlaceOrderResultEnum result = targetPlaceOrderFacadeImpl.beforePlaceOrder(cartModel);

        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(TargetPlaceOrderResultEnum.PAYMENT_FAILURE);
    }

    @Test
    public void testBeforePlaceOrderWithIpgPaymentMethodWithSuccessQueryTransaction()
            throws InsufficientStockLevelException {
        final List<PaymentTransactionEntryModel> list = new ArrayList<>();
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        list.add(paymentTransactionEntryModel);

        given(cartModel.getUser()).willReturn(customerModel);
        given(paymentMethodStrategy.getPaymentMethod(paymentInfoModel)).willReturn(ipgPaymentMethod);
        given(paymentTransactionModel.getEntries()).willReturn(list);
        doReturn(Boolean.TRUE).when(mockTargetFraudCommerceCheckoutService).beforePlaceOrder(cartModel,
                paymentTransactionModel);

        doReturn(Boolean.TRUE).when(queryResult).isSuccess();
        doReturn(queryResult).when(targetPaymentService)
                .queryTransactionDetails(ipgPaymentMethod, paymentInfoModel);

        given(targetPaymentService.createPaymentTransactionEntry(cartModel)).willReturn(
                paymentTransactionEntryModel);
        given(targetPaymentService.createTransactionWithQueryResult(cartModel, queryResult)).willReturn(
                paymentTransactionModel);


        final TargetPlaceOrderResultEnum result = targetPlaceOrderFacadeImpl.beforePlaceOrder(cartModel);

        verify(targetPaymentService).createTransactionWithQueryResult(
                cartModel,
                queryResult);
        verify(targetCustomerAccountService).removeCustomerIPGSavedCards(customerModel);

        assertThat(result).isNull();
    }

    @Test
    public void testReverseGiftCardSuccessful() {
        doReturn(Boolean.TRUE).when(targetPaymentService).reverseGiftCardPayment(cartModel);

        final boolean isGiftCardReversalSucceeded = targetPlaceOrderFacadeImpl.reverseGiftCardPayment();

        assertThat(isGiftCardReversalSucceeded).isTrue();
    }

    @Test
    public void testReverseGiftCardFail() {
        doReturn(Boolean.FALSE).when(targetPaymentService).reverseGiftCardPayment(cartModel);

        final boolean isGiftCardReversalSucceeded = targetPlaceOrderFacadeImpl.reverseGiftCardPayment();

        assertThat(isGiftCardReversalSucceeded).isFalse();
    }

    @Test
    public void testGetGiftCardsWhenCardResultsIsNull() {
        final List<TargetCardResult> cardResults = null;
        final List<TargetCardResult> giftCards = targetPlaceOrderFacadeImpl.getGiftCards(cardResults);

        assertThat(giftCards).isNotNull().isEmpty();
    }

    @Test
    public void testGetGiftCardsWhenCardResultsIsEmpty() {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        final List<TargetCardResult> giftCards = targetPlaceOrderFacadeImpl.getGiftCards(cardResults);

        assertThat(giftCards).isNotNull().isEmpty();
    }

    @Test
    public void testGetGiftCardsWhenCardResultsHasOnlyOneCreditCard() {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        final TargetCardResult creditCard = new TargetCardResult();
        creditCard.setCardType(CreditCardType.VISA.getCode());
        cardResults.add(creditCard);

        final List<TargetCardResult> giftCards = targetPlaceOrderFacadeImpl.getGiftCards(cardResults);

        assertThat(giftCards).isNotNull().isEmpty();
    }

    @Test
    public void testGetGiftCardsWhenCardResultsHasMultipleCreditCards() {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        final TargetCardResult creditCard1 = new TargetCardResult();
        creditCard1.setCardType(CreditCardType.VISA.getCode());
        cardResults.add(creditCard1);

        final TargetCardResult creditCard2 = new TargetCardResult();
        creditCard2.setCardType(CreditCardType.VISA.getCode());
        cardResults.add(creditCard2);

        final List<TargetCardResult> giftCards = targetPlaceOrderFacadeImpl.getGiftCards(cardResults);

        assertThat(giftCards).isNotNull().isEmpty();
    }

    @Test
    public void testGetGiftCardsWhenCardResultsHasOnlyOneGiftCard() {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        final TargetCardResult creditCard = new TargetCardResult();
        creditCard.setCardType(CreditCardType.GIFTCARD.getCode());
        cardResults.add(creditCard);

        final List<TargetCardResult> giftCards = targetPlaceOrderFacadeImpl.getGiftCards(cardResults);

        assertThat(giftCards).isNotNull().isNotEmpty().hasSize(1).onProperty("cardType")
                .containsExactly(CreditCardType.GIFTCARD.getCode());
    }

    @Test
    public void testGetGiftCardsWhenCardResultsHasMixOfCreditCardsAndGiftCards() {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        final TargetCardResult creditCard1 = new TargetCardResult();
        creditCard1.setCardType(CreditCardType.VISA.getCode());
        cardResults.add(creditCard1);

        final TargetCardResult giftCard1 = new TargetCardResult();
        giftCard1.setCardType(CreditCardType.GIFTCARD.getCode());
        cardResults.add(giftCard1);

        final TargetCardResult creditCard2 = new TargetCardResult();
        creditCard2.setCardType(CreditCardType.VISA.getCode());
        cardResults.add(creditCard2);

        final TargetCardResult giftCard2 = new TargetCardResult();
        giftCard2.setCardType(CreditCardType.GIFTCARD.getCode());
        cardResults.add(giftCard2);

        final List<TargetCardResult> giftCards = targetPlaceOrderFacadeImpl.getGiftCards(cardResults);

        assertThat(giftCards).isNotNull().isNotEmpty().hasSize(2).onProperty("cardType")
                .containsExactly(CreditCardType.GIFTCARD.getCode(), CreditCardType.GIFTCARD.getCode());
    }

    @Test
    public void testBeforePlaceOrderWhenNotNeedToReversePayments() throws InsufficientStockLevelException {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        given(paymentMethodStrategy.getPaymentMethod(paymentInfoModel)).willReturn(ipgPaymentMethod);
        doReturn(queryResult).when(targetPaymentService).queryTransactionDetails(ipgPaymentMethod,
                paymentInfoModel);
        doReturn(Boolean.TRUE).when(queryResult).isSuccess();

        doReturn(Boolean.FALSE).when(mockTargetFraudCommerceCheckoutService).shouldPaymentBeReversed(cartModel,
                cardResults);

        targetPlaceOrderFacadeImpl.beforePlaceOrder(cartModel);

        verify(targetPaymentService, times(0)).reversePayment(cartModel, cardResults);
    }

    @Test
    public void testBeforePlaceOrderWhenNeedToReversePayments() throws InsufficientStockLevelException {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        given(paymentMethodStrategy.getPaymentMethod(paymentInfoModel)).willReturn(ipgPaymentMethod);
        doReturn(queryResult).when(targetPaymentService).queryTransactionDetails(ipgPaymentMethod,
                paymentInfoModel);
        doReturn(Boolean.TRUE).when(queryResult).isSuccess();

        doReturn(Boolean.TRUE).when(mockTargetFraudCommerceCheckoutService).shouldPaymentBeReversed(cartModel,
                cardResults);

        targetPlaceOrderFacadeImpl.beforePlaceOrder(cartModel);

        verify(targetPaymentService).reversePayment(cartModel, cardResults);
    }

    @Test
    public void testBeforePlaceOrderReturnsCorrectResultWhenNeedToReversePayments()
            throws InsufficientStockLevelException {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getPaymentInfo()).willReturn(paymentInfoModel);
        given(paymentMethodStrategy.getPaymentMethod(paymentInfoModel)).willReturn(ipgPaymentMethod);
        doReturn(queryResult).when(targetPaymentService).queryTransactionDetails(ipgPaymentMethod,
                paymentInfoModel);
        doReturn(Boolean.TRUE).when(queryResult).isSuccess();

        doReturn(Boolean.TRUE).when(mockTargetFraudCommerceCheckoutService).shouldPaymentBeReversed(mockCartModel,
                cardResults);

        final TargetPlaceOrderResultEnum placeOrderResult = targetPlaceOrderFacadeImpl.beforePlaceOrder(mockCartModel);

        verify(targetPaymentService).reversePayment(mockCartModel, cardResults);

        assertThat(placeOrderResult).isEqualTo(TargetPlaceOrderResultEnum.PAYMENT_FAILURE_DUETO_MISSMATCH_TOTALS);
    }

    @Test
    public void testBeforePlaceOrderDoNotRemoveSavedCardsAfterPaymentForPreOrder()
            throws InsufficientStockLevelException {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getPaymentInfo()).willReturn(paymentInfoModel);
        given(paymentMethodStrategy.getPaymentMethod(paymentInfoModel)).willReturn(ipgPaymentMethod);
        given(targetPaymentService.queryTransactionDetails(ipgPaymentMethod,
                paymentInfoModel)).willReturn(queryResult);
        willReturn(Boolean.TRUE).given(queryResult).isSuccess();

        willReturn(Boolean.TRUE).given(mockTargetFraudCommerceCheckoutService).shouldPaymentBeReversed(mockCartModel,
                cardResults);

        given(queryResult.getSavedCards()).willReturn(null);
        willReturn(Boolean.TRUE).given(mockCartModel).getContainsPreOrderItems();

        targetPlaceOrderFacadeImpl.beforePlaceOrder(mockCartModel);

        verify(targetCustomerAccountService, never()).removeCustomerIPGSavedCards(customerModel);
        verify(targetPaymentService, never()).updateSavedCards(mockCartModel, queryResult.getSavedCards());

    }

    @Test
    public void testGetQueryTransactionDetailResults() {
        final TargetQueryTransactionDetailsResult result = new TargetQueryTransactionDetailsResult();
        result.setCancel(true);

        given(targetPaymentService.queryTransactionDetails(paymentMethod, paymentInfoModel)).willReturn(result);

        final TargetQueryTransactionDetailsResult returnedResult = targetPlaceOrderFacadeImpl
                .getQueryTransactionDetailResults();

        assertThat(returnedResult).isNotNull();
        assertThat(returnedResult.isCancel()).isTrue();
    }

    @Test
    public void testGiftcardReversalWhenCartHasPaidAmount() {
        final TargetCustomerModel customer1 = mock(TargetCustomerModel.class);

        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getUser()).willReturn(customer1);

        given(mockCheckoutFacade.getCart()).willReturn(mockCartModel);
        given(mockUserService.getCurrentUser()).willReturn(customer1);

        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(mockCartModel)).willReturn(new Double("1.0"));

        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.prepareForPlaceOrder();

        assertThat(result).isNotNull();
        verify(targetPlaceOrderFacadeImpl).reverseGiftCardPayment();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.UNKNOWN);
    }

    @Test
    public void testNoGiftcardReversalWhenCartHasNoPaidAmount() {
        final TargetCustomerModel customer1 = mock(TargetCustomerModel.class);

        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getUser()).willReturn(customer1);

        given(mockCheckoutFacade.getCart()).willReturn(mockCartModel);
        given(mockUserService.getCurrentUser()).willReturn(customer1);

        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(mockCartModel)).willReturn(new Double("0.0"));

        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.prepareForPlaceOrder();

        assertThat(result).isNotNull();
        verify(targetPlaceOrderFacadeImpl, never()).reverseGiftCardPayment();
    }

    @Test
    public void testReverseGiftCardByToken() {
        targetPlaceOrderFacadeImpl.reverseGiftCardPayment("token", "session");
        verify(targetPaymentService).reverseGiftCardPayment("token", "session");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReverseGiftCardWhenTokenIsNull() {
        targetPlaceOrderFacadeImpl.reverseGiftCardPayment(null, "session");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReverseGiftCardWhenSessionIsNull() {
        targetPlaceOrderFacadeImpl.reverseGiftCardPayment("token", null);
    }

    public void testGetAmountPaidSoFar() {
        final CartModel mockCartModel = mock(CartModel.class);
        final Double amount = Double.valueOf(1d);
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(mockCartModel)).willReturn(amount);

        final Double amountPaidSoFar = targetPlaceOrderFacadeImpl.getAmountPaidSoFar(mockCartModel);
        assertThat(amountPaidSoFar).isNotNull().isEqualTo(amount);
    }

    @Test
    public void testHasPaidAmount() {
        final Double amount = Double.valueOf(2.5d);
        final CartModel mockCartModel = mock(CartModel.class);

        doReturn(amount).when(targetPlaceOrderFacadeImpl).getAmountPaidSoFar(mockCartModel);
        final boolean hasPaidAmount = targetPlaceOrderFacadeImpl.hasPaidAmount(mockCartModel);

        assertThat(hasPaidAmount).isTrue();
    }

    @Test
    public void verifyPlaceOrderWhenCartIsNotCalculated() throws InvalidCartException {
        given(cartModel.getCalculated()).willReturn(Boolean.FALSE);
        targetPlaceOrderFacadeImpl.placeOrder(cartModel);
        verify(mockCheckoutFacade).recalculateCart();
    }

    @Test
    public void verifyPlaceOrderWhenCartIsCalculated() throws InvalidCartException {
        given(cartModel.getCalculated()).willReturn(Boolean.TRUE);
        given(mockCheckoutFacade.getCart()).willReturn(cartModel);
        targetPlaceOrderFacadeImpl.placeOrder(cartModel);
        verify(mockCheckoutFacade, never()).recalculateCart();
    }

    @Test
    public void verifyPlaceOrderWhenCartIsNull() throws InvalidCartException {
        given(cartModel.getCalculated()).willReturn(Boolean.TRUE);
        given(mockCheckoutFacade.getCart()).willReturn(null);
        targetPlaceOrderFacadeImpl.placeOrder(cartModel);
        verify(mockCheckoutFacade, never()).recalculateCart();
    }

    @Test
    public void verifyPlaceOrderWhenCartHasNullCalculatedValue() throws InvalidCartException {
        given(cartModel.getCalculated()).willReturn(null);
        given(mockCheckoutFacade.getCart()).willReturn(cartModel);
        targetPlaceOrderFacadeImpl.placeOrder(cartModel);
        verify(mockCheckoutFacade, never()).recalculateCart();
    }

    @Test
    public void verifyPlaceOrderOnRequest() {
        final TargetPlaceOrderResult placeOrderResult = mock(TargetPlaceOrderResult.class);
        doReturn(placeOrderResult).when(targetPlaceOrderFacadeImpl).placeOrder();
        prepareCart();
        given(cartModel.getDeliveryAddress()).willReturn(addressModel);
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        given(placeOrderResult.getPlaceOrderResult()).willReturn(TargetPlaceOrderResultEnum.SUCCESS);

        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();
        ipgPaymentInfo.setIpgToken("ipgToken");

        final TargetPlaceOrderRequest request = new TargetPlaceOrderRequest();
        request.setPaymentInfo(ipgPaymentInfo);
        request.setIpAddress("1.1.1.1");
        request.setCookieString("cookie");
        final TargetPlaceOrderResult actualResult = targetPlaceOrderFacadeImpl.placeOrder(request);
        assertThat(actualResult).isEqualTo(placeOrderResult);
        verify(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        verify(mockCheckoutFacade).validateDeliveryMode();


        final ArgumentCaptor<ClientDetails> argumentCaptor = ArgumentCaptor.forClass(ClientDetails.class);
        verify(mockTargetFraudCommerceCheckoutService).populateClientDetailsForFraudDetection(
                eq(cartModel), argumentCaptor.capture());

        final ClientDetails capturedClientDetails = argumentCaptor.getValue();
        assertThat(capturedClientDetails.getIpAddress()).isEqualTo("1.1.1.1");
        assertThat(capturedClientDetails.getBrowserCookies()).isEqualTo("cookie");

        assertThat(actualResult.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.SUCCESS);
        verify(flybuysDiscountFacade).reassessFlybuysDiscountOnCheckoutCart();
    }

    @Test
    public void verifyPlaceOrderOnRequestWitPaypal() {
        final String payPalPayerId = "payPalPayerId";
        final String payPalSessionToken = "payPalSessionToken";

        final TargetPlaceOrderResult placeOrderResult = mock(TargetPlaceOrderResult.class);
        doReturn(placeOrderResult).when(targetPlaceOrderFacadeImpl).placeOrder();
        prepareCart();
        given(cartModel.getDeliveryAddress()).willReturn(addressModel);
        given(placeOrderResult.getPlaceOrderResult()).willReturn(TargetPlaceOrderResultEnum.SUCCESS);
        given(Boolean.valueOf(mockCheckoutFacade.isPaymentModePaypal())).willReturn(Boolean.TRUE);

        final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfo = new TargetPlaceOrderPayPalPaymentInfoData();
        payPalPaymentInfo.setPaypalPayerId(payPalPayerId);
        payPalPaymentInfo.setPaypalSessionToken(payPalSessionToken);

        final TargetPlaceOrderRequest request = new TargetPlaceOrderRequest();
        request.setPaymentInfo(payPalPaymentInfo);
        request.setIpAddress("1.1.1.1");
        request.setCookieString("cookie");
        final TargetPlaceOrderResult actualResult = targetPlaceOrderFacadeImpl.placeOrder(request);
        assertThat(actualResult).isEqualTo(placeOrderResult);
        verify(mockCheckoutFacade).validateDeliveryMode();

        final ArgumentCaptor<ClientDetails> argumentCaptor = ArgumentCaptor.forClass(ClientDetails.class);
        verify(mockTargetFraudCommerceCheckoutService).populateClientDetailsForFraudDetection(
                eq(cartModel), argumentCaptor.capture());

        final ClientDetails capturedClientDetails = argumentCaptor.getValue();
        assertThat(capturedClientDetails.getIpAddress()).isEqualTo("1.1.1.1");
        assertThat(capturedClientDetails.getBrowserCookies()).isEqualTo("cookie");

        assertThat(actualResult.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.SUCCESS);
        verify(flybuysDiscountFacade).reassessFlybuysDiscountOnCheckoutCart();
        verify(mockCheckoutFacade).createPayPalPaymentInfo(payPalSessionToken, payPalPayerId);
    }

    /**
     * 
     */
    private void prepareCart() {
        given(mockCheckoutFacade.getCart()).willReturn(cartModel);
        given(targetPurchaseOptionHelper.getCurrentPurchaseOptionModel()).willReturn(purchaseOptionModel);
        given(mockCheckoutFacade.getCheckoutCart()).willReturn(cartData);
        final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
        given(cartModel.getEntries()).willReturn(ImmutableList.of(orderEntry));
        doReturn(Boolean.FALSE).when(cartData).isInsufficientAmount();
    }

    @Test
    public void verifyPlaceOrderOnRequestWithAdapterException() {
        prepareCart();
        final AdapterException adapterException = mock(AdapterException.class);
        doThrow(adapterException).when(targetPlaceOrderFacadeImpl).placeOrder();
        given(cartModel.getDeliveryAddress()).willReturn(addressModel);
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");

        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();
        ipgPaymentInfo.setIpgToken("ipgToken");

        final TargetPlaceOrderRequest request = new TargetPlaceOrderRequest();
        request.setPaymentInfo(ipgPaymentInfo);
        request.setIpAddress("1.1.1.1");
        request.setCookieString("cookie");
        final TargetPlaceOrderResult actualResult = targetPlaceOrderFacadeImpl.placeOrder(request);
        verify(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        verify(mockCheckoutFacade).validateDeliveryMode();

        final ArgumentCaptor<ClientDetails> argumentCaptor = ArgumentCaptor.forClass(ClientDetails.class);
        verify(mockTargetFraudCommerceCheckoutService).populateClientDetailsForFraudDetection(
                eq(cartModel), argumentCaptor.capture());

        final ClientDetails capturedClientDetails = argumentCaptor.getValue();
        assertThat(capturedClientDetails.getIpAddress()).isEqualTo("1.1.1.1");
        assertThat(capturedClientDetails.getBrowserCookies()).isEqualTo("cookie");

        verify(targetPaymentService).reverseGiftCardPayment(cartModel);
        assertThat(actualResult.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.SERVICE_UNAVAILABLE);
        verify(flybuysDiscountFacade, times(2)).reassessFlybuysDiscountOnCheckoutCart();
    }

    @Test
    public void verifyPlaceOrderOnRequestWithException() {
        prepareCart();
        final NullPointerException exception = mock(NullPointerException.class);
        doThrow(exception).when(targetPlaceOrderFacadeImpl).placeOrder();
        given(cartModel.getCode()).willReturn("0010000");
        given(cartModel.getDeliveryAddress()).willReturn(addressModel);
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");

        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();
        ipgPaymentInfo.setIpgToken("ipgToken");

        final TargetPlaceOrderRequest request = new TargetPlaceOrderRequest();
        request.setPaymentInfo(ipgPaymentInfo);
        request.setIpAddress("1.1.1.1");
        request.setCookieString("cookie");
        final TargetPlaceOrderResult actualResult = targetPlaceOrderFacadeImpl.placeOrder(request);
        verify(mockCheckoutFacade).validateDeliveryMode();

        final ArgumentCaptor<ClientDetails> argumentCaptor = ArgumentCaptor.forClass(ClientDetails.class);
        verify(mockTargetFraudCommerceCheckoutService).populateClientDetailsForFraudDetection(
                eq(cartModel), argumentCaptor.capture());

        final ClientDetails capturedClientDetails = argumentCaptor.getValue();
        assertThat(capturedClientDetails.getIpAddress()).isEqualTo("1.1.1.1");
        assertThat(capturedClientDetails.getBrowserCookies()).isEqualTo("cookie");

        verify(targetPaymentService).reverseGiftCardPayment(cartModel);
        verify(targetOrderErrorHandlerFacade).handleOrderCreationFailed(TargetPlaceOrderResultEnum.UNKNOWN, "0010000");
        assertThat(actualResult.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.UNKNOWN);
        verify(flybuysDiscountFacade, times(2)).reassessFlybuysDiscountOnCheckoutCart();
    }

    @Test
    public void verifyPlaceOrderOnRequestWithGiftcard() {
        prepareCart();
        final TargetPlaceOrderResult placeOrderResult = mock(TargetPlaceOrderResult.class);
        doReturn(placeOrderResult).when(targetPlaceOrderFacadeImpl).placeOrder();
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).isPaymentModeGiftCard();
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).isGiftCardPaymentAllowedForCart();
        given(cartModel.getDeliveryAddress()).willReturn(addressModel);
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        given(placeOrderResult.getPlaceOrderResult()).willReturn(TargetPlaceOrderResultEnum.SUCCESS);

        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();
        ipgPaymentInfo.setIpgToken("ipgToken");

        final TargetPlaceOrderRequest request = new TargetPlaceOrderRequest();
        request.setPaymentInfo(ipgPaymentInfo);
        request.setIpAddress("1.1.1.1");
        request.setCookieString("cookie");
        final TargetPlaceOrderResult actualResult = targetPlaceOrderFacadeImpl.placeOrder(request);
        assertThat(actualResult).isEqualTo(placeOrderResult);
        verify(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        verify(mockCheckoutFacade).validateDeliveryMode();

        final ArgumentCaptor<ClientDetails> argumentCaptor = ArgumentCaptor.forClass(ClientDetails.class);
        verify(mockTargetFraudCommerceCheckoutService).populateClientDetailsForFraudDetection(
                eq(cartModel), argumentCaptor.capture());

        final ClientDetails capturedClientDetails = argumentCaptor.getValue();
        assertThat(capturedClientDetails.getIpAddress()).isEqualTo("1.1.1.1");
        assertThat(capturedClientDetails.getBrowserCookies()).isEqualTo("cookie");

        verify(flybuysDiscountFacade).reassessFlybuysDiscountOnCheckoutCart();
    }

    @Test
    public void verifyPlaceOrderOnRequestWithSoh() {
        prepareCart();
        given(cartModel.getDeliveryAddress()).willReturn(addressModel);
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        final AdjustedCartEntriesData adjustedData = mock(AdjustedCartEntriesData.class);
        doReturn(Boolean.TRUE).when(adjustedData).isAdjusted();
        given(mockCheckoutFacade.adjustCart()).willReturn(adjustedData);

        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();
        ipgPaymentInfo.setIpgToken("ipgToken");

        final TargetPlaceOrderRequest request = new TargetPlaceOrderRequest();
        request.setPaymentInfo(ipgPaymentInfo);
        request.setIpAddress("1.1.1.1");
        request.setCookieString("cookie");
        final TargetPlaceOrderResult actualResult = targetPlaceOrderFacadeImpl.placeOrder(request);
        assertThat(actualResult.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.INSUFFICIENT_STOCK);
        assertThat(actualResult.getAdjustedCartEntriesData()).isEqualTo(adjustedData);
        verify(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        verify(mockCheckoutFacade).adjustCart();

        verify(flybuysDiscountFacade).reassessFlybuysDiscountOnCheckoutCart();
    }

    @Test
    public void verifyPlaceOrderOnRequestWithNullCart() {
        final TargetPlaceOrderResult placeOrderResult = mock(TargetPlaceOrderResult.class);
        doReturn(placeOrderResult).when(targetPlaceOrderFacadeImpl).placeOrder();
        given(mockCheckoutFacade.getCart()).willReturn(null);

        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();
        ipgPaymentInfo.setIpgToken("ipgToken");

        final TargetPlaceOrderRequest request = new TargetPlaceOrderRequest();
        request.setPaymentInfo(ipgPaymentInfo);
        request.setIpAddress("1.1.1.1");
        request.setCookieString("cookie");
        final TargetPlaceOrderResult actualResult = targetPlaceOrderFacadeImpl.placeOrder(request);
        assertThat(actualResult).isNull();
        verifyZeroInteractions(flybuysDiscountFacade);
    }

    @Test
    public void verifyPlaceOrderOnRequestWithIncompleteDeliveryInfo() {
        final TargetPlaceOrderResult placeOrderResult = mock(TargetPlaceOrderResult.class);
        doReturn(placeOrderResult).when(targetPlaceOrderFacadeImpl).placeOrder();
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).hasIncompleteDeliveryDetails();
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        prepareCart();

        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();
        ipgPaymentInfo.setIpgToken("ipgToken");

        final TargetPlaceOrderRequest request = new TargetPlaceOrderRequest();
        request.setPaymentInfo(ipgPaymentInfo);
        request.setIpAddress("1.1.1.1");
        request.setCookieString("cookie");
        final TargetPlaceOrderResult actualResult = targetPlaceOrderFacadeImpl.placeOrder(request);
        verify(targetPaymentService).reverseGiftCardPayment(cartModel);
        verify(mockCheckoutFacade).removeDeliveryInfo();
        assertThat(actualResult.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.INCOMPLETE_DELIVERY_INFO);
        verify(flybuysDiscountFacade).reassessFlybuysDiscountOnCheckoutCart();
    }

    @Test
    public void verifyPlaceOrderOnRequestWithSohForGiftCard() {
        prepareCart();
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).isPaymentModeGiftCard();
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        final AdjustedCartEntriesData adjustedData = mock(AdjustedCartEntriesData.class);
        doReturn(Boolean.TRUE).when(adjustedData).isAdjusted();
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).isGiftCardPaymentAllowedForCart();
        given(mockCheckoutFacade.adjustCart()).willReturn(adjustedData);

        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();
        ipgPaymentInfo.setIpgToken("ipgToken");

        final TargetPlaceOrderRequest request = new TargetPlaceOrderRequest();
        request.setPaymentInfo(ipgPaymentInfo);
        request.setIpAddress("1.1.1.1");
        request.setCookieString("cookie");
        final TargetPlaceOrderResult actualResult = targetPlaceOrderFacadeImpl.placeOrder(request);
        verify(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        verify(mockCheckoutFacade, never()).validateDeliveryMode();
        verify(mockCheckoutFacade).adjustCart();
        verifyZeroInteractions(mockTargetFraudCommerceCheckoutService);
        verify(targetPaymentService).reverseGiftCardPayment(cartModel);
        assertThat(actualResult.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.INSUFFICIENT_STOCK);
        assertThat(actualResult.getAdjustedCartEntriesData()).isEqualTo(adjustedData);
        verify(flybuysDiscountFacade).reassessFlybuysDiscountOnCheckoutCart();
    }

    @Test
    public void verifyPlaceOrderOnRequestWithSohForGiftCardWithFlybuysRemoval() {
        prepareCart();
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).isPaymentModeGiftCard();
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        final AdjustedCartEntriesData adjustedData = mock(AdjustedCartEntriesData.class);
        doReturn(Boolean.TRUE).when(adjustedData).isAdjusted();
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).isGiftCardPaymentAllowedForCart();
        given(mockCheckoutFacade.adjustCart()).willReturn(adjustedData);
        given(Boolean.valueOf(flybuysDiscountFacade.reassessFlybuysDiscountOnCheckoutCart())).willReturn(Boolean.TRUE);

        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();
        ipgPaymentInfo.setIpgToken("ipgToken");

        final TargetPlaceOrderRequest request = new TargetPlaceOrderRequest();
        request.setPaymentInfo(ipgPaymentInfo);
        request.setIpAddress("1.1.1.1");
        request.setCookieString("cookie");
        final TargetPlaceOrderResult actualResult = targetPlaceOrderFacadeImpl.placeOrder(request);
        verify(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        verify(mockCheckoutFacade, never()).validateDeliveryMode();
        verify(mockCheckoutFacade).adjustCart();
        verifyZeroInteractions(mockTargetFraudCommerceCheckoutService);
        verify(targetPaymentService).reverseGiftCardPayment(cartModel);
        assertThat(actualResult.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.INSUFFICIENT_STOCK_FLYBUYS);
        assertThat(actualResult.getAdjustedCartEntriesData()).isEqualTo(adjustedData);
        verify(flybuysDiscountFacade).reassessFlybuysDiscountOnCheckoutCart();
    }

    @Test
    public void verifyPlaceOrderOnRequestWhenGiftCardIsNotAllowed() {
        prepareCart();
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).isPaymentModeGiftCard();
        final AdjustedCartEntriesData adjustedData = mock(AdjustedCartEntriesData.class);
        doReturn(Boolean.TRUE).when(adjustedData).isAdjusted();
        doReturn(Boolean.FALSE).when(mockCheckoutFacade).isGiftCardPaymentAllowedForCart();
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        given(mockCheckoutFacade.adjustCart()).willReturn(adjustedData);

        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();
        ipgPaymentInfo.setIpgToken("ipgToken");

        final TargetPlaceOrderRequest request = new TargetPlaceOrderRequest();
        request.setPaymentInfo(ipgPaymentInfo);
        request.setIpAddress("1.1.1.1");
        request.setCookieString("cookie");
        final TargetPlaceOrderResult actualResult = targetPlaceOrderFacadeImpl.placeOrder(request);
        verify(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        verify(mockCheckoutFacade, times(0)).validateDeliveryMode();
        verify(mockCheckoutFacade).adjustCart();
        verifyZeroInteractions(mockTargetFraudCommerceCheckoutService);
        verify(targetPaymentService).reverseGiftCardPayment(cartModel);
        assertThat(actualResult.getPlaceOrderResult()).isEqualTo(
                TargetPlaceOrderResultEnum.GIFTCARD_PAYMENT_NOT_ALLOWED);
        assertThat(actualResult.getAdjustedCartEntriesData()).isEqualTo(adjustedData);
        verify(flybuysDiscountFacade).reassessFlybuysDiscountOnCheckoutCart();
    }

    @Test
    public void verifyPlaceOrderOnRequestWithIncompleteBillingAddressData() {
        prepareCart();
        given(cartModel.getDeliveryAddress()).willReturn(null);
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).isPaymentModeGiftCard();
        final AdjustedCartEntriesData adjustedData = mock(AdjustedCartEntriesData.class);
        doReturn(Boolean.TRUE).when(adjustedData).isAdjusted();
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).hasIncompleteBillingAddress();
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        given(mockCheckoutFacade.adjustCart()).willReturn(adjustedData);

        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();
        ipgPaymentInfo.setIpgToken("ipgToken");

        final TargetPlaceOrderRequest request = new TargetPlaceOrderRequest();
        request.setPaymentInfo(ipgPaymentInfo);
        request.setIpAddress("1.1.1.1");
        request.setCookieString("cookie");
        final TargetPlaceOrderResult actualResult = targetPlaceOrderFacadeImpl.placeOrder(request);
        verify(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        verify(mockCheckoutFacade, never()).validateDeliveryMode();
        verify(mockCheckoutFacade).adjustCart();
        verifyZeroInteractions(mockTargetFraudCommerceCheckoutService);
        verify(targetPaymentService).reverseGiftCardPayment(cartModel);
        assertThat(actualResult.getPlaceOrderResult()).isEqualTo(
                TargetPlaceOrderResultEnum.INCOMPLETE_BILLING_ADDRESS);
        assertThat(actualResult.getAdjustedCartEntriesData()).isEqualTo(adjustedData);
        verify(flybuysDiscountFacade).reassessFlybuysDiscountOnCheckoutCart();
    }

    @Test
    public void verifyPlaceOrderOnRequestWithNoPurchaseOption() {
        prepareCart();
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        given(targetPurchaseOptionHelper.getCurrentPurchaseOptionModel()).willReturn(null);

        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();
        ipgPaymentInfo.setIpgToken("ipgToken");

        final TargetPlaceOrderRequest request = new TargetPlaceOrderRequest();
        request.setPaymentInfo(ipgPaymentInfo);
        request.setIpAddress("1.1.1.1");
        request.setCookieString("cookie");
        final TargetPlaceOrderResult actualResult = targetPlaceOrderFacadeImpl.placeOrder(request);
        verify(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        verify(mockCheckoutFacade, never()).validateDeliveryMode();
        verify(mockCheckoutFacade).adjustCart();
        verifyZeroInteractions(mockTargetFraudCommerceCheckoutService);
        verify(targetPaymentService).reverseGiftCardPayment(cartModel);
        assertThat(actualResult.getPlaceOrderResult()).isEqualTo(
                TargetPlaceOrderResultEnum.INVALID_CART);
        verify(flybuysDiscountFacade).reassessFlybuysDiscountOnCheckoutCart();
    }

    @Test
    public void verifyPlaceOrderOnRequestWithNoCartEntry() {
        prepareCart();
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        given(cartModel.getEntries()).willReturn(null);

        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();
        ipgPaymentInfo.setIpgToken("ipgToken");

        final TargetPlaceOrderRequest request = new TargetPlaceOrderRequest();
        request.setPaymentInfo(ipgPaymentInfo);
        request.setIpAddress("1.1.1.1");
        request.setCookieString("cookie");
        final TargetPlaceOrderResult actualResult = targetPlaceOrderFacadeImpl.placeOrder(request);
        verify(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        verify(mockCheckoutFacade, never()).validateDeliveryMode();
        verify(mockCheckoutFacade).adjustCart();
        verifyZeroInteractions(mockTargetFraudCommerceCheckoutService);
        verify(targetPaymentService).reverseGiftCardPayment(cartModel);
        assertThat(actualResult.getPlaceOrderResult()).isEqualTo(
                TargetPlaceOrderResultEnum.INVALID_CART);
        verify(flybuysDiscountFacade).reassessFlybuysDiscountOnCheckoutCart();
    }

    @Test
    public void verifyPlaceOrderOnRequestWithMissingProduct() {
        prepareCart();
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        doReturn(Boolean.TRUE).when(targetCommerceCartService).removeMissingProducts(cartModel);

        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();
        ipgPaymentInfo.setIpgToken("ipgToken");

        final TargetPlaceOrderRequest request = new TargetPlaceOrderRequest();
        request.setPaymentInfo(ipgPaymentInfo);
        request.setIpAddress("1.1.1.1");
        request.setCookieString("cookie");
        final TargetPlaceOrderResult actualResult = targetPlaceOrderFacadeImpl.placeOrder(request);
        verify(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        verify(mockCheckoutFacade, never()).validateDeliveryMode();
        verify(mockCheckoutFacade).adjustCart();
        verifyZeroInteractions(mockTargetFraudCommerceCheckoutService);
        verify(targetPaymentService).reverseGiftCardPayment(cartModel);
        assertThat(actualResult.getPlaceOrderResult()).isEqualTo(
                TargetPlaceOrderResultEnum.INVALID_PRODUCT_IN_CART);
        verify(flybuysDiscountFacade).reassessFlybuysDiscountOnCheckoutCart();
    }

    @Test
    public void verifyPlaceOrderOnRequestWithInsuffientAmount() {
        prepareCart();
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        doReturn(Boolean.TRUE).when(cartData).isInsufficientAmount();

        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();
        ipgPaymentInfo.setIpgToken("ipgToken");

        final TargetPlaceOrderRequest request = new TargetPlaceOrderRequest();
        request.setPaymentInfo(ipgPaymentInfo);
        request.setIpAddress("1.1.1.1");
        request.setCookieString("cookie");
        final TargetPlaceOrderResult actualResult = targetPlaceOrderFacadeImpl.placeOrder(request);
        verify(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        verify(mockCheckoutFacade, never()).validateDeliveryMode();
        verify(mockCheckoutFacade).adjustCart();
        verifyZeroInteractions(mockTargetFraudCommerceCheckoutService);
        verify(targetPaymentService).reverseGiftCardPayment(cartModel);
        assertThat(actualResult.getPlaceOrderResult()).isEqualTo(
                TargetPlaceOrderResultEnum.INVALID_CART);
        verify(flybuysDiscountFacade).reassessFlybuysDiscountOnCheckoutCart();
    }

    @Test
    public void verifyPlaceOrderOnRequestWithIpgTokanAndPaypalPaymentInfo() {
        prepareCart();
        given(cartModel.getDeliveryAddress()).willReturn(null);
        final AdjustedCartEntriesData adjustedData = mock(AdjustedCartEntriesData.class);
        doReturn(Boolean.TRUE).when(adjustedData).isAdjusted();
        given(mockCheckoutFacade.adjustCart()).willReturn(adjustedData);

        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();
        ipgPaymentInfo.setIpgToken("ipgToken");

        final TargetPlaceOrderRequest request = new TargetPlaceOrderRequest();
        request.setPaymentInfo(ipgPaymentInfo);
        request.setIpAddress("1.1.1.1");
        request.setCookieString("cookie");
        final TargetPlaceOrderResult actualResult = targetPlaceOrderFacadeImpl.placeOrder(request);
        verify(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        verify(mockCheckoutFacade, never()).validateDeliveryMode();
        verify(mockCheckoutFacade).adjustCart();
        verifyZeroInteractions(mockTargetFraudCommerceCheckoutService);
        verify(targetPaymentService).reverseGiftCardPayment(cartModel);
        assertThat(actualResult.getPlaceOrderResult()).isEqualTo(
                TargetPlaceOrderResultEnum.PAYMENT_METHOD_MISMATCH);
        assertThat(actualResult.getAdjustedCartEntriesData()).isEqualTo(adjustedData);
        verify(flybuysDiscountFacade).reassessFlybuysDiscountOnCheckoutCart();
    }

    @Test
    public void testCreatePaymentInfoForPayPalPayment() {
        final String payPalPayerId = "payPalPayerId";
        final String payPalSessionToken = "payPalSessionToken";

        willReturn(Boolean.TRUE).given(mockCheckoutFacade).isPaymentModePaypal();

        final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfo = new TargetPlaceOrderPayPalPaymentInfoData();
        payPalPaymentInfo.setPaypalPayerId(payPalPayerId);
        payPalPaymentInfo.setPaypalSessionToken(payPalSessionToken);

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(payPalPaymentInfo);

        targetPlaceOrderFacadeImpl.createPaymentInfo(placeOrderRequest);

        verify(mockCheckoutFacade).createPayPalPaymentInfo(payPalSessionToken, payPalPayerId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreatePaymentInfoForPayPalPaymentWrongPaymentInfoType() {
        willReturn(Boolean.TRUE).given(mockCheckoutFacade).isPaymentModePaypal();

        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(ipgPaymentInfo);

        targetPlaceOrderFacadeImpl.createPaymentInfo(placeOrderRequest);
    }

    @Test
    public void testCreatePaymentInfoForAfterpayPayment() {
        final String afterpayOrderToken = "afterpayOrderToken";

        willReturn(Boolean.TRUE).given(mockCheckoutFacade).isPaymentModeAfterpay();

        final TargetPlaceOrderAfterpayPaymentInfoData afterpayPaymentInfo = new TargetPlaceOrderAfterpayPaymentInfoData();
        afterpayPaymentInfo.setAfterpayOrderToken(afterpayOrderToken);

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(afterpayPaymentInfo);

        targetPlaceOrderFacadeImpl.createPaymentInfo(placeOrderRequest);

        verify(mockCheckoutFacade).createAfterpayPaymentInfo(afterpayOrderToken);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreatePaymentInfoForAfterpayPaymentWrongPaymentInfoType() {
        willReturn(Boolean.TRUE).given(mockCheckoutFacade).isPaymentModePaypal();

        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(ipgPaymentInfo);

        targetPlaceOrderFacadeImpl.createPaymentInfo(placeOrderRequest);
    }

    @Test
    public void testCreatePaymentInfoForNotPaypalPayment() {
        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();

        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setPaymentInfo(ipgPaymentInfo);

        targetPlaceOrderFacadeImpl.createPaymentInfo(placeOrderRequest);

        verify(mockCheckoutFacade).isPaymentModePaypal();
        verify(mockCheckoutFacade).isPaymentModeAfterpay();
        verify(mockCheckoutFacade).paymentModeEquals(TgtFacadesConstants.ZIPPAY);
        verifyNoMoreInteractions(mockCheckoutFacade);
    }

    @Test
    public void verifyPlaceOrderWithInvalidFlybuysRedemption() {
        prepareCart();
        given(cartModel.getDeliveryAddress()).willReturn(addressModel);
        doReturn(Boolean.TRUE).when(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");

        final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();
        ipgPaymentInfo.setIpgToken("ipgToken");

        final TargetPlaceOrderRequest request = new TargetPlaceOrderRequest();
        request.setPaymentInfo(ipgPaymentInfo);
        request.setIpAddress("1.1.1.1");
        request.setCookieString("cookie");
        given(Boolean.valueOf(flybuysDiscountFacade.reassessFlybuysDiscountOnCheckoutCart())).willReturn(Boolean.TRUE);
        final TargetPlaceOrderResult actualResult = targetPlaceOrderFacadeImpl.placeOrder(request);
        verify(mockCheckoutFacade).updateIpgPaymentInfoWithToken("ipgToken");
        assertThat(actualResult.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.INVALID_FLYBUYS_REDEMPTION);
        verify(flybuysDiscountFacade, times(2)).reassessFlybuysDiscountOnCheckoutCart();
    }

    @Test
    public void testPlaceOrderFundingFailureFromPaypal() throws InsufficientStockLevelException {
        final TargetPlaceOrderRequest placeOrderRequest = mock(TargetPlaceOrderRequest.class);
        given(placeOrderRequest.getIpAddress()).willReturn("mockIp");
        given(placeOrderRequest.getCookieString()).willReturn("mockCookie");
        final AdjustedCartEntriesData adjustedCartEntriesData = mock(AdjustedCartEntriesData.class);
        given(mockCheckoutFacade.adjustCart()).willReturn(adjustedCartEntriesData);
        given(mockCheckoutFacade.getCart()).willReturn(cartModel);
        final List<AbstractOrderEntryModel> abstractOrderEntryModels = new ArrayList<>();
        abstractOrderEntryModels.add(mock(AbstractOrderEntryModel.class));
        given(cartModel.getEntries()).willReturn(abstractOrderEntryModels);
        given(mockCheckoutFacade.getCheckoutCart()).willReturn(cartData);
        given(targetPurchaseOptionHelper.getCurrentPurchaseOptionModel()).willReturn(purchaseOptionModel);
        willThrow(new BadFundingMethodException("")).given(mockTargetFraudCommerceCheckoutService)
                .beforePlaceOrder(cartModel, null);
        final TargetPlaceOrderResult result = targetPlaceOrderFacadeImpl.placeOrder(placeOrderRequest);
        assertThat(result).isNotNull();
        assertThat(result.getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.FUNDING_FAILURE);
    }

    @Test
    public void testCompareItemsWhenBothListMatches() {

        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final AbstractOrderEntryModel entry1 = createAbstractOrderEntryModel(1L, "123456");
        final AbstractOrderEntryModel entry2 = createAbstractOrderEntryModel(2L, "999999");
        entries.add(entry1);
        entries.add(entry2);
        final List<LineItem> lineItems = new ArrayList<>();
        final LineItem lineItem1 = createLineItem(1L, "123456");
        final LineItem lineItem2 = createLineItem(2L, "999999");
        lineItems.add(lineItem1);
        lineItems.add(lineItem2);
        final boolean result = targetPlaceOrderFacadeImpl.compareItems(entries, lineItems);

        assertThat(result).isTrue();
    }

    @Test
    public void testCompareItemsWhenQuantityDiffers() {

        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final AbstractOrderEntryModel entry1 = createAbstractOrderEntryModel(2L, "123456");
        final AbstractOrderEntryModel entry2 = createAbstractOrderEntryModel(2L, "999999");
        entries.add(entry1);
        entries.add(entry2);
        final List<LineItem> lineItems = new ArrayList<>();
        final LineItem lineItem1 = createLineItem(1L, "123456");
        final LineItem lineItem2 = createLineItem(2L, "999999");
        lineItems.add(lineItem1);
        lineItems.add(lineItem2);
        final boolean result = targetPlaceOrderFacadeImpl.compareItems(entries, lineItems);

        assertThat(result).isFalse();
    }

    @Test
    public void testCompareItemsWhenProductCodeDiffers() {

        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final AbstractOrderEntryModel entry1 = createAbstractOrderEntryModel(1L, "123456");
        final AbstractOrderEntryModel entry2 = createAbstractOrderEntryModel(2L, "999999");
        entries.add(entry1);
        entries.add(entry2);
        final List<LineItem> lineItems = new ArrayList<>();
        final LineItem lineItem1 = createLineItem(1L, "123456");
        final LineItem lineItem2 = createLineItem(2L, "2222222");
        lineItems.add(lineItem1);
        lineItems.add(lineItem2);
        final boolean result = targetPlaceOrderFacadeImpl.compareItems(entries, lineItems);

        assertThat(result).isFalse();
    }

    @Test
    public void testBeforePlaceOrderWithAfterpayWhenTotalMismatch() throws InsufficientStockLevelException {
        given(cartModel.getPaymentInfo()).willReturn(afterpayPaymentInfoModel);
        given(paymentMethodStrategy.getPaymentMethod(afterpayPaymentInfoModel)).willReturn(afterpayPaymentMethod);

        final TargetGetSubscriptionResult result = mock(TargetGetSubscriptionResult.class);
        // using any() as object is created inside the method
        given(afterpayPaymentMethod.getSubscription(any(SubscriptionDataRequest.class))).willReturn(result);
        given(result.getAmount()).willReturn(new BigDecimal("12.00"));

        given(cartModel.getTotalPrice()).willReturn(new Double("10.00"));

        final TargetPlaceOrderResultEnum beforePlaceOrder = targetPlaceOrderFacadeImpl.beforePlaceOrder(cartModel);
        assertThat(beforePlaceOrder).isEqualTo(TargetPlaceOrderResultEnum.PAYMENT_FAILURE_DUETO_MISSMATCH_TOTALS);

    }

    @Test
    public void testBeforePlaceOrderWithAfterpayWhenEntriesMismatch() throws InsufficientStockLevelException {
        given(cartModel.getPaymentInfo()).willReturn(afterpayPaymentInfoModel);
        given(paymentMethodStrategy.getPaymentMethod(afterpayPaymentInfoModel)).willReturn(afterpayPaymentMethod);

        final TargetGetSubscriptionResult result = mock(TargetGetSubscriptionResult.class);
        final List<LineItem> lineItems = new ArrayList<>();
        final LineItem lineItem1 = createLineItem(1L, "123456");
        final LineItem lineItem2 = createLineItem(2L, "2222222");
        lineItems.add(lineItem1);
        lineItems.add(lineItem2);
        result.setLineItems(lineItems);
        // using any() as object is created inside the method
        final ArgumentCaptor<SubscriptionDataRequest> captor = ArgumentCaptor.forClass(SubscriptionDataRequest.class);
        given(afterpayPaymentMethod.getSubscription(captor.capture())).willReturn(result);
        given(result.getAmount()).willReturn(new BigDecimal("10.00"));
        given(result.getLineItems()).willReturn(lineItems);

        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final AbstractOrderEntryModel entry1 = createAbstractOrderEntryModel(1L, "123456");
        final AbstractOrderEntryModel entry2 = createAbstractOrderEntryModel(2L, "999999");
        entries.add(entry1);
        entries.add(entry2);
        given(cartModel.getTotalPrice()).willReturn(new Double("10.00"));
        given(cartModel.getEntries()).willReturn(entries);

        final TargetPlaceOrderResultEnum beforePlaceOrder = targetPlaceOrderFacadeImpl.beforePlaceOrder(cartModel);
        assertThat(beforePlaceOrder).isEqualTo(TargetPlaceOrderResultEnum.ITEM_MISMATCH_IN_CART_AFTER_PAYMENT);
        final SubscriptionDataRequest capturedValue = captor.getValue();
        assertThat(capturedValue.getSubscriptionID()).isEqualTo(afterpayPaymentInfoModel.getAfterpayToken());

    }

    @Test
    public void testBeforePlaceOrderWithAfterpayWhenSizeMismatch() throws InsufficientStockLevelException {
        given(cartModel.getPaymentInfo()).willReturn(afterpayPaymentInfoModel);
        given(paymentMethodStrategy.getPaymentMethod(afterpayPaymentInfoModel)).willReturn(afterpayPaymentMethod);

        final TargetGetSubscriptionResult result = mock(TargetGetSubscriptionResult.class);
        final List<LineItem> lineItems = new ArrayList<>();
        final LineItem lineItem1 = createLineItem(1L, "123456");
        lineItems.add(lineItem1);
        result.setLineItems(lineItems);
        // using any() as object is created inside the method
        given(afterpayPaymentMethod.getSubscription(any(SubscriptionDataRequest.class))).willReturn(result);
        given(result.getAmount()).willReturn(new BigDecimal("10.00"));
        given(result.getLineItems()).willReturn(lineItems);

        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final AbstractOrderEntryModel entry1 = createAbstractOrderEntryModel(1L, "123456");
        final AbstractOrderEntryModel entry2 = createAbstractOrderEntryModel(2L, "999999");
        entries.add(entry1);
        entries.add(entry2);
        given(cartModel.getTotalPrice()).willReturn(new Double("10.00"));
        given(cartModel.getEntries()).willReturn(entries);

        final TargetPlaceOrderResultEnum beforePlaceOrder = targetPlaceOrderFacadeImpl.beforePlaceOrder(cartModel);
        assertThat(beforePlaceOrder).isEqualTo(TargetPlaceOrderResultEnum.ITEM_MISMATCH_IN_CART_AFTER_PAYMENT);
    }

    @Test
    public void testBeforePlaceOrderWithFluentException() throws InsufficientStockLevelException {
        doThrow(new FluentOrderException()).when(mockTargetFraudCommerceCheckoutService)
                .beforePlaceOrder(cartModel, null);

        final TargetPlaceOrderResultEnum result = targetPlaceOrderFacadeImpl.beforePlaceOrder(cartModel);

        verifyZeroInteractions(mockModelService);
        assertThat(result).isEqualTo(TargetPlaceOrderResultEnum.FLUENT_ORDER_EXCEPTION);
    }

    @Test
    public void testGetQueryTransactionDetailResultsWithOrderModel() {
        final TargetQueryTransactionDetailsResult result = new TargetQueryTransactionDetailsResult();
        result.setCancel(true);

        final OrderModel mockOrderModel = mock(OrderModel.class);

        given(mockOrderModel.getPaymentInfo()).willReturn(paymentInfoModel);
        given(paymentMethodStrategy.getPaymentMethod(paymentInfoModel)).willReturn(paymentMethod);
        given(targetPaymentService.queryTransactionDetails(paymentMethod, paymentInfoModel)).willReturn(result);

        final TargetQueryTransactionDetailsResult returnedResult = targetPlaceOrderFacadeImpl
                .getQueryTransactionDetailResults(mockOrderModel);

        assertThat(returnedResult).isNotNull();
        assertThat(returnedResult.isCancel()).isTrue();
    }

    private LineItem createLineItem(final long quantity, final String productCode) {
        final LineItem lineItem = new LineItem();
        lineItem.setQuantity(quantity);
        lineItem.setProductCode(productCode);

        return lineItem;
    }

    private AbstractOrderEntryModel createAbstractOrderEntryModel(final long quantity, final String productCode) {
        final AbstractOrderEntryModel entry1 = new AbstractOrderEntryModel();
        entry1.setQuantity(new Long(quantity));
        final ProductModel product = new ProductModel();
        product.setCode(productCode);
        entry1.setProduct(product);
        return entry1;
    }

    @Test
    public void testCreatePaymentInfoZipPay(){
        given(mockCheckoutFacade.isPaymentModePaypal()).willReturn(Boolean.FALSE);
        given(mockCheckoutFacade.isPaymentModeAfterpay()).willReturn(Boolean.FALSE);
        given(mockCheckoutFacade.paymentModeEquals(TgtFacadesConstants.ZIPPAY)).willReturn(Boolean.TRUE);
        String randomToken = UUID.randomUUID().toString();

        final TargetPlaceOrderZipPaymentInfoData paymentInfoData = new TargetPlaceOrderZipPaymentInfoData();
        paymentInfoData.setCheckoutId(randomToken);
        paymentInfoData.setResult("approved");
        TargetPlaceOrderRequest request = mock(TargetPlaceOrderRequest.class);
        given(request.getPaymentInfo()).willReturn(paymentInfoData);
        targetPlaceOrderFacadeImpl.createPaymentInfo(request);

        verify(mockCheckoutFacade, never()).createAfterpayPaymentInfo(randomToken);
        verify(mockCheckoutFacade).createZipPaymentInfo(randomToken);
    }

    @Test
    public void testCreatePaymentInfoZipPayException(){
        given(mockCheckoutFacade.isPaymentModePaypal()).willReturn(Boolean.FALSE);
        given(mockCheckoutFacade.isPaymentModeAfterpay()).willReturn(Boolean.FALSE);
        given(mockCheckoutFacade.paymentModeEquals(TgtFacadesConstants.ZIPPAY)).willReturn(Boolean.TRUE);
        String randomToken = null;

        final TargetPlaceOrderAfterpayPaymentInfoData paymentInfoData = new TargetPlaceOrderAfterpayPaymentInfoData();
        TargetPlaceOrderRequest request = mock(TargetPlaceOrderRequest.class);
        given(request.getPaymentInfo()).willReturn(paymentInfoData);
        exceptionRule.expect(IllegalArgumentException.class);
        targetPlaceOrderFacadeImpl.createPaymentInfo(request);

        verify(mockCheckoutFacade, never()).createAfterpayPaymentInfo(randomToken);
        verify(mockCheckoutFacade).createZipPaymentInfo(randomToken);
    }
}