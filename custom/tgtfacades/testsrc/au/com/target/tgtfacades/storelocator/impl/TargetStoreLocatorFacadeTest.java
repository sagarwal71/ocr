/**
 * 
 */
package au.com.target.tgtfacades.storelocator.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storefinder.TargetStoreFinderService;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtfacades.order.converters.AbstractOrderHelper;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;


@SuppressWarnings("deprecation")
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetStoreLocatorFacadeTest {

    @InjectMocks
    private final TargetStoreLocatorFacadeImpl targetStoreLocatorFacade = new TargetStoreLocatorFacadeImpl();

    @Mock
    private TargetPointOfServiceService targetPointOfServiceService;

    @Mock
    private Converter<TargetPointOfServiceModel, TargetPointOfServiceData> targetPointOfServiceConverter;

    @Mock
    private TargetPointOfServiceModel pointOfServiceModel;

    @Mock
    private TargetStoreFinderService storeFinderService;

    @Mock
    private Converter<StoreFinderSearchPageData<PointOfServiceDistanceData>, StoreFinderSearchPageData<PointOfServiceData>> searchPagePointOfServiceDistanceConverter;

    @Mock
    private BaseStoreService baseStoreService;

    @Mock
    private AbstractOrderHelper abstractOrderHelper;

    @Mock
    private PageableData pageableData;

    @Mock
    private CartModel cartModel;

    @Mock
    private GeoPoint geoPoint;

    @Mock
    private BaseStoreModel baseStore;

    @Before
    public void setUp() {
        given(baseStoreService.getCurrentBaseStore()).willReturn(baseStore);
    }

    @Test
    public void testGetPointOfServiceUnknownIdentifier() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        BDDMockito.given(targetPointOfServiceService.getPOSByStoreNumber(Integer.valueOf(5566))).willThrow(
                new TargetUnknownIdentifierException(""));
        final PointOfServiceData posd = targetStoreLocatorFacade.getPointOfService(Integer.valueOf(5566));
        Assert.assertNull(posd);
    }

    @Test
    public void testGetPointOfServicetAmbiguousIdentifier() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        BDDMockito.given(targetPointOfServiceService.getPOSByStoreNumber(Integer.valueOf(5566))).willThrow(
                new TargetAmbiguousIdentifierException(""));
        final PointOfServiceData posd = targetStoreLocatorFacade.getPointOfService(Integer.valueOf(5566));
        Assert.assertNull(posd);
    }

    @Test
    public void testGetPointOfServicet() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetPointOfServiceData serviceData = new TargetPointOfServiceData();
        serviceData.setName("test-store");

        BDDMockito.given(targetPointOfServiceConverter.convert(pointOfServiceModel)).willReturn(serviceData);

        BDDMockito.given(targetPointOfServiceService.getPOSByStoreNumber(Integer.valueOf(5566))).willReturn(
                pointOfServiceModel);

        final AddressModel address = new AddressModel();
        address.setDistrict("VIC");
        BDDMockito.given(pointOfServiceModel.getAddress()).willReturn(address);

        final PointOfServiceData posd = targetStoreLocatorFacade.getPointOfService(Integer.valueOf(5566));

        Assert.assertNotNull(posd);
        Assert.assertEquals("test-store", posd.getName());
    }

    @Test
    public void testGetAllStateAndStores() {
        final Map<String, List<TargetPointOfServiceData>> pointOfServiceData = new TreeMap<String, List<TargetPointOfServiceData>>();
        final Map<String, Set<TargetPointOfServiceModel>> storesModel = new TreeMap<String, Set<TargetPointOfServiceModel>>();
        BDDMockito.given(targetPointOfServiceService.getAllStateAndStores()).willReturn(
                storesModel);
        final TargetStoreLocatorFacadeImpl targetStoreLocatorFacadeSpy = spy(targetStoreLocatorFacade);
        BDDMockito.given(targetStoreLocatorFacadeSpy.convertToTargetPointOfServiceData(Mockito.anyMap())).willReturn(
                pointOfServiceData);
        final Map<String, List<TargetPointOfServiceData>> resultMap = targetStoreLocatorFacadeSpy
                .getAllStateAndStores();
        Assert.assertEquals(pointOfServiceData, resultMap);
    }

    @Test
    public void testGetStoresByStateNull() {
        BDDMockito.given(targetPointOfServiceService.getAllStateAndStores()).willReturn(
                null);
        final List result = targetStoreLocatorFacade.getStoresByState("not-a-real-state");
        Assert.assertEquals(Collections.emptyList(), result);
    }

    @Test
    public void testGetStoresByStateEmpty() {
        final Map<String, Set<TargetPointOfServiceModel>> storesModel = new TreeMap<String, Set<TargetPointOfServiceModel>>();
        BDDMockito.given(targetPointOfServiceService.getAllStateAndStores()).willReturn(
                storesModel);
        final List result = targetStoreLocatorFacade.getStoresByState("not-a-real-state");
        Assert.assertEquals(Collections.emptyList(), result);
    }

    @Test
    public void testGetStoresByState() {
        final Map<String, Set<TargetPointOfServiceModel>> storesModel = new HashMap<String, Set<TargetPointOfServiceModel>>();
        storesModel.put("VIC", generateStores(2));
        storesModel.put("SA", generateStores(3));
        storesModel.put("TAS", generateStores(4));
        storesModel.put("QLD", generateStores(5));
        BDDMockito.given(targetPointOfServiceService.getAllStateAndStores()).willReturn(
                storesModel);
        Assert.assertEquals(storesModel.get("VIC").size(), targetStoreLocatorFacade.getStoresByState("VIC").size());
        Assert.assertEquals(storesModel.get("SA").size(), targetStoreLocatorFacade.getStoresByState("SA").size());
        Assert.assertEquals(storesModel.get("TAS").size(), targetStoreLocatorFacade.getStoresByState("TAS").size());
        Assert.assertEquals(storesModel.get("QLD").size(), targetStoreLocatorFacade.getStoresByState("QLD").size());
        Assert.assertEquals(Collections.emptyList(), targetStoreLocatorFacade.getStoresByState("NUP"));
    }

    @Test
    public void testGetCncStoresByPositionSearch() {
        final List<TargetPointOfServiceModel> storesModel = Mockito.mock(List.class);
        final StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageData = Mockito
                .mock(StoreFinderSearchPageData.class);
        BDDMockito.given(targetPointOfServiceService.getStoresForCart(cartModel)).willReturn(
                storesModel);
        BDDMockito.given(storeFinderService.doSearch(null, geoPoint, pageableData, storesModel)).willReturn(
                storeFinderSearchPageData);

        targetStoreLocatorFacade.getCncStoresByPositionSearch(cartModel, geoPoint, pageableData);
        Mockito.verify(targetPointOfServiceService).getStoresForCart(cartModel);
        Mockito.verify(storeFinderService).doSearch(null, geoPoint, pageableData, storesModel);
        Mockito.verify(searchPagePointOfServiceDistanceConverter).convert(storeFinderSearchPageData);
    }


    @Test
    public void testGetCncStoresByPostCode() {
        final String locationText = "Test";
        final List<TargetPointOfServiceModel> storesModel = Mockito.mock(List.class);
        final StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageData = Mockito
                .mock(StoreFinderSearchPageData.class);
        BDDMockito.given(baseStoreService.getCurrentBaseStore()).willReturn(baseStore);
        BDDMockito.given(targetPointOfServiceService.getStoresForCart(cartModel)).willReturn(
                storesModel);
        BDDMockito.given(storeFinderService.doSearchByLocation(baseStore, locationText, pageableData, storesModel))
                .willReturn(
                        storeFinderSearchPageData);

        targetStoreLocatorFacade.getCncStoresByPostCode(locationText, cartModel, pageableData);
        Mockito.verify(targetPointOfServiceService).getStoresForCart(cartModel);
        Mockito.verify(storeFinderService).doSearchByLocation(baseStore, locationText, pageableData, storesModel);
        Mockito.verify(searchPagePointOfServiceDistanceConverter).convert(storeFinderSearchPageData);
    }

    @Test
    public void testSearchStoresWithCncAvailbilityWithNoResult() {
        final List<TargetPointOfServiceModel> storesModel = Mockito.mock(List.class);
        given(storeFinderService.doSearchByLocation(baseStore, "location", pageableData, storesModel))
                .willReturn(null);
        given(this.targetPointOfServiceService.getAllOpenStores()).willReturn(storesModel);
        given(searchPagePointOfServiceDistanceConverter.convert(null)).willReturn(null);
        final StoreFinderSearchPageData<PointOfServiceData> pageResult = targetStoreLocatorFacade
                .searchStoresWithCncAvailability("location", cartModel, pageableData);
        assertNull(pageResult);
    }

    @Test
    public void testSearchStoresWithCncAvailbilityWithEmptyResult() {
        final List<TargetPointOfServiceModel> storesModel = Mockito.mock(List.class);
        final StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageDistanceData = mock(
                StoreFinderSearchPageData.class);
        final StoreFinderSearchPageData<PointOfServiceData> storeFinderSearchPageData = mock(
                StoreFinderSearchPageData.class);
        given(searchPagePointOfServiceDistanceConverter.convert(storeFinderSearchPageDistanceData)).willReturn(
                storeFinderSearchPageData);
        given(this.targetPointOfServiceService.getAllOpenStores()).willReturn(storesModel);
        given(storeFinderService.doSearchByLocation(baseStore, "location", pageableData, storesModel))
                .willReturn(storeFinderSearchPageDistanceData);
        given(storeFinderSearchPageData.getResults()).willReturn(null);
        final StoreFinderSearchPageData<PointOfServiceData> pageResult = targetStoreLocatorFacade
                .searchStoresWithCncAvailability("location", cartModel, pageableData);
        assertNotNull(pageResult);
        Assertions.assertThat(pageResult.getResults()).isNullOrEmpty();
    }


    @Test
    public void testSearchStoresWithCncAvailbility() {
        final List<TargetPointOfServiceModel> storesModel = Mockito.mock(List.class);
        final StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageDistanceData = mock(
                StoreFinderSearchPageData.class);
        final StoreFinderSearchPageData<PointOfServiceData> storeFinderSearchPageData = mock(
                StoreFinderSearchPageData.class);
        final Set<String> cartProductTypes = mock(Set.class);
        final TargetPointOfServiceData store = mock(TargetPointOfServiceData.class);
        given(abstractOrderHelper.getProductTypeSet(cartModel)).willReturn(cartProductTypes);
        given(searchPagePointOfServiceDistanceConverter.convert(storeFinderSearchPageDistanceData)).willReturn(
                storeFinderSearchPageData);
        given(this.targetPointOfServiceService.getAllOpenStores()).willReturn(storesModel);
        given(storeFinderService.doSearchByLocation(baseStore, "location", pageableData, storesModel))
                .willReturn(storeFinderSearchPageDistanceData);
        given(storeFinderSearchPageData.getResults()).willReturn(
                ImmutableList.of((PointOfServiceData)store));
        final StoreFinderSearchPageData<PointOfServiceData> pageResult = targetStoreLocatorFacade
                .searchStoresWithCncAvailability("location", cartModel, pageableData);
        final List<PointOfServiceData> stores = pageResult.getResults();
        assertEquals(1, stores.size());
        verify(abstractOrderHelper).updateStoreWithPickupAvailability(cartProductTypes, store);
    }

    private Set<TargetPointOfServiceModel> generateStores(final int num) {
        final Set<TargetPointOfServiceModel> state = new HashSet<>();
        for (int i = 0; i < num; i++) {
            final TargetPointOfServiceModel targetPointOfServiceModel = Mockito.mock(TargetPointOfServiceModel.class);
            state.add(targetPointOfServiceModel);
        }
        return state;
    }

}
