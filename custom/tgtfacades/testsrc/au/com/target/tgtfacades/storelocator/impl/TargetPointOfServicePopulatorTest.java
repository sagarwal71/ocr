/**
 * 
 */
package au.com.target.tgtfacades.storelocator.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.commercefacades.storelocator.data.OpeningScheduleData;
import de.hybris.platform.commercefacades.storelocator.data.WeekdayOpeningDayData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.postcode.service.TargetPostCodeService;
import au.com.target.tgtfacades.config.TargetSharedConfigFacade;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.storelocator.converters.populator.TargetPointOfServicePopulator;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtfacades.url.impl.TargetPointOfServiceUrlResolver;
import au.com.target.tgtfacades.util.TargetPointOfServiceHelper;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPointOfServicePopulatorTest {

    @InjectMocks
    private final TargetPointOfServicePopulator targetPointOfServicePopulator = new TargetPointOfServicePopulator();

    @Mock
    private TypeService mockTypeService;

    @Mock
    private Converter<AddressModel, AddressData> mockAddressConverter;

    @Mock
    private TargetPointOfServiceUrlResolver resolver;

    @Mock
    private TargetPostCodeService targetPostCodeService;

    @Mock
    private TargetPointOfServiceHelper helper;

    @Mock
    private TargetSharedConfigFacade targetSharedConfigFacade;

    @Mock
    private AbstractPopulatingConverter<OpeningScheduleModel, OpeningScheduleData> openingScheduleConverter;

    @Test
    public void testPopulate() {

        final TargetPointOfServiceModel source = createPointOfServiceModel();
        final AddressModel mockAddressModel = Mockito.mock(AddressModel.class);
        given(mockAddressModel.getDistrict()).willReturn("VIC");
        source.setAddress(mockAddressModel);
        final AddressData addressData = new AddressData();
        addressData.setFirstName("Seabook");
        addressData.setLastName("Chen");
        addressData.setLine1("Geelong");
        addressData.setPostalCode("3025");
        given(mockAddressConverter.convert(mockAddressModel)).willReturn(addressData);
        given(resolver.resolve(source)).willReturn("/store/vic/mt-druitt/100");
        final PostCodeModel postCodeModel = Mockito.mock(PostCodeModel.class);
        given(postCodeModel.getTimeZoneId()).willReturn("AEST");
        given(targetPostCodeService.getPostCode(addressData.getPostalCode())).willReturn(postCodeModel);
        final TargetPointOfServiceData target = new TargetPointOfServiceData();
        target.setAddress(addressData);
        // run method
        targetPointOfServicePopulator.populate(source, target);
        // assert
        assertThat(target.getAcceptCNC().booleanValue()).isTrue();
        assertThat(target.getClosed().booleanValue()).isTrue();
        assertThat(target.getAcceptLayBy().booleanValue()).isTrue();
        assertThat("Seabook").isEqualTo(target.getTargetAddressData().getFirstName());
        assertThat("Chen").isEqualTo(target.getTargetAddressData().getLastName());
        assertThat("Geelong").isEqualTo(target.getTargetAddressData().getLine1());
        assertThat("VIC").isEqualTo(target.getTargetAddressData().getState());
        assertThat("AEST").isEqualTo(target.getTimeZone());
        assertThat(target.getUrl()).isNotNull();
        assertThat(1).isEqualTo(target.getAcceptedProductTypes().size());
        assertThat(target.getAvailableForPickup()).isNull();
        verify(helper).populateDirectionUrl(StringUtils.EMPTY, target);
    }

    @Test
    public void testPopulateWithNoAcceptedProductTypes() {
        final TargetPointOfServiceModel source = createPointOfServiceModel();
        source.setProductTypes(null);
        final TargetPointOfServiceData target = new TargetPointOfServiceData();
        // run method
        targetPointOfServicePopulator.populate(source, target);
        assertThat(target.getAcceptedProductTypes()).isNull();
        assertThat(target.getAvailableForPickup()).isNull();
    }

    /**
     * Method to populate and verify short lead time. Here short lead time value verify to given value.
     */
    @Test
    public void testPopulateWithShortLeadTime() {
        final TargetPointOfServiceModel source = createPointOfServiceModel();
        source.setShortLeadTime(Integer.valueOf(10));
        final TargetPointOfServiceData target = new TargetPointOfServiceData();
        targetPointOfServicePopulator.populate(source, target);
        assertThat(target.getShortLeadTime()).as("Short Lead Time").isEqualTo(Integer.valueOf(10));
    }

    /**
     * Method to populate and verify default short lead time. Here short lead time value verify to 3.
     */
    @Test
    public void testPopulateWithDefaultShortLeadTime() {
        final TargetPointOfServiceModel source = mock(TargetPointOfServiceModel.class);
        given(source.getShortLeadTime()).willReturn(null);
        willReturn(Integer.valueOf(3)).given(targetSharedConfigFacade).getInt(
                TgtFacadesConstants.ShortLeadTime.LEADTIME_DEFAULT_CONFIG_CODE,
                TgtFacadesConstants.ShortLeadTime.DEFAULT_SHORT_LEAD_TIME_VALUE);
        final TargetPointOfServiceData target = new TargetPointOfServiceData();
        targetPointOfServicePopulator.populate(source, target);
        assertThat(target.getShortLeadTime()).as("Short Lead Time").isEqualTo(Integer.valueOf(3));
    }

    /**
     * Method to verify target weekdays opening hours. Here method will populate target store hours.
     */
    @Test
    public void testPopulateOpeningHoursWithOpeningScheduleData() {
        final TargetPointOfServiceModel source = mock(TargetPointOfServiceModel.class);
        final TargetPointOfServiceData target = new TargetPointOfServiceData();
        final OpeningScheduleModel openingScheduleSource = mock(OpeningScheduleModel.class);
        willReturn(openingScheduleSource).given(source).getOpeningSchedule();
        final OpeningScheduleData openingScheduleTarget = new OpeningScheduleData();
        openingScheduleTarget.setCode("target-hours-0");
        openingScheduleTarget.setName("Test_Schedule_Name");
        final List<WeekdayOpeningDayData> weekDayOpeningList = new ArrayList<>();
        final WeekdayOpeningDayData weekDayOpeningDay = new WeekdayOpeningDayData();
        weekDayOpeningDay.setClosed(true);
        weekDayOpeningDay.setWeekDay("Sunday");
        weekDayOpeningList.add(weekDayOpeningDay);
        openingScheduleTarget.setWeekDayOpeningList(weekDayOpeningList);
        targetPointOfServicePopulator.setOpeningScheduleConverter(openingScheduleConverter);
        given(openingScheduleConverter.convert(openingScheduleSource)).willReturn(openingScheduleTarget);
        targetPointOfServicePopulator.populate(source, target);
        final OpeningScheduleData openingSchedule = target.getOpeningHours();
        assertThat(openingSchedule).isNotNull();
        assertThat(openingSchedule.getCode()).isEqualTo("target-hours-0");
        assertThat(openingSchedule.getName()).isEqualTo("Test_Schedule_Name");
        assertThat(openingSchedule.getWeekDayOpeningList()).contains(weekDayOpeningDay);
    }

    /**
     * Method to verify target weekdays opening hours. Here method will not return store hours and evaluated to null.
     */
    @Test
    public void testPopulateOpeningHoursWithoutOpeningScheduleData() {
        final TargetPointOfServiceModel source = mock(TargetPointOfServiceModel.class);
        final TargetPointOfServiceData target = new TargetPointOfServiceData();
        targetPointOfServicePopulator.populate(source, target);
        assertThat(target.getOpeningHours()).isNull();
    }

    private TargetPointOfServiceModel createPointOfServiceModel() {
        // set up TargetPointOfServiceModel
        final TargetPointOfServiceModel source = new TargetPointOfServiceModel();
        source.setAcceptCNC(Boolean.TRUE);
        source.setAcceptLayBy(Boolean.TRUE);
        source.setClosed(Boolean.TRUE);
        source.setType(PointOfServiceTypeEnum.TARGETCOUNTRY);
        source.setName("Mt.Druitt");
        source.setStoreNumber(Integer.valueOf(10));
        source.setShortLeadTime(Integer.valueOf(3));
        final Set<ProductTypeModel> acceptedProductTypeSet = new HashSet<>();
        final ProductTypeModel normal = new ProductTypeModel();
        normal.setCode("normal");
        acceptedProductTypeSet.add(normal);
        source.setProductTypes(acceptedProductTypeSet);
        final EnumerationValueModel evm = Mockito.mock(EnumerationValueModel.class);
        given(mockTypeService.getEnumerationValue(PointOfServiceTypeEnum._TYPECODE, source.getType().getCode()))
                .willReturn(evm);
        given(evm.getName()).willReturn("Target");
        return source;
    }
}
