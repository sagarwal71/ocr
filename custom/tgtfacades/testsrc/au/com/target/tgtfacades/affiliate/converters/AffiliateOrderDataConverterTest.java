/**
 * 
 */
package au.com.target.tgtfacades.affiliate.converters;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.affiliate.data.AffiliateOrderData;
import au.com.target.tgtfacades.order.data.FlybuysDiscountData;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.order.data.TargetOrderEntryData;


/**
 * Unit test for {@link AffiliateOrderDataConverter}
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AffiliateOrderDataConverterTest {

    private final AffiliateOrderDataConverter converter = new AffiliateOrderDataConverter();

    private PriceData price1;

    private final BigDecimal priceValue1 = BigDecimal.valueOf(4.5);

    private TargetOrderData orderData;

    private PriceData price2;

    private final BigDecimal priceValue2 = BigDecimal.valueOf(2);

    private PriceData zeroPrice;

    private FlybuysDiscountData flybuysDiscountData;

    @Mock
    private AbstractOrderModel source;


    @Before
    public void setup() {

        price1 = new PriceData();
        price1.setValue(priceValue1);
        price2 = new PriceData();
        price2.setValue(priceValue2);
        zeroPrice = new PriceData();
        zeroPrice.setValue(BigDecimal.ZERO);

        orderData = new TargetOrderData();
        orderData.setEntries(new ArrayList<OrderEntryData>());

        flybuysDiscountData = new FlybuysDiscountData();
        flybuysDiscountData.setValue(price2);

        converter.setDelimiter("|");
        converter.setDiscountLabel("Discount");
    }

    @Test
    public void testGetOrderDiscounts() {

        orderData.setOrderDiscounts(price1);
        final BigDecimal result = converter.getOrderDiscounts(orderData);

        assertThat(result).isEqualTo(priceValue1);
    }

    @Test
    public void testGetOrderDiscountsWithNullOrderDiscounts() {

        orderData.setOrderDiscounts(null);
        final BigDecimal result = converter.getOrderDiscounts(orderData);

        assertThat(result).isNull();
    }

    @Test
    public void testGetOrderDiscountsWithNullOrderDiscountsValue() {

        orderData.setOrderDiscounts(price1);
        price1.setValue(null);
        final BigDecimal result = converter.getOrderDiscounts(orderData);

        assertThat(result).isNull();
    }

    @Test
    public void testGetFlybuysDiscount() {

        orderData.setFlybuysDiscountData(flybuysDiscountData);
        final BigDecimal result = converter.getFlybuysDiscount(orderData);

        assertThat(result).isEqualTo(priceValue2);
    }

    @Test
    public void testGetFlybuysDiscountWithNullFlybuys() {

        orderData.setFlybuysDiscountData(null);
        final BigDecimal result = converter.getFlybuysDiscount(orderData);

        assertThat(result).isNull();
    }

    @Test
    public void testGetFlybuysDiscountWithNullFlybuysValue() {

        orderData.setFlybuysDiscountData(flybuysDiscountData);
        price2.setValue(null);
        final BigDecimal result = converter.getFlybuysDiscount(orderData);

        assertThat(result).isNull();
    }

    @Test
    public void testGetDiscountExcludingFlybuys() {

        orderData.setOrderDiscounts(price1);
        orderData.setFlybuysDiscountData(flybuysDiscountData);
        final BigDecimal result = converter.getDiscountExcludingFlybuys(orderData);

        assertThat(result.longValue()).isEqualTo(priceValue1.subtract(priceValue2).longValue());
    }

    @Test
    public void testGetDiscountExcludingFlybuysWithNoDiscount() {

        orderData.setOrderDiscounts(null);
        orderData.setFlybuysDiscountData(flybuysDiscountData);
        final BigDecimal result = converter.getDiscountExcludingFlybuys(orderData);

        assertThat(result).isNull();
    }

    @Test
    public void testGetDiscountExcludingFlybuysWithNoFlybuys() {

        orderData.setOrderDiscounts(price1);
        orderData.setFlybuysDiscountData(null);
        final BigDecimal result = converter.getDiscountExcludingFlybuys(orderData);

        assertThat(result).isEqualTo(priceValue1);
    }

    @Test
    public void testPopulateNoEntries() {

        converter.populate(source, orderData);

        assertThat(orderData.getAffiliateOrderData()).isNull();
    }

    @Test
    public void testPopulateNoProductCode() {

        addOrderEntry(null, Long.valueOf(1), price1);
        converter.populate(source, orderData);

        assertThat(orderData.getAffiliateOrderData()).isNull();
    }

    @Test
    public void testPopulateNoQty() {

        addOrderEntry("1234", null, price1);
        converter.populate(source, orderData);

        assertThat(orderData.getAffiliateOrderData()).isNull();
    }

    @Test
    public void testPopulateNoPrice() {

        addOrderEntry("1234", Long.valueOf(1), null);
        converter.populate(source, orderData);

        assertThat(orderData.getAffiliateOrderData()).isNull();
    }

    @Test
    public void testPopulate() {

        addOrderEntry("1231", Long.valueOf(1), price1);
        addOrderEntry("1232", Long.valueOf(2), price2);
        orderData.setOrderDiscounts(price1);
        converter.populate(source, orderData);

        final AffiliateOrderData data = orderData.getAffiliateOrderData();
        assertThat(data).isNotNull();
        assertThat(data.getCodes()).isNotNull().isEqualTo("1231|1232|Discount");
        assertThat(data.getQuantities()).isNotNull().isEqualTo("1|2|0");
        assertThat(data.getPrices()).isNotNull().isEqualTo("450|200|-450");
    }

    @Test
    public void testPopulateWithZeroDiscount() {

        addOrderEntry("1231", Long.valueOf(1), price1);
        addOrderEntry("1232", Long.valueOf(2), price2);
        orderData.setOrderDiscounts(zeroPrice);
        converter.populate(source, orderData);

        final AffiliateOrderData data = orderData.getAffiliateOrderData();
        assertThat(data).isNotNull();
        assertThat(data.getCodes()).isNotNull().isEqualTo("1231|1232");
        assertThat(data.getQuantities()).isNotNull().isEqualTo("1|2");
        assertThat(data.getPrices()).isNotNull().isEqualTo("450|200");
    }

    @Test
    public void testPopulateWithTargetOrderEntryDataSameCategory() {

        addTargetOrderEntry("1231", Long.valueOf(1), price1, "Apparel");
        addTargetOrderEntry("1232", Long.valueOf(2), price2, "Apparel");
        orderData.setOrderDiscounts(price1);
        converter.populate(source, orderData);

        final AffiliateOrderData data = orderData.getAffiliateOrderData();
        assertThat(data).isNotNull();
        assertThat(data.getCodes()).isNotNull().isEqualTo("Apparel|Discount");
        assertThat(data.getQuantities()).isNotNull().isEqualTo("3|0");
        assertThat(data.getPrices()).isNotNull().isEqualTo("650|-450");
    }

    @Test
    public void testPopulateWithTargetOrderEntryDataDiffCategory() {

        addTargetOrderEntry("1231", Long.valueOf(1), price1, "Apparel");
        addTargetOrderEntry("1232", Long.valueOf(2), price2, "Kids");
        orderData.setOrderDiscounts(price1);
        converter.populate(source, orderData);

        final AffiliateOrderData data = orderData.getAffiliateOrderData();
        assertThat(data).isNotNull();
        assertThat(data.getCodes()).isNotNull().isEqualTo("Apparel|Kids|Discount");
        assertThat(data.getQuantities()).isNotNull().isEqualTo("1|2|0");
        assertThat(data.getPrices()).isNotNull().isEqualTo("450|200|-450");
    }

    @Test
    public void testPopulateWithTargetOrderEntryDataWith2SameCategoryOneDiffCategory() {

        addTargetOrderEntry("1231", Long.valueOf(1), price1, "Apparel");
        addTargetOrderEntry("1232", Long.valueOf(2), price2, "Kids");
        addTargetOrderEntry("3245", Long.valueOf(2), price2, "Apparel");
        orderData.setOrderDiscounts(price1);
        converter.populate(source, orderData);

        final AffiliateOrderData data = orderData.getAffiliateOrderData();
        assertThat(data).isNotNull();
        assertThat(data.getCodes()).isNotNull().isEqualTo("Apparel|Kids|Discount");
        assertThat(data.getQuantities()).isNotNull().isEqualTo("3|2|0");
        assertThat(data.getPrices()).isNotNull().isEqualTo("650|200|-450");
    }

    private void addTargetOrderEntry(final String code, final Long qty, final PriceData price,
            final String affiliateCategory) {
        final TargetOrderEntryData entry = new TargetOrderEntryData();
        final ProductData product = new ProductData();
        product.setCode(code);
        entry.setTotalPrice(price);
        entry.setProduct(product);
        entry.setQuantity(qty);
        entry.setAffiliateOfferCategory(affiliateCategory);
        orderData.getEntries().add(entry);
    }


    private void addOrderEntry(final String code, final Long qty, final PriceData price) {
        final OrderEntryData entry = new OrderEntryData();
        final ProductData product = new ProductData();
        product.setCode(code);

        entry.setTotalPrice(price);
        entry.setProduct(product);
        entry.setQuantity(qty);

        orderData.getEntries().add(entry);
    }

}
