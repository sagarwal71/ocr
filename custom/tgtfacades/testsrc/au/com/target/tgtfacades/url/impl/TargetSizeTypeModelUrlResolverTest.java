/**
 * 
 */
package au.com.target.tgtfacades.url.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;
import au.com.target.tgtfacades.url.transform.UrlTransformer;

import com.google.common.collect.ImmutableList;


/**
 * @author manoj
 *
 */
/**
 * Unit test for {@link TargetSizeTypeModelUrlResolver}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetSizeTypeModelUrlResolverTest {
    private static final String URL_PATTERN = "/size-chart/{sizetype.code}";

    @InjectMocks
    private final TargetSizeTypeModelUrlResolver resolver = new TargetSizeTypeModelUrlResolver();
    @Mock
    private UrlTransformer urlTransformer;
    @Mock
    private UrlTokenTransformer urlTokenTransformer;
    @Mock
    private SizeTypeModel typeModel;

    /**
     * Initializes resolver by setting specific url pattern and applied transformers.
     */
    @Before
    public void setUp() {
        resolver.setPattern(URL_PATTERN);
        resolver.setUrlTransformers(ImmutableList.of(urlTransformer));
        resolver.setUrlTokenTransformers(ImmutableList.of(urlTokenTransformer));
    }

    /**
     * Verifies that url level transformations are applied.
     */
    @Test
    public void testUrlTransformation() {
        final String sizeCode = "babysize";
        when(typeModel.getCode()).thenReturn(sizeCode);
        resolver.resolveInternal(typeModel);
        verify(urlTransformer).transform("/size-chart/" + sizeCode);
    }

    /**
     * Verifies that url level transformations are applied.
     */
    @Test
    public void testUrlTransformationSpecialCharactor() {
        final String sizeCode = "baby_size";
        when(typeModel.getCode()).thenReturn(sizeCode);
        final String strURL = resolver.resolveInternal(typeModel);
        Assert.assertNull(strURL);
    }

    /**
     * Verifies that null parameter.
     */
    @Test
    public void testSizeTypeNull() {
        final String strURL = resolver.resolveInternal(null);
        Assert.assertNull(strURL);
    }

    /**
     * Verifies that url token level transformations are applied.
     */
    @Test
    public void testUrlTokenTransformation() {
        final String token = "baby_siz_e";
        resolver.urlSafe(token);
        verify(urlTokenTransformer).transform(token);
    }

}
