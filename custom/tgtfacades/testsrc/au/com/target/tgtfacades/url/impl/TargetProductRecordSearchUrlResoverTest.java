/**
 * 
 */
package au.com.target.tgtfacades.url.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtfacades.url.transform.TargetUrlTokenTransformer;
import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;

import com.endeca.infront.cartridge.model.Attribute;
import com.endeca.infront.cartridge.model.Record;
import org.apache.commons.lang.StringUtils;
import com.google.common.collect.ImmutableList;


/**
 * @author bhuang3
 *
 */
@UnitTest
public class TargetProductRecordSearchUrlResoverTest {

    private static final String COLOUR_VARIANT_CODE = "colourVariantCode";
    private static final String PRODUCT_NAME = "productName";

    private final TargetProductRecordSearchUrlResover resolver = new TargetProductRecordSearchUrlResover();

    private final UrlTokenTransformer urlTokenTransformer = new TargetUrlTokenTransformer();

    @Before
    public void setUp()
    {
        resolver.setUrlTokenTransformers(ImmutableList.of(urlTokenTransformer));
    }

    @Test
    public void testResolveInternal() {
        final Record record = new Record();
        final Map<String, Attribute> attributeMap = new HashMap<String, Attribute>();
        final Attribute colourVariantCodeAttribute = new Attribute();
        colourVariantCodeAttribute.add("testColourVariantCode");
        attributeMap.put(COLOUR_VARIANT_CODE, colourVariantCodeAttribute);
        final Attribute productNameAttribute = new Attribute();
        productNameAttribute.add("testproductname");
        attributeMap.put(PRODUCT_NAME, productNameAttribute);
        record.setAttributes(attributeMap);

        final String result = resolver.resolveInternal(record);
        Assert.assertEquals("/p/testproductname/testColourVariantCode", result);
    }

    @Test
    public void testResolveInternalWithNullAttributeMap() {
        final Record record = new Record();
        final Map<String, Attribute> attributeMap = new HashMap<String, Attribute>();
        record.setAttributes(attributeMap);

        final String result = resolver.resolveInternal(record);
        Assert.assertEquals(StringUtils.EMPTY, result);
    }

    @Test
    public void testResolveInternalWithSpaceInProductName() {
        final Record record = new Record();
        final Map<String, Attribute> attributeMap = new HashMap<String, Attribute>();
        final Attribute colourVariantCodeAttribute = new Attribute();
        colourVariantCodeAttribute.add("testColourVariantCode");
        attributeMap.put(COLOUR_VARIANT_CODE, colourVariantCodeAttribute);
        final Attribute productNameAttribute = new Attribute();
        productNameAttribute.add("test product name");
        attributeMap.put(PRODUCT_NAME, productNameAttribute);
        record.setAttributes(attributeMap);

        final String result = resolver.resolveInternal(record);
        Assert.assertEquals("/p/test-product-name/testColourVariantCode", result);
    }

    @Test
    public void testResolveInternalWithSpaceAndCapitalInProductName() {
        final Record record = new Record();
        final Map<String, Attribute> attributeMap = new HashMap<String, Attribute>();
        final Attribute colourVariantCodeAttribute = new Attribute();
        colourVariantCodeAttribute.add("testColourVariantCode");
        attributeMap.put(COLOUR_VARIANT_CODE, colourVariantCodeAttribute);
        final Attribute productNameAttribute = new Attribute();
        productNameAttribute.add("Test Product Name");
        attributeMap.put(PRODUCT_NAME, productNameAttribute);
        record.setAttributes(attributeMap);

        final String result = resolver.resolveInternal(record);
        Assert.assertEquals("/p/test-product-name/testColourVariantCode", result);
    }

}
