package au.com.target.tgtfacades.url.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;
import au.com.target.tgtfacades.url.transform.UrlTransformer;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;


/**
 * Test suite for {@link TargetCategoryModelUrlResolver}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCategoryModelUrlResolverTest {


    private static final String URL_PATTERN = "/c/{category-path}/{category-code}";

    @InjectMocks
    private final TargetCategoryModelUrlResolver resolver = new TargetCategoryModelUrlResolver();

    @Mock
    private UrlTransformer urlTransformer;
    @Mock
    private UrlTokenTransformer urlTokenTransformer;
    @Mock
    private CommerceCategoryService commerceCategoryService;


    /**
     * Initializes resolver by setting specific url pattern and applied transformers.
     */
    @Before
    public void setUp() {
        resolver.setPattern(URL_PATTERN);
        resolver.setUrlTransformers(ImmutableList.of(urlTransformer));
        resolver.setUrlTokenTransformers(ImmutableList.of(urlTokenTransformer));
    }

    /**
     * Verifies that url level transformations are applied.
     */
    @Test
    public void testUrlTransformationWithAllProductsCategory() {
        final String categoryCode = "W12345";
        final String allProductsName = "/All-Products";
        final String url = "/Body-Beauty/";
        final CategoryModel category = mockCategory(categoryCode, allProductsName + url);
        resolver.resolveInternal(category);
        verify(urlTransformer).transform("/c" + url + categoryCode);
    }



    /**
     * Verifies that url level transformations are applied.
     */
    @Test
    public void testUrlTransformationForAllProductsCategoryOnly() {
        final String categoryCode = "W12345";
        final String allProductsUrl = "/All-Products/";
        final CategoryModel category = mockCategory(categoryCode, allProductsUrl);
        resolver.resolveInternal(category);
        verify(urlTransformer).transform("/c" + allProductsUrl + categoryCode);
    }

    /**
     * Verifies that url token level transformations are applied.
     */
    @Test
    public void testUrlTokenTransformation() {
        final String token = "Body + Beauty";
        resolver.urlSafe(token);
        verify(urlTokenTransformer).transform(token);
    }

    /**
     * Mocks category behaviour.
     * 
     * @param code
     *            the category code
     * @param url
     *            the category path URL
     * @return the mocked category
     */
    protected CategoryModel mockCategory(final String code, final String url) {
        final CategoryModel mockSupercategory = mock(TargetProductCategoryModel.class);
        final CategoryModel category = mock(TargetProductCategoryModel.class);
        final List<CategoryModel> path = new ArrayList<>();
        final Iterable<String> categoryStrings = Splitter.on('/').omitEmptyStrings().split(url);
        boolean first = true;
        for (final String categoryString : categoryStrings) {
            final CategoryModel cat = mock(TargetProductCategoryModel.class);
            when(cat.getName()).thenReturn(categoryString);
            when(urlTokenTransformer.transform(categoryString)).thenReturn(categoryString);
            if (first) {
                first = false;
            }
            else {
                when(cat.getSupercategories()).thenReturn(Collections.singletonList(mockSupercategory));
            }
            path.add(cat);
        }
        when(category.getCode()).thenReturn(code);
        when(commerceCategoryService.getPathsForCategory(category)).thenReturn(ImmutableList.of(path));
        return category;
    }

}
