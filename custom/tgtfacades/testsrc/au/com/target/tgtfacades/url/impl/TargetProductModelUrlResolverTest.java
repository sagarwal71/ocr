package au.com.target.tgtfacades.url.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.site.BaseSiteService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;
import au.com.target.tgtfacades.url.transform.UrlTransformer;
import com.google.common.collect.ImmutableList;

/**
 * Unit test for {@link TargetProductModelUrlResolver}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductModelUrlResolverTest {

    private static final String URL_PATTERN = "/p/{product-name}/{product-code}";

    @InjectMocks
    private TargetProductModelUrlResolver resolver = new TargetProductModelUrlResolver();

    @Mock
    private UrlTransformer urlTransformer;
    @Mock
    private UrlTokenTransformer urlTokenTransformer;
    @Mock
    private CommerceCategoryService commerceCategoryService;
    @Mock
    private BaseSiteService baseSiteService;

    @Mock
    private ProductModel product;

    /**
     * Initializes resolver by setting specific url pattern and applied transformers.
     */
    @Before
    public void setUp() {
        resolver.setDefaultPattern(URL_PATTERN);
        resolver.setUrlTransformers(ImmutableList.of(urlTransformer));
        resolver.setUrlTokenTransformers(ImmutableList.of(urlTokenTransformer));
    }

    /**
     * Verifies that url level transformations are applied.
     */
    @Test
    public void testUrlTransformation() {
        final String productCode = "P4025-Blue-L";
        final String productName = "Shorts-product-Blue";
        when(product.getName()).thenReturn(productName);
        when(product.getCode()).thenReturn(productCode);
        when(product.getSupercategories()).thenReturn(ImmutableList.<CategoryModel>of());
        when(urlTokenTransformer.transform(productName)).thenReturn(productName);
        resolver.resolveInternal(product);
        verify(urlTransformer).transform("/p/" + productName + "/" + productCode);
    }

    /**
     * Verifies that url token level transformations are applied.
     */
    @Test
    public void testUrlTokenTransformation() {
        final String token = "P4025_Blue_L";
        resolver.urlSafe(token);
        verify(urlTokenTransformer).transform(token);
    }

}
