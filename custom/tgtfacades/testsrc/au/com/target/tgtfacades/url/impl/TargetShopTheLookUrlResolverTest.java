/**
 * 
 */
package au.com.target.tgtfacades.url.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Arrays;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtfacades.url.transform.TargetUrlTokenTransformer;
import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;


/**
 * @author bhuang3
 * 
 */
@UnitTest
public class TargetShopTheLookUrlResolverTest {

    @InjectMocks
    private final TargetShopTheLookUrlResolver resolver = new TargetShopTheLookUrlResolver();

    @Mock
    private TargetShopTheLookModel targetShopTheLookModel;

    private UrlTokenTransformer transformer;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(targetShopTheLookModel.getId()).thenReturn("STL1234567");
        when(targetShopTheLookModel.getName()).thenReturn("Kids");
        transformer = new TargetUrlTokenTransformer();
        ((TargetUrlTokenTransformer)transformer).initialize();
        resolver.setUrlTokenTransformers(Arrays.asList(transformer));
    }

    @Test
    public void testresolveUrlWithDot() {
        final String name = "kids.inspiration";
        when(targetShopTheLookModel.getName()).thenReturn(name);
        final String url = resolver.resolveInternal(targetShopTheLookModel);
        Assertions.assertThat(url).isEqualTo("/inspiration/kids-inspiration/STL1234567");
    }


    @Test
    public void testresolveUrlWithSpace() {
        final String name = "kids inspiration";
        when(targetShopTheLookModel.getName()).thenReturn(name);
        final String url = resolver.resolveInternal(targetShopTheLookModel);
        Assertions.assertThat(url).isEqualTo("/inspiration/kids-inspiration/STL1234567");
    }


    @Test
    public void testresolveUrlWithSpecialCharacters() {
        final String name = "kids\\'&****inspiration";
        when(targetShopTheLookModel.getName()).thenReturn(name);
        final String url = resolver.resolveInternal(targetShopTheLookModel);
        Assertions.assertThat(url).isEqualTo("/inspiration/kids-inspiration/STL1234567");
    }

    @Test
    public void testresolveUrlWithSpecialCharacters1() {
        final String name = "kids<>{}|||+$##@@inspiration";
        when(targetShopTheLookModel.getName()).thenReturn(name);
        final String url = resolver.resolveInternal(targetShopTheLookModel);
        Assertions.assertThat(url).isEqualTo("/inspiration/kids-inspiration/STL1234567");
    }
}
