package au.com.target.tgtfacades.url.transform;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Before;
import org.junit.Test;

/**
 * Test suite for {@link TargetUrlTokenTransformer}.
 */
@UnitTest
public class TargetUrlTokenTransformerTest {

    private final TargetUrlTokenTransformer transformer = new TargetUrlTokenTransformer();

    /**
     * Initializes transformer.
     */
    @Before
    public void setUp() {
        transformer.initialize();
    }

    /**
     * Verifies any number of consequent spaces is replaced with a hyphen.
     */
    @Test
    public void testTransformSpaces() {
        assertEquals("boys-1-7", transformer.transform("Boys  1-7"));
    }

    /**
     * Verifies that special characters are replaced with hyphen.
     */
    @Test
    public void testTransformSpecialCharacters() {
        assertEquals("body-beauty", transformer.transform("Body + Beauty"));
    }

    /**
     * Verifies that leading and trailing hyphens are removed.
     */
    @Test
    public void testRemoveLeadingAndTrailingHyphens() {
        assertEquals("more", transformer.transform("More..."));
    }

    /**
     * Verifies complex token scenario.
     */
    @Test
    public void testComplexCase() {
        assertEquals("kids-bath-bodycare-shampoo", transformer.transform("Kids' Bath + Bodycare & Shampoo"));
    }
}
