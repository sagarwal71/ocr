/**
 * 
 */
package au.com.target.tgtfacades.offer.converter;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.offers.data.TargetMobileOfferHeadingData;
import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetMobileOfferHeadingListConverterTest {

    @InjectMocks
    private final TargetMobileOfferHeadingListConverter targetMobileOfferHeadingListConverter = new TargetMobileOfferHeadingListConverter();

    @Mock
    private TargetMobileOfferHeadingConverter targetMobileOfferHeadingConverter;

    @Test
    public void testConvertTargetMobileOfferHeadingList() {

        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel1 = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel2 = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel3 = Mockito
                .mock(TargetMobileOfferHeadingModel.class);

        final TargetMobileOfferHeadingData targetMobileOfferHeadingData1 = Mockito
                .mock(TargetMobileOfferHeadingData.class);
        final TargetMobileOfferHeadingData targetMobileOfferHeadingData2 = Mockito
                .mock(TargetMobileOfferHeadingData.class);
        final TargetMobileOfferHeadingData targetMobileOfferHeadingData3 = Mockito
                .mock(TargetMobileOfferHeadingData.class);

        BDDMockito.given(targetMobileOfferHeadingConverter.convert(targetMobileOfferHeadingModel1)).willReturn(
                targetMobileOfferHeadingData1);
        BDDMockito.given(targetMobileOfferHeadingConverter.convert(targetMobileOfferHeadingModel2)).willReturn(
                targetMobileOfferHeadingData2);
        BDDMockito.given(targetMobileOfferHeadingConverter.convert(targetMobileOfferHeadingModel3)).willReturn(
                targetMobileOfferHeadingData3);

        final List<TargetMobileOfferHeadingData> targetMobileOfferHeadingDataList = targetMobileOfferHeadingListConverter
                .convert(new ArrayList<TargetMobileOfferHeadingModel>(Arrays.asList(
                        targetMobileOfferHeadingModel1, targetMobileOfferHeadingModel2, targetMobileOfferHeadingModel3)));
        Assertions.assertThat(targetMobileOfferHeadingDataList).isNotNull();
        Assertions.assertThat(targetMobileOfferHeadingDataList).contains(targetMobileOfferHeadingData1);
        Assertions.assertThat(targetMobileOfferHeadingDataList).contains(targetMobileOfferHeadingData2);
        Assertions.assertThat(targetMobileOfferHeadingDataList).contains(targetMobileOfferHeadingData3);
    }

    @Test
    public void testConvertTargetMobileOfferHeadingListForEmptyHeadings() {
        final List<TargetMobileOfferHeadingData> targetMobileOfferHeadingDataList = targetMobileOfferHeadingListConverter
                .convert(new ArrayList<TargetMobileOfferHeadingModel>());
        Assertions.assertThat(targetMobileOfferHeadingDataList).isNull();
    }
}
