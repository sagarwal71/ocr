/**
 * 
 */
package au.com.target.tgtfacades.offer.converter.populator;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Test;

import au.com.target.tgtfacades.offers.data.TargetMobileOfferHeadingData;
import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;


/**
 * @author pthoma20
 *
 */
@UnitTest
public class TargetMobileOfferHeadingPopulatorTest {

    private final TargetMobileOfferHeadingPopulator targetMobileOfferHeadingPopulator = new TargetMobileOfferHeadingPopulator();

    @Test
    public void testPopulate() {
        final TargetMobileOfferHeadingModel source = new TargetMobileOfferHeadingModel();
        final TargetMobileOfferHeadingData target = new TargetMobileOfferHeadingData();

        source.setCode("women");
        source.setColour("#fffff");
        source.setName("WOMEN");

        targetMobileOfferHeadingPopulator.populate(source, target);

        Assert.assertEquals(source.getCode(), target.getCode());
        Assert.assertEquals(source.getColour(), target.getColour());
        Assert.assertEquals(source.getName(), target.getName());
    }
}
