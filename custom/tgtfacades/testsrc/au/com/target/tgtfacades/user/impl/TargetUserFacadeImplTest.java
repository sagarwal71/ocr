/**
 * 
 */
package au.com.target.tgtfacades.user.impl;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.Boolean.valueOf;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtmarketing.segments.TargetUserSegmentService;
import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;


/**
 * @author gbaker2
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetUserFacadeImplTest {

    private final DateTimeFormatter formatYear = DateTimeFormat.forPattern("yy");
    private final DateTimeFormatter formatMonth = DateTimeFormat.forPattern("MM");

    @InjectMocks
    private final TargetUserFacadeImpl targetUserFacadeImpl = new TargetUserFacadeImpl();

    @Mock
    private UserService userService;

    @Mock
    private CustomerModel currentCustomer;

    @Mock
    private CustomerAccountService customerAccountService;

    @Mock
    private TargetUserSegmentService targetUserSegmentService;

    @Mock
    private AbstractPopulatingConverter<CreditCardPaymentInfoModel, CCPaymentInfoData> creditCardPaymentInfoConverter;

    @Mock
    private CheckoutCustomerStrategy checkoutCustomerStrategy;

    private List<CreditCardPaymentInfoModel> ccPaymentInfoList;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        targetUserFacadeImpl.setCreditCardPaymentInfoConverter(creditCardPaymentInfoConverter);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testIsCurrentUserAnonymous() {
        final UserModel mockUserModel = Mockito.mock(UserModel.class);
        Mockito.when(userService.getCurrentUser()).thenReturn(mockUserModel);
        Mockito.when(userService.isAnonymousUser(mockUserModel)).thenReturn(true);
        Assert.assertTrue(targetUserFacadeImpl.isCurrentUserAnonymous());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testIsNotCurrentUserAnonymous() {
        final UserModel mockUserModel = Mockito.mock(UserModel.class);
        Mockito.when(userService.getCurrentUser()).thenReturn(mockUserModel);
        Mockito.when(userService.isAnonymousUser(mockUserModel)).thenReturn(false);
        Assert.assertFalse(targetUserFacadeImpl.isCurrentUserAnonymous());
    }

    @Test
    public void testIsNullCurrentUserAnonymous() {
        Mockito.when(userService.getCurrentUser()).thenReturn(null);
        Assert.assertFalse(targetUserFacadeImpl.isCurrentUserAnonymous());
    }

    @Test
    public void updateDefaultPaymentInfoShouldCancelIfDefaultPaymentInfoExists() {
        ccPaymentInfoList = new ArrayList<>();
        when(customerAccountService.getCreditCardPaymentInfos(currentCustomer, true)).thenReturn(ccPaymentInfoList);
        final CreditCardPaymentInfoModel defaultPayment = mock(CreditCardPaymentInfoModel.class);
        when(currentCustomer.getDefaultPaymentInfo()).thenReturn(defaultPayment);

        targetUserFacadeImpl.updateDefaultPaymentInfo(currentCustomer);

        verifyZeroInteractions(customerAccountService);
    }

    @Test
    public void updateDefaultPaymentInfoShouldCancelIfNoMoreSavedPaymentInfo() {
        ccPaymentInfoList = new ArrayList<>();
        when(customerAccountService.getCreditCardPaymentInfos(currentCustomer, true)).thenReturn(ccPaymentInfoList);
        when(currentCustomer.getDefaultPaymentInfo()).thenReturn(null);

        targetUserFacadeImpl.updateDefaultPaymentInfo(currentCustomer);

        verify(customerAccountService).getCreditCardPaymentInfos(currentCustomer, true);
        verifyNoMoreInteractions(customerAccountService);
    }

    @Test
    public void updateDefaultPaymentInfoShouldUpdateLastSavedCardAsDefault() {
        ccPaymentInfoList = new ArrayList<>();
        final CreditCardPaymentInfoModel firstCard = mock(CreditCardPaymentInfoModel.class);
        final CreditCardPaymentInfoModel lastCard = mock(CreditCardPaymentInfoModel.class);
        ccPaymentInfoList.add(firstCard);
        ccPaymentInfoList.add(lastCard);
        when(customerAccountService.getCreditCardPaymentInfos(currentCustomer, true)).thenReturn(ccPaymentInfoList);
        when(currentCustomer.getDefaultPaymentInfo()).thenReturn(null);

        targetUserFacadeImpl.updateDefaultPaymentInfo(currentCustomer);

        verify(customerAccountService).getCreditCardPaymentInfos(currentCustomer, true);
        verify(customerAccountService).setDefaultPaymentInfo(currentCustomer, lastCard);
    }

    @Test
    public void testHasSavedValidIpgCreditCards() {
        ccPaymentInfoList = new ArrayList<>();
        final IpgCreditCardPaymentInfoModel firstCard = mock(IpgCreditCardPaymentInfoModel.class);
        final DateTime toDate = DateTime.now().plusYears(5);
        when(firstCard.getValidToYear()).thenReturn(formatYear.print(toDate));
        when(firstCard.getValidToMonth()).thenReturn(formatMonth.print(toDate));
        final CreditCardPaymentInfoModel lastCard = mock(CreditCardPaymentInfoModel.class);
        ccPaymentInfoList.add(firstCard);
        ccPaymentInfoList.add(lastCard);
        final CustomerModel mockCustomerModel = Mockito.mock(CustomerModel.class);
        Mockito.when(userService.getCurrentUser()).thenReturn(mockCustomerModel);
        when(customerAccountService.getCreditCardPaymentInfos(mockCustomerModel, true)).thenReturn(ccPaymentInfoList);
        final boolean result = targetUserFacadeImpl.hasSavedValidIpgCreditCards();
        Assert.assertEquals(TRUE, valueOf(result));
    }

    @Test
    public void testHasSavedValidIpgCreditCardsWithEmptyCardList() {
        ccPaymentInfoList = new ArrayList<>();
        final CustomerModel mockCustomerModel = Mockito.mock(CustomerModel.class);
        Mockito.when(userService.getCurrentUser()).thenReturn(mockCustomerModel);
        when(customerAccountService.getCreditCardPaymentInfos(mockCustomerModel, true)).thenReturn(ccPaymentInfoList);
        final boolean result = targetUserFacadeImpl.hasSavedValidIpgCreditCards();
        Assert.assertEquals(FALSE, valueOf(result));
    }

    @Test
    public void testHasSavedValidIpgCreditCardsWithoutIpgCreditCards() {
        ccPaymentInfoList = new ArrayList<>();
        final CreditCardPaymentInfoModel firstCard = mock(CreditCardPaymentInfoModel.class);
        final CreditCardPaymentInfoModel lastCard = mock(CreditCardPaymentInfoModel.class);
        ccPaymentInfoList.add(firstCard);
        ccPaymentInfoList.add(lastCard);
        final CustomerModel mockCustomerModel = Mockito.mock(CustomerModel.class);
        Mockito.when(userService.getCurrentUser()).thenReturn(mockCustomerModel);
        when(customerAccountService.getCreditCardPaymentInfos(mockCustomerModel, true)).thenReturn(ccPaymentInfoList);
        final boolean result = targetUserFacadeImpl.hasSavedValidIpgCreditCards();
        Assert.assertEquals(FALSE, valueOf(result));
    }

    @Test
    public void testHasSavedValidIpgCreditCardsWithExpiredCard() {
        ccPaymentInfoList = new ArrayList<>();
        final IpgCreditCardPaymentInfoModel firstCard = mock(IpgCreditCardPaymentInfoModel.class);
        final DateTime toDate = DateTime.now().minusMonths(5);
        when(firstCard.getValidToYear()).thenReturn(formatYear.print(toDate));
        when(firstCard.getValidToMonth()).thenReturn(formatMonth.print(toDate));
        final CreditCardPaymentInfoModel lastCard = mock(CreditCardPaymentInfoModel.class);
        ccPaymentInfoList.add(firstCard);
        ccPaymentInfoList.add(lastCard);
        final CustomerModel mockCustomerModel = Mockito.mock(CustomerModel.class);
        Mockito.when(userService.getCurrentUser()).thenReturn(mockCustomerModel);
        when(customerAccountService.getCreditCardPaymentInfos(mockCustomerModel, true)).thenReturn(ccPaymentInfoList);
        final boolean result = targetUserFacadeImpl.hasSavedValidIpgCreditCards();
        Assert.assertEquals(FALSE, valueOf(result));
    }

    @Test
    public void testHasSavedValidIpgCreditCardsWithExpiredCardEqualsCurrentMonth() {
        ccPaymentInfoList = new ArrayList<>();
        final IpgCreditCardPaymentInfoModel firstCard = mock(IpgCreditCardPaymentInfoModel.class);
        final DateTime toDate = DateTime.now();
        when(firstCard.getValidToYear()).thenReturn(formatYear.print(toDate));
        when(firstCard.getValidToMonth()).thenReturn(formatMonth.print(toDate));
        final CreditCardPaymentInfoModel lastCard = mock(CreditCardPaymentInfoModel.class);
        ccPaymentInfoList.add(firstCard);
        ccPaymentInfoList.add(lastCard);
        final CustomerModel mockCustomerModel = Mockito.mock(CustomerModel.class);
        Mockito.when(userService.getCurrentUser()).thenReturn(mockCustomerModel);
        when(customerAccountService.getCreditCardPaymentInfos(mockCustomerModel, true)).thenReturn(ccPaymentInfoList);
        final boolean result = targetUserFacadeImpl.hasSavedValidIpgCreditCards();
        Assert.assertEquals(TRUE, valueOf(result));
    }

    @Test
    public void testGetSavedCreditCardPaymentInfosInValidTime()
    {
        given(checkoutCustomerStrategy.getCurrentUserForCheckout()).willReturn(currentCustomer);
        final CCPaymentInfoData ccPaymentInfoData = Mockito.mock(CCPaymentInfoData.class);
        ccPaymentInfoList = new ArrayList<>();
        final CreditCardPaymentInfoModel cc1 = Mockito.mock(CreditCardPaymentInfoModel.class);
        final CreditCardPaymentInfoModel cc2 = Mockito.mock(CreditCardPaymentInfoModel.class);
        final DateTime toDate1 = DateTime.now().plusYears(35);
        final DateTime toDate2 = DateTime.now();
        when(cc1.getValidToMonth()).thenReturn(formatMonth.print(toDate1));
        when(cc1.getValidToYear()).thenReturn(formatYear.print(toDate1));
        when(cc2.getValidToMonth()).thenReturn(formatMonth.print(toDate2));
        when(cc2.getValidToYear()).thenReturn(formatYear.print(toDate2));
        ccPaymentInfoList.add(cc1);
        ccPaymentInfoList.add(cc2);
        when(customerAccountService.getCreditCardPaymentInfos(currentCustomer, true)).thenReturn(ccPaymentInfoList);
        final CreditCardPaymentInfoModel defaultPayment = mock(CreditCardPaymentInfoModel.class);
        when(currentCustomer.getDefaultPaymentInfo()).thenReturn(defaultPayment);
        given(creditCardPaymentInfoConverter.convert(Mockito.any(CreditCardPaymentInfoModel.class))).willReturn(
                ccPaymentInfoData);
        final List<CCPaymentInfoData> list = targetUserFacadeImpl.getSavedCreditCardPaymentInfosInValidTime();
        Assert.assertNotNull(list);
        Assert.assertEquals(2, list.size());
    }

    @Test
    public void testGetSavedCreditCardPaymentInfosInValidTimeWithExpiredCard()
    {
        given(checkoutCustomerStrategy.getCurrentUserForCheckout()).willReturn(currentCustomer);
        final CCPaymentInfoData ccPaymentInfoData = Mockito.mock(CCPaymentInfoData.class);
        ccPaymentInfoList = new ArrayList<>();
        final CreditCardPaymentInfoModel cc1 = Mockito.mock(CreditCardPaymentInfoModel.class);
        final CreditCardPaymentInfoModel cc2 = Mockito.mock(CreditCardPaymentInfoModel.class);
        final DateTime toDate1 = DateTime.now().plusMonths(5);
        final DateTime toDate2 = DateTime.now().minusMonths(1);
        when(cc1.getValidToMonth()).thenReturn(formatMonth.print(toDate1));
        when(cc1.getValidToYear()).thenReturn(formatYear.print(toDate1));
        when(cc2.getValidToMonth()).thenReturn(formatMonth.print(toDate2));
        when(cc2.getValidToYear()).thenReturn(formatYear.print(toDate2));
        ccPaymentInfoList.add(cc1);
        ccPaymentInfoList.add(cc2);
        when(customerAccountService.getCreditCardPaymentInfos(currentCustomer, true)).thenReturn(ccPaymentInfoList);
        final CreditCardPaymentInfoModel defaultPayment = mock(CreditCardPaymentInfoModel.class);
        when(currentCustomer.getDefaultPaymentInfo()).thenReturn(defaultPayment);
        given(creditCardPaymentInfoConverter.convert(Mockito.any(CreditCardPaymentInfoModel.class))).willReturn(
                ccPaymentInfoData);
        final List<CCPaymentInfoData> list = targetUserFacadeImpl.getSavedCreditCardPaymentInfosInValidTime();
        Assert.assertNotNull(list);
        Assert.assertEquals(1, list.size());
    }

    @Test
    public void testGetAllUserSegmentsForUserNullUser() {
        BDDMockito.when(userService.getCurrentUser()).thenReturn(null);
        final List<String> segments = targetUserFacadeImpl.getAllUserSegmentsForCurrentUser();
        Assert.assertNotNull(segments);
        Assert.assertTrue(CollectionUtils.isEmpty(segments));
    }

    @Test
    public void testGetAllUserSegmentsForUserNullSegments() {
        BDDMockito.when(userService.getCurrentUser()).thenReturn(currentCustomer);
        BDDMockito.when(targetUserSegmentService.getAllUserSegmentsForUser(currentCustomer)).thenReturn(null);
        final List<String> segments = targetUserFacadeImpl.getAllUserSegmentsForCurrentUser();
        Assert.assertNotNull(segments);
        Assert.assertTrue(CollectionUtils.isEmpty(segments));
    }

    @Test
    public void testGetAllUserSegmentsForUserEmptySegments() {
        BDDMockito.when(userService.getCurrentUser()).thenReturn(currentCustomer);
        BDDMockito.when(targetUserSegmentService.getAllUserSegmentsForUser(currentCustomer)).thenReturn(
                new ArrayList<PrincipalGroupModel>());
        final List<String> segments = targetUserFacadeImpl.getAllUserSegmentsForCurrentUser();
        Assert.assertNotNull(segments);
        Assert.assertTrue(CollectionUtils.isEmpty(segments));
    }

    @Test
    public void testGetAllUserSegmentsForUserValidSegments() {
        BDDMockito.when(userService.getCurrentUser()).thenReturn(currentCustomer);
        BDDMockito.when(targetUserSegmentService.getAllUserSegmentsForUser(currentCustomer)).thenReturn(
                buildUserSegments());
        final List<String> segments = targetUserFacadeImpl.getAllUserSegmentsForCurrentUser();
        Assert.assertNotNull(segments);
        Assert.assertEquals(6, segments.size());
        final String strSegments = StringUtils.join(segments, "|");
        Assert.assertEquals("Body|Kids|Men|School|Toys|Womens", strSegments);
    }

    /**
     * @return List of principal group models
     */
    private List<PrincipalGroupModel> buildUserSegments() {
        final List<PrincipalGroupModel> principalGroupModel = new ArrayList<>();
        principalGroupModel.add(buildPrincipalGroup("Womens"));
        principalGroupModel.add(buildPrincipalGroup("Kids"));
        principalGroupModel.add(buildPrincipalGroup("Toys"));
        principalGroupModel.add(buildPrincipalGroup("Men"));
        principalGroupModel.add(buildPrincipalGroup("Body"));
        principalGroupModel.add(buildPrincipalGroup("School"));
        return principalGroupModel;
    }

    /**
     * @return PrincipalGroupModel
     */
    private PrincipalGroupModel buildPrincipalGroup(final String uid) {
        final PrincipalGroupModel principalGroup = new PrincipalGroupModel();
        principalGroup.setUid(uid);
        return principalGroup;
    }

}
