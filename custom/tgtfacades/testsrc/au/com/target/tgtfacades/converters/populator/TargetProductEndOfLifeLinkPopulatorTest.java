/**
 * 
 */
package au.com.target.tgtfacades.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.service.EndecaDimensionService;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.stl.TargetShopTheLookFacade;
import au.com.target.tgtfacades.url.impl.TargetCategoryModelUrlResolver;
import au.com.target.tgtfacades.url.impl.TargetShopTheLookUrlResolver;
import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;
import au.com.target.tgtfacades.url.transform.UrlTransformer;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;


/**
 * @author fkhan4
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductEndOfLifeLinkPopulatorTest {

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;
    @Mock
    private TargetCategoryModelUrlResolver targetCategoryModelUrlResolver;

    @Mock
    private UrlTransformer urlTransformer;

    @Mock
    private UrlTokenTransformer urlTokenTransformer;
    @Mock
    private TargetShopTheLookFacade targetShopTheLookFacade;
    @Mock
    private TargetShopTheLookUrlResolver targetShopTheLookUrlResolver;

    @Mock
    private EndecaDimensionService endecaDimensionService;
    @Mock
    private TargetProductCategoryModel originalCategoryModel;

    @Mock
    private TargetProductCategoryModel allProductCategoryModel;
    @Mock
    private TargetProductCategoryModel topLevelCategoryModel;
    @Mock
    private TargetShopTheLookModel shopTheLookModel;
    @Mock
    private TargetProductModel productModel;

    @InjectMocks
    private final TargetProductEndOfLifeLinkPopulator targetEndOfLifeLinkProductPopulator = new TargetProductEndOfLifeLinkPopulator();



    @Test
    public void testPopulateProductEndLinksWithOriginalCategory() {
        final TargetProductData productData = new TargetProductData();
        populateAllProductCategory();
        populateTopLevelCategory();
        populateOriginalCategory(topLevelCategoryModel);

        given(productModel.getOriginalCategory()).willReturn(originalCategoryModel);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isProductEndOfLifeEnabled();

        given(urlTokenTransformer.transform("tops")).willReturn("tops");
        given(targetCategoryModelUrlResolver.getUrlTokenTransformers())
                .willReturn(ImmutableList.of(urlTokenTransformer));
        given(targetCategoryModelUrlResolver.getUrlTransformers()).willReturn(ImmutableList.of(urlTransformer));
        given(targetCategoryModelUrlResolver.resolve(originalCategoryModel)).willReturn("/c/tops/W1234");
        targetEndOfLifeLinkProductPopulator.populate(productModel, productData);
        assertThat(productData.getProductEndLinks().get(0)).isNotNull();
        assertThat(productData.getProductEndLinks().get(0).getUrl()).isNotNull().isEqualTo("/c/tops/W1234");
        assertThat(productData.getProductEndLinks().get(0).getCode()).isEqualTo("W1234");
        assertThat(productData.getProductEndLinks().get(0).getName()).isEqualTo("tops");
    }


    @Test
    public void testPopulateProductEndLinksWithoutOriginalCategory() {
        final TargetProductData productData = new TargetProductData();
        given(productModel.getOriginalCategory()).willReturn(null);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isProductEndOfLifeEnabled();
        targetEndOfLifeLinkProductPopulator.populate(productModel, productData);
        assertThat(productData.getProductEndLinks()).isNull();
    }

    @Test
    public void testPopulateProductEndLinksWithInspiration() {
        final TargetProductData productData = new TargetProductData();

        populateAllProductCategory();
        populateTopLevelCategory();
        populateShopTheLook();
        populateOriginalCategory(topLevelCategoryModel);

        given(productModel.getPrimarySuperCategory()).willReturn(topLevelCategoryModel);
        given(productModel.getOriginalCategory()).willReturn(originalCategoryModel);

        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isProductEndOfLifeEnabled();
        given(targetShopTheLookFacade.getShopTheLookByCategoryCode(topLevelCategoryModel.getCode()))
                .willReturn(shopTheLookModel);
        given(urlTokenTransformer.transform("womens-impiration")).willReturn("womens-impiration");
        given(targetShopTheLookUrlResolver.getUrlTokenTransformers())
                .willReturn(ImmutableList.of(urlTokenTransformer));

        given(targetShopTheLookUrlResolver.resolve(shopTheLookModel))
                .willReturn("/inspiration/womens-impiration/STL001");
        targetEndOfLifeLinkProductPopulator.populate(productModel, productData);
        assertThat(productData.getProductEndLinks().get(1)).isNotNull();
        assertThat(productData.getProductEndLinks().get(1).getUrl()).isNotNull()
                .isEqualTo("/inspiration/womens-impiration/STL001");
        assertThat(productData.getProductEndLinks().get(1).getCode()).isEqualTo("STL001");
        assertThat(productData.getProductEndLinks().get(1).getName()).isEqualTo("Shop-by-inspiration");
    }


    @Test
    public void testPopulateProductEndLinksNoCategoryForShopTheLook() {
        final TargetProductData productData = new TargetProductData();

        populateAllProductCategory();
        populateTopLevelCategory();
        populateShopTheLook();
        populateOriginalCategory(topLevelCategoryModel);

        given(productModel.getPrimarySuperCategory()).willReturn(topLevelCategoryModel);
        given(productModel.getOriginalCategory()).willReturn(originalCategoryModel);

        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isProductEndOfLifeEnabled();
        given(targetShopTheLookFacade.getShopTheLookByCategoryCode(topLevelCategoryModel.getCode()))
                .willReturn(null);
        given(urlTokenTransformer.transform("impiration")).willReturn("impiration");
        given(targetShopTheLookUrlResolver.getUrlTokenTransformers())
                .willReturn(ImmutableList.of(urlTokenTransformer));

        given(targetShopTheLookUrlResolver.resolve(shopTheLookModel))
                .willReturn("/inspiration/impiration/STL001");
        targetEndOfLifeLinkProductPopulator.populate(productModel, productData);
        assertThat(productData.getProductEndLinks()).hasSize(1);

    }

    @Test
    public void testPopulateProductEndLinksWithOriginalCategoryAtSecondLevel() {
        final TargetProductData productData = new TargetProductData();

        populateAllProductCategory();
        populateOriginalCategory(allProductCategoryModel);

        given(productModel.getOriginalCategory()).willReturn(originalCategoryModel);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isProductEndOfLifeEnabled();

        given(urlTokenTransformer.transform("tops")).willReturn("tops");
        given(targetCategoryModelUrlResolver.getUrlTokenTransformers())
                .willReturn(ImmutableList.of(urlTokenTransformer));
        given(targetCategoryModelUrlResolver.getUrlTransformers()).willReturn(ImmutableList.of(urlTransformer));
        given(targetCategoryModelUrlResolver.resolve(originalCategoryModel)).willReturn("/c/tops/W1234");
        targetEndOfLifeLinkProductPopulator.populate(productModel, productData);
        assertThat(productData.getProductEndLinks().get(0)).isNotNull();
        assertThat(productData.getProductEndLinks().get(0).getUrl()).isNotNull().isEqualTo("/c/tops/W1234");
        assertThat(productData.getProductEndLinks().get(0).getCode()).isEqualTo("W1234");
        assertThat(productData.getProductEndLinks().get(0).getName()).isEqualTo("tops");
    }

    @Test
    public void testProductEndLinksWithNewArrivals() {
        final TargetProductData productData = new TargetProductData();
        final Map<String, String> dimensionIdMap = getEndecaFields();

        populateAllProductCategory();
        populateTopLevelCategory();
        populateOriginalCategory(topLevelCategoryModel);

        given(productModel.getPrimarySuperCategory()).willReturn(topLevelCategoryModel);
        given(productModel.getOriginalCategory()).willReturn(originalCategoryModel);

        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isProductEndOfLifeEnabled();

        given(urlTokenTransformer.transform("womens")).willReturn("womens");
        given(targetCategoryModelUrlResolver.getUrlTokenTransformers())
                .willReturn(ImmutableList.of(urlTokenTransformer));

        given(endecaDimensionService.getDimensionNavigationState(dimensionIdMap)).willReturn("40");

        given(targetCategoryModelUrlResolver.resolve(topLevelCategoryModel))
                .willReturn("/c/womens/W0000");
        targetEndOfLifeLinkProductPopulator.populate(productModel, productData);
        assertThat(productData.getProductEndLinks().get(1)).isNotNull();
        assertThat(productData.getProductEndLinks().get(1).getUrl()).isNotNull()
                .isEqualTo("/c/womens/W0000?N=40");
        assertThat(productData.getProductEndLinks().get(1).getCode()).isEqualTo("W0000");
        assertThat(productData.getProductEndLinks().get(1).getName()).isEqualTo("New Arrivals");
    }

    @Test
    public void testProductEndLinksWithoutNewArrivals() {
        final TargetProductData productData = new TargetProductData();
        final Map<String, String> dimensionIdMap = getEndecaFields();

        populateAllProductCategory();
        populateTopLevelCategory();
        populateOriginalCategory(topLevelCategoryModel);

        given(productModel.getPrimarySuperCategory()).willReturn(topLevelCategoryModel);
        given(productModel.getOriginalCategory()).willReturn(originalCategoryModel);

        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isProductEndOfLifeEnabled();

        given(urlTokenTransformer.transform("tops")).willReturn("tops");
        given(targetCategoryModelUrlResolver.getUrlTokenTransformers())
                .willReturn(ImmutableList.of(urlTokenTransformer));

        given(endecaDimensionService.getDimensionNavigationState(dimensionIdMap)).willReturn(null);

        given(targetCategoryModelUrlResolver.resolve(originalCategoryModel))
                .willReturn("/c/tops/W1234");
        targetEndOfLifeLinkProductPopulator.populate(productModel, productData);
        assertThat(productData.getProductEndLinks().get(0)).isNotNull();
        assertThat(productData.getProductEndLinks().get(0).getUrl()).isNotNull()
                .isEqualTo("/c/tops/W1234");
        assertThat(productData.getProductEndLinks().get(0).getCode()).isEqualTo("W1234");
        assertThat(productData.getProductEndLinks().get(0).getName()).isEqualTo("tops");
    }

    @Test
    public void testProductEndLinksWithOriginalCategoryAndNewArrivals() {
        final TargetProductData productData = new TargetProductData();
        final Map<String, String> dimensionIdMap = getEndecaFields();

        populateAllProductCategory();
        populateTopLevelCategory();
        populateOriginalCategory(topLevelCategoryModel);

        given(productModel.getPrimarySuperCategory()).willReturn(topLevelCategoryModel);
        given(productModel.getOriginalCategory()).willReturn(originalCategoryModel);

        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isProductEndOfLifeEnabled();

        given(urlTokenTransformer.transform("tops")).willReturn("tops");
        given(targetCategoryModelUrlResolver.getUrlTokenTransformers())
                .willReturn(ImmutableList.of(urlTokenTransformer));

        given(endecaDimensionService.getDimensionNavigationState(dimensionIdMap)).willReturn("40");

        given(targetCategoryModelUrlResolver.resolve(originalCategoryModel))
                .willReturn("/c/tops/W1234");
        given(targetCategoryModelUrlResolver.resolve(topLevelCategoryModel))
                .willReturn("/c/women/W0000");
        targetEndOfLifeLinkProductPopulator.populate(productModel, productData);
        assertThat(productData.getProductEndLinks().get(0)).isNotNull();
        assertThat(productData.getProductEndLinks().get(0).getUrl()).isNotNull()
                .isEqualTo("/c/tops/W1234");
        assertThat(productData.getProductEndLinks().get(0).getCode()).isEqualTo("W1234");
        assertThat(productData.getProductEndLinks().get(0).getName()).isEqualTo("tops");

        assertThat(productData.getProductEndLinks().get(1).getUrl()).isNotNull()
                .isEqualTo("/c/women/W0000?N=40");
        assertThat(productData.getProductEndLinks().get(1).getCode()).isEqualTo("W0000");
        assertThat(productData.getProductEndLinks().get(1).getName()).isEqualTo("New Arrivals");
    }

    @Test
    public void testProductEndLinksWithOriginalCategoryInspirationAndNewArrivals() {
        final TargetProductData productData = new TargetProductData();
        final Map<String, String> dimensionIdMap = getEndecaFields();

        populateAllProductCategory();
        populateTopLevelCategory();
        populateShopTheLook();
        populateOriginalCategory(topLevelCategoryModel);

        given(productModel.getPrimarySuperCategory()).willReturn(topLevelCategoryModel);
        given(productModel.getOriginalCategory()).willReturn(originalCategoryModel);

        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isProductEndOfLifeEnabled();
        given(targetShopTheLookFacade.getShopTheLookByCategoryCode(topLevelCategoryModel.getCode()))
                .willReturn(shopTheLookModel);

        given(urlTokenTransformer.transform("tops")).willReturn("tops");
        given(targetCategoryModelUrlResolver.getUrlTokenTransformers())
                .willReturn(ImmutableList.of(urlTokenTransformer));

        given(endecaDimensionService.getDimensionNavigationState(dimensionIdMap)).willReturn("40");

        given(targetCategoryModelUrlResolver.resolve(originalCategoryModel))
                .willReturn("/c/tops/W1234");

        given(targetShopTheLookUrlResolver.resolve(shopTheLookModel))
                .willReturn("/inspiration/womens-impiration/STL001");

        given(targetCategoryModelUrlResolver.resolve(topLevelCategoryModel))
                .willReturn("/c/women/W0000");

        targetEndOfLifeLinkProductPopulator.populate(productModel, productData);

        assertThat(productData.getProductEndLinks().get(0)).isNotNull();
        assertThat(productData.getProductEndLinks().get(0).getCode()).isEqualTo("W1234");
        assertThat(productData.getProductEndLinks().get(0).getName()).isEqualTo("tops");
        assertThat(productData.getProductEndLinks().get(0).getUrl()).isNotNull()
                .isEqualTo("/c/tops/W1234");

        assertThat(productData.getProductEndLinks().get(1).getCode()).isEqualTo("STL001");
        assertThat(productData.getProductEndLinks().get(1).getName()).isEqualTo("Shop-by-inspiration");
        assertThat(productData.getProductEndLinks().get(1).getUrl()).isNotNull()
                .isEqualTo("/inspiration/womens-impiration/STL001");

        assertThat(productData.getProductEndLinks().get(2).getCode()).isEqualTo("W0000");
        assertThat(productData.getProductEndLinks().get(2).getName()).isEqualTo("New Arrivals");
        assertThat(productData.getProductEndLinks().get(2).getUrl()).isNotNull()
                .isEqualTo("/c/women/W0000?N=40");


    }


    /**
     * @return dimensionIdMap
     */
    private Map<String, String> getEndecaFields() {
        final Map<String, String> dimensionIdMap = new HashMap();
        dimensionIdMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY, "W0000");
        dimensionIdMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PROD_TAGS,
                EndecaConstants.DimensionValue.NEW_ARRIVALS);
        return dimensionIdMap;

    }

    private void populateOriginalCategory(final TargetProductCategoryModel category) {
        given(originalCategoryModel.getCode()).willReturn("W1234");
        given(originalCategoryModel.getName()).willReturn("tops");
        given(originalCategoryModel.getSupercategories())
                .willReturn(ImmutableList.of((CategoryModel)category));

    }


    private void populateTopLevelCategory() {
        given(topLevelCategoryModel.getSupercategories())
                .willReturn(ImmutableList.of((CategoryModel)allProductCategoryModel));
        given(topLevelCategoryModel.getCode()).willReturn("W0000");
        given(topLevelCategoryModel.getName()).willReturn("Women");

    }


    private void populateAllProductCategory() {
        given(allProductCategoryModel.getCode()).willReturn("AP01");
        given(allProductCategoryModel.getName()).willReturn("All Products");

    }

    private void populateShopTheLook() {
        given(shopTheLookModel.getId()).willReturn("STL001");
        given(shopTheLookModel.getName()).willReturn("Shop-by-inspiration");
        given(shopTheLookModel.getDescription()).willReturn("womens-impiration");
        given(shopTheLookModel.getCategory()).willReturn(topLevelCategoryModel);

    }


}


