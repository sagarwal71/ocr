/**
 * 
 */
package au.com.target.tgtfacades.converters.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import org.junit.Test;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetDiscountCategoryModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * @author ajit
 *
 */
@UnitTest
public class TargetCategoryUtilTest {

    @Test
    public void testGetTopLevelCategory() {
        final TargetProductModel productModel = new TargetProductModel();
        final TargetProductCategoryModel targetCategoryModel = new TargetProductCategoryModel();
        final CategoryModel targetCategoryModel1 = new TargetProductCategoryModel();
        final CategoryModel targetCategoryModel2 = new TargetDiscountCategoryModel();
        final List<CategoryModel> listOfCategory = new ArrayList<>();
        listOfCategory.add(targetCategoryModel1);
        listOfCategory.add(targetCategoryModel2);
        targetCategoryModel.setSupercategories(listOfCategory);
        productModel.setPrimarySuperCategory(targetCategoryModel);
        final TargetColourVariantProductModel colourVariant = new TargetColourVariantProductModel();
        colourVariant.setBaseProduct(productModel);
        final TargetSizeVariantProductModel sizeVariant = new TargetSizeVariantProductModel();
        sizeVariant.setBaseProduct(colourVariant);
        final CategoryModel resultModel = TargetCategoryUtil.getTopLevelCategory(productModel);
        Assert.assertTrue(resultModel instanceof TargetProductCategoryModel);

    }

    @Test
    public void testGetTopLevelCategoryAsNull() {
        final TargetProductModel productModel = new TargetProductModel();
        productModel.setPrimarySuperCategory(null);
        final TargetColourVariantProductModel colourVariant = new TargetColourVariantProductModel();
        colourVariant.setBaseProduct(productModel);
        final TargetSizeVariantProductModel sizeVariant = new TargetSizeVariantProductModel();
        sizeVariant.setBaseProduct(colourVariant);
        final CategoryModel resultModel = TargetCategoryUtil.getTopLevelCategory(productModel);
        Assert.assertNull(resultModel);
    }

    @Test
    public void testGetTopLevelCategoryWhenNoSuperCategoryPresent() {
        final TargetProductModel productModel = new TargetProductModel();
        final TargetProductCategoryModel targetCategoryModel = new TargetProductCategoryModel();
        productModel.setPrimarySuperCategory(targetCategoryModel);
        final TargetColourVariantProductModel colourVariant = new TargetColourVariantProductModel();
        colourVariant.setBaseProduct(productModel);
        final TargetSizeVariantProductModel sizeVariant = new TargetSizeVariantProductModel();
        sizeVariant.setBaseProduct(colourVariant);
        final CategoryModel resultModel = TargetCategoryUtil.getTopLevelCategory(productModel);
        Assert.assertTrue(resultModel instanceof TargetProductCategoryModel);
    }

    @Test
    public void testGetTopLevelCategoryWhenNoTargetProductModel() {
        final ProductModel productModel = new ProductModel();
        final CategoryModel resultModel = TargetCategoryUtil.getTopLevelCategory(productModel);
        Assert.assertNull(resultModel);
    }
}
