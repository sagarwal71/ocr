/**
 * 
 */
package au.com.target.tgtfacades.converters.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;

import org.junit.Test;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * @author rsamuel3
 *
 */
@UnitTest
public class TargetConvertersUtilTest {

    @Test
    public void testGetBaseProductNullProductPassedIn() {
        assertNull(TargetConvertersUtil.getBaseProduct(null));
    }

    @Test
    public void testGetBaseProductProductModelPassedIn() {
        final ProductModel productModel = new ProductModel();
        final ProductModel baseProduct = TargetConvertersUtil.getBaseProduct(productModel);
        assertNotNull(baseProduct);
        assertEquals(productModel, baseProduct);
    }

    @Test
    public void testGetBaseProductColourVariantPassedIn() {
        final ProductModel productModel = new ProductModel();
        final TargetColourVariantProductModel colourVariant = buildColourVariant(productModel);
        final ProductModel baseProduct = TargetConvertersUtil.getBaseProduct(colourVariant);
        assertNotNull(baseProduct);
        assertEquals(productModel, baseProduct);
    }

    @Test
    public void testGetBaseProductSizeVariantPassedIn() {
        final ProductModel productModel = new ProductModel();
        final TargetColourVariantProductModel colourVariant = buildColourVariant(productModel);
        final TargetSizeVariantProductModel sizeVariant = new TargetSizeVariantProductModel();
        sizeVariant.setBaseProduct(colourVariant);
        final ProductModel baseProduct = TargetConvertersUtil.getBaseProduct(sizeVariant);
        assertNotNull(baseProduct);
        assertEquals(productModel, baseProduct);
    }

    /**
     * @param productModel
     * @return {@link TargetColourVariantProductModel}
     */
    protected TargetColourVariantProductModel buildColourVariant(final ProductModel productModel) {
        final TargetColourVariantProductModel colourVariant = new TargetColourVariantProductModel();
        colourVariant.setBaseProduct(productModel);
        return colourVariant;
    }
}
