/**
 * 
 */
package au.com.target.tgtfacades.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.HostConfigService;
import de.hybris.platform.commerceservices.enums.SalesApplication;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.constants.ThirdPartyConstants;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;


/**
 * Unit tests for SalesApplicationPropertyResolver
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SalesApplicationPropertyResolverTest {

    @InjectMocks
    private final SalesApplicationPropertyResolver salesApplicationPropertyResolver = new SalesApplicationPropertyResolver();

    @Mock
    private HostConfigService hostConfigService;

    @Mock
    private SalesApplicationFacade salesApplicationFacade;

    @Before
    public void setupMockData() {
        BDDMockito
                .when(hostConfigService.getProperty(ThirdPartyConstants.Analytics.UNIVERSAL_KIOSK_ACCOUNT_ID, "dev"))
                .thenReturn("kioskGA");
        BDDMockito.when(hostConfigService.getProperty(ThirdPartyConstants.Analytics.UNIVERSAL_MOBILEAPP_ACCOUNT_ID,
                "dev")).thenReturn("mobileGA");
        BDDMockito.when(hostConfigService.getProperty(ThirdPartyConstants.Analytics.UNIVERSAL_ACCOUNT_ID, "dev"))
                .thenReturn("universalGA");
        BDDMockito
                .when(hostConfigService.getProperty(ThirdPartyConstants.Analytics.UNIVERSAL_KIOSK_ACCOUNT_ID, ""))
                .thenReturn("kioskGA");
        BDDMockito.when(hostConfigService.getProperty(ThirdPartyConstants.Analytics.UNIVERSAL_MOBILEAPP_ACCOUNT_ID,
                "")).thenReturn("mobileGA");
        BDDMockito.when(hostConfigService.getProperty(ThirdPartyConstants.Analytics.UNIVERSAL_ACCOUNT_ID, ""))
                .thenReturn("universalGA");
    }

    @Test
    public void getPropertiesWhenHostSuffixAndSalesAppNull() {
        salesApplicationPropertyResolver.setHostSuffix(null);
        BDDMockito.when(salesApplicationFacade.getCurrentSalesApplication()).thenReturn(null);
        Assertions.assertThat(salesApplicationPropertyResolver.getGAValueForSalesApplication())
                .isEqualTo("universalGA");
    }

    @Test
    public void getPropertiesWhenHostConfigNullSalesAppKiosk() {
        salesApplicationPropertyResolver.setHostSuffix(null);
        BDDMockito.when(salesApplicationFacade.getCurrentSalesApplication()).thenReturn(SalesApplication.KIOSK);
        Assertions.assertThat(salesApplicationPropertyResolver.getGAValueForSalesApplication()).isEqualTo("kioskGA");
    }

    @Test
    public void getPropertiesWhenHostConfigNullSalesAppMobile() {
        salesApplicationPropertyResolver.setHostSuffix(null);
        BDDMockito.when(salesApplicationFacade.getCurrentSalesApplication()).thenReturn(SalesApplication.MOBILEAPP);
        Assertions.assertThat(salesApplicationPropertyResolver.getGAValueForSalesApplication()).isEqualTo("mobileGA");
    }

    @Test
    public void getPropertiesWhenHostConfigNullSalesAppWeb() {
        salesApplicationPropertyResolver.setHostSuffix(null);
        BDDMockito.when(salesApplicationFacade.getCurrentSalesApplication()).thenReturn(SalesApplication.WEB);
        Assertions.assertThat(salesApplicationPropertyResolver.getGAValueForSalesApplication())
                .isEqualTo("universalGA");
    }

    @Test
    public void getPropertiesWhenSalesAppKiosk() {
        salesApplicationPropertyResolver.setHostSuffix("dev");
        BDDMockito.when(salesApplicationFacade.getCurrentSalesApplication()).thenReturn(SalesApplication.KIOSK);
        Assertions.assertThat(salesApplicationPropertyResolver.getGAValueForSalesApplication()).isEqualTo("kioskGA");
    }

    @Test
    public void getPropertiesWhenSalesAppMobile() {
        salesApplicationPropertyResolver.setHostSuffix("dev");
        BDDMockito.when(salesApplicationFacade.getCurrentSalesApplication()).thenReturn(SalesApplication.MOBILEAPP);
        Assertions.assertThat(salesApplicationPropertyResolver.getGAValueForSalesApplication()).isEqualTo("mobileGA");
    }

    @Test
    public void getPropertiesWhenSalesAppWeb() {
        salesApplicationPropertyResolver.setHostSuffix("dev");
        BDDMockito.when(salesApplicationFacade.getCurrentSalesApplication()).thenReturn(SalesApplication.WEB);
        Assertions.assertThat(salesApplicationPropertyResolver.getGAValueForSalesApplication())
                .isEqualTo("universalGA");
    }
}
