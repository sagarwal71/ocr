/**
 * 
 */
package au.com.target.tgtfacades.stl.converters.populator;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Assert;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtfacades.looks.data.LooksCollectionData;
import au.com.target.tgtfacades.looks.data.ShopTheLookPageData;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;
import au.com.target.tgtmarketing.shopthelook.ShopTheLookService;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetShopTheLookPageDataPopulatorTest {

    @InjectMocks
    private final TargetShopTheLookPageDataPopulator populator = new TargetShopTheLookPageDataPopulator();

    @Mock
    private ShopTheLookService shopTheLookService;

    @Mock
    private Converter<TargetLookModel, LookDetailsData> targetLookDetailsDataConverter;

    @Mock
    private Converter<TargetLookCollectionModel, LooksCollectionData> targetLooksCollectionDataConverter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testPopulate() throws IllegalArgumentException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetShopTheLookModel source = Mockito.mock(TargetShopTheLookModel.class);
        final ShopTheLookPageData target = new ShopTheLookPageData();
        when(source.getName()).thenReturn("name");
        when(source.getDescription()).thenReturn("description");
        when(source.getId()).thenReturn("id");
        final TargetLookCollectionModel collection1 = new TargetLookCollectionModel();
        final TargetLookCollectionModel collection2 = new TargetLookCollectionModel();

        final TargetLookModel look1 = new TargetLookModel();
        look1.setStartDate(DateUtils.addDays(new Date(), 4));
        look1.setCollection(collection1);
        final TargetLookModel look2 = new TargetLookModel();
        look2.setStartDate(DateUtils.addDays(new Date(), 3));
        look2.setCollection(collection1);
        final TargetLookModel look3 = new TargetLookModel();
        look3.setStartDate(DateUtils.addDays(new Date(), 2));
        look3.setCollection(collection2);
        final TargetLookModel look4 = new TargetLookModel();
        look4.setStartDate(DateUtils.addDays(new Date(), 1));
        look4.setCollection(collection2);

        final LookDetailsData lookData1 = new LookDetailsData();
        final LookDetailsData lookData2 = new LookDetailsData();
        final LookDetailsData lookData3 = new LookDetailsData();
        final LookDetailsData lookData4 = new LookDetailsData();

        final LooksCollectionData collectionData1 = new LooksCollectionData();
        final LooksCollectionData collectionData2 = new LooksCollectionData();

        when(targetLookDetailsDataConverter.convert(look1)).thenReturn(lookData1);
        when(targetLookDetailsDataConverter.convert(look2)).thenReturn(lookData2);
        when(targetLookDetailsDataConverter.convert(look3)).thenReturn(lookData3);
        when(targetLookDetailsDataConverter.convert(look4)).thenReturn(lookData4);
        when(
                targetLooksCollectionDataConverter.convert(Mockito.any(TargetLookCollectionModel.class),
                        Mockito.any(LooksCollectionData.class))).thenReturn(collectionData1, collectionData2);

        final List<TargetLookModel> lookModels = Arrays.asList(look1, look2, look3, look4);
        when(shopTheLookService.getListOfLooksForShopTheLookUsingCode("id")).thenReturn(lookModels);

        populator.populate(source, target);

        Assert.assertEquals("name", target.getTitle());
        Assert.assertEquals("description", target.getDescription());
        Assert.assertEquals(4, target.getCountOfLooks());
        Assert.assertEquals(Arrays.asList(lookData1, lookData2, lookData3, lookData4), target.getLooks());
        Assert.assertEquals(Arrays.asList(collectionData1, collectionData2), target.getCollections());
    }

    @Test
    public void testPopulateWithEmptyModelList() throws IllegalArgumentException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetShopTheLookModel source = Mockito.mock(TargetShopTheLookModel.class);
        final ShopTheLookPageData target = new ShopTheLookPageData();
        when(source.getName()).thenReturn("name");
        when(source.getDescription()).thenReturn("description");
        when(source.getId()).thenReturn("id");
        when(shopTheLookService.getListOfLooksForShopTheLookUsingCode("id")).thenReturn(null);

        populator.populate(source, target);

        Assert.assertEquals("name", target.getTitle());
        Assert.assertEquals("description", target.getDescription());
        Assert.assertEquals(0, target.getCountOfLooks());
        Assert.assertNull(target.getLooks());
        Assert.assertNull(target.getCollections());
    }

}
