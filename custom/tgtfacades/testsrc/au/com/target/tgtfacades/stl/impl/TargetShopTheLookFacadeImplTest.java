/**
 * 
 */
package au.com.target.tgtfacades.stl.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.fest.util.Collections;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtfacades.looks.data.ShopTheLookPageData;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetLookProductModel;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;
import au.com.target.tgtmarketing.shopthelook.LookService;
import au.com.target.tgtmarketing.shopthelook.ShopTheLookService;
import au.com.target.tgtmarketing.shopthelook.dao.ShopTheLookDao;
import org.junit.Assert;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetShopTheLookFacadeImplTest {

    @InjectMocks
    private final TargetShopTheLookFacadeImpl targetShopTheLookFacadeImpl = new TargetShopTheLookFacadeImpl();

    @Mock
    private ShopTheLookService shopTheLookService;

    @Mock
    private Converter<TargetShopTheLookModel, ShopTheLookPageData> targetShopTheLookPageDataConverter;

    @Mock
    private LookService lookService;

    @Mock
    private Converter<TargetLookModel, LookDetailsData> targetLookDetailsDataConverter;
    @Mock
    private SearchRestrictionService searchRestrictionService;
    @Mock
    private ShopTheLookDao shopTheLookDao;

    @Test
    public void testGetVisibleLookForCollection() throws ParseException {
        final List<TargetLookModel> looks = Arrays.asList(Mockito.mock(TargetLookModel.class),
                Mockito.mock(TargetLookModel.class));
        final String code = "L01";
        BDDMockito.when(lookService.getVisibleLooksForCollection(code)).thenReturn(looks);

        final List<TargetLookModel> looksForCollection = targetShopTheLookFacadeImpl.getVisibleLooksForCollection(code);
        Assert.assertEquals(looks, looksForCollection);
    }

    @Test
    public void testGetShopTheLookByCode() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final String code = "testCode";
        final TargetShopTheLookModel model = Mockito.mock(TargetShopTheLookModel.class);
        Mockito.when(shopTheLookService.getShopTheLookForCode(code)).thenReturn(model);
        final TargetShopTheLookModel result = targetShopTheLookFacadeImpl.getShopTheLookByCode(code);
        Assert.assertEquals(model, result);
    }

    @Test
    public void testGetShopTheLookByCodeWithNoModelFound() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final String code = "testCode";
        Mockito.when(shopTheLookService.getShopTheLookForCode(code)).thenThrow(
                new TargetUnknownIdentifierException("TargetUnknownIdentifierException"));
        final TargetShopTheLookModel result = targetShopTheLookFacadeImpl.getShopTheLookByCode(code);
        Assert.assertNull(result);
    }

    @Test
    public void testGetShopTheLookByCodeWithAmbiguousResult() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final String code = "testCode";
        Mockito.when(shopTheLookService.getShopTheLookForCode(code)).thenThrow(
                new TargetAmbiguousIdentifierException("TargetAmbiguousIdentifierException"));
        final TargetShopTheLookModel result = targetShopTheLookFacadeImpl.getShopTheLookByCode(code);
        Assert.assertNull(result);
    }

    @Test
    public void testPopulateShopTheLookPageData() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final String code = "testCode";
        final ShopTheLookPageData data = Mockito.mock(ShopTheLookPageData.class);
        final TargetShopTheLookModel model = Mockito.mock(TargetShopTheLookModel.class);
        Mockito.when(shopTheLookService.getShopTheLookForCode(code)).thenReturn(model);
        Mockito.when(targetShopTheLookPageDataConverter.convert(model)).thenReturn(data);
        final ShopTheLookPageData result = targetShopTheLookFacadeImpl.populateShopTheLookPageData(code);
        Assert.assertEquals(data, result);
    }

    @Test
    public void testPopulateShopTheLookPageDataWithNullModel() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final String code = "testCode";
        Mockito.when(shopTheLookService.getShopTheLookForCode(code)).thenReturn(null);
        final ShopTheLookPageData result = targetShopTheLookFacadeImpl.populateShopTheLookPageData(code);
        Assert.assertNull(result);
    }

    @Test
    public void testGetLookByCode() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final String code = "testCode";
        final TargetLookModel model = Mockito.mock(TargetLookModel.class);
        Mockito.when(lookService.getLookForCode(code)).thenReturn(model);
        final TargetLookModel result = targetShopTheLookFacadeImpl.getLookByCode(code);
        Assert.assertEquals(model, result);
    }

    @Test
    public void testGetLookByCodeWithNoModelFound() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final String code = "testCode";
        Mockito.when(lookService.getLookForCode(code)).thenThrow(
                new TargetUnknownIdentifierException("TargetUnknownIdentifierException"));
        final TargetLookModel result = targetShopTheLookFacadeImpl.getLookByCode(code);
        Assert.assertNull(result);
    }

    @Test
    public void testGetLookByCodeWithAmbiguousResult() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final String code = "testCode";
        Mockito.when(lookService.getLookForCode(code)).thenThrow(
                new TargetAmbiguousIdentifierException("TargetAmbiguousIdentifierException"));
        final TargetLookModel result = targetShopTheLookFacadeImpl.getLookByCode(code);
        Assert.assertNull(result);
    }

    @Test
    public void testPopulateLookDetailsData() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final String code = "testCode";
        final TargetLookModel model = Mockito.mock(TargetLookModel.class);
        final LookDetailsData lookDetailsData = Mockito.mock(LookDetailsData.class);
        Mockito.when(lookService.getLookForCode(code)).thenReturn(model);
        Mockito.when(targetLookDetailsDataConverter.convert(model)).thenReturn(lookDetailsData);
        final LookDetailsData result = targetShopTheLookFacadeImpl.populateLookDetailsData(code);
        Assert.assertEquals(lookDetailsData, result);
    }

    @Test
    public void testPopulateLookDetailsDataWithNullModel() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final String code = "testCode";
        Mockito.when(lookService.getLookForCode(code)).thenReturn(null);
        final LookDetailsData result = targetShopTheLookFacadeImpl.populateLookDetailsData(code);
        Assert.assertNull(result);
    }

    @Test
    public void testGetProductCodeListByLook() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final String code = "testCode";
        final TargetLookModel model = Mockito.mock(TargetLookModel.class);
        Mockito.when(lookService.getLookForCode(code)).thenReturn(model);

        final TargetLookProductModel lookProductModel1 = new TargetLookProductModel();
        lookProductModel1.setProductCode("product1");
        lookProductModel1.setPosition(9);
        final TargetLookProductModel lookProductModel2 = new TargetLookProductModel();
        lookProductModel2.setProductCode("product2");
        lookProductModel2.setPosition(0);
        final TargetLookProductModel lookProductModel3 = new TargetLookProductModel();
        lookProductModel3.setProductCode("product3");
        lookProductModel3.setPosition(3);
        final TargetLookProductModel lookProductModel4 = new TargetLookProductModel();
        lookProductModel4.setProductCode("product4");
        lookProductModel4.setPosition(4);

        final Set<TargetLookProductModel> lookProductModelList = Collections.set(lookProductModel1, lookProductModel2,
                lookProductModel3, lookProductModel4);

        Mockito.when(model.getProducts()).thenReturn(lookProductModelList);

        final List<String> result = targetShopTheLookFacadeImpl.getProductCodeListByLook(code);

        Assert.assertEquals("product2", result.get(0));
        Assert.assertEquals("product3", result.get(1));
        Assert.assertEquals("product4", result.get(2));
        Assert.assertEquals("product1", result.get(3));
    }

    @Test
    public void testGetShopTheLookByCategoryCode()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final String code = "W12345";
        final TargetShopTheLookModel model = mock(TargetShopTheLookModel.class);
        given(shopTheLookService.getShopTheLookByCategoryCode(code)).willReturn(model);
        willReturn(Boolean.FALSE).given(searchRestrictionService).isSearchRestrictionsEnabled();
        final TargetShopTheLookModel result = targetShopTheLookFacadeImpl.getShopTheLookByCategoryCode(code);
        assertThat(result).isEqualTo(result);
    }

}
