/**
 * 
 */
package au.com.target.tgtfacades.stl.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.looks.data.LooksCollectionData;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetLooksCollectionDataPopulatorTest {

    @InjectMocks
    private final TargetLooksCollectionDataPopulator populator = new TargetLooksCollectionDataPopulator();

    @Test
    public void testPopluator() {
        final TargetLookCollectionModel source = Mockito.mock(TargetLookCollectionModel.class);
        Mockito.when(source.getId()).thenReturn("id");
        Mockito.when(source.getName()).thenReturn("name");
        Mockito.when(source.getDescription()).thenReturn("description");
        final LooksCollectionData target = new LooksCollectionData();
        populator.populate(source, target);
        Assert.assertEquals("id", target.getId());
        Assert.assertEquals("name", target.getName());
        Assert.assertEquals("description", target.getDescription());
    }

    @Test
    public void testPopluatorWithNullSource() {
        final TargetLookCollectionModel source = null;
        final LooksCollectionData target = new LooksCollectionData();
        populator.populate(source, target);
        Assert.assertNull(target.getId());
        Assert.assertNull(target.getName());
        Assert.assertNull(target.getDescription());
        Assert.assertEquals(0, target.getCountOfLooks());
    }
}
