package au.com.target.tgtfacades.product.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.variants.model.VariantTypeModel;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.url.impl.TargetProductDataUrlResolver;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetVariantProductListerBasicDataPopulatorTest {

    @Mock
    private TargetProductDataUrlResolver targetProductDataUrlResolver;

    @InjectMocks
    private final TargetVariantProductListerBasicDataPopulator targetVariantProductListerBasicDataPopulator = new TargetVariantProductListerBasicDataPopulator();

    @Test
    public void testPopulate() throws Exception {
        final AbstractTargetVariantProductModel source = Mockito.mock(AbstractTargetVariantProductModel.class);
        final TargetVariantProductListerData target = new TargetVariantProductListerData();
        BDDMockito.given(source.getCode()).willReturn("code");
        BDDMockito.given(source.getDisplayName()).willReturn("name");
        BDDMockito.given(targetProductDataUrlResolver.resolve("name", "code")).willReturn("url");
        BDDMockito.given(source.getItemtype()).willReturn("itemtype");
        final VariantTypeModel variantTypeModel = Mockito.mock(VariantTypeModel.class);
        BDDMockito.given(source.getVariantType()).willReturn(variantTypeModel);
        given(source.getPreview()).willReturn(Boolean.TRUE);
        given(source.getEan()).willReturn("1234567890123");
        BDDMockito.given(variantTypeModel.getCode()).willReturn("variantCode");
        targetVariantProductListerBasicDataPopulator.populate(source, target);
        Assertions.assertThat(target.getName()).isEqualTo("name");
        Assertions.assertThat(target.getCode()).isEqualTo("code");
        Assertions.assertThat(target.getItemType()).isEqualTo("itemtype");
        Assertions.assertThat(target.getVariantType()).isEqualTo("variantCode");
        assertThat(target.isPreview()).isTrue();
        assertThat(target.getApn()).isEqualTo("1234567890123");
    }

}
