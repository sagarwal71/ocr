/**
 *
 */
package au.com.target.tgtfacades.product.converters.populator;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.promotions.PromotionResultService;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.jalo.TargetDealWithRewardResult;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.model.TargetDealWithRewardResultModel;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPromotionsPopulatorTest {

    @InjectMocks
    private TargetPromotionsPopulator targetPromotionsPopulator;
    
    private JaloSession session;
    private SessionContext ctx;
  
    @Mock
    private PromotionResultService promotionResultService;
    
    @Mock
    private ModelService modelService;
    
    @Before
    public void setUp() {
        
        session = mock(JaloSession.class);
        ctx = mock(SessionContext.class);

    }

    @Test
    public void testGetFiredPromotionsMessagesWithPromotionResult() {

        final String description = "This item is part of a deal";
        final AbstractPromotionModel abstractPromotion = mock(ProductPromotionModel.class);
   
        final PromotionOrderResults promoOrderResults = mock(PromotionOrderResults.class);
        
        final List<PromotionResultModel> firedPromotionResultModels = new ArrayList<>();
        final PromotionResultModel resultModel1 = mock(PromotionResultModel.class);
        firedPromotionResultModels.add(resultModel1);
        given(resultModel1.getPromotion()).willReturn(abstractPromotion);
        given(promotionResultService.getDescription(resultModel1)).willReturn(description);
        
        given(promotionResultService.getFiredProductPromotions(promoOrderResults, abstractPromotion)).willReturn(firedPromotionResultModels);
  
        final List<String> descList = targetPromotionsPopulator.getFiredPromotionsMessages(promoOrderResults,
                abstractPromotion);
        Assert.assertEquals(1, descList.size());
        Assert.assertEquals(description, descList.get(0));


    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetFiredPromotionsMessagesWithTargetDelRewardResult() {

        final AbstractPromotionModel abstractPromotion = mock(ProductPromotionModel.class);
  
        final PromotionOrderResults promoOrderResults = mock(PromotionOrderResults.class);
        
        final List<PromotionResultModel> firedPromotionResultModels = new ArrayList<>();
        final TargetDealWithRewardResultModel resultModel1 = mock(TargetDealWithRewardResultModel.class);
        final TargetDealWithRewardResultModel resultModel2 = mock(TargetDealWithRewardResultModel.class);
        firedPromotionResultModels.add(resultModel1);
        firedPromotionResultModels.add(resultModel2);
        given(resultModel1.getPromotion()).willReturn(abstractPromotion);
        given(resultModel2.getPromotion()).willReturn(abstractPromotion);
        given(promotionResultService.getFiredProductPromotions(promoOrderResults, abstractPromotion)).willReturn(firedPromotionResultModels);
        final TargetDealWithRewardResult mockTargetDealWithRewardResult = mock(TargetDealWithRewardResult.class);
        given(modelService.getSource(resultModel1)).willReturn(mockTargetDealWithRewardResult);
        given(modelService.getSource(resultModel2)).willReturn(mockTargetDealWithRewardResult);
        given(mockTargetDealWithRewardResult.getCouldhaveMoreRewards()).willReturn(Boolean.TRUE);
      
        final List<String> descList = targetPromotionsPopulator.getFiredPromotionsMessages(promoOrderResults,
                abstractPromotion);
        Assert.assertEquals(descList.size(), 0);

    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetCouldFiredPromotionsMessagesWithPromotionResult() {

        final String description = "This item is part of a deal";
        final AbstractPromotionModel abstractPromotion = mock(ProductPromotionModel.class);
        final PromotionOrderResults promoOrderResults = mock(PromotionOrderResults.class);
  
        final List<PromotionResultModel> promotionResultModels = new ArrayList<>();
        final PromotionResultModel resultModel1 = mock(PromotionResultModel.class);
        final PromotionResultModel resultModel2 = mock(PromotionResultModel.class);
        promotionResultModels.add(resultModel1);
        promotionResultModels.add(resultModel2);
        
        final List<PromotionResult> results = new ArrayList<>();
        
        final PromotionResult mockPromotionResult = mock(PromotionResult.class);
        results.add(mockPromotionResult);
        results.add(mockPromotionResult);
        
        given(mockPromotionResult.getSession()).willReturn(session);
        given(mockPromotionResult.getSession().getSessionContext()).willReturn(ctx);
        given(mockPromotionResult.getCouldFire()).willReturn(Boolean.TRUE);
        given(promoOrderResults.getAllProductPromotions()).willReturn(results);
        given(modelService.getAll(Mockito.eq(results), Mockito.anyCollection())).willReturn(promotionResultModels);
        
      
        given(resultModel1.getPromotion()).willReturn(abstractPromotion);
        given(resultModel2.getPromotion()).willReturn(abstractPromotion);
  
        given(modelService.getSource(resultModel1)).willReturn(mockPromotionResult);
        given(modelService.getSource(resultModel2)).willReturn(mockPromotionResult);
        
        given(promotionResultService.getDescription(resultModel1)).willReturn(description);
        given(promotionResultService.getDescription(resultModel2)).willReturn(description);
  
        final List<String> descList = targetPromotionsPopulator.getCouldFirePromotionsMessages(promoOrderResults,
                abstractPromotion);
        Assert.assertEquals(2, descList.size());
        Assert.assertEquals(description, descList.get(0));
        Assert.assertEquals(description, descList.get(1));


    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetCouldFiredPromotionsMessagesWithTargetDealRewardResult() {

        final AbstractPromotionModel abstractPromotion = mock(ProductPromotionModel.class);
        final PromotionOrderResults promoOrderResults = mock(PromotionOrderResults.class);

        final List<PromotionResultModel> promotionResultModels = new ArrayList<>();
        final TargetDealWithRewardResultModel resultModel1 = mock(TargetDealWithRewardResultModel.class);
        final TargetDealWithRewardResultModel resultModel2 = mock(TargetDealWithRewardResultModel.class);
        promotionResultModels.add(resultModel1);
        promotionResultModels.add(resultModel2);
  
        final List<PromotionResult> results = new ArrayList<>();
        final TargetDealWithRewardResult result1 = mock(TargetDealWithRewardResult.class);
        final TargetDealWithRewardResult result2 = mock(TargetDealWithRewardResult.class);
        results.add(result1);
        results.add(result2);
  
        given(result1.getSession()).willReturn(session);
        given(result1.getSession().getSessionContext()).willReturn(ctx);
        given(result1.getCouldFire()).willReturn(Boolean.TRUE);
        given(result1.getCouldhaveMoreRewards()).willReturn(Boolean.FALSE);
        
        given(result2.getSession()).willReturn(session);
        given(result2.getSession().getSessionContext()).willReturn(ctx);
        given(result2.getCouldFire()).willReturn(Boolean.FALSE);
        given(result2.getCouldhaveMoreRewards()).willReturn(Boolean.TRUE);
        
        given(promoOrderResults.getAllProductPromotions()).willReturn(results);
        given(modelService.getAll(Mockito.eq(results), Mockito.anyCollection())).willReturn(promotionResultModels);
  
        given(resultModel1.getPromotion()).willReturn(abstractPromotion);
        given(resultModel2.getPromotion()).willReturn(abstractPromotion);
    
        given(modelService.getSource(resultModel1)).willReturn(result1);
        given(modelService.getSource(resultModel2)).willReturn(result2);
        
        final List<String> descList = targetPromotionsPopulator.getCouldFirePromotionsMessages(promoOrderResults,
                abstractPromotion);
        Assert.assertEquals(2, descList.size());


    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetCouldFiredPromotionsMessagesWithTargetDealRewardResultFired() {
        final AbstractPromotionModel abstractPromotion = mock(ProductPromotionModel.class);
        final PromotionOrderResults promoOrderResults = mock(PromotionOrderResults.class);
    
        final List<PromotionResultModel> promotionResultModels = new ArrayList<>();
        final TargetDealWithRewardResultModel resultModel1 = mock(TargetDealWithRewardResultModel.class);
        final TargetDealWithRewardResultModel resultModel2 = mock(TargetDealWithRewardResultModel.class);
        promotionResultModels.add(resultModel1);
        promotionResultModels.add(resultModel2);
    
        final List<PromotionResult> results = new ArrayList<>();
        final TargetDealWithRewardResult result1 = mock(TargetDealWithRewardResult.class);
        final TargetDealWithRewardResult result2 = mock(TargetDealWithRewardResult.class);
        results.add(result1);
        results.add(result2);
    
        given(result1.getSession()).willReturn(session);
        given(result1.getSession().getSessionContext()).willReturn(ctx);
        given(result1.getCouldFire()).willReturn(Boolean.FALSE);
        given(result1.getCouldhaveMoreRewards()).willReturn(Boolean.FALSE);
    
        given(result2.getSession()).willReturn(session);
        given(result2.getSession().getSessionContext()).willReturn(ctx);
        given(result2.getCouldFire()).willReturn(Boolean.FALSE);
        given(result2.getCouldhaveMoreRewards()).willReturn(Boolean.FALSE);
    
        given(promoOrderResults.getAllProductPromotions()).willReturn(results);
        given(modelService.getAll(Mockito.eq(results), Mockito.anyCollection())).willReturn(promotionResultModels);
    
        given(resultModel1.getPromotion()).willReturn(abstractPromotion);
        given(resultModel2.getPromotion()).willReturn(abstractPromotion);
    
        given(modelService.getSource(resultModel1)).willReturn(result1);
        given(modelService.getSource(resultModel2)).willReturn(result2);
        
        final List<String> descList = targetPromotionsPopulator.getCouldFirePromotionsMessages(promoOrderResults,
                abstractPromotion);
        Assert.assertEquals(descList.size(), 0);
        
    }

}
