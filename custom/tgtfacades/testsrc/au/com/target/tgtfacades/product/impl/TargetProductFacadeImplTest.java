package au.com.target.tgtfacades.product.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.stock.strategy.StockLevelStatusStrategy;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantTypeModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.deals.strategy.TargetDealDescriptionStrategy;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.jalo.TargetColourVariantProduct;
import au.com.target.tgtcore.jalo.TargetSizeVariantProduct;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.product.TargetProductDepartmentService;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtcore.product.data.TargetProductCountMerchDepRowData;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfacades.category.exceptions.NotAPrimarySuperCategoryException;
import au.com.target.tgtfacades.kiosk.product.data.BulkyBoardProductData;
import au.com.target.tgtfacades.product.data.TargetProductCountDepartmentData;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.data.FluentStock;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.service.FluentStockLookupService;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtsale.product.data.POSProduct;
import au.com.target.tgtsale.stock.service.TargetStoreStockService;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


/**
 * Test suite for {@link TargetProductFacadeImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductFacadeImplTest {

    private static final String CATEGORY_CODE = "12345";

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException exception = ExpectedException.none();
    //CHECKSTYLE:ON

    @Mock
    private CategoryService categoryService;

    @Mock
    private TargetProductService productService;

    @Mock
    private Converter<ProductModel, ProductData> productConverter;

    @Mock
    private TargetPointOfServiceService targetPointOfServiceService;

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private TargetStockService targetStockService;

    @Mock
    private StockLevelStatusStrategy stockLevelStatusStrategy;

    @Mock
    private TargetProductDepartmentService targetProductDepartmentService;

    @Mock
    private CategoryModel category;

    @Mock
    private ProductModel product;

    @Mock
    private TargetProductData mockProductData;

    @Mock
    private TargetProductData data;

    @Mock
    private TargetDealDescriptionStrategy mockTargetDealDescStrategy;

    @Mock
    private CatalogVersionModel catalogVersionModel;

    @Mock
    private CatalogVersionService catalogVersionService;

    @Mock
    private Converter<ProductModel, ProductData> productUrlConverter;

    @Mock
    private TargetStoreStockService targetStoreStockService;

    @Mock
    private FluentStockLookupService fluentStockLookupService;

    @Mock
    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;

    @InjectMocks
    @Spy
    private final TargetProductFacadeImpl facade = new TargetProductFacadeImpl();

    /**
     * Initializes test cases before run.
     */
    @Before
    public void setUp() {
        when(categoryService.getCategoryForCode(CATEGORY_CODE)).thenReturn(category);
        when(productService.getOnlineProductsForCategory(category)).thenReturn(ImmutableList.of(product));

        when(category.getCategories()).thenReturn(ImmutableList.<CategoryModel> of());
    }

    /**
     * Verifies that exceptions thrown from category service, e.g. unknown or ambiguous category code, are bubbled up
     * from facade layer.
     * 
     * @throws NotAPrimarySuperCategoryException
     *             if given category code does not represent a leaf category
     */
    @Test
    public void testUnknownCategoryCodeGivenToFindProducts() throws NotAPrimarySuperCategoryException {
        when(categoryService.getCategoryForCode(CATEGORY_CODE)).thenThrow(new UnknownIdentifierException(""));
        exception.expect(UnknownIdentifierException.class);
        facade.getProductsForPrimarySuperCategory(CATEGORY_CODE);
    }

    /**
     * Verifies that expected exception is thrown if not a leaf category was requested for associated product retrieval.
     * 
     * @throws NotAPrimarySuperCategoryException
     *             if given category code does not represent a leaf category
     */
    @Test
    public void testNotALeafCategoryGiven() throws NotAPrimarySuperCategoryException {
        when(category.getCategories()).thenReturn(ImmutableList.of(category));
        exception.expect(NotAPrimarySuperCategoryException.class);
        facade.getProductsForPrimarySuperCategory(CATEGORY_CODE);
    }

    /**
     * Verifies that {@link TargetProductFacadeImpl#getProductsForPrimarySuperCategory(String)} will return DTOs for
     * only {@link au.com.target.tgtcore.model.TargetColourVariantProductModel} instances.
     * 
     * @throws NotAPrimarySuperCategoryException
     *             if given category code does not represent a leaf category
     */
    @Test
    public void testProductForCategoryReturnsOnlyBaseProduct() throws NotAPrimarySuperCategoryException {
        final VariantProductModel mockColourVariantUnderBaseProduct = mock(TargetColourVariantProductModel.class);
        final TargetProductModel mockProductModel = mock(TargetProductModel.class);
        when(mockProductModel.getVariants()).thenReturn(Collections.singletonList(mockColourVariantUnderBaseProduct));

        final TargetColourVariantProductModel mockColourVariantModel = mock(TargetColourVariantProductModel.class);
        when(productService.getProductsForPrimarySuperCategory(category)).thenReturn(
                ImmutableList.of(mockColourVariantModel, mockProductModel));
        when(productConverter.convert(mockColourVariantUnderBaseProduct)).thenReturn(mockProductData);
        when(category.getProducts()).thenReturn(Collections.singletonList((ProductModel)mockProductModel));

        final List<TargetProductData> results = facade.getProductsForPrimarySuperCategory(CATEGORY_CODE);
        assertEquals(mockProductData, results.get(0));
    }

    @Test
    public void testGetBulkyBoardProductForOptionsWithNoStoreNumber() {
        final TargetProductModel mockProduct = mock(TargetProductModel.class);

        given(productConverter.convert(mockProduct)).willReturn(mockProductData);
        given(facade.getProductForCodeAndOptions(any(String.class), any(Collection.class))).willReturn(mockProductData);

        final TargetProductData result = (TargetProductData)facade.getBulkyBoardProductForOptions(mockProduct, null,
                null);
        assertThat(result).isNotNull().isEqualTo(mockProductData);
        assertThat(result.getBulkyBoard()).isNull();

        verifyZeroInteractions(targetPointOfServiceService, targetWarehouseService, targetStockService,
                stockLevelStatusStrategy);
    }

    @Test
    public void testGetBulkyBoardProductForOptionsWithWrongModelType() {
        final ProductModel mockProduct = mock(ProductModel.class);

        final TargetProductData productData = new TargetProductData();
        given(productConverter.convert(mockProduct)).willReturn(productData);

        given(facade.getProductForCodeAndOptions(any(String.class), any(Collection.class))).willReturn(productData);

        final TargetProductData result = (TargetProductData)facade.getBulkyBoardProductForOptions(mockProduct, null,
                "5001");
        assertThat(result).isNotNull().isEqualTo(productData);
        assertThat(result.getBulkyBoard()).isNull();

        verifyZeroInteractions(targetPointOfServiceService, targetWarehouseService, targetStockService,
                stockLevelStatusStrategy);
    }

    @Test
    public void testGetBulkyBoardProductForOptionsWithNonBulkyBoardTargetProduct() {
        final TargetProductModel mockProduct = mock(TargetProductModel.class);
        given(mockProduct.getBulkyBoardProduct()).willReturn(Boolean.FALSE);

        final TargetProductData productData = new TargetProductData();
        given(productConverter.convert(mockProduct)).willReturn(productData);

        given(facade.getProductForCodeAndOptions(any(String.class), any(Collection.class))).willReturn(productData);

        final TargetProductData result = (TargetProductData)facade.getBulkyBoardProductForOptions(mockProduct, null,
                "5001");
        assertThat(result).isNotNull().isEqualTo(productData);
        assertThat(result.getBulkyBoard()).isNull();

        verifyZeroInteractions(targetPointOfServiceService, targetWarehouseService, targetStockService,
                stockLevelStatusStrategy);
    }

    @Test
    public void testGetBulkyBoardProductForOptionsWithNonBulkyBoardTargetVariantProduct() {
        final TargetColourVariantProductModel mockProduct = mock(TargetColourVariantProductModel.class);
        given(mockProduct.getBulkyBoardProduct()).willReturn(Boolean.FALSE);

        final TargetProductData productData = new TargetProductData();
        given(productConverter.convert(mockProduct)).willReturn(productData);

        given(facade.getProductForCodeAndOptions(any(String.class), any(Collection.class))).willReturn(productData);

        final TargetProductData result = (TargetProductData)facade.getBulkyBoardProductForOptions(mockProduct, null,
                "5001");
        assertThat(result).isNotNull().isEqualTo(productData);
        assertThat(result.getBulkyBoard()).isNull();

        verifyZeroInteractions(targetPointOfServiceService, targetWarehouseService, targetStockService,
                stockLevelStatusStrategy);
    }

    @Test
    public void testGetBulkyBoardProductForOptionsWithInvalidStore() throws TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException {
        final TargetProductModel mockProduct = mock(TargetProductModel.class);
        given(mockProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);

        final TargetProductData productData = new TargetProductData();
        given(productConverter.convert(mockProduct)).willReturn(productData);

        given(targetPointOfServiceService.getPOSByStoreNumber(any(Integer.class))).willThrow(
                new TargetUnknownIdentifierException("Unknown store"));
        given(facade.getProductForCodeAndOptions(any(String.class), any(Collection.class))).willReturn(productData);

        final TargetProductData result = (TargetProductData)facade.getBulkyBoardProductForOptions(mockProduct, null,
                "5001");
        assertThat(result).isNotNull().isEqualTo(productData);
        assertThat(result.getBulkyBoard()).isNull();

        verifyZeroInteractions(targetWarehouseService, targetStockService, stockLevelStatusStrategy);
    }

    @Test
    public void testGetBulkyBoardProductForOptionsNoWarehouseForStore() throws TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException {
        final TargetProductModel mockProduct = mock(TargetProductModel.class);
        given(mockProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);

        final TargetProductData productData = new TargetProductData();
        given(productConverter.convert(mockProduct)).willReturn(productData);

        final TargetPointOfServiceModel mockTargetPoS = mock(TargetPointOfServiceModel.class);
        given(mockTargetPoS.getName()).willReturn("Frankston");
        given(mockTargetPoS.getStoreNumber()).willReturn(Integer.valueOf(5098));
        given(targetPointOfServiceService.getPOSByStoreNumber(any(Integer.class))).willReturn(mockTargetPoS);
        given(facade.getProductForCodeAndOptions(any(String.class), any(Collection.class))).willReturn(productData);

        given(targetWarehouseService.getWarehouseForPointOfService(mockTargetPoS)).willReturn(null);

        final TargetProductData result = (TargetProductData)facade.getBulkyBoardProductForOptions(mockProduct, null,
                "5001");
        assertThat(result).isNotNull().isEqualTo(productData);
        assertThat(result.getBulkyBoard()).isNull();

        verifyZeroInteractions(targetStockService, stockLevelStatusStrategy);
    }

    @Test
    public void testGetBulkyBoardProductForOptionsNoStockLevelForProductAndStore()
            throws TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException {
        final TargetProductModel mockProduct = mock(TargetProductModel.class);
        given(mockProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);

        final TargetProductData productData = new TargetProductData();
        given(productConverter.convert(mockProduct)).willReturn(productData);

        final TargetPointOfServiceModel mockTargetPoS = mock(TargetPointOfServiceModel.class);
        given(mockTargetPoS.getName()).willReturn("Frankston");
        given(mockTargetPoS.getStoreNumber()).willReturn(Integer.valueOf(5098));
        given(targetPointOfServiceService.getPOSByStoreNumber(any(Integer.class))).willReturn(mockTargetPoS);

        final WarehouseModel mockWarehouse = mock(WarehouseModel.class);
        given(targetWarehouseService.getWarehouseForPointOfService(mockTargetPoS)).willReturn(mockWarehouse);

        given(targetStockService.getStockLevel(mockProduct, mockWarehouse)).willReturn(null);
        given(facade.getProductForCodeAndOptions(any(String.class), any(Collection.class))).willReturn(productData);

        final TargetProductData result = (TargetProductData)facade.getBulkyBoardProductForOptions(mockProduct, null,
                "5001");
        assertThat(result).isNotNull().isEqualTo(productData);
        assertThat(result.getBulkyBoard()).isNull();

        verifyZeroInteractions(stockLevelStatusStrategy);
    }

    @Test
    public void testGetBulkyBoardProductForOptionsOutOfStockInStore()
            throws TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException {
        final TargetProductModel mockProduct = mock(TargetProductModel.class);
        given(mockProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);

        final TargetProductData productData = new TargetProductData();
        given(productConverter.convert(mockProduct)).willReturn(productData);

        final TargetPointOfServiceModel mockTargetPoS = mock(TargetPointOfServiceModel.class);
        given(mockTargetPoS.getName()).willReturn("Frankston");
        given(mockTargetPoS.getStoreNumber()).willReturn(Integer.valueOf(5098));
        given(targetPointOfServiceService.getPOSByStoreNumber(any(Integer.class))).willReturn(mockTargetPoS);

        final WarehouseModel mockWarehouse = mock(WarehouseModel.class);
        given(targetWarehouseService.getWarehouseForPointOfService(mockTargetPoS)).willReturn(mockWarehouse);

        final StockLevelModel mockStockLevel = mock(StockLevelModel.class);
        given(targetStockService.getStockLevel(mockProduct, mockWarehouse)).willReturn(mockStockLevel);

        given(stockLevelStatusStrategy.checkStatus(mockStockLevel)).willReturn(StockLevelStatus.OUTOFSTOCK);
        given(facade.getProductForCodeAndOptions(any(String.class), any(Collection.class))).willReturn(productData);

        final TargetProductData result = (TargetProductData)facade.getBulkyBoardProductForOptions(mockProduct, null,
                "5001");
        assertThat(result).isNotNull().isEqualTo(productData);
        assertThat(result.getBulkyBoard()).isNotNull();

        final BulkyBoardProductData bulkyBoard = result.getBulkyBoard();
        assertThat(bulkyBoard.getPosName()).isEqualTo("Frankston");
        assertThat(bulkyBoard.getPosNumber()).isEqualTo("5098");
        assertThat(bulkyBoard.isAvailableForInStorePurchase()).isTrue();
        assertThat(bulkyBoard.isInStockForInStorePurchase()).isFalse();
    }

    @Test
    public void testGetBulkyBoardProductForOptionsInStockInStore()
            throws TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException {
        final TargetProductModel mockProduct = mock(TargetProductModel.class);
        given(mockProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);

        final TargetProductData productData = new TargetProductData();
        given(productConverter.convert(mockProduct)).willReturn(productData);

        final TargetPointOfServiceModel mockTargetPoS = mock(TargetPointOfServiceModel.class);
        given(mockTargetPoS.getName()).willReturn("Frankston");
        given(mockTargetPoS.getStoreNumber()).willReturn(Integer.valueOf(5098));
        given(targetPointOfServiceService.getPOSByStoreNumber(any(Integer.class))).willReturn(mockTargetPoS);

        final WarehouseModel mockWarehouse = mock(WarehouseModel.class);
        given(targetWarehouseService.getWarehouseForPointOfService(mockTargetPoS)).willReturn(mockWarehouse);

        final StockLevelModel mockStockLevel = mock(StockLevelModel.class);
        given(targetStockService.getStockLevel(mockProduct, mockWarehouse)).willReturn(mockStockLevel);

        given(stockLevelStatusStrategy.checkStatus(mockStockLevel)).willReturn(StockLevelStatus.INSTOCK);
        given(facade.getProductForCodeAndOptions(any(String.class), any(Collection.class))).willReturn(productData);

        final TargetProductData result = (TargetProductData)facade.getBulkyBoardProductForOptions(mockProduct, null,
                "5001");
        assertThat(result).isNotNull().isEqualTo(productData);
        assertThat(result.getBulkyBoard()).isNotNull();

        final BulkyBoardProductData bulkyBoard = result.getBulkyBoard();
        assertThat(bulkyBoard.getPosName()).isEqualTo("Frankston");
        assertThat(bulkyBoard.getPosNumber()).isEqualTo("5098");
        assertThat(bulkyBoard.isAvailableForInStorePurchase()).isTrue();
        assertThat(bulkyBoard.isInStockForInStorePurchase()).isTrue();
    }

    @Test
    public void testGetBulkyBoardProductForOptionsLowStockInStore()
            throws TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException {
        final TargetProductModel mockProduct = mock(TargetProductModel.class);
        given(mockProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);

        final TargetProductData productData = new TargetProductData();
        given(productConverter.convert(mockProduct)).willReturn(productData);

        final TargetPointOfServiceModel mockTargetPoS = mock(TargetPointOfServiceModel.class);
        given(mockTargetPoS.getName()).willReturn("Frankston");
        given(mockTargetPoS.getStoreNumber()).willReturn(Integer.valueOf(5098));
        given(targetPointOfServiceService.getPOSByStoreNumber(any(Integer.class))).willReturn(mockTargetPoS);

        final WarehouseModel mockWarehouse = mock(WarehouseModel.class);
        given(targetWarehouseService.getWarehouseForPointOfService(mockTargetPoS)).willReturn(mockWarehouse);

        final StockLevelModel mockStockLevel = mock(StockLevelModel.class);
        given(targetStockService.getStockLevel(mockProduct, mockWarehouse)).willReturn(mockStockLevel);

        given(stockLevelStatusStrategy.checkStatus(mockStockLevel)).willReturn(StockLevelStatus.LOWSTOCK);
        given(facade.getProductForCodeAndOptions(any(String.class), any(Collection.class))).willReturn(productData);

        final TargetProductData result = (TargetProductData)facade.getBulkyBoardProductForOptions(mockProduct, null,
                "5001");
        assertThat(result).isNotNull().isEqualTo(productData);
        assertThat(result.getBulkyBoard()).isNotNull();

        final BulkyBoardProductData bulkyBoard = result.getBulkyBoard();
        assertThat(bulkyBoard.getPosName()).isEqualTo("Frankston");
        assertThat(bulkyBoard.getPosNumber()).isEqualTo("5098");
        assertThat(bulkyBoard.isAvailableForInStorePurchase()).isTrue();
        assertThat(bulkyBoard.isInStockForInStorePurchase()).isTrue();
    }

    @Test
    public void testGetBulkyBoardProductForOptionsColourVariantInStockInStore()
            throws TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException {
        final TargetColourVariantProductModel mockProduct = mock(TargetColourVariantProductModel.class);
        given(mockProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);

        final TargetProductData productData = new TargetProductData();
        given(productConverter.convert(mockProduct)).willReturn(productData);

        final TargetPointOfServiceModel mockTargetPoS = mock(TargetPointOfServiceModel.class);
        given(mockTargetPoS.getName()).willReturn("Frankston");
        given(mockTargetPoS.getStoreNumber()).willReturn(Integer.valueOf(5098));
        given(targetPointOfServiceService.getPOSByStoreNumber(any(Integer.class))).willReturn(mockTargetPoS);

        final WarehouseModel mockWarehouse = mock(WarehouseModel.class);
        given(targetWarehouseService.getWarehouseForPointOfService(mockTargetPoS)).willReturn(mockWarehouse);

        final StockLevelModel mockStockLevel = mock(StockLevelModel.class);
        given(targetStockService.getStockLevel(mockProduct, mockWarehouse)).willReturn(mockStockLevel);

        given(stockLevelStatusStrategy.checkStatus(mockStockLevel)).willReturn(StockLevelStatus.INSTOCK);

        given(facade.getProductForCodeAndOptions(any(String.class), any(Collection.class))).willReturn(productData);

        final TargetProductData result = (TargetProductData)facade.getBulkyBoardProductForOptions(mockProduct, null,
                "5001");
        assertThat(result).isNotNull().isEqualTo(productData);
        assertThat(result.getBulkyBoard()).isNotNull();

        final BulkyBoardProductData bulkyBoard = result.getBulkyBoard();
        assertThat(bulkyBoard.getPosName()).isEqualTo("Frankston");
        assertThat(bulkyBoard.getPosNumber()).isEqualTo("5098");
        assertThat(bulkyBoard.isAvailableForInStorePurchase()).isTrue();
        assertThat(bulkyBoard.isInStockForInStorePurchase()).isTrue();
    }

    @Test
    public void testValidateStoreNumber() {

        Integer result;

        result = facade.validateStoreNumber("1234");
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(1234);

        result = facade.validateStoreNumber(null);
        assertThat(result).isNull();

        result = facade.validateStoreNumber("blargh");
        assertThat(result).isNull();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testValidatePosData() {

        final String ean = "1111";

        // Price should be present and positive
        // Was is optional but if present is positive

        final POSProduct posProduct = new POSProduct();
        posProduct.setItemCode("1234");
        posProduct.setPriceInCents(100);
        posProduct.setWasPriceInCents(200);

        assertThat(facade.validatePosData(posProduct, ean)).isTrue();

        posProduct.setWasPriceInCents(-1);
        assertThat(facade.validatePosData(posProduct, ean)).isFalse();

        posProduct.setWasPriceInCents(null);
        assertThat(facade.validatePosData(posProduct, ean)).isTrue();

        posProduct.setPriceInCents(0);
        assertThat(facade.validatePosData(posProduct, ean)).isFalse();

        posProduct.setPriceInCents(null);
        assertThat(facade.validatePosData(posProduct, ean)).isFalse();
    }

    @Test
    public void testGetSummaryDealDescription() {
        final TargetColourVariantProductModel model = Mockito.mock(TargetColourVariantProductModel.class);
        facade.getSummaryDealDescription(model);
        Mockito.verify(mockTargetDealDescStrategy, Mockito.times(1)).getSummaryDealDescription(model);
    }

    @Test
    public void testGetSummaryDealDescriptionWithDesc() {
        final TargetColourVariantProductModel model = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(mockTargetDealDescStrategy.getSummaryDealDescription(model)).willReturn("Deal Description");
        assertEquals("Deal Description", facade.getSummaryDealDescription(model));

    }

    @Test
    public void testGetSummaryDealDescriptionWithNullDesc() {
        final TargetColourVariantProductModel model = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(mockTargetDealDescStrategy.getSummaryDealDescription(model)).willReturn(null);
        assertThat(facade.getSummaryDealDescription(model)).isNull();

    }

    @Test
    public void testGetProductForCodeAndOptionsWithDealMessage() {
        final String productCode = "P1234";
        final TargetColourVariantProductModel model = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(productService.getProductForCode(productCode)).willReturn(model);
        BDDMockito.given(mockProductData.getVariantType()).willReturn(
                TargetSizeVariantProduct.class.getSimpleName());
        final Collection<ProductOption> colOptions = Arrays.asList(ProductOption.PROMOTIONS);
        Mockito.doReturn(productCode).when(model).getCode();
        Mockito.doReturn(data).when(facade).getProductForCodeAndOptions(productCode, colOptions);
        Mockito.doReturn(TargetSizeVariantProduct.class.getSimpleName()).when(data).getVariantType();
        facade.getProductForCodeAndOptionsWithDealMessage(productCode, colOptions);
        Mockito.verify(facade, Mockito.times(1)).getSummaryDealDescription(model);
    }

    @Test
    public void testGetProductForCodeAndOptionsWithDealMessageWithNoSizeVariant() {
        final String productCode = "P1234";
        final TargetColourVariantProductModel model = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(productService.getProductForCode(productCode)).willReturn(model);
        BDDMockito.given(mockProductData.getVariantType()).willReturn(
                TargetSizeVariantProduct.class.getSimpleName());
        final Collection<ProductOption> colOptions = Arrays.asList(ProductOption.PROMOTIONS);
        Mockito.doReturn(productCode).when(model).getCode();
        Mockito.doReturn(data).when(facade).getProductForCodeAndOptions(productCode, colOptions);
        Mockito.doReturn(null).when(data).getVariantType();
        facade.getProductForCodeAndOptionsWithDealMessage(productCode, colOptions);
        Mockito.verify(facade, Mockito.never()).getSummaryDealDescription(model);
    }

    @Test
    public void testGetProductforMerchCodeWithNullMerchDept() {

        BDDMockito.given(catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.ONLINE_VERSION)).willReturn(catalogVersionModel);
        BDDMockito.given(
                targetProductDepartmentService.getDepartmentCategoryModel(catalogVersionModel, new Integer(10)))
                .willReturn(null);
        facade.getProductsForMerchantDept(10, 0, 10);
        Mockito.verify(productService, Mockito.never()).getProductsForMerchDeptCode(
                Mockito.any(TargetMerchDepartmentModel.class), Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    public void testGetProductforMerchCodeWithMerchDept() {

        final TargetMerchDepartmentModel merchModel = Mockito.mock(TargetMerchDepartmentModel.class);
        BDDMockito.given(catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.ONLINE_VERSION)).willReturn(catalogVersionModel);
        BDDMockito.given(
                targetProductDepartmentService.getDepartmentCategoryModel(catalogVersionModel, new Integer(10)))
                .willReturn(merchModel);
        final List<TargetColourVariantProductModel> productModelList = new ArrayList<>();
        BDDMockito.given(
                productService.getProductsForMerchDeptCode(Mockito.any(TargetMerchDepartmentModel.class),
                        Mockito.anyInt(), Mockito.anyInt()))
                .willReturn(productModelList);
        facade.getProductsForMerchantDept(10, 0, 10);
        Mockito.verify(productService, Mockito.times(1)).getProductsForMerchDeptCode(
                Mockito.any(TargetMerchDepartmentModel.class), Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    public void testGetProductforMerchCodeWithMerchDeptandProducts() {

        final TargetMerchDepartmentModel merchModel = Mockito.mock(TargetMerchDepartmentModel.class);
        BDDMockito.given(catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.ONLINE_VERSION)).willReturn(catalogVersionModel);
        BDDMockito.given(
                targetProductDepartmentService.getDepartmentCategoryModel(catalogVersionModel, new Integer(10)))
                .willReturn(merchModel);
        final List<TargetColourVariantProductModel> productModelList = new ArrayList<>();
        final TargetColourVariantProductModel productModel = Mockito.mock(TargetColourVariantProductModel.class);
        productModelList.add(productModel);
        BDDMockito.given(
                productService.getProductsForMerchDeptCode(Mockito.any(TargetMerchDepartmentModel.class),
                        Mockito.anyInt(), Mockito.anyInt()))
                .willReturn(productModelList);
        facade.getProductsForMerchantDept(10, 0, 10);
        Mockito.verify(productService, Mockito.times(1)).getProductsForMerchDeptCode(
                Mockito.any(TargetMerchDepartmentModel.class), Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(productUrlConverter, Mockito.atLeastOnce()).convert(Mockito.any(ProductModel.class));
    }

    public void testGetProductPageCountForDepartmentWithEmptyRowData() {
        final List<TargetProductCountMerchDepRowData> rowDataList = new ArrayList<>();
        BDDMockito.given(targetProductDepartmentService.findProductCountForEachMerchDep()).willReturn(rowDataList);
        final List<TargetProductCountDepartmentData> resultList = facade.getProductPageCountForDepartment(2);
        assertThat(resultList.isEmpty());
    }

    @Test
    public void testGetProductPageCountForDepartmentWithInvalidPageSize() {
        final List<TargetProductCountMerchDepRowData> rowDataList = new ArrayList<>();
        final TargetProductCountMerchDepRowData rowData = new TargetProductCountMerchDepRowData();
        rowDataList.add(rowData);
        BDDMockito.given(targetProductDepartmentService.findProductCountForEachMerchDep()).willReturn(rowDataList);
        final List<TargetProductCountDepartmentData> resultList = facade.getProductPageCountForDepartment(0);
        assertThat(resultList.isEmpty());
    }

    @Test
    public void testGetProductPageCountForDepartment() {
        final List<TargetProductCountMerchDepRowData> rowDataList = new ArrayList<>();
        final TargetProductCountMerchDepRowData rowData1 = new TargetProductCountMerchDepRowData();
        final TargetMerchDepartmentModel dModel1 = new TargetMerchDepartmentModel();
        dModel1.setCode("001");
        rowData1.setTargetMerchDepartmentModel(dModel1);
        rowData1.setProductCount(1);
        final TargetProductCountMerchDepRowData rowData2 = new TargetProductCountMerchDepRowData();
        final TargetMerchDepartmentModel dModel2 = new TargetMerchDepartmentModel();
        dModel2.setCode("002");
        rowData2.setTargetMerchDepartmentModel(dModel2);
        rowData2.setProductCount(701);
        rowDataList.add(rowData1);
        rowDataList.add(rowData2);
        BDDMockito.given(targetProductDepartmentService.findProductCountForEachMerchDep()).willReturn(rowDataList);
        final List<TargetProductCountDepartmentData> resultList = facade.getProductPageCountForDepartment(200);
        assertThat(resultList.size() == 2);
        assertEquals("001", resultList.get(0).getTargetMerchDepartmentCode());
        assertEquals(1, resultList.get(0).getPageCount());
        assertEquals("002", resultList.get(1).getTargetMerchDepartmentCode());
        assertEquals(4, resultList.get(1).getPageCount());
    }

    @Test
    public void testGetBaseTargetProductWithNullInput() {
        final AbstractTargetVariantProductModel productModel = null;
        final TargetProductModel result = facade.getBaseTargetProduct(productModel);
        assertEquals(null, result);
    }

    @Test
    public void testGetBaseTargetProductWithColorVariantProductInput() {
        final TargetProductModel targetProductModel = new TargetProductModel();
        final AbstractTargetVariantProductModel colorProductModel = new TargetColourVariantProductModel();
        colorProductModel.setBaseProduct(targetProductModel);
        final TargetProductModel result = facade.getBaseTargetProduct(colorProductModel);
        assertEquals(targetProductModel, result);
    }

    @Test
    public void testGetBaseTargetProductWithSizeVariantProductInput() {
        final TargetProductModel targetProductModel = new TargetProductModel();
        final AbstractTargetVariantProductModel colorProductModel = new TargetColourVariantProductModel();
        final AbstractTargetVariantProductModel sizeProductModel = new TargetSizeVariantProductModel();
        colorProductModel.setBaseProduct(targetProductModel);
        sizeProductModel.setBaseProduct(colorProductModel);
        final TargetProductModel result = facade.getBaseTargetProduct(sizeProductModel);
        assertEquals(targetProductModel, result);
    }

    @Test
    public void testGetProductToRedirectwithMultipleInstockVariant() {
        final AbstractTargetVariantProductModel colorProductModel = new TargetColourVariantProductModel();
        final VariantProductModel sizeProductModel = new TargetSizeVariantProductModel();
        final VariantProductModel sizeProductModelOne = new TargetSizeVariantProductModel();
        final VariantProductModel sizeProductModelTwo = new TargetSizeVariantProductModel();
        colorProductModel.setVariants(Arrays.asList(sizeProductModel, sizeProductModelOne, sizeProductModelTwo));
        when(targetStockService.getProductStatus(sizeProductModel)).thenReturn(
                StockLevelStatus.INSTOCK);
        when(targetStockService.getProductStatus(sizeProductModelOne)).thenReturn(
                StockLevelStatus.INSTOCK);
        when(targetStockService.getProductStatus(sizeProductModelTwo)).thenReturn(
                StockLevelStatus.OUTOFSTOCK);
        final ProductModel result = facade.getProductToRedirect(colorProductModel);
        assertEquals(colorProductModel, result);

    }

    @Test
    public void testGetProductToRedirectWithSingleInStockVariants() {
        final AbstractTargetVariantProductModel colorProductModel = new TargetColourVariantProductModel();
        final VariantProductModel sizeProductModel = new TargetSizeVariantProductModel();
        final VariantProductModel sizeProductModelOne = new TargetSizeVariantProductModel();
        final VariantProductModel sizeProductModelTwo = new TargetSizeVariantProductModel();
        colorProductModel.setVariants(Arrays.asList(sizeProductModel, sizeProductModelOne, sizeProductModelTwo));
        when(targetStockService.getProductStatus(sizeProductModel)).thenReturn(
                StockLevelStatus.INSTOCK);
        when(targetStockService.getProductStatus(sizeProductModelOne)).thenReturn(
                StockLevelStatus.OUTOFSTOCK);
        when(targetStockService.getProductStatus(sizeProductModelTwo)).thenReturn(
                StockLevelStatus.OUTOFSTOCK);
        final ProductModel result = facade.getProductToRedirect(colorProductModel);
        assertEquals(sizeProductModel, result);

    }

    @Test
    public void testGetProductToRedirectWithBaseProduct() {

        final VariantProductModel colorProductModel = new TargetColourVariantProductModel();
        final VariantProductModel sizeProductModel = new TargetSizeVariantProductModel();
        final VariantProductModel sizeProductModelOne = new TargetSizeVariantProductModel();
        final VariantProductModel sizeProductModelTwo = new TargetSizeVariantProductModel();
        colorProductModel.setVariants(Arrays.asList(sizeProductModel, sizeProductModelOne, sizeProductModelTwo));
        final TargetProductModel baseProductModel = new TargetProductModel();
        baseProductModel.setVariants(Arrays.asList(colorProductModel));
        final VariantTypeModel variant = new VariantTypeModel();
        variant.setCode(TargetColourVariantProduct.class.getSimpleName());
        baseProductModel.setVariantType(variant);
        when(targetStockService.getProductStatus(sizeProductModel)).thenReturn(
                StockLevelStatus.INSTOCK);
        when(targetStockService.getProductStatus(sizeProductModelOne)).thenReturn(
                StockLevelStatus.OUTOFSTOCK);
        when(targetStockService.getProductStatus(sizeProductModelTwo)).thenReturn(
                StockLevelStatus.OUTOFSTOCK);
        final ProductModel result = facade.getProductToRedirect(baseProductModel);
        assertEquals(sizeProductModel, result);

    }

    @Test
    public void testGetProductToRedirectWithBaseProductMultipleInStockVairants() {

        final VariantProductModel colorProductModel = new TargetColourVariantProductModel();
        final VariantProductModel sizeProductModel = new TargetSizeVariantProductModel();
        final VariantProductModel sizeProductModelOne = new TargetSizeVariantProductModel();
        final VariantProductModel sizeProductModelTwo = new TargetSizeVariantProductModel();
        colorProductModel.setVariants(Arrays.asList(sizeProductModel, sizeProductModelOne, sizeProductModelTwo));
        final TargetProductModel baseProductModel = new TargetProductModel();
        baseProductModel.setVariants(Arrays.asList(colorProductModel));
        final VariantTypeModel variant = new VariantTypeModel();
        variant.setCode(TargetColourVariantProduct.class.getSimpleName());
        baseProductModel.setVariantType(variant);
        when(targetStockService.getProductStatus(sizeProductModel)).thenReturn(
                StockLevelStatus.INSTOCK);
        when(targetStockService.getProductStatus(sizeProductModelOne)).thenReturn(
                StockLevelStatus.INSTOCK);
        when(targetStockService.getProductStatus(sizeProductModelTwo)).thenReturn(
                StockLevelStatus.OUTOFSTOCK);
        final ProductModel result = facade.getProductToRedirect(baseProductModel);
        assertEquals(colorProductModel, result);

    }

    @Test
    public void testGetProductToRedirectWithBaseProductMultipleVairants() {

        final VariantProductModel colorProductModel = new TargetColourVariantProductModel();
        final VariantProductModel colorProductModelOne = new TargetColourVariantProductModel();
        final VariantProductModel sizeProductModel = new TargetSizeVariantProductModel();
        final VariantProductModel sizeProductModelOne = new TargetSizeVariantProductModel();
        final VariantProductModel sizeProductModelTwo = new TargetSizeVariantProductModel();
        final VariantProductModel sizeProductModelThree = new TargetSizeVariantProductModel();
        colorProductModel.setVariants(Arrays.asList(sizeProductModel, sizeProductModelOne, sizeProductModelTwo));
        colorProductModelOne.setVariants(Arrays.asList(sizeProductModelThree));
        final TargetProductModel baseProductModel = new TargetProductModel();
        baseProductModel.setVariants(Arrays.asList(colorProductModel, colorProductModelOne));
        final VariantTypeModel variant = new VariantTypeModel();
        variant.setCode(TargetColourVariantProduct.class.getSimpleName());
        baseProductModel.setVariantType(variant);
        when(targetStockService.getProductStatus(sizeProductModel)).thenReturn(
                StockLevelStatus.INSTOCK);
        when(targetStockService.getProductStatus(sizeProductModelOne)).thenReturn(
                StockLevelStatus.INSTOCK);
        when(targetStockService.getProductStatus(sizeProductModelTwo)).thenReturn(
                StockLevelStatus.OUTOFSTOCK);
        when(targetStockService.getProductStatus(sizeProductModelThree)).thenReturn(
                StockLevelStatus.INSTOCK);
        final ProductModel result = facade.getProductToRedirect(baseProductModel);
        assertEquals(colorProductModel, result);

    }


    @Test
    public void testGetProductToRedirectWithBaseProductMultipleVairantsAndSingleInstockVariant() {

        final VariantProductModel colorProductModel = new TargetColourVariantProductModel();
        final VariantProductModel colorProductModelOne = new TargetColourVariantProductModel();
        final VariantProductModel sizeProductModel = new TargetSizeVariantProductModel();
        final VariantProductModel sizeProductModelOne = new TargetSizeVariantProductModel();
        final VariantProductModel sizeProductModelTwo = new TargetSizeVariantProductModel();
        final VariantProductModel sizeProductModelThree = new TargetSizeVariantProductModel();
        colorProductModel.setVariants(Arrays.asList(sizeProductModel, sizeProductModelOne, sizeProductModelTwo));
        colorProductModelOne.setVariants(Arrays.asList(sizeProductModelThree));
        final TargetProductModel baseProductModel = new TargetProductModel();
        baseProductModel.setVariants(Arrays.asList(colorProductModelOne, colorProductModel));
        final VariantTypeModel variant = new VariantTypeModel();
        variant.setCode(TargetColourVariantProduct.class.getSimpleName());
        baseProductModel.setVariantType(variant);
        when(targetStockService.getProductStatus(sizeProductModel)).thenReturn(
                StockLevelStatus.INSTOCK);
        when(targetStockService.getProductStatus(sizeProductModelOne)).thenReturn(
                StockLevelStatus.INSTOCK);
        when(targetStockService.getProductStatus(sizeProductModelTwo)).thenReturn(
                StockLevelStatus.OUTOFSTOCK);
        when(targetStockService.getProductStatus(sizeProductModelThree)).thenReturn(
                StockLevelStatus.INSTOCK);
        final ProductModel result = facade.getProductToRedirect(baseProductModel);
        assertEquals(sizeProductModelThree, result);

    }

    @Test
    public void testGetProductToRedirectWithStyleGroupProducts() {

        final VariantProductModel colorProductModel = new TargetColourVariantProductModel();
        final VariantProductModel colorProductModelOne = new TargetColourVariantProductModel();
        final TargetProductModel baseProductModel = new TargetProductModel();
        baseProductModel.setVariants(Arrays.asList(colorProductModelOne, colorProductModel));
        final VariantTypeModel variant = new VariantTypeModel();
        variant.setCode(TargetColourVariantProduct.class.getSimpleName());
        baseProductModel.setVariantType(variant);
        when(targetStockService.getProductStatus(colorProductModel)).thenReturn(
                StockLevelStatus.INSTOCK);
        when(targetStockService.getProductStatus(colorProductModelOne)).thenReturn(
                StockLevelStatus.OUTOFSTOCK);
        final ProductModel result = facade.getProductToRedirect(baseProductModel);
        assertEquals(colorProductModel, result);

    }

    @Test
    public void testGetProductToRedirectWithAllProductsOutOfStock() {

        final VariantProductModel colorProductModel = new TargetColourVariantProductModel();
        final VariantProductModel colorProductModelOne = new TargetColourVariantProductModel();
        final TargetProductModel baseProductModel = new TargetProductModel();
        baseProductModel.setVariants(Arrays.asList(colorProductModelOne, colorProductModel));
        final VariantTypeModel variant = new VariantTypeModel();
        variant.setCode(TargetColourVariantProduct.class.getSimpleName());
        baseProductModel.setVariantType(variant);
        when(targetStockService.getProductStatus(colorProductModel)).thenReturn(
                StockLevelStatus.OUTOFSTOCK);
        when(targetStockService.getProductStatus(colorProductModelOne)).thenReturn(
                StockLevelStatus.OUTOFSTOCK);
        final ProductModel result = facade.getProductToRedirect(baseProductModel);
        assertEquals(colorProductModelOne, result);

    }

    @Test
    public void testGetAllVariantsWithStockByBaseProductCode() {
        prepareP1000();
        final List<ProductData> allVariants = facade.getAllSellableVariantsWithOptions("P1000",
                Arrays.asList(ProductOption.STOCK));
        assertThat(allVariants).hasSize(4);
    }

    @Test
    public void testGetAllVariantsWithStockByColourVariant() {
        prepareP1000();
        final List<ProductData> allVariants = facade.getAllSellableVariantsWithOptions("P1000_Red",
                Arrays.asList(ProductOption.STOCK));
        assertThat(allVariants).hasSize(4);
    }

    @Test
    public void testGetAllVariantsWithStockBySizeVariant() {
        prepareP1000();
        final List<ProductData> allVariants = facade.getAllSellableVariantsWithOptions("P1000_Red_L",
                Arrays.asList(ProductOption.STOCK));
        assertThat(allVariants).hasSize(4);
    }

    @Test
    public void testGetAllSellableVariantsWithStoreStockAndOptions() {
        prepareP1000();
        final List<ProductData> allVariants = facade.getAllSellableVariantsWithStoreStockAndOptions("P1000_Red_L",
                Integer.valueOf(7001),
                Arrays.asList(ProductOption.STOCK));
        assertThat(allVariants).hasSize(4);
        assertThat(allVariants.get(0).getStock().getStockLevelStatus()).isEqualTo(StockLevelStatus.OUTOFSTOCK);
        assertThat(allVariants.get(1).getStock().getStockLevelStatus()).isEqualTo(StockLevelStatus.LOWSTOCK);
        assertThat(allVariants.get(2).getStock().getStockLevelStatus()).isEqualTo(StockLevelStatus.INSTOCK);
        assertThat(allVariants.get(3).getStock()).isNull();
    }

    @Test
    public void testGetAllSellableVariantsWithStoreStockAndOptionsEmpty() {
        prepareP1000();
        final Map<String, StockLevelStatus> stockMap = new HashMap<>();
        given(targetStoreStockService.getStoreStockLevelStatusMapForProducts(Integer.valueOf(7001),
                Arrays.asList("P1000_Red_L", "P1000_Red_M", "P1000_Blue_L", "P1000_Blue_M")))
                        .willReturn(stockMap);
        final List<ProductData> allVariants = facade.getAllSellableVariantsWithStoreStockAndOptions("P1000_Red_L",
                Integer.valueOf(7001),
                Arrays.asList(ProductOption.STOCK));
        assertThat(allVariants).isNull();
    }

    @Test
    public void testGetFluentStock() throws FluentClientException {
        willReturn("P01").given(facade).getBaseProductCode(product);
        final List<ProductData> variants = mock(List.class);
        willReturn(variants).given(facade).getAllSellableVariantsWithOptions("P01", null);
        final Map<String, FluentStock> stockDataMap = mock(Map.class);
        given(fluentStockLookupService.lookupStock("P01", "L01")).willReturn(stockDataMap);
        willDoNothing().given(facade).variantStock(variants, stockDataMap, "L01");

        assertThat(facade.getFluentStock(product, "L01")).isEqualTo(variants);
        verify(facade).variantStock(variants, stockDataMap, "L01");
    }

    @Test
    public void testGetFluentStockFail() throws FluentClientException {
        willReturn("P01").given(facade).getBaseProductCode(product);
        final List<ProductData> variants = mock(List.class);
        willReturn(variants).given(facade).getAllSellableVariantsWithOptions("P01", null);
        given(fluentStockLookupService.lookupStock("P01", "L01")).willThrow(new FluentClientException(new Error()));

        assertThat(facade.getFluentStock(product, "L01")).isEmpty();
    }

    @Test
    public void testGetFluentStockUsingCode() {
        willReturn(product).given(facade).getProductForCode("P01");
        final List<ProductData> variants = mock(List.class);
        willReturn(variants).given(facade).getFluentStock(product, "L01");

        facade.getFluentStock("P01", "L01");
        verify(facade).getProductForCode("P01");
        verify(facade).getFluentStock(product, "L01");
    }

    @Test
    public void testGetBaseProductCode() {
        final TargetProductModel product = mock(TargetProductModel.class);
        given(product.getCode()).willReturn("P01");

        assertThat(facade.getBaseProductCode(product)).isEqualTo("P01");
    }

    @Test
    public void testGetBaseProductCodeForNull() {
        assertThat(facade.getBaseProductCode(null)).isNull();
    }

    @Test
    public void testGetBaseProductCodeForColourVariant() {
        final TargetProductModel product = mock(TargetProductModel.class);
        given(product.getCode()).willReturn("P01");
        final TargetColourVariantProductModel colourVariant = mock(TargetColourVariantProductModel.class);
        given(colourVariant.getBaseProduct()).willReturn(product);

        assertThat(facade.getBaseProductCode(colourVariant)).isEqualTo("P01");
    }

    @Test
    public void testGetBaseProductCodeForColourVariantWithNullBaseProduct() {
        final TargetColourVariantProductModel colourVariant = mock(TargetColourVariantProductModel.class);

        assertThat(facade.getBaseProductCode(colourVariant)).isNull();
    }

    @Test
    public void testGetBaseProductCodeForSizeVariant() {
        final TargetProductModel product = mock(TargetProductModel.class);
        given(product.getCode()).willReturn("P01");
        final TargetColourVariantProductModel colourVariant = mock(TargetColourVariantProductModel.class);
        given(colourVariant.getBaseProduct()).willReturn(product);
        final TargetSizeVariantProductModel sizeVariant = mock(TargetSizeVariantProductModel.class);
        given(sizeVariant.getBaseProduct()).willReturn(colourVariant);

        assertThat(facade.getBaseProductCode(sizeVariant)).isEqualTo("P01");
    }

    @Test
    public void testVariantStock() {
        final PurchaseOptionModel purchaseOption = mock(PurchaseOptionModel.class);
        given(purchaseOption.getCode()).willReturn("buynow");
        given(targetPurchaseOptionHelper.getCurrentPurchaseOptionModel()).willReturn(purchaseOption);
        final List<ProductData> variants = new ArrayList<>();
        final TargetProductData variant = mock(TargetProductData.class);
        given(variant.getCode()).willReturn("V01");
        variants.add(variant);
        final Map<String, FluentStock> stockDataMap = new HashMap<>();
        final FluentStock fluentStock = mock(FluentStock.class);
        final StockData stockData = mock(StockData.class);
        given(stockData.getStockLevelStatus()).willReturn(StockLevelStatus.INSTOCK);
        given(fluentStock.getAts()).willReturn(stockData);
        stockDataMap.put("V01", fluentStock);

        facade.variantStock(variants, stockDataMap, null);
        verify(variant).setStock(fluentStock.getAts());
        verify(variant).setConsolidatedStoreStock(fluentStock.getConsolidated());
        verify(variant).setInStock(true);
        final ArgumentCaptor<Map> availableCaptor = ArgumentCaptor.forClass(Map.class);
        verify(variant).setAvailable(availableCaptor.capture());
        assertThat(availableCaptor.getValue()).isNotEmpty().hasSize(1);
        assertThat((Boolean)availableCaptor.getValue().get("buynow")).isTrue();
    }

    @Test
    public void testVariantStockWithStoreNumber() {
        final PurchaseOptionModel purchaseOption = mock(PurchaseOptionModel.class);
        given(purchaseOption.getCode()).willReturn("buynow");
        given(targetPurchaseOptionHelper.getCurrentPurchaseOptionModel()).willReturn(purchaseOption);
        final List<ProductData> variants = new ArrayList<>();
        final TargetProductData variant = mock(TargetProductData.class);
        given(variant.getCode()).willReturn("V01");
        variants.add(variant);
        final Map<String, FluentStock> stockDataMap = new HashMap<>();
        final FluentStock fluentStock = mock(FluentStock.class);
        final StockData stockData = mock(StockData.class);
        given(stockData.getStockLevelStatus()).willReturn(StockLevelStatus.INSTOCK);
        given(fluentStock.getStore()).willReturn(stockData);
        stockDataMap.put("V01", fluentStock);

        facade.variantStock(variants, stockDataMap, "L01");
        verify(variant).setStock(fluentStock.getStore());
        verify(variant).setConsolidatedStoreStock(fluentStock.getConsolidated());
        verify(variant).setInStock(true);
        final ArgumentCaptor<Map> availableCaptor = ArgumentCaptor.forClass(Map.class);
        verify(variant).setAvailable(availableCaptor.capture());
        assertThat(availableCaptor.getValue()).isNotEmpty().hasSize(1);
        assertThat((Boolean)availableCaptor.getValue().get("buynow")).isTrue();
    }

    @Test
    public void testVariantStockWithEmptyStockDataMap() {
        final PurchaseOptionModel purchaseOption = mock(PurchaseOptionModel.class);
        given(purchaseOption.getCode()).willReturn("buynow");
        given(targetPurchaseOptionHelper.getCurrentPurchaseOptionModel()).willReturn(purchaseOption);
        final List<ProductData> variants = new ArrayList<>();
        final TargetProductData variant = mock(TargetProductData.class);
        given(variant.getCode()).willReturn("V01");
        variants.add(variant);
        final Map<String, FluentStock> stockDataMap = Collections.emptyMap();

        facade.variantStock(variants, stockDataMap, null);
        final ArgumentCaptor<StockData> stockDataCaptor = ArgumentCaptor.forClass(StockData.class);
        verify(variant).setStock(stockDataCaptor.capture());
        verify(variant).setConsolidatedStoreStock(stockDataCaptor.capture());
        assertThat(stockDataCaptor.getAllValues()).hasSize(2);
        assertThat(stockDataCaptor.getAllValues()).onProperty("stockLevelStatus")
                .containsExactly(StockLevelStatus.OUTOFSTOCK, StockLevelStatus.OUTOFSTOCK);
        assertThat(stockDataCaptor.getAllValues()).onProperty("stockLevel").containsExactly(Long.valueOf(0),
                Long.valueOf(0));
        final ArgumentCaptor<Map> availableCaptor = ArgumentCaptor.forClass(Map.class);
        verify(variant).setAvailable(availableCaptor.capture());
        assertThat(availableCaptor.getValue()).isNotEmpty().hasSize(1);
        assertThat((Boolean)availableCaptor.getValue().get("buynow")).isTrue();
    }

    @Test
    public void testVariantStockSkipDisplayOnly() {
        final PurchaseOptionModel purchaseOption = mock(PurchaseOptionModel.class);
        given(purchaseOption.getCode()).willReturn("buynow");
        given(targetPurchaseOptionHelper.getCurrentPurchaseOptionModel()).willReturn(purchaseOption);
        final List<ProductData> variants = new ArrayList<>();
        final TargetProductData variant = mock(TargetProductData.class);
        willReturn(Boolean.TRUE).given(variant).isDisplayOnly();
        variants.add(variant);
        final Map<String, FluentStock> stockDataMap = Collections.emptyMap();

        facade.variantStock(variants, stockDataMap, null);
        verify(variant).isDisplayOnly();
        verifyNoMoreInteractions(variant);
    }

    public void prepareP1000() {
        final ProductModel p1000 = mock(TargetProductModel.class);
        final AbstractTargetVariantProductModel p1000Red = mock(TargetColourVariantProductModel.class);
        final AbstractTargetVariantProductModel p1000RedL = mock(TargetSizeVariantProductModel.class);
        final AbstractTargetVariantProductModel p1000RedM = mock(TargetSizeVariantProductModel.class);
        final AbstractTargetVariantProductModel p1000Blue = mock(TargetColourVariantProductModel.class);
        final AbstractTargetVariantProductModel p1000BlueL = mock(TargetSizeVariantProductModel.class);
        final AbstractTargetVariantProductModel p1000BlueM = mock(TargetSizeVariantProductModel.class);
        doReturn(p1000).when(facade).getBaseTargetProduct(p1000Red);
        doReturn(p1000).when(facade).getBaseTargetProduct(p1000RedL);
        doReturn(p1000).when(facade).getBaseTargetProduct(p1000RedM);
        doReturn(p1000).when(facade).getBaseTargetProduct(p1000Blue);
        doReturn(p1000).when(facade).getBaseTargetProduct(p1000BlueL);
        doReturn(p1000).when(facade).getBaseTargetProduct(p1000BlueM);
        given(productService.getProductForCode("P1000")).willReturn(p1000);
        given(productService.getProductForCode("P1000_Red")).willReturn(p1000Red);
        given(productService.getProductForCode("P1000_Red_L")).willReturn(p1000RedL);
        given(productService.getProductForCode("P1000_Red_M")).willReturn(p1000RedM);
        given(productService.getProductForCode("P1000_Blue")).willReturn(p1000Blue);
        given(productService.getProductForCode("P1000_Red_M")).willReturn(p1000BlueL);
        given(productService.getProductForCode("P1000_Blue_M")).willReturn(p1000BlueM);
        given(p1000.getVariants())
                .willReturn(Arrays.asList((VariantProductModel)p1000Red, (VariantProductModel)p1000Blue));
        given(p1000Red.getVariants())
                .willReturn(Arrays.asList((VariantProductModel)p1000RedL, (VariantProductModel)p1000RedM));
        given(p1000Blue.getVariants())
                .willReturn(Arrays.asList((VariantProductModel)p1000BlueL, (VariantProductModel)p1000BlueM));
        final ProductData p1000RedLData = new TargetProductData();
        final ProductData p1000RedMData = new TargetProductData();
        final ProductData p1000BlueLData = new TargetProductData();
        final ProductData p1000BlueMData = new TargetProductData();
        p1000RedLData.setCode("P1000_Red_L");
        p1000RedMData.setCode("P1000_Red_M");
        p1000BlueLData.setCode("P1000_Blue_L");
        p1000BlueMData.setCode("P1000_Blue_M");
        given(p1000RedL.getCode()).willReturn("P1000_Red_L");
        given(p1000RedM.getCode()).willReturn("P1000_Red_M");
        given(p1000BlueL.getCode()).willReturn("P1000_Blue_L");
        given(p1000BlueM.getCode()).willReturn("P1000_Blue_M");

        doReturn(p1000RedLData).when(facade).getProductForCodeAndOptions("P1000_Red_L",
                Arrays.asList(ProductOption.STOCK));
        doReturn(p1000RedMData).when(facade).getProductForCodeAndOptions("P1000_Red_M",
                Arrays.asList(ProductOption.STOCK));
        doReturn(p1000BlueLData).when(facade).getProductForCodeAndOptions("P1000_Blue_L",
                Arrays.asList(ProductOption.STOCK));
        doReturn(p1000BlueMData).when(facade).getProductForCodeAndOptions("P1000_Blue_M",
                Arrays.asList(ProductOption.STOCK));

        final Map<String, StockLevelStatus> stockMap = new HashMap<>();
        stockMap.put("P1000_Red_L", StockLevelStatus.OUTOFSTOCK);
        stockMap.put("P1000_Red_M", StockLevelStatus.LOWSTOCK);
        stockMap.put("P1000_Blue_L", StockLevelStatus.INSTOCK);
        given(targetStoreStockService.getStoreStockLevelStatusMapForProducts(Integer.valueOf(7001),
                Arrays.asList("P1000_Red_L", "P1000_Red_M", "P1000_Blue_L", "P1000_Blue_M"))).willReturn(stockMap);
    }


}
