package au.com.target.tgtfacades.product.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtfacades.product.data.BrandData;


@UnitTest
public class TargetBrandPopulatorTest {

    @InjectMocks
    private final TargetBrandPopulator populator = new TargetBrandPopulator();

    private BrandModel brandModel = new BrandModel();

    private final BrandData brandData = new BrandData();

    @Test(expected = IllegalArgumentException.class)
    public void testPopulateNullSource() {
        brandModel = null;
        populator.populate(brandModel, brandData);
    }

    @Test
    public void testPopulate() {
        final String code = "code";
        final String name = "name";

        brandModel.setCode(code);
        brandModel.setName(name);

        populator.populate(brandModel, brandData);

        Assert.assertEquals(code, brandData.getCode());
        Assert.assertEquals(name, brandData.getName());
    }

}
