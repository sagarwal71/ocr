package au.com.target.tgtfacades.process.email.context;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.urlencoder.UrlEncoderService;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.CustomerEmailResolutionService;
import de.hybris.platform.commons.model.DocumentModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.taxinvoice.TaxInvoiceService;
import au.com.target.tgtfacades.order.data.OrderModificationData;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderShippingNoticeEmailContextTest {


    @Mock
    private OrderProcessModel businessProcessModel;

    @Mock
    private OrderModel orderModel;

    @Mock
    private BaseSiteModel siteModel;

    @Mock
    private TargetCustomerModel customerModel;

    @Mock
    private EmailPageModel emailPageModel;

    @Mock
    private ConsignmentModel consignmentModel;

    @Mock
    private DocumentModel taxInvoiceMedia;

    @Mock
    private Converter<ConsignmentModel, ConsignmentData> consignmentConverter;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private UrlEncoderService urlEncoderService;

    @Mock
    private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

    @Mock
    private CustomerEmailResolutionService customerEmailResolutionService;

    @Mock
    private Converter<UserModel, CustomerData> customerConverter;

    @Mock
    private Converter<OrderModel, TargetOrderData> orderConverter;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Mock
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Mock
    private TargetPointOfServiceData selectedStore;

    @Mock
    private TaxInvoiceService taxInvoiceService;

    private ConsignmentData consignment1, consignment2, consignment3, consignment4, consignment5, currentConsignment;

    private TargetOrderData order;

    private ProductData product1, product2, product3, product4;

    private OrderEntryData orderEntry1, orderEntry2, orderEntry3, orderEntry4, orderEntry5, orderEntry6;
    private final List<OrderModificationData> cancelledProducts = new ArrayList<>();

    private OrderShippingNoticeEmailContext contextSetUp() {
        final OrderShippingNoticeEmailContext context = new OrderShippingNoticeEmailContext();
        context.setConsignmentConverter(consignmentConverter);
        context.setCustomerConverter(customerConverter);
        context.setOrderConverter(orderConverter);
        context.setOrderProcessParameterHelper(orderProcessParameterHelper);
        context.setSelectedStore(selectedStore);
        context.setTargetStoreLocatorFacade(targetStoreLocatorFacade);
        context.setTargetDeliveryModeHelper(targetDeliveryModeHelper);
        context.setCustomerEmailResolutionService(customerEmailResolutionService);
        context.setOrderProcessParameterHelper(orderProcessParameterHelper);
        context.setSiteBaseUrlResolutionService(siteBaseUrlResolutionService);
        given(urlEncoderService.getUrlEncodingPatternForEmail(businessProcessModel)).willReturn("");
        context.setUrlEncoderService(urlEncoderService);
        given(businessProcessModel.getOrder()).willReturn(orderModel);
        given(businessProcessModel.getOrder().getSite()).willReturn(siteModel);
        given(context.getSite(businessProcessModel)).willReturn(siteModel);

        given(orderModel.getUser()).willReturn(customerModel);
        final CustomerData customerData = new CustomerData();
        given(customerConverter.convert(customerModel)).willReturn(customerData);
        given(taxInvoiceService.getTaxInvoiceForOrder(orderModel)).willReturn(taxInvoiceMedia);
        given(context.getOrderProcessParameterHelper().getConsignment(businessProcessModel))
                .willReturn(consignmentModel);

        createOrderData();
        given(consignmentConverter.convert(consignmentModel)).willReturn(currentConsignment);
        given(orderConverter.convert((OrderModel)any())).willReturn(order);

        return context;
    }

    private void createOrderData() {

        order = new TargetOrderData();
        orderEntry1 = new OrderEntryData();
        product1 = new ProductData();
        product1.setCode("p1");
        orderEntry1.setEntryNumber(1);
        orderEntry1.setProduct(product1);
        orderEntry1.setQuantity(2L);

        consignment1 = new ConsignmentData();
        consignment1.setCode("code1");
        final Calendar cal1 = Calendar.getInstance();
        cal1.set(Calendar.HOUR_OF_DAY, 10);
        consignment1.setStatusDate(cal1.getTime());
        final List<ConsignmentEntryData> consignment1Entries = new ArrayList<>();
        final ConsignmentEntryData consignment1Entry = new ConsignmentEntryData();
        consignment1Entry.setShippedQuantity(2L);
        consignment1Entry.setOrderEntry(orderEntry1);
        consignment1Entries.add(consignment1Entry);
        consignment1.setEntries(consignment1Entries);
        consignment1.setIsDigitalDelivery(true);
        consignment1.setStatus(ConsignmentStatus.SHIPPED);

        orderEntry2 = new OrderEntryData();
        product2 = new ProductData();
        product2.setCode("p2");
        orderEntry2.setEntryNumber(2);
        orderEntry2.setProduct(product2);
        orderEntry2.setQuantity(2L);

        consignment2 = new ConsignmentData();
        consignment2.setCode("code2");
        final Calendar cal2 = Calendar.getInstance();
        cal2.set(Calendar.HOUR_OF_DAY, 11);
        consignment2.setStatusDate(cal2.getTime());

        consignment2.setStatus(ConsignmentStatus.PICKED);
        final ConsignmentEntryData consignment2Entry = new ConsignmentEntryData();
        consignment2Entry.setOrderEntry(orderEntry2);
        final List<ConsignmentEntryData> consignment2Entries = new ArrayList<>();
        consignment2Entries.add(consignment2Entry);
        consignment2.setEntries(consignment2Entries);

        orderEntry3 = new OrderEntryData();
        product3 = new ProductData();
        product3.setCode("p3");
        orderEntry3.setEntryNumber(3);
        orderEntry3.setProduct(product3);
        orderEntry3.setQuantity(2L);
        consignment3 = new ConsignmentData();
        consignment3.setCode("code3");
        final Calendar cal3 = Calendar.getInstance();
        cal3.set(Calendar.HOUR_OF_DAY, 12);
        consignment3.setStatusDate(cal3.getTime());
        consignment3.setStatus(ConsignmentStatus.SHIPPED);

        final ConsignmentEntryData consignment3Entry = new ConsignmentEntryData();
        consignment3Entry.setOrderEntry(orderEntry3);
        consignment3Entry.setShippedQuantity(2L);
        final List<ConsignmentEntryData> consignment3Entries = new ArrayList<>();
        consignment3Entries.add(consignment3Entry);
        consignment3.setEntries(consignment3Entries);


        orderEntry4 = new OrderEntryData();
        product4 = new ProductData();
        product4.setCode("p4");
        orderEntry4.setEntryNumber(4);
        orderEntry4.setProduct(product4);
        orderEntry4.setQuantity(2L);

        consignment4 = new ConsignmentData();
        consignment4.setCode("code4");
        final Calendar cal4 = Calendar.getInstance();
        cal4.set(Calendar.HOUR_OF_DAY, 13);
        consignment4.setStatusDate(cal4.getTime());
        consignment4.setStatus(ConsignmentStatus.SENT_TO_WAREHOUSE);

        final ConsignmentEntryData consignment4Entry = new ConsignmentEntryData();
        consignment4Entry.setOrderEntry(orderEntry4);
        final List<ConsignmentEntryData> consignment4Entries = new ArrayList<>();
        consignment4Entries.add(consignment4Entry);
        consignment4.setEntries(consignment4Entries);


        orderEntry5 = new OrderEntryData();
        final ProductData product5 = new ProductData();
        product5.setCode("p5");
        orderEntry5.setEntryNumber(5);
        orderEntry5.setProduct(product5);
        orderEntry5.setQuantity(2L);

        consignment5 = new ConsignmentData();
        consignment5.setCode("code5");
        final Calendar cal5 = Calendar.getInstance();
        cal5.set(Calendar.HOUR_OF_DAY, 14);
        consignment5.setStatusDate(cal4.getTime());
        consignment5.setStatus(ConsignmentStatus.CREATED);

        final ConsignmentEntryData consignment5Entry = new ConsignmentEntryData();
        consignment5Entry.setOrderEntry(orderEntry5);
        consignment5Entry.setShippedQuantity(2L);
        final List<ConsignmentEntryData> consignment5Entries = new ArrayList<>();
        consignment5Entries.add(consignment5Entry);
        consignment5.setEntries(consignment5Entries);

        orderEntry6 = new OrderEntryData();
        final ProductData product6 = new ProductData();
        product6.setCode("p6");
        orderEntry6.setEntryNumber(6);
        orderEntry6.setProduct(product6);
        orderEntry6.setQuantity(1L);

        final List<OrderEntryData> orderEntries = new ArrayList<>();
        orderEntries.add(orderEntry1);
        orderEntries.add(orderEntry2);
        orderEntries.add(orderEntry3);
        orderEntries.add(orderEntry4);
        orderEntries.add(orderEntry5);
        orderEntries.add(orderEntry6);
        order.setEntries(orderEntries);


        final OrderModificationData orderModificationData1 = new OrderModificationData();
        orderModificationData1.setProduct(product5);
        orderModificationData1.setQuantity(2L);
        cancelledProducts.add(orderModificationData1);
        final OrderModificationData orderModificationData2 = new OrderModificationData();
        orderModificationData2.setProduct(product6);
        orderModificationData2.setQuantity(1L);
        cancelledProducts.add(orderModificationData2);

        order.setCancelledProducts(cancelledProducts);

        currentConsignment = consignment5;
        given(consignmentConverter.convert(consignmentModel)).willReturn(currentConsignment);
        final List<ConsignmentData> consignments = new ArrayList<>();
        consignments.add(consignment2);
        consignments.add(consignment1);
        consignments.add(consignment4);
        consignments.add(consignment3);
        consignments.add(consignment5);
        order.setConsignment(consignments);
    }


    @Test
    public void testOrderShippingNoticeEmailContextStatus() {

        final OrderShippingNoticeEmailContext context = contextSetUp();
        context.init(businessProcessModel, emailPageModel);
        final List<ConsignmentData> expectedConsignmentList = Arrays.asList(consignment1, consignment2, consignment3,
                consignment4, consignment5);
        assertThat(context.getOrder()).isNotNull();
        assertThat(context.getOrder().getConsignment()).isEqualTo(expectedConsignmentList);
        final List<OrderEntryData> expectedInProgressEntries = Arrays.asList(orderEntry2, orderEntry4, orderEntry6);
        final List<ConsignmentData> expectedShippedConsignmentList = Collections.singletonList(consignment3);
        final List<ConsignmentData> expectedDigitallySentConsignmentList = Collections.singletonList(consignment1);
        assertThat(context.getShippedConsignments()).isEqualTo(expectedShippedConsignmentList);
        assertThat(context.getSentDigitalConsignments()).isEqualTo(expectedDigitallySentConsignmentList);
        assertThat(context.getInProgressEntries()).isEqualTo(expectedInProgressEntries);
        assertThat(context.getOrder().getCancelledProducts()).isEqualTo(cancelledProducts);
        assertThat(context.getOrder().getConsignment()).isEqualTo(order.getConsignment());
    }

}
