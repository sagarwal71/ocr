package au.com.target.tgtfacades.device.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.device.data.DeviceData;
import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.commerceservices.enums.UiExperienceLevel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtfacades.constants.TgtFacadesConstants;


@UnitTest
public class TargetDeviceDetectionFacadeTest {

    @Mock
    private Converter<HttpServletRequest, DeviceData> requestDeviceDataConverter;

    @Mock
    private SessionService sessionService;
    @Mock
    private UiExperienceService uiExperienceService;

    //CHECKSTYLE:OFF mockito cannot access private members
    @InjectMocks
    TargetDeviceDetectionFacade targetDeviceDetectionFacade = new TargetDeviceDetectionFacade();

    //CHECKSTYLE:ON

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        targetDeviceDetectionFacade.setRequestDeviceDataConverter(requestDeviceDataConverter);
    }

    @Test
    public void testInitializeRequestWithRequestParaMeterNullAndCurrentDetectedDeviceHavingNullValue() {
        final HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);
        given(mockRequest.getParameter("clear")).willReturn(null);
        given(targetDeviceDetectionFacade.getCurrentDetectedDevice()).willReturn(null);
        final DeviceData deviceData = Mockito.mock(DeviceData.class);
        given(requestDeviceDataConverter.convert(mockRequest)).willReturn(deviceData);
        targetDeviceDetectionFacade.initializeRequest(mockRequest);
        verify(uiExperienceService).setDetectedUiExperienceLevel(UiExperienceLevel.DESKTOP);
        verify(sessionService).setAttribute(TargetDeviceDetectionFacade.DETECTED_DEVICE, deviceData);


    }

    @Test
    public void testInitializeRequestWithRequestParaMeterIsTrueAndCurrentDetectedDeviceHavingNullValue() {
        final HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);
        given(mockRequest.getParameter("clear")).willReturn("true");
        given(targetDeviceDetectionFacade.getCurrentDetectedDevice()).willReturn(null);
        final DeviceData deviceData = Mockito.mock(DeviceData.class);
        given(requestDeviceDataConverter.convert(mockRequest)).willReturn(deviceData);
        targetDeviceDetectionFacade.initializeRequest(mockRequest);
        verify(uiExperienceService).setDetectedUiExperienceLevel(UiExperienceLevel.DESKTOP);
        verify(sessionService).setAttribute(TargetDeviceDetectionFacade.DETECTED_DEVICE, deviceData);

    }


    @Test
    public void testInitializeRequestWithRequestParaMeterNullAndCurrentDetectedDeviceHavingValue() {

        final HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);
        given(mockRequest.getParameter("clear")).willReturn("false");
        final DeviceData deviceData = Mockito.mock(DeviceData.class);
        given(targetDeviceDetectionFacade.getCurrentDetectedDevice()).willReturn(deviceData);
        targetDeviceDetectionFacade.initializeRequest(mockRequest);
        verifyZeroInteractions(uiExperienceService);
        verify(sessionService, Mockito.times(1)).getAttribute(TargetDeviceDetectionFacade.DETECTED_DEVICE);
        verifyNoMoreInteractions(sessionService);
    }

    @Test
    public void testDetectIosAppWithIOSWrongHeaders() {
        final HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);
        final HttpSession mockSession = Mockito.mock(HttpSession.class);
        given(mockRequest.getHeader("x-target")).willReturn("mobile-app");
        given(mockRequest.getHeader("x-app-platform")).willReturn("andrioad");
        given(mockRequest.getSession()).willReturn(mockSession);
        targetDeviceDetectionFacade.detectIosApp(mockRequest);
        verifyNoMoreInteractions(mockSession);
    }

    @Test
    public void testDetectIosAppWithRequestHeaders() {
        final HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);
        final HttpSession mockSession = Mockito.mock(HttpSession.class);
        given(mockRequest.getSession()).willReturn(mockSession);
        given(mockRequest.getHeader("x-target")).willReturn("mobile-app");
        given(mockRequest.getHeader("x-app-platform")).willReturn("iOS");
        targetDeviceDetectionFacade.detectIosApp(mockRequest);
        verify(mockSession).setAttribute(TgtFacadesConstants.IOS_APP_WV, Boolean.TRUE);
    }

}
