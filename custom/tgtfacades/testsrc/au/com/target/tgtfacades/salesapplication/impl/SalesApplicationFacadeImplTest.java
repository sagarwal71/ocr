/**
 * 
 */
package au.com.target.tgtfacades.salesapplication.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.servicelayer.session.MockSessionService;
import de.hybris.platform.servicelayer.session.SessionService;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.salesapplication.impl.TargetSalesApplicationServiceImpl;
import org.junit.Assert;


/**
 * Unit tests for SalesApplicationFacade
 * 
 * @author jjayawa1
 * 
 */
@UnitTest
public class SalesApplicationFacadeImplTest {

    protected static final String SELECTED_SALES_APPLICATION = "Selected-Sales-Application";

    private final SalesApplicationFacadeImpl salesApplicationFacade = new SalesApplicationFacadeImpl();
    private final TargetSalesApplicationServiceImpl targetSalesApplicationService = new TargetSalesApplicationServiceImpl();


    @Before
    public void prepare() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testInitializeWithExpectedChannelKiosk() {
        final String channel = "kiosk";
        final HttpServletRequest request = BDDMockito.mock(HttpServletRequest.class);
        BDDMockito.doReturn(channel).when(request).getHeader("x-target");
        final SessionService sessionService = new MockSessionService();
        targetSalesApplicationService.setSessionService(sessionService);
        salesApplicationFacade.setTargetSalesApplicationService(targetSalesApplicationService);
        salesApplicationFacade.initializeRequest(request);
        final SalesApplication detectedChannel = targetSalesApplicationService.getCurrentSalesApplication();
        Assert.assertEquals(SalesApplication.KIOSK, detectedChannel);
    }

    @Test
    public void testInitializeWithUnExpectedChannel() {
        final String channel = "nonKiosk";
        final HttpServletRequest request = BDDMockito.mock(HttpServletRequest.class);
        BDDMockito.doReturn(channel).when(request).getHeader("x-target");
        final SessionService sessionService = new MockSessionService();
        targetSalesApplicationService.setSessionService(sessionService);
        salesApplicationFacade.setSessionService(sessionService);
        salesApplicationFacade.setTargetSalesApplicationService(targetSalesApplicationService);
        salesApplicationFacade.initializeRequest(request);

        final SalesApplication detectedChannel = targetSalesApplicationService.getCurrentSalesApplication();
        Assert.assertEquals(SalesApplication.WEB, detectedChannel);
    }

    @Test
    public void testGetCurrentSalesApplicationKiosk() {
        final SalesApplication channel = SalesApplication.KIOSK;
        final SessionService sessionService = new MockSessionService();
        sessionService.setAttribute(SELECTED_SALES_APPLICATION, SalesApplication.KIOSK);
        targetSalesApplicationService.setSessionService(sessionService);
        salesApplicationFacade.setTargetSalesApplicationService(targetSalesApplicationService);
        Assert.assertEquals(channel, salesApplicationFacade.getCurrentSalesApplication());
    }

    @Test
    public void testInitializeWithExpectedChannelMobileApp() {
        final String channel = "mobile-app";
        final HttpServletRequest request = BDDMockito.mock(HttpServletRequest.class);
        BDDMockito.doReturn(channel).when(request).getHeader("x-target");
        final SessionService sessionService = new MockSessionService();
        targetSalesApplicationService.setSessionService(sessionService);
        salesApplicationFacade.setSessionService(sessionService);
        salesApplicationFacade.setTargetSalesApplicationService(targetSalesApplicationService);
        salesApplicationFacade.initializeRequest(request);

        final SalesApplication detectedChannel = targetSalesApplicationService.getCurrentSalesApplication();
        Assert.assertEquals(SalesApplication.MOBILEAPP, detectedChannel);
    }

    @Test
    public void testGetCurrentSalesApplicationMobileApp() {
        final SalesApplication channel = SalesApplication.MOBILEAPP;
        final SessionService sessionService = new MockSessionService();
        sessionService.setAttribute(SELECTED_SALES_APPLICATION, SalesApplication.MOBILEAPP);
        targetSalesApplicationService.setSessionService(sessionService);
        salesApplicationFacade.setTargetSalesApplicationService(targetSalesApplicationService);
        Assert.assertEquals(channel, salesApplicationFacade.getCurrentSalesApplication());
    }

    @Test
    public void testIsKioskApplication() {
        final SessionService sessionService = new MockSessionService();
        sessionService.setAttribute(SELECTED_SALES_APPLICATION, SalesApplication.KIOSK);
        targetSalesApplicationService.setSessionService(sessionService);
        salesApplicationFacade.setTargetSalesApplicationService(targetSalesApplicationService);
        Assert.assertTrue(salesApplicationFacade.isKioskApplication());
    }

    @Test
    public void testIsKioskApplicationWhenWEB() {
        final SessionService sessionService = new MockSessionService();
        sessionService.setAttribute(SELECTED_SALES_APPLICATION, SalesApplication.WEB);
        targetSalesApplicationService.setSessionService(sessionService);
        salesApplicationFacade.setTargetSalesApplicationService(targetSalesApplicationService);
        Assert.assertFalse(salesApplicationFacade.isKioskApplication());
    }

}
