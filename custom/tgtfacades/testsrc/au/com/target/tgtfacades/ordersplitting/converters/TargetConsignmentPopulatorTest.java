/**
 * 
 */
package au.com.target.tgtfacades.ordersplitting.converters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;


/**
 * @author rmcalave
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetConsignmentPopulatorTest {
    private static final String TEST_TRACKING_URL = "http://testUrl/";

    @InjectMocks
    private final TargetConsignmentPopulator consignmentConverter = new TargetConsignmentPopulator();

    @Mock
    private EnumerationService mockEnumerationService;

    @Mock
    private TargetStoreConsignmentService mockTargetStoreConsignmentService;

    @Mock
    private Converter<ConsignmentEntryModel, ConsignmentEntryData> mockConsignmentEntryConverter;

    @Mock
    private TargetCarrierModel carrier;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Before
    public void setUp() {
        consignmentConverter.setConsignmentEntryConverter(mockConsignmentEntryConverter);
        given(carrier.getTrackingUrl()).willReturn(TEST_TRACKING_URL);
    }

    @Test
    public void testPopulate() {
        final ConsignmentStatus consignmentStatus = ConsignmentStatus.SHIPPED;
        final String consignmentCode = "23568974";
        final String consignmentTrackingId = "784512784512";

        final ConsignmentEntryModel mockConsignmentEntryModel = mock(ConsignmentEntryModel.class);

        final ConsignmentEntryData consignmentEntry = new ConsignmentEntryData();
        given(mockConsignmentEntryConverter.convert(mockConsignmentEntryModel)).willReturn(consignmentEntry);

        given(mockEnumerationService.getEnumerationName(consignmentStatus)).willReturn(consignmentStatus.getCode());

        final OrderModel mockOrderModel = Mockito.mock(OrderModel.class);
        given(mockOrderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);

        final TargetConsignmentModel mockConsignmentModel = mock(TargetConsignmentModel.class);
        given(mockConsignmentModel.getConsignmentEntries())
                .willReturn(Collections.singleton(mockConsignmentEntryModel));
        given(mockConsignmentModel.getStatus()).willReturn(consignmentStatus);
        given(mockConsignmentModel.getCode()).willReturn(consignmentCode);
        given(mockConsignmentModel.getTrackingID()).willReturn(consignmentTrackingId);
        given(mockConsignmentModel.getOrder()).willReturn(mockOrderModel);
        given(mockConsignmentModel.getTargetCarrier()).willReturn(carrier);

        final ConsignmentData result = new ConsignmentData();
        consignmentConverter.populate(mockConsignmentModel, result);

        assertEquals(consignmentCode, result.getCode());
        assertEquals(consignmentStatus, result.getStatus());
        assertEquals(consignmentStatus.getCode(), result.getStatusDisplay());

        assertEquals(1, result.getEntries().size());
        assertEquals(consignmentEntry, result.getEntries().get(0));

        assertEquals(TEST_TRACKING_URL + consignmentTrackingId, result.getTrackingID());
    }

    @Test
    public void testPopulateWithNoEntries() {
        final TargetConsignmentModel mockConsignmentModel = mock(TargetConsignmentModel.class);

        final ConsignmentData mockResult = mock(ConsignmentData.class);
        consignmentConverter.populate(mockConsignmentModel, mockResult);

        verifyZeroInteractions(mockResult);
    }

    @Test
    public void testPopulateWithNoStatus() {
        final String consignmentCode = "23568974";
        final String consignmentTrackingId = "784512784512";

        final ConsignmentEntryModel mockConsignmentEntryModel = mock(ConsignmentEntryModel.class);

        final ConsignmentEntryData consignmentEntry = new ConsignmentEntryData();
        given(mockConsignmentEntryConverter.convert(mockConsignmentEntryModel)).willReturn(consignmentEntry);

        final OrderModel mockOrderModel = Mockito.mock(OrderModel.class);
        given(mockOrderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);

        final TargetConsignmentModel mockConsignmentModel = mock(TargetConsignmentModel.class);
        given(mockConsignmentModel.getConsignmentEntries())
                .willReturn(Collections.singleton(mockConsignmentEntryModel));
        given(mockConsignmentModel.getCode()).willReturn(consignmentCode);
        given(mockConsignmentModel.getTrackingID()).willReturn(consignmentTrackingId);
        given(mockConsignmentModel.getOrder()).willReturn(mockOrderModel);
        given(mockConsignmentModel.getTargetCarrier()).willReturn(carrier);

        final ConsignmentData result = new ConsignmentData();
        consignmentConverter.populate(mockConsignmentModel, result);

        assertEquals(consignmentCode, result.getCode());
        assertNull(result.getStatus());
        assertNull(result.getStatusDisplay());

        assertEquals(1, result.getEntries().size());
        assertEquals(consignmentEntry, result.getEntries().get(0));

        assertEquals(TEST_TRACKING_URL + consignmentTrackingId, result.getTrackingID());

        verifyZeroInteractions(mockEnumerationService);
    }

    @Test
    public void testPopulateCreatedWithNoTrackingId() {
        final ConsignmentStatus consignmentStatus = ConsignmentStatus.CREATED;
        final String consignmentCode = "23568974";

        final ConsignmentEntryModel mockConsignmentEntryModel = mock(ConsignmentEntryModel.class);

        final ConsignmentEntryData consignmentEntry = new ConsignmentEntryData();
        given(mockConsignmentEntryConverter.convert(mockConsignmentEntryModel)).willReturn(consignmentEntry);

        given(mockEnumerationService.getEnumerationName(consignmentStatus)).willReturn(consignmentStatus.getCode());

        final TargetConsignmentModel mockConsignmentModel = mock(TargetConsignmentModel.class);
        given(mockConsignmentModel.getConsignmentEntries())
                .willReturn(Collections.singleton(mockConsignmentEntryModel));
        given(mockConsignmentModel.getStatus()).willReturn(consignmentStatus);
        given(mockConsignmentModel.getCode()).willReturn(consignmentCode);
        given(mockConsignmentModel.getTargetCarrier()).willReturn(carrier);

        final ConsignmentData result = new ConsignmentData();
        consignmentConverter.populate(mockConsignmentModel, result);

        assertEquals(consignmentCode, result.getCode());
        assertEquals(consignmentStatus, result.getStatus());
        assertEquals(consignmentStatus.getCode(), result.getStatusDisplay());

        assertEquals(1, result.getEntries().size());
        assertEquals(consignmentEntry, result.getEntries().get(0));

        assertNull(result.getTrackingID());
    }

    @Test
    public void testPopulateShippedWithNoTrackingIdLayby() {
        final ConsignmentStatus consignmentStatus = ConsignmentStatus.SHIPPED;
        final String consignmentCode = "23568974";

        final ConsignmentEntryModel mockConsignmentEntryModel = mock(ConsignmentEntryModel.class);

        final ConsignmentEntryData consignmentEntry = new ConsignmentEntryData();
        given(mockConsignmentEntryConverter.convert(mockConsignmentEntryModel)).willReturn(consignmentEntry);

        given(mockEnumerationService.getEnumerationName(consignmentStatus)).willReturn(consignmentStatus.getCode());

        final TargetConsignmentModel mockConsignmentModel = mock(TargetConsignmentModel.class);
        given(mockConsignmentModel.getConsignmentEntries())
                .willReturn(Collections.singleton(mockConsignmentEntryModel));
        given(mockConsignmentModel.getStatus()).willReturn(consignmentStatus);
        given(mockConsignmentModel.getCode()).willReturn(consignmentCode);
        given(mockConsignmentModel.getTargetCarrier()).willReturn(carrier);

        given(Boolean.valueOf(mockTargetStoreConsignmentService.isConsignmentAssignedToAnyStore(mockConsignmentModel)))
                .willReturn(Boolean.FALSE);

        final ConsignmentData result = new ConsignmentData();
        consignmentConverter.populate(mockConsignmentModel, result);

        assertEquals(consignmentCode, result.getCode());
        assertEquals(null, result.getStatus());
        assertEquals(null, result.getStatusDisplay());

        assertEquals(1, result.getEntries().size());
        assertEquals(consignmentEntry, result.getEntries().get(0));

        assertNull(result.getTrackingID());
    }

    @Test
    public void testPopulateShippedWithNoTrackingIdInstore() {
        final ConsignmentStatus consignmentStatus = ConsignmentStatus.SHIPPED;
        final String consignmentCode = "23568974";

        final ConsignmentEntryModel mockConsignmentEntryModel = mock(ConsignmentEntryModel.class);

        final ConsignmentEntryData consignmentEntry = new ConsignmentEntryData();
        given(mockConsignmentEntryConverter.convert(mockConsignmentEntryModel)).willReturn(consignmentEntry);

        given(mockEnumerationService.getEnumerationName(consignmentStatus)).willReturn(consignmentStatus.getCode());

        final TargetConsignmentModel mockConsignmentModel = mock(TargetConsignmentModel.class);
        given(mockConsignmentModel.getConsignmentEntries())
                .willReturn(Collections.singleton(mockConsignmentEntryModel));
        given(mockConsignmentModel.getStatus()).willReturn(consignmentStatus);
        given(mockConsignmentModel.getCode()).willReturn(consignmentCode);
        given(mockConsignmentModel.getTargetCarrier()).willReturn(carrier);

        given(Boolean.valueOf(mockTargetStoreConsignmentService.isConsignmentAssignedToAnyStore(mockConsignmentModel)))
                .willReturn(Boolean.TRUE);

        final ConsignmentData result = new ConsignmentData();
        consignmentConverter.populate(mockConsignmentModel, result);

        assertEquals(consignmentCode, result.getCode());
        assertEquals(consignmentStatus, result.getStatus());
        assertEquals(consignmentStatus.getCode(), result.getStatusDisplay());

        assertEquals(1, result.getEntries().size());
        assertEquals(consignmentEntry, result.getEntries().get(0));

        assertNull(result.getTrackingID());
    }

    @Test
    public void testPopulateWithCancelledOrderStatus() {
        final ConsignmentStatus consignmentStatus = ConsignmentStatus.SHIPPED;
        final String consignmentCode = "23568974";
        final String consignmentTrackingId = "784512784512";

        final ConsignmentEntryModel mockConsignmentEntryModel = mock(ConsignmentEntryModel.class);

        final ConsignmentEntryData consignmentEntry = new ConsignmentEntryData();
        given(mockConsignmentEntryConverter.convert(mockConsignmentEntryModel)).willReturn(consignmentEntry);

        given(mockEnumerationService.getEnumerationName(consignmentStatus)).willReturn(consignmentStatus.getCode());

        final OrderModel mockOrderModel = Mockito.mock(OrderModel.class);
        given(mockOrderModel.getStatus()).willReturn(OrderStatus.CANCELLED);

        final TargetConsignmentModel mockConsignmentModel = mock(TargetConsignmentModel.class);
        given(mockConsignmentModel.getConsignmentEntries())
                .willReturn(Collections.singleton(mockConsignmentEntryModel));
        given(mockConsignmentModel.getStatus()).willReturn(consignmentStatus);
        given(mockConsignmentModel.getCode()).willReturn(consignmentCode);
        given(mockConsignmentModel.getTrackingID()).willReturn(consignmentTrackingId);
        given(mockConsignmentModel.getOrder()).willReturn(mockOrderModel);

        final ConsignmentData result = new ConsignmentData();
        consignmentConverter.populate(mockConsignmentModel, result);

        assertEquals(consignmentCode, result.getCode());
        assertEquals(consignmentStatus, result.getStatus());
        assertEquals(consignmentStatus.getCode(), result.getStatusDisplay());

        assertEquals(1, result.getEntries().size());
        assertEquals(consignmentEntry, result.getEntries().get(0));

        assertNull(result.getTrackingID());
    }

    @Test
    public void testPopulateWithCancelledConsignmentStatus() {
        final ConsignmentStatus consignmentStatus = ConsignmentStatus.CANCELLED;
        final String consignmentCode = "23568974";
        final String consignmentTrackingId = "784512784512";

        final ConsignmentEntryModel mockConsignmentEntryModel = mock(ConsignmentEntryModel.class);

        final ConsignmentEntryData consignmentEntry = new ConsignmentEntryData();
        given(mockConsignmentEntryConverter.convert(mockConsignmentEntryModel)).willReturn(consignmentEntry);

        given(mockEnumerationService.getEnumerationName(consignmentStatus)).willReturn(consignmentStatus.getCode());

        final OrderModel mockOrderModel = Mockito.mock(OrderModel.class);
        given(mockOrderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);

        final TargetConsignmentModel mockConsignmentModel = mock(TargetConsignmentModel.class);
        given(mockConsignmentModel.getConsignmentEntries())
                .willReturn(Collections.singleton(mockConsignmentEntryModel));
        given(mockConsignmentModel.getStatus()).willReturn(consignmentStatus);
        given(mockConsignmentModel.getCode()).willReturn(consignmentCode);
        given(mockConsignmentModel.getTrackingID()).willReturn(consignmentTrackingId);
        given(mockConsignmentModel.getOrder()).willReturn(mockOrderModel);

        final ConsignmentData result = new ConsignmentData();
        consignmentConverter.populate(mockConsignmentModel, result);

        assertEquals(consignmentCode, result.getCode());
        assertEquals(consignmentStatus, result.getStatus());
        assertEquals(consignmentStatus.getCode(), result.getStatusDisplay());

        assertEquals(1, result.getEntries().size());
        assertEquals(consignmentEntry, result.getEntries().get(0));

        assertNull(result.getTrackingID());
    }
}
