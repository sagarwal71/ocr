/**
 * 
 */
package au.com.target.tgtfacades.config.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetSharedConfigModel;


/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetSharedConfigFacadeImplTest {

    @Mock
    private TargetSharedConfigService targetSharedConfigService;

    @Mock
    private TargetSharedConfigModel configModel;

    @InjectMocks
    private final TargetSharedConfigFacadeImpl facade = new TargetSharedConfigFacadeImpl();

    @Test
    public void testGetConfigByCodeNull() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final String configCode = null;
        final String configValue = facade.getConfigByCode(configCode);
        verify(targetSharedConfigService).getConfigByCode(configCode);
        assertThat(configValue).isNull();
    }

    @Test
    public void testGetConfigByCode() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final String configCode = "urgencytoPurchase.excludeCategory";
        when(targetSharedConfigService.getConfigByCode(configCode)).thenReturn("Clearance");
        final String value = facade.getConfigByCode(configCode);
        verify(targetSharedConfigService).getConfigByCode(configCode);
        assertThat(value).isNotNull();
        assertThat(value).isEqualTo("Clearance");
    }

    @Test
    public void testGetInt() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        doReturn(Integer.valueOf(2)).when(targetSharedConfigService).getInt("code", 0);
        assertThat(facade.getInt("code", 0)).isEqualTo(2);
        verify(targetSharedConfigService).getInt("code", 0);
    }
}
