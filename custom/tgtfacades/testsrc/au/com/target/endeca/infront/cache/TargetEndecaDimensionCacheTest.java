/**
 * 
 */
package au.com.target.endeca.infront.cache;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.Callable;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.endeca.navigation.ENEQueryException;

import au.com.target.endeca.infront.data.EndecaDimensions;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.service.EndecaDimensionService;
import org.junit.Assert;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetEndecaDimensionCacheTest {

    @Mock
    private EndecaDimensionService endecaDimensionService;

    @InjectMocks
    private final TargetEndecaDimensionCacheTestable targetEndecaDimensionCacheTestable = new TargetEndecaDimensionCacheTestable();

    public class TargetEndecaDimensionCacheTestable extends TargetEndecaDimensionCache {
        public void setCacheForTestPurpose(final EndecaDimensions endecaDimension) {
            cache.put("EndecaDimensionCache", endecaDimension);
        }

        public void invalidateCacheForTestPurpose() {
            cache.remove("EndecaDimensionCache");
        }

        public EndecaDimensions getCacheDetailForTestPurpose() {
            return cache.get("EndecaDimensionCache");
        }
    }

    @Test
    public void testGetDimensionCacheWhenValid() throws ENEQueryException, TargetEndecaException {
        final EndecaDimensions endecaDimension = mock(EndecaDimensions.class);
        targetEndecaDimensionCacheTestable.setCacheForTestPurpose(endecaDimension);
        Assertions.assertThat(targetEndecaDimensionCacheTestable.getDimensionCache()).isEqualTo((endecaDimension));
    }

    @Test
    public void testGetDimensionCacheWhenInValid() throws ENEQueryException, TargetEndecaException {
        final EndecaDimensions endecaDimension = mock(EndecaDimensions.class);
        targetEndecaDimensionCacheTestable.setCacheForTestPurpose(endecaDimension);
        targetEndecaDimensionCacheTestable.invalidateCacheForTestPurpose();
        final EndecaDimensions endecaDimensionsRefreshed = mock(EndecaDimensions.class);
        given(endecaDimensionService.getDimensions()).willReturn(endecaDimensionsRefreshed);
        Assertions.assertThat(targetEndecaDimensionCacheTestable.getDimensionCache())
                .isEqualTo(endecaDimensionsRefreshed);
    }

    @Test
    public void testRefreshCache() throws ENEQueryException, TargetEndecaException {
        final EndecaDimensions endecaDimension = mock(EndecaDimensions.class);
        targetEndecaDimensionCacheTestable.setCacheForTestPurpose(endecaDimension);
        final EndecaDimensions endecaDimensionsRefreshed = mock(EndecaDimensions.class);
        given(endecaDimensionService.getDimensions()).willReturn(endecaDimensionsRefreshed);
        targetEndecaDimensionCacheTestable.refreshCache();
        Assertions.assertThat(targetEndecaDimensionCacheTestable.getCacheDetailForTestPurpose())
                .isEqualTo(endecaDimensionsRefreshed);
    }

    @Test
    public void testMultipleThreadsTryingToGetInvalidatedCacheOnlyInitializeOnce()
            throws ENEQueryException, TargetEndecaException, InterruptedException, ExecutionException {
        final EndecaDimensions endecaDimension = mock(EndecaDimensions.class);
        targetEndecaDimensionCacheTestable.setCacheForTestPurpose(endecaDimension);
        targetEndecaDimensionCacheTestable.invalidateCacheForTestPurpose();

        // barrier to ensure all threads start at exactly the same time
        final int noOfThreads = 10;
        final CyclicBarrier barrier = new CyclicBarrier(noOfThreads);
        final ExecutorService executor = Executors.newFixedThreadPool(noOfThreads);
        final Callable<EndecaDimensions> testRunnable = new Callable<EndecaDimensions>() {
            @Override
            public EndecaDimensions call() {
                try {
                    barrier.await();
                    return targetEndecaDimensionCacheTestable.getDimensionCache();
                }
                catch (final ENEQueryException | TargetEndecaException | InterruptedException
                        | BrokenBarrierException e) {
                    Assert.fail("Exception Testing Dimension Cache" + e);
                    return null;
                }
            }
        };
        final EndecaDimensions endecaDimensionsRefreshed = mock(EndecaDimensions.class);
        given(endecaDimensionService.getDimensions()).willReturn(endecaDimensionsRefreshed);

        final List<Future<EndecaDimensions>> futures = new ArrayList<>(10);
        for (int i = 0; i < noOfThreads; i++) {
            futures.add(executor.submit(testRunnable));
        }

        for (int i = 0; i < noOfThreads; i++) {
            // make sure all threads are done
            futures.get(i).get();
        }
        verify(endecaDimensionService, times(1)).getDimensions();
    }

}
