/**
 * 
 */
package au.com.target.endeca.infront.querybuilder;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.web.util.UriUtils;

import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.UrlENEQuery;

import au.com.target.endeca.infront.cache.EndecaDimensionCacheService;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * @author mjanarth
 *
 */
public class EndecaProductQueryBuilderTest {
    /**
     * 
     */
    private static final String ENCODING_UTF_8 = "UTF-8";
    private static final int THIRTY_SIX = 36;
    private static final int FIFTY = 50;
    @Mock
    private ConfigurationService configurationService;
    @Mock
    private Configuration configuration;
    @Mock
    private EndecaDimensionCacheService endecaDimensionCacheService;
    @Mock
    private TargetProductModel targetProductModel;
    @Mock
    private TargetProductCategoryModel originalCategoryModel;
    @InjectMocks
    private final EndecaProductQueryBuilder productQueryBuilder = new EndecaProductQueryBuilder();

    private final String fieldList = "productCode|Colorvariantcode";

    @SuppressWarnings("boxing")
    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
        Mockito.when(configurationService.getConfiguration()).thenReturn(configuration);
        Mockito.when(
                configurationService.getConfiguration().getInt("storefront.recentlyviewedproducts.component.size",
                        FIFTY))
                .thenReturn(FIFTY);
        Mockito.when(
                configurationService.getConfiguration().getString(EndecaConstants.FieldList.COMPONENT_FIELDLIST))
                .thenReturn(fieldList);
    }

    @Test
    public void testBuildProductQueryFeaturedComponent() throws UnsupportedEncodingException {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P1000");
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSearchField("productCode");
        searchStateData.setNpSearchState(EndecaConstants.ENDECA_NP_2);
        searchStateData.setFieldListConfig(EndecaConstants.FieldList.COMPONENT_FIELDLIST);
        final UrlENEQuery query = productQueryBuilder.buildProductQuery(searchStateData, null, THIRTY_SIX);
        final String decodedQuery = UriUtils.decode(query.toString(), ENCODING_UTF_8);

        assertThat(query.getNavERecsPerAggrERec()).isEqualTo(2);
        assertThat(decodedQuery.contains("inStockFlag|1")).isTrue();
        assertThat(decodedQuery.contains("onlineDate|1")).isTrue();
        assertThat(decodedQuery.contains("Nu=productCode")).isTrue();
        assertThat(decodedQuery.contains("colourVariantCode")).isFalse();
        assertThat(query.getNavNumAggrERecs()).isEqualTo(THIRTY_SIX);
        assertThat(query.getNavMerchRuleFilter()).isEqualTo("endeca.internal.nonexistent");
    }

    /**
     * Method for decode URL.
     * 
     * @param query
     *            that needs to be decoded
     * @return decoded url
     * @throws UnsupportedEncodingException
     */
    private String decodeUrl(final String query) throws UnsupportedEncodingException {
        final String decodedQuery = URLDecoder.decode(query, ENCODING_UTF_8);
        return decodedQuery;
    }

    @Test
    public void testBuildProductQueryisRecentlyViewedComponent() throws UnsupportedEncodingException {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P1000");
        productCodes.add("P1001");
        productCodes.add("P1002");
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSkipDefaultSort(true);
        searchStateData.setSearchField("colourVariantCode");
        searchStateData.setFieldListConfig(EndecaConstants.FieldList.COMPONENT_FIELDLIST);
        final UrlENEQuery query = productQueryBuilder.buildProductQuery(searchStateData, null, FIFTY);
        final String decodedQuery = decodeUrl(query.toString());
        assertThat(query.getNavERecsPerAggrERec()).isEqualTo(1);
        assertThat(decodedQuery.contains("|inStockFlag|1||onlineDate|1")).isFalse();
        assertThat(decodedQuery.contains("Nu=productCode")).isTrue();
        assertThat(query.getNavNumAggrERecs()).isEqualTo(FIFTY);
        assertThat(decodedQuery.contains("colourVariantCode")).isTrue();
        assertThat(query.getNavMerchRuleFilter()).isEqualTo("endeca.internal.nonexistent");

    }

    @Test
    public void testBuildProductQueryFeaturedComponentWithEmptyProducts() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> productCodes = new ArrayList<>();
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSearchField("productCode");
        searchStateData.setFieldListConfig(EndecaConstants.FieldList.COMPONENT_FIELDLIST);
        final UrlENEQuery query = productQueryBuilder.buildProductQuery(searchStateData, null, FIFTY);
        assertThat(query).isNull();

    }

    @Test
    public void testBuildProductQueryFeaturedComponentWithCategories() throws UnsupportedEncodingException {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> categoryCodes = new ArrayList<>();
        categoryCodes.add("W12345");
        categoryCodes.add("W7896");
        searchStateData.setNpSearchState(EndecaConstants.ENDECA_NP_2);

        searchStateData.setCategoryCodes(categoryCodes);
        searchStateData.setFieldListConfig(EndecaConstants.FieldList.COMPONENT_FIELDLIST);
        final UrlENEQuery query = productQueryBuilder.buildProductQuery(searchStateData, null, THIRTY_SIX);
        final String decodedQuery = decodeUrl(query.toString());
        assertThat(query.getNavERecsPerAggrERec()).isEqualTo(2);
        assertThat(decodedQuery.contains("inStockFlag|1")).isTrue();
        assertThat(decodedQuery.contains("onlineDate|1")).isTrue();
        assertThat(decodedQuery.contains("Nu=productCode")).isTrue();
        assertThat(query.getNavRecordFilter().toString().contains("W12345")).isTrue();
        assertThat(query.getNavRecordFilter().toString().contains("W7896")).isTrue();
        assertThat(query.getNavNumAggrERecs()).isEqualTo(THIRTY_SIX);
        assertThat(query.getNavMerchRuleFilter()).isEqualTo("endeca.internal.nonexistent");

    }

    @Test
    public void testBuildProductQueryFeaturedComponentWithProductModels() throws UnsupportedEncodingException {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<ProductModel> productModels = new ArrayList<>();
        final ProductModel mockProductA = Mockito.mock(ProductModel.class);
        final ProductModel mockProductB = Mockito.mock(ProductModel.class);
        productModels.add(mockProductA);
        productModels.add(mockProductB);
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P1000");
        productCodes.add("P1001");
        productCodes.add("P1002");
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSearchField("productCode");
        searchStateData.setSearchField("colourVariantCode");
        searchStateData.setFieldListConfig(EndecaConstants.FieldList.COMPONENT_FIELDLIST);
        final UrlENEQuery query = productQueryBuilder
                .buildProductQuery(searchStateData, productModels, THIRTY_SIX);
        final String decodedQuery = decodeUrl(query.toString());
        assertThat(decodedQuery.contains("inStockFlag|1")).isTrue();
        assertThat(decodedQuery.contains("onlineDate|1")).isTrue();
        assertThat(decodedQuery.contains("Nu=productCode")).isTrue();
        assertThat(query.getNavNumAggrERecs()).isEqualTo(38);

    }

    @Test
    public void testBuildProductQueryWithSkipStockFilter() throws UnsupportedEncodingException {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P1000");
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSearchField("productCode");
        searchStateData.setSkipInStockFilter(true);
        searchStateData.setNpSearchState(EndecaConstants.ENDECA_NP_2);
        searchStateData.setFieldListConfig(EndecaConstants.FieldList.COMPONENT_FIELDLIST);
        final UrlENEQuery query = productQueryBuilder.buildProductQuery(searchStateData, null, THIRTY_SIX);
        final String decodedQuery = decodeUrl(query.toString());
        assertThat(query.getNavERecsPerAggrERec()).isEqualTo(2);
        assertThat(decodedQuery.contains("inStockFlag|1")).isTrue();
        assertThat(decodedQuery.contains("onlineDate|1")).isTrue();
        assertThat(decodedQuery.contains("Nu=productCode")).isTrue();
        assertThat(decodedQuery.contains("colourVariantCode")).isFalse();
        assertThat(query.getNavNumAggrERecs()).isEqualTo(THIRTY_SIX);
        assertThat(query.getNavMerchRuleFilter()).isEqualTo("endeca.internal.nonexistent");
    }

    @Test
    public void testBuildQueryWtihNoRollupField() throws UnsupportedEncodingException {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("CP1000");
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSearchField("colourVariantCode");
        searchStateData.setSkipInStockFilter(true);
        searchStateData.setNpSearchState(EndecaConstants.ENDECA_N_0);
        searchStateData.setFieldListConfig(EndecaConstants.FieldList.STORESTOCK_FIELDLIST);
        searchStateData.setNuRollupField(null);
        final UrlENEQuery query = productQueryBuilder.buildProductQuery(searchStateData, null, THIRTY_SIX);
        final String decodedQuery = decodeUrl(query.toString());

        assertThat(query.getNavERecsPerAggrERec()).isEqualTo(0);
        assertThat(decodedQuery.contains("inStockFlag|1")).isTrue();
        assertThat(decodedQuery.contains("onlineDate|1")).isTrue();
        assertThat(decodedQuery.contains("colourVariantCode=\"CP1000\"")).isTrue();
        assertThat(decodedQuery.contains("Nu=")).isFalse();
        assertThat(query.getNavNumAggrERecs()).isEqualTo(THIRTY_SIX);
        assertThat(query.getNavMerchRuleFilter()).isEqualTo("endeca.internal.nonexistent");

    }

    @Test
    public void testBuildYouMayAlsoLikeQueryWithSizeGroup()
            throws ENEQueryException, TargetEndecaException, UnsupportedEncodingException {

        final boolean isSizeGroup = true;
        final EndecaSearchStateData searchStateData = getEndecaSearchData(isSizeGroup);

        given(originalCategoryModel.getCode()).willReturn("W1000");
        given(targetProductModel.getOriginalCategory()).willReturn(originalCategoryModel);
        given(endecaDimensionCacheService.getDimension(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY,
                targetProductModel.getOriginalCategory().getCode())).willReturn("12345");

        final UrlENEQuery query = productQueryBuilder.buildYouMayAlsoLikeQuery(searchStateData, targetProductModel);
        final String decodedQuery = decodeUrl(query.toString());

        assertThat(query.getNavERecsPerAggrERec()).isEqualTo(2);
        assertThat(decodedQuery.contains("onlineDate|1")).isTrue();
        assertThat(decodedQuery.contains("Nu=productCode")).isTrue();
        assertThat(decodedQuery.contains("colourVariantCode")).isFalse();
        assertThat(query.getNavNumAggrERecs()).isEqualTo(12);
        assertThat(query.getNavActiveSortKeys()).isNotEmpty();
        assertThat(query.getNavMerchRuleFilter()).isEqualTo("endeca.internal.nonexistent");
    }

    @Test
    public void testBuildYouMayAlsoLikeQueryWithoutSizeGroup()
            throws ENEQueryException, TargetEndecaException, UnsupportedEncodingException {

        final boolean isSizeGroup = false;
        final EndecaSearchStateData searchStateData = getEndecaSearchData(isSizeGroup);

        given(originalCategoryModel.getCode()).willReturn("W1000");
        given(targetProductModel.getOriginalCategory()).willReturn(originalCategoryModel);
        given(endecaDimensionCacheService.getDimension(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY,
                targetProductModel.getOriginalCategory().getCode())).willReturn("12345");

        final UrlENEQuery query = productQueryBuilder.buildYouMayAlsoLikeQuery(searchStateData, targetProductModel);
        final String decodedQuery = decodeUrl(query.toString());

        assertThat(query.getNavERecsPerAggrERec()).isEqualTo(2);
        assertThat(decodedQuery.contains("onlineDate|1")).isTrue();
        assertThat(decodedQuery.contains("Nu=productCode")).isTrue();
        assertThat(decodedQuery.contains("colourVariantCode")).isFalse();
        assertThat(query.getNavNumAggrERecs()).isEqualTo(12);
        assertThat(query.getNavActiveSortKeys()).hasSize(1);
        assertThat(query.getNavMerchRuleFilter()).isEqualTo("endeca.internal.nonexistent");
    }


    /**
     * @param isSizeGroup
     * @return searchStateData
     */
    private EndecaSearchStateData getEndecaSearchData(final boolean isSizeGroup) {

        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> categoryCodes = new ArrayList<>();
        categoryCodes.add("W1000");
        searchStateData.setCategoryCodes(categoryCodes);
        searchStateData.setNpSearchState(EndecaConstants.ENDECA_NP_2);
        searchStateData.setFieldListConfig(EndecaConstants.FieldList.COMPONENT_FIELDLIST);
        searchStateData.setItemsPerPage(12);
        if (isSizeGroup) {
            searchStateData.setSizeGroup("babywear");
        }
        return searchStateData;
    }


}
