/**
 * 
 */
package au.com.target.endeca.infront.service;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.endeca.navigation.ENEQueryResults;

import au.com.target.endeca.infront.data.EndecaDimensions;


/**
 * @author smudumba
 * 
 */
@UnitTest
public class EndecaDimensionServiceImplTest {

    @Mock
    private ENEQueryResults results;

    @Test
    public void testGetDimensions() throws Exception {
        final EndecaDimensions eds = new EndecaDimensions();
        final Map<String, Map<String, String>> dims = new HashMap<String, Map<String, String>>();
        final Map<String, String> refinement = new HashMap<String, String>();
        refinement.put("AFL", "12345");
        dims.put("brand", refinement);
        eds.setDimensions(dims);
        final String excpectedValue = "12345";
        final EndecaDimensionServiceImpl mockGetDimensions = Mockito.mock(EndecaDimensionServiceImpl.class);
        BDDMockito.given(mockGetDimensions.getDimensions()).willReturn(eds);
        final EndecaDimensions result = mockGetDimensions.getDimensions();
        final String value = result.getDimensions().get("brand").get("AFL");
        Assert.assertEquals(value, excpectedValue);
        Assert.assertEquals(result, eds);
    }


    @Test
    public void testGetDimensionsNull() throws Exception {
        final EndecaDimensions eds = new EndecaDimensions();
        eds.setDimensions(null);
        final EndecaDimensionServiceImpl mockGetDimensions = Mockito.mock(EndecaDimensionServiceImpl.class);
        BDDMockito.given(mockGetDimensions.getDimensions()).willReturn(eds);
        final EndecaDimensions result = mockGetDimensions.getDimensions();
        Assert.assertEquals(result, eds);
    }
}
