package au.com.target.endeca.infront.converter.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.endeca.infront.assembler.BasicContentItem;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.model.NavigationAction;
import com.endeca.infront.cartridge.model.Record;
import com.endeca.infront.cartridge.model.RecordAction;
import com.endeca.infront.cartridge.model.SortOptionLabel;
import com.endeca.infront.cartridge.model.StringAttribute;

import au.com.target.endeca.infront.data.EndecaProduct;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.tgtutility.util.TargetDateUtil;



@UnitTest
public class EndecaContentItemResultsListProcessorTest {


    private final EndecaContentItemResultsListProcessor processor = new EndecaContentItemResultsListProcessor();
    private final EndecaSearch search = new EndecaSearch();
    private final BasicContentItem contentItem = new BasicContentItem();
    private final String navigationState = "?N=26uy&Nr=productDisplayFlag";
    private final String recordState = "recordState";



    @Test
    public void testProcessContentItemNull() {
        contentItem.put("totalNumRecs", null);
        processor.process(contentItem, search);
        assertThat(search.getRecords().getTotalCount()).isNull();
        assertThat(search.getRecords().getRecordsPerPage()).isNull();
        assertThat(search.getRecords().getCurrentNavState()).isNull();
        assertThat(search.getRecords().getOption()).isNull();
    }

    @Test
    public void testProcessContentItemWithoutSortOptions() {
        contentItem.put("totalNumRecs", "2");
        contentItem.put("recsPerPage", "10");
        processor.process(contentItem, search);
        assertThat(search.getRecords().getTotalCount()).isEqualTo("2");
        assertThat(search.getRecords().getRecordsPerPage()).isEqualTo("10");
        assertThat(search.getRecords().getOption()).isEqualTo(null);

    }

    @Test
    public void testProcessContentItemWithNavState() {
        final NavigationAction action = new NavigationAction();
        action.setNavigationState(navigationState);
        contentItem.put("pagingActionTemplate", action);
        processor.process(contentItem, search);
        assertThat(search.getRecords().getTotalCount()).isEqualTo(null);
        assertThat(search.getRecords().getRecordsPerPage()).isEqualTo(null);
        assertThat(search.getRecords().getCurrentNavState()).isEqualTo(navigationState);
    }

    @Test
    public void testProcessContentItemWithSortOptions() {
        contentItem.put("totalNumRecs", "2");
        contentItem.put("recsPerPage", "10");
        populateSortOption(contentItem);
        processor.process(contentItem, search);
        assertThat(search.getRecords().getTotalCount()).isEqualTo("2");
        assertThat(search.getRecords().getRecordsPerPage()).isEqualTo("10");
        assertThat(search.getRecords().getOption().size()).isEqualTo(1);
        assertThat(search.getRecords().getOption().get(0).getName()).isEqualTo("Price");
        assertThat(search.getRecords().getOption().get(0).getNavigationState()).isEqualTo(navigationState);
    }

    @Test
    public void testProcessContentItemWithRecords() {
        contentItem.put("recsPerPage", "10");
        populateRecordList(contentItem, false);
        processor.process(contentItem, search);
        assertThat(search.getRecords().getProduct().size()).isEqualTo(1);
        final EndecaProduct product = search.getRecords().getProduct().get(0);
        assertThat(product);
        assertThat(product.getCode()).isEqualTo("CP2012");
        assertThat(product.getMaxAvailStoreQty()).isEqualTo("50");
        assertThat(product.getNewLowerPriceFlag()).isTrue();
        assertThat(product.getMaxAvailOnlineQty()).isNull();
    }

    @Test
    public void testProcessContentItemWithChildRecords() {
        contentItem.put("recsPerPage", "10");
        populateRecordList(contentItem, true);
        processor.process(contentItem, search);
        assertThat(search.getRecords().getProduct().size()).isEqualTo(1);
        final EndecaProduct product = search.getRecords().getProduct().get(0);
        assertThat(product.getProduct().size()).isEqualTo(1);
        final EndecaProduct childRecord = product.getProduct().get(0);
        assertThat(product).isNotNull();
        assertThat(childRecord).isNotNull();
        assertThat(product.getCode()).isEqualTo("CP2012");
        assertThat(product.getMaxAvailStoreQty()).isEqualTo("50");
        assertThat(childRecord.getColour()).isEqualTo("red");
        assertThat(product.getNewLowerPriceFlag()).isTrue();
        assertThat(product.getMaxAvailOnlineQty()).isNull();
        assertThat(childRecord.getProductDisplayType()).isEqualTo("comingSoon");
        assertThat(childRecord.getNormalSaleStartDateTime())
                .isEqualTo(TargetDateUtil.getStringAsDate("2018-07-31 00:00:00.0", "yyyy-MM-dd HH:mm:ss.S"));
    }

    private void populateRecordList(final ContentItem content, final boolean includeChildAttr) {
        final List<Record> recordList = new ArrayList<>();
        final Record record = new Record();
        final RecordAction action = new RecordAction();
        action.setRecordState(recordState);
        final Map attributes = new HashMap<String, StringAttribute>();
        final StringAttribute attribute = new StringAttribute();
        attribute.add("50");
        final StringAttribute prdCodeAttr = new StringAttribute();
        prdCodeAttr.add("CP2012");
        final StringAttribute newLowerPriceFlag = new StringAttribute();
        newLowerPriceFlag.add("1");
        attributes.put("max_availQtyInStore", attribute);
        attributes.put("productCode", prdCodeAttr);
        attributes.put("newLowerPriceFlag", newLowerPriceFlag);

        if (includeChildAttr) {
            final Record childRecord = new Record();
            final List<Record> childList = new ArrayList<>();
            final Map childAttributes = new HashMap<String, StringAttribute>();
            final StringAttribute color = new StringAttribute();
            color.add("red");
            childAttributes.put("colour", color);
            childAttributes.put("productDisplayType", "comingSoon");
            childAttributes.put("normalSaleStartDateTime", "2018-07-31 00:00:00.0");
            childRecord.setAttributes(childAttributes);
            childRecord.setDetailsAction(action);
            childList.add(childRecord);
            record.setRecords(childList);
        }
        record.setAttributes(attributes);
        record.setDetailsAction(action);
        record.setNumRecords(5);
        recordList.add(record);
        content.put("records", recordList);
    }



    private void populateSortOption(final ContentItem content) {
        final List<SortOptionLabel> sortList = new ArrayList<>();
        final SortOptionLabel label = new SortOptionLabel();
        label.setLabel("Price");
        label.setSelected(false);
        label.setNavigationState(navigationState);
        sortList.add(label);
        content.put("sortOptions", sortList);

    }
}
