/**
 * 
 */
package au.com.target.endeca.infront.querybuilder;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.data.EndecaSearchStateData;


/**
 * @author Paul
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EndecaQueryParamBuilderTest {

    @Test
    public void testConstructQueryParamsWithNoValues() {
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();

        final String result = EndecaQueryParamBuilder.constructQueryParams(endecaSearchStateData);

        assertThat(result).isEmpty();
    }

    /**
     * Test method for
     * {@link au.com.target.endeca.infront.querybuilder.EndecaQueryParamBuilder#constructQueryParams(EndecaSearchStateData)}
     * .
     */
    @Test
    public void testConstructQueryParamsNavState() {

        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        final List<String> navList = new ArrayList<>();
        navList.add("10223");
        navList.add("11234");

        endecaSearchStateData.setNavigationState(navList);

        final String result = EndecaQueryParamBuilder.constructQueryParams(endecaSearchStateData);
        assertThat(result).isEqualTo("N=10223+11234");

    }

    /**
     * Test method for
     * {@link au.com.target.endeca.infront.querybuilder.EndecaQueryParamBuilder#constructQueryParams(EndecaSearchStateData)}
     * .
     */
    @Test
    public void testConstructQueryRecordsPerPage() {

        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setItemsPerPage(30);

        final String result = EndecaQueryParamBuilder.constructQueryParams(endecaSearchStateData);
        assertThat(result).isEqualTo("Nrpp=30");

    }

    /**
     * Test method for
     * {@link au.com.target.endeca.infront.querybuilder.EndecaQueryParamBuilder#constructQueryParams(EndecaSearchStateData)}
     * .
     */
    @Test
    public void testConstructQueryRecordOffset() {

        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();

        endecaSearchStateData.setItemsPerPage(30);
        endecaSearchStateData.setPageNumber(3);

        final String result = EndecaQueryParamBuilder.constructQueryParams(endecaSearchStateData);
        assertThat(result).isEqualTo("Nrpp=30&No=90");

    }

    /**
     * Test method for
     * {@link au.com.target.endeca.infront.querybuilder.EndecaQueryParamBuilder#constructQueryParams(EndecaSearchStateData)}
     * .
     */
    @Test
    public void testConstructQuerySearchTerm() {

        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();

        final List<String> searchTermList = new ArrayList<>();
        searchTermList.add("red");
        searchTermList.add("dress");

        endecaSearchStateData.setSearchTerm(searchTermList);
        endecaSearchStateData.setPageNumber(3);

        final String result = EndecaQueryParamBuilder.constructQueryParams(endecaSearchStateData);
        assertThat(result).isEqualTo("Ntt=red+dress");

    }

    /**
     * Test method for
     * {@link au.com.target.endeca.infront.querybuilder.EndecaQueryParamBuilder#constructQueryParams(EndecaSearchStateData)}
     * .
     */
    @Test
    public void testConstructQuerySortTerm() {

        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();

        final Map<String, Integer> sortMap = new LinkedHashMap<String, Integer>();
        sortMap.put("productCode", Integer.valueOf(0));
        sortMap.put("productPrice", Integer.valueOf(1));
        sortMap.put("brand", Integer.valueOf(0));

        endecaSearchStateData.setSortStateMap(sortMap);

        final String result = EndecaQueryParamBuilder.constructQueryParams(endecaSearchStateData);
        assertThat(result).isEqualTo("Ns=productCode||productPrice|1||brand");

    }

    /**
     * Test method for
     * {@link au.com.target.endeca.infront.querybuilder.EndecaQueryParamBuilder#constructQueryParams(EndecaSearchStateData)}
     * .
     */
    @Test
    public void testConstructQueryViewAs() {
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();

        endecaSearchStateData.setViewAs("grid");
        final String result = EndecaQueryParamBuilder.constructQueryParams(endecaSearchStateData);
        assertThat(result).isEqualTo("viewAs=grid");
    }

    @Test
    public void testConstructRecordFiltersParamWithOneFilter() {
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();

        final Map<String, String> recordFilters = new HashMap<>();
        recordFilters.put("productDisplayFlag", "1");

        endecaSearchStateData.setRecordFilters(recordFilters);

        final String result = EndecaQueryParamBuilder.constructQueryParams(endecaSearchStateData);

        assertThat(result).isEqualTo("Nr=productDisplayFlag:1");
    }

    @Test
    public void testConstructRecordFiltersParamWithTwoFilters() {
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();

        final Map<String, String> recordFilters = new HashMap<>();
        recordFilters.put("productDisplayFlag", "1");
        recordFilters.put("availableOnline", "Available Online");
        recordFilters.put("preOrder", "Pre-order");

        endecaSearchStateData.setRecordFilters(recordFilters);

        final String result = EndecaQueryParamBuilder.constructQueryParams(endecaSearchStateData);

        assertThat(result)
                .isEqualTo("Nr=AND(productDisplayFlag:1,preOrder:Pre-order,availableOnline:Available Online)");
    }

    /**
     * Test method for
     * {@link au.com.target.endeca.infront.querybuilder.EndecaQueryParamBuilder#constructQueryParams(EndecaSearchStateData)}
     * .
     */
    @Test
    public void testConstructQueryParamFull() {

        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        final List<String> navList = new ArrayList<>();
        navList.add("10223");
        navList.add("11234");

        final List<String> searchTermList = new ArrayList<>();
        searchTermList.add("red");
        searchTermList.add("dress");
        final Map<String, Integer> sortMap = new LinkedHashMap<String, Integer>();
        sortMap.put("productPrice", Integer.valueOf(1));

        endecaSearchStateData.setSearchTerm(searchTermList);
        endecaSearchStateData.setNavigationState(navList);
        endecaSearchStateData.setItemsPerPage(30);
        endecaSearchStateData.setPageNumber(3);
        endecaSearchStateData.setSortStateMap(sortMap);
        endecaSearchStateData.setMatchMode("matchall");
        endecaSearchStateData.setSearchField("productName");
        endecaSearchStateData.setViewAs("grid");

        final String result = EndecaQueryParamBuilder.constructQueryParams(endecaSearchStateData);
        assertThat(result).isEqualTo(
                "N=10223+11234&Nrpp=30&No=90&Ntt=red+dress&Ntk=productName&Ns=productPrice|1&Ntx=mode+matchall&viewAs=grid");

    }
}
