package au.com.target.endeca.infront.converter.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.LinkedList;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.data.EndecaSearch;

import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.content.ContentException;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EndecaContentItemRecommendationsProcessorTest {

    @InjectMocks
    private final EndecaContentItemRecommendationsProcessor processor = new EndecaContentItemRecommendationsProcessor();

    private final EndecaSearch endecaSearch = new EndecaSearch();

    @Test
    public void testProcessEmptyContentItems() throws ContentException {
        processor.process(Mockito.mock(ContentItem.class), endecaSearch);

        Assert.assertEquals(1, endecaSearch.getRecommendations().size());
    }

    @Test
    public void testProcessExistingContentItems() throws ContentException {
        final LinkedList recomms = new LinkedList<>();
        recomms.add(Mockito.mock(ContentItem.class));
        recomms.add(Mockito.mock(ContentItem.class));
        endecaSearch.setRecommendations(recomms);

        processor.process(Mockito.mock(ContentItem.class), endecaSearch);

        Assert.assertEquals(3, endecaSearch.getRecommendations().size());
    }

}
