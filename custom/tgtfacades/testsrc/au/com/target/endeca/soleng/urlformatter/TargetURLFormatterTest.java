/**
 * 
 */
package au.com.target.endeca.soleng.urlformatter;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.UnsupportedEncodingException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.endeca.navigation.MutableDimLocation;
import com.endeca.navigation.MutableDimLocationList;
import com.endeca.navigation.MutableDimVal;
import com.endeca.soleng.urlformatter.UrlFormatException;
import com.endeca.soleng.urlformatter.UrlState;
import com.endeca.soleng.urlformatter.seo.SeoAggrERecFormatter;
import com.endeca.soleng.urlformatter.seo.SeoERecFormatter;
import com.endeca.soleng.urlformatter.seo.SeoNavStateEncoder;
import com.endeca.soleng.urlformatter.seo.SeoNavStateFormatter;
import com.endeca.soleng.urlformatter.seo.UrlParamEncoder;
import com.endeca.soleng.urlformatter.support.UrlParamImpl;


/**
 * Test cases to test TargetURLFormatter.
 * 
 * @author jjayawa1
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetURLFormatterTest {

    private final TargetURLFormatter pUrlFormatter = new TargetURLFormatter();

    @Before
    public void setupTestData() {
        final SeoNavStateEncoder navStateEncoder = new SeoNavStateEncoder();
        navStateEncoder.setParamKey("N");
        final UrlParamEncoder[] urlParamEncoders = { navStateEncoder };
        pUrlFormatter.setUrlParamEncoders(urlParamEncoders);

        final String[] pathParamKeys = { "R", "A", "N" };
        pUrlFormatter.setPathParamKeys(pathParamKeys);

        final SeoAggrERecFormatter aggrERecFormatter = new SeoAggrERecFormatter();
        pUrlFormatter.setAggrERecFormatter(aggrERecFormatter);

        final SeoERecFormatter seoErecFormatter = new SeoERecFormatter();
        pUrlFormatter.setERecFormatter(seoErecFormatter);

        final SeoNavStateFormatter seoNavStateFormatter = new SeoNavStateFormatter();
        seoNavStateFormatter.setUseDimensionNameAsKey(true);
        pUrlFormatter.setNavStateFormatter(seoNavStateFormatter);
    }

    @Test
    public void encodeMultipleDimensionsInNavigationState() throws UrlFormatException {
        final MutableDimLocationList dimLocationList = new MutableDimLocationList();
        final MutableDimLocation dimLocation1 = new MutableDimLocation();
        final MutableDimLocation dimLocation2 = new MutableDimLocation();
        final MutableDimVal dimVal1 = new MutableDimVal();
        dimVal1.setDimValId(279853090l);
        dimVal1.setDimensionId(279853090l);
        final MutableDimVal dimVal2 = new MutableDimVal();
        dimVal2.setDimValId(1753687403l);
        dimVal2.setDimensionId(1753687403l);
        dimLocation1.setDimValue(dimVal1);
        dimLocation2.setDimValue(dimVal2);
        dimLocationList.appendDimLocation(dimLocation1);
        dimLocationList.appendDimLocation(dimLocation2);

        final UrlState urlState = new UrlState(pUrlFormatter, "UTF-8");
        urlState.setNavState(dimLocationList);
        final String encodedValue = pUrlFormatter.formatUrl(urlState);
        Assert.assertNotNull(encodedValue);
        Assert.assertEquals("?N=4mm80yZt03luz", encodedValue);
    }

    @Test
    public void encodeMultipleDimensionsInNavigationStateWithCanonicalizationEnabled() throws UrlFormatException {
        final MutableDimLocationList dimLocationList = new MutableDimLocationList();
        final MutableDimLocation dimLocation1 = new MutableDimLocation();
        final MutableDimLocation dimLocation2 = new MutableDimLocation();
        final MutableDimVal dimVal1 = new MutableDimVal();
        dimVal1.setDimValId(279853090l);
        dimVal1.setDimensionId(279853090l);
        final MutableDimVal dimVal2 = new MutableDimVal();
        dimVal2.setDimValId(1753687403l);
        dimVal2.setDimensionId(1753687403l);
        dimLocation1.setDimValue(dimVal1);
        dimLocation2.setDimValue(dimVal2);
        dimLocationList.appendDimLocation(dimLocation1);
        dimLocationList.appendDimLocation(dimLocation2);

        final UrlState urlState = new UrlState(pUrlFormatter, "UTF-8");
        urlState.setNavState(dimLocationList);
        final String encodedValue = pUrlFormatter.formatCanonicalUrl(urlState);
        Assert.assertNotNull(encodedValue);
        Assert.assertEquals("?N=4mm80yZt03luz", encodedValue);
    }

    @Test
    public void decodeMultipleDimensionsInNavigationState() throws UrlFormatException, UnsupportedEncodingException {
        final UrlState state = pUrlFormatter.parseRequest("N=4mm80yZt03luz",
                "", "UTF-8");
        final UrlParamImpl urlParam = (UrlParamImpl)state.getParameters().toArray()[0];
        Assert.assertNotNull(urlParam.getValue());
        Assert.assertEquals("279853090 1753687403", urlParam.getValue());
    }

    @Test
    public void encodeSingleDimensionInNavigationState() throws UrlFormatException {
        final MutableDimLocationList dimLocationList = new MutableDimLocationList();
        final MutableDimLocation dimLocation1 = new MutableDimLocation();
        final MutableDimVal dimVal1 = new MutableDimVal();
        dimVal1.setDimValId(279853090l);
        dimVal1.setDimensionId(279853090l);
        dimLocation1.setDimValue(dimVal1);
        dimLocationList.appendDimLocation(dimLocation1);

        final UrlState urlState = new UrlState(pUrlFormatter, "UTF-8");
        urlState.setNavState(dimLocationList);
        final String encodedValue = pUrlFormatter.formatUrl(urlState);
        Assert.assertNotNull(encodedValue);
        Assert.assertEquals("?N=4mm80y", encodedValue);
    }

    @Test
    public void decodeSingleDimensionInNavigationState() throws UrlFormatException {
        final UrlState state = pUrlFormatter.parseRequest("N=4mm80y",
                "", "UTF-8");
        final UrlParamImpl urlParam = (UrlParamImpl)state.getParameters().toArray()[0];
        Assert.assertNotNull(urlParam.getValue());
        Assert.assertEquals("279853090", urlParam.getValue());
    }

    @Test
    public void behaviorForMultipleNParamsWithoutSeperation() throws UrlFormatException {
        final UrlState state = pUrlFormatter.parseRequest("N=4mm80yN=t03luz",
                "", "UTF-8");
        Assert.assertEquals(0, state.getParameters().size());
    }

    @Test
    public void behaviorForMultipleNParamsWithSeperation() throws UrlFormatException {
        final UrlState state = pUrlFormatter.parseRequest("N=4mm80y&N=t03luz&Npp=30",
                "", "UTF-8");
        final UrlParamImpl urlParam = (UrlParamImpl)state.getParameters().toArray()[0];
        Assert.assertNotNull(urlParam.getValue());
        Assert.assertEquals("1753687403", urlParam.getValue());
    }

}
