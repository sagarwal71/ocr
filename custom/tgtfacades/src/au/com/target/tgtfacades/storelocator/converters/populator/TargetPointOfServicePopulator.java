/**
 *
 */
package au.com.target.tgtfacades.storelocator.converters.populator;

import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.commercefacades.storelocator.data.OpeningScheduleData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.postcode.service.TargetPostCodeService;
import au.com.target.tgtfacades.config.TargetSharedConfigFacade;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtfacades.url.impl.TargetPointOfServiceUrlResolver;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtfacades.util.TargetPointOfServiceHelper;


public class TargetPointOfServicePopulator implements
        Populator<TargetPointOfServiceModel, TargetPointOfServiceData> {

    private TypeService typeService;

    private TargetPointOfServiceUrlResolver targetPointOfServiceUrlResolver;

    private TargetPostCodeService targetPostCodeService;

    private TargetPointOfServiceHelper targetPointOfServiceHelper;

    private TargetSharedConfigFacade targetSharedConfigFacade;

    private Converter<OpeningScheduleModel, OpeningScheduleData> openingScheduleConverter;

    @Override
    public void populate(final TargetPointOfServiceModel source, final TargetPointOfServiceData target) {

        final AddressData addressData = target.getAddress();
        final TargetAddressData targetAddressData = new TargetAddressData();

        if (addressData != null) {
            BeanUtils.copyProperties(addressData, targetAddressData);
            targetAddressData.setState(source.getAddress() == null ? "" : source.getAddress().getDistrict());
            targetAddressData.setBuilding(source.getAddress() == null ? "" : source.getAddress().getBuilding());
        }

        target.setStoreNumber(source.getStoreNumber());
        target.setAcceptCNC(source.getAcceptCNC());
        target.setAcceptLayBy(source.getAcceptLayBy());
        target.setClosed(source.getClosed());
        target.setTargetAddressData(targetAddressData);
        target.setShortLeadTime(getShortLeadTime(source));
        if (null != source.getType()) {
            target.setType(typeService
                    .getEnumerationValue(PointOfServiceTypeEnum._TYPECODE, source.getType().getCode())
                    .getName());
        }
        target.setUrl(targetPointOfServiceUrlResolver
                .resolve(source));
        if (null != targetAddressData.getPostalCode()) {
            final PostCodeModel postCodeModel = targetPostCodeService.getPostCode(targetAddressData.getPostalCode());
            if (null != postCodeModel) {
                target.setTimeZone(postCodeModel.getTimeZoneId());
            }
        }
        if (source.getProductTypes() != null) {
            final Set<String> acceptedProductTypes = new HashSet<>();
            for (final ProductTypeModel productTypeModel : source.getProductTypes()) {
                acceptedProductTypes.add(productTypeModel.getCode());
            }
            target.setAcceptedProductTypes(acceptedProductTypes);
        }
        if (source.getOpeningSchedule() != null) {
            target.setOpeningHours(openingScheduleConverter.convert(source.getOpeningSchedule()));
        }
        targetPointOfServiceHelper.populateDirectionUrl(StringUtils.EMPTY, target);

    }

    /**
     * Method to get default short lead time from shared config if no short lead time stored in target point of service.
     *
     * @param source
     * @return Integer
     */
    private Integer getShortLeadTime(final TargetPointOfServiceModel source) {
        if (source.getShortLeadTime() == null) {
            return Integer.valueOf(
                    targetSharedConfigFacade.getInt(TgtFacadesConstants.ShortLeadTime.LEADTIME_DEFAULT_CONFIG_CODE,
                            TgtFacadesConstants.ShortLeadTime.DEFAULT_SHORT_LEAD_TIME_VALUE));
        }
        return source.getShortLeadTime();
    }

    /**
     * @param typeService
     *            the typeService to set
     */
    @Required
    public void setTypeService(final TypeService typeService) {
        this.typeService = typeService;
    }

    /**
     * @param targetPointOfServiceUrlResolver
     *            the targetPointOfServiceUrlResolver to set
     */
    @Required
    public void setTargetPointOfServiceUrlResolver(final TargetPointOfServiceUrlResolver targetPointOfServiceUrlResolver) {
        this.targetPointOfServiceUrlResolver = targetPointOfServiceUrlResolver;
    }

    /**
     * @param targetPostCodeService
     *            the targetPostCodeService to set
     */
    @Required
    public void setTargetPostCodeService(final TargetPostCodeService targetPostCodeService) {
        this.targetPostCodeService = targetPostCodeService;
    }

    /**
     * @param targetPointOfServiceHelper
     *            the targetPointOfServiceHelper to set
     */
    @Required
    public void setTargetPointOfServiceHelper(final TargetPointOfServiceHelper targetPointOfServiceHelper) {
        this.targetPointOfServiceHelper = targetPointOfServiceHelper;
    }

    /**
     * @param targetSharedConfigFacade
     *            the targetSharedConfigFacade to set
     */
    @Required
    public void setTargetSharedConfigFacade(final TargetSharedConfigFacade targetSharedConfigFacade) {
        this.targetSharedConfigFacade = targetSharedConfigFacade;
    }

    /**
     * @param openingScheduleConverter
     *            the openingScheduleConverter to set
     */
    @Required
    public void setOpeningScheduleConverter(
            final Converter<OpeningScheduleModel, OpeningScheduleData> openingScheduleConverter) {
        this.openingScheduleConverter = openingScheduleConverter;
    }
}
