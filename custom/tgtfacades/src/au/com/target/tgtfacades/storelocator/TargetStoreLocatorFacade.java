/**
 * 
 */
package au.com.target.tgtfacades.storelocator;

import de.hybris.platform.commercefacades.storelocator.StoreLocatorFacade;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.core.model.order.CartModel;

import java.util.List;
import java.util.Map;

import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;


/**
 * Target extension to {@link StoreLocatorFacade}.
 * 
 */
@SuppressWarnings("deprecation")
public interface TargetStoreLocatorFacade extends StoreLocatorFacade {
    /**
     * 
     * @param storeNumber
     * @return TargetpointOfServiceData
     * 
     */
    TargetPointOfServiceData getPointOfService(Integer storeNumber);

    /**
     * @param checkoutCartForPurchaseOption
     * @return Map
     */
    Map<String, List<TargetPointOfServiceData>> getStateAndStoresForCart(CartModel checkoutCartForPurchaseOption);

    /**
     * 
     * @return Map of the DTO of the all the store
     */
    Map<String, List<TargetPointOfServiceData>> getAllStateAndStores();

    /**
     * @param state
     *            Uppercase shortform state string, eg. VIC, SA
     * @return the list of pos data
     */
    List<TargetPointOfServiceData> getStoresByState(String state);

    /**
     * get cnc stores by position search
     * 
     * @param geoPoint
     * @param pageableData
     * @return StoreFinderSearchPageData<PointOfServiceData>
     */
    StoreFinderSearchPageData<PointOfServiceData> getCncStoresByPositionSearch(CartModel checkoutCartModel,
            GeoPoint geoPoint,
            PageableData pageableData);

    /**
     * find cnc stores by post code
     * 
     * @param locationText
     * @param checkoutCartModel
     * @param pageableData
     * @return StoreFinderSearchPageData<PointOfServiceData>
     */
    StoreFinderSearchPageData<PointOfServiceData> getCncStoresByPostCode(String locationText,
            CartModel checkoutCartModel,
            PageableData pageableData);

    /**
     * find stores by post code, return all opened stores with cnc availability for the given cart.
     * 
     * @param locationText
     * @param checkoutCartModel
     * @param pageableData
     * @return StoreFinderSearchPageData<PointOfServiceData>
     */
    StoreFinderSearchPageData<PointOfServiceData> searchStoresWithCncAvailability(String locationText,
            CartModel checkoutCartModel,
            PageableData pageableData);
}
