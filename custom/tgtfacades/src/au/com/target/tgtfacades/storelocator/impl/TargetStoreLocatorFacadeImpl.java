/**
 * 
 */
package au.com.target.tgtfacades.storelocator.impl;

import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.storelocator.impl.DefaultStoreLocatorFacade;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storefinder.TargetStoreFinderService;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtfacades.order.converters.AbstractOrderHelper;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;


@SuppressWarnings("deprecation")
public class TargetStoreLocatorFacadeImpl extends DefaultStoreLocatorFacade implements TargetStoreLocatorFacade {
    private static final Logger LOG = Logger.getLogger(TargetStoreLocatorFacadeImpl.class);

    private TargetPointOfServiceService targetPointOfServiceService;

    private Converter<TargetPointOfServiceModel, TargetPointOfServiceData> targetPointOfServiceConverter;

    private TargetStoreFinderService storeFinderService;

    private Converter<StoreFinderSearchPageData<PointOfServiceDistanceData>, StoreFinderSearchPageData<PointOfServiceData>> searchPagePointOfServiceDistanceConverter;

    private BaseStoreService baseStoreService;

    private AbstractOrderHelper abstractOrderHelper;

    @Override
    public TargetPointOfServiceData getPointOfService(final Integer storeNumber) {

        TargetPointOfServiceModel pointOfServiceModel = null;
        try {
            pointOfServiceModel = targetPointOfServiceService.getPOSByStoreNumber(storeNumber);
        }
        catch (final TargetUnknownIdentifierException e) {
            LOG.warn("Can't find the correct TargetPointOfService for storeNumber=" + storeNumber);
            return null;
        }
        catch (final TargetAmbiguousIdentifierException e) {
            LOG.error("find more than one TargetPointOfService for storeNumber=" + storeNumber);
            return null;
        }

        return targetPointOfServiceConverter.convert(pointOfServiceModel);

    }

    @Override
    public Map<String, List<TargetPointOfServiceData>> getStateAndStoresForCart(
            final CartModel cartModel) {

        final Map<String, Set<TargetPointOfServiceModel>> storesModel = targetPointOfServiceService
                .getStateAndStoresForCart(cartModel);

        return convertToTargetPointOfServiceData(storesModel);
    }

    protected Map<String, List<TargetPointOfServiceData>> convertToTargetPointOfServiceData(
            final Map<String, Set<TargetPointOfServiceModel>> storesModel) {
        if (storesModel == null) {
            return Collections.emptyMap();
        }

        final Map<String, List<TargetPointOfServiceData>> stores = new TreeMap<String, List<TargetPointOfServiceData>>();

        for (final Entry<String, Set<TargetPointOfServiceModel>> entry : storesModel.entrySet()) {
            final List<TargetPointOfServiceData> targetPointOfServices = convertSetTargetPointOfServiceData(entry
                    .getValue());
            stores.put(entry.getKey(), targetPointOfServices);
        }

        return stores;
    }

    protected List<TargetPointOfServiceData> convertSetTargetPointOfServiceData(
            final Set<TargetPointOfServiceModel> storesSet) {
        if (storesSet == null) {
            return Collections.emptyList();
        }

        final List<TargetPointOfServiceData> targetPointOfServices = new ArrayList<>();
        for (final TargetPointOfServiceModel targetPointOfServiceModel : storesSet) {
            targetPointOfServices.add(targetPointOfServiceConverter
                    .convert(targetPointOfServiceModel));
        }
        return targetPointOfServices;
    }


    @Override
    public Map<String, List<TargetPointOfServiceData>> getAllStateAndStores() {

        final Map<String, Set<TargetPointOfServiceModel>> storesModel = targetPointOfServiceService
                .getAllStateAndStores();

        return convertToTargetPointOfServiceData(storesModel);
    }

    @Override
    public List<TargetPointOfServiceData> getStoresByState(final String state) {

        final Map<String, Set<TargetPointOfServiceModel>> storesModel = targetPointOfServiceService
                .getAllStateAndStores();

        if (storesModel == null) {
            return Collections.emptyList();
        }

        return convertSetTargetPointOfServiceData(storesModel.get(state));
    }

    @Override
    public StoreFinderSearchPageData<PointOfServiceData> getCncStoresByPositionSearch(final CartModel cartModel,
            final GeoPoint geoPoint,
            final PageableData pageableData) {
        final List<TargetPointOfServiceModel> pointOfServiceListForCnc = targetPointOfServiceService
                .getStoresForCart(cartModel);
        final StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageData = storeFinderService
                .doSearch(null, geoPoint, pageableData, pointOfServiceListForCnc);
        return searchPagePointOfServiceDistanceConverter.convert(storeFinderSearchPageData);
    }

    @Override
    public StoreFinderSearchPageData<PointOfServiceData> getCncStoresByPostCode(final String locationText,
            final CartModel checkoutCartModel, final PageableData pageableData) {
        final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
        final List<TargetPointOfServiceModel> pointOfServiceListForCnc = targetPointOfServiceService
                .getStoresForCart(checkoutCartModel);
        final StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageData = storeFinderService
                .doSearchByLocation(currentBaseStore, locationText, pageableData, pointOfServiceListForCnc);
        return searchPagePointOfServiceDistanceConverter.convert(storeFinderSearchPageData);

    }

    @Override
    public StoreFinderSearchPageData<PointOfServiceData> searchStoresWithCncAvailability(final String locationText,
            final CartModel checkoutCartModel, final PageableData pageableData) {
        final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
        final List<TargetPointOfServiceModel> pointOfServiceList = targetPointOfServiceService
                .getAllOpenStores();
        final StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageData = storeFinderService
                .doSearchByLocation(currentBaseStore, locationText, pageableData, pointOfServiceList);
        final StoreFinderSearchPageData<PointOfServiceData> result = searchPagePointOfServiceDistanceConverter
                .convert(storeFinderSearchPageData);
        updateStoresWithCartAvailability(result, checkoutCartModel);
        return result;
    }

    /**
     * @param result
     * @param cart
     */

    private void updateStoresWithCartAvailability(final StoreFinderSearchPageData<PointOfServiceData> result,
            final CartModel cart) {
        if (result != null && result.getResults() != null) {
            final Set<String> cartProductTypeSet = abstractOrderHelper.getProductTypeSet(cart);
            for (final PointOfServiceData posData : result.getResults()) {
                abstractOrderHelper.updateStoreWithPickupAvailability(cartProductTypeSet, posData);
            }
        }
    }

    @Required
    public void setTargetPointOfServiceService(final TargetPointOfServiceService targetPointOfServiceService) {
        this.targetPointOfServiceService = targetPointOfServiceService;
    }

    @Required
    public void setTargetPointOfServiceConverter(
            final Converter<TargetPointOfServiceModel, TargetPointOfServiceData> targetPointOfServiceConverter) {
        this.targetPointOfServiceConverter = targetPointOfServiceConverter;
    }

    @Required
    public void setStoreFinderService(final TargetStoreFinderService storeFinderService) {
        this.storeFinderService = storeFinderService;
    }

    @Required
    public void setSearchPagePointOfServiceDistanceConverter(
            final Converter<StoreFinderSearchPageData<PointOfServiceDistanceData>, StoreFinderSearchPageData<PointOfServiceData>> searchPagePointOfServiceDistanceConverter) {
        this.searchPagePointOfServiceDistanceConverter = searchPagePointOfServiceDistanceConverter;
    }

    @Required
    public void setBaseStoreService(final BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    @Required
    public void setAbstractOrderHelper(final AbstractOrderHelper abstractOrderHelper) {
        this.abstractOrderHelper = abstractOrderHelper;
    }

}
