/**
 * 
 */
package au.com.target.tgtfacades.storelocator.data;

import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;

import java.util.Map;
import java.util.Set;

import au.com.target.tgtfacades.storelocator.utils.StoreLocatorUtil;
import au.com.target.tgtfacades.user.data.TargetAddressData;


/**
 * Data object representing the TargetPointOfService Model.
 */
public class TargetPointOfServiceData extends PointOfServiceData {
    private Integer storeNumber;

    private Boolean closed;

    private Boolean acceptCNC;

    private Boolean acceptLayBy;

    private TargetAddressData targetAddressData;

    private String type;

    private String timeZone;

    //flag for whether allow pick up from store for current cart
    private Boolean availableForPickUp;

    private Set<String> acceptedProductTypes;

    private String directionUrl;

    private Integer shortLeadTime;

    /**
     * @return the storeNumber
     */
    public Integer getStoreNumber() {
        return storeNumber;
    }



    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final Integer storeNumber) {
        this.storeNumber = storeNumber;
    }



    /**
     * @return the closed
     */
    public Boolean getClosed() {
        return closed;
    }



    /**
     * @param closed
     *            the closed to set
     */
    public void setClosed(final Boolean closed) {
        this.closed = closed;
    }



    /**
     * @return the acceptCNC
     */
    public Boolean getAcceptCNC() {
        return acceptCNC;
    }



    /**
     * @param acceptCNC
     *            the acceptCNC to set
     */
    public void setAcceptCNC(final Boolean acceptCNC) {
        this.acceptCNC = acceptCNC;
    }



    /**
     * @return the acceptLayBy
     */
    public Boolean getAcceptLayBy() {
        return acceptLayBy;
    }



    /**
     * @param acceptLayBy
     *            the acceptLayBy to set
     */
    public void setAcceptLayBy(final Boolean acceptLayBy) {
        this.acceptLayBy = acceptLayBy;
    }



    /**
     * @return the targetAddressData
     */
    public TargetAddressData getTargetAddressData() {
        return targetAddressData;
    }



    /**
     * @param targetAddressData
     *            the targetAddressData to set
     */
    public void setTargetAddressData(final TargetAddressData targetAddressData) {
        this.targetAddressData = targetAddressData;
    }



    /**
     * @return the type
     */
    public String getType() {
        return type;
    }



    /**
     * @param type
     *            the type to set
     */
    public void setType(final String type) {
        this.type = type;
    }


    /**
     * Get Formatted Date
     * 
     * @return Map
     */
    public Map<String, String> formattedOpeningDate() {
        return StoreLocatorUtil.getFormattedStoreOpeningSchedule(this);
    }



    /**
     * @return the timeZone
     */
    public String getTimeZone() {
        return timeZone;
    }



    /**
     * @param timeZone
     *            the timeZone to set
     */
    public void setTimeZone(final String timeZone) {
        this.timeZone = timeZone;
    }



    /**
     * @return true if cnc is available for given cart
     */
    public Boolean getAvailableForPickup() {
        return availableForPickUp;
    }



    /**
     * @param available
     *            the available to set
     */
    public void setAvailableForPickup(final Boolean available) {
        this.availableForPickUp = available;
    }



    /**
     * @return the acceptedProductTypes
     */
    public Set<String> getAcceptedProductTypes() {
        return acceptedProductTypes;
    }



    /**
     * @param acceptedProductTypes
     *            the acceptedProductTypes to set
     */
    public void setAcceptedProductTypes(final Set<String> acceptedProductTypes) {
        this.acceptedProductTypes = acceptedProductTypes;
    }



    /**
     * @return the directionUrl
     */
    public String getDirectionUrl() {
        return directionUrl;
    }



    /**
     * @param directionUrl
     *            the directionUrl to set
     */
    public void setDirectionUrl(final String directionUrl) {
        this.directionUrl = directionUrl;
    }



    /**
     * @return the shortLeadTime
     */
    public Integer getShortLeadTime() {
        return shortLeadTime;
    }



    /**
     * @param shortLeadTime
     *            the shortLeadTime to set
     */
    public void setShortLeadTime(final Integer shortLeadTime) {
        this.shortLeadTime = shortLeadTime;
    }

}
