/**
 * 
 */
package au.com.target.tgtfacades.community.utils;

import javax.annotation.Resource;

import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;


/**
 * @author knemalik
 * 
 */

public class TargetCommunityDonationHelper {

    @Resource(name = "targetStoreLocatorFacade")
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    /**
     * get the store state of a POS based on the storeNumber
     * 
     * @param storeNumber
     * @return storeState
     */
    public String getStoreState(final String storeNumber) {

        final TargetPointOfServiceData selectedStoreState = targetStoreLocatorFacade.getPointOfService(Integer
                .valueOf(storeNumber));

        if (selectedStoreState == null || selectedStoreState.getTargetAddressData() == null) {
            return null;
        }

        final String storeState = selectedStoreState.getTargetAddressData().getState();

        return storeState;

    }
}