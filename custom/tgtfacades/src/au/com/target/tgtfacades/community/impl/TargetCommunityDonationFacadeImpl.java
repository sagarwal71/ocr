/**
 * 
 */
package au.com.target.tgtfacades.community.impl;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.community.dto.TargetCommunityDonationRequestDto;
import au.com.target.tgtcore.community.service.TargetCommunityDonationService;
import au.com.target.tgtfacades.community.TargetCommunityDonationFacade;
import au.com.target.tgtfacades.community.utils.TargetCommunityDonationHelper;


/**
 * @author knemalik
 * 
 */
public class TargetCommunityDonationFacadeImpl implements TargetCommunityDonationFacade {

    private TargetCommunityDonationHelper targetCommunityDonationHelper;
    private TargetCommunityDonationService targetCommunityDonationService;


    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.community.TargetCommunityDonationFacade#sendDonationRequest(au.com.target.tgtcore.community.dto.TargetCommunityDonationRequestDto)
     */
    @Override
    public boolean sendDonationRequest(final TargetCommunityDonationRequestDto targetCommunityDonationRequestDto) {

        if (targetCommunityDonationRequestDto == null) {
            return false;
        }

        final String storeState = targetCommunityDonationHelper.getStoreState(targetCommunityDonationRequestDto
                .getStoreNumber());

        targetCommunityDonationRequestDto.setStoreState(storeState);
        return targetCommunityDonationService.sendDonationRequest(targetCommunityDonationRequestDto);

    }

    /**
     * @param targetCommunityDonationHelper
     *            the targetCommunityDonationHelper to set
     */
    @Required
    public void setTargetCommunityDonationHelper(final TargetCommunityDonationHelper targetCommunityDonationHelper) {
        this.targetCommunityDonationHelper = targetCommunityDonationHelper;
    }

    /**
     * @param targetCommunityDonationService
     *            the targetCommunityDonationService to set
     */
    @Required
    public void setTargetCommunityDonationService(final TargetCommunityDonationService targetCommunityDonationService) {
        this.targetCommunityDonationService = targetCommunityDonationService;
    }
}
