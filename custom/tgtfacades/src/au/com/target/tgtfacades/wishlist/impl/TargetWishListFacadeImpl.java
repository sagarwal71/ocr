/**
 * 
 */
package au.com.target.tgtfacades.wishlist.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.product.data.CustomerProductInfo;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.wishlist.TargetWishListFacade;
import au.com.target.tgtfacades.wishlist.converters.TargetCustomerProductInfoConverter;
import au.com.target.tgtfacades.wishlist.converters.TargetProductDataToCustomerWishListProductConverter;
import au.com.target.tgtfacades.wishlist.converters.TargetWishListEntryModelConverter;
import au.com.target.tgtwishlist.data.TargetCustomerWishListDto;
import au.com.target.tgtwishlist.data.TargetCustomerWishListProductsDto;
import au.com.target.tgtwishlist.data.TargetRecipientDto;
import au.com.target.tgtwishlist.data.TargetWishListEntryData;
import au.com.target.tgtwishlist.data.TargetWishListSendRequestDto;
import au.com.target.tgtwishlist.enums.WishListTypeEnum;
import au.com.target.tgtwishlist.exception.InvalidWishListUserException;
import au.com.target.tgtwishlist.logger.TgtWishListLogger;
import au.com.target.tgtwishlist.model.TargetWishListEntryModel;
import au.com.target.tgtwishlist.model.TargetWishListModel;
import au.com.target.tgtwishlist.service.TargetWishListService;


/**
 * @author paul
 *
 */
public class TargetWishListFacadeImpl implements TargetWishListFacade {

    protected static final Logger LOG = Logger.getLogger(TargetWishListFacadeImpl.class);

    private TargetWishListService targetWishListService;

    private UserService userService;

    private SalesApplicationFacade salesApplicationFacade;

    private TargetCustomerProductInfoConverter targetCustomerProductInfoConverter;

    private TargetWishListEntryModelConverter targetWishListEntryModelConverter;

    private TargetProductDataToCustomerWishListProductConverter targetProductDataToCustomerWishListProductConverter;

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.wishlist.TargetWishListFacade#addProductToList(java.lang.String, java.lang.String, java.lang.String, au.com.target.tgtwishlist.enums.WishListTypeEnum)
     */
    @Override
    public void addProductToList(final CustomerProductInfo customerProductInfo, final String listName,
            final WishListTypeEnum wishListTypeEum) {

        final UserModel userModel = userService.getCurrentUser();
        checkUserPermission(userModel, TgtWishListLogger.ADD_FAV_LIST_ACTION);

        final TargetWishListModel targetWishListModel = targetWishListService.verifyAndCreateList(listName, userModel,
                wishListTypeEum);

        final TargetWishListEntryData targetWishListEntryData = targetCustomerProductInfoConverter
                .convert(customerProductInfo);
        final SalesApplication salesApplication = salesApplicationFacade.getCurrentSalesApplication();
        targetWishListService.addProductToWishList(targetWishListModel, targetWishListEntryData,
                TgtWishListLogger.ADD_FAV_LIST_ACTION, salesApplication);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.wishlist.TargetWishListFacade#removeProductFromList(au.com.target.tgtfacades.product.data.CustomerProductInfo, java.lang.String, au.com.target.tgtwishlist.enums.WishListTypeEnum)
     */
    @Override
    public void removeProductsFromList(final List<CustomerProductInfo> customerProductInfo, final String listName,
            final WishListTypeEnum wishListTypeEnum) {
        final UserModel userModel = userService.getCurrentUser();
        checkUserPermission(userModel, TgtWishListLogger.REMOVE_FAV_LIST_ACTION);

        if (CollectionUtils.isEmpty(customerProductInfo)) {
            return;
        }

        final List<TargetWishListEntryData> wishListEntryData = convertCustomerProductInfo(customerProductInfo);

        final SalesApplication salesApplication = salesApplicationFacade.getCurrentSalesApplication();

        final TargetWishListModel wishListModel = targetWishListService.removeWishListEntriesForUser(userModel,
                wishListEntryData,
                salesApplication);

        if (wishListModel == null) {
            TgtWishListLogger.logFavListUnExpectedError(LOG, TgtWishListLogger.REMOVE_FAV_LIST_ACTION,
                    TgtWishListLogger.UNEXPECTED_REMOVE_FAV_LIST_ERROR,
                    String.valueOf(userModel.getPk()));
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.wishlist.TargetWishListFacade#syncFavouritesList(java.util.List)
     */
    @Override
    public List<CustomerProductInfo> syncFavouritesList(final List<CustomerProductInfo> customerProductInfo) {
        final UserModel userModel = userService.getCurrentUser();

        checkUserPermission(userModel, TgtWishListLogger.SYNC_FAV_LIST_ACTION);

        final List<TargetWishListEntryData> wishListEntryData = convertCustomerProductInfo(customerProductInfo);

        final SalesApplication salesApplication = salesApplicationFacade.getCurrentSalesApplication();

        final TargetWishListModel wishLists = targetWishListService.syncWishListForUser(userModel,
                wishListEntryData, salesApplication);
        if (wishLists != null) {
            return convertWishListEntryModel(wishLists.getEntries());
        }


        TgtWishListLogger.logFavListUnExpectedError(LOG, TgtWishListLogger.SYNC_FAV_LIST_ACTION,
                TgtWishListLogger.UNEXPECTED_SYNCH_LIST_ERROR,
                String.valueOf(userModel.getPk()));

        return customerProductInfo;
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.wishlist.TargetWishListFacade#retrieveFavouriteProducts()
     */
    @Override
    public List<CustomerProductInfo> retrieveFavouriteProducts() {

        final UserModel userModel = userService.getCurrentUser();
        checkUserPermission(userModel, TgtWishListLogger.RETRIEVE_LIST_ACTION);
        final TargetWishListModel wishLists = targetWishListService.retrieveWishList(StringUtils.EMPTY, userModel,
                WishListTypeEnum.FAVOURITE);
        if (wishLists != null) {
            return convertWishListEntryModel(wishLists.getEntries());
        }

        TgtWishListLogger.logFavListUnExpectedError(LOG, TgtWishListLogger.RETRIEVE_LIST_ACTION,
                TgtWishListLogger.UNEXPECTED_SYNCH_LIST_ERROR,
                String.valueOf(userModel.getPk()));

        return null;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.wishlist.TargetWishListFacade#shareWishListForUser(java.util.List, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public boolean shareWishListForUser(final List<TargetProductListerData> targetProductListerData,
            final String receipientEmailAddress, final String name, final String messageText) {
        if (CollectionUtils.isNotEmpty(targetProductListerData)) {
            Assert.notNull(receipientEmailAddress, "Recipient Email address cannot be null");
            Assert.notNull(name, "Recipient Name cannot be null");

            final UserModel userModel = userService.getCurrentUser();

            Assert.isInstanceOf(TargetCustomerModel.class, userModel, "User not a Target Customer");

            final TargetCustomerModel customer = (TargetCustomerModel)userModel;
            //conversion
            final TargetWishListSendRequestDto requestDto = new TargetWishListSendRequestDto();

            final TargetCustomerWishListDto customerWishListDto = new TargetCustomerWishListDto();
            customerWishListDto.setFirstName(customer.getFirstname());
            customerWishListDto.setLastName(customer.getLastname());
            customerWishListDto.setEmailAddress(customer.getContactEmail());

            final TargetRecipientDto recipient = new TargetRecipientDto();
            recipient.setEmailAddress(receipientEmailAddress);
            recipient.setName(name);
            recipient.setMessage(messageText);
            customerWishListDto.setRecipients(Collections.singletonList(recipient));

            final List<TargetCustomerWishListProductsDto> targetCustomerWishList = new ArrayList<>();

            for (final TargetProductListerData product : targetProductListerData) {
                final TargetCustomerWishListProductsDto wishListProductDto = targetProductDataToCustomerWishListProductConverter
                        .convert(product);
                targetCustomerWishList.add(wishListProductDto);
            }
            customerWishListDto.setProducts(targetCustomerWishList);

            requestDto.setCustomer(customerWishListDto);

            return targetWishListService.shareWishList(requestDto);
        }
        return false;
    }

    /**
     * @param userModel
     */
    protected void checkUserPermission(final UserModel userModel, final String action) {
        if (!isUserValidForListOperation(userModel)) {
            final String userId = userModel != null ? String.valueOf(userModel.getPk()) : null;
            throw new InvalidWishListUserException(MessageFormat.format(TgtWishListLogger.INVALID_WISHLIST_USER,
                    action, userId));
        }
    }

    /**
     * @param entries
     * @return List of CustomerProductInfo objects
     */
    private List<CustomerProductInfo> convertWishListEntryModel(final List<Wishlist2EntryModel> entries) {
        final List<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(entries)) {
            for (final Wishlist2EntryModel entry : entries) {
                if (entry instanceof TargetWishListEntryModel && entry.getProduct() != null) {
                    customerProductInfos
                            .add(targetWishListEntryModelConverter.convert((TargetWishListEntryModel)entry));
                }
                else {
                    LOG.warn("WISHLIST: product empty in wishListEntry:" + entry.getPk());
                }
            }
        }

        Collections.sort(customerProductInfos, new Comparator<CustomerProductInfo>() {
            @Override
            public final int compare(final CustomerProductInfo c1, final CustomerProductInfo c2) {
                try {
                    if (Long.parseLong(c2.getTimeStamp()) > Long.parseLong(c1.getTimeStamp())) {
                        return 1;
                    }
                    return -1;
                }
                catch (final NumberFormatException ne) {
                    return -1;
                }
            }
        });

        return customerProductInfos;
    }

    /**
     * @param customerProductInfo
     */
    private List<TargetWishListEntryData> convertCustomerProductInfo(
            final List<CustomerProductInfo> customerProductInfo) {
        final List<TargetWishListEntryData> wishListEntryData = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(customerProductInfo)) {
            for (final CustomerProductInfo customerInfo : customerProductInfo) {
                wishListEntryData.add(targetCustomerProductInfoConverter.convert(customerInfo));
            }
        }
        return wishListEntryData;
    }

    private boolean isUserValidForListOperation(final UserModel userModel) {
        if (!userService.isAnonymousUser(userModel) && userModel instanceof TargetCustomerModel) {
            return true;
        }
        return false;
    }

    /**
     * @param targetWishListService
     *            the targetWishListService to set
     */
    @Required
    public void setTargetWishListService(final TargetWishListService targetWishListService) {
        this.targetWishListService = targetWishListService;
    }


    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }


    /**
     * @param salesApplicationFacade
     *            the salesApplicationFacade to set
     */
    @Required
    public void setSalesApplicationFacade(final SalesApplicationFacade salesApplicationFacade) {
        this.salesApplicationFacade = salesApplicationFacade;
    }

    /**
     * @param targetCustomerProductInfoConverter
     *            the targetCustomerProductInfoConverter to set
     */
    @Required
    public void setTargetCustomerProductInfoConverter(
            final TargetCustomerProductInfoConverter targetCustomerProductInfoConverter) {
        this.targetCustomerProductInfoConverter = targetCustomerProductInfoConverter;
    }

    /**
     * @param targetWishListEntryModelConverter
     *            the targetWishListEntryModelConverter to set
     */
    @Required
    public void setTargetWishListEntryModelConverter(
            final TargetWishListEntryModelConverter targetWishListEntryModelConverter) {
        this.targetWishListEntryModelConverter = targetWishListEntryModelConverter;
    }

    /**
     * @param targetProductDataToCustomerWishListProductConverter
     *            the targetProductDataToCustomerWishListProductConverter to set
     */
    @Required
    public void setTargetProductDataToCustomerWishListProductConverter(
            final TargetProductDataToCustomerWishListProductConverter targetProductDataToCustomerWishListProductConverter) {
        this.targetProductDataToCustomerWishListProductConverter = targetProductDataToCustomerWishListProductConverter;
    }

    @Override
    public void replaceFavouritesWithLatest(final List<CustomerProductInfo> customerProductInfo,
            final List<CustomerProductInfo> updatedCustomerProductInfo) {
        if (CollectionUtils.size(updatedCustomerProductInfo) < CollectionUtils.size(customerProductInfo)) {
            final UserModel userModel = userService.getCurrentUser();
            if (isUserValidForListOperation(userModel)) {
                targetWishListService.replaceFavouritesWithLatest(userModel,
                        convertCustomerProductInfo(updatedCustomerProductInfo),
                        salesApplicationFacade.getCurrentSalesApplication());
            }
        }
    }
}
