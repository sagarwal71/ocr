/**
 * 
 */
package au.com.target.tgtfacades.wishlist.converters;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.util.Assert;

import au.com.target.tgtfacades.product.data.CustomerProductInfo;
import au.com.target.tgtwishlist.model.TargetWishListEntryModel;


/**
 * @author rsamuel3
 *
 */
public class TargetWishListEntryModelConverter implements Converter<TargetWishListEntryModel, CustomerProductInfo> {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.dto.converter.Converter#convert(java.lang.Object)
     */
    @Override
    public CustomerProductInfo convert(final TargetWishListEntryModel wishListEntryData) throws ConversionException {
        return convert(wishListEntryData, new CustomerProductInfo());
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.dto.converter.Converter#convert(java.lang.Object, java.lang.Object)
     */
    @Override
    public CustomerProductInfo convert(final TargetWishListEntryModel wishListEntryData,
            final CustomerProductInfo customerProductInfo)
            throws ConversionException {
        Assert.notNull(wishListEntryData, "TargetWishListEntryData cannot be null");
        Assert.notNull(customerProductInfo, "CustomerProductInfo cannot be null");
        customerProductInfo.setBaseProductCode(wishListEntryData.getProduct().getCode());
        customerProductInfo.setSelectedVariantCode(wishListEntryData.getSelectedVariant());
        customerProductInfo.setTimeStamp(String.valueOf(wishListEntryData.getAddedDate().getTime()));
        return customerProductInfo;
    }

}
