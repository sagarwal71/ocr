/**
 * 
 */
package au.com.target.tgtfacades.data;

/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class CanonicalData {

    private String url;

    private String code;

    private String name;

    private String nextUrl;

    private String prevUrl;

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     *            the url to set
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the nextUrl
     */
    public String getNextUrl() {
        return nextUrl;
    }

    /**
     * @param nextUrl
     *            the nextUrl to set
     */
    public void setNextUrl(final String nextUrl) {
        this.nextUrl = nextUrl;
    }

    /**
     * @return the prevUrl
     */
    public String getPrevUrl() {
        return prevUrl;
    }

    /**
     * @param prevUrl
     *            the prevUrl to set
     */
    public void setPrevUrl(final String prevUrl) {
        this.prevUrl = prevUrl;
    }

}
