/**
 *
 */
package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.platform.basecommerce.enums.OrderEntryStatus;
import de.hybris.platform.commercefacades.order.converters.populator.OrderPopulator;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.IpgGiftCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordercancel.OrderUtils;
import de.hybris.platform.ordercancel.model.OrderCancelRecordModel;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.returns.model.OrderReturnRecordModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.voucher.VoucherService;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.barcode.TargetBarCodeService;
import au.com.target.tgtcore.formatter.InvoiceFormatterHelper;
import au.com.target.tgtcore.util.OrderEncodingTools;
import au.com.target.tgtcore.util.ProductUtil;
import au.com.target.tgtfacades.affiliate.converters.AffiliateOrderDataConverter;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.order.converters.AbstractOrderHelper;
import au.com.target.tgtfacades.order.converters.OrderEntryModificationRecordEntryConverter;
import au.com.target.tgtfacades.order.converters.TargetFlybuysDiscountConverter;
import au.com.target.tgtfacades.order.data.LaybyTransactionData;
import au.com.target.tgtfacades.order.data.OrderModificationData;
import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.order.enums.OrderModificationStatusEnum;
import au.com.target.tgtfluent.constants.TgtFluentConstants;
import au.com.target.tgtlayby.order.TargetLaybyPaymentDueService;
import au.com.target.tgtlayby.order.strategies.OrderOutstandingStrategy;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalHerePaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;
import au.com.target.tgtpayment.model.ZippayPaymentInfoModel;
import au.com.target.tgtpayment.util.PaymentTransactionHelper;


/**
 * @author Benoit VanalderWeireldt
 *
 */
public class TargetOrderPopulator extends OrderPopulator {

    private static final Logger LOG = Logger.getLogger(TargetOrderPopulator.class);

    private static final String PAYPAL_PAYMENTMETHOD = "PayPal";

    private AbstractOrderHelper abstractOrderHelper;

    private TargetLaybyPaymentDueService targetLaybyPaymentDueService;

    private Converter<PaypalPaymentInfoModel, TargetCCPaymentInfoData> targetPayPalPaymentInfoConverter;

    private Converter<PaypalHerePaymentInfoModel, TargetCCPaymentInfoData> targetPayPalHerePaymentInfoConverter;

    private Converter<PinPadPaymentInfoModel, TargetCCPaymentInfoData> targetPinPadPaymentInfoConverter;

    private Converter<IpgPaymentInfoModel, TargetCCPaymentInfoData> targetIpgPaymentInfoConverter;

    private Converter<IpgGiftCardPaymentInfoModel, TargetCCPaymentInfoData> targetIpgGiftCardPaymentInfoConverter;

    private Converter<AfterpayPaymentInfoModel, TargetCCPaymentInfoData> targetAfterpayPaymentInfoConverter;

    private Converter<ZippayPaymentInfoModel, TargetCCPaymentInfoData> targetZipPaymentInfoConverter;

    private OrderEntryModificationRecordEntryConverter orderEntryModificationRecordEntryConverter;

    private TargetBarCodeService targetBarCodeService;

    private TypeService typeService;

    private OrderOutstandingStrategy orderOutstandingStrategy;

    private TargetFlybuysDiscountConverter targetFlybuysDiscountConverter;

    private VoucherService voucherService;

    private AffiliateOrderDataConverter affiliateOrderDataConverter;

    private InvoiceFormatterHelper invoiceFormatterHelper;

    @Override
    protected void addEntries(final AbstractOrderModel source, final AbstractOrderData prototype) {
        prototype.setEntries(Converters.convertAll(getLivingEntries(source), getOrderEntryConverter()));
    }

    private List<AbstractOrderEntryModel> getLivingEntries(final AbstractOrderModel source) {

        if (!OrderUtils.hasLivingEntries(source)) {
            return Collections.EMPTY_LIST;
        }
        final List<AbstractOrderEntryModel> livingEntries = new ArrayList<>();
        for (final AbstractOrderEntryModel entry : source.getEntries()) {
            if (entry.getQuantityStatus() == null || OrderEntryStatus.LIVING.equals(entry.getQuantityStatus())) {
                livingEntries.add(entry);
            }
        }
        return livingEntries;
    }

    @Override
    public void populate(final OrderModel source, final OrderData target) {
        super.populate(source, target);
        if (target instanceof TargetOrderData) {
            final TargetOrderData targetOrderData = (TargetOrderData)target;
            targetOrderData.setEmail(((CustomerModel)source.getUser()).getContactEmail());
            targetOrderData.setCncStoreNumber(source.getCncStoreNumber());
            targetOrderData.setTmdNumber(source.getTmdCardNumber());
            targetOrderData.setFlybuysNumber(source.getFlyBuysCode());
            targetOrderData
                    .setMaskedFlybuysNumber(invoiceFormatterHelper.getMaskedFlybuysNumber(source.getFlyBuysCode()));

            final SalesApplication salesApplication = source.getSalesApplication();
            if (SalesApplication.TRADEME.equals(salesApplication)) {
                targetOrderData.setTradeMeOrder(true);
                targetOrderData.seteBayOrderNumber(source.getEBayOrderNumber());
            }
            else if (SalesApplication.EBAY.equals(salesApplication)) {
                targetOrderData.setEbayOrder(true);
                targetOrderData.seteBayOrderNumber(source.getEBayOrderNumber());
            }
            else {
                targetOrderData.setEbayOrder(false);
            }

            if (source.getPurchaseOption() != null) {
                targetOrderData.setPurchaseOptionCode(source.getPurchaseOption().getCode());
            }

            targetOrderData.setHasDigitalProducts(abstractOrderHelper.hasDigitalProducts(source));
            targetOrderData.setHasPhysicalGiftCards(abstractOrderHelper.hasPhysicalGiftCards(source));

            setLayByInfo(source, targetOrderData);

            setTransactionPaymentInfo(source, targetOrderData);

            setAppliedVoucherCodes(source, targetOrderData);

            setCancelledReturnsProducts(source, targetOrderData);
            if (BooleanUtils.isTrue(source.getContainsPreOrderItems())
                    || source.getNormalSaleStartDateTime() != null) {
                targetOrderData.setContainsPreOrderItems(BooleanUtils.toBoolean(source.getContainsPreOrderItems()));
                setPreOrderInfo(source, targetOrderData);
            }
            if (null != source.getCncStoreNumber()) {
                String barcodePngUrl = StringUtils.EMPTY;
                String barcodeSvgUrl = StringUtils.EMPTY;
                final String enCodedOrderNumber = encodeOrderId(source.getCode());
                try {
                    final String urlEncodedOrderNumber = URLEncoder
                            .encode(enCodedOrderNumber, TgtFacadesConstants.ENCOD);
                    barcodeSvgUrl = TgtFacadesConstants.ORDER_BARCODE
                            + TgtFacadesConstants.BARCODE_SVG_FORMAT
                            + TgtFacadesConstants.ORDER_NUMBER + urlEncodedOrderNumber;
                    barcodePngUrl = TgtFacadesConstants.ORDER_BARCODE
                            + TgtFacadesConstants.BARCODE_PNG_FORMAT
                            + TgtFacadesConstants.ORDER_NUMBER + urlEncodedOrderNumber;
                }
                catch (final UnsupportedEncodingException e) {
                    LOG.error("Not Supported encoding", e);
                }
                targetOrderData.setBarcodePngUrl(barcodePngUrl);
                targetOrderData.setBarcodeSvgUrl(barcodeSvgUrl);
                if (null != enCodedOrderNumber) {
                    targetOrderData.setBarcodeNumber(targetBarCodeService.getPlainBarcode(enCodedOrderNumber));
                }
            }

            //Check multiple consiments
            int totalQuantity = 0;
            for (final AbstractOrderEntryModel entry : source.getEntries()) {
                final ProductModel productModel = entry.getProduct();
                if (null != source.getDeliveryMode()
                        && TgtFluentConstants.DeliveryMode.HOME_DELIVERY.equals(source.getDeliveryMode().getCode())
                        && !ProductUtil.isProductTypeDigital(productModel)) {
                    totalQuantity += entry.getQuantity().intValue();
                }
            }
            if (totalQuantity > 1) {
                targetOrderData.setMultipleConsignments(true);
            }
            targetFlybuysDiscountConverter.convertFlybuysDiscount(source, target);
            affiliateOrderDataConverter.populate(source, targetOrderData);

        }
    }



    protected String encodeOrderId(final String orderId) {
        return OrderEncodingTools.encodeOrderId(orderId);
    }


    @Override
    protected void addPaymentInformation(final AbstractOrderModel source, final AbstractOrderData prototype) {
        final PaymentInfoModel paymentInfo = source.getPaymentInfo();
        if (paymentInfo instanceof CreditCardPaymentInfoModel) {
            prototype.setPaymentInfo(getCreditCardPaymentInfoConverter().convert(
                    (CreditCardPaymentInfoModel)paymentInfo));
        }
        else if (paymentInfo instanceof PaypalPaymentInfoModel) {
            prototype.setPaymentInfo(targetPayPalPaymentInfoConverter.convert(
                    (PaypalPaymentInfoModel)paymentInfo));
        }
        else if (paymentInfo instanceof PinPadPaymentInfoModel) {
            prototype.setPaymentInfo(getTargetPinPadPaymentInfoConverter().convert(
                    (PinPadPaymentInfoModel)paymentInfo));
        }
        else if (paymentInfo instanceof IpgPaymentInfoModel) {
            prototype.setPaymentInfo(getTargetIpgPaymentInfoConverter().convert(
                    (IpgPaymentInfoModel)paymentInfo));
        }
        else if (paymentInfo instanceof PaypalHerePaymentInfoModel) {
            prototype.setPaymentInfo(targetPayPalHerePaymentInfoConverter.convert(
                    (PaypalHerePaymentInfoModel)paymentInfo));
        }
        else if (paymentInfo instanceof AfterpayPaymentInfoModel) {
            prototype.setPaymentInfo(targetAfterpayPaymentInfoConverter.convert((AfterpayPaymentInfoModel)paymentInfo));
        }
        else if (paymentInfo instanceof ZippayPaymentInfoModel) {
            prototype.setPaymentInfo(targetZipPaymentInfoConverter.convert((ZippayPaymentInfoModel)paymentInfo));
        }
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.commercefacades.order.converters.AbstractOrderConverter#addPromotions(de.hybris.platform.core.model.order.AbstractOrderModel, de.hybris.platform.promotions.result.PromotionOrderResults, de.hybris.platform.commercefacades.order.data.AbstractOrderData)
     */
    @Override
    protected void addPromotions(final AbstractOrderModel source, final AbstractOrderData prototype) {
        super.addPromotions(source, prototype);

        AbstractOrderHelper.addTargetProductPromotions(
                getPromotionsService().getPromotionResults(source),
                getModelService(), getPromotionResultConverter(), prototype);
        AbstractOrderHelper.addTargetOrderPromotions(
                getPromotionsService().getPromotionResults(source),
                getModelService(), getPromotionResultConverter(), prototype);
    }

    /**
     * @param source
     * @param targetOrderData
     */
    private void setCancelledReturnsProducts(final OrderModel source, final TargetOrderData targetOrderData) {
        if (CollectionUtils.isEmpty(source.getModificationRecords())) {
            return;
        }
        final List<OrderModificationData> cancelledProducts = new ArrayList<>();
        final List<OrderModificationData> returnProducts = new ArrayList<>();

        for (final Iterator iterator = source.getModificationRecords().iterator(); iterator.hasNext();) {
            final Object modificationRecord = iterator.next();
            if (modificationRecord instanceof OrderReturnRecordModel) {
                addModification(source, returnProducts, (OrderModificationRecordModel)modificationRecord);
                continue;
            }
            if (modificationRecord instanceof OrderCancelRecordModel) {
                addModification(source, cancelledProducts, (OrderModificationRecordModel)modificationRecord);
                continue;
            }
        }
        targetOrderData.setCancelledProducts(cancelledProducts);
        targetOrderData.setReturnAndRefundProducts(returnProducts);
    }

    /**
     * Method will populate initial deposit amount and total outstanding amount for the pre-order product.
     *
     * @param source
     * @param targetOrderData
     */
    private void setPreOrderInfo(final OrderModel source, final TargetOrderData targetOrderData) {
        final BigDecimal totalPrice = BigDecimal
                .valueOf(source.getTotalPrice().doubleValue());
        final BigDecimal preOrderDepositAmount = BigDecimal
                .valueOf(source.getPreOrderDepositAmount().doubleValue());
        final BigDecimal preOrderOutstandingAmount = totalPrice.subtract(preOrderDepositAmount);
        targetOrderData.setPreOrderDepositAmount(
                abstractOrderHelper.createPrice(source, preOrderDepositAmount));
        targetOrderData.setPreOrderOutstandingAmount(
                abstractOrderHelper.createPrice(source, preOrderOutstandingAmount));

        setOutstandingAmountTransactionPaymentInfo(source, targetOrderData);
    }

    /**
     * @param source
     * @param modificationsList
     * @param modificationRecord
     */
    private void addModification(final OrderModel source, final List<OrderModificationData> modificationsList,
            final OrderModificationRecordModel modificationRecord) {
        if (CollectionUtils.isEmpty(modificationRecord.getModificationRecordEntries())) {
            return;
        }
        for (final OrderModificationRecordEntryModel orderModificationRecordEntryModel : modificationRecord
                .getModificationRecordEntries()) {
            for (final OrderEntryModificationRecordEntryModel orderEntryModificationRecordEntryModel : orderModificationRecordEntryModel
                    .getOrderEntriesModificationEntries()) {
                if (orderEntryModificationRecordEntryModel.getOrderEntry() == null) {
                    LOG.warn("Can't load order entry for order modifications, order number : " + source.getCode());
                    continue;
                }

                final OrderModificationData orderModificationData = getOrderEntryModificationRecordEntryConverter()
                        .convert(orderEntryModificationRecordEntryModel);
                orderModificationData.setStatus(OrderModificationStatusEnum.getStatusType(modificationRecord));
                modificationsList.add(orderModificationData);
            }
        }
    }

    /**
     * @param source
     * @param targetOrderData
     */
    protected void setTransactionPaymentInfo(final OrderModel source, final TargetOrderData targetOrderData) {
        final PaymentTransactionModel paymentTransactionModel = PaymentTransactionHelper.findCaptureTransaction(source);
        if (paymentTransactionModel == null || CollectionUtils.isEmpty(paymentTransactionModel.getEntries())) {
            return;
        }

        for (final PaymentTransactionEntryModel paymentTransactionEntryModel : paymentTransactionModel.getEntries()) {
            if (PaymentTransactionType.CAPTURE.equals(paymentTransactionEntryModel.getType())
                    && TransactionStatus.ACCEPTED.toString().equalsIgnoreCase(
                            paymentTransactionEntryModel.getTransactionStatus())) {

                targetOrderData.setReceiptNumber(paymentTransactionEntryModel.getReceiptNo());
                targetOrderData.setTransactionStatus(TransactionStatus.ACCEPTED);

                CCPaymentInfoData paymentInfoData = null;
                final PaymentInfoModel ipgPaymentInfo = paymentTransactionEntryModel.getIpgPaymentInfo();

                if (ipgPaymentInfo != null) {
                    if (ipgPaymentInfo instanceof CreditCardPaymentInfoModel) {
                        paymentInfoData = getCreditCardPaymentInfoConverter().convert(
                                (CreditCardPaymentInfoModel)ipgPaymentInfo);

                    }
                    else if (ipgPaymentInfo instanceof IpgGiftCardPaymentInfoModel) {
                        paymentInfoData = getTargetIpgGiftCardPaymentInfoConverter().convert(
                                (IpgGiftCardPaymentInfoModel)ipgPaymentInfo);
                    }

                    final CCPaymentInfoData ipgPaymentInfoData = targetOrderData.getPaymentInfo();
                    if (paymentInfoData != null && ipgPaymentInfoData != null) {
                        paymentInfoData.setBillingAddress(ipgPaymentInfoData.getBillingAddress());
                    }
                }
                else {
                    paymentInfoData = targetOrderData.getPaymentInfo();
                }

                if (paymentInfoData != null && paymentInfoData instanceof TargetCCPaymentInfoData) {
                    final PriceData amount = getPriceDataFactory().create(PriceDataType.BUY,
                            paymentTransactionEntryModel.getAmount(), source.getCurrency().getIsocode());

                    ((TargetCCPaymentInfoData)paymentInfoData).setAmount(amount);
                    ((TargetCCPaymentInfoData)paymentInfoData).setReceiptNumber(paymentTransactionEntryModel
                            .getReceiptNo());
                }
                targetOrderData.addPaymentInfo(paymentInfoData);
            }
        }
    }

    private void setOutstandingAmountTransactionPaymentInfo(final OrderModel source,
            final TargetOrderData targetOrderData) {

        final PaymentTransactionEntryModel paymentTxnEntryModel = PaymentTransactionHelper
                .findTransactionEntryWithState(source.getPaymentTransactions(), PaymentTransactionType.DEFERRED);

        if (paymentTxnEntryModel == null) {
            // means update card never happened for preOrder product, hence copy the existing payment info
            for (final CCPaymentInfoData paymentInfoData : targetOrderData.getPaymentsInfo()) {
                final TargetCCPaymentInfoData copiedPaymentInfo = new TargetCCPaymentInfoData();
                copiedPaymentInfo.setCardNumber(paymentInfoData.getCardNumber());
                copiedPaymentInfo.setExpiryMonth(paymentInfoData.getExpiryMonth());
                copiedPaymentInfo.setExpiryYear(paymentInfoData.getExpiryYear());
                copiedPaymentInfo.setCardTypeData(paymentInfoData.getCardTypeData());
                targetOrderData.addOutstandingAmountPaymentInfo(copiedPaymentInfo);
            }

        }
        else {
            CCPaymentInfoData outstandingAmountPaymentInfoData = null;
            final PaymentInfoModel ipgPaymentInfo = paymentTxnEntryModel.getIpgPaymentInfo();

            if (ipgPaymentInfo != null && ipgPaymentInfo instanceof CreditCardPaymentInfoModel) {
                outstandingAmountPaymentInfoData = getCreditCardPaymentInfoConverter().convert(
                        (CreditCardPaymentInfoModel)ipgPaymentInfo);

            }

            if (outstandingAmountPaymentInfoData != null
                    && outstandingAmountPaymentInfoData instanceof TargetCCPaymentInfoData) {
                final PriceData amount = getPriceDataFactory().create(PriceDataType.BUY,
                        paymentTxnEntryModel.getAmount(), source.getCurrency().getIsocode());

                ((TargetCCPaymentInfoData)outstandingAmountPaymentInfoData).setAmount(amount);
                ((TargetCCPaymentInfoData)outstandingAmountPaymentInfoData).setReceiptNumber(paymentTxnEntryModel
                        .getReceiptNo());
            }

            targetOrderData.addOutstandingAmountPaymentInfo(outstandingAmountPaymentInfoData);
        }

    }

    protected void setAppliedVoucherCodes(final OrderModel source, final TargetOrderData targetOrderData) {
        final Collection<String> vouchers = voucherService.getAppliedVoucherCodes(source);
        if (CollectionUtils.isNotEmpty(vouchers)) {
            targetOrderData.setAppliedVoucherCodes(new ArrayList<String>(vouchers));
        }

    }

    /**
     * @param targetOrderData
     * @param source
     *
     */
    private void setLayByInfo(final OrderModel source, final TargetOrderData targetOrderData) {
        if (!abstractOrderHelper.isLayBy(source)) {
            targetOrderData.setLayBy(false);
            return;
        }
        targetOrderData.setLayBy(true);
        final double dOutstandingAmt = orderOutstandingStrategy.calculateAmt(source);
        targetOrderData.setOutstandingAmount(abstractOrderHelper.createPrice(source, dOutstandingAmt));

        targetOrderData.setLayByFee(createPrice(source, source.getLayByFee()));


        targetOrderData.setDueDates(abstractOrderHelper.createAmountDueDates(targetLaybyPaymentDueService
                .getAllPaymentDuesForAbstractOrder(source)));
        targetOrderData.setFullPaymentDueDate(AbstractOrderHelper.DUE_DATE_FORMATTER
                .format(targetLaybyPaymentDueService.getFinalPaymentDue(source).getDueDate()));

        final BigDecimal totalPrice = BigDecimal.valueOf(source.getTotalPrice().doubleValue());
        final BigDecimal outstandingAmount = BigDecimal.valueOf(dOutstandingAmt);
        if (!de.hybris.platform.core.enums.OrderStatus.CANCELLED.equals(source.getStatus())) {
            final BigDecimal layByAmountPaid = totalPrice.subtract(outstandingAmount);
            targetOrderData.setLayByAmountPaid(abstractOrderHelper.createPrice(source, layByAmountPaid));
        }

        targetOrderData.setInitialLayByPaymentAmount(createPrice(source, source.getAmountCurrentPayment()));
        targetOrderData
                .setAmountDueToday(abstractOrderHelper.createPrice(source,
                        targetLaybyPaymentDueService.getInitialPaymentDue(source).getAmount()));

        setLaybyTransactions(source, targetOrderData);
    }

    /**
     * @param source
     * @param targetOrderData
     */
    private void setLaybyTransactions(final OrderModel source, final TargetOrderData targetOrderData) {
        if (CollectionUtils.isEmpty(source.getPaymentTransactions())) {
            targetOrderData.setLaybyTransactions(Collections.EMPTY_LIST);
            return;
        }
        final List<LaybyTransactionData> transactions = new ArrayList<>();
        LaybyTransactionData laybyTransactionData;
        for (final PaymentTransactionModel paymentTransactionModel : source.getPaymentTransactions()) {
            if (CollectionUtils.isEmpty(paymentTransactionModel.getEntries())) {
                continue;
            }
            for (final PaymentTransactionEntryModel paymentTransactionEntryModel : paymentTransactionModel
                    .getEntries()) {
                if (TransactionStatus.ACCEPTED.toString().equalsIgnoreCase(
                        paymentTransactionEntryModel.getTransactionStatus())) {
                    laybyTransactionData = new LaybyTransactionData();

                    setPaymentType(paymentTransactionModel, laybyTransactionData);
                    laybyTransactionData.setPaymentDate(paymentTransactionEntryModel.getCreationtime());
                    laybyTransactionData.setReceiptNumber(paymentTransactionEntryModel.getReceiptNo());
                    laybyTransactionData.setStatus(TransactionStatus.ACCEPTED.name());
                    laybyTransactionData.setTotal(abstractOrderHelper.createPrice(source,
                            paymentTransactionEntryModel.getAmount()));
                    transactions.add(laybyTransactionData);
                    break;
                }
            }
        }
        targetOrderData.setLaybyTransactions(transactions);
    }

    /**
     * set payment type form a payment info model
     *
     * @param paymentTransactionModel
     * @param laybyTransactionData
     */
    private void setPaymentType(final PaymentTransactionModel paymentTransactionModel,
            final LaybyTransactionData laybyTransactionData) {
        final PaymentInfoModel info = paymentTransactionModel.getInfo();

        if (info == null) {
            return;
        }
        if (info instanceof CreditCardPaymentInfoModel) {
            final CreditCardPaymentInfoModel creditCardPaymentInfo = (CreditCardPaymentInfoModel)info;
            final CreditCardType creditCardType = creditCardPaymentInfo.getType();

            laybyTransactionData.setPaymentType(getTypeService().getEnumerationValue(creditCardType).getName());
        }
        else if (info instanceof PaypalPaymentInfoModel) {
            laybyTransactionData.setPaymentType(PAYPAL_PAYMENTMETHOD); // This is just for view purpose, it's not hard coded
        }
        else {
            laybyTransactionData.setPaymentType(info.getCode());
        }
    }

    /**
     * @param abstractOrderHelper
     *            the abstractOrderHelper to set
     */
    @Required
    public void setAbstractOrderHelper(final AbstractOrderHelper abstractOrderHelper) {
        this.abstractOrderHelper = abstractOrderHelper;
    }

    /**
     *
     * @param targetLaybyPaymentDueService
     *            the targetLaybyPaymentDueService to set
     */
    @Required
    public void setTargetLaybyPaymentDueService(final TargetLaybyPaymentDueService targetLaybyPaymentDueService) {
        this.targetLaybyPaymentDueService = targetLaybyPaymentDueService;
    }

    /**
     *
     * @param targetPayPalPaymentInfoConverter
     */
    @Required
    public void setTargetPayPalPaymentInfoConverter(
            final Converter<PaypalPaymentInfoModel, TargetCCPaymentInfoData> targetPayPalPaymentInfoConverter) {
        this.targetPayPalPaymentInfoConverter = targetPayPalPaymentInfoConverter;
    }

    /**
     * @param targetPayPalHerePaymentInfoConverter
     *            the targetPayPalHerePaymentInfoConverter to set
     */
    public void setTargetPayPalHerePaymentInfoConverter(
            final Converter<PaypalHerePaymentInfoModel, TargetCCPaymentInfoData> targetPayPalHerePaymentInfoConverter) {
        this.targetPayPalHerePaymentInfoConverter = targetPayPalHerePaymentInfoConverter;
    }

    /**
     * @return the orderEntryModificationRecordEntryConverter
     */
    protected OrderEntryModificationRecordEntryConverter getOrderEntryModificationRecordEntryConverter() {
        return orderEntryModificationRecordEntryConverter;
    }

    /**
     * @param orderEntryModificationRecordEntryConverter
     *            the orderEntryModificationRecordEntryConverter to set
     */
    @Required
    public void setOrderEntryModificationRecordEntryConverter(
            final OrderEntryModificationRecordEntryConverter orderEntryModificationRecordEntryConverter) {
        this.orderEntryModificationRecordEntryConverter = orderEntryModificationRecordEntryConverter;
    }

    /**
     * @param targetBarCodeService
     *            the targetBarCodeService to set
     */
    @Required
    public void setTargetBarCodeService(final TargetBarCodeService targetBarCodeService) {
        this.targetBarCodeService = targetBarCodeService;
    }


    /**
     * @return the typeService
     */
    @Override
    protected TypeService getTypeService() {
        return typeService;
    }

    /**
     * @param typeService
     *            the typeService to set
     */
    @Override
    @Required
    public void setTypeService(final TypeService typeService) {
        this.typeService = typeService;
    }

    /**
     * @param orderOutstandingStrategy
     *            the orderOutstandingStrategy to set
     */
    @Required
    public void setOrderOutstandingStrategy(final OrderOutstandingStrategy orderOutstandingStrategy) {
        this.orderOutstandingStrategy = orderOutstandingStrategy;
    }

    /**
     * @return the targetPinPadPaymentInfoConverter
     */
    protected Converter<PinPadPaymentInfoModel, TargetCCPaymentInfoData> getTargetPinPadPaymentInfoConverter() {
        return targetPinPadPaymentInfoConverter;
    }

    /**
     * @return the targetIpgPaymentInfoConverter
     */
    protected Converter<IpgPaymentInfoModel, TargetCCPaymentInfoData> getTargetIpgPaymentInfoConverter() {
        return targetIpgPaymentInfoConverter;
    }

    /**
     * @param targetPinPadPaymentInfoConverter
     *            the targetPinPadPaymentInfoConverter to set
     */
    @Required
    public void setTargetPinPadPaymentInfoConverter(
            final Converter<PinPadPaymentInfoModel, TargetCCPaymentInfoData> targetPinPadPaymentInfoConverter) {
        this.targetPinPadPaymentInfoConverter = targetPinPadPaymentInfoConverter;
    }

    /**
     * @param targetIpgPaymentInfoConverter
     *            the targetIpgPaymentInfoConverter to set
     */
    @Required
    public void setTargetIpgPaymentInfoConverter(
            final Converter<IpgPaymentInfoModel, TargetCCPaymentInfoData> targetIpgPaymentInfoConverter) {
        this.targetIpgPaymentInfoConverter = targetIpgPaymentInfoConverter;
    }

    /**
     * @param targetAfterpayPaymentInfoConverter
     *            the targetAfterpayPaymentInfoConverter to set
     */
    @Required
    public void setTargetAfterpayPaymentInfoConverter(
            final Converter<AfterpayPaymentInfoModel, TargetCCPaymentInfoData> targetAfterpayPaymentInfoConverter) {
        this.targetAfterpayPaymentInfoConverter = targetAfterpayPaymentInfoConverter;
    }

    /**
     * @param targetZipPaymentInfoConverter
     *            the targetZipPaymentInfoConverter to set
     */
    public void setTargetZipPaymentInfoConverter(
            final Converter<ZippayPaymentInfoModel, TargetCCPaymentInfoData> targetZipPaymentInfoConverter) {
        this.targetZipPaymentInfoConverter = targetZipPaymentInfoConverter;
    }

    /**
     * @param targetFlybuysDiscountConverter
     *            the targetFlybuysDiscountConverter to set
     */
    @Required
    public void setTargetFlybuysDiscountConverter(final TargetFlybuysDiscountConverter targetFlybuysDiscountConverter) {
        this.targetFlybuysDiscountConverter = targetFlybuysDiscountConverter;
    }

    /**
     * @param voucherService
     *            the voucherService to set
     */
    @Required
    public void setVoucherService(final VoucherService voucherService) {
        this.voucherService = voucherService;
    }

    /**
     * @param affiliateOrderDataConverter
     *            the affiliateOrderDataConverter to set
     */
    @Required
    public void setAffiliateOrderDataConverter(final AffiliateOrderDataConverter affiliateOrderDataConverter) {
        this.affiliateOrderDataConverter = affiliateOrderDataConverter;
    }

    /**
     * @param invoiceFormatterHelper
     *            the invoiceFormatterHelper to set
     */
    @Required
    public void setInvoiceFormatterHelper(final InvoiceFormatterHelper invoiceFormatterHelper) {
        this.invoiceFormatterHelper = invoiceFormatterHelper;
    }

    /**
     * @return the targetIpgGiftCardPaymentInfoConverter
     */
    public Converter<IpgGiftCardPaymentInfoModel, TargetCCPaymentInfoData> getTargetIpgGiftCardPaymentInfoConverter() {
        return targetIpgGiftCardPaymentInfoConverter;
    }

    /**
     * @param targetIpgGiftCardPaymentInfoConverter
     *            the targetIpgGiftCardPaymentInfoConverter to set
     */
    @Required
    public void setTargetIpgGiftCardPaymentInfoConverter(
            final Converter<IpgGiftCardPaymentInfoModel, TargetCCPaymentInfoData> targetIpgGiftCardPaymentInfoConverter) {
        this.targetIpgGiftCardPaymentInfoConverter = targetIpgGiftCardPaymentInfoConverter;
    }
}