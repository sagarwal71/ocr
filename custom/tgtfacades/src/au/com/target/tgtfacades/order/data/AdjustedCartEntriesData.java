/**
 * 
 */
package au.com.target.tgtfacades.order.data;

import java.util.ArrayList;
import java.util.List;


/**
 * @author gbaker2
 * 
 */
public class AdjustedCartEntriesData {

    private List<AdjustedEntryQuantityData> outOfStockItems;
    private List<AdjustedEntryQuantityData> adjustedItems;
    private String purchaseType;

    /**
     * 
     */
    public AdjustedCartEntriesData() {
        //Nothing to do
    }

    /**
     * @param purchaseType
     */
    public AdjustedCartEntriesData(final String purchaseType) {
        super();
        this.purchaseType = purchaseType;
        this.outOfStockItems = new ArrayList<>();
        this.adjustedItems = new ArrayList<>();
    }

    public boolean isAdjusted() {
        return (outOfStockItems.size() + adjustedItems.size()) > 0;
    }

    /**
     * @return the outOfStockItems
     */
    public List<AdjustedEntryQuantityData> getOutOfStockItems() {
        return outOfStockItems;
    }

    /**
     * @param outOfStockItems
     *            the outOfStockItems to set
     */
    public void setOutOfStockItems(final List<AdjustedEntryQuantityData> outOfStockItems) {
        this.outOfStockItems = outOfStockItems;
    }

    /**
     * @return the adjustedItems
     */
    public List<AdjustedEntryQuantityData> getAdjustedItems() {
        return adjustedItems;
    }

    /**
     * @param adjustedItems
     *            the adjustedItems to set
     */
    public void setAdjustedItems(final List<AdjustedEntryQuantityData> adjustedItems) {
        this.adjustedItems = adjustedItems;
    }

    /**
     * @return the purchaseType
     */
    public String getPurchaseType() {
        return purchaseType;
    }

    /**
     * @param purchaseType
     *            the purchaseType to set
     */
    public void setPurchaseType(final String purchaseType) {
        this.purchaseType = purchaseType;
    }

}
