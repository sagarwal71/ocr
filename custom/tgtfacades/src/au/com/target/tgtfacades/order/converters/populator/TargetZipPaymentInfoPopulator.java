/**
 *
 */
package au.com.target.tgtfacades.order.converters.populator;

import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.user.data.ZipPaymentInfoData;
import au.com.target.tgtpayment.model.ZippayPaymentInfoModel;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * Converter to convert ZippayPaymentInfoModel to TargetCCPaymentInfoData. AbstractOrderData contains the type
 * CCPaymentInfoData for payment info therefore this class will convert to a TargetCCPaymentInfoData which already
 * extends CCPaymentInfoData.
 *
 * @author ramsatish
 *
 */
public class TargetZipPaymentInfoPopulator implements
        Populator<ZippayPaymentInfoModel, TargetCCPaymentInfoData> {

    @Override
    public void populate(final ZippayPaymentInfoModel source, final TargetCCPaymentInfoData target)
            throws ConversionException {
        target.setId(source.getPk().toString());
        target.setZipPaymentInfo(true);
        target.setZipPaymentInfoData(new ZipPaymentInfoData());
    }
}
