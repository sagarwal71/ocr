/**
 * 
 */
package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.platform.commercefacades.order.converters.populator.CartPopulator;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;

import java.math.BigDecimal;

import javax.annotation.Resource;

import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.util.TargetPriceHelper;


/**
 * @author siddharam
 *
 */
public class TargetExtendedCartPopulator extends CartPopulator {
    @Resource(name = "targetPriceHelper")
    private TargetPriceHelper targetPriceHelper;

    @Override
    public void populate(final CartModel source, final CartData target) {
        super.populate(source, target);
        addDeliveryAddress(source, target);
        addPaymentInformation(source, target);
        addDeliveryMethod(source, target);
    }

    @Override
    protected void addTotals(final AbstractOrderModel source, final AbstractOrderData prototype) {
        prototype.setTotalPrice(createPrice(source, source.getTotalPrice()));
        prototype.setTotalTax(createPrice(source, source.getTotalTax()));
        prototype.setSubTotal(createPrice(source, source.getSubtotal()));
        prototype.setDeliveryCost(
                source.getDeliveryMode() != null && source.getZoneDeliveryModeValue() != null
                        ? createPrice(source, source.getDeliveryCost()) : null);
        prototype.setTotalPriceWithTax((createPrice(source, calcTotalWithTax(source))));
        prototype.setInstallmentPrice(targetPriceHelper.calculateAfterpayInstallmentPrice(prototype.getTotalPrice()));
    }

    @Override
    protected void addEntries(final AbstractOrderModel source, final AbstractOrderData prototype) {
        super.addEntries(source, prototype);
        boolean excludeForAfterpay = false;
        boolean excludeForZipPayment = false;
        for (final OrderEntryData orderEntryData : prototype.getEntries()) {
            if (orderEntryData.getProduct() instanceof TargetProductData) {
                excludeForAfterpay |= ((TargetProductData)orderEntryData.getProduct()).isExcludeForAfterpay();
                excludeForZipPayment |= ((TargetProductData)orderEntryData.getProduct()).isExcludeForZipPayment();
            }
            if (excludeForAfterpay && excludeForZipPayment) {
                break;
            }
        }
        prototype.setExcludeForAfterpay(excludeForAfterpay);
        prototype.setExcludeForZipPayment(excludeForZipPayment);
    }

    protected PriceData createPrice(final AbstractOrderModel source, final BigDecimal val) {
        if (source == null) {
            throw new IllegalArgumentException("source order must not be null");
        }

        final CurrencyModel currency = source.getCurrency();
        if (currency == null) {
            throw new IllegalArgumentException("source order currency must not be null");
        }
        return getPriceDataFactory().create(PriceDataType.BUY, val != null ? val : BigDecimal.valueOf(0), currency);
    }
}
