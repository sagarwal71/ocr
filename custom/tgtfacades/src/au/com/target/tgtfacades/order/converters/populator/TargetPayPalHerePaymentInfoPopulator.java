/**
 *
 */
package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.user.data.PayPalHereInfoData;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtpayment.model.PaypalHerePaymentInfoModel;


/**
 * Converter to convert PinPadPaymentInfoModel to TargetCCPaymentInfoData. AbstractOrderData contains the type
 * CCPaymentInfoData for payment info therefore this class will convert to a TargetCCPaymentInfoData which already
 * extends CCPaymentInfoData.
 *
 * @author jjayawa1
 *
 */
public class TargetPayPalHerePaymentInfoPopulator implements
        Populator<PaypalHerePaymentInfoModel, TargetCCPaymentInfoData> {

    private Converter<AddressModel, TargetAddressData> addressConverter;


    @Override
    public void populate(final PaypalHerePaymentInfoModel source, final TargetCCPaymentInfoData target)
    {
        target.setId(source.getPk().toString());
        target.setPayPalHerePaymentInfo(true);
        final PayPalHereInfoData payPalHereInfoData = new PayPalHereInfoData();
        target.setPayPalHereInfoData(payPalHereInfoData);
        if (source.getBillingAddress() != null) {
            target.setBillingAddress(addressConverter.convert(source.getBillingAddress()));
        }
    }

    /**
     * @param addressConverter
     *            the addressConverter to set
     */
    @Required
    public void setAddressConverter(final Converter<AddressModel, TargetAddressData> addressConverter) {
        this.addressConverter = addressConverter;
    }
}
