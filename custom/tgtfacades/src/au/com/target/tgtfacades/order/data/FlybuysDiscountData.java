/**
 * 
 */
package au.com.target.tgtfacades.order.data;

import de.hybris.platform.commercefacades.product.data.PriceData;




/**
 * @author knemalik
 * 
 */
public class FlybuysDiscountData {

    private String flybuysCardNumber;
    private PriceData value;
    private Integer pointsConsumed;


    /**
     * @return the flybuysCardNumber
     */
    public String getFlybuysCardNumber() {
        return flybuysCardNumber;
    }

    /**
     * @return the value
     */
    public PriceData getValue() {
        return value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(final PriceData value) {
        this.value = value;
    }

    /**
     * @param flybuysCardNumber
     *            the flybuysCardNumber to set
     */
    public void setFlybuysCardNumber(final String flybuysCardNumber) {
        this.flybuysCardNumber = flybuysCardNumber;
    }


    /**
     * @return the pointsConsumed
     */
    public Integer getPointsConsumed() {
        return pointsConsumed;
    }

    /**
     * @param pointsConsumed
     *            the pointsConsumed to set
     */
    public void setPointsConsumed(final Integer pointsConsumed) {
        this.pointsConsumed = pointsConsumed;
    }




}
