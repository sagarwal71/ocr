/**
 *
 */
package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.platform.commercefacades.order.converters.populator.OrderHistoryPopulator;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.core.model.order.OrderModel;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.order.converters.AbstractOrderHelper;
import au.com.target.tgtfacades.order.data.TargetOrderHistoryData;
import au.com.target.tgtlayby.order.strategies.OrderOutstandingStrategy;


/**
 * @author Benoit Vanalderweireldt
 *
 */
public class TargetOrderHistoryPopulator extends OrderHistoryPopulator {

    private AbstractOrderHelper abstractOrderHelper;

    private OrderOutstandingStrategy orderOutstandingStrategy;

    @Override
    public void populate(final OrderModel source, final OrderHistoryData target) {
        super.populate(source, target);
        if (target instanceof TargetOrderHistoryData) {
            final TargetOrderHistoryData targetOrderHistoryData = (TargetOrderHistoryData)target;
            final Double totalPrice = source.getTotalPrice();
            targetOrderHistoryData.setTotal(abstractOrderHelper.createPrice(source,
                    totalPrice == null ? 0.0 : totalPrice.doubleValue()));

            if (BooleanUtils.isTrue(source.getPurchaseOptionConfig().getAllowPaymentDues())) {
                targetOrderHistoryData.setLayBy(true);
                targetOrderHistoryData.setOutstandingAmount(abstractOrderHelper.createPrice(source,
                        orderOutstandingStrategy.calculateAmt(source)));
            }
            else {
                targetOrderHistoryData.setLayBy(false);
            }
        }
    }

    /**
     * @param abstractOrderHelper
     *            the abstractOrderHelper to set
     */
    @Required
    public void setAbstractOrderHelper(final AbstractOrderHelper abstractOrderHelper) {
        this.abstractOrderHelper = abstractOrderHelper;
    }

    /**
     * @param orderOutstandingStrategy
     *            the orderOutstandingStrategy to set
     */
    @Required
    public void setOrderOutstandingStrategy(final OrderOutstandingStrategy orderOutstandingStrategy) {
        this.orderOutstandingStrategy = orderOutstandingStrategy;
    }
}
