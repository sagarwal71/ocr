/**
 *
 */
package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.user.data.AfterpayPaymentInfoData;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;


/**
 * Converter to convert AfterpayPaymentInfoModel to TargetCCPaymentInfoData. AbstractOrderData contains the type
 * CCPaymentInfoData for payment info therefore this class will convert to a TargetCCPaymentInfoData which already
 * extends CCPaymentInfoData.
 *
 * @author mgazal
 *
 */
public class TargetAfterpayPaymentInfoPopulator implements
        Populator<AfterpayPaymentInfoModel, TargetCCPaymentInfoData> {

    @Override
    public void populate(final AfterpayPaymentInfoModel source, final TargetCCPaymentInfoData target)
            throws ConversionException {
        target.setId(source.getPk().toString());
        target.setAfterpayPaymentInfo(true);
        target.setAfterpayPaymentInfoData(new AfterpayPaymentInfoData());
    }
}
