/**
 * 
 */
package au.com.target.tgtfacades.order.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import au.com.target.tgtfacades.cart.data.TargetCheckoutCartData;
import au.com.target.tgtfacades.user.data.TargetCustomerInfoData;


/**
 * DTO object to hold global session data.
 * 
 * @author jjayawa1
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)

public class SessionData {
    private Boolean guest;
    private String csrfToken;
    private TargetCustomerInfoData customer;
    private TargetCheckoutCartData cart;
    private boolean mobileApp;
    private boolean checkoutRegisteredAsGuest;

    /**
     * @return the guest
     */
    public Boolean isGuest() {
        return guest;
    }

    /**
     * @param guest
     *            the guest to set
     */
    public void setGuest(final Boolean guest) {
        this.guest = guest;
    }

    /**
     * @return the csrfToken
     */
    public String getCsrfToken() {
        return csrfToken;
    }

    /**
     * @param csrfToken
     *            the csrfToken to set
     */
    public void setCsrfToken(final String csrfToken) {
        this.csrfToken = csrfToken;
    }

    /**
     * @return the customer
     */
    public TargetCustomerInfoData getCustomer() {
        return customer;
    }

    /**
     * @param customer
     *            the customer to set
     */
    public void setCustomer(final TargetCustomerInfoData customer) {
        this.customer = customer;
    }

    /**
     * @return the cart
     */
    public TargetCheckoutCartData getCart() {
        return cart;
    }

    /**
     * @param cart
     *            the cart to set
     */
    public void setCart(final TargetCheckoutCartData cart) {
        this.cart = cart;
    }

    /**
     * @return the mobileApp
     */
    public boolean isMobileApp() {
        return mobileApp;
    }

    /**
     * @param mobileApp
     *            the mobileApp to set
     */
    public void setMobileApp(final boolean mobileApp) {
        this.mobileApp = mobileApp;
    }

    /**
     * @return the checkoutRegisteredAsGuest
     */
    public boolean isCheckoutRegisteredAsGuest() {
        return checkoutRegisteredAsGuest;
    }

    /**
     * @param checkoutRegisteredAsGuest
     *            the checkoutRegisteredAsGuest to set
     */
    public void setCheckoutRegisteredAsGuest(final boolean checkoutRegisteredAsGuest) {
        this.checkoutRegisteredAsGuest = checkoutRegisteredAsGuest;
    }

}
