/**
 *
 */
package au.com.target.tgtfacades.order;

import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import java.math.BigDecimal;
import java.util.List;

import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;
import au.com.target.tgtfacades.order.data.ClickAndCollectDeliveryDetailData;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;


/**
 * @author rmcalave
 *
 */
public interface TargetCheckoutFacade extends CheckoutFacade {
    @Override
    boolean setDeliveryMode(String deliveryTypeCode);


    boolean setClickAndCollectDeliveryDetails(ClickAndCollectDeliveryDetailData cncData);

    /**
     * Save selected pickup store number to the checkout cart if it is available for pickup the order. Return null if
     * cannot find a valid store for given storeNumber.
     * 
     * @param storeNumber
     * @return TargetPointOfServiceData
     */
    TargetPointOfServiceData setClickAndCollectStore(int storeNumber);

    /**
     * Set the team member discount card number to the checkout cart
     *
     * @param cardNumber
     *            The card number to set
     * @return true if the operation completed successfully
     */
    boolean setTeamMemberDiscountCardNumber(String cardNumber);

    /**
     *
     * @return TargetCreateSubscriptionResult with TNS session token
     */
    TargetCreateSubscriptionResult getTNSSessionToken();

    /**
     * Get the PayPal session token.
     *
     * @param returnUrl
     *            The URL that PayPal should redirect to when the user clicks 'Pay Now'
     * @param cancelUrl
     *            The URL that PayPal should redirect to when the user clicks 'Cancel'
     * @return TargetCreateSubscriptionResult with the PayPal session token
     */
    TargetCreateSubscriptionResult getPayPalSessionToken(String returnUrl, String cancelUrl);

    /**
     * Get the Afterpay session token.
     *
     * @param returnUrl
     *            The URL that Afterpay should redirect to when the payment is successful
     * @param cancelUrl
     *            The URL that Afterpay should redirect to when the payment fails
     * @return TargetCreateSubscriptionResult with Afterpay session token
     */
    TargetCreateSubscriptionResult getAfterpaySessionToken(String returnUrl, String cancelUrl);

    /**
     * Get the Zippay session token.
     *
     * @param redirectUrl
     *            The URL that Afterpay should redirect to when the payment is successful
     * @return TargetCreateSubscriptionResult with Zippay session token
     */
    TargetCreateSubscriptionResult getZippaySessionToken(final String redirectUrl);

    /**
     *
     * @param creditCardType
     * @return CreditCardType
     */
    CreditCardType getCreditCardType(final String creditCardType);

    /**
     * Get the checkout cart for the active purchase option
     *
     * @return The checkout cart
     */
    CartModel getCart();

    /**
     * Get the active purchase option from the session.
     *
     * @return The active purchase option
     */
    PurchaseOptionModel getActivePurchaseOption();

    /**
     * Get the active purchase option config from the session.
     *
     * @return The active purchase option config
     */
    PurchaseOptionConfigModel getActivePurchaseOptionConfig();

    /**
     *
     * Set the pay today amount on the cart.
     *
     * @param payTodayAmount
     *            The pay today amount
     * @return true if the value was set successfully, false otherwise
     */
    boolean setAmountCurrentPayment(final String payTodayAmount);

    /**
     * validate delivery mode
     */
    void validateDeliveryMode();

    /**
     *
     * Create the PayPal payment info using the token and payerId returned from PayPal.
     *
     * @param token
     *            The token returned from PayPal
     * @param payerId
     *            The payerId returned from PayPal
     */
    void createPayPalPaymentInfo(String token, String payerId);

    /**
     * Set the flybuys number on the checkout cart.
     *
     * @param flybuysNumber
     *            The flybuys number to set
     * @return true if the flybuys number is valid and is set successfully, false otherwise
     */
    boolean setFlybuysNumber(final String flybuysNumber);


    /**
     * Copy the user's flybuys card number to the cart if they have one.
     */
    void applyUsersFlybuysNumberToCart();

    /**
     * get order user uid
     *
     * @param orderCode
     * @return user uid
     */
    String getOrderUserUid(String orderCode);


    /**
     * set Payment info
     *
     * @param paymentModeCode
     * @return true
     */
    boolean setPaymentMode(String paymentModeCode);


    /**
     * @param voucher
     * @return successfully applied to the cart
     */
    boolean applyVoucher(String voucher);

    /**
     * @return the first voucher applied to the cart
     */
    String getFirstAppliedVoucher();

    /**
     * @param voucher
     */
    void removeVoucher(String voucher);

    /**
     * @return the first voucher price applied to the cart
     */
    PriceData getAppliedVoucherPrice(String voucher);

    /**
     *
     * Create the PinPad payment info using the Billing Address.
     *
     * @param targetBillingAddressData
     *            The Billing Address required from PinPad
     */
    void createPinPadPaymentInfo(TargetAddressData targetBillingAddressData);

    /**
     * 
     * @param targetBillingAddressData
     * @param ipgPaymentTemplateType
     */
    void createIpgPaymentInfo(TargetAddressData targetBillingAddressData,
            IpgPaymentTemplateType ipgPaymentTemplateType);

    /**
     * @param kioskStoreNumber
     * @return true if the kiosk store number is set successfully on the cart, false otherwise
     */
    boolean setKioskStoreNumber(String kioskStoreNumber);

    /**
     * @param storeEmployeeUid
     * @return true if the employee and store number is set successfully on the cart, false otherwise
     */
    boolean setStoreAssisted(String storeEmployeeUid);

    /**
     * For this delivery mode find the first currently active value valid for this product
     *
     * @param targetDeliveryModeModel
     * @param productModel
     * @return the bulky price data
     */
    PriceData getFirstPrice(TargetZoneDeliveryModeModel targetDeliveryModeModel, ProductModel productModel);

    /**
     * For this delivery mode find the last currently active value and return the minimum spend price
     *
     * @param targetDeliveryModeModel
     * @param productModel
     * @return the threshold price data
     */
    PriceData getLastThreshold(TargetZoneDeliveryModeModel targetDeliveryModeModel, ProductModel productModel);

    /**
     * Find out if the specified delivery mode can be used for this cart given the entries in the cart.
     *
     * @param cartModel
     * @param deliveryModeModel
     *            the delivery mode that is we want to know if it is allowed to be used for this cart
     * @return true if the delivery mode can be used for all entries in the cart
     */
    boolean hasDeliveryOptionConflicts(final CartModel cartModel,
            DeliveryModeModel deliveryModeModel);

    /**
     * Method to convert cartModel into cartData
     *
     * @param cartModel
     *            to convert
     * @return cartData object
     */
    CartData getCheckoutCart(CartModel cartModel);


    /**
     * Get the supported delivery addresses based on the chosen delivery mode.
     *
     * @param deliveryModeCode
     * @param visibleAddressesOnly
     * @return the collection of supported delivery addresses
     */
    List<AddressData> getSupportedDeliveryAddresses(String deliveryModeCode, boolean visibleAddressesOnly);

    /**
     * Generate the sessionID for ThreatMatix, it comes from the cartModel code.
     *
     * @return string MD5 encoding the cartModel code value.
     */
    String getThreatMatrixSessionID();

    /**
     * persist the threatMatrixSessionId into the cartModel
     *
     * @param cartModel
     */
    void setThreatMatrixSessionID(CartModel cartModel);

    /**
     * Checks if flybuys discount already applied.
     *
     * @param excludeFlybuysNumber
     *            the flybuys number that is excluded from the test
     * @return true, if flybuys already applied and false, if same flybuys is applied again or no flybuys is applied.
     */
    boolean isFlybuysDiscountAlreadyApplied(final String excludeFlybuysNumber);

    /**
     * recalculate the current cart
     *
     */
    void recalculateCart();

    /**
     * Remove the payment info from the cart.
     *
     */
    void removePaymentInfo();

    /**
     * Remove the selectedCncStore from the cart.
     *
     */
    void removeSelectedCncStore();

    /**
     * Method to update the cart with selected delivery mode
     * 
     * @param selectedDeliveryMode
     */
    void updateDeliveryMode(String selectedDeliveryMode);

    /**
     * Method to remove delivery mode and delivery address from cart.
     */
    void removeDeliveryInfo();

    /**
     * Returns IPG session token
     * 
     * @param returnUrl
     * 
     */
    TargetCreateSubscriptionResult getIpgSessionToken(String returnUrl);

    /**
     * Returns IPG base URL
     * 
     * @return base URL
     */
    String getIpgBaseUrl();


    /**
     * update current ipg payment info with a new token
     * 
     * @return true if token is set to the payment info successfully, false if the payment info is not ipg or cart is
     *         null
     */
    boolean updateIpgPaymentInfoWithToken(String token);

    /**
     * Checks if delivery fee changed after change address.
     *
     * @param deliveryFee
     *            the delivery fee
     * @return true, if delivery fee changed after change address
     */
    boolean isDeliveryFeeVariesAfterAddressChange(BigDecimal deliveryFee);

    /**
     * Method to create IPG's IFrame URL.
     * 
     * @param sessionId
     * @param token
     * @return url
     */
    String createIpgUrl(final String sessionId, final String token);

    /**
     * Method to check if gift card payment not allowed for a cart.
     * 
     * @return true, if cart doesn't contain gift card in it.
     */
    boolean isGiftCardPaymentAllowedForCart();

    /**
     * Method to check if cart has gift card in it
     * 
     * @return true, if cart contain gift card in it.
     */
    boolean isGiftCardInCart();

    /**
     * Method to check whether cart has gift cards only
     * 
     * @return true, if cart has gift cards only
     */
    boolean doesCartHaveGiftCardOnly();

    /**
     * Method to check if payment mode is of type gift card in cart
     * 
     * @return true, if payment mode is gift card.
     */
    boolean isPaymentModeGiftCard();

    /**
     * Method to check if payment mode is of type paypal in cart
     * 
     * @return true, if payment mode is paypal.
     */
    boolean isPaymentModePaypal();

    /**
     * Method to check if payment mode is of type Afterpay in cart
     * 
     * @return true, if payment mode is Afterpay.
     */
    boolean isPaymentModeAfterpay();

    /**
     * Get IPG session token associated with given cart
     * 
     * @param cartData
     *            - cart
     * @return session token
     */
    String getIpgSessionTokenFromCart(CartData cartData);

    /**
     * Checks whether the given delivery mode is supported by the cart based on the product mix. If there are no
     * products a delivery mode cannot be set.
     * 
     * @param deliveryModeCode
     * @return true, if delivery mode is supported for a cart.
     */
    boolean isSupportedDeliveryMode(final String deliveryModeCode);

    /**
     * Get delivery modes applicable to cart
     * 
     * @return list of delivery modes
     */
    List<TargetZoneDeliveryModeData> getDeliveryModes();

    /**
     * Get delivery mode with range info for cart.
     * 
     * @param deliveryData
     * @return TargetZoneDeliveryModeData
     */
    TargetZoneDeliveryModeData getDeliveryModeWithRangeInfoForCart(TargetZoneDeliveryModeData deliveryData);

    AdjustedCartEntriesData adjustCart();

    boolean hasIncompleteDeliveryDetails();

    boolean hasIncompleteBillingAddress();

    /**
     * Creates billing address (associated with cart)
     * 
     * @param newAddressData
     */
    void createBillingAddress(TargetAddressData newAddressData);

    /**
     * Determines whether guest checkout is allowed for the current cart.
     * 
     * @return true if guest checkout is allowed, false otherwise
     */
    boolean isGuestCheckoutAllowed();

    /**
     * Remove flybuys number from cart
     * 
     * @return true if removed
     */
    boolean removeFlybuysNumber();


    /**
     * @param addressId
     * @return true if billing address update successfully
     */
    boolean setBillingAddress(String addressId);

    /**
     *
     * Create the Afterpay payment info using the token returned from Afterpay.
     *
     * @param afterpayToken
     *            The token returned from Afterpay
     */
    void createAfterpayPaymentInfo(String afterpayToken);


    /**
     * Modify delivery information using fluent ats data
     * 
     * @param deliveryModes
     */
    void overlayFluentStockDataOnDeliveryModes(List<TargetZoneDeliveryModeData> deliveryModes);

    /**
     * checks if cart is having a preorder item
     * 
     * @return - true if cart is having a preOrder Item.
     */
    boolean isCartHavingPreOrderItem();

    /**
     * 
     * @param deliveryModes
     */
    void checkFastlineStockOnExpressDeliveryModes(List<TargetZoneDeliveryModeData> deliveryModes);

    /**
     * Method to check if the cart contains mixed basket with gift card.
     *
     * @return true - if the cart contains gift card and other item(s) with it.
     */
    boolean isGiftCardMixedBasket();

    /**
     * Compares payment method and returns result
     * @param zippay
     * @return
     */
    boolean paymentModeEquals(String zippay);

    /**
     * Creates payment info based on the checkoutId
     * @param checkoutId
     */
    void createZipPaymentInfo(String checkoutId);
}