package au.com.target.tgtfacades.order.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.request.SubscriptionDataRequest;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import au.com.target.tgtcore.customer.TargetCustomerAccountService;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtcore.order.TargetCommerceCheckoutService;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtfacades.cart.TargetOrderErrorHandlerFacade;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.TargetPlaceOrderFacade;
import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.order.data.TargetPlaceOrderRequest;
import au.com.target.tgtfacades.order.data.TargetPlaceOrderResult;
import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderAfterpayPaymentInfoData;
import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderIPGPaymentInfoData;
import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderPayPalPaymentInfoData;
import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderZipPaymentInfoData;
import au.com.target.tgtfacades.order.enums.TargetPlaceOrderResultEnum;
import au.com.target.tgtfraud.order.ClientDetails;
import au.com.target.tgtfraud.order.TargetFraudCommerceCheckoutService;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpayment.commands.result.TargetGetSubscriptionResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.dto.LineItem;
import au.com.target.tgtpayment.exceptions.BadFundingMethodException;
import au.com.target.tgtpayment.methods.TargetAfterpayPaymentMethod;
import au.com.target.tgtpayment.methods.TargetIpgPaymentMethod;
import au.com.target.tgtpayment.methods.TargetPaymentMethod;
import au.com.target.tgtpayment.methods.TargetZippayPaymentMethod;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;
import au.com.target.tgtpayment.model.ZippayPaymentInfoModel;
import au.com.target.tgtpayment.service.TargetPaymentService;
import au.com.target.tgtpayment.strategy.PaymentMethodStrategy;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


/**
 * @author rmcalave
 * 
 */
public class TargetPlaceOrderFacadeImpl implements TargetPlaceOrderFacade {

    private static final Logger LOG = Logger.getLogger(TargetPlaceOrderFacadeImpl.class);

    @Autowired
    private TargetFraudCommerceCheckoutService targetFraudCommerceCheckoutService;

    @Autowired
    private ModelService modelService;

    @Autowired
    private TargetCheckoutFacade checkoutFacade;

    @Autowired
    private UserService userService;

    @Autowired
    private Converter<OrderModel, OrderData> orderConverter;

    @Autowired
    private CartService cartService;

    @Autowired
    private UiExperienceService uiExperienceService;

    @Autowired
    private TargetSalesApplicationService targetSalesApplicationService;

    @Autowired
    private PaymentMethodStrategy paymentMethodStrategy;

    @Autowired
    private EnumerationService enumerationService;

    @Autowired
    private TargetPaymentService targetPaymentService;

    @Autowired
    private TargetCustomerAccountService targetCustomerAccountService;

    @Autowired
    private FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    @Autowired
    private TargetCommerceCartService targetCommerceCartService;

    @Autowired
    private TargetOrderErrorHandlerFacade targetOrderErrorHandlerFacade;

    @Autowired
    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;

    @Autowired
    private FlybuysDiscountFacade flybuysDiscountFacade;

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.impl.TargetPlaceOrderFacade#placeOrder()
     */
    @Override
    public TargetPlaceOrderResult prepareForPlaceOrder() {

        final CartModel cart = getCheckoutFacade().getCart();

        final TargetPlaceOrderResultEnum validateResult = validateCart(cart);
        if (validateResult != null) {
            return new TargetPlaceOrderResult(validateResult, cart);
        }

        // Check to see if there is already a successful payment on the cart, we only allow one.
        // If there is one then something has gone wrong so bail to prevent taking another payment
        if (hasPaidAmount(cart)) {
            // Before redirecting, reverse any gift card payments captured during this session
            reverseGiftCardPayment();

            LOG.error("Cart already has a payment but trying to place order again! Code=" + cart.getCode());
            return new TargetPlaceOrderResult(TargetPlaceOrderResultEnum.UNKNOWN, cart);
        }

        final TargetPlaceOrderResultEnum placeOrderStatus = beforePlaceOrder(cart);
        if (placeOrderStatus != null) {
            return new TargetPlaceOrderResult(placeOrderStatus, cart);
        }
        return new TargetPlaceOrderResult(TargetPlaceOrderResultEnum.PREPARE_SUCCESS, cart);
    }

    /**
     * check if the cart has captured value already.
     * 
     * @param cart
     * @return true if there is any amount captured successfully.
     */
    protected boolean hasPaidAmount(final CartModel cart) {
        return getAmountPaidSoFar(cart).doubleValue() > 0d;
    }

    @Override
    public Double getAmountPaidSoFar(final CartModel cart) {
        return findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(cart);
    }

    @Override
    public TargetPlaceOrderResult finishPlaceOrder() {

        final CartModel cart = getCheckoutFacade().getCart();

        final TargetPlaceOrderResultEnum validateResult = validateCart(cart);
        if (validateResult != null) {
            return new TargetPlaceOrderResult(validateResult, cart);
        }

        if (!(getTargetCommerceCheckoutService().isCaptureOrderSuccessful(cart))) {
            getTargetCommerceCheckoutService().handlePaymentFailure(cart);
            return new TargetPlaceOrderResult(TargetPlaceOrderResultEnum.PAYMENT_FAILURE, cart);
        }

        TargetPlaceOrderResultEnum placeOrderStatus = null;
        OrderModel orderModel = null;

        try {
            orderModel = placeOrder(cart);
        }
        catch (final InvalidCartException ex) {
            LOG.warn("InvalidCartException when placing order: ", ex);
            placeOrderStatus = TargetPlaceOrderResultEnum.INVALID_CART;
        }
        catch (final Exception ex) {
            // Something else has gone wrong
            LOG.error("Exception when placing order: ", ex);
            placeOrderStatus = TargetPlaceOrderResultEnum.UNKNOWN;
        }

        if (null == placeOrderStatus && orderModel != null) {

            afterPlaceOrder(orderModel);
            final OrderData orderData = getOrderConverter().convert(orderModel);

            if (orderData != null) {
                placeOrderStatus = TargetPlaceOrderResultEnum.SUCCESS;
            }
        }

        if (placeOrderStatus == null) {
            placeOrderStatus = TargetPlaceOrderResultEnum.UNKNOWN;
        }

        return new TargetPlaceOrderResult(placeOrderStatus, orderModel);
    }


    @Override
    public TargetPlaceOrderResult placeOrder() {

        final TargetPlaceOrderResult preparePlaceOrderResult = prepareForPlaceOrder();

        if (null != preparePlaceOrderResult &&
                !TargetPlaceOrderResultEnum.PREPARE_SUCCESS.equals(preparePlaceOrderResult.getPlaceOrderResult())) {
            return preparePlaceOrderResult;
        }

        return finishPlaceOrder();
    }

    @Override
    public TargetPlaceOrderResult placeOrder(final TargetPlaceOrderRequest request) {
        Assert.notNull(request, "place order request cannot be null");
        final CartModel cartModel = checkoutFacade.getCart();
        TargetPlaceOrderResult result = null;
        if (cartModel != null) {
            final String cartCode = cartModel.getCode();
            synchronized (cartModel.getPk()) {
                try {
                    final AdjustedCartEntriesData adjustedCartEntriesData = checkoutFacade.adjustCart();
                    result = prePlaceOrderCheck(request, adjustedCartEntriesData, cartModel);
                    if (result == null) {
                        createPaymentInfo(request);
                        checkoutFacade.validateDeliveryMode();
                        setClientDetailsForFraudDetection(request.getIpAddress(), request.getCookieString());
                        result = placeOrder();
                    }
                    result.setAdjustedCartEntriesData(adjustedCartEntriesData);
                    result.setCartCode(cartCode);
                }
                catch (final AdapterException e) {
                    LOG.debug("Adapter exception during TargetPlaceOrderFacadeImpl place order", e);
                    result = new TargetPlaceOrderResult(TargetPlaceOrderResultEnum.SERVICE_UNAVAILABLE,
                            null);
                }
                catch (final BadFundingMethodException e) {
                    LOG.debug("BadFundingMethod exception during TargetPlaceOrderFacadeImpl place order", e);
                    result = new TargetPlaceOrderResult(TargetPlaceOrderResultEnum.FUNDING_FAILURE,
                            null);
                }
                catch (final Exception e) {
                    LOG.error("Exception during place order for cart=" + cartCode, e);
                    result = new TargetPlaceOrderResult(TargetPlaceOrderResultEnum.UNKNOWN,
                            null);
                }
                final TargetPlaceOrderResultEnum placeOrderResult = result.getPlaceOrderResult();
                if (TargetPlaceOrderResultEnum.SUCCESS != placeOrderResult) {
                    reverseGiftCardPayment();
                    if (flybuysDiscountFacade.reassessFlybuysDiscountOnCheckoutCart()
                            && TargetPlaceOrderResultEnum.INSUFFICIENT_STOCK == placeOrderResult) {
                        result.setPlaceOrderResult(TargetPlaceOrderResultEnum.INSUFFICIENT_STOCK_FLYBUYS);
                    }
                }
                if (TargetPlaceOrderResultEnum.UNKNOWN == placeOrderResult) {
                    targetOrderErrorHandlerFacade.handleOrderCreationFailed(placeOrderResult, cartCode);
                }
            }
        }
        return result;
    }

    /**
     * @param request
     * @param adjustedCartEntriesData
     * @param cartModel
     * @return TargetPlaceOrderResult
     */
    protected TargetPlaceOrderResult prePlaceOrderCheck(final TargetPlaceOrderRequest request,
            final AdjustedCartEntriesData adjustedCartEntriesData, final CartModel cartModel) {

        final TargetCartData cartData = (TargetCartData)checkoutFacade.getCheckoutCart();
        if (request.getPaymentInfo() instanceof TargetPlaceOrderIPGPaymentInfoData) {
            final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfoData = (TargetPlaceOrderIPGPaymentInfoData)request
                    .getPaymentInfo();
            if (ipgPaymentInfoData.getIpgToken() != null
                    && !checkoutFacade.updateIpgPaymentInfoWithToken(ipgPaymentInfoData.getIpgToken())) {
                return new TargetPlaceOrderResult(
                        TargetPlaceOrderResultEnum.PAYMENT_METHOD_MISMATCH,
                        null);
            }
        }

        TargetPlaceOrderResult result = null;
        if (targetPurchaseOptionHelper.getCurrentPurchaseOptionModel() == null
                || CollectionUtils.isEmpty(cartModel.getEntries())
                || cartData.isInsufficientAmount()) {
            result = new TargetPlaceOrderResult(
                    TargetPlaceOrderResultEnum.INVALID_CART,
                    null);
        }
        else if (targetCommerceCartService.removeMissingProducts(cartModel)) {
            result = new TargetPlaceOrderResult(
                    TargetPlaceOrderResultEnum.INVALID_PRODUCT_IN_CART,
                    null);
        }
        else if (checkoutFacade.hasIncompleteDeliveryDetails()) {
            checkoutFacade.removeDeliveryInfo();
            result = new TargetPlaceOrderResult(
                    TargetPlaceOrderResultEnum.INCOMPLETE_DELIVERY_INFO,
                    null);
        }
        else if (checkoutFacade.hasIncompleteBillingAddress()) {
            result = new TargetPlaceOrderResult(
                    TargetPlaceOrderResultEnum.INCOMPLETE_BILLING_ADDRESS,
                    null);
        }
        else if (checkoutFacade.isPaymentModeGiftCard() && !checkoutFacade.isGiftCardPaymentAllowedForCart()) {
            result = new TargetPlaceOrderResult(
                    TargetPlaceOrderResultEnum.GIFTCARD_PAYMENT_NOT_ALLOWED,
                    null);
        }
        else if (adjustedCartEntriesData != null && adjustedCartEntriesData.isAdjusted()) {
            result = new TargetPlaceOrderResult(TargetPlaceOrderResultEnum.INSUFFICIENT_STOCK,
                    null);
        }
        else if (flybuysDiscountFacade.reassessFlybuysDiscountOnCheckoutCart()) {
            result = new TargetPlaceOrderResult(TargetPlaceOrderResultEnum.INVALID_FLYBUYS_REDEMPTION, null);
        }
        return result;
    }

    /**
     * Perform operations required before the order is placed.
     * 
     * @param cartModel
     * @return {@link TargetPlaceOrderResultEnum#INSUFFICIENT_STOCK}, {@link TargetPlaceOrderResultEnum#PAYMENT_FAILURE}
     *         if beforeOrder fails, or null if beforeOrder succeeds.
     */
    protected TargetPlaceOrderResultEnum beforePlaceOrder(final CartModel cartModel) {
        PaymentTransactionModel paymentTransactionModel = null;
        try {

            if (cartModel != null) {
                final PaymentInfoModel paymentInfoModel = cartModel.getPaymentInfo();
                final TargetPaymentMethod paymentMethod = getPaymentMethodStrategy().getPaymentMethod(paymentInfoModel);

                if (paymentMethod != null && paymentMethod instanceof TargetIpgPaymentMethod) {
                    // 1. Query transaction details submitted to IFrame are validated
                    final TargetQueryTransactionDetailsResult result = targetPaymentService.queryTransactionDetails(
                            paymentMethod,
                            paymentInfoModel);

                    final TargetPlaceOrderResultEnum ipgInformationResult = processIpgInformation(cartModel, result);
                    if (ipgInformationResult != null) {
                        return ipgInformationResult;
                    }
                    else {
                        // Persist split payments into one transaction with multiple entries
                        paymentTransactionModel = targetPaymentService
                                .createTransactionWithQueryResult(cartModel, result);
                    }
                }
                final TargetPlaceOrderResultEnum paymentResult = processPaymentInformation(paymentMethod,
                        paymentInfoModel, cartModel);
                if (paymentResult != null) {
                    return paymentResult;
                }
            }

            final boolean isSucceed = getTargetCommerceCheckoutService().beforePlaceOrder(cartModel,
                    paymentTransactionModel);

            if (isSucceed) {
                return null;
            }
        }
        catch (final InsufficientStockLevelException ex) {
            if (paymentTransactionModel != null) {
                getModelService().remove(paymentTransactionModel);
            }
            return TargetPlaceOrderResultEnum.INSUFFICIENT_STOCK;
        }
        catch (final FluentOrderException e) {
            if (paymentTransactionModel != null) {
                getModelService().remove(paymentTransactionModel);
            }
            return TargetPlaceOrderResultEnum.FLUENT_ORDER_EXCEPTION;
        }

        return TargetPlaceOrderResultEnum.PAYMENT_FAILURE;
    }


    private TargetPlaceOrderResultEnum processIpgInformation(final CartModel cartModel,
            final TargetQueryTransactionDetailsResult result) {


        // 2. If valid, save card result to be used later. If not, exit order flow with exception
        if (result != null && result.isSuccess()) {

            //Remove the existing saved cards and update new saved cards from IPG result.
            if (null != cartModel.getUser() && BooleanUtils.isFalse(cartModel.getContainsPreOrderItems())) {
                // for preOrders, we are not sending the saved cards to IPG and always return null list and this removes the already saved cards of the customer.
                targetCustomerAccountService
                        .removeCustomerIPGSavedCards((CustomerModel)cartModel.getUser());
                targetPaymentService.updateSavedCards(cartModel, result.getSavedCards());
            }

            // In case of split payment or single payment with gift card, check whether 
            // order total amount matches total amount submitted from iFrame. If they 
            // don't match, reverse payments and exit order flow with exception.

            final boolean shouldReversePayments = getTargetCommerceCheckoutService()
                    .shouldPaymentBeReversed(cartModel,
                            result.getCardResults());

            if (shouldReversePayments) {
                getTargetPaymentService().reversePayment(cartModel,
                        getGiftCards(result.getCardResults()));

                return TargetPlaceOrderResultEnum.PAYMENT_FAILURE_DUETO_MISSMATCH_TOTALS;
            }


        }
        else {
            return TargetPlaceOrderResultEnum.PAYMENT_FAILURE;
        }
        return null;

    }

    /**
     * check afterpay payment method and validate the cart data wrt to data sent to afterpay
     * 
     * @param paymentMethod
     * @param paymentInfoModel
     * @param cartModel
     * @return - {@link TargetPlaceOrderResultEnum} only if validation fails, else return null means no failures in
     *         validation
     */
    private TargetPlaceOrderResultEnum processPaymentInformation(final TargetPaymentMethod paymentMethod,
                                                                 final PaymentInfoModel paymentInfoModel, final CartModel cartModel) {
        TargetGetSubscriptionResult result = null;
        String paymentMode = null;
        if(paymentMethod != null) {
            if (paymentMethod instanceof TargetAfterpayPaymentMethod){
                final AfterpayPaymentInfoModel afterpayPaymentModel = (AfterpayPaymentInfoModel) paymentInfoModel;
                final SubscriptionDataRequest request = new SubscriptionDataRequest(null,
                        afterpayPaymentModel.getAfterpayToken(), null);
                result = paymentMethod.getSubscription(request);
                paymentMode = TgtFacadesConstants.AFTERPAY;
            } else if (paymentMethod instanceof TargetZippayPaymentMethod) {
                    final ZippayPaymentInfoModel payment = (ZippayPaymentInfoModel) paymentInfoModel;
                    result = paymentMethod.getSubscription(new SubscriptionDataRequest(null,
                            payment.getCheckoutId(), null));
                    paymentMode = TgtFacadesConstants.ZIPPAY;
            }
        }
        return result != null ? getCartResult(cartModel, result, paymentMode) : null;
    }

    private TargetPlaceOrderResultEnum getCartResult(final CartModel cartModel, final TargetGetSubscriptionResult result, final String paymentMode) {

        validateParameterNotNull(result, "Parameter result must not be null");
        validateParameterNotNull(paymentMode, "Parameter paymentMode must not be null");
        validateParameterNotNull(cartModel, "Parameter cartModel must not be null");

        // Handling result mismatch scenario for zippay
        if(StringUtils.equals(paymentMode, TgtFacadesConstants.ZIPPAY) && !StringUtils.equalsIgnoreCase("approved", result.getOrderState())){
            return TargetPlaceOrderResultEnum.PAYMENT_FAILURE;
        }

        if (result.getAmount().compareTo(BigDecimal.valueOf(cartModel.getTotalPrice().doubleValue())) != 0) {
            return TargetPlaceOrderResultEnum.PAYMENT_FAILURE_DUETO_MISSMATCH_TOTALS;
        }

        final List<AbstractOrderEntryModel> cartEntries = cartModel.getEntries();
        final List<LineItem> lineItemsAfterpay = result.getLineItems();
        if (cartEntries.size() != lineItemsAfterpay.size() || !compareItems(cartEntries, lineItemsAfterpay)) {
            // even if one of entry mismtaches
            return TargetPlaceOrderResultEnum.ITEM_MISMATCH_IN_CART_AFTER_PAYMENT;
        }

        return null;
    }

    protected boolean compareItems(final List<AbstractOrderEntryModel> entries, final List<LineItem> lineItems) {
        for (final AbstractOrderEntryModel entry : entries) {
            boolean isInList = false;
            for (final LineItem lineItem : lineItems) {
                if (compare(entry, lineItem)) {
                    // if item matches, then break the inner if loop
                    isInList = true;
                    break;
                }
            }
            if (!isInList) {
                // if item is not in entryList, reutrn false
                return false;
            }
        }

        return true;
    }

    /**
     * @param entry
     * @param lineItem
     */
    private boolean compare(final AbstractOrderEntryModel entry, final LineItem lineItem) {
        return entry.getProduct().getCode().equals(lineItem.getProductCode())
                && entry.getQuantity().longValue() == lineItem.getQuantity();

    }

    /**
     * Filters gift cards from card used for payment
     * 
     * @param cardResults
     *            - card details used for payment
     * @return list of gift cards
     */
    protected List<TargetCardResult> getGiftCards(final List<TargetCardResult> cardResults) {
        final List<TargetCardResult> giftCards = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(cardResults)) {
            for (final TargetCardResult targetCardResult : cardResults) {
                if (targetCardResult.getCardType().equalsIgnoreCase(CreditCardType.GIFTCARD.getCode())) {
                    giftCards.add(targetCardResult);
                }
            }
        }
        return giftCards;
    }

    /**
     * Place the order after determining the correct {@link SalesApplication} (WEBMOBILE for mobile, WEB otherwise).
     * 
     * @param cartModel
     *            The cart to place the order for
     * @return The order model created as part of placing the order
     * @throws InvalidCartException
     *             If cart validation fails
     */
    protected OrderModel placeOrder(final CartModel cartModel) throws InvalidCartException {
        final SalesApplication salesApplication = getTargetSalesApplicationService().getCurrentSalesApplication();
        if (null != cartModel && null != cartModel.getCalculated()) {
            if (cartModel.getCalculated().booleanValue() == false) {
                checkoutFacade.recalculateCart();
            }
        }
        final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
        checkoutParameter.setCart(cartModel);
        checkoutParameter.setSalesApplication(salesApplication);
        return getTargetCommerceCheckoutService().placeOrder(checkoutParameter).getOrder();
    }

    /**
     * Perform operations required after the order has been placed.
     * 
     * @param orderModel
     *            The order model
     */
    protected void afterPlaceOrder(final OrderModel orderModel) {
        final CartModel cart = getCheckoutFacade().getCart();

        if (cart == null) {
            return;
        }

        getTargetCommerceCheckoutService().afterPlaceOrder(cart, orderModel);
    }

    protected TargetPlaceOrderResultEnum validateCart(final CartModel cartModel) {
        if (cartModel == null) {
            return TargetPlaceOrderResultEnum.INVALID_CART;
        }

        if (isUserMismatch(cartModel)) {
            return TargetPlaceOrderResultEnum.USER_MISMATCH;
        }

        return null;
    }

    protected boolean isUserMismatch(final CartModel cartModel) {
        final TargetCustomerModel cartUser = (TargetCustomerModel)cartModel.getUser();
        final UserModel currentUser = getUserService().getCurrentUser();
        if (cartUser.equals(currentUser)) {
            return false;
        }

        return !CustomerType.GUEST.equals(cartUser.getType());
    }

    protected void createPaymentInfo(final TargetPlaceOrderRequest request) {
        if (checkoutFacade.isPaymentModePaypal()) {

            LOG.debug("Start checkoutFacade.isPaymentModePaypal()");

            final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfo;

            if (request.getPaymentInfo() instanceof TargetPlaceOrderPayPalPaymentInfoData) {
                payPalPaymentInfo = (TargetPlaceOrderPayPalPaymentInfoData)request.getPaymentInfo();
            }
            else {
                throw new IllegalArgumentException(
                        "Expected request.paymentInfo to be an instance of TargetPlaceOrderPayPalPaymentInfoData");
            }

            getCheckoutFacade().createPayPalPaymentInfo(payPalPaymentInfo.getPaypalSessionToken(),
                    payPalPaymentInfo.getPaypalPayerId());
            LOG.debug("Exit checkoutFacade.isPaymentModePaypal()");

        }
        else if (checkoutFacade.isPaymentModeAfterpay()) {
            LOG.debug("Start checkoutFacade.isPaymentModeAfterpay()");

            final TargetPlaceOrderAfterpayPaymentInfoData afterpayPaymentInfo;

            if (request.getPaymentInfo() instanceof TargetPlaceOrderAfterpayPaymentInfoData) {
                afterpayPaymentInfo = (TargetPlaceOrderAfterpayPaymentInfoData)request.getPaymentInfo();
            }
            else {
                throw new IllegalArgumentException(
                        "Expected request.paymentInfo to be an instance of TargetPlaceOrderAfterpayPaymentInfoData");
            }

            getCheckoutFacade().createAfterpayPaymentInfo(afterpayPaymentInfo.getAfterpayOrderToken());

            LOG.debug("Exit checkoutFacade.isPaymentModeAfterpay()");
        } else if (checkoutFacade.paymentModeEquals(TgtFacadesConstants.ZIPPAY)) {
            LOG.debug("Start checkoutFacade.ZIPPAY");

            if (request.getPaymentInfo() instanceof TargetPlaceOrderZipPaymentInfoData) {
                getCheckoutFacade().createZipPaymentInfo(
                        ((TargetPlaceOrderZipPaymentInfoData) request.getPaymentInfo()).getCheckoutId());
            }
            else {
                throw new IllegalArgumentException(
                        "Expected request.paymentInfo to be an instance of TargetPlaceOrderZipPaymentInfoData");
            }

            LOG.debug("Exit checkoutFacade.ZIPPAY");
        }
    }

    @Override
    public void setClientDetailsForFraudDetection(final String ipAddress, final String browserCookies) {
        validateParameterNotNullStandardMessage("ipAddress", ipAddress);
        validateParameterNotNullStandardMessage("browserCookies", browserCookies);

        final CartModel cart = getCheckoutFacade().getCart();

        if (cart != null) {
            final ClientDetails clientDetails = new ClientDetails();
            clientDetails.setBrowserCookies(browserCookies);
            clientDetails.setIpAddress(ipAddress);
            targetFraudCommerceCheckoutService.populateClientDetailsForFraudDetection(cart, clientDetails);
        }
    }

    @Override
    public void addPayPalTransactionIdToPaymentTransactionEntry(final String transactionId) {
        final CartModel cart = getCheckoutFacade().getCart();

        final List<PaymentTransactionModel> paymentTransactions = cart.getPaymentTransactions();

        PaymentTransactionEntryModel paymentTransactionEntryToUpdate = null;
        ptLoop: for (final PaymentTransactionModel paymentTransaction : paymentTransactions) {
            final List<PaymentTransactionEntryModel> paymentTransactionEntries = paymentTransaction.getEntries();

            for (final PaymentTransactionEntryModel paymentTransactionEntry : paymentTransactionEntries) {
                if (!TransactionStatus.REVIEW.toString().equals(paymentTransactionEntry.getTransactionStatus())) {
                    continue;
                }

                paymentTransactionEntryToUpdate = paymentTransactionEntry;
                break ptLoop;
            }
        }

        if (paymentTransactionEntryToUpdate == null) {
            return;
        }

        paymentTransactionEntryToUpdate.setCode(transactionId);
        paymentTransactionEntryToUpdate.setRequestId(transactionId);
        getModelService().save(paymentTransactionEntryToUpdate);

        getModelService().refresh(cart);
    }

    @Override
    public List<TargetCardResult> getGiftCardPayments(final CartModel cartModel) {
        return targetPaymentService.getGiftCardPayments(cartModel);
    }

    @Override
    public boolean reverseGiftCardPayment() {
        final CartModel cartModel = getCheckoutFacade().getCart();
        return targetPaymentService.reverseGiftCardPayment(cartModel);
    }

    @Override
    public boolean reverseGiftCardPayment(final String ipgToken, final String ipgSessionId) {
        Assert.notNull(ipgToken, "ipgToken should not be null");
        Assert.notNull(ipgSessionId, "ipgSessionId should not be null");
        return targetPaymentService.reverseGiftCardPayment(ipgToken, ipgSessionId);
    }

    @Override
    public TargetQueryTransactionDetailsResult getQueryTransactionDetailResults() {
        final CartModel cartModel = getCheckoutFacade().getCart();

        return retrieveQueryTransactionResults(cartModel);
    }

    @Override
    public TargetQueryTransactionDetailsResult getQueryTransactionDetailResults(final AbstractOrderModel orderModel) {

        return retrieveQueryTransactionResults(orderModel);
    }

    /**
     * @param orderModel
     * @return targetQueryTransactionDetailsResult
     */
    private TargetQueryTransactionDetailsResult retrieveQueryTransactionResults(final AbstractOrderModel orderModel) {
        final PaymentInfoModel paymentInfoModel = orderModel.getPaymentInfo();
        final TargetPaymentMethod paymentMethod = getPaymentMethodStrategy().getPaymentMethod(paymentInfoModel);

        return targetPaymentService.queryTransactionDetails(paymentMethod, paymentInfoModel);
    }

    /**
     * @return the uiExperienceService
     */
    protected UiExperienceService getUiExperienceService() {
        return uiExperienceService;
    }

    /**
     * @return the instance of TargetCommerceCheckoutService
     */
    protected TargetCommerceCheckoutService getTargetCommerceCheckoutService() {
        return targetFraudCommerceCheckoutService;
    }

    /**
     * @return the modelService
     */
    protected ModelService getModelService() {
        return modelService;
    }

    /**
     * @return the checkoutFacade
     */
    protected TargetCheckoutFacade getCheckoutFacade() {
        return checkoutFacade;
    }

    /**
     * @return the userService
     */
    protected UserService getUserService() {
        return userService;
    }

    /**
     * @return the orderConverter
     */
    protected Converter<OrderModel, OrderData> getOrderConverter() {
        return orderConverter;
    }

    /**
     * @return the cartService
     */
    protected CartService getCartService() {
        return cartService;
    }

    /**
     * @return the targetSalesApplicationService
     */
    protected TargetSalesApplicationService getTargetSalesApplicationService() {
        return targetSalesApplicationService;
    }

    /**
     * @param targetSalesApplicationService
     *            the targetSalesApplicationService to set
     */
    public void setTargetSalesApplicationService(final TargetSalesApplicationService targetSalesApplicationService) {
        this.targetSalesApplicationService = targetSalesApplicationService;
    }

    /**
     * @param service
     */
    public void setTargetCommerceCheckoutService(final TargetFraudCommerceCheckoutService service) {
        targetFraudCommerceCheckoutService = service;
    }

    /**
     * @return the paymentMethodStrategy
     */
    public PaymentMethodStrategy getPaymentMethodStrategy() {
        return paymentMethodStrategy;
    }

    /**
     * @return the enumerationService
     */
    public EnumerationService getEnumerationService() {
        return enumerationService;
    }

    /**
     * @return the targetPaymentService
     */
    public TargetPaymentService getTargetPaymentService() {
        return targetPaymentService;
    }
}