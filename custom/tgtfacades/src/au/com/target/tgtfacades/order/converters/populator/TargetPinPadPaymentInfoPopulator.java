/**
 * 
 */
package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.CardTypeData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.user.data.PinPadPaymentInfoData;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;


/**
 * Populator to populate PinPadPaymentInfoModel to TargetCCPaymentInfoData. AbstractOrderData contains the type
 * CCPaymentInfoData for payment info therefore this class will convert to a TargetCCPaymentInfoData which already
 * extends CCPaymentInfoData.
 * 
 * @author jjayawa1
 * 
 */
public class TargetPinPadPaymentInfoPopulator implements
        Populator<PinPadPaymentInfoModel, TargetCCPaymentInfoData> {

    private Converter<AddressModel, TargetAddressData> addressConverter;

    private Converter<CreditCardType, CardTypeData> cardTypeConverter;


    @Override
    public void populate(final PinPadPaymentInfoModel source, final TargetCCPaymentInfoData target) {

        target.setId(source.getPk().toString());
        target.setPinPadPaymentInfo(true);

        final PinPadPaymentInfoData pinPadInfoData = new PinPadPaymentInfoData();
        pinPadInfoData.setJournRoll(source.getJournRoll());
        pinPadInfoData.setRespAscii(source.getRespAscii());
        pinPadInfoData.setAccountType(source.getAccountType());
        pinPadInfoData.setMaskedCardNumber(source.getMaskedCardNumber());
        pinPadInfoData.setRrn(source.getRrn());

        if (source.getCardType() != null) {
            pinPadInfoData.setCardType(source.getCardType().getCode());

            final CardTypeData cardTypeData = cardTypeConverter.convert(source.getCardType());
            target.setCardType(cardTypeData.getCode());
            target.setCardTypeData(cardTypeData);
        }

        pinPadInfoData.setAuthCode(source.getAuthCode());

        target.setPinPadPaymentInfoData(pinPadInfoData);

        if (source.getBillingAddress() != null) {
            target.setBillingAddress(addressConverter.convert(source.getBillingAddress()));
        }
    }

    /**
     * @param addressConverter
     *            the addressConverter to set
     */
    @Required
    public void setAddressConverter(final Converter<AddressModel, TargetAddressData> addressConverter) {
        this.addressConverter = addressConverter;
    }

    /**
     * @param cardTypeConverter
     *            the cardTypeConverter to set
     */
    @Required
    public void setCardTypeConverter(final Converter<CreditCardType, CardTypeData> cardTypeConverter) {
        this.cardTypeConverter = cardTypeConverter;
    }

}
