/**
 * 
 */
package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commercefacades.order.converters.populator.OrderConsignmentPopulator;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtfacades.order.data.TargetOrderData;


/**
 * Populator class to populate Consignment details in TargetOrderData
 * 
 * @author Pratik
 *
 */
public class TargetOrderConsignmentPopulator extends OrderConsignmentPopulator<OrderModel, TargetOrderData> {

    /**
     * Populate method to populate consignment details in TargetOrderData
     */
    @Override
    public void populate(final OrderModel source, final TargetOrderData target) throws ConversionException {

        if (CollectionUtils.isEmpty(source.getConsignments())) {
            return;
        }

        final List<ConsignmentData> consignments = new ArrayList<>();
        for (final ConsignmentModel consignmentModel : source.getConsignments()) {
            if (CollectionUtils.isEmpty(consignmentModel.getConsignmentEntries()) ||
                    consignmentModel.getStatus().equals(ConsignmentStatus.CANCELLED)) {
                continue;
            }
            consignments.add(getConsignmentConverter().convert(consignmentModel));
        }
        if (!consignments.isEmpty()) {
            target.setConsignment(consignments);
        }
    }

}
