/**
 *
 */
package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;

import org.apache.commons.lang.BooleanUtils;

import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;


/**
 * @author asingh78
 *
 */
public class TargetCreditCardPaymentInfoPopulator implements
        Populator<CreditCardPaymentInfoModel, TargetCCPaymentInfoData> {

    @Override
    public void populate(final CreditCardPaymentInfoModel source, final TargetCCPaymentInfoData target)
    {
        final TargetCCPaymentInfoData targetCCPaymentInfoData = target;
        targetCCPaymentInfoData.setPaymentSucceeded(BooleanUtils.isTrue(source.getIsPaymentSucceeded()));
    }
}
