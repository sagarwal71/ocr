/**
 * 
 */
package au.com.target.tgtfacades.order.data;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import au.com.target.tgtfacades.featureswitch.data.FeatureSwitchData;


/**
 * DTO to hold environment data.
 * 
 * @author jjayawa1
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)

public class EnvironmentData {
    private String gaId;
    private String gtmId;
    private String fqdn;
    private String zipMoneyThreshold;

    private List<FeatureSwitchData> switches;

    /**
     * @return the gaId
     */
    public String getGaId() {
        return gaId;
    }

    /**
     * @param gaId
     *            the gaId to set
     */
    public void setGaId(final String gaId) {
        this.gaId = gaId;
    }

    /**
     * @return the gtmId
     */
    public String getGtmId() {
        return gtmId;
    }

    /**
     * @param gtmId
     *            the gtmId to set
     */
    public void setGtmId(final String gtmId) {
        this.gtmId = gtmId;
    }

    /**
     * @return the fqdn
     */
    public String getFqdn() {
        return fqdn;
    }

    /**
     * @param fqdn
     *            the fqdn to set
     */
    public void setFqdn(final String fqdn) {
        this.fqdn = fqdn;
    }

    /**
     * @return the switches
     */
    public List<FeatureSwitchData> getSwitches() {
        return switches;
    }

    /**
     * @param switches
     *            the switches to set
     */
    public void setSwitches(final List<FeatureSwitchData> switches) {
        this.switches = switches;
    }

    /**
     *
     * @return
     */
    public String getZipMoneyThreshold() {
        return zipMoneyThreshold;
    }

    /**
     *
     * @param zipMoneyThreshold
     */
    public void setZipMoneyThreshold(final String zipMoneyThreshold) {
        this.zipMoneyThreshold = zipMoneyThreshold;
    }
}
