/**
 * 
 */
package au.com.target.tgtfacades.order.converters;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.ordercancel.model.OrderEntryCancelRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.returns.model.OrderEntryReturnRecordEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.order.data.OrderModificationData;
import au.com.target.tgtfacades.order.enums.OrderModificationStatusEnum;


/**
 * @author rmcalave
 * 
 */
public class OrderEntryModificationRecordEntryConverter extends
        AbstractPopulatingConverter<OrderEntryModificationRecordEntryModel, OrderModificationData> {

    private Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter;
    private PriceDataFactory priceDataFactory;


    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.impl.AbstractPopulatingConverter#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final OrderEntryModificationRecordEntryModel source, final OrderModificationData target) {
        getOrderEntryConverter().convert(source.getOrderEntry(), target);
        target.setDate(source.getCreationtime());

        if (source instanceof OrderEntryReturnRecordEntryModel) {
            final OrderEntryReturnRecordEntryModel returnRecord = (OrderEntryReturnRecordEntryModel)source;
            target.setQuantity(returnRecord.getReturnedQuantity());
            target.setStatus(OrderModificationStatusEnum.Refunded);
            target.setReason(returnRecord.getRefundReason());

        }
        else if (source instanceof OrderEntryCancelRecordEntryModel) {
            final OrderEntryCancelRecordEntryModel cancelRecordEntry = (OrderEntryCancelRecordEntryModel)source;
            if (cancelRecordEntry.getCancelReason() != null) {
                target.setReason(cancelRecordEntry.getCancelReason().getCode());
            }
            if (cancelRecordEntry.getCancelledQuantity() != null) {
                target.setQuantity(Long.valueOf(cancelRecordEntry.getCancelledQuantity().longValue()));
            }
        }

        if (target.getBasePrice() != null && target.getQuantity() != null) {
            final BigDecimal basePrice = target.getBasePrice().getValue();
            final BigDecimal quantity = new BigDecimal(target.getQuantity().longValue());
            final BigDecimal totalPrice = basePrice.multiply(quantity);
            final String currencyIsoCode = source.getOrderEntry().getOrder().getCurrency().getIsocode();
            target.setTotalPrice(getPriceDataFactory().create(PriceDataType.BUY, totalPrice, currencyIsoCode));
        }

        super.populate(source, target);
    }

    /**
     * @return the orderEntryConverter
     */
    protected Converter<AbstractOrderEntryModel, OrderEntryData> getOrderEntryConverter() {
        return orderEntryConverter;
    }

    /**
     * @param orderEntryConverter
     *            the orderEntryConverter to set
     */
    @Required
    public void setOrderEntryConverter(final Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter) {
        this.orderEntryConverter = orderEntryConverter;
    }

    /**
     * @return the priceDataFactory
     */
    protected PriceDataFactory getPriceDataFactory() {
        return priceDataFactory;
    }

    /**
     * @param priceDataFactory
     *            the priceDataFactory to set
     */
    @Required
    public void setPriceDataFactory(final PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }
}
