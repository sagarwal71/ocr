/**
 * 
 */
package au.com.target.tgtfacades.flybuys.impl;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.InstanceofPredicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetRuntimeException;
import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysAuthenticateResponseDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysRedeemTierDto;
import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;
import au.com.target.tgtcore.flybuys.service.FlybuysDiscountService;
import au.com.target.tgtcore.flybuys.service.FlybuysRedeemConfigService;
import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.model.FlybuysRedeemConfigModel;
import au.com.target.tgtcore.util.ProductUtil;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedeemConfigData;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedeemTierData;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedemptionData;
import au.com.target.tgtfacades.flybuys.data.FlybuysRequestStatusData;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;


/**
 * @author rmcalave
 * 
 */
public class FlybuysDiscountFacadeImpl implements FlybuysDiscountFacade {

    private static final Logger LOG = Logger.getLogger(FlybuysDiscountFacadeImpl.class);

    private PriceDataFactory priceDataFactory;

    private TargetCheckoutFacade targetCheckoutFacade;

    private TargetVoucherService targetVoucherService;

    private FlybuysDiscountService flybuysDiscountService;

    private FlybuysRedeemConfigService flybuysRedeemConfigService;

    private SessionService sessionService;

    private Converter<FlybuysRedeemConfigModel, FlybuysRedeemConfigData> flybuysRedeemConfigConverter;

    private ModelService modelService;

    private CommerceCartService commerceCartService;

    private ConfigurationService configurationService;

    @Override
    public FlybuysRequestStatusData getFlybuysRedemptionData(final String flybuysCardNumber,
            final Date dateOfBirth,
            final String postCode) {


        final FlybuysRequestStatusData status = new FlybuysRequestStatusData();


        final FlybuysAuthenticateResponseDto flybuysAuthenticateResponseDto = flybuysDiscountService
                .authenticateFlybuys(flybuysCardNumber, dateOfBirth, postCode);
        if (flybuysAuthenticateResponseDto == null) {
            final String errorMessage = "Null response received from FlybuysDiscountService for authentication request.";
            LOG.error(errorMessage);
            status.setResponse(FlybuysResponseType.UNAVAILABLE);
            status.setErrorMessage(errorMessage);
        }
        else if (!FlybuysResponseType.SUCCESS.equals(flybuysAuthenticateResponseDto.getResponse())) {
            status.setResponse(flybuysAuthenticateResponseDto.getResponse());
            status.setErrorMessage(flybuysAuthenticateResponseDto.getErrorMessage());
        }
        else {
            sessionService.setAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE,
                    flybuysAuthenticateResponseDto);
            status.setResponse(FlybuysResponseType.SUCCESS);

        }

        return status;
    }

    /**
     * Retrieves the flybuys authentication data and redeem tiers from the session and filters them based on the value
     * of the cart.
     * 
     * @return the flybuys redemption data, or null if there's nothing in session
     * @see au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade#getFlybuysRedemptionDataForCheckoutCart()
     */
    @Override
    public FlybuysRedemptionData getFlybuysRedemptionDataForCheckoutCart() {
        return getFlybuysRedemptionDataForCart(getTargetCheckoutFacade().getCart());
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade#applyFlybuysDiscountToCheckoutCart(java.lang.String)
     */
    @Override
    public boolean applyFlybuysDiscountToCheckoutCart(final String redeemCode) {
        if (StringUtils.isBlank(redeemCode)) {
            return false;
        }

        final CartModel checkoutCart = getTargetCheckoutFacade().getCart();

        // Remove any existing flybuys discount first. Either way, if there's no details in
        // session or we're applying a new one, we want to remove what's there.
        if (!targetVoucherService.removeFlybuysDiscount(checkoutCart)) {
            LOG.info("Could not apply flybuys Discount " + redeemCode + " to cart " + checkoutCart.getCode()
                    + " because existing discounts couldn't be removed.");
            return false;
        }

        // If flybuys discounts aren't allowed for this cart don't allow it
        final FlybuysDiscountDenialDto flybuysDiscountDenialDto = targetVoucherService
                .getFlybuysDiscountAllowed(checkoutCart);
        if (flybuysDiscountDenialDto != null && flybuysDiscountDenialDto.isDenied()) {
            LOG.info("Could not apply flybuys Discount " + redeemCode + " to cart " + checkoutCart.getCode()
                    + " because flybuys discounts aren't allowed for this cart.");
            return false;
        }

        final FlybuysAuthenticateResponseDto flybuysAuthenticateResponseDto = sessionService
                .getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE);

        if (flybuysAuthenticateResponseDto == null) {
            LOG.info("Could not apply flybuys Discount " + redeemCode + " to cart " + checkoutCart.getCode()
                    + " because flybuys redemption details exist in session.");
            return false;
        }

        final Collection<FlybuysRedeemTierDto> redeemTiers = flybuysAuthenticateResponseDto.getRedeemTiers();

        if (CollectionUtils.isEmpty(redeemTiers)) {
            LOG.info("Could not apply flybuys Discount " + redeemCode + " to cart " + checkoutCart.getCode()
                    + " because the flybuys redemption details in session have no redeem tiers.");
            return false;
        }

        FlybuysRedeemTierDto redeemTierToApply = null;
        for (final FlybuysRedeemTierDto redeemTier : redeemTiers) {
            if (redeemCode.equals(redeemTier.getRedeemCode())) {
                redeemTierToApply = redeemTier;
                break;
            }
        }

        if (redeemTierToApply == null) {
            LOG.info("Could not apply flybuys Discount " + redeemCode + " to cart " + checkoutCart.getCode()
                    + " because no redeem tier exists for this redeem code.");
            return false;
        }


        try {
            final boolean result = targetVoucherService.applyFlybuysDiscount(checkoutCart,
                    redeemTierToApply.getRedeemCode(),
                    redeemTierToApply.getDollarAmt(), redeemTierToApply.getPoints(),
                    flybuysAuthenticateResponseDto.getSecurityToken());

            if (result) {
                final CommerceCartParameter cartParameter = new CommerceCartParameter();
                cartParameter.setCart(checkoutCart);
                commerceCartService.recalculateCart(cartParameter);
                getModelService().refresh(checkoutCart);
            }
            return result;
        }
        catch (final CalculationException e) {
            LOG.error("Failed to recalculate cartModel including flybuys", e);
            return false;
        }
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade#removeFlybuysDiscountFromCheckoutCart()
     */
    @Override
    public boolean removeFlybuysDiscountFromCheckoutCart() {
        final CartModel checkoutCart = getTargetCheckoutFacade().getCart();

        final boolean result = targetVoucherService.removeFlybuysDiscount(checkoutCart);
        if (result) {
            try {
                final CommerceCartParameter cartParameter = new CommerceCartParameter();
                cartParameter.setCart(checkoutCart);
                commerceCartService.recalculateCart(cartParameter);
                getModelService().refresh(checkoutCart);
                return result;
            }
            catch (final CalculationException e) {
                LOG.error("Failed to recalculate cartModel after removing flybuys", e);
                return false;
            }
        }
        return result;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade#getFlybuysConfigData(au.com.target.tgtcore.model.FlybuysDiscountModel)
     */
    @Override
    public FlybuysRedeemConfigData getFlybuysRedeemConfigData() {
        FlybuysRedeemConfigModel flybuysRedeemConfigModel = null;

        try {
            flybuysRedeemConfigModel = flybuysRedeemConfigService.getFlybuysRedeemConfig();
        }
        catch (final TargetRuntimeException e) {
            LOG.info("No FlybuysRedeemConfigModel found in the system.");
        }
        if (null != flybuysRedeemConfigModel) {
            return getFlybuysRedeemConfigConverter().convert(flybuysRedeemConfigModel);
        }
        return null;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade#getAppliedFlybuysRedeemTierForCheckoutFart()
     */
    @Override
    public FlybuysRedeemTierData getAppliedFlybuysRedeemTierForCheckoutCart() {
        final FlybuysAuthenticateResponseDto flybuysAuthenticateResponseDto = sessionService
                .getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE);
        if (flybuysAuthenticateResponseDto == null) {
            return null;
        }

        final Collection<FlybuysRedeemTierDto> redeemTiers = flybuysAuthenticateResponseDto.getRedeemTiers();

        if (CollectionUtils.isEmpty(redeemTiers)) {
            return null;
        }

        final CartModel checkoutCart = getTargetCheckoutFacade().getCart();
        final FlybuysDiscountModel appliedFlybuysDiscount = targetVoucherService
                .getFlybuysDiscountForOrder(checkoutCart);

        if (appliedFlybuysDiscount == null) {
            return null;
        }

        for (final FlybuysRedeemTierDto redeemTier : redeemTiers) {
            if (redeemTier.getRedeemCode().equals(appliedFlybuysDiscount.getRedeemCode())) {
                return createFlybuysRedeemTierData(redeemTier, checkoutCart.getCurrency().getIsocode());
            }
        }

        return null;
    }

    @Override
    public FlybuysDiscountDenialDto getFlybuysDiscountAllowed() {
        return targetVoucherService.getFlybuysDiscountAllowed(getTargetCheckoutFacade().getCart());
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade#reassessFlybuysDiscountOnCheckoutCart()
     */
    @Override
    public boolean reassessFlybuysDiscountOnCheckoutCart() {
        final CartModel cartModel = getTargetCheckoutFacade().getCart();

        if (cartModel == null) {
            return false;
        }
        final Collection<DiscountModel> appliedVouchers = targetVoucherService.getAppliedVouchers(cartModel);
        CollectionUtils.filter(appliedVouchers, InstanceofPredicate.getInstance(FlybuysDiscountModel.class));

        if (CollectionUtils.isEmpty(appliedVouchers)) {
            // There is no flybuys discount, so nothing changed
            return false;
        }

        final FlybuysRedemptionData flybuysRedemptionData = getFlybuysRedemptionDataForCart(cartModel);
        final FlybuysDiscountModel appliedFlybuysDiscount = (FlybuysDiscountModel)appliedVouchers.iterator().next();

        if (flybuysRedemptionData != null && CollectionUtils.isNotEmpty(flybuysRedemptionData.getRedeemTiers())
                && null == getFlybuysDiscountAllowed()) {
            for (final FlybuysRedeemTierData redeemTier : flybuysRedemptionData.getRedeemTiers()) {
                if (redeemTier.getRedeemCode().equals(appliedFlybuysDiscount.getRedeemCode())) {
                    return false;
                }
            }
        }
        return removeFlybuysDiscountFromCheckoutCart();
    }

    protected FlybuysRedemptionData getFlybuysRedemptionDataForCart(final CartModel cart) {
        final FlybuysAuthenticateResponseDto flybuysAuthenticateResponseDto = sessionService
                .getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE);

        if (flybuysAuthenticateResponseDto == null) {
            return null;
        }

        final FlybuysRedemptionData flybuysRedemptionData = new FlybuysRedemptionData();

        flybuysRedemptionData.setAvailablePoints(flybuysAuthenticateResponseDto.getAvailPoints());

        final List<FlybuysRedeemTierData> flybuysRedeemTierDataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(flybuysAuthenticateResponseDto.getRedeemTiers())) {
            final String currencyIsoCode = cart.getCurrency().getIsocode();

            //Flybuys redemption is not applicable for gift cards in the cart, hence excluding the values from cart sub-total.
            final BigDecimal cartSubTotal = BigDecimal.valueOf(cart.getSubtotal().doubleValue())
                    .subtract(giftCardAmount(cart));

            final BigDecimal flybuysCartThresholdLimit = configurationService.getConfiguration().getBigDecimal(
                    TgtFacadesConstants.FLYBUYS_CART_THRESHOLD_LIMIT, BigDecimal.ZERO);
            final BigDecimal cartTotalWithThreshold = cartSubTotal.subtract(flybuysCartThresholdLimit);

            for (final FlybuysRedeemTierDto flybuysRedeemTier : flybuysAuthenticateResponseDto.getRedeemTiers()) {
                if (!(cartTotalWithThreshold.compareTo(flybuysRedeemTier.getDollarAmt()) > 0)) {
                    continue;
                }

                flybuysRedeemTierDataList.add(createFlybuysRedeemTierData(flybuysRedeemTier, currencyIsoCode));
            }

            Collections.sort(flybuysRedeemTierDataList, new Comparator<FlybuysRedeemTierData>() {

                /* (non-Javadoc)
                 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
                 */
                @Override
                public int compare(final FlybuysRedeemTierData o1, final FlybuysRedeemTierData o2) {
                    return o2.getPoints().compareTo(o1.getPoints());
                }

            });
        }
        flybuysRedemptionData.setRedeemTiers(flybuysRedeemTierDataList);

        return flybuysRedemptionData;
    }

    /**
     * Method to check all the gift cards present in the cart and and sum the total price of gift cards present.
     *
     * @param cart
     * @return BigDecimal
     */
    private BigDecimal giftCardAmount(final CartModel cart) {
        BigDecimal totalPriceForGiftCards = BigDecimal.valueOf(0.0);
        for (final AbstractOrderEntryModel orderEntry : cart.getEntries()) {
            final ProductModel productModel = orderEntry.getProduct();
            if (ProductUtil.isProductTypeDigital(productModel)
                    || ProductUtil.isProductTypePhysicalGiftcard(productModel)) {
                totalPriceForGiftCards = totalPriceForGiftCards
                        .add(BigDecimal.valueOf(orderEntry.getTotalPrice().doubleValue()));
            }
        }
        return totalPriceForGiftCards;
    }

    @Override
    public boolean checkFlybuysDiscountUsed() {
        final CartModel cartModel = getTargetCheckoutFacade().getCart();
        if (null != targetVoucherService.getFlybuysDiscountForOrder(cartModel)) {
            return true;
        }
        return false;
    }

    private FlybuysRedeemTierData createFlybuysRedeemTierData(final FlybuysRedeemTierDto flybuysRedeemTierDto,
            final String currencyIsoCode) {
        if (flybuysRedeemTierDto == null) {
            return null;
        }

        final FlybuysRedeemTierData flybuysRedeemTierData = new FlybuysRedeemTierData();

        flybuysRedeemTierData.setPoints(flybuysRedeemTierDto.getPoints());
        flybuysRedeemTierData.setRedeemCode(flybuysRedeemTierDto.getRedeemCode());

        flybuysRedeemTierData.setDollars(getPriceDataFactory().create(PriceDataType.BUY,
                flybuysRedeemTierDto.getDollarAmt(), currencyIsoCode));

        return flybuysRedeemTierData;
    }

    /**
     * @return the priceDataFactory
     */
    protected PriceDataFactory getPriceDataFactory() {
        return priceDataFactory;
    }

    /**
     * @param priceDataFactory
     *            the priceDataFactory to set
     */
    @Required
    public void setPriceDataFactory(final PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }

    /**
     * @return the targetCheckoutFacade
     */
    protected TargetCheckoutFacade getTargetCheckoutFacade() {
        return targetCheckoutFacade;
    }

    /**
     * @param targetCheckoutFacade
     *            the targetCheckoutFacade to set
     */
    @Required
    public void setTargetCheckoutFacade(final TargetCheckoutFacade targetCheckoutFacade) {
        this.targetCheckoutFacade = targetCheckoutFacade;
    }

    /**
     * @param sessionService
     *            the sessionService to set
     */
    @Required
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

    /**
     * @param targetVoucherService
     *            the targetVoucherService to set
     */
    @Required
    public void setTargetVoucherService(final TargetVoucherService targetVoucherService) {
        this.targetVoucherService = targetVoucherService;
    }

    /**
     * @param flybuysDiscountService
     *            the flybuysDiscountService to set
     */
    @Required
    public void setFlybuysDiscountService(final FlybuysDiscountService flybuysDiscountService) {
        this.flybuysDiscountService = flybuysDiscountService;
    }

    /**
     * @param flybuysRedeemConfigService
     *            the flybuysRedeemConfigService to set
     */
    @Required
    public void setFlybuysRedeemConfigService(final FlybuysRedeemConfigService flybuysRedeemConfigService) {
        this.flybuysRedeemConfigService = flybuysRedeemConfigService;
    }

    /**
     * @return the flybuysRedeemConfigConverter
     */
    protected Converter<FlybuysRedeemConfigModel, FlybuysRedeemConfigData> getFlybuysRedeemConfigConverter() {
        return flybuysRedeemConfigConverter;
    }

    /**
     * @param flybuysRedeemConfigConverter
     *            the flybuysRedeemConfigConverter to set
     */
    @Required
    public void setFlybuysRedeemConfigConverter(
            final Converter<FlybuysRedeemConfigModel, FlybuysRedeemConfigData> flybuysRedeemConfigConverter) {
        this.flybuysRedeemConfigConverter = flybuysRedeemConfigConverter;
    }


    /**
     * @return the modelService
     */

    public ModelService getModelService() {
        return modelService;
    }


    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param commerceCartService
     *            the commerceCartService to set
     */
    @Required
    public void setCommerceCartService(final CommerceCartService commerceCartService) {
        this.commerceCartService = commerceCartService;
    }

    /**
     * @param configurationService
     *            the configurationService to set
     */
    @Required
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

}
