/**
 * 
 */
package au.com.target.tgtfacades.user.data;

/**
 * @author asingh78
 * 
 */
public class PaypalInfoData {
    public static final String PAYPAL = "PayPal";
    private String paypalEmailId;

    /**
     * @return the paymentProvider
     */
    public String getPaymentProvider() {
        return PAYPAL;
    }

    /**
     * @return the paypalEmailId
     */
    public String getPaypalEmailId() {
        return paypalEmailId;
    }

    /**
     * @param paypalEmailId
     *            the paypalEmailId to set
     */
    public void setPaypalEmailId(final String paypalEmailId) {
        this.paypalEmailId = paypalEmailId;
    }

}
