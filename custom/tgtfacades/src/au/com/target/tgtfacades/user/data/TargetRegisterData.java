/**
 * 
 */
package au.com.target.tgtfacades.user.data;

import de.hybris.platform.commercefacades.user.data.RegisterData;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class TargetRegisterData extends RegisterData {

    private String mobileNumber;
    private String uid;

    /**
     * @return the mobileNumber
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * @param mobileNumber
     *            the mobileNumber to set
     */
    public void setMobileNumber(final String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * @return the uid
     */
    public String getUid() {
        return uid;
    }

    /**
     * @param uid
     *            the uid to set
     */
    public void setUid(final String uid) {
        this.uid = uid;
    }
}
