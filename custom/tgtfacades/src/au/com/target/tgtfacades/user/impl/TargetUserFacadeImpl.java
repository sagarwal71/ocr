/**
 * 
 */
package au.com.target.tgtfacades.user.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.impl.DefaultUserFacade;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtmarketing.segments.TargetUserSegmentService;
import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;


/**
 * @author asingh78
 * 
 */


public class TargetUserFacadeImpl extends DefaultUserFacade implements TargetUserFacade {

    private static final Logger LOG = Logger.getLogger(TargetUserFacadeImpl.class);
    private static final String ERROR_IPG_CREDIT_CARD_DATE = "Could not convert the IPG credit card date for the ipgCreditCardPaymentInfoModel pk={0} ";
    private static final DateTimeFormatter DATA_TIME_FORMATTER = new DateTimeFormatterBuilder().appendMonthOfYear(2)
            .appendTwoDigitYear(2050).toFormatter().withLocale(Locale.US);

    private TargetUserSegmentService targetUserSegmentService;

    @Override
    public void addAddress(final AddressData addressData)
    {
        validateParameterNotNullStandardMessage("addressData", addressData);

        final CustomerModel currentCustomer = getCheckoutCustomerStrategy().getCurrentUserForCheckout();

        final boolean makeThisAddressTheDefault = addressData.isDefaultAddress() || getAddressBook().isEmpty()
                || currentCustomer.getDefaultShipmentAddress() == null;

        // Create the new address model
        final TargetAddressModel newAddress = getModelService().create(TargetAddressModel.class);
        getAddressReversePopulator().populate(addressData, newAddress);

        // Store the address against the user
        getCustomerAccountService().saveAddressEntry(currentCustomer, newAddress);

        // Update the address ID in the newly created address
        addressData.setId(newAddress.getPk().toString());

        if (makeThisAddressTheDefault)
        {
            getCustomerAccountService().setDefaultAddressEntry(currentCustomer, newAddress);
        }
    }

    @Override
    public void editAddress(final AddressData addressData)
    {
        validateParameterNotNullStandardMessage("addressData", addressData);
        final CustomerModel currentCustomer = getCheckoutCustomerStrategy().getCurrentUserForCheckout();
        final AddressModel addressModel = getCustomerAccountService().getAddressForCode(currentCustomer,
                addressData.getId());
        getAddressReversePopulator().populate(addressData, addressModel);
        getCustomerAccountService().saveAddressEntry(currentCustomer, addressModel);
        if (addressData.isDefaultAddress())
        {
            getCustomerAccountService().setDefaultAddressEntry(currentCustomer, addressModel);
        }
        else if (addressModel.equals(currentCustomer.getDefaultShipmentAddress()))
        {
            getCustomerAccountService().clearDefaultAddressEntry(currentCustomer);
        }
    }

    @Override
    public AddressData getDefaultAddress() {
        final CustomerModel currentCustomer = (CustomerModel)getUserService().getCurrentUser();
        AddressData defaultAddressData = null;

        final AddressModel defaultAddress = getCustomerAccountService().getDefaultAddress(currentCustomer);
        if (defaultAddress != null)
        {
            defaultAddressData = getAddressConverter().convert(defaultAddress);
            return defaultAddressData;
        }

        return null;
    }

    @Override
    public boolean isAddressDefault(final String addressId) {
        if (StringUtils.isBlank(addressId)) {
            return false;
        }

        final AddressModel address = getCustomerAccountService().getAddressForCode(
                (CustomerModel)getUserService().getCurrentUser(),
                addressId);
        if (address == null) {
            return false;
        }

        final AddressModel defaultAddress = getCustomerAccountService().getDefaultAddress(
                (CustomerModel)getUserService().getCurrentUser());
        if (defaultAddress == null) {
            return false;
        }

        return defaultAddress.equals(address);
    }

    @Override
    public boolean isCurrentUserAnonymous() {
        return getUserService().isAnonymousUser(getUserService().getCurrentUser());
    }

    @Override
    protected void updateDefaultPaymentInfo(final CustomerModel currentCustomer)
    {
        if (currentCustomer.getDefaultPaymentInfo() == null)
        {
            final List<CreditCardPaymentInfoModel> ccPaymentInfoModelList = getCustomerAccountService()
                    .getCreditCardPaymentInfos(
                            currentCustomer, true);
            if (CollectionUtils.isNotEmpty(ccPaymentInfoModelList))
            {
                getCustomerAccountService().setDefaultPaymentInfo(currentCustomer,
                        ccPaymentInfoModelList.get(ccPaymentInfoModelList.size() - 1));
            }
        }
    }

    @Override
    public boolean hasSavedValidIpgCreditCards() {
        final List<CreditCardPaymentInfoModel> creditCardPaymentInfoList = getCustomerAccountService()
                .getCreditCardPaymentInfos((CustomerModel)getUserService().getCurrentUser(), true);
        if (CollectionUtils.isNotEmpty(creditCardPaymentInfoList)) {
            for (final CreditCardPaymentInfoModel creditCardPaymentInfoModel : creditCardPaymentInfoList) {
                if (creditCardPaymentInfoModel instanceof IpgCreditCardPaymentInfoModel) {
                    final IpgCreditCardPaymentInfoModel ipgCreditCardPaymentInfoModel = (IpgCreditCardPaymentInfoModel)creditCardPaymentInfoModel;

                    try {
                        final DateTime expiredDate = DateTime
                                .parse(ipgCreditCardPaymentInfoModel.getValidToMonth()
                                        + ipgCreditCardPaymentInfoModel.getValidToYear(),
                                        DATA_TIME_FORMATTER);
                        if (expiredDate.plusMonths(1).isAfterNow()) {
                            return true;
                        }
                    }
                    catch (final IllegalArgumentException e) {
                        LOG.error(
                                MessageFormat.format(ERROR_IPG_CREDIT_CARD_DATE, ipgCreditCardPaymentInfoModel.getPk()),
                                e);
                    }
                }
            }
        }
        return false;
    }

    @Override
    public List<CCPaymentInfoData> getSavedCreditCardPaymentInfosInValidTime() {
        final CustomerModel currentCustomer = getCurrentUserForCheckout();
        final List<CreditCardPaymentInfoModel> creditCards = getCustomerAccountService().getCreditCardPaymentInfos(
                currentCustomer,
                true);
        final List<CCPaymentInfoData> ccPaymentInfos = new ArrayList<>();
        final PaymentInfoModel defaultPaymentInfoModel = currentCustomer.getDefaultPaymentInfo();
        for (final CreditCardPaymentInfoModel ccPaymentInfoModel : creditCards)
        {
            try {
                final DateTime expiredDate = DateTime
                        .parse(ccPaymentInfoModel.getValidToMonth() + ccPaymentInfoModel.getValidToYear(),
                                DATA_TIME_FORMATTER);
                if (expiredDate.plusMonths(1).isAfterNow()) {

                    final CCPaymentInfoData defaultPaymentInfoData = getCreditCardPaymentInfoConverter().convert(
                            ccPaymentInfoModel);
                    if (ccPaymentInfoModel.equals(defaultPaymentInfoModel))
                    {
                        defaultPaymentInfoData.setDefaultPaymentInfo(true);
                    }
                    ccPaymentInfos.add(defaultPaymentInfoData);
                }
            }
            catch (final IllegalArgumentException e) {
                LOG.error(
                        MessageFormat.format(ERROR_IPG_CREDIT_CARD_DATE, ccPaymentInfoModel.getPk()),
                        e);
            }
        }
        return ccPaymentInfos;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.user.TargetUserFacade#getAllUserSegmentsForUser(de.hybris.platform.core.model.user.UserModel)
     */
    @Override
    public List<String> getAllUserSegmentsForCurrentUser() {
        final UserModel currentUser = getUserService().getCurrentUser();
        if (currentUser != null) {
            final List<PrincipalGroupModel> userSegments = targetUserSegmentService
                    .getAllUserSegmentsForUser(currentUser);
            final List<String> strSegments = new ArrayList<>();
            if (CollectionUtils.isNotEmpty(userSegments)) {
                for (final PrincipalGroupModel segment : userSegments) {
                    strSegments.add(segment.getUid());
                }
                Collections.sort(strSegments);
                return strSegments;
            }
        }
        return ListUtils.EMPTY_LIST;
    }

    /**
     * @param targetUserSegmentService
     *            the targetUserSegmentService to set
     */
    @Required
    public void setTargetUserSegmentService(final TargetUserSegmentService targetUserSegmentService) {
        this.targetUserSegmentService = targetUserSegmentService;
    }
}