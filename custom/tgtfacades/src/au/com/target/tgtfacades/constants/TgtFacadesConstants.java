/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtfacades.constants;

import java.math.BigDecimal;


/**
 * Global class for all TgtFacades constants.
 */
@SuppressWarnings("deprecation")
public class TgtFacadesConstants extends GeneratedTgtFacadesConstants {

    public static final String EXTENSIONNAME = "tgtfacades";
    public static final String ACTIVE_PURCHASE_OPTION = "active_purchase_option";
    public static final String CREDIT_CARD = "creditcard";
    public static final String PAYPAL = "paypal";
    public static final String AFTERPAY = "afterpay";
    public static final String ZIPPAY = "zippay";
    public static final String ZIPPAY_LOG_FORMAT = "PAYMENT-Zippay";
    public static final String PINPAD = "pinpad";
    public static final String IPG = "ipg";
    public static final String ZIP = "zip";
    public static final String GIFT_CARD = "giftcard";
    public static final String ORDER_BARCODE = "/order-barcode/";
    public static final String BARCODE_PNG_FORMAT = "png";
    public static final String BARCODE_SVG_FORMAT = "svg";
    public static final String ORDER_NUMBER = "?orderId=";
    public static final String ENCOD = "UTF-8";
    public static final String LOGIN_CART_MODIFICATIONS_SESSION_ATTRIBUTE = "LOGIN_CART_MODIFICATIONS";

    public static final String ASSISTED_CHECKOUT_USER_SESSION_KEY = "assisted-checkout-user";

    public static final String FLYBUYS_AUTHENTICATION_RESPONSE = "flybuys-authentication-response";

    public static final String NEW_ARRIVALS = "New Arrivals";

    public static final String NO_DEL_MODE_VALUE_LONG_MSG = "tgtcore.deliveryModeData.longDesc";

    public static final String FREE_LABEL = "FREE ";

    public static final String ANONYMOUS_CHECKOUT = "anonymous_checkout";

    public static final String CHECKOUT_REGISTERED_AS_GUEST = "checkout_registered_as_guest";

    public static final String BULKY_BOARD_STORE_NUMBER = "bbsn=";

    public static final String IOS_APP_WV = "iosAppWV";

    public static final BigDecimal AFTER_PAY_INSTALLMENTS = BigDecimal.valueOf(4);

    public static final String FLYBUYS_CART_THRESHOLD_LIMIT = "tgtfacades.flybuysCartThresholdLimit";
    public static final String GIFT_CARD_QUANTITY_INCART_LIMIT = "tgtfacades.giftcard.product.quantityInCartLimit";

    public static final String NO_DEL_MODE_VALUE_LONG_MSG_WITH_GIFTCARD = "tgtcore.deliveryModeData.longDesc.bulkybasket.with.giftcard";
    public static final String DIGITAL_GIFT_CARD_DELIVERY = "digital-gift-card";

    public static final String IPG_BASE_URL = "tgtpaymentprovider.ipg.ipgSessionUrl";

    public static final String IMAGE_WIDE_FORMAT = "wide";

    public static final String ZIP_PAYMENT_THRESHOLD_AMOUNT = "zipPayment.thresholdAmount";

    public interface WebServiceError {
        String ERR_LOGIN = "ERR_LOGIN_";
        String ERR_PASSWORDRESET = "ERR_PASSWORDRESET_";
        String ERR_LOGIN_REQUIRED = ERR_LOGIN + "REQUIRED";
        String ERR_LOGIN_BAD_CREDENTIALS = ERR_LOGIN + "BAD_CREDENTIALS";
        String ERR_LOGIN_ACCOUNT_LOCKED = ERR_LOGIN + "ACCOUNT_LOCKED";
        String ERR_LOGIN_UNKNOWN = ERR_LOGIN + "UNKNOWN";

        String ERR_LOGIN_GUEST_NOT_ALLOWED = ERR_LOGIN + "GUEST_NOT_ALLOWED";
        String ERR_LOGIN_GUEST_NO_GIFT_CARDS = ERR_LOGIN + "GUEST_NO_GIFT_CARDS";
        String ERR_LOGIN_GUEST_NO_PREORDERS = ERR_LOGIN + "GUEST_NO_PREORDERS";
        String ERR_LOGIN_GUEST_USER_EXISTS = ERR_LOGIN + "GUEST_USER_EXISTS";

        String ERR_LOGIN_REGISTER_USER_EXISTS = ERR_LOGIN + "REGISTER_USER_EXISTS";
        String ERR_LOGIN_REGISTER_WEAK_PASSWORD = ERR_LOGIN + "REGISTER_WEAK_PASSWORD";
        String ERR_PASSWORDRESET_INVALID_TOKEN = ERR_PASSWORDRESET + "INVALID_TOKEN";
        String ERR_PASSWORDRESET_REUSE_PASSWORD = ERR_PASSWORDRESET + "REUSE_PASSWORD";
        String ERR_PASSWORDRESET_GENERAL = ERR_PASSWORDRESET + "GENERAL";
        String ERR_EMPTY_VARIANTS = "ERR_EMPTY_VARIANTS";
        String ERR_EMPTY_BASE_PRODUCT = "ERR_EMPTY_BASE_PRODUCT";
        String ERR_BASE_PRODUCT_UNKNOWN = "ERR_BASE_PRODUCT_UNKNOWN";
        String ERR_EMPTY_OPTIONS = "ERR_EMPTY_OPTIONS";
        String ERR_STOCK_LOOKUP = "ERR_STOCK_LOOKUP";
    }

    public interface WebServiceMessage {
        String ERR_MESS_PASSWORDRESET_GENERAL = "We're sorry, there was an error with your previous request to reset your password."
                + " Please submit your email address below to try again.";
        String ERR_MESS_PASSWORDRESET_REUSE_PASSWORD = "Please enter a new password that is not the same as your previous password.";
        String ERR_EMPTY_VARIANTS = "variants can't be empty";
        String ERR_EMPTY_BASE_PRODUCT = "base product can't be empty";
        String ERR_BASE_PRODUCT_UNKNOWN = "base product unknown";
        String ERR_EMPTY_OPTIONS = "deliveryTypes and locations both can't be empty";
        String ERR_STOCK_LOOKUP = "Failed to lookup stock";
    }

    public interface Page {
        String MY_ACCOUNT = "/my-account";
        String SINGLE_PAGE_CHECKOUT = "/spc";
        String SINGLE_PAGE_CHECKOUT_AND_LOGIN = SINGLE_PAGE_CHECKOUT + "/order";
    }

    public interface CustomHeaders {
        String X_TARGET_HEADER = "x-target";
        String X_APP_PLATFORM_HEADER = "x-app-platform";
        String X_APP_VERSION_HEADER = "x-app-version";

        String KIOSK_MODE = "kiosk";
        String MOBILE_APP = "mobile-app";
        String IOS = "ios";
    }

    public interface ShortLeadTime {
        String LEADTIME_HD_SHORT = "leadtime.hd.short.";
        String LEADTIME_ED_SHORT = "leadtime.ed.short.";
        String LEADTIME_BULKY1_SHORT = "leadtime.bulky1.short.";
        String LEADTIME_BULKY2_SHORT = "leadtime.bulky2.short.";
        String LEADTIME_DEFAULT_CONFIG_CODE = "default.short.leadtime";
        int DEFAULT_SHORT_LEAD_TIME_VALUE = 3;
    }

    private TgtFacadesConstants() {
        //empty
    }
}
