/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtfacades.constants;

public interface ThirdPartyConstants {
    interface Analytics {
        String UNIVERSAL_ACCOUNT_ID = "tgtstorefront.universalanalytics.id";
        String UNIVERSAL_KIOSK_ACCOUNT_ID = "tgtstorefront.universalanalytics.kiosk.id";
        String UNIVERSAL_MOBILEAPP_ACCOUNT_ID = "tgtstorefront.universalanalytics.mobileapp.id";
        String DISPLAY_ADVERTISER_SUPPORT_ENABLED = "tgtstorefront.analytics.displayadvertisersupport";
        String UNIVERSAL_ANALYTICS_COOKIE_DOMAIN = "tgtstorefront.universalanalytics.cookie.domain";
    }

    interface TagManager {
        String CONTAINER = "tgtstorefront.tagmanager.container";
    }

    interface GOptimize {
        String CONTAINER = "tgtstorefront.goptimize.container";
    }

    interface GMaps {
        String API_VERSION = "tgtstorefront.gmaps.api.version";
        String API_CLIENT = "tgtstorefront.gmaps.api.client";
    }

    interface AddThis {
        String PUB_ID = "tgtstorefront.addthis.pubid";
    }

    interface BazaarVoice {
        String API_URI = "tgtstorefront.bazaarvoice.api.uri";
    }

    interface Android {
        String SMARTAPPBANNER_APP_ID = "tgtstorefront.smartappbanner.android.appid";
        String SMARTAPPBANNER_APP_SCHEME = "tgtstorefront.smartappbanner.android.scheme";
        String SMARTAPPBANNER_APP_STORE_URL = "tgtstorefront.smartappbanner.android.appstoreurl";
    }

    interface Apple {
        String SMARTAPPBANNER_APP_ID = "tgtstorefront.smartappbanner.apple.appid";
        String SMARTAPPBANNER_APP_SCHEME = "tgtstorefront.smartappbanner.apple.scheme";
        String SMARTAPPBANNER_APP_STORE_URL = "tgtstorefront.smartappbanner.apple.appstoreurl";
    }
}
