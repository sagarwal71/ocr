/**
 * 
 */
package au.com.target.tgtfacades.storefinder.converters;

import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.converters.impl.AbstractConverter;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemResponseDto;
import au.com.target.tgtfacades.config.TargetSharedConfigFacade;
import au.com.target.tgtfacades.storefinder.data.StoreStockVisiblityHolder;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceStockData;
import au.com.target.tgtfacades.util.TargetPointOfServiceHelper;


/**
 * @author htan3
 */
public class TargetProductStoreStockConverter extends
        AbstractConverter<StoreStockVisiblityHolder, TargetPointOfServiceStockData> {
    private Populator<PointOfServiceDistanceData, PointOfServiceData> pointOfServiceDistanceDataPopulator;
    private Converter<PointOfServiceModel, PointOfServiceData> pointOfServiceConverter;
    private TargetSharedConfigFacade targetSharedConfigFacade;
    private String[] stockStatusLevel;
    private TargetPointOfServiceHelper targetPointOfServiceHelper;

    @Override
    public void populate(final StoreStockVisiblityHolder storeStockVisiblityHolder,
            final TargetPointOfServiceStockData pointOfServiceStockData) {
        Assert.notNull(storeStockVisiblityHolder, "storeStockVisiblityHolder cannot be null");
        final PointOfServiceDistanceData pointOfServiceDistanceData = storeStockVisiblityHolder
                .getPointOfServiceDistanceData();
        Assert.notNull(pointOfServiceDistanceData, "pointOfServiceDistanceData cannot be null");
        Assert.notNull(pointOfServiceDistanceData.getPointOfService(), "pointOfService cannot be null");
        pointOfServiceConverter.convert(pointOfServiceDistanceData.getPointOfService(), pointOfServiceStockData);
        pointOfServiceDistanceDataPopulator.populate(pointOfServiceDistanceData, pointOfServiceStockData);
        targetPointOfServiceHelper.populateDirectionUrl(storeStockVisiblityHolder.getLocationText(),
                pointOfServiceStockData);
        populateStock(storeStockVisiblityHolder, pointOfServiceStockData);
    }

    /**
     * @param storeStockVisiblityHolder
     * @param pointOfServiceStockData
     */
    protected void populateStock(final StoreStockVisiblityHolder storeStockVisiblityHolder,
            final TargetPointOfServiceStockData pointOfServiceStockData) {
        final StockVisibilityItemLookupResponseDto stockLookupResult = storeStockVisiblityHolder.getStockLookupResult();
        if (stockLookupResult != null) {
            final List<StockVisibilityItemResponseDto> items = stockLookupResult.getItems();
            if (items != null) {
                for (final StockVisibilityItemResponseDto item : items) {
                    if (item.getCode().equals(
                            storeStockVisiblityHolder.getProduct().getCode())
                            && item.getStoreNumber().equals(pointOfServiceStockData.getStoreNumber().toString())) {
                        final String soh = item.getSoh();
                        pointOfServiceStockData.setStockLevel(intepretStockLevel(soh));
                        return;
                    }
                }
            }
        }
    }

    /**
     * @param soh
     * @return String indicating whether no,low or high stock
     */
    private String intepretStockLevel(final String soh) {
        if (StringUtils.isNotEmpty(soh) && StringUtils.isNumeric(soh)) {
            final int stock = Integer.parseInt(soh);
            if (stock > getLimitedStockThreshold()) {
                return stockStatusLevel[2];
            }
            if (stock <= getLimitedStockThreshold() && stock > getNoStockThreshold()) {
                return stockStatusLevel[1];
            }
        }
        return stockStatusLevel[0];
    }

    private int getLimitedStockThreshold() {
        final String limitedStoreThreshold = targetSharedConfigFacade
                .getConfigByCode(TgtCoreConstants.Config.INSTORE_STOCK_LIMITED_THRESHOLD);
        return limitedStoreThreshold != null ? Integer
                .parseInt(limitedStoreThreshold) : 8;
    }

    private int getNoStockThreshold() {
        final String limitedStoreThreshold = targetSharedConfigFacade
                .getConfigByCode(TgtCoreConstants.Config.INSTORE_NO_STOCK_THRESHOLD);
        return limitedStoreThreshold != null ? Integer
                .parseInt(limitedStoreThreshold) : 8;
    }



    @Required
    public void setPointOfServiceDistanceDataPopulator(
            final Populator<PointOfServiceDistanceData, PointOfServiceData> pointOfServiceDistanceDataPopulator) {
        this.pointOfServiceDistanceDataPopulator = pointOfServiceDistanceDataPopulator;
    }

    @Required
    public void setPointOfServiceConverter(
            final Converter<PointOfServiceModel, PointOfServiceData> pointOfServiceConverter) {
        this.pointOfServiceConverter = pointOfServiceConverter;
    }


    /**
     * @param stockStatusLevel
     *            the stockStatusLevel to set
     */
    @Required
    public void setStockStatusLevel(final String[] stockStatusLevel) {
        this.stockStatusLevel = stockStatusLevel;
    }

    /**
     * @param targetSharedConfigFacade
     *            the targetSharedConfigFacade to set
     */
    @Required
    public void setTargetSharedConfigFacade(final TargetSharedConfigFacade targetSharedConfigFacade) {
        this.targetSharedConfigFacade = targetSharedConfigFacade;
    }

    /**
     * @param targetPointOfServiceHelper
     *            the targetPointOfServiceHelper to set
     */
    @Required
    public void setTargetPointOfServiceHelper(final TargetPointOfServiceHelper targetPointOfServiceHelper) {
        this.targetPointOfServiceHelper = targetPointOfServiceHelper;
    }



}
