/**
 * 
 */
package au.com.target.tgtfacades.storefinder.impl;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtcore.storefinder.TargetStoreFinderService;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtfacades.config.TargetSharedConfigFacade;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.HomeDeliveryLocation;
import au.com.target.tgtfacades.response.data.LocationResponseData;
import au.com.target.tgtfacades.response.data.ProductStockSearchResultResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.storefinder.TargetStoreFinderStockFacade;
import au.com.target.tgtfacades.storefinder.data.StoreStockVisiblityHolder;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceStockData;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.service.FluentStockLookupService;
import au.com.target.tgtsale.stock.service.TargetStoreStockService;
import au.com.target.tgtutility.util.TargetDateUtil;


/**
 * @author htan3
 *
 */
@SuppressWarnings("deprecation")
public class TargetStoreFinderStockFacadeImpl implements TargetStoreFinderStockFacade {

    private static final String STOCK_ERROR_TRYAGAIN = "Sorry, there was a problem locating stores. Please try again.";

    private static final String STOCK_ERROR = "Sorry, there was a problem locating stores.";

    private static final String STOCK_SERVICE_DISABLED = "Sorry, the store stock service is disabled.";

    private static final String CLICK_AND_COLLECT = "cnc";

    private static final String HOME_DELIVERY = "hd";

    private BaseStoreService baseStoreService;

    private TargetStoreFinderService<PointOfServiceDistanceData, StoreFinderSearchPageData<PointOfServiceDistanceData>> storeFinderService;

    private ProductService productService;

    private Converter<StoreStockVisiblityHolder, TargetPointOfServiceStockData> storeStockConverter;

    private TargetStoreStockService targetStoreStockService;

    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    private TargetPointOfServiceService targetPointOfServiceService;

    private FluentStockLookupService fluentStockLookupService;

    private String[] stockStatusLevel;

    private TargetSharedConfigFacade targetSharedConfigFacade;

    @Override
    public Response doSearchProductStock(final String productCode, final String location, final Double latitude,
            final Double longitude, final Integer storeNumber, final PageableData pageableData,
            final boolean moveNearestInStockStoreToTop) {
        if (!targetFeatureSwitchFacade.isInStoreStockVisibilityEnabled()) {
            return createErrorResponse("ERR_CHECK_STOCK_SERVICE_ERROR", STOCK_SERVICE_DISABLED);
        }

        final StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageData = searchStoreByLocationOrPosition(
                location, latitude, longitude, storeNumber, pageableData);

        if (storeFinderSearchPageData == null
                || CollectionUtils.isEmpty(storeFinderSearchPageData.getResults())) {
            return createErrorResponse("ERR_CHECK_STOCK_LOCATION_NOTFOUND", STOCK_ERROR_TRYAGAIN);
        }

        final StoreStockVisiblityHolder storeStockHolder = new StoreStockVisiblityHolder();

        if (StringUtils.isNotBlank(productCode)) {
            addStockForProductInStores(productCode, storeFinderSearchPageData, storeStockHolder);

            if (storeStockHolder.getStockLookupResult() == null) {
                return createErrorResponse("ERR_CHECK_STOCK_SERVICE_ERROR", STOCK_ERROR_TRYAGAIN);
            }
            else if (CollectionUtils.isEmpty(storeStockHolder.getStockLookupResult().getItems())) {
                return createErrorResponse("ERR_CHECK_STOCK_SERVICE_FAILURE", STOCK_ERROR);
            }
        }

        return checkStock(storeFinderSearchPageData, storeStockHolder, moveNearestInStockStoreToTop);
    }

    @Override
    public Response nearestStoreLocation(final String deliveryType, final String location,
            final Integer storeNumber,
            final PageableData pageableData) {
        StoreFinderSearchPageData<PointOfServiceDistanceData> storeLocationData = null;
        if (CLICK_AND_COLLECT.equalsIgnoreCase(deliveryType)) {
            storeLocationData = searchCncStoreByLocationOrStoreNumber(
                    location, storeNumber, pageableData);
        }
        else if (HOME_DELIVERY.equalsIgnoreCase(deliveryType)) {
            storeLocationData = searchStoreByLocationOrPosition(
                    location, null, null, storeNumber, pageableData);
        }
        if (!isPOSDataAvailable(storeLocationData)) {
            return createErrorResponse("STORE_LOCATION_NOTFOUND_OR_FOUND_MORE_THAN_ONE_LOCATION", STOCK_ERROR_TRYAGAIN);
        }
        final StoreStockVisiblityHolder storeVisibilityHolder = new StoreStockVisiblityHolder();
        final LocationResponseData locationResponseData = new LocationResponseData();

        if (storeLocationData != null && storeLocationData.getResults() != null) {
            storeVisibilityHolder.setPointOfServiceDistanceData(storeLocationData.getResults().get(0));
        }
        if (CLICK_AND_COLLECT.equalsIgnoreCase(deliveryType)) {
            locationResponseData.setClickAndCollectPos(storeStockConverter.convert(storeVisibilityHolder));
        }
        else if (HOME_DELIVERY.equalsIgnoreCase(deliveryType)) {
            populateHomeDeliveryLocationData(locationResponseData, storeStockConverter.convert(storeVisibilityHolder));
        }
        final Response response = new Response(true);
        response.setData(locationResponseData);
        return response;
    }

    private boolean isPOSDataAvailable(
            final StoreFinderSearchPageData<PointOfServiceDistanceData> storeLocation) {
        if (storeLocation == null || CollectionUtils.isEmpty(storeLocation.getResults())
                || storeLocation.getResults().size() > 1) {
            return false;
        }
        return true;
    }

    /**
     * Method will set home delivery data(only get postal code and location name). These data will be populated in JSON
     * response.
     *
     * @param locationResponseData
     * @param targetPointOfServiceStockData
     */
    private void populateHomeDeliveryLocationData(final LocationResponseData locationResponseData,
            final TargetPointOfServiceStockData targetPointOfServiceStockData) {
        final HomeDeliveryLocation homeDeliveryLocationData = new HomeDeliveryLocation();
        if (targetPointOfServiceStockData != null) {
            homeDeliveryLocationData.setSuburb(targetPointOfServiceStockData.getName());
            final AddressData addressData = targetPointOfServiceStockData.getAddress();
            if (addressData != null) {
                homeDeliveryLocationData.setPostalCode(addressData.getPostalCode());
            }
            setShortLeadTimesForHomeDelivery(homeDeliveryLocationData, targetPointOfServiceStockData);
        }
        locationResponseData.setHomeDeliveryPos(homeDeliveryLocationData);
    }

    /**
     * Setting short lead times for the states ACT(Australian Capital Territory), NSW(New South Wales),NT(Northern
     * Territory),QLD(Queensland),SA(South Australia),TAS(Tasmania),VIC(Victoria), WA(Western Australia) across
     * Australia.
     *
     * @param homeDeliveryLocationData
     * @param targetPointOfServiceStockData
     */
    private void setShortLeadTimesForHomeDelivery(final HomeDeliveryLocation homeDeliveryLocationData,
            final TargetPointOfServiceStockData targetPointOfServiceStockData) {
        final TargetAddressData targetAddressData = targetPointOfServiceStockData.getTargetAddressData();
        if (targetAddressData == null) {
            return;
        }
        Assert.notNull(targetAddressData.getState(), "State can not be null!");
        final String state = targetAddressData.getState().toLowerCase();
        final List<String> stateList = Arrays.asList("act", "nsw", "nt", "qld", "sa", "tas", "vic", "wa");
        if (StringUtils.isNotEmpty(state) && stateList.contains(state)) {
            final int defaultShortLeadTime = getShortLeadTime(
                    TgtFacadesConstants.ShortLeadTime.LEADTIME_DEFAULT_CONFIG_CODE, null,
                    TgtFacadesConstants.ShortLeadTime.DEFAULT_SHORT_LEAD_TIME_VALUE);
            homeDeliveryLocationData.setHdShortLeadTime(
                    getShortLeadTime(TgtFacadesConstants.ShortLeadTime.LEADTIME_HD_SHORT, state, defaultShortLeadTime));
            homeDeliveryLocationData.setEdShortLeadTime(
                    getShortLeadTime(TgtFacadesConstants.ShortLeadTime.LEADTIME_ED_SHORT, state, defaultShortLeadTime));
            homeDeliveryLocationData.setBulky1ShortLeadTime(getShortLeadTime(
                    TgtFacadesConstants.ShortLeadTime.LEADTIME_BULKY1_SHORT, state, defaultShortLeadTime));
            homeDeliveryLocationData.setBulky2ShortLeadTime(getShortLeadTime(
                    TgtFacadesConstants.ShortLeadTime.LEADTIME_BULKY2_SHORT, state, defaultShortLeadTime));
        }
    }

    /**
     * Method to retrieve short lead time.
     * 
     * @param configCode
     * @param state
     * @param defaultShortLeadTime
     * @return int
     */
    private int getShortLeadTime(final String configCode, final String state, final int defaultShortLeadTime) {
        return targetSharedConfigFacade.getInt(StringUtils.isNotEmpty(state) ? configCode.concat(state) : configCode,
                defaultShortLeadTime);
    }

    private void addStockForProductInStores(final String productCode,
            final StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageData,
            final StoreStockVisiblityHolder storeStockHolder) {
        ProductModel productModel = null;
        try {
            productModel = productService.getProductForCode(productCode);
        }
        catch (final UnknownIdentifierException | AmbiguousIdentifierException ex) {
            return;
        }

        final List<PointOfServiceDistanceData> stores = storeFinderSearchPageData.getResults();
        StockVisibilityItemLookupResponseDto stockResult;
        if (targetFeatureSwitchFacade.isFluentEnabled()) {
            try {
                stockResult = fluentStockLookupService.lookupStock(ImmutableList.of(productCode),
                        getStoreNumbers(stores));
            }
            catch (final FluentClientException e) {
                stockResult = null;
            }
        }
        else {
            stockResult = targetStoreStockService.lookupStockForItemsInStores(
                    getStoreNumbers(stores),
                    ImmutableList.of(productCode));
        }

        storeStockHolder.setStockLookupResult(stockResult);
        storeStockHolder.setProduct(productModel);
    }

    /**
     * Check stock for given productCode and list of stores
     * 
     * @param storeFinderSearchPageData
     * @return response with store and stock info
     */
    protected Response checkStock(final StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageData,
            final StoreStockVisiblityHolder storeStockHolder, final boolean moveNearestInStockStoreToTop) {
        final String locationText = storeFinderSearchPageData.getLocationText();
        final List<PointOfServiceDistanceData> stores = storeFinderSearchPageData.getResults();

        final ProductStockSearchResultResponseData responseData = new ProductStockSearchResultResponseData();
        responseData.setSelectedLocation(storeFinderSearchPageData.getLocationText());
        responseData.setAvailabilityTime(TargetDateUtil.getCurrentFormattedDate());

        boolean hasStockInStore = false;

        storeStockHolder.setLocationText(StringUtils.isNotEmpty(locationText) ? locationText : "Current+Location");

        final List<TargetPointOfServiceStockData> storeStockList = new ArrayList<>();
        TargetPointOfServiceStockData topStore = null;
        for (final PointOfServiceDistanceData store : stores) {
            storeStockHolder.setPointOfServiceDistanceData(store);
            final TargetPointOfServiceStockData storeStockData = storeStockConverter.convert(storeStockHolder);
            storeStockList.add(storeStockData);
            if (storeStockData.getStockLevel() == null || storeStockData.getStockLevel().equals(stockStatusLevel[0])
                    || topStore != null) {
                continue;
            }
            if (moveNearestInStockStoreToTop) {
                topStore = storeStockData;
            }
            hasStockInStore = true;
        }
        if (topStore != null) {
            storeStockList.remove(topStore);
            storeStockList.add(0, topStore);
        }

        responseData.setStockAvailable(hasStockInStore);
        responseData.setStockDataIncluded(storeStockHolder.getProduct() != null);

        final Response response = new Response(true);
        responseData.setStores(storeStockList);
        response.setData(responseData);
        return response;
    }

    private StoreFinderSearchPageData<PointOfServiceDistanceData> searchStoreByLocationOrPosition(
            final String location, final Double latitude, final Double longitude, final Integer storeNumber,
            final PageableData pageableData) {
        StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageData = null;
        if (storeNumber != null) {
            try {
                final TargetPointOfServiceModel pos = targetPointOfServiceService.getPOSByStoreNumber(storeNumber);

                final GeoPoint point = createGeoPoint(pos.getLatitude(), pos.getLongitude());
                storeFinderSearchPageData = storeFinderService
                        .positionSearch(baseStoreService.getCurrentBaseStore(), point, pageableData);
            }
            catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException ex) {
                return null;
            }
        }
        else if (latitude != null && longitude != null) {
            final GeoPoint point = createGeoPoint(latitude, longitude);
            storeFinderSearchPageData = storeFinderService
                    .positionSearch(baseStoreService.getCurrentBaseStore(), point, pageableData);

        }
        else if (location != null) {
            storeFinderSearchPageData = storeFinderService
                    .doSearchByLocation(baseStoreService.getCurrentBaseStore(), location, pageableData);
        }
        return storeFinderSearchPageData;
    }

    private StoreFinderSearchPageData<PointOfServiceDistanceData> searchCncStoreByLocationOrStoreNumber(
            final String location, final Integer storeNumber,
            final PageableData pageableData) {
        StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageData = null;
        if (storeNumber != null) {
            try {
                final TargetPointOfServiceModel pointOfService = targetPointOfServiceService
                        .getPOSByStoreNumber(storeNumber);
                final GeoPoint point = createGeoPoint(pointOfService.getLatitude(), pointOfService.getLongitude());
                storeFinderSearchPageData = storeFinderService
                        .doSearchNearestClickAndCollectLocation(location, point,
                                pageableData);
            }
            catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException ex) {
                return null;
            }
        }
        else if (StringUtils.isNotEmpty(location)) {
            storeFinderSearchPageData = storeFinderService
                    .doSearchClickAndCollectStoresByLocation(baseStoreService.getCurrentBaseStore(), location,
                            pageableData);
        }
        return storeFinderSearchPageData;
    }

    protected GeoPoint createGeoPoint(final Double latitude, final Double longitude) {
        final GeoPoint point = new GeoPoint();
        point.setLatitude(latitude.doubleValue());
        point.setLongitude(longitude.doubleValue());
        return point;
    }

    private List<String> getStoreNumbers(final List<PointOfServiceDistanceData> stores) {
        final List<String> storeNumbers = new ArrayList<>();
        for (final PointOfServiceDistanceData store : stores) {
            storeNumbers.add(((TargetPointOfServiceModel)store.getPointOfService()).getStoreNumber().toString());
        }
        return storeNumbers;
    }

    private Response createErrorResponse(final String errCode, final String errMsg) {
        final Response response = new Response(false);
        final au.com.target.tgtfacades.response.data.Error error = new au.com.target.tgtfacades.response.data.Error(
                errCode);
        error.setMessage(errMsg);
        final BaseResponseData responseData = new BaseResponseData();
        responseData.setError(error);
        response.setData(responseData);
        return response;
    }

    @Required
    public void setBaseStoreService(final BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    @Required
    public void setStoreFinderService(
            final TargetStoreFinderService<PointOfServiceDistanceData, StoreFinderSearchPageData<PointOfServiceDistanceData>> storeFinderService) {
        this.storeFinderService = storeFinderService;
    }

    @Required
    public void setProductService(final ProductService productService) {
        this.productService = productService;
    }

    @Required
    public void setStoreStockConverter(
            final Converter<StoreStockVisiblityHolder, TargetPointOfServiceStockData> storeStockConverter) {
        this.storeStockConverter = storeStockConverter;
    }

    @Required
    public void setTargetStoreStockService(final TargetStoreStockService targetStoreStockService) {
        this.targetStoreStockService = targetStoreStockService;
    }

    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }

    @Required
    public void setTargetPointOfServiceService(final TargetPointOfServiceService targetPointOfServiceService) {
        this.targetPointOfServiceService = targetPointOfServiceService;
    }

    /**
     * @param fluentStockLookupService
     *            the fluentStockLookupService to set
     */
    @Required
    public void setFluentStockLookupService(final FluentStockLookupService fluentStockLookupService) {
        this.fluentStockLookupService = fluentStockLookupService;
    }

    /**
     * @param stockStatusLevel
     *            the stockStatusLevel to set
     */
    @Required
    public void setStockStatusLevel(final String[] stockStatusLevel) {
        this.stockStatusLevel = stockStatusLevel;
    }

    /**
     * @param targetSharedConfigFacade
     *            the targetSharedConfigFacade to set
     */
    @Required
    public void setTargetSharedConfigFacade(final TargetSharedConfigFacade targetSharedConfigFacade) {
        this.targetSharedConfigFacade = targetSharedConfigFacade;
    }
}
