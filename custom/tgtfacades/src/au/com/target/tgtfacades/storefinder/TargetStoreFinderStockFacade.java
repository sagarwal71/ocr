/**
 * 
 */
package au.com.target.tgtfacades.storefinder;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;

import au.com.target.tgtfacades.response.data.Response;


/**
 * @author htan3
 *
 */
@SuppressWarnings("deprecation")
public interface TargetStoreFinderStockFacade {

    /**
     * Search stock with nearest stores for the given product.
     * 
     * @param productCode
     * @param location
     * @param latitude
     * @param longitude
     * @param storeNumber
     * @param pageableData
     * @return response with ProductStockSearchResultResponseData
     */
    Response doSearchProductStock(final String productCode, final String location, final Double latitude,
            final Double longitude, Integer storeNumber, final PageableData pageableData,
            final boolean moveNearestInStockStoreToTop);

    /**
     * Search store location based on location and / or store number.
     * 
     * @param deliveryType
     * @param location
     * @param storeNumber
     * @param pageableData
     * @return response with LocationResponseData
     */
    Response nearestStoreLocation(final String deliveryType, final String location, Integer storeNumber,
            final PageableData pageableData);
}
