/**
 * 
 */
package au.com.target.tgtfacades.devicedata.converters;

import de.hybris.platform.acceleratorfacades.device.data.DeviceData;
import de.hybris.platform.commerceservices.enums.UiExperienceLevel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;


/**
 * @author asingh78
 * 
 */
public class TargetDeviceDataUiExperienceLevelConverter implements Converter<DeviceData, UiExperienceLevel>
{
    @Override
    public UiExperienceLevel convert(final DeviceData deviceData) throws ConversionException
    {
        if (deviceData.getDesktopBrowser().booleanValue())
        {
            return UiExperienceLevel.DESKTOP;
        }
        else if (deviceData.getMobileBrowser().booleanValue() || deviceData.getTabletBrowser().booleanValue())
        {
            return UiExperienceLevel.MOBILE;
        }

        // Default to the DESKTOP
        return UiExperienceLevel.DESKTOP;
    }

    @Override
    public UiExperienceLevel convert(final DeviceData deviceData, final UiExperienceLevel prototype)
            throws ConversionException
    {
        return convert(deviceData);
    }
}
