/**
 * 
 */
package au.com.target.tgtfacades.featureswitch;

import java.util.List;
import java.util.Map;

import au.com.target.tgtfacades.featureswitch.data.FeatureSwitchData;




/**
 * Facade to fetch feature's switch is enabled or disabled
 * 
 * @author pratik
 *
 */
public interface TargetFeatureSwitchFacade {

    /**
     * Method to check if feature is enabled or not
     * 
     * @param featureName
     * @return true if switch doesn't exist and if exists it is enabled
     */
    boolean isFeatureEnabled(String featureName);

    /**
     * Method is to return enabled features as a Map. When includeOnlyUIEnabled is true, it restricts to list of
     * features to ones that are also UI enabled.
     * 
     * @return map
     */
    public Map<String, Boolean> getEnabledFeatureMap(boolean includeOnlyUIEnabled);

    /**
     * Method to check whether ExpeditedCheckout is enabled.
     */
    boolean isExpeditedCheckoutEnabled();

    /**
     * Methods to check whether IPG payment is enabled
     * 
     * @return true if IPG enabled else false
     */
    boolean isIpgEnabled();

    /**
     * Methods to check whether Paypal payment is enabled
     * 
     * @return true if Paypal enabled else false
     */
    boolean isPaypalEnabled();

    /**
     * Methods to check whether Gift Card payment is enabled
     * 
     * @return true if GiftCard payment enabled else false
     */
    boolean isGiftCardPaymentEnabled();

    /**
     * @return true if the sizeordering is enabled
     */
    boolean isSizeOrderingEnabled();

    /**
     * @return true if the sizeordering for UI is enabled
     */
    boolean isSizeOrderingForUIEnabled();

    /**
     * @return true if the sizeorderingfacets is enabled
     */
    boolean isSizeOrderingFacetsEnabled();

    /**
     * 
     * @return true if urgencyToPurchasePDP is enabled
     */
    boolean isUrgenyToPurchasePDPEnabled();

    /**
     * 
     * @return true if inStoreStockVisibilityEnabled is enabled
     */
    boolean isInStoreStockVisibilityEnabled();

    /**
     * Determine whether to enable store stock check only for kiosks
     * 
     * @return true - if feature kioskInStoreStockVisibility is enabled
     */
    boolean isStockVisibilityOnlyForKiosks();

    /**
     * Determine if spc login and checkout is turned on
     * 
     * @return true - if feature is enabled
     */
    boolean isSpcLoginAndCheckout();

    /**
     * Determine if pre-populate checkout fields feature is turned on
     * 
     * @return true - if feature is enabled
     */
    boolean shouldPrepopulateCheckoutFields();

    boolean isConsolidatedStoreStockAvailable();

    /**
     * Checks whether Afterpay payment is enabled
     * 
     * @return true - if feature is enabled
     */
    boolean isAfterpayEnabled();

    /**
     * Method for retrieving all the feature switches and its status in the system
     * 
     * @return - list of feature switches
     */
    List<FeatureSwitchData> getAllFeatures();

    /**
     * Check whether end of life links are enabled or not
     * 
     * @return true if feature is enabled
     */
    boolean isProductEndOfLifeEnabled();

    /**
     * @return true if feature switch is enabled
     */
    boolean isProductEndOfLifeCrossSellEnabled();

    /**
     * check whether Aus Post delivery club is enabled.
     * 
     * @return true - if feature is enabled
     */
    boolean isShipsterEnabled();

    /**
     * Check whether FIS phase 2 store stock visibility is enabled.
     * 
     * @return true if feature is enabled
     */
    boolean isFindInStoreStockVisibilityEnabled();

    /**
     * Check whether Fluent integration is enabled.
     * 
     * @return true if feature is enabled
     */
    boolean isFluentEnabled();

    /**
     * Check whether Fluent stock lookup is enabled.
     * 
     * @return true if feature is enabled
     */
    boolean isFluentStockLookupEnabled();

    /**
     * Check whether to serve assets out of new UI repository
     * 
     * @return true if feature is enabled
     */
    boolean isUiServeNewAssetsEnabled();

    /**
     * Check whether update card is enabled for pre-order.
     * 
     * return true if feature is enabled
     */
    boolean isPreOrderUpdateCardEnabled();

    /**
     * Check whether zip payment feature enabled
     * 
     * @return true if feature is enabled
     */
    boolean isZipEnabled();

    /**
     * Check whether ReactJS is enabled on listing page
     *
     * @return true if feature is enabled
     */
    boolean isReactListingEnabled();
}
