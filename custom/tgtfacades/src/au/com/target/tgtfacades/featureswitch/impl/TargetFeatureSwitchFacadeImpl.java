/**
 * 
 */
package au.com.target.tgtfacades.featureswitch.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetFeatureSwitchModel;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.featureswitch.data.FeatureSwitchData;


/**
 * @author pratik
 *
 */
public class TargetFeatureSwitchFacadeImpl implements TargetFeatureSwitchFacade {

    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Override
    public boolean isFeatureEnabled(final String featureName) {
        return targetFeatureSwitchService.isFeatureEnabled(featureName);
    }

    /**
     * Method is to return enabled features as a Map. When includeOnlyUIEnabled is true, it restricts to list of
     * features to ones that are also UI enabled.
     * 
     * @param includeOnlyUIEnabled
     * @return enabledFeatureMap
     */
    @Override
    public Map<String, Boolean> getEnabledFeatureMap(final boolean includeOnlyUIEnabled) {

        final Map<String, Boolean> enabledFeatureMap = new HashMap<>();

        final List<TargetFeatureSwitchModel> enabledFeatures = targetFeatureSwitchService
                .getEnabledFeatures(includeOnlyUIEnabled);

        for (final TargetFeatureSwitchModel targetFeatureSwitchModel : enabledFeatures) {
            enabledFeatureMap.put(targetFeatureSwitchModel.getAttrName(), targetFeatureSwitchModel.getEnabled());
        }
        return enabledFeatureMap;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * Method to check whether ExpeditedCheckout is enabled.
     * 
     * @return false now as Expedited Checkout is not implemented yet
     */
    @Override
    public boolean isExpeditedCheckoutEnabled() {
        return false;
    }

    @Override
    public boolean isIpgEnabled() {
        return isFeatureEnabled("payment.ipg");
    }

    @Override
    public boolean isPaypalEnabled() {
        return isFeatureEnabled("payment.paypal");
    }

    @Override
    public boolean isGiftCardPaymentEnabled() {
        return isFeatureEnabled("payment.giftcard");
    }

    @Override
    public boolean isSizeOrderingEnabled() {
        return isFeatureEnabled("sizeOrdering");
    }

    @Override
    public boolean isSizeOrderingForUIEnabled() {
        return isFeatureEnabled("sizeOrderingUI");
    }

    @Override
    public boolean isSizeOrderingFacetsEnabled() {
        return isFeatureEnabled("sizeOrderingFacets");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade#isUrgenyToPurchasePDPEnabled()
     */
    @Override
    public boolean isUrgenyToPurchasePDPEnabled() {
        return isFeatureEnabled("urgencyToPurchasePdp");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade#isInStoreStockAvailabilityEnabled()
     */
    @Override
    public boolean isInStoreStockVisibilityEnabled() {
        return isFeatureEnabled("inStoreStockVisibility");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade#useExistingStockService()
     */
    @Override
    public boolean isStockVisibilityOnlyForKiosks() {
        return isFeatureEnabled("kioskInStoreStockVisibility");
    }

    @Override
    public boolean isSpcLoginAndCheckout() {
        return isFeatureEnabled("spc.login.and.checkout");
    }

    @Override
    public boolean shouldPrepopulateCheckoutFields() {
        return isFeatureEnabled("checkout.prepopulate.fields");
    }

    @Override
    public boolean isConsolidatedStoreStockAvailable() {
        return isFeatureEnabled("consolidatedStoreStockVisibility");
    }

    @Override
    public boolean isAfterpayEnabled() {
        return isFeatureEnabled("payment.afterpay");
    }

    @Override
    public boolean isZipEnabled() {
        return isFeatureEnabled(TgtCoreConstants.FeatureSwitch.ZIP);
    }

    @Override
    public List<FeatureSwitchData> getAllFeatures() {
        final List<FeatureSwitchData> features = new ArrayList<>();
        for (final TargetFeatureSwitchModel targetFeatureSwitchModel : targetFeatureSwitchService.getAllFeatures()) {
            final FeatureSwitchData featureSwitchData = new FeatureSwitchData();
            featureSwitchData.setName(targetFeatureSwitchModel.getName());
            featureSwitchData.setAttrName(targetFeatureSwitchModel.getAttrName());
            featureSwitchData.setComment(targetFeatureSwitchModel.getName());
            featureSwitchData.setEnabled(BooleanUtils.isTrue(targetFeatureSwitchModel.getEnabled()));
            featureSwitchData.setFormattedModifiedDate(targetFeatureSwitchModel.getModifiedtime());
            features.add(featureSwitchData);
        }
        return features;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade#isProductEndOfLifeEnabled()
     */
    @Override
    public boolean isProductEndOfLifeEnabled() {
        return isFeatureEnabled("product.end.of.life");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade#isProductEOLCrossSellEnabled()
     */
    @Override
    public boolean isProductEndOfLifeCrossSellEnabled() {
        return isFeatureEnabled("product.end.of.life.cross.sell");
    }

    @Override
    public boolean isShipsterEnabled() {
        return isFeatureEnabled("shipster.available");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade#isFisStoreStockVisibilityEnabled()
     */
    @Override
    public boolean isFindInStoreStockVisibilityEnabled() {
        return isFeatureEnabled("fis2.store.stock.visibility");
    }

    @Override
    public boolean isFluentEnabled() {
        return isFeatureEnabled("fluent");
    }

    @Override
    public boolean isFluentStockLookupEnabled() {
        return isFeatureEnabled("fluentStockLookup");
    }

    @Override
    public boolean isUiServeNewAssetsEnabled() {
        return isFeatureEnabled("ui.serve.new.assets");
    }

    @Override
    public boolean isPreOrderUpdateCardEnabled() {
        return isFeatureEnabled("preorder.update.card");
    }

    @Override
    public boolean isReactListingEnabled() {
        return isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_LISTING);
    }
}
