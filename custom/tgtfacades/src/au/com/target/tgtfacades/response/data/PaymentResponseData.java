/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author htan3
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaymentResponseData extends BaseResponseData {

    private String paymentMethod;

    private String iframeUrl;

    private String paypalUrl;

    private String paypalSessionToken;

    private String afterpaySessionToken;

    private String zipPaymentSessionToken;

    private String zipPaymentRedirectUrl;

    @JsonProperty("SST")
    private String ipgToken;

    private String sessionId;

    private Boolean hasPartialPayment;

    /**
     * @return the hasPartialPayment
     */
    public Boolean getHasPartialPayment() {
        return hasPartialPayment;
    }

    /**
     * @param hasPartialPayment
     *            the hasPartialPayment to set
     */
    public void setHasPartialPayment(final Boolean hasPartialPayment) {
        this.hasPartialPayment = hasPartialPayment;
    }

    /**
     * @return the paymentMethod
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * @param paymentMethod
     *            the paymentMethod to set
     */
    public void setPaymentMethod(final String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /**
     * @return the iframeUrl
     */
    public String getIframeUrl() {
        return iframeUrl;
    }

    /**
     * @param iframeUrl
     *            the iframeUrl to set
     */
    public void setIframeUrl(final String iframeUrl) {
        this.iframeUrl = iframeUrl;
    }

    /**
     * @return the paypalUrl
     */
    public String getPaypalUrl() {
        return paypalUrl;
    }

    /**
     * @param paypalUrl
     *            the paypalUrl to set
     */
    public void setPaypalUrl(final String paypalUrl) {
        this.paypalUrl = paypalUrl;
    }

    /**
     * @return the paypalSessionToken
     */
    public String getPaypalSessionToken() {
        return paypalSessionToken;
    }

    /**
     * @param paypalSessionToken
     *            the paypalSessionToken to set
     */
    public void setPaypalSessionToken(final String paypalSessionToken) {
        this.paypalSessionToken = paypalSessionToken;
    }

    /**
     * @return the afterpaySessionToken
     */
    public String getAfterpaySessionToken() {
        return afterpaySessionToken;
    }

    /**
     * @param afterpaySessionToken
     *            the afterpaySessionToken to set
     */
    public void setAfterpaySessionToken(final String afterpaySessionToken) {
        this.afterpaySessionToken = afterpaySessionToken;
    }


    /**
     * @return the zipPaymentSessionToken
     */
    public String getZipPaymentSessionToken() {
        return zipPaymentSessionToken;
    }

    /**
     * @param zipPaymentSessionToken
     *            the zipPaymentSessionToken to set
     */
    public void setZipPaymentSessionToken(final String zipPaymentSessionToken) {
        this.zipPaymentSessionToken = zipPaymentSessionToken;
    }

    /**
     * @return the zipPaymentRedirectUrl
     */
    public String getZipPaymentRedirectUrl() {
        return zipPaymentRedirectUrl;
    }

    /**
     * @param zipPaymentRedirectUrl
     *            the zipPaymentRedirectUrl to set
     */
    public void setZipPaymentRedirectUrl(final String zipPaymentRedirectUrl) {
        this.zipPaymentRedirectUrl = zipPaymentRedirectUrl;
    }

    /**
     * @return the ipgToken
     */
    public String getIpgToken() {
        return ipgToken;
    }

    /**
     * @param ipgToken
     *            the ipgToken to set
     */
    public void setIpgToken(final String ipgToken) {
        this.ipgToken = ipgToken;
    }

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId
     *            the sessionId to set
     */
    public void setSessionId(final String sessionId) {
        this.sessionId = sessionId;
    }

}
