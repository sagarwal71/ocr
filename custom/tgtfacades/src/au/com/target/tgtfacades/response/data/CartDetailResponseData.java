package au.com.target.tgtfacades.response.data;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.AddressData;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedemptionData;
import au.com.target.tgtfacades.order.data.FlybuysDiscountData;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtfacades.user.data.ContactDetailsData;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtfacades.voucher.data.VoucherData;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class CartDetailResponseData extends BaseResponseData {

    private String id;

    private String tmid;

    private TargetZoneDeliveryModeData deliveryMode;

    private ContactDetailsData contact;

    private TargetPointOfServiceData store;

    private String tmdNumber;

    private TargetAddressData deliveryAddress;

    private VoucherData voucher;

    private FlybuysResponseData flybuysData;

    private PriceData subtotal;

    private PriceData deliveryFee;

    private PriceData total;

    private Boolean deliveryFeeChanged;

    private List<OrderEntryData> entries;

    private PriceData orderDiscounts;

    private PriceData gst;

    private AddressData billingAddress;

    private Boolean containsDigitalEntriesOnly;

    private FlybuysRedemptionData flybuysRedemptionData;

    private FlybuysDiscountData flybuysDiscountData;

    private boolean excludedForAfterpay;

    private PriceData installmentPrice;

    private PriceData preOrderDepositAmount;

    private PriceData preOrderOutstandingAmount;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * @return the tmid
     */
    public String getTmid() {
        return tmid;
    }

    /**
     * @param tmid
     *            the tmid to set
     */
    public void setTmid(final String tmid) {
        this.tmid = tmid;
    }

    /**
     * @return the deliveryMode
     */
    public TargetZoneDeliveryModeData getDeliveryMode() {
        return deliveryMode;
    }

    /**
     * @param deliveryMode
     *            the deliveryMode to set
     */
    public void setDeliveryMode(final TargetZoneDeliveryModeData deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    /**
     * @return the contact
     */
    public ContactDetailsData getContact() {
        return contact;
    }

    /**
     * @param contact
     *            the contact to set
     */
    public void setContact(final ContactDetailsData contact) {
        this.contact = contact;
    }

    /**
     * @return the store
     */
    public TargetPointOfServiceData getStore() {
        return store;
    }

    /**
     * @param store
     *            the store to set
     */
    public void setStore(final TargetPointOfServiceData store) {
        this.store = store;
    }

    public String getTmdNumber() {
        return tmdNumber;
    }

    public void setTmdNumber(final String tmdNumber) {
        this.tmdNumber = tmdNumber;
    }

    /**
     * @return the deliveryAddress
     */
    public TargetAddressData getDeliveryAddress() {
        return deliveryAddress;
    }

    /**
     * @param deliveryAddress
     *            the deliveryAddress to set
     */
    public void setDeliveryAddress(final TargetAddressData deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    /**
     * @return the voucher
     */
    public VoucherData getVoucher() {
        return voucher;
    }

    /**
     * @param voucher
     *            the voucher to set
     */
    public void setVoucher(final VoucherData voucher) {
        this.voucher = voucher;
    }

    /**
     * @return the flybuysData
     */
    public FlybuysResponseData getFlybuysData() {
        return flybuysData;
    }

    /**
     * @param flybuysData
     *            the flybuysData to set
     */
    public void setFlybuysData(final FlybuysResponseData flybuysData) {
        this.flybuysData = flybuysData;
    }

    /**
     * @return the subtotal
     */
    public PriceData getSubtotal() {
        return subtotal;
    }

    /**
     * @param subtotal
     *            the subtotal to set
     */
    public void setSubtotal(final PriceData subtotal) {
        this.subtotal = subtotal;
    }

    /**
     * @return the deliveryFee
     */
    public PriceData getDeliveryFee() {
        return deliveryFee;
    }

    /**
     * @param deliveryFee
     *            the deliveryFee to set
     */
    public void setDeliveryFee(final PriceData deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    /**
     * @return the total
     */
    public PriceData getTotal() {
        return total;
    }

    /**
     * @param total
     *            the total to set
     */
    public void setTotal(final PriceData total) {
        this.total = total;
    }

    /**
     * @return the deliveryFeeChanged
     */
    public Boolean getDeliveryFeeChanged() {
        return deliveryFeeChanged;
    }

    /**
     * @param deliveryFeeChanged
     *            the deliveryFeeChanged to set
     */
    public void setDeliveryFeeChanged(final Boolean deliveryFeeChanged) {
        this.deliveryFeeChanged = deliveryFeeChanged;
    }

    /**
     * @return the entries
     */
    public List<OrderEntryData> getEntries() {
        return entries;
    }

    /**
     * @param entries
     *            the entries to set
     */
    public void setEntries(final List<OrderEntryData> entries) {
        this.entries = entries;
    }

    /**
     * @return the orderDiscounts
     */
    public PriceData getOrderDiscounts() {
        return orderDiscounts;
    }

    /**
     * @param orderDiscounts
     *            the orderDiscounts to set
     */
    public void setOrderDiscounts(final PriceData orderDiscounts) {
        this.orderDiscounts = orderDiscounts;
    }

    /**
     * @return the gst
     */
    public PriceData getGst() {
        return gst;
    }

    /**
     * @param gst
     *            the gst to set
     */
    public void setGst(final PriceData gst) {
        this.gst = gst;
    }

    /**
     * @return the billingAddress
     */
    public AddressData getBillingAddress() {
        return billingAddress;
    }

    /**
     * @param billingAddress
     *            the billingAddress to set
     */
    public void setBillingAddress(final AddressData billingAddress) {
        this.billingAddress = billingAddress;
    }

    /**
     * @return the containsDigitalEntriesOnly
     */
    public Boolean getContainsDigitalEntriesOnly() {
        return containsDigitalEntriesOnly;
    }

    /**
     * @param containsDigitalEntriesOnly
     *            the containsDigitalEntriesOnly to set
     */
    public void setContainsDigitalEntriesOnly(final Boolean containsDigitalEntriesOnly) {
        this.containsDigitalEntriesOnly = containsDigitalEntriesOnly;
    }

    /**
     * @return the flybuysRedemptionData
     */
    public FlybuysRedemptionData getFlybuysRedemptionData() {
        return flybuysRedemptionData;
    }

    /**
     * @param flybuysRedemptionData
     *            the flybuysRedemptionData to set
     */
    public void setFlybuysRedemptionData(final FlybuysRedemptionData flybuysRedemptionData) {
        this.flybuysRedemptionData = flybuysRedemptionData;
    }

    /**
     * @return the flybuysDiscountData
     */
    public FlybuysDiscountData getFlybuysDiscountData() {
        return flybuysDiscountData;
    }

    /**
     * @param flybuysDiscountData
     *            the flybuysDiscountData to set
     */
    public void setFlybuysDiscountData(final FlybuysDiscountData flybuysDiscountData) {
        this.flybuysDiscountData = flybuysDiscountData;
    }

    /**
     * @return the excludedForAfterpay
     */
    public boolean isExcludedForAfterpay() {
        return excludedForAfterpay;
    }

    /**
     * @param excludedForAfterpay
     *            the excludedForAfterpay to set
     */
    public void setExcludedForAfterpay(final boolean excludedForAfterpay) {
        this.excludedForAfterpay = excludedForAfterpay;
    }

    /**
     * @return the installmentPrice
     */
    public PriceData getInstallmentPrice() {
        return installmentPrice;
    }

    /**
     * @param installmentPrice
     *            the installmentPrice to set
     */
    public void setInstallmentPrice(final PriceData installmentPrice) {
        this.installmentPrice = installmentPrice;
    }

    /**
     * @return the preOrderDepositAmount
     */
    public PriceData getPreOrderDepositAmount() {
        return preOrderDepositAmount;
    }

    /**
     * @param preOrderDepositAmount
     *            the preOrderDepositAmount to set
     */
    public void setPreOrderDepositAmount(final PriceData preOrderDepositAmount) {
        this.preOrderDepositAmount = preOrderDepositAmount;
    }

    /**
     * @return the preOrderOutstandingAmount
     */
    public PriceData getPreOrderOutstandingAmount() {
        return preOrderOutstandingAmount;
    }

    /**
     * @param preOrderOutstandingAmount
     *            the preOrderOutstandingAmount to set
     */
    public void setPreOrderOutstandingAmount(final PriceData preOrderOutstandingAmount) {
        this.preOrderOutstandingAmount = preOrderOutstandingAmount;
    }
}
