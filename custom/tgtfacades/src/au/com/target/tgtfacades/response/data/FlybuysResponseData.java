/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author bhuang3
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)

public class FlybuysResponseData {

    private String code;

    private boolean canRedeemPoints;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the canRedeemPoints
     */
    public boolean isCanRedeemPoints() {
        return canRedeemPoints;
    }

    /**
     * @param canRedeemPoints
     *            the canRedeemPoints to set
     */
    public void setCanRedeemPoints(final boolean canRedeemPoints) {
        this.canRedeemPoints = canRedeemPoints;
    }



}
