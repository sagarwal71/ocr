/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import de.hybris.platform.commercefacades.user.data.AddressData;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;


/**
 * @author htan3
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CheckoutOptionsResponseData extends BaseResponseData {

    private List<TargetZoneDeliveryModeData> deliveryModes;

    private List<AddressData> deliveryAddresses;

    private List<PaymentMethodData> paymentMethods;

    /** the address suggestions from qas **/
    private List<au.com.target.tgtverifyaddr.data.AddressData> addressSuggestions;

    /**
     * @return the addressSuggestions
     */
    public List<au.com.target.tgtverifyaddr.data.AddressData> getAddressSuggestions() {
        return addressSuggestions;
    }

    /**
     * @param addressSuggestions
     *            the addressSuggestions to set
     */
    public void setAddressSuggestions(final List<au.com.target.tgtverifyaddr.data.AddressData> addressSuggestions) {
        this.addressSuggestions = addressSuggestions;
    }

    /**
     * @return the deliveryModes
     */
    public List<TargetZoneDeliveryModeData> getDeliveryModes() {
        return deliveryModes;
    }

    /**
     * @param deliveryModes
     *            the deliveryModes to set
     */
    public void setDeliveryModes(final List<TargetZoneDeliveryModeData> deliveryModes) {
        this.deliveryModes = deliveryModes;
    }

    /**
     * @return the deliveryAddresses
     */
    public List<AddressData> getDeliveryAddresses() {
        return deliveryAddresses;
    }

    /**
     * @param deliveryAddresses
     *            the deliveryAddresses to set
     */
    public void setDeliveryAddresses(final List<AddressData> deliveryAddresses) {
        this.deliveryAddresses = deliveryAddresses;
    }

    /**
     * @return the paymentMethods
     */
    public List<PaymentMethodData> getPaymentMethods() {
        return paymentMethods;
    }

    /**
     * @param paymentMethods
     *            the paymentMethods to set
     */
    public void setPaymentMethods(final List<PaymentMethodData> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

}
