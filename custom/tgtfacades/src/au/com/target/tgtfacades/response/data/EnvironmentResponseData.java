/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import au.com.target.tgtfacades.order.data.EnvironmentData;


/**
 * Response object to wrap environment data.
 * 
 * @author jjayawa1
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)

public class EnvironmentResponseData extends BaseResponseData {
    private EnvironmentData env;

    /**
     * @return the env
     */
    public EnvironmentData getEnv() {
        return env;
    }

    /**
     * @param env
     *            the env to set
     */
    public void setEnv(final EnvironmentData env) {
        this.env = env;
    }

}
