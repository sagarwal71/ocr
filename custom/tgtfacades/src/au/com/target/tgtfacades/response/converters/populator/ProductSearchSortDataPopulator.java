/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.tgtfacades.response.data.ProductSearchResponseData;
import au.com.target.tgtfacades.response.data.SortResponseData;
import au.com.target.tgtfacades.search.data.TargetProductCategorySearchPageData;
import au.com.target.tgtfacades.sort.TargetSortData;


/**
 * @author bhuang3
 *
 */
public class ProductSearchSortDataPopulator
        implements
        Populator<TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData>, ProductSearchResponseData> {

    @Override
    public void populate(
            final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> source,
            final ProductSearchResponseData target) throws ConversionException {
        final List<TargetSortData> targetSortDataList = source.getTargetSortData();
        final List<SortResponseData> sortResponseDataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(targetSortDataList)) {
            for (final TargetSortData targetSortData : targetSortDataList) {
                final SortResponseData sortResponseData = new SortResponseData();
                this.populateSortResponseData(targetSortData, sortResponseData);
                sortResponseDataList.add(sortResponseData);
            }
        }
        target.setSortDataList(sortResponseDataList);
    }

    private void populateSortResponseData(final TargetSortData source, final SortResponseData target) {
        if (source != null) {
            target.setName(source.getCode());
            target.setSelected(source.isSelected());
            target.setUrl(source.getUrl());
            target.setQueryUrl(source.getWsUrl());
        }
    }

}
