/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import de.hybris.platform.commercefacades.product.data.PriceData;

import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author mgazal
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)

public class OrderDetailsData {

    private String status;

    private String email;

    private PriceData totalPrice;

    private boolean displayMutipleConsignment;

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @return the displayMutipleConsignment
     */
    public boolean isDisplayMutipleConsignment() {
        return displayMutipleConsignment;
    }

    /**
     * @param displayMutipleConsignment
     *            the displayMutipleConsignment to set
     */
    public void setDisplayMutipleConsignment(final boolean displayMutipleConsignment) {
        this.displayMutipleConsignment = displayMutipleConsignment;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * @return the totalPrice
     */
    public PriceData getTotalPrice() {
        return totalPrice;
    }

    /**
     * @param totalPrice
     *            the totalPrice to set
     */
    public void setTotalPrice(final PriceData totalPrice) {
        this.totalPrice = totalPrice;
    }
}
