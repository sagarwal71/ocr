/**
 * 
 */
package au.com.target.tgtfacades.response.data;


import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * This is the base response data object which should be extended for other response data
 * 
 * @author rsamuel3
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResponseData {
    private Error error;

    /**
     * @return the error
     */
    public Error getError() {
        return error;
    }

    /**
     * @param error
     *            the error to set
     */
    public void setError(final Error error) {
        this.error = error;
    }
}
