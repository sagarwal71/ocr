/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtfacades.user.data.ContactDetailsData;


/**
 * @author htan3
 *
 */
public class CartDetailClickAndCollectDetailPopulator implements Populator<TargetCartData, CartDetailResponseData> {

    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Override
    public void populate(final TargetCartData source, final CartDetailResponseData target) throws ConversionException {
        if (source.getCncStoreNumber() != null) {
            final TargetPointOfServiceData posData = targetStoreLocatorFacade.getPointOfService(source
                    .getCncStoreNumber());
            target.setStore(posData);
        }
        final AddressData deliveryAddress = source.getDeliveryAddress();
        if (deliveryAddress != null) {
            final ContactDetailsData contactDetails = new ContactDetailsData();
            contactDetails.setFirstName(deliveryAddress.getFirstName());
            contactDetails.setFormattedTitle(deliveryAddress.getTitle());
            contactDetails.setLastName(deliveryAddress.getLastName());
            contactDetails.setMobileNumber(deliveryAddress.getPhone());
            contactDetails.setTitle(deliveryAddress.getTitleCode());
            target.setContact(contactDetails);
        }
    }

    @Required
    public void setTargetStoreLocatorFacade(final TargetStoreLocatorFacade targetStoreLocatorFacade) {
        this.targetStoreLocatorFacade = targetStoreLocatorFacade;
    }
}
