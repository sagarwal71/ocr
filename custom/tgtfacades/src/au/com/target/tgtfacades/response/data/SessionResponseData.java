/**
 * 
 */
package au.com.target.tgtfacades.response.data;


import com.fasterxml.jackson.annotation.JsonInclude;

import au.com.target.tgtfacades.order.data.SessionData;


/**
 * Response object to wrap global session data.
 * 
 * @author jjayawa1
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SessionResponseData extends BaseResponseData {
    private SessionData session;

    /**
     * @return the session
     */
    public SessionData getSession() {
        return session;
    }

    /**
     * @param session
     *            the session to set
     */
    public void setSession(final SessionData session) {
        this.session = session;
    }

}
