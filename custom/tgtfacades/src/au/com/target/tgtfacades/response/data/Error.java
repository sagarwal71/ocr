/**
 * 
 */
package au.com.target.tgtfacades.response.data;


import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author rsamuel3
 *
 */
public class Error {

    private final String code;

    private String message;

    @JsonIgnore
    private String stackTrace;

    @JsonIgnore
    private String requestUri;



    public Error(final String code) {
        this.code = code;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(final String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ERROR=");
        sb.append(code);
        sb.append("(");
        sb.append(message);
        sb.append(") ");
        sb.append(" , requestUri=");
        sb.append(requestUri);
        sb.append(" ,\n StackTrace:");
        sb.append(stackTrace);
        return sb.toString();
    }

    /**
     * @return the stackTrace
     */
    public String getStackTrace() {
        return stackTrace;
    }

    /**
     * @param stackTrace
     *            the stackTrace to set
     */
    public void setStackTrace(final String stackTrace) {
        this.stackTrace = stackTrace;
    }

    /**
     * @return the requestUri
     */
    public String getRequestUri() {
        return requestUri;
    }

    /**
     * @param requestUri
     *            the requestUri to set
     */
    public void setRequestUri(final String requestUri) {
        this.requestUri = requestUri;
    }

}
