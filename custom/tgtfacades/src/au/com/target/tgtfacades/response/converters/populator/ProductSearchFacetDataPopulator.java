/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.data.TargetFacetData;
import au.com.target.tgtfacades.response.data.FacetOptionResponseData;
import au.com.target.tgtfacades.response.data.FacetResponseData;
import au.com.target.tgtfacades.response.data.ProductSearchResponseData;
import au.com.target.tgtfacades.search.data.TargetProductCategorySearchPageData;


/**
 * @author bhuang3
 *
 */
public class ProductSearchFacetDataPopulator
        implements
        Populator<TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData>, ProductSearchResponseData> {

    private List<String> isolatedFacets;

    @Override
    public void populate(
            final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> source,
            final ProductSearchResponseData target)
            throws ConversionException {
        final List<TargetFacetData<EndecaSearchStateData>> facetDataList = source.getFacets();
        final List<FacetResponseData> facetResponseDataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(facetDataList)) {
            for (final TargetFacetData<EndecaSearchStateData> targetFacetData : facetDataList) {
                final FacetResponseData facetResponseData = new FacetResponseData();
                this.populateFacetResponseData(targetFacetData, facetResponseData);
                facetResponseDataList.add(facetResponseData);
            }
        }
        target.setFacetList(facetResponseDataList);
    }

    private void populateFacetResponseData(final TargetFacetData<EndecaSearchStateData> targetFacetData,
            final FacetResponseData facetResponseData) {
        if (targetFacetData != null) {
            facetResponseData.setId(targetFacetData.getCode());
            facetResponseData.setName(targetFacetData.getName());
            facetResponseData.setMultiSelect(targetFacetData.isMultiSelect());
            facetResponseData.setExpand(targetFacetData.isExpand());
            facetResponseData.setSeoFollowEnabled(targetFacetData.isSeoFollowEnabled());
            if (isolatedFacets.contains(targetFacetData.getCode())) {
                facetResponseData.setIsolated(true);
            }
            if (CollectionUtils.isNotEmpty(targetFacetData.getValues())) {
                final List<FacetOptionResponseData> optionDataList = new ArrayList<>(
                        targetFacetData.getValues().size());
                for (final FacetValueData<EndecaSearchStateData> valueData : (List<FacetValueData<EndecaSearchStateData>>)(targetFacetData
                        .getValues())) {
                    optionDataList.add(createFacetOptionData(valueData));
                }
                facetResponseData.setFacetOptionList(optionDataList);
            }
            if (CollectionUtils.isNotEmpty(targetFacetData.getFacets())) {
                final List<FacetResponseData> facetResponseDataList = new ArrayList<>();
                for (final TargetFacetData<EndecaSearchStateData> facetData : targetFacetData.getFacets()) {
                    final FacetResponseData subFacetResponseData = new FacetResponseData();
                    this.populateFacetResponseData(facetData, subFacetResponseData);
                    facetResponseDataList.add(subFacetResponseData);
                }
                facetResponseData.setFacetList(facetResponseDataList);
            }
        }
    }

    private FacetOptionResponseData createFacetOptionData(final FacetValueData<EndecaSearchStateData> source) {
        FacetOptionResponseData option = null;
        if (source != null) {
            option = new FacetOptionResponseData();
            option.setId(source.getCode());
            option.setCount(source.getCount());
            option.setSelected(source.isSelected());
            option.setName(source.getName());
            if (source.getQuery() != null) {
                option.setUrl(source.getQuery().getUrl());
                option.setQueryUrl(source.getQuery().getWsUrl());
            }
        }
        return option;

    }

    /**
     * @param isolatedFacets
     *            the isolatedFacets to set
     */
    @Required
    public void setIsolatedFacets(final List<String> isolatedFacets) {
        this.isolatedFacets = isolatedFacets;
    }



}
