/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.order.data.TargetOrderEntryData;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.response.data.EcommEntry;
import au.com.target.tgtfacades.response.data.OrderDetailGaData;


/**
 * @author mgazal
 *
 */
public class OrderDetailGaPopulator implements Populator<TargetOrderData, OrderDetailGaData> {

    private CMSSiteService cmsSiteService;

    @Override
    public void populate(final TargetOrderData source, final OrderDetailGaData target) throws ConversionException {
        if (source.getTotalPrice() != null) {
            target.setEcommCurrency(source.getTotalPrice().getCurrencyIso());
            target.setEcommTotalValue(source.getTotalPrice().getValue());
        }
        if (source.getTotalTax() != null) {
            target.setEcommTotalTax(source.getTotalTax().getValue());
        }
        final CMSSiteModel site = cmsSiteService.getCurrentSite();
        target.setEcommSite(site != null ? site.getName() : "");

        target.setEcommCode(source.getCode());
        if (source.getDeliveryCost() != null) {
            target.setEcommDeliveryValue(source.getDeliveryCost().getValue());
        }
        if (source.getDeliveryAddress() != null) {
            target.setEcommDeliveryTown(source.getDeliveryAddress().getTown());
            target.setEcommDeliveryPostCode(source.getDeliveryAddress().getPostalCode());
            if (source.getDeliveryAddress().getCountry() != null) {
                target.setEcommDeliveryCountry(source.getDeliveryAddress().getCountry().getName());
            }
        }
        if (source.getDeliveryMode() != null) {
            target.setEcommDeliveryMode(source.getDeliveryMode().getName());
        }
        if (CollectionUtils.isNotEmpty(source.getAppliedVoucherCodes())) {
            target.setEcommVoucher(source.getAppliedVoucherCodes().get(0));
        }
        if (source.getAffiliateOrderData() != null) {
            target.setAffiliateCodes(source.getAffiliateOrderData().getCodes());
            target.setAffiliateQuantities(source.getAffiliateOrderData().getQuantities());
            target.setAffiliatePrices(source.getAffiliateOrderData().getPrices());
        }

        if (CollectionUtils.isNotEmpty(source.getEntries())) {
            populateEcommEntries(source, target);
        }
    }

    /**
     * Populate ecommEntries from orderEnties in source.
     * 
     * @param source
     * @param target
     */
    protected void populateEcommEntries(final TargetOrderData source, final OrderDetailGaData target) {
        target.setEcommEntries(new ArrayList<EcommEntry>(source.getEntries().size()));
        for (final OrderEntryData orderEntryData : source.getEntries()) {
            final EcommEntry ecommEntry = new EcommEntry();
            if (orderEntryData.getProduct() instanceof TargetProductData) {
                ecommEntry.setBaseCode(((TargetProductData)orderEntryData.getProduct()).getBaseProductCode());
                ecommEntry.setBaseName(((TargetProductData)orderEntryData.getProduct()).getBaseName());
                ecommEntry.setTopLevelCategory(((TargetProductData)orderEntryData.getProduct()).getTopLevelCategory());
                ecommEntry.setBrand(((TargetProductData)orderEntryData.getProduct()).getBrand());
                ecommEntry.setAssorted(((TargetProductData)orderEntryData.getProduct()).isAssorted());
            }
            if (orderEntryData.getProduct() != null) {
                ecommEntry.setCode(orderEntryData.getProduct().getCode());
                ecommEntry.setName(orderEntryData.getProduct().getName());
                ecommEntry.setCategory(getCategory(orderEntryData.getProduct().getCategories()));
            }
            ecommEntry.setQuantity(orderEntryData.getQuantity());
            if (orderEntryData.getBasePrice() != null) {
                ecommEntry.setPrice(orderEntryData.getBasePrice().getValue());
            }
            if (orderEntryData instanceof TargetOrderEntryData) {
                ecommEntry.setDepartmentCode(((TargetOrderEntryData)orderEntryData).getDepartmentCode());
            }
            target.getEcommEntries().add(ecommEntry);
        }
    }

    /**
     * Gets the category string to be logged to GA.
     * 
     * @param categories
     * @return the last category in the format {categoryCode}/{categoryName}
     */
    protected String getCategory(final Collection<CategoryData> categories) {
        if (CollectionUtils.isNotEmpty(categories)) {
            final CategoryData category = ((List<CategoryData>)categories).get(categories.size() - 1);
            return category.getCode() + "/" + category.getName();
        }
        return null;
    }

    /**
     * @param cmsSiteService
     *            the cmsSiteService to set
     */
    @Required
    public void setCmsSiteService(final CMSSiteService cmsSiteService) {
        this.cmsSiteService = cmsSiteService;
    }

}
