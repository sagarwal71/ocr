/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;


/**
 * @author htan3
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)

public class PlaceOrderResponseData extends BaseResponseData {
    private String redirectUrl;

    private AdjustedCartEntriesData sohUpdates;

    /**
     * @return the sohUpdates
     */
    public AdjustedCartEntriesData getSohUpdates() {
        return sohUpdates;
    }

    /**
     * @param sohUpdates
     *            the sohUpdates to set
     */
    public void setSohUpdates(final AdjustedCartEntriesData sohUpdates) {
        this.sohUpdates = sohUpdates;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(final String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
