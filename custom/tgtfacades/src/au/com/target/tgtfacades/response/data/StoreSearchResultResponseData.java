/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author htan3
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StoreSearchResultResponseData extends BaseResponseData {

    private List<PointOfServiceData> stores;

    /**
     * @return the stores
     */
    public List<PointOfServiceData> getStores() {
        return stores;
    }

    /**
     * @param stores
     *            the stores to set
     */
    public void setStores(final List<PointOfServiceData> stores) {
        this.stores = stores;
    }

}
