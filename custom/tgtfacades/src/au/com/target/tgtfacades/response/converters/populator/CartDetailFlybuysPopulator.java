/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;
import au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedemptionData;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;
import au.com.target.tgtfacades.response.data.FlybuysResponseData;


/**
 * Populator for flybuys number.
 * 
 * @author jjayawa1
 *
 */
public class CartDetailFlybuysPopulator implements Populator<TargetCartData, CartDetailResponseData> {

    private FlybuysDiscountFacade flybuysDiscountFacade;
    private TargetCheckoutFacade targetCheckoutFacade;

    @Override
    public void populate(final TargetCartData source, final CartDetailResponseData target) throws ConversionException {
        if (StringUtils.isNotEmpty(source.getFlybuysNumber())) {
            final FlybuysResponseData flybuysData = new FlybuysResponseData();
            flybuysData.setCode(source.getFlybuysNumber());
            final FlybuysDiscountDenialDto denialData = flybuysDiscountFacade.getFlybuysDiscountAllowed();
            if (denialData != null && denialData.isDenied()) {
                flybuysData.setCanRedeemPoints(false);
            }
            else if (targetCheckoutFacade.doesCartHaveGiftCardOnly()) {
                flybuysData.setCanRedeemPoints(false);
            }
            else {
                flybuysData.setCanRedeemPoints(true);
            }
            target.setFlybuysData(flybuysData);

            if (flybuysData.isCanRedeemPoints()) {
                final FlybuysRedemptionData redeemOptions = flybuysDiscountFacade
                        .getFlybuysRedemptionDataForCheckoutCart();
                target.setFlybuysRedemptionData(redeemOptions);
            }
            target.setFlybuysDiscountData(source.getFlybuysDiscountData());
        }
    }


    /**
     * @param flybuysDiscountFacade
     *            the flybuysDiscountFacade to set
     */
    @Required
    public void setFlybuysDiscountFacade(final FlybuysDiscountFacade flybuysDiscountFacade) {
        this.flybuysDiscountFacade = flybuysDiscountFacade;
    }

    /**
     * @param targetCheckoutFacade
     *            the targetCheckoutFacade to set
     */
    @Required
    public void setTargetCheckoutFacade(final TargetCheckoutFacade targetCheckoutFacade) {
        this.targetCheckoutFacade = targetCheckoutFacade;
    }



}
