/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author mgazal
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)

public class OrderDetailGaData {

    private String ecommCurrency;

    private BigDecimal ecommTotalValue;

    private BigDecimal ecommTotalTax;

    private String ecommSite;

    private String ecommCode;

    private BigDecimal ecommDeliveryValue;

    private String ecommDeliveryTown;

    private String ecommDeliveryPostCode;

    private String ecommDeliveryCountry;

    private String ecommDeliveryMode;

    private String ecommVoucher;

    private String affiliateCodes;

    private String affiliateQuantities;

    private String affiliatePrices;

    private List<EcommEntry> ecommEntries;

    /**
     * @return the ecommCurrency
     */
    public String getEcommCurrency() {
        return ecommCurrency;
    }

    /**
     * @param ecommCurrency
     *            the ecommCurrency to set
     */
    public void setEcommCurrency(final String ecommCurrency) {
        this.ecommCurrency = ecommCurrency;
    }

    /**
     * @return the ecommTotalValue
     */
    public BigDecimal getEcommTotalValue() {
        return ecommTotalValue;
    }

    /**
     * @param ecommTotalValue
     *            the ecommTotalValue to set
     */
    public void setEcommTotalValue(final BigDecimal ecommTotalValue) {
        this.ecommTotalValue = ecommTotalValue;
    }

    /**
     * @return the ecommTotalTax
     */
    public BigDecimal getEcommTotalTax() {
        return ecommTotalTax;
    }

    /**
     * @param ecommTotalTax
     *            the ecommTotalTax to set
     */
    public void setEcommTotalTax(final BigDecimal ecommTotalTax) {
        this.ecommTotalTax = ecommTotalTax;
    }

    /**
     * @return the ecommSite
     */
    public String getEcommSite() {
        return ecommSite;
    }

    /**
     * @param ecommSite
     *            the ecommSite to set
     */
    public void setEcommSite(final String ecommSite) {
        this.ecommSite = ecommSite;
    }

    /**
     * @return the ecommCode
     */
    public String getEcommCode() {
        return ecommCode;
    }

    /**
     * @param ecommCode
     *            the ecommCode to set
     */
    public void setEcommCode(final String ecommCode) {
        this.ecommCode = ecommCode;
    }

    /**
     * @return the ecommDeliveryValue
     */
    public BigDecimal getEcommDeliveryValue() {
        return ecommDeliveryValue;
    }

    /**
     * @param ecommDeliveryValue
     *            the ecommDeliveryValue to set
     */
    public void setEcommDeliveryValue(final BigDecimal ecommDeliveryValue) {
        this.ecommDeliveryValue = ecommDeliveryValue;
    }

    /**
     * @return the ecommDeliveryTown
     */
    public String getEcommDeliveryTown() {
        return ecommDeliveryTown;
    }

    /**
     * @param ecommDeliveryTown
     *            the ecommDeliveryTown to set
     */
    public void setEcommDeliveryTown(final String ecommDeliveryTown) {
        this.ecommDeliveryTown = ecommDeliveryTown;
    }

    /**
     * @return the ecommDeliveryPostCode
     */
    public String getEcommDeliveryPostCode() {
        return ecommDeliveryPostCode;
    }

    /**
     * @param ecommDeliveryPostCode
     *            the ecommDeliveryPostCode to set
     */
    public void setEcommDeliveryPostCode(final String ecommDeliveryPostCode) {
        this.ecommDeliveryPostCode = ecommDeliveryPostCode;
    }

    /**
     * @return the ecommDeliveryCountry
     */
    public String getEcommDeliveryCountry() {
        return ecommDeliveryCountry;
    }

    /**
     * @param ecommDeliveryCountry
     *            the ecommDeliveryCountry to set
     */
    public void setEcommDeliveryCountry(final String ecommDeliveryCountry) {
        this.ecommDeliveryCountry = ecommDeliveryCountry;
    }

    /**
     * @return the ecommDeliveryMode
     */
    public String getEcommDeliveryMode() {
        return ecommDeliveryMode;
    }

    /**
     * @param ecommDeliveryMode
     *            the ecommDeliveryMode to set
     */
    public void setEcommDeliveryMode(final String ecommDeliveryMode) {
        this.ecommDeliveryMode = ecommDeliveryMode;
    }

    /**
     * @return the ecommVoucher
     */
    public String getEcommVoucher() {
        return ecommVoucher;
    }

    /**
     * @param ecommVoucher
     *            the ecommVoucher to set
     */
    public void setEcommVoucher(final String ecommVoucher) {
        this.ecommVoucher = ecommVoucher;
    }

    /**
     * @return the affiliateCodes
     */
    public String getAffiliateCodes() {
        return affiliateCodes;
    }

    /**
     * @param affiliateCodes
     *            the affiliateCodes to set
     */
    public void setAffiliateCodes(final String affiliateCodes) {
        this.affiliateCodes = affiliateCodes;
    }

    /**
     * @return the affiliateQuantities
     */
    public String getAffiliateQuantities() {
        return affiliateQuantities;
    }

    /**
     * @param affiliateQuantities
     *            the affiliateQuantities to set
     */
    public void setAffiliateQuantities(final String affiliateQuantities) {
        this.affiliateQuantities = affiliateQuantities;
    }

    /**
     * @return the affiliatePrices
     */
    public String getAffiliatePrices() {
        return affiliatePrices;
    }

    /**
     * @param affiliatePrices
     *            the affiliatePrices to set
     */
    public void setAffiliatePrices(final String affiliatePrices) {
        this.affiliatePrices = affiliatePrices;
    }

    /**
     * @return the ecommEntries
     */
    public List<EcommEntry> getEcommEntries() {
        return ecommEntries;
    }

    /**
     * @param ecommEntries
     *            the ecommEntries to set
     */
    public void setEcommEntries(final List<EcommEntry> ecommEntries) {
        this.ecommEntries = ecommEntries;
    }

}
