/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.commercefacades.product.data.PriceData;

import java.util.List;


/**
 * @author bhuang3
 *
 */
public class ProductResponseData {

    private String code;

    private String name;

    private boolean inStock;

    private boolean onlineExclusive;

    private String primaryImageUrl;

    private String secondaryImageUrl;

    private String baseProductCode;

    private String baseName;

    private String brand;

    private String topLevelCategory;

    private String url;

    private String productPromotionalDeliverySticker;

    private boolean newArrived;

    private boolean giftCard;

    private String description;

    private boolean showWasPrice;

    private boolean preview;

    private Integer numberOfReviews;

    private Double averageRating;

    private String dealDescription;

    private String promotionStatus;

    private PriceRangeData priceRange;

    private PriceRangeData wasPriceRange;

    private PriceData wasPrice;

    private PriceData price;

    private Boolean assorted;

    private Boolean displayOnly;

    private List<ProductVariantResponseData> productVariantDataList;

    private Integer maxAvailOnlineQty;

    private Integer maxAvailStoreQty;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the inStock
     */
    public boolean isInStock() {
        return inStock;
    }

    /**
     * @param inStock
     *            the inStock to set
     */
    public void setInStock(final boolean inStock) {
        this.inStock = inStock;
    }

    /**
     * @return the onlineExclusive
     */
    public boolean isOnlineExclusive() {
        return onlineExclusive;
    }

    /**
     * @param onlineExclusive
     *            the onlineExclusive to set
     */
    public void setOnlineExclusive(final boolean onlineExclusive) {
        this.onlineExclusive = onlineExclusive;
    }

    /**
     * @return the primaryImageUrl
     */
    public String getPrimaryImageUrl() {
        return primaryImageUrl;
    }

    /**
     * @param primaryImageUrl
     *            the primaryImageUrl to set
     */
    public void setPrimaryImageUrl(final String primaryImageUrl) {
        this.primaryImageUrl = primaryImageUrl;
    }

    /**
     * @return the secondaryImageUrl
     */
    public String getSecondaryImageUrl() {
        return secondaryImageUrl;
    }

    /**
     * @param secondaryImageUrl
     *            the secondaryImageUrl to set
     */
    public void setSecondaryImageUrl(final String secondaryImageUrl) {
        this.secondaryImageUrl = secondaryImageUrl;
    }

    /**
     * @return the baseProductCode
     */
    public String getBaseProductCode() {
        return baseProductCode;
    }

    /**
     * @param baseProductCode
     *            the baseProductCode to set
     */
    public void setBaseProductCode(final String baseProductCode) {
        this.baseProductCode = baseProductCode;
    }

    /**
     * @return the baseName
     */
    public String getBaseName() {
        return baseName;
    }

    /**
     * @param baseName
     *            the baseName to set
     */
    public void setBaseName(final String baseName) {
        this.baseName = baseName;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand
     *            the brand to set
     */
    public void setBrand(final String brand) {
        this.brand = brand;
    }

    /**
     * @return the topLevelCategory
     */
    public String getTopLevelCategory() {
        return topLevelCategory;
    }

    /**
     * @param topLevelCategory
     *            the topLevelCategory to set
     */
    public void setTopLevelCategory(final String topLevelCategory) {
        this.topLevelCategory = topLevelCategory;
    }


    /**
     * @return the maxAvailOnlineQty
     */
    public Integer getMaxAvailOnlineQty() {
        return maxAvailOnlineQty;
    }

    /**
     * @param maxAvailOnlineQty
     *            the maxAvailOnlineQty to set
     */
    public void setMaxAvailOnlineQty(final Integer maxAvailOnlineQty) {
        this.maxAvailOnlineQty = maxAvailOnlineQty;
    }

    /**
     * @return the maxAvailStoreQty
     */
    public Integer getMaxAvailStoreQty() {
        return maxAvailStoreQty;
    }

    /**
     * @param maxAvailStoreQty
     *            the maxAvailStoreQty to set
     */
    public void setMaxAvailStoreQty(final Integer maxAvailStoreQty) {
        this.maxAvailStoreQty = maxAvailStoreQty;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     *            the url to set
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    /**
     * @return the productPromotionalDeliverySticker
     */
    public String getProductPromotionalDeliverySticker() {
        return productPromotionalDeliverySticker;
    }

    /**
     * @param productPromotionalDeliverySticker
     *            the productPromotionalDeliverySticker to set
     */
    public void setProductPromotionalDeliverySticker(final String productPromotionalDeliverySticker) {
        this.productPromotionalDeliverySticker = productPromotionalDeliverySticker;
    }

    /**
     * @return the newArrived
     */
    public boolean isNewArrived() {
        return newArrived;
    }

    /**
     * @param newArrived
     *            the newArrived to set
     */
    public void setNewArrived(final boolean newArrived) {
        this.newArrived = newArrived;
    }

    /**
     * @return the giftCard
     */
    public boolean isGiftCard() {
        return giftCard;
    }

    /**
     * @param giftCard
     *            the giftCard to set
     */
    public void setGiftCard(final boolean giftCard) {
        this.giftCard = giftCard;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return the showWasPrice
     */
    public boolean isShowWasPrice() {
        return showWasPrice;
    }

    /**
     * @param showWasPrice
     *            the showWasPrice to set
     */
    public void setShowWasPrice(final boolean showWasPrice) {
        this.showWasPrice = showWasPrice;
    }

    /**
     * @return the preview
     */
    public boolean isPreview() {
        return preview;
    }

    /**
     * @param preview
     *            the preview to set
     */
    public void setPreview(final boolean preview) {
        this.preview = preview;
    }

    /**
     * @return the numberOfReviews
     */
    public Integer getNumberOfReviews() {
        return numberOfReviews;
    }

    /**
     * @param numberOfReviews
     *            the numberOfReviews to set
     */
    public void setNumberOfReviews(final Integer numberOfReviews) {
        this.numberOfReviews = numberOfReviews;
    }

    /**
     * @return the averageRating
     */
    public Double getAverageRating() {
        return averageRating;
    }

    /**
     * @param averageRating
     *            the averageRating to set
     */
    public void setAverageRating(final Double averageRating) {
        this.averageRating = averageRating;
    }

    /**
     * @return the dealDescription
     */
    public String getDealDescription() {
        return dealDescription;
    }

    /**
     * @param dealDescription
     *            the dealDescription to set
     */
    public void setDealDescription(final String dealDescription) {
        this.dealDescription = dealDescription;
    }

    /**
     * @return the promotionStatus
     */
    public String getPromotionStatus() {
        return promotionStatus;
    }

    /**
     * @param promotionStatus
     *            the promotionStatus to set
     */
    public void setPromotionStatus(final String promotionStatus) {
        this.promotionStatus = promotionStatus;
    }

    /**
     * @return the priceRange
     */
    public PriceRangeData getPriceRange() {
        return priceRange;
    }

    /**
     * @param priceRange
     *            the priceRange to set
     */
    public void setPriceRange(final PriceRangeData priceRange) {
        this.priceRange = priceRange;
    }

    /**
     * @return the wasPriceRange
     */
    public PriceRangeData getWasPriceRange() {
        return wasPriceRange;
    }

    /**
     * @param wasPriceRange
     *            the wasPriceRange to set
     */
    public void setWasPriceRange(final PriceRangeData wasPriceRange) {
        this.wasPriceRange = wasPriceRange;
    }

    /**
     * @return the wasPrice
     */
    public PriceData getWasPrice() {
        return wasPrice;
    }

    /**
     * @param wasPrice
     *            the wasPrice to set
     */
    public void setWasPrice(final PriceData wasPrice) {
        this.wasPrice = wasPrice;
    }

    /**
     * @return the price
     */
    public PriceData getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(final PriceData price) {
        this.price = price;
    }

    /**
     * @return the assorted
     */
    public Boolean getAssorted() {
        return assorted;
    }

    /**
     * @param assorted
     *            the assorted to set
     */
    public void setAssorted(final Boolean assorted) {
        this.assorted = assorted;
    }

    /**
     * @return the displayOnly
     */
    public Boolean getDisplayOnly() {
        return displayOnly;
    }

    /**
     * @param displayOnly
     *            the displayOnly to set
     */
    public void setDisplayOnly(final Boolean displayOnly) {
        this.displayOnly = displayOnly;
    }

    /**
     * @return the productVariantDataList
     */
    public List<ProductVariantResponseData> getProductVariantDataList() {
        return productVariantDataList;
    }

    /**
     * @param productVariantDataList
     *            the productVariantDataList to set
     */
    public void setProductVariantDataList(final List<ProductVariantResponseData> productVariantDataList) {
        this.productVariantDataList = productVariantDataList;
    }

}
