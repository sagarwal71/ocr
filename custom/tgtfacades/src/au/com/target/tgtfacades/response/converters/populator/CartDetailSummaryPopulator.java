/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;


/**
 * Summary populator for cart.
 * 
 * @author jjayawa1
 *
 */
public class CartDetailSummaryPopulator implements Populator<TargetCartData, CartDetailResponseData> {

    @Override
    public void populate(final TargetCartData source, final CartDetailResponseData target) throws ConversionException {
        target.setDeliveryFee(source.getDeliveryCost());
        target.setTotal(source.getTotalPrice());
        target.setSubtotal(source.getSubTotal());
        target.setEntries(source.getEntries());
        target.setGst(source.getTotalTax());
        target.setOrderDiscounts(source.getOrderDiscounts());
        target.setInstallmentPrice(source.getInstallmentPrice());
        target.setPreOrderDepositAmount(source.getPreOrderDepositAmount());
        target.setPreOrderOutstandingAmount(source.getPreOrderOutstandingAmount());
    }
}