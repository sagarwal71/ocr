/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.response.data.ProductResponseData;
import au.com.target.tgtfacades.response.data.ProductSearchResponseData;
import au.com.target.tgtfacades.response.data.ProductVariantResponseData;
import au.com.target.tgtfacades.search.data.TargetProductCategorySearchPageData;
import au.com.target.tgtutility.util.TargetDateUtil;


/**
 * @author bhuang3
 *
 */
public class ProductSearchProductDataPopulator
        implements
        Populator<TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData>, ProductSearchResponseData> {

    private static final String ENDECA_DELIMITER = "**";
    private static final String ENDECA_DELIMITER_PATTERN = "\\*\\*";

    @Override
    public void populate(
            final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> source,
            final ProductSearchResponseData target) throws ConversionException {
        @SuppressWarnings("deprecation")
        final List<TargetProductListerData> productListerDataList = source.getResults();
        final List<ProductResponseData> productResponseDataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(productListerDataList)) {
            for (final TargetProductListerData listerData : productListerDataList) {
                final ProductResponseData productResponseData = new ProductResponseData();
                this.populate(listerData, productResponseData, source.getBulkyBoardNumber());
                productResponseDataList.add(productResponseData);
            }
        }
        target.setProductDataList(productResponseDataList);
    }

    private void populate(final TargetProductListerData listerData,
            final ProductResponseData productResponseData, final Integer bulkBoardNubmer) throws ConversionException {
        productResponseData.setCode(listerData.getCode());
        productResponseData.setName(listerData.getName());
        productResponseData.setInStock(listerData.isInStock());
        productResponseData.setOnlineExclusive(listerData.isOnlineExclusive());
        productResponseData.setPrimaryImageUrl(getImageUrl(listerData, 0));
        productResponseData.setSecondaryImageUrl(getImageUrl(listerData, 1));
        productResponseData.setBaseProductCode(listerData.getBaseProductCode());
        productResponseData.setBaseName(listerData.getBaseName());
        productResponseData.setBrand(listerData.getBrand());
        productResponseData.setTopLevelCategory(listerData.getTopLevelCategory());
        productResponseData.setUrl(this.appendBukyboardNumber(bulkBoardNubmer, listerData.getUrl()));
        productResponseData.setProductPromotionalDeliverySticker(listerData.getProductPromotionalDeliverySticker());
        productResponseData.setNewArrived(listerData.isNewArrived());
        productResponseData.setGiftCard(listerData.isGiftCard());
        productResponseData.setDescription(listerData.getDescription());
        productResponseData.setShowWasPrice(listerData.isShowWasPrice());
        productResponseData.setPreview(listerData.isPreview());
        productResponseData.setNumberOfReviews(listerData.getNumberOfReviews());
        productResponseData.setAverageRating(listerData.getAverageRating());
        productResponseData.setDealDescription(listerData.getDealDescription());
        productResponseData.setAssorted(isAssorted(listerData));
        productResponseData.setDisplayOnly(isDisplayOnly(listerData));
        productResponseData.setPromotionStatus(CollectionUtils.isNotEmpty(listerData
                .getPromotionStatuses()) ? listerData.getPromotionStatuses().get(0).getPromotionClass()
                        : null);
        productResponseData.setPriceRange(listerData.getPriceRange());
        productResponseData.setWasPriceRange(listerData.getWasPriceRange());
        productResponseData.setWasPrice(listerData.getWasPrice());
        productResponseData.setPrice(listerData.getPrice());
        productResponseData.setProductVariantDataList(createProductVariantDataList(listerData, bulkBoardNubmer));
        productResponseData.setMaxAvailOnlineQty(listerData.getMaxAvailOnlineQty());
        productResponseData.setMaxAvailStoreQty(listerData.getMaxAvailStoreQty());
    }

    private List<ProductVariantResponseData> createProductVariantDataList(final TargetProductListerData listerData,
            final Integer bulkBoardNubmer) {
        final List<ProductVariantResponseData> productVariantResponseDataList = new ArrayList<>();
        if (listerData != null && CollectionUtils.isNotEmpty(listerData.getTargetVariantProductListerData())) {
            for (final TargetVariantProductListerData variantProductListerData : listerData
                    .getTargetVariantProductListerData()) {
                final ProductVariantResponseData productVariantResponseData = createProductVariantResponseData(
                        variantProductListerData, bulkBoardNubmer);
                if (productVariantResponseData != null) {
                    productVariantResponseDataList.add(productVariantResponseData);
                }
            }
        }
        return productVariantResponseDataList;
    }

    private ProductVariantResponseData createProductVariantResponseData(
            final TargetVariantProductListerData variantProductListerData, final Integer bulkBoardNubmer) {
        ProductVariantResponseData productVariantResponseData = null;
        if (variantProductListerData != null) {
            productVariantResponseData = new ProductVariantResponseData();
            productVariantResponseData.setColourVariantCode(variantProductListerData.getColourVariantCode());
            productVariantResponseData
                    .setUrl(this.appendBukyboardNumber(bulkBoardNubmer, variantProductListerData.getUrl()));
            productVariantResponseData.setInStock(variantProductListerData.isInStock());
            productVariantResponseData.setColourName(variantProductListerData.getColourName());
            productVariantResponseData.setSwatchColour(variantProductListerData.getSwatchColour());
            productVariantResponseData.setDisplayPrice(variantProductListerData.getDisplayPrice());
            productVariantResponseData.setAssorted(variantProductListerData.getAssorted());
            productVariantResponseData.setDisplayOnly(Boolean.valueOf(variantProductListerData.isDisplayOnly()));
            final String thumbImageUrl = CollectionUtils.isNotEmpty(variantProductListerData
                    .getThumbImageUrl()) ? variantProductListerData.getThumbImageUrl().get(0) : null;
            productVariantResponseData.setThumbImageUrl(splitEndecaUrlField(thumbImageUrl));
            final String gridImageUrl = CollectionUtils.isNotEmpty(variantProductListerData
                    .getGridImageUrls()) ? variantProductListerData.getGridImageUrls().get(0) : null;
            productVariantResponseData.setGridImageUrl(splitEndecaUrlField(gridImageUrl));
            productVariantResponseData.setProductDisplayType(variantProductListerData.getProductDisplayType());
            productVariantResponseData.setNormalSaleStartDate(
                    formatNormalSaleStartDate(variantProductListerData.getNormalSaleStartDate()));
        }
        return productVariantResponseData;
    }

    private String formatNormalSaleStartDate(final Date normalSaleStartDate) {
        if (normalSaleStartDate != null) {
            return TargetDateUtil.getDateAsString(normalSaleStartDate, "dd MMM");
        }
        return null;
    }

    private Boolean isAssorted(final TargetProductListerData listerData) {
        Boolean isAssorted = null;
        if (CollectionUtils.isNotEmpty(listerData.getTargetVariantProductListerData())) {
            for (final TargetVariantProductListerData variantProductListerData : listerData
                    .getTargetVariantProductListerData()) {
                if (variantProductListerData.getAssorted() != null) {
                    isAssorted = variantProductListerData.getAssorted();
                    break;
                }
            }
        }
        return isAssorted;
    }

    private Boolean isDisplayOnly(final TargetProductListerData listerData) {
        Boolean isDisplayOnly = null;
        if (CollectionUtils.isNotEmpty(listerData.getTargetVariantProductListerData())) {
            for (final TargetVariantProductListerData variantProductListerData : listerData
                    .getTargetVariantProductListerData()) {
                isDisplayOnly = Boolean.valueOf(variantProductListerData.isDisplayOnly());
                if (BooleanUtils.isFalse(isDisplayOnly)) {
                    break;
                }
            }
        }
        return isDisplayOnly;
    }

    private String getImageUrl(final TargetProductListerData listerData, final int index) {
        String url = StringUtils.EMPTY;
        final List<TargetVariantProductListerData> variantProductListerDataList = listerData
                .getTargetVariantProductListerData();
        if (CollectionUtils.isNotEmpty(variantProductListerDataList)) {
            final TargetVariantProductListerData targetVariantProductListerData = variantProductListerDataList
                    .get(0);
            if (targetVariantProductListerData != null
                    && CollectionUtils.isNotEmpty(targetVariantProductListerData.getGridImageUrls())
                    && targetVariantProductListerData.getGridImageUrls().size() > index) {
                url = targetVariantProductListerData.getGridImageUrls().get(index);
            }
        }
        return splitEndecaUrlField(url);
    }

    private String splitEndecaUrlField(final String original) {
        if (StringUtils.isNotEmpty(original) && original.indexOf(ENDECA_DELIMITER) > -1) {
            final String[] parts = original.split(ENDECA_DELIMITER_PATTERN);
            if (parts != null && parts.length == 2 && StringUtils.isNotEmpty(parts[1])) {
                return parts[1];
            }
            else {
                return StringUtils.EMPTY;
            }
        }
        return original;
    }

    private String appendBukyboardNumber(final Integer bulkyBoardNumber, final String url) {
        if (bulkyBoardNumber != null) {
            final StringBuilder sb = new StringBuilder(url);
            sb.append(EndecaConstants.QUERY_PARAM_SEPERATOR_QNMRK).append(TgtFacadesConstants.BULKY_BOARD_STORE_NUMBER)
                    .append(bulkyBoardNumber);
            return sb.toString();
        }
        return url;
    }

}
