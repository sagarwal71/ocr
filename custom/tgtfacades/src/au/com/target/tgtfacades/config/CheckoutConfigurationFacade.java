/**
 * 
 */
package au.com.target.tgtfacades.config;

import au.com.target.tgtfacades.order.data.SessionData;
import au.com.target.tgtfacades.response.data.Response;


/**
 * Interface to define facade for fetching environment config for checkout.
 * 
 * @author jjayawa1
 *
 */
public interface CheckoutConfigurationFacade {

    /**
     * Returns a list of environment configuration properties to use in the checkout process.
     * 
     * @return Response
     */
    Response getCheckoutEnvConfigs();

    /**
     * Return the details of the session the current user belongs to.
     * 
     * @param orderCode
     * @return SessionData
     */
    SessionData getSessionInformation(String orderCode);
}
