/**
 * 
 */
package au.com.target.tgtfacades.config;

import de.hybris.platform.acceleratorservices.config.HostConfigService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserConstants;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.cart.data.TargetCheckoutCartData;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.constants.ThirdPartyConstants;
import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.featureswitch.data.FeatureSwitchData;
import au.com.target.tgtfacades.login.TargetAuthenticationFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.TargetOrderFacade;
import au.com.target.tgtfacades.order.data.EnvironmentData;
import au.com.target.tgtfacades.order.data.SessionData;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.response.data.EnvironmentResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.user.data.TargetCustomerInfoData;
import au.com.target.tgtfacades.util.SalesApplicationPropertyResolver;


/**
 * Facade for fetching environment config for checkout.
 * 
 * @author jjayawa1
 *
 */
public class CheckoutConfigurationFacadeImpl implements CheckoutConfigurationFacade {

    private SalesApplicationPropertyResolver salesApplicationPropertyResolver;

    private TargetCustomerFacade targetCustomerFacade;

    private HostConfigService hostConfigService;

    private String hostSuffix;

    private TargetCheckoutFacade targetCheckoutFacade;

    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    private SalesApplicationFacade salesApplicationFacade;

    private List<String> includedFeatureSwitches;

    private TargetAuthenticationFacade targetAuthenticationFacade;

    private SessionService sessionService;

    private TargetOrderFacade targetOrderFacade;

    private TargetSharedConfigService targetSharedConfigService;

    @Override
    public Response getCheckoutEnvConfigs() {
        final Response response = new Response(true);
        final EnvironmentResponseData responseData = new EnvironmentResponseData();
        final EnvironmentData envData = new EnvironmentData();
        final String serverName = hostSuffix != null ? hostSuffix : StringUtils.EMPTY;

        envData.setGaId(salesApplicationPropertyResolver.getGAValueForSalesApplication());
        envData.setGtmId(hostConfigService.getProperty(ThirdPartyConstants.TagManager.CONTAINER, serverName));
        envData.setFqdn(hostConfigService.getProperty("tgtstorefront.host.fe.fqdn",
                serverName));
        envData.setSwitches(getFeatureSwitches());
        String zipPaymentThreshold = targetSharedConfigService.getConfigByCode(TgtFacadesConstants.ZIP_PAYMENT_THRESHOLD_AMOUNT);
        if(StringUtils.isNotEmpty(zipPaymentThreshold)) {
            envData.setZipMoneyThreshold(zipPaymentThreshold);
        }
        responseData.setEnv(envData);
        response.setData(responseData);
        return response;
    }

    @Override
    public SessionData getSessionInformation(final String orderCode) {
        final SessionData sessionData = new SessionData();
        final TargetCustomerInfoData customer = targetCustomerFacade.getCustomerInfo();
        if (null != customer) {
            sessionData.setCustomer(customer);
            if (StringUtils.isNotEmpty(customer.getUid())) {
                if (customer.getUid().equals(UserConstants.ANONYMOUS_CUSTOMER_UID)) {
                    sessionData.setGuest(Boolean.TRUE);
                }
                else {
                    sessionData.setGuest(Boolean.FALSE);
                }
            }
            sessionData.setCheckoutRegisteredAsGuest(Boolean.TRUE
                    .equals(sessionService.getAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST)));
        }
        if (StringUtils.isNotBlank(orderCode) && StringUtils.isBlank(targetCustomerFacade.getCartUserEmail())) {
            sessionData.setCart(getOrderData(orderCode));
        }
        else {
            sessionData.setCart(getCheckoutCartData());
        }
        sessionData.setMobileApp(salesApplicationFacade.isSalesChannelMobileApp());
        return sessionData;
    }

    private TargetCheckoutCartData getCheckoutCartData() {
        final TargetCheckoutCartData data = new TargetCheckoutCartData();

        data.setIsGiftCardInCart(targetCheckoutFacade.isGiftCardInCart());
        data.setIsGuestCheckoutEnabled(targetCheckoutFacade.isGuestCheckoutAllowed());
        data.setCartHavingPreOrderItem(targetCheckoutFacade.isCartHavingPreOrderItem());
        data.setGiftCardMixedBasket(targetCheckoutFacade.isGiftCardMixedBasket());
        final String cartEmail = targetCustomerFacade.getCartUserEmail();
        if (StringUtils.isNotEmpty(cartEmail)) {
            data.setCheckoutEmail(cartEmail);
            isCustomerRegistered(cartEmail, data);

        }
        return data;
    }

    /**
     * Get order data for order just placed.
     * 
     * @param orderCode
     * @return TargetCheckoutCartData
     */
    private TargetCheckoutCartData getOrderData(final String orderCode) {
        final TargetCheckoutCartData data = new TargetCheckoutCartData();
        final TargetOrderData orderData = targetOrderFacade.getOrderDetailsForCode(orderCode);
        data.setIsGuestCheckoutEnabled(targetCheckoutFacade.isGuestCheckoutAllowed());
        if (orderData != null) {
            isCustomerRegistered(orderData.getEmail(), data);
        }
        return data;
    }

    private List<FeatureSwitchData> getFeatureSwitches() {
        if (CollectionUtils.isEmpty(includedFeatureSwitches)) {
            return null;
        }

        final List<FeatureSwitchData> featureSwitches = new ArrayList<>();
        for (final String featureSwitchName : includedFeatureSwitches) {
            featureSwitches.add(new FeatureSwitchData(featureSwitchName,
                    targetFeatureSwitchFacade.isFeatureEnabled(featureSwitchName)));
        }

        return featureSwitches;
    }

    private void isCustomerRegistered(final String email, final TargetCheckoutCartData data) {
        final TargetCustomerModel customer = targetCustomerFacade.getCustomerForUsername(email);
        if (customer == null) {
            data.setRegistered(false);
        }
        else {
            data.setRegistered(true);
            data.setAccountLocked(targetAuthenticationFacade.isAccountLocked(customer));
        }
    }

    /**
     * @param salesApplicationPropertyResolver
     *            the salesApplicationPropertyResolver to set
     */
    @Required
    public void setSalesApplicationPropertyResolver(
            final SalesApplicationPropertyResolver salesApplicationPropertyResolver) {
        this.salesApplicationPropertyResolver = salesApplicationPropertyResolver;
    }

    /**
     * @param targetCustomerFacade
     *            the targetCustomerFacade to set
     */
    @Required
    public void setTargetCustomerFacade(final TargetCustomerFacade targetCustomerFacade) {
        this.targetCustomerFacade = targetCustomerFacade;
    }

    /**
     * @param hostConfigService
     *            the hostConfigService to set
     */
    @Required
    public void setHostConfigService(final HostConfigService hostConfigService) {
        this.hostConfigService = hostConfigService;
    }

    /**
     * @param hostSuffix
     *            the hostSuffix to set
     */
    @Required
    public void setHostSuffix(final String hostSuffix) {
        this.hostSuffix = hostSuffix;
    }

    /**
     * @param targetCheckoutFacade
     *            the targetCheckoutFacade to set
     */
    @Required
    public void setTargetCheckoutFacade(final TargetCheckoutFacade targetCheckoutFacade) {
        this.targetCheckoutFacade = targetCheckoutFacade;
    }

    /**
     * @param targetFeatureSwitchFacade
     *            the targetFeatureSwitchFacade to set
     */
    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }

    /**
     * @param includedFeatureSwitches
     *            the includedFeatureSwitches to set
     */
    public void setIncludedFeatureSwitches(final List<String> includedFeatureSwitches) {
        this.includedFeatureSwitches = includedFeatureSwitches;
    }

    /*
     * @param salesApplicationFacade
     *            the salesApplicationFacade to set
     */
    @Required
    public void setSalesApplicationFacade(final SalesApplicationFacade salesApplicationFacade) {
        this.salesApplicationFacade = salesApplicationFacade;
    }

    /**
     * @param targetAuthenticationFacade
     *            the targetAuthenticationFacade to set
     */
    @Required
    public void setTargetAuthenticationFacade(final TargetAuthenticationFacade targetAuthenticationFacade) {
        this.targetAuthenticationFacade = targetAuthenticationFacade;
    }

    /**
     * @param sessionService
     *            the sessionService to set
     */
    @Required
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

    /**
     * @param targetOrderFacade
     *            the targetOrderFacade to set
     */
    @Required
    public void setTargetOrderFacade(final TargetOrderFacade targetOrderFacade) {
        this.targetOrderFacade = targetOrderFacade;
    }

    public TargetSharedConfigService getTargetSharedConfigService() {
        return targetSharedConfigService;
    }

    @Required
    public void setTargetSharedConfigService(final TargetSharedConfigService targetSharedConfigService) {
        this.targetSharedConfigService = targetSharedConfigService;
    }
}
