/**
 * 
 */
package au.com.target.tgtfacades.config.impl;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtfacades.config.TargetSharedConfigFacade;


/**
 * @author mjanarth
 *
 */
public class TargetSharedConfigFacadeImpl implements TargetSharedConfigFacade {

    private TargetSharedConfigService targetSharedConfigService;

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.config.TargetSharedConfigFacade#getConfigByCode(java.lang.String)
     */
    @Override
    public String getConfigByCode(final String configCode) {
        return targetSharedConfigService.getConfigByCode(configCode);
    }

    @Override
    public int getInt(final String configCode, final int defaultValue) {
        return targetSharedConfigService.getInt(configCode, defaultValue);
    }

    /**
     * @param targetSharedConfigService
     *            the targetSharedConfigService to set
     */
    @Required
    public void setTargetSharedConfigService(final TargetSharedConfigService targetSharedConfigService) {
        this.targetSharedConfigService = targetSharedConfigService;
    }

}
