/**
 * 
 */
package au.com.target.tgtfacades.delivery;

import java.util.List;

import au.com.target.tgtfacades.delivery.data.TargetRegionData;


/**
 * @author rmcalave
 * 
 */
public interface TargetDeliveryFacade {
    /**
     * Retrieve a list of states or regions for the given country.
     * 
     * @param countryIsoCode
     *            The ISO code of the country
     * @return A list of states or regions for the given country
     * @throws IllegalArgumentException
     *             is countryIsoCode is null
     */
    List<TargetRegionData> getRegionsForCountry(String countryIsoCode);

    /**
     * Calls delivery service to check whether the given post code is valid for the given delivery mode.
     * 
     * @param deliveryModeCode
     * @param postCode
     * @return true if post code valid
     */
    boolean isDeliveryModePostCodeCombinationValid(final String deliveryModeCode, final String postCode);
}
