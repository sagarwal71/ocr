/**
 * 
 */
package au.com.target.tgtfacades.delivery.data;

/**
 * @author rmcalave
 * 
 */
public class TargetRegionData {
    private String name;

    private String abbreviation;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the abbreviation
     */
    public String getAbbreviation() {
        return abbreviation;
    }

    /**
     * @param abbreviation
     *            the abbreviation to set
     */
    public void setAbbreviation(final String abbreviation) {
        this.abbreviation = abbreviation;
    }
}
