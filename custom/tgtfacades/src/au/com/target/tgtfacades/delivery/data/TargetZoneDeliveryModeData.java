/**
 * 
 */
package au.com.target.tgtfacades.delivery.data;

import de.hybris.platform.commercefacades.order.data.ZoneDeliveryModeData;



/**
 * @author dcwillia
 * 
 */
public class TargetZoneDeliveryModeData extends ZoneDeliveryModeData {

    private boolean available;
    private String shortDescription;
    private String longDescription;
    private boolean deliveryToStore;
    private int displayOrder;
    private String incentiveMetMessage;
    private String potentialIncentiveMessage;
    private boolean postCodeNotSupported;
    private boolean digitalDelivery;
    private String disclaimer;
    private Boolean feeRange;
    private ShipsterData shipsterData;


    /**
     * @return the incentiveMetMessage
     */
    public String getIncentiveMetMessage() {
        return incentiveMetMessage;
    }

    /**
     * @param incentiveMetMessage
     *            the incentiveMetMessage to set
     */
    public void setIncentiveMetMessage(final String incentiveMetMessage) {
        this.incentiveMetMessage = incentiveMetMessage;
    }

    /**
     * @return the potentialIncentiveMessage
     */
    public String getPotentialIncentiveMessage() {
        return potentialIncentiveMessage;
    }

    /**
     * @param potentialIncentiveMessage
     *            the potentialIncentiveMessage to set
     */
    public void setPotentialIncentiveMessage(final String potentialIncentiveMessage) {
        this.potentialIncentiveMessage = potentialIncentiveMessage;
    }

    /**
     * @return the available
     */
    public boolean isAvailable() {
        return available;
    }

    /**
     * @param available
     *            the available to set
     */
    public void setAvailable(final boolean available) {
        this.available = available;
    }

    /**
     * @return the longDescription
     */
    public String getLongDescription() {
        return longDescription;
    }

    /**
     * @param longDescription
     *            the longDescription to set
     */
    public void setLongDescription(final String longDescription) {
        this.longDescription = longDescription;
    }

    /**
     * @return the shortDescription
     */
    public String getShortDescription() {
        return shortDescription;
    }

    /**
     * @param shortDescription
     *            the shortDescription to set
     */
    public void setShortDescription(final String shortDescription) {
        this.shortDescription = shortDescription;
    }

    /**
     * @return the deliveryToStore
     */
    public boolean isDeliveryToStore() {
        return deliveryToStore;
    }

    /**
     * @param deliveryToStore
     *            the deliveryToStore to set
     */
    public void setDeliveryToStore(final boolean deliveryToStore) {
        this.deliveryToStore = deliveryToStore;
    }

    /**
     * @return the displayOrder
     */
    public int getDisplayOrder() {
        return displayOrder;
    }

    /**
     * @param displayOrder
     *            the displayOrder to set
     */
    public void setDisplayOrder(final int displayOrder) {
        this.displayOrder = displayOrder;
    }

    /**
     * @return the postCodeNotSupported
     */
    public boolean isPostCodeNotSupported() {
        return postCodeNotSupported;
    }

    /**
     * @param postCodeNotSupported
     *            the postCodeNotSupported to set
     */
    public void setPostCodeNotSupported(final boolean postCodeNotSupported) {
        this.postCodeNotSupported = postCodeNotSupported;
    }

    /**
     * @return the digitalDelivery
     */
    public boolean isDigitalDelivery() {
        return digitalDelivery;
    }

    /**
     * @param digitalDelivery
     *            the digitalDelivery to set
     */
    public void setDigitalDelivery(final boolean digitalDelivery) {
        this.digitalDelivery = digitalDelivery;
    }

    /**
     * @return the disclaimer
     */
    public String getDisclaimer() {
        return disclaimer;
    }

    /**
     * @param disclaimer
     *            the disclaimer to set
     */
    public void setDisclaimer(final String disclaimer) {
        this.disclaimer = disclaimer;
    }

    /**
     * @return the feeRange
     */
    public Boolean getFeeRange() {
        return feeRange;
    }

    /**
     * @param feeRange
     *            the feeRange to set
     */
    public void setFeeRange(final Boolean feeRange) {
        this.feeRange = feeRange;
    }

    /**
     * @return the shipsterData
     */
    public ShipsterData getShipsterData() {
        return shipsterData;
    }

    /**
     * @param shipsterData
     *            the shipsterData to set
     */
    public void setShipsterData(final ShipsterData shipsterData) {
        this.shipsterData = shipsterData;
    }

}
