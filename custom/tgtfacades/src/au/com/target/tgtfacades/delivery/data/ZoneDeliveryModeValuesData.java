/**
 * 
 */
package au.com.target.tgtfacades.delivery.data;

import java.util.List;


/**
 * @author dcwillia
 * 
 *         Lists are sorted descending on threshold value
 */
public class ZoneDeliveryModeValuesData {
    private List<ZoneDeliveryModeValueData> homeDelivery;
    private List<ZoneDeliveryModeValueData> clickNCollect;

    /**
     * @return the homeDelivery
     */
    public List<ZoneDeliveryModeValueData> getHomeDelivery() {
        return homeDelivery;
    }

    /**
     * @param homeDelivery
     *            the homeDelivery to set
     */
    public void setHomeDelivery(final List<ZoneDeliveryModeValueData> homeDelivery) {
        this.homeDelivery = homeDelivery;
    }

    /**
     * @return the clickNCollect
     */
    public List<ZoneDeliveryModeValueData> getClickNCollect() {
        return clickNCollect;
    }

    /**
     * @param clickNCollect
     *            the clickNCollect to set
     */
    public void setClickNCollect(final List<ZoneDeliveryModeValueData> clickNCollect) {
        this.clickNCollect = clickNCollect;
    }
}
