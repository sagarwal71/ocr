/**
 * 
 */
package au.com.target.tgtfacades.url.impl;

import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.util.TargetServicesUtil;
import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;

import com.endeca.infront.cartridge.model.DimensionSearchValue;


/**
 * @author bhuang3
 *
 */
public class TargetBrandDimensionSearchUrlResolver extends AbstractUrlResolver<DimensionSearchValue> {

    private static final String BASE_BRAND_PAGE_URL = "/b/";
    private List<UrlTokenTransformer> urlTokenTransformers = new ArrayList<>();

    @Override
    protected String resolveInternal(final DimensionSearchValue dimensionSearchValue) {
        final StringBuilder url = new StringBuilder();
        if (dimensionSearchValue != null) {
            final String brandName = dimensionSearchValue.getLabel();
            if (StringUtils.isNotEmpty(brandName)) {
                final String brandCode = TargetServicesUtil.extractSafeCodeFromName(brandName, true);
                url.append(BASE_BRAND_PAGE_URL).append(brandCode);
                final String navigationState = dimensionSearchValue.getNavigationState();
                if (StringUtils.isNotEmpty(navigationState)) {
                    url.append(navigationState);
                }
            }
        }
        return url.toString();
    }

    @Override
    protected String urlSafe(final String text) {
        String transformedText = text;
        for (final UrlTokenTransformer urlTokenTransformer : getUrlTokenTransformers()) {
            transformedText = urlTokenTransformer.transform(transformedText);
        }
        return super.urlSafe(transformedText);
    }

    @Required
    public void setUrlTokenTransformers(final List<UrlTokenTransformer> urlTokenTransformers) {
        this.urlTokenTransformers = urlTokenTransformers;
    }

    /**
     * @return the urlTokenTransformers
     */
    protected List<UrlTokenTransformer> getUrlTokenTransformers() {
        return urlTokenTransformers;
    }

}
