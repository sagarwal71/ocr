package au.com.target.tgtfacades.url.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.DefaultCategoryModelUrlResolver;

import java.util.ArrayList;
import java.util.List;

import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;
import au.com.target.tgtfacades.url.transform.UrlTransformer;


/**
 * Extension to {@link DefaultCategoryModelUrlResolver} which applies url and token -level category URL transformations,
 * imposed by Target standards.
 */
public class TargetCategoryModelUrlResolver extends DefaultCategoryModelUrlResolver {

    private List<UrlTransformer> urlTransformers = new ArrayList<>();
    private List<UrlTokenTransformer> urlTokenTransformers = new ArrayList<>();


    @Override
    protected String resolveInternal(final CategoryModel source) {
        String url = super.resolveInternal(source);
        for (final UrlTransformer urlTransformer : getUrlTransformers()) {
            url = urlTransformer.transform(url);
        }
        return url;
    }

    @Override
    protected String urlSafe(final String text) {
        String transformedText = text;
        for (final UrlTokenTransformer urlTokenTransformer : getUrlTokenTransformers()) {
            transformedText = urlTokenTransformer.transform(transformedText);
        }
        return super.urlSafe(transformedText);
    }

    @Override
    protected String buildPathString(final List<CategoryModel> path)
    {
        final StringBuilder result = new StringBuilder();

        boolean first = true;
        for (final CategoryModel category : path) {
            if (!(category instanceof TargetProductCategoryModel)) {
                continue;
            }

            if (category.getSupercategories().isEmpty() && path.size() > 1) {
                continue;
            }

            if (!first) {
                result.append('/');
            }

            result.append(urlSafe(category.getName()));
            first = false;
        }

        return result.toString();
    }

    /**
     * Returns the list of url level transformers.
     * 
     * @return the list of transformers
     */
    public List<UrlTransformer> getUrlTransformers() {
        return urlTransformers;
    }

    /**
     * Sets the list of url level transformers
     * 
     * @param urlTransformers
     *            the list of transformers to set
     */
    public void setUrlTransformers(final List<UrlTransformer> urlTransformers) {
        this.urlTransformers = urlTransformers;
    }

    /**
     * Returns the list of url token level transformers.
     * 
     * @return the list of transformers
     */
    public List<UrlTokenTransformer> getUrlTokenTransformers() {
        return urlTokenTransformers;
    }

    /**
     * Sets the list of url token level transformers.
     * 
     * @param urlTokenTransformers
     *            the list of transformers to set
     */
    public void setUrlTokenTransformers(final List<UrlTokenTransformer> urlTokenTransformers) {
        this.urlTokenTransformers = urlTokenTransformers;
    }
}
