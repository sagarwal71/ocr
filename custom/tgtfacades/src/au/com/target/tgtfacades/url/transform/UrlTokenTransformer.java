package au.com.target.tgtfacades.url.transform;

/**
 * Processes a single URL part(s) between forward slashes. Handy for cleansing particular category
 * or product name before it's encoded into full URL.
 */
public interface UrlTokenTransformer {

    /**
     * Processes a token before it's encoded into full URL.
     *
     * @param token the token to cleanse
     * @return cleansed token
     */
    String transform(String token);
}
