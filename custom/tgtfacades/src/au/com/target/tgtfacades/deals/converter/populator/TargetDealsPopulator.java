/**
 *
 */
package au.com.target.tgtfacades.deals.converter.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.deals.data.TargetDealData;
import au.com.target.tgtfacades.offer.converter.TargetMobileOfferHeadingListConverter;


/**
 * The Class TargetDealsPopulator.
 *
 * @author pthoma20
 */
public class TargetDealsPopulator implements
        Populator<AbstractDealModel, TargetDealData> {

    private TargetMobileOfferHeadingListConverter targetMobileOfferHeadingListConverter;

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.impl.AbstractPopulatingConverter#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final AbstractDealModel source, final TargetDealData target) {
        target.setCode(source.getCode());
        target.setTitle(source.getTitle());
        target.setStartDate(source.getStartDate());
        target.setEndDate(source.getEndDate());
        target.setPriority(source.getPriority());
        target.setLongTitle(source.getLongTitle());
        target.setEnabled(source.getEnabled());
        target.setFeatured(source.getFeatured());
        target.setDescription(source.getDescription());
        if (null != source.getStartDate()) {
            target.setStartTimeFormatted(new DateTime(source.getStartDate()));
        }
        if (null != source.getEndDate()) {
            target.setEndTimeFormatted(new DateTime(source.getEndDate()));
        }
        if (null != source.getCreationtime()) {
            target.setCreatedTimeFormatted(new DateTime(source.getCreationtime()));
        }
        if (null != source.getModifiedtime()) {
            target.setModifiedTimeFormatted(new DateTime(source.getModifiedtime()));
        }
        target.setAvailableForMobile(source.getAvailableForMobile());
        if (source instanceof AbstractSimpleDealModel) {
            final AbstractSimpleDealModel abstractSimpleDealModel = (AbstractSimpleDealModel)source;
            target.setPageTitle(abstractSimpleDealModel.getPageTitle());
            target.setFacetTitle(abstractSimpleDealModel.getFacetTitle());
        }
        target.setTargetMobileOfferHeadings(targetMobileOfferHeadingListConverter
                .convert(source.getTargetMobileOfferHeadings()));
    }

    /**
     * @param targetMobileOfferHeadingListConverter
     *            the targetMobileOfferHeadingListConverter to set
     */
    @Required
    public void setTargetMobileOfferHeadingListConverter(
            final TargetMobileOfferHeadingListConverter targetMobileOfferHeadingListConverter) {
        this.targetMobileOfferHeadingListConverter = targetMobileOfferHeadingListConverter;
    }
}