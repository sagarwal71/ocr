/**
 * 
 */
package au.com.target.tgtfacades.voucher.data;

import de.hybris.platform.commercefacades.product.data.PriceData;

import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * DTO object to hold voucher data.
 * 
 * @author jjayawa1
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)

public class VoucherData {
    private String code;

    private PriceData discount;

    /**
     * @return the discount
     */
    public PriceData getDiscount() {
        return discount;
    }

    /**
     * @param discount
     *            the discount to set
     */
    public void setDiscount(final PriceData discount) {
        this.discount = discount;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }
}
