/**
 * 
 */
package au.com.target.tgtfacades.voucher.impl;

import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.voucher.model.DateRestrictionModel;
import de.hybris.platform.voucher.model.PromotionVoucherModel;
import de.hybris.platform.voucher.model.RestrictionModel;
import de.hybris.platform.voucher.model.VoucherModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.voucher.service.TargetVoucherService;
import au.com.target.tgtfacades.deals.data.TargetVouchersData;
import au.com.target.tgtfacades.offer.TargetMobileOfferHeadingFacade;
import au.com.target.tgtfacades.voucher.TargetVoucherFacade;
import au.com.target.tgtfacades.voucher.data.TargetPromotionVoucherData;
import au.com.target.tgtmarketing.voucher.TargetMobileVoucherService;


/**
 * @author paul
 *
 */
public class TargetVoucherFacadeImpl implements TargetVoucherFacade {

    private TargetMobileVoucherService targetMobileVoucherService;

    private TargetMobileOfferHeadingFacade targetMobileOfferHeadingFacade;

    private Converter<PromotionVoucherModel, TargetPromotionVoucherData> targetPromotionVoucherConverter;

    private TargetVoucherService targetVoucherService;

    private TimeService timeService;

    protected List<TargetPromotionVoucherData> getAllMobileActivePromotionVouchers() {
        final List<TargetPromotionVoucherData> targetPromotionVouchersDataList = new ArrayList<>();
        final List<PromotionVoucherModel> promotionVoucherModelList = targetMobileVoucherService
                .getAllActiveMobileVouchers();

        if (CollectionUtils.isEmpty(promotionVoucherModelList)) {
            return targetPromotionVouchersDataList;
        }
        for (final PromotionVoucherModel promotionVoucherModel : promotionVoucherModelList) {
            final TargetPromotionVoucherData targetPromotionVoucherData = targetPromotionVoucherConverter
                    .convert(promotionVoucherModel);
            targetPromotionVouchersDataList.add(targetPromotionVoucherData);
        }
        return targetPromotionVouchersDataList;
    }

    @Override
    public TargetVouchersData getAllMobileActivePromotionVouchersWithOfferHeadings() {
        final TargetVouchersData targetVouchersData = new TargetVouchersData();
        targetVouchersData.setTargetPromotionVouchers(getAllMobileActivePromotionVouchers());
        targetVouchersData.setTargetMobileOfferHeadings(targetMobileOfferHeadingFacade.getTargetMobileOfferHeadings());
        return targetVouchersData;
    }

    @Override
    public boolean doesExist(final String voucherCode) {
        Assert.notNull(voucherCode, "Voucher code cannot be null");
        return null != targetVoucherService.getVoucher(voucherCode);
    }

    @Override
    public boolean isExpired(final String voucherCode) {
        Assert.notNull(voucherCode, "Voucher code cannot be null");
        final Date currentTime = timeService.getCurrentTime();
        Assert.notNull(currentTime, "Cannot compute current time");
        final VoucherModel voucher = targetVoucherService.getVoucher(voucherCode);
        if (null != voucher && CollectionUtils.isNotEmpty(voucher.getRestrictions())) {
            for (final RestrictionModel restriction : voucher.getRestrictions()) {
                if (restriction instanceof DateRestrictionModel) {
                    final DateRestrictionModel dateRestriction = (DateRestrictionModel)restriction;
                    if (null != dateRestriction.getEndDate() && dateRestriction.getEndDate().before(currentTime)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param targetMobileOfferHeadingFacade
     *            the targetMobileOfferHeadingFacade to set
     */
    @Required
    public void setTargetMobileOfferHeadingFacade(final TargetMobileOfferHeadingFacade targetMobileOfferHeadingFacade) {
        this.targetMobileOfferHeadingFacade = targetMobileOfferHeadingFacade;
    }

    /**
     * @param targetMobileVoucherService
     *            the targetMobileVoucherService to set
     */
    @Required
    public void setTargetMobileVoucherService(final TargetMobileVoucherService targetMobileVoucherService) {
        this.targetMobileVoucherService = targetMobileVoucherService;
    }

    /**
     * @param targetPromotionVoucherConverter
     *            the targetPromotionVoucherConverter to set
     */
    @Required
    public void setTargetPromotionVoucherConverter(
            final Converter<PromotionVoucherModel, TargetPromotionVoucherData> targetPromotionVoucherConverter) {
        this.targetPromotionVoucherConverter = targetPromotionVoucherConverter;
    }

    /**
     * @param targetVoucherService
     *            the targetVoucherService to set
     */
    @Required
    public void setTargetVoucherService(final TargetVoucherService targetVoucherService) {
        this.targetVoucherService = targetVoucherService;
    }

    /**
     * @param timeService
     *            the timeService to set
     */
    @Required
    public void setTimeService(final TimeService timeService) {
        this.timeService = timeService;
    }
}
