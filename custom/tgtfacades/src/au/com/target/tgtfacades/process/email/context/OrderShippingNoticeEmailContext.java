/**
 *
 */
package au.com.target.tgtfacades.process.email.context;

import static java.util.Collections.sort;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.tools.generic.DateTool;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.order.data.TargetOrderData;


/**
 * Email context for shipping notice emails, with tax invoice.
 */
public class OrderShippingNoticeEmailContext extends OrderNotificationEmailContext {

    private ConsignmentData consignment;
    private List<ConsignmentData> shippedConsignments;
    private List<ConsignmentData> sentDigitalConsignments;
    private List<OrderEntryData> inProgressEntries;
    private Converter<ConsignmentModel, ConsignmentData> consignmentConverter;
    private DateTool dateTool;



    /*
     * (non-Javadoc)
     *
     * @see
     * au.com.target.tgtfacades.process.email.context.OrderNotificationEmailContext#
     * init(de.hybris.platform.processengine.model.BusinessProcessModel,
     * de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel)
     */
    @Override
    public void init(final BusinessProcessModel businessProcessModel, final EmailPageModel emailPageModel) {
        super.init(businessProcessModel, emailPageModel);
        dateTool = new DateTool();
        if (businessProcessModel instanceof OrderProcessModel) {
            final ConsignmentModel consignmentModel = getOrderProcessParameterHelper()
                    .getConsignment((OrderProcessModel)businessProcessModel);
            if (consignmentModel != null) {
                consignment = consignmentConverter.convert(consignmentModel);
            }
        }




        if (getOrder() != null && CollectionUtils.isNotEmpty(getOrder().getConsignment())
                && CollectionUtils.size(getOrder().getConsignment()) > 1) {
            sortOrderConsignmentsByStatusDate(getOrder().getConsignment());
            updateShippedConsignmentList(getOrder(), consignment);
        }

        if (getOrder() != null && CollectionUtils.isNotEmpty(getOrder().getEntries())) {
            inProgressEntries = new ArrayList<>();
            inProgressEntries.addAll(getOrder().getEntries());
            updateInProgressEntries();
        }
    }

    protected void sortOrderConsignmentsByStatusDate(final List<ConsignmentData> consignments) {
        if (CollectionUtils.isNotEmpty(consignments)) {
            sort(consignments, new Comparator<ConsignmentData>() {
                public int compare(final ConsignmentData c1, final ConsignmentData c2) {
                    if (c1.getStatusDate() == null || c2.getStatusDate() == null) {
                        return 0;
                    }
                    else {
                        return c1.getStatusDate().compareTo(c2.getStatusDate());
                    }
                }
            });
        }
    }

    protected void updateShippedConsignmentList(final TargetOrderData orderData,
            final ConsignmentData currentConsignment) {
        final List<ConsignmentData> consignments = orderData.getConsignment();

        for (final ConsignmentData orderConsignment : consignments) {
            if ((currentConsignment == null || (orderConsignment != null
                    && !StringUtils.equals(currentConsignment.getCode(), orderConsignment.getCode())))) {

                addToShippedOrSentConsignmentList(orderConsignment);
            }
        }
    }

    private void updateInProgressEntries() {

        if (CollectionUtils.isNotEmpty(shippedConsignments)) {
            updateInProgressItemsFromConsignments(shippedConsignments);
        }
        if (CollectionUtils.isNotEmpty(sentDigitalConsignments)) {
            updateInProgressItemsFromConsignments(sentDigitalConsignments);
        }

        if (consignment != null) {
            updateInProgressItemFromConsignment(consignment);
        }
    }

    private void updateInProgressEntryQuantity(final Long shippedOrCancelledQty,
            final Iterator<OrderEntryData> inProgressEntriesIter, final OrderEntryData inProgressEntry) {

        if (inProgressEntry.getQuantity().equals(shippedOrCancelledQty)) {
            inProgressEntriesIter.remove();
        }
        else {
            inProgressEntry.setQuantity(inProgressEntry.getQuantity() - shippedOrCancelledQty);
        }
    }

    private void updateInProgressItemsFromConsignments(final List<ConsignmentData> shippedConsignments) {
        for (final ConsignmentData shippedConsignment : shippedConsignments) {
            updateInProgressItemFromConsignment(shippedConsignment);
        }
    }

    private void updateInProgressItemFromConsignment(final ConsignmentData consignmentData) {
        for (final ConsignmentEntryData consignmentEntryData : consignmentData.getEntries()) {

            final Iterator<OrderEntryData> inProgressEntriesIterator = inProgressEntries.iterator();
            while (inProgressEntriesIterator.hasNext()) {
                final OrderEntryData inProgressEntry = inProgressEntriesIterator.next();
                if (checkIfOrderEntryProductIsShipped(consignmentEntryData, inProgressEntry)) {
                    updateInProgressEntryQuantity(consignmentEntryData.getShippedQuantity(), inProgressEntriesIterator,
                            inProgressEntry);
                }
            }
        }
    }

    private boolean checkIfOrderEntryProductIsShipped(final ConsignmentEntryData consignmentEntryData,
            final OrderEntryData inProgressEntry) {
        if (inProgressEntry == null || consignmentEntryData == null || inProgressEntry.getProduct() == null
                || consignmentEntryData.getOrderEntry() == null) {
            return false;
        }
        else if (consignmentEntryData.getOrderEntry().getProduct() != null
                && StringUtils.equals(inProgressEntry.getProduct().getCode(),
                        consignmentEntryData.getOrderEntry().getProduct().getCode())
                && consignmentEntryData.getShippedQuantity() != null) {
            return true;
        }

        return false;
    }

    private void addToShippedOrSentConsignmentList(final ConsignmentData consignmentData) {
        if (consignmentData.isIsDigitalDelivery()) {
            if (sentDigitalConsignments == null) {
                sentDigitalConsignments = new ArrayList<>();
            }
            sentDigitalConsignments.add(consignmentData);
        }
        else if (ConsignmentStatus.SHIPPED.equals(consignmentData.getStatus())) {
            if (shippedConsignments == null) {
                shippedConsignments = new ArrayList<>();
            }
            shippedConsignments.add(consignmentData);
        }
    }

    /**
     * @return the dateTool
     */
    public DateTool getDateTool() {
        return dateTool;
    }

    /**
     * @return the consignment
     */
    public ConsignmentData getConsignment() {
        return consignment;
    }

    /**
     * @return the shippedConsignments
     */
    public List<ConsignmentData> getShippedConsignments() {
        return shippedConsignments;
    }

    /**
     * @return the sentDigitalConsignments
     */
    public List<ConsignmentData> getSentDigitalConsignments() {
        return sentDigitalConsignments;
    }

    /**
     * @return the inProgressEntries
     */
    public List<OrderEntryData> getInProgressEntries() {
        return inProgressEntries;
    }

    /**
     * @param consignmentConverter
     *            the consignmentConverter to set
     */
    @Required
    public void setConsignmentConverter(final Converter<ConsignmentModel, ConsignmentData> consignmentConverter) {
        this.consignmentConverter = consignmentConverter;
    }

}
