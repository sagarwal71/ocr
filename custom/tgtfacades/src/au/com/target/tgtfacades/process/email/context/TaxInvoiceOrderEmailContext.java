/**
 * 
 */
package au.com.target.tgtfacades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.taxinvoice.TaxInvoiceService;
import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * Order notification email which has the tax invoice as attachment
 * 
 */
public class TaxInvoiceOrderEmailContext extends OrderNotificationEmailContext {

    private TaxInvoiceService taxInvoiceService;
    private MediaModel taxInvoiceMedia;

    @Override
    public void init(final BusinessProcessModel businessProcessModel, final EmailPageModel emailPageModel) {
        super.init(businessProcessModel, emailPageModel);
        if (businessProcessModel instanceof OrderProcessModel)
        {
            final OrderProcessModel orderProcess = (OrderProcessModel)businessProcessModel;

            final OrderModel orderModel = orderProcess.getOrder();
            taxInvoiceMedia = taxInvoiceService.getTaxInvoiceForOrder(orderModel);
            final String newEmail = (String)getOrderProcessParameterHelper().getNewEmailAddr(orderProcess);

            if (StringUtils.isNotEmpty(newEmail) && TargetValidationCommon.Email.PATTERN.matcher(newEmail).matches()) {
                put(EMAIL, newEmail);
            }
        }
    }

    @Override
    public List<MediaModel> getAttachments() {

        if (taxInvoiceMedia != null) {
            return Arrays.asList(taxInvoiceMedia);
        }

        return super.getAttachments();
    }

    /**
     * @param taxInvoiceService
     *            the taxInvoiceService to set
     */
    @Required
    public void setTaxInvoiceService(final TaxInvoiceService taxInvoiceService) {
        this.taxInvoiceService = taxInvoiceService;
    }


}
