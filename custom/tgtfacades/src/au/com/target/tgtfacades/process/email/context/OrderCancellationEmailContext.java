/**
 * 
 */
package au.com.target.tgtfacades.process.email.context;

import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;


/**
 * Context for order cancellation emails
 * 
 */
public class OrderCancellationEmailContext extends OrderModificationEmailContext {

    @Override
    protected OrderModificationRecordEntryModel getModificationRecord(final BusinessProcessModel businessProcessModel) {
        return getOrderProcessParameterHelper().getOrderCancelRequest((OrderProcessModel)businessProcessModel);
    }
}
