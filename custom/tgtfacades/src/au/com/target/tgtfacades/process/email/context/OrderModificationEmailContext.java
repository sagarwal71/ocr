/**
 * 
 */
package au.com.target.tgtfacades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtfacades.order.converters.OrderEntryModificationRecordEntryConverter;
import au.com.target.tgtfacades.order.data.OrderModificationData;


/**
 * Base context for cancel or refund emails
 * 
 */
public abstract class OrderModificationEmailContext extends OrderNotificationEmailContext {
    private static final Logger LOG = Logger.getLogger(OrderModificationEmailContext.class);

    private OrderEntryModificationRecordEntryConverter orderEntryModificationRecordEntryConverter;
    private PriceDataFactory priceDataFactory;

    private List<OrderModificationData> orderModificationDataList;
    private PriceData refundAmount;
    private PriceData deliveryRefundAmount;


    /**
     * This might be a cancel record or a return record
     */
    protected abstract OrderModificationRecordEntryModel getModificationRecord(
            final BusinessProcessModel businessProcessModel);



    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.process.email.context.OrderNotificationEmailContext#init(de.hybris.platform.processengine.model.BusinessProcessModel, de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel)
     */
    @Override
    public void init(final BusinessProcessModel businessProcessModel, final EmailPageModel emailPageModel) {
        super.init(businessProcessModel, emailPageModel);

        if (businessProcessModel instanceof OrderProcessModel) {

            final OrderModificationRecordEntryModel orderCancelRecordEntry = getModificationRecord(businessProcessModel);

            if (orderCancelRecordEntry == null) {
                LOG.warn("orderCancelRecordEntry is null for process : " + businessProcessModel.getCode());
                return;
            }

            readOrderEntryModificationData(orderCancelRecordEntry);

            final CurrencyModel currency = getCurrency(orderCancelRecordEntry);

            if (currency == null) {
                LOG.warn("currency is null for process : " + businessProcessModel.getCode());
            }

            readRefundAmount((OrderProcessModel)businessProcessModel, currency);

            readDeliveryRefundAmount(orderCancelRecordEntry, currency);
        }
    }


    /**
     * Convert the data for the modification entries
     * 
     * @param orderCancelRecordEntry
     */
    protected void readOrderEntryModificationData(final OrderModificationRecordEntryModel orderCancelRecordEntry) {

        orderModificationDataList = new ArrayList<>();
        for (final OrderEntryModificationRecordEntryModel orderEntryModificationRecordEntryModel : orderCancelRecordEntry
                .getOrderEntriesModificationEntries()) {
            if (orderEntryModificationRecordEntryModel.getOrderEntry() == null) {
                LOG.warn("Can't load order entry for order modifications, orderCancelRecordEntry : "
                        + orderCancelRecordEntry.getCode());
                continue;
            }

            orderModificationDataList.add(getOrderEntryModificationRecordEntryConverter().convert(
                    orderEntryModificationRecordEntryModel));
        }

    }


    /**
     * Get the refund amount from the process and convert
     * 
     * @param process
     * @param currency
     */
    protected void readRefundAmount(final OrderProcessModel process, final CurrencyModel currency) {
        Assert.notNull(currency, "currency must not be null");

        final BigDecimal processAmount = getOrderProcessParameterHelper().getRefundAmount(process);
        if (processAmount == null) {
            LOG.warn("Refund amount is null in process: " + process.getCode());
            return;
        }

        refundAmount = createPrice(currency, processAmount);
    }


    /**
     * Get the refunded delivery cost and convert
     * 
     * @param orderCancelRecordEntry
     * @param currency
     */
    protected void readDeliveryRefundAmount(final OrderModificationRecordEntryModel orderCancelRecordEntry,
            final CurrencyModel currency) {
        Assert.notNull(currency, "currency must not be null");

        final Double dDeliveryRefundAmount = orderCancelRecordEntry.getRefundedShippingAmount();
        if (dDeliveryRefundAmount == null) {
            LOG.warn("Delivery Refund amount is null in order cancel record: " + orderCancelRecordEntry.getCode());
            return;
        }

        deliveryRefundAmount = createPrice(currency, BigDecimal.valueOf(dDeliveryRefundAmount.doubleValue()));
    }



    /**
     * Return the currency for this order
     * 
     * @param orderCancelRecordEntry
     * @return currency
     */
    protected CurrencyModel getCurrency(final OrderModificationRecordEntryModel orderCancelRecordEntry) {

        // Convert refund amount and delivery refund amount
        if (orderCancelRecordEntry.getModificationRecord() != null) {

            final OrderModel order = orderCancelRecordEntry.getModificationRecord().getOrder();

            return order.getCurrency();
        }

        return null;
    }


    /**
     * Copied code from Hybris base Converter
     * 
     * @param currency
     * @param val
     * @return PriceData
     */
    protected PriceData createPrice(final CurrencyModel currency, final BigDecimal val)
    {
        Assert.notNull(currency, "currency must not be null");

        return priceDataFactory.create(PriceDataType.BUY, val, currency.getIsocode());
    }


    /**
     * @return the orderModificationDataList
     */
    public List<OrderModificationData> getOrderModificationDataList() {
        return orderModificationDataList;
    }


    /**
     * @return the refundAmount
     */
    public PriceData getRefundAmount() {
        return refundAmount;
    }


    /**
     * @return the deliveryRefundAmount
     */
    public PriceData getDeliveryRefundAmount() {
        return deliveryRefundAmount;
    }


    /**
     * @return the orderEntryModificationRecordEntryConverter
     */
    protected OrderEntryModificationRecordEntryConverter getOrderEntryModificationRecordEntryConverter() {
        return orderEntryModificationRecordEntryConverter;
    }

    /**
     * @param orderEntryModificationRecordEntryConverter
     *            the orderEntryModificationRecordEntryConverter to set
     */
    @Required
    public void setOrderEntryModificationRecordEntryConverter(
            final OrderEntryModificationRecordEntryConverter orderEntryModificationRecordEntryConverter) {
        this.orderEntryModificationRecordEntryConverter = orderEntryModificationRecordEntryConverter;
    }

    /**
     * @param priceDataFactory
     *            the priceDataFactory to set
     */
    @Required
    public void setPriceDataFactory(final PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }

}
