/**
 * 
 */
package au.com.target.tgtfacades.process.email.data;

import java.io.Serializable;



/**
 * @author knemalik
 * 
 */
public class ClickAndCollectEmailData implements Serializable {

    private String orderNumber;
    private String letterType;
    private String storeNumber;



    /**
     * @return the storeNumber
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * @param orderNumber
     *            the orderNumber to set
     */
    public void setOrderNumber(final String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * @return the letterType
     */
    public String getLetterType() {
        return letterType;
    }

    /**
     * @param letterType
     *            the letterType to set
     */
    public void setLetterType(final String letterType) {
        this.letterType = letterType;
    }


}
