/**
 *
 */
package au.com.target.tgtfacades.util;

import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.commercefacades.product.data.PriceData;

import org.apache.commons.lang.StringUtils;


/**
 * @author rmcalave
 *
 */
public class PriceFormatUtils {

    private static final String PRICE_SUFFIX_ZERO_CENTS = ".00";

    private PriceFormatUtils() {

    }

    /**
     * Removes cent value from the provided price if the cent value is zero.
     * 
     * @param price
     *            The price to format
     * @return the formatted price
     */
    public static final String formatPrice(final String price) {
        if (StringUtils.endsWith(price, PRICE_SUFFIX_ZERO_CENTS)) {
            return StringUtils.substringBefore(price, PRICE_SUFFIX_ZERO_CENTS);
        }

        return price;
    }


    /**
     * Remove the cents value from the provided price object and returns string representation of it eg. $10 or $39.99
     * 
     * @param price
     * @return String
     */
    public static final String formatPrice(final PriceData price) {
        if (price != null) {
            final String priceAsString = price.getFormattedValue();
            if (StringUtils.endsWith(priceAsString, PRICE_SUFFIX_ZERO_CENTS)) {
                return StringUtils.substringBefore(priceAsString, PRICE_SUFFIX_ZERO_CENTS);
            }

            return priceAsString;
        }
        return null;
    }

    /**
     * takes a price range and returns a string representation of it $10 - $39.99
     * 
     * @param price
     * @return String
     */
    public static final String formatPrice(final PriceRangeData price) {
        if (price != null && price.getMinPrice() != null && price.getMaxPrice() != null) {
            return formatPrice(price.getMinPrice()) + " - "
                    + formatPrice(price.getMaxPrice());
        }
        return null;
    }
}
