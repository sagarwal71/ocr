/**
 * 
 */
package au.com.target.tgtfacades.util;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.price.data.PriceRangeInformation;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;


/**
 * @author pthoma20
 *
 */
public class TargetPriceHelper {

    private CommonI18NService commonI18NService;

    private PriceDataFactory priceDataFactory;


    /**
     * Populate prices from range information.
     *
     * the product data
     * 
     * @param priceRange
     *            the price range
     * 
     * @return targetPriceInformation
     */
    public TargetPriceInformation populatePricesFromRangeInformation(final PriceRangeInformation priceRange) {
        validateParameterNotNull(priceRange, "PriceRange model cannot be null");

        final TargetPriceInformation targetPriceInformation = new TargetPriceInformation();

        if (priceRange.sellPriceIsRange()) {
            final PriceRangeData priceRangeData = createPriceRangeData(priceRange.getFromPrice(),
                    priceRange.getToPrice());
            targetPriceInformation.setPriceRangeData(priceRangeData);
        }
        else if (priceRange.getFromPrice() != null) {
            targetPriceInformation.setPriceData(createPriceData(priceRange.getFromPrice()));
        }
        else {
            targetPriceInformation.setMissingPrice(true);
        }

        // show 'was price' only when it exists and is higher then current
        if (priceRange.getFromWasPrice() != null
                && priceRange.getFromPrice() != null
                && priceRange.getFromWasPrice().doubleValue() > priceRange.getFromPrice().doubleValue()) {
            targetPriceInformation.setShowWasPrice(true);
        }

        if (targetPriceInformation.isShowWasPrice()) {
            if (priceRange.wasPriceIsRange()) {
                final PriceRangeData wasPriceRangeData = createPriceRangeData(priceRange.getFromWasPrice(),
                        priceRange.getToWasPrice());
                targetPriceInformation.setWasPriceRangeData(wasPriceRangeData);
            }
            else if (priceRange.getFromWasPrice() != null) {
                targetPriceInformation.setWasPriceData(createPriceData(priceRange.getFromWasPrice()));
            }
        }
        return targetPriceInformation;
    }

    /**
     * This method will return the a formatter price range string for the data when a price range exists.
     * 
     * @param priceRangeData
     * @return String which contains the formatted price range information(example $10 - $20)
     */
    public String createFormattedPriceRange(final PriceRangeData priceRangeData) {
        final PriceData maxPriceData = priceRangeData.getMaxPrice();
        final PriceData minPriceData = priceRangeData.getMinPrice();
        if (maxPriceData == null || minPriceData == null
                || maxPriceData.getFormattedValue() == null || minPriceData.getFormattedValue() == null) {
            return null;
        }
        return PriceFormatUtils.formatPrice(minPriceData.getFormattedValue()) + " - "
                + PriceFormatUtils.formatPrice(maxPriceData.getFormattedValue());
    }

    /**
     * Gets the price range data.
     *
     * @param fromPrice
     *            the from price
     * @param toPrice
     *            the to price
     * @return the price range data
     */
    private PriceRangeData createPriceRangeData(final Double fromPrice, final Double toPrice) {
        final PriceRangeData priceRangeData = new PriceRangeData();
        priceRangeData.setMinPrice(createPriceData(fromPrice));
        priceRangeData.setMaxPrice(createPriceData(toPrice));
        return priceRangeData;
    }

    /**
     * Creates the price data.
     *
     * @param price
     *            the price
     * @return a price data
     */
    public PriceData createPriceData(final BigDecimal price) {
        validateParameterNotNull(price, "Price model cannot be null");
        return priceDataFactory.create(PriceDataType.BUY, price, commonI18NService.getCurrentCurrency().getIsocode());
    }

    /**
     * Creates the price data.
     *
     * @param price
     *            the price
     * @return a price data
     */
    public PriceData createPriceData(final Double price) {
        validateParameterNotNull(price, "Price model cannot be null");
        return priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(price.doubleValue()),
                commonI18NService.getCurrentCurrency().getIsocode());
    }

    public PriceData calculateAfterpayInstallmentPrice(final PriceData price) {
        validateParameterNotNull(price, "Price model cannot be null");
        return createPriceData(
                price.getValue().divide(TgtFacadesConstants.AFTER_PAY_INSTALLMENTS, 2, RoundingMode.HALF_UP));
    }

    /**
     * @param commonI18NService
     *            the commonI18NService to set
     */
    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    /**
     * @param priceDataFactory
     *            the priceDataFactory to set
     */
    @Required
    public void setPriceDataFactory(final PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }

}
