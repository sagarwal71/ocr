/**
 * 
 */
package au.com.target.tgtfacades.util;

import de.hybris.platform.acceleratorservices.config.HostConfigService;
import de.hybris.platform.commerceservices.enums.SalesApplication;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.constants.ThirdPartyConstants;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;


/**
 * Helper to get properties for current sales Application.
 * 
 * @author jjayawa1
 *
 */
public class SalesApplicationPropertyResolver {

    private SalesApplicationFacade salesApplicationFacade;
    private HostConfigService hostConfigService;
    private String hostSuffix;

    public String getGAValueForSalesApplication() {
        final String serverName = hostSuffix != null ? hostSuffix : StringUtils.EMPTY;
        final SalesApplication currentSalesApplication = salesApplicationFacade.getCurrentSalesApplication();

        if (SalesApplication.KIOSK.equals(currentSalesApplication)) {
            return hostConfigService.getProperty(ThirdPartyConstants.Analytics.UNIVERSAL_KIOSK_ACCOUNT_ID,
                    serverName);
        }
        else if (SalesApplication.MOBILEAPP.equals(currentSalesApplication)) {
            return hostConfigService.getProperty(ThirdPartyConstants.Analytics.UNIVERSAL_MOBILEAPP_ACCOUNT_ID,
                    serverName);
        }
        else {
            return hostConfigService.getProperty(ThirdPartyConstants.Analytics.UNIVERSAL_ACCOUNT_ID,
                    serverName);
        }
    }

    /**
     * @param salesApplicationFacade
     *            the salesApplicationFacade to set
     */
    @Required
    public void setSalesApplicationFacade(final SalesApplicationFacade salesApplicationFacade) {
        this.salesApplicationFacade = salesApplicationFacade;
    }

    /**
     * @param hostConfigService
     *            the hostConfigService to set
     */
    @Required
    public void setHostConfigService(final HostConfigService hostConfigService) {
        this.hostConfigService = hostConfigService;
    }

    /**
     * @param hostSuffix
     *            the hostSuffix to set
     */
    @Required
    public void setHostSuffix(final String hostSuffix) {
        this.hostSuffix = hostSuffix;
    }

}
