/**
 * 
 */
package au.com.target.tgtfacades.kiosk.product.data;

/**
 * @author gbaker2
 * 
 */
public class BulkyBoardProductData {

    private boolean availableForInStorePurchase;
    private boolean inStockForInStorePurchase;
    private String posName;
    private String posNumber;

    /**
     * @return the availableForInStorePurchase
     */
    public boolean isAvailableForInStorePurchase() {
        return availableForInStorePurchase;
    }

    /**
     * @param availableForInStorePurchase
     *            the availableForInStorePurchase to set
     */
    public void setAvailableForInStorePurchase(final boolean availableForInStorePurchase) {
        this.availableForInStorePurchase = availableForInStorePurchase;
    }

    /**
     * @return the inStockForInStorePurchase
     */
    public boolean isInStockForInStorePurchase() {
        return inStockForInStorePurchase;
    }

    /**
     * @param inStockForInStorePurchase
     *            the inStockForInStorePurchase to set
     */
    public void setInStockForInStorePurchase(final boolean inStockForInStorePurchase) {
        this.inStockForInStorePurchase = inStockForInStorePurchase;
    }

    /**
     * @return the posName
     */
    public String getPosName() {
        return posName;
    }

    /**
     * @param posName
     *            the posName to set
     */
    public void setPosName(final String posName) {
        this.posName = posName;
    }

    /**
     * @return the posNumber
     */
    public String getPosNumber() {
        return posNumber;
    }

    /**
     * @param posNumber
     *            the posNumber to set
     */
    public void setPosNumber(final String posNumber) {
        this.posNumber = posNumber;
    }

}
