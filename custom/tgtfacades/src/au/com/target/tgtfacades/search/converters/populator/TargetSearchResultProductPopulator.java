/**
 *
 */
package au.com.target.tgtfacades.search.converters.populator;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commercefacades.search.converters.populator.SearchResultProductPopulator;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import au.com.target.tgtcore.price.data.PriceRangeInformation;
import au.com.target.tgtfacades.converters.populator.TargetProductPricePopulator;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.data.enums.TargetPromotionStatusEnum;


public class TargetSearchResultProductPopulator extends SearchResultProductPopulator {

    private TargetProductPricePopulator targetProductPricePopulator;

    private List<String> searchResultImageFormat;

    @Override
    public void populate(final SearchResultValueData source, final ProductData target)
    {
        target.setNumberOfReviews(this.<Integer> getValue(source, "numberOfReviews"));

        if (!(target instanceof TargetProductData)) {
            return;
        }

        final TargetProductData targetProductData = (TargetProductData)target;
        targetProductData.setDealDescription(this.<String> getValue(source, "dealDescription"));

        final List<TargetPromotionStatusEnum> promotionStatuses = targetProductData.getPromotionStatuses();
        if (promotionStatuses != null && promotionStatuses.contains(TargetPromotionStatusEnum.ONLINE_EXCLUSIVE)) {
            targetProductData.setOnlineExclusive(true);
        }
    }


    /* (non-Javadoc)
     * @see de.hybris.platform.commercefacades.search.converters.SearchResultProductConverter#populateStock(de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData, de.hybris.platform.commercefacades.product.data.ProductData)
     */
    @Override
    public void populateStock(final SearchResultValueData source, final ProductData target) {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        final TargetProductData targetProductData = (TargetProductData)target;

        if (targetProductData.getPrice() == null && targetProductData.getPriceRange() == null) {
            final StockData stockData = new StockData();
            stockData.setStockLevelStatus(StockLevelStatus.OUTOFSTOCK);
            targetProductData.setStock(stockData);
        }
        else {
            final String stockLevelStatus = this.<String> getValue(source, "stockLevelStatus");
            final StockLevelStatus stockLevelStatusEnum = StockLevelStatus.valueOf(stockLevelStatus);
            targetProductData.getStock().setStockLevelStatus(stockLevelStatusEnum);
        }

        targetProductData.setShowWhenOutOfStock(BooleanUtils.isTrue(this.<Boolean> getValue(source,
                "showWhenOutOfStock")));
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.commercefacades.search.converters.SearchResultProductConverter#populatePromotions(de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData, de.hybris.platform.commercefacades.product.data.ProductData)
     */
    @Override
    protected void populatePromotions(final SearchResultValueData source, final ProductData target) {
        if (target instanceof TargetProductData) {
            final TargetProductData targetData = (TargetProductData)target;

            targetData.setPromotionStatuses(getPromotionStatuses(source));
        }

        super.populatePromotions(source, target);
    }

    private List<TargetPromotionStatusEnum> getPromotionStatuses(final SearchResultValueData source) {
        final List<TargetPromotionStatusEnum> promotionStatuses = new ArrayList<>();

        if (isPromotionStatusApplied(source, "onlineExclusive")) {
            promotionStatuses.add(TargetPromotionStatusEnum.ONLINE_EXCLUSIVE);
        }

        if (isPromotionStatusApplied(source, "targetExclusive")) {
            promotionStatuses.add(TargetPromotionStatusEnum.TARGET_EXCLUSIVE);
        }

        if (isPromotionStatusApplied(source, "clearance")) {
            promotionStatuses.add(TargetPromotionStatusEnum.CLEARANCE);
        }

        if (isPromotionStatusApplied(source, "hotProduct")) {
            promotionStatuses.add(TargetPromotionStatusEnum.HOT_PRODUCT);
        }

        if (isPromotionStatusApplied(source, "essential")) {
            promotionStatuses.add(TargetPromotionStatusEnum.ESSENTIALS);
        }

        return promotionStatuses;
    }

    private boolean isPromotionStatusApplied(final SearchResultValueData source, final String propertyName) {
        final List<Boolean> promoStatusValues = this.<List<Boolean>> getValue(source, propertyName);

        // If the list of values is empty or doesn't contain a True then the promotion flag is not applied
        if (CollectionUtils.isEmpty(promoStatusValues)
                || !CollectionUtils.contains(promoStatusValues.iterator(), Boolean.TRUE)) {
            return false;
        }

        // Check for any values in the list that are not True. If there are any then the promotion status should not apply.
        for (final Boolean promoStatusFlag : promoStatusValues) {
            if (BooleanUtils.isNotTrue(promoStatusFlag)) {
                return false;
            }
        }

        // If we reach here then the promotion status has been applied.
        return true;
    }

    @Override
    protected void populatePrices(final SearchResultValueData source, final ProductData target)
    {
        if (target instanceof TargetProductData)
        {
            final PriceRangeInformation priceRangeInformation = this.<PriceRangeInformation> getValue(source,
                    "priceRangeInfo");
            if (priceRangeInformation != null) {
                getTargetProductPricePopulator().populatePricesFromRangeInformation((TargetProductData)target,
                        priceRangeInformation);
                return;
            }
        }
        super.populatePrices(source, target);
    }

    /**
     * @return the targetProductPricePopulator
     */
    public TargetProductPricePopulator getTargetProductPricePopulator() {
        return targetProductPricePopulator;
    }

    /**
     * @param targetProductPricePopulator
     *            the targetProductPricePopulator to set
     */
    @Required
    public void setTargetProductPricePopulator(
            final TargetProductPricePopulator targetProductPricePopulator) {
        this.targetProductPricePopulator = targetProductPricePopulator;
    }

    @Override
    protected List<ImageData> createImageData(final SearchResultValueData source) {
        final List<ImageData> result = new ArrayList<>();
        for (final String imageFormat : searchResultImageFormat) {
            addImageData(source, imageFormat, result);
        }
        return result;
    }

    /**
     * @param searchResultImageFormat
     *            the searchResultImageFormat to set
     */
    public void setSearchResultImageFormat(final List<String> searchResultImageFormat) {
        this.searchResultImageFormat = searchResultImageFormat;
    }
}
