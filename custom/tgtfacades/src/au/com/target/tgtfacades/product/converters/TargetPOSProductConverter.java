/**
 * 
 */
package au.com.target.tgtfacades.product.converters;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtfacades.product.data.TargetPOSProductData;
import au.com.target.tgtfacades.product.data.TargetPOSProductData.ErrorType;
import au.com.target.tgtsale.product.data.POSProduct;


/**
 * Converter for POS product data
 * 
 */
public class TargetPOSProductConverter extends AbstractPopulatingConverter<POSProduct, TargetPOSProductData> {

    private PriceDataFactory priceDataFactory;
    private CommonI18NService commonI18NService;


    @Override
    public void populate(final POSProduct source, final TargetPOSProductData target)
    {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        target.setDescription(source.getDescription());
        target.setItemCode(source.getItemCode());

        if (source.getPriceInCents() != null) {
            target.setPrice(createPriceData(priceInDollars(source.getPriceInCents().intValue())));
        }
        if (source.getWasPriceInCents() != null) {
            target.setWasPrice(createPriceData(priceInDollars(source.getWasPriceInCents().intValue())));
        }

        target.setFailureReason(source.getError());

        if (source.getSystemError()) {
            target.setErrorType(ErrorType.SYSTEM_ERROR);
        }
        else if (StringUtils.isNotEmpty(source.getError())) {
            target.setErrorType(ErrorType.POS);
        }
        else {
            target.setErrorType(ErrorType.NONE);
        }

        super.populate(source, target);
    }


    private Double priceInDollars(final int priceInCents) {

        return Double.valueOf(BigDecimal.valueOf(priceInCents).divide(BigDecimal.valueOf(100)).setScale(2)
                .doubleValue());
    }

    private PriceData createPriceData(final Double price) {

        return getPriceDataFactory().create(PriceDataType.BUY,
                BigDecimal.valueOf(price.doubleValue()),
                getCommonI18NService().getCurrentCurrency().getIsocode());
    }


    /**
     * @return the priceDataFactory
     */
    public PriceDataFactory getPriceDataFactory() {
        return priceDataFactory;
    }

    /**
     * @param priceDataFactory
     *            the priceDataFactory to set
     */
    @Required
    public void setPriceDataFactory(final PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }

    /**
     * @return the commonI18NService
     */
    public CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    /**
     * @param commonI18NService
     *            the commonI18NService to set
     */
    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

}
