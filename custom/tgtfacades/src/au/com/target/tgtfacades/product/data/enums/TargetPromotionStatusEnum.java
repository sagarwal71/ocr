/**
 * 
 */
package au.com.target.tgtfacades.product.data.enums;

/**
 * @author rmcalave
 * 
 */
public enum TargetPromotionStatusEnum {
    ESSENTIALS("essentials"),
    TARGET_EXCLUSIVE("only-at"),
    HOT_PRODUCT("hot-product"),
    CLEARANCE("clearance"),
    ONLINE_EXCLUSIVE("online-only"),
    NEW_LOWER_PRICE("new-lower-price");

    private String promotionClass;

    private TargetPromotionStatusEnum(final String promotionClass) {
        this.promotionClass = promotionClass;
    }

    /**
     * @return the promotionClass
     */
    public String getPromotionClass() {
        return promotionClass;
    }
}
