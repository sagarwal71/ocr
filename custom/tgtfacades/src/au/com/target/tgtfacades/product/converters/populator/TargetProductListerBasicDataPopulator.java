/**
 * 
 */
package au.com.target.tgtfacades.product.converters.populator;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.variants.model.VariantTypeModel;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.converters.populator.AbstractTargetProductPopulator;
import au.com.target.tgtfacades.converters.util.TargetCategoryUtil;
import au.com.target.tgtfacades.product.data.AbstractTargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.url.impl.TargetProductDataUrlResolver;


/**
 * @author rmcalave
 *
 */
public class TargetProductListerBasicDataPopulator<SOURCE extends TargetProductModel, TARGET extends TargetProductListerData>
        extends AbstractTargetProductPopulator<TargetProductModel, TargetProductListerData> {

    protected static final Logger LOG = Logger.getLogger(TargetProductListerBasicDataPopulator.class);

    private TargetProductDataUrlResolver targetProductDataUrlResolver;

    private Converter<CategoryModel, CategoryData> categoryConverter;


    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final ProductModel source, final AbstractTargetProductData target)
            throws ConversionException {

        if (!(source instanceof TargetProductModel) || !(target instanceof TargetProductListerData)) {
            return;
        }

        final TargetProductModel productModel = (TargetProductModel)source;
        final TargetProductListerData productData = (TargetProductListerData)target;
        productData.setItemType(source.getItemtype());
        populateTargetProductListerData(productModel, productData);
        populateProductTypeInformation(productModel, productData);
        populateSize(productModel, productData);
        populateIsGiftCard(productModel, productData);

        final VariantTypeModel variantTypeModel = source.getVariantType();
        if (variantTypeModel != null) {
            target.setVariantType(variantTypeModel.getCode());
        }
    }

    private void populateTargetProductListerData(final TargetProductModel source,
            final TargetProductListerData target) {
        target.setName(source.getName());
        target.setCode(source.getCode());

        if (source.getBrand() != null) {
            target.setBrand(source.getBrand().getCode());
        }


        if (StringUtils.isNotBlank(source.getName()) && StringUtils.isNotBlank(source.getCode())) {
            target.setUrl(targetProductDataUrlResolver.resolve(source.getName(), source.getCode()));
        }
        target.setDescription(source.getDescription());

        if (source.getNumberOfReviews() != null) {
            target.setNumberOfReviews(source.getNumberOfReviews());
        }
        if (source.getAverageRating() != null) {
            target.setAverageRating(source.getAverageRating());
        }

        if (null != source.getOriginalCategory()) {
            target.setOriginalCategoryCode(source.getOriginalCategory().getCode());
            target.setOriginalCategoryName(source.getOriginalCategory().getName());
        }

        if (null != source.getMerchDepartment()) {
            target.setMerchDepartmentCode(source.getMerchDepartment().getCode());
            target.setMerchDepartmentName(source.getMerchDepartment().getName());
        }

        populateVideoInformation(source, target);
        target.setCareInstructions(getProductFeatureList(source.getCareInstructions()));
        target.setMaterials(getProductFeatureList(source.getMaterials()));
        target.setProductFeatures(getProductFeatureList(source.getProductFeatures()));
        target.setExtraInfo(source.getExtraInfo());
        target.setPreview(BooleanUtils.isTrue(source.getPreview()));
        target.setBaseProductCode(source.getCode());
        target.setBulkyBoardProduct(BooleanUtils.isTrue(source.getBulkyBoardProduct()));
        target.setTopLevelCategory(getTopLevelCategory(source).getName());
        target.setCategories(Converters.convertAll(source.getSupercategories(), getCategoryConverter()));
    }



    /**
     * @param targetProductDataUrlResolver
     *            the targetProductDataUrlResolver to set
     */
    @Required
    public void setTargetProductDataUrlResolver(final TargetProductDataUrlResolver targetProductDataUrlResolver) {
        this.targetProductDataUrlResolver = targetProductDataUrlResolver;
    }

    protected TargetProductCategoryModel getTopLevelCategory(final TargetProductModel product) {
        return TargetCategoryUtil.getTopLevelCategory(product);
    }

    /**
     * @return the categoryConverter
     */
    public Converter<CategoryModel, CategoryData> getCategoryConverter() {
        return categoryConverter;
    }

    /**
     * @param categoryConverter
     *            the categoryConverter to set
     */
    @Required
    public void setCategoryConverter(final Converter<CategoryModel, CategoryData> categoryConverter) {
        this.categoryConverter = categoryConverter;
    }



}
