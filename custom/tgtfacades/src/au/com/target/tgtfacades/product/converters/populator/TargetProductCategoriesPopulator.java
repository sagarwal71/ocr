package au.com.target.tgtfacades.product.converters.populator;

import de.hybris.platform.commercefacades.product.converters.populator.ProductCategoriesPopulator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantProductModel;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtfacades.product.data.TargetProductData;


/**
 * Collect the super-categories from base product if not available at the variant level
 */
public class TargetProductCategoriesPopulator extends ProductCategoriesPopulator<ProductModel, TargetProductData> {

    @Override
    public void populate(final ProductModel productModel, final TargetProductData productData)
            throws ConversionException {

        super.populate(productModel, productData);

        if (CollectionUtils.isEmpty(productData.getCategories()) && productModel instanceof VariantProductModel) {
            populate(((VariantProductModel)productModel).getBaseProduct(), productData);
        }
    }
}
