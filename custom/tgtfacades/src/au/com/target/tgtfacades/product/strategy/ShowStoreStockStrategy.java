/**
 * 
 */
package au.com.target.tgtfacades.product.strategy;

import au.com.target.tgtcore.model.TargetProductModel;


/**
 * @author rmcalave
 *
 */
public interface ShowStoreStockStrategy {

    /**
     * Determine whether store stock data should be displayed for the provided product.
     * 
     * @param product
     *            The product
     * @return true if stock data should be displayed, false otherwise
     */
    boolean showStoreStockForProduct(TargetProductModel product);

}
