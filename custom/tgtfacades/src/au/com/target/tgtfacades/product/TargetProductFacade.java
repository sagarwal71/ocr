package au.com.target.tgtfacades.product;

import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Collection;
import java.util.List;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.category.exceptions.NotAPrimarySuperCategoryException;
import au.com.target.tgtfacades.product.data.TargetPOSProductData;
import au.com.target.tgtfacades.product.data.TargetProductCountDepartmentData;
import au.com.target.tgtfacades.product.data.TargetProductData;


/**
 * Target extension to default product facade.
 * 
 * @see ProductFacade
 */
public interface TargetProductFacade extends ProductFacade {

    /**
     * Returns product data that have {@code categoryCode} as their primarysupercategory.
     * 
     * @param categoryCode
     *            the category code to search products for
     * @return the list of products
     * @throws NotAPrimarySuperCategoryException
     *             if requested {@code categoryCode} does not appear to represent a primarysupercategory
     */
    List<TargetProductData> getProductsForPrimarySuperCategory(String categoryCode)
            throws NotAPrimarySuperCategoryException;

    /**
     * Gets the product data, and populates bulky board specific data (like store data and stock levels).
     * 
     * @param productModel
     *            the productModel
     * @param options
     *            the options
     * @param storeNumber
     *            the store number (for stock levels)
     * @return the {@link ProductData}
     * @see ProductFacade#getProductForOptions(ProductModel, Collection)
     */
    ProductData getBulkyBoardProductForOptions(final ProductModel productModel,
            final Collection<? extends ProductOption> options, String storeNumber);

    /**
     * Retrieve POS product data
     * 
     * @param storeNumber
     * @param barcode
     * @return pos product data
     */
    TargetPOSProductData getPOSProductDetails(String storeNumber, String barcode);

    /**
     * Gets the summary of deal description
     * 
     * @param productModel
     * @return dealDescription
     */
    String getSummaryDealDescription(ProductModel productModel);

    /**
     * Populate the TargetProductData with the deal Message,if it has size variant
     * 
     * @param productCode
     * @param options
     * @return TargetProductData
     */
    TargetProductData getProductForCodeAndOptionsWithDealMessage(String productCode,
            Collection<? extends ProductOption> options);

    /**
     * Returns the list of products for the specific merchDeptcode
     * 
     * @param merchCode
     * @param start
     * @param count
     * @return List
     */
    List<ProductData> getProductsForMerchantDept(final int merchCode, final int start, final int count);

    /**
     * Get the product page count for each mechDepartment from page size.
     * 
     * @param pageSize
     * @return List<TargetProductCountDepartmentData>
     */
    List<TargetProductCountDepartmentData> getProductPageCountForDepartment(int pageSize);

    /**
     * Get base target product from targetVarianProduct
     * 
     * @param productModel
     * @return TargetProductModel
     */
    TargetProductModel getBaseTargetProduct(final AbstractTargetVariantProductModel productModel);


    /**
     * @param productModel
     * @return product to Redirect
     */
    ProductModel getProductToRedirect(ProductModel productModel);

    /**
     * Return all sellable variants with options for the base product of given product code
     * 
     * @param productCode
     * @return all variants with options
     */
    List<ProductData> getAllSellableVariantsWithOptions(String productCode, List<ProductOption> options);

    /**
     * Return all sellable variants with options for the base product of given product code and store stock
     * 
     * @param productCode
     * @return all variants with options
     */
    List<ProductData> getAllSellableVariantsWithStoreStockAndOptions(String productCode, Integer storeNumber,
            List<ProductOption> options);

    /**
     * Return product model based on product code.
     * 
     * @param productCode
     * @return productModel
     */
    ProductModel getProductForCode(String productCode);

    /**
     * TODO remove
     * 
     * @param productModel
     * @param storeNumber
     * @return list of {@link ProductData}
     */
    List<ProductData> getFluentStock(ProductModel productModel, String storeNumber);

    /**
     * TODO remove
     * 
     * @param productCode
     * @param storeNumber
     * @return list of {@link ProductData}
     */
    List<ProductData> getFluentStock(String productCode, String storeNumber);
}
