package au.com.target.tgtfacades.product.impl;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commercefacades.product.impl.DefaultProductFacade;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.stock.strategy.StockLevelStatusStrategy;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.deals.strategy.TargetDealDescriptionStrategy;
import au.com.target.tgtcore.jalo.TargetSizeVariantProduct;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.TargetProductDepartmentService;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtcore.product.data.TargetProductCountMerchDepRowData;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfacades.category.exceptions.NotAPrimarySuperCategoryException;
import au.com.target.tgtfacades.kiosk.product.data.BulkyBoardProductData;
import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.product.converters.TargetPOSProductConverter;
import au.com.target.tgtfacades.product.data.TargetPOSProductData;
import au.com.target.tgtfacades.product.data.TargetPOSProductData.ErrorType;
import au.com.target.tgtfacades.product.data.TargetProductCountDepartmentData;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.util.TargetProductDataHelper;
import au.com.target.tgtfluent.data.FluentStock;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.service.FluentStockLookupService;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtsale.product.TargetPOSProductService;
import au.com.target.tgtsale.product.data.POSProduct;
import au.com.target.tgtsale.stock.service.TargetStoreStockService;
import au.com.target.tgtutility.util.BarcodeTools;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


/**
 * Default implementation for {@link TargetProductFacade}.
 * 
 * @see DefaultProductFacade
 */
public class TargetProductFacadeImpl extends DefaultProductFacade implements TargetProductFacade {

    protected static final Logger LOG = Logger.getLogger(TargetProductFacadeImpl.class);

    private CategoryService categoryService;

    private TargetPointOfServiceService targetPointOfServiceService;

    private TargetWarehouseService targetWarehouseService;

    private TargetStockService targetStockService;

    private StockLevelStatusStrategy stockLevelStatusStrategy;

    private TargetPOSProductService targetPOSProductService;

    private TargetPOSProductConverter targetPOSProductConverter;

    private TargetDealDescriptionStrategy targetDealDescriptionStrategy;

    private TargetProductService targetProductService;

    private TargetProductDepartmentService targetProductDepartmentService;

    private Converter<ProductModel, ProductData> productUrlConverter;

    private CatalogVersionService catalogVersionService;

    private TargetStoreStockService targetStoreStockService;

    private FluentStockLookupService fluentStockLookupService;

    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;

    @Override
    public List<TargetProductData> getProductsForPrimarySuperCategory(final String categoryCode)
            throws NotAPrimarySuperCategoryException {
        final CategoryModel category = getCategoryService().getCategoryForCode(categoryCode);
        if (category.getProducts().isEmpty()) {
            throw new NotAPrimarySuperCategoryException();
        }
        final List<ProductModel> products = ((TargetProductService)getProductService())
                .getProductsForPrimarySuperCategory(category);
        final Predicate<Object> typePredicate = Predicates.instanceOf(TargetProductModel.class);
        final Iterable<ProductModel> targetProducts = Iterables.filter(products, typePredicate);

        final List<TargetProductData> productData = new ArrayList<>();
        for (final ProductModel product : targetProducts) {
            for (final VariantProductModel variantProduct : product.getVariants()) {
                if (!(variantProduct instanceof TargetColourVariantProductModel)) {
                    continue;
                }

                productData.add((TargetProductData)getProductConverter().convert(variantProduct));
            }
        }

        return productData;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.product.TargetProductFacade#getBulkyBoardProductForOptions(de.hybris.platform.core.model.product.ProductModel, java.util.Collection, java.lang.String)
     */
    @Override
    public ProductData getBulkyBoardProductForOptions(final ProductModel productModel,
            final Collection<? extends ProductOption> options, final String bulkyBoardStoreNumber) {
        final TargetProductData productData = (TargetProductData)getProductForCodeAndOptions(productModel.getCode(),
                options);

        if (StringUtils.isBlank(bulkyBoardStoreNumber)) {
            return productData;
        }

        if (productModel instanceof TargetProductModel) {
            if (!Boolean.TRUE.equals(((TargetProductModel)productModel).getBulkyBoardProduct())) {
                return productData;
            }
        }
        else if (productModel instanceof AbstractTargetVariantProductModel) {
            if (!Boolean.TRUE.equals(((AbstractTargetVariantProductModel)productModel).getBulkyBoardProduct())) {
                return productData;
            }
        }
        else {
            return productData;
        }

        TargetPointOfServiceModel pointOfService = null;
        try {
            pointOfService = targetPointOfServiceService
                    .getPOSByStoreNumber(Integer.valueOf(bulkyBoardStoreNumber));
        }
        catch (final Exception ex) {
            // No store for number or number is invalid, return product data as is
            return productData;
        }


        // Store doesn't have it's own warehouse, is not a Bulky Board store
        final WarehouseModel storeWarehouse = targetWarehouseService.getWarehouseForPointOfService(pointOfService);
        if (storeWarehouse == null) {
            return productData;
        }


        // No stock level configured at the store, treat it as a non-bulkyboard product
        final StockLevelModel storeStockLevel = targetStockService.getStockLevel(productModel, storeWarehouse);
        if (storeStockLevel == null) {
            return productData;
        }


        // A stock level exists on this product for this store, so it's available for in store purchase
        // in this store.
        final BulkyBoardProductData bulkyBoardProductData = new BulkyBoardProductData();
        bulkyBoardProductData.setPosNumber(pointOfService.getStoreNumber().toString());
        bulkyBoardProductData.setPosName(pointOfService.getName());
        bulkyBoardProductData.setAvailableForInStorePurchase(true);

        final StockLevelStatus stockLevelStatus = stockLevelStatusStrategy.checkStatus(storeStockLevel);
        bulkyBoardProductData.setInStockForInStorePurchase(!StockLevelStatus.OUTOFSTOCK
                .equals(stockLevelStatus));

        productData.setBulkyBoard(bulkyBoardProductData);

        return productData;
    }


    @Override
    public TargetPOSProductData getPOSProductDetails(final String storeNumber, final String ean) {

        // Validation on input parameters
        final Integer iStoreNumber = validateStoreNumber(storeNumber);
        if (iStoreNumber == null) {
            LOG.info("Invalid store number: " + storeNumber);
            return createPosInvalidData("Invalid store number");
        }

        if (!BarcodeTools.testEANCheckDigit(ean)) {
            LOG.info("Invalid ean: " + ean);
            return createPosInvalidData("Invalid ean");
        }

        final POSProduct posProduct = targetPOSProductService.getPOSProductDetails(iStoreNumber, ean);

        if (StringUtils.isEmpty(posProduct.getError()) && !validatePosData(posProduct, ean)) {
            return createPosInvalidData("Invalid price data returned from POS");
        }

        final TargetPOSProductData data = targetPOSProductConverter.convert(posProduct);
        return data;
    }


    @Override
    public List<ProductData> getProductsForMerchantDept(final int merchCode, final int start, final int count) {

        final TargetMerchDepartmentModel targetMerchDeptModel = targetProductDepartmentService
                .getDepartmentCategoryModel(catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                        TgtCoreConstants.Catalog.ONLINE_VERSION), Integer.valueOf(merchCode));

        if (null == targetMerchDeptModel) {
            LOG.error("Not a Valid MerchDepartment " + merchCode);
            return null;
        }
        else {
            final List<TargetColourVariantProductModel> productModelList = targetProductService
                    .getProductsForMerchDeptCode(
                            targetMerchDeptModel,
                            start * count, count);
            final List<ProductData> productData = new ArrayList<>();
            for (final TargetColourVariantProductModel product : productModelList) {
                productData.add(productUrlConverter.convert(product));
            }
            return productData;
        }
    }

    /**
     * This method gets the dealMessage,if the product has sizevariants
     * 
     * @param productCode
     * @param options
     * @return productData
     */

    @Override
    public TargetProductData getProductForCodeAndOptionsWithDealMessage(final String productCode,
            final Collection<? extends ProductOption> options) {
        final ProductModel productModel = getProductService().getProductForCode(productCode);
        final TargetProductData productData = (TargetProductData)getProductForCodeAndOptions(productModel.getCode(),
                options);
        if ((productData.getVariantType() != null)
                && (productData.getVariantType().equalsIgnoreCase(TargetSizeVariantProduct.class.getSimpleName()))) {
            final String dealdescription = getSummaryDealDescription(productModel);
            productData.setDealDescription(dealdescription);
        }
        return productData;
    }

    @Override
    public List<TargetProductCountDepartmentData> getProductPageCountForDepartment(final int pageSize) {
        final List<TargetProductCountMerchDepRowData> rowDataList = targetProductDepartmentService
                .findProductCountForEachMerchDep();
        final List<TargetProductCountDepartmentData> resultDataList = new ArrayList<>();
        if (pageSize > 0 && CollectionUtils.isNotEmpty(rowDataList)) {
            for (final TargetProductCountMerchDepRowData rowData : rowDataList) {
                final TargetProductCountDepartmentData data = new TargetProductCountDepartmentData();
                data.setTargetMerchDepartmentCode(rowData.getTargetMerchDepartmentModel().getCode());
                data.setPageCount((rowData.getProductCount() + pageSize - 1) / pageSize);
                resultDataList.add(data);
            }
        }
        return resultDataList;
    }

    @Override
    public TargetProductModel getBaseTargetProduct(final AbstractTargetVariantProductModel productModel) {
        if (productModel == null) {
            return null;
        }
        AbstractTargetVariantProductModel baseProductModel = productModel;
        while (baseProductModel.getBaseProduct() != null) {
            if (baseProductModel.getBaseProduct() instanceof TargetProductModel) {
                return (TargetProductModel)baseProductModel.getBaseProduct();
            }
            else if (baseProductModel.getBaseProduct() instanceof AbstractTargetVariantProductModel) {
                baseProductModel = (AbstractTargetVariantProductModel)baseProductModel.getBaseProduct();
            }
            else {
                break;
            }
        }
        return null;
    }


    /**
     * To redirect to suitable Product Variant
     * 
     * @param productModel
     * @return product to Redirect
     */
    @Override
    public ProductModel getProductToRedirect(final ProductModel productModel) {
        ProductModel redirectProductModel = productModel;
        // Base product to color variant redirection
        if (TargetProductDataHelper.isBaseProduct(productModel) && productModel instanceof TargetProductModel) {
            redirectProductModel = getColourProductVariantToRedirect((TargetProductModel)productModel);
        }
        // Now try color variant to size variant redirection
        if (redirectProductModel != null && redirectProductModel instanceof TargetColourVariantProductModel) {
            return getProductByValidatingVariantStock((AbstractTargetVariantProductModel)redirectProductModel);
        }
        return productModel;
    }

    @Override
    public List<ProductData> getAllSellableVariantsWithOptions(final String productCode,
            final List<ProductOption> options) {
        ProductModel productModel = targetProductService.getProductForCode(productCode);
        if (productModel instanceof AbstractTargetVariantProductModel) {
            productModel = getBaseTargetProduct((AbstractTargetVariantProductModel)productModel);
        }
        return getAllSellableVariants(productModel, options);
    }

    @Override
    public List<ProductData> getAllSellableVariantsWithStoreStockAndOptions(final String productCode,
            final Integer storeNumber,
            final List<ProductOption> options) {
        final List<ProductData> variants = getAllSellableVariantsWithOptions(productCode, options);
        final Map<String, StockLevelStatus> productStockMap = targetStoreStockService
                .getStoreStockLevelStatusMapForProducts(storeNumber, getItemCodes(variants));
        if (productStockMap.isEmpty()) {
            return null;
        }
        for (final ProductData variant : variants) {
            if (productStockMap.containsKey(variant.getCode())) {
                final StockData stockData = new StockData();
                stockData.setStockLevelStatus(productStockMap.get(variant.getCode()));
                variant.setStock(stockData);
            }
        }
        return variants;
    }

    @Override
    public String getSummaryDealDescription(final ProductModel productModel) {

        String dealDescription = null;
        if (productModel instanceof TargetColourVariantProductModel) {
            dealDescription = targetDealDescriptionStrategy
                    .getSummaryDealDescription((TargetColourVariantProductModel)productModel);
        }

        return dealDescription;
    }

    @Override
    public ProductModel getProductForCode(final String productCode) {
        return targetProductService.getProductForCode(productCode);
    }

    @Override
    public List<ProductData> getFluentStock(final ProductModel productModel, final String storeNumber) {
        final String baseProductCode = getBaseProductCode(productModel);
        final List<ProductData> variants = getAllSellableVariantsWithOptions(baseProductCode, null);
        try {
            final Map<String, FluentStock> stockDataMap = fluentStockLookupService.lookupStock(baseProductCode,
                    storeNumber);
            variantStock(variants, stockDataMap, storeNumber);
            return variants;
        }
        catch (final FluentClientException e) {
            LOG.error("FLUENT: Failed looking up stock", e);
            return Collections.emptyList();
        }
    }

    @Override
    public List<ProductData> getFluentStock(final String productCode, final String storeNumber) {
        final ProductModel productModel = getProductForCode(productCode);
        return getFluentStock(productModel, storeNumber);
    }

    /**
     * @param variants
     * @param stockDataMap
     * @param storeNumber
     */
    protected void variantStock(final List<ProductData> variants,
            final Map<String, FluentStock> stockDataMap, final String storeNumber) {

        final PurchaseOptionModel purchaseOptionModel = targetPurchaseOptionHelper.getCurrentPurchaseOptionModel();
        final Map<String, Boolean> available = new HashMap<>();
        available.put(purchaseOptionModel.getCode(), Boolean.TRUE);

        final StockData emptyStockData = new StockData();
        emptyStockData.setStockLevel(Long.valueOf(0));
        emptyStockData.setStockLevelStatus(StockLevelStatus.OUTOFSTOCK);

        TargetProductData variant;

        for (final ProductData productData : variants) {
            variant = (TargetProductData)productData;
            if (variant.isDisplayOnly()) {
                continue;
            }
            final FluentStock fluentStock = stockDataMap.get(variant.getCode());
            if (fluentStock == null) {
                LOG.warn("FLUENT: no stock data for " + variant.getCode());
                variant.setStock(emptyStockData);
                variant.setConsolidatedStoreStock(emptyStockData);
                variant.setAvailable(available);
                continue;
            }
            final StockData stockData = storeNumber == null ? fluentStock.getAts() : fluentStock.getStore();
            variant.setStock(stockData);
            variant.setInStock(stockData.getStockLevelStatus() != StockLevelStatus.OUTOFSTOCK);
            variant.setConsolidatedStoreStock(fluentStock.getConsolidated());
            variant.setAvailable(available);
        }
    }

    /**
     * @param source
     */
    protected String getBaseProductCode(final ProductModel source) {
        if (source == null) {
            return null;
        }
        if (source instanceof TargetProductModel) {
            return source.getCode();
        }
        return getBaseProductCode(((AbstractTargetVariantProductModel)source).getBaseProduct());
    }

    /**
     * Validate that storenumber is a valid integer
     * 
     * @return Integer value or null if not valid
     */
    protected Integer validateStoreNumber(final String storeNumber) {

        try {
            return Integer.valueOf(storeNumber);
        }
        catch (final Exception e) {
            // Cannot parse the store number
            return null;
        }
    }

    /**
     * Validate pos prices returned
     * 
     * @param posProduct
     * @return true if all ok
     */
    protected boolean validatePosData(final POSProduct posProduct, final String ean) {

        if (posProduct.getPriceInCents() == null) {
            LOG.info("No product price for ean: " + ean);
            return false;
        }
        if (posProduct.getPriceInCents().intValue() <= 0) {
            LOG.info("Non-positive price from pos for ean: " + ean);
            return false;
        }
        if (posProduct.getWasPriceInCents() != null && posProduct.getWasPriceInCents().intValue() <= 0) {
            LOG.info("Non-positive was price from pos for ean: " + ean);
            return false;
        }

        return true;
    }

    private TargetPOSProductData createPosInvalidData(final String message) {

        // Generic invalid data message
        final TargetPOSProductData data = new TargetPOSProductData();
        data.setErrorType(ErrorType.INVALID_DATA);
        data.setFailureReason(message);
        return data;
    }

    private List<String> getItemCodes(final List<ProductData> variants) {
        return Lists.transform(variants, new Function<ProductData, String>() {
            @Override
            public String apply(final ProductData product) {
                return product.getCode();
            }
        });
    }

    private List<ProductData> getAllSellableVariants(final ProductModel productModel,
            final List<ProductOption> options) {
        final List<ProductData> productDataList = new ArrayList<>();
        final Collection<VariantProductModel> variants = productModel.getVariants();
        if (CollectionUtils.isEmpty(variants)) {
            final ProductData productData = getProductForCodeAndOptions(productModel.getCode(), options);
            productDataList.add(productData);
        }
        else {
            for (final VariantProductModel variant : variants) {
                productDataList.addAll(getAllSellableVariants(variant, options));
            }
        }
        return productDataList;
    }

    private AbstractTargetVariantProductModel getProductByValidatingVariantStock(
            final AbstractTargetVariantProductModel productModel) {
        if (CollectionUtils.isNotEmpty(productModel.getVariants())) {
            final List<AbstractTargetVariantProductModel> productsInStock = getProductsInStock(productModel
                    .getVariants());
            if (productsInStock.size() == 1) {
                return productsInStock.get(0);
            }
        }
        return productModel;
    }

    private List<AbstractTargetVariantProductModel> getProductsInStock(
            final Collection<VariantProductModel> productsList) {
        final List<AbstractTargetVariantProductModel> productsInStock = new ArrayList<>();
        for (final VariantProductModel eachProductModel : productsList) {
            if (isVariantInStock(eachProductModel)
                    && eachProductModel instanceof AbstractTargetVariantProductModel) {
                productsInStock.add((AbstractTargetVariantProductModel)eachProductModel);
            }
        }
        return productsInStock;
    }

    private boolean isVariantInStock(final VariantProductModel eachProductModel) {
        return StockLevelStatus.OUTOFSTOCK != targetStockService
                .getProductStatus(eachProductModel);
    }

    /**
     * return First color Variant Instock
     * 
     * @param baseProduct
     * @return ColourVariant Product
     */
    private TargetColourVariantProductModel getFirstColourVariantInStock(final ProductModel baseProduct) {

        VariantProductModel colourVariantInStock = null;
        for (final VariantProductModel variantProductModel : baseProduct.getVariants()) {

            // If CV has SVs see if any of them is in stock
            if (CollectionUtils.isNotEmpty(variantProductModel.getVariants())
                    && hasVariantInStock(variantProductModel.getVariants())) {
                colourVariantInStock = variantProductModel;
                break;
            }
            // If CV has no SVs then return it, if it is in stock
            if (CollectionUtils.isEmpty(variantProductModel.getVariants()) && isVariantInStock(variantProductModel)) {
                colourVariantInStock = variantProductModel;
                break;
            }
        }
        if (colourVariantInStock instanceof TargetColourVariantProductModel) {
            return (TargetColourVariantProductModel)colourVariantInStock;
        }
        return null;
    }


    private boolean hasVariantInStock(final Collection<VariantProductModel> variantProducts) {
        return CollectionUtils.isNotEmpty(getProductsInStock(variantProducts));
    }

    private TargetColourVariantProductModel getColourProductVariantToRedirect(final TargetProductModel productModel) {
        if (CollectionUtils.isNotEmpty(productModel.getVariants())) {
            final TargetColourVariantProductModel productsInStock = getFirstColourVariantInStock(productModel);
            if (productsInStock != null) {
                return productsInStock;
            }
            else {
                return (TargetColourVariantProductModel)productModel.getVariants().iterator().next();
            }
        }
        LOG.warn("ProductRedirect: Cannot redirect base to any colour variant, product=" + productModel.getCode());
        return null;
    }

    /**
     * Returns internally used category service.
     * 
     * @return the category service
     */
    public CategoryService getCategoryService() {
        return categoryService;
    }

    /**
     * Sets the category service.
     * 
     * @param categoryService
     *            the category service to set
     */
    public void setCategoryService(final CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /**
     * @param productUrlConverter
     *            the productUrlConverter to set
     */
    @Required
    public void setProductUrlConverter(
            final AbstractPopulatingConverter<ProductModel, ProductData> productUrlConverter) {
        this.productUrlConverter = productUrlConverter;
    }

    /**
     * @param targetProductDepartmentService
     *            the targetProductDepartmentService to set
     */
    @Required
    public void setTargetProductDepartmentService(final TargetProductDepartmentService targetProductDepartmentService) {
        this.targetProductDepartmentService = targetProductDepartmentService;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @param targetPointOfServiceService
     *            the targetPointOfServiceService to set
     */
    @Required
    public void setTargetPointOfServiceService(final TargetPointOfServiceService targetPointOfServiceService) {
        this.targetPointOfServiceService = targetPointOfServiceService;
    }

    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */
    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }

    /**
     * @param targetStockService
     *            the stockService to set
     */
    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }

    /**
     * @param stockLevelStatusStrategy
     *            the stockLevelStatusStrategy to set
     */
    @Required
    public void setStockLevelStatusStrategy(final StockLevelStatusStrategy stockLevelStatusStrategy) {
        this.stockLevelStatusStrategy = stockLevelStatusStrategy;
    }

    /**
     * @param targetPOSProductService
     */
    @Required
    public void setTargetPOSProductService(final TargetPOSProductService targetPOSProductService) {
        this.targetPOSProductService = targetPOSProductService;
    }

    /**
     * @param targetPOSProductConverter
     */
    @Required
    public void setTargetPOSProductConverter(final TargetPOSProductConverter targetPOSProductConverter) {
        this.targetPOSProductConverter = targetPOSProductConverter;
    }

    /**
     * @param targetDealDescriptionStrategy
     *            the targetDealDescriptionStrategy to set
     */
    @Required
    public void setTargetDealDescriptionStrategy(final TargetDealDescriptionStrategy targetDealDescriptionStrategy) {
        this.targetDealDescriptionStrategy = targetDealDescriptionStrategy;
    }

    /**
     * @param targetProductService
     *            the targetProductService to set
     */
    @Required
    public void setTargetProductService(final TargetProductService targetProductService) {
        this.targetProductService = targetProductService;
    }

    /**
     * @param targetStoreStockService
     *            the targetStoreStockService to set
     */
    @Required
    public void setTargetStoreStockService(final TargetStoreStockService targetStoreStockService) {
        this.targetStoreStockService = targetStoreStockService;
    }

    /**
     * @param fluentStockLookupService
     *            the fluentStockLookupService to set
     */
    @Required
    public void setFluentStockLookupService(final FluentStockLookupService fluentStockLookupService) {
        this.fluentStockLookupService = fluentStockLookupService;
    }

    /**
     * @param targetPurchaseOptionHelper
     *            the targetPurchaseOptionHelper to set
     */
    @Required
    public void setTargetPurchaseOptionHelper(final TargetPurchaseOptionHelper targetPurchaseOptionHelper) {
        this.targetPurchaseOptionHelper = targetPurchaseOptionHelper;
    }
}
