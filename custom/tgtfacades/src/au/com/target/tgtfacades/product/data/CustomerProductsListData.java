/**
 * 
 */
package au.com.target.tgtfacades.product.data;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author mjanarth
 *
 */

public class CustomerProductsListData {


    private String version;
    @JsonProperty("p")
    private List<CustomerProductInfo> products;

    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version
     *            the version to set
     */
    public void setVersion(final String version) {
        this.version = version;
    }

    /**
     * @return the products
     */
    public List<CustomerProductInfo> getProducts() {
        return products;
    }

    /**
     * @param products
     *            the products to set
     */
    public void setProducts(final List<CustomerProductInfo> products) {
        this.products = products;
    }


}
