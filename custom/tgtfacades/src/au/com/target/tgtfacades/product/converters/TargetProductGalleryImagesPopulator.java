package au.com.target.tgtfacades.product.converters;

import static au.com.target.tgtfacades.constants.TgtFacadesConstants.IMAGE_WIDE_FORMAT;

import de.hybris.platform.commercefacades.product.converters.populator.ProductGalleryImagesPopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class TargetProductGalleryImagesPopulator <SOURCE extends ProductModel, TARGET extends ProductData>  extends ProductGalleryImagesPopulator<SOURCE, TARGET>  {


	@Override
	public void populate(final SOURCE productModel, final TARGET productData) throws ConversionException
	{
		// Collect the media containers on the product
		final List<MediaContainerModel> mediaContainers = new ArrayList<>();
		collectMediaContainers(productModel, mediaContainers);

		if (!mediaContainers.isEmpty())
		{
			final List<ImageData> imageList = new ArrayList<>();
			final List<ImageData> wideImageList = new ArrayList<>();

			// fill our image list with the product's existing images
			if (productData.getImages() != null)
			{
				imageList.addAll(productData.getImages());
			}

			// Use all the images as gallery images
			int galleryIndex = 0;
			for (final MediaContainerModel mediaContainer : mediaContainers)
			{
				addImagesInFormats(mediaContainer, ImageDataType.GALLERY, galleryIndex++, imageList);
			}

			Iterator<ImageData> it = imageList.iterator();
			while (it.hasNext()) {
				ImageData imageData = it.next();
				if (imageData.getAltText() == null)
				{
					imageData.setAltText(productModel.getName());
				}
				if(IMAGE_WIDE_FORMAT.equalsIgnoreCase(imageData.getFormat())){
					wideImageList.add(imageData);
					it.remove();
				}
			}

			// Overwrite the existing list of images
			productData.setImages(imageList);
			productData.setWideImages(wideImageList);
		}
	}


}
