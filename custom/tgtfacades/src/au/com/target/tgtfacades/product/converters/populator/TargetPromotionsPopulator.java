/**
 *
 */
package au.com.target.tgtfacades.product.converters.populator;

import de.hybris.platform.commercefacades.product.converters.populator.PromotionsPopulator;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.jalo.TargetDealWithRewardResult;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.model.TargetDealWithRewardResultModel;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;


/**
 * @author mjanarth
 *
 */
public class TargetPromotionsPopulator extends PromotionsPopulator {

    private ModelService modelService;

    @Override
    protected List<String> getFiredPromotionsMessages(final PromotionOrderResults promoOrderResults,
            final AbstractPromotionModel promotion) {
        final List<String> descriptions = new LinkedList<>();

        final List<PromotionResultModel> list = new ArrayList<>();
        final List<PromotionResultModel> firedProductPromotions = getPromotionResultService()
                .getFiredProductPromotions(promoOrderResults, promotion);

        for (final PromotionResultModel result : firedProductPromotions) {

            if (!(result instanceof TargetDealWithRewardResultModel)) {
                list.add(result);
            }
            else {

                final TargetDealWithRewardResultModel rewardResult = (TargetDealWithRewardResultModel)result;
                if (!getCouldHaveMoreRewards(rewardResult)) {
                    list.add(result);
                }
            }
        }
        if (promotion instanceof ProductPromotionModel)
        {
            addDescriptions(descriptions, filter(list, promotion));
        }
        else
        {
            final List<PromotionResultModel> firedOrderPromotions = getPromotionResultService()
                    .getFiredOrderPromotions(promoOrderResults, promotion);
            addDescriptions(descriptions, filter(firedOrderPromotions, promotion));
        }

        return descriptions;
    }

    @Override
    protected List<String> getCouldFirePromotionsMessages(final PromotionOrderResults promoOrderResults,
            final AbstractPromotionModel promotion) {

        final List<String> descriptions = new LinkedList<>();

        final List<PromotionResultModel> list = new ArrayList<>();
        final List<PromotionResultModel> allProductPromotions = getAllProductPromotions(promoOrderResults);
        for (final PromotionResultModel result : allProductPromotions) {
            if (getCouldFire(result)) {

                list.add(result);
            }
            else if (result instanceof TargetDealWithRewardResultModel) {

                final TargetDealWithRewardResultModel rewardResult = (TargetDealWithRewardResultModel)result;
                if (getCouldHaveMoreRewards(rewardResult)) {
                    list.add(result);
                }
            }
        }
        if (promotion instanceof ProductPromotionModel) {
            addDescriptions(descriptions, filter(list, promotion));
        }
        else {
            final List<PromotionResultModel> potentialOrderPromotions = getPromotionResultService()
                    .getPotentialOrderPromotions(promoOrderResults, promotion);
            addDescriptions(descriptions, filter(potentialOrderPromotions, promotion));

        }
        return descriptions;
    }
    
    private List<PromotionResultModel> getAllProductPromotions(final PromotionOrderResults promoOrderResults){
      final List<PromotionResult> allProductPromotions = promoOrderResults.getAllProductPromotions();
      return modelService.getAll(allProductPromotions,new LinkedList<PromotionResultModel>());
    
    }

    private boolean getCouldHaveMoreRewards(final TargetDealWithRewardResultModel rewardResultModel) {
        final TargetDealWithRewardResult rewardResult = getModelService().getSource(rewardResultModel);
        return rewardResult.getCouldhaveMoreRewards();
    }

    private boolean getCouldFire(final PromotionResultModel resultModel) {
        final PromotionResult result = getModelService().getSource(resultModel);
        return result.getCouldFire();
    }

    /**
     * @return the modelService
     */
    protected ModelService getModelService() {
        return modelService;
    }

    /**
     * @param modelService the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }




}
