/**
 * 
 */
package au.com.target.tgtfacades.product;

import de.hybris.platform.commercefacades.product.ProductOption;

import java.util.List;

import au.com.target.tgtfacades.product.data.TargetProductListerData;



/**
 * @author rmcalave
 *
 */
public interface TargetProductListerFacade {

    /**
     * Retrieves the base product data for the given product or variant code.
     * 
     * @param code
     *            The product or variant code
     * @return The base product data, or null if not found
     */
    TargetProductListerData getBaseProductDataByCode(String code, List<ProductOption> options);

}