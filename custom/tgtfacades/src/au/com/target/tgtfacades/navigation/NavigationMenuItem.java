/**
 * 
 */
package au.com.target.tgtfacades.navigation;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;


/**
 * @author rmcalave
 * 
 */
public class NavigationMenuItem {
    private final String name;

    private final String link;

    private Set<NavigationMenuItem> children;

    private final boolean isCurrentPage;

    private final boolean isNewWindow;

    private final boolean isExternal;

    private String style;

    public NavigationMenuItem(final String name, final String link, final Set<NavigationMenuItem> children,
            final boolean isCurrentPage, final boolean isNewWindow, final boolean isExternal) {
        super();
        this.name = name;
        this.link = link;
        this.children = children;
        this.isCurrentPage = isCurrentPage;
        this.isNewWindow = isNewWindow;
        this.isExternal = isExternal;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @return the children
     */
    public Set<NavigationMenuItem> getChildren() {
        return children;
    }

    public void setChildren(final Set<NavigationMenuItem> children) {
        this.children = children;
    }

    /**
     * @return the isCurrentPage
     */
    public boolean isCurrentPage() {
        return isCurrentPage;
    }

    /**
     * @return the isNewWindow
     */
    public boolean isNewWindow() {
        return isNewWindow;
    }

    /**
     * @return the isExternal
     */
    public boolean isExternal() {
        return isExternal;
    }

    /**
     * @return the style
     */
    public String getStyle() {
        return style;
    }

    /**
     * @param style
     *            the style to set
     */
    public void setStyle(final String style) {
        this.style = style;
    }

    public boolean isCurrentPageInChildren() {
        if (isCurrentPage() || CollectionUtils.isEmpty(children)) {
            return false;
        }

        for (final NavigationMenuItem child : children) {
            if (child.isCurrentPage() || child.isCurrentPageInChildren()) {
                return true;
            }
        }

        return false;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(157, 97).appendSuper(super.hashCode()).append(name).append(link).append(children)
                .append(isCurrentPage).append(isNewWindow).append(isExternal).toHashCode();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof NavigationMenuItem)) {
            return false;
        }

        final NavigationMenuItem that = (NavigationMenuItem)obj;

        return new EqualsBuilder().append(this.name, that.name).append(this.link, that.link)
                .append(this.children, that.children)
                .append(this.isCurrentPage, that.isCurrentPage).append(this.isNewWindow, that.isNewWindow)
                .append(this.isExternal, that.isExternal).isEquals();
    }
}
