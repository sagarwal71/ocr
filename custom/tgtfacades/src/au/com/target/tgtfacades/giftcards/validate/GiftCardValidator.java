/**
 * 
 */
package au.com.target.tgtfacades.giftcards.validate;

import de.hybris.platform.core.model.product.ProductModel;

import au.com.target.tgtcore.product.exception.ProductNotFoundException;
import au.com.target.tgtfacades.giftcards.exceptions.GiftCardValidationException;


/**
 * Interface definition for validating gift cards.
 * 
 * @author jjayawa1
 *
 */
public interface GiftCardValidator {
    /**
     * Validation interface for GiftCards.
     * 
     * @param productCode
     * @param newQty
     */
    void validateGiftCards(final String productCode, final long newQty)
            throws GiftCardValidationException, ProductNotFoundException;

    /**
     * @param productCode
     * @param qty
     * @throws GiftCardValidationException
     */
    void validateGiftCardsUpdateQty(String productCode, long qty)
            throws GiftCardValidationException;

    /**
     * @param productModel
     * @param qty
     * @throws GiftCardValidationException
     */
    public void validatePhysicalGiftCards(ProductModel productModel, long qty)
            throws GiftCardValidationException;
}
