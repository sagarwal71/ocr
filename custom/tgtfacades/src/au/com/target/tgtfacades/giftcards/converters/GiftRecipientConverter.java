/**
 * 
 */
package au.com.target.tgtfacades.giftcards.converters;

import org.springframework.util.Assert;

import au.com.target.tgtcore.converters.TargetConverter;
import au.com.target.tgtcore.model.GiftRecipientModel;
import au.com.target.tgtfacades.order.data.GiftRecipientData;


/**
 * @author smishra1
 *
 */
public class GiftRecipientConverter implements
        TargetConverter<GiftRecipientModel, GiftRecipientData> {

    private void convert(final GiftRecipientModel source, final GiftRecipientData target) {
        Assert.notNull(source, "GiftRecipientModel cannot be null");
        target.setId(source.getPk().toString());
        target.setFirstName(source.getFirstName());
        target.setLastName(source.getLastName());
        target.setMessageText(source.getMessageText());
        target.setRecipientEmailAddress(source.getEmail());
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.converters.TargetConverter#convert(java.lang.Object)
     */
    @Override
    public GiftRecipientData convert(final GiftRecipientModel source) {
        final GiftRecipientData recipientData = new GiftRecipientData();
        convert(source, recipientData);

        return recipientData;
    }

}
