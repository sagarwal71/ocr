package au.com.target.tgtfacades.checkout.flow.impl;

import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import au.com.target.tgtcore.customer.TargetCustomerAccountService;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.checkout.flow.TargetCheckoutCustomerStrategy;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtutility.registry.RegistryFacade;


/**
 * @author asingh78
 * 
 */
public class TargetCheckoutCustomerStrategyImpl extends AbstractBusinessService implements
        TargetCheckoutCustomerStrategy {

    protected static final Logger LOG = Logger.getLogger(TargetCheckoutCustomerStrategyImpl.class);

    private static final String MASTER = "master";

    @Autowired
    private UserService userService;
    @Autowired
    private TargetLaybyCartService targetLaybyCartService;
    @Autowired
    private TargetCustomerAccountService customerAccountService;
    @Resource
    private RegistryFacade registryFacade;

    @Override
    public boolean isAnonymousCheckout() {
        return userService.isAnonymousUser(userService.getCurrentUser()) || Boolean.TRUE
                .equals(getSessionService().getAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST))
                || Boolean.TRUE.equals(getSessionService().getAttribute(TgtFacadesConstants.ANONYMOUS_CHECKOUT));

    }

    @Override
    public TargetCustomerModel getCurrentUserForCheckout() {
        if (isAnonymousCheckout()) {
            final CartModel cart = targetLaybyCartService.getSessionCart();
            return getCartUser(cart);
        }
        final UserModel user = userService.getCurrentUser();
        if (user instanceof TargetCustomerModel) {
            return (TargetCustomerModel)user;
        }
        return null;
    }

    @Override
    public void removeGuestCheckoutCustomer(final TargetCustomerModel guestCustomer, final UserModel newCartCustomer) {
        final CartModel cart = targetLaybyCartService.getSessionCart();
        removeGuestCheckoutCustomer(guestCustomer, newCartCustomer, cart);
    }

    @Override
    public void resetGuestCartWithCurrentUser(final HttpSession session) {
        registryFacade.setCurrentTenantByID(MASTER);
        final UserModel user = customerAccountService.getCurrentUserModelBySession(session);
        if (user != null) {
            final CartModel cart = targetLaybyCartService.getCurrentSessionCartBySession(session);
            final TargetCustomerModel cartUser = getCartUser(cart);
            if (cartUser != null && cartUser != user && CustomerType.GUEST.equals(cartUser.getType())) {
                removeGuestCheckoutCustomer(cartUser, user, cart);
            }
        }
    }

    private void removeGuestCheckoutCustomer(final TargetCustomerModel guestCustomer, final UserModel newCartCustomer,
            final CartModel cart) {
        cart.setUser(newCartCustomer);
        getModelService().save(cart);
        // Removing the customer will remove all the associated checkout carts
        getModelService().remove(guestCustomer);
        LOG.debug("Removed the guest checkout user from the cart");
    }

    private TargetCustomerModel getCartUser(final CartModel cart) {
        TargetCustomerModel targetCustomerModel = null;
        if (null != cart) {
            final UserModel customerModel = cart.getUser();
            if (customerModel instanceof TargetCustomerModel) {
                targetCustomerModel = (TargetCustomerModel)customerModel;
            }
        }
        return targetCustomerModel;
    }

}
