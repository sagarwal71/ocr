/**
 * 
 */
package au.com.target.tgtfacades.offer.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.offer.TargetMobileOfferHeadingFacade;
import au.com.target.tgtfacades.offer.converter.TargetMobileOfferHeadingConverter;
import au.com.target.tgtfacades.offers.data.TargetMobileOfferHeadingData;
import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;
import au.com.target.tgtmarketing.offers.TargetMobileOfferHeadingService;


/**
 * @author paul
 *
 */
public class TargetMobileOfferHeadingFacadeImpl implements TargetMobileOfferHeadingFacade {

    private TargetMobileOfferHeadingService targetMobileOfferHeadingsService;

    private TargetMobileOfferHeadingConverter targetMobileOfferHeadingConverter;

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.offer.TargetMobileOfferHeadingFacade#getTargetMobileOfferHeadings()
     */
    @Override
    public List<TargetMobileOfferHeadingData> getTargetMobileOfferHeadings() {
        final List<TargetMobileOfferHeadingData> tgtMoboffrHeadingDataList = new ArrayList<>();
        final List<TargetMobileOfferHeadingModel> tgtMobOffrHdngModelList = targetMobileOfferHeadingsService
                .getAllTargetMobileOfferHeadings();

        if (CollectionUtils.isEmpty(tgtMobOffrHdngModelList)) {
            return tgtMoboffrHeadingDataList;
        }
        for (final TargetMobileOfferHeadingModel tgtMobOffrHdngModel : tgtMobOffrHdngModelList) {
            final TargetMobileOfferHeadingData tgtMobOfferHeadingsData = targetMobileOfferHeadingConverter
                    .convert(tgtMobOffrHdngModel);
            tgtMoboffrHeadingDataList.add(tgtMobOfferHeadingsData);
        }
        return tgtMoboffrHeadingDataList;
    }

    /**
     * @param targetMobileOfferHeadingConverter
     *            the targetMobileOfferHeadingConverter to set
     */
    @Required
    public void setTargetMobileOfferHeadingConverter(
            final TargetMobileOfferHeadingConverter targetMobileOfferHeadingConverter) {
        this.targetMobileOfferHeadingConverter = targetMobileOfferHeadingConverter;
    }

    /**
     * @param targetMobileOfferHeadingsService
     *            the targetMobileOfferHeadingsService to set
     */
    @Required
    public void setTargetMobileOfferHeadingsService(
            final TargetMobileOfferHeadingService targetMobileOfferHeadingsService) {
        this.targetMobileOfferHeadingsService = targetMobileOfferHeadingsService;
    }


}
