/**
 * 
 */
package au.com.target.tgtfacades.marketing.impl;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

import org.apache.commons.lang.Validate;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.marketing.TargetSocialMediaProductsFacade;
import au.com.target.tgtfacades.marketing.data.SocialMediaProductsData;
import au.com.target.tgtfacades.marketing.data.SocialMediaProductsListData;
import au.com.target.tgtmarketing.model.SocialMediaProductsModel;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.shopthelook.LookService;
import au.com.target.tgtmarketing.social.TargetSocialMediaProductsService;


/**
 * @author rmcalave
 *
 */
public class TargetSocialMediaProductsFacadeImpl implements TargetSocialMediaProductsFacade {

    private TargetSocialMediaProductsService targetSocialMediaProductsService;

    private Converter<SocialMediaProductsModel, SocialMediaProductsData> targetSocialMediaProductsConverter;

    private Converter<TargetLookModel, SocialMediaProductsData> targetSocialMediaProductsDataConverter;

    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    private LookService lookService;

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.marketing.impl.TargetSocialMediaProductsFacade#getSocialMediaProductsByCreationTimeDescending(int)
     */
    @Override
    public SocialMediaProductsListData getSocialMediaProductsByCreationTimeDescending(final int count) {
        Validate.isTrue(count > 0, "count must be greater than zero");

        final SocialMediaProductsListData socialMediaProductsList = new SocialMediaProductsListData();
        if (targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.STEP_SHOP_THE_LOOK_APP)) {
            final int numberOfLooks = targetFeatureSwitchFacade
                    .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.STEP_SHOP_THE_LOOK_APP_LIMIT) ? count : -1;
            final List<TargetLookModel> looks = lookService.getAllVisibleLooksWithInstagramUrl(numberOfLooks);
            socialMediaProductsList.setTargetSocialMediaProductsList(
                    Converters.convertAll(looks, targetSocialMediaProductsDataConverter));
        }
        else {
            final List<SocialMediaProductsModel> socialMediaProducts = targetSocialMediaProductsService
                    .getSocialMediaProductsByCreationTimeDescending(count);


            socialMediaProductsList.setTargetSocialMediaProductsList(
                    Converters.convertAll(socialMediaProducts, targetSocialMediaProductsConverter));
        }

        return socialMediaProductsList;
    }

    /**
     * @param targetSocialMediaProductsService
     *            the targetSocialMediaProductsService to set
     */
    @Required
    public void setTargetSocialMediaProductsService(
            final TargetSocialMediaProductsService targetSocialMediaProductsService) {
        this.targetSocialMediaProductsService = targetSocialMediaProductsService;
    }

    /**
     * @param targetSocialMediaProductsConverter
     *            the targetSocialMediaProductsConverter to set
     */
    @Required
    public void setTargetSocialMediaProductsConverter(
            final Converter<SocialMediaProductsModel, SocialMediaProductsData> targetSocialMediaProductsConverter) {
        this.targetSocialMediaProductsConverter = targetSocialMediaProductsConverter;
    }

    /**
     * @param targetSocialMediaProductsDataConverter
     *            the targetSocialMediaProductsDataConverter to set
     */
    @Required
    public void setTargetSocialMediaProductsDataConverter(
            final Converter<TargetLookModel, SocialMediaProductsData> targetSocialMediaProductsDataConverter) {
        this.targetSocialMediaProductsDataConverter = targetSocialMediaProductsDataConverter;
    }

    /**
     * @param targetFeatureSwitchFacade
     *            the targetFeatureSwitchFacade to set
     */
    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }

    /**
     * @param lookService
     *            the lookService to set
     */
    @Required
    public void setLookService(final LookService lookService) {
        this.lookService = lookService;
    }
}
