/**
 * 
 */
package au.com.target.tgtfacades.sort;

import de.hybris.platform.core.servicelayer.data.SortData;


/**
 * For SortData details
 * 
 * @author smudumba
 * 
 */
public class TargetSortData extends SortData {

    private String url;

    private String wsUrl;

    private boolean selected;

    private String name;

    /**
     * Constructor
     */
    public TargetSortData() {
        super();
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     *            the url to set
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    /**
     * @return the wsUrl
     */
    public String getWsUrl() {
        return wsUrl;
    }

    /**
     * @param wsUrl
     *            the wsUrl to set
     */
    public void setWsUrl(final String wsUrl) {
        this.wsUrl = wsUrl;
    }

    /**
     * @return the selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * @param selected
     *            the selected to set
     */
    public void setSelected(final boolean selected) {
        this.selected = selected;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }



}
