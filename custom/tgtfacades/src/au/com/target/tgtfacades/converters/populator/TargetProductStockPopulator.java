/**
 * 
 */
package au.com.target.tgtfacades.converters.populator;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.price.TargetCommercePriceService;
import au.com.target.tgtcore.price.data.PriceRangeInformation;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtfacades.product.data.AbstractTargetProductData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class TargetProductStockPopulator<SOURCE extends ProductModel, TARGET extends AbstractTargetProductData> extends
        AbstractProductPopulator<SOURCE, TARGET> {

    private TargetStockService targetStockService;

    private Converter<StockLevelStatus, StockData> stockLevelStatusConverter;

    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;

    private TargetCommercePriceService targetCommercePriceService;

    @Override
    public void populate(final ProductModel source, final AbstractTargetProductData target) throws ConversionException {
        target.setAvailable(new HashMap<String, Boolean>());
        ProductModel baseProduct = source;
        while (baseProduct instanceof AbstractTargetVariantProductModel) {
            baseProduct = ((AbstractTargetVariantProductModel)baseProduct).getBaseProduct();
        }
        Assert.isInstanceOf(TargetProductModel.class, baseProduct, "Must be a valid target product");

        final PurchaseOptionModel purchaseOptionModel = targetPurchaseOptionHelper.getCurrentPurchaseOptionModel();

        target.getAvailable().put(purchaseOptionModel.getCode(), Boolean.TRUE);

        final PriceRangeInformation priceRangeInformation = targetCommercePriceService
                .getPriceRangeInfoForProduct(source);

        if (priceRangeInformation.getFromPrice() == null && !priceRangeInformation.sellPriceIsRange()) {
            target.setAvailable(Collections.EMPTY_MAP);
            target.setInStock(false);
        }
        else if (CollectionUtils.isEmpty(source.getVariants())) {
            populateTargetInStocks(source, target);
        }
        else {
            populateTargetInStocksFromVariants(source, target);
        }

        populateStockForTargetVariantLister(target);

        setStockLevelStatusFromAvailablePurchaseMethod(source, target);
    }


    private void populateStockForTargetVariantLister(final AbstractTargetProductData target) {
        if (!(target instanceof TargetVariantProductListerData)) {
            return;
        }
        final boolean instockFlag = target.isInStock();
        ((TargetVariantProductListerData)target).setInStock(instockFlag);
    }

    /**
     * populate in stocks for all supported purchase methods
     * 
     * @param source
     * @param target
     */
    private void populateTargetInStocks(final ProductModel source, final AbstractTargetProductData target) {
        if (source instanceof AbstractTargetVariantProductModel) {
            final AbstractTargetVariantProductModel variant = (AbstractTargetVariantProductModel)source;
            if (BooleanUtils.isTrue(variant.getDisplayOnly())) {
                return;
            }
        }
        final StockData stockData = targetStockService.getStockLevelAndStatusAmount(source);
        final StockLevelStatus stock = stockData.getStockLevelStatus();
        target.setInStock(stock == StockLevelStatus.OUTOFSTOCK ? false : true);

        target.setStock(stockData);
    }


    /**
     * Populate in stocks for all supported methods from product variants
     * 
     * @param source
     * @param target
     */
    private boolean populateTargetInStocksFromVariants(final ProductModel source,
            final AbstractTargetProductData target) {
        boolean availableOnline = false;
        for (final VariantProductModel variantProductModel : source.getVariants()) {
            if (availableOnline) {
                break;
            }
            if (CollectionUtils.isEmpty(variantProductModel.getVariants())) {
                availableOnline = populateTargetInStocksFromVariant(variantProductModel, target);
            }
            else {
                availableOnline = populateTargetInStocksFromVariants(variantProductModel, target);
            }
        }
        return availableOnline;
    }

    /**
     * @param variantProductModel
     * @param target
     */
    private boolean populateTargetInStocksFromVariant(final VariantProductModel variantProductModel,
            final AbstractTargetProductData target) {
        if (BooleanUtils.isTrue(((AbstractTargetVariantProductModel)variantProductModel).getDisplayOnly())) {
            return false;
        }
        final StockLevelStatus stock = targetStockService.getProductStatus(variantProductModel);

        return populateTargetInStocksFromStockLevelStatus(stock, target);
    }

    /**
     * @param stock
     * @param target
     */
    private boolean populateTargetInStocksFromStockLevelStatus(final StockLevelStatus stock,
            final AbstractTargetProductData target) {
        if (stock != StockLevelStatus.OUTOFSTOCK) {
            target.setInStock(true);
            return true;
        }
        return false;
    }


    /**
     * Compute the global stock level of a product, to be INSTOCK a product must have an available purchase option and
     * stock for this purchase option
     * 
     * @param source
     * 
     * @param target
     */
    private void setStockLevelStatusFromAvailablePurchaseMethod(final ProductModel source,
            final AbstractTargetProductData target) {
        for (final Entry<String, Boolean> entry : target.getAvailable().entrySet()) {
            if (entry.getValue().booleanValue()) {
                if (target.isInStock()) {
                    if (CollectionUtils.isEmpty(source.getVariants())) {
                        target.setStock(targetStockService
                                .getStockLevelAndStatusAmount(source));
                        return;
                    }
                    target.setStock(stockLevelStatusConverter.convert(StockLevelStatus.INSTOCK));
                    return;
                }
            }
        }
        final StockData stockData = stockLevelStatusConverter.convert(StockLevelStatus.OUTOFSTOCK);
        if (CollectionUtils.isNotEmpty(source.getVariants())) {
            stockData.setStockLevel(null);
        }
        target.setStock(stockData);
    }

    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }

    /**
     * @param stockLevelStatusConverter
     *            the stockLevelStatusConverter to set
     */
    @Required
    public void setStockLevelStatusConverter(final Converter<StockLevelStatus, StockData> stockLevelStatusConverter) {
        this.stockLevelStatusConverter = stockLevelStatusConverter;
    }

    /**
     * @return the targetPurchaseOptionHelper
     */
    public TargetPurchaseOptionHelper getTargetPurchaseOptionHelper() {
        return targetPurchaseOptionHelper;
    }

    /**
     * @param targetPurchaseOptionHelper
     *            the targetPurchaseOptionHelper to set
     */
    @Required
    public void setTargetPurchaseOptionHelper(final TargetPurchaseOptionHelper targetPurchaseOptionHelper) {
        this.targetPurchaseOptionHelper = targetPurchaseOptionHelper;
    }

    /**
     * @param targetCommercePriceService
     *            the targetCommercePriceService to set
     */
    @Required
    public void setTargetCommercePriceService(final TargetCommercePriceService targetCommercePriceService) {
        this.targetCommercePriceService = targetCommercePriceService;
    }
}
