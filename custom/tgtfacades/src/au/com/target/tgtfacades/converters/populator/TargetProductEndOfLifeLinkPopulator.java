/**
 * 
 */
package au.com.target.tgtfacades.converters.populator;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.CollectionUtils;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.service.EndecaDimensionService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.product.data.AbstractTargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductEndLinkData;
import au.com.target.tgtfacades.stl.TargetShopTheLookFacade;
import au.com.target.tgtfacades.url.impl.TargetCategoryModelUrlResolver;
import au.com.target.tgtfacades.url.impl.TargetShopTheLookUrlResolver;
import au.com.target.tgtfacades.util.TargetProductDataHelper;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;


/**
 * @author fkhan4
 *
 */
public class TargetProductEndOfLifeLinkPopulator<S extends ProductModel, T extends AbstractTargetProductData>
        extends AbstractProductPopulator<S, T> {


    private TargetCategoryModelUrlResolver targetCategoryModelUrlResolver;
    private TargetShopTheLookFacade targetShopTheLookFacade;
    private TargetShopTheLookUrlResolver targetShopTheLookUrlResolver;
    private EndecaDimensionService endecaDimensionService;


    /* (non-Javadoc)
     * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final ProductModel source, final AbstractTargetProductData target) throws ConversionException {

        final TargetProductData targetProductData = (TargetProductData)target;
        TargetProductModel sourceModel = null;

        if (source instanceof TargetProductModel) {
            sourceModel = (TargetProductModel)source;
        }
        else if (source instanceof AbstractTargetVariantProductModel) {
            sourceModel = (TargetProductModel)TargetProductDataHelper.getBaseProduct(source);
        }
        if (null == sourceModel) {
            return;
        }
        populateProductEndLinks(targetProductData, sourceModel);

    }

    /**
     * @param targetProductData
     * @param sourceModel
     */
    private void populateProductEndLinks(final TargetProductData targetProductData,
            final TargetProductModel sourceModel) {

        if (null != sourceModel.getOriginalCategory()) {
            final List<TargetProductEndLinkData> productEndLinks = new ArrayList();
            getLinkShopByCategory(productEndLinks, sourceModel);
            final TargetProductCategoryModel topLevelCategory = getTopLevelCategoryOfOriginalCategory(
                    sourceModel);
            getLinkShopByInspiration(productEndLinks, topLevelCategory);
            getLinkShopByNewArrivals(productEndLinks, topLevelCategory);
            targetProductData.setProductEndLinks(productEndLinks);
        }

    }

    /**
     * @param productEndLinks
     * @param topLevelCategory
     */
    private void getLinkShopByNewArrivals(final List<TargetProductEndLinkData> productEndLinks,
            final TargetProductCategoryModel topLevelCategory) {

        if (null != topLevelCategory) {
            final TargetProductEndLinkData productEndLinkData = new TargetProductEndLinkData();
            final String navigationState = getDimForNewArrivalsInTopCategory(topLevelCategory.getCode());
            if (StringUtils.isNotEmpty(navigationState)) {
                productEndLinkData.setCode(topLevelCategory.getCode());
                productEndLinkData.setName(EndecaConstants.DimensionValue.NEW_ARRIVALS);
                productEndLinkData.setUrl(
                        targetCategoryModelUrlResolver.resolve(topLevelCategory)
                                + EndecaConstants.EndecaFilterQuery.ENDECA_NAV_STATE_QUERY_PARAM_TERM
                                + navigationState);
                productEndLinks.add(productEndLinkData);
            }

        }

    }

    /**
     * @param categoryCode
     * @return navigationState
     */
    private String getDimForNewArrivalsInTopCategory(final String categoryCode) {
        final Map<String, String> dimensionIdMap = new HashMap();
        dimensionIdMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY, categoryCode);
        dimensionIdMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PROD_TAGS,
                EndecaConstants.DimensionValue.NEW_ARRIVALS);
        return endecaDimensionService.getDimensionNavigationState(dimensionIdMap);

    }

    /**
     * @param productEndLinks
     * @param topLevelCategory
     */
    private void getLinkShopByInspiration(final List<TargetProductEndLinkData> productEndLinks,
            final TargetProductCategoryModel topLevelCategory) {
        final TargetProductEndLinkData productEndLinkData = new TargetProductEndLinkData();

        if (null != topLevelCategory) {
            final TargetShopTheLookModel shopTheLookModel = targetShopTheLookFacade
                    .getShopTheLookByCategoryCode(topLevelCategory.getCode());
            if (null != shopTheLookModel) {
                productEndLinkData.setCode(shopTheLookModel.getId());
                productEndLinkData.setName(shopTheLookModel.getName());
                productEndLinkData.setUrl(targetShopTheLookUrlResolver.resolve(shopTheLookModel));
                productEndLinks.add(productEndLinkData);
            }
        }
    }

    private TargetProductCategoryModel getTopLevelCategoryOfOriginalCategory(
            final TargetProductModel sourceModel) {
        TargetProductCategoryModel category = sourceModel.getOriginalCategory();
        TargetProductCategoryModel rootCategory = category;
        do {
            category = getSuperCategory(category);
            if (null != category && TgtCoreConstants.Category.ALL_PRODUCTS.equals(category.getCode())) {
                break;
            }
            rootCategory = category;
        }
        while (null != category);

        return rootCategory;
    }

    /**
     * @param category
     * @return TargetProductCategoryModel
     */
    private TargetProductCategoryModel getSuperCategory(final TargetProductCategoryModel category) {

        final List<CategoryModel> superCategoryList = category.getSupercategories();
        if (CollectionUtils.isEmpty(superCategoryList)) {
            return category;
        }
        for (final CategoryModel categoryModel : superCategoryList) {
            if (categoryModel instanceof TargetProductCategoryModel) {
                return (TargetProductCategoryModel)categoryModel;
            }
        }
        return null;
    }

    /**
     * 
     * @param productEndLinks
     * @param sourceModel
     */

    private void getLinkShopByCategory(final List<TargetProductEndLinkData> productEndLinks,
            final TargetProductModel sourceModel) {

        final TargetProductEndLinkData productEndLinkData = new TargetProductEndLinkData();
        productEndLinkData.setCode(sourceModel.getOriginalCategory().getCode());
        productEndLinkData.setName(sourceModel.getOriginalCategory().getName());
        productEndLinkData.setUrl(targetCategoryModelUrlResolver.resolve(sourceModel.getOriginalCategory()));
        productEndLinks.add(productEndLinkData);

    }

    /**
     * @param targetCategoryModelUrlResolver
     *            the targetCategoryModelUrlResolver to set
     */
    @Required
    public void setTargetCategoryModelUrlResolver(final TargetCategoryModelUrlResolver targetCategoryModelUrlResolver) {
        this.targetCategoryModelUrlResolver = targetCategoryModelUrlResolver;
    }

    /**
     * @param targetShopTheLookFacade
     *            the targetShopTheLookFacade to set
     */
    @Required
    public void setTargetShopTheLookFacade(final TargetShopTheLookFacade targetShopTheLookFacade) {
        this.targetShopTheLookFacade = targetShopTheLookFacade;
    }

    /**
     * @param targetShopTheLookUrlResolver
     *            the targetShopTheLookUrlResolver to set
     */
    @Required
    public void setTargetShopTheLookUrlResolver(final TargetShopTheLookUrlResolver targetShopTheLookUrlResolver) {
        this.targetShopTheLookUrlResolver = targetShopTheLookUrlResolver;
    }

    /**
     * @param endecaDimensionService
     *            the endecaDimensionService to set
     */
    @Required
    public void setEndecaDimensionService(final EndecaDimensionService endecaDimensionService) {
        this.endecaDimensionService = endecaDimensionService;
    }

}
