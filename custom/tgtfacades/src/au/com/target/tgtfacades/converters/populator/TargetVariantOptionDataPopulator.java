/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package au.com.target.tgtfacades.converters.populator;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.commercefacades.product.ImageFormatMapping;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.product.data.VariantOptionQualifierData;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.product.VariantsService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.media.MediaContainerService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.variants.model.VariantAttributeDescriptorModel;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantTypeModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.ColourModel;
import au.com.target.tgtcore.model.TargetProductSizeModel;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.product.data.TargetProductSizeData;
import au.com.target.tgtfacades.product.data.TargetVariantOptionQualifierData;


/**
 *
 */
public class TargetVariantOptionDataPopulator implements Populator<VariantProductModel, VariantOptionData> {

    private static final Logger LOG = Logger.getLogger(TargetVariantOptionDataPopulator.class);

    private VariantsService variantsService;

    private StockService stockService;

    private CommercePriceService commercePriceService;

    private WarehouseService warehouseService;

    private PriceDataFactory priceDataFactory;

    private UrlResolver<ProductModel> productModelUrlResolver;

    private Converter<MediaModel, ImageData> imageConverter;

    private TypeService typeService;

    private MediaService mediaService;

    private MediaContainerService mediaContainerService;

    private ImageFormatMapping imageFormatMapping;

    private Map<String, String> variantAttributeMapping;

    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    private Map<String, String> excludeVariantQualifiers;

    @Override
    public void populate(final VariantProductModel source, final VariantOptionData target) {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        final VariantTypeModel variantType = source.getBaseProduct().getVariantType();
        final List<VariantAttributeDescriptorModel> descriptorModels = getVariantsService()
                .getVariantAttributesForVariantType(
                        variantType);

        final Collection<VariantOptionQualifierData> variantOptionQualifiers = new ArrayList<>();
        for (final VariantAttributeDescriptorModel descriptorModel : descriptorModels) {
            // Create the variant qualifier
            final TargetVariantOptionQualifierData variantOptionQualifier = new TargetVariantOptionQualifierData();
            final String qualifier = descriptorModel.getQualifier();
            variantOptionQualifier.setQualifier(qualifier);
            variantOptionQualifier.setName(descriptorModel.getName());

            final boolean skipPopulate = MapUtils.isNotEmpty(excludeVariantQualifiers)
                    && excludeVariantQualifiers.containsKey(qualifier)
                    && !targetFeatureSwitchFacade.isFeatureEnabled(excludeVariantQualifiers.get(qualifier));

            if (!skipPopulate) {
                // Lookup the value
                final Object variantAttributeValue = getVariantsService().getVariantAttributeValue(source,
                        descriptorModel.getQualifier());
                if (null != variantAttributeValue && variantAttributeValue instanceof ColourModel) {
                    final ColourModel colourModel = (ColourModel)variantAttributeValue;

                    if (Boolean.TRUE.equals(colourModel.getDisplay())) {
                        variantOptionQualifier.setValue(colourModel.getName());
                    }
                    else {
                        variantOptionQualifier.setValue("");
                    }
                }
                else if (null != variantAttributeValue && variantAttributeValue instanceof TargetProductSizeModel
                        && targetFeatureSwitchFacade.isSizeOrderingForUIEnabled()) {
                    final TargetProductSizeModel targetProductSizeModel = (TargetProductSizeModel)variantAttributeValue;
                    final TargetProductSizeData productSizeData = new TargetProductSizeData();
                    productSizeData.setSize(targetProductSizeModel.getSize());
                    productSizeData.setPosition(targetProductSizeModel.getPosition());
                    variantOptionQualifier.setObjectValue(productSizeData);
                }
                else {
                    variantOptionQualifier.setValue(variantAttributeValue == null ? "" : variantAttributeValue
                            .toString());
                }
                // Add to list of variants
                variantOptionQualifiers.add(variantOptionQualifier);
            }
        }
        target.setVariantOptionQualifiers(variantOptionQualifiers);

        target.setUrl(getProductModelUrlResolver().resolve(source));
        target.setCode(source.getCode());

        final PriceDataType priceType;
        final PriceInformation info;
        if (CollectionUtils.isEmpty(source.getVariants())) {
            priceType = PriceDataType.BUY;
            info = getCommercePriceService().getWebPriceForProduct(source);
        }
        else {
            priceType = PriceDataType.FROM;
            info = getCommercePriceService().getFromPriceForProduct(source);
        }

        if (info != null) {
            final PriceData priceData = getPriceDataFactory().create(priceType,
                    BigDecimal.valueOf(info.getPriceValue().getValue()),
                    info.getPriceValue().getCurrencyIso());
            target.setPriceData(priceData);
        }

        final MediaContainerModel mediaContainer = getPrimaryImageMediaContainer(source);
        if (mediaContainer != null) {
            final ComposedTypeModel productType = getTypeService().getComposedTypeForClass(source.getClass());
            for (final VariantOptionQualifierData variantOptionQualifier : target.getVariantOptionQualifiers()) {
                final MediaModel media = getMediaWithImageFormat(mediaContainer,
                        lookupImageFormat(productType, variantOptionQualifier.getQualifier()));
                if (media != null) {
                    variantOptionQualifier.setImage(getImageConverter().convert(media));
                }
            }
        }
    }

    protected int calculateTotalActualAmount(final Collection<StockLevelModel> stockLevels) {
        int totalActualAmount = 0;
        for (final StockLevelModel stockLevel : stockLevels) {
            final int actualAmount = stockLevel.getAvailable() - stockLevel.getReserved();
            if (((actualAmount <= 0) && (stockLevel.isTreatNegativeAsZero()))
                    || stockLevel.getInStockStatus().equals(InStockStatus.FORCEOUTOFSTOCK)) {
                continue;
            }
            totalActualAmount += actualAmount;
        }

        return totalActualAmount;
    }

    protected MediaModel getMediaWithImageFormat(final MediaContainerModel mediaContainer, final String imageFormat) {
        if (mediaContainer != null && imageFormat != null) {
            final String mediaFormatQualifier = getImageFormatMapping().getMediaFormatQualifierForImageFormat(
                    imageFormat);
            if (mediaFormatQualifier != null) {
                final MediaFormatModel mediaFormat = getMediaService().getFormat(mediaFormatQualifier);
                if (mediaFormat != null) {
                    try {
                        return getMediaContainerService().getMediaForFormat(mediaContainer, mediaFormat);
                    }
                    catch (final ModelNotFoundException mex) {
                        LOG.error("There was an error retrieving MediaModel, possibly missing a swatch image.", mex);
                        return null;
                    }
                }
            }
        }
        return null;
    }

    protected String lookupImageFormat(final ComposedTypeModel productType, final String attributeQualifier) {
        if (productType == null) {
            return null;
        }

        // Lookup the image format mapping
        final String key = productType.getCode() + "." + attributeQualifier;
        final String imageFormat = getVariantAttributeMapping().get(key);

        // Try super type of not found for this type
        return imageFormat != null ? imageFormat : lookupImageFormat(productType.getSuperType(), attributeQualifier);
    }

    protected MediaContainerModel getPrimaryImageMediaContainer(final VariantProductModel variantProductModel) {
        final MediaModel picture = variantProductModel.getPicture();
        if (picture != null) {
            return picture.getMediaContainer();
        }
        return null;
    }

    protected TypeService getTypeService() {
        return typeService;
    }

    @Required
    public void setTypeService(final TypeService typeService) {
        this.typeService = typeService;
    }

    protected MediaService getMediaService() {
        return mediaService;
    }

    @Required
    public void setMediaService(final MediaService mediaService) {
        this.mediaService = mediaService;
    }

    protected MediaContainerService getMediaContainerService() {
        return mediaContainerService;
    }

    @Required
    public void setMediaContainerService(final MediaContainerService mediaContainerService) {
        this.mediaContainerService = mediaContainerService;
    }

    protected ImageFormatMapping getImageFormatMapping() {
        return imageFormatMapping;
    }

    @Required
    public void setImageFormatMapping(final ImageFormatMapping imageFormatMapping) {
        this.imageFormatMapping = imageFormatMapping;
    }

    protected Map<String, String> getVariantAttributeMapping() {
        return variantAttributeMapping;
    }

    @Required
    public void setVariantAttributeMapping(final Map<String, String> variantAttributeMapping) {
        this.variantAttributeMapping = variantAttributeMapping;
    }

    protected WarehouseService getWarehouseService() {
        return warehouseService;
    }

    @Required
    public void setWarehouseService(final WarehouseService warehouseService) {
        this.warehouseService = warehouseService;
    }

    protected Converter<MediaModel, ImageData> getImageConverter() {
        return imageConverter;
    }

    /**
     * Sets the image converter.
     *
     * @param imageConverter
     *            the image converter
     */
    @Required
    public void setImageConverter(final Converter<MediaModel, ImageData> imageConverter) {
        this.imageConverter = imageConverter;
    }

    @Required
    protected VariantsService getVariantsService() {
        return variantsService;
    }

    @Required
    public void setVariantsService(final VariantsService variantsService) {
        this.variantsService = variantsService;
    }

    protected UrlResolver<ProductModel> getProductModelUrlResolver() {
        return productModelUrlResolver;
    }

    @Required
    public void setProductModelUrlResolver(final UrlResolver<ProductModel> productModelUrlResolver) {
        this.productModelUrlResolver = productModelUrlResolver;
    }

    protected StockService getStockService() {
        return stockService;
    }

    @Required
    public void setStockService(final StockService stockService) {
        this.stockService = stockService;
    }

    protected CommercePriceService getCommercePriceService() {
        return commercePriceService;
    }

    @Required
    public void setCommercePriceService(final CommercePriceService commercePriceService) {
        this.commercePriceService = commercePriceService;
    }

    protected PriceDataFactory getPriceDataFactory() {
        return priceDataFactory;
    }

    @Required
    public void setPriceDataFactory(final PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }

    /**
     * @param targetFeatureSwitchFacade
     *            the targetFeatureSwitchFacade to set
     */
    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }

    /**
     * @param excludeVariantQualifiers
     *            the excludeVariantQualifiers to set
     */
    @Required
    public void setExcludeVariantQualifiers(final Map<String, String> excludeVariantQualifiers) {
        this.excludeVariantQualifiers = excludeVariantQualifiers;
    }

}
