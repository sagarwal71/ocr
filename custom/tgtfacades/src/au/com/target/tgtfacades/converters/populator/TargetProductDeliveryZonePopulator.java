/**
 * 
 */
package au.com.target.tgtfacades.converters.populator;

import de.hybris.platform.commercefacades.order.data.ZoneDeliveryModeData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.order.TargetPreOrderService;
import au.com.target.tgtcore.price.data.PriceRangeInformation;
import au.com.target.tgtcore.product.data.ProductDisplayType;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.product.data.AbstractTargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductData;


/**
 * @author mgazal
 *
 */
public class TargetProductDeliveryZonePopulator
        extends AbstractTargetProductPopulator<ProductModel, TargetProductData> {

    private Converter<ZoneDeliveryModeModel, ZoneDeliveryModeData> targetZoneDeliveryModeConverter;

    private TargetDeliveryService targetDeliveryService;

    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    private TargetSalesApplicationService targetSalesApplicationService;

    private TargetPreOrderService targetPreOrderService;

    @Override
    public void populate(final ProductModel source, final AbstractTargetProductData target) throws ConversionException {
        TargetProductModel sourceModel = null;
        AbstractTargetVariantProductModel targetVariantModel = null;
        if (source instanceof TargetProductModel) {
            sourceModel = (TargetProductModel)source;
        }
        else if (source instanceof AbstractTargetVariantProductModel) {
            targetVariantModel = (AbstractTargetVariantProductModel)source;
            sourceModel = getBaseTargetProduct(targetVariantModel);
        }
        if (sourceModel == null) {
            return;
        }
        populateDeliveryModes(targetVariantModel != null ? targetVariantModel : sourceModel, target);
    }

    /**
     * Populate delivery modes.
     *
     * @param source
     *            the product model
     * @param target
     *            the target data
     */
    protected void populateDeliveryModes(final ProductModel source, final AbstractTargetProductData target) {
        final PriceRangeInformation priceRange = getPriceRangeInformationForProduct(source);

        final List<TargetZoneDeliveryModeData> deliveryModeList = new ArrayList<>();

        final TargetZoneDeliveryModeModel digitalDeliveryMode = targetDeliveryModeHelper.getDigitalDeliveryMode(source);
        if (digitalDeliveryMode != null) {
            final TargetZoneDeliveryModeData deliveryModeData = populateDeliveryModeData(source,
                    digitalDeliveryMode, true, priceRange != null ? priceRange.getFromPrice() : null);
            deliveryModeList.add(deliveryModeData);
        }
        else {
            populateNonDigitalDeliveryModes(source, target, deliveryModeList, priceRange);
        }
        target.setDeliveryModes(deliveryModeList);
    }

    /**
     * Populate non digital delivery modes.
     * 
     * @param source
     * @param target
     * @param deliveryModeList
     * @param priceRange
     */
    private void populateNonDigitalDeliveryModes(final ProductModel source, final AbstractTargetProductData target,
            final List<TargetZoneDeliveryModeData> deliveryModeList, final PriceRangeInformation priceRange) {
        boolean deliveryOnlyToStore = true;
        boolean allowDeliverToStore = false;
        // first fetching the list of available delivery modes for the product
        final SalesApplication salesApplication = targetSalesApplicationService.getCurrentSalesApplication();
        final Collection<TargetZoneDeliveryModeModel> allActiveDeliveryModes = targetDeliveryService
                .getAllCurrentlyActiveDeliveryModes(salesApplication);

        for (final TargetZoneDeliveryModeModel deliveryModeModel : allActiveDeliveryModes) {

            final boolean foundDeliveryMode = targetDeliveryModeHelper.isProductAvailableForDeliveryMode(source,
                    deliveryModeModel);

            if (foundDeliveryMode) {
                if (BooleanUtils.isTrue(deliveryModeModel.getIsDeliveryToStore())) {
                    allowDeliverToStore = true;
                }
                else {
                    deliveryOnlyToStore = false;
                }
            }
            final TargetZoneDeliveryModeData deliveryModeData = populateDeliveryModeData(source, deliveryModeModel,
                    foundDeliveryMode, priceRange != null ? priceRange.getFromPrice() : null);

            deliveryModeList.add(deliveryModeData);
        }
        target.setProductDeliveryToStoreOnly(deliveryOnlyToStore);
        target.setAllowDeliverToStore(allowDeliverToStore);
    }

    /**
     * Method to get price range information for product
     * 
     * @param source
     * @return PriceRangeInformation
     */
    private PriceRangeInformation getPriceRangeInformationForProduct(final ProductModel source) {
        final PriceRangeInformation priceRange;
        if (source instanceof AbstractTargetVariantProductModel) {
            priceRange = getTargetCommercePriceService().getPriceRangeInfoForProduct(
                    ((AbstractTargetVariantProductModel)source).getBaseProduct());
        }
        else {
            priceRange = getTargetCommercePriceService().getPriceRangeInfoForProduct(
                    source);
        }
        return priceRange;
    }

    /**
     * convert the deliveryMode to data and populate some details
     * 
     * @param source
     * @param deliveryMode
     * @param isValidDelModeForProduct
     * @return a valid {@link TargetZoneDeliveryModeData}
     */
    private TargetZoneDeliveryModeData populateDeliveryModeData(final ProductModel source,
            final TargetZoneDeliveryModeModel deliveryMode, final boolean isValidDelModeForProduct,
            final Double minCostOfProduct) {
        final TargetZoneDeliveryModeData deliveryModeData = (TargetZoneDeliveryModeData)targetZoneDeliveryModeConverter
                .convert(deliveryMode);
        final List<AbstractTargetZoneDeliveryModeValueModel> valueModelList = targetDeliveryService
                .getAllApplicableDeliveryValuesForModeAndProduct(deliveryMode, source);
        if (CollectionUtils.isNotEmpty(valueModelList)) {
            final AbstractTargetZoneDeliveryModeValueModel deliveryValueModel = valueModelList.get(0);

            if (source instanceof AbstractTargetVariantProductModel
                    && ProductDisplayType.PREORDER_AVAILABLE.equals(targetPreOrderService
                            .getProductDisplayType((AbstractTargetVariantProductModel)source))) {
                deliveryModeData.setShortDescription(deliveryValueModel.getPreOrderMessage() != null
                        ? deliveryValueModel.getPreOrderMessage() : deliveryValueModel.getShortMessage());

                deliveryModeData.setLongDescription(deliveryValueModel.getPreOrderLongMessage() != null
                        ? deliveryValueModel.getPreOrderLongMessage() : deliveryValueModel.getMessage());
            }
            else {
                deliveryModeData.setLongDescription(deliveryValueModel.getMessage());
                deliveryModeData.setShortDescription(deliveryValueModel
                        .getShortMessage());
            }

            deliveryModeData.setDisclaimer(deliveryValueModel.getDisclaimer());
            // now setting the highest priority mode value available for a particular mode
            if (isValidDelModeForProduct) {
                deliveryModeData.setAvailable(true);
                if (BooleanUtils.isNotTrue(deliveryMode.getIsDigital())
                        && checkIfFreeDeliveryIsApplicable(minCostOfProduct, source, deliveryMode)) {
                    deliveryModeData.setName(TgtFacadesConstants.FREE_LABEL + deliveryModeData.getName());
                }
            }
        }
        return deliveryModeData;
    }

    /**
     * Method to check if minCostOfProduct is not null and free delivery is applicable for the product
     * 
     * @param minCostOfProduct
     * @param source
     * @param deliveryMode
     * @return boolean
     */
    private boolean checkIfFreeDeliveryIsApplicable(final Double minCostOfProduct, final ProductModel source,
            final TargetZoneDeliveryModeModel deliveryMode) {
        return minCostOfProduct != null && targetDeliveryService
                .isFreeDeliveryApplicableOnProductForDeliveryMode(source, minCostOfProduct.doubleValue(), deliveryMode);
    }

    /**
     * @param targetZoneDeliveryModeConverter
     *            the targetZoneDeliveryModeConverter to set
     */
    @Required
    public void setTargetZoneDeliveryModeConverter(
            final Converter<ZoneDeliveryModeModel, ZoneDeliveryModeData> targetZoneDeliveryModeConverter) {
        this.targetZoneDeliveryModeConverter = targetZoneDeliveryModeConverter;
    }

    /**
     * @param targetDeliveryService
     *            the targetDeliveryService to set
     */
    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        this.targetDeliveryService = targetDeliveryService;
    }

    /**
     * @param targetDeliveryModeHelper
     *            the targetDeliveryModeHelper to set
     */
    @Required
    public void setTargetDeliveryModeHelper(final TargetDeliveryModeHelper targetDeliveryModeHelper) {
        this.targetDeliveryModeHelper = targetDeliveryModeHelper;
    }

    /**
     * @param targetSalesApplicationService
     *            the targetSalesApplicationService to set
     */
    @Required
    public void setTargetSalesApplicationService(final TargetSalesApplicationService targetSalesApplicationService) {
        this.targetSalesApplicationService = targetSalesApplicationService;
    }

    /**
     * @param targetPreOrderService
     *            the targetPreOrderService to set
     */
    @Required
    public void setTargetPreOrderService(final TargetPreOrderService targetPreOrderService) {
        this.targetPreOrderService = targetPreOrderService;
    }

}
