/**
 * 
 */
package au.com.target.tgtfacades.converters.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang.BooleanUtils;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.product.data.TargetProductData;


/**
 * @author asingh78
 * 
 */
public class TargetSizeVariantPopulator<SOURCE extends ProductModel, TARGET extends ProductData> extends
        AbstractTargetVariantProductPopulator<SOURCE, TARGET> {

    /* 
     * Populate All TargetSizeVariantProductModel data
     */
    @Override
    public void populate(final ProductModel source, final ProductData target) throws ConversionException {
        if (target instanceof TargetProductData && source instanceof TargetSizeVariantProductModel) {
            final TargetProductData targetData = (TargetProductData)target;
            final TargetSizeVariantProductModel sourceModel = (TargetSizeVariantProductModel)source;

            targetData.setName(sourceModel.getDisplayName()); // override name for size variant

            targetData.setSizeVariant(true);
            targetData.setSellableVariantDisplayCode(sourceModel.getCode());
            targetData.setDisplayOnly(BooleanUtils.isTrue(sourceModel.getDisplayOnly()));

            if (sourceModel.getBaseProduct() instanceof TargetColourVariantProductModel) {
                final TargetColourVariantProductModel colourVariantSource = (TargetColourVariantProductModel)sourceModel
                        .getBaseProduct();
                targetData.setColourVariantCode(colourVariantSource.getCode());
                targetData.setPromotionStatuses(getPromotionStatuses(colourVariantSource));
                targetData.setOnlineExclusive(colourVariantSource.getOnlineExclusive().booleanValue());
                targetData.setAssorted(colourVariantSource.isAssorted());
                if (BooleanUtils.isTrue(colourVariantSource.getNewLowerPriceFlag())) {
                    targetData.setNewLowerPriceStartDate(colourVariantSource.getNewLowerPriceStartDate());
                }
                targetData.setExcludeForAfterpay(colourVariantSource.isExcludeForAfterpay());
                targetData.setExcludeForZipPayment(colourVariantSource.isExcludeForZipPayment());
            }

            final TargetProductModel baseProductModel = getProductFacade().getBaseTargetProduct(sourceModel);
            if (baseProductModel != null) {
                targetData.setBaseProductCode(baseProductModel.getCode());
                targetData.setShowStoreStockForProduct(BooleanUtils.isTrue(baseProductModel.getShowStoreStock()));
            }
        }
    }

    /**
     * Lookup for TargetProductFacade
     * 
     * @return the productFacade
     */
    public TargetProductFacade getProductFacade() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getProductFacade().");
    }


}
