/**
 * 
 */
package au.com.target.endeca.infront.cartridge;

import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.RefinementMenuConfig;


/**
 * @author pvarghe2
 *
 */
public class TargetRefinementMenuConfig extends RefinementMenuConfig {

    private static final String SEO_FOLLOW_ENABLED = "seoFollowEnabled";

    public TargetRefinementMenuConfig() {
        //default constructor
    }

    public TargetRefinementMenuConfig(final String pType) {
        super(pType);
    }

    public TargetRefinementMenuConfig(final ContentItem contentItem) {
        super(contentItem);
    }

    public boolean isSeoFollowEnabled() {
        return getBooleanProperty(SEO_FOLLOW_ENABLED, false);
    }

    public void setSeoFollowEnabled(final boolean seoFollowEnabled) {
        put(SEO_FOLLOW_ENABLED, Boolean.valueOf(seoFollowEnabled));
    }


}
