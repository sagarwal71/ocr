/**
 * 
 */
package au.com.target.endeca.infront.converter.populator;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.data.EndecaProduct;
import au.com.target.tgtcore.product.data.ProductDisplayType;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.url.impl.TargetProductDataUrlResolver;



/**
 * @author pthoma20
 * 
 */
public class TargetProductListerDataVariantPopulator<SOURCE extends EndecaProduct, TARGET extends TargetProductListerData>
        implements Populator<EndecaProduct, TargetProductListerData> {

    private TargetProductDataUrlResolver targetProductDataUrlResolver;
    private Converter<StockLevelStatus, StockData> stockLevelStatusConverter;

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final EndecaProduct source, final TargetProductListerData target) throws ConversionException {

        //set the stock levels to out of stock by default.
        target.setStock(stockLevelStatusConverter.convert(StockLevelStatus.OUTOFSTOCK));
        target.setInStock(false);

        final List<String> colourVariantCodesProcessed = new ArrayList<>();

        final List<TargetVariantProductListerData> targetVariantProductListerDataList = new ArrayList<>();

        for (final EndecaProduct endecaVariantProduct : source.getProduct()) {

            //TODO: Remove this temp fix until colour varaint code is obtained.
            if (endecaVariantProduct.getColourVariantCode() == null) {
                endecaVariantProduct.setColourVariantCode(target.getCode() + "_"
                        + endecaVariantProduct.getColour());
            }
            //temp fix end


            if (colourVariantCodesProcessed.contains(endecaVariantProduct.getColourVariantCode())) {
                continue;
            }
            colourVariantCodesProcessed.add(endecaVariantProduct.getColourVariantCode());

            final TargetVariantProductListerData targetVariantProductListerData = new TargetVariantProductListerData();

            targetVariantProductListerData.setColourVariantCode(endecaVariantProduct.getColourVariantCode());
            targetVariantProductListerData.setSwatchColour(endecaVariantProduct.getSwatchColour());

            targetVariantProductListerData.setColourName(endecaVariantProduct.getColour());
            targetVariantProductListerData.setGridImageUrls(endecaVariantProduct.getGridImage());

            targetVariantProductListerData.setInStock(false);

            //TODO: This needs to be changed once the data ingestion is correct.
            if (endecaVariantProduct.getInStockFlag() != null
                    && (endecaVariantProduct.getInStockFlag().equals("1") || endecaVariantProduct.getInStockFlag()
                            .equals("instock"))) {
                targetVariantProductListerData.setInStock(true);
            }
            targetVariantProductListerData.setGridImageUrls(endecaVariantProduct.getGridImage());
            targetVariantProductListerData.setHeroImageUrls(endecaVariantProduct.getHeroImage());
            targetVariantProductListerData.setListImageUrls(endecaVariantProduct.getListImage());
            targetVariantProductListerData.setSwatchImageUrls(endecaVariantProduct.getSwatchImage());
            targetVariantProductListerData.setThumbImageUrl(endecaVariantProduct.getThumbImage());

            targetVariantProductListerData.setShowWhenOutOfStock(false);
            if (endecaVariantProduct.getShowwhenoutofstock() != null) {
                targetVariantProductListerData.setShowWhenOutOfStock(BooleanUtils.toBoolean(
                        endecaVariantProduct.getShowwhenoutofstock(),
                        "1", "0"));
            }

            targetVariantProductListerData.setUrl(targetProductDataUrlResolver.resolve(target.getName(),
                    targetVariantProductListerData.getColourVariantCode()));

            //Check if product inStock Flag is not set to true and if child is instock then set product as instock.
            // This is to handle the logic where even if one sellable variant is instock then the product should be instock
            if (!target.isInStock() && targetVariantProductListerData.isInStock()) {
                target.setInStock(true);
                target.setStock(stockLevelStatusConverter.convert(StockLevelStatus.INSTOCK));
            }
            targetVariantProductListerData.setAssorted(endecaVariantProduct.getAssorted());
            targetVariantProductListerData
                    .setDisplayOnly(BooleanUtils.toBoolean(isDisplayOnly(source.getProduct())));

            targetVariantProductListerData
                    .setProductDisplayType(endecaVariantProduct.getProductDisplayType() != null
                            ? ProductDisplayType.valueOf(endecaVariantProduct.getProductDisplayType()) : null);
            targetVariantProductListerData.setNormalSaleStartDate(endecaVariantProduct.getNormalSaleStartDateTime());
            targetVariantProductListerDataList.add(targetVariantProductListerData);

        }

        target.setTargetVariantProductListerData(targetVariantProductListerDataList);
    }

    public Boolean isDisplayOnly(final List<EndecaProduct> list) {
        Boolean isDisplayOnly = null;
        if (CollectionUtils.isNotEmpty(list)) {
            for (final EndecaProduct endecaVariantProduct : list) {
                isDisplayOnly = endecaVariantProduct.getDisplayOnlyFlag();
                if (BooleanUtils.isFalse(isDisplayOnly)) {
                    break;
                }
            }
        }
        return isDisplayOnly;
    }

    /**
     * @param targetProductDataUrlResolver
     *            the targetProductDataUrlResolver to set
     */
    @Required
    public void setTargetProductDataUrlResolver(final TargetProductDataUrlResolver targetProductDataUrlResolver) {
        this.targetProductDataUrlResolver = targetProductDataUrlResolver;
    }

    /**
     * @param stockLevelStatusConverter
     *            the stockLevelStatusConverter to set
     */
    @Required
    public void setStockLevelStatusConverter(final Converter<StockLevelStatus, StockData> stockLevelStatusConverter) {
        this.stockLevelStatusConverter = stockLevelStatusConverter;
    }

}
