/**
 * 
 */
package au.com.target.endeca.infront.data;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;


/**
 * @author rsamuel3
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class EndecaDimension {

    @XmlElement
    private String name;

    @XmlElement
    private String code;

    @XmlElement
    private boolean multiSelect;

    private boolean selected;

    @XmlElement
    private String navigationState;

    @XmlElement
    private String dimensionName;

    @XmlElement
    private String count;

    @XmlElement
    private boolean seoFollowEnabled;

    private boolean actualFollowup;

    @XmlElement
    @XmlElementWrapper(name = "refinements")
    private List<EndecaDimension> refinement;

    /**
     * @return the seoFollowEnabled
     */
    public boolean isSeoFollowEnabled() {
        return seoFollowEnabled;
    }

    /**
     * @param seoFollowEnabled
     *            the seoFollowEnabled to set
     */
    public void setSeoFollowEnabled(final boolean seoFollowEnabled) {
        this.seoFollowEnabled = seoFollowEnabled;
    }


    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the multiSelect
     */
    public boolean isMultiSelect() {
        return multiSelect;
    }

    /**
     * @param multiSelect
     *            the multiSelect to set
     */
    public void setMultiSelect(final boolean multiSelect) {
        this.multiSelect = multiSelect;
    }

    /**
     * @return the selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * @param selected
     *            the selected to set
     */
    public void setSelected(final boolean selected) {
        this.selected = selected;
    }

    /**
     * @return the refinement
     */
    public List<EndecaDimension> getRefinement() {
        return refinement;
    }

    /**
     * @param refinement
     *            the refinement to set
     */
    public void setRefinement(final List<EndecaDimension> refinement) {
        this.refinement = refinement;
    }

    /**
     * @return the count
     */
    public String getCount() {
        return count;
    }

    /**
     * @param count
     *            the count to set
     */
    public void setCount(final String count) {
        this.count = count;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the dimensionName
     */
    public String getDimensionName() {
        return dimensionName;
    }

    /**
     * @param dimensionName
     *            the dimensionName to set
     */
    public void setDimensionName(final String dimensionName) {
        this.dimensionName = dimensionName;
    }

    /**
     * @return the navigationState
     */
    public String getNavigationState() {
        return navigationState;
    }

    /**
     * @param navigationState
     *            the navigationState to set
     */
    public void setNavigationState(final String navigationState) {
        this.navigationState = navigationState;
    }

    /**
     * @return the actualFollowup
     */
    public boolean isActualFollowup() {
        return actualFollowup;
    }

    /**
     * @param actualFollowup
     *            the actualFollowup to set
     */
    public void setActualFollowup(final boolean actualFollowup) {
        this.actualFollowup = actualFollowup;
    }


}
