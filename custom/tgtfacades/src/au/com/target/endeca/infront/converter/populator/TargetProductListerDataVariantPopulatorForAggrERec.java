/**
 * 
 */
package au.com.target.endeca.infront.converter.populator;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaProduct;
import au.com.target.tgtcore.product.data.ProductDisplayType;
import au.com.target.tgtcore.stock.TargetStockLevelStatusStrategy;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetSelectedVariantData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.url.impl.TargetProductDataUrlResolver;
import au.com.target.tgtfacades.util.TargetPriceHelper;
import au.com.target.tgtutility.util.TargetDateUtil;

import com.endeca.navigation.AggrERec;
import com.endeca.navigation.AssocDimLocations;
import com.endeca.navigation.AssocDimLocationsList;
import com.endeca.navigation.DimLocation;
import com.endeca.navigation.ERec;
import com.endeca.navigation.ERecList;
import com.endeca.navigation.PropertyMap;



/**
 * Populate product with variant information.
 * 
 * @author jjayawa1
 * 
 */
public class TargetProductListerDataVariantPopulatorForAggrERec<SOURCE extends AggrERec, TARGET extends TargetProductListerData>
        implements EndecaComponentPopulator<SOURCE, TARGET> {
    private static final String STR_NO_COLOUR = "No Colour";

    private TargetProductDataUrlResolver targetProductDataUrlResolver;
    private Converter<StockLevelStatus, StockData> stockLevelStatusConverter;
    private TargetStockLevelStatusStrategy onlineStocklevelStatusStrategy;
    private TargetPriceHelper targetPriceHelper;

    @Override
    public void populate(final AggrERec source, final TargetProductListerData target, final String selectedVariant)
            throws ConversionException {


        //set the stock levels to out of stock by default.
        int childRecordsCount = 0;
        //stock should be based on availableQty
        target.setStock(getStockData(StockLevelStatus.OUTOFSTOCK));
        //inStock flag is based on inStockFlag in endeca
        target.setInStock(false);
        final List<String> colourVariantCodesProcessed = new ArrayList<>();
        final List<TargetVariantProductListerData> targetVariantProductListerDataList = new ArrayList<>();
        String colourVariantCode = null;
        String sizeVariantCode = null;
        String onlyVariantCode = null;
        boolean displayOnlyFlag = true;
        //To populate the dimension values ,set here the dimension names
        final List<String> dimensionKeys = new ArrayList<>();
        dimensionKeys.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR);
        dimensionKeys.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SWATCH_COLOUR);
        dimensionKeys.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE);
        final ERecList childRecords = source.getERecs();
        if (CollectionUtils.isEmpty(childRecords)) {
            return;
        }
        childRecordsCount = childRecords.size();
        for (final Iterator<ERec> it = childRecords.iterator(); it.hasNext();) {
            final ERec record = it.next();
            final PropertyMap representativeRecordProperties = record.getProperties();
            final EndecaProduct endecaVariantProduct = new EndecaProduct();
            final Map<String, String> dimValues = getDimensionValuesForDimensions(record, dimensionKeys);
            if (displayOnlyFlag) {
                displayOnlyFlag = calculateDisplayOnlyFlag(representativeRecordProperties);
            }
            populateStockOnBaseProductLevel(target, representativeRecordProperties);
            if (null != representativeRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE)) {
                colourVariantCode = (String)representativeRecordProperties
                        .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
            }
            if (null != representativeRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE)) {
                sizeVariantCode = (String)representativeRecordProperties
                        .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE);
            }
            //populate the onlyVariant code,if there is only one variant and the selected variant is empty
            if (childRecordsCount == 1 && StringUtils.isEmpty(selectedVariant)) {
                if (StringUtils.isNotEmpty(sizeVariantCode)) {
                    onlyVariantCode = sizeVariantCode;
                }
                else {
                    onlyVariantCode = colourVariantCode;
                }
            }
            if (representativeRecordProperties
                    .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_COUNT)) {
                target.setColourVariantCount(populatePropertyIntValue(representativeRecordProperties,
                        EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_COUNT));
            }

            if (StringUtils.isNotEmpty(selectedVariant) && representativeRecordProperties
                    .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE)) {
                if (selectedVariant.equals(
                        colourVariantCode) || selectedVariant.equals(
                        sizeVariantCode)) {
                    endecaVariantProduct.setColourVariantCode(colourVariantCode);
                    target.setCode(selectedVariant);
                    target.setUrl(targetProductDataUrlResolver.resolve(target.getName(),
                            selectedVariant));
                }
                else {
                    continue;
                }
            }
            else {
                if (representativeRecordProperties
                        .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE)) {
                    endecaVariantProduct.setColourVariantCode((String)representativeRecordProperties
                            .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE));
                }
            }
            if (colourVariantCodesProcessed.contains(endecaVariantProduct.getColourVariantCode())) {
                continue;
            }
            colourVariantCodesProcessed.add(endecaVariantProduct.getColourVariantCode());
            final TargetVariantProductListerData targetVariantProductListerData = populateColourVariant(
                    record, endecaVariantProduct, target);
            if (null != targetVariantProductListerData && null != dimValues) {
                targetVariantProductListerData.setColourName(dimValues
                        .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR));
                targetVariantProductListerData.setSwatchColour(dimValues
                        .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SWATCH_COLOUR));
            }
            //Check if product inStock Flag is not set to true and if child is instock then set product as instock.
            // This is to handle the logic where even if one sellable variant is instock then the product should be instock
            if (!target.isInStock() && null != targetVariantProductListerData
                    && targetVariantProductListerData.isInStock()) {
                target.setInStock(true);
            }
            if (StringUtils.isNotEmpty(selectedVariant)) {
                final TargetSelectedVariantData selectedVariantData = populateSelectedVariantData(
                        representativeRecordProperties, selectedVariant, target.getName(), dimValues);
                target.setSelectedVariantData(selectedVariantData);
            }
            //Selected variant needs to be populated for baseproduct which has only one record
            if (StringUtils.isNotEmpty(onlyVariantCode)) {
                final TargetSelectedVariantData selectedVariantData = populateSelectedVariantData(
                        representativeRecordProperties, onlyVariantCode, target.getName(), dimValues);
                target.setSelectedVariantData(selectedVariantData);
            }
            targetVariantProductListerDataList.add(targetVariantProductListerData);
        }
        if (childRecordsCount > 1) {
            target.setOtherVariantsAvailable(true);
        }
        //only if all variants' display only flag is true, then display only flag is true
        target.setDisplayOnly(displayOnlyFlag);
        target.setTargetVariantProductListerData(targetVariantProductListerDataList);
    }

    /**
     * Populates the dimension values based on the list which has dimension keys
     * 
     * @param record
     * @param dimensionKeys
     * @return Map
     */
    protected Map<String, String> getDimensionValuesForDimensions(final ERec record, final List<String> dimensionKeys) {
        final Map<String, String> dimensionKeyValueMap = new HashMap<String, String>();
        final AssocDimLocationsList dimensionLocationList = record.getDimValues();
        if (CollectionUtils.isEmpty(dimensionKeys) || CollectionUtils.isEmpty(dimensionLocationList)) {
            return null;
        }
        for (final Iterator<AssocDimLocations> dimIt = dimensionLocationList.iterator(); dimIt.hasNext();) {
            final AssocDimLocations dimLoc = dimIt.next();
            final String dimensionKey = dimLoc.getDimRoot().getDimensionName();
            if (dimensionKeys.contains(dimensionKey)) {
                dimensionKeyValueMap.put(dimensionKey, ((DimLocation)dimLoc.get(0)).getDimValue().getName());
            }

        }
        return dimensionKeyValueMap;
    }

    /**
     * 
     * @param repRecord
     * @param selectedVariant
     * @return TargetSelectedVariantData
     */
    protected TargetSelectedVariantData populateSelectedVariantData(final PropertyMap repRecord,
            final String selectedVariant, final String productName, final Map<String, String> dimKeyValues) {

        TargetSelectedVariantData selectedVariantData = null;
        if (null == selectedVariant) {
            return selectedVariantData;
        }
        String sizeVariantCode = null;
        String colorVariantCode = null;
        Double price = null;
        if (repRecord.get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRICE) != null) {
            price = new Double((String)repRecord
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRICE));
        }

        Double wasPrice = null;
        if (repRecord.get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_WASPRICE) != null) {
            wasPrice = new Double((String)repRecord
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_WASPRICE));
        }

        if ((StringUtils.isNotEmpty(selectedVariant) && (repRecord
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE) || repRecord
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE)))) {
            if (null != repRecord
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE)) {
                sizeVariantCode = (String)repRecord
                        .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE);
            }
            if (null != repRecord
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE)) {
                colorVariantCode = (String)repRecord
                        .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
            }
            if (selectedVariant.equalsIgnoreCase(sizeVariantCode)) {
                selectedVariantData = populateCommonSelectedVariantData(repRecord, selectedVariant, productName,
                        dimKeyValues);
                if (null != dimKeyValues) {
                    selectedVariantData.setSize(dimKeyValues
                            .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE));
                }
                selectedVariantData.setAvailableQty(populatePropertyIntValue(repRecord,
                        EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY));
                selectedVariantData.setSellableVariant(true);
            }
            else if (selectedVariant.equalsIgnoreCase(colorVariantCode)) {
                selectedVariantData = populateCommonSelectedVariantData(repRecord, selectedVariant, productName,
                        dimKeyValues);
                if (StringUtils.isEmpty(sizeVariantCode)) {
                    selectedVariantData.setSellableVariant(true);
                    selectedVariantData.setAvailableQty(populatePropertyIntValue(repRecord,
                            EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY));
                }
            }
        }
        if (price != null && selectedVariantData != null && selectedVariantData.isSellableVariant()) {
            selectedVariantData.setPrice(targetPriceHelper.createPriceData(price));
            if (wasPrice != null) {
                selectedVariantData.setWasPrice(targetPriceHelper.createPriceData(wasPrice));
            }
        }
        if (selectedVariantData != null
                && repRecord.containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_ASSORTED)) {
            final String assorted = (String)repRecord.get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_ASSORTED);
            selectedVariantData.setAssorted(EndecaConstants.ENDECA_BOOLEAN_VALUE.equals(assorted));
        }
        return selectedVariantData;

    }

    private TargetVariantProductListerData populateColourVariant(final ERec record,
            final EndecaProduct endecaVariantProduct, final TargetProductListerData target) {

        final PropertyMap representativeRecordProperties = record.getProperties();

        final TargetVariantProductListerData targetVariantProductListerData = new TargetVariantProductListerData();

        targetVariantProductListerData.setColourVariantCode(endecaVariantProduct.getColourVariantCode());

        if (representativeRecordProperties.containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_IMG_LARGE)) {
            targetVariantProductListerData.setLargeImageUrls((List<String>)representativeRecordProperties
                    .getValues(EndecaConstants.EndecaRecordSpecificFields.ENDECA_IMG_LARGE));
        }

        if (representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_COUNT)) {
            targetVariantProductListerData.setSizeVariantCount(populatePropertyIntValue(representativeRecordProperties,
                    EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_COUNT));
        }
        if (representativeRecordProperties.containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_IMG_GRID)) {
            targetVariantProductListerData.setGridImageUrls((List<String>)representativeRecordProperties
                    .getValues(EndecaConstants.EndecaRecordSpecificFields.ENDECA_IMG_GRID));
        }

        if (representativeRecordProperties.containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_IMG_HERO)) {
            targetVariantProductListerData.setHeroImageUrls((List<String>)representativeRecordProperties
                    .getValues(EndecaConstants.EndecaRecordSpecificFields.ENDECA_IMG_HERO));
        }

        if (representativeRecordProperties.containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_IMG_LIST)) {
            targetVariantProductListerData.setListImageUrls((List<String>)representativeRecordProperties
                    .getValues(EndecaConstants.EndecaRecordSpecificFields.ENDECA_IMG_LIST));
        }

        if (representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_IMG_SWATCH)) {
            targetVariantProductListerData.setSwatchImageUrls((List<String>)representativeRecordProperties
                    .getValues(EndecaConstants.EndecaRecordSpecificFields.ENDECA_IMG_SWATCH));
        }

        if (representativeRecordProperties.containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_IMG_THUMB)) {
            targetVariantProductListerData.setThumbImageUrl((List<String>)representativeRecordProperties
                    .getValues(EndecaConstants.EndecaRecordSpecificFields.ENDECA_IMG_THUMB));
        }

        targetVariantProductListerData.setShowWhenOutOfStock(false);
        if (representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SHOW_WHEN_OUT_OF_STOCK)) {
            targetVariantProductListerData.setShowWhenOutOfStock(BooleanUtils
                    .toBoolean((String)representativeRecordProperties
                            .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SHOW_WHEN_OUT_OF_STOCK), "1",
                            "0"));
        }

        if (representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_INSTOCK_FLAG)) {
            final String inStockFlag = (String)representativeRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_INSTOCK_FLAG);
            if (inStockFlag.equals("1") || inStockFlag.equals("instock")) {
                targetVariantProductListerData.setInStock(true);
            }
        }

        if (representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_ASSORTED)) {
            final String assorted = (String)representativeRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_ASSORTED);
            targetVariantProductListerData.setAssorted(Boolean.valueOf(EndecaConstants.ENDECA_BOOLEAN_VALUE
                    .equals(assorted)));
        }

        if (representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_NORMAL_SALE_START_DATE_TIME)) {
            final String normalSaleDateStr = (String)representativeRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_NORMAL_SALE_START_DATE_TIME);
            targetVariantProductListerData.setNormalSaleStartDate(TargetDateUtil.getStringAsDate(normalSaleDateStr,
                    "yyyy-MM-dd HH:mm:ss.S"));
        }

        if (representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_DISPLAY_TYPE)) {
            final String prdDisplayType = (String)representativeRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_DISPLAY_TYPE);
            targetVariantProductListerData.setProductDisplayType(ProductDisplayType.valueOf(prdDisplayType));
        }

        if (representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DISPLAYONLY_FLAG)) {
            final String displayOnly = (String)representativeRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DISPLAYONLY_FLAG);
            targetVariantProductListerData.setDisplayOnly(EndecaConstants.ENDECA_BOOLEAN_VALUE
                    .equals(displayOnly));
        }
        targetVariantProductListerData.setUrl(targetProductDataUrlResolver.resolve(target.getName(),
                targetVariantProductListerData.getColourVariantCode()));
        return targetVariantProductListerData;
    }

    private StockData getStockData(final StockLevelStatus stockLevelStatus) {
        return stockLevelStatusConverter.convert(stockLevelStatus);
    }

    private void populateStockOnBaseProductLevel(final TargetProductListerData target,
            final PropertyMap representativeRecordProperties) {
        //below makes sure the base product's stock will be OUTOFSTOCK only when all variants are out of stock
        //at the moment, it doesn't matter whether it is high or low
        if (StockLevelStatus.OUTOFSTOCK == target.getStock().getStockLevelStatus() && representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY)) {
            final String availableQty = (String)representativeRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY);
            target.setStock(getStockData(onlineStocklevelStatusStrategy.checkStatus(availableQty)));
        }
    }

    private boolean calculateDisplayOnlyFlag(final PropertyMap representativeRecordProperties) {
        if (representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DISPLAYONLY_FLAG)) {
            final String displayOnlyFlagStr = (String)representativeRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DISPLAYONLY_FLAG);
            if (!StringUtils.equals(EndecaConstants.ENDECA_BOOLEAN_VALUE, displayOnlyFlagStr)) {
                return false;
            }
        }
        return true;
    }



    /**
     * Populates common attributes for both size and color variant
     * 
     * @param repRecord
     * @param selectedVariant
     * @param prdName
     * @return TargetSelectedVariantData
     */
    private TargetSelectedVariantData populateCommonSelectedVariantData(final PropertyMap repRecord,
            final String selectedVariant,
            final String prdName, final Map<String, String> dimKeyValues) {
        final TargetSelectedVariantData selectedVariantData = new TargetSelectedVariantData();
        selectedVariantData.setProductCode(selectedVariant);
        selectedVariantData.setUrl(targetProductDataUrlResolver.resolve(prdName,
                selectedVariant));
        if (null != dimKeyValues) {
            final String swatchColour = dimKeyValues
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SWATCH_COLOUR);
            if (STR_NO_COLOUR.equalsIgnoreCase(swatchColour)) {
                selectedVariantData.setColor(StringUtils.EMPTY);
                return selectedVariantData;
            }
            selectedVariantData.setColor(dimKeyValues
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SWATCH_COLOUR));
        }
        return selectedVariantData;


    }

    /**
     * Populates the int value of the property
     * 
     * @param repRecord
     * @return int
     */
    private int populatePropertyIntValue(final PropertyMap repRecord, final String propertyName) {
        int propertyIntValue = 0;
        if (null != repRecord.get(propertyName)) {
            final String propertyStrValue = (String)repRecord
                    .get(propertyName);
            propertyIntValue = StringUtils.isNotBlank(propertyStrValue) ? Integer.parseInt(propertyStrValue) : 0;
        }
        return propertyIntValue;
    }


    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final SOURCE source, final TARGET target) throws ConversionException {
        populate(source, target, null);
    }

    /**
     * @param targetProductDataUrlResolver
     *            the targetProductDataUrlResolver to set
     */
    @Required
    public void setTargetProductDataUrlResolver(final TargetProductDataUrlResolver targetProductDataUrlResolver) {
        this.targetProductDataUrlResolver = targetProductDataUrlResolver;
    }

    /**
     * @param stockLevelStatusConverter
     *            the stockLevelStatusConverter to set
     */
    @Required
    public void setStockLevelStatusConverter(final Converter<StockLevelStatus, StockData> stockLevelStatusConverter) {
        this.stockLevelStatusConverter = stockLevelStatusConverter;
    }

    /**
     * @param targetPriceHelper
     *            the targetPriceHelper to set
     */
    @Required
    public void setTargetPriceHelper(final TargetPriceHelper targetPriceHelper) {
        this.targetPriceHelper = targetPriceHelper;
    }

    /**
     * @param onlineStocklevelStatusStrategy
     *            the onlineStocklevelStatusStrategy to set
     */
    @Required
    public void setOnlineStocklevelStatusStrategy(final TargetStockLevelStatusStrategy onlineStocklevelStatusStrategy) {
        this.onlineStocklevelStatusStrategy = onlineStocklevelStatusStrategy;
    }

}
