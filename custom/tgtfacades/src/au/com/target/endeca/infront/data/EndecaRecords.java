/**
 * 
 */
package au.com.target.endeca.infront.data;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author rsamuel3
 * 
 */
@XmlRootElement(name = "Records")
@XmlAccessorType(XmlAccessType.FIELD)
public class EndecaRecords {
    @XmlElement
    private String totalCount;

    @XmlElement
    @XmlElementWrapper(name = "sortOptions")
    private List<EndecaOptions> option;

    @XmlElement
    private String recordsPerPage;

    @XmlElement
    private String currentNavState;

    @XmlElement
    @XmlElementWrapper(name = "resultList")
    private List<EndecaProduct> product;

    /**
     * @return the totalCount
     */
    public String getTotalCount() {
        return totalCount;
    }

    /**
     * @param totalCount
     *            the totalCount to set
     */
    public void setTotalCount(final String totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * @return the recordsPerPage
     */
    public String getRecordsPerPage() {
        return recordsPerPage;
    }

    /**
     * @param recordsPerPage
     *            the recordsPerPage to set
     */
    public void setRecordsPerPage(final String recordsPerPage) {
        this.recordsPerPage = recordsPerPage;
    }

    /**
     * @return the option
     */
    public List<EndecaOptions> getOption() {
        return option;
    }

    /**
     * @param option
     *            the option to set
     */
    public void setOption(final List<EndecaOptions> option) {
        this.option = option;
    }

    /**
     * @return the product
     */
    public List<EndecaProduct> getProduct() {
        return product;
    }

    /**
     * @param product
     *            the product to set
     */
    public void setProduct(final List<EndecaProduct> product) {
        this.product = product;
    }

    /**
     * @return the currentNavState
     */
    public String getCurrentNavState() {
        return currentNavState;
    }

    /**
     * @param currentNavState
     *            the currentNavState to set
     */
    public void setCurrentNavState(final String currentNavState) {
        this.currentNavState = currentNavState;
    }
}
