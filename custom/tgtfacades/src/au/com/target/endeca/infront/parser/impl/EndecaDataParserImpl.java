/**
 * 
 */
package au.com.target.endeca.infront.parser.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.converter.EndecaContentItemConverter;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.endeca.infront.parser.EndecaDataParser;

import com.endeca.infront.assembler.ContentItem;


/**
 * @author rsamuel3
 * 
 */
public class EndecaDataParserImpl implements EndecaDataParser {


    private static final Logger LOG = Logger.getLogger(EndecaDataParserImpl.class);
    private Map<String, EndecaContentItemConverter> endecaContentItemConverters;

    @Override
    public EndecaSearch convertSourceToSearchObjects(final List<ContentItem> list) {
        EndecaSearch search = null;
        if (CollectionUtils.isNotEmpty(list)) {
            for (final ContentItem item : list) {
                final long startTime = System.currentTimeMillis();
                search = new EndecaSearch();
                populateData(item, search);
                final long endTime = System.currentTimeMillis();
                LOG.info("Time taken for transformation of each item:" + (endTime - startTime)
                        + "ms");
            }
        }
        return search;
    }

    /**
     * @param item
     * @param search
     */
    private void populateData(final ContentItem item, final EndecaSearch search) {
        if (MapUtils.isNotEmpty(endecaContentItemConverters)) {
            for (final String key : endecaContentItemConverters.keySet()) {
                if (item.containsKey(key)) {
                    endecaContentItemConverters.get(key).convertContentItem(item, search);
                }
            }
        }


    }

    /**
     * @param endecaContentItemConverters
     *            the endecaContentItemConverters to set
     */
    @Required
    public void setEndecaContentItemConverters(final Map<String, EndecaContentItemConverter> endecaContentItemConverters) {
        this.endecaContentItemConverters = endecaContentItemConverters;
    }




}
