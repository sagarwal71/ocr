/**
 * 
 */
package au.com.target.endeca.infront.parser;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;

import au.com.target.endeca.infront.data.EndecaSearch;

import com.endeca.infront.assembler.ContentItem;



/**
 * Parsers for the data received from Endeca
 * 
 * @author rsamuel3
 * 
 */
public interface EndecaDataParser {
    /**
     * Takes the first element in the list to convert to Endeca Search Object
     * 
     * @param list
     * @return EndecaSearch object
     * @throws IOException
     * @throws TransformerException
     * @throws JAXBException
     */
    EndecaSearch convertSourceToSearchObjects(final List<ContentItem> list) throws JAXBException,
            TransformerException, IOException;

}
