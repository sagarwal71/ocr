/**
 * 
 */
package au.com.target.endeca.infront.querybuilder;

import au.com.target.endeca.infront.constants.EndecaConstants;

import org.apache.commons.lang.StringUtils;
import com.endeca.navigation.UrlENEQuery;
import com.endeca.navigation.UrlENEQueryParseException;


/**
 * @author rmcalave
 *
 */
public class TargetDealProductsQueryBuilder extends AbstractEndecaQueryBuilder {

    private static final String FIELDS_REQUIRED_DEAL_PRODUCTS = "tgtpublicws.dealProducts.selection.fieldlist";
    private static final int MAX_RECORDS = 1000;

    /**
     * Build an Endeca query to retrieve products that are part of the given deal category dimension.
     * 
     * @param dealCategoryDimId
     *            An Endeca dimension ID for a deal category
     * @return The Endeca query
     * @throws UrlENEQueryParseException
     */
    public UrlENEQuery getQueryForFetchingAllProductsForDealCategory(final String dealCategoryDimId)
            throws UrlENEQueryParseException {
        if (StringUtils.isBlank(dealCategoryDimId)) {
            return null;
        }

        final UrlENEQuery query = new UrlENEQuery("", EndecaConstants.CODE_LITERAL);
        query.setNr(buildRecordFilter());
        query.setN(dealCategoryDimId);
        query.setNavNumAggrERecs(MAX_RECORDS);
        query.setNavERecsPerAggrERec(Integer.parseInt(EndecaConstants.ENDECA_NP_1));
        query.setNavMerchRuleFilter("endeca.internal.nonexistent");
        query.setSelection(getFieldList(FIELDS_REQUIRED_DEAL_PRODUCTS));
        query.setNu(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE);
        return query;
    }

    private String buildRecordFilter() {
        final StringBuilder filterBuffer = new StringBuilder();

        filterBuffer.append(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_DISPLAY_FLAG);
        filterBuffer.append(EndecaConstants.EndecaFilterQuery.ENDECA_FILTER_KEY_VALUE_SEPERATOR);
        filterBuffer.append(EndecaConstants.ENDECA_BOOLEAN_VALUE);

        return filterBuffer.toString();
    }

}
