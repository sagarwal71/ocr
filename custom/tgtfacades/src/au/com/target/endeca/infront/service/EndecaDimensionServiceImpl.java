/**
 * 
 */
package au.com.target.endeca.infront.service;

import static au.com.target.tgtwebcore.constants.TgtwebcoreConstants.LOG_IDENTIFIER_REQUEST_USERAGENT;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Required;

import com.endeca.navigation.DimLocation;
import com.endeca.navigation.DimLocationList;
import com.endeca.navigation.DimVal;
import com.endeca.navigation.DimValList;
import com.endeca.navigation.DimensionSearchResultGroup;
import com.endeca.navigation.DimensionSearchResultGroupList;
import com.endeca.navigation.ENEConnection;
import com.endeca.navigation.ENEQuery;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.HttpENEConnection;
import com.endeca.navigation.MutableDimLocation;
import com.endeca.navigation.MutableDimLocationList;
import com.endeca.navigation.MutableDimVal;
import com.endeca.navigation.PropertyMap;
import com.endeca.navigation.UrlENEQuery;
import com.endeca.navigation.UrlENEQueryParseException;
import com.endeca.soleng.urlformatter.UrlFormatException;
import com.endeca.soleng.urlformatter.UrlState;

import au.com.target.endeca.infront.cache.EndecaDimensionCacheService;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaDimensions;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.soleng.urlformatter.TargetURLFormatter;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * @author smudumba
 * 
 */
public class EndecaDimensionServiceImpl implements EndecaDimensionService {

    protected static final Logger LOG = Logger.getLogger(EndecaDimensionServiceImpl.class);

    private String host;

    private String port;

    private String dimsIds;

    private String dimSearchQuery;

    private HttpENEConnection connection;

    private EndecaDimensionCacheService endecaDimensionCacheService;

    private TargetURLFormatter targetUrlFormatter;



    /*
     * Method to get Endeca Dimensions
     */
    @Override
    public EndecaDimensions getDimensions() throws ENEQueryException, TargetEndecaException {

        return initialize();

    }

    /**
     * Method to create httpenconnection
     * 
     * @return HttpENEConnection
     * @throws TargetEndecaException
     */
    private HttpENEConnection getConection() throws TargetEndecaException {


        if (null == connection) {
            if (StringUtils.isEmpty(host) || StringUtils.isEmpty(port)) {
                throw new TargetEndecaException("Connection Details Not Set");
            }
            connection = new HttpENEConnection(host, port);
        }

        return connection;
    }

    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * @param host
     *            the host to set
     */
    @Required
    public void setHost(final String host) {
        this.host = host;
    }

    /**
     * @return the port
     */

    public String getPort() {
        return port;
    }

    /**
     * @param port
     *            the port to set
     */
    @Required
    public void setPort(final String port) {
        this.port = port;

    }

    /**
     * @param query
     * @param conn
     * @return ENEQueryResults
     * @throws ENEQueryException
     * @throws UrlENEQueryParseException
     */
    @Override
    public ENEQueryResults excecuteDimensionQuery(final String query, final ENEConnection conn)
            throws ENEQueryException,
            UrlENEQueryParseException {
        final ENEQuery eneQuery = new UrlENEQuery(query, "UTF-8");


        ENEQueryResults results = null;
        try {
            results = conn.query(eneQuery);
        }
        catch (final ENEQueryException e) {
            throw e;
        }

        return results;

    }


    /**
     * Method to process results from Endeca
     * 
     * @param results
     */
    private EndecaDimensions processQueryResults(final ENEQueryResults results) {
        final EndecaDimensions endecaDimensions = new EndecaDimensions();
        final DimensionSearchResultGroupList dsrl = results.getDimensionSearch().getResults();
        for (int i = 0; i < dsrl.size(); i++) {
            final DimensionSearchResultGroup dsrg = (DimensionSearchResultGroup)dsrl.get(i);
            final String rootName = processRoots(dsrg.getRoots());
            if (StringUtils.isNotEmpty(rootName.toLowerCase())) {
                for (int j = 0; j < dsrg.size(); j++) {
                    final DimLocationList dll = (DimLocationList)dsrg.get(j);
                    for (int k = 0; k < dll.size(); k++) {
                        processDimVal(((DimLocation)dll.get(k)).getDimValue(), rootName.toLowerCase(),
                                endecaDimensions);
                    }
                }

            }
        }

        if (null != endecaDimensions.getDimensions()) {


            LOG.info("Size of the dimensions put in to cache is:" + endecaDimensions.getDimensions().size());
            final String brandKey = EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND.toLowerCase();
            if (endecaDimensions.getDimensions().containsKey(brandKey)
                    && null != endecaDimensions.getDimensions().get(brandKey)) {
                LOG.info("Size of  " + brandKey + ":"
                        + endecaDimensions.getDimensions().get(
                                EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND.toLowerCase()).size());
            }

            final String catKey = EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY.toLowerCase();
            if (endecaDimensions.getDimensions().containsKey(catKey)
                    && null != endecaDimensions.getDimensions().get(catKey)) {
                LOG.info("Size of  " + catKey + ":"
                        + endecaDimensions.getDimensions().get(catKey).size());
            }
            final String bbKey = EndecaConstants.EndecaRecordSpecificFields.BULKYBOARD.toLowerCase();
            if (endecaDimensions.getDimensions()
                    .containsKey(bbKey)
                    && null != endecaDimensions.getDimensions().get(bbKey)) {
                LOG.info("Size of  " + bbKey + ":"
                        + endecaDimensions.getDimensions().get(bbKey).size());

            }
            final String colourKey = EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR.toLowerCase();
            if (endecaDimensions.getDimensions()
                    .containsKey(colourKey)
                    && null != endecaDimensions.getDimensions().get(colourKey)) {
                LOG.info("Size of  " + colourKey + ":"
                        + endecaDimensions.getDimensions().get(colourKey).size());

            }
            final String genderKey = EndecaConstants.EndecaRecordSpecificFields.ENDECA_GENDER.toLowerCase();
            if (endecaDimensions.getDimensions()
                    .containsKey(genderKey)
                    && null != endecaDimensions.getDimensions().get(genderKey)) {
                LOG.info("Size of  " + genderKey + ":"
                        + endecaDimensions.getDimensions().get(genderKey).size());

            }
            final String psKey = EndecaConstants.EndecaRecordSpecificFields.ENDECA_PROMOTIONAL_STATUS.toLowerCase();
            if (endecaDimensions.getDimensions()
                    .containsKey(psKey)
                    && null != endecaDimensions.getDimensions().get(psKey)) {
                LOG.info("Size of  "
                        + psKey
                        + ":"
                        + endecaDimensions.getDimensions().get(psKey)
                                .size());

            }
            final String priceKey = EndecaConstants.EndecaRecordSpecificFields.ENDECA_SHOP_BY_PRICE.toLowerCase();
            if (endecaDimensions.getDimensions()
                    .containsKey(priceKey)
                    && null != endecaDimensions.getDimensions().get(priceKey)) {
                LOG.info("Size of  " + priceKey
                        + ":"
                        + endecaDimensions.getDimensions().get(priceKey).size());

            }
        }
        else {
            LOG.info("Endeca dimensions was not populated");
        }


        return endecaDimensions;

    }

    /**
     * Method to get details of the Dimension
     * 
     * @param roots
     * @return String
     */
    private String processRoots(final DimValList roots) {
        if (roots.size() != 1) {
            //if the root name is empty that category will not be processed
            return StringUtils.EMPTY;
        }
        final DimVal dimVal = roots.getDimValue(0);
        return dimVal.getDimensionName();
    }



    /**
     * Method to load the refinements for dimensions
     * 
     * @param dimVal
     * @param rootName
     */
    private void processDimVal(final DimVal dimVal, final String rootName,
            final EndecaDimensions endecaDimensions) {

        final PropertyMap pm = dimVal.getProperties();
        if (StringUtils.isNotEmpty(rootName)) {
            //if null intialize
            if (null == endecaDimensions.getDimensions()) {
                endecaDimensions.setDimensions(new HashMap<String, Map<String, String>>());
            }
            //if the key does not exist create a new one
            if (!(endecaDimensions.getDimensions().containsKey(rootName))) {
                endecaDimensions.getDimensions().put(rootName, new HashMap<String, String>());
            }
            //use the code value as key for categories
            if (EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY.equalsIgnoreCase(rootName)) {
                endecaDimensions.getDimensions().get(rootName)
                        .put((null != pm.get("code") ? pm.get("code").toString().toLowerCase() : ""),
                                Long.toString(dimVal.getId()));
            }
            else {

                endecaDimensions.getDimensions().get(rootName)
                        .put(dimVal.getName().toLowerCase(), Long.toString(dimVal.getId()));

            }
        }


    }


    /*
     * Method constructs Dimension query stirng
     */
    private String buildQuery() {
        final StringBuilder sb = new StringBuilder();
        String result = StringUtils.EMPTY;
        if (StringUtils.isNotEmpty(dimsIds)) {
            final String[] ids = dimsIds.split("\\|");
            if (null != ids) {
                for (final String id : ids) {
                    sb.append(id).append(EndecaConstants.QUERY_PARAM_SEPERATOR_PLUS);
                }
            }
        }
        result = sb.toString();

        if (StringUtils.isNotEmpty(result)) {
            //remove the additonal + sign
            LOG.debug("Query executed is " + result);
            return result.substring(0, result.length() - 1);
        }
        else {
            LOG.debug("Query executed is empty " + result);
            return result;
        }


    }

    /*
     * Method builds the EndecaDimensions Object.
     * 
     */
    private EndecaDimensions initialize() throws ENEQueryException, TargetEndecaException {


        ENEQueryResults results = null;
        final String query = buildQuery();
        try {
            results = excecuteDimensionQuery(this.dimSearchQuery + query, getConection());
        }
        catch (final ENEQueryException e) {
            throw e;
        }
        catch (final TargetEndecaException te) {
            throw te;
        }

        if (null != results) {
            return processQueryResults(results);
        }
        return null;

    }

    /**
     * This method calls the assembler dimension search for getting the navigation state for corresponding category
     * 
     * @param dimensionIdMap
     * @return java.lang.String
     */
    @Override
    public String getDimensionNavigationState(final Map<String, String> dimensionIdMap) {

        String encodedNavState = "";

        final MutableDimLocationList dimLocationList = new MutableDimLocationList();

        try {
            for (final Map.Entry<String, String> entry : dimensionIdMap.entrySet()) {
                final MutableDimLocation dimLocation = new MutableDimLocation();
                final MutableDimVal dimVal = new MutableDimVal();
                final String result = endecaDimensionCacheService.getDimension(entry.getKey(), entry.getValue());
                if (StringUtils
                        .isNotEmpty(result)) {
                    dimVal.setDimValId(Long.parseLong(result));
                    dimLocation.setDimValue(dimVal);
                    dimLocationList.appendDimLocation(dimLocation);
                    LOG.info("DIMENSION Details in the Cache - for key:" + entry.getKey() + ":renfinement:"
                            + entry.getValue()
                            + ":dimension:" + result);
                }
                else {
                    LOG.error("DIMENSION_NOT_FOUND_IN_CACHE - for key " + entry.getKey() + ":" + entry.getValue()
                            + result
                            + " for useragent=" + MDC.get(LOG_IDENTIFIER_REQUEST_USERAGENT));
                }
            }
            if (CollectionUtils.isEmpty(dimLocationList)) {
                throw new TargetEndecaException(SplunkLogFormatter.formatMessage(
                        "CATEGORY CODE NOT FOUND IN CACHE FOR CATEGORY",
                        TgtutilityConstants.ErrorCode.ERR_SEARCH, ""));
            }

            encodedNavState = getEncodedNavigationState(dimLocationList);
        }
        catch (final UrlFormatException e) {
            LOG.error(SplunkLogFormatter.formatMessage(
                    "URL FORMATTER EXCEPTION",
                    TgtutilityConstants.ErrorCode.ERR_SEARCH, ""), e);
        }
        catch (final NumberFormatException e) {
            LOG.error(SplunkLogFormatter.formatMessage(
                    " Dimenstion navigation state is not a number",
                    TgtutilityConstants.ErrorCode.ERR_SEARCH, ""), e);
        }
        catch (final ENEQueryException e) {
            LOG.error(SplunkLogFormatter.formatMessage(
                    " Exception when querying endeca",
                    TgtutilityConstants.EndecaErrorCodes.ENDECA_ENE_QUERY_EXCEPTION, ""), e);
        }
        catch (final TargetEndecaException e) {
            LOG.error(
                    SplunkLogFormatter.formatMessage(
                            "CATEGORY CODE NOT FOUND IN CACHE FOR CATEGORY",
                            TgtutilityConstants.ErrorCode.ERR_SEARCH,
                            "useragent=" + (String)MDC.get(LOG_IDENTIFIER_REQUEST_USERAGENT)));
        }
        return encodedNavState;
    }

    @Override
    public String getEncodedNavigationState(final MutableDimLocationList dimLocationList) throws UrlFormatException {

        final UrlState urlState = new UrlState(targetUrlFormatter, EndecaConstants.CODE_LITERAL);
        urlState.setNavState(dimLocationList);

        String encodedNavState = targetUrlFormatter.formatUrl(urlState);
        if (StringUtils.contains(encodedNavState,
                EndecaConstants.EndecaFilterQuery.ENDECA_NAV_STATE_QUERY_PARAM_TERM)) {
            encodedNavState = StringUtils.strip(encodedNavState,
                    EndecaConstants.EndecaFilterQuery.ENDECA_NAV_STATE_QUERY_PARAM_TERM);
        }

        return encodedNavState;
    }


    /**
     * @return the dimsIds
     */
    public String getDimsIds() {
        return dimsIds;
    }

    /**
     * @param dimsIds
     *            the dimsIds to set
     */
    @Required
    public void setDimsIds(final String dimsIds) {
        this.dimsIds = dimsIds;
    }

    /**
     * @return the dimSearchQuery
     */
    public String getDimSearchQuery() {
        return dimSearchQuery;
    }

    /**
     * @param dimSearchQuery
     *            the dimSearchQuery to set
     */
    public void setDimSearchQuery(final String dimSearchQuery) {
        this.dimSearchQuery = dimSearchQuery;
    }


    /**
     * @param endecaDimensionCacheService
     *            the endecaDimensionCacheService to set
     */
    @Required
    public void setEndecaDimensionCacheService(final EndecaDimensionCacheService endecaDimensionCacheService) {
        this.endecaDimensionCacheService = endecaDimensionCacheService;
    }

    /**
     * @param targetUrlFormatter
     *            the targetUrlFormatter to set
     */
    @Required
    public void setTargetUrlFormatter(final TargetURLFormatter targetUrlFormatter) {
        this.targetUrlFormatter = targetUrlFormatter;
    }

}
