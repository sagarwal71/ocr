/**
 * 
 */
package au.com.target.endeca.infront.querybuilder;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;



/**
 * @author Paul
 * 
 */
public class EndecaQueryParamBuilder {

    private EndecaQueryParamBuilder() {
        //To prevent instantiation
    }

    /**
     * This method is responsible for calling various different methods for constructing the query parameter which needs
     * to be sent to Endeca in order to apply filters and conditions to get the appropriate data.
     * 
     * @return string
     */
    public static String constructQueryParams(final EndecaSearchStateData endecaSearchStateData) {

        final StringBuilder generateQueryParam = new StringBuilder();
        constructNavigationQueryParam(endecaSearchStateData, generateQueryParam);
        constructRecordsPerPageQueryParam(endecaSearchStateData, generateQueryParam);
        constructRecordsOffsetParam(endecaSearchStateData, generateQueryParam);
        contructSearchTermQueryParam(endecaSearchStateData, generateQueryParam);
        constructSearchFieldParam(endecaSearchStateData, generateQueryParam);
        constructSortQueryParam(endecaSearchStateData, generateQueryParam);
        constructMatchModeParam(endecaSearchStateData, generateQueryParam);
        constructViewAsParam(endecaSearchStateData, generateQueryParam);
        constructProductCodesParam(endecaSearchStateData, generateQueryParam);
        constructRecordFiltersParam(endecaSearchStateData, generateQueryParam);

        return generateQueryParam.toString();

    }

    /**
     * This method is used for generating the query String seperator between the 2 different fields.
     */
    private static void appendGeneratedQueryParamSeperator(final StringBuilder generateQueryParam) {
        if (generateQueryParam != null && generateQueryParam.length() != 0) {
            generateQueryParam.append(EndecaConstants.QUERY_PARAM_SEPERATOR_AMP);
        }
    }

    /**
     * This method is responsible for constructing the navigation query parameters
     */
    private static void constructNavigationQueryParam(final EndecaSearchStateData endecaSearchStateData,
            final StringBuilder generateQueryParam) {
        StringBuilder generatedNavState = new StringBuilder();
        if (endecaSearchStateData.getNavigationState() == null) {
            return;
        }
        for (final String navState : endecaSearchStateData.getNavigationState()) {
            if (generatedNavState.length() == 0) {
                generatedNavState = generatedNavState.append(EndecaConstants.EndecaFilterQuery.ENDECA_NAV_STATE_TERM)
                        .append(navState);
            }
            else {
                generatedNavState = generatedNavState.append(EndecaConstants.QUERY_PARAM_SEPERATOR_PLUS).append(
                        navState);
            }

        }
        if (generatedNavState.length() != 0) {
            appendGeneratedQueryParamSeperator(generateQueryParam);
            generateQueryParam.append(generatedNavState.toString());
        }


    }

    /**
     * This method is responsible for handling the records per page query param.
     */
    private static void constructRecordsPerPageQueryParam(final EndecaSearchStateData endecaSearchStateData,
            final StringBuilder generateQueryParam) {

        if (endecaSearchStateData.getItemsPerPage() != 0) {
            appendGeneratedQueryParamSeperator(generateQueryParam);
            generateQueryParam.append(EndecaConstants.EndecaFilterQuery.ENDECA_NO_OF_RECORDS
                    + endecaSearchStateData.getItemsPerPage());
        }
    }

    /**
     * This method is responsible for constructing the search term query param
     */
    private static void contructSearchTermQueryParam(final EndecaSearchStateData endecaSearchStateData,
            final StringBuilder generateQueryParam) {
        StringBuilder searchTermQuery = new StringBuilder();
        if (endecaSearchStateData.getSearchTerm() == null) {
            return;
        }
        for (final String searchTerm : endecaSearchStateData.getSearchTerm()) {
            if (searchTermQuery.length() == 0) {
                searchTermQuery = searchTermQuery.append(EndecaConstants.EndecaFilterQuery.ENDECA_SEARCH_TERM).append(
                        searchTerm);
            }
            else {
                searchTermQuery = searchTermQuery.append(EndecaConstants.QUERY_PARAM_SEPERATOR_PLUS).append(searchTerm);
            }

        }
        if (searchTermQuery.length() != 0) {
            appendGeneratedQueryParamSeperator(generateQueryParam);
            generateQueryParam.append(searchTermQuery);
        }

    }


    /**
     * This method is responsible for constructing the sort term parameter.
     */
    private static void constructSortQueryParam(final EndecaSearchStateData endecaSearchStateData,
            final StringBuilder generateQueryParam) {
        StringBuilder sortTermQuery = new StringBuilder();
        if (endecaSearchStateData.getSortStateMap() == null) {
            return;
        }

        for (final Map.Entry<String, Integer> mapEntry : (endecaSearchStateData.getSortStateMap()).entrySet()) {

            if (sortTermQuery.length() == 0) {
                sortTermQuery = sortTermQuery.append(EndecaConstants.EndecaFilterQuery.ENDECA_SORT_TERM).append(
                        mapEntry.getKey());
            }
            else {
                sortTermQuery = sortTermQuery.append(EndecaConstants.QUERY_PARAM_DOUBLE_PIPE).append(mapEntry.getKey());
            }
            if (mapEntry.getValue() != null && mapEntry.getValue().intValue() != 0) {
                sortTermQuery = sortTermQuery.append(EndecaConstants.QUERY_PARAM_SINGLE_PIPE)
                        .append(Integer.valueOf(1));
            }

        }
        if (sortTermQuery.length() != 0) {
            appendGeneratedQueryParamSeperator(generateQueryParam);
            generateQueryParam.append(sortTermQuery);
        }

    }

    /**
     * This method is responsible for constructing the search field params
     */
    private static void constructSearchFieldParam(final EndecaSearchStateData endecaSearchStateData,
            final StringBuilder generateQueryParam) {

        if (StringUtils.isNotBlank(endecaSearchStateData.getSearchField())) {
            appendGeneratedQueryParamSeperator(generateQueryParam);
            generateQueryParam.append(EndecaConstants.EndecaFilterQuery.ENDECA_SEARCH_FIELD
                    + endecaSearchStateData.getSearchField());
        }
    }

    /**
     * This method is responsible for constructing the records offset.
     */
    private static void constructRecordsOffsetParam(final EndecaSearchStateData endecaSearchStateData,
            final StringBuilder generateQueryParam) {
        if (endecaSearchStateData.getItemsPerPage() == 0 || endecaSearchStateData.getPageNumber() == 0) {
            return;
        }
        final int recordOffset = (endecaSearchStateData.getItemsPerPage() * endecaSearchStateData.getPageNumber());
        appendGeneratedQueryParamSeperator(generateQueryParam);
        generateQueryParam.append(EndecaConstants.EndecaFilterQuery.ENDECA_RECORDS_OFFSET
                + recordOffset);

    }

    /**
     * Append view as param
     */
    private static void constructViewAsParam(final EndecaSearchStateData endecaSearchStateData,
            final StringBuilder generateQueryParam) {
        if (StringUtils.isNotEmpty(endecaSearchStateData.getViewAs())) {
            appendGeneratedQueryParamSeperator(generateQueryParam);
            generateQueryParam.append(EndecaConstants.LISTING_VIEW_AS_KEY
                    + endecaSearchStateData.getViewAs());
        }
    }


    /**
     * This method is responsible for constructing the match Mode
     */
    private static void constructMatchModeParam(final EndecaSearchStateData endecaSearchStateData,
            final StringBuilder generateQueryParam) {

        if (StringUtils.isNotBlank(endecaSearchStateData.getMatchMode())) {
            appendGeneratedQueryParamSeperator(generateQueryParam);
            generateQueryParam.append(EndecaConstants.EndecaFilterQuery.ENDECA_SEARCH_MATCH_MODE
                    + endecaSearchStateData.getMatchMode());
        }
    }

    /**
     * method to append product codes for which recommendations are required.
     */
    private static void constructProductCodesParam(final EndecaSearchStateData endecaSearchStateData,
            final StringBuilder generateQueryParam) {
        if (CollectionUtils.isNotEmpty(endecaSearchStateData.getProductCodes())) {
            final StringBuilder searchTermQuery = new StringBuilder();
            searchTermQuery.append(StringUtils.join(endecaSearchStateData.getProductCodes(),
                    EndecaConstants.EndecaFilterQuery.ENDECA_MULTIPLE_FILTER_VALUES_SEPERATOR));

            appendGeneratedQueryParamSeperator(generateQueryParam);
            generateQueryParam.append(EndecaConstants.EndecaFilterQuery.ENDECA_SEARCH_PRODUCT_CODES
                    + searchTermQuery.toString());
        }
    }

    /**
     * @param endecaSearchStateData
     * @param generateQueryParam
     */
    private static void constructRecordFiltersParam(final EndecaSearchStateData endecaSearchStateData,
            final StringBuilder generateQueryParam) {

        if (MapUtils.isNotEmpty(endecaSearchStateData.getRecordFilters())) {
            final StringBuilder recordFiltersQuery = new StringBuilder();
            recordFiltersQuery.append(EndecaConstants.EndecaFilterQuery.ENDECA_RECORD_FILTERS);

            boolean multipleTerms = false;
            if (endecaSearchStateData.getRecordFilters().size() > 1) {
                recordFiltersQuery.append(EndecaConstants.EndecaFilterQuery.ENDECA_AND_FILTER_KEY)
                        .append(EndecaConstants.EndecaFilterQuery.ENDECA_OR_FILTER_START);
                multipleTerms = true;
            }

            for (final Iterator<Entry<String, String>> i = endecaSearchStateData.getRecordFilters().entrySet()
                    .iterator(); i.hasNext();) {
                final Entry recordFilter = i.next();
                recordFiltersQuery.append(recordFilter.getKey())
                        .append(EndecaConstants.EndecaFilterQuery.ENDECA_FILTER_KEY_VALUE_SEPERATOR)
                        .append(recordFilter.getValue());

                if (multipleTerms && i.hasNext()) {
                    recordFiltersQuery
                            .append(EndecaConstants.EndecaFilterQuery.ENDECA_MULTIPLE_FILTER_VALUES_SEPERATOR);
                }
            }

            if (multipleTerms) {
                recordFiltersQuery.append(EndecaConstants.EndecaFilterQuery.ENDECA_OR_FILTER_END);
            }

            appendGeneratedQueryParamSeperator(generateQueryParam);
            generateQueryParam.append(recordFiltersQuery);
        }

    }

}
