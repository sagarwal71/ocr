/**
 * 
 */
package au.com.target.tgtpaymentprovider.paynow.commands.impl;

import org.apache.commons.lang.NotImplementedException;

import au.com.target.tgtpayment.commands.TargetCaptureCommand;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;


/**
 * @author ragarwa3
 * 
 */
public class PayNowCaptureCommandImpl extends AbstractPayNowCommand implements TargetCaptureCommand {

    static final String COMMAND_CAPTURE = "capture";

    @Override
    public TargetCaptureResult perform(final TargetCaptureRequest request) {
        throw new NotImplementedException(getCommandNotImplementedMessage(COMMAND_CAPTURE));
    }

}
