/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author mjanarth
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class CardDetails {

    @XmlElement(name = "Token")
    private String token;
    @XmlElement(name = "TokenExpiry")
    private String tokenExpiry;
    @XmlElement(name = "CardType")
    private String cardType;
    @XmlElement(name = "MaskedCard")
    private String cardNumber;
    @XmlElement(name = "CardExpiry")
    private String cardExpiry;
    @XmlElement(name = "DefaultCard")
    private String isDefaultCard;
    @XmlElement(name = "BIN")
    private String bin;
    @XmlElement(name = "PromoID")
    private String promoId;

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token
     *            the token to set
     */
    public void setToken(final String token) {
        this.token = token;
    }

    /**
     * @return the cardNumber
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * @param cardNumber
     *            the cardNumber to set
     */
    public void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * @return the isDefaultCard
     */
    public String getIsDefaultCard() {
        return isDefaultCard;
    }

    /**
     * @param isDefaultCard
     *            the isDefaultCard to set
     */
    public void setIsDefaultCard(final String isDefaultCard) {
        this.isDefaultCard = isDefaultCard;
    }

    /**
     * @return the bin
     */
    public String getBin() {
        return bin;
    }

    /**
     * @param bin
     *            the bin to set
     */
    public void setBin(final String bin) {
        this.bin = bin;
    }

    /**
     * @return the cardExpiry
     */
    public String getCardExpiry() {
        return cardExpiry;
    }

    /**
     * @param cardExpiry
     *            the cardExpiry to set
     */
    public void setCardExpiry(final String cardExpiry) {
        this.cardExpiry = cardExpiry;
    }

    /**
     * @return the cardType
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * @param cardType
     *            the cardType to set
     */
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }

    /**
     * @return the tokenExpiry
     */
    public String getTokenExpiry() {
        return tokenExpiry;
    }

    /**
     * @param tokenExpiry
     *            the tokenExpiry to set
     */
    public void setTokenExpiry(final String tokenExpiry) {
        this.tokenExpiry = tokenExpiry;
    }

    /**
     * @return the promoId
     */
    public String getPromoId() {
        return promoId;
    }

    /**
     * @param promoId
     *            the promoId to set
     */
    public void setPromoId(final String promoId) {
        this.promoId = promoId;
    }

}
