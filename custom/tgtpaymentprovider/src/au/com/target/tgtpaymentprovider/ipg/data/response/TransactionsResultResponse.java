/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data.response;

import au.com.target.tgtpaymentprovider.ipg.data.CardResultsData;
import au.com.target.tgtpaymentprovider.ipg.data.SavedCardsData;



/**
 * @author mjanarth
 *
 */
public class TransactionsResultResponse {
    private String queryResult;
    private String queryError;
    private String sessionId;
    private String sessionToken;
    private String sessionKey;
    private String custReference;
    private String customerNumber;
    private String amount;
    private String transactionResult;
    //encoded string from IPG
    private String encodedCardResultsData;
    //encoded savedcards from IPG
    private String encodedSavedCardsData;
    private SavedCardsData savedCards;
    private CardResultsData cardResults;


    /**
     * @return the queryResult
     */
    public String getQueryResult() {
        return queryResult;
    }

    /**
     * @param queryResult
     *            the queryResult to set
     */
    public void setQueryResult(final String queryResult) {
        this.queryResult = queryResult;
    }

    /**
     * @return the queryError
     */
    public String getQueryError() {
        return queryError;
    }

    /**
     * @param queryError
     *            the queryError to set
     */
    public void setQueryError(final String queryError) {
        this.queryError = queryError;
    }

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId
     *            the sessionId to set
     */
    public void setSessionId(final String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @return the sessionToken
     */
    public String getSessionToken() {
        return sessionToken;
    }

    /**
     * @param sessionToken
     *            the sessionToken to set
     */
    public void setSessionToken(final String sessionToken) {
        this.sessionToken = sessionToken;
    }

    /**
     * @return the sessionKey
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * @param sessionKey
     *            the sessionKey to set
     */
    public void setSessionKey(final String sessionKey) {
        this.sessionKey = sessionKey;
    }

    /**
     * @return the custReference
     */
    public String getCustReference() {
        return custReference;
    }

    /**
     * @param custReference
     *            the custReference to set
     */
    public void setCustReference(final String custReference) {
        this.custReference = custReference;
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final String amount) {
        this.amount = amount;
    }

    /**
     * @return the transactionResult
     */
    public String getTransactionResult() {
        return transactionResult;
    }

    /**
     * @param transactionResult
     *            the transactionResult to set
     */
    public void setTransactionResult(final String transactionResult) {
        this.transactionResult = transactionResult;
    }

    /**
     * @return the encodedCardResultsData
     */
    public String getEncodedCardResultsData() {
        return encodedCardResultsData;
    }

    /**
     * @param encodedCardResultsData
     *            the encodedCardResultsData to set
     */
    public void setEncodedCardResultsData(final String encodedCardResultsData) {
        this.encodedCardResultsData = encodedCardResultsData;
    }

    /**
     * @return the cardResults
     */
    public CardResultsData getCardResults() {
        return cardResults;
    }

    /**
     * @param cardResults
     *            the cardResults to set
     */
    public void setCardResults(final CardResultsData cardResults) {
        this.cardResults = cardResults;
    }

    /**
     * @return the customerNumber
     */
    public String getCustomerNumber() {
        return customerNumber;
    }

    /**
     * @param customerNumber
     *            the customerNumber to set
     */
    public void setCustomerNumber(final String customerNumber) {
        this.customerNumber = customerNumber;
    }

    /**
     * @return the encodedSavedCardsData
     */
    public String getEncodedSavedCardsData() {
        return encodedSavedCardsData;
    }

    /**
     * @param encodedSavedCardsData
     *            the encodedSavedCardsData to set
     */
    public void setEncodedSavedCardsData(final String encodedSavedCardsData) {
        this.encodedSavedCardsData = encodedSavedCardsData;
    }

    /**
     * @return the savedCards
     */
    public SavedCardsData getSavedCards() {
        return savedCards;
    }

    /**
     * @param savedCards
     *            the savedCards to set
     */
    public void setSavedCards(final SavedCardsData savedCards) {
        this.savedCards = savedCards;
    }


}
