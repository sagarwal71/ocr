/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data.request;

import au.com.target.tgtpaymentprovider.ipg.data.SavedCardsData;

/**
 * @author mjanarth
 *
 */
public class TokenizeRequest {

    //sessionID
    private String jsessionID;
    private String sessionKey;
    //config for Mobile,Desktop
    private String iFrameConfig;
    //Merchant Number - Store Number
    private String merchantNumber;
    private String storeNumber;
    //controller url
    private String userUrl;
    //order total
    private String tenderAmount;
    //customer Reference
    private String cartId;
    //Customer Code
    private String customerNumber;
    private String posCond;
    //List of encoded customer saved cards
    private SavedCardsData savedCards;

    /**
     * @return the jsessionID
     */
    public String getJsessionID() {
        return jsessionID;
    }

    /**
     * @param jsessionID
     *            the jsessionID to set
     */
    public void setJsessionID(final String jsessionID) {
        this.jsessionID = jsessionID;
    }

    /**
     * @return the sessionKey
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * @param sessionKey
     *            the sessionKey to set
     */
    public void setSessionKey(final String sessionKey) {
        this.sessionKey = sessionKey;
    }

    /**
     * @return the iFrameConfig
     */
    public String getiFrameConfig() {
        return iFrameConfig;
    }

    /**
     * @param iFrameConfig
     *            the iFrameConfig to set
     */
    public void setiFrameConfig(final String iFrameConfig) {
        this.iFrameConfig = iFrameConfig;
    }

    /**
     * @return the merchantNumber
     */
    public String getMerchantNumber() {
        return merchantNumber;
    }

    /**
     * @param merchantNumber
     *            the merchantNumber to set
     */
    public void setMerchantNumber(final String merchantNumber) {
        this.merchantNumber = merchantNumber;
    }

    /**
     * @return the storeNumber
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the userUrl
     */
    public String getUserUrl() {
        return userUrl;
    }

    /**
     * @param userUrl
     *            the userUrl to set
     */
    public void setUserUrl(final String userUrl) {
        this.userUrl = userUrl;
    }

    /**
     * @return the tenderAmount
     */
    public String getTenderAmount() {
        return tenderAmount;
    }

    /**
     * @param tenderAmount
     *            the tenderAmount to set
     */
    public void setTenderAmount(final String tenderAmount) {
        this.tenderAmount = tenderAmount;
    }

    /**
     * @return the cartId
     */
    public String getCartId() {
        return cartId;
    }

    /**
     * @param cartId
     *            the cartId to set
     */
    public void setCartId(final String cartId) {
        this.cartId = cartId;
    }

    /**
     * @return the posCond
     */
    public String getPosCond() {
        return posCond;
    }

    /**
     * @param posCond
     *            the posCond to set
     */
    public void setPosCond(final String posCond) {
        this.posCond = posCond;
    }

    /**
     * @return the customerNumber
     */
    public String getCustomerNumber() {
        return customerNumber;
    }

    /**
     * @param customerNumber
     *            the customerNumber to set
     */
    public void setCustomerNumber(final String customerNumber) {
        this.customerNumber = customerNumber;
    }

    /**
     * @return the savedCards
     */
    public SavedCardsData getSavedCards() {
        return savedCards;
    }

    /**
     * @param savedCards
     *            the savedCards to set
     */
    public void setSavedCards(final SavedCardsData savedCards) {
        this.savedCards = savedCards;
    }

}
