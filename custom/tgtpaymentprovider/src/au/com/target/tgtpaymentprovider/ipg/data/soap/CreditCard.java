/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


@XmlAccessorType(XmlAccessType.FIELD)
public class CreditCard {
    @XmlElement(name = "CardNumber")
    private String cardNumber;

    @XmlElement(name = "CardType")
    private String cardType;

    @XmlElement(name = "MaskedCardNumber")
    private String maskedCardNumber;

    @XmlElement(name = "ThreeDS")
    private ThreeDS threeDS;

    /**
     * @return the cardNumber
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * @param cardNumber
     *            the cardNumber to set
     */
    public void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * @return the cardType
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * @param cardType
     *            the cardType to set
     */
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }

    /**
     * @return the maskedCardNumber
     */
    public String getMaskedCardNumber() {
        return maskedCardNumber;
    }

    /**
     * @param maskedCardNumber
     *            the maskedCardNumber to set
     */
    public void setMaskedCardNumber(final String maskedCardNumber) {
        this.maskedCardNumber = maskedCardNumber;
    }

    /**
     * @return the threeDS
     */
    public ThreeDS getThreeDS() {
        return threeDS;
    }

    /**
     * @param threeDS
     *            the threeDS to set
     */
    public void setThreeDS(final ThreeDS threeDS) {
        this.threeDS = threeDS;
    }


}
