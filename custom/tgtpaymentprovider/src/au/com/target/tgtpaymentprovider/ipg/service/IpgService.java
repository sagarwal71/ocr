/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.service;

import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentVoidRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.result.TargetCardPaymentResult;
import au.com.target.tgtpaymentprovider.ipg.data.response.SessionResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.TransactionsResultResponse;
import au.com.target.tgtpaymentprovider.ipg.data.soap.QueryResponse;
import au.com.target.tgtpaymentprovider.ipg.exception.IpgServiceException;


/**
 * Service definitions for IPG
 *
 */
public interface IpgService {

    /**
     * Prior to launching an iHPP, the merchant is required to request an iHPP session with the IPG. If the request for
     * an iHPP session is successful, the IPG will generate a unique Secure Session Token (SST).
     * 
     * @param createRequest
     *            - request
     * @return SessionResponse - session response
     * 
     * @throws IpgServiceException
     *             - Wrapped IPG exceptions
     */
    SessionResponse createSession(TargetCreateSubscriptionRequest createRequest) throws IpgServiceException;

    /**
     * Query the results of a transaction from IPG. Example scenario: - Customer submit the payment to IPG by clicking
     * on submit button on IFrame - Then this method will be used to query the status of the transaction (including
     * other information) which has been submitted by customer as we don't expect IPG to respond back the status of the
     * transaction
     * 
     * @param sessionId
     *            - session id of the transaction (JSESSIONID is normally used)
     * 
     * @param sessionToken
     *            - secure session token established with IPG initially for transaction
     * 
     * @return - transaction result response
     * 
     * @throws IpgServiceException
     *             - Wrapped IPG exceptions
     */
    TransactionsResultResponse queryTransactionResults(final String sessionId, final String sessionToken)
            throws IpgServiceException;

    /**
     * Submit one IPG card payment which is captured previously via the ipg iFrame to deduct the money
     * 
     * @param paramRequest
     *            - request
     * 
     * @return transaction result - payment capture result
     * 
     * @throws IpgServiceException
     *             - service exception
     */
    TargetCardPaymentResult submitSinglePayment(TargetCaptureRequest paramRequest) throws IpgServiceException;

    /**
     * Submit the request to IPG for refund to creditcard or giftcard. If it is giftcard refund request, the method will
     * call submitSingleVoid in the implementation
     * 
     * @param request
     *            - single refund request
     * @return IpgTransactionResult - single result result
     * 
     * @throws IpgServiceException
     *             - service exception
     */
    TargetCardPaymentResult submitSingleRefund(final TargetFollowOnRefundRequest request)
            throws IpgServiceException;

    /**
     * Submit the request to IPG to void the payment (used for gift card reversal)
     * 
     * @param request
     *            - payment void request
     * @return transaction result - payment void result
     * 
     * @throws IpgServiceException
     *             - service exception
     */
    TargetCardPaymentResult submitSingleVoid(final TargetPaymentVoidRequest request) throws IpgServiceException;

    /**
     * @param targetRetrieveTransactionRequest
     * @return queryResponse
     */
    QueryResponse queryTransaction(TargetRetrieveTransactionRequest targetRetrieveTransactionRequest);

}