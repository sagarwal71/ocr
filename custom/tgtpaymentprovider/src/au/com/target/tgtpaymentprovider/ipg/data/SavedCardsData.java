/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author mjanarth
 *
 */

@XmlRootElement(name = "SavedCards")
@XmlAccessorType(XmlAccessType.FIELD)
public class SavedCardsData {

    @XmlElement(name = "Card")
    private List<CardDetails> cardRequests;

    /**
     * @return the cardRequests
     */
    public List<CardDetails> getCardRequests() {
        return cardRequests;
    }

    /**
     * @param cardRequests
     *            the cardRequests to set
     */
    public void setCardRequests(final List<CardDetails> cardRequests) {
        this.cardRequests = cardRequests;
    }

    /**
     * Add card details
     * 
     * @param cardDetails
     */
    public void addCardDetails(final CardDetails cardDetails) {
        if (cardRequests == null) {
            cardRequests = new ArrayList<>();
        }
        cardRequests.add(cardDetails);
    }
}
