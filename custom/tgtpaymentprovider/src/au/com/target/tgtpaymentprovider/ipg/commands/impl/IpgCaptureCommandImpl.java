/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.commands.impl;

import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtpayment.commands.TargetCaptureCommand;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpayment.commands.result.TargetCardPaymentResult;
import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpaymentprovider.ipg.exception.IpgServiceException;
import au.com.target.tgtpaymentprovider.ipg.util.IpgToHybrisResponseMap;


/**
 * Capture command for IPG
 */
public class IpgCaptureCommandImpl extends AbstractIpgCommand implements TargetCaptureCommand {

    @Override
    public TargetCaptureResult perform(final TargetCaptureRequest paramRequest) {
        final BigDecimal amount = paramRequest.getTotalAmount();

        final TargetCaptureResult result = new TargetCaptureResult();
        result.setMerchantTransactionCode(paramRequest.getTransactionId());
        result.setPaymentProvider(paramRequest.getPayerId());
        result.setRequestTime(new Date());
        result.setTotalAmount(amount);

        final TargetCardResult cardResult = paramRequest.getCardResult();
        if (cardResult == null) {
            result.setTransactionStatus(TransactionStatus.ERROR);
        }
        else {
            final TargetCardPaymentResult cardPaymentResult;
            if (cardResult.getCardType().equals(CreditCardType.GIFTCARD.getCode())) {
                // Gift card support immediate payment model. So no need to execute capture call
                // Return empty transaction result
                cardPaymentResult = new TargetCardPaymentResult();
            }
            else {
                try {
                    cardPaymentResult = this.getIpgService()
                            .submitSinglePayment(paramRequest);

                    final String declineCode = cardPaymentResult.getDeclinedCode();
                    result.setTransactionStatus(IpgToHybrisResponseMap.getTransactionStatus(cardPaymentResult
                            .getResponseCode()));
                    final TransactionStatusDetails transactionStatusDetails = IpgToHybrisResponseMap
                            .getTransactionStatusDetails(declineCode);

                    //Handling only failed scenario
                    if (StringUtils.isNotEmpty(declineCode)) {
                        if (null != transactionStatusDetails) {
                            result.setTransactionStatusDetails(transactionStatusDetails);
                        }
                        else {
                            result.setTransactionStatusDetails(TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
                        }
                        result.setDeclineCode(declineCode);
                        result.setDeclineMessage(cardPaymentResult.getDeclinedMessage());
                    }
                    //if the decline code is empty or null,then the transaction status is successful
                    else {
                        result.setTransactionStatusDetails(transactionStatusDetails);
                    }
                    result.setReconciliationId(cardPaymentResult.getReceiptNumber());
                    result.setRequestToken(cardResult.getToken());
                }
                catch (final IpgServiceException e) {
                    throw new AdapterException(e);
                }
                cardResult.setTargetCardPaymentResult(cardPaymentResult);
            }
        }
        return result;
    }
}