/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.service.impl;

import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentVoidRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.result.TargetCardPaymentResult;
import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpayment.dto.CreditCard;
import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpaymentprovider.ipg.client.IPGClient;
import au.com.target.tgtpaymentprovider.ipg.data.CardDetails;
import au.com.target.tgtpaymentprovider.ipg.data.SavedCardsData;
import au.com.target.tgtpaymentprovider.ipg.data.request.SingleVoidRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSinglePaymentRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSingleRefundRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.TokenizeRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.TransactionQueryRequest;
import au.com.target.tgtpaymentprovider.ipg.data.response.SessionResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.SingleVoidResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.SubmitSingleRefundResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.TransactionsResultResponse;
import au.com.target.tgtpaymentprovider.ipg.data.soap.AdditionalData;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Criteria;
import au.com.target.tgtpaymentprovider.ipg.data.soap.QueryResponse;
import au.com.target.tgtpaymentprovider.ipg.data.soap.QueryTransaction;
import au.com.target.tgtpaymentprovider.ipg.data.soap.RefundInfo;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Response;
import au.com.target.tgtpaymentprovider.ipg.data.soap.VoidInfo;
import au.com.target.tgtpaymentprovider.ipg.exception.IpgServiceException;
import au.com.target.tgtpaymentprovider.ipg.service.IpgService;
import au.com.target.tgtpaymentprovider.ipg.util.IPGHelper;
import au.com.target.tgtpaymentprovider.ipg.util.IpgTransactionBuilder;
import au.com.target.tgtutility.constants.TgtutilityConstants.InfoCode;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * Implementation for {@link IpgService}. This class would consume {@link IPGClient} to communicate with IPG
 */
public class IpgServiceImpl implements IpgService {
    //CHECKSTYLE:OFF
    private static final String IPG_DATE_FORMAT = "yyyy-MMM-dd HH:MM:00";
    private static final int QUERY_START_OFFSET_DAYS = -60;
    private static final int QUERY_END_OFFSET_DAYS = 2;
    private static final String GIFTCARD_REVERSAL_SUCCESS_RESPONSE_CODE = "0";
    private static final String ERROR_FAIL_TO_GET_IPG_TOKEN = "Error occured when retrieving IPG session token for order:{0}, error:{1}";
    private static final String GIFTCARD_REVERSAL_REQUEST_INFO_MESSAGE = "Gift card reversal request sent, receiptNumber=[%s]";
    private static final String GIFTCARD_REVERSAL_RESPONSE_ERROR_MESSAGE = "Gift card reversal failed, receiptNumber=[%s]";
    private static final String GIFTCARD_REVERSAL_RESPONSE_INFO_MESSAGE = "Gift card reversal response received, receiptNumber=[%s], "
            + "responseCode=[%s], responseReceiptNumber=[%s]";
    //CHECKSTYLE:ON

    private static final Logger LOG = Logger.getLogger(IpgServiceImpl.class);

    /**
     * Represents single pay iHPP template is to be used
     */
    private String directLinkSinglePay;

    /**
     * Represents single pay for pre-order iHPP template is to be used
     */
    private String directLinkSinglePayPreOrder;

    /**
     * Represents multiple pay iHPP template is to be used
     */
    private String directLinkMultiplePay;

    /**
     * Represents update card iHPP template is to be used
     */
    private String directLinkUpdateCard;

    /**
     * Represent the POSCond value to be sent to IPG (48 for E-Commerce)
     */
    private String posCond;

    /**
     * Represents store number (e.g. for online 5599 in production)
     */
    private String storeNumber;

    /**
     * Used to generate session key which is unique for each IPG session
     */
    private KeyGenerator sessionKeyGenerator;

    /**
     * {@link IPGClient} is used to communicate with IPG
     */
    private IPGClient ipgClient;



    @Override
    public SessionResponse createSession(final TargetCreateSubscriptionRequest request) throws IpgServiceException {
        final SessionResponse sessionResponse;
        final TokenizeRequest tokenizeRequest = createTokenizeRequest(request);

        try {
            sessionResponse = ipgClient.requestIPGSessionToken(tokenizeRequest);
        }
        catch (final Exception e) {
            LOG.error("Error occured when retrieving IPG session token", e);
            throw new IpgServiceException("Error occured when retrieving IPG session token", e);
        }
        if (!sessionResponse.isSessionStored()) {
            throw new IpgServiceException(MessageFormat.format(ERROR_FAIL_TO_GET_IPG_TOKEN,
                    request.getOrder().getOrderId(),
                    sessionResponse.getSessionStoredError()));
        }

        return sessionResponse;
    }

    @Override
    public TransactionsResultResponse queryTransactionResults(final String sessionId, final String sessionToken)
            throws IpgServiceException {
        final TransactionsResultResponse transactionResultResponse;
        final TransactionQueryRequest transactionQueryRequest = createTransactionQueryRequest(sessionId, sessionToken);
        try {
            transactionResultResponse = ipgClient.getTransactionResults(transactionQueryRequest);
        }
        catch (final Exception e) {
            LOG.error("Error occured when retrieving transaction result from IPG", e);
            throw new IpgServiceException("Error occured when retrieving transaction result from IPG", e);
        }
        return transactionResultResponse;
    }

    @SuppressWarnings("boxing")
    @Override
    public TargetCardPaymentResult submitSinglePayment(final TargetCaptureRequest captureRequest)
            throws IpgServiceException {
        Assert.notNull(captureRequest, "capture request must not be null");
        final TargetCardResult cardResult = captureRequest.getCardResult();
        Assert.notNull(cardResult, "card result must not be null");
        final TargetCardPaymentResult transactionResult = cardResult.getTargetCardPaymentResult();
        Assert.notNull(transactionResult, "transaction result must not be null");
        final SubmitSinglePaymentRequest request = createSinglePaymentRequest(captureRequest);

        try {
            final Response response = ipgClient.submitSinglePayment(request).getResponse();
            transactionResult.setResponseCode(response.getResponseCode());
            transactionResult.setReceiptNumber(response.getReceipt());
            transactionResult.setTransactionDateTime(response.getTimestamp());

            final String declinedCode = response.getDeclinedCode();
            final String declinedMessage = response.getDeclinedMessage();

            transactionResult.setDeclinedCode(declinedCode);
            transactionResult.setDeclinedMessage(declinedMessage);

            if (StringUtils.isNotEmpty(declinedCode)) {
                logDeclinedDetails(declinedCode, declinedMessage);
            }
        }
        catch (final Exception e) {
            LOG.error("Failed to make ipg payment", e);
            throw new IpgServiceException("Failed to make ipg payment:" + e.getMessage());
        }
        return transactionResult;
    }

    protected void logDeclinedDetails(final String declinedCode, final String declinedMessage) {
        LOG.error("Declined response received from IPG, Code=" + declinedCode + ", Message=" + declinedMessage);
    }

    @Override
    public TargetCardPaymentResult submitSingleRefund(final TargetFollowOnRefundRequest request)
            throws IpgServiceException {
        Assert.notNull(request, "The refund request should not be null");
        TargetCardPaymentResult refundResult = null;
        Response response = null;
        if (request.isGiftcard()) {
            final TargetPaymentVoidRequest giftcardVoidRequest = new TargetPaymentVoidRequest();
            giftcardVoidRequest.setReceiptNumber(request.getReceiptNumber());
            refundResult = submitSingleVoid(giftcardVoidRequest);
        }
        else {
            try {
                final SubmitSingleRefundResponse refundResponse = ipgClient
                        .submitSingleRefund(createSingleRefundRequest(request));
                response = refundResponse.getResponse();
                refundResult = new TargetCardPaymentResult();
                refundResult.setResponseCode(response.getResponseCode());
                refundResult.setAmount(request.getTotalAmount());
                refundResult.setReceiptNumber(response.getReceipt());
                refundResult.setTransactionDateTime(response.getTimestamp());
                refundResult.setSettlementDate(response.getSettlementDate());

                final String declinedCode = response.getDeclinedCode();
                final String declinedMessage = response.getDeclinedMessage();

                refundResult.setDeclinedCode(declinedCode);
                refundResult.setDeclinedMessage(declinedMessage);

                if (StringUtils.isNotEmpty(declinedCode)) {
                    logDeclinedDetails(declinedCode, declinedMessage);
                }
            }
            catch (final Exception e) {
                LOG.error("Failed to refund on ipg payment", e);
                throw new IpgServiceException("Failed to refund on ipg payment:" + e.getMessage());
            }
        }
        return refundResult;
    }

    @Override
    public TargetCardPaymentResult submitSingleVoid(final TargetPaymentVoidRequest request) throws IpgServiceException {
        TargetCardPaymentResult voidResult = null;
        Response response = null;

        try {
            final SingleVoidRequest voidRequest = createSingleVoidRequest(request);

            LOG.info(SplunkLogFormatter.formatInfoMessage(
                    String.format(GIFTCARD_REVERSAL_REQUEST_INFO_MESSAGE, request.getReceiptNumber()),
                    InfoCode.INFO_GIFTCARD_PAYMENT));

            final SingleVoidResponse voidResponse = ipgClient.submitSingleVoid(voidRequest);

            if (voidResponse != null) {
                response = voidResponse.getResponse();
                if (null != response) {

                    final String reversalResponseStatus = response.getResponseCode().equals(
                            GIFTCARD_REVERSAL_SUCCESS_RESPONSE_CODE) ? "SUCEESS" : "FAIL";

                    LOG.info(SplunkLogFormatter.formatInfoMessage(String.format(
                            GIFTCARD_REVERSAL_RESPONSE_INFO_MESSAGE, request.getReceiptNumber(),
                            reversalResponseStatus, response.getReceipt()), InfoCode.INFO_GIFTCARD_PAYMENT));

                    voidResult = new TargetCardPaymentResult();
                    voidResult.setResponseCode(response.getResponseCode());
                    voidResult.setReceiptNumber(response.getReceipt());
                    voidResult.setTransactionDateTime(response.getTimestamp());
                    voidResult.setSettlementDate(response.getSettlementDate());

                    final String declinedCode = response.getDeclinedCode();
                    final String declinedMessage = response.getDeclinedMessage();

                    voidResult.setDeclinedCode(declinedCode);
                    voidResult.setDeclinedMessage(declinedMessage);

                    if (StringUtils.isNotEmpty(declinedCode)) {
                        logDeclinedDetails(declinedCode, declinedMessage);
                    }
                }
            }
            else {
                LOG.info(SplunkLogFormatter.formatMessage(
                        String.format(GIFTCARD_REVERSAL_RESPONSE_ERROR_MESSAGE, request.getReceiptNumber()),
                        InfoCode.INFO_GIFTCARD_PAYMENT));
            }
        }
        catch (final Exception e) {
            LOG.error("Failed to void ipg payment", e);
            throw new IpgServiceException("Failed to void ipg payment:" + e.getMessage());
        }
        return voidResult;
    }

    @Override
    public QueryResponse queryTransaction(final TargetRetrieveTransactionRequest targetRetrieveTransactionRequest) {
        QueryResponse response = null;
        final QueryTransaction queryRequest = buildQueryTransactionRequest(targetRetrieveTransactionRequest);
        try {
            response = ipgClient.queryTransaction(queryRequest);
        }
        catch (final Exception e) {
            LOG.error("Failed to query transaction via ipg", e);
        }
        return response;
    }

    /**
     * @param targetRetrieveTransactionRequest
     * @return queryRequest
     */
    protected QueryTransaction buildQueryTransactionRequest(
            final TargetRetrieveTransactionRequest targetRetrieveTransactionRequest) {
        Assert.notNull(targetRetrieveTransactionRequest, "TargetRetrieveTransactionRequest cannot be null");
        Assert.notNull(targetRetrieveTransactionRequest.getOrderDate(),
                "TargetRetrieveTransactionRequest date cannot be null");
        final QueryTransaction queryRequest = new QueryTransaction();
        final Criteria critera = new Criteria();
        critera.setCustRef(targetRetrieveTransactionRequest.getOrderId());
        critera.setReceipt(targetRetrieveTransactionRequest.getReceiptNumber());
        if (null != targetRetrieveTransactionRequest.getEntryAmount()) {
            final int amtInCents = IPGHelper.getAmountInCents(targetRetrieveTransactionRequest.getEntryAmount());
            critera.setAmount("" + amtInCents);
        }
        final Date fromDate = DateUtils.addDays(targetRetrieveTransactionRequest.getOrderDate(),
                QUERY_START_OFFSET_DAYS);
        critera.setTrnStartTimestamp(DateFormatUtils.format(fromDate, IPG_DATE_FORMAT).toUpperCase());
        final Date endDate = DateUtils.addDays(targetRetrieveTransactionRequest.getOrderDate(), QUERY_END_OFFSET_DAYS);
        critera.setTrnEndTimestamp(DateFormatUtils.format(endDate, IPG_DATE_FORMAT).toUpperCase());
        final AdditionalData addtionalData = new AdditionalData();
        addtionalData.addCore("Amount");
        addtionalData.addCore("CustRef");
        addtionalData.addCore("CustNumber");
        addtionalData.addCore("TrnTypeID");
        addtionalData.addCore("TrnStatusID");
        queryRequest.setAddtionalData(addtionalData);
        queryRequest.setCriteria(critera);
        return queryRequest;
    }

    @SuppressWarnings("boxing")
    protected SubmitSinglePaymentRequest createSinglePaymentRequest(final TargetCaptureRequest captureRequest) {
        final SubmitSinglePaymentRequest request = new SubmitSinglePaymentRequest();
        final IpgTransactionBuilder builder = IpgTransactionBuilder.create();
        final Order order = captureRequest.getOrder();
        builder.addAmountInCents(Integer.valueOf(IPGHelper.getAmountInCents(captureRequest.getTotalAmount())));
        builder.addCustomer(order.getOrderId(), order.getUserId());
        builder.addIpgCardInfo(captureRequest.getCardResult());
        builder.setStoreId(storeNumber);
        builder.setPosCond(posCond);

        request.setTransaction(builder.build());
        return request;
    }

    private SubmitSingleRefundRequest createSingleRefundRequest(
            final TargetFollowOnRefundRequest ipgRefundRequest) {
        int amtInCents = 0;
        final SubmitSingleRefundRequest request = new SubmitSingleRefundRequest();
        final RefundInfo refundInfo = new RefundInfo();
        if (null != ipgRefundRequest.getTotalAmount()) {
            amtInCents = IPGHelper.getAmountInCents(ipgRefundRequest.getTotalAmount());
        }
        refundInfo.setAmount(String.valueOf(amtInCents));
        refundInfo.setReceiptNumber(ipgRefundRequest.getReceiptNumber());
        request.setRefundInfo(refundInfo);
        return request;
    }

    /**
     * Creates a {@link SingleVoidRequest} to be sent to IPG
     * 
     * @param voidRequest
     *            - request to be converted
     * @return - request to be sent to IPG
     */
    protected SingleVoidRequest createSingleVoidRequest(final TargetPaymentVoidRequest voidRequest) {
        final SingleVoidRequest request = new SingleVoidRequest();
        final VoidInfo voidInfo = new VoidInfo();

        voidInfo.setReceiptNumber(voidRequest.getReceiptNumber());
        request.setVoidInfo(voidInfo);
        return request;
    }

    /**
     * Create a {@link TokenizeRequest} to be sent to IPG client
     * 
     * @param request
     *            holds all info
     * @return TokenizeRequest
     */
    protected TokenizeRequest createTokenizeRequest(final TargetCreateSubscriptionRequest request) {
        final TokenizeRequest tokenizeRequest = new TokenizeRequest();

        // IPG - SessionID (JSESSIONID is used)
        tokenizeRequest.setJsessionID(request.getSessionId());

        // IPG - SessionKey (auto generated value)
        tokenizeRequest.setSessionKey(String.valueOf(sessionKeyGenerator.generate()));

        final Order order = request.getOrder();

        // IPG - DL (Direct link value configured)
        if (IpgPaymentTemplateType.CREDITCARDSINGLE.equals(request.getIpgPaymentTemplateType())) {
            if (BooleanUtils.isTrue(order.isContainPreOrderItems())) {
                tokenizeRequest.setiFrameConfig(directLinkSinglePayPreOrder);
            }
            else {
                tokenizeRequest.setiFrameConfig(directLinkSinglePay);
            }
        }
        else if (IpgPaymentTemplateType.GIFTCARDMULTIPLE.equals(request.getIpgPaymentTemplateType())) {
            tokenizeRequest.setiFrameConfig(directLinkMultiplePay);
        }

        else if (IpgPaymentTemplateType.UPDATECARD.equals(request.getIpgPaymentTemplateType())) {
            tokenizeRequest.setiFrameConfig(directLinkUpdateCard);
        }

        // IPG - Amount (in cents e.g. $55.00 = 5500)
        if (null != order.getOrderTotalCents()) {
            tokenizeRequest.setTenderAmount(order.getOrderTotalCents().toString());
        }

        // IPG - CustRef (order code is used here)
        if (StringUtils.isNotEmpty(order.getOrderId())) {
            tokenizeRequest.setCartId(order.getOrderId());
        }

        // IPG - POSCond (configured 48 - E-Commerce)
        if (StringUtils.isNotEmpty(posCond)) {
            tokenizeRequest.setPosCond(posCond);
        }

        // IPG - StoreID
        if (StringUtils.isNotEmpty(storeNumber)) {
            tokenizeRequest.setStoreNumber(storeNumber);
        }

        // IPG - CustNumber
        if (StringUtils.isNotEmpty(order.getUserId())) {
            tokenizeRequest.setCustomerNumber(order.getUserId());
        }

        tokenizeRequest.setUserUrl(request.getReturnUrl());

        populateSavedCardDetails(request, tokenizeRequest);

        return tokenizeRequest;
    }

    /**
     * Populate saved credit card information in the request
     * 
     * @param request
     * @param tokenizeRequest
     */
    protected void populateSavedCardDetails(final TargetCreateSubscriptionRequest request,
            final TokenizeRequest tokenizeRequest) {
        final List<CreditCard> savedCards = request.getSavedCreditCards();

        if (CollectionUtils.isNotEmpty(savedCards)) {
            final SavedCardsData savedCardsData = new SavedCardsData();
            tokenizeRequest.setSavedCards(savedCardsData);

            for (final CreditCard creditCard : savedCards) {
                final CardDetails card = new CardDetails();
                card.setBin(creditCard.getBin());
                card.setCardExpiry(creditCard.getCardExpiry());
                card.setCardNumber(creditCard.getMaskedCard());
                card.setCardType(creditCard.getCardType());
                card.setIsDefaultCard(creditCard.getDefaultCard().booleanValue() ? "1" : "0");
                card.setPromoId(creditCard.getPromoId());
                card.setToken(creditCard.getToken());
                card.setTokenExpiry(creditCard.getTokenExpiry());

                tokenizeRequest.getSavedCards().addCardDetails(card);
            }
        }
    }

    /**
     * Creates a {@link TransactionQueryRequest} to be sent to IPG client
     * 
     * @param sessionId
     *            - session id
     * @param sessionToken
     *            - session token
     * @return - transaction query request
     */
    protected TransactionQueryRequest createTransactionQueryRequest(final String sessionId, final String sessionToken) {
        final TransactionQueryRequest request = new TransactionQueryRequest();
        request.setSessionId(sessionId);
        request.setSessionToken(sessionToken);
        return request;
    }

    /**
     * @param sessionKeyGenerator
     *            the sessionKeyGenerator to set
     */
    @Required
    public void setSessionKeyGenerator(final KeyGenerator sessionKeyGenerator) {
        this.sessionKeyGenerator = sessionKeyGenerator;
    }

    /**
     * @param ipgClient
     *            the ipgClient to set
     */
    @Required
    public void setIpgClient(final IPGClient ipgClient) {
        this.ipgClient = ipgClient;
    }



    /**
     * @param directLinkSinglePay
     *            the directLinkSinglePay to set
     */
    @Required
    public void setDirectLinkSinglePay(final String directLinkSinglePay) {
        this.directLinkSinglePay = directLinkSinglePay;
    }

    /**
     * @param directLinkMultiplePay
     *            the directLinkMultiplePay to set
     */
    @Required
    public void setDirectLinkMultiplePay(final String directLinkMultiplePay) {
        this.directLinkMultiplePay = directLinkMultiplePay;
    }

    /**
     * @param posCond
     *            the posCond to set
     */
    @Required
    public void setPosCond(final String posCond) {
        this.posCond = posCond;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    @Required
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the directLinkSinglePayPreOrder
     */
    public String getDirectLinkSinglePayPreOrder() {
        return directLinkSinglePayPreOrder;
    }

    /**
     * @param directLinkSinglePayPreOrder
     *            the directLinkSinglePayPreOrder to set
     */
    public void setDirectLinkSinglePayPreOrder(final String directLinkSinglePayPreOrder) {
        this.directLinkSinglePayPreOrder = directLinkSinglePayPreOrder;
    }

    /**
     * @param directLinkUpdateCard
     *            the directLinkUpdateCard to set
     */
    public void setDirectLinkUpdateCard(final String directLinkUpdateCard) {
        this.directLinkUpdateCard = directLinkUpdateCard;
    }

}