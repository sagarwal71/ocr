/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.client.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.ippayments._interface.api.dts.DtsSoap;
import au.com.target.tgtpaymentprovider.constants.TgtpaymentproviderConstants;
import au.com.target.tgtpaymentprovider.constants.TgtpaymentproviderConstants.IPGRequestAttributes;
import au.com.target.tgtpaymentprovider.ipg.client.IPGClient;
import au.com.target.tgtpaymentprovider.ipg.data.CardResultsData;
import au.com.target.tgtpaymentprovider.ipg.data.SavedCardsData;
import au.com.target.tgtpaymentprovider.ipg.data.request.SingleVoidRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSinglePaymentRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSingleRefundRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.TokenizeRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.TransactionQueryRequest;
import au.com.target.tgtpaymentprovider.ipg.data.response.SessionResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.SingleVoidResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.SubmitSinglePaymentResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.SubmitSingleRefundResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.TransactionsResultResponse;
import au.com.target.tgtpaymentprovider.ipg.data.soap.QueryResponse;
import au.com.target.tgtpaymentprovider.ipg.data.soap.QueryTransaction;
import au.com.target.tgtpaymentprovider.ipg.data.soap.RefundInfo;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Security;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Transaction;
import au.com.target.tgtpaymentprovider.ipg.data.soap.VoidInfo;
import au.com.target.tgtpaymentprovider.ipg.exception.IPGClientException;
import au.com.target.tgtpaymentprovider.ipg.util.IPGHelper;
import au.com.target.tgtpaymentprovider.ipg.util.IPGRequestParametersBuilder;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.http.factory.HttpClientFactory;
import au.com.target.tgtutility.util.HtmlParserUtil;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * @author mjanarth
 *
 */
public class IPGClientImpl implements IPGClient {

    private static final Logger LOG = Logger.getLogger(IPGClientImpl.class);
    private String sessionRequestUrl;
    private IPGRequestParametersBuilder ipgRequestParamatersBuilder;
    private HttpClientFactory paymentProviderHttpClientFactory;
    private String userName;
    private String password;
    private String accountNumber;
    private DtsSoap ipgSoapService;
    private IPGHelper ipgHelper;
    private final Security security = new Security();

    private int connectTimeout;

    private int readTimeout;

    /**
     * @return the ipgHelper
     */
    public IPGHelper getIpgHelper() {
        return ipgHelper;
    }

    /**
     * @param ipgHelper
     *            the ipgHelper to set
     */
    public void setIpgHelper(final IPGHelper ipgHelper) {
        this.ipgHelper = ipgHelper;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtpaymentprovider.ipg.client.IPGClient#requestIPGSessionToken(au.com.target.tgtpaymentprovider.ipg.data.request.TokenizeRequest)
     */
    @Override
    public SessionResponse requestIPGSessionToken(final TokenizeRequest request) throws IPGClientException {
        final String htmlResponse = postSessionRequest(request);
        final SessionResponse response = populateSessionResponse(htmlResponse);
        return response;
    }

    @Override
    public QueryResponse queryTransaction(final QueryTransaction queryRequest) throws IPGClientException {
        queryRequest.setSecurity(security);
        queryRequest.getCriteria().setAccountNumber(accountNumber);
        try {
            final String responseXml = ipgSoapService.queryTransaction(ipgHelper.toXml(queryRequest));
            return (QueryResponse)ipgHelper.fromXml(responseXml);
        }
        catch (final JAXBException ex) {
            LOG.warn(SplunkLogFormatter.formatMessage("queryTransaction with IPG gateway failed",
                    TgtutilityConstants.ErrorCode.ERR_IPGPAYMENT), ex);
            throw new IPGClientException(ex);
        }
    }

    @Override
    public SubmitSinglePaymentResponse submitSinglePayment(final SubmitSinglePaymentRequest request)
            throws IPGClientException {
        SubmitSinglePaymentResponse response = null;
        try {
            if (request != null) {
                final Transaction transaction = request.getTransaction();
                if (transaction != null) {
                    transaction.setSecurity(security);
                    transaction.setAccountNumber(accountNumber);

                    final String responseXml = ipgSoapService.submitSinglePayment(ipgHelper
                            .marshalTransaction(transaction));
                    response = new SubmitSinglePaymentResponse();
                    response.setResponse(ipgHelper.unmarshalResponse(responseXml));
                }
            }
        }
        catch (final JAXBException ex) {
            LOG.error(SplunkLogFormatter.formatMessage("SubmitSinglePayment with IPG gateway failed",
                    TgtutilityConstants.ErrorCode.ERR_IPGPAYMENT), ex);
            throw new IPGClientException(ex);
        }
        return response;
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtpaymentprovider.ipg.client.IPGClient#submitSingleRefund(au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSingleRefundRequest)
     */
    @Override
    public SubmitSingleRefundResponse submitSingleRefund(final SubmitSingleRefundRequest request)
            throws IPGClientException {
        SubmitSingleRefundResponse response = null;
        try {
            if (request != null) {
                final RefundInfo refundInfo = request.getRefundInfo();
                if (refundInfo != null) {
                    refundInfo.setSecurity(security);
                    final String responseXml = ipgSoapService
                            .submitSingleRefund(ipgHelper.marshalRefundInfo(refundInfo));
                    response = new SubmitSingleRefundResponse();
                    response.setResponse(ipgHelper.unmarshalResponse(responseXml));
                }
            }
        }
        catch (final JAXBException ex) {
            LOG.error(SplunkLogFormatter.formatMessage("SubmitSingleRefund with IPG gateway failed",
                    TgtutilityConstants.ErrorCode.ERR_IPGPAYMENT), ex);
            throw new IPGClientException(ex);
        }
        if (response == null || response.getResponse() == null) {
            throw new IPGClientException(SplunkLogFormatter.formatMessage(
                    "Failed to perform a single refund: invalid or empty request/response",
                    TgtutilityConstants.ErrorCode.ERR_IPGPAYMENT));
        }
        return response;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtpaymentprovider.ipg.client.IPGClient#submitSingleVoid(au.com.target.tgtpaymentprovider.ipg.data.request.SingleVoidRequest)
     */
    @Override
    public SingleVoidResponse submitSingleVoid(final SingleVoidRequest request) throws IPGClientException {
        SingleVoidResponse response = null;
        try {
            if (request != null) {
                final VoidInfo voidInfo = request.getVoidInfo();
                if (voidInfo != null) {
                    voidInfo.setSecurity(security);

                    final String responseXml = ipgSoapService.submitSingleVoid(ipgHelper.marshalVoidInfo(voidInfo));

                    response = new SingleVoidResponse();
                    response.setResponse(ipgHelper.unmarshalResponse(responseXml));
                }
            }
        }
        catch (final JAXBException ex) {
            LOG.error(SplunkLogFormatter.formatMessage("SubmitSingleVoid with IPG gateway failed",
                    TgtutilityConstants.ErrorCode.ERR_IPGPAYMENT), ex);
        }
        if (response == null || response.getResponse() == null) {
            throw new IPGClientException(SplunkLogFormatter.formatMessage(
                    "Failed to perform a single void: invalid or empty request/response",
                    TgtutilityConstants.ErrorCode.ERR_IPGPAYMENT));
        }
        return response;
    }

    /**
     * post the request to IPG url
     * 
     * @param request
     * @return SessionResponse
     * @throws IPGClientException
     */
    protected String postSessionRequest(final TokenizeRequest request) throws IPGClientException {
        HttpPost httpPost = null;
        try {
            httpPost = new HttpPost(sessionRequestUrl);
            List<NameValuePair> params = new ArrayList<>();
            params = ipgRequestParamatersBuilder.buildRequestParametersForSessionToken(request);

            setCredentials(params);

            final UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params);
            httpPost.setEntity(entity);
            httpPost.addHeader(TgtpaymentproviderConstants.IPGRequestAttributes.HEADER_CONTENT_TYPE,
                    TgtpaymentproviderConstants.IPGRequestAttributes.HEADER_CONTENT_TYPE_FORM_URLENCODED);

            final ResponseHandler<String> handler = createResponseHandler();
            final HttpClient httpClient = getHttpClient();

            final String response = httpClient.execute(httpPost, handler);
            return response;
        }
        catch (final IOException | JAXBException ex) {
            LOG.error(SplunkLogFormatter.formatMessage("Communication with IPG gateway failed",
                    TgtutilityConstants.ErrorCode.ERR_IPGPAYMENT), ex);
            throw new IPGClientException(ex);
        }
        finally {
            if (httpPost != null) {
                httpPost.releaseConnection();
            }
        }
    }

    /**
     * Set credentials in the request
     */
    protected void setCredentials(final List<NameValuePair> params) {
        params.add(new BasicNameValuePair(TgtpaymentproviderConstants.IPGRequestAttributes.USERNAME, userName));
        params.add(new BasicNameValuePair(TgtpaymentproviderConstants.IPGRequestAttributes.PASSWD, password));
        params.add(new BasicNameValuePair(TgtpaymentproviderConstants.IPGRequestAttributes.ACCOUNTNUMBER,
                accountNumber));
    }

    /**
     * Parse the html string response and populates the session response
     * 
     * @param htmlResponse
     * @return SessionResponse
     */
    protected SessionResponse populateSessionResponse(final String htmlResponse) throws IPGClientException {
        final HashMap<String, String> responseMap = HtmlParserUtil.parseHtmlFormInputParameters(htmlResponse);
        if (null != responseMap && !responseMap.isEmpty()) {
            final SessionResponse response = new SessionResponse();
            response.setSecureSessionToken(responseMap.get(IPGRequestAttributes.SESSIONTOKEN));
            response.setSessionStoredError(responseMap.get(IPGRequestAttributes.SESSION_STORED_ERROR));
            final String isSessionStored = responseMap.get(IPGRequestAttributes.SESSIONSTORED);
            if (StringUtils.isNotEmpty(isSessionStored)) {
                if (isSessionStored.equalsIgnoreCase("True")) {
                    response.setSessionStored(true);
                }
                else {
                    response.setSessionStored(false);
                }
            }
            return response;
        }
        else {
            LOG.error("Failed to parse html response:" + htmlResponse);
            throw new IPGClientException("Cannot parse ipg response");
        }
    }


    /**
     * @param sessionRequestUrl
     *            the sessionRequestUrl to set
     */
    @Required
    public void setSessionRequestUrl(final String sessionRequestUrl) {
        this.sessionRequestUrl = sessionRequestUrl;
    }

    /**
     * @param userName
     *            the userName to set
     */
    @Required
    public void setUserName(final String userName) {
        this.userName = userName;
        security.setUserName(userName);
    }

    /**
     * @param password
     *            the password to set
     */

    @Required
    public void setPassword(final String password) {
        this.password = password;
        security.setPassword(password);
    }

    /**
     * @param paymentProviderHttpClientFactory
     *            the paymentProviderHttpClientFactory to set
     */
    @Required
    public void setPaymentProviderHttpClientFactory(final HttpClientFactory paymentProviderHttpClientFactory) {
        this.paymentProviderHttpClientFactory = paymentProviderHttpClientFactory;
    }

    /**
     * @param ipgRequestParamatersBuilder
     *            the ipgRequestParamatersBuilder to set
     */
    @Required
    public void setIpgRequestParamatersBuilder(final IPGRequestParametersBuilder ipgRequestParamatersBuilder) {
        this.ipgRequestParamatersBuilder = ipgRequestParamatersBuilder;
    }

    /**
     * @return the httpClient
     */
    protected HttpClient getHttpClient() {
        final RequestConfig config = RequestConfig.custom()
                .setConnectionRequestTimeout(connectTimeout)
                .setSocketTimeout(readTimeout).build();
        return paymentProviderHttpClientFactory
                .createHttpClientBuilderWithProxy()
                .setDefaultRequestConfig(config).build();
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtpaymentprovider.ipg.client.IPGClient#getTransactionResults(au.com.target.tgtpaymentprovider.ipg.data.request.TransactionQueryRequest)
     */
    @Override
    public TransactionsResultResponse getTransactionResults(final TransactionQueryRequest request)
            throws IPGClientException {
        final String htmlResponse = postGetTransactionResultsRequest(request);
        final TransactionsResultResponse response = populateTransactionResultsResponse(htmlResponse);
        return response;
    }

    /**
     * Post the transaction Request
     * 
     * @param request
     * @return String
     * @throws IPGClientException
     */
    protected String postGetTransactionResultsRequest(final TransactionQueryRequest request) throws IPGClientException {
        HttpPost httpPost = null;
        try {
            httpPost = new HttpPost(sessionRequestUrl);
            List<NameValuePair> params = new ArrayList<>();
            params = ipgRequestParamatersBuilder
                    .buildRequestParamatersForGetTransactionResult(request);
            params.add(new BasicNameValuePair(TgtpaymentproviderConstants.IPGRequestAttributes.USERNAME, userName));
            params.add(new BasicNameValuePair(TgtpaymentproviderConstants.IPGRequestAttributes.PASSWD, password));
            final UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params);
            httpPost.setEntity(entity);
            httpPost.addHeader(TgtpaymentproviderConstants.IPGRequestAttributes.HEADER_CONTENT_TYPE,
                    TgtpaymentproviderConstants.IPGRequestAttributes.HEADER_CONTENT_TYPE_FORM_URLENCODED);
            final ResponseHandler<String> handler = createResponseHandler();
            final HttpClient httpClient = getHttpClient();
            final String response = httpClient.execute(httpPost, handler);
            return response;
        }
        catch (final IOException ex) {
            LOG.error(SplunkLogFormatter.formatMessage("Communication with IPG gateway failed",
                    TgtutilityConstants.ErrorCode.ERR_IPGPAYMENT), ex);
            throw new IPGClientException(ex);
        }
        finally {
            if (httpPost != null) {
                httpPost.releaseConnection();
            }
        }
    }

    /**
     * Parse the html string response and populates the Transaction results response
     * 
     * @param htmlResponse
     * @return SessionResponse
     * @throws IPGClientException
     *
     */
    protected TransactionsResultResponse populateTransactionResultsResponse(final String htmlResponse)
            throws IPGClientException {
        final HashMap<String, String> responseMap = HtmlParserUtil.parseHtmlFormInputParameters(htmlResponse);
        final TransactionsResultResponse response = new TransactionsResultResponse();
        if (null != responseMap && !responseMap.isEmpty()) {
            response.setQueryError(responseMap.get(IPGRequestAttributes.QUERY_ERROR));
            response.setQueryResult(responseMap.get(IPGRequestAttributes.QUERY_RESULT));
            response.setSessionId(responseMap.get(IPGRequestAttributes.SESSION_ID));
            response.setSessionToken(responseMap.get(IPGRequestAttributes.SESSIONTOKEN));
            response.setSessionKey(responseMap.get(IPGRequestAttributes.SESSIONKEY));
            response.setCustReference(responseMap.get(IPGRequestAttributes.CUSTREF));
            response.setAmount(responseMap.get(IPGRequestAttributes.AMOUNT));
            response.setTransactionResult(responseMap.get(IPGRequestAttributes.TRANSACTION_RESULT));
            response.setEncodedCardResultsData(responseMap.get(IPGRequestAttributes.CARD_RESULTS));
            response.setEncodedSavedCardsData(responseMap.get(IPGRequestAttributes.SAVED_CARDS));
            try {
                if (StringUtils.isNotEmpty(response.getEncodedCardResultsData())) {
                    final String urlDecodedString = URLDecoder.decode(response.getEncodedCardResultsData(), "utf-8");
                    final CardResultsData cardResults = ipgHelper.unmarshalCardResults(ipgHelper
                            .decode(urlDecodedString));
                    response.setCardResults(cardResults);
                }
                if (StringUtils.isNotEmpty(response.getEncodedSavedCardsData())) {
                    final String urlDecodedString = URLDecoder.decode(response.getEncodedSavedCardsData(), "utf-8");
                    final SavedCardsData savedCards = ipgHelper.unmarshalSavedCards(ipgHelper
                            .decode(urlDecodedString));
                    response.setSavedCards(savedCards);
                }
                final String custNumber = responseMap.get(IPGRequestAttributes.CUSTOMER_NUMBER);
                if (null != custNumber) {
                    response.setCustomerNumber(URLDecoder.decode(custNumber, "utf-8"));
                }
            }

            catch (final JAXBException ex) {
                LOG.error(SplunkLogFormatter.formatMessage(
                        "Unable to umarshall CardResultsData",
                        TgtutilityConstants.ErrorCode.ERR_IPGPAYMENT), ex);
                throw new IPGClientException(ex);
            }
            catch (final UnsupportedEncodingException ex) {
                LOG.error(SplunkLogFormatter.formatMessage(
                        "Unable to decode CardResultsData",
                        TgtutilityConstants.ErrorCode.ERR_IPGPAYMENT), ex);
                throw new IPGClientException(ex);
            }
            return response;
        }

        return null;
    }

    /**
     * @return the ipgSoapService
     */
    public DtsSoap getIpgSoapService() {
        return ipgSoapService;
    }

    /**
     * @param ipgSoapService
     *            the ipgSoapService to set
     */
    public void setIpgSoapService(final DtsSoap ipgSoapService) {
        this.ipgSoapService = ipgSoapService;
    }

    /**
     * @return the security
     */
    protected Security getSecurity() {
        return security;
    }


    /**
     * @param accountNumber
     *            the accountNumber to set
     */
    @Required
    public void setAccountNumber(final String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /*
     * Creates ResponseHandler
     * 
     * @return ResponseHandler
     */
    private ResponseHandler<String> createResponseHandler() {
        final ResponseHandler<String> handler = new ResponseHandler<String>() {
            /*
             * (non-Javadoc)
             * 
             * @see org.apache.http.client.ResponseHandler#handleResponse(org.apache.http.HttpResponse)
             */
            @Override
            public String handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {
                final HttpEntity entity = response.getEntity();
                String responseHtml = null;
                if (entity != null) {
                    responseHtml = EntityUtils.toString(entity);
                }
                EntityUtils.consumeQuietly(entity);
                return responseHtml;
            }
        };
        return handler;
    }

    /**
     * @param connectTimeout
     *            the connectTimeout to set
     */
    @Required
    public void setConnectTimeout(final int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    /**
     * @param readTimeout
     *            the readTimeout to set
     */
    @Required
    public void setReadTimeout(final int readTimeout) {
        this.readTimeout = readTimeout;
    }

}
