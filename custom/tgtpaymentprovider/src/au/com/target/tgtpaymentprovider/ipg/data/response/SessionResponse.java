/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data.response;

/**
 * @author mjanarth
 *
 */
public class SessionResponse {

    private boolean sessionStored;

    private String sessionStoredError;

    private String secureSessionToken;

    /**
     * @return the sessionStored
     */
    public boolean isSessionStored() {
        return sessionStored;
    }

    /**
     * @param sessionStored
     *            the sessionStored to set
     */
    public void setSessionStored(final boolean sessionStored) {
        this.sessionStored = sessionStored;
    }

    /**
     * @return the sessionStoredError
     */
    public String getSessionStoredError() {
        return sessionStoredError;
    }

    /**
     * @param sessionStoredError
     *            the sessionStoredError to set
     */
    public void setSessionStoredError(final String sessionStoredError) {
        this.sessionStoredError = sessionStoredError;
    }

    /**
     * @return the secureSessionToken
     */
    public String getSecureSessionToken() {
        return secureSessionToken;
    }

    /**
     * @param secureSessionToken
     *            the secureSessionToken to set
     */
    public void setSecureSessionToken(final String secureSessionToken) {
        this.secureSessionToken = secureSessionToken;
    }


}
