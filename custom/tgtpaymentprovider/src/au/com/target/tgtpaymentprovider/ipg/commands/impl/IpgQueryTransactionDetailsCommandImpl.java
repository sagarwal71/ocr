/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.commands.impl;

import de.hybris.platform.payment.AdapterException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.tgtpayment.commands.TargetQueryTransactionDetailsCommand;
import au.com.target.tgtpayment.commands.request.TargetQueryTransactionDetailsRequest;
import au.com.target.tgtpayment.commands.result.TargetCardPaymentResult;
import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpaymentprovider.ipg.data.CardDetails;
import au.com.target.tgtpaymentprovider.ipg.data.CardResultData;
import au.com.target.tgtpaymentprovider.ipg.data.CardResultsData;
import au.com.target.tgtpaymentprovider.ipg.data.SavedCardsData;
import au.com.target.tgtpaymentprovider.ipg.data.TransactionDetails;
import au.com.target.tgtpaymentprovider.ipg.data.response.TransactionsResultResponse;
import au.com.target.tgtpaymentprovider.ipg.exception.IpgServiceException;


/**
 * Query transaction details from IPG
 */
public class IpgQueryTransactionDetailsCommandImpl extends AbstractIpgCommand implements
        TargetQueryTransactionDetailsCommand {

    private static final String SUCCESS = "1"; //Successful
    private static final String IN_PROGRESS = "2"; //Session still in progress
    private static final String DEFAULT_CARD = "1";
    private static final String CANCEL = "4"; // Cancel

    @Override
    public TargetQueryTransactionDetailsResult perform(final TargetQueryTransactionDetailsRequest paramRequest) {
        final TargetQueryTransactionDetailsResult result;
        try {
            final TransactionsResultResponse transactionsResultResponse = getIpgService().queryTransactionResults(
                    paramRequest.getSessionId(), paramRequest.getSessionToken());

            result = mapResponse(transactionsResultResponse);
        }
        catch (final IpgServiceException e) {
            throw new AdapterException(e);
        }
        return result;
    }

    /**
     * 
     * @param transactionsResultResponse
     * @return TargetQueryTransactionDetailsResult
     */
    protected TargetQueryTransactionDetailsResult mapResponse(
            final TransactionsResultResponse transactionsResultResponse) {
        TargetQueryTransactionDetailsResult result = null;

        if (transactionsResultResponse != null) {
            result = new TargetQueryTransactionDetailsResult();
            //meta information
            result.setSessionId(transactionsResultResponse.getSessionId());
            result.setSessionToken(transactionsResultResponse.getSessionToken());
            result.setSessionKey(transactionsResultResponse.getSessionKey());
            result.setCustReference(transactionsResultResponse.getCustReference());
            result.setCustNumber(transactionsResultResponse.getCustomerNumber());

            // something went wrong when retrieving transaction details
            if (StringUtils.isNotEmpty(transactionsResultResponse.getQueryResult())) {
                result.setSuccess(false);
                result.setErrorDescription(transactionsResultResponse.getQueryError());
            }
            else {
                // transaction retrieval successful 
                result.setTrnResult(transactionsResultResponse.getTransactionResult());

                if (StringUtils.isNotEmpty(transactionsResultResponse.getTransactionResult())) {
                    // successful transaction
                    result.setSuccess(transactionsResultResponse.getTransactionResult().equals(SUCCESS));

                    // cancel transaction
                    result.setCancel(transactionsResultResponse.getTransactionResult().equals(CANCEL));

                    // Total payment hasn't been completed yet. Query transaction details from IPG before completing the payment
                    result.setPending(transactionsResultResponse.getTransactionResult().equals(IN_PROGRESS));
                }

                if (transactionsResultResponse.getAmount() != null) {
                    final BigDecimal returnedAmountInCents = new BigDecimal(transactionsResultResponse.getAmount());
                    final BigDecimal amount = returnedAmountInCents.divide(new BigDecimal("100"));
                    result.setAmount(amount);
                }

                final CardResultsData cardResultsData = transactionsResultResponse.getCardResults();
                if (cardResultsData != null) {
                    final List<CardResultData> cardResults = cardResultsData.getCardResults();

                    if (CollectionUtils.isNotEmpty(cardResults)) {
                        for (final CardResultData cardResultData : cardResults) {
                            final TargetCardResult cardResult = new TargetCardResult();

                            final CardDetails cardDetails = cardResultData.getCardDetails();
                            if (cardDetails != null) {
                                populateCardDetails(cardResult, cardDetails);
                            }

                            final TransactionDetails transactionDetails = cardResultData.getTransactionDetails();
                            if (transactionDetails != null) {
                                final TargetCardPaymentResult cardPaymentResult = new TargetCardPaymentResult();

                                if (transactionDetails.getAmount() != null) {
                                    final BigDecimal amountInCents = new BigDecimal(transactionDetails.getAmount());
                                    final BigDecimal cardAmount = amountInCents.divide(new BigDecimal("100"));
                                    cardPaymentResult.setAmount(cardAmount);
                                }

                                cardPaymentResult.setReceiptNumber(transactionDetails.getReceiptNumber());
                                cardPaymentResult.setResponseCode(transactionDetails.getResponseCode());
                                cardPaymentResult.setResponseText(transactionDetails.getResponseText());
                                cardPaymentResult.setSettlementDate(transactionDetails.getSettlementDate());
                                cardPaymentResult.setTransactionDateTime(transactionDetails.getTransDateTime());
                                cardPaymentResult.setTransactionType(transactionDetails.getTransType());

                                cardResult.setTargetCardPaymentResult(cardPaymentResult);
                            }
                            result.addCardResult(cardResult);
                        }
                    }
                }
                final List<TargetCardResult> ipgSavedCards = populateSavedCards(transactionsResultResponse);
                result.setSavedCards(ipgSavedCards);
            }
        }
        return result;
    }

    /**
     * Populates saved cards
     * 
     * @return List
     */
    protected List<TargetCardResult> populateSavedCards(final TransactionsResultResponse response) {
        final SavedCardsData savedCardsReturned = response.getSavedCards();
        if (null != savedCardsReturned) {
            final List<TargetCardResult> savedCards = new ArrayList<>();

            if (CollectionUtils.isNotEmpty(savedCardsReturned.getCardRequests())) {
                for (final CardDetails cardDetails : savedCardsReturned.getCardRequests()) {
                    final TargetCardResult cardResult = new TargetCardResult();
                    populateCardDetails(cardResult, cardDetails);
                    savedCards.add(cardResult);
                }
                return savedCards;
            }
        }
        return null;
    }

    /**
     * 
     * @param cardResult
     * @param cardDetails
     */
    private void populateCardDetails(final TargetCardResult cardResult, final CardDetails cardDetails) {
        cardResult.setCardType(cardDetails.getCardType());
        cardResult.setCardNumber(cardDetails.getCardNumber());
        cardResult.setCardExpiry(cardDetails.getCardExpiry());
        cardResult.setBin(cardDetails.getBin());
        cardResult.setPromoId(cardDetails.getPromoId());
        cardResult.setToken(cardDetails.getToken());
        cardResult.setTokenExpiry(cardDetails.getTokenExpiry());
        if (StringUtils.isNotEmpty(cardDetails.getIsDefaultCard())) {
            cardResult.setDefaultCard(cardDetails.getIsDefaultCard().equals(DEFAULT_CARD));
        }
    }
}