/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author htan3
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class UserDefined {
    @XmlElement(name = "CCExpiry")
    private String ccExpiry;
    @XmlElement(name = "Reference1")
    private String reference1;
    @XmlElement(name = "Reference2")
    private String reference2;
    @XmlElement(name = "Reference3")
    private String reference3;
    @XmlElement(name = "TokenExpiry")
    private String tokenExpiry;
    @XmlElement(name = "BIN")
    private String bin;
    @XmlElement(name = "Token")
    private String token;
    @XmlElement(name = "POSCond")
    private String posCond;
    @XmlElement(name = "StoreID")
    private String storeId;
    @XmlElement(name = "PromoID")
    private String promoId;

    /**
     * @return the ccExpiry
     */
    public String getCcExpiry() {
        return ccExpiry;
    }

    /**
     * @param ccExpiry
     *            the ccExpiry to set
     */
    public void setCcExpiry(final String ccExpiry) {
        this.ccExpiry = ccExpiry;
    }

    /**
     * @return the reference1
     */
    public String getReference1() {
        return reference1;
    }

    /**
     * @param reference1
     *            the reference1 to set
     */
    public void setReference1(final String reference1) {
        this.reference1 = reference1;
    }

    /**
     * @return the reference2
     */
    public String getReference2() {
        return reference2;
    }

    /**
     * @param reference2
     *            the reference2 to set
     */
    public void setReference2(final String reference2) {
        this.reference2 = reference2;
    }

    /**
     * @return the reference3
     */
    public String getReference3() {
        return reference3;
    }

    /**
     * @param reference3
     *            the reference3 to set
     */
    public void setReference3(final String reference3) {
        this.reference3 = reference3;
    }

    /**
     * @return the tokenExpiry
     */
    public String getTokenExpiry() {
        return tokenExpiry;
    }

    /**
     * @param tokenExpiry
     *            the tokenExpiry to set
     */
    public void setTokenExpiry(final String tokenExpiry) {
        this.tokenExpiry = tokenExpiry;
    }

    /**
     * @return the bin
     */
    public String getBin() {
        return bin;
    }

    /**
     * @param bin
     *            the bin to set
     */
    public void setBin(final String bin) {
        this.bin = bin;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token
     *            the token to set
     */
    public void setToken(final String token) {
        this.token = token;
    }

    /**
     * @return the posCond
     */
    public String getPosCond() {
        return posCond;
    }

    /**
     * @param posCond
     *            the posCond to set
     */
    public void setPosCond(final String posCond) {
        this.posCond = posCond;
    }

    /**
     * @return the storeId
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     * @param storeId
     *            the storeId to set
     */
    public void setStoreId(final String storeId) {
        this.storeId = storeId;
    }

    /**
     * @return the promoId
     */
    public String getPromoId() {
        return promoId;
    }

    /**
     * @param promoId
     *            the promoId to set
     */
    public void setPromoId(final String promoId) {
        this.promoId = promoId;
    }

}
