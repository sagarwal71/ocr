/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author mjanarth
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ReDShieldFraudData {

    @XmlElement(name = "ReDOrderNumber")
    private String reDOrderNumber;
    @XmlElement(name = "FRAUD_STAT_CD")
    private String fraudStatusCd;
    @XmlElement(name = "REQ_ID")
    private String requestId;
    @XmlElement(name = "ORD_ID")
    private String orderId;
    @XmlElement(name = "STAT_CD")
    private String statCd;
    @XmlElement(name = "FRAUD_RSP_CD")
    private String fraudRspCd;

    /**
     * @return the reDOrderNumber
     */
    public String getReDOrderNumber() {
        return reDOrderNumber;
    }

    /**
     * @param reDOrderNumber
     *            the reDOrderNumber to set
     */
    public void setReDOrderNumber(final String reDOrderNumber) {
        this.reDOrderNumber = reDOrderNumber;
    }

    /**
     * @return the fraudStatusCd
     */
    public String getFraudStatusCd() {
        return fraudStatusCd;
    }

    /**
     * @param fraudStatusCd
     *            the fraudStatusCd to set
     */
    public void setFraudStatusCd(final String fraudStatusCd) {
        this.fraudStatusCd = fraudStatusCd;
    }

    /**
     * @return the requestId
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * @param requestId
     *            the requestId to set
     */
    public void setRequestId(final String requestId) {
        this.requestId = requestId;
    }

    /**
     * @return the orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * @param orderId
     *            the orderId to set
     */
    public void setOrderId(final String orderId) {
        this.orderId = orderId;
    }

    /**
     * @return the statCd
     */
    public String getStatCd() {
        return statCd;
    }

    /**
     * @param statCd
     *            the statCd to set
     */
    public void setStatCd(final String statCd) {
        this.statCd = statCd;
    }

    /**
     * @return the fraudRspCd
     */
    public String getFraudRspCd() {
        return fraudRspCd;
    }

    /**
     * @param fraudRspCd
     *            the fraudRspCd to set
     */
    public void setFraudRspCd(final String fraudRspCd) {
        this.fraudRspCd = fraudRspCd;
    }

}
