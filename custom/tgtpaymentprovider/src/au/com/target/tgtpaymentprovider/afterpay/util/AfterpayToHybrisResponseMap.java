/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.util;

import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.util.HashMap;
import java.util.Map;


/**
 * @author bpottass
 *
 */
public final class AfterpayToHybrisResponseMap {

    private static Map<String, TransactionStatus> resultMap = new HashMap<>();

    private static Map<String, TransactionStatusDetails> responseMap = new HashMap<>();


    private AfterpayToHybrisResponseMap() {

    }


    static {
        resultMap.put("APPROVED", TransactionStatus.ACCEPTED);
        resultMap.put("DECLINED", TransactionStatus.REJECTED);

        responseMap.put("APPROVED", TransactionStatusDetails.SUCCESFULL);
        responseMap.put("DECLINED", TransactionStatusDetails.BANK_DECLINE);

    }

    /**
     * Return {@link TransactionStatusDetails} for given Afterpay response status
     * 
     * @param status
     * @return transaction status details
     */
    public static TransactionStatusDetails getTransactionStatusDetails(final String status) {
        return responseMap.get(status);
    }

    /**
     * Return {@link TransactionStatus} for given Afterpay response status
     * 
     * @param status
     * @return transaction status
     */
    public static TransactionStatus getTransactionStatus(final String status) {
        return resultMap.get(status);
    }




}
