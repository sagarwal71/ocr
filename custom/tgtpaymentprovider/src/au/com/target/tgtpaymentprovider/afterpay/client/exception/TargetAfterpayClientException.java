/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.client.exception;

/**
 * @author bhuang3
 *
 */
public class TargetAfterpayClientException extends Exception {

    public TargetAfterpayClientException() {
        super();
    }

    public TargetAfterpayClientException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public TargetAfterpayClientException(final String message) {
        super(message);
    }

    public TargetAfterpayClientException(final Throwable cause) {
        super(cause);
    }

}
