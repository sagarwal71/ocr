/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.client;

import au.com.target.tgtpaymentprovider.afterpay.client.exception.TargetAfterpayClientException;
import au.com.target.tgtpaymentprovider.afterpay.data.request.CapturePaymentRequest;
import au.com.target.tgtpaymentprovider.afterpay.data.request.CreateOrderRequest;
import au.com.target.tgtpaymentprovider.afterpay.data.request.CreateRefundRequest;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CapturePaymentResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CreateOrderResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CreateRefundResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetConfigurationResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetOrderResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetPaymentResponse;


/**
 * @author bhuang3
 *
 */
public interface TargetAfterpayClient {

    GetConfigurationResponse getConfiguration() throws TargetAfterpayClientException;

    boolean isAfterpayConnectionAvailable();

    /**
     * Create Order
     * 
     * @param request
     * @return {@link CreateOrderResponse}
     * @throws TargetAfterpayClientException
     * @see <a href="http://docs.afterpay.com.au/merchant-api-v1.html#create-order">Create Order</a>
     */
    CreateOrderResponse createOrder(CreateOrderRequest request) throws TargetAfterpayClientException;

    /**
     * 
     * @param request
     * @return (@link CapturePaymentResponse}
     * @throws TargetAfterpayClientException
     * @see <a href="https://docs.afterpay.com.au/merchant-api-v1.html#direct-capture-payment">Capture Payment</a>
     */
    CapturePaymentResponse capturePayment(CapturePaymentRequest request) throws TargetAfterpayClientException;

    /**
     * get order details from Afterpay to re-confirm before taking payment
     * 
     * @param afterpayToken
     * @return - {@link GetOrderResponse}
     * @throws TargetAfterpayClientException
     */
    GetOrderResponse getOrder(String afterpayToken) throws TargetAfterpayClientException;

    /**
     * get the payment details using the afterpay token
     *
     * @param afterpayToken
     * @return {@link GetPaymentResponse}
     * @throws TargetAfterpayClientException
     * @see <a href="https://docs.afterpay.com.au/merchant-api-v1.html#get-payment">Get Payment</a>
     */
    GetPaymentResponse getPayment(String afterpayToken) throws TargetAfterpayClientException;

    /**
     * perform a full or partial refund
     * 
     * @param paymentId
     * @param request
     * @return {@link CreateRefundResponse}
     * @throws TargetAfterpayClientException
     * @see <a href="https://docs.afterpay.com.au/merchant-api-v1.html#create-refund">Create Refund</a>
     */
    CreateRefundResponse createRefund(String paymentId, CreateRefundRequest request)
            throws TargetAfterpayClientException;

}
