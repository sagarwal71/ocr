/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.exception;

/**
 * @author vmuthura
 * 
 */
public class InvalidProfileException extends Exception
{
    /**
     * 
     */
    public InvalidProfileException()
    {
        super();
    }

    /**
     * @param message
     *            - message explaining the cause
     */
    public InvalidProfileException(final String message)
    {
        super(message);
    }

    /**
     * @param message
     *            - message explaining the cause
     * @param exception
     *            - the root cause
     */
    public InvalidProfileException(final String message, final Exception exception)
    {
        super(message, exception);
    }
}
