package au.com.target.tgtpaymentprovider.paypal.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.fest.util.Collections;
import org.springframework.beans.factory.annotation.Required;

import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentReq;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentResponseType;
import urn.ebay.api.PayPalAPI.DoReferenceTransactionReq;
import urn.ebay.api.PayPalAPI.DoReferenceTransactionResponseType;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsReq;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsResponseType;
import urn.ebay.api.PayPalAPI.GetTransactionDetailsReq;
import urn.ebay.api.PayPalAPI.GetTransactionDetailsResponseType;
import urn.ebay.api.PayPalAPI.PayPalAPIInterfaceServiceService;
import urn.ebay.api.PayPalAPI.RefundTransactionReq;
import urn.ebay.api.PayPalAPI.RefundTransactionResponseType;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutReq;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutResponseType;
import urn.ebay.apis.eBLBaseComponents.AckCodeType;
import urn.ebay.apis.eBLBaseComponents.ErrorType;
import au.com.target.tgtpayment.exceptions.BadFundingMethodException;
import au.com.target.tgtpaymentprovider.constants.TgtpaymentproviderConstants;
import au.com.target.tgtpaymentprovider.paypal.PayPalService;
import au.com.target.tgtpaymentprovider.paypal.exception.InvalidRequestException;
import au.com.target.tgtpaymentprovider.paypal.exception.PayPalServiceException;
import au.com.target.tgtpaymentprovider.paypal.exception.ValidationFailedException;
import au.com.target.tgtpaymentprovider.paypal.util.PayPalErrorCodeEnum;


public class PayPalServiceImpl implements PayPalService
{

    private static final Logger LOG = Logger.getLogger(PayPalServiceImpl.class);

    private PayPalAPIInterfaceServiceService payPalAPIInterfaceServiceService;

    @Override
    public SetExpressCheckoutResponseType setExpressCheckout(final SetExpressCheckoutReq request)
            throws InvalidRequestException, ValidationFailedException, PayPalServiceException
    {
        SetExpressCheckoutResponseType response = null;
        try {
            response = payPalAPIInterfaceServiceService.setExpressCheckout(request);
        }
        catch (final Exception ex) {
            throw new PayPalServiceException(ex);
        }

        if (response != null) {
            handleError(response.getAck(), response.getErrors());
        }

        return response;
    }

    @Override
    public GetExpressCheckoutDetailsResponseType getCheckoutDetails(
            final GetExpressCheckoutDetailsReq request)
            throws InvalidRequestException, ValidationFailedException,
            PayPalServiceException {

        GetExpressCheckoutDetailsResponseType response = null;
        try {
            response = payPalAPIInterfaceServiceService.getExpressCheckoutDetails(request);
        }
        catch (final Exception ex) {
            throw new PayPalServiceException(ex);
        }

        handleError(response.getAck(), response.getErrors());

        return response;
    }

    @Override
    public DoExpressCheckoutPaymentResponseType doExpressCheckout(final DoExpressCheckoutPaymentReq request)
            throws InvalidRequestException, ValidationFailedException, PayPalServiceException
    {
        DoExpressCheckoutPaymentResponseType response = null;
        try {
            response = payPalAPIInterfaceServiceService.doExpressCheckoutPayment(request);
        }
        catch (final Exception ex) {
            throw new PayPalServiceException(ex);
        }

        handleError(response.getAck(), response.getErrors());

        return response;
    }

    @Override
    public RefundTransactionResponseType refund(final RefundTransactionReq request)
            throws InvalidRequestException,
            ValidationFailedException, PayPalServiceException
    {
        RefundTransactionResponseType response = null;
        try {
            response = payPalAPIInterfaceServiceService.refundTransaction(request);
        }
        catch (final Exception ex) {
            throw new PayPalServiceException(ex);
        }

        handleError(response.getAck(), response.getErrors());

        return response;
    }

    @Override
    public DoReferenceTransactionResponseType performReferenceTransaction(final Object request)
            throws InvalidRequestException,
            ValidationFailedException, PayPalServiceException
    {
        DoReferenceTransactionResponseType response = null;
        try {
            response = payPalAPIInterfaceServiceService.doReferenceTransaction((DoReferenceTransactionReq)request);
        }
        catch (final Exception ex) {
            throw new PayPalServiceException(ex);
        }

        handleError(response.getAck(), response.getErrors());

        return response;
    }

    @Override
    public GetTransactionDetailsResponseType retrieve(final GetTransactionDetailsReq request)
            throws InvalidRequestException, ValidationFailedException, PayPalServiceException
    {
        GetTransactionDetailsResponseType response = null;
        try {
            response = payPalAPIInterfaceServiceService.getTransactionDetails(request);
        }
        catch (final Exception ex) {
            throw new PayPalServiceException(ex);
        }

        handleError(response.getAck(), response.getErrors());

        return response;
    }

    /**
     * @param ackCode
     *            the ackCode from the response
     * @param errors
     *            the errors from the response
     * 
     * @throws InvalidRequestException
     *             - if the request is missing parameters
     * @throws ValidationFailedException
     *             - if any of the data is invalid
     * @throws PayPalServiceException
     *             - if there is an internal error/other specific paypal errorcodes
     */
    private void handleError(final AckCodeType ackCode, final List<ErrorType> errors) throws InvalidRequestException,
            ValidationFailedException, PayPalServiceException
    {
        if (ackCode == null || AckCodeType.SUCCESS.equals(ackCode))
        {
            return;
        }
        else if (AckCodeType.SUCCESSWITHWARNING.equals(ackCode))
        {
            LOG.warn(getConsolidatedErrorMessage(errors));
        }
        else
        {
            if (Collections.isEmpty(errors))
            {
                LOG.warn("Got AckCode as: " + ackCode + ", with no errors");
                return;
            }
            //Pick-up the first error and throw an exception, with consolidated messages for *all* the errors
            final PayPalErrorCodeEnum errorCodeEnum = PayPalErrorCodeEnum.fromCode(errors.get(0).getErrorCode()
                    .toString());
            if (errorCodeEnum == null)
            {
                throw new PayPalServiceException(getConsolidatedErrorMessage(errors));
            }

            translateErrorToException(errorCodeEnum, getConsolidatedErrorMessage(errors));
        }
    }

    /**
     * Translates the certain paypal error codes to the various exceptions. Other codes are simply wrapped into a
     * {@link PayPalServiceException}
     * 
     * @param errorCodeEnum
     *            - {@link PayPalErrorCodeEnum}
     * @param errorMessage
     *            - message, to be part of the exception
     * @throws InvalidRequestException
     *             - for the following error codes {@link PayPalErrorCodeEnum#AUTH_FAILED},
     *             {@link PayPalErrorCodeEnum#VERSION_ERROR}, {@link PayPalErrorCodeEnum#SECURITY_ERROR},
     *             {@link PayPalErrorCodeEnum#API_CALL_RATE_LIMITED}, {@link PayPalErrorCodeEnum#UNAUTHORIZED_ACCESS},
     *             {@link PayPalErrorCodeEnum#MISSING_ARGUMENT}, {@link PayPalErrorCodeEnum#MISSING_PARAMETER_UNKNOWN},
     *             {@link PayPalErrorCodeEnum#UNSPECIFIED_METHOD}, {@link PayPalErrorCodeEnum#MISSING_PARAMETER},
     *             {@link PayPalErrorCodeEnum#EC_TOKEN_EXPIRED}, {@link PayPalErrorCodeEnum#BUYER_HAS_NO_FUNDS},
     *             {@link PayPalErrorCodeEnum#PERMISSION_DENIED}, {@link PayPalErrorCodeEnum#TXN_IN_PROGRESS_FOR_TOKEN},
     *             {@link PayPalErrorCodeEnum#DUPLICATE_INVOICE}, {@link PayPalErrorCodeEnum#UNSUPPORTED_OPTION},
     *             {@link PayPalErrorCodeEnum#TXN_REFUSED}
     * @throws ValidationFailedException
     *             - for the following errorcodes {@link PayPalErrorCodeEnum#INVALID_PARAMETER_UNKNOWN},
     *             {@link PayPalErrorCodeEnum#INVALID_PARAMETER}, {@link PayPalErrorCodeEnum#CURRENCY_NOT_SUPPORTED},
     *             {@link PayPalErrorCodeEnum#EC_TOKEN_MISSING}, {@link PayPalErrorCodeEnum#INVALID_ARGUMENT},
     *             {@link PayPalErrorCodeEnum#EC_TOKEN_INVLAID}, {@link PayPalErrorCodeEnum#PAYERID_INVALID},
     *             {@link PayPalErrorCodeEnum#INVALID_TXN_ID}
     * @throws PayPalServiceException
     *             - for the following errorcodes {@link PayPalErrorCodeEnum#INTERNAL_ERROR},
     *             {@link PayPalErrorCodeEnum#INTERNAL_ERROR_VALIDATION},
     *             {@link PayPalErrorCodeEnum#API_TEMPORARILY_NOT_AVAILABLE} and all other paypal errorcodes not covered
     *             by {@link PayPalErrorCodeEnum}
     */
    private void translateErrorToException(final PayPalErrorCodeEnum errorCodeEnum, final String errorMessage)
            throws InvalidRequestException, ValidationFailedException, PayPalServiceException
    {
        switch (errorCodeEnum)
        {
            case AUTH_FAILED:
            case VERSION_ERROR:
            case SECURITY_ERROR:
            case API_CALL_RATE_LIMITED:
            case UNAUTHORIZED_ACCESS:
            case MISSING_ARGUMENT:
            case MISSING_PARAMETER_UNKNOWN:
            case UNSPECIFIED_METHOD:
            case MISSING_PARAMETER:
            case EC_TOKEN_EXPIRED:
            case BUYER_HAS_NO_FUNDS:
            case PERMISSION_DENIED:
            case TXN_IN_PROGRESS_FOR_TOKEN:
            case DUPLICATE_INVOICE:
            case UNSUPPORTED_OPTION:
            case TXN_REFUSED:
            {
                LOG.error(TgtpaymentproviderConstants.ERR_PAYPAL_INVALIDREQUEST + " " + errorMessage);
                throw new InvalidRequestException(errorMessage);
            }
            case BAD_FUNDING_METHOD:
            {
                LOG.error(TgtpaymentproviderConstants.ERR_PAYPAL_INVALIDREQUEST + " " + errorMessage);
                throw new BadFundingMethodException(errorMessage);
            }
            case INVALID_PARAMETER_UNKNOWN:
            case INVALID_PARAMETER:
            case CURRENCY_NOT_SUPPORTED:
            case EC_TOKEN_MISSING:
            case INVALID_ARGUMENT:
            case EC_TOKEN_INVLAID:
            case PAYERID_INVALID:
            case INVALID_TXN_ID:
            {
                LOG.error(TgtpaymentproviderConstants.ERR_PAYPAL_VALIDATION + " " + errorMessage);
                throw new ValidationFailedException(errorMessage);
            }

            case INTERNAL_ERROR:
            case INTERNAL_ERROR_VALIDATION:
            case API_TEMPORARILY_NOT_AVAILABLE:

            default:
            {
                LOG.error(TgtpaymentproviderConstants.ERR_PAYPAL_SERVER_FAILED + " " + errorMessage);
                throw new PayPalServiceException(errorMessage);
            }
        }

    }

    /**
     * @param errors
     *            - Array of errors {@link ErrorType}
     * @return - Consolidated message, with paypal errorcode and message
     */
    private String getConsolidatedErrorMessage(final List<ErrorType> errors)
    {
        final StringBuilder builder = new StringBuilder();
        for (final ErrorType error : errors)
        {
            builder.append(error.getErrorCode());
            builder.append(": ");
            builder.append(error.getShortMessage());
            builder.append("; ");
            builder.append(error.getLongMessage());
            builder.append('\n');
        }

        return builder.toString();
    }

    /**
     * @param payPalAPIInterfaceServiceService
     *            the payPalAPIInterfaceServiceService to set
     */
    @Required
    public void setPayPalAPIInterfaceServiceService(
            final PayPalAPIInterfaceServiceService payPalAPIInterfaceServiceService) {
        this.payPalAPIInterfaceServiceService = payPalAPIInterfaceServiceService;
    }
}