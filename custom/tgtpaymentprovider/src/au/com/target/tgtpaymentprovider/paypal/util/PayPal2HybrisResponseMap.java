/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.util;

import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.util.HashMap;
import java.util.Map;

import urn.ebay.apis.eBLBaseComponents.AckCodeType;
import urn.ebay.apis.eBLBaseComponents.PaymentStatusCodeType;


/**
 * @author bjames4
 * 
 */

public final class PayPal2HybrisResponseMap
{

    //CHECKSTYLE:OFF
    PaymentStatusCodeType ps;
    private static Map<PaymentStatusCodeType, TransactionStatusDetails> responseMap = new HashMap<PaymentStatusCodeType, TransactionStatusDetails>();
    private static Map<AckCodeType, TransactionStatus> resultMap = new HashMap<AckCodeType, TransactionStatus>();

    //CHECKSTYLE:ON

    /**
     * 
     */
    private PayPal2HybrisResponseMap()
    {
        //Avoid instantiation
    }

    static
    {
        // Init response map
        responseMap.put(PaymentStatusCodeType.COMPLETED, TransactionStatusDetails.SUCCESFULL); //Completed 
        responseMap.put(PaymentStatusCodeType.FAILED, TransactionStatusDetails.BANK_DECLINE); //Failed
        responseMap.put(PaymentStatusCodeType.PENDING, TransactionStatusDetails.COMMUNICATION_PROBLEM);//Pending
        responseMap.put(PaymentStatusCodeType.DENIED, TransactionStatusDetails.BANK_DECLINE);//Denied
        responseMap.put(PaymentStatusCodeType.EXPIRED, TransactionStatusDetails.TIMEOUT);//Expired

        // Init resultMap
        resultMap.put(AckCodeType.SUCCESS, TransactionStatus.ACCEPTED);
        resultMap.put(AckCodeType.SUCCESSWITHWARNING, TransactionStatus.ACCEPTED);
        resultMap.put(AckCodeType.FAILURE, TransactionStatus.REJECTED);
        resultMap.put(AckCodeType.FAILUREWITHWARNING, TransactionStatus.REJECTED);
        resultMap.put(AckCodeType.PARTIALSUCCESS, TransactionStatus.REVIEW);
    }

    /**
     * @param paymentStatusCodeType
     *            - {@link PaymentStatusCodeType}
     * @return -
     */
    public static TransactionStatusDetails getHybrisTransactionStatusDetails(
            final PaymentStatusCodeType paymentStatusCodeType)
    {
        return responseMap.get(paymentStatusCodeType);
    }

    /**
     * @param ackCodeType
     *            -
     * @return -
     */
    public static TransactionStatus getHybrisTransactionStatus(final AckCodeType ackCodeType)
    {
        return resultMap.get(ackCodeType);
    }
}
