/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.commands.impl;

import de.hybris.platform.payment.AdapterException;

import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpaymentprovider.tns.data.response.CreateSessionResponse;
import au.com.target.tgtpaymentprovider.tns.exception.TnsServiceException;
import au.com.target.tgtpaymentprovider.tns.util.Tns2HybrisResponseMap;


/**
 * @author vmuthura
 * 
 */
public class TnsCreateSubcriptionCommandImpl extends AbstractTnsCommand implements TargetCreateSubscriptionCommand
{

    @Override
    public TargetCreateSubscriptionResult perform(final TargetCreateSubscriptionRequest targetCreateSubscriptionRequest)
    {
        final TargetCreateSubscriptionResult result = new TargetCreateSubscriptionResult();
        try
        {
            final CreateSessionResponse response = getTnsService().createSession();
            result.setTransactionStatus(Tns2HybrisResponseMap.getHybrisTransactionStatus(response.getResult()));
            result.setRequestToken(response.getSession());
        }
        catch (final TnsServiceException e) {
            throw new AdapterException(e);
        }

        return result;
    }

}
