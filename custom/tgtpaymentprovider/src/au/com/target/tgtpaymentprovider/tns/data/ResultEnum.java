/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data;

/**
 * @author bjames4
 * 
 */
public enum ResultEnum {

    SUCCESS, //The operation was successfully processed
    PENDING, //The operation is currently in progress or pending processing
    FAILURE, //The operation was declined or rejected by the gateway, acquirer or issuer
    UNKNOWN, //The result of the operation is unknown
    ERROR;
}
