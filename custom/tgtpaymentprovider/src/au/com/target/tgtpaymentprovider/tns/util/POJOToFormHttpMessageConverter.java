/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.util.Assert;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;


/**
 * @author vmuthura
 * 
 */
public class POJOToFormHttpMessageConverter<T> extends AbstractHttpMessageConverter<T> {

    public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");
    private ObjectMapper objectMapper;

    /**
     * @param objectMapper
     *            - Jackson {@link ObjectMapper}
     */
    public void setObjectMapper(final ObjectMapper objectMapper) {
        Assert.notNull(objectMapper, "ObjectMapper must not be null");
        this.objectMapper = objectMapper;
    }

    /**
     * @return {@link ObjectMapper} - Jackson object mapper
     */
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    /**
     * @param clazz
     *            - Class object
     * @return - {@link JavaType}
     */
    protected JavaType getJavaType(final Class clazz) {
        return TypeFactory.defaultInstance().constructType(clazz);
    }

    @Override
    public boolean canRead(final Class clazz, final MediaType mediaType) {
        final JavaType javaType = getJavaType(clazz);

        return objectMapper.canDeserialize(javaType) && canRead(mediaType);
    }

    @Override
    public boolean canWrite(final Class clazz, final MediaType mediaType) {

        return objectMapper.canSerialize(clazz) && canWrite(mediaType);
    }

    @Override
    protected boolean supports(final Class clazz) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected T readInternal(final Class<? extends T> responseType, final HttpInputMessage inputmessage)
            throws IOException, HttpMessageNotReadableException {
        final JavaType javaType = getJavaType(responseType);
        try {
            final Map map = readUrlEncodedFormIntoMap(inputmessage, DEFAULT_CHARSET);
            return objectMapper.convertValue(map, javaType);
        }
        catch (final JsonProcessingException ex) {
            throw new HttpMessageNotReadableException("Could not read JSON: ", ex);
        }
    }

    @Override
    protected void writeInternal(final T object, final HttpOutputMessage outputMessage) throws IOException,
            HttpMessageNotWritableException {
        try {
            final Map<String, String> valuesMap = objectMapper.convertValue(object, Map.class);
            writeForm(valuesMap, MediaType.APPLICATION_FORM_URLENCODED, outputMessage);
        }
        catch (final JsonProcessingException ex) {
            throw new HttpMessageNotWritableException("Could not write JSON: ", ex);
        }

    }

    /**
     * @param form
     *            - Form data in a {@link Map}
     * @param contentType
     *            - content type {@link MediaType}
     * @param outputMessage
     *            - {@link HttpOutputMessage}
     * @throws IOException
     *             -
     */
    private void writeForm(final Map form, final MediaType contentType, final HttpOutputMessage outputMessage)
            throws IOException {
        final Charset charset;
        if (contentType == null) {
            outputMessage.getHeaders().setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            charset = DEFAULT_CHARSET;
        }
        else {
            outputMessage.getHeaders().setContentType(contentType);
            charset = contentType.getCharset() == null ? DEFAULT_CHARSET : contentType.getCharset();
        }

        final StringBuilder builder = new StringBuilder();
        for (final Iterator nameIterator = form.keySet().iterator(); nameIterator.hasNext();) {
            final String name = (String)nameIterator.next();
            final String value = (String)form.get(name);
            builder.append(URLEncoder.encode(name, charset.name()));
            if (value != null) {
                builder.append('=');
                builder.append(URLEncoder.encode(value, charset.name()));
            }

            if (nameIterator.hasNext()) {
                builder.append('&');
            }
        }

        final byte bytes[] = builder.toString().getBytes(charset.name());
        outputMessage.getHeaders().setContentLength(bytes.length);
        FileCopyUtils.copy(bytes, outputMessage.getBody());
    }

    /**
     * @param inputMessage
     *            -
     * @param charset
     *            -
     * @return - Map of name-value pairs
     * @throws IOException
     *             -
     * @throws HttpMessageNotReadableException
     *             -
     */
    private Map<String, String> readUrlEncodedFormIntoMap(final HttpInputMessage inputMessage,
            final Charset charset) throws IOException, HttpMessageNotReadableException {
        final String body = FileCopyUtils.copyToString(new InputStreamReader(inputMessage.getBody(), charset));
        final String pairs[] = StringUtils.tokenizeToStringArray(body, "&");
        final Map<String, String> result = new HashMap<String, String>(pairs.length);
        for (int i = 0; i < pairs.length; i++) {
            final String pair = pairs[i];
            final int idx = pair.indexOf('=');
            if (idx == -1) {
                result.put(URLDecoder.decode(pair, charset.name()), null);
            }
            else {
                final String name = URLDecoder.decode(pair.substring(0, idx), charset.name());
                final String value = URLDecoder.decode(pair.substring(idx + 1), charset.name());
                result.put(name, value);
            }
        }

        return result;
    }

}
