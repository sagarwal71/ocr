/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data;

import javax.validation.constraints.Size;


/**
 * @author bjames4
 * 
 */
public class Acquirer {
    @Size(min = 1, max = 2048, message = "customData - between 1 and 2048 characters")
    private String customData;
    private String id;

    /**
     * @return the customData
     */
    public String getCustomData() {
        return customData;
    }

    /**
     * @param customData
     *            the customData to set
     */
    public void setCustomData(final String customData) {
        this.customData = customData;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final String id) {
        this.id = id;
    }
}
