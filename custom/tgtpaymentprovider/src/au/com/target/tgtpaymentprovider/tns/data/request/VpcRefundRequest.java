/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data.request;

import com.fasterxml.jackson.annotation.JsonProperty;



/**
 * @author vmuthura
 * 
 */
public class VpcRefundRequest extends AbstractVpcRequest
{
    @JsonProperty(value = "vpc_Amount")
    private String amount;

    @JsonProperty(value = "vpc_TransNo")
    private String transactionNumber;

    @JsonProperty(value = "vpc_MerchTxnRef")
    private String merchTxnRef;

    @JsonProperty(value = "vpc_Currency")
    private String currency;

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final String amount) {
        this.amount = amount;
    }

    /**
     * @return the transactionNumber
     */
    public String getTransactionNumber() {
        return transactionNumber;
    }

    /**
     * @param transactionNumber
     *            the transactionNumber to set
     */
    public void setTransactionNumber(final String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    /**
     * @return the merchTxnRef
     */
    public String getMerchTxnRef() {
        return merchTxnRef;
    }

    /**
     * @param merchTxnRef
     *            the merchTxnRef to set
     */
    public void setMerchTxnRef(final String merchTxnRef) {
        this.merchTxnRef = merchTxnRef;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency
     *            the currency to set
     */
    public void setCurrency(final String currency) {
        this.currency = currency;
    }

}
