/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data;

/**
 * @author bjames4
 * 
 */
public class SourceOfFundDetails {
    private Card card;

    /**
     * @return the card
     */
    public Card getCard() {
        return card;
    }

    /**
     * @param card
     *            the card to set
     */
    public void setCard(final Card card) {
        this.card = card;
    }
}
