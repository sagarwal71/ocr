/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data;

import javax.validation.constraints.Size;


/**
 * @author bjames4
 * 
 */
public class AddressDetails {
    @Size(min = 1, max = 100, message = "city - between 1 and 100 characters")
    private String city;

    @Size(min = 3, max = 3, message = "country - three digit country code")
    private String country;

    @Size(min = 1, max = 10, message = "postcodeZip - between 1 and 10 characters")
    private String postcodeZip;

    @Size(min = 1, max = 20, message = "stateProvince - between 1 and 20 characters")
    private String stateProvince;

    @Size(min = 1, max = 100, message = "street - between 1 and 100 characters")
    private String street;

    @Size(min = 1, max = 100, message = "street2 - between 1 and 100 characters")
    private String street2;

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city
     *            the city to set
     */
    public void setCity(final String city) {
        this.city = city;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country
     *            the country to set
     */
    public void setCountry(final String country) {
        this.country = country;
    }

    /**
     * @return the postcodeZip
     */
    public String getPostcodeZip() {
        return postcodeZip;
    }

    /**
     * @param postcodeZip
     *            the postcodeZip to set
     */
    public void setPostcodeZip(final String postcodeZip) {
        this.postcodeZip = postcodeZip;
    }

    /**
     * @return the stateProvince
     */
    public String getStateProvince() {
        return stateProvince;
    }

    /**
     * @param stateProvince
     *            the stateProvince to set
     */
    public void setStateProvince(final String stateProvince) {
        this.stateProvince = stateProvince;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street
     *            the street to set
     */
    public void setStreet(final String street) {
        this.street = street;
    }

    /**
     * @return the street2
     */
    public String getStreet2() {
        return street2;
    }

    /**
     * @param street2
     *            the street2 to set
     */
    public void setStreet2(final String street2) {
        this.street2 = street2;
    }
}
