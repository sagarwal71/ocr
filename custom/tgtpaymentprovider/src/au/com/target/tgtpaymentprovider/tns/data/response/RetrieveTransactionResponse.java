/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data.response;


/**
 * @author vmuthura
 * 
 *         Response wrapper for Retrieve Transaction response, for TNS. At the moment, it is same as
 *         {@link GenericTransactionResponse}, but any specific fields should be added here.
 */
public class RetrieveTransactionResponse extends GenericTransactionResponse
{
    //
}
