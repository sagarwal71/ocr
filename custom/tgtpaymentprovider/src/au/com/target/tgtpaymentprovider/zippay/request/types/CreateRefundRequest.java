/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.request.types;


import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author salexa10
 *
 */
public abstract class CreateRefundRequest {

    @JsonProperty("charge_id")
    abstract String getChargeId();
}
