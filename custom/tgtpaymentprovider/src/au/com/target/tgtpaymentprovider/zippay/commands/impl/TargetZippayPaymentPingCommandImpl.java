/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.commands.impl;

import au.com.target.tgtpayment.commands.TargetPaymentPingCommand;
import au.com.target.tgtpayment.commands.request.TargetPaymentPingRequest;
import au.com.target.tgtpayment.commands.result.TargetPaymentPingResult;


/**
 * @author salexa10
 *
 */
public class TargetZippayPaymentPingCommandImpl extends TargetAbstractZippayCommand
        implements TargetPaymentPingCommand {

    @Override
    public TargetPaymentPingResult perform(final TargetPaymentPingRequest request) {
        final boolean zippayConnectionAvailable = getTargetZippayClient().isZippayConnectionAvailable();

        final TargetPaymentPingResult result = new TargetPaymentPingResult();
        result.setSuccess(zippayConnectionAvailable);
        return result;
    }
}
