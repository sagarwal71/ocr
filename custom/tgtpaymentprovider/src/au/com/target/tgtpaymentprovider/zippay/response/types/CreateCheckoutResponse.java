/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.response.types;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author salexa10
 *
 */
@JsonIgnoreProperties(value = { "type", "shopper", "order", "metadata", "created", "state", "config" })
public abstract class CreateCheckoutResponse {
    // No properties required as we are trying to ignore the ones which are defined in json and not in POJO
}
