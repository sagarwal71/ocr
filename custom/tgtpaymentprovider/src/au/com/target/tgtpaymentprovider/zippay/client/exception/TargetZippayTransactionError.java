/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.client.exception;

/**
 * @author salexa10
 *
 */
public class TargetZippayTransactionError extends RuntimeException {

    public TargetZippayTransactionError() {
        super();
    }

    public TargetZippayTransactionError(final String message, final Throwable cause) {
        super(message, cause);
    }

    public TargetZippayTransactionError(final String message) {
        super(message);
    }

    public TargetZippayTransactionError(final Throwable cause) {
        super(cause);
    }

}
