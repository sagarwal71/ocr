package au.com.target.tgtpaymentprovider.zippay.response.types;


import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class CreateChargeResponse {

    @JsonProperty("receipt_number")
    abstract String getReceiptNumber();


}
