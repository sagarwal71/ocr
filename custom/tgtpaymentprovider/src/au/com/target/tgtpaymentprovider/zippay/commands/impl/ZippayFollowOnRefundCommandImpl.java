/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.commands.impl;

import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.TransactionStatus;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtpayment.commands.TargetFollowOnRefundCommand;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpaymentprovider.constants.TgtpaymentproviderConstants;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayClientException;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayTransactionError;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayTransactionRejectedException;
import au.com.target.tgtpaymentprovider.zippay.data.request.CreateRefundRequest;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateRefundResponse;


/**
 * @author salexa10
 *
 */
public class ZippayFollowOnRefundCommandImpl extends TargetAbstractZippayCommand
        implements TargetFollowOnRefundCommand {

    @Override
    public TargetFollowOnRefundResult perform(final TargetFollowOnRefundRequest paramRequest) {
        if (paramRequest == null) {
            throw new IllegalArgumentException("TargetFollowOnRefundRequest is null");
        }
        try {
            final CreateRefundResponse createRefundResponse = getTargetZippayClient().createRefund(
                    translateRequest(paramRequest));
            return translateResponse(createRefundResponse, TransactionStatus.ACCEPTED);
        }
        // Check for HTTP 402 - Payment Required
        catch (final TargetZippayTransactionRejectedException targetZippayTransactionRejectedException) {
            return translateResponse(null, TransactionStatus.REJECTED);
        }
        // Check for error code other than HTTP 402 - Payment Required
        catch (final TargetZippayTransactionError targetZippayTransactionError) {
            return translateResponse(null, TransactionStatus.ERROR);
        }
        // Otherwise
        catch (final TargetZippayClientException e) {
            throw new AdapterException(e);
        }
    }

    /**
     * @param paramRequest
     * @return {@link CreateRefundRequest}
     */
    private CreateRefundRequest translateRequest(final TargetFollowOnRefundRequest paramRequest) {
        final CreateRefundRequest refundRequest = new CreateRefundRequest();
        refundRequest.setChargeId(paramRequest.getCaptureTransactionId());
        refundRequest.setAmount(paramRequest.getTotalAmount().doubleValue());
        refundRequest.setReason(TgtpaymentproviderConstants.ZIPPAY_REFUND);
        return refundRequest;
    }

    /**
     * @param createRefundResponse
     * @return {@link TargetFollowOnRefundResult}
     */
    private TargetFollowOnRefundResult translateResponse(final CreateRefundResponse createRefundResponse,
            final TransactionStatus transactionStatus) {
        final TargetFollowOnRefundResult refundResult = new TargetFollowOnRefundResult();
        if (null != createRefundResponse && StringUtils.isNotEmpty(createRefundResponse.getId())) {
            refundResult.setReconciliationId(createRefundResponse.getId());
        }
        refundResult.setTransactionStatus(transactionStatus);
        return refundResult;
    }
}
