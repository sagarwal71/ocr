/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.client.exception;

/**
 * @author salexa10
 *
 */
public class TargetZippayClientException extends Exception {

    public TargetZippayClientException() {
        super();
    }

    public TargetZippayClientException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public TargetZippayClientException(final String message) {
        super(message);
    }

    public TargetZippayClientException(final Throwable cause) {
        super(cause);
    }

}
