/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.request.types;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author salexa10
 *
 */
public abstract class Shopper {

    @JsonProperty("first_name")
    abstract String getFirstName();

    @JsonProperty("last_name")
    abstract String getLastName();

    @JsonProperty("billing_address")
    abstract Address getBillingAddress();

}
