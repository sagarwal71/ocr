/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.UnitTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.util.StringUtils;

import au.com.target.tgtpaymentprovider.ipg.data.CardDetails;
import au.com.target.tgtpaymentprovider.ipg.data.CardResultsData;
import au.com.target.tgtpaymentprovider.ipg.data.SavedCardsData;
import au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSinglePaymentRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSingleRefundRequest;
import au.com.target.tgtpaymentprovider.ipg.data.soap.RefundInfo;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Response;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Security;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Transaction;
import au.com.target.tgtpaymentprovider.ipg.data.soap.VoidInfo;


/**
 * @author mjanarth
 *
 */
@UnitTest
public class IPGHelperTest {

    private final IPGHelper ipgHelper = new IPGHelper();

    @Test
    public void testConvertSavedCardRequestWithNullRequest() throws JAXBException {
        final SavedCardsData request = null;
        final String xmlString = ipgHelper.convertSavedCardRequestToXML(request);
        Assert.assertNull(xmlString);
    }

    @Test
    public void testConvertSavedCardRequestWithEmptyCards() throws JAXBException {
        final List<CardDetails> cards = new ArrayList<>();
        final SavedCardsData request = new SavedCardsData();
        request.setCardRequests(cards);
        final String xmlString = ipgHelper.convertSavedCardRequestToXML(request);
        Assert.assertEquals("<SavedCards/>", xmlString);
    }

    @Test
    public void testConvertSavedCardRequestWithSingleCardToXML() throws JAXBException {
        final List<CardDetails> cards = new ArrayList<>();
        final SavedCardsData request = new SavedCardsData();
        final CardDetails card = new CardDetails();
        card.setBin("1234");
        card.setTokenExpiry("12/06/2016");
        card.setCardType("VISA");
        card.setCardNumber("12345678998765");
        card.setIsDefaultCard("1");
        card.setCardExpiry("12/06/2016");
        card.setToken("abdc##789034");
        cards.add(card);
        request.setCardRequests(cards);
        final String xmlString = ipgHelper.convertSavedCardRequestToXML(request);
        final StringBuilder expectedPayload = new StringBuilder();
        expectedPayload.append("<SavedCards><Card>");
        expectedPayload.append("<Token>abdc##789034</Token>");
        expectedPayload.append("<TokenExpiry>12/06/2016</TokenExpiry>");
        expectedPayload.append("<CardType>VISA</CardType>");
        expectedPayload.append("<MaskedCard>12345678998765</MaskedCard>");
        expectedPayload.append("<CardExpiry>12/06/2016</CardExpiry>");
        expectedPayload.append("<DefaultCard>1</DefaultCard>");
        expectedPayload.append("<BIN>1234</BIN>");
        expectedPayload.append("</Card></SavedCards>");
        Assert.assertEquals(xmlString, expectedPayload.toString());
    }


    @Test
    public void testConvertSavedCardRequestWithTwoCardsToXML() throws JAXBException {
        final List<CardDetails> cards = new ArrayList<>();
        final SavedCardsData request = new SavedCardsData();
        final CardDetails cardVisa = new CardDetails();
        final CardDetails cardMaster = new CardDetails();
        cardVisa.setBin("1234");
        cardVisa.setCardExpiry("12/06/2016");
        cardVisa.setTokenExpiry("12/06/2016");
        cardVisa.setCardType("VISA");
        cardVisa.setCardNumber("12345678998765");
        cardVisa.setIsDefaultCard("1");
        cardVisa.setToken("abdc##789034");
        cardMaster.setBin("1234");
        cardMaster.setCardExpiry("12/06/2016");
        cardMaster.setTokenExpiry("12/06/2016");
        cardMaster.setCardType("Master");
        cardMaster.setCardNumber("12345678998765");
        cardMaster.setIsDefaultCard("0");
        cardMaster.setToken("abdc##789034");
        cards.add(cardVisa);
        cards.add(cardMaster);
        request.setCardRequests(cards);
        final String xmlString = ipgHelper.convertSavedCardRequestToXML(request);
        final StringBuilder expectedPayload = new StringBuilder();
        expectedPayload.append("<SavedCards>");
        expectedPayload.append("<Card>");
        expectedPayload.append("<Token>abdc##789034</Token>");
        expectedPayload.append("<TokenExpiry>12/06/2016</TokenExpiry>");
        expectedPayload.append("<CardType>VISA</CardType>");
        expectedPayload.append("<MaskedCard>12345678998765</MaskedCard>");
        expectedPayload.append("<CardExpiry>12/06/2016</CardExpiry>");
        expectedPayload.append("<DefaultCard>1</DefaultCard>");
        expectedPayload.append("<BIN>1234</BIN>");
        expectedPayload.append("</Card>");
        expectedPayload.append("<Card>");
        expectedPayload.append("<Token>abdc##789034</Token>");
        expectedPayload.append("<TokenExpiry>12/06/2016</TokenExpiry>");
        expectedPayload.append("<CardType>Master</CardType>");
        expectedPayload.append("<MaskedCard>12345678998765</MaskedCard>");
        expectedPayload.append("<CardExpiry>12/06/2016</CardExpiry>");
        expectedPayload.append("<DefaultCard>0</DefaultCard>");
        expectedPayload.append("<BIN>1234</BIN>");
        expectedPayload.append("</Card>");
        expectedPayload.append("</SavedCards>");
        Assert.assertEquals(xmlString, expectedPayload.toString());
    }


    @Test
    public void testEncodeBase64WithEmptyString() {
        final String input = "";
        final String encodedStr = ipgHelper.encodeBase64(input);
        Assert.assertNull(encodedStr);
    }

    @Test
    public void testEncodeBase64WithNull() {
        final String input = null;
        final String encodedStr = ipgHelper.encodeBase64(input);
        Assert.assertNull(encodedStr);
    }

    @Test
    public void testEncodeBase64ValidStr() {
        final String input = "abcdefgh";
        final String encodedStr = ipgHelper.encodeBase64(input);
        Assert.assertNotNull(encodedStr);
    }

    @Test
    public void testUnmarshalCardResultsWithNullRequest() throws JAXBException {
        final String xmlRequest = null;
        final CardResultsData data = ipgHelper.unmarshalCardResults(xmlRequest);
        Assert.assertNull(data);
    }

    @Test
    public void testUnmarshalCardResultsWithEmptyRequest() throws JAXBException {
        final String xmlRequest = "";
        final CardResultsData data = ipgHelper.unmarshalCardResults(xmlRequest);
        Assert.assertNull(data);
    }

    @Test
    public void testUnmarshalCardResultsWithValidRequest() throws JAXBException {
        final String xmlRequest = "<CardResults><Result><Details><Amount>1000</Amount><TransType>1</TransType><Receipt>82626354</Receipt><RespCode>0</RespCode><RespText>Approved</RespText><TrnDateTime>2014-11-11 15:30:00</TrnDateTime><SettlementDate>2014-11-12</SettlementDate>"
                + "</Details><Card><CardType>VISA</CardType><MaskedCard>424242******4242</MaskedCard><CardExpiry>12/15</CardExpiry><BIN>42424</BIN> <PromoID>2000</PromoID><Token>36639373737373737726</Token>"
                + "<TokenExpiry>01/12/14</TokenExpiry></Card><Fraud><ThreeDS>"
                + "<ThreeDSSessToken></ThreeDSSessToken><ECI></ECI><VER></VER><PAR></PAR><SigVerif></SigVerif><CAVV></CAVV><XID></XID><CAR>98</CAR></ThreeDS><ReD><ReDOrderNumber></ReDOrderNumber>"
                + "<FRAUD_STAT_CD>aaaa</FRAUD_STAT_CD><REQ_ID></REQ_ID><ORD_ID></ORD_ID><STAT_CD>aaaa</STAT_CD><FRAUD_RSP_CD></FRAUD_RSP_CD></ReD></Fraud></Result></CardResults>";
        final CardResultsData data = ipgHelper.unmarshalCardResults(xmlRequest);
        Assert.assertNotNull(data);
        Assert.assertNotNull(data.getCardResults());
        Assert.assertEquals(data.getCardResults().size(), 1);
        Assert.assertNotNull(data.getCardResults().get(0).getFraudDetails());
        Assert.assertNotNull(data.getCardResults().get(0).getTransactionDetails());
    }

    @Test
    public void tesDeccodeBase64WithEmptyString() {
        final String input = "";
        final String decodedStr = ipgHelper.decode(input);
        Assert.assertNull(decodedStr);
    }

    @Test
    public void tesDeccodeBase64WithNullString() {
        final String input = null;
        final String decodedStr = ipgHelper.decode(input);
        Assert.assertNull(decodedStr);
    }

    @Test
    public void tesDeccodeBase64WithValidString() {
        final String input = "ENhcmRSZXN1bHRzPjxSZXN1bHQ%2bPERldGFpbHM";
        final String decodedStr = ipgHelper.decode(input);
        Assert.assertNotNull(decodedStr);
    }

    @Test
    public void testSubmitSinglePaymentRequestToXml() throws JAXBException {
        final SubmitSinglePaymentRequest request = new SubmitSinglePaymentRequest();
        final Transaction transaction = new Transaction();
        final Security security = new Security();
        security.setPassword("123");
        security.setUserName("test");
        transaction.setSecurity(security);
        request.setTransaction(transaction);
        transaction.setAccountNumber("123");
        final String xml = ipgHelper.marshalTransaction(request.getTransaction());
        assertEquals(
                "<Transaction><AccountNumber>123</AccountNumber>" +
                        "<Security><UserName>test</UserName><Password>123</Password></Security></Transaction>",
                xml);
    }

    @Test
    public void testSubmitSingleRefundRequestToXml() throws JAXBException {
        final SubmitSingleRefundRequest request = new SubmitSingleRefundRequest();
        final RefundInfo refundInfo = new RefundInfo();
        final Security security = new Security();
        refundInfo.setAmount("1200");
        refundInfo.setReceiptNumber("123456");
        security.setPassword("123");
        security.setUserName("test");
        refundInfo.setSecurity(security);
        request.setRefundInfo(refundInfo);
        final String xml = ipgHelper.marshalRefundInfo(request.getRefundInfo());
        assertEquals(
                "<Refund><Receipt>123456</Receipt><Amount>1200</Amount><Security><UserName>test</UserName><Password>123</Password></Security></Refund>",
                xml);
    }

    @Test
    public void testUnmarshalResponse() throws JAXBException {
        final String responseXml = "<Response><ResponseCode>0</ResponseCode><Timestamp>02-Jul-2014 11:07:08"
                + "</Timestamp><Receipt>10001197</Receipt><SettlementDate>02-Jul-2014</SettlementDate><DeclinedCode>"
                + "123</DeclinedCode><DeclinedMessage>456</DeclinedMessage></Response>";

        final Response response = ipgHelper.unmarshalResponse(responseXml);
        assertEquals("0", response.getResponseCode());
        assertEquals("02-Jul-2014 11:07:08", response.getTimestamp());
        assertEquals("10001197", response.getReceipt());
    }

    @Test
    public void testUnmarshalSavedCardsWithEmptyRequest() throws JAXBException {
        final String xmlRequest = "";
        final SavedCardsData data = ipgHelper.unmarshalSavedCards(xmlRequest);
        Assert.assertNull(data);
    }

    @Test
    public void testUnmarshalSavedCardsWithNull() throws JAXBException {
        final String xmlRequest = null;
        final SavedCardsData data = ipgHelper.unmarshalSavedCards(xmlRequest);
        Assert.assertNull(data);
    }

    @Test
    public void testUnmarshalSavedCards() throws JAXBException {
        final String xmlRequest = "<SavedCards><Card>"
                + "<Token>99442123456704242</Token><TokenExpiry>08/12/20</TokenExpiry><CardExpiry>04/20</CardExpiry><CardType>VISA</CardType><MaskedCard>424242******4242</MaskedCard>"
                + "<DefaultCard>1</DefaultCard><BIN>42</BIN></Card></SavedCards>";
        final SavedCardsData data = ipgHelper.unmarshalSavedCards(xmlRequest);
        Assert.assertNotNull(data);
        Assert.assertNotNull(data.getCardRequests());
        Assert.assertEquals(1, data.getCardRequests().size());
        Assert.assertEquals("99442123456704242", data.getCardRequests().get(0).getToken());
        Assert.assertEquals("08/12/20", data.getCardRequests().get(0).getTokenExpiry());
        Assert.assertEquals("04/20", data.getCardRequests().get(0).getCardExpiry());
        Assert.assertEquals("VISA", data.getCardRequests().get(0).getCardType());
        Assert.assertEquals("424242******4242", data.getCardRequests().get(0).getCardNumber());


    }

    @Test
    public void testGetAmountInCents() {
        assertEquals(1001, IPGHelper.getAmountInCents(new BigDecimal(10.009d)));
        assertEquals(998, IPGHelper.getAmountInCents(new BigDecimal(9.9808d)));
        assertEquals(3390, IPGHelper.getAmountInCents(new BigDecimal(33.899f)));
        assertEquals(3389, IPGHelper.getAmountInCents(new BigDecimal(33.89f)));
    }

    @Test
    public void testMarshalVoidInfo() throws JAXBException {
        final VoidInfo voidInfo = new VoidInfo();
        voidInfo.setReceiptNumber("90384670");

        final Security security = new Security();
        security.setPassword("123");
        security.setUserName("test");

        voidInfo.setSecurity(security);
        final String xml = ipgHelper.marshalVoidInfo(voidInfo);
        assertNotNull(xml);
        assertEquals(
                "<Void><Receipt>90384670</Receipt><Security><UserName>test</UserName><Password>123</Password></Security></Void>",
                xml);
    }

    @Test
    public void testUnmarshalSingleVoidResponse() throws JAXBException {
        final String xmlResponse = "<Response><ResponseCode>0</ResponseCode>" +
                "<Timestamp>02-Jul-2014 11:07:08</Timestamp><Receipt>10001197</Receipt>" +
                "<DeclinedCode></DeclinedCode><DeclinedMessage></DeclinedMessage></Response>";

        final Response response = ipgHelper.unmarshalResponse(xmlResponse);

        assertNotNull(response);
        assertEquals("0", response.getResponseCode());
        assertEquals("10001197", response.getReceipt());
        assertTrue(StringUtils.isEmpty(response.getDeclinedCode()));
        assertTrue(StringUtils.isEmpty(response.getDeclinedMessage()));
    }
}