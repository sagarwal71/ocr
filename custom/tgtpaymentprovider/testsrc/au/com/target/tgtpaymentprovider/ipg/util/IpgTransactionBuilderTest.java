/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.util;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpaymentprovider.ipg.data.soap.CreditCard;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Security;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Transaction;


/**
 * @author htan3
 *
 */
@UnitTest
public class IpgTransactionBuilderTest {

    private IpgTransactionBuilder builder = null;

    @Before
    public void setUp() {
        builder = IpgTransactionBuilder.create();
    }

    @Test
    public void testApplySecurity() {
        final Security security = mock(Security.class);
        builder.applySecurity(security);
        assertEquals(security, builder.build().getSecurity());
    }

    @Test
    public void testAddAccountNumber() {
        builder.addAccountNumber("123", "456");
        final Transaction transaction = builder.build();
        assertEquals("123", transaction.getAccountNumber());
        assertEquals("456", transaction.getMerchantNumber());
    }

    @Test
    public void testAddIpgCard() {
        final TargetCardResult ipgCard = new TargetCardResult();
        ipgCard.setBin("bin");
        ipgCard.setCardExpiry("12/2018");
        ipgCard.setCardNumber("434234");
        ipgCard.setCardType("visa");
        ipgCard.setToken("wewqew");
        ipgCard.setTokenExpiry("12/2016");
        builder.addIpgCardInfo(ipgCard);
        final Transaction transaction = builder.build();
        final CreditCard creditCard = transaction.getCreditCard();
        assertEquals("wewqew", creditCard.getCardNumber());
        assertEquals("bin", transaction.getUserDefined().getBin());
        assertEquals("12/2018", transaction.getUserDefined().getCcExpiry());
        assertEquals("12/2016", transaction.getUserDefined().getTokenExpiry());
        assertEquals("434234", creditCard.getMaskedCardNumber());
        assertEquals("visa", creditCard.getCardType());
    }

    @Test
    public void test() {
        builder.addClientIP("1.2.3.4");
        final Transaction transaction = builder.build();
        assertEquals("1.2.3.4", transaction.getTrnSource());
    }

    @Test
    public void testStoreId() {
        final String storeId = "5599";
        builder.setStoreId(storeId);
        final Transaction transaction = builder.build();
        assertEquals(storeId, transaction.getUserDefined().getStoreId());
    }
}