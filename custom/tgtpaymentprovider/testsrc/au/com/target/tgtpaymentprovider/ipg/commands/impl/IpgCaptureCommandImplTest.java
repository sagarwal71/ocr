/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.commands.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpayment.commands.result.TargetCardPaymentResult;
import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpaymentprovider.ipg.exception.IpgServiceException;
import au.com.target.tgtpaymentprovider.ipg.service.IpgService;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class IpgCaptureCommandImplTest {
    @InjectMocks
    private final IpgCaptureCommandImpl captureCommand = new IpgCaptureCommandImpl();

    @Mock
    private IpgService ipgService;

    @Mock
    private TargetCaptureRequest paramRequest;

    @Mock
    private TargetQueryTransactionDetailsResult queryDetails;

    @Mock
    private TargetCardResult cardResult;

    @Mock
    private Order order;

    @Mock
    private TargetCardPaymentResult transactionResult;

    @Before
    public void setUp() throws IpgServiceException {
        when(paramRequest.getOrder()).thenReturn(order);
        when(paramRequest.getTotalAmount()).thenReturn(BigDecimal.ONE);
        when(ipgService.submitSinglePayment(paramRequest)).thenReturn(transactionResult);
    }

    @Test
    public void shouldReturnErrorIfNoCardInfoCaptured() {
        final TargetCaptureResult captureResult = captureCommand.perform(paramRequest);
        verify(paramRequest).getTransactionId();
        verify(paramRequest).getPayerId();
        verify(paramRequest).getTotalAmount();
        assertEquals(TransactionStatus.ERROR, captureResult.getTransactionStatus());
    }

    @Test
    public void shouldReturnRejectedIfResponseCodeIs1() {
        when(paramRequest.getCardResult()).thenReturn(cardResult);
        when(cardResult.getCardType()).thenReturn(CreditCardType.VISA.getCode());
        final List<TargetCardResult> list = new ArrayList<>();
        list.add(cardResult);
        when(queryDetails.getCardResults()).thenReturn(list);
        when(transactionResult.getResponseCode()).thenReturn("1");

        final TargetCaptureResult captureResult = captureCommand.perform(paramRequest);

        assertEquals(TransactionStatus.ERROR, captureResult.getTransactionStatus());
        assertNull(captureResult.getReconciliationId());
    }

    @Test
    public void shouldReturnErrorIfResponseCodeIs3() {
        when(paramRequest.getCardResult()).thenReturn(cardResult);
        when(cardResult.getCardType()).thenReturn(CreditCardType.VISA.getCode());
        final List<TargetCardResult> list = new ArrayList<>();
        list.add(cardResult);
        when(queryDetails.getCardResults()).thenReturn(list);
        when(transactionResult.getResponseCode()).thenReturn("3");

        final TargetCaptureResult captureResult = captureCommand.perform(paramRequest);

        assertEquals(TransactionStatus.ERROR, captureResult.getTransactionStatus());
        assertNull(captureResult.getReconciliationId());
    }

    @Test
    public void shouldReturnBankDeclineIfDeclineCodeIs13() {
        when(paramRequest.getCardResult()).thenReturn(cardResult);
        when(cardResult.getCardType()).thenReturn(CreditCardType.VISA.getCode());
        when(transactionResult.getResponseCode()).thenReturn("1");
        when(transactionResult.getDeclinedCode()).thenReturn("13");

        final TargetCaptureResult captureResult = captureCommand.perform(paramRequest);

        assertEquals(TransactionStatusDetails.BANK_DECLINE, captureResult.getTransactionStatusDetails());
        assertNull(captureResult.getReconciliationId());
    }

    @Test
    public void shouldReturnInvalidExpiryDateIfDeclineCodeIs33() {
        when(paramRequest.getCardResult()).thenReturn(cardResult);
        when(cardResult.getCardType()).thenReturn(CreditCardType.VISA.getCode());
        when(transactionResult.getResponseCode()).thenReturn("1");
        when(transactionResult.getDeclinedCode()).thenReturn("33");

        final TargetCaptureResult captureResult = captureCommand.perform(paramRequest);

        assertEquals(TransactionStatusDetails.INVALID_CARD_EXPIRATION_DATE, captureResult.getTransactionStatusDetails());
        assertNull(captureResult.getReconciliationId());
    }

    @Test
    public void shouldReturnInsuffcientFundIfDeclineCodeIs51() {
        when(paramRequest.getCardResult()).thenReturn(cardResult);
        when(cardResult.getCardType()).thenReturn(CreditCardType.VISA.getCode());
        when(transactionResult.getResponseCode()).thenReturn("1");
        when(transactionResult.getDeclinedCode()).thenReturn("51");

        final TargetCaptureResult captureResult = captureCommand.perform(paramRequest);

        assertEquals(TransactionStatusDetails.INSUFFICIENT_FUNDS, captureResult.getTransactionStatusDetails());
        assertNull(captureResult.getReconciliationId());
    }

    @Test
    public void shouldReturnGeneralSystemErrorIfDeclineCodeIs68() {
        when(paramRequest.getCardResult()).thenReturn(cardResult);
        when(cardResult.getCardType()).thenReturn(CreditCardType.VISA.getCode());
        when(transactionResult.getResponseCode()).thenReturn("1");
        when(transactionResult.getDeclinedCode()).thenReturn("68");

        final TargetCaptureResult captureResult = captureCommand.perform(paramRequest);

        assertEquals(TransactionStatusDetails.GENERAL_SYSTEM_ERROR, captureResult.getTransactionStatusDetails());
        assertEquals(TransactionStatus.ERROR, captureResult.getTransactionStatus());
        assertNull(captureResult.getReconciliationId());
    }

    @Test
    public void shouldReturnGeneralSystemError() {
        when(paramRequest.getCardResult()).thenReturn(cardResult);
        when(cardResult.getCardType()).thenReturn(CreditCardType.VISA.getCode());
        when(transactionResult.getResponseCode()).thenReturn("1");
        when(transactionResult.getDeclinedCode()).thenReturn("17");
        final TargetCaptureResult captureResult = captureCommand.perform(paramRequest);
        assertEquals(TransactionStatusDetails.GENERAL_SYSTEM_ERROR, captureResult.getTransactionStatusDetails());
        assertEquals("17", captureResult.getDeclineCode());
    }

    @Test
    public void shouldReturnGeneralSystemErrorfor103() {
        when(paramRequest.getCardResult()).thenReturn(cardResult);
        when(cardResult.getCardType()).thenReturn(CreditCardType.VISA.getCode());
        when(transactionResult.getResponseCode()).thenReturn("1");
        when(transactionResult.getDeclinedCode()).thenReturn("103");
        final TargetCaptureResult captureResult = captureCommand.perform(paramRequest);
        assertEquals(TransactionStatusDetails.GENERAL_SYSTEM_ERROR, captureResult.getTransactionStatusDetails());
        assertEquals("103", captureResult.getDeclineCode());
    }

    @Test
    public void shouldReturnRejectedIfResponseCodeIs0() {
        when(paramRequest.getCardResult()).thenReturn(cardResult);
        when(cardResult.getCardType()).thenReturn(CreditCardType.VISA.getCode());
        final List<TargetCardResult> list = new ArrayList<>();
        list.add(cardResult);
        when(queryDetails.getCardResults()).thenReturn(list);
        when(transactionResult.getResponseCode()).thenReturn("0");
        when(transactionResult.getReceiptNumber()).thenReturn("1232321");

        final TargetCaptureResult captureResult = captureCommand.perform(paramRequest);

        assertEquals(TransactionStatus.ACCEPTED, captureResult.getTransactionStatus());
        assertEquals(TransactionStatusDetails.SUCCESFULL, captureResult.getTransactionStatusDetails());
        assertEquals("1232321", captureResult.getReconciliationId());
    }

    @Test
    public void testGiftCardCapturePayment() {
        when(paramRequest.getCardResult()).thenReturn(cardResult);
        when(cardResult.getCardType()).thenReturn(CreditCardType.GIFTCARD.getCode());
        final TargetCaptureResult captureResult = captureCommand.perform(paramRequest);
        assertNull(captureResult.getTransactionStatus());
    }

    @Test(expected = AdapterException.class)
    public void shouldThrowExceptionIfFailedToSubmitPayment() throws IpgServiceException {
        when(paramRequest.getCardResult()).thenReturn(cardResult);
        when(cardResult.getCardType()).thenReturn(CreditCardType.VISA.getCode());
        final List<TargetCardResult> list = new ArrayList<>();
        list.add(cardResult);
        when(queryDetails.getCardResults()).thenReturn(list);
        when(transactionResult.getResponseCode()).thenReturn("0");
        when(transactionResult.getReceiptNumber()).thenReturn("1232321");
        when(ipgService.submitSinglePayment(paramRequest)).thenThrow(
                new IpgServiceException("test"));

        captureCommand.perform(paramRequest);
    }
}
