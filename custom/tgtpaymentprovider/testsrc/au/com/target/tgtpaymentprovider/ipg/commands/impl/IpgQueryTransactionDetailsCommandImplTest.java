package au.com.target.tgtpaymentprovider.ipg.commands.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.TargetQueryTransactionDetailsCommand;
import au.com.target.tgtpayment.commands.request.TargetQueryTransactionDetailsRequest;
import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpaymentprovider.ipg.data.CardDetails;
import au.com.target.tgtpaymentprovider.ipg.data.CardResultData;
import au.com.target.tgtpaymentprovider.ipg.data.CardResultsData;
import au.com.target.tgtpaymentprovider.ipg.data.SavedCardsData;
import au.com.target.tgtpaymentprovider.ipg.data.TransactionDetails;
import au.com.target.tgtpaymentprovider.ipg.data.response.TransactionsResultResponse;
import au.com.target.tgtpaymentprovider.ipg.exception.IpgServiceException;
import au.com.target.tgtpaymentprovider.ipg.service.IpgService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class IpgQueryTransactionDetailsCommandImplTest {
    @Mock
    private IpgService ipgService;

    @InjectMocks
    private final TargetQueryTransactionDetailsCommand queryTransactionDetailsCommand = new IpgQueryTransactionDetailsCommandImpl();

    private TargetQueryTransactionDetailsRequest request;

    private final IpgQueryTransactionDetailsCommandImpl queryTransaction = new IpgQueryTransactionDetailsCommandImpl();


    @Before
    public void setup() {
        request = new TargetQueryTransactionDetailsRequest();
    }

    @Test(expected = AdapterException.class)
    public void testPerformWhenIpgServiceThrowsException() throws IpgServiceException {
        given(ipgService.queryTransactionResults(anyString(), anyString())).willThrow(new IpgServiceException());
        queryTransactionDetailsCommand.perform(request);
    }

    @Test
    public void testPerformSuccess() throws IpgServiceException {
        final String amount = "100";
        final String sessionId = "fdsfsdf";
        final TransactionsResultResponse transactionResultResponse = new TransactionsResultResponse();
        transactionResultResponse.setAmount(amount);
        transactionResultResponse.setSessionId(sessionId);
        transactionResultResponse.setTransactionResult("1");
        given(ipgService.queryTransactionResults(anyString(), anyString())).willReturn(transactionResultResponse);
        final TargetQueryTransactionDetailsResult response = queryTransactionDetailsCommand.perform(request);

        assertNotNull(response);
        // TODO: More attribute validation when client mapping is finalized
    }

    @Test
    public void testMapResponseWhenNoTransactionResponse() {
        final TransactionsResultResponse response = null;
        final TargetQueryTransactionDetailsResult result = ((IpgQueryTransactionDetailsCommandImpl)queryTransactionDetailsCommand)
                .mapResponse(response);
        assertNull(result);
    }

    @Test
    public void testMapResponesWhenTransactionResponseHasQueryResults() {
        final String queryResult = "False";
        final String queryError = "Error calling query transaction results";
        final TransactionsResultResponse response = new TransactionsResultResponse();
        response.setQueryResult(queryResult);
        response.setQueryError(queryError);
        final TargetQueryTransactionDetailsResult result = ((IpgQueryTransactionDetailsCommandImpl)queryTransactionDetailsCommand)
                .mapResponse(response);
        assertNotNull(result);
        assertFalse(result.isSuccess());
        assertEquals(queryError, result.getErrorDescription());
    }

    @Test
    public void testMapResponesWhenTransactionResponseHasTransactionResultsSuccess() {
        final String transactionResult = "1";
        final TransactionsResultResponse response = new TransactionsResultResponse();
        response.setTransactionResult(transactionResult);
        final TargetQueryTransactionDetailsResult result = ((IpgQueryTransactionDetailsCommandImpl)queryTransactionDetailsCommand)
                .mapResponse(response);
        assertNotNull(result);
        assertTrue(result.isSuccess());
    }

    @Test
    public void testMapResponesWhenTransactionResponseHasTransactionResultsNotSuccess() {
        final String transactionResult = "0";
        final TransactionsResultResponse response = new TransactionsResultResponse();
        response.setTransactionResult(transactionResult);
        final TargetQueryTransactionDetailsResult result = ((IpgQueryTransactionDetailsCommandImpl)queryTransactionDetailsCommand)
                .mapResponse(response);

        assertNotNull(result);
        assertFalse(result.isSuccess());
    }

    @Test
    public void testMapResponesWhenTransactionResponseHasTransactionResultsButNotCardResults() {
        final String transactionResult = "1";
        final String amount = "15000";
        final String sessionId = "123456897";
        final String sessionToken = "1234566798";
        final TransactionsResultResponse response = new TransactionsResultResponse();
        response.setTransactionResult(transactionResult);
        response.setAmount(amount);
        response.setSessionId(sessionId);
        response.setSessionToken(sessionToken);
        final TargetQueryTransactionDetailsResult result = ((IpgQueryTransactionDetailsCommandImpl)queryTransactionDetailsCommand)
                .mapResponse(response);
        assertNotNull(result);
        assertTrue(result.isSuccess());
        assertEquals(new BigDecimal("150"), result.getAmount());
        assertEquals(sessionId, result.getSessionId());
        assertEquals(sessionToken, result.getSessionToken());
        assertNull(result.getCardResults());
    }

    @Test
    public void testMapResponesWhenTransactionResponseHasTransactionResultsWithCardResultsAndTransactionResults() {
        final String transType = "1";
        final String transactionResult = transType;
        final String amount = "1955";
        final String sessionId = "123456897";
        final String sessionToken = "1234566798";
        final CardDetails cardDetails = new CardDetails();
        final String bin = "42424";
        final String cardExpiry = "12/15";
        final String cardNumber = "451245124512";
        final String cardType = "VISA";
        final String promoId = "2000";
        final String token = "36639373737373737726";
        final String tokenExpiry = "05/05/2015";

        cardDetails.setBin(bin);
        cardDetails.setCardExpiry(cardExpiry);
        cardDetails.setCardNumber(cardNumber);
        cardDetails.setCardType(cardType);
        cardDetails.setPromoId(promoId);
        cardDetails.setToken(token);
        cardDetails.setTokenExpiry(tokenExpiry);
        cardDetails.setIsDefaultCard("1");

        final String receiptNumber = "123";
        final String responseCode = "0";
        final String responseText = "Approved";
        final String settlementDate = "2015-11-12";
        final String transDateTime = "2015-11-11 15:30:00";

        final TransactionDetails transactionDetails = new TransactionDetails();
        transactionDetails.setAmount(amount);
        transactionDetails.setReceiptNumber(receiptNumber);
        transactionDetails.setResponseCode(responseCode);
        transactionDetails.setResponseText(responseText);
        transactionDetails.setSettlementDate(settlementDate);
        transactionDetails.setTransDateTime(transDateTime);
        transactionDetails.setTransType(transType);

        final CardResultData cardResult = new CardResultData();
        cardResult.setTransactionDetails(transactionDetails);
        cardResult.setCardDetails(cardDetails);

        final List<CardResultData> cardResults = new ArrayList<>();
        cardResults.add(cardResult);

        final CardResultsData cardResultsData = new CardResultsData();
        cardResultsData.setCardResults(cardResults);

        final TransactionsResultResponse response = new TransactionsResultResponse();

        response.setCardResults(cardResultsData);
        response.setTransactionResult(transactionResult);
        response.setAmount(amount);
        response.setSessionId(sessionId);
        response.setSessionToken(sessionToken);

        final TargetQueryTransactionDetailsResult result = ((IpgQueryTransactionDetailsCommandImpl)queryTransactionDetailsCommand)
                .mapResponse(response);

        assertNotNull(result);
        assertTrue(result.isSuccess());
        assertEquals(new BigDecimal("19.55"), result.getAmount());
        assertEquals(sessionId, result.getSessionId());
        assertEquals(sessionToken, result.getSessionToken());
        assertNotNull(result.getCardResults());
        assertEquals(1, result.getCardResults().size());
        assertEquals(bin, (result.getCardResults().get(0)).getBin());
        assertEquals(cardExpiry, (result.getCardResults().get(0)).getCardExpiry());
        assertEquals(cardNumber, (result.getCardResults().get(0)).getCardNumber());
        assertEquals(cardType, (result.getCardResults().get(0)).getCardType());
        assertEquals(promoId, (result.getCardResults().get(0)).getPromoId());
        assertEquals(token, (result.getCardResults().get(0)).getToken());
        assertEquals(tokenExpiry, (result.getCardResults().get(0)).getTokenExpiry());
        assertTrue((result.getCardResults().get(0)).isDefaultCard());
        assertNotNull((result.getCardResults().get(0)).getTargetCardPaymentResult());
        assertEquals(new BigDecimal("19.55"),
                (result.getCardResults().get(0)).getTargetCardPaymentResult().getAmount());
        assertEquals(receiptNumber, (result.getCardResults().get(0)).getTargetCardPaymentResult().getReceiptNumber());
        assertEquals(responseCode, (result.getCardResults().get(0)).getTargetCardPaymentResult().getResponseCode());
        assertEquals(responseText, (result.getCardResults().get(0)).getTargetCardPaymentResult().getResponseText());
        assertEquals(settlementDate, (result.getCardResults().get(0)).getTargetCardPaymentResult().getSettlementDate());
        assertEquals(transDateTime, (result.getCardResults().get(0)).getTargetCardPaymentResult()
                .getTransactionDateTime());
        assertEquals(transType, (result.getCardResults().get(0)).getTargetCardPaymentResult().getTransactionType());
    }

    @Test
    public void testMapResponseWithSavedCards() {
        final CardDetails cardDetails = new CardDetails();
        cardDetails.setBin("1234");
        cardDetails.setCardExpiry("12/20");
        cardDetails.setCardNumber("433456**123");
        cardDetails.setCardType("VISA");
        cardDetails.setPromoId("123");
        cardDetails.setToken("123###456");
        cardDetails.setTokenExpiry("12/12/2017");
        cardDetails.setIsDefaultCard("1");
        final List<CardDetails> cardDetailsList = new ArrayList<>();
        cardDetailsList.add(cardDetails);
        final SavedCardsData savedCards = new SavedCardsData();
        savedCards.setCardRequests(cardDetailsList);
        final TransactionsResultResponse response = new TransactionsResultResponse();
        response.setSavedCards(savedCards);
        final TargetQueryTransactionDetailsResult result = ((IpgQueryTransactionDetailsCommandImpl)queryTransactionDetailsCommand)
                .mapResponse(response);
        assertNotNull(result);
        assertEquals(1, result.getSavedCards().size());
        assertEquals("1234", result.getSavedCards().get(0).getBin());
        assertEquals("12/20", result.getSavedCards().get(0).getCardExpiry());
        assertEquals("433456**123", result.getSavedCards().get(0).getCardNumber());
        assertEquals("VISA", result.getSavedCards().get(0).getCardType());
        assertEquals("123###456", result.getSavedCards().get(0).getToken());
        assertEquals("12/12/2017", result.getSavedCards().get(0).getTokenExpiry());
        assertEquals("123", result.getSavedCards().get(0).getPromoId());

    }

    @Test
    public void testPopulateSavedCardsWithNullSavedCards() {
        final TransactionsResultResponse response = new TransactionsResultResponse();
        response.setSavedCards(null);
        final List<TargetCardResult> ipgSavedCards = queryTransaction.populateSavedCards(response);
        assertNull(ipgSavedCards);
    }

    @Test
    public void testPopulateSavedCardsWithEmptySavedCards() {
        final TransactionsResultResponse response = new TransactionsResultResponse();
        final SavedCardsData savedCardsData = new SavedCardsData();
        final List<CardDetails> savedCardList = new ArrayList<>();
        savedCardsData.setCardRequests(savedCardList);
        response.setSavedCards(savedCardsData);
        final List<TargetCardResult> ipgSavedCards = queryTransaction.populateSavedCards(response);
        assertNull(ipgSavedCards);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateSavedCardsSavedCards() {
        final TransactionsResultResponse response = new TransactionsResultResponse();
        final SavedCardsData savedCardsData = new SavedCardsData();
        final List<CardDetails> savedCardList = new ArrayList<>();
        final CardDetails cardVisa = new CardDetails();
        cardVisa.setBin("1234");
        cardVisa.setCardExpiry("12/12/2015");
        cardVisa.setCardType("VISA");
        cardVisa.setPromoId("123");
        cardVisa.setCardNumber("4242******967");
        cardVisa.setToken("123@@@456");
        cardVisa.setTokenExpiry("15/12/2016");
        cardVisa.setIsDefaultCard("1");
        savedCardList.add(cardVisa);
        savedCardsData.setCardRequests(savedCardList);
        response.setSavedCards(savedCardsData);
        final List<TargetCardResult> ipgSavedCards = queryTransaction.populateSavedCards(response);
        assertNotNull(ipgSavedCards);
        assertEquals(1, ipgSavedCards.size());
        assertEquals("VISA", ipgSavedCards.get(0).getCardType());
        assertEquals("1234", ipgSavedCards.get(0).getBin());
        assertEquals("12/12/2015", ipgSavedCards.get(0).getCardExpiry());
        assertEquals("123@@@456", ipgSavedCards.get(0).getToken());
        assertEquals("15/12/2016", ipgSavedCards.get(0).getTokenExpiry());
        assertEquals("4242******967", ipgSavedCards.get(0).getCardNumber());
        assertEquals(true, ipgSavedCards.get(0).isDefaultCard());

    }

    @Test
    public void testMapResponseWithPendingTransaction() {
        final String transactionResultInProgress = "2";
        final TransactionsResultResponse response = new TransactionsResultResponse();
        response.setTransactionResult(transactionResultInProgress);

        final TargetQueryTransactionDetailsResult result = ((IpgQueryTransactionDetailsCommandImpl)queryTransactionDetailsCommand)
                .mapResponse(response);

        assertNotNull(result);
        assertFalse(result.isSuccess());
        assertTrue(result.isPending());
    }

    @Test
    public void testMapResponesWhenTransactionResponseHasTransactionResultsCancel() {
        final String transactionResult = "4";
        final TransactionsResultResponse response = new TransactionsResultResponse();
        response.setTransactionResult(transactionResult);
        final TargetQueryTransactionDetailsResult result = ((IpgQueryTransactionDetailsCommandImpl)queryTransactionDetailsCommand)
                .mapResponse(response);
        assertNotNull(result);
        assertTrue(result.isCancel());
        assertFalse(result.isSuccess());
        assertFalse(result.isPending());
    }
}