/**
 *
 */
package au.com.target.tgtpaymentprovider.ipg.client.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import au.com.ippayments._interface.api.dts.DtsSoap;
import au.com.target.tgtpaymentprovider.ipg.data.request.SingleVoidRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSinglePaymentRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSingleRefundRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.TokenizeRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.TransactionQueryRequest;
import au.com.target.tgtpaymentprovider.ipg.data.response.SessionResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.SingleVoidResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.SubmitSinglePaymentResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.SubmitSingleRefundResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.TransactionsResultResponse;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Criteria;
import au.com.target.tgtpaymentprovider.ipg.data.soap.QueryTransaction;
import au.com.target.tgtpaymentprovider.ipg.data.soap.RefundInfo;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Response;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Security;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Transaction;
import au.com.target.tgtpaymentprovider.ipg.data.soap.VoidInfo;
import au.com.target.tgtpaymentprovider.ipg.exception.IPGClientException;
import au.com.target.tgtpaymentprovider.ipg.util.IPGHelper;
import au.com.target.tgtpaymentprovider.ipg.util.IPGRequestParametersBuilder;
import au.com.target.tgtutility.http.factory.HttpClientFactory;


/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class IPGClientImplTest {

    private static final String URL = "https://demo.ippayments.com.au/ipgpaypage/index.aspx";

    @Mock
    private HttpClientFactory mockHttpClientFactory;

    @Mock
    private IPGRequestParametersBuilder ipgRequestParamatersBuilder;

    @Mock
    private IPGHelper ipgHelper;

    @Mock
    private CloseableHttpClient mockHttpClient;

    @Mock
    private DtsSoap ipgSoapService;

    @Mock
    private Security security;

    @InjectMocks
    private final IPGClientImpl ipgClient = new IPGClientImpl();

    @Before
    public void setUp() {
        final HttpClientBuilder builder = mock(HttpClientBuilder.class);
        given(mockHttpClientFactory.createHttpClientBuilderWithProxy()).willReturn(builder);
        given(builder.build()).willReturn(mockHttpClient);
        ipgClient.setSessionRequestUrl(URL);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateSessionResponse() throws IPGClientException {
        final StringBuilder htmlResponse = new StringBuilder();
        htmlResponse.append("<html><body><form>");
        htmlResponse.append("<input type=\"hidden\" name=\"SessionStored\" value=\"False\" />");
        htmlResponse.append("<input type=\"hidden\" name=\"SessionStoredError\" value=\"\" />");
        htmlResponse.append("<input type=\"hidden\" name=\"SST\" value=\"239-7392-2836-2771\" />");
        htmlResponse.append("</form></body></html>");
        final SessionResponse response = ipgClient.populateSessionResponse(htmlResponse.toString());
        Assert.assertEquals(response.getSecureSessionToken(), "239-7392-2836-2771");
        Assert.assertEquals(response.getSessionStoredError(), "");
        Assert.assertEquals(response.isSessionStored(), false);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateSessionResponseWithoutSessionError() throws IPGClientException {
        final StringBuilder htmlResponse = new StringBuilder();
        htmlResponse.append("<html><body><form>");
        htmlResponse.append("<input type=\"hidden\" name=\"SessionStored\" value=\"False\" />");
        htmlResponse.append("<input type=\"hidden\" name=\"SST\" value=\"239-7392-2836-2771\" />");
        htmlResponse.append("</form></body></html>");
        final SessionResponse response = ipgClient.populateSessionResponse(htmlResponse.toString());
        Assert.assertEquals(response.getSecureSessionToken(), "239-7392-2836-2771");
        Assert.assertEquals(response.getSessionStoredError(), null);
        Assert.assertEquals(response.isSessionStored(), false);
    }

    @SuppressWarnings("boxing")
    @Test(expected = IPGClientException.class)
    public void testPopulateSessionResponseWithNullHtml() throws IPGClientException {
        final StringBuilder htmlResponse = new StringBuilder();
        htmlResponse.append("<html><body><form>");
        htmlResponse.append("dfsdfdsf</body></html>");
        ipgClient.populateSessionResponse(htmlResponse.toString());
    }

    @SuppressWarnings("boxing")
    @Test(expected = IPGClientException.class)
    public void testPopulateSessionResponseWithInvalidHtml() throws IPGClientException {
        final String htmlResponse = null;
        ipgClient.populateSessionResponse(htmlResponse);
    }

    @Test
    public void testPostSessionRequest() throws IPGClientException, IllegalStateException, IOException, JAXBException {
        final TokenizeRequest request = populateRequest();
        final String htmlResponse = createHtmlResponse();
        final InputStream responseInputStream = new ByteArrayInputStream(htmlResponse.getBytes());
        final HttpEntity mockHttpEntity = mock(HttpEntity.class);
        given(mockHttpEntity.getContent()).willReturn(responseInputStream);
        final HttpResponse mockHttpResponse = mock(HttpResponse.class);
        given(mockHttpResponse.getEntity()).willReturn(mockHttpEntity);
        when(mockHttpClient.execute(any(HttpPost.class), Matchers.<ResponseHandler<String>>any())).thenAnswer(new Answer() {
            /*
             * (non-Javadoc)
             * 
             * @see org.mockito.stubbing.Answer#answer(org.mockito.invocation.InvocationOnMock)
             */
            @Override
            public Object answer(final InvocationOnMock invocation) throws IOException {
                final Object[] args = invocation.getArguments();
                final ResponseHandler responseHandler = (ResponseHandler)args[1];
                return responseHandler.handleResponse(mockHttpResponse);
            }
        });
        final String response = ipgClient.postSessionRequest(request);
        assertThat(response).isNotNull();
        assertThat(response).isEqualTo(htmlResponse);
        final ArgumentCaptor<HttpPost> httpPostCaptor = ArgumentCaptor.forClass(HttpPost.class);
        final ArgumentCaptor<ResponseHandler> responseHandlerCaptor = ArgumentCaptor.forClass(ResponseHandler.class);
        verify(mockHttpClient).execute(httpPostCaptor.capture(), responseHandlerCaptor.capture());
        verify(ipgRequestParamatersBuilder).buildRequestParametersForSessionToken(request);
        final HttpPost httpPost = httpPostCaptor.getValue();
        assertThat(httpPost.getURI().toString()).isEqualTo(URL);
        final HttpEntity requestEntity = httpPost.getEntity();
        assertThat(requestEntity).isInstanceOf(UrlEncodedFormEntity.class);
        final UrlEncodedFormEntity urlEncodedFormEntity = (UrlEncodedFormEntity)requestEntity;
        assertThat(urlEncodedFormEntity.getContentType().getName()).isEqualTo("Content-Type");
        assertThat(urlEncodedFormEntity.getContentType().getValue()).contains("application/x-www-form-urlencoded");
    }

    @Test
    public void testPostSessionRequestWithNullResponse() throws IPGClientException, IllegalStateException, IOException,
            JAXBException {
        final TokenizeRequest request = populateRequest();
        final HttpResponse mockHttpResponse = mock(HttpResponse.class);
        given(mockHttpResponse.getEntity()).willReturn(null);
        when(mockHttpClient.execute(any(HttpPost.class), Matchers.<ResponseHandler<String>>any())).thenAnswer(new Answer() {
            /*
             * (non-Javadoc)
             * 
             * @see org.mockito.stubbing.Answer#answer(org.mockito.invocation.InvocationOnMock)
             */
            @Override
            public Object answer(final InvocationOnMock invocation) throws IOException {
                final Object[] args = invocation.getArguments();
                final ResponseHandler responseHandler = (ResponseHandler)args[1];
                return responseHandler.handleResponse(mockHttpResponse);
            }
        });
        final String response = ipgClient.postSessionRequest(request);
        assertThat(response).isNull();
        final ArgumentCaptor<HttpPost> httpPostCaptor = ArgumentCaptor.forClass(HttpPost.class);
        final ArgumentCaptor<ResponseHandler> responseHandlerCaptor = ArgumentCaptor.forClass(ResponseHandler.class);
        verify(mockHttpClient).execute(httpPostCaptor.capture(), responseHandlerCaptor.capture());
        verify(ipgRequestParamatersBuilder).buildRequestParametersForSessionToken(request);
        final HttpPost httpPost = httpPostCaptor.getValue();
        assertThat(httpPost.getURI().toString()).isEqualTo(URL);
        final HttpEntity requestEntity = httpPost.getEntity();
        assertThat(requestEntity).isInstanceOf(UrlEncodedFormEntity.class);
        final UrlEncodedFormEntity urlEncodedFormEntity = (UrlEncodedFormEntity)requestEntity;
        assertThat(urlEncodedFormEntity.getContentType().getName()).isEqualTo("Content-Type");
        assertThat(urlEncodedFormEntity.getContentType().getValue()).contains("application/x-www-form-urlencoded");
    }

    @Test
    public void testSubmitSinglePayment() throws IPGClientException, JAXBException {
        final SubmitSinglePaymentRequest request = mock(SubmitSinglePaymentRequest.class);
        final Transaction transaction = mock(Transaction.class);
        when(request.getTransaction()).thenReturn(transaction);
        when(ipgHelper.marshalTransaction(transaction)).thenReturn("xml");
        when(ipgSoapService.submitSinglePayment("xml")).thenReturn("responseXml");
        final Response response = mock(Response.class);
        when(ipgHelper.unmarshalResponse("responseXml")).thenReturn(response);
        ipgClient.setUserName("userName");
        ipgClient.setPassword("123");

        final SubmitSinglePaymentResponse ipgResponse = ipgClient.submitSinglePayment(request);

        verify(ipgHelper).marshalTransaction(transaction);
        verify(transaction).setSecurity(ipgClient.getSecurity());
        verify(ipgSoapService).submitSinglePayment("xml");
        assertEquals(ipgResponse.getResponse(), response);

    }

    @Test
    public void testSubmitSingleRefund() throws IPGClientException, JAXBException {
        final SubmitSingleRefundRequest request = mock(SubmitSingleRefundRequest.class);
        final RefundInfo refundInfo = mock(RefundInfo.class);
        when(request.getRefundInfo()).thenReturn(refundInfo);
        when(ipgHelper.marshalRefundInfo(refundInfo)).thenReturn("xml");
        when(ipgSoapService.submitSingleRefund("xml")).thenReturn("responseXml");
        final Response response = mock(Response.class);
        when(ipgHelper.unmarshalResponse("responseXml")).thenReturn(response);
        ipgClient.setUserName("userName");
        ipgClient.setPassword("123");
        final SubmitSingleRefundResponse ipgResponse = ipgClient.submitSingleRefund(request);
        verify(ipgHelper).marshalRefundInfo(refundInfo);
        verify(refundInfo).setSecurity(ipgClient.getSecurity());
        verify(ipgSoapService).submitSingleRefund("xml");
        assertEquals(response, ipgResponse.getResponse());

    }

    @Test(expected = IPGClientException.class)
    public void testSubmitSingleRefundWithEmptyInfo() throws IPGClientException, JAXBException {
        final SubmitSingleRefundRequest request = mock(SubmitSingleRefundRequest.class);
        when(request.getRefundInfo()).thenReturn(null);
        ipgClient.submitSingleRefund(request);
    }

    @Test(expected = IPGClientException.class)
    public void testSubmitSingleRefundWithJAXBException() throws IPGClientException, JAXBException {
        final SubmitSingleRefundRequest request = mock(SubmitSingleRefundRequest.class);
        final RefundInfo refundInfo = mock(RefundInfo.class);
        when(request.getRefundInfo()).thenReturn(refundInfo);
        when(ipgHelper.marshalRefundInfo(refundInfo)).thenReturn("xml");
        when(ipgSoapService.submitSingleRefund("xml")).thenReturn("responseXml");
        when(ipgHelper.unmarshalResponse("responseXml")).thenThrow(new JAXBException("test"));
        ipgClient.setUserName("userName");
        ipgClient.setPassword("123");
        ipgClient.submitSingleRefund(request);

    }

    @Test(expected = IPGClientException.class)
    public void testSubmitSingleRefundWithEmptyResponse() throws IPGClientException, JAXBException {
        final SubmitSingleRefundRequest request = mock(SubmitSingleRefundRequest.class);
        final RefundInfo refundInfo = mock(RefundInfo.class);
        when(request.getRefundInfo()).thenReturn(refundInfo);
        when(ipgHelper.marshalRefundInfo(refundInfo)).thenReturn("xml");
        when(ipgSoapService.submitSingleRefund("xml")).thenReturn("responseXml");
        when(ipgHelper.unmarshalResponse("responseXml")).thenReturn(null);
        ipgClient.setUserName("userName");
        ipgClient.setPassword("123");
        ipgClient.submitSingleRefund(request);

    }

    @Test(expected = IPGClientException.class)
    public void testSubmitSingleRefundWithNullRequest() throws IPGClientException, JAXBException {
        final SubmitSingleRefundRequest request = null;
        ipgClient.submitSingleRefund(request);
        Mockito.verifyZeroInteractions(ipgSoapService);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateTransactionResponseWithError() throws IPGClientException {
        final String html = populateHtmlResponseErrorTransactionResult();
        final TransactionsResultResponse response = ipgClient.populateTransactionResultsResponse(html);
        Assert.assertNotNull(response);
        Assert.assertEquals("Could not find details for supplied SessionId and SST", response.getQueryError());
        Assert.assertEquals("False", response.getQueryResult());
        Assert.assertNull(response.getSessionId());
        Assert.assertNull(response.getSessionKey());
        Assert.assertNull(response.getSessionToken());
        Assert.assertNull(response.getAmount());
        Assert.assertNull(response.getEncodedCardResultsData());
        Assert.assertNull(response.getCustomerNumber());
        Assert.assertNull(response.getCustReference());
        Assert.assertNull(response.getTransactionResult());

    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateTransactionResponseWithNullHtml() throws IPGClientException {
        final String html = null;
        final TransactionsResultResponse response = ipgClient.populateTransactionResultsResponse(html);
        Assert.assertNull(response);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateTransactionResponseWithEmptyString() throws IPGClientException {
        final String html = "";
        final TransactionsResultResponse response = ipgClient.populateTransactionResultsResponse(html);
        Assert.assertNull(response);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateTransactionResponseSucccess() throws IPGClientException {
        final String html = populateHtmlResponseValidTransactionResult();
        final TransactionsResultResponse response = ipgClient.populateTransactionResultsResponse(html);
        Assert.assertNotNull(response);
        Assert.assertNull(response.getQueryError());
        Assert.assertNull(response.getQueryResult());
        Assert.assertEquals("ABC123", response.getSessionId());
        Assert.assertEquals("8b9daccb", response.getSessionToken());
        Assert.assertEquals("XYZ543", response.getSessionKey());
        Assert.assertEquals("123456", response.getCustReference());
        Assert.assertEquals("10000", response.getAmount());
        Assert.assertEquals("madhu@test.com", response.getCustomerNumber());
        Assert.assertEquals("1", response.getTransactionResult());
        Assert.assertEquals(
                "PENhcmRSZXN1bHRzPjxSZXN1bHQ%2bPERldGFpbHM%2bPEFtb3VudD4zOTAwPC9BbW91bnQ%2bPFRyYW5zVHlwZT4yPC9UcmFuc1R5cGU%2bPFJlY2VpcHQ%2bODk4ODc1Njc8L1JlY2VpcHQ%2bPFJlc3BDb2RlPjA8L1Jlc3BDb2RlPjxSZXNwVGV4dD5BcHByb3ZlZDwvUmVzcFRleHQ%2bPFRybkRhdGVUaW1lPjIwMTUtMDktMDcgMTY6MzA6NTE8L1RybkRhdGVUaW1lPjxTZXR0bGVtZW50RGF0ZT4yMDE1LTA5LTA3PC9TZXR0bGVtZW50RGF0ZT48L0RldGFpbHM%2bPENhcmQ%2bPENhcmRUeXBlPlZpc2E8L0NhcmRUeXBlPjxNYXNrZWRDYXJkPjQyNDI0MioqKioqKjQyNDI8L01hc2tlZENhcmQ%2bPENhcmRFeHBpcnk%2bMTIvMjA8L0NhcmRFeHBpcnk%2bPEJJTj40MjwvQklOPjxQcm9tb0lEPjwvUHJvbW9JRD48VG9rZW4%2bOTk0NDIxMjM0NTY3MDQyNDI8L1Rva2VuPjxUb2tlbkV4cGlyeT4wOC8xMi8yMDwvVG9rZW5FeHBpcnk%2bPC9DYXJkPjxGcmF1ZD48VGhyZWVEUz48VGhyZWVEU09yZGVyTnVtPjwvVGhyZWVEU09yZGVyTnVtPjxUaHJlZURTU2Vzc1Rva2VuPjwvVGhyZWVEU1Nlc3NUb2tlbj48RUNJPjwvRUNJPjxWRVI%2bPC9WRVI%2bPFBBUj48L1BBUj48U2lnVmVyaWY%2bPC9TaWdWZXJpZj48Q0FWVj48L0NBVlY%2bPFhJRD48L1hJRD48Q0FSPjwvQ0FSPjwvVGhyZWVEUz48UmVEPjxSZURPcmRlck51bWJlcj48L1JlRE9yZGVyTnVtYmVyPjxSRVFfSUQ%2bPC9SRVFfSUQ%2bPEZSQVVEX1JTUF9DRD48L0ZSQVVEX1JTUF9DRD48RlJBVURfU1RBVF9DRD48L0ZSQVVEX1NUQVRfQ0Q%2bPFNUQVRfQ0Q%2bPC9TVEFUX0NEPjxPUkRfSUQ%2bPC9PUkRfSUQ%2bPC9SZUQ%2bPC9GcmF1ZD48L1Jlc3VsdD48L0NhcmRSZXN1bHRzPg%3d%3d",
                response.getEncodedCardResultsData());

    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateTransactionResponseWithCustomeremail() throws IPGClientException {
        final String html = populateHtmlResponseValidTransactionResult();
        final TransactionsResultResponse response = ipgClient.populateTransactionResultsResponse(html);
        Assert.assertNotNull(response);
        Assert.assertNull(response.getQueryError());
        Assert.assertNull(response.getQueryResult());
        Assert.assertEquals("ABC123", response.getSessionId());
        Assert.assertEquals("8b9daccb", response.getSessionToken());
        Assert.assertEquals("XYZ543", response.getSessionKey());
        Assert.assertEquals("123456", response.getCustReference());
        Assert.assertEquals("10000", response.getAmount());
        Assert.assertEquals("madhu@test.com", response.getCustomerNumber());
        Assert.assertEquals("1", response.getTransactionResult());
        Assert.assertEquals(
                "PENhcmRSZXN1bHRzPjxSZXN1bHQ%2bPERldGFpbHM%2bPEFtb3VudD4zOTAwPC9BbW91bnQ%2bPFRyYW5zVHlwZT4yPC9UcmFuc1R5cGU%2bPFJlY2VpcHQ%2bODk4ODc1Njc8L1JlY2VpcHQ%2bPFJlc3BDb2RlPjA8L1Jlc3BDb2RlPjxSZXNwVGV4dD5BcHByb3ZlZDwvUmVzcFRleHQ%2bPFRybkRhdGVUaW1lPjIwMTUtMDktMDcgMTY6MzA6NTE8L1RybkRhdGVUaW1lPjxTZXR0bGVtZW50RGF0ZT4yMDE1LTA5LTA3PC9TZXR0bGVtZW50RGF0ZT48L0RldGFpbHM%2bPENhcmQ%2bPENhcmRUeXBlPlZpc2E8L0NhcmRUeXBlPjxNYXNrZWRDYXJkPjQyNDI0MioqKioqKjQyNDI8L01hc2tlZENhcmQ%2bPENhcmRFeHBpcnk%2bMTIvMjA8L0NhcmRFeHBpcnk%2bPEJJTj40MjwvQklOPjxQcm9tb0lEPjwvUHJvbW9JRD48VG9rZW4%2bOTk0NDIxMjM0NTY3MDQyNDI8L1Rva2VuPjxUb2tlbkV4cGlyeT4wOC8xMi8yMDwvVG9rZW5FeHBpcnk%2bPC9DYXJkPjxGcmF1ZD48VGhyZWVEUz48VGhyZWVEU09yZGVyTnVtPjwvVGhyZWVEU09yZGVyTnVtPjxUaHJlZURTU2Vzc1Rva2VuPjwvVGhyZWVEU1Nlc3NUb2tlbj48RUNJPjwvRUNJPjxWRVI%2bPC9WRVI%2bPFBBUj48L1BBUj48U2lnVmVyaWY%2bPC9TaWdWZXJpZj48Q0FWVj48L0NBVlY%2bPFhJRD48L1hJRD48Q0FSPjwvQ0FSPjwvVGhyZWVEUz48UmVEPjxSZURPcmRlck51bWJlcj48L1JlRE9yZGVyTnVtYmVyPjxSRVFfSUQ%2bPC9SRVFfSUQ%2bPEZSQVVEX1JTUF9DRD48L0ZSQVVEX1JTUF9DRD48RlJBVURfU1RBVF9DRD48L0ZSQVVEX1NUQVRfQ0Q%2bPFNUQVRfQ0Q%2bPC9TVEFUX0NEPjxPUkRfSUQ%2bPC9PUkRfSUQ%2bPC9SZUQ%2bPC9GcmF1ZD48L1Jlc3VsdD48L0NhcmRSZXN1bHRzPg%3d%3d",
                response.getEncodedCardResultsData());

    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateTransactionResponseWithSavedCards() throws IPGClientException {
        final String html = populateHtmlResponseValidTransactionResultWithSavedCards();
        final TransactionsResultResponse response = ipgClient.populateTransactionResultsResponse(html);
        Assert.assertNotNull(response);
        Assert.assertNull(response.getQueryError());
        Assert.assertNull(response.getQueryResult());
        Assert.assertEquals("ABC123", response.getSessionId());
        Assert.assertEquals("8b9daccb", response.getSessionToken());
        Assert.assertEquals("XYZ543", response.getSessionKey());
        Assert.assertEquals("123456", response.getCustReference());
        Assert.assertEquals("10000", response.getAmount());
        Assert.assertEquals("madhu@test.com", response.getCustomerNumber());
        Assert.assertEquals("1", response.getTransactionResult());
        Assert.assertEquals(
                "PENhcmRSZXN1bHRzPjxSZXN1bHQ%2bPERldGFpbHM%2bPEFtb3VudD4zOTAwPC9BbW91bnQ%2bPFRyYW5zVHlwZT4yPC9UcmFuc1R5cGU%2bPFJlY2VpcHQ%2bODk4ODc1Njc8L1JlY2VpcHQ%2bPFJlc3BDb2RlPjA8L1Jlc3BDb2RlPjxSZXNwVGV4dD5BcHByb3ZlZDwvUmVzcFRleHQ%2bPFRybkRhdGVUaW1lPjIwMTUtMDktMDcgMTY6MzA6NTE8L1RybkRhdGVUaW1lPjxTZXR0bGVtZW50RGF0ZT4yMDE1LTA5LTA3PC9TZXR0bGVtZW50RGF0ZT48L0RldGFpbHM%2bPENhcmQ%2bPENhcmRUeXBlPlZpc2E8L0NhcmRUeXBlPjxNYXNrZWRDYXJkPjQyNDI0MioqKioqKjQyNDI8L01hc2tlZENhcmQ%2bPENhcmRFeHBpcnk%2bMTIvMjA8L0NhcmRFeHBpcnk%2bPEJJTj40MjwvQklOPjxQcm9tb0lEPjwvUHJvbW9JRD48VG9rZW4%2bOTk0NDIxMjM0NTY3MDQyNDI8L1Rva2VuPjxUb2tlbkV4cGlyeT4wOC8xMi8yMDwvVG9rZW5FeHBpcnk%2bPC9DYXJkPjxGcmF1ZD48VGhyZWVEUz48VGhyZWVEU09yZGVyTnVtPjwvVGhyZWVEU09yZGVyTnVtPjxUaHJlZURTU2Vzc1Rva2VuPjwvVGhyZWVEU1Nlc3NUb2tlbj48RUNJPjwvRUNJPjxWRVI%2bPC9WRVI%2bPFBBUj48L1BBUj48U2lnVmVyaWY%2bPC9TaWdWZXJpZj48Q0FWVj48L0NBVlY%2bPFhJRD48L1hJRD48Q0FSPjwvQ0FSPjwvVGhyZWVEUz48UmVEPjxSZURPcmRlck51bWJlcj48L1JlRE9yZGVyTnVtYmVyPjxSRVFfSUQ%2bPC9SRVFfSUQ%2bPEZSQVVEX1JTUF9DRD48L0ZSQVVEX1JTUF9DRD48RlJBVURfU1RBVF9DRD48L0ZSQVVEX1NUQVRfQ0Q%2bPFNUQVRfQ0Q%2bPC9TVEFUX0NEPjxPUkRfSUQ%2bPC9PUkRfSUQ%2bPC9SZUQ%2bPC9GcmF1ZD48L1Jlc3VsdD48L0NhcmRSZXN1bHRzPg%3d%3d",
                response.getEncodedCardResultsData());
        Assert.assertEquals(
                "PFNhdmVkQ2FyZHM%2bPENhcmQ%2bPFRva2VuPjk5NDQyMTIzNDU2NzA0MjQyPC9Ub2tlbj48VG9rZW5FeHBpcnk%2bMDgvMTIvMjA8L1Rva2VuRXhwaXJ5PjxDYXJkRXhwaXJ5PjA0LzIwPC9DYXJkRXhwaXJ5PjxDYXJkVHlwZT5WSVNBPC9DYXJkVHlwZT48TWFza2VkQ2FyZD40MjQyNDIqKioqKio0MjQyPC9NYXNrZWRDYXJkPiA8RGVmYXVsdENhcmQ%2bMTwvRGVmYXVsdENhcmQ%2bPEJJTj40MjwvQklOPjwvQ2FyZD48L1NhdmVkQ2FyZHM%2b",
                response.getEncodedSavedCardsData());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateTransactionResponseWithEmptySavedCards() throws IPGClientException {
        final String html = populateHtmlResponseValidTransactionResultWithEmptySavedCards();
        final TransactionsResultResponse response = ipgClient.populateTransactionResultsResponse(html);
        Assert.assertNotNull(response);
        Assert.assertNull(response.getQueryError());
        Assert.assertNull(response.getQueryResult());
        Assert.assertEquals("ABC123", response.getSessionId());
        Assert.assertEquals("8b9daccb", response.getSessionToken());
        Assert.assertEquals("XYZ543", response.getSessionKey());
        Assert.assertEquals("123456", response.getCustReference());
        Assert.assertEquals("10000", response.getAmount());
        Assert.assertEquals("madhu@test.com", response.getCustomerNumber());
        Assert.assertEquals("1", response.getTransactionResult());
        Assert.assertEquals(
                "PENhcmRSZXN1bHRzPjxSZXN1bHQ%2bPERldGFpbHM%2bPEFtb3VudD4zOTAwPC9BbW91bnQ%2bPFRyYW5zVHlwZT4yPC9UcmFuc1R5cGU%2bPFJlY2VpcHQ%2bODk4ODc1Njc8L1JlY2VpcHQ%2bPFJlc3BDb2RlPjA8L1Jlc3BDb2RlPjxSZXNwVGV4dD5BcHByb3ZlZDwvUmVzcFRleHQ%2bPFRybkRhdGVUaW1lPjIwMTUtMDktMDcgMTY6MzA6NTE8L1RybkRhdGVUaW1lPjxTZXR0bGVtZW50RGF0ZT4yMDE1LTA5LTA3PC9TZXR0bGVtZW50RGF0ZT48L0RldGFpbHM%2bPENhcmQ%2bPENhcmRUeXBlPlZpc2E8L0NhcmRUeXBlPjxNYXNrZWRDYXJkPjQyNDI0MioqKioqKjQyNDI8L01hc2tlZENhcmQ%2bPENhcmRFeHBpcnk%2bMTIvMjA8L0NhcmRFeHBpcnk%2bPEJJTj40MjwvQklOPjxQcm9tb0lEPjwvUHJvbW9JRD48VG9rZW4%2bOTk0NDIxMjM0NTY3MDQyNDI8L1Rva2VuPjxUb2tlbkV4cGlyeT4wOC8xMi8yMDwvVG9rZW5FeHBpcnk%2bPC9DYXJkPjxGcmF1ZD48VGhyZWVEUz48VGhyZWVEU09yZGVyTnVtPjwvVGhyZWVEU09yZGVyTnVtPjxUaHJlZURTU2Vzc1Rva2VuPjwvVGhyZWVEU1Nlc3NUb2tlbj48RUNJPjwvRUNJPjxWRVI%2bPC9WRVI%2bPFBBUj48L1BBUj48U2lnVmVyaWY%2bPC9TaWdWZXJpZj48Q0FWVj48L0NBVlY%2bPFhJRD48L1hJRD48Q0FSPjwvQ0FSPjwvVGhyZWVEUz48UmVEPjxSZURPcmRlck51bWJlcj48L1JlRE9yZGVyTnVtYmVyPjxSRVFfSUQ%2bPC9SRVFfSUQ%2bPEZSQVVEX1JTUF9DRD48L0ZSQVVEX1JTUF9DRD48RlJBVURfU1RBVF9DRD48L0ZSQVVEX1NUQVRfQ0Q%2bPFNUQVRfQ0Q%2bPC9TVEFUX0NEPjxPUkRfSUQ%2bPC9PUkRfSUQ%2bPC9SZUQ%2bPC9GcmF1ZD48L1Jlc3VsdD48L0NhcmRSZXN1bHRzPg%3d%3d",
                response.getEncodedCardResultsData());
        Assert.assertEquals("", response.getEncodedSavedCardsData());

    }

    @Test
    public void testPostGetTransactionRequest() throws IPGClientException, IllegalStateException, IOException {
        final TransactionQueryRequest request = populateTransactionRequest();
        final String htmlResponse = createHtmlResponse();
        final InputStream responseInputStream = new ByteArrayInputStream(htmlResponse.getBytes());
        final HttpEntity mockHttpEntity = mock(HttpEntity.class);
        given(mockHttpEntity.getContent()).willReturn(responseInputStream);
        final HttpResponse mockHttpResponse = mock(HttpResponse.class);
        given(mockHttpResponse.getEntity()).willReturn(mockHttpEntity);
        when(mockHttpClient.execute(any(HttpPost.class), Matchers.<ResponseHandler<String>>any())).thenAnswer(new Answer() {
            /*
             * (non-Javadoc)
             * 
             * @see org.mockito.stubbing.Answer#answer(org.mockito.invocation.InvocationOnMock)
             */
            @Override
            public Object answer(final InvocationOnMock invocation) throws IOException {
                final Object[] args = invocation.getArguments();
                final ResponseHandler responseHandler = (ResponseHandler)args[1];
                return responseHandler.handleResponse(mockHttpResponse);
            }
        });
        final String response = ipgClient.postGetTransactionResultsRequest(request);
        assertThat(response).isNotNull();
        assertThat(response).isEqualTo(htmlResponse);
        final ArgumentCaptor<HttpPost> httpPostCaptor = ArgumentCaptor.forClass(HttpPost.class);
        final ArgumentCaptor<ResponseHandler> responseHandlerCaptor = ArgumentCaptor.forClass(ResponseHandler.class);
        verify(mockHttpClient).execute(httpPostCaptor.capture(), responseHandlerCaptor.capture());
        verify(ipgRequestParamatersBuilder).buildRequestParamatersForGetTransactionResult(request);
        final HttpPost httpPost = httpPostCaptor.getValue();
        assertThat(httpPost.getURI().toString()).isEqualTo(URL);
        final HttpEntity requestEntity = httpPost.getEntity();
        assertThat(requestEntity).isInstanceOf(UrlEncodedFormEntity.class);
        final UrlEncodedFormEntity urlEncodedFormEntity = (UrlEncodedFormEntity)requestEntity;
        assertThat(urlEncodedFormEntity.getContentType().getName()).isEqualTo("Content-Type");
        assertThat(urlEncodedFormEntity.getContentType().getValue()).contains("application/x-www-form-urlencoded");
    }


    @Test
    public void testPostTransactionResultWithNullResponse() throws IPGClientException, IllegalStateException,
            IOException,
            JAXBException {
        final TransactionQueryRequest request = populateTransactionRequest();
        final HttpResponse mockHttpResponse = mock(HttpResponse.class);
        given(mockHttpResponse.getEntity()).willReturn(null);
        when(mockHttpClient.execute(any(HttpPost.class), Matchers.<ResponseHandler<String>>any())).thenAnswer(new Answer() {
            /*
             * (non-Javadoc)
             * 
             * @see org.mockito.stubbing.Answer#answer(org.mockito.invocation.InvocationOnMock)
             */
            @Override
            public Object answer(final InvocationOnMock invocation) throws IOException {
                final Object[] args = invocation.getArguments();
                final ResponseHandler responseHandler = (ResponseHandler)args[1];
                return responseHandler.handleResponse(mockHttpResponse);
            }
        });
        final String response = ipgClient.postGetTransactionResultsRequest(request);
        assertThat(response).isNull();
        final ArgumentCaptor<HttpPost> httpPostCaptor = ArgumentCaptor.forClass(HttpPost.class);
        final ArgumentCaptor<ResponseHandler> responseHandlerCaptor = ArgumentCaptor.forClass(ResponseHandler.class);
        verify(mockHttpClient).execute(httpPostCaptor.capture(), responseHandlerCaptor.capture());
        verify(ipgRequestParamatersBuilder).buildRequestParamatersForGetTransactionResult(request);
        final HttpPost httpPost = httpPostCaptor.getValue();
        assertThat(httpPost.getURI().toString()).isEqualTo(URL);
        final HttpEntity requestEntity = httpPost.getEntity();
        assertThat(requestEntity).isInstanceOf(UrlEncodedFormEntity.class);
        final UrlEncodedFormEntity urlEncodedFormEntity = (UrlEncodedFormEntity)requestEntity;
        assertThat(urlEncodedFormEntity.getContentType().getName()).isEqualTo("Content-Type");
        assertThat(urlEncodedFormEntity.getContentType().getValue()).contains("application/x-www-form-urlencoded");
    }

    @Test(expected = IPGClientException.class)
    public void testSubmitSingleVoidWithNullRequset() throws IPGClientException {
        final SingleVoidRequest request = null;
        ipgClient.submitSingleVoid(request);
        Mockito.verifyZeroInteractions(ipgSoapService);
    }

    @Test(expected = IPGClientException.class)
    public void testSubmitSingleVoidWithNullVoidInfoRequset() throws IPGClientException {
        final SingleVoidRequest request = new SingleVoidRequest();
        ipgClient.submitSingleVoid(request);
        Mockito.verifyZeroInteractions(ipgSoapService);
    }

    @Test(expected = IPGClientException.class)
    public void testSubmitSingleVoidInvocationWithJAXBException() throws IPGClientException, JAXBException {
        final SingleVoidRequest request = mock(SingleVoidRequest.class);
        final VoidInfo voidInfo = mock(VoidInfo.class);

        when(request.getVoidInfo()).thenReturn(voidInfo);
        when(ipgHelper.marshalVoidInfo(voidInfo)).thenReturn("xml");
        when(ipgSoapService.submitSingleVoid("xml")).thenReturn("responseXml");
        when(ipgHelper.unmarshalResponse("responseXml")).thenThrow(new JAXBException("test"));

        ipgClient.setUserName("userName");
        ipgClient.setPassword("123");

        ipgClient.submitSingleVoid(request);
    }

    @Test
    public void testSubmitSingleVoidInvocation() throws IPGClientException, JAXBException {
        final SingleVoidRequest request = mock(SingleVoidRequest.class);
        final VoidInfo voidInfo = mock(VoidInfo.class);

        when(request.getVoidInfo()).thenReturn(voidInfo);
        when(ipgHelper.marshalVoidInfo(voidInfo)).thenReturn("xml");
        when(ipgSoapService.submitSingleVoid("xml")).thenReturn("responseXml");

        final Response response = mock(Response.class);
        when(ipgHelper.unmarshalResponse("responseXml")).thenReturn(response);

        ipgClient.setUserName("userName");
        ipgClient.setPassword("123");

        final SingleVoidResponse ipgResponse = ipgClient.submitSingleVoid(request);

        verify(ipgHelper).marshalVoidInfo(voidInfo);
        verify(voidInfo).setSecurity(ipgClient.getSecurity());
        verify(ipgSoapService).submitSingleVoid("xml");
        assertEquals(response, ipgResponse.getResponse());
    }

    private String createHtmlResponse() {
        final StringBuilder builder = new StringBuilder();
        builder.append("<html><body><form>");
        builder.append("<input type=\"hidden\" name=\"SessionStored\" value=\"False\" />");
        builder.append("<input type=\"hidden\" name=\"SessionStoredError\" value=\"\" />");
        builder.append("<input type=\"hidden\" name=\"SST\" value=\"239-7392-2836-2771\" />");
        builder.append("</form></body></html>");
        return builder.toString();

    }

    private TokenizeRequest populateRequest() {
        final TokenizeRequest request = new TokenizeRequest();
        request.setCartId("100190");
        request.setCustomerNumber("cust001");
        request.setiFrameConfig("MobileConfig");
        request.setJsessionID("CE537711586707F4CAEE54CB777CE050");
        request.setMerchantNumber("5500");
        request.setPosCond("48");
        request.setSessionKey("12345688");
        request.setStoreNumber("5500");
        request.setTenderAmount("50.00");
        request.setUserUrl("/userurl");
        return request;

    }

    private TransactionQueryRequest populateTransactionRequest() {
        final TransactionQueryRequest request = new TransactionQueryRequest();
        request.setSessionId("1234555");
        request.setSessionToken("abcdefgh");
        return request;
    }

    private String populateHtmlResponseErrorTransactionResult() {
        final StringBuilder builder = new StringBuilder();
        builder.append("<html><body><form>");
        builder.append("<input type=\"hidden\" name=\"QueryResult\" value=\"False\" />");
        builder.append(
                "<input type=\"hidden\" name=\"QueryError\" value=\"Could not find details for supplied SessionId and SST\" />");
        builder.append("</form></body></html>");
        return builder.toString();

    }

    private String populateHtmlResponseValidTransactionResult() {
        final StringBuilder builder = new StringBuilder();
        builder.append("<html><body><form>");
        builder.append("<input type=\"hidden\" name=\"SessionId\" value=\"ABC123\" />");
        builder.append("<input type=\"hidden\" name=\"SST\" value=\"8b9daccb\" />");
        builder.append("<input type=\"hidden\" name=\"SessionKey\" value=\"XYZ543\" />");
        builder.append("<input type=\"hidden\" name=\"CustNumber\" value=\"madhu%40test.com\" />");
        builder.append("<input type=\"hidden\" name=\"CustRef\" value=\"123456\" />");
        builder.append("<input type=\"hidden\" name=\"Amount\" value=\"10000\" />");
        builder.append("<input type=\"hidden\"  name=\"TrnResult\" value=\"1\" />");
        builder.append(
                "<input type=\"hidden\" name=\"CardResults\" value=\"PENhcmRSZXN1bHRzPjxSZXN1bHQ%2bPERldGFpbHM%2bPEFtb3VudD4zOTAwPC9BbW91bnQ%2bPFRyYW5zVHlwZT4yPC9UcmFuc1R5cGU%2bPFJlY2VpcHQ%2bODk4ODc1Njc8L1JlY2VpcHQ%2bPFJlc3BDb2RlPjA8L1Jlc3BDb2RlPjxSZXNwVGV4dD5BcHByb3ZlZDwvUmVzcFRleHQ%2bPFRybkRhdGVUaW1lPjIwMTUtMDktMDcgMTY6MzA6NTE8L1RybkRhdGVUaW1lPjxTZXR0bGVtZW50RGF0ZT4yMDE1LTA5LTA3PC9TZXR0bGVtZW50RGF0ZT48L0RldGFpbHM%2bPENhcmQ%2bPENhcmRUeXBlPlZpc2E8L0NhcmRUeXBlPjxNYXNrZWRDYXJkPjQyNDI0MioqKioqKjQyNDI8L01hc2tlZENhcmQ%2bPENhcmRFeHBpcnk%2bMTIvMjA8L0NhcmRFeHBpcnk%2bPEJJTj40MjwvQklOPjxQcm9tb0lEPjwvUHJvbW9JRD48VG9rZW4%2bOTk0NDIxMjM0NTY3MDQyNDI8L1Rva2VuPjxUb2tlbkV4cGlyeT4wOC8xMi8yMDwvVG9rZW5FeHBpcnk%2bPC9DYXJkPjxGcmF1ZD48VGhyZWVEUz48VGhyZWVEU09yZGVyTnVtPjwvVGhyZWVEU09yZGVyTnVtPjxUaHJlZURTU2Vzc1Rva2VuPjwvVGhyZWVEU1Nlc3NUb2tlbj48RUNJPjwvRUNJPjxWRVI%2bPC9WRVI%2bPFBBUj48L1BBUj48U2lnVmVyaWY%2bPC9TaWdWZXJpZj48Q0FWVj48L0NBVlY%2bPFhJRD48L1hJRD48Q0FSPjwvQ0FSPjwvVGhyZWVEUz48UmVEPjxSZURPcmRlck51bWJlcj48L1JlRE9yZGVyTnVtYmVyPjxSRVFfSUQ%2bPC9SRVFfSUQ%2bPEZSQVVEX1JTUF9DRD48L0ZSQVVEX1JTUF9DRD48RlJBVURfU1RBVF9DRD48L0ZSQVVEX1NUQVRfQ0Q%2bPFNUQVRfQ0Q%2bPC9TVEFUX0NEPjxPUkRfSUQ%2bPC9PUkRfSUQ%2bPC9SZUQ%2bPC9GcmF1ZD48L1Jlc3VsdD48L0NhcmRSZXN1bHRzPg%3d%3d\"/>/");
        builder.append("</form></body></html>");
        return builder.toString();

    }

    private String populateHtmlResponseValidTransactionResultWithSavedCards() {
        final StringBuilder builder = new StringBuilder();
        builder.append("<html><body><form>");
        builder.append("<input type=\"hidden\" name=\"SessionId\" value=\"ABC123\" />");
        builder.append("<input type=\"hidden\" name=\"SST\" value=\"8b9daccb\" />");
        builder.append("<input type=\"hidden\" name=\"SessionKey\" value=\"XYZ543\" />");
        builder.append("<input type=\"hidden\" name=\"CustNumber\" value=\"madhu%40test.com\" />");
        builder.append("<input type=\"hidden\" name=\"CustRef\" value=\"123456\" />");
        builder.append("<input type=\"hidden\" name=\"Amount\" value=\"10000\" />");
        builder.append("<input type=\"hidden\"  name=\"TrnResult\" value=\"1\" />");
        builder.append(
                "<input type=\"hidden\" name=\"CardResults\" value=\"PENhcmRSZXN1bHRzPjxSZXN1bHQ%2bPERldGFpbHM%2bPEFtb3VudD4zOTAwPC9BbW91bnQ%2bPFRyYW5zVHlwZT4yPC9UcmFuc1R5cGU%2bPFJlY2VpcHQ%2bODk4ODc1Njc8L1JlY2VpcHQ%2bPFJlc3BDb2RlPjA8L1Jlc3BDb2RlPjxSZXNwVGV4dD5BcHByb3ZlZDwvUmVzcFRleHQ%2bPFRybkRhdGVUaW1lPjIwMTUtMDktMDcgMTY6MzA6NTE8L1RybkRhdGVUaW1lPjxTZXR0bGVtZW50RGF0ZT4yMDE1LTA5LTA3PC9TZXR0bGVtZW50RGF0ZT48L0RldGFpbHM%2bPENhcmQ%2bPENhcmRUeXBlPlZpc2E8L0NhcmRUeXBlPjxNYXNrZWRDYXJkPjQyNDI0MioqKioqKjQyNDI8L01hc2tlZENhcmQ%2bPENhcmRFeHBpcnk%2bMTIvMjA8L0NhcmRFeHBpcnk%2bPEJJTj40MjwvQklOPjxQcm9tb0lEPjwvUHJvbW9JRD48VG9rZW4%2bOTk0NDIxMjM0NTY3MDQyNDI8L1Rva2VuPjxUb2tlbkV4cGlyeT4wOC8xMi8yMDwvVG9rZW5FeHBpcnk%2bPC9DYXJkPjxGcmF1ZD48VGhyZWVEUz48VGhyZWVEU09yZGVyTnVtPjwvVGhyZWVEU09yZGVyTnVtPjxUaHJlZURTU2Vzc1Rva2VuPjwvVGhyZWVEU1Nlc3NUb2tlbj48RUNJPjwvRUNJPjxWRVI%2bPC9WRVI%2bPFBBUj48L1BBUj48U2lnVmVyaWY%2bPC9TaWdWZXJpZj48Q0FWVj48L0NBVlY%2bPFhJRD48L1hJRD48Q0FSPjwvQ0FSPjwvVGhyZWVEUz48UmVEPjxSZURPcmRlck51bWJlcj48L1JlRE9yZGVyTnVtYmVyPjxSRVFfSUQ%2bPC9SRVFfSUQ%2bPEZSQVVEX1JTUF9DRD48L0ZSQVVEX1JTUF9DRD48RlJBVURfU1RBVF9DRD48L0ZSQVVEX1NUQVRfQ0Q%2bPFNUQVRfQ0Q%2bPC9TVEFUX0NEPjxPUkRfSUQ%2bPC9PUkRfSUQ%2bPC9SZUQ%2bPC9GcmF1ZD48L1Jlc3VsdD48L0NhcmRSZXN1bHRzPg%3d%3d\"/>/");
        builder.append(
                "<input type=\"hidden\" name=\"SavedCards\" value=\"PFNhdmVkQ2FyZHM%2bPENhcmQ%2bPFRva2VuPjk5NDQyMTIzNDU2NzA0MjQyPC9Ub2tlbj48VG9rZW5FeHBpcnk%2bMDgvMTIvMjA8L1Rva2VuRXhwaXJ5PjxDYXJkRXhwaXJ5PjA0LzIwPC9DYXJkRXhwaXJ5PjxDYXJkVHlwZT5WSVNBPC9DYXJkVHlwZT48TWFza2VkQ2FyZD40MjQyNDIqKioqKio0MjQyPC9NYXNrZWRDYXJkPiA8RGVmYXVsdENhcmQ%2bMTwvRGVmYXVsdENhcmQ%2bPEJJTj40MjwvQklOPjwvQ2FyZD48L1NhdmVkQ2FyZHM%2b\"/>/");
        builder.append("</form></body></html>");
        return builder.toString();

    }

    private String populateHtmlResponseValidTransactionResultWithEmptySavedCards() {
        final StringBuilder builder = new StringBuilder();
        builder.append("<html><body><form>");
        builder.append("<input type=\"hidden\" name=\"SessionId\" value=\"ABC123\" />");
        builder.append("<input type=\"hidden\" name=\"SST\" value=\"8b9daccb\" />");
        builder.append("<input type=\"hidden\" name=\"SessionKey\" value=\"XYZ543\" />");
        builder.append("<input type=\"hidden\" name=\"CustNumber\" value=\"madhu%40test.com\" />");
        builder.append("<input type=\"hidden\" name=\"CustRef\" value=\"123456\" />");
        builder.append("<input type=\"hidden\" name=\"Amount\" value=\"10000\" />");
        builder.append("<input type=\"hidden\"  name=\"TrnResult\" value=\"1\" />");
        builder.append(
                "<input type=\"hidden\" name=\"CardResults\" value=\"PENhcmRSZXN1bHRzPjxSZXN1bHQ%2bPERldGFpbHM%2bPEFtb3VudD4zOTAwPC9BbW91bnQ%2bPFRyYW5zVHlwZT4yPC9UcmFuc1R5cGU%2bPFJlY2VpcHQ%2bODk4ODc1Njc8L1JlY2VpcHQ%2bPFJlc3BDb2RlPjA8L1Jlc3BDb2RlPjxSZXNwVGV4dD5BcHByb3ZlZDwvUmVzcFRleHQ%2bPFRybkRhdGVUaW1lPjIwMTUtMDktMDcgMTY6MzA6NTE8L1RybkRhdGVUaW1lPjxTZXR0bGVtZW50RGF0ZT4yMDE1LTA5LTA3PC9TZXR0bGVtZW50RGF0ZT48L0RldGFpbHM%2bPENhcmQ%2bPENhcmRUeXBlPlZpc2E8L0NhcmRUeXBlPjxNYXNrZWRDYXJkPjQyNDI0MioqKioqKjQyNDI8L01hc2tlZENhcmQ%2bPENhcmRFeHBpcnk%2bMTIvMjA8L0NhcmRFeHBpcnk%2bPEJJTj40MjwvQklOPjxQcm9tb0lEPjwvUHJvbW9JRD48VG9rZW4%2bOTk0NDIxMjM0NTY3MDQyNDI8L1Rva2VuPjxUb2tlbkV4cGlyeT4wOC8xMi8yMDwvVG9rZW5FeHBpcnk%2bPC9DYXJkPjxGcmF1ZD48VGhyZWVEUz48VGhyZWVEU09yZGVyTnVtPjwvVGhyZWVEU09yZGVyTnVtPjxUaHJlZURTU2Vzc1Rva2VuPjwvVGhyZWVEU1Nlc3NUb2tlbj48RUNJPjwvRUNJPjxWRVI%2bPC9WRVI%2bPFBBUj48L1BBUj48U2lnVmVyaWY%2bPC9TaWdWZXJpZj48Q0FWVj48L0NBVlY%2bPFhJRD48L1hJRD48Q0FSPjwvQ0FSPjwvVGhyZWVEUz48UmVEPjxSZURPcmRlck51bWJlcj48L1JlRE9yZGVyTnVtYmVyPjxSRVFfSUQ%2bPC9SRVFfSUQ%2bPEZSQVVEX1JTUF9DRD48L0ZSQVVEX1JTUF9DRD48RlJBVURfU1RBVF9DRD48L0ZSQVVEX1NUQVRfQ0Q%2bPFNUQVRfQ0Q%2bPC9TVEFUX0NEPjxPUkRfSUQ%2bPC9PUkRfSUQ%2bPC9SZUQ%2bPC9GcmF1ZD48L1Jlc3VsdD48L0NhcmRSZXN1bHRzPg%3d%3d\"/>/");
        builder.append("<input type=\"hidden\" name=\"SavedCards\" value=\"\"/>/");
        builder.append("</form></body></html>");
        return builder.toString();

    }

    @Test
    public void testSetCreditials() throws IPGClientException {
        final List<NameValuePair> params = new ArrayList<>();
        final String userName = "Target";
        final String password = "Target123";
        final String accountNumber = "123456789";

        ipgClient.setUserName(userName);
        ipgClient.setPassword(password);
        ipgClient.setAccountNumber(accountNumber);

        ipgClient.setCredentials(params);

        assertTrue(CollectionUtils.isNotEmpty(params));
        assertEquals(3, params.size());

        for (final NameValuePair nameValuePair : params) {
            if (nameValuePair.getName().equals("UserName")) {
                assertEquals(userName, nameValuePair.getValue());
            }
            if (nameValuePair.getName().equals("Password")) {
                assertEquals(password, nameValuePair.getValue());
            }
            if (nameValuePair.getName().equals("AccountNumber")) {
                assertEquals(accountNumber, nameValuePair.getValue());
            }
        }
    }

    @Test
    public void testSubmitSingleCardSetAccountNumber() throws IPGClientException {
        final SubmitSinglePaymentRequest request = new SubmitSinglePaymentRequest();
        final Transaction transaction = new Transaction();
        request.setTransaction(transaction);
        final String accountNumber = "123456789";

        ipgClient.setAccountNumber(accountNumber);
        ipgClient.submitSinglePayment(request);

        assertEquals(accountNumber, request.getTransaction().getAccountNumber());
    }

    @Test
    public void testQueryTransaction() throws IPGClientException {
        final QueryTransaction queryRequest = mock(QueryTransaction.class);
        final Criteria criteria = mock(Criteria.class);
        when(queryRequest.getCriteria()).thenReturn(criteria);
        ipgClient.queryTransaction(queryRequest);
        verify(queryRequest).setSecurity(Mockito.any(Security.class));
        verify(criteria).setAccountNumber(anyString());
    }

    @Test(expected = IPGClientException.class)
    public void testQueryTransactionWithException() throws IPGClientException, JAXBException {
        final QueryTransaction queryRequest = mock(QueryTransaction.class);
        final Criteria criteria = mock(Criteria.class);
        when(queryRequest.getCriteria()).thenReturn(criteria);
        when(ipgHelper.toXml(queryRequest)).thenThrow(new JAXBException("test"));
        ipgClient.queryTransaction(queryRequest);
        verify(queryRequest).setSecurity(security);
        verify(criteria).setAccountNumber(anyString());
    }
}