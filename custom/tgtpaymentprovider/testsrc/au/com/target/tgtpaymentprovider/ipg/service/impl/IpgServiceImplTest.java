package au.com.target.tgtpaymentprovider.ipg.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentVoidRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.result.TargetCardPaymentResult;
import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpayment.dto.AmountType;
import au.com.target.tgtpayment.dto.CreditCard;
import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpaymentprovider.ipg.client.IPGClient;
import au.com.target.tgtpaymentprovider.ipg.data.request.SingleVoidRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSinglePaymentRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSingleRefundRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.TokenizeRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.TransactionQueryRequest;
import au.com.target.tgtpaymentprovider.ipg.data.response.SessionResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.SingleVoidResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.SubmitSinglePaymentResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.SubmitSingleRefundResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.TransactionsResultResponse;
import au.com.target.tgtpaymentprovider.ipg.data.soap.QueryResponse;
import au.com.target.tgtpaymentprovider.ipg.data.soap.QueryTransaction;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Response;
import au.com.target.tgtpaymentprovider.ipg.exception.IPGClientException;
import au.com.target.tgtpaymentprovider.ipg.exception.IpgServiceException;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class IpgServiceImplTest {
    /**
     * 
     */
    private static final String[] IPG_DATE_FORMATS = new String[] { "yyyy-MM-dd HH:mm:ss" };
    private static final String DIRECT_LINK = "Target_SavedCard";
    private static final String DIRECT_LINK_PREORDER = "target_preorder_savedcard_deferredcapture";
    private static final String DIRECT_LINK_UPDATECARD = "target_preorder_updatecard_deferredcapture";
    private static final String DIRECT_LINK_MULTIPLE = "Target_SavedCard_MultiplePay";
    private static final String POS_COND = "48";
    private static final String STORE_NUMBER = "5599";
    private static final String SESSIONID = "121654ABCDE";
    private static final String SESSION_KEY = "dfhdsfhdsf-dsflkdslkfsd-324324-fjsdlkfj";
    private static final String ORDER_ID = "oid_123456";
    private static final String USER_ID = "uid_123456";
    private static final String SESSION_TOKEN = "6af55619-96ab-42d2-89bc-4bbed482ab77";


    @InjectMocks
    private final IpgServiceImpl ipgService = new IpgServiceImpl();

    @Mock
    private KeyGenerator keyGenerator;

    @Mock
    private IPGClient ipgClient;

    @Mock
    private TargetCaptureRequest captureRequest;

    @Mock
    private TargetCardResult ipgCard;

    private Order order;

    private TargetCreateSubscriptionRequest request;

    @Before
    public void setup() {
        order = createOrder();
        request = new TargetCreateSubscriptionRequest();
        request.setReturnUrl("returnUrl");
        request.setOrder(order);
        request.setSessionId(SESSIONID);
        request.setIpgPaymentTemplateType(IpgPaymentTemplateType.CREDITCARDSINGLE);
        given(keyGenerator.generate()).willReturn(SESSION_KEY);
        ipgService.setDirectLinkSinglePay(DIRECT_LINK);
        ipgService.setDirectLinkSinglePayPreOrder(DIRECT_LINK_PREORDER);
        ipgService.setDirectLinkMultiplePay(DIRECT_LINK_MULTIPLE);
        ipgService.setDirectLinkUpdateCard(DIRECT_LINK_UPDATECARD);
        ipgService.setPosCond(POS_COND);
        ipgService.setStoreNumber(STORE_NUMBER);
        given(captureRequest.getTotalAmount()).willReturn(BigDecimal.TEN);
        given(captureRequest.getOrder()).willReturn(order);
        given(captureRequest.getCardResult()).willReturn(ipgCard);
    }

    private Order createOrder() {
        final Order testOrder = new Order();

        final BigDecimal totalAmount = new BigDecimal("50.55");
        final AmountType amountType = new AmountType();
        amountType.setAmount(totalAmount);
        testOrder.setOrderTotalCents(Integer.valueOf(10000));
        testOrder.setOrderTotalAmount(amountType);
        testOrder.setOrderId(ORDER_ID);
        testOrder.setUserId(USER_ID);

        return testOrder;
    }

    @Test
    public void testCreateTokenizeRequest() {

        final TokenizeRequest tokenizeRequest = ipgService.createTokenizeRequest(request);
        assertThat(tokenizeRequest).isNotNull();
        assertThat(tokenizeRequest.getJsessionID()).isEqualTo(SESSIONID);
        assertThat(tokenizeRequest.getSessionKey()).isEqualTo(SESSION_KEY);
        assertThat(tokenizeRequest.getiFrameConfig()).isEqualTo(DIRECT_LINK);
        assertThat(tokenizeRequest.getCartId()).isEqualTo(ORDER_ID);
        assertThat(tokenizeRequest.getCustomerNumber()).isEqualTo(USER_ID);
        assertThat(tokenizeRequest.getPosCond()).isEqualTo(POS_COND);
        assertThat(tokenizeRequest.getStoreNumber()).isEqualTo(STORE_NUMBER);
        assertThat(tokenizeRequest.getTenderAmount()).isEqualTo("10000");
    }

    @Test
    public void testCreateTokenizeRequestForPreOrder() {
        order.setContainPreOrderItems(Boolean.TRUE);
        request.setOrder(order);
        final TokenizeRequest tokenizeRequest = ipgService.createTokenizeRequest(request);
        assertThat(tokenizeRequest).isNotNull();
        assertThat(tokenizeRequest.getJsessionID()).isEqualTo(SESSIONID);
        assertThat(tokenizeRequest.getSessionKey()).isEqualTo(SESSION_KEY);
        assertThat(tokenizeRequest.getiFrameConfig()).isEqualTo(DIRECT_LINK_PREORDER);
        assertThat(tokenizeRequest.getCartId()).isEqualTo(ORDER_ID);
        assertThat(tokenizeRequest.getCustomerNumber()).isEqualTo(USER_ID);
        assertThat(tokenizeRequest.getPosCond()).isEqualTo(POS_COND);
        assertThat(tokenizeRequest.getStoreNumber()).isEqualTo(STORE_NUMBER);
        assertThat(tokenizeRequest.getTenderAmount()).isEqualTo("10000");
    }

    @Test
    public void testCreateTokenizeRequestForUpdateCard() {
        request.setOrder(order);
        request.setIpgPaymentTemplateType(IpgPaymentTemplateType.UPDATECARD);
        final TokenizeRequest tokenizeRequest = ipgService.createTokenizeRequest(request);

        assertThat(tokenizeRequest);
        assertThat(tokenizeRequest.getJsessionID()).isEqualTo(SESSIONID);
        assertThat(tokenizeRequest.getSessionKey()).isEqualTo(SESSION_KEY);
        assertThat(tokenizeRequest.getCartId()).isEqualTo(ORDER_ID);
        assertThat(tokenizeRequest.getCustomerNumber()).isEqualTo(USER_ID);
        assertThat(tokenizeRequest.getPosCond()).isEqualTo(POS_COND);
        assertThat(tokenizeRequest.getStoreNumber()).isEqualTo(STORE_NUMBER);
        assertThat(tokenizeRequest.getTenderAmount()).isEqualTo("10000");
        assertThat(tokenizeRequest.getiFrameConfig()).isEqualTo(DIRECT_LINK_UPDATECARD);
    }

    @Test(expected = IpgServiceException.class)
    public void testRequestIPGSessionTokenWhenClientThrowsException() throws IPGClientException, IpgServiceException {
        given(ipgClient.requestIPGSessionToken(any(TokenizeRequest.class))).willThrow(
                new IPGClientException("Error while communicating with IPG"));
        ipgService.createSession(request);
    }

    @Test(expected = IpgServiceException.class)
    public void testRequestIPGSessionTokenWhenClientThrowsRuntimeException() throws IPGClientException,
            IpgServiceException {
        given(ipgClient.requestIPGSessionToken(any(TokenizeRequest.class))).willThrow(
                new RuntimeException("Error while communicating with IPG"));
        ipgService.createSession(request);
    }

    @Test
    public void testRequestIPGSessionSuccess() throws IPGClientException, IpgServiceException {
        final SessionResponse sessionResponse = new SessionResponse();
        sessionResponse.setSecureSessionToken(SESSION_TOKEN);
        sessionResponse.setSessionStored(true);
        sessionResponse.setSessionStoredError(null);

        given(ipgClient.requestIPGSessionToken(any(TokenizeRequest.class))).willReturn(sessionResponse);

        final SessionResponse sessionResponseReturned = ipgService.createSession(request);

        assertThat(sessionResponseReturned).isNotNull();
        assertThat(sessionResponseReturned.getSecureSessionToken()).isEqualTo(SESSION_TOKEN);
        assertThat(sessionResponseReturned.getSessionStoredError()).isNull();
    }

    @Test
    public void testCreateTransactionQueryRequest() {
        final TransactionQueryRequest transactionQueryRequest = ipgService.createTransactionQueryRequest(SESSIONID,
                SESSION_TOKEN);

        assertThat(transactionQueryRequest).isNotNull();
        assertThat(transactionQueryRequest.getSessionId()).isEqualTo(SESSIONID);
        assertThat(transactionQueryRequest.getSessionToken()).isEqualTo(SESSION_TOKEN);
    }

    @Test(expected = IpgServiceException.class)
    public void testQueryTransactionResultsWhenClientThrowsException() throws IPGClientException, IpgServiceException {
        given(ipgClient.getTransactionResults(any(TransactionQueryRequest.class))).willThrow(
                new IPGClientException("Error while retrieving transaction results"));
        ipgService.queryTransactionResults(SESSIONID, SESSION_TOKEN);
    }

    @Test(expected = IpgServiceException.class)
    public void testQueryTransactionResultsWhenClientThrowsRuntimeException() throws IPGClientException,
            IpgServiceException {
        given(ipgClient.getTransactionResults(any(TransactionQueryRequest.class))).willThrow(
                new RuntimeException("Error while retrieving transaction results"));
        ipgService.queryTransactionResults(SESSIONID, SESSION_TOKEN);
    }

    @Test
    public void testQueryTransactionResults() throws IPGClientException, IpgServiceException {
        final TransactionsResultResponse transactionResultResponse = new TransactionsResultResponse();
        given(ipgClient.getTransactionResults(any(TransactionQueryRequest.class)))
                .willReturn(transactionResultResponse);

        final TransactionsResultResponse transactionResultResponseReturned = ipgService.queryTransactionResults(
                SESSIONID,
                SESSION_TOKEN);

        assertThat(transactionResultResponseReturned).isNotNull();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testSubmitSinglePayment() throws IpgServiceException, IPGClientException {
        final TargetCardPaymentResult transactionResult = mock(TargetCardPaymentResult.class);
        final SubmitSinglePaymentResponse submitSinglePaymentResponse = mock(SubmitSinglePaymentResponse.class);
        final Response response = mock(Response.class);

        given(ipgCard.getTargetCardPaymentResult()).willReturn(transactionResult);
        given(submitSinglePaymentResponse.getResponse()).willReturn(response);
        given(ipgClient.submitSinglePayment(any(SubmitSinglePaymentRequest.class))).willReturn(
                submitSinglePaymentResponse);
        given(response.getResponseCode()).willReturn("01");
        given(response.getReceipt()).willReturn("re2");
        given(transactionResult.getAmount()).willReturn(BigDecimal.valueOf(11.25));
        final Order ord = mock(Order.class);
        final AmountType amount = new AmountType();
        amount.setAmount(new BigDecimal("12.99"));
        given(ord.getOrderTotalAmount()).willReturn(amount);

        final TargetCardPaymentResult result = ipgService.submitSinglePayment(captureRequest);

        verify(response).getResponseCode();
        verify(response).getReceipt();
        verify(response).getTimestamp();
        verify(response).getDeclinedCode();
        verify(response).getDeclinedMessage();
        verify(ord, times(0)).getOrderTotalAmount();
        verifyNoMoreInteractions(response);
        verify(ipgClient).submitSinglePayment(any(SubmitSinglePaymentRequest.class));
        assertThat(result == transactionResult).isTrue();
        assertThat(response.getResponseCode()).isEqualTo("01");
        assertThat(response.getReceipt()).isEqualTo("re2");
    }

    @SuppressWarnings("boxing")
    @Test(expected = IpgServiceException.class)
    public void testSubmitSinglePaymentWithException() throws IpgServiceException, IPGClientException {
        final TargetCardPaymentResult transactionResult = mock(TargetCardPaymentResult.class);
        given(ipgCard.getTargetCardPaymentResult()).willReturn(transactionResult);
        given(ipgClient.submitSinglePayment(any(SubmitSinglePaymentRequest.class))).willThrow(
                new IPGClientException("test"));

        ipgService.submitSinglePayment(captureRequest);
    }

    @SuppressWarnings("boxing")
    @Test(expected = IpgServiceException.class)
    public void testSubmitSinglePaymentWithRuntimeException() throws IpgServiceException, IPGClientException {
        final TargetCardPaymentResult transactionResult = mock(TargetCardPaymentResult.class);
        given(ipgCard.getTargetCardPaymentResult()).willReturn(transactionResult);
        given(ipgClient.submitSinglePayment(any(SubmitSinglePaymentRequest.class))).willThrow(
                new RuntimeException("test"));

        ipgService.submitSinglePayment(captureRequest);
    }

    @Test
    public void testSubmitSingleRefundForCreditCard() throws IpgServiceException, IPGClientException {

        final TargetFollowOnRefundRequest refundRequest = mock(TargetFollowOnRefundRequest.class);
        final SubmitSingleRefundResponse submitSingleRefundResponse = mock(SubmitSingleRefundResponse.class);
        final Response response = mock(Response.class);
        given(submitSingleRefundResponse.getResponse()).willReturn(response);
        given(ipgClient.submitSingleRefund(any(SubmitSingleRefundRequest.class))).willReturn(
                submitSingleRefundResponse);
        given(response.getResponseCode()).willReturn("01");
        given(response.getReceipt()).willReturn("re2");
        given(response.getDeclinedCode()).willReturn("123");
        given(response.getDeclinedMessage()).willReturn("Decline Message");
        final TargetCardPaymentResult transactionResult = ipgService.submitSingleRefund(refundRequest);
        verify(ipgClient).submitSingleRefund(any(SubmitSingleRefundRequest.class));
        assertThat(response.getResponseCode()).isEqualTo(transactionResult.getResponseCode());
        assertThat(response.getDeclinedCode()).isEqualTo(transactionResult.getDeclinedCode());
        assertThat(response.getDeclinedMessage()).isEqualTo(transactionResult.getDeclinedMessage());
        assertThat(response.getReceipt()).isEqualTo(transactionResult.getReceiptNumber());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testSubmitSingleRefundForGiftCardCard() throws IpgServiceException, IPGClientException {

        final TargetFollowOnRefundRequest refundRequest = mock(TargetFollowOnRefundRequest.class);
        final SingleVoidResponse singleVoidResponse = mock(SingleVoidResponse.class);
        final Response response = mock(Response.class);
        given(refundRequest.isGiftcard()).willReturn(true);
        given(singleVoidResponse.getResponse()).willReturn(response);
        given(ipgClient.submitSingleVoid(any(SingleVoidRequest.class))).willReturn(
                singleVoidResponse);
        given(response.getResponseCode()).willReturn("01");
        given(response.getReceipt()).willReturn("re2");
        given(response.getDeclinedCode()).willReturn("123");
        given(response.getDeclinedMessage()).willReturn("Decline Message");
        final TargetCardPaymentResult transactionResult = ipgService.submitSingleRefund(refundRequest);
        verify(ipgClient).submitSingleVoid(any(SingleVoidRequest.class));
        assertThat(response.getResponseCode()).isEqualTo(transactionResult.getResponseCode());
        assertThat(response.getDeclinedCode()).isEqualTo(transactionResult.getDeclinedCode());
        assertThat(response.getDeclinedMessage()).isEqualTo(transactionResult.getDeclinedMessage());
        assertThat(response.getReceipt()).isEqualTo(transactionResult.getReceiptNumber());
    }

    @Test
    public void testSubmitSingleVoid() throws IPGClientException, IpgServiceException {
        final String receiptNumber = "13456789";
        final TargetPaymentVoidRequest voidRequest = new TargetPaymentVoidRequest();
        voidRequest.setReceiptNumber(receiptNumber);

        final SingleVoidResponse singleVoidResponse = new SingleVoidResponse();
        final Response voidResponse = new Response();
        singleVoidResponse.setResponse(voidResponse);

        final String declinedCode = StringUtils.EMPTY;
        final String declinedMessage = StringUtils.EMPTY;
        final String responseCode = "0";
        final String settlementDate = "29/10/2015";

        voidResponse.setDeclinedCode(declinedCode);
        voidResponse.setDeclinedMessage(declinedMessage);
        voidResponse.setReceipt(receiptNumber);
        voidResponse.setResponseCode(responseCode);
        voidResponse.setSettlementDate(settlementDate);

        given(ipgClient.submitSingleVoid(any(SingleVoidRequest.class))).willReturn(singleVoidResponse);
        final TargetCardPaymentResult result = ipgService.submitSingleVoid(voidRequest);

        assertThat(result).isNotNull();
        assertThat(result.getReceiptNumber()).isEqualTo(receiptNumber);
        assertThat(result.getDeclinedCode()).isEqualTo(declinedCode);
        assertThat(result.getDeclinedMessage()).isEqualTo(declinedMessage);
        assertThat(result.getResponseCode()).isEqualTo(responseCode);
        assertThat(result.getSettlementDate()).isEqualTo(settlementDate);
        assertThat(result.isPaymentSuccessful()).isTrue();
    }

    @Test
    public void testCreateSingleVoidRequest() {
        final String receiptNumber = "13456789";
        final TargetPaymentVoidRequest paymentVoidReques = new TargetPaymentVoidRequest();
        paymentVoidReques.setReceiptNumber(receiptNumber);

        final SingleVoidRequest singleVoidRequest = ipgService.createSingleVoidRequest(paymentVoidReques);

        assertThat(singleVoidRequest).isNotNull();
        assertThat(singleVoidRequest.getVoidInfo()).isNotNull();
        assertThat(singleVoidRequest.getVoidInfo().getReceiptNumber()).isEqualTo(receiptNumber);
    }

    @Test
    public void testPopulateSavedCardDetails() {
        final TargetCreateSubscriptionRequest createSubscriptionRequest = new TargetCreateSubscriptionRequest();
        final TokenizeRequest tokenizeRequest = new TokenizeRequest();
        final CreditCard creditCard = new CreditCard();
        final List<CreditCard> savedCreditCards = new ArrayList<>();

        final String bin = "34";
        final String cardExpiry = "03/17";
        final String maskedCard = "451224*****2134";
        final String cardType = "VISA";
        final Boolean isDefault = Boolean.TRUE;
        final String promoId = "1324";
        final String token = "123456987";
        final String tokenExpiry = "01/01/2016";

        creditCard.setBin(bin);
        creditCard.setCardExpiry(cardExpiry);
        creditCard.setMaskedCard(maskedCard);
        creditCard.setCardType(cardType);
        creditCard.setDefaultCard(isDefault);
        creditCard.setPromoId(promoId);
        creditCard.setToken(token);
        creditCard.setTokenExpiry(tokenExpiry);
        savedCreditCards.add(creditCard);
        createSubscriptionRequest.setSavedCreditCards(savedCreditCards);

        ipgService.populateSavedCardDetails(createSubscriptionRequest, tokenizeRequest);

        assertThat(tokenizeRequest.getSavedCards()).isNotNull();
        assertThat(tokenizeRequest.getSavedCards().getCardRequests()).isNotNull();
        assertThat(tokenizeRequest.getSavedCards().getCardRequests()).hasSize(1);
        assertThat(tokenizeRequest.getSavedCards().getCardRequests().get(0).getBin()).isEqualTo(bin);
        assertThat(tokenizeRequest.getSavedCards().getCardRequests().get(0).getCardExpiry()).isEqualTo(cardExpiry);
        assertThat(tokenizeRequest.getSavedCards().getCardRequests().get(0).getCardNumber()).isEqualTo(maskedCard);
        assertThat(tokenizeRequest.getSavedCards().getCardRequests().get(0).getCardType()).isEqualTo(cardType);
        assertThat(tokenizeRequest.getSavedCards().getCardRequests().get(0).getIsDefaultCard()).isEqualTo("1");
        assertThat(tokenizeRequest.getSavedCards().getCardRequests().get(0).getPromoId()).isEqualTo(promoId);
        assertThat(tokenizeRequest.getSavedCards().getCardRequests().get(0).getToken()).isEqualTo(token);
        assertThat(tokenizeRequest.getSavedCards().getCardRequests().get(0).getTokenExpiry()).isEqualTo(tokenExpiry);
    }

    @Test
    public void testPopulateNullSavedCardDetails() {
        final TargetCreateSubscriptionRequest createSubscriptionRequest = new TargetCreateSubscriptionRequest();
        createSubscriptionRequest.setSavedCreditCards(null);
        final TokenizeRequest tokenizeRequest = new TokenizeRequest();
        ipgService.populateSavedCardDetails(createSubscriptionRequest, tokenizeRequest);
        assertNull(tokenizeRequest.getSavedCards());
    }

    @Test
    public void testPopulateEmptySavedCardDetails() {
        final TargetCreateSubscriptionRequest createSubscriptionRequest = new TargetCreateSubscriptionRequest();
        final List<CreditCard> savedCreditCards = new ArrayList<>();
        createSubscriptionRequest.setSavedCreditCards(savedCreditCards);
        final TokenizeRequest tokenizeRequest = new TokenizeRequest();
        ipgService.populateSavedCardDetails(createSubscriptionRequest, tokenizeRequest);
        assertNull(tokenizeRequest.getSavedCards());
    }

    @Test
    public void testCreateSinglePaymentRequest() {
        given(captureRequest.getTotalAmount()).willReturn(new BigDecimal("33.8999"));
        order.setOrderId("order123");
        order.setUserId("user123");
        ipgService.setStoreNumber("5599");
        ipgService.setPosCond("48");

        final SubmitSinglePaymentRequest req = ipgService.createSinglePaymentRequest(captureRequest);

        assertThat(req).isNotNull();
        assertThat(req.getTransaction()).isNotNull();
        assertThat(req.getTransaction().getAmount()).isEqualTo("3390");
        assertThat(req.getTransaction().getCustRef()).isEqualTo("order123");
        assertThat(req.getTransaction().getCustNumber()).isEqualTo("user123");
        assertThat(req.getTransaction().getUserDefined().getStoreId()).isEqualTo("5599");
        assertThat(req.getTransaction().getUserDefined().getPosCond()).isEqualTo("48");
    }

    @Test
    public void testRequestIPGSessionFailed() throws IPGClientException, IpgServiceException {
        final SessionResponse sessionResponse = new SessionResponse();
        sessionResponse.setSecureSessionToken("");
        sessionResponse.setSessionStored(false);
        sessionResponse.setSessionStoredError("error");

        given(ipgClient.requestIPGSessionToken(any(TokenizeRequest.class))).willReturn(sessionResponse);

        try {
            ipgService.createSession(request);
            fail("Should throw exception");
        }
        catch (final IpgServiceException e) {
            assertThat(e.getMessage()).isEqualTo(
                    "Error occured when retrieving IPG session token for order:" + ORDER_ID + ", error:error");
        }
    }

    @Test
    public void testBuildQueryTransacitonRequest() throws ParseException {
        final TargetRetrieveTransactionRequest targetRetrieveTransactionRequest = createRetrieveTransactionRequest();
        final QueryTransaction transaction = ipgService.buildQueryTransactionRequest(targetRetrieveTransactionRequest);
        assertThat(transaction).isNotNull();
        assertThat(transaction.getCriteria().getCustRef()).isEqualTo("54343221");
        assertThat(transaction.getCriteria().getTrnStartTimestamp().length()).isEqualTo(20);
        assertThat(transaction.getCriteria().getTrnEndTimestamp().length()).isEqualTo(20);
        assertThat(transaction.getCriteria().getTrnStartTimestamp().substring(0, 11)).isEqualTo("2015-OCT-01");
        assertThat(transaction.getCriteria().getTrnEndTimestamp().substring(0, 11)).isEqualTo("2015-DEC-02");
        assertThat(transaction.getAddtionalData().getCoreList()).hasSize(5);
    }

    @Test
    public void testBuildQueryTransacitonRequestWithAmount() throws ParseException {
        final TargetRetrieveTransactionRequest targetRetrieveTransactionRequest = createRetrieveTransactionRequest();
        targetRetrieveTransactionRequest.setEntryAmount(BigDecimal.TEN);
        final QueryTransaction transaction = ipgService.buildQueryTransactionRequest(targetRetrieveTransactionRequest);
        assertThat(transaction.getCriteria().getAmount()).isEqualTo("1000");
    }

    @Test
    public void testQueryTransaction() throws ParseException, IPGClientException {
        final QueryResponse queryResponse = mock(QueryResponse.class);
        given(ipgClient.queryTransaction(any(QueryTransaction.class))).willReturn(queryResponse);
        assertThat(ipgService.queryTransaction(createRetrieveTransactionRequest())).isNotNull();
        verify(ipgClient).queryTransaction(any(QueryTransaction.class));
    }

    @Test
    public void testQueryTransactionWithException() throws ParseException, IPGClientException {
        given(ipgClient.queryTransaction(any(QueryTransaction.class))).willThrow(new IPGClientException("test"));
        assertNull(ipgService.queryTransaction(createRetrieveTransactionRequest()));
        verify(ipgClient).queryTransaction(any(QueryTransaction.class));
    }

    private TargetRetrieveTransactionRequest createRetrieveTransactionRequest() throws ParseException {
        final TargetRetrieveTransactionRequest targetRetrieveTransactionRequest = new TargetRetrieveTransactionRequest();
        targetRetrieveTransactionRequest.setQueryGiftCardOnly(true);
        targetRetrieveTransactionRequest.setOrderId("54343221");
        targetRetrieveTransactionRequest.setOrderDate(DateUtils.parseDate("2015-11-30 00:00:00", IPG_DATE_FORMATS));
        targetRetrieveTransactionRequest.setReceiptNumber("123");
        return targetRetrieveTransactionRequest;
    }

    @Test
    public void testLogDeclinedDetailsNotInvokedWhenSubmitSinglePaymentReturnNullDeclinedCode()
            throws IPGClientException,
            IpgServiceException {
        final TargetCardPaymentResult transactionResult = mock(TargetCardPaymentResult.class);
        final SubmitSinglePaymentResponse submitSinglePaymentResponse = mock(SubmitSinglePaymentResponse.class);
        final Response response = mock(Response.class);

        given(ipgCard.getTargetCardPaymentResult()).willReturn(transactionResult);
        given(submitSinglePaymentResponse.getResponse()).willReturn(response);
        given(ipgClient.submitSinglePayment(any(SubmitSinglePaymentRequest.class))).willReturn(
                submitSinglePaymentResponse);

        final String declinedCode = null;
        final String declinedMessage = null;

        given(response.getDeclinedCode()).willReturn(declinedCode);
        given(response.getDeclinedMessage()).willReturn(declinedMessage);

        final IpgServiceImpl spy = spy(ipgService);
        spy.submitSinglePayment(captureRequest);

        verify(response).getDeclinedCode();
        verify(response).getDeclinedMessage();
        verify(spy, times(0)).logDeclinedDetails(declinedCode, declinedMessage);
    }

    @Test
    public void testLogDeclinedDetailsNotInvokedWhenSubmitSinglePaymentReturnEmptyDeclinedCode()
            throws IPGClientException,
            IpgServiceException {
        final TargetCardPaymentResult transactionResult = mock(TargetCardPaymentResult.class);
        final SubmitSinglePaymentResponse submitSinglePaymentResponse = mock(SubmitSinglePaymentResponse.class);
        final Response response = mock(Response.class);

        given(ipgCard.getTargetCardPaymentResult()).willReturn(transactionResult);
        given(submitSinglePaymentResponse.getResponse()).willReturn(response);
        given(ipgClient.submitSinglePayment(any(SubmitSinglePaymentRequest.class))).willReturn(
                submitSinglePaymentResponse);

        final String declinedCode = StringUtils.EMPTY;
        final String declinedMessage = StringUtils.EMPTY;

        given(response.getDeclinedCode()).willReturn(declinedCode);
        given(response.getDeclinedMessage()).willReturn(declinedMessage);

        final IpgServiceImpl spy = spy(ipgService);
        spy.submitSinglePayment(captureRequest);

        verify(response).getDeclinedCode();
        verify(response).getDeclinedMessage();
        verify(spy, times(0)).logDeclinedDetails(declinedCode, declinedMessage);
    }

    @Test
    public void testLogDeclinedDetailsInvokedWhenSubmitSinglePaymentReturnDeclinedCode() throws IPGClientException,
            IpgServiceException {
        final TargetCardPaymentResult transactionResult = mock(TargetCardPaymentResult.class);
        final SubmitSinglePaymentResponse submitSinglePaymentResponse = mock(SubmitSinglePaymentResponse.class);
        final Response response = mock(Response.class);

        given(ipgCard.getTargetCardPaymentResult()).willReturn(transactionResult);
        given(submitSinglePaymentResponse.getResponse()).willReturn(response);
        given(ipgClient.submitSinglePayment(any(SubmitSinglePaymentRequest.class))).willReturn(
                submitSinglePaymentResponse);

        final String declinedCode = "200";
        final String declinedMessage = "Interface error";

        given(response.getDeclinedCode()).willReturn(declinedCode);
        given(response.getDeclinedMessage()).willReturn(declinedMessage);

        final IpgServiceImpl spy = spy(ipgService);
        spy.submitSinglePayment(captureRequest);

        verify(response).getDeclinedCode();
        verify(response).getDeclinedMessage();
        verify(spy).logDeclinedDetails(declinedCode, declinedMessage);
    }

    @Test
    public void testLogDeclinedDetailsNotInvokedWhenSubmitSingleRefundReturnsNullDeclinedCode()
            throws IpgServiceException,
            IPGClientException {

        final TargetFollowOnRefundRequest refundRequest = mock(TargetFollowOnRefundRequest.class);
        final SubmitSingleRefundResponse submitSingleRefundResponse = mock(SubmitSingleRefundResponse.class);
        final Response response = mock(Response.class);
        given(submitSingleRefundResponse.getResponse()).willReturn(response);
        given(ipgClient.submitSingleRefund(any(SubmitSingleRefundRequest.class))).willReturn(
                submitSingleRefundResponse);

        final String declinedCode = null;
        final String declinedMessage = null;

        given(response.getDeclinedCode()).willReturn(declinedCode);
        given(response.getDeclinedMessage()).willReturn(declinedMessage);

        final IpgServiceImpl spy = spy(ipgService);
        spy.submitSingleRefund(refundRequest);

        verify(response).getDeclinedCode();
        verify(response).getDeclinedMessage();
        verify(spy, times(0)).logDeclinedDetails(declinedCode, declinedMessage);
    }

    @Test
    public void testLogDeclinedDetailsNotInvokedWhenSubmitSingleRefundReturnsEmptyDeclinedCode()
            throws IpgServiceException,
            IPGClientException {

        final TargetFollowOnRefundRequest refundRequest = mock(TargetFollowOnRefundRequest.class);
        final SubmitSingleRefundResponse submitSingleRefundResponse = mock(SubmitSingleRefundResponse.class);
        final Response response = mock(Response.class);
        given(submitSingleRefundResponse.getResponse()).willReturn(response);
        given(ipgClient.submitSingleRefund(any(SubmitSingleRefundRequest.class))).willReturn(
                submitSingleRefundResponse);

        final String declinedCode = StringUtils.EMPTY;
        final String declinedMessage = StringUtils.EMPTY;

        given(response.getDeclinedCode()).willReturn(declinedCode);
        given(response.getDeclinedMessage()).willReturn(declinedMessage);

        final IpgServiceImpl spy = spy(ipgService);
        spy.submitSingleRefund(refundRequest);

        verify(response).getDeclinedCode();
        verify(response).getDeclinedMessage();
        verify(spy, times(0)).logDeclinedDetails(declinedCode, declinedMessage);
    }

    @Test
    public void testLogDeclinedDetailsInvokedWhenSubmitSingleRefundReturnsDeclinedCode()
            throws IpgServiceException,
            IPGClientException {

        final TargetFollowOnRefundRequest refundRequest = mock(TargetFollowOnRefundRequest.class);
        final SubmitSingleRefundResponse submitSingleRefundResponse = mock(SubmitSingleRefundResponse.class);
        final Response response = mock(Response.class);
        given(submitSingleRefundResponse.getResponse()).willReturn(response);
        given(ipgClient.submitSingleRefund(any(SubmitSingleRefundRequest.class))).willReturn(
                submitSingleRefundResponse);

        final String declinedCode = "200";
        final String declinedMessage = "Interface error";

        given(response.getDeclinedCode()).willReturn(declinedCode);
        given(response.getDeclinedMessage()).willReturn(declinedMessage);

        final IpgServiceImpl spy = spy(ipgService);
        spy.submitSingleRefund(refundRequest);

        verify(response).getDeclinedCode();
        verify(response).getDeclinedMessage();
        verify(spy).logDeclinedDetails(declinedCode, declinedMessage);
    }

    @Test
    public void testLogDeclinedDetailsNotInvokedWhenSubmitSingleVoidReturnsNullDeclinedCode()
            throws IPGClientException, IpgServiceException {
        final String receiptNumber = "13456789";
        final TargetPaymentVoidRequest voidRequest = new TargetPaymentVoidRequest();
        voidRequest.setReceiptNumber(receiptNumber);

        final SingleVoidResponse singleVoidResponse = new SingleVoidResponse();
        final Response response = mock(Response.class);
        singleVoidResponse.setResponse(response);

        final String declinedCode = null;
        final String declinedMessage = null;

        response.setDeclinedCode(declinedCode);
        given(response.getDeclinedCode()).willReturn(declinedCode);
        given(response.getDeclinedMessage()).willReturn(declinedMessage);
        given(response.getResponseCode()).willReturn("0");
        given(ipgClient.submitSingleVoid(any(SingleVoidRequest.class))).willReturn(singleVoidResponse);

        final IpgServiceImpl spy = spy(ipgService);
        spy.submitSingleVoid(voidRequest);

        verify(response).getDeclinedCode();
        verify(response).getDeclinedMessage();
        verify(spy, times(0)).logDeclinedDetails(declinedCode, declinedMessage);
    }

    @Test
    public void testLogDeclinedDetailsNotInvokedWhenSubmitSingleVoidReturnsEmptyDeclinedCode()
            throws IPGClientException, IpgServiceException {
        final String receiptNumber = "13456789";
        final TargetPaymentVoidRequest voidRequest = new TargetPaymentVoidRequest();
        voidRequest.setReceiptNumber(receiptNumber);

        final SingleVoidResponse singleVoidResponse = new SingleVoidResponse();
        final Response response = mock(Response.class);
        singleVoidResponse.setResponse(response);

        final String declinedCode = StringUtils.EMPTY;
        final String declinedMessage = StringUtils.EMPTY;

        response.setDeclinedCode(declinedCode);
        given(response.getDeclinedCode()).willReturn(declinedCode);
        given(response.getDeclinedMessage()).willReturn(declinedMessage);
        given(response.getResponseCode()).willReturn("0");
        given(ipgClient.submitSingleVoid(any(SingleVoidRequest.class))).willReturn(singleVoidResponse);

        final IpgServiceImpl spy = spy(ipgService);
        spy.submitSingleVoid(voidRequest);

        verify(response).getDeclinedCode();
        verify(response).getDeclinedMessage();
        verify(spy, times(0)).logDeclinedDetails(declinedCode, declinedMessage);
    }

    @Test
    public void testLogDeclinedDetailsInvokedWhenSubmitSingleVoidReturnsDeclinedCode()
            throws IPGClientException, IpgServiceException {
        final String receiptNumber = "13456789";
        final TargetPaymentVoidRequest voidRequest = new TargetPaymentVoidRequest();
        voidRequest.setReceiptNumber(receiptNumber);

        final SingleVoidResponse singleVoidResponse = new SingleVoidResponse();
        final Response response = mock(Response.class);
        singleVoidResponse.setResponse(response);

        final String declinedCode = "51";
        final String declinedMessage = "Insufficient funds";

        response.setDeclinedCode(declinedCode);
        given(response.getDeclinedCode()).willReturn(declinedCode);
        given(response.getDeclinedMessage()).willReturn(declinedMessage);
        given(response.getResponseCode()).willReturn("0");
        given(ipgClient.submitSingleVoid(any(SingleVoidRequest.class))).willReturn(singleVoidResponse);

        final IpgServiceImpl spy = spy(ipgService);
        spy.submitSingleVoid(voidRequest);

        verify(response).getDeclinedCode();
        verify(response).getDeclinedMessage();
        verify(spy).logDeclinedDetails(declinedCode, declinedMessage);
    }
}