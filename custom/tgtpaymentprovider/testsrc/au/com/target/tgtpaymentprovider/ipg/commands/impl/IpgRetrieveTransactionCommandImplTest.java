package au.com.target.tgtpaymentprovider.ipg.commands.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.dto.TransactionStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtpayment.commands.TargetRetrieveTransactionCommand;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest.TrnType;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;
import au.com.target.tgtpaymentprovider.ipg.data.response.TransactionsResultResponse;
import au.com.target.tgtpaymentprovider.ipg.data.soap.QueryResponse;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Response;
import au.com.target.tgtpaymentprovider.ipg.exception.IpgServiceException;
import au.com.target.tgtpaymentprovider.ipg.service.IpgService;
import au.com.target.tgtpaymentprovider.ipg.util.IpgPaymentStatus;

import com.google.common.collect.ImmutableList;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class IpgRetrieveTransactionCommandImplTest {
    @Mock
    private IpgService ipgService;

    @Mock
    private TargetOrderService targetOrderService;

    @InjectMocks
    private final TargetRetrieveTransactionCommand targetRetrieveTransactionCommand = new IpgRetrieveTransactionCommandImpl();

    private TargetRetrieveTransactionRequest request;

    @Mock
    private QueryResponse queryResponse;

    @Before
    public void setup() {
        request = new TargetRetrieveTransactionRequest();
        when(ipgService.queryTransaction(request)).thenReturn(queryResponse);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPerformWhenRequestIsNull() throws IpgServiceException {
        targetRetrieveTransactionCommand.perform(null);
    }

    @Test
    public void testPerformSuccess() throws IpgServiceException {
        final TransactionsResultResponse transactionsResultResponse = new TransactionsResultResponse();
        request.setSessionId("123");
        request.setSessionToken("123");
        given(ipgService.queryTransactionResults(any(String.class), any(String.class))).willReturn(
                transactionsResultResponse);
        final TargetRetrieveTransactionResult result = targetRetrieveTransactionCommand.perform(request);
        assertNotNull(result);
    }

    @Test
    public void testGetFailedGiftCardAttempts() {
        request.setOrderId("orderId");
        request.setOrderDate(new Date());
        request.setQueryGiftCardOnly(true);
        request.setTrnType(TrnType.FAILED);
        targetRetrieveTransactionCommand.perform(request);
        verify(ipgService).queryTransaction(request);
    }

    @Test
    public void testMapFailedGiftCardAttemptsWithEmptyResponse() {
        request.setOrderId("orderId");
        request.setOrderDate(new Date());
        request.setQueryGiftCardOnly(true);
        request.setTrnType(TrnType.FAILED);
        when(ipgService.queryTransaction(request)).thenReturn(queryResponse);
        final TargetRetrieveTransactionResult result = targetRetrieveTransactionCommand.perform(request);
        verify(ipgService).queryTransaction(request);
        assertNotNull(result);
        assertNull(result.getPayments());
    }

    @Test
    public void testMapSuccessfulCCAttemptsWithEmptyResponse() {
        request.setOrderId("orderId");
        request.setOrderDate(new Date());
        request.setQueryGiftCardOnly(false);
        request.setTrnType(TrnType.APPROVED);
        when(ipgService.queryTransaction(request)).thenReturn(null);
        final TargetRetrieveTransactionResult result = targetRetrieveTransactionCommand.perform(request);
        verify(ipgService).queryTransaction(request);
        assertNotNull(result);
        assertNull(result.getPayments());
    }

    @Test
    public void testMapFailedGiftCardAttempt() {
        when(queryResponse.getResponse())
                .thenReturn(ImmutableList.of(createResponse("1000", "receipt1", "47", "1"),
                        createResponse("1511", "receipt2", "47", "2"),
                        createResponse("1600", "receipt3", "47", "3"),
                        createResponse("2541", "receipt4", "40", "2")));
        request.setOrderId("orderId");
        request.setOrderDate(new Date());
        request.setQueryGiftCardOnly(true);
        request.setTrnType(TrnType.FAILED);
        when(ipgService.queryTransaction(request)).thenReturn(queryResponse);
        final TargetRetrieveTransactionResult result = targetRetrieveTransactionCommand.perform(request);
        verify(ipgService).queryTransaction(request);
        assertNotNull(result);
        assertNotNull(result.getPayments());
        assertEquals(1, result.getPayments().size());
        assertEquals(new BigDecimal("15.11"), result.getPayments().get(0).getAmount());
    }

    @Test
    public void testMapApprovedGiftCardAttempt() {
        when(queryResponse.getResponse())
                .thenReturn(ImmutableList.of(createResponse("1000", "receipt1", "47", "1"),
                        createResponse("1511", "receipt2", "47", "2"),
                        createResponse("1600", "receipt3", "40", "1"),
                        createResponse("2541", "receipt4", "47", "1")));
        request.setOrderId("orderId");
        request.setOrderDate(new Date());
        request.setQueryGiftCardOnly(true);
        request.setTrnType(TrnType.APPROVED);
        when(ipgService.queryTransaction(request)).thenReturn(queryResponse);
        final TargetRetrieveTransactionResult result = targetRetrieveTransactionCommand.perform(request);
        verify(ipgService).queryTransaction(request);
        assertNotNull(result);
        assertNotNull(result.getPayments());
        assertEquals(2, result.getPayments().size());
        assertEquals(new BigDecimal("10.00"), result.getPayments().get(0).getAmount());
    }

    @Test
    public void testMapAllGiftCardAttempt() {
        when(queryResponse.getResponse())
                .thenReturn(ImmutableList.of(createResponse("1000", "receipt1", "47", "1"),
                        createResponse("1511", "receipt2", "47", "2"),
                        createResponse("1600", "receipt3", "40", "1"),
                        createResponse("2541", "receipt4", "47", "3")));
        request.setOrderId("orderId");
        request.setOrderDate(new Date());
        request.setQueryGiftCardOnly(true);
        request.setTrnType(TrnType.ALL);
        when(ipgService.queryTransaction(request)).thenReturn(queryResponse);
        final TargetRetrieveTransactionResult result = targetRetrieveTransactionCommand.perform(request);
        verify(ipgService).queryTransaction(request);
        assertNotNull(result);
        assertNotNull(result.getPayments());
        assertEquals(3, result.getPayments().size());
        assertEquals(new BigDecimal("10.00"), result.getPayments().get(0).getAmount());
    }

    @Test
    public void testMapGiftCardAttempt() {
        when(queryResponse.getResponse())
                .thenReturn(ImmutableList.of(createResponse("1000", "receipt1", "47", "1"),
                        createResponse("1511", "receipt2", "47", "2"),
                        createResponse("1600", "receipt3", "40", "1"),
                        createResponse("2541", "receipt4", "47", "3")));
        request.setOrderId("orderId");
        request.setOrderDate(new Date());
        request.setQueryGiftCardOnly(true);
        when(ipgService.queryTransaction(request)).thenReturn(queryResponse);
        final TargetRetrieveTransactionResult result = targetRetrieveTransactionCommand.perform(request);
        verify(ipgService).queryTransaction(request);
        assertNotNull(result);
        assertNotNull(result.getPayments());
        assertEquals(3, result.getPayments().size());
        assertEquals(new BigDecimal("10.00"), result.getPayments().get(0).getAmount());
    }

    @Test
    public void testQueryGiftCardNoResult() {
        request.setOrderId("orderId");
        request.setOrderDate(new Date());
        request.setQueryGiftCardOnly(true);
        when(ipgService.queryTransaction(request)).thenReturn(null);
        final TargetRetrieveTransactionResult result = targetRetrieveTransactionCommand.perform(request);
        verify(ipgService).queryTransaction(request);
        assertNotNull(result);
        assertNull(result.getPayments());
    }

    @Test
    public void testQueryGiftCardEmptyResult() {
        when(queryResponse.getResponse())
                .thenReturn(new ArrayList<Response>());
        request.setOrderId("orderId");
        request.setOrderDate(new Date());
        request.setQueryGiftCardOnly(true);
        when(ipgService.queryTransaction(request)).thenReturn(queryResponse);
        final TargetRetrieveTransactionResult result = targetRetrieveTransactionCommand.perform(request);
        verify(ipgService).queryTransaction(request);
        assertNotNull(result);
        assertNull(result.getPayments());
    }

    @Test
    public void testMapMultipleApprovedCCAttempt() {
        when(queryResponse.getResponse())
                .thenReturn(ImmutableList.of(createResponse("1000", "receipt1", "50", "1"),
                        createResponse("1000", "receipt2", "50", "2"),
                        createResponse("1000", "receipt3", "50", "1"),
                        createResponse("1000", "receipt4", "50", "1")));
        request.setOrderId("orderId");
        request.setOrderDate(new Date());
        request.setQueryGiftCardOnly(false);
        request.setTrnType(TrnType.APPROVED);
        when(ipgService.queryTransaction(request)).thenReturn(queryResponse);
        final TargetRetrieveTransactionResult result = targetRetrieveTransactionCommand.perform(request);
        verify(ipgService).queryTransaction(request);
        assertNotNull(result);
        assertNotNull(result.getPayments());
        assertEquals(3, result.getPayments().size());
        assertEquals(new BigDecimal("10.00"), result.getPayments().get(0).getAmount());
        assertNull(result.getCapturedAmount());
    }

    @Test
    public void testMapFailedCCAttempt() {
        when(queryResponse.getResponse())
                .thenReturn(ImmutableList.of(createResponse("1000", "receipt1", "50", "1"),
                        createResponse("1000", "receipt2", "50", "2"),
                        createResponse("1000", "receipt3", "50", "1"),
                        createResponse("1000", "receipt4", "50", "1")));
        request.setOrderId("orderId");
        request.setOrderDate(new Date());
        request.setQueryGiftCardOnly(false);
        request.setTrnType(TrnType.FAILED);
        when(ipgService.queryTransaction(request)).thenReturn(queryResponse);
        final TargetRetrieveTransactionResult result = targetRetrieveTransactionCommand.perform(request);
        verify(ipgService).queryTransaction(request);
        assertNotNull(result);
        assertNotNull(result.getPayments());
        assertEquals(1, result.getPayments().size());
        assertEquals(new BigDecimal("10.00"), result.getPayments().get(0).getAmount());
        assertNotNull(result.getDeclineCode());
    }

    @Test
    public void testMapSuccessfulCCAttempt() {
        when(queryResponse.getResponse())
                .thenReturn(ImmutableList.of(createResponse("1000", "receipt1", "50", "1"),
                        createResponse("1000", "receipt2", "50", "2")));
        request.setOrderId("orderId");
        request.setOrderDate(new Date());
        request.setQueryGiftCardOnly(false);
        request.setTrnType(TrnType.APPROVED);
        when(ipgService.queryTransaction(request)).thenReturn(queryResponse);
        final TargetRetrieveTransactionResult result = targetRetrieveTransactionCommand.perform(request);
        verify(ipgService).queryTransaction(request);
        assertNotNull(result);
        assertNotNull(result.getPayments());
        assertEquals(1, result.getPayments().size());
        assertEquals(new BigDecimal("10.00"), result.getPayments().get(0).getAmount());
        assertNull(result.getDeclineCode());
        assertEquals("receipt1", result.getReconciliationId());
        assertEquals(TransactionStatus.ACCEPTED, result.getTransactionStatus());
    }

    /**
     * 
     * @param amount
     * @param receipt
     * @param trnTypeId
     * @param trnStatusId
     * @return response
     */
    private Response createResponse(final String amount, final String receipt, final String trnTypeId,
            final String trnStatusId) {
        final Response response = new Response();
        response.setAmount(amount);
        response.setReceipt(receipt);
        response.setTrnStatusId(trnStatusId);
        response.setTrnTypeId(trnTypeId);
        if (!IpgPaymentStatus.APPROVED.getCode().equals(trnStatusId)) {
            response.setDeclinedCode("declinedCode");
            response.setDeclinedMessage("declinedMsg");
        }
        return response;
    }
}