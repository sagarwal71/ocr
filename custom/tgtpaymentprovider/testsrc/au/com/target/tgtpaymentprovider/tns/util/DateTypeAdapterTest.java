/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;


/**
 * @author vmuthura
 * 
 */
@Ignore
public class DateTypeAdapterTest
{
    private final DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");

    @Before
    public void setUp()
    {
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @Test
    public void testMarshalDate() throws Exception
    {
        final DateTypeAdapter adapter = new DateTypeAdapter();
        final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

        assertEquals("Date format", format.format(calendar.getTime()), adapter.marshal(calendar.getTime()));
    }

    @Test
    public void testUnmarshalString() throws Exception
    {
        final DateTypeAdapter adapter = new DateTypeAdapter();
        final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

        assertNull(adapter.unmarshal(null));
        assertEquals("Date", calendar.getTime(), adapter.unmarshal(format.format(calendar.getTime())));
    }

}
