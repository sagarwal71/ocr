/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data.response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.IOException;
import java.util.Properties;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.target.tgtpaymentprovider.tns.data.Order;
import au.com.target.tgtpaymentprovider.tns.data.ResponseDetails;
import au.com.target.tgtpaymentprovider.tns.data.ResultEnum;
import au.com.target.tgtpaymentprovider.tns.data.ShippingDetails;
import au.com.target.tgtpaymentprovider.tns.data.SourceOfFunds;
import au.com.target.tgtpaymentprovider.tns.data.Transaction;


/**
 * @author bjames4
 * 
 */
@UnitTest
public class GenericTransactionResponseTest {

    /**
     * 
     */
    @Test
    public void toJsonTest() {

        final GenericTransactionResponse genericResponse = new GenericTransactionResponse();
        genericResponse.setMerchant("testtarget2");
        genericResponse.setResult(ResultEnum.SUCCESS);
        genericResponse.setSession("GERGE23423SRGEGRerglkn3948985989");
        genericResponse.setMerchant("testtarget2");
        genericResponse.setOrder(new Order());
        genericResponse.setResponse(new ResponseDetails());
        genericResponse.setShipping(new ShippingDetails());
        genericResponse.setSourceOfFunds(new SourceOfFunds());
        genericResponse.setTransaction(new Transaction());

        final ObjectMapper objectMapper = new ObjectMapper();
        final Properties jsonStringProps = new Properties();

        try {
            jsonStringProps.load(GenericTransactionResponseTest.class.getResourceAsStream("/ExpectedJSON.properties"));
            final String jsonString = jsonStringProps.getProperty("genericTransactionResponse");
            assertNotNull("Expecting non null JSON string", objectMapper.writeValueAsString(genericResponse));
            assertEquals("Unexpected JSON string", jsonString.replaceAll("\\s*", ""),
                    objectMapper.writeValueAsString(genericResponse));

        }
        catch (final JsonGenerationException e) {
            fail("Unexpected exception:" + e.getMessage());
        }
        catch (final JsonMappingException e) {
            fail("Unexpected exception:" + e.getMessage());
        }
        catch (final IOException e) {
            fail("Unexpected exception:" + e.getMessage());
        }
    }
}
