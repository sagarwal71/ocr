/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.commands.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.math.BigDecimal;
import java.util.Currency;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpaymentprovider.tns.TnsService;
import au.com.target.tgtpaymentprovider.tns.TnsWebOperations;
import au.com.target.tgtpaymentprovider.tns.data.GatewayResponseEnum;
import au.com.target.tgtpaymentprovider.tns.data.ResponseDetails;
import au.com.target.tgtpaymentprovider.tns.data.ResultEnum;
import au.com.target.tgtpaymentprovider.tns.data.Transaction;
import au.com.target.tgtpaymentprovider.tns.data.response.PayResponse;
import au.com.target.tgtpaymentprovider.tns.exception.TnsServiceException;


/**
 * @author bjames4
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TnsCaptureCommandImplTest {

    @Mock
    private TnsService tnsService;

    @Mock
    private TnsWebOperations tnsClient;

    @InjectMocks
    private final TnsCaptureCommandImpl tnsCaptureCommandImpl = new TnsCaptureCommandImpl();

    private final TargetCaptureRequest captureRequest = new TargetCaptureRequest();

    private String transactionId;
    private String reconcillationId;

    /**
     * @throws TnsServiceException
     *             -
     */
    @Before
    public void init() throws TnsServiceException {
        transactionId = "123456";
        reconcillationId = "123";
        final PayResponse payResponse = new PayResponse();
        payResponse.setResult(ResultEnum.SUCCESS);
        final ResponseDetails responseDetails = new ResponseDetails();
        responseDetails.setGatewayCode(GatewayResponseEnum.APPROVED);
        final Transaction transaction = new Transaction();
        transaction.setId(transactionId);
        transaction.setReceipt(reconcillationId);
        transaction.setAmount("10.00");
        payResponse.setTransaction(transaction);
        payResponse.setResponse(responseDetails);

        when(tnsService.pay(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
                (BigDecimal)Mockito.anyObject(), (Currency)Mockito.anyObject())).thenReturn(payResponse);
    }

    /**
     * 
     */
    @Test
    public void testPerform() {
        captureRequest.setTotalAmount(BigDecimal.valueOf(10.00));
        captureRequest.setCurrency(Currency.getInstance("AUD"));
        captureRequest.setOrderId("1");
        captureRequest.setTransactionId("12");
        captureRequest.setPayerId("123");
        captureRequest.setToken("1234");
        final TargetCaptureResult response = tnsCaptureCommandImpl.perform(captureRequest);
        assertEquals("Expecting response", TransactionStatus.ACCEPTED, response.getTransactionStatus());
        assertEquals("Expecting response", TransactionStatusDetails.SUCCESFULL,
                response.getTransactionStatusDetails());
        assertEquals("Expecting MerchantTransactionCode", "12", response.getMerchantTransactionCode());
        assertEquals("Expecting ReconciliationId", "123", response.getReconciliationId());
        assertEquals("Expecting RequestId", "123456", response.getRequestId());
        assertEquals("Expecting RequestToken", "1234", response.getRequestToken());
        assertEquals("Expecting TotalAmount", "10.00", response.getTotalAmount().toString());
    }

}
