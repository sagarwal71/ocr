/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data.request;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.IOException;
import java.util.Properties;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.target.tgtpaymentprovider.tns.data.Transaction;


/**
 * @author bjames4
 * 
 */
@UnitTest
public class RefundRequestTest {

    /**
     * 
     */
    @Test
    public void toJsonTest() {
        final RefundRequest refundRequest = new RefundRequest();
        refundRequest.setApiOperation("REFUND");
        refundRequest.setTransaction(new Transaction());
        refundRequest.getTransaction().setAmount("19.99");
        refundRequest.getTransaction().setCurrency("AUD");
        refundRequest.getTransaction().setId("123459875877");
        refundRequest.getTransaction().setReference("DFGDF087097dsfgdf122");
        refundRequest.getTransaction().setTargetTransactionId("targetTransactionId");

        final ObjectMapper objectMapper = new ObjectMapper();
        final Properties jsonStringProps = new Properties();

        try {
            jsonStringProps.load(RefundRequestTest.class.getResourceAsStream("/ExpectedJSON.properties"));
            final String jsonString = jsonStringProps.getProperty("refundRequest");
            assertNotNull("Expecting non null JSON string", objectMapper.writeValueAsString(refundRequest));
            assertEquals("Unexpected JSON string", jsonString.replaceAll("\\s*", ""),
                    objectMapper.writeValueAsString(refundRequest));
        }
        catch (final JsonGenerationException e) {
            fail("Unexpected exception:" + e.getMessage());
        }
        catch (final JsonMappingException e) {
            fail("Unexpected exception:" + e.getMessage());
        }
        catch (final IOException e) {
            fail("Unexpected exception:" + e.getMessage());
        }
    }
}
