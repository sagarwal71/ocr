/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.UUID;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import au.com.target.tgtpaymentprovider.tns.data.Card;
import au.com.target.tgtpaymentprovider.tns.data.CardExpiry;
import au.com.target.tgtpaymentprovider.tns.data.PaymentType;
import au.com.target.tgtpaymentprovider.tns.data.ResultEnum;
import au.com.target.tgtpaymentprovider.tns.data.SourceOfFundDetails;
import au.com.target.tgtpaymentprovider.tns.data.SourceOfFunds;
import au.com.target.tgtpaymentprovider.tns.data.Transaction;
import au.com.target.tgtpaymentprovider.tns.data.request.PayRequest;
import au.com.target.tgtpaymentprovider.tns.data.request.RefundRequest;
import au.com.target.tgtpaymentprovider.tns.data.request.SaveCardRequest;
import au.com.target.tgtpaymentprovider.tns.data.response.CreateSessionResponse;
import au.com.target.tgtpaymentprovider.tns.data.response.GenericTransactionResponse;
import au.com.target.tgtpaymentprovider.tns.exception.InvalidRequestException;
import au.com.target.tgtpaymentprovider.tns.exception.RequestRejectedException;
import au.com.target.tgtpaymentprovider.tns.exception.TnsServerException;




/**
 * @author vmuthura
 * 
 */
@Ignore
//Ignored since this is more of integration test, than a unit test
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:tgtpaymentprovider-spring-test.xml")
public class TnsClientTest
{

    private static final String URL = "https://secure.ap.tnspayments.com/api/rest/version/11/merchant/TESTTARGET2/";

    private static String orderId;
    private static String transactionId;
    private static String session;

    @Autowired
    private TnsWebOperations tnsClient;

    /**
     * 
     */
    @BeforeClass
    public static void setUpData()
    {
        orderId = UUID.randomUUID().toString().intern();
        transactionId = orderId;
    }

    /**
     * @throws RequestRejectedException
     *             - for illegal request
     * @throws InvalidRequestException
     *             - for bad request
     * @throws TnsServerException
     *             - for connection related
     */
    @Test(expected = InvalidRequestException.class)
    public void testCreateSessionWithInvalidEndpoint() throws RequestRejectedException, InvalidRequestException,
            TnsServerException
    {
        tnsClient.post(URL + "/SESSION", null, CreateSessionResponse.class);
    }


    /**
     * @throws RequestRejectedException
     *             - for illegal request
     * @throws InvalidRequestException
     *             - for bad request
     * @throws TnsServerException
     *             - for connection related
     */
    @Test
    public void testTnsOperations() throws RequestRejectedException, InvalidRequestException, TnsServerException
    {
        // Create session
        final CreateSessionResponse response = tnsClient.post(URL + "/session", null, CreateSessionResponse.class);

        assertEquals("Expecting result ", ResultEnum.SUCCESS, response.getResult());
        assertEquals("Expecting merchant ", "TESTTARGET2", response.getMerchant());
        assertNotNull("Expecting session ", response.getSession());
        session = response.getSession();

        // Pay
        String url = URL + "/order/" + orderId + "/transaction/" + transactionId;
        final PayRequest payRequest = new PayRequest();
        payRequest.setApiOperation("PAY");
        SourceOfFunds sourceOfFunds = new SourceOfFunds();
        Transaction transaction = new Transaction();

        transaction.setAmount("31.00");
        transaction.setCurrency("AUD");
        sourceOfFunds.setToken("9414214093908489");
        sourceOfFunds.setType(PaymentType.CARD);
        payRequest.setSourceOfFunds(sourceOfFunds);
        payRequest.setTransaction(transaction);

        final GenericTransactionResponse payResponse = tnsClient.put(url, payRequest, GenericTransactionResponse.class);
        assertEquals("Expecting result ", ResultEnum.SUCCESS, payResponse.getResult());
        assertEquals("Expecting merchant ", "TESTTARGET2", payResponse.getMerchant());
        assertEquals("Expecting amount ", "31.00", payResponse.getTransaction().getAmount());

        // Retrieve
        url = URL + "/order/" + orderId + "/transaction/" + transactionId;
        GenericTransactionResponse refundResponse = tnsClient.get(url, GenericTransactionResponse.class);

        assertEquals("Expecting result ", ResultEnum.SUCCESS, refundResponse.getResult());
        assertEquals("Expecting merchant ", "TESTTARGET2", refundResponse.getMerchant());
        assertEquals("Expecting amount ", "31.00", refundResponse.getTransaction().getAmount());

        // Refund
        url = URL + "/order/" + orderId + "/transaction/" + transactionId + 'R';
        final RefundRequest refundRequest = new RefundRequest();
        refundRequest.setApiOperation("REFUND");
        sourceOfFunds = new SourceOfFunds();
        transaction = new Transaction();

        transaction.setAmount("31.00");
        transaction.setCurrency("AUD");
        sourceOfFunds.setToken("9414214093908489");
        sourceOfFunds.setType(PaymentType.CARD);
        refundRequest.setSourceOfFunds(sourceOfFunds);
        refundRequest.setTransaction(transaction);

        refundResponse = tnsClient.put(url, refundRequest, GenericTransactionResponse.class);

        assertEquals("Expecting result ", ResultEnum.SUCCESS, refundResponse.getResult());
        assertEquals("Expecting merchant ", "TESTTARGET2", refundResponse.getMerchant());
        assertEquals("Expecting amount ", "31.00", refundResponse.getTransaction().getAmount());

        // Retrieve
        url = URL + "/order/" + orderId + "/transaction/" + transactionId + 'R';
        refundResponse = tnsClient.get(url, GenericTransactionResponse.class);

        assertEquals("Expecting result ", ResultEnum.SUCCESS, refundResponse.getResult());
        assertEquals("Expecting merchant ", "TESTTARGET2", refundResponse.getMerchant());
        assertEquals("Expecting amount ", "31.00", refundResponse.getTransaction().getAmount());

    }

    /**
     * @throws RequestRejectedException
     *             - for illegal request
     * @throws InvalidRequestException
     *             - for bad request
     * @throws TnsServerException
     *             - for connection related
     */
    @Test(expected = InvalidRequestException.class)
    // Form session details not provided by cardholder
    public void testSaveCard() throws RequestRejectedException, InvalidRequestException, TnsServerException
    {
        final String url = URL + "/token/9410824219733465";
        final SaveCardRequest saveCardRequest = new SaveCardRequest();
        final SourceOfFunds sourceOfFunds = new SourceOfFunds();
        final CardExpiry cardExpiry = new CardExpiry();
        cardExpiry.setMonth("05");
        cardExpiry.setYear("17");
        final Card card = new Card();
        card.setNumber("5123456789012346");
        card.setSecurityCode("111");
        card.setExpiry(cardExpiry);
        sourceOfFunds.setProvided(new SourceOfFundDetails());
        sourceOfFunds.getProvided().setCard(card);
        sourceOfFunds.setSession(session);
        sourceOfFunds.setType(PaymentType.CARD);
        saveCardRequest.setSourceOfFunds(sourceOfFunds);

        tnsClient.put(url, saveCardRequest, GenericTransactionResponse.class);
    }

    /**
     * @throws RequestRejectedException
     *             - for illegal request
     * @throws InvalidRequestException
     *             - for bad request
     * @throws TnsServerException
     *             - for connection related
     */
    @Test(expected = InvalidRequestException.class)
    public void testDeleteToken() throws RequestRejectedException, InvalidRequestException, TnsServerException
    {
        final GenericTransactionResponse response = tnsClient.delete(URL + "token/1234", null,
                GenericTransactionResponse.class);
        assertEquals("Expecting result ", ResultEnum.ERROR, response.getResult());
    }
}
