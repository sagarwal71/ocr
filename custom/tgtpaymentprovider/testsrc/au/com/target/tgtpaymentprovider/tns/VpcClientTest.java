/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import au.com.target.tgtpaymentprovider.tns.data.PaymentType;
import au.com.target.tgtpaymentprovider.tns.data.ResultEnum;
import au.com.target.tgtpaymentprovider.tns.data.SourceOfFunds;
import au.com.target.tgtpaymentprovider.tns.data.Transaction;
import au.com.target.tgtpaymentprovider.tns.data.request.PayRequest;
import au.com.target.tgtpaymentprovider.tns.data.request.VpcRefundRequest;
import au.com.target.tgtpaymentprovider.tns.data.response.GenericTransactionResponse;
import au.com.target.tgtpaymentprovider.tns.data.response.VpcRefundResponse;
import au.com.target.tgtpaymentprovider.tns.exception.InvalidRequestException;
import au.com.target.tgtpaymentprovider.tns.exception.RequestRejectedException;
import au.com.target.tgtpaymentprovider.tns.exception.TnsServerException;


/**
 * @author vmuthura
 * 
 */
@Ignore
//Ignored since this is more of integration test, than a unit test
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:tgtpaymentprovider-spring-test.xml")
public class VpcClientTest {

    private static final String TNSURL = "https://secure.ap.tnspayments.com/api/rest/version/11/merchant/TESTTARGET2/";
    private static final String VPCURL = "https://im.dialectpayments.com/vpcdps";

    private static long orderId;
    private static long transactionId;

    @Autowired
    private TnsWebOperations tnsClient;

    @Autowired
    private TnsWebOperations vpcClient;

    /**
     * 
     */
    @BeforeClass
    public static void setUpData()
    {
        orderId = System.currentTimeMillis() / 100;
        transactionId = orderId;
    }

    /**
     * @throws RequestRejectedException
     *             - for illegal request
     * @throws InvalidRequestException
     *             - for bad request
     * @throws TnsServerException
     *             - for connection related
     */
    @Test(expected = InvalidRequestException.class)
    public void testVpcRefundInvalidCredentials() throws RequestRejectedException, InvalidRequestException,
            TnsServerException {

        final VpcRefundRequest vpcRefundRequest = new VpcRefundRequest();
        vpcRefundRequest.setCommand("refund");
        vpcRefundRequest.setAmount("3100");
        vpcRefundRequest.setCurrency("AUD");

        vpcClient.post(VPCURL, vpcRefundRequest, VpcRefundResponse.class);
    }


    /**
     * @throws RequestRejectedException
     *             - for illegal request
     * @throws InvalidRequestException
     *             - for bad request
     * @throws TnsServerException
     *             - for connection related
     */
    @Test(expected = InvalidRequestException.class)
    public void testVpcRefundMissingTranNumber() throws RequestRejectedException, InvalidRequestException,
            TnsServerException {

        final VpcRefundRequest vpcRefundRequest = new VpcRefundRequest();
        vpcRefundRequest.setCommand("refund");
        vpcRefundRequest.setAmount("3100");
        vpcRefundRequest.setCurrency("AUD");

        vpcClient.post(VPCURL, vpcRefundRequest, VpcRefundResponse.class);
    }

    /**
     * @throws RequestRejectedException
     *             - for illegal request
     * @throws InvalidRequestException
     *             - for bad request
     * @throws TnsServerException
     *             - for connection related
     */
    @Test(expected = InvalidRequestException.class)
    public void testVpcRefundOverAndOver() throws RequestRejectedException, InvalidRequestException,
            TnsServerException {

        final VpcRefundRequest vpcRefundRequest = new VpcRefundRequest();
        vpcRefundRequest.setCommand("refund");
        vpcRefundRequest.setMerchTxnRef("1363326676");
        vpcRefundRequest.setTransactionNumber("13633266761");
        vpcRefundRequest.setAmount("3100");
        vpcRefundRequest.setCurrency("AUD");

        vpcClient.post(VPCURL, vpcRefundRequest, VpcRefundResponse.class);
    }

    /**
     * @throws RequestRejectedException
     *             - for illegal request
     * @throws InvalidRequestException
     *             - for bad request
     * @throws TnsServerException
     *             - for connection related
     */
    @Test(expected = InvalidRequestException.class)
    public void testVpcRefundBeforeDirectPay() throws RequestRejectedException, InvalidRequestException,
            TnsServerException {

        final VpcRefundRequest vpcRefundRequest = new VpcRefundRequest();
        vpcRefundRequest.setCommand("refund");
        vpcRefundRequest.setMerchTxnRef(String.valueOf(orderId + 1));
        vpcRefundRequest.setTransactionNumber(String.valueOf(orderId));
        vpcRefundRequest.setAmount("3100");
        vpcRefundRequest.setCurrency("AUD");

        vpcClient.post(VPCURL, vpcRefundRequest, VpcRefundResponse.class);
    }

    /**
     * @throws RequestRejectedException
     *             - for illegal request
     * @throws InvalidRequestException
     *             - for bad request
     * @throws TnsServerException
     *             - for connection related
     */
    @Test
    public void testVpcRefundSameAmount() throws RequestRejectedException, InvalidRequestException, TnsServerException {

        //Perform a PAY
        final String url = TNSURL + "/order/" + orderId + "/transaction/" + transactionId;
        final PayRequest payRequest = new PayRequest();
        payRequest.setApiOperation("PAY");
        final SourceOfFunds sourceOfFunds = new SourceOfFunds();
        final Transaction transaction = new Transaction();

        transaction.setAmount("31.00");
        transaction.setCurrency("AUD");
        sourceOfFunds.setToken("9414214093908489");
        sourceOfFunds.setType(PaymentType.CARD);
        payRequest.setSourceOfFunds(sourceOfFunds);
        payRequest.setTransaction(transaction);

        final GenericTransactionResponse payResponse = tnsClient.put(url, payRequest, GenericTransactionResponse.class);
        assertEquals("Expecting result ", ResultEnum.SUCCESS, payResponse.getResult());
        assertEquals("Expecting merchant ", "TESTTARGET2", payResponse.getMerchant());
        assertEquals("Expecting amount ", "31.00", payResponse.getTransaction().getAmount());

        final VpcRefundRequest vpcRefundRequest = new VpcRefundRequest();
        vpcRefundRequest.setCommand("refund");
        vpcRefundRequest.setMerchTxnRef(String.valueOf(orderId + 1));
        vpcRefundRequest.setTransactionNumber(String.valueOf(orderId));
        vpcRefundRequest.setAmount("3100");
        vpcRefundRequest.setCurrency("AUD");

        // refund
        VpcRefundResponse response = vpcClient.post(VPCURL, vpcRefundRequest, VpcRefundResponse.class);

        assertEquals("Expecting Message", "Approved", response.getMessage());
        assertNotNull("Expecting non null auth id", response.getAuthrorizeId());
        assertEquals("Expecting card", "MC", response.getCard());
        assertEquals("Expecting amount", "3100", response.getCapturedAmount());
        assertEquals("Expecting refund amount", "3100", response.getRefundedAmount());
        assertEquals("Expecting auth amount", "3100", response.getAuthorisedAmount());
        assertEquals("Expecting acquirer response code", "00", response.getAcquirerResponseCode());
        assertNotNull("Expecting non null reciept number", response.getReceiptNumber());
        assertNotNull("Expecting non null shop transaction num", response.getShopTransactionNo());

        // excessive refund
        vpcRefundRequest.setMerchTxnRef(String.valueOf(orderId + 1));
        vpcRefundRequest.setTransactionNumber(String.valueOf(orderId));
        vpcRefundRequest.setAmount("1000");
        vpcRefundRequest.setCurrency("AUD");

        response = vpcClient.post(VPCURL, vpcRefundRequest, VpcRefundResponse.class);

        assertEquals("Expecting message", "Approved", response.getMessage());
        assertNotNull("Expecting non null auth id", response.getAuthrorizeId());
        assertEquals("Expecting card", "MC", response.getCard());
        assertEquals("Expecting captured amount", "3100", response.getCapturedAmount());
        assertEquals("Expecting refund amount", "4100", response.getRefundedAmount()); // total amount refunded
        assertEquals("Expecting auth amount", "3100", response.getAuthorisedAmount());
        assertEquals("Expecting acquirer response code", "00", response.getAcquirerResponseCode());
        assertNotNull("Expecting non null reciept number", response.getReceiptNumber());
        assertNotNull("Expecting non null shop transaction number", response.getShopTransactionNo());
    }
}
