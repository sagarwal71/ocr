/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentReq;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentRequestType;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsReq;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsRequestType;
import urn.ebay.api.PayPalAPI.RefundTransactionReq;
import urn.ebay.api.PayPalAPI.RefundTransactionRequestType;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutReq;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutRequestType;
import urn.ebay.apis.CoreComponentTypes.BasicAmountType;
import urn.ebay.apis.eBLBaseComponents.AddressType;
import urn.ebay.apis.eBLBaseComponents.BillingAgreementDetailsType;
import urn.ebay.apis.eBLBaseComponents.BillingCodeType;
import urn.ebay.apis.eBLBaseComponents.CountryCodeType;
import urn.ebay.apis.eBLBaseComponents.CurrencyCodeType;
import urn.ebay.apis.eBLBaseComponents.DoExpressCheckoutPaymentRequestDetailsType;
import urn.ebay.apis.eBLBaseComponents.PaymentActionCodeType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsType;
import urn.ebay.apis.eBLBaseComponents.SetExpressCheckoutRequestDetailsType;


/**
 * @author bjames4
 * 
 */
public final class PaypalTransactionHelper
{
    /**
     * 
     */
    private PaypalTransactionHelper() {
    }

    /**
     * @param amount
     *            Payment amount
     * @return SetExpressCheckoutReq
     */
    public static SetExpressCheckoutReq createSetExpCheckoutReq(final String amount)
    {
        final SetExpressCheckoutRequestType setExpCheckoutReq = new SetExpressCheckoutRequestType();

        final SetExpressCheckoutRequestDetailsType details = new SetExpressCheckoutRequestDetailsType();
        details.setReturnURL("http://www.target.com.au");
        details.setCancelURL("http://www.target.com.au");

        final List<PaymentDetailsType> paymentDetailsTypes = new ArrayList<>();
        final PaymentDetailsType paymentDetailsType = getPaymentDetailsItem(amount);
        paymentDetailsTypes.add(paymentDetailsType);
        details.setPaymentDetails(paymentDetailsTypes);

        details.setReqBillingAddress("1");
        details.setAllowNote("0");
        details.setAddressOverride("1");
        details.setMaxAmount(new BasicAmountType(CurrencyCodeType.AUD, amount));

        final List<BillingAgreementDetailsType> billingAgreementDetails = new ArrayList<>();
        final BillingAgreementDetailsType billingAgreementDetailsType = new BillingAgreementDetailsType();
        billingAgreementDetailsType.setBillingType(BillingCodeType.MERCHANTINITIATEDBILLINGSINGLEAGREEMENT);
        billingAgreementDetails.add(billingAgreementDetailsType);
        details.setBillingAgreementDetails(billingAgreementDetails);

        setExpCheckoutReq.setSetExpressCheckoutRequestDetails(details);

        final SetExpressCheckoutReq request = new SetExpressCheckoutReq();
        request.setSetExpressCheckoutRequest(setExpCheckoutReq);

        return request;
    }

    /**
     * @param amount
     *            payment amount
     * @return payment details type
     */
    public static PaymentDetailsType getPaymentDetailsItem(final String amount)
    {
        final PaymentDetailsType paymDetailsType = new PaymentDetailsType();
        paymDetailsType.setOrderDescription("Sale - Order number 12345678");

        final AddressType shipToAddress = new AddressType();
        shipToAddress.setName("Bob Builder");
        shipToAddress.setStreet1("10 Thompson Road");
        shipToAddress.setStreet2("North Geelong");
        shipToAddress.setCityName("Geelong");
        shipToAddress.setStateOrProvince("Vic");
        shipToAddress.setCountryName("Australia");
        shipToAddress.setCountry(CountryCodeType.AU);
        shipToAddress.setPostalCode("3214");
        paymDetailsType.setShipToAddress(shipToAddress);

        paymDetailsType.setOrderTotal(new BasicAmountType(CurrencyCodeType.AUD, amount));
        paymDetailsType.setPaymentAction(PaymentActionCodeType.SALE);

        return paymDetailsType;
    }

    /**
     * @param token
     *            paypal token from SetExpressCheckout
     * @return GetExpressCheckoutDetailsReq
     */
    public static GetExpressCheckoutDetailsReq createGetExpCheckoutDetailsReq(final String token)
    {
        final GetExpressCheckoutDetailsRequestType getExpCheckoutDetailsReq =
                new GetExpressCheckoutDetailsRequestType();
        getExpCheckoutDetailsReq.setToken(token);

        final GetExpressCheckoutDetailsReq request = new GetExpressCheckoutDetailsReq();
        request.setGetExpressCheckoutDetailsRequest(getExpCheckoutDetailsReq);
        return request;
    }

    /**
     * @param token
     *            paypal token from SetExpressCheckout
     * @param payerId
     *            returned from GetExpressCheckout
     * @param amount
     *            payment amount
     * @return DoExpressCheckoutPaymentReq
     */
    public static DoExpressCheckoutPaymentReq createDoExpCheckoutPaymentReq(final String token,
            final String payerId,
            final String amount)
    {
        final DoExpressCheckoutPaymentRequestType doExpCheckoutPaymentReqType =
                new DoExpressCheckoutPaymentRequestType();

        final DoExpressCheckoutPaymentRequestDetailsType paymentDetailsRequestType =
                new DoExpressCheckoutPaymentRequestDetailsType();
        paymentDetailsRequestType.setToken(token);
        paymentDetailsRequestType.setPayerID(payerId);
        paymentDetailsRequestType.setPaymentAction(PaymentActionCodeType.SALE);
        final List<PaymentDetailsType> paymentDetailsTypes = new ArrayList<>();
        final PaymentDetailsType paymentDetailsType = getPaymentDetailsItem(amount);
        paymentDetailsTypes.add(paymentDetailsType);
        paymentDetailsRequestType.setPaymentDetails(paymentDetailsTypes);

        doExpCheckoutPaymentReqType.setDoExpressCheckoutPaymentRequestDetails(paymentDetailsRequestType);

        final DoExpressCheckoutPaymentReq request = new DoExpressCheckoutPaymentReq();
        request.setDoExpressCheckoutPaymentRequest(doExpCheckoutPaymentReqType);
        return request;


    }

    /**
     * @param transactionId
     *            - transaction to refund
     * @param amount
     *            - amount to refund
     * @param currency
     *            - iso currency code
     * @return - refund transaction request
     */
    public static RefundTransactionReq createRefundTransactionReq(final String transactionId,
            final BigDecimal amount, final Currency currency) {

        final RefundTransactionRequestType refundTransactionReqType = new RefundTransactionRequestType();
        final BasicAmountType amtType = new BasicAmountType();
        final CurrencyCodeType currencyCode = CurrencyCodeType.fromValue(currency.toString());

        amtType.setValue(amount.toString());
        amtType.setCurrencyID(currencyCode);
        refundTransactionReqType.setAmount(amtType);
        refundTransactionReqType.setTransactionID(transactionId);

        final RefundTransactionReq request = new RefundTransactionReq();
        request.setRefundTransactionRequest(refundTransactionReqType);
        return request;


    }
}
