/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.commands.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.TransactionStatus;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpaymentprovider.zippay.client.TargetZippayClient;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayClientException;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayTransactionError;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayTransactionRejectedException;
import au.com.target.tgtpaymentprovider.zippay.data.request.CreateRefundRequest;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateRefundResponse;


/**
 * @author salexa10
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ZippayFollowOnRefundCommandImplTest {

    @Mock
    private TargetZippayClient targetZippayClient;

    @InjectMocks
    private final ZippayFollowOnRefundCommandImpl command = new ZippayFollowOnRefundCommandImpl();

    @Mock
    private TargetFollowOnRefundRequest paramRequest;

    @Mock
    private CreateRefundResponse response;

    @Before
    public void setup() {
        given(paramRequest.getCaptureTransactionId()).willReturn("ch_OdvddBkayYR3xAn6AWwjT5");
        given(paramRequest.getTotalAmount()).willReturn(BigDecimal.valueOf(10));
        given(response.getId()).willReturn("rf_H5J24ZnqpKMByItvF2Jh85");
    }

    @Test
    public void testPerformWithSuccess()
            throws TargetZippayClientException, TargetZippayTransactionRejectedException, TargetZippayTransactionError {
        final ArgumentCaptor<CreateRefundRequest> createRefundRequestCapture = ArgumentCaptor
                .forClass(CreateRefundRequest.class);
        given(targetZippayClient.createRefund(createRefundRequestCapture.capture()))
                .willReturn(response);
        final TargetFollowOnRefundResult followOnRefundResult = command.perform(paramRequest);
        assertThat(followOnRefundResult).isNotNull();
        assertThat(followOnRefundResult.getReconciliationId()).isEqualTo("rf_H5J24ZnqpKMByItvF2Jh85");
        final CreateRefundRequest createRefundRequest = createRefundRequestCapture.getValue();
        assertThat(createRefundRequest).isNotNull();
        assertThat(createRefundRequest.getAmount()).isEqualTo(10.00);
        assertThat(createRefundRequest.getChargeId()).isNotNull();
    }

    @Test
    public void testPerformWithTransactionRejected()
            throws TargetZippayClientException, TargetZippayTransactionRejectedException, TargetZippayTransactionError {
        given(targetZippayClient.createRefund(Mockito.any(CreateRefundRequest.class)))
                .willThrow(new TargetZippayTransactionRejectedException());
        final TargetFollowOnRefundResult followOnRefundResult = command.perform(paramRequest);
        assertThat(followOnRefundResult.getTransactionStatus()).isEqualTo(TransactionStatus.REJECTED);
    }

    @Test
    public void testPerformWithTransactionError()
            throws TargetZippayClientException, TargetZippayTransactionRejectedException, TargetZippayTransactionError {
        given(targetZippayClient.createRefund(Mockito.any(CreateRefundRequest.class)))
                .willThrow(new TargetZippayTransactionError());
        final TargetFollowOnRefundResult followOnRefundResult = command.perform(paramRequest);
        assertThat(followOnRefundResult.getTransactionStatus()).isEqualTo(TransactionStatus.ERROR);
    }

    @Test(expected = AdapterException.class)
    public void testPerformWithClientExpection()
            throws TargetZippayClientException, TargetZippayTransactionRejectedException, TargetZippayTransactionError {
        given(targetZippayClient.createRefund(Mockito.any(CreateRefundRequest.class)))
                .willThrow(new TargetZippayClientException());
        command.perform(paramRequest);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPerformWithNullRequest() throws TargetZippayClientException {
        command.perform(null);
    }

}
