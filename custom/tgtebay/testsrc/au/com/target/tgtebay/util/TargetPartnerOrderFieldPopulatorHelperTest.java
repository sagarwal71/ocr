/**
 * 
 */
package au.com.target.tgtebay.util;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.payment.strategy.TransactionCodeGenerator;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtebay.constants.TgtebayConstants;
import au.com.target.tgtebay.dto.EbayTargetAddressDTO;
import au.com.target.tgtebay.dto.EbayTargetOrderDTO;
import au.com.target.tgtebay.dto.EbayTargetOrderEntryDTO;
import au.com.target.tgtebay.dto.EbayTargetPayInfoDTO;
import au.com.target.tgtpayment.constants.TgtpaymentConstants;
import au.com.target.tgtpayment.methods.impl.TargetPayPalPaymentMethodImpl;
import au.com.target.tgtpayment.model.PaynowPaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgtpayment.strategy.PaymentMethodStrategy;


/**
 * @author mjanarth
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPartnerOrderFieldPopulatorHelperTest {

    class MyTargetPartnerOrderFieldPopulatorHelper extends TargetPartnerOrderFieldPopulatorHelper {

        @Override
        protected CurrencyModel getCurrencyModelInstance() {
            return currencyModel;
        }
    }

    @Mock
    private ModelService mockModelService;
    @Mock
    private FlexibleSearchService mockFlexibleSearchService;
    @Mock
    private ProductService mockProductService;
    @Mock
    private UnitService mockUnitService;
    @Mock
    private TransactionCodeGenerator transactionCodeGenerator;
    @Mock
    private PaymentMethodStrategy mockpaymentMethodStrategy;
    @Mock
    private TargetPartnerOrderFieldValidator targetPartnerOrderFieldValidator;

    @InjectMocks
    private final MyTargetPartnerOrderFieldPopulatorHelper ebayTargetFieldPopulator = new MyTargetPartnerOrderFieldPopulatorHelper();

    private final CurrencyModel currencyModel = new CurrencyModel();

    @Test
    public void testPaypalPopulatePaymentInfo() {

        final PaypalPaymentInfoModel paymentInfo = new PaypalPaymentInfoModel();
        final CustomerModel customer = new CustomerModel();
        final AddressModel billingAddress = new AddressModel();
        billingAddress.setStreetname("thompson road");
        billingAddress.setStreetnumber("110");
        billingAddress.setTown("Geelong");
        billingAddress.setDistrict("vic");
        customer.setDefaultPaymentAddress(billingAddress);

        final EbayTargetPayInfoDTO payInfoDTO = new EbayTargetPayInfoDTO();
        payInfoDTO.setEmailId("test@test.com");
        payInfoDTO.setPaypalAccountId("test.ebay");
        payInfoDTO.setPaymentType("paypal");
        payInfoDTO.setTransactionId("123456789");
        BDDMockito.given(mockModelService.create(PaypalPaymentInfoModel.class)).willReturn(paymentInfo);
        BDDMockito.given(mockModelService.clone(billingAddress)).willReturn(new AddressModel());
        
        final PaymentInfoModel paymentInfoModel = ebayTargetFieldPopulator.populatePaymentInfo(
                TgtpaymentConstants.PAY_PAL_PAYMENT_TYPE, payInfoDTO,
                customer);
        Assert.assertNotNull(paymentInfoModel);
        Assert.assertTrue(paymentInfoModel instanceof PaypalPaymentInfoModel);
        final PaypalPaymentInfoModel paypalPaymentInfoModel = (PaypalPaymentInfoModel)paymentInfoModel;
        Assert.assertEquals(paypalPaymentInfoModel.getPayerId(), TgtebayConstants.EBAY_PAYPAL_PAYERID);
        Assert.assertEquals(paypalPaymentInfoModel.getIsPaymentSucceeded(), Boolean.TRUE);
        Assert.assertEquals(paypalPaymentInfoModel.getEmailId(), payInfoDTO.getPaypalAccountId());
        Assert.assertEquals(paypalPaymentInfoModel.getToken(), TgtebayConstants.EBAY_PAYPAL_TOKEN);

    }

    @Test
    public void testPaynowPopulatePaymentInfo() {

        final PaynowPaymentInfoModel paymentInfo = new PaynowPaymentInfoModel();
        final CustomerModel customer = new CustomerModel();
        final AddressModel billingAddress = new AddressModel();
        billingAddress.setStreetname("thompson road");
        billingAddress.setStreetnumber("110");
        billingAddress.setTown("Geelong");
        billingAddress.setDistrict("vic");
        customer.setDefaultPaymentAddress(billingAddress);

        final EbayTargetPayInfoDTO payInfoDTO = new EbayTargetPayInfoDTO();
        payInfoDTO.setEmailId("test@test.com");
        payInfoDTO.setPaypalAccountId("test.ebay");
        payInfoDTO.setPaymentType("trademe");
        payInfoDTO.setTransactionId("123456789");
        BDDMockito.given(mockModelService.create(PaynowPaymentInfoModel.class)).willReturn(paymentInfo);
        BDDMockito.given(mockModelService.clone(billingAddress)).willReturn(new AddressModel());

        final PaymentInfoModel paymentInfoModel = ebayTargetFieldPopulator
                .populatePaymentInfo(TgtpaymentConstants.PAY_NOW_PAYMENT_TYPE, payInfoDTO,
                        customer);
        Assert.assertNotNull(paymentInfoModel);
        Assert.assertTrue(paymentInfoModel instanceof PaynowPaymentInfoModel);
        final PaynowPaymentInfoModel paynowPaymentInfoModel = (PaynowPaymentInfoModel)paymentInfoModel;
        Assert.assertEquals(paynowPaymentInfoModel.getPayerId(), TgtebayConstants.TRADEME_PAYNOW_PAYERID);
        Assert.assertEquals(paynowPaymentInfoModel.getIsPaymentSucceeded(), Boolean.TRUE);
        Assert.assertEquals(paynowPaymentInfoModel.getEmailId(), payInfoDTO.getPaypalAccountId());

    }

    @Test
    public void testPopulateAddressModelCountryAU() {
        final EbayTargetAddressDTO address = new EbayTargetAddressDTO();
        final String ebayOrderId = "12345678";
        AddressModel addressModel = new AddressModel();
        final CountryModel country = new CountryModel();
        country.setIsocode("AU");
        address.setCountry("AU");
        address.setAddressLine1("Emert St");
        address.setStreetNumber("12");
        address.setTown("wentworthville");
        address.setDistrict("NSW");
        address.setFirstName("FirstName");
        address.setLastName("LastName");
        address.setPostcode("2145");
        address.setFax("123456789");
        address.setPhone1("0452355156");
        address.setPhone2("0123456789");

        ebayTargetFieldPopulator.setFlexibleSearchService(mockFlexibleSearchService);
        BDDMockito.given(mockModelService.create(AddressModel.class)).willReturn(addressModel);
        BDDMockito.given(mockFlexibleSearchService.getModelByExample(Mockito.any(CountryModel.class)))
                .willReturn(country);

        addressModel = ebayTargetFieldPopulator.populateAddressModel(address, ebayOrderId);

        Assert.assertEquals(addressModel.getStreetname(), address.getAddressLine1());
        Assert.assertEquals(addressModel.getStreetnumber(), address.getAddressLine2());
        Assert.assertEquals(addressModel.getDistrict(), address.getDistrict());
        Assert.assertEquals(addressModel.getFirstname(), address.getFirstName());
        Assert.assertEquals(addressModel.getLastname(), address.getLastName());
        Assert.assertEquals(addressModel.getPostalcode(), address.getPostcode());
        Assert.assertNotNull(addressModel.getCountry());
        Assert.assertEquals(addressModel.getCountry().getIsocode(), address.getCountry());

    }

    @Test
    public void testPopulateAddressModelCountryNZ() {
        final EbayTargetAddressDTO address = new EbayTargetAddressDTO();
        final String ebayOrderId = "12345678";
        AddressModel addressModel = new AddressModel();
        final CountryModel country = new CountryModel();
        country.setIsocode("NZ");
        address.setCountry("NZ");
        address.setFirstName("FirstName");
        address.setLastName("LastName");

        ebayTargetFieldPopulator.setFlexibleSearchService(mockFlexibleSearchService);
        BDDMockito.given(mockModelService.create(AddressModel.class)).willReturn(addressModel);
        BDDMockito.given(mockFlexibleSearchService.getModelByExample(Mockito.any(CountryModel.class)))
                .willReturn(country);

        addressModel = ebayTargetFieldPopulator.populateAddressModel(address, ebayOrderId);

        Assert.assertNotNull(addressModel.getCountry());
        Assert.assertEquals(addressModel.getCountry().getIsocode(), address.getCountry());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateOrderEntries() throws InvalidCartException {
        final EbayTargetOrderEntryDTO entry = new EbayTargetOrderEntryDTO();
        final CartEntryModel entryModel = new CartEntryModel();
        final ProductModel product = new ProductModel();
        final UnitModel unit = new UnitModel();
        unit.setCode("pieces");
        product.setCode("CP2033");
        final CartModel order = new CartModel();
        final List<EbayTargetOrderEntryDTO> eBayOrderList = new ArrayList<>();
        ebayTargetFieldPopulator.setModelService(mockModelService);
        entry.setBasePrice("4");
        entry.setProductCode("CP2033");
        entry.setQuantity("2");
        entry.setTotalPrice("8.00");
        eBayOrderList.add(entry);
        BDDMockito.given(mockProductService.getProductForCode(entry.getProductCode())).willReturn(product);
        BDDMockito.given(mockModelService.create(CartEntryModel.class)).willReturn(entryModel);
        BDDMockito.given(mockUnitService.getUnitForCode("pieces")).willReturn(unit);
        BDDMockito.given(mockModelService.create(CartModel.class)).willReturn(order);
        List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries = ebayTargetFieldPopulator.populateOrderEntries(eBayOrderList, order);
        Assert.assertEquals(orderEntries.size(), 1);
        Assert.assertEquals(orderEntries.get(0).getGiveAway(), Boolean.FALSE);
        Assert.assertEquals(0, Double.compare(orderEntries.get(0).getBasePrice().doubleValue(), 0.0));
    }


    @Test
    public void populatepaymentInfoTest() {
        final EbayTargetPayInfoDTO payInfo = new EbayTargetPayInfoDTO();
        final CurrencyModel currency = new CurrencyModel();
        final PaypalPaymentInfoModel paypalInfo = new PaypalPaymentInfoModel();
        final TargetPayPalPaymentMethodImpl payment = new TargetPayPalPaymentMethodImpl();
        final EbayTargetOrderDTO order = new EbayTargetOrderDTO();
        currency.setIsocode("AUD");
        payInfo.setEmailId("test@test.com");
        payInfo.setPaymentType("paypal");
        payInfo.setPlannedAmount("10.00");
        payInfo.setTransactionId("trans123");
        order.setPaymentInfo(payInfo);
        final CartModel cart = new CartModel();
        cart.setCode("100100");
        final PaymentTransactionModel paymentTransactionModel = new PaymentTransactionModel();
        List<PaymentTransactionModel> paymentList = new ArrayList<>();
        BDDMockito.given(mockModelService.create(PaymentTransactionModel.class)).willReturn(paymentTransactionModel);
        BDDMockito.given(mockFlexibleSearchService.getModelByExample(Mockito.any(CurrencyModel.class))).willReturn(
                currency);
        final PaymentTransactionEntryModel paymentTransactionEntry = new PaymentTransactionEntryModel();
        BDDMockito.given(mockpaymentMethodStrategy.getPaymentMethod(Mockito.any(PaypalPaymentInfoModel.class)))
                .willReturn(payment);

        BDDMockito.given(mockModelService.create(PaymentTransactionEntryModel.class)).willReturn(
                paymentTransactionEntry);
        paymentList = ebayTargetFieldPopulator.populatePaymentTransaction(order, cart, paypalInfo, "test@test.com");
        Assert.assertEquals(paymentList.size(), 1);
        Assert.assertEquals(paymentList.get(0).getEntries().size(), 1);
        Assert.assertTrue(paymentList.get(0).getCode().contains("test@test.com"));
        Assert.assertEquals(paymentList.get(0).getOrder().getCode(), cart.getCode());
    }

    @Test
    public void testPopulateNamesWithStandardNames() {
        final EbayTargetAddressDTO addressDto = new EbayTargetAddressDTO();
        addressDto.setFirstName("Bruce");
        addressDto.setLastName("Wayne");

        final AddressModel mockAddress = mock(AddressModel.class);

        ebayTargetFieldPopulator.populateNames(addressDto, mockAddress, null);

        verify(mockAddress).setFirstname("Bruce");
        verify(mockAddress).setLastname("Wayne");
    }

    @Test
    public void testPopulateNamesWithShortNames() {
        final EbayTargetAddressDTO addressDto = new EbayTargetAddressDTO();
        addressDto.setFirstName("B");
        addressDto.setLastName("W");

        final AddressModel mockAddress = mock(AddressModel.class);

        ebayTargetFieldPopulator.populateNames(addressDto, mockAddress, null);

        verify(mockAddress).setFirstname("BW");
        verify(mockAddress).setLastname("BW");
    }

    @Test
    public void testPopulateNamesWithShortFirstName() {
        final EbayTargetAddressDTO addressDto = new EbayTargetAddressDTO();
        addressDto.setFirstName("B");
        addressDto.setLastName("Wayne");

        final AddressModel mockAddress = mock(AddressModel.class);

        ebayTargetFieldPopulator.populateNames(addressDto, mockAddress, null);

        verify(mockAddress).setFirstname("BWayne");
        verify(mockAddress).setLastname("Wayne");
    }

    @Test
    public void testPopulateNamesWithShortLastName() {
        final EbayTargetAddressDTO addressDto = new EbayTargetAddressDTO();
        addressDto.setFirstName("Bruce");
        addressDto.setLastName("W");

        final AddressModel mockAddress = mock(AddressModel.class);

        ebayTargetFieldPopulator.populateNames(addressDto, mockAddress, null);

        verify(mockAddress).setFirstname("Bruce");
        verify(mockAddress).setLastname("BruceW");
    }

    @Test
    public void testPopulateNamesWithSymbolInFirstName() {
        final EbayTargetAddressDTO addressDto = new EbayTargetAddressDTO();
        addressDto.setFirstName("Mr & Mrs");
        addressDto.setLastName("Smith");

        final AddressModel mockAddress = mock(AddressModel.class);

        ebayTargetFieldPopulator.populateNames(addressDto, mockAddress, null);

        verify(mockAddress).setFirstname("Mr Mrs");
        verify(mockAddress).setLastname("Smith");
    }

    @Test
    public void testPopulateNamesWithEverythingWrong() {
        final EbayTargetAddressDTO addressDto = new EbayTargetAddressDTO();
        addressDto.setFirstName("            B +    r % u    * c    ^  e              ");
        addressDto.setLastName("O'''M a     ll  $$$#(%*%*%*%!)_!)!_#)@*%&%& ey");

        final AddressModel mockAddress = mock(AddressModel.class);

        ebayTargetFieldPopulator.populateNames(addressDto, mockAddress, null);

        verify(mockAddress).setFirstname("B r u c e");
        verify(mockAddress).setLastname("O'''M a ll ey");
    }

    @Test
    public void testPopulateNamesWithNotEnoughDataLeft() {
        final EbayTargetAddressDTO addressDto = new EbayTargetAddressDTO();
        addressDto.setFirstName("B#(%$$");
        addressDto.setLastName("@)$(!*");

        final AddressModel mockAddress = mock(AddressModel.class);

        ebayTargetFieldPopulator.populateNames(addressDto, mockAddress, null);

        verify(mockAddress).setFirstname("B#(%$$");
        verify(mockAddress).setLastname("@)$(!*");
    }

    @Test
    public void testPopulateNamesWithLongNames() {
        final EbayTargetAddressDTO addressDto = new EbayTargetAddressDTO();
        addressDto.setFirstName("BruceBruceBruceBruceBruceBruceBruceBruce");
        addressDto.setLastName("WayneWayneWayneWayneWayneWayneWayneWayne");

        final AddressModel mockAddress = mock(AddressModel.class);

        ebayTargetFieldPopulator.populateNames(addressDto, mockAddress, null);

        verify(mockAddress).setFirstname("BruceBruceBruceBruceBruceBruceBr");
        verify(mockAddress).setLastname("WayneWayneWayneWayneWayneWayneWa");
    }

    @Test
    public void testPopulateNamesWithDash() {
        final EbayTargetAddressDTO addressDto = new EbayTargetAddressDTO();
        addressDto.setFirstName("Bruce-");
        addressDto.setLastName("-Bruce");

        final AddressModel mockAddress = mock(AddressModel.class);

        ebayTargetFieldPopulator.populateNames(addressDto, mockAddress, null);

        verify(mockAddress).setFirstname("Bruce");
        verify(mockAddress).setLastname("Bruce");
    }

    @Test
    public void testPopulateNamesWithDashAndSpace() {
        final EbayTargetAddressDTO addressDto = new EbayTargetAddressDTO();
        addressDto.setFirstName("Wayne- Bruce");
        addressDto.setLastName("Wayne -Bruce");

        final AddressModel mockAddress = mock(AddressModel.class);

        ebayTargetFieldPopulator.populateNames(addressDto, mockAddress, null);

        verify(mockAddress).setFirstname("Wayne-Bruce");
        verify(mockAddress).setLastname("Wayne-Bruce");
    }

    @Test
    public void testPopulateNamesWithApostrophe() {
        final EbayTargetAddressDTO addressDto = new EbayTargetAddressDTO();
        addressDto.setFirstName("Wayne'");
        addressDto.setLastName("'Wayne");

        final AddressModel mockAddress = mock(AddressModel.class);

        ebayTargetFieldPopulator.populateNames(addressDto, mockAddress, null);

        verify(mockAddress).setFirstname("Wayne");
        verify(mockAddress).setLastname("Wayne");
    }

    @Test
    public void testPopulateNamesWithApostropheAndSpace() {
        final EbayTargetAddressDTO addressDto = new EbayTargetAddressDTO();
        addressDto.setFirstName("Wayne ' Bruce");
        addressDto.setLastName("Wayne'Bruce");

        final AddressModel mockAddress = mock(AddressModel.class);

        ebayTargetFieldPopulator.populateNames(addressDto, mockAddress, null);

        verify(mockAddress).setFirstname("Wayne'Bruce");
        verify(mockAddress).setLastname("Wayne'Bruce");
    }

    @Test
    public void testGetCurrencyByCodeNull() {
        ebayTargetFieldPopulator.getCurrencyByCode(null);

        Mockito.verify(mockFlexibleSearchService)
                .getModelByExample(getCurrencyModelExample(TgtebayConstants.CURRENCY_CODE_AUD));
    }

    @Test
    public void testGetCurrencyByCodeEmpty() {
        ebayTargetFieldPopulator.getCurrencyByCode(StringUtils.EMPTY);

        Mockito.verify(mockFlexibleSearchService)
                .getModelByExample(getCurrencyModelExample(TgtebayConstants.CURRENCY_CODE_AUD));
    }

    @Test
    public void testGetCurrencyByCodeAUD() {
        ebayTargetFieldPopulator.getCurrencyByCode(TgtebayConstants.CURRENCY_CODE_AUD);

        Mockito.verify(mockFlexibleSearchService)
                .getModelByExample(getCurrencyModelExample(TgtebayConstants.CURRENCY_CODE_AUD));
    }

    @Test
    public void testGetCurrencyByCodeNZD() {
        ebayTargetFieldPopulator.getCurrencyByCode(TgtebayConstants.CURRENCY_CODE_NZD);

        Mockito.verify(mockFlexibleSearchService)
                .getModelByExample(getCurrencyModelExample(TgtebayConstants.CURRENCY_CODE_NZD));
    }

    private CurrencyModel getCurrencyModelExample(final String code) {
        currencyModel.setIsocode(code);
        return currencyModel;
    }

}
