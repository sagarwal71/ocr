package au.com.target.tgtebay.jobs;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.dao.TargetConsignmentDao;


/**
 * 
 * @author Trinadh N
 * 
 *         Cron job to auto update partner with "picked-up" click and collect status for the orders in particular period
 * 
 */
public class AutoUpdatePartnerPickedupJob extends AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(AutoUpdatePartnerPickedupJob.class);
    private static final String INFO_MESSAGE_AUTO_UPDATE_PICKED_JOB = "AutoUpdatePartnerPickedupJob : ";

    private TargetConsignmentDao targetConsignmentDao;

    private TargetBusinessProcessService targetBusinessProcessService;

    private int autoPickedUpWaitingDays;

    private String ebayClickAndCollectDeliveryCode;


    @Override
    public PerformResult perform(final CronJobModel cronJob) {

        LOG.info(INFO_MESSAGE_AUTO_UPDATE_PICKED_JOB + "Started");

        final List<TargetConsignmentModel> consignmentsToPickedUp = targetConsignmentDao
                .getConsignmentsForAutoUpdatePartnerPickedup(ebayClickAndCollectDeliveryCode, autoPickedUpWaitingDays);
        if (null != consignmentsToPickedUp) {
            for (final TargetConsignmentModel consignment : consignmentsToPickedUp) {
                consignment.setPickedUpAutoDate(new Date());
                modelService.save(consignment);
                targetBusinessProcessService.startSendClickAndCollectPickedupNotificationProcess(
                        (OrderModel)consignment.getOrder(), true);
                LOG.info(MessageFormat.format(
                        INFO_MESSAGE_AUTO_UPDATE_PICKED_JOB
                                + "Sending Auto pickedup notification to partner with orderNumber={0}",
                        consignment.getOrder().getCode()));
            }
        }
        LOG.info(INFO_MESSAGE_AUTO_UPDATE_PICKED_JOB + "Successful");
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }


    /**
     * @param targetConsignmentDao
     *            the targetConsignmentDao to set
     */
    @Required
    public void setTargetConsignmentDao(final TargetConsignmentDao targetConsignmentDao) {
        this.targetConsignmentDao = targetConsignmentDao;
    }

    /**
     * @param autoPickedUpWaitingDays
     *            the autoPickedUpWaitingDays to set
     */
    @Required
    public void setAutoPickedUpWaitingDays(final int autoPickedUpWaitingDays) {
        this.autoPickedUpWaitingDays = autoPickedUpWaitingDays;
    }


    /**
     * @param ebayClickAndCollectDeliveryCode
     *            the ebayClickAndCollectDeliveryCode to set
     */
    @Required
    public void setEbayClickAndCollectDeliveryCode(final String ebayClickAndCollectDeliveryCode) {
        this.ebayClickAndCollectDeliveryCode = ebayClickAndCollectDeliveryCode;
    }


    /**
     * @param targetBusinessProcessService
     *            the targetBusinessProcessService to set
     */
    @Required
    public void setTargetBusinessProcessService(final TargetBusinessProcessService targetBusinessProcessService) {
        this.targetBusinessProcessService = targetBusinessProcessService;
    }


}
