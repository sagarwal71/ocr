/**
 * 
 */
package au.com.target.tgtebay.exceptions;

/**
 * @author siddharam
 *
 */
public class ImportOrderException extends Exception {

    public ImportOrderException(final String message) {
        super(message);
    }

    public ImportOrderException(final Throwable cause) {
        super(cause);
    }

    public ImportOrderException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
