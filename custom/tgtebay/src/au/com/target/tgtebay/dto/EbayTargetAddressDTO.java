/**
 * 
 */
package au.com.target.tgtebay.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * 
 * @author mjanarth
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class EbayTargetAddressDTO {

    @XmlElement
    private String title;

    @XmlElement
    private String firstName;

    @XmlElement
    private String lastName;

    @XmlElement
    private String streetname;

    @XmlElement
    private String streetNumber;

    @XmlElement
    private String addressLine1;

    @XmlElement
    private String addressLine2;

    @XmlElement
    private String building;

    @XmlElement
    private String town;

    @XmlElement
    private String district;

    @XmlElement
    private String postcode;

    @XmlElement
    private String country;

    @XmlElement
    private String phone1;

    @XmlElement
    private String phone2;

    @XmlElement
    private String company;

    @XmlElement
    private String fax;

    @XmlElement
    private String shippingCarrier;

    @XmlElement
    private String shippingClass;

    public String getStreetname() {
        return streetname;
    }

    public void setStreetname(final String streetname) {
        this.streetname = streetname;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(final String building) {
        this.building = building;
    }


    public String getTown() {
        return town;
    }

    public void setTown(final String town) {
        this.town = town;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(final String district) {
        this.district = district;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(final String postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(final String country) {
        this.country = country;
    }

    /**
     * @return the addressLine1
     */
    public String getAddressLine1() {
        return addressLine1;
    }

    /**
     * @param addressLine1
     *            the addressLine1 to set
     */
    public void setAddressLine1(final String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    /**
     * @return the addressLine2
     */
    public String getAddressLine2() {
        return addressLine2;
    }

    /**
     * @param addressLine2
     *            the addressLine2 to set
     */
    public void setAddressLine2(final String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    /**
     * @return the streetNumber
     */
    public String getStreetNumber() {
        return streetNumber;
    }

    /**
     * @param streetNumber
     *            the streetNumber to set
     */
    public void setStreetNumber(final String streetNumber) {
        this.streetNumber = streetNumber;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the phone1
     */
    public String getPhone1() {
        return phone1;
    }

    /**
     * @param phone1
     *            the phone1 to set
     */
    public void setPhone1(final String phone1) {
        this.phone1 = phone1;
    }

    /**
     * @return the phone2
     */
    public String getPhone2() {
        return phone2;
    }

    /**
     * @param phone2
     *            the phone2 to set
     */
    public void setPhone2(final String phone2) {
        this.phone2 = phone2;
    }

    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param company
     *            the company to set
     */
    public void setCompany(final String company) {
        this.company = company;
    }

    /**
     * @return the fax
     */
    public String getFax() {
        return fax;
    }

    /**
     * @param fax
     *            the fax to set
     */
    public void setFax(final String fax) {
        this.fax = fax;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * @return the shippingCarrier
     */
    public String getShippingCarrier() {
        return shippingCarrier;
    }

    /**
     * @param shippingCarrier
     *            the shippingCarrier to set
     */
    public void setShippingCarrier(final String shippingCarrier) {
        this.shippingCarrier = shippingCarrier;
    }

    /**
     * @return the shippingClass
     */
    public String getShippingClass() {
        return shippingClass;
    }

    /**
     * @param shippingClass
     *            the shippingClass to set
     */
    public void setShippingClass(final String shippingClass) {
        this.shippingClass = shippingClass;
    }

}
