/**
 * 
 */
package au.com.target.tgtjms.jms.converter;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.xml.sax.SAXException;

import au.com.target.tgtcore.customer.impl.TargetCustomerEmailResolutionService;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtjms.jms.message.CncOrderExtractMessage;
import au.com.target.tgtjms.jms.model.CncOrderExtract;
import au.com.target.tgtlayby.model.PurchaseOptionModel;


/**
 * @author fkratoch
 * 
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({ "au.com.target.tgtjms.jms.converter.CncOrderExtractMessageConverter" })
@PrepareForTest({ Registry.class, CncOrderExtractMessageConverter.class })
public class CncOrderExtractMessageConverterTest extends XMLTestCase {

    private final String consignmentCode = "testCode1";

    @Mock
    private AddressModel deliveryAddress;

    @Mock
    private AddressModel paymentAddress;

    @Mock
    private TitleModel title;

    @Mock
    private PurchaseOptionModel purchaseOpt;

    @Mock
    private TargetZoneDeliveryModeModel delivery;

    @Mock
    private OrderModel order;

    @Mock
    private TargetConsignmentModel con;

    @Mock
    private TargetCustomerModel customer;

    @Mock
    private TargetCustomerEmailResolutionService targetCustomerEmailResolutionService;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Override
    @Before
    public void setUp() throws IllegalArgumentException, IllegalAccessException, JAXBException {
        MockitoAnnotations.initMocks(this);
        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled("cncOrderExtractOnConsignment")))
                .willReturn(Boolean.TRUE);
        final Field fieldfeatureService = PowerMockito.field(CncOrderExtractMessageConverter.class,
                "targetFeatureSwitchService");
        fieldfeatureService.set(TargetFeatureSwitchService.class, targetFeatureSwitchService);
        final Field fieldLogger = PowerMockito.field(CncOrderExtractMessageConverter.class,
                "LOG");
        fieldLogger.set(Logger.class, Logger.getLogger(CncOrderExtractMessage.class));
        final Field fieldJAXBContext = PowerMockito.field(CncOrderExtractMessageConverter.class,
                "cncOrderExtractJaxbContext");
        fieldJAXBContext.set(JAXBContext.class, JAXBContext.newInstance(CncOrderExtract.class));
    }

    @Test
    public void convertOrderToCncExtractMessageByConsignmentTest() throws SAXException, IOException {
        setupExpectations();



        final CncOrderExtractMessage orderMsg = new CncOrderExtractMessage(null, con,
                targetCustomerEmailResolutionService);
        final String msg = CncOrderExtractMessageConverter.getMessagePayload(orderMsg,
                targetCustomerEmailResolutionService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<order type=\"P\" id=\"1234567890\" storeNum=\"9999\">"
                + "<fulfilmentId>" + consignmentCode
                + "</fulfilmentId><title>Mr</title><firstName>Jane</firstName><surname>Smith</surname>"
                + "<email>john.smith@target.com.au</email>"
                + "<mobile>0432123456</mobile><parcelCount>3</parcelCount><orderCompleted>N</orderCompleted>"
                + "<palletNum></palletNum><paymentStatus>C</paymentStatus></order>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void convertOrderToCncExtractMessageTest() throws SAXException, IOException {
        setupExpectations();
        final CncOrderExtractMessage orderMsg = new CncOrderExtractMessage(order, null,
                targetCustomerEmailResolutionService
                );
        final String msg = CncOrderExtractMessageConverter.getMessagePayload(orderMsg,
                targetCustomerEmailResolutionService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<order type=\"P\" id=\"1234567890\" storeNum=\"9999\">"
                + "<fulfilmentId></fulfilmentId><title>Mr</title><firstName>Jane</firstName><surname>Smith</surname>"
                + "<email>john.smith@target.com.au</email>"
                + "<mobile>0432123456</mobile><parcelCount>0</parcelCount><orderCompleted>Y</orderCompleted>"
                + "<palletNum></palletNum><paymentStatus>C</paymentStatus></order>";
        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }


    @Test(expected = IllegalArgumentException.class)
    public void expectExceptionForNonCncOrderTest() {

        Mockito.when(order.getCode()).thenReturn("1234567890");
        Mockito.when(order.getCncStoreNumber()).thenReturn(Integer.valueOf(9999));
        Mockito.when(order.getStatus()).thenReturn(OrderStatus.COMPLETED);

        Mockito.when(purchaseOpt.getCode()).thenReturn("buynow");

        Mockito.when(paymentAddress.getFirstname()).thenReturn("John");
        Mockito.when(paymentAddress.getLastname()).thenReturn("Smith");
        Mockito.when(paymentAddress.getEmail()).thenReturn("john.smith@target.com.au");
        Mockito.when(paymentAddress.getCellphone()).thenReturn("0432123456");

        Mockito.when(title.getCode()).thenReturn("Mr");

        Mockito.when(delivery.getCode()).thenReturn("buy-now");

        //final Set<ConsignmentModel> cons = Mockito.mock(Set.class);
        final Set<ConsignmentModel> cons = new HashSet<>();
        Mockito.when(con.getParcelCount()).thenReturn(Integer.valueOf(3));
        cons.add(con);


        Mockito.when(order.getPaymentAddress()).thenReturn(paymentAddress);
        Mockito.when(paymentAddress.getTitle()).thenReturn(title);
        Mockito.when(order.getConsignments()).thenReturn(cons);
        Mockito.when(order.getPurchaseOption()).thenReturn(purchaseOpt);
        Mockito.when(order.getDeliveryMode()).thenReturn(delivery);

        final CncOrderExtractMessage orderMsg = new CncOrderExtractMessage(order, null,
                targetCustomerEmailResolutionService
                );
        final String msg = CncOrderExtractMessageConverter.getMessagePayload(orderMsg,
                targetCustomerEmailResolutionService);

        assertEquals("Can convert CNC Order Extract", "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<order type=\"P\" id=\"1234567890\" storeNum=\"9999\">"
                + "<title>Mr</title><firstName>John</firstName><surname>Smith</surname>"
                + "<email>john.smith@target.com.au</email>"
                + "<mobile>0432123456</mobile><parcelCount>3</parcelCount>"
                + "<palletNum></palletNum><paymentStatus>C</paymentStatus></order>", msg);
    }

    public void setupExpectations() {
        when(con.getParcelCount()).thenReturn(Integer.valueOf(3));
        when(con.getOrder()).thenReturn(order);
        when(con.getDeliveryMode()).thenReturn(delivery);
        when(con.getCode()).thenReturn(consignmentCode);
        Mockito.when(order.getCode()).thenReturn("1234567890");
        Mockito.when(order.getCncStoreNumber()).thenReturn(Integer.valueOf(9999));
        Mockito.when(order.getStatus()).thenReturn(OrderStatus.COMPLETED);

        Mockito.when(purchaseOpt.getCode()).thenReturn("buynow");

        Mockito.when(paymentAddress.getFirstname()).thenReturn("John");
        Mockito.when(paymentAddress.getLastname()).thenReturn("Smith");
        Mockito.when(paymentAddress.getEmail()).thenReturn("john.smith@target.com.au");
        Mockito.when(paymentAddress.getPhone1()).thenReturn("0432123456");

        Mockito.when(deliveryAddress.getFirstname()).thenReturn("Jane");
        Mockito.when(deliveryAddress.getLastname()).thenReturn("Smith");
        Mockito.when(deliveryAddress.getTitle()).thenReturn(title);
        Mockito.when(deliveryAddress.getEmail()).thenReturn("john.smith@target.com.au");
        Mockito.when(deliveryAddress.getPhone1()).thenReturn("0432123456");

        Mockito.when(title.getCode()).thenReturn("Mr");

        Mockito.when(delivery.getCode()).thenReturn("click-and-collect");
        Mockito.when(delivery.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);

        Mockito.when(order.getDeliveryAddress()).thenReturn(deliveryAddress);
        Mockito.when(order.getPaymentAddress()).thenReturn(paymentAddress);
        Mockito.when(order.getPurchaseOption()).thenReturn(purchaseOpt);
        Mockito.when(order.getDeliveryMode()).thenReturn(delivery);
        Mockito.when(order.getUser()).thenReturn(customer);
        Mockito.when(targetCustomerEmailResolutionService.getEmailForCustomer(customer)).thenReturn(
                "john.smith@target.com.au");
    }
}