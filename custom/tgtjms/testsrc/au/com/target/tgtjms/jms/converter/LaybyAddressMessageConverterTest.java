/**
 * 
 */
package au.com.target.tgtjms.jms.converter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import au.com.target.tgtjms.jms.message.LaybyAddressMessage;
import au.com.target.tgtjms.util.JmsMessageHelper;


/**
 * @author fkratoch
 * 
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest(JmsMessageHelper.class)
public class LaybyAddressMessageConverterTest {

    private static final String STORE_NUMBER = "5972";
    private static final String ORDER_NUMBER = "1234567890";

    /**
     * Initializes test cases before run.
     */
    @Before
    public void setUp() {
        PowerMockito.mockStatic(JmsMessageHelper.class);
        when(JmsMessageHelper.getStoreNumber()).thenReturn(STORE_NUMBER);
        when(JmsMessageHelper.getLaybyNumForPos(ORDER_NUMBER)).thenReturn(STORE_NUMBER + ORDER_NUMBER);
    }

    @Test
    public void convertOrderToLaybyAddressExtractMessageTest()
    {

        final OrderModel order = Mockito.mock(OrderModel.class);
        Mockito.when(order.getCode()).thenReturn(ORDER_NUMBER);

        final AddressModel deliveryAddress = Mockito.mock(AddressModel.class);
        Mockito.when(deliveryAddress.getLine1()).thenReturn("12 Thompson Road");
        Mockito.when(deliveryAddress.getLine2()).thenReturn("North Geelong");
        Mockito.when(deliveryAddress.getTown()).thenReturn("Geelong");
        Mockito.when(deliveryAddress.getPostalcode()).thenReturn("3215");
        Mockito.when(deliveryAddress.getPhone1()).thenReturn("52349876");
        Mockito.when(deliveryAddress.getCellphone()).thenReturn("0432123456");

        final DeliveryModeModel delivery = Mockito.mock(DeliveryModeModel.class);
        Mockito.when(delivery.getCode()).thenReturn("longterm-layby");

        final CountryModel country = Mockito.mock(CountryModel.class);
        Mockito.when(country.getName()).thenReturn("Australia");

        Mockito.when(order.getDeliveryMode()).thenReturn(delivery);
        Mockito.when(order.getDeliveryAddress()).thenReturn(deliveryAddress);
        Mockito.when(deliveryAddress.getCountry()).thenReturn(country);

        final LaybyAddressMessage addressMsg = new LaybyAddressMessage(order);
        final String msg = LaybyAddressMessageConverter.getMessagePayload(addressMsg);

        assertEquals(
                "Can create layby order address extract",
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><order><laybyNum>59721234567890</laybyNum>"
                        + "<storeNum>5972</storeNum><addrLine1>12 Thompson Road</addrLine1><addrLine2>North Geelong</addrLine2>"
                        + "<city>Geelong</city><country>Australia</country><postCode>3215</postCode><phone>52349876</phone>"
                        + "<mobile>0432123456</mobile></order>", msg);
    }

    @Test
    public void emptyFieldsInConvertOrderToLaybyAddressExtractMessageTest()
    {

        final OrderModel order = Mockito.mock(OrderModel.class);
        Mockito.when(order.getCode()).thenReturn(ORDER_NUMBER);

        final AddressModel deliveryAddress = Mockito.mock(AddressModel.class);
        Mockito.when(deliveryAddress.getLine1()).thenReturn("12 Thompson Road");
        Mockito.when(deliveryAddress.getLine2()).thenReturn(null);
        Mockito.when(deliveryAddress.getTown()).thenReturn("Geelong");
        Mockito.when(deliveryAddress.getPostalcode()).thenReturn("3215");
        Mockito.when(deliveryAddress.getPhone1()).thenReturn("52349876");
        Mockito.when(deliveryAddress.getCellphone()).thenReturn("0432123456");

        final DeliveryModeModel delivery = Mockito.mock(DeliveryModeModel.class);
        Mockito.when(delivery.getCode()).thenReturn("longterm-layby");

        Mockito.when(order.getDeliveryMode()).thenReturn(delivery);
        Mockito.when(order.getDeliveryAddress()).thenReturn(deliveryAddress);

        final LaybyAddressMessage addressMsg = new LaybyAddressMessage(order);
        final String msg = LaybyAddressMessageConverter.getMessagePayload(addressMsg);

        assertEquals(
                "Can create layby order address extract",
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><order><laybyNum>59721234567890</laybyNum>"
                        + "<storeNum>5972</storeNum><addrLine1>12 Thompson Road</addrLine1><addrLine2></addrLine2>"
                        + "<city>Geelong</city><country></country><postCode>3215</postCode><phone>52349876</phone>"
                        + "<mobile>0432123456</mobile></order>", msg);
    }
}
