/**
 * 
 */
package au.com.target.tgtjms.jms.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.customer.impl.TargetCustomerEmailResolutionService;
import au.com.target.tgtfulfilment.ConsignmentSentToWarehouseReason;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseResponse;
import au.com.target.tgtjms.jms.gateway.JmsMessageDispatcher;
import au.com.target.tgtjms.jms.message.AbstractMessage;


/**
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class JmsSendToWarehouseProtocolTest {

    private static final String CON_EXTRACT_Q = "consignmentExtractQueueId";

    @Mock
    private TargetCustomerEmailResolutionService targetCustomerEmailResolutionService;

    @Mock
    private JmsMessageDispatcher messageDispatcher;

    @InjectMocks
    private final JmsSendToWarehouseProtocol fulfilmentProcessService = new JmsSendToWarehouseProtocol() {
        @Override
        public AbstractMessage getInstanceOfExtractMessage(final ConsignmentModel consignment,
                final String warehouseCode, final ConsignmentSentToWarehouseReason reason) {
            final AbstractMessage message = Mockito.mock(AbstractMessage.class);
            return message;
        }
    };

    @Before
    public void setup() {
        fulfilmentProcessService.setConsignmentExtractQueueId(CON_EXTRACT_Q);
    }

    @Test
    public void testSendConsignmentExtract()
    {
        final ConsignmentModel consignment = Mockito.mock(ConsignmentModel.class);
        final OrderModel order = Mockito.mock(OrderModel.class);
        final WarehouseModel warehouse = Mockito.mock(WarehouseModel.class);
        BDDMockito.given(consignment.getOrder()).willReturn(order);
        BDDMockito.given(consignment.getWarehouse()).willReturn(warehouse);
        BDDMockito.given(warehouse.getCode()).willReturn("fred");

        final SendToWarehouseResponse response = fulfilmentProcessService.sendConsignmentExtract(consignment,
                ConsignmentSentToWarehouseReason.NEW);

        Assert.assertTrue(response.isSuccess());
        // verify using correct queue
        Mockito.verify(messageDispatcher).send(Mockito.eq("fred" + CON_EXTRACT_Q), Mockito.any(AbstractMessage.class));
    }

    @Test
    public void testSendConsignmentExtractNoWarehouse()
    {
        final ConsignmentModel consignment = Mockito.mock(ConsignmentModel.class);
        final OrderModel order = Mockito.mock(OrderModel.class);
        BDDMockito.given(consignment.getOrder()).willReturn(order);

        final SendToWarehouseResponse response = fulfilmentProcessService.sendConsignmentExtract(consignment,
                ConsignmentSentToWarehouseReason.NEW);
        Assert.assertTrue(response.isSuccess());
        // verify using correct queue
        Mockito.verify(messageDispatcher).send(Mockito.eq("unknown" + CON_EXTRACT_Q),
                Mockito.any(AbstractMessage.class));
    }

}