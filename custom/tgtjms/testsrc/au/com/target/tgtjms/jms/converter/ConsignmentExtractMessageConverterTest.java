package au.com.target.tgtjms.jms.converter;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.model.ValueBundleDealModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.xml.sax.SAXException;

import au.com.target.tgtcore.currency.conversion.service.CurrencyConversionFactorService;
import au.com.target.tgtcore.customer.impl.TargetCustomerEmailResolutionService;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TMDiscountPromotionModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;
import au.com.target.tgtfulfilment.ConsignmentSentToWarehouseReason;
import au.com.target.tgtfulfilment.model.ConsignmentExtractRecordModel;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;
import au.com.target.tgtfulfilment.salesapplication.TargetFulfilmentSalesApplicationConfigService;
import au.com.target.tgtjms.constants.TgtjmsConstants;
import au.com.target.tgtjms.jms.message.FastlineWarehouseConsignmentExtractMessage;
import au.com.target.tgtlayby.model.PurchaseOptionModel;


/**
 * Test suite for {@link ConsignmentExtractMessageConverter}.
 * 
 * @author mmaki
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ConsignmentExtractMessageConverterTest extends XMLTestCase {

    @Mock
    private TargetCustomerEmailResolutionService targetCustomerEmailResolutionService;

    @Mock
    private CurrencyConversionFactorService currencyConversionFactorService;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private OrderModel order;

    @Mock
    private OrderEntryModel entry;

    @Mock
    private ConsignmentEntryModel consignmentEntry;

    @Mock
    private TargetSizeVariantProductModel product;

    @Mock
    private TargetProductDimensionsModel dimensions;

    @Mock
    private TargetCarrierModel carrier;

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private AddressModel address;

    @Mock
    private TargetVoucherService targetVoucherService;

    @Mock
    private CommonI18NService commonI18NService;

    @Mock
    private CurrencyModel baseCurrency;

    @Mock
    private TargetFulfilmentSalesApplicationConfigService salesApplicationConfigService;

    @Mock
    private ConsignmentExtractRecordModel consignmentExtractRecord;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        targetCustomerEmailResolutionService = Mockito.mock(TargetCustomerEmailResolutionService.class);
        carrier = Mockito.mock(TargetCarrierModel.class);
        consignment = Mockito.mock(TargetConsignmentModel.class);
        targetVoucherService = Mockito.mock(TargetVoucherService.class);
        when(consignment.getTargetCarrier()).thenReturn(carrier);
        when(carrier.getWarehouseCode()).thenReturn("APC");
        when(consignment.getOrder()).thenReturn(order);
        when(consignment.getCode()).thenReturn("testcode12334");
        when(order.getCode()).thenReturn("1234567890");

        final PurchaseOptionModel purchaseOptModel = Mockito.mock(PurchaseOptionModel.class);
        when(purchaseOptModel.getCode()).thenReturn("buynow");
        when(order.getPurchaseOption()).thenReturn(purchaseOptModel);

        final Set<ConsignmentEntryModel> entries = new HashSet<>();

        when(consignmentEntry.getOrderEntry()).thenReturn(entry);
        when(entry.getTotalPrice()).thenReturn(Double.valueOf(10.0));
        when(entry.getOrder()).thenReturn(order);
        when(order.getSubtotal()).thenReturn(Double.valueOf(20.0));
        when(consignmentEntry.getQuantity()).thenReturn(Long.valueOf(2));
        when(consignmentEntry.getConsignment()).thenReturn(consignment);
        when(Double.valueOf(currencyConversionFactorService
                .convertPriceToBaseCurrency(consignmentEntry.getOrderEntry().getTotalPrice().doubleValue(),
                        (OrderModel)consignmentEntry.getOrderEntry().getOrder()))).thenReturn(Double.valueOf(10.0));
        when(Double.valueOf(currencyConversionFactorService
                .convertPriceToBaseCurrency(consignmentEntry.getOrderEntry().getOrder().getSubtotal().doubleValue(),
                        (OrderModel)consignmentEntry.getOrderEntry().getOrder()))).thenReturn(Double.valueOf(20.0));
        entries.add(consignmentEntry);
        when(consignment.getConsignmentEntries()).thenReturn(entries);

        final ProductTypeModel prodTypeModel = Mockito.mock(ProductTypeModel.class);
        when(product.getCode()).thenReturn("P4000_red_M");
        when(product.getEan()).thenReturn("89475847");
        when(product.getName()).thenReturn("Red T-shirt");
        when(product.getProductType()).thenReturn(prodTypeModel);
        when(entry.getProduct()).thenReturn(product);

        final TargetCustomerModel customer = Mockito.mock(TargetCustomerModel.class);
        when(customer.getCustomerID()).thenReturn("Cust984723");
        when(order.getUser()).thenReturn(customer);

        address = Mockito.mock(AddressModel.class);
        when(address.getStreetname()).thenReturn("Victoria Street");
        when(address.getStreetnumber()).thenReturn("23");
        when(address.getFirstname()).thenReturn("John");
        when(address.getLastname()).thenReturn("Doe");
        when(address.getPostalcode()).thenReturn("3000");
        when(address.getDistrict()).thenReturn("VIC");
        when(address.getTown()).thenReturn("Melbourne");
        when(address.getPhone1()).thenReturn("+61498765432");
        when(targetCustomerEmailResolutionService.getEmailForCustomer(customer)).thenReturn("test@test.com");

        final CountryModel country = Mockito.mock(CountryModel.class);
        when(country.getName()).thenReturn("Australia");
        when(address.getCountry()).thenReturn(country);
        when(order.getDeliveryAddress()).thenReturn(address);
        when(order.getPaymentAddress()).thenReturn(address);
        when(order.getCncStoreNumber()).thenReturn(null);
        when(order.getDeliveryCost()).thenReturn(null);

        Mockito.doReturn(Boolean.TRUE).when(salesApplicationConfigService).isDenyShortPicks(SalesApplication.TRADEME);

        when(carrier.getServiceCode()).thenReturn("x");

        when(Boolean.valueOf(
                targetFeatureSwitchService.isFeatureEnabled(TgtjmsConstants.FEATURE_VALUES_IN_CONSIGNMENT_EXTRACT)))
                        .thenReturn(Boolean.TRUE);

        when(Boolean.valueOf(targetFeatureSwitchService
                .isFeatureEnabled(TgtjmsConstants.FEATURE_FASTLINE_ORDEREXTRACT_CONSIGNMENTID)))
                        .thenReturn(Boolean.FALSE);

        when(Boolean.valueOf(currencyConversionFactorService.canConvertToBaseCurrency(order)))
                .thenReturn(Boolean.TRUE);
    }

    @Test
    public void testToMessageNew() throws SAXException, IOException {
        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>N</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";



        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testToMessageUpdate() throws SAXException, IOException {
        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";



        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testToMessageCancel() throws SAXException, IOException {
        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.CANCELLED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>C</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";



        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testToMessageWithBulky1CarrierService() throws SAXException, IOException {
        when(carrier.getServiceCode()).thenReturn("v");
        when(carrier.getWarehouseCode()).thenReturn("TOLL");
        when(carrier.getTransformPhoneNumber()).thenReturn(true);
        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>0498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>TOLL</carrier><carrierService>v</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testToMessageWithItemPartOfFiredDeal() throws SAXException, IOException {
        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final ValueBundleDealModel valueBundleDeal = Mockito.mock(ValueBundleDealModel.class);
        final PromotionResultModel promoResult = Mockito.mock(PromotionResultModel.class);
        final PromotionOrderEntryConsumedModel promotionOrderEntryConsumed = Mockito
                .mock(PromotionOrderEntryConsumedModel.class);

        when(promotionOrderEntryConsumed.getOrderEntry()).thenReturn(entry);
        when(promoResult.getCertainty()).thenReturn(Float.valueOf(1.0f)); //fired
        when(promoResult.getPromotion()).thenReturn(valueBundleDeal); //Target Deal type
        when(promoResult.getConsumedEntries()).thenReturn(Collections.singleton(promotionOrderEntryConsumed));

        when(order.getAllPromotionResults()).thenReturn(Collections.singleton(promoResult));

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testToMessageWithItemPartOfPotentialDeal() throws SAXException, IOException {
        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final ValueBundleDealModel valueBundleDeal = Mockito.mock(ValueBundleDealModel.class);
        final PromotionResultModel promoResult = Mockito.mock(PromotionResultModel.class);
        final PromotionOrderEntryConsumedModel promotionOrderEntryConsumed = Mockito
                .mock(PromotionOrderEntryConsumedModel.class);

        when(promotionOrderEntryConsumed.getOrderEntry()).thenReturn(entry);
        when(promoResult.getCertainty()).thenReturn(Float.valueOf(0.7f)); //fired
        when(promoResult.getPromotion()).thenReturn(valueBundleDeal); //Target Deal type
        when(promoResult.getConsumedEntries()).thenReturn(Collections.singleton(promotionOrderEntryConsumed));

        when(order.getAllPromotionResults()).thenReturn(Collections.singleton(promoResult));

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testToMessageWithItemPartOfTMD() throws SAXException, IOException {
        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final TMDiscountPromotionModel tmDiscountPromotion = Mockito.mock(TMDiscountPromotionModel.class);
        final PromotionResultModel promoResult = Mockito.mock(PromotionResultModel.class);
        final PromotionOrderEntryConsumedModel promotionOrderEntryConsumed = Mockito
                .mock(PromotionOrderEntryConsumedModel.class);

        when(promotionOrderEntryConsumed.getOrderEntry()).thenReturn(entry);
        when(promoResult.getPromotion()).thenReturn(tmDiscountPromotion); //TMD
        when(promoResult.getConsumedEntries()).thenReturn(Collections.singleton(promotionOrderEntryConsumed));

        when(order.getAllPromotionResults()).thenReturn(Collections.singleton(promoResult));

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testToMessageWithItemPartOfTMDAndFiredDeal() throws SAXException, IOException {
        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final TMDiscountPromotionModel tmDiscountPromotion = Mockito.mock(TMDiscountPromotionModel.class);
        final PromotionResultModel tmdPromoResult = Mockito.mock(PromotionResultModel.class);
        final PromotionOrderEntryConsumedModel tmdPromotionOrderEntryConsumed = Mockito
                .mock(PromotionOrderEntryConsumedModel.class);
        when(tmdPromotionOrderEntryConsumed.getOrderEntry()).thenReturn(entry);
        when(tmdPromoResult.getPromotion()).thenReturn(tmDiscountPromotion); //TMD
        when(tmdPromoResult.getConsumedEntries()).thenReturn(Collections.singleton(tmdPromotionOrderEntryConsumed));

        final ValueBundleDealModel valueBundleDeal = Mockito.mock(ValueBundleDealModel.class);
        final PromotionResultModel dealPromoResult = Mockito.mock(PromotionResultModel.class);
        final PromotionOrderEntryConsumedModel dealOrderEntryConsumed = Mockito
                .mock(PromotionOrderEntryConsumedModel.class);
        when(dealOrderEntryConsumed.getOrderEntry()).thenReturn(entry);
        when(dealOrderEntryConsumed.getOrderEntry()).thenReturn(entry);
        when(dealPromoResult.getCertainty()).thenReturn(Float.valueOf(1.0f)); //fired
        when(dealPromoResult.getPromotion()).thenReturn(valueBundleDeal); //Target Deal type
        when(dealPromoResult.getConsumedEntries()).thenReturn(Collections.singleton(dealOrderEntryConsumed)); //Target Deal type

        final Set<PromotionResultModel> promoResults = new HashSet<>();
        promoResults.add(tmdPromoResult);
        promoResults.add(dealPromoResult);
        when(order.getAllPromotionResults()).thenReturn(promoResults);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetMessagePayloadWithShipPhonewithCountryCode() throws SAXException, IOException {
        when(carrier.getServiceCode()).thenReturn("v");
        when(carrier.getWarehouseCode()).thenReturn("TOLL");
        when(address.getPhone1()).thenReturn("+61498765432");
        when(carrier.getTransformPhoneNumber()).thenReturn(true);
        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>0498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>TOLL</carrier><carrierService>v</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetMessagePayloadWithShipPhonewithCountryCodeMinuPlus() throws SAXException, IOException {
        when(carrier.getServiceCode()).thenReturn("v");
        when(carrier.getWarehouseCode()).thenReturn("TOLL");
        when(carrier.getTransformPhoneNumber()).thenReturn(true);
        when(address.getPhone1()).thenReturn("61498765432");
        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>0498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>TOLL</carrier><carrierService>v</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
    }

    @Test
    public void testGetMessagePayloadWithShipPhoneWithLenghtGreaterThanTen() throws SAXException, IOException {
        when(address.getPhone1()).thenReturn("+61498760498765432");

        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498760498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetMessagePayloadWithShipPhoneWithSpaces() throws SAXException, IOException {
        when(carrier.getServiceCode()).thenReturn("v");
        when(carrier.getWarehouseCode()).thenReturn("TOLL");
        when(address.getPhone1()).thenReturn("+6149876  04987  65432");
        when(carrier.getTransformPhoneNumber()).thenReturn(true);

        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>0498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>TOLL</carrier><carrierService>v</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";
        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testGetMessagePayloadWithShipPhoneWithoutCountryCode() throws SAXException, IOException {
        when(address.getPhone1()).thenReturn("0498765432");

        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>0498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testToMessageWithBulky1AndOrderInstructionsCarrierService() throws SAXException, IOException {
        when(carrier.getServiceCode()).thenReturn("v");
        when(carrier.getWarehouseCode()).thenReturn("TOLL");

        when(carrier.getOrderInstructions1()).thenReturn("Order Instruction 1");
        when(carrier.getOrderInstructions2()).thenReturn("Order Instruction 2");
        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<orderInstructions1>Order Instruction 1</orderInstructions1>"
                + "<orderInstructions2>Order Instruction 2</orderInstructions2>"
                + "<carrier>TOLL</carrier><carrierService>v</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testGetMessagePayloadWithFlybuys() throws SAXException, IOException {
        final FlybuysDiscountModel flybuysDiscountModel = Mockito.mock(FlybuysDiscountModel.class);

        when(address.getPhone1()).thenReturn("+61498765432");
        when(targetVoucherService.getFlybuysDiscountForOrder(order)).thenReturn(flybuysDiscountModel);

        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testGetMessagePayloadWithoutFlybuys() throws SAXException, IOException {
        when(address.getPhone1()).thenReturn("+61498765432");
        when(targetVoucherService.getFlybuysDiscountForOrder(order)).thenReturn(null);

        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testGetMessagePayloadWithTradeMeOrderExtract() throws SAXException, IOException {
        when(order.getSalesApplication()).thenReturn(SalesApplication.TRADEME);

        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter
                .getMessagePayload(object,
                        targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED,
                        salesApplicationConfigService, currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>N</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testGetMessagePayloadWithEBayOrderExtract() throws SAXException, IOException {
        when(order.getSalesApplication()).thenReturn(SalesApplication.EBAY);

        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter
                .getMessagePayload(object,
                        targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED,
                        salesApplicationConfigService, currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testToMessageNoCurrencyConversionAvailable() throws SAXException, IOException {
        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testToMessageWithoutWeight() throws SAXException, IOException {
        when(product.getProductPackageDimensions()).thenReturn(null);

        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testToMessageWithZeroWeight() throws SAXException, IOException {
        when(dimensions.getWeight()).thenReturn(Double.valueOf(0));
        when(product.getProductPackageDimensions()).thenReturn(dimensions);

        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<itemWeight>0.0</itemWeight>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testToMessageWithNonZeroWeight() throws SAXException, IOException {
        when(dimensions.getWeight()).thenReturn(Double.valueOf(0.5));
        when(product.getProductPackageDimensions()).thenReturn(dimensions);

        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<itemWeight>0.5</itemWeight>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testGetMessagePayloadWhenNewOrderExtractIsSentWithConsgIdAsIdentifier()
            throws SAXException, IOException {
        when(order.getSalesApplication()).thenReturn(SalesApplication.WEB);

        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        when(Boolean.valueOf(targetFeatureSwitchService
                .isFeatureEnabled(TgtjmsConstants.FEATURE_FASTLINE_ORDEREXTRACT_CONSIGNMENTID)))
                        .thenReturn(Boolean.TRUE);

        final String msg = ConsignmentExtractMessageConverter
                .getMessagePayload(object,
                        targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW,
                        salesApplicationConfigService, currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>N</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>testcode12334</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
        verify(targetFeatureSwitchService).saveConsignmentExtractRecord(consignment, true);
    }

    @Test
    public void testGetMessagePayloadWhenNewOrderExtractIsSentWithOrderNumberAsIdentifier()
            throws SAXException, IOException {
        when(order.getSalesApplication()).thenReturn(SalesApplication.WEB);

        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter
                .getMessagePayload(object,
                        targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW,
                        salesApplicationConfigService, currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>N</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
        verify(targetFeatureSwitchService).saveConsignmentExtractRecord(consignment, false);
    }

    @Test
    public void testGetMessagePayloadWhenUpdatedOrderExtractIsSentWithOrderNumberAsIdentifierDuringTransition()
            throws SAXException, IOException {
        when(order.getSalesApplication()).thenReturn(SalesApplication.WEB);

        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        when(targetFeatureSwitchService.getConsExtractRecordByConsignment(consignment)).thenReturn(null);

        final String msg = ConsignmentExtractMessageConverter
                .getMessagePayload(object,
                        targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED,
                        salesApplicationConfigService, currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testGetMessagePayloadWhenUpdatedOrderExtractIsSentWithConsgIdAsIdentifier()
            throws SAXException, IOException {
        when(order.getSalesApplication()).thenReturn(SalesApplication.WEB);

        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        when(targetFeatureSwitchService.getConsExtractRecordByConsignment(consignment))
                .thenReturn(consignmentExtractRecord);
        when(consignmentExtractRecord.getConsignmentAsId()).thenReturn(Boolean.TRUE);

        final String msg = ConsignmentExtractMessageConverter
                .getMessagePayload(object,
                        targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED,
                        salesApplicationConfigService, currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>testcode12334</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testGetMessagePayloadWhenUpdatedOrderExtractIsSentWithOrderNumberAsIdentifier()
            throws SAXException, IOException {
        when(order.getSalesApplication()).thenReturn(SalesApplication.WEB);

        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        when(targetFeatureSwitchService.getConsExtractRecordByConsignment(consignment))
                .thenReturn(consignmentExtractRecord);
        when(consignmentExtractRecord.getConsignmentAsId()).thenReturn(Boolean.FALSE);

        final String msg = ConsignmentExtractMessageConverter
                .getMessagePayload(object,
                        targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED,
                        salesApplicationConfigService, currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testGetMessagePayloadWhenOrderExtractIsResentWithOrderNumberAsIdentifier()
            throws SAXException, IOException {
        when(order.getSalesApplication()).thenReturn(SalesApplication.WEB);

        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        when(targetFeatureSwitchService.getConsExtractRecordByConsignment(consignment))
                .thenReturn(consignmentExtractRecord);
        when(consignmentExtractRecord.getConsignmentAsId()).thenReturn(Boolean.FALSE);

        final String msg = ConsignmentExtractMessageConverter
                .getMessagePayload(object,
                        targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW,
                        salesApplicationConfigService, currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>N</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testGetMessagePayloadWhenOrderExtractIsResentWithConsgIdAsIdentifier()
            throws SAXException, IOException {
        when(order.getSalesApplication()).thenReturn(SalesApplication.WEB);

        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        when(targetFeatureSwitchService.getConsExtractRecordByConsignment(consignment))
                .thenReturn(consignmentExtractRecord);
        when(consignmentExtractRecord.getConsignmentAsId()).thenReturn(Boolean.TRUE);

        final String msg = ConsignmentExtractMessageConverter
                .getMessagePayload(object,
                        targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW,
                        salesApplicationConfigService, currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>N</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>testcode12334</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testToMessageForDeliveryFee() throws SAXException, IOException {
        final Double deliveryCost = Double.valueOf(9);
        when(order.getDeliveryCost()).thenReturn(deliveryCost);
        when(Double.valueOf(currencyConversionFactorService
                .convertPriceToBaseCurrency(deliveryCost.doubleValue(),
                        order))).thenReturn(Double.valueOf(9.0));
        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "<deliveryFee>9.0</deliveryFee>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testToMessageForZeroDeliveryFee() throws SAXException, IOException {
        when(order.getDeliveryCost()).thenReturn(Double.valueOf(0));

        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "<deliveryFee>0.0</deliveryFee>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testToMessageWithoutDeliveryFee() throws SAXException, IOException {
        when(order.getDeliveryCost()).thenReturn(null);

        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(
                consignment, targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);

        final String msg = ConsignmentExtractMessageConverter.getMessagePayload(object,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.UPDATED, null,
                currencyConversionFactorService, targetFeatureSwitchService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<integration-orderExtractList><orderExtract><orderNumber>1234567890</orderNumber>"
                + "<customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName>"
                + "<shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1>"
                + "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>"
                + "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>"
                + "<shipToPhone>+61498765432</shipToPhone><shipToEmail>test@test.com</shipToEmail>"
                + "<shipToPostCode>3000</shipToPostCode><orderLineNum>1</orderLineNum>"
                + "<itemEAN>89475847</itemEAN><itemNumber>P4000_red_M</itemNumber>"
                + "<itemDescription1>RED T-SHIRT</itemDescription1><quantityOrdered>2</quantityOrdered>"
                + "<carrier>APC</carrier><carrierService>x</carrierService><orderPurpose>U</orderPurpose>"
                + "<orderType>B1</orderType><allowShortPick>Y</allowShortPick>"
                + "<itemAmount>10.0</itemAmount><orderGoodsAmount>20.0</orderGoodsAmount>"
                + "<consignmentId>1234567890</consignmentId>"
                + "</orderExtract></integration-orderExtractList>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }
}
