/**
 * 
 */
package au.com.target.tgtjms.jms.service.impl;

import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import au.com.target.tgtsale.integration.dto.TargetLaybyOrder;
import au.com.target.tgtsale.integration.exception.UnsuccessfulESBOperationException;
import au.com.target.tgtsale.pos.service.TargetGetPosLaybyOrderService;


/**
 * this class is not to be included in the test suite as it requires Activemq
 * 
 * @author rsamuel3
 * 
 */
@Ignore
public class IntegrationTestGetPosLaybyOrderService extends ServicelayerTransactionalTest {

    @Resource
    private TargetGetPosLaybyOrderService targetGetPosLaybyOrderService;

    @Test
    public void testRetrieveLaybyOrderFromPos() throws UnsuccessfulESBOperationException {
        final TargetLaybyOrder order = targetGetPosLaybyOrderService.retrieveLaybyOrderFromPos("00008001");
        Assert.assertNotNull(order);
    }
}
