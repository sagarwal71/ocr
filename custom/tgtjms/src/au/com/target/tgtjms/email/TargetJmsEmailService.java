/**
 * 
 */
package au.com.target.tgtjms.email;

import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.servicelayer.media.MediaService;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import au.com.target.tgtbusproc.email.EmailServiceException;
import au.com.target.tgtbusproc.email.SendHybrisEmailService;
import au.com.target.tgtjms.jms.gateway.JmsMessageDispatcher;
import au.com.target.tgtmail.converter.EmailMessageConverter;
import au.com.target.tgtmail.model.Email;


/**
 * @author Olivier Lamy
 */
public class TargetJmsEmailService implements SendHybrisEmailService {

    private static final Logger LOG = Logger.getLogger(TargetJmsEmailService.class);

    @Autowired
    private EmailMessageConverter emailMessageConverter;

    @Autowired
    private MediaService mediaService;

    @Autowired
    private JmsMessageDispatcher jmsMessageDispatcher;

    private String jmsEmailQueueName;

    /**
     * @see au.com.target.tgtbusproc.email.SendHybrisEmailService#sendEmail(EmailMessageModel )
     */
    @Override
    public void sendEmail(final EmailMessageModel emailMessageModel) throws EmailServiceException {
        try {

            final Email email = buildEmailFromHybrisModel(emailMessageModel);

            final String xmlMessage = emailMessageConverter.convert(email);

            jmsMessageDispatcher.send(this.jmsEmailQueueName, xmlMessage);

            LOG.info("Email sent");

        }
        catch (final JAXBException | IOException e) {
            LOG.info("Failed to send email: " + e.getMessage(), e);
            throw new EmailServiceException(e.getMessage(), e);
        }
    }


    protected Email buildEmailFromHybrisModel(final EmailMessageModel emailMessageModel) throws IOException {
        final Email email = new Email.Builder().withEmailMessageModel(emailMessageModel).withMediaService(mediaService)
                .build();
        return email;
    }


    public String getJmsEmailQueueName() {
        return jmsEmailQueueName;
    }


    public void setJmsEmailQueueName(final String jmsEmailQueueName) {
        this.jmsEmailQueueName = jmsEmailQueueName;
    }


    public void setJmsMessageDispatcher(final JmsMessageDispatcher jmsMessageDispatcher) {
        this.jmsMessageDispatcher = jmsMessageDispatcher;
    }




}
