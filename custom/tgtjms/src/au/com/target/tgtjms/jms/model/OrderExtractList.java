/**
 * 
 */
package au.com.target.tgtjms.jms.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "integration-orderExtractList")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderExtractList implements Serializable
{

    private static final long serialVersionUID = 1L;

    @XmlElement(name = "orderExtract")
    private List<ConsignmentExtract> orderExtracts = new ArrayList<>();

    /**
     * @return the orderExtracts
     */
    public List<ConsignmentExtract> getOrderExtracts()
    {
        return orderExtracts;
    }

    /**
     * @param orderExtracts
     *            the orderExtracts to set
     */
    public void setOrderExtracts(final List<ConsignmentExtract> orderExtracts)
    {
        this.orderExtracts = orderExtracts;
    }

    public void addOrderExtract(final ConsignmentExtract orderExtract)
    {
        if (orderExtracts == null)
        {
            orderExtracts = new ArrayList<>();
        }
        orderExtracts.add(orderExtract);
    }

}