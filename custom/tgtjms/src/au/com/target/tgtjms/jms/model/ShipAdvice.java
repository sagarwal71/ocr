/**
 * 
 */
package au.com.target.tgtjms.jms.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "integration-shipAdvice")
@XmlAccessorType(XmlAccessType.FIELD)
public class ShipAdvice implements Serializable
{

    private static final long serialVersionUID = 1L;

    @XmlElement
    private String orderNumber;

    @XmlElement
    private Date deliveryDate;

    @XmlElement
    private String customerCode;

    @XmlElement
    private String shipToLocName;

    @XmlElement
    private String shipToLocAddr1;

    @XmlElement
    private String shipToLocAddr2;

    @XmlElement
    private String shipToCity;

    @XmlElement
    private String shipToState;

    @XmlElement
    private String shipToCountry;

    @XmlElement
    private String shipToPostCode;

    @XmlElement
    private String shipToPhone;

    @XmlElement
    private String shipToEmail;

    public String getOrderNumber()
    {
        return orderNumber;
    }

    public void setOrderNumber(final String orderNumber)
    {
        this.orderNumber = orderNumber;
    }

    public Date getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setDeliveryDate(final Date deliveryDate)
    {
        this.deliveryDate = deliveryDate;
    }

    public String getCustomerCode()
    {
        return customerCode;
    }

    public void setCustomerCode(final String customerCode)
    {
        this.customerCode = customerCode;
    }

    public String getShipToLocName()
    {
        return shipToLocName;
    }

    public void setShipToLocName(final String shipToLocName)
    {
        this.shipToLocName = shipToLocName;
    }

    public String getShipToLocAddr1()
    {
        return shipToLocAddr1;
    }

    public void setShipToLocAddr1(final String shipToLocAddr1)
    {
        this.shipToLocAddr1 = shipToLocAddr1;
    }

    public String getShipToLocAddr2()
    {
        return shipToLocAddr2;
    }

    public void setShipToLocAddr2(final String shipToLocAddr2)
    {
        this.shipToLocAddr2 = shipToLocAddr2;
    }

    public String getShipToCity()
    {
        return shipToCity;
    }

    public void setShipToCity(final String shipToCity)
    {
        this.shipToCity = shipToCity;
    }

    public String getShipToState()
    {
        return shipToState;
    }

    public void setShipToState(final String shipToState)
    {
        this.shipToState = shipToState;
    }

    public String getShipToCountry()
    {
        return shipToCountry;
    }

    public void setShipToCountry(final String shipToCountry)
    {
        this.shipToCountry = shipToCountry;
    }

    public String getShipToPostCode()
    {
        return shipToPostCode;
    }

    public void setShipToPostCode(final String shipToPostCode)
    {
        this.shipToPostCode = shipToPostCode;
    }

    /**
     * @return the shipToPhone
     */
    public String getShipToPhone() {
        return shipToPhone;
    }

    /**
     * @param shipToPhone
     *            the shipToPhone to set
     */
    public void setShipToPhone(final String shipToPhone) {
        this.shipToPhone = shipToPhone;
    }

    /**
     * @return the shipToEmail
     */
    public String getShipToEmail() {
        return shipToEmail;
    }

    /**
     * @param shipToEmail
     *            the shipToEmail to set
     */
    public void setShipToEmail(final String shipToEmail) {
        this.shipToEmail = shipToEmail;
    }

}