/**
 * 
 */
package au.com.target.tgtjms.jms.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author mmaki
 * 
 */
@XmlRootElement(name = "integration-eNewsSubscription")
@XmlAccessorType(XmlAccessType.FIELD)
public class ENewsSubscription implements Serializable {

    private static final long serialVersionUID = 1L;

    @XmlElement
    private String documentId;

    @XmlElement
    private String accountNumber;

    @XmlElement
    private String customerEmail;

    @XmlElement
    private String firstName;

    @XmlElement
    private String lastName;

    @XmlElement
    private String title;

    @XmlElement
    private String address;

    @XmlElement
    private String suburb;

    @XmlElement
    private String state;

    @XmlElement
    private String postCode;

    @XmlElement
    private Date dateOfBirth;

    @XmlElement
    private String gender;

    @XmlElement
    private String mobilePhone;

    @XmlElement
    private String smsAlerts;

    @XmlElement
    private String womenswear;

    @XmlElement
    private String books;

    @XmlElement
    private String footwear;

    @XmlElement
    private String menswear;

    @XmlElement
    private String dft;

    @XmlElement
    private String toys;

    @XmlElement
    private String underwear;

    @XmlElement
    private String freeFusion;

    @XmlElement
    private String musicMovies;

    @XmlElement
    private String moda;

    @XmlElement
    private String baby;

    @XmlElement
    private String mrbig;

    @XmlElement
    private String electricalAppliances;

    @XmlElement
    private String kidswear;

    @XmlElement
    private String cosmeticsFragrances;

    @XmlElement
    private String technoGames;

    @XmlElement
    private String homewares;

    @XmlElement
    private String noEntry; //competitions

    @XmlElement
    private String importedFrom;

    @XmlElement
    private String targetDealOfTheDay;

    @XmlElement
    private String flybuys;

    @XmlElement
    private String emailOptin;

    /**
     * @return the documentId
     */
    public String getDocumentId() {
        return documentId;
    }

    /**
     * @param documentId
     *            the documentId to set
     */
    public void setDocumentId(final String documentId) {
        this.documentId = documentId;
    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * @param accountNumber
     *            the accountNumber to set
     */
    public void setAccountNumber(final String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * @return the customerEmail
     */
    public String getCustomerEmail() {
        return customerEmail;
    }

    /**
     * @param customerEmail
     *            the customerEmail to set
     */
    public void setCustomerEmail(final String customerEmail) {
        this.customerEmail = customerEmail;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address
     *            the address to set
     */
    public void setAddress(final String address) {
        this.address = address;
    }

    /**
     * @return the suburb
     */
    public String getSuburb() {
        return suburb;
    }

    /**
     * @param suburb
     *            the suburb to set
     */
    public void setSuburb(final String suburb) {
        this.suburb = suburb;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(final String state) {
        this.state = state;
    }

    /**
     * @return the postCode
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * @param postCode
     *            the postCode to set
     */
    public void setPostCode(final String postCode) {
        this.postCode = postCode;
    }

    /**
     * @return the dateOfBirth
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth
     *            the dateOfBirth to set
     */
    public void setDateOfBirth(final Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender
     *            the gender to set
     */
    public void setGender(final String gender) {
        this.gender = gender;
    }

    /**
     * @return the mobilePhone
     */
    public String getMobilePhone() {
        return mobilePhone;
    }

    /**
     * @param mobilePhone
     *            the mobilePhone to set
     */
    public void setMobilePhone(final String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    /**
     * @return the smsAlerts
     */
    public String getSmsAlerts() {
        return smsAlerts;
    }

    /**
     * @param smsAlerts
     *            the smsAlerts to set
     */
    public void setSmsAlerts(final String smsAlerts) {
        this.smsAlerts = smsAlerts;
    }

    /**
     * @return the womenswear
     */
    public String getWomenswear() {
        return womenswear;
    }

    /**
     * @param womenswear
     *            the womenswear to set
     */
    public void setWomenswear(final String womenswear) {
        this.womenswear = womenswear;
    }

    /**
     * @return the books
     */
    public String getBooks() {
        return books;
    }

    /**
     * @param books
     *            the books to set
     */
    public void setBooks(final String books) {
        this.books = books;
    }

    /**
     * @return the footwear
     */
    public String getFootwear() {
        return footwear;
    }

    /**
     * @param footwear
     *            the footwear to set
     */
    public void setFootwear(final String footwear) {
        this.footwear = footwear;
    }

    /**
     * @return the menswear
     */
    public String getMenswear() {
        return menswear;
    }

    /**
     * @param menswear
     *            the menswear to set
     */
    public void setMenswear(final String menswear) {
        this.menswear = menswear;
    }

    /**
     * @return the dft
     */
    public String getDft() {
        return dft;
    }

    /**
     * @param dft
     *            the dft to set
     */
    public void setDft(final String dft) {
        this.dft = dft;
    }

    /**
     * @return the toys
     */
    public String getToys() {
        return toys;
    }

    /**
     * @param toys
     *            the toys to set
     */
    public void setToys(final String toys) {
        this.toys = toys;
    }

    /**
     * @return the underwear
     */
    public String getUnderwear() {
        return underwear;
    }

    /**
     * @param underwear
     *            the underwear to set
     */
    public void setUnderwear(final String underwear) {
        this.underwear = underwear;
    }

    /**
     * @return the freeFusion
     */
    public String getFreeFusion() {
        return freeFusion;
    }

    /**
     * @param freeFusion
     *            the freeFusion to set
     */
    public void setFreeFusion(final String freeFusion) {
        this.freeFusion = freeFusion;
    }

    /**
     * @return the musicMovies
     */
    public String getMusicMovies() {
        return musicMovies;
    }

    /**
     * @param musicMovies
     *            the musicMovies to set
     */
    public void setMusicMovies(final String musicMovies) {
        this.musicMovies = musicMovies;
    }

    /**
     * @return the moda
     */
    public String getModa() {
        return moda;
    }

    /**
     * @param moda
     *            the moda to set
     */
    public void setModa(final String moda) {
        this.moda = moda;
    }

    /**
     * @return the baby
     */
    public String getBaby() {
        return baby;
    }

    /**
     * @param baby
     *            the baby to set
     */
    public void setBaby(final String baby) {
        this.baby = baby;
    }

    /**
     * @return the mrbig
     */
    public String getMrbig() {
        return mrbig;
    }

    /**
     * @param mrbig
     *            the mrbig to set
     */
    public void setMrbig(final String mrbig) {
        this.mrbig = mrbig;
    }

    /**
     * @return the electricalAppliances
     */
    public String getElectricalAppliances() {
        return electricalAppliances;
    }

    /**
     * @param electricalAppliances
     *            the electricalAppliances to set
     */
    public void setElectricalAppliances(final String electricalAppliances) {
        this.electricalAppliances = electricalAppliances;
    }

    /**
     * @return the kidswear
     */
    public String getKidswear() {
        return kidswear;
    }

    /**
     * @param kidswear
     *            the kidswear to set
     */
    public void setKidswear(final String kidswear) {
        this.kidswear = kidswear;
    }

    /**
     * @return the cosmeticsFragrances
     */
    public String getCosmeticsFragrances() {
        return cosmeticsFragrances;
    }

    /**
     * @param cosmeticsFragrances
     *            the cosmeticsFragrances to set
     */
    public void setCosmeticsFragrances(final String cosmeticsFragrances) {
        this.cosmeticsFragrances = cosmeticsFragrances;
    }

    /**
     * @return the technoGames
     */
    public String getTechnoGames() {
        return technoGames;
    }

    /**
     * @param technoGames
     *            the technoGames to set
     */
    public void setTechnoGames(final String technoGames) {
        this.technoGames = technoGames;
    }

    /**
     * @return the homewares
     */
    public String getHomewares() {
        return homewares;
    }

    /**
     * @param homewares
     *            the homewares to set
     */
    public void setHomewares(final String homewares) {
        this.homewares = homewares;
    }

    /**
     * @return the noEntry
     */
    public String getNoEntry() {
        return noEntry;
    }

    /**
     * @param noEntry
     *            the noEntry to set
     */
    public void setNoEntry(final String noEntry) {
        this.noEntry = noEntry;
    }

    /**
     * @return the importedFrom
     */
    public String getImportedFrom() {
        return importedFrom;
    }

    /**
     * @param importedFrom
     *            the importedFrom to set
     */
    public void setImportedFrom(final String importedFrom) {
        this.importedFrom = importedFrom;
    }

    /**
     * @return the targetDealOfTheDay
     */
    public String getTargetDealOfTheDay() {
        return targetDealOfTheDay;
    }

    /**
     * @param targetDealOfTheDay
     *            the targetDealOfTheDay to set
     */
    public void setTargetDealOfTheDay(final String targetDealOfTheDay) {
        this.targetDealOfTheDay = targetDealOfTheDay;
    }

    /**
     * @return the flybuys
     */
    public String getFlybuys() {
        return flybuys;
    }

    /**
     * @param flybuys
     *            the flybuys to set
     */
    public void setFlybuys(final String flybuys) {
        this.flybuys = flybuys;
    }

    /**
     * @return the emailOptin
     */
    public String getEmailOptin() {
        return emailOptin;
    }

    /**
     * @param emailOptin
     *            the emailOptin to set
     */
    public void setEmailOptin(final String emailOptin) {
        this.emailOptin = emailOptin;
    }


}
