/**
 * 
 */
package au.com.target.tgtjms.jms.converter;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtjms.constants.TgtjmsConstants;


/**
 * @author rsamuel3
 * 
 */
public final class ConverterUtil {
    private ConverterUtil() {
        //prevent construction
    }


    /**
     * Formats the addressLine1 for a Target/Target COuntry store address (for CnC orders) and appends
     * {@link TgtjmsConstants#CNC_STORE_PREFIX} at the start.
     * 
     * @param addresssLine1
     * @param storeNumber
     * @return Uppercase address, with "TARGET-" prefix, if the storeNumber is NOT NULL
     */
    public static String formatAddressLine(final String addresssLine1, final Integer storeNumber)
    {
        if (StringUtils.isBlank(addresssLine1))
        {
            return null;
        }

        return (storeNumber != null)
                ? (TgtjmsConstants.CNC_STORE_PREFIX + "-" + addresssLine1).toUpperCase()
                : addresssLine1.toUpperCase();
    }
}
