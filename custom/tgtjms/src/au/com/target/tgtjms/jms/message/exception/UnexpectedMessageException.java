/**
 * 
 */
package au.com.target.tgtjms.jms.message.exception;


/**
 * Exception thrown when creation of the message was unsuccessful.
 * 
 * @author rsamuel3
 * 
 */
public class UnexpectedMessageException extends Exception {
    public UnexpectedMessageException(final String message, final Throwable e) {
        super(message, e);
    }
}
