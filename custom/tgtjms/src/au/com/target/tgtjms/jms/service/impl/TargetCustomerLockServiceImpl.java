/**
 * 
 */
package au.com.target.tgtjms.jms.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtjms.jms.converter.LockStatusMessageConverter;
import au.com.target.tgtjms.jms.gateway.JmsMessageDispatcher;
import au.com.target.tgtjms.jms.model.LockEnum;
import au.com.target.tgtjms.jms.model.LockStatus;
import au.com.target.tgtjms.util.JmsMessageHelper;
import au.com.target.tgtsale.pos.service.TargetCustomerLockService;


/**
 * Default implementation for {@link TargetCustomerLockService}.
 * 
 * @author mmaki
 */
public class TargetCustomerLockServiceImpl implements TargetCustomerLockService
{

    private static final Logger LOG = Logger.getLogger(TargetCustomerLockServiceImpl.class);

    private LockStatusMessageConverter lockStatusMessageConverter;
    private JmsMessageDispatcher messageDispatcher;

    private String incomingQueue;
    private String outgoingQueue;


    @Override
    public boolean posLockDirect(final OrderModel order) {
        final LockStatus status = new LockStatus();
        status.setOrderCode(JmsMessageHelper.getLaybyNumForPos(order.getCode()));
        status.setLockMode(LockEnum.DIRECT);
        return processLockOperation(order, status);
    }

    @Override
    public boolean posUnLockDirect(final OrderModel order) {
        final LockStatus status = new LockStatus();
        status.setOrderCode(JmsMessageHelper.getLaybyNumForPos(order.getCode()));
        status.setLockMode(LockEnum.CLEAR);
        return processLockOperation(order, status);
    }

    @Override
    public boolean convertLockIndirect(final OrderModel order) {
        final LockStatus status = new LockStatus();
        status.setOrderCode(JmsMessageHelper.getLaybyNumForPos(order.getCode()));
        status.setLockMode(LockEnum.INDIRECT);
        return processLockOperation(order, status);
    }

    /**
     * Sends lock request to ESB and returns {@code true} if operation succeeded.
     * 
     * @param order
     *            the order to extract user from and send lock for
     * @param status
     *            the lock DTO
     * @return {@code true} if lock operation was successful, {@code false} otherwise
     */
    public boolean processLockOperation(final OrderModel order, final LockStatus status) {
        final UserModel user = order.getUser();
        if (user instanceof TargetCustomerModel) {
            final TargetCustomerModel customer = (TargetCustomerModel)user;
            status.setCustomerCode(customer.getCustomerID());
            try {
                final String text = getLockStatusMessageConverter().getMessagePayload(status);
                final Message msg = getMessageDispatcher().sendAndReceive(getOutgoingQueue(), text, getIncomingQueue());
                return isOperationSuccessful(msg);
            }
            catch (final JAXBException e) {
                LOG.error("ERR-LOCKORDER-JAXBEXCEPTION : "
                        + "exception thrown when trying to marshall the lock object", e);
            }
            catch (final JMSException e) {
                LOG.error("ERR-LOCKORDER-JMSEXCEPTION : "
                        + "exception thrown when trying to marshall the lock object", e);
            }
        }
        return false;
    }


    /**
     * Checks whether the returned message indicates an operation success or failure.
     * 
     * @param message
     *            the response to check
     * @return {@code true} if response indicates successful operation, {@code false} otherwise
     * @throws JMSException
     *             if an error occurs during message read process
     */
    private boolean isOperationSuccessful(final Message message) throws JMSException {
        boolean successful = false;
        if (message instanceof TextMessage) {
            final String response = ((TextMessage)message).getText();
            LOG.info("Response received: " + response);
            if (response != null && response.contains("success")) {
                LOG.info("INFO-LOCKORDER-SUCCESS : with the message " + response);
                successful = true;
            }
            else {
                LOG.error("ERR-LOCKORDER-FAILED : with the message " + response);
            }
        }
        return successful;
    }

    /**
     * Returns the lock message converter.
     * 
     * @return the lock message converter
     */
    public LockStatusMessageConverter getLockStatusMessageConverter() {
        return lockStatusMessageConverter;
    }

    /**
     * @param lockStatusMessageConverter
     *            the lockStatusMessageConverter to set
     */
    public void setLockStatusMessageConverter(final LockStatusMessageConverter lockStatusMessageConverter) {
        this.lockStatusMessageConverter = lockStatusMessageConverter;
    }

    /**
     * Returns the message dispatcher.
     * 
     * @return the message dispatcher
     */
    public JmsMessageDispatcher getMessageDispatcher() {
        return messageDispatcher;
    }

    /**
     * Sets the message dispatcher.
     * 
     * @param messageDispatcher
     *            the message dispatcher to set
     */
    public void setMessageDispatcher(final JmsMessageDispatcher messageDispatcher) {
        this.messageDispatcher = messageDispatcher;
    }

    /**
     * Returns the incoming queue.
     * 
     * @return the incoming queue
     */
    public String getIncomingQueue() {
        return incomingQueue;
    }

    /**
     * Sets the incoming queue.
     * 
     * @param incomingQueue
     *            the incoming queue to set
     */
    public void setIncomingQueue(final String incomingQueue) {
        this.incomingQueue = incomingQueue;
    }

    /**
     * Returns the outgoing queue.
     * 
     * @return the outgoing queue
     */
    public String getOutgoingQueue() {
        return outgoingQueue;
    }

    /**
     * Sets the outgoing queue.
     * 
     * @param outgoingQueue
     *            the outgoing queue to set
     */
    public void setOutgoingQueue(final String outgoingQueue) {
        this.outgoingQueue = outgoingQueue;
    }
}
