/**
 * 
 */
package au.com.target.tgtjms.jms.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author rsamuel3
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LockStatus {
    @XmlElement
    private String orderCode;
    @XmlElement
    private String customerCode;
    @XmlElement
    private LockEnum lockMode;

    /**
     * 
     */
    public LockStatus() {
        // TODO Auto-generated constructor stub
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(final String orderCode) {
        this.orderCode = orderCode;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(final String customerCode) {
        this.customerCode = customerCode;
    }

    public LockEnum getLockMode() {
        return lockMode;
    }

    public void setLockMode(final LockEnum lockMode) {
        this.lockMode = lockMode;
    }

}
