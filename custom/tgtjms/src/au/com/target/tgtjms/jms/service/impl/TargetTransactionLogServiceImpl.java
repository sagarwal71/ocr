/**
 * 
 */
package au.com.target.tgtjms.jms.service.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.returns.model.OrderReturnRecordEntryModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.user.UserService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.jms.JmsException;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.currency.conversion.service.CurrencyConversionFactorService;
import au.com.target.tgtcore.order.TargetDiscountService;
import au.com.target.tgtcore.order.data.AppliedFlybuysDiscountData;
import au.com.target.tgtcore.order.data.AppliedVoucherData;
import au.com.target.tgtjms.jms.converter.TLOGMessageConverter;
import au.com.target.tgtjms.jms.gateway.JmsMessageDispatcher;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtpayment.util.PaymentTransactionHelper;
import au.com.target.tgtsale.tlog.TargetTransactionLogService;
import au.com.target.tgtsale.tlog.converter.TlogConverter;
import au.com.target.tgtsale.tlog.data.DealDetails;
import au.com.target.tgtsale.tlog.data.DealResults;
import au.com.target.tgtsale.tlog.data.DiscountInfo;
import au.com.target.tgtsale.tlog.data.DiscountStyleEnum;
import au.com.target.tgtsale.tlog.data.DiscountTypeEnum;
import au.com.target.tgtsale.tlog.data.Flybuys;
import au.com.target.tgtsale.tlog.data.ItemInfo;
import au.com.target.tgtsale.tlog.data.Payment;
import au.com.target.tgtsale.tlog.data.TLog;
import au.com.target.tgtsale.tlog.data.TenderInfo;
import au.com.target.tgtsale.tlog.data.TenderTypeEnum;
import au.com.target.tgtsale.tlog.data.Transaction;
import au.com.target.tgtsale.tlog.data.TransactionDeal;
import au.com.target.tgtsale.tlog.data.TransactionTypeEnum;
import au.com.target.tgtsale.tlog.data.Version;
import au.com.target.tgtsale.tlog.exception.PublishTlogFailedException;
import au.com.target.tgtutility.logging.JaxbLogger;


/**
 * @author vmuthura
 * 
 */
public class TargetTransactionLogServiceImpl extends AbstractBusinessService implements TargetTransactionLogService {
    private static final Logger LOG = Logger.getLogger(TargetTransactionLogServiceImpl.class);

    private JmsMessageDispatcher messageDispatcher;
    private String queueName;
    private String fileVersionNumber;
    private TLOGMessageConverter tlogMessageConverter;
    private TargetDiscountService targetDiscountService;
    private UserService userService;
    private ConfigurationService configurationService;
    private PromotionsService promotionsService;
    private TlogConverter tlogConverter;
    private Map<String, String> deals;

    private CurrencyConversionFactorService currencyConversionFactorService;

    /**
     * 
     * @param messageDispatcher
     *            the message dispatcher
     */
    @Required
    public void setMessageDispatcher(final JmsMessageDispatcher messageDispatcher) {
        this.messageDispatcher = messageDispatcher;
    }

    /**
     * 
     * @param queueName
     *            the TLog message queue name
     */
    @Required
    public void setQueueName(final String queueName) {
        this.queueName = queueName;
    }

    /**
     * @param fileVersionNumber
     *            the fileVersionNumber to set
     */
    @Required
    public void setFileVersionNumber(final String fileVersionNumber) {
        this.fileVersionNumber = fileVersionNumber;
    }

    /**
     * @param tlogMessageConverter
     *            the tlogMessageConverter to set
     */
    @Required
    public void setTlogMessageConverter(final TLOGMessageConverter tlogMessageConverter) {
        this.tlogMessageConverter = tlogMessageConverter;
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }


    /**
     * @param configurationService
     *            the configurationService to set
     */
    @Required
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    /**
     * @param targetDiscountService
     *            the targetDiscountService to set
     */
    @Required
    public void setTargetDiscountService(final TargetDiscountService targetDiscountService) {
        this.targetDiscountService = targetDiscountService;
    }

    /**
     * @param currencyConversionFactorService
     *            the currencyConversionFactorService to set
     */
    @Required
    public void setCurrencyConversionFactorService(
            final CurrencyConversionFactorService currencyConversionFactorService) {
        this.currencyConversionFactorService = currencyConversionFactorService;
    }

    @Override
    public void publishTlogForOrderSale(final OrderModel orderModel) {
        if (orderModel == null) {
            throw new IllegalArgumentException("Order is null");
        }
        final PurchaseOptionConfigModel purchaseOptionConfig = orderModel.getPurchaseOptionConfig();
        final Boolean isPreOrder = orderModel.getContainsPreOrderItems();
        final UserModel userModel = orderModel.getUser();
        final PromotionOrderResults promotionOrderResults = getPromotionsService().getPromotionResults(orderModel);
        final Transaction transaction = new Transaction();
        tlogConverter.addTransactionType(purchaseOptionConfig, transaction, isPreOrder);
        transaction.setTimestamp(orderModel.getDate());
        if (null != orderModel.getCurrency()) {
            if (SalesApplication.TRADEME.equals(orderModel.getSalesApplication())) {
                transaction.setCurrency(TgtCoreConstants.AUSTRALIAN_DOLLARS);
            }
            else {
                transaction.setCurrency(orderModel.getCurrency().getIsocode());
            }
        }

        if (null != orderModel.getSalesApplication()) {
            transaction.setSalesChannel(orderModel.getSalesApplication().toString());
        }
        transaction.setOrder(tlogConverter.getOrderInfoFromOrderModel(orderModel,
                userModel != null ? userModel.getDisplayName() : null));

        final DealResults dealResults = tlogConverter.getDetailsFromOrderEntriesNPromotions(orderModel,
                promotionOrderResults, getDeals(), targetDiscountService.getTotalTMDiscount(orderModel));
        final List<ItemInfo> itemListFromOrderEntries = (dealResults != null && CollectionUtils.isNotEmpty(dealResults
                .getItems())) ? dealResults.getItems()
                        : null;
        transaction.setItems(itemListFromOrderEntries);

        //Deals will not be available for pre order products
        if (!BooleanUtils.isTrue(isPreOrder)) {
            transaction.setTransactionDeal(getTransactionDeals(dealResults));
        }
        // This adds TMD and voucher discounts to the data
        addTransactionDiscounts(orderModel, promotionOrderResults, transaction, isPreOrder);

        transaction.setFlybuys(new Flybuys(orderModel.getFlyBuysCode()));
        final double baseCurrencyDeliveryCost = currencyConversionFactorService.convertPriceToBaseCurrency(orderModel
                .getDeliveryCost().doubleValue(), orderModel);
        transaction.setShippingInfo(tlogConverter.getShippingInfo(BigDecimal.valueOf(baseCurrencyDeliveryCost)
                .setScale(2, RoundingMode.HALF_UP), null));


        //need to call this after items and shipping info get set into transaction
        final BigDecimal paymentAmount = addTender(transaction, orderModel);
        if (purchaseOptionConfig.getAllowPaymentDues().booleanValue()) {
            tlogConverter.addLaybyInformationDetails(orderModel, transaction, paymentAmount);
        }
        if (BooleanUtils.isTrue(isPreOrder)) {
            tlogConverter.addPaymentAndCustomerInfoForPreOrder(orderModel, transaction, paymentAmount);
            transaction.setDueDate(orderModel.getNormalSaleStartDateTime());
        }
        JaxbLogger.logXml(LOG, transaction);

        try {
            final String tlogMessage = tlogMessageConverter.getMessagePayload(
                    new TLog(new Version(fileVersionNumber), ImmutableList.of(transaction)));
            messageDispatcher.send(queueName, tlogMessage);
        }
        catch (final JmsException e) {
            LOG.error("Failed to send TLOG message for order: " + orderModel.getCode(), e);
            throw new PublishTlogFailedException("Failed to send TLOG message for order: " + orderModel.getCode(), e);
        }
    }

    @Override
    public void publishTlogForPreOrderFulfilment(final OrderProcessModel process) {
        final OrderModel orderModel = process.getOrder();
        if (orderModel == null) {
            throw new IllegalArgumentException("Order is null");
        }
        final Transaction transaction = tlogConverter.getTransactionForPreOrderFulfilment(process, orderModel);
        JaxbLogger.logXml(LOG, transaction);

        try {
            final String tlogMessage = tlogMessageConverter.getMessagePayload(
                    new TLog(new Version(fileVersionNumber), ImmutableList.of(transaction)));
            messageDispatcher.send(queueName, tlogMessage);
        }
        catch (final JmsException e) {
            LOG.error("Failed to send TLOG message for Fulfilment Pre Order: ", e);
            throw new PublishTlogFailedException("Failed to send TLOG message for Cancelling Pre Order : ", e);
        }
    }


    /**
     * Add Tender to the transaction
     * 
     * @param transaction
     * @param orderModel
     * @return paymentAmount
     */
    protected BigDecimal addTender(final Transaction transaction, final OrderModel orderModel) {
        BigDecimal paymentAmount = null;
        final List<TenderInfo> tenderList = new LinkedList<>();
        // if the sales channel is trade me, set the tender type cash.
        // We must not convert the tender amount from the transaction entry, as this will/may create a variance in the total tender amount (due to rounding during currency conversion)
        //compared to the sum of items contained within the order, causing POS to reject to feed. We just add up item totals and shipping amount.
        if (SalesApplication.TRADEME.equals(orderModel.getSalesApplication())) {
            final TenderInfo tender = new TenderInfo();
            tender.setType(TenderTypeEnum.CASH);
            tender.setApproved("Y");

            BigDecimal amount = BigDecimal.ZERO;
            if (transaction.getShippingInfo() != null && transaction.getShippingInfo().getAmount() != null) {
                amount = amount.add(transaction.getShippingInfo().getAmount());
            }
            if (CollectionUtils.isNotEmpty(transaction.getItems())) {
                for (final ItemInfo item : transaction.getItems()) {
                    amount = amount.add(item.getPrice().multiply(BigDecimal.valueOf(item.getQuantity().longValue())));
                }
            }
            tender.setAmount(amount.setScale(2, RoundingMode.HALF_UP));
            tenderList.add(tender);
        }
        else {
            final PaymentTransactionModel paymentTransaction = PaymentTransactionHelper
                    .findCaptureTransaction(orderModel);
            final List<PaymentTransactionEntryModel> transactionEntries = paymentTransaction.getEntries();
            if (transactionEntries != null) {
                paymentAmount = BigDecimal.ZERO;
                for (final PaymentTransactionEntryModel transactionEntry : transactionEntries) {
                    if ((PaymentTransactionType.CAPTURE.equals(transactionEntry.getType())
                            && TransactionStatus.ACCEPTED.toString()
                                    .equals(transactionEntry.getTransactionStatus()))) {
                        tenderList.add(tlogConverter.getTenderInfoFromPaymentEntry(transactionEntry, false));
                        paymentAmount = paymentAmount.add(transactionEntry.getAmount());
                    }
                }
            }
        }
        transaction.setTender(tenderList.isEmpty() ? null : tenderList);
        return paymentAmount;
    }

    /**
     * Add discounts from the order to the TLOG Transaction data structure
     * 
     * @param orderModel
     * @param promotionOrderResults
     * @param transaction
     */
    protected void addTransactionDiscounts(final OrderModel orderModel,
            final PromotionOrderResults promotionOrderResults, final Transaction transaction,
            final Boolean isPreOrder) {

        // List of discounts
        final List<DiscountInfo> discountInfos = new ArrayList<>();

        // First do TMD discount
        final Map<PromotionResult, Double> allPromos = tlogConverter
                .getAllTMDFromPromotionResults(promotionOrderResults);
        if (MapUtils.isNotEmpty(allPromos)) {
            for (final Entry<PromotionResult, Double> promo : allPromos.entrySet()) {
                final double totalCurrentTMDiscount = promo.getValue().doubleValue();
                discountInfos.add(tlogConverter.getDiscountInfoFromPromotion(
                        (ProductPromotionModel)getModelService().get(promo.getKey().getPromotion()),
                        orderModel.getTmdCardNumber(),
                        BigDecimal.valueOf(totalCurrentTMDiscount)));
            }
        }

        // Vouchers and Flybuys,exclude voucher and Flybuys redemption for PreOrder
        if (!BooleanUtils.isTrue(isPreOrder)) {
            final Collection<AppliedVoucherData> vouchers = targetDiscountService
                    .getAppliedVouchersAndFlybuysData(orderModel);
            addVouchers(discountInfos, vouchers);
        }

        if (CollectionUtils.isNotEmpty(discountInfos)) {
            transaction.setDiscounts(discountInfos);
        }

    }

    private void addVouchers(final List<DiscountInfo> discountInfos, final Collection<AppliedVoucherData> vouchers) {

        if (CollectionUtils.isNotEmpty(vouchers)) {
            for (final AppliedVoucherData voucher : vouchers) {
                discountInfos.add(convertVoucherToDiscountInfo(voucher));
            }
        }
    }

    private DiscountInfo convertVoucherToDiscountInfo(final AppliedVoucherData voucher) {

        final DiscountInfo info = new DiscountInfo();
        info.setCode(voucher.getVoucherCode());

        if (voucher.getAppliedValue() != null) {
            info.setAmount(BigDecimal.valueOf(voucher.getAppliedValue().doubleValue()));
        }

        if (voucher instanceof AppliedFlybuysDiscountData) {
            // Adding flybuys info
            final AppliedFlybuysDiscountData flybuy = (AppliedFlybuysDiscountData)voucher;
            info.setType(DiscountTypeEnum.FLYBUYS);
            info.setCode(flybuy.getVoucherCode());
            info.setPoints(flybuy.getPoints());
            info.setRedeemCode(flybuy.getRedeemCode());
            info.setConfirmationCode(flybuy.getConfirmationCode());
        }
        else {
            // Adding voucher info
            info.setType(DiscountTypeEnum.VOUCHER);
            // DOLLAR_OFF is default unless voucher is set not absolute
            if (voucher.isAbsolute() != null && !voucher.isAbsolute().booleanValue()) {
                info.setStyle(DiscountStyleEnum.PERCENT_OFF);
                info.setPercentage(voucher.getPercent());
            }
            else {
                info.setStyle(DiscountStyleEnum.DOLLAR_OFF);
            }

            // Incremental is always "N"
            info.setIncremental("N");
        }

        return info;
    }

    @Override
    public void publishTlogForPayment(final PaymentTransactionEntryModel paymentTransactionEntryModel) {
        if (paymentTransactionEntryModel == null) {
            throw new IllegalArgumentException("PaymentTransactionEntryModel is null");
        }

        final OrderModel orderModel = (OrderModel)paymentTransactionEntryModel.getPaymentTransaction().getOrder();
        final UserModel userModel = orderModel.getUser();

        final Transaction transaction = new Transaction();
        transaction.setType(TransactionTypeEnum.LAYBY_PAYMENT);
        transaction.setTimestamp(paymentTransactionEntryModel.getCreationtime());
        transaction.setOrder(tlogConverter.getOrderInfoFromOrderModel(orderModel,
                userModel != null ? userModel.getDisplayName() : null));
        transaction.setFlybuys(new Flybuys(orderModel.getFlyBuysCode()));
        transaction.setPayment(new Payment(paymentTransactionEntryModel.getAmount()));
        transaction.setLaybyType(orderModel.getPurchaseOptionConfig().getPosCode());

        final List<TenderInfo> tenderList = new LinkedList<>();
        tenderList.add(tlogConverter.getTenderInfoFromPaymentEntry(paymentTransactionEntryModel, false));
        transaction.setTender(tenderList);

        JaxbLogger.logXml(LOG, transaction);

        try {
            final String tlogMessage = tlogMessageConverter.getMessagePayload(
                    new TLog(new Version(fileVersionNumber), ImmutableList.of(transaction)));
            messageDispatcher.send(queueName, tlogMessage);
        }
        catch (final JmsException e) {
            LOG.error("Failed to send TLOG message for layby payment: ", e);
            throw new PublishTlogFailedException("Failed to send TLOG message for layby payment: ", e);
        }
    }

    @Override
    public void publishTlogForOrderReturn(final OrderReturnRecordEntryModel orderReturnRecordEntryModel,
            final List<PaymentTransactionEntryModel> refundTransactionEntries, final BigDecimal refundAmountRemaining) {
        if (orderReturnRecordEntryModel == null) {
            throw new IllegalArgumentException("OrderReturnRecordEntryModel is null");
        }

        final Transaction transaction = tlogConverter.getTransactionForModificationRecordEntry(
                orderReturnRecordEntryModel,
                getUserNameFromPrincipalModel(orderReturnRecordEntryModel.getPrincipal()), refundTransactionEntries,
                refundAmountRemaining);

        JaxbLogger.logXml(LOG, transaction);

        try {
            final String tlogMessage = tlogMessageConverter.getMessagePayload(
                    new TLog(new Version(fileVersionNumber), ImmutableList.of(transaction)));
            messageDispatcher.send(queueName, tlogMessage);
        }
        catch (final JmsException e) {
            LOG.error("Failed to send TLOG message for return order: ", e);
            throw new PublishTlogFailedException("Failed to send TLOG message for return order: ", e);
        }
    }

    @Override
    public void publishTlogForOrderCancel(final OrderCancelRecordEntryModel orderCancelRecordEntryModel,
            final List<PaymentTransactionEntryModel> refundTransactionEntries, final BigDecimal refundAmountRemaining) {
        if (orderCancelRecordEntryModel == null) {
            throw new IllegalArgumentException("OrderCancelRecordEntryModel is null");
        }

        final Transaction transaction = tlogConverter.getTransactionForModificationRecordEntry(
                orderCancelRecordEntryModel,
                getUserNameFromPrincipalModel(orderCancelRecordEntryModel.getPrincipal()), refundTransactionEntries,
                refundAmountRemaining);

        JaxbLogger.logXml(LOG, transaction);

        try {
            final String tlogMessage = tlogMessageConverter.getMessagePayload(
                    new TLog(new Version(fileVersionNumber), ImmutableList.of(transaction)));
            messageDispatcher.send(queueName, tlogMessage);
        }
        catch (final JmsException e) {
            LOG.error("Failed to send TLOG message for cancelled order: ", e);
            throw new PublishTlogFailedException("Failed to send TLOG message for cancelled order: ", e);
        }
    }

    /**
     * ****** NOTE : Please note that the refunded amount needs to be set on the cancelRequestModel for the TLOG's to
     * work in case of this action*****
     * 
     * @see au.com.target.tgtsale.tlog.TargetTransactionLogService#publishTlogForOrderRejected(OrderCancelRecordEntryModel,
     *      List)
     */
    @Override
    @Deprecated
    public void publishTlogForOrderRejected(final OrderCancelRecordEntryModel orderCancelRecordEntryModel,
            final List<PaymentTransactionEntryModel> refundTransactionEntries) {

        if (orderCancelRecordEntryModel == null) {
            throw new IllegalArgumentException("OrderCancelRecordEntryModel is null");
        }

        final Transaction transaction = tlogConverter.getTransactionForModificationRecordEntry(
                orderCancelRecordEntryModel,
                getUserNameFromPrincipalModel(orderCancelRecordEntryModel.getPrincipal()), refundTransactionEntries,
                null);

        JaxbLogger.logXml(LOG, transaction);

        try {
            final String tlogMessage = tlogMessageConverter.getMessagePayload(
                    new TLog(new Version(fileVersionNumber), ImmutableList.of(transaction)));
            messageDispatcher.send(queueName, tlogMessage);
        }
        catch (final JmsException e) {
            LOG.error("Failed to send TLOG message for rejected order: ", e);
            throw new PublishTlogFailedException("Failed to send TLOG message for rejected order: ", e);
        }
    }

    @Override
    public void publishTlogForOrderZeroPick(final OrderCancelRecordEntryModel orderCancelRecordEntryModel,
            final List<PaymentTransactionEntryModel> refundTransactionEntries) {
        if (orderCancelRecordEntryModel == null) {
            throw new IllegalArgumentException("OrderCancelRecordEntryModel is null");
        }

        final Transaction transaction = tlogConverter.getTransactionForModificationRecordEntry(
                orderCancelRecordEntryModel,
                getUserNameFromPrincipalModel(orderCancelRecordEntryModel.getPrincipal()), refundTransactionEntries,
                null);

        JaxbLogger.logXml(LOG, transaction);

        try {
            final String tlogMessage = tlogMessageConverter.getMessagePayload(
                    new TLog(new Version(fileVersionNumber), ImmutableList.of(transaction)));
            messageDispatcher.send(queueName, tlogMessage);
        }
        catch (final JmsException e) {
            LOG.error("Failed to send TLOG message for zero pick order: ", e);
            throw new PublishTlogFailedException("Failed to send TLOG message for rejected order : ", e);
        }
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.TargetTransactionLogService#publishTlogForPreOrderCancel(de.hybris.platform.core.model.order.OrderModel, java.util.List)
     */
    @Override
    public void publishTlogForPreOrderCancel(final OrderModel orderModel,
            final List<PaymentTransactionEntryModel> refundTransactionEntries) {
        final Transaction transaction = tlogConverter.getTransactionForPreOrderCancel(orderModel,
                refundTransactionEntries);
        JaxbLogger.logXml(LOG, transaction);

        try {
            final String tlogMessage = tlogMessageConverter.getMessagePayload(
                    new TLog(new Version(fileVersionNumber), ImmutableList.of(transaction)));
            messageDispatcher.send(queueName, tlogMessage);
        }
        catch (final JmsException e) {
            LOG.error("Failed to send TLOG message for Cancelling Pre Order: ", e);
            throw new PublishTlogFailedException("Failed to send TLOG message for Cancelling Pre Order : ", e);
        }
    }

    private String getUserNameFromPrincipalModel(final PrincipalModel principal) {
        if (principal instanceof EmployeeModel) {
            final EmployeeModel user = (EmployeeModel)principal;
            final UserGroupModel userGroup = userService.getUserGroupForUID(configurationService.getConfiguration()
                    .getString("csagent.group.uid",
                            "csagentgroup"));

            if (userGroup != null && userService.isMemberOfGroup(user, userGroup)) {
                return user.getDisplayName();
            }
        }

        return configurationService.getConfiguration().getString("tgtsale.tlog.systemuser.uid", "system");
    }


    /**
     * Compile all the results for the different instances in the dealResults
     * 
     * @param dealResults
     * @return List of all the transDeals
     */
    private List<TransactionDeal> getTransactionDeals(final DealResults dealResults) {
        if (dealResults == null) {
            return null;
        }
        final Map<String, DealDetails> dealDetails = dealResults.getDealDetailsForInstance();
        if (MapUtils.isNotEmpty(dealDetails)) {
            final List<TransactionDeal> transDeals = new ArrayList<>();
            for (final String instance : dealDetails.keySet()) {
                final DealDetails details = dealDetails.get(instance);
                final TransactionDeal deal = new TransactionDeal();
                deal.setId(details.getCode());
                deal.setInstance(details.getInstance());
                deal.setMarkdown(details.getMarkdown().toString());
                deal.setType(details.getType());
                transDeals.add(deal);
            }
            return transDeals;
        }
        return null;
    }



    /**
     * Gets the promotion service.
     * 
     * @return the promotionService
     */
    public PromotionsService getPromotionsService() {
        return promotionsService;
    }

    /**
     * Sets the promotion service.
     * 
     * @param promotionsService
     *            the promotionService to set
     */
    @Required
    public void setPromotionsService(final PromotionsService promotionsService) {
        this.promotionsService = promotionsService;
    }

    /**
     * @param tlogConverter
     *            the tlogConverter to set
     */
    @Required
    public void setTlogConverter(final TlogConverter tlogConverter) {
        this.tlogConverter = tlogConverter;
    }

    /**
     * @return the deals
     */
    public Map<String, String> getDeals() {
        return deals;
    }

    /**
     * @param deals
     *            the deals to set
     */
    @Required
    public void setDeals(final Map<String, String> deals) {
        this.deals = deals;
    }


}
