/**
 * 
 */
package au.com.target.tgtmarketing.offers.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;
import au.com.target.tgtmarketing.offers.dao.TargetMobileOfferHeadingsDao;


/**
 * @author paul
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetMobileOfferHeadingServiceImplTest {

    @InjectMocks
    private final TargetMobileOfferHeadingServiceImpl targetMobileOfferHeadingService = new TargetMobileOfferHeadingServiceImpl();

    @Mock
    private TargetMobileOfferHeadingsDao targetMobileOfferHeadingsDao;


    @Before
    public void setUp() {
        targetMobileOfferHeadingService.setTargetMobileOfferHeadingsDao(targetMobileOfferHeadingsDao);
    }

    @Test
    public void testGetAllActiveDealsWithResults() {
        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModelMock1 = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModelMock2 = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        BDDMockito.given(targetMobileOfferHeadingsDao.getAllTargetMobileOfferHeadings()).willReturn(
                new ArrayList<TargetMobileOfferHeadingModel>(Arrays.asList(targetMobileOfferHeadingModelMock1,
                        targetMobileOfferHeadingModelMock2)));
        final List<TargetMobileOfferHeadingModel> targetMobileOfferHeadingModelList = targetMobileOfferHeadingService
                .getAllTargetMobileOfferHeadings();
        Assert.assertEquals(2, targetMobileOfferHeadingModelList.size());
        Assert.assertEquals(targetMobileOfferHeadingModelMock1, targetMobileOfferHeadingModelList.get(0));
        Assert.assertEquals(targetMobileOfferHeadingModelMock2, targetMobileOfferHeadingModelList.get(1));
    }

    @Test
    public void testGetAllActiveDealsWithNoResult() {
        BDDMockito.given(targetMobileOfferHeadingsDao.getAllTargetMobileOfferHeadings()).willReturn(null);
        final List<TargetMobileOfferHeadingModel> targetMobileOfferHeadingModelList = targetMobileOfferHeadingService
                .getAllTargetMobileOfferHeadings();
        Assert.assertNull(targetMobileOfferHeadingModelList);
    }
}
