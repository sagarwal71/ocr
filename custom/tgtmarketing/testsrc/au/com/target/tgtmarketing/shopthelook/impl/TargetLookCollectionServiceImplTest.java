/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;
import au.com.target.tgtmarketing.shopthelook.LookCollectionService;
import au.com.target.tgtmarketing.shopthelook.dao.LookCollectionDao;
import org.junit.Assert;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetLookCollectionServiceImplTest {

    private static final String CODE = "LC001";

    @Mock
    private LookCollectionDao lookCollectionDao;

    @InjectMocks
    private final LookCollectionService lookCollectionService = new TargetLookCollectionServiceImpl();

    @Test
    public void testGetLookCollection() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(lookCollectionDao.getLookCollectionForCode(CODE))
                .willReturn(new TargetLookCollectionModel());

        final TargetLookCollectionModel lookCollection = lookCollectionService.getLookCollectionForCode(CODE);
        Assert.assertNotNull(lookCollection);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetLookCollectionNullCode()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        lookCollectionService.getLookCollectionForCode(null);
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testGetLookCollectionMissing()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(lookCollectionDao.getLookCollectionForCode(CODE))
                .willThrow(new TargetUnknownIdentifierException(CODE));
        lookCollectionService.getLookCollectionForCode(CODE);
    }

}
