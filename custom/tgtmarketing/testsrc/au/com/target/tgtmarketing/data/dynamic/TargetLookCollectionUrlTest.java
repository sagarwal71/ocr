/**
 * 
 */
package au.com.target.tgtmarketing.data.dynamic;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtmarketing.model.TargetLookCollectionModel;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetLookCollectionUrlTest {

    private final TargetLookCollectionUrl targetLookCollectionUrl = new TargetLookCollectionUrl();

    private final String lookCollectionUrlPrefix = "test.prefix/";

    @Before
    public void setUp() {
        targetLookCollectionUrl.setLookCollectionUrlPrefix(lookCollectionUrlPrefix);
    }

    @Test
    public void testGet() {
        final TargetLookCollectionModel lookCollection = new TargetLookCollectionModel();
        final TargetShopTheLookModel shopTheLookModel = new TargetShopTheLookModel();
        shopTheLookModel.setName("shop The Look Name");
        shopTheLookModel.setId("stl123456");
        lookCollection.setShopTheLook(shopTheLookModel);
        lookCollection.setId("coll123456");
        final String result = targetLookCollectionUrl.get(lookCollection);
        Assert.assertEquals("test.prefix/shop-the-look-name/stl123456#coll123456", result);
    }
}
