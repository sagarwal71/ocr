package au.com.target.tgtmarketing.social.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtmarketing.model.SocialMediaProductsModel;
import au.com.target.tgtmarketing.social.dao.TargetSocialMediaProductsDao;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetSocialMediaProductsServiceImplTest {
    @Mock
    private TargetSocialMediaProductsDao mockTargetSocialMediaProductsDao;

    @InjectMocks
    private final TargetSocialMediaProductsServiceImpl targetSocialMediaProductsServiceImpl = new TargetSocialMediaProductsServiceImpl();

    @Test
    public void testGetSocialMediaProductsByCreationTimeDescending() {
        final List<SocialMediaProductsModel> searchResults = new ArrayList<>();
        given(mockTargetSocialMediaProductsDao.findSocialMediaProductsByCreationTimeDescending(11))
                .willReturn(searchResults);

        final List<SocialMediaProductsModel> results = targetSocialMediaProductsServiceImpl
                .getSocialMediaProductsByCreationTimeDescending(11);
        assertThat(results).isEqualTo(searchResults);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetSocialMediaProductsByCreationTimeDescendingCountZero() {
        targetSocialMediaProductsServiceImpl.getSocialMediaProductsByCreationTimeDescending(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetSocialMediaProductsByCreationTimeDescendingCountNegative() {
        targetSocialMediaProductsServiceImpl.getSocialMediaProductsByCreationTimeDescending(-1);
    }
}
