/**
 * 
 */
package au.com.target.tgtmarketing.segments.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.usergroups.model.UserSegmentModel;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.SetUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetUserSegmentServiceImplTest {

    @InjectMocks
    private final TargetUserSegmentServiceImpl targetUserSegmentServiceImpl = new TargetUserSegmentServiceImpl();


    @Test
    public void testGetAllUserSegmentWhenUserDoesNotHaveAnyGroups() {

        final UserModel userModel = Mockito.mock(UserModel.class);
        BDDMockito.given(userModel.getGroups()).willReturn(SetUtils.EMPTY_SET);
        Assertions.assertThat(targetUserSegmentServiceImpl.getAllUserSegmentsForUser(userModel)).isEmpty();

    }

    @Test
    public void testGetAllUserSegmentWhenNoSegmentExist() {
        final UserModel userModel = Mockito.mock(UserModel.class);
        final UserGroupModel userGroup = Mockito.mock(UserGroupModel.class);
        final Set<PrincipalGroupModel> userGroups = new HashSet<>();
        userGroups.add(userGroup);
        BDDMockito.given(userModel.getGroups()).willReturn(userGroups);
        Assertions.assertThat(targetUserSegmentServiceImpl.getAllUserSegmentsForUser(userModel)).isEmpty();
    }

    @Test
    public void testGetAllUserSegmentWhenUserHasAUserSegment() {
        final UserModel userModel = Mockito.mock(UserModel.class);

        final UserGroupModel userGroup = Mockito.mock(UserGroupModel.class);
        final UserSegmentModel userSegment = Mockito.mock(UserSegmentModel.class);
        final Set<PrincipalGroupModel> userGroups = new HashSet<>();
        userGroups.add(userGroup);
        userGroups.add(userSegment);

        BDDMockito.given(userModel.getGroups()).willReturn(userGroups);
        final List<PrincipalGroupModel> groups = targetUserSegmentServiceImpl.getAllUserSegmentsForUser(userModel);
        Assertions.assertThat(groups).isNotEmpty();
        Assertions.assertThat(groups).hasSize(1);
        Assertions.assertThat(groups.get(0)).isInstanceOf(UserSegmentModel.class);
    }

    @Test
    public void testGetAllUserSegmentWhenUserHasSubUserSegments() {
        final UserModel userModel = Mockito.mock(UserModel.class);
        final UserGroupModel userGroup = Mockito.mock(UserGroupModel.class);
        final UserSegmentModel userSegment = Mockito.mock(UserSegmentModel.class);
        final UserSegmentModel subUserSegment = Mockito.mock(UserSegmentModel.class);
        final Set<PrincipalGroupModel> subUserSegmentGroups = new HashSet<>();
        subUserSegmentGroups.add(subUserSegment);
        BDDMockito.given(userSegment.getGroups()).willReturn(subUserSegmentGroups);
        final Set<PrincipalGroupModel> userGroups = new HashSet<>();
        userGroups.add(userGroup);
        userGroups.add(userSegment);

        BDDMockito.given(userModel.getGroups()).willReturn(userGroups);
        final List<PrincipalGroupModel> groups = targetUserSegmentServiceImpl.getAllUserSegmentsForUser(userModel);
        Assertions.assertThat(groups).isNotEmpty().hasSize(2);
        Assertions.assertThat(groups).containsExactly(userSegment, subUserSegment);
    }

}
