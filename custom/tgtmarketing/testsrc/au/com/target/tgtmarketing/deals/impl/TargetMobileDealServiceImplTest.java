/**
 * 
 */
package au.com.target.tgtmarketing.deals.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.AbstractDealModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtmarketing.deals.dao.TargetMobileDealsDao;


/**
 * @author paul
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetMobileDealServiceImplTest {

    @InjectMocks
    private final TargetMobileDealServiceImpl targetMobileDealService = new TargetMobileDealServiceImpl();

    @Mock
    private TargetMobileDealsDao targetMobileDealDao;


    @Before
    public void setUp() {
        targetMobileDealService.setTargetMobileDealDao(targetMobileDealDao);
    }

    @Test
    public void testGetAllActiveDealsWithResults() {
        final AbstractDealModel abstractTargetDealMock1 = Mockito.mock(AbstractDealModel.class);
        final AbstractDealModel abstractTargetDealMock2 = Mockito.mock(AbstractDealModel.class);
        BDDMockito.given(targetMobileDealDao.getAllActiveMobileDeals()).willReturn(
                new ArrayList<AbstractDealModel>(Arrays.asList(abstractTargetDealMock1, abstractTargetDealMock2)));
        final List<AbstractDealModel> targetMobileDealsList = targetMobileDealService.getAllActiveMobileDeals();
        Assert.assertEquals(2, targetMobileDealsList.size());
        Assert.assertEquals(abstractTargetDealMock1, targetMobileDealsList.get(0));
        Assert.assertEquals(abstractTargetDealMock2, targetMobileDealsList.get(1));
    }

    @Test
    public void testGetAllActiveDealsWithNoResult() {
        BDDMockito.given(targetMobileDealDao.getAllActiveMobileDeals()).willReturn(null);
        final List<AbstractDealModel> targetMobileDealsList = targetMobileDealService.getAllActiveMobileDeals();
        Assert.assertNull(targetMobileDealsList);
    }
}
