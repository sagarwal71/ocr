package au.com.target.tgtmarketing.shopthelook.dao;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;


/**
 * @author mgazal
 */
public interface LookCollectionDao {

    /**
     * Get the look collection for a given code.
     * 
     * @param code
     * @return {@link TargetLookCollectionModel}
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    TargetLookCollectionModel getLookCollectionForCode(String code)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;

}
