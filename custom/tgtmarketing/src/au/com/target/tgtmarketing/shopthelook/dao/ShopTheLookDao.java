package au.com.target.tgtmarketing.shopthelook.dao;

import java.util.List;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;


/**
 * @author mgazal
 */
public interface ShopTheLookDao {

    /**
     * Retrieve shop the look for a code.
     * 
     * @param code
     * @return {@link TargetShopTheLookModel}
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    TargetShopTheLookModel getShopTheLookForCode(String code)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;


    /**
     * Find the looks for a shop the look code
     * 
     * @param code
     * @return {@link TargetShopTheLookModel}
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    List<TargetLookModel> getListOfLooksForShopTheLookUsingCode(String code)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;

    /**
     * @param code
     * @return TargetShopTheLookModel
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    TargetShopTheLookModel getShopTheLookByCategoryCode(String code)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;

}
