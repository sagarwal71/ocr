/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook.dao.impl;

import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.internal.dao.SortParameters.SortOrder;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.util.TargetServicesUtil;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;
import au.com.target.tgtmarketing.shopthelook.dao.ShopTheLookDao;
import au.com.target.tgtutility.constants.TgtutilityConstants;


/**
 * 
 * @author mgazal
 *
 */
public class TargetShopTheLookDaoImpl extends DefaultGenericDao<TargetShopTheLookModel>
        implements ShopTheLookDao {
    private static final Logger LOG = Logger.getLogger(TargetShopTheLookDaoImpl.class);

    public TargetShopTheLookDaoImpl() {
        super(TargetShopTheLookModel._TYPECODE);
    }

    @Override
    public TargetShopTheLookModel getShopTheLookForCode(final String code)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        ServicesUtil.validateParameterNotNullStandardMessage("code", code);

        final Map<String, String> params = new HashMap<>();
        params.put(TargetShopTheLookModel.ID, code);
        final List<TargetShopTheLookModel> shopTheLookModels = find(params);

        TargetServicesUtil.validateIfSingleResult(shopTheLookModels, TargetShopTheLookModel.class,
                TargetShopTheLookModel.ID, code);

        return shopTheLookModels.get(0);
    }

    @Override
    public List<TargetLookModel> getListOfLooksForShopTheLookUsingCode(final String id)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        final StringBuilder query = new StringBuilder();
        query.append("SELECT {lk:" + TargetLookModel.PK + "} FROM {" + TargetLookModel._TYPECODE + "  AS lk");
        query.append(" JOIN " + TargetLookCollectionModel._TYPECODE + " AS coll ON {coll:"
                + TargetLookCollectionModel.PK + "} = {lk:"
                + TargetLookModel.COLLECTION + "}");
        query.append(" JOIN " + TargetShopTheLookModel._TYPECODE + " AS stl ON {stl:" + TargetShopTheLookModel.PK
                + "} = {coll:" + TargetLookCollectionModel.SHOPTHELOOK + "} "
                + "AND {stl:" + TargetShopTheLookModel.ID + "} = ?shopTheLookId ");
        query.append(" }");
        query.append(" WHERE {lk:" + TargetLookModel.ENABLED + "} = 1 ");
        query.append(" AND  {lk:" + TargetLookModel.STARTDATE + "} <= ?timestamp ");
        query.append(" AND ({lk:" + TargetLookModel.ENDDATE + "} >= ?timestamp OR {lk:" + TargetLookModel.ENDDATE
                + "} IS NULL) ");
        query.append(" ORDER BY {lk:" + TargetLookModel.STARTDATE + "} " + SortOrder.DESCENDING);

        final Date currentDay = DateUtils.round(new Date(), Calendar.MINUTE);
        final Map<String, Object> params = new HashMap();
        params.put("shopTheLookId", id);
        params.put("timestamp", currentDay);

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.addQueryParameters(params);
        final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
        LOG.info(TgtutilityConstants.InfoCode.TOTAL_RECORDS_RETURNED + " = "
                + (searchResult.getCount())
                + "," + TgtutilityConstants.InfoCode.QUERY_STRING + " = " + id);
        return searchResult.getResult();
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtmarketing.shopthelook.dao.ShopTheLookDao#getShopTheLookByCagegoryCode(java.lang.String)
     */
    @Override
    public TargetShopTheLookModel getShopTheLookByCategoryCode(final String code)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        final StringBuilder shopTheLookQuery = new StringBuilder();
        shopTheLookQuery.append(
                "SELECT {stl:" + TargetShopTheLookModel.PK + "} FROM {" + TargetShopTheLookModel._TYPECODE
                        + " AS stl ");
        shopTheLookQuery.append(" JOIN " + TargetProductCategoryModel._TYPECODE + " AS tpc ON {tpc:"
                + TargetProductCategoryModel.PK + "}={stl:" + TargetShopTheLookModel.CATEGORY + "} ");
        shopTheLookQuery.append(" JOIN " + CatalogVersionModel._TYPECODE + " AS cv ON {tpc:"
                + TargetProductCategoryModel.CATALOGVERSION + "}={cv:" + CatalogVersionModel.PK + "} ");
        shopTheLookQuery.append(" JOIN " + CatalogModel._TYPECODE + " AS c ON {c:" + CatalogModel.PK + "}={cv:"
                + CatalogVersionModel.CATALOG + "} }");
        shopTheLookQuery.append(" WHERE {tpc:" + TargetProductCategoryModel.CODE + "}=?categoryCode");
        shopTheLookQuery.append(" AND {cv:" + CatalogVersionModel.VERSION + "}=?version");
        shopTheLookQuery.append(" AND {c:" + CatalogModel.ID + "}=?catalogId");

        final Map<String, Object> params = new HashMap();
        params.put("categoryCode", code);
        params.put("version", TgtCoreConstants.Catalog.OFFLINE_VERSION);
        params.put("catalogId", TgtCoreConstants.Catalog.PRODUCTS);

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(shopTheLookQuery.toString());
        searchQuery.addQueryParameters(params);
        final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
        TargetServicesUtil.validateIfSingleResult(searchResult.getResult(), TargetShopTheLookModel.class,
                TargetShopTheLookModel.ID, code);
        return (TargetShopTheLookModel)searchResult.getResult().get(0);
    }

}
