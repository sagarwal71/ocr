/**
 * 
 */
package au.com.target.tgtmarketing.interceptor;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;


/**
 * @author paul
 *
 */
public class TgtMarketingInterceptorUtil {

    /**
     * 
     */
    private TgtMarketingInterceptorUtil() {
        // should not instantiate this class
    }

    /**
     * This method takes in a list TargetMobileOfferHeadingModel and checks for duplicate. If duplicate found then
     * returns true else false.
     * 
     * @param targetMobileOfferHeadingModelList
     * @return boolean whether duplicate found.
     */
    public static final boolean mobileOfferHeadingContainDuplicates(
            final List<TargetMobileOfferHeadingModel> targetMobileOfferHeadingModelList) {
        if (CollectionUtils.isEmpty(targetMobileOfferHeadingModelList)) {
            return false;
        }
        final List<String> targetMobileOfferHeadingCodes = new ArrayList<>();
        for (final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel : targetMobileOfferHeadingModelList) {
            if (targetMobileOfferHeadingCodes.contains(targetMobileOfferHeadingModel.getCode())) {
                return true;
            }
            targetMobileOfferHeadingCodes.add(targetMobileOfferHeadingModel.getCode());
        }
        return false;
    }

}
