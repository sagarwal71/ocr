/**
 * 
 */
package au.com.target.tgtmarketing.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;


/**
 * @author pthoma20
 * 
 */
public class TgtMobileOfferHeadingModelValidateInterceptor implements ValidateInterceptor {

    private static final String COLOUR_HEX_REGEX_PATTERN = "^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$";

    /* (non-Jav
     * adoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof TargetMobileOfferHeadingModel)) {
            return;
        }

        final TargetMobileOfferHeadingModel tgtMobileOfferHeadingModel = (TargetMobileOfferHeadingModel)model;
        if (StringUtils.isBlank(tgtMobileOfferHeadingModel.getColour())) {
            throw new InterceptorException(ErrorMessages.TARGET_MOBILE_OFFER_HEADING_ERROR
                    + ErrorMessages.COLOUR_REQUIRED);
        }

        final Pattern pattern = Pattern.compile(COLOUR_HEX_REGEX_PATTERN);
        final Matcher matcher = pattern.matcher(tgtMobileOfferHeadingModel.getColour());

        if (!matcher.matches()) {
            throw new InterceptorException(ErrorMessages.TARGET_MOBILE_OFFER_HEADING_ERROR
                    + ErrorMessages.INVALID_COLOUR_FORMAT);
        }

    }

    protected interface ErrorMessages {
        public static final String TARGET_MOBILE_OFFER_HEADING_ERROR = "Error Saving TargetMobileOfferHeadingModel-";
        public static final String COLOUR_REQUIRED = "Please Add colour.";
        public static final String INVALID_COLOUR_FORMAT = "Please add a hex colour value for colour.";
    }

}
