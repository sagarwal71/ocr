package au.com.target.tgtmarketing.deals;

import de.hybris.platform.promotions.model.AbstractDealModel;

import java.util.List;


/**
 * 
 * Service API to manage {@link AbstractDealModel}
 * 
 */
public interface TargetMobileDealService {

    /**
     * @return list of active deals
     */
    List<AbstractDealModel> getAllActiveMobileDeals();

}
