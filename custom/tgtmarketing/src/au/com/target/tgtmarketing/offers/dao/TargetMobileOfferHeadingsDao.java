package au.com.target.tgtmarketing.offers.dao;

import java.util.List;

import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;



/**
 * DAO to manage {@link TargetMobileOfferHeadingsDao}
 */
public interface TargetMobileOfferHeadingsDao {

    /**
     * @return all Mobile Offer Headings
     */
    List<TargetMobileOfferHeadingModel> getAllTargetMobileOfferHeadings();


}
