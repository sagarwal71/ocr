/**
 * 
 */
package au.com.target.tgtmarketing.social.dao;

import java.util.List;

import au.com.target.tgtmarketing.model.SocialMediaProductsModel;


/**
 * @author rmcalave
 *
 */
public interface TargetSocialMediaProductsDao {

    /**
     * Find <code>SocialMediaProducts</code> ordered by descending creation date, limited by <code>count</code>.
     * 
     * @param count
     *            The maximum number of records to retrieve
     * @return A list of <code>SocialMediaProductsModel</code>
     */
    List<SocialMediaProductsModel> findSocialMediaProductsByCreationTimeDescending(int count);

}