/**
 * 
 */
package au.com.target.tgtshareddto.eftwrapper.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;



/**
 * Dummy classes created to just do a POC
 * 
 * @author rsamuel3
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentDetailsDto implements Serializable
{
    @XmlElement
    private String journalRoll;
    @XmlElement
    private String authCode;
    @XmlElement
    private String pan;
    @XmlElement
    private String rrn;
    @XmlElement
    private String respAscii;

    @XmlElement
    private String cardType;
    @XmlElement
    private String cardName;
    @XmlElement
    private String accountType;
    @XmlElement
    private String journal;
    @XmlElement
    private boolean transactionSuccess;

    /**
     * @return the journalRoll
     */
    public String getJournalRoll() {
        return journalRoll;
    }

    /**
     * @param journalRoll
     *            the journalRoll to set
     */
    public void setJournalRoll(final String journalRoll) {
        this.journalRoll = journalRoll;
    }

    /**
     * @return the authCode
     */
    public String getAuthCode() {
        return authCode;
    }

    /**
     * @param authCode
     *            the authCode to set
     */
    public void setAuthCode(final String authCode) {
        this.authCode = authCode;
    }

    /**
     * @return the pan
     */
    public String getPan() {
        return pan;
    }

    /**
     * @param pan
     *            the pan to set
     */
    public void setPan(final String pan) {
        this.pan = pan;
    }

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @param rrn
     *            the rrn to set
     */
    public void setRrn(final String rrn) {
        this.rrn = rrn;
    }

    /**
     * @return the respAscii
     */
    public String getRespAscii() {
        return respAscii;
    }

    /**
     * @param respAscii
     *            the respAscii to set
     */
    public void setRespAscii(final String respAscii) {
        this.respAscii = respAscii;
    }

    /**
     * @return the cardType
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * @param cardType
     *            the cardType to set
     */
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }

    /**
     * @return the cardName
     */
    public String getCardName() {
        return cardName;
    }

    /**
     * @param cardName
     *            the cardName to set
     */
    public void setCardName(final String cardName) {
        this.cardName = cardName;
    }

    /**
     * @return the accountType
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * @param accountType
     *            the accountType to set
     */
    public void setAccountType(final String accountType) {
        this.accountType = accountType;
    }

    /**
     * @return the journal
     */
    public String getJournal() {
        return journal;
    }

    /**
     * @param journal
     *            the journal to set
     */
    public void setJournal(final String journal) {
        this.journal = journal;
    }

    /**
     * @return the transactionSucess
     */
    public boolean isTransactionSuccess() {
        return transactionSuccess;
    }

    /**
     * @param transactionSuccess
     *            the transactionSucess to set
     */
    public void setTransactionSuccess(final boolean transactionSuccess) {
        this.transactionSuccess = transactionSuccess;
    }


}
