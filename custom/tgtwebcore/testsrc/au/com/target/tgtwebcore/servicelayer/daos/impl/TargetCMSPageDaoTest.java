package au.com.target.tgtwebcore.servicelayer.daos.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.enums.CmsPageStatus;
import de.hybris.platform.cms2.model.contents.contentslot.ContentSlotModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.jalo.JaloConnection;
import de.hybris.platform.persistence.property.PersistenceManager;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.tenant.MockTenant;
import de.hybris.platform.util.Config;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;


@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Registry.class, Config.class })
@PowerMockIgnore({ "org.apache.logging.log4j.*" })
@SuppressStaticInitializationFor({ "de.hybris.platform.core.Registry" })
public class TargetCMSPageDaoTest {

    @InjectMocks
    private final TargetCMSPageDao cmsPageDao = spy(TargetCMSPageDao.class);

    @Mock
    private FlexibleSearchService flexibleSearchService;

    @Mock
    private MockTenant currentTenant;

    @Mock
    private PersistenceManager persistanceManager;

    @Mock
    private JaloConnection jaloConnection;

    private final List<CmsPageStatus> pageStatuses = Arrays.asList(CmsPageStatus.ACTIVE);

    private Collection<CatalogVersionModel> catalogVersions;

    Collection<ContentSlotModel> contentSlots;

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(Registry.class, Mockito.RETURNS_MOCKS);
        PowerMockito.mockStatic(Config.class, Mockito.RETURNS_MOCKS);
        given(Registry.getCurrentTenant()).willReturn(currentTenant);

        given(cmsPageDao.findPagesByContentSlots(contentSlots, catalogVersions)).willCallRealMethod();
    }

    @Test
    public void testFindPagesByContentSlotsWithNoContentSlots() {

        contentSlots = Collections.EMPTY_LIST;

        cmsPageDao.findPagesByContentSlots(contentSlots, catalogVersions);

        Mockito.verify(cmsPageDao, times(0)).findPagesByContentSlotsAndPageStatuses(contentSlots, catalogVersions,
                pageStatuses);
    }

    @Test
    public void testFindPagesByContentSlotsWithNull() {

        contentSlots = null;

        cmsPageDao.findPagesByContentSlots(contentSlots, catalogVersions);

        Mockito.verify(cmsPageDao, times(0)).findPagesByContentSlotsAndPageStatuses(contentSlots, catalogVersions,
                pageStatuses);
    }

    /**
     * Testing that the super method is called when contentSlots is not empty
     */
    @Test
    public void testFindPagesByContentSlots() {

        contentSlots = mock(Collection.class);

        doReturn(Boolean.FALSE).when(contentSlots).isEmpty();

        cmsPageDao.findPagesByContentSlots(contentSlots, catalogVersions);

        Mockito.verify(cmsPageDao).findPagesByContentSlotsAndPageStatuses(contentSlots, catalogVersions, pageStatuses);
    }

}
