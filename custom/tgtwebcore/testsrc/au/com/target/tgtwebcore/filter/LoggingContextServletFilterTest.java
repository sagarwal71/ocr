/**
 * 
 */
package au.com.target.tgtwebcore.filter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.MDC;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class LoggingContextServletFilterTest {
    @InjectMocks
    protected final LoggingContextServletFilter filter = new LoggingContextServletFilter();

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession httpSession; //NOPMD
    @Mock
    private FilterChain filterChain; //NOPMD
    @Mock
    private UserService userService; //NOPMD
    @Mock
    private SessionService sessionService;

    @Before
    public void setUp() {
        MDC.clear();
    }

    @Test
    public void testSetLoggingContext() throws IOException, ServletException {
        when(request.getSession()).thenReturn(httpSession);
        when(httpSession.getId()).thenReturn("sessionId");
        when(request.getHeader("User-Agent")).thenReturn("agent");
        filter.doFilter(request, response, filterChain);
        assertEquals("agent", MDC.get("User-Agent"));
        assertEquals("sessionId", MDC.get("TomcatSessionId"));
    }

    @Test
    public void testSetLoggingContextWithNoUserAgent() throws IOException, ServletException {
        when(request.getSession()).thenReturn(httpSession);
        when(httpSession.getId()).thenReturn("sessionId");
        when(request.getHeader("User-Agent")).thenReturn(null);
        filter.doFilter(request, response, filterChain);
        assertNull(MDC.get("User-Agent"));
        assertEquals("sessionId", MDC.get("TomcatSessionId"));
    }

}
