/**
 * 
 */
package au.com.target.tgtwebcore.cms.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.restrictions.AbstractRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSRestrictionService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NavigableMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Vivek
 * 
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetRankingCMSRestrictionServiceTest {

    @InjectMocks
    private final TargetRankingCMSRestrictionService targetRankingCMSRestrictionService = new TargetRankingCMSRestrictionService() {
        @Override
        protected boolean evaluate(final List<AbstractRestrictionModel> restrictions,
                final RestrictionData restrictionData, final boolean oneRestrictionApply) {
            return true;
        }

        @Override
        protected Collection<AbstractPageModel> getDefaultPages(
                final Collection<AbstractPageModel> abstractPages) {
            final Collection<AbstractPageModel> defaultPages = new ArrayList<>();
            final AbstractPageModel page1 = new AbstractPageModel();
            defaultPages.add(page1);
            final AbstractPageModel page2 = new AbstractPageModel();
            defaultPages.add(page2);
            return defaultPages;
        }
    };

    @Mock
    private RestrictionData data;

    @Mock
    private DefaultCMSRestrictionService cmsRestrictionService;

    @Mock
    private NavigableMap<Integer, List<AbstractPageModel>> allowedPages;

    private final List<AbstractPageModel> pages = new ArrayList<>();

    @Before
    public void setUp() {

        final List<AbstractRestrictionModel> restrictions = new ArrayList<>();
        final AbstractRestrictionModel restriction1 = new AbstractRestrictionModel();
        final AbstractPageModel page1 = new AbstractPageModel();
        restrictions.add(restriction1);
        page1.setRestrictions(restrictions);
        page1.setOnlyOneRestrictionMustApply(true);
        pages.add(page1);
    }

    @Test
    public void testEvaluatePagesWithAllowedPages() {

        Collection<AbstractPageModel> result = new ArrayList<>();

        result = targetRankingCMSRestrictionService.evaluatePages(pages, data);

        org.junit.Assert.assertNotNull(result);
    }

    @Test
    public void testEvaluatePagesWithNoAllowedPages() {

        final List<AbstractPageModel> abstractPages = new ArrayList<>();
        Collection<AbstractPageModel> result = new ArrayList<>();

        result = targetRankingCMSRestrictionService.evaluatePages(abstractPages, data);

        org.junit.Assert.assertNotNull(result);
    }
}
