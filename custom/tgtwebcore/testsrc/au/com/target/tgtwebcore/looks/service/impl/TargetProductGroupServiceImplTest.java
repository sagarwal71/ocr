/**
 * 
 */
package au.com.target.tgtwebcore.looks.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.enums.CmsApprovalStatus;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Assert;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtwebcore.model.cms2.pages.TargetProductGroupPageModel;


/**
 * @author Nandini
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductGroupServiceImplTest {

    @InjectMocks
    private final TargetProductGroupServiceImpl targetGroupPageService = new TargetProductGroupServiceImpl();

    @Mock
    private TargetProductGroupPageModel groupPage1, groupPage2, groupPage3;

    @Mock
    private VariantProductModel variant;

    @Mock
    private TargetProductModel productModel;

    @Mock
    private TargetColourVariantProductModel colourVariant;

    private Date now;
    private Date today;
    private Date yesterday;
    private Date dayBeforeYesterday;

    @Before
    public void setUp() {
        now = new Date();
        today = DateUtils.truncate(now, Calendar.DAY_OF_MONTH);
        yesterday = DateUtils.addDays(today, -1);
        dayBeforeYesterday = DateUtils.addDays(today, -2);

        Mockito.when(groupPage1.getApprovalStatus()).thenReturn(CmsApprovalStatus.APPROVED);
        Mockito.when(groupPage1.getCreationtime()).thenReturn(yesterday);

        Mockito.when(groupPage2.getApprovalStatus()).thenReturn(CmsApprovalStatus.APPROVED);
        Mockito.when(groupPage2.getCreationtime()).thenReturn(today);

        Mockito.when(groupPage3.getApprovalStatus()).thenReturn(CmsApprovalStatus.APPROVED);
        Mockito.when(groupPage3.getCreationtime()).thenReturn(dayBeforeYesterday);

    }

    @Test
    public void testGetAvailableLooksForNonColourVarintProduct() {

        final Collection<VariantProductModel> variants = new ArrayList<>();
        variants.add(variant);
        Mockito.when(productModel.getVariants()).thenReturn(variants);
        final List<TargetProductGroupPageModel> looks = targetGroupPageService
                .getAllActiveLooksForProduct(productModel);
        Assert.assertEquals(0, looks.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetAvailableLooksForNullProduct() {

        final Collection<VariantProductModel> variants = new ArrayList<>();
        variants.add(variant);
        Mockito.when(productModel.getVariants()).thenReturn(variants);
        final List<TargetProductGroupPageModel> looks = targetGroupPageService
                .getAllActiveLooksForProduct(null);
        Assert.assertEquals(0, looks.size());
    }

    @Test
    public void testGetAllActiveLooksForProduct() {

        final Collection<VariantProductModel> variants = new ArrayList<>();

        final List<TargetProductGroupPageModel> productGroupPages = new ArrayList<>();

        variants.add(colourVariant);

        productGroupPages.add(groupPage1);
        productGroupPages.add(groupPage2);
        Mockito.when(productModel.getVariants()).thenReturn(variants);
        Mockito.when(colourVariant.getTargetProductGroupPages()).thenReturn(productGroupPages);
        final List<TargetProductGroupPageModel> looks = targetGroupPageService
                .getAllActiveLooksForProduct(productModel);
        Assert.assertNotNull(looks);
        Assert.assertEquals(2, looks.size());
        Assert.assertEquals(groupPage2, looks.get(0));
        Assert.assertEquals(groupPage1, looks.get(1));
    }


    @Test
    public void testGetAllActiveLooksForProductWithEmptyLooks() {

        final Collection<VariantProductModel> variants = new ArrayList<>();

        final List<TargetProductGroupPageModel> productGroupPages = new ArrayList<>();

        variants.add(colourVariant);

        Mockito.when(productModel.getVariants()).thenReturn(variants);
        Mockito.when(colourVariant.getTargetProductGroupPages()).thenReturn(productGroupPages);
        final List<TargetProductGroupPageModel> looks = targetGroupPageService
                .getAllActiveLooksForProduct(productModel);
        Assert.assertEquals(0, looks.size());
    }

    @Test
    public void testGetAllActiveLooksForProductForNotApprovedGroupPages() {

        final Collection<VariantProductModel> variants = new ArrayList<>();

        final List<TargetProductGroupPageModel> productGroupPages = new ArrayList<>();

        variants.add(colourVariant);
        Mockito.when(groupPage1.getApprovalStatus()).thenReturn(CmsApprovalStatus.UNAPPROVED);
        Mockito.when(groupPage1.getCreationtime()).thenReturn(yesterday);

        Mockito.when(groupPage2.getApprovalStatus()).thenReturn(CmsApprovalStatus.APPROVED);
        Mockito.when(groupPage2.getCreationtime()).thenReturn(today);

        productGroupPages.add(groupPage1);
        productGroupPages.add(groupPage2);
        Mockito.when(productModel.getVariants()).thenReturn(variants);
        Mockito.when(colourVariant.getTargetProductGroupPages()).thenReturn(productGroupPages);
        final List<TargetProductGroupPageModel> looks = targetGroupPageService
                .getAllActiveLooksForProduct(productModel);
        Assert.assertNotNull(looks);
        Assert.assertEquals(1, looks.size());
    }

    @Test
    public void testGetAllActiveLooksForProductForMaxLooksWithSortOrder() {

        final Collection<VariantProductModel> variants = new ArrayList<>();

        final List<TargetProductGroupPageModel> productGroupPages = new ArrayList<>();

        variants.add(colourVariant);

        productGroupPages.add(groupPage1);
        productGroupPages.add(groupPage2);
        productGroupPages.add(groupPage3);

        Mockito.when(productModel.getVariants()).thenReturn(variants);
        Mockito.when(colourVariant.getTargetProductGroupPages()).thenReturn(productGroupPages);
        final List<TargetProductGroupPageModel> looks = targetGroupPageService
                .getAllActiveLooksForProduct(productModel);
        Assert.assertNotNull(looks);
        Assert.assertEquals(groupPage2, looks.get(0));
        Assert.assertEquals(groupPage1, looks.get(1));
        Assert.assertEquals(groupPage3, looks.get(2));
    }
}
