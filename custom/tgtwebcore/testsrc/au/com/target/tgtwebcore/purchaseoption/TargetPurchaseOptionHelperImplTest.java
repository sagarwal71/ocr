/**
 * 
 */
package au.com.target.tgtwebcore.purchaseoption;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.purchase.PurchaseOptionService;
import au.com.target.tgtwebcore.purchaseoption.impl.TargetPurchaseOptionHelperImpl;
import org.junit.Assert;


/**
 * @author asingh78
 * 
 */
@UnitTest
public class TargetPurchaseOptionHelperImplTest {

    @Mock
    private PurchaseOptionService purchaseOptionService;

    @InjectMocks
    private final TargetPurchaseOptionHelper targetPurchaseOptionHelper = new TargetPurchaseOptionHelperImpl();

    @Before
    public void prepare() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getPurchaseOptionModelTargetUnknownIdentifierExceptionTest() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        BDDMockito.doThrow(new TargetUnknownIdentifierException("Error")).when(purchaseOptionService)
                .getPurchaseOptionForCode(BDDMockito.anyString());
        final PurchaseOptionModel purchaseOptionModel = targetPurchaseOptionHelper.getPurchaseOptionModel("test");
        Assert.assertNull(purchaseOptionModel);

    }

    @Test
    public void getPurchaseOptionModelTargetTargetAmbiguousIdentifierExceptionTest()
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        BDDMockito.doThrow(new TargetAmbiguousIdentifierException("Error")).when(purchaseOptionService)
                .getPurchaseOptionForCode(BDDMockito.anyString());
        final PurchaseOptionModel purchaseOptionModel = targetPurchaseOptionHelper.getPurchaseOptionModel("test");
        Assert.assertNull(purchaseOptionModel);

    }

    @Test
    public void getPurchaseOptionModelTargetTargetWithoutAnyExceptionTest()
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final PurchaseOptionModel purchaseOptionModel = BDDMockito.mock(PurchaseOptionModel.class);
        BDDMockito.doReturn(purchaseOptionModel).when(purchaseOptionService)
                .getPurchaseOptionForCode(BDDMockito.anyString());
        final PurchaseOptionModel resultPurchaseOptionModel = targetPurchaseOptionHelper
                .getPurchaseOptionModel("test");
        Assert.assertEquals(purchaseOptionModel, resultPurchaseOptionModel);

    }
}
