package au.com.target.tgtwebcore.filter;

import de.hybris.platform.media.exceptions.MediaInvalidLocationException;
import de.hybris.platform.media.exceptions.MediaNotFoundException;
import de.hybris.platform.media.services.MediaHeadersRegistry;
import de.hybris.platform.mediaweb.MediaFilter;
import de.hybris.platform.mediaweb.SecureResponseWrapper;
import de.hybris.platform.util.MediaUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;


@SuppressWarnings("deprecation")
public class TargetMediaFilter extends MediaFilter {
    private static final Logger LOG = Logger.getLogger(TargetMediaFilter.class);
    private static final String TGTMEDIA_FOLDER = "tgtproductmedia";

    private static final String MASTER_TENANT = "master";
    private static final Splitter CTX_SPLITTER = Splitter.on("|");
    private static final Pattern FOLDER_PATTERN = Pattern.compile(
            "(([\\w\\-]+?/)??)(([\\w\\-]+?/)??)((h\\w{2}/)*([0-9]+)(/[\\w\\-.]+)?(\\.\\w+?))", 2);

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException {
        if (((request instanceof HttpServletRequest)) && ((response instanceof HttpServletResponse))) {
            final HttpServletRequest httpRequest = (HttpServletRequest)request;
            final HttpServletResponse httpResponse = new SecureResponseWrapper((HttpServletResponse)response);
            try {
                doFilterMedia(httpRequest, httpResponse, getMediaContext(httpRequest));
            }
            catch (final IllegalArgumentException e) {
                sendBadRequestResponseStatus(httpResponse, e);
            }
        }
        else {
            chain.doFilter(request, response);
        }
    }

    protected void doFilterMedia(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final Iterable<String> mediaContext)
            throws IOException, ServletException {
        setCurretTenantByID(mediaContext);
        if (getMediaManager().isSecuredFolder(getContextPart(ContextPart.FOLDER, mediaContext))) {
            httpResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
        else {
            final String resourcePath = getResourcePath(httpRequest);
            if (isResourceFromClassLoader(resourcePath)) {
                loadFromClassLoader(httpResponse, resourcePath);
            }
            else {
                final String responseETag = generateETag(getContextPart(ContextPart.LOCATION, mediaContext));
                if (responseETag == null) {
                    processStandardResponse(httpRequest, httpResponse, mediaContext);
                }
                else {
                    httpResponse.setHeader("ETag", responseETag);
                    final String requestETag = httpRequest.getHeader("If-None-Match");
                    if (responseETag.equals(requestETag)) {
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("ETag [" + responseETag + "] equal to If-None-Match, sending 304");
                        }
                        readConfiguredHeaderParamsAndWriteToResponse(httpResponse);
                        httpResponse.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
                    }
                    else {
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("ETag [" + responseETag
                                    + "] is not equal to If-None-Match, sending standard response");
                        }
                        processStandardResponse(httpRequest, httpResponse, mediaContext);
                    }
                }
            }
        }
    }

    protected Iterable<String> getMediaContext(final HttpServletRequest httpRequest) {
        final String encodedMediaCtx = getLocalMediaWebUrlContextParam(httpRequest);
        if (isLegacyPrettyUrlSupport()) {
            return createLegacyLocalMediaWebUrlContext(httpRequest);
        }
        return createLocalMediawebUrlContext(encodedMediaCtx);
    }

    protected Iterable<String> createLegacyLocalMediaWebUrlContext(final HttpServletRequest httpRequest) {
        String resourcePath = getLegacyResourcePath(httpRequest);
        if (resourcePath.startsWith("/")) {
            resourcePath = resourcePath.substring(1);
        }
        return splitLegacyPath(resourcePath);
    }

    @Override
    protected Iterable<String> splitLegacyPath(final String path) {
        final String tenant = getTenantNameFromLegacyUrl(path);
        if (StringUtils.isEmpty(tenant)) {
            final List result = new ArrayList(6);
            result.add(MASTER_TENANT);
            result.add(MediaUtil.removeTrailingFileSepIfNeeded(TGTMEDIA_FOLDER));
            result.add(null);
            result.add("-");
            result.add(path);
            result.add("-");

            return result;
        }
        final String pathExp = getPathFromLegacyUrl(path);
        final Matcher matcher = FOLDER_PATTERN.matcher(pathExp);
        if (matcher.matches()) {
            final String folderQualifier = determineFolderQualifierFromPartOfPath(matcher.group(1));
            final String folderPath = determineFolderPathFromPartOfPath(matcher.group(3));
            final String locationWithRealFileName = matcher.group(5);
            final String realFileName = matcher.group(8);
            final String location = realFileName != null ? locationWithRealFileName.replace(realFileName, "")
                    : locationWithRealFileName;

            final List<String> result = new ArrayList(6);
            result.add(tenant);
            result.add(MediaUtil.removeTrailingFileSepIfNeeded(folderQualifier));
            result.add(null);
            result.add("-");
            result.add(MediaUtil.addTrailingFileSepIfNeeded(folderPath) + location);
            result.add("-");

            return result;
        }
        throw new IllegalArgumentException("invalid legacy media path '" + path + "'");
    }

    protected String getTenantNameFromLegacyUrl(final String url) {
        if (url.startsWith("sys_")) {

            final int slashAfterTenant = url.indexOf('/');
            final String tenantExpr = url.substring(0, slashAfterTenant);
            return tenantExpr.substring("sys_".length());
        }
        return StringUtils.EMPTY;
    }

    protected String getPathFromLegacyUrl(final String url) {
        return url.substring(url.indexOf('/') + 1);
    }

    protected String determineFolderQualifierFromPartOfPath(final String part) {
        return StringUtils.isEmpty(part) ? "root" : part;
    }

    protected String determineFolderPathFromPartOfPath(final String part) {
        return "root".equals(MediaUtil.removeTrailingFileSepIfNeeded(part)) ? "" : part;
    }

    protected String getLegacyResourcePath(final HttpServletRequest httpRequest) {
        String resourcePath = httpRequest.getServletPath();
        if ((resourcePath == null) || (resourcePath.trim().isEmpty())) {
            final String reqURI = httpRequest.getRequestURI();
            final String ctxPath = httpRequest.getContextPath();
            resourcePath = reqURI.replace(ctxPath, "");
        }
        return resourcePath;
    }

    protected Iterable<String> createLocalMediawebUrlContext(final String encodedMediaCtx) {
        Preconditions.checkArgument(!GenericValidator.isBlankOrNull(encodedMediaCtx),
                "incorrect media context in request");
        final Iterable<String> mediaContext = CTX_SPLITTER.split(decodeBase64(encodedMediaCtx));
        Preconditions.checkArgument(Iterables.size(mediaContext) == 6, "incorrect media context in request");
        return mediaContext;
    }

    protected String getLocalMediaWebUrlContextParam(final HttpServletRequest httpRequest) {
        return httpRequest.getParameter("context");
    }

    protected String generateETag(final String location) {
        final HashFunction md5 = Hashing.md5();
        return md5.hashUnencodedChars(location).toString();
    }

    protected String getResourcePath(final HttpServletRequest httpRequest) {
        String resourcePath = httpRequest.getServletPath();
        if (GenericValidator.isBlankOrNull(resourcePath)) {
            final String reqURI = httpRequest.getRequestURI();
            final String ctxPath = httpRequest.getContextPath();
            resourcePath = reqURI.replace(ctxPath, "");
        }
        return resourcePath;
    }

    protected void processStandardResponse(final HttpServletRequest httpRequest,
            final HttpServletResponse httpResponse,
            final Iterable<String> mediaContext)
            throws IOException, ServletException {
        final String resourcePath = getResourcePath(httpRequest);

        readConfiguredHeaderParamsAndWriteToResponse(httpResponse);
        addContentDisposition(httpRequest, httpResponse, resourcePath);
        addContentType(httpResponse, mediaContext, resourcePath);
        loadFromMediaStorage(httpResponse, mediaContext);
    }

    protected void readConfiguredHeaderParamsAndWriteToResponse(final HttpServletResponse httpResponse)
            throws UnsupportedEncodingException {
        final MediaHeadersRegistry mediaHeadersRegistry = getMediaManager().getMediaHeadersRegistry();
        if (mediaHeadersRegistry != null) {
            final Map<String, String> headerParams = mediaHeadersRegistry.getHeaders();
            for (final Map.Entry<String, String> me : headerParams.entrySet()) {
                httpResponse.setHeader(me.getKey(), me.getValue());
            }
        }
    }

    protected void addContentDisposition(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final String resourcePath) {
        if (isAddContentDisposition(httpRequest, resourcePath)) {
            httpResponse.addHeader("Content-Disposition:", " attachment; filename="
                    + getRealFileNameFromResource(resourcePath));
        }
    }

    protected boolean isAddContentDisposition(final HttpServletRequest httpRequest, final String resourcePath) {
        final boolean addContentDisposition = Boolean.parseBoolean(httpRequest
                .getParameter("attachment"));
        if (!addContentDisposition) {
            final Set<String> extensions = getAllowedExtensions("media.force.download.dialog.fileextensions");
            if ((extensions != null) && (!extensions.isEmpty())) {
                final String lowerCaseResource = resourcePath.toLowerCase();
                for (final String ext : extensions) {
                    if (lowerCaseResource.endsWith(ext)) {
                        return true;
                    }
                }
            }
        }
        return addContentDisposition;
    }

    protected boolean isResourceFromClassLoader(final String resourcePath) {
        return (resourcePath != null) && (resourcePath.contains("/fromjar"));
    }

    protected String getRealFileNameFromResource(final String resourcePath) {
        final int index = resourcePath.lastIndexOf('/');
        return index >= 0 ? resourcePath.substring(index + 1) : resourcePath;
    }

    protected void loadFromClassLoader(final HttpServletResponse httpResponse, final String resourcePath)
            throws IOException {
        final String resourceName = resourcePath.substring(resourcePath.indexOf("/fromjar") + (1 + "fromjar".length()));
        if (LOG.isDebugEnabled()) {
            LOG.debug("Trying to load resource '" + resourceName + "' from classloader.");
        }
        if (isDeniedByExtensionForClassloader(resourceName)) {
            httpResponse.setContentType("text/plain");
            httpResponse
                    .getOutputStream()
                    .println(
                            "not allowed to load media '"
                                    + resourceName
                                    + "' from classloader. Check parameter "
                                    + "media.allowed.extensions.for.ClassLoader"
                                    +
                                    " in advanced.properties to change the file extensions (e.g. *.gif) that are allowed to download.");
        }
        else {
            InputStream inputStream = null;
            try {
                inputStream = getResourceAsStream(resourceName);
                if (inputStream == null) {
                    httpResponse.setContentType("text/plain");
                    httpResponse.getOutputStream().println("file '" + resourceName + "' not found!");
                }
                else {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Loading resource '" + resourceName + "' from classloader.");
                    }
                    IOUtils.copy(inputStream, httpResponse.getOutputStream());
                }
            }
            finally {
                IOUtils.closeQuietly(inputStream);
            }
        }
    }

    protected boolean isDeniedByExtensionForClassloader(final String resourceName) {
        final Set<String> extensions = getAllowedExtensions("media.allowed.extensions.for.ClassLoader");
        if ((extensions == null) || (extensions.isEmpty())) {
            return false;
        }
        final String check = resourceName.toLowerCase();
        for (final String ext : extensions) {
            if (check.endsWith(ext)) {
                return false;
            }
        }
        return true;
    }


    protected void loadFromMediaStorage(final HttpServletResponse httpResponse, final Iterable<String> mediaContext)
            throws IOException {
        InputStream inputStream = null;
        try {
            final String folderQualifier = getContextPart(ContextPart.FOLDER, mediaContext);
            final String size = getContextPart(ContextPart.SIZE, mediaContext);
            final String location = getContextPart(ContextPart.LOCATION, mediaContext);
            final String locationHash = getContextPart(ContextPart.LOCATION_HASH, mediaContext);
            if (!"-".equals(locationHash)) {
                verifyHashForLocation(folderQualifier, location, locationHash);
            }
            if (size != null) {
                httpResponse.setContentLength(Integer.parseInt(size));
            }
            inputStream = getMediaAsStream(folderQualifier, location);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Loading resource [location: " + location + "] from media storage.");
            }
            IOUtils.copy(inputStream, httpResponse.getOutputStream());
        }
        catch (final MediaInvalidLocationException e) {
            sendForbiddenResponseStatus(httpResponse, e);
        }
        catch (final MediaNotFoundException e) {
            sendResourceNotFoundResponseStatus(httpResponse, e);
        }
        catch (final Exception e) {
            sendBadRequestResponseStatus(httpResponse, e);
        }
        finally {
            IOUtils.closeQuietly(inputStream);
            unsetCurrentTenant();
        }
    }


    protected void verifyHashForLocation(final String folderQualifier, final String location, final String storedHash) {
        getMediaManager().verifyMediaHashForLocation(folderQualifier, location, storedHash);
    }

    protected InputStream getMediaAsStream(final String folderQualifier, final String location) {
        return getMediaManager().getMediaAsStream(folderQualifier, location);
    }

    protected String decodeBase64(final String value) {
        String decodedValue = "";
        if (StringUtils.isNotBlank(value)) {
            try {
                decodedValue = new String(new Base64(-1, null, true).decode(value));
            }
            catch (final Exception localException) {
                throw new IllegalArgumentException("Cannot decode base32 coded string: " + value);
            }
        }
        return decodedValue;
    }

    protected String getContextPart(final ContextPart contextPart, final Iterable<String> mediaContext) {
        return Iterables.get(mediaContext, contextPart.getPartNumber());
    }

    protected static enum ContextPart {
        TENANT(0), FOLDER(1), SIZE(2), MIME(3), LOCATION(4), LOCATION_HASH(5);

        protected final int partNumber;

        private ContextPart(final int partNumber) {
            this.partNumber = partNumber;
        }

        public int getPartNumber() {
            return this.partNumber;
        }
    }

    protected void sendForbiddenResponseStatus(final HttpServletResponse httpResponse, final Exception exception) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Access forbidden for given media", exception);
        }
        httpResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
    }

    protected void sendBadRequestResponseStatus(final HttpServletResponse httpResponse, final Exception exception) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("The request sent by the client was syntactically incorrect", exception);
        }
        httpResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }

    protected void sendResourceNotFoundResponseStatus(final HttpServletResponse httpResponse,
            final Exception exception) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Requested resource not found", exception);
        }
        httpResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }

    protected Set<String> getAllowedExtensions(final String configParameter) {
        final String str = getConfig().getParameter(configParameter);
        final Set<String> extensions;
        if (StringUtils.isBlank(str)) {
            extensions = Collections.emptySet();
        }
        else {
            final Set<String> set = new LinkedHashSet();
            for (final StringTokenizer tok = new StringTokenizer(str, ",;"); tok.hasMoreTokens();) {
                final String strElement = tok.nextToken().toLowerCase().trim();
                if (strElement.length() > 0) {
                    set.add(strElement);
                }
            }
            extensions = Collections.unmodifiableSet(set);
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug(configParameter + ": Supported media extensions: " + extensions.toString());
        }
        return extensions;
    }

}
