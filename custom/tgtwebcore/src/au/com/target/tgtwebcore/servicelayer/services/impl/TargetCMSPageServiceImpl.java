package au.com.target.tgtwebcore.servicelayer.services.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSPageService;
import de.hybris.platform.core.model.type.ComposedTypeModel;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.CollectionUtils;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtcore.product.BrandService;
import au.com.target.tgtcore.product.TargetSizeTypeService;
import au.com.target.tgtwebcore.model.cms2.pages.BrandPageModel;
import au.com.target.tgtwebcore.model.cms2.pages.SizePageModel;
import au.com.target.tgtwebcore.servicelayer.data.TargetCMSDataFactory;
import au.com.target.tgtwebcore.servicelayer.services.TargetCMSPageService;


public class TargetCMSPageServiceImpl extends DefaultCMSPageService implements TargetCMSPageService {

    private BrandService brandService;
    private TargetCMSDataFactory targetCMSDataFactory;
    private TargetSizeTypeService targetSizeTypeService;

    @Override
    @SuppressWarnings("deprecation")
    public BrandPageModel getPageForBrand(final BrandModel brandModel) throws CMSItemNotFoundException {
        final BrandPageModel page = (BrandPageModel)getSinglePage(BrandPageModel._TYPECODE);
        if (page != null) {
            LOG.debug("Only one BrandPage for brand [" + brandModel.getCode()
                    + "] found. Considering this as default.");
            return page;
        }
        final ComposedTypeModel type = getTypeService().getComposedTypeForCode(BrandPageModel._TYPECODE);
        final Collection<CatalogVersionModel> versions = getCatalogVersionService().getSessionCatalogVersions();
        final RestrictionData data = targetCMSDataFactory.createRestrictionData(brandModel);
        final Collection<AbstractPageModel> pages = getCmsPageDao().findAllPagesByTypeAndCatalogVersions(type, versions);
        final Collection<AbstractPageModel> result = getCmsRestrictionService().evaluatePages(pages, data);
        if (CollectionUtils.isEmpty(result)) {
            throw new CMSItemNotFoundException("No page for brand [" + brandModel.getCode() + "] found.");
        }
        else if (result.size() > 1) {
            LOG.warn("More than one page found for brand [" + brandModel.getCode() + "]. Returning default.");
        }
        return (BrandPageModel)result.iterator().next();
    }

    @Override
    public BrandPageModel getPageForBrandCode(final String brandCode) throws CMSItemNotFoundException {
        final BrandModel brand;
        try {
            brand = brandService.getBrandForCode(brandCode);
        }
        catch (final Exception e) {
            throw new CMSItemNotFoundException("Could not find brand with code [" + brandCode + "]:"
                    + e.getMessage(), e);
        }
        return getPageForBrand(brand);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebcore.servicelayer.services.TargetCMSPageService#getPageForSizeCode(java.lang.String)
     */
    @Override
    public SizePageModel getPageForSizeCode(final String sizeCode) throws CMSItemNotFoundException {
        final SizeTypeModel sizeType;
        try {
            sizeType = targetSizeTypeService.findSizeTypeForCode(sizeCode);
        }
        catch (final Exception e) {
            throw new CMSItemNotFoundException("Could not find SizeType with code [" + sizeCode + "]:"
                    + e.getMessage(), e);
        }
        return getPageForSize(sizeType);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebcore.servicelayer.services.TargetCMSPageService#getPageForSize(au.com.target.tgtcore.model.SizeTypeModel)
     */
    @SuppressWarnings("deprecation")
    @Override
    public SizePageModel getPageForSize(final SizeTypeModel sizeTypeModel) throws CMSItemNotFoundException {
        final SizePageModel page = (SizePageModel)getSinglePage(SizePageModel._TYPECODE);
        if (page != null) {
            LOG.debug("Only one SizePage for Size [" + page.getPk()
                    + "] found. Considering this as default.");
            return page;
        }
        final ComposedTypeModel type = getTypeService().getComposedTypeForCode(SizePageModel._TYPECODE);
        final Set<CatalogVersionModel> versions = getCatalogService().getSessionCatalogVersions();
        if (null != sizeTypeModel) {
            final RestrictionData data = targetCMSDataFactory.createRestrictionData(sizeTypeModel);

            final Collection<AbstractPageModel> pages = getCmsPageDao().findAllPagesByTypeAndCatalogVersions(type,
                    versions);
            final Collection<AbstractPageModel> result = getCmsRestrictionService().evaluatePages(pages, data);

            if (CollectionUtils.isEmpty(result)) {
                throw new CMSItemNotFoundException("No page for brand [" + sizeTypeModel.getCode() + "] found.");
            }
            else if (result.size() > 1) {
                LOG.warn("More than one page found for brand [" + sizeTypeModel.getCode() + "]. Returning default.");
            }
            return (SizePageModel)result.iterator().next();
        }
        else {
            final Collection<AbstractPageModel> result = getCmsPageDao().findDefaultPageByTypeAndCatalogVersions(type,
                    versions);
            if (CollectionUtils.isEmpty(result)) {
                throw new CMSItemNotFoundException("No default page found");
            }
            return (SizePageModel)result.iterator().next();
        }
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtwebcore.servicelayer.services.TargetCMSPageService#getPageForIdWithRestrictions(java.lang.String)
     */
    @SuppressWarnings("deprecation")
    @Override
    public ContentPageModel getPageForIdWithRestrictions(final String id) throws CMSItemNotFoundException {
        final AbstractPageModel page = getPageForId(id);
        if (!(page instanceof ContentPageModel)) {
            throw new CMSItemNotFoundException(
                    "getPageForIdWithRestrictions: Page not found or is not a ContentPageModel, id=" + id);
        }

        final Collection<AbstractPageModel> result = getCmsRestrictionService()
                .evaluatePages(Collections.singletonList(page), null);
        if (result.isEmpty()) {
            throw new CMSItemNotFoundException(
                    "getPageForIdWithRestrictions: Page not visible after evaluating restrictions, id=" + id);
        }
        return (ContentPageModel)result.iterator().next();
    }

    /**
     * @param targetSizeTypeService
     *            the targetSizeTypeService to set
     */
    @Required
    public void setTargetSizeTypeService(final TargetSizeTypeService targetSizeTypeService) {
        this.targetSizeTypeService = targetSizeTypeService;
    }

    /**
     * @param brandService
     *            the brandService to set
     */
    @Required
    public void setBrandService(final BrandService brandService) {
        this.brandService = brandService;
    }

    /**
     * @param targetCMSDataFactory
     *            the targetCMSDataFactory to set
     */
    @Required
    public void setTargetCMSDataFactory(final TargetCMSDataFactory targetCMSDataFactory) {
        this.targetCMSDataFactory = targetCMSDataFactory;
    }

}
