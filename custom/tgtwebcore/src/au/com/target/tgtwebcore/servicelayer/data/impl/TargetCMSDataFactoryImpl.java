package au.com.target.tgtwebcore.servicelayer.data.impl;

import de.hybris.platform.cms2.servicelayer.data.impl.DefaultCMSDataFactory;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtwebcore.servicelayer.data.TargetCMSDataFactory;
import au.com.target.tgtwebcore.servicelayer.data.TargetCMSRestrictionData;


public class TargetCMSDataFactoryImpl extends DefaultCMSDataFactory implements TargetCMSDataFactory {

    @Override
    public TargetCMSRestrictionData createRestrictionData(final BrandModel brandModel) {
        final TargetCMSRestrictionData data = new TargetCMSRestrictionDataImpl();
        data.setBrand(brandModel);
        return data;
    }

    @Override
    public TargetCMSRestrictionData createRestrictionData(final SizeTypeModel sizeTypeModel) {
        final TargetCMSRestrictionData data = new TargetCMSRestrictionDataImpl();
        data.setSizeType(sizeTypeModel);
        return data;
    }

}
