package au.com.target.tgtwebcore.servicelayer.daos.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.model.contents.contentslot.ContentSlotModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.servicelayer.daos.impl.DefaultCMSPageDao;

import java.util.Collection;
import java.util.Collections;

import org.apache.commons.collections.CollectionUtils;

/**
 * TargetCMSPageDao
 */
public class TargetCMSPageDao extends DefaultCMSPageDao {
  
  /**
   * Gets pages by Contenslots. This custom method handles the case when contentslots is empty or null.
   *
   * @param contentSlots
   * @param catalogVersions
   * @return Pages
   */
  @Override
  public Collection<AbstractPageModel> findPagesByContentSlots(final Collection<ContentSlotModel> contentSlots, final Collection<CatalogVersionModel> catalogVersions) {

    if(CollectionUtils.isEmpty(contentSlots)){
      return Collections.emptyList();
    }

    return super.findPagesByContentSlots(contentSlots, catalogVersions);
  }

}
