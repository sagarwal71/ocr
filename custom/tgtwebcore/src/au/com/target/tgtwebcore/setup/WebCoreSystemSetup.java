package au.com.target.tgtwebcore.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.validation.services.ValidationService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import au.com.target.tgtwebcore.constants.TgtwebcoreConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 * 
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = TgtwebcoreConstants.EXTENSIONNAME)
public class WebCoreSystemSetup extends AbstractSystemSetup {

    public static final String IMPORT_ESSENTIAL_CMS_CONTENT = "importEssentialCMSContent";
    public static final String IMPORT_SYNC_CONTENT_CATALOGS = "contentCatalogs";
    public static final String TARGET = "target";

    /**
     * This method will be called by system creator during initialization and system update. Be sure that this method
     * can be called repeatedly.
     * 
     * @param context
     *            the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
    public void createEssentialData(final SystemSetupContext context) {

        importImpexFile(context, "/tgtwebcore/import/common/taxInvoice.impex");

        final boolean importEssentialCMSContent = getBooleanSystemSetupParameter(context, IMPORT_ESSENTIAL_CMS_CONTENT);
        if (importEssentialCMSContent) {

            final AbstractImportCatalogAwareDataStrategy target = getDataStrategy(context);
            target.importEssentialCMSData();

            final ValidationService validation = getBeanForName("validationService");
            validation.reloadValidationEngine();
        }

    }

    @SystemSetup(type = Type.PROJECT, process = Process.INIT)
    public void createProjectData(final SystemSetupContext context) {
        importImpexFile(context, "/tgtwebcore/import/common/akamai-cache-configuration.impex", true);
        importImpexFile(context, "/tgtwebcore/import/common/size-constraints.impex", true);
        importImpexFile(context, "/tgtwebcore/import/common/pattern-constraints.impex", true);

        final ValidationService validation = getBeanForName("validationService");
        validation.reloadValidationEngine();
    }

    private AbstractImportCatalogAwareDataStrategy getDataStrategy(final SystemSetupContext context) {
        return new AbstractImportCatalogAwareDataStrategy(context) {

            @Override
            protected String getProductCatalogName() {
                return TARGET;
            }

            @Override
            protected List<String> getContentCatalogNames() {
                return Arrays.asList(TARGET);
            }

            @Override
            protected List<String> getStoreNames() {
                return Arrays.asList(TARGET);
            }

        };
    }

    protected <T> T getBeanForName(final String name) {
        return (T)Registry.getApplicationContext().getBean(name);
    }

    @Override
    @SystemSetupParameterMethod
    public List<SystemSetupParameter> getInitializationOptions() {
        final List<SystemSetupParameter> params = new ArrayList<>();
        params.add(
                createBooleanSystemSetupParameter(IMPORT_ESSENTIAL_CMS_CONTENT, "Import Essential CMS Content", true));
        params.add(createBooleanSystemSetupParameter(IMPORT_SYNC_CONTENT_CATALOGS,
                "Execute a Sync on the Content Catalog", true));

        return params;
    }

    private abstract class AbstractImportCatalogAwareDataStrategy {

        private final SystemSetupContext context;

        public AbstractImportCatalogAwareDataStrategy(final SystemSetupContext context) {
            this.context = context;
        }

        protected abstract String getProductCatalogName();

        protected abstract List<String> getContentCatalogNames();

        protected abstract List<String> getStoreNames();

        private boolean isSyncCatalog() {
            return getBooleanSystemSetupParameter(context, IMPORT_SYNC_CONTENT_CATALOGS);
        }

        public void importEssentialCMSData() {
            createCommon();
            for (final String contentCatalogName : getContentCatalogNames()) {
                createContentCatalog(contentCatalogName);
                createContentCatalogSyncJob(context, contentCatalogName + "ContentCatalog");
            }

            for (final String storeName : getStoreNames()) {
                createStore(storeName);
            }

            for (final String contentCatalogName : getContentCatalogNames()) {
                createContentCatalogItem(contentCatalogName);
            }

            createCockpitItemTemplates();

            for (final String contentCatalogName : getContentCatalogNames()) {
                if (isSyncCatalog()) {
                    syncContentCatalog(contentCatalogName);
                }
            }

        }

        /**
         * fires content catalog with synchronization with respect for a depends on CatalogVersionSyncJob
         * 
         */
        private void syncContentCatalog(final String contentCatalogName) {
            executeCatalogSyncJob(context, contentCatalogName + "ContentCatalog");
        }


        private void createContentCatalog(final String contentCatalogName) {
            logInfo(context, "Begin importing catalog [" + contentCatalogName + "]");
            final String contentPath = "/tgtwebcore/import/contentCatalogs/" + contentCatalogName
                    + "ContentCatalog/";

            importImpexFile(context, contentPath + "catalog.impex", true);
        }

        private void createStore(final String storeName) {
            logInfo(context, "Begin importing store [" + storeName + "]");

            importImpexFile(context, "/tgtwebcore/import/stores/" + storeName + "/site.impex");

            logInfo(context, "Done importing store [" + storeName + "]");
        }

        private void createCockpitItemTemplates() {
            logInfo(context, "Begin importing cockpit item templates");
            importImpexFile(context, "/tgtwebcore/import/cockpits/cmscockpit/cmscockpit-itemtemplates.impex", false);
            logInfo(context, "Done importing cockpit item templates");
        }

        /**
         * fires common import
         */
        private void createContentCatalogItem(final String contentCatalogName) {
            logInfo(context, "Begin importing Item for catalog [" + contentCatalogName + "]");

            final String contentPath = "/tgtwebcore/import/contentCatalogs/" + contentCatalogName
                    + "ContentCatalog/";

            importImpexFile(context, contentPath + "cms-page-templates.impex", true);
            importImpexFile(context, contentPath + "navigation.impex", true);
            importImpexFile(context, contentPath + "cms-page-support-modals.impex", true);
            importImpexFile(context, contentPath + "email-content.impex", true);
            importImpexFile(context, contentPath + "sms-content.impex", true);
            importImpexFile(context, contentPath + "size-chart-config.impex", false);
            importImpexFile(context, contentPath + "tax-invoice-media.impex", true);

            // Kiosk
            importImpexFile(context, contentPath + "kiosk/cms-kiosk.impex", true);

            //mobile-App
            importImpexFile(context, contentPath + "mobile-app/cms-mobile-app-pages.impex", true);
            importImpexFile(context, contentPath + "mobile-app/non-mobile-app-cms-sales-channel-restriction.impex",
                    true);
            importImpexFile(context, contentPath + "shopTheLook-page.impex",
                    true);

            logInfo(context, "Done importing common");
        }

        private void createCommon() {
            logInfo(context, "Begin importing common Item for catalog ");
            final String contentPath = "/tgtwebcore/import/common/";
            importImpexFile(context, contentPath + "size-chart.impex", true);
        }
    }
}
