/**
 * 
 */
package au.com.target.tgtwebcore.fluent;

import java.util.List;


/**
 * @author mgazal
 *
 */
public class StockRequestDto {

    private List<String> variants;

    private List<String> deliveryTypes;

    private List<String> locations;

    private String baseProductCode;

    private String variantCode;

    /**
     * @return the variants
     */
    public List<String> getVariants() {
        return variants;
    }

    /**
     * @param variants
     *            the variants to set
     */
    public void setVariants(final List<String> variants) {
        this.variants = variants;
    }

    /**
     * @return the deliveryTypes
     */
    public List<String> getDeliveryTypes() {
        return deliveryTypes;
    }

    /**
     * @param deliveryTypes
     *            the deliveryTypes to set
     */
    public void setDeliveryTypes(final List<String> deliveryTypes) {
        this.deliveryTypes = deliveryTypes;
    }

    /**
     * @return the locations
     */
    public List<String> getLocations() {
        return locations;
    }

    /**
     * @param locations
     *            the locations to set
     */
    public void setLocations(final List<String> locations) {
        this.locations = locations;
    }

    /**
     * @return the baseProductCode
     */
    public String getBaseProductCode() {
        return baseProductCode;
    }

    /**
     * @param baseProductCode
     *            the baseProductCode to set
     */
    public void setBaseProductCode(final String baseProductCode) {
        this.baseProductCode = baseProductCode;
    }

    /**
     * @return the variantCode
     */
    public String getVariantCode() {
        return variantCode;
    }

    /**
     * @param variantCode
     *            the variantCode to set
     */
    public void setVariantCode(final String variantCode) {
        this.variantCode = variantCode;
    }

}
