/**
 * 
 */
package au.com.target.tgtwebcore.looks.service.impl;

import de.hybris.platform.cms2.enums.CmsApprovalStatus;
import de.hybris.platform.commerceservices.util.AbstractComparator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtwebcore.looks.service.TargetProductGroupService;
import au.com.target.tgtwebcore.model.cms2.pages.TargetProductGroupPageModel;


/**
 * @author Nandini
 *
 */
public class TargetProductGroupServiceImpl implements TargetProductGroupService {

    @Override
    public List<TargetProductGroupPageModel> getAllActiveLooksForProduct(final ProductModel productModel) {

        Assert.notNull(productModel, "Product Model cannot be null");
        final List<TargetProductGroupPageModel> productGroupPages = new ArrayList<>();

        if (CollectionUtils.isNotEmpty((productModel.getVariants()))) {
            final Collection<VariantProductModel> variants = productModel.getVariants();
            for (final VariantProductModel variant : variants) {
                if (variant instanceof TargetColourVariantProductModel) {
                    addLooksForCurrentColourVariant(variant, productGroupPages);
                }
            }
        }
        if (CollectionUtils.isNotEmpty(productGroupPages)) {
            Collections.sort(productGroupPages, TargetProductGroupPageComparator.INSTANCE);
        }

        return productGroupPages;
    }

    /**
     * Add Looks for the current colourVariant
     * 
     * @param variant
     * @param productGroupPages
     */
    private void addLooksForCurrentColourVariant(final VariantProductModel variant,
            final List<TargetProductGroupPageModel> productGroupPages) {

        if (CollectionUtils.isNotEmpty(variant.getTargetProductGroupPages())) {
            for (final TargetProductGroupPageModel groupPage : variant.getTargetProductGroupPages()) {
                if (groupPage.getApprovalStatus().equals(CmsApprovalStatus.APPROVED)) {
                    productGroupPages.add(groupPage);
                }
            }
        }
    }

    private static class TargetProductGroupPageComparator extends AbstractComparator<TargetProductGroupPageModel> {
        private static final TargetProductGroupPageComparator INSTANCE = new TargetProductGroupPageComparator();

        @Override
        protected int compareInstances(final TargetProductGroupPageModel groupPage1,
                final TargetProductGroupPageModel groupPage2) {
            // sort Looks by TargetProductGroupPage creation date.
            final int comp = compareValues(groupPage2.getCreationtime(), groupPage1.getCreationtime());
            return comp;
        }
    }

}
