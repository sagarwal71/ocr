/**
 * 
 */
package au.com.target.tgtwebcore.userreviews.data;

/**
 * @author knemalik
 * 
 */
public class TargetUserReviewResponse {

    private String content;
    private boolean status;
    private String validationErrorMessage;

    /**
     * @return the validationErrorMessage
     */
    public String getValidationErrorMessage() {
        return validationErrorMessage;
    }

    /**
     * @param validationErrorMessage
     *            the validationErrorMessage to set
     */
    public void setValidationErrorMessage(final String validationErrorMessage) {
        this.validationErrorMessage = validationErrorMessage;
    }

    /**
     * @return the status
     */
    public boolean getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(final boolean status) {
        this.status = status;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content
     *            the content to set
     */
    public void setContent(final String content) {
        this.content = content;
    }


}
