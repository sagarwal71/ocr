/**
 *
 */
package au.com.target.tgtwebcore.userreviews.impl;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.regex.Pattern;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtwebcore.userreviews.TargetUserReviewService;
import au.com.target.tgtwebcore.userreviews.data.TargetUserReviewRequest;
import au.com.target.tgtwebcore.userreviews.data.TargetUserReviewResponse;

import com.bazaarvoice.seo.sdk.BVManagedUIContent;
import com.bazaarvoice.seo.sdk.BVUIContent;
import com.bazaarvoice.seo.sdk.config.BVClientConfig;
import com.bazaarvoice.seo.sdk.config.BVConfiguration;
import com.bazaarvoice.seo.sdk.config.BVSdkConfiguration;
import com.bazaarvoice.seo.sdk.model.BVParameters;
import com.bazaarvoice.seo.sdk.model.ContentType;
import com.bazaarvoice.seo.sdk.model.SubjectType;
import com.bazaarvoice.seo.sdk.util.BVMessageUtil;
import com.bazaarvoice.seo.sdk.validation.BVDefaultValidator;
import com.bazaarvoice.seo.sdk.validation.BVValidator;


/**
 * @author knemalik
 * 
 */
public class TargetBazaarvoiceServiceImpl implements TargetUserReviewService {

    private static final Logger LOG = Logger.getLogger(TargetBazaarvoiceServiceImpl.class);
    private static final String BV_SDK_ENABLED = "tgtwebcore.bvseo.sdk.enabled";
    private static final String EXECUTION_TIMEOUT_BOT_WARNING = "EXECUTION_TIMEOUT_BOT is less than the minimum value";
    private static final String EXECUTION_TIMEOUT_WARNING = "EXECUTION_TIMEOUT is set to 0 ms; JavaScript-only Display";
    private static final String EXECUTION_TIMEOUT_ERROR = "Execution timed out, exceeded";
    private static final String EXECUTION_TIMEOUT_BOT_ERROR = "Execution timed out for search bot, exceeded";
    private String rootFolder;
    private String staging;
    private String cloudKey;
    private String executionTimeout;
    private String executionTimeoutBot;
    private String proxyHost;
    private String proxyPort;
    private String sslEnabled;
    private String crawlerAgentPattern;

    private ConfigurationService configurationService;

    /* (non-Javadoc)
     * @see au.com.target.tgtwebcore.userreviews.TargetGetContentService#getBVContent(au.com.target.tgtwebcore.userreviews.data.TargetBVParameters)
     */
    @Override
    public TargetUserReviewResponse getUserReviewsContent(final TargetUserReviewRequest targetBVParams) {
        final TargetUserReviewResponse targetUserReviewResponse = new TargetUserReviewResponse();
        final String seoSdkEnabled = configurationService.getConfiguration().getString(BV_SDK_ENABLED);
        if (!BooleanUtils.toBoolean(seoSdkEnabled)) {
            LOG.debug("Bazaarvoice SDK enabled : False");
            targetUserReviewResponse.setStatus(false);
            return targetUserReviewResponse;
        }
        else if (!showUserAgentSEOContent(targetBVParams)) {
            LOG.debug("Not a valid user agent : False");
            targetUserReviewResponse.setStatus(false);
            return targetUserReviewResponse;
        }

        LOG.debug("Starting Bazaarvoice SEO to get user generated content");
        final BVParameters bvParam = new BVParameters();
        bvParam.setUserAgent(targetBVParams.getUserAgent());
        bvParam.setBaseURI(targetBVParams.getBaseURI());
        bvParam.setPageURI(targetBVParams.getPageURI());
        bvParam.setContentType(ContentType.REVIEWS);
        bvParam.setSubjectType(SubjectType.PRODUCT);
        bvParam.setSubjectId(targetBVParams.getProduct());
        final BVConfiguration bvConfig = createBVConfig(seoSdkEnabled);
        final String validationError = getBVValidator().validate(bvConfig, bvParam);
        if (StringUtils.isNotBlank(validationError))
        {
            targetUserReviewResponse.setStatus(false);
            targetUserReviewResponse.setValidationErrorMessage(validationError);
            return targetUserReviewResponse;
        }

        final BVUIContent bvOutput = getBVManagedUIContentOutput(bvConfig);
        final String content = bvOutput.getContent(bvParam);
        final String resultMessage = getBVErrorMessage(content);
        if (StringUtils.isNotEmpty(resultMessage)) {
            LOG.info("Bazaarvoice returned with message : " + resultMessage + " for product code "
                    + targetBVParams.getProduct());
            LOG.debug(content);
            targetUserReviewResponse.setStatus(false);
            return targetUserReviewResponse;
        }

        if (StringUtils.isBlank(content)) {
            LOG.info("Bazaarvoice returned content  is blank or null.");
            targetUserReviewResponse.setStatus(false);
            return targetUserReviewResponse;
        }
        else {


            targetUserReviewResponse.setContent(content);
            targetUserReviewResponse.setStatus(true);
            LOG.debug("Response has been setup with Bazaarvoice content successfully");
            return targetUserReviewResponse;
        }

    }

    protected BVValidator getBVValidator() {
        return new BVDefaultValidator();
    }

    /**
     * Get Bazaarvoice error messages from content String.
     * 
     * @param content
     * @return String
     */
    protected String getBVErrorMessage(final String content) {
        if (StringUtils.contains(content, EXECUTION_TIMEOUT_BOT_WARNING)) {
            return EXECUTION_TIMEOUT_BOT_WARNING;
        }
        else if (StringUtils.contains(content, EXECUTION_TIMEOUT_ERROR)) {
            return EXECUTION_TIMEOUT_ERROR;
        }
        else if (StringUtils.contains(content, EXECUTION_TIMEOUT_BOT_ERROR)) {
            return EXECUTION_TIMEOUT_BOT_ERROR;
        }
        else if (StringUtils.contains(content, EXECUTION_TIMEOUT_WARNING)) {
            return EXECUTION_TIMEOUT_WARNING;
        }
        else if (StringUtils.contains(content, BVMessageUtil.getMessage("ERR0000"))) {
            return BVMessageUtil.getMessage("ERR0000");
        }
        else if (StringUtils.contains(content, BVMessageUtil.getMessage("ERR0002"))) {
            return BVMessageUtil.getMessage("ERR0002");
        }
        else if (StringUtils.contains(content, BVMessageUtil.getMessage("ERR0003"))) {
            return BVMessageUtil.getMessage("ERR0003");
        }
        else if (StringUtils.contains(content, BVMessageUtil.getMessage("ERR0004"))) {
            return BVMessageUtil.getMessage("ERR0004");
        }
        else if (StringUtils.contains(content, BVMessageUtil.getMessage("ERR0005"))) {
            return BVMessageUtil.getMessage("ERR0005");
        }
        else if (StringUtils.contains(content, BVMessageUtil.getMessage("ERR0006"))) {
            return BVMessageUtil.getMessage("ERR0006");
        }
        else if (StringUtils.contains(content, BVMessageUtil.getMessage("ERR0008"))) {
            return BVMessageUtil.getMessage("ERR0008");
        }
        else if (StringUtils.contains(content, BVMessageUtil.getMessage("ERR0009"))) {
            return BVMessageUtil.getMessage("ERR0009");
        }
        else if (StringUtils.contains(content, BVMessageUtil.getMessage("ERR0012"))) {
            return BVMessageUtil.getMessage("ERR0012");
        }
        else if (StringUtils.contains(content, BVMessageUtil.getMessage("ERR0013"))) {
            return BVMessageUtil.getMessage("ERR0013");
        }
        else if (StringUtils.contains(content, BVMessageUtil.getMessage("ERR0019"))) {
            return BVMessageUtil.getMessage("ERR0019");
        }
        else if (StringUtils.contains(content, BVMessageUtil.getMessage("ERR0020"))) {
            return BVMessageUtil.getMessage("ERR0020");
        }
        else if (StringUtils.contains(content, BVMessageUtil.getMessage("ERR0024"))) {
            return BVMessageUtil.getMessage("ERR0024");
        }

        //Logging content message to identify whether there are new error messages in BV SDK.
        LOG.debug("Logging content : " + content);

        return StringUtils.EMPTY;
    }

    /**
     * Establish a new BVConfiguration
     * 
     * @param seoSdkEnabled
     * @return BVConfiguration
     */
    protected BVConfiguration createBVConfig(final String seoSdkEnabled) {
        LOG.debug("Creating Bazaarvoice configuration");
        final BVConfiguration bvConfig = new BVSdkConfiguration();

        if (!StringUtils.isBlank(rootFolder)) {
            bvConfig.addProperty(BVClientConfig.BV_ROOT_FOLDER, rootFolder);
        }

        if (!StringUtils.isBlank(cloudKey)) {
            bvConfig.addProperty(BVClientConfig.CLOUD_KEY, cloudKey);
        }

        if (!StringUtils.isBlank(staging)) {
            bvConfig.addProperty(BVClientConfig.STAGING, staging);
        }

        if (!StringUtils.isBlank(seoSdkEnabled)) {
            bvConfig.addProperty(BVClientConfig.SEO_SDK_ENABLED, seoSdkEnabled);
        }

        if (!StringUtils.isBlank(executionTimeout)) {
            bvConfig.addProperty(BVClientConfig.EXECUTION_TIMEOUT, executionTimeout);
        }

        if (!StringUtils.isBlank(executionTimeoutBot)) {
            bvConfig.addProperty(BVClientConfig.EXECUTION_TIMEOUT_BOT, executionTimeoutBot);
        }

        if (!StringUtils.isBlank(crawlerAgentPattern)) {
            bvConfig.addProperty(BVClientConfig.CRAWLER_AGENT_PATTERN, crawlerAgentPattern);
        }

        if (!StringUtils.isBlank(proxyHost)) {
            bvConfig.addProperty(BVClientConfig.PROXY_HOST, proxyHost);
        }

        if (!StringUtils.isBlank(proxyPort)) {
            bvConfig.addProperty(BVClientConfig.PROXY_PORT, proxyPort);
        }

        if (!StringUtils.isBlank(sslEnabled)) {
            bvConfig.addProperty(BVClientConfig.SSL_ENABLED, sslEnabled);
        }

        return bvConfig;
    }

    /**
     * Check whether the user agent is a bot by using code in
     * https://github.com/bazaarvoice/seo_sdk_java/blob/master/src
     * /main/java/com/bazaarvoice/seo/sdk/BVUIContentServiceProvider.java
     * 
     * @param bvParameters
     * @return boolean
     */
    private boolean showUserAgentSEOContent(final TargetUserReviewRequest bvParameters) {
        if (bvParameters == null || bvParameters.getUserAgent() == null) {
            return false;
        }

        final StringBuilder modifiedCrawlerAgentPattern = new StringBuilder();
        if (!StringUtils.isBlank(crawlerAgentPattern)) {
            modifiedCrawlerAgentPattern.append(".*(").append(crawlerAgentPattern).append(").*");
        }

        final Pattern pattern = Pattern.compile(modifiedCrawlerAgentPattern.toString(), Pattern.CASE_INSENSITIVE);
        LOG.debug("userAgent is : " + bvParameters.getUserAgent());

        return (pattern.matcher(bvParameters.getUserAgent()).matches());
    }

    /**
     * Get the BVManagedUIContent using BV configuration
     * 
     * @param bvConfig
     * @return BVManagedUIContent
     */
    protected BVUIContent getBVManagedUIContentOutput(final BVConfiguration bvConfig) {
        LOG.debug("Getting Bazaarvoice Managed UIContent");
        return new BVManagedUIContent(bvConfig);
    }

    /**
     * @param executionTimeoutBot
     *            the executionTimeoutBot to set
     */
    @Required
    public void setExecutionTimeoutBot(final String executionTimeoutBot) {
        this.executionTimeoutBot = executionTimeoutBot;
    }

    /**
     * @param rootFolder
     *            the rootFolder to set
     */
    @Required
    public void setRootFolder(final String rootFolder) {
        this.rootFolder = rootFolder;
    }

    /**
     * @param cloudKey
     *            the cloudKey to set
     */
    @Required
    public void setCloudKey(final String cloudKey) {
        this.cloudKey = cloudKey;
    }


    /**
     * @param executionTimeout
     *            the executionTimeout to set
     */
    @Required
    public void setExecutionTimeout(final String executionTimeout) {
        this.executionTimeout = executionTimeout;
    }

    /**
     * @param proxyHost
     *            the proxyHost to set
     */
    @Required
    public void setProxyHost(final String proxyHost) {
        this.proxyHost = proxyHost;
    }

    /**
     * @param proxyPort
     *            the proxyPort to set
     */
    @Required
    public void setProxyPort(final String proxyPort) {
        this.proxyPort = proxyPort;
    }

    /**
     * @param sslEnabled
     *            the sslEnabled to set
     */
    @Required
    public void setSslEnabled(final String sslEnabled) {
        this.sslEnabled = sslEnabled;
    }

    /**
     * @param staging
     *            the staging to set
     */
    @Required
    public void setStaging(final String staging) {
        this.staging = staging;
    }

    /**
     * @param crawlerAgentPattern
     *            the crawlerAgentPattern to set
     */
    @Required
    public void setCrawlerAgentPattern(final String crawlerAgentPattern) {
        this.crawlerAgentPattern = crawlerAgentPattern;
    }

    /**
     * @param configurationService
     *            the configurationService to set
     */
    @Required
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }
}
