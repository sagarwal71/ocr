/**
 * 
 */
package au.com.target.tgtwebcore.exception;

/**
 * Used when requests to an external integration point fail.
 * 
 */
public class TargetIntegrationException extends Exception {

    public TargetIntegrationException() {
        super();
    }

    public TargetIntegrationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public TargetIntegrationException(final String message) {
        super(message);
    }

    public TargetIntegrationException(final Throwable cause) {
        super(cause);
    }
}
