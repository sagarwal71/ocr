/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import au.com.target.tgttest.automation.ImpexImporter;


/**
 * @author rmcalave
 * 
 */
public final class StoreStockUtil {

    private StoreStockUtil() {
    }

    public static void initialise() {
        ImpexImporter.importCsv("/tgttest/automation-impex/store-stock/products.impex");
        ImpexImporter.importCsv("/tgttest/automation-impex/store-stock/store-warehouses.impex");
        ImpexImporter.importCsv("/tgttest/automation-impex/store-stock/stock-levels.impex");
    }

}
