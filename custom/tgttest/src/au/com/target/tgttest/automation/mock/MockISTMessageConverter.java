/**
 * 
 */
package au.com.target.tgttest.automation.mock;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import au.com.target.tgtjms.jms.converter.ISTMessageConverter;
import au.com.target.tgtjms.jms.service.impl.TargetInterStoreTransferServiceImpl;
import au.com.target.tgtsale.ist.dto.StockAdjustmentDto;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgtutility.logging.JaxbLogger;


/**
 * @author Vivek
 *
 */
public final class MockISTMessageConverter {

    private static final Logger LOG = Logger.getLogger(MockISTMessageConverter.class.getName());

    private static StockAdjustmentDto adjustmentDtos;

    private MockISTMessageConverter() {
        //util
    }

    /**
     * Set the mock converter
     * 
     * @throws JAXBException
     */
    public static void setMock() throws JAXBException {

        // Mock ISTMessageConverter
        final ISTMessageConverter converter = new ISTMessageConverter() {

            @Override
            public String getMessagePayload(final StockAdjustmentDto stockAdjustmentDto) {
                adjustmentDtos = stockAdjustmentDto;

                // Log it
                LOG.setLevel(Level.DEBUG);
                JaxbLogger.logXml(LOG, adjustmentDtos);

                return "dummy";
            }
        };

        final TargetInterStoreTransferServiceImpl interStoreService = (TargetInterStoreTransferServiceImpl)ServiceLookup
                .getTargetInterStoreTransferService();
        interStoreService.setIstMessageConverter(converter);
    }

    /**
     * Retrieve the last adjustmentDtos converted.
     * 
     * @return adjustmentDtos
     */
    public static StockAdjustmentDto getAdjustmentDto() {
        return adjustmentDtos;
    }

    /**
     * Clear captured transaction
     */
    public static void clear() {
        adjustmentDtos = null;
    }

}
