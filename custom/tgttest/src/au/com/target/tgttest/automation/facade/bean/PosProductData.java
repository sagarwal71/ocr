/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * Represent product info in pos
 * 
 */
public class PosProductData {

    private final int store;
    private final String ean;
    private final String itemcode;
    private final String desc;
    private final Integer price;
    private final Integer was;
    private final String errorCode;

    /**
     * @param store
     * @param ean
     * @param itemcode
     * @param desc
     * @param price
     * @param was
     */
    public PosProductData(final int store, final String ean, final String itemcode, final String desc,
            final Integer price, final Integer was, final String errorCode) {
        super();
        this.store = store;
        this.ean = ean;
        this.itemcode = itemcode;
        this.desc = desc;
        this.price = price;
        this.was = was;
        this.errorCode = errorCode;

    }

    public int getStore() {
        return store;
    }

    public String getEan() {
        return ean;
    }

    public String getItemcode() {
        return itemcode;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getPrice() {
        return price;
    }

    public Integer getWas() {
        return was;
    }

    public String getErrorCode() {
        return errorCode;
    }


}
