/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

import java.math.BigDecimal;


/**
 * @author bhuang3
 *
 */
public class ExpectedFlybuysDiscountData {

    private String flybuysCardNumber;

    private BigDecimal value;

    private Integer pointsConsumed;

    private Integer availablePoints;

    /**
     * @return the flybuysCardNumber
     */
    public String getFlybuysCardNumber() {
        return flybuysCardNumber;
    }

    /**
     * @param flybuysCardNumber
     *            the flybuysCardNumber to set
     */
    public void setFlybuysCardNumber(final String flybuysCardNumber) {
        this.flybuysCardNumber = flybuysCardNumber;
    }

    /**
     * @return the value
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(final BigDecimal value) {
        this.value = value;
    }

    /**
     * @return the pointsConsumed
     */
    public Integer getPointsConsumed() {
        return pointsConsumed;
    }

    /**
     * @param pointsConsumed
     *            the pointsConsumed to set
     */
    public void setPointsConsumed(final Integer pointsConsumed) {
        this.pointsConsumed = pointsConsumed;
    }

    /**
     * @return the availablePoints
     */
    public Integer getAvailablePoints() {
        return availablePoints;
    }

    /**
     * @param availablePoints
     *            the availablePoints to set
     */
    public void setAvailablePoints(final Integer availablePoints) {
        this.availablePoints = availablePoints;
    }



}
