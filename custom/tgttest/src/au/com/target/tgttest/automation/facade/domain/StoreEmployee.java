/**
 * 
 */
package au.com.target.tgttest.automation.facade.domain;



/**
 * @author ssundara
 * 
 */
public final class StoreEmployee {
    private static StoreEmployee storeEmployee;

    private final String uid;

    private StoreEmployee(final String uid) {
        this.uid = uid;
    }

    public static StoreEmployee getInstance() {
        return storeEmployee;
    }

    public static void setStoreEmployee(final String uid) {
        storeEmployee = new StoreEmployee(uid);
    }


    public String getUid() {
        return uid;
    }

}
