/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author rmcalave
 *
 */
public class ProductListerData {
    private String productCode;
    private String colourVariantCode;
    private int sizeVariantCount;
    private String name;
    private String displayPrice;
    private String displayWasPrice;
    private String colour;
    private String swatchColour;
    private boolean imageUrlsPresent;
    private String displayName;
    private String sizeVariantCode;
    private String size;
    private String displayPriceAtColourLevel;
    private String displayWasPriceAtColourLevel;
    private boolean inStock;
    private boolean videoUrlPresent;
    private boolean giftCard;
    private boolean assorted;

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the colourVariantCode
     */
    public String getColourVariantCode() {
        return colourVariantCode;
    }

    /**
     * @param colourVariantCode
     *            the colourVariantCode to set
     */
    public void setColourVariantCode(final String colourVariantCode) {
        this.colourVariantCode = colourVariantCode;
    }

    /**
     * @return the sizeVariantCount
     */
    public int getSizeVariantCount() {
        return sizeVariantCount;
    }

    /**
     * @param sizeVariantCount
     *            the sizeVariantCount to set
     */
    public void setSizeVariantCount(final int sizeVariantCount) {
        this.sizeVariantCount = sizeVariantCount;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the displayPrice
     */
    public String getDisplayPrice() {
        return displayPrice;
    }

    /**
     * @param displayPrice
     *            the displayPrice to set
     */
    public void setDisplayPrice(final String displayPrice) {
        this.displayPrice = displayPrice;
    }

    /**
     * @return the displayWasPrice
     */
    public String getDisplayWasPrice() {
        return displayWasPrice;
    }

    /**
     * @param displayWasPrice
     *            the displayWasPrice to set
     */
    public void setDisplayWasPrice(final String displayWasPrice) {
        this.displayWasPrice = displayWasPrice;
    }

    /**
     * @return the colour
     */
    public String getColour() {
        return colour;
    }

    /**
     * @param colour
     *            the colour to set
     */
    public void setColour(final String colour) {
        this.colour = colour;
    }

    /**
     * @return the swatchColour
     */
    public String getSwatchColour() {
        return swatchColour;
    }

    /**
     * @param swatchColour
     *            the swatchColour to set
     */
    public void setSwatchColour(final String swatchColour) {
        this.swatchColour = swatchColour;
    }

    /**
     * @return the imageUrlsPresent
     */
    public boolean isImageUrlsPresent() {
        return imageUrlsPresent;
    }

    /**
     * @param imageUrlsPresent
     *            the imageUrlsPresent to set
     */
    public void setImageUrlsPresent(final boolean imageUrlsPresent) {
        this.imageUrlsPresent = imageUrlsPresent;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName
     *            the displayName to set
     */
    public void setDisplayName(final String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return the sizeVariantCode
     */
    public String getSizeVariantCode() {
        return sizeVariantCode;
    }

    /**
     * @param sizeVariantCode
     *            the sizeVariantCode to set
     */
    public void setSizeVariantCode(final String sizeVariantCode) {
        this.sizeVariantCode = sizeVariantCode;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size
     *            the size to set
     */
    public void setSize(final String size) {
        this.size = size;
    }

    /**
     * @return the displayPriceAtColourLevel
     */
    public String getDisplayPriceAtColourLevel() {
        return displayPriceAtColourLevel;
    }

    /**
     * @param displayPriceAtColourLevel
     *            the displayPriceAtColourLevel to set
     */
    public void setDisplayPriceAtColourLevel(final String displayPriceAtColourLevel) {
        this.displayPriceAtColourLevel = displayPriceAtColourLevel;
    }

    /**
     * @return the displayWasPriceAtColourLevel
     */
    public String getDisplayWasPriceAtColourLevel() {
        return displayWasPriceAtColourLevel;
    }

    /**
     * @param displayWasPriceAtColourLevel
     *            the displayWasPriceAtColourLevel to set
     */
    public void setDisplayWasPriceAtColourLevel(final String displayWasPriceAtColourLevel) {
        this.displayWasPriceAtColourLevel = displayWasPriceAtColourLevel;
    }

    /**
     * @return the inStock
     */
    public boolean isInStock() {
        return inStock;
    }

    /**
     * @param inStock
     *            the inStock to set
     */
    public void setInStock(final boolean inStock) {
        this.inStock = inStock;
    }

    /**
     * @return the videoUrlPresent
     */
    public boolean isVideoUrlPresent() {
        return videoUrlPresent;
    }

    /**
     * @param videoUrlPresent
     *            the videoUrlPresent to set
     */
    public void setVideoUrlPresent(final boolean videoUrlPresent) {
        this.videoUrlPresent = videoUrlPresent;
    }

    /**
     * @return the giftCard
     */
    public boolean isGiftCard() {
        return giftCard;
    }

    /**
     * @param giftCard
     *            the giftCard to set
     */
    public void setGiftCard(final boolean giftCard) {
        this.giftCard = giftCard;
    }

    /**
     * @return the assorted
     */
    public boolean isAssorted() {
        return assorted;
    }

    /**
     * @param assorted
     *            the assorted to set
     */
    public void setAssorted(final boolean assorted) {
        this.assorted = assorted;
    }
}
