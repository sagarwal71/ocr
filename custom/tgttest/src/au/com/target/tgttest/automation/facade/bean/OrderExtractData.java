/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author ajit
 *
 */
public class OrderExtractData {

    private String consignmentId;
    private String orderPurpose;

    /**
     * @return the consignmentId
     */
    public String getConsignmentId() {
        return consignmentId;
    }

    /**
     * @param consignmentId
     *            the consignmentId to set
     */
    public void setConsignmentId(final String consignmentId) {
        this.consignmentId = consignmentId;
    }

    /**
     * @return the orderPurpose
     */
    public String getOrderPurpose() {
        return orderPurpose;
    }

    /**
     * @param orderPurpose
     *            the orderPurpose to set
     */
    public void setOrderPurpose(final String orderPurpose) {
        this.orderPurpose = orderPurpose;
    }

}
