/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author rmcalave
 *
 */
public class UserLoginData {
    private String username;

    private String password;

    private String passwordEncoding;

    private String firstName;

    private String lastName;

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     *            the username to set
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * @return the passwordEncoding
     */
    public String getPasswordEncoding() {
        return passwordEncoding;
    }

    /**
     * @param passwordEncoding
     *            the passwordEncoding to set
     */
    public void setPasswordEncoding(final String passwordEncoding) {
        this.passwordEncoding = passwordEncoding;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

}
