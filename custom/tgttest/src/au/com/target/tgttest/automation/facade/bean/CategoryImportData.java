/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author bpottass
 *
 */
public class CategoryImportData {

    private String categoryCode;
    private String categoryName;
    private String principalUid;
    private String supercategoryCode;
    private String fluentId;

    /**
     * @return the categoryCode
     */
    public String getCategoryCode() {
        return categoryCode;
    }

    /**
     * @param categoryCode
     *            the categoryCode to set
     */
    public void setCategoryCode(final String categoryCode) {
        this.categoryCode = categoryCode;
    }

    /**
     * @return the categoryName
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * @param categoryName
     *            the categoryName to set
     */
    public void setCategoryName(final String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * @return the principalUid
     */
    public String getPrincipalUid() {
        return principalUid;
    }

    /**
     * @param principalUid
     *            the principalUid to set
     */
    public void setPrincipalUid(final String principalUid) {
        this.principalUid = principalUid;
    }

    /**
     * @return the supercategoryCode
     */
    public String getSupercategoryCode() {
        return supercategoryCode;
    }

    /**
     * @param supercategoryCode
     *            the supercategoryCode to set
     */
    public void setSupercategoryCode(final String supercategoryCode) {
        this.supercategoryCode = supercategoryCode;
    }

    /**
     * @return the fluentId
     */
    public String getFluentId() {
        return fluentId;
    }

    /**
     * @param fluentId
     *            the fluentId to set
     */
    public void setFluentId(final String fluentId) {
        this.fluentId = fluentId;
    }


}
