/**
 * 
 */
package au.com.target.tgttest.automation.facade.domain;

import au.com.target.tgtsale.integration.dto.IntegrationPosProductsDto;


/**
 * @author bhuang3
 * 
 */
public final class PosProductsDto {

    private static PosProductsDto posProductsDto;

    private final IntegrationPosProductsDto integrationPosProductsDto;

    private PosProductsDto(final IntegrationPosProductsDto integrationPosProductsDto) {
        this.integrationPosProductsDto = integrationPosProductsDto;
    }

    public static PosProductsDto getInstance() {
        return posProductsDto;
    }

    public static void setPosProductsDto(final IntegrationPosProductsDto integrationPosProductsDto) {
        posProductsDto = new PosProductsDto(integrationPosProductsDto);
    }

    public IntegrationPosProductsDto getIntegrationPosProductsDto() {
        return integrationPosProductsDto;
    }
}
