/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author bbaral1
 *
 */
public class NearestLocationData {

    private String state;

    private Integer storeNumber;

    private String storeName;

    private String targetOpeningHours;

    private String formattedAddress;

    private String phone;

    private double latitude;

    private double longitude;

    private String timeZone;

    private String postcode;

    private Integer shortLeadTime;

    private String suburb;

    private int hdShortLeadTime;

    private int edShortLeadTime;

    private int bulky1ShortLeadTime;

    private int bulky2ShortLeadTime;

    private Boolean closed;

    private Boolean acceptCNC;

    private Boolean acceptLayBy;

    private String url;

    private String type;

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(final String state) {
        this.state = state;
    }

    /**
     * @return the storeNumber
     */
    public Integer getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final Integer storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the storeName
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     * @param storeName
     *            the storeName to set
     */
    public void setStoreName(final String storeName) {
        this.storeName = storeName;
    }

    /**
     * @return the targetOpeningHours
     */
    public String getTargetOpeningHours() {
        return targetOpeningHours;
    }

    /**
     * @param targetOpeningHours
     *            the targetOpeningHours to set
     */
    public void setTargetOpeningHours(final String targetOpeningHours) {
        this.targetOpeningHours = targetOpeningHours;
    }

    /**
     * @return the formattedAddress
     */
    public String getFormattedAddress() {
        return formattedAddress;
    }

    /**
     * @param formattedAddress
     *            the formattedAddress to set
     */
    public void setFormattedAddress(final String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    /**
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude
     *            the latitude to set
     */
    public void setLatitude(final double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude
     *            the longitude to set
     */
    public void setLongitude(final double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the timeZone
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * @param timeZone
     *            the timeZone to set
     */
    public void setTimeZone(final String timeZone) {
        this.timeZone = timeZone;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     *            the phone to set
     */
    public void setPhone(final String phone) {
        this.phone = phone;
    }

    /**
     * @return the shortLeadTime
     */
    public Integer getShortLeadTime() {
        return shortLeadTime;
    }

    /**
     * @param shortLeadTime
     *            the shortLeadTime to set
     */
    public void setShortLeadTime(final Integer shortLeadTime) {
        this.shortLeadTime = shortLeadTime;
    }

    /**
     * @return the suburb
     */
    public String getSuburb() {
        return suburb;
    }

    /**
     * @return the hdShortLeadTime
     */
    public int getHdShortLeadTime() {
        return hdShortLeadTime;
    }

    /**
     * @return the edShortLeadTime
     */
    public int getEdShortLeadTime() {
        return edShortLeadTime;
    }

    /**
     * @return the bulky1ShortLeadTime
     */
    public int getBulky1ShortLeadTime() {
        return bulky1ShortLeadTime;
    }

    /**
     * @return the bulky2ShortLeadTime
     */
    public int getBulky2ShortLeadTime() {
        return bulky2ShortLeadTime;
    }

    /**
     * @param suburb
     *            the suburb to set
     */
    public void setSuburb(final String suburb) {
        this.suburb = suburb;
    }

    /**
     * @param hdShortLeadTime
     *            the hdShortLeadTime to set
     */
    public void setHdShortLeadTime(final int hdShortLeadTime) {
        this.hdShortLeadTime = hdShortLeadTime;
    }

    /**
     * @param edShortLeadTime
     *            the edShortLeadTime to set
     */
    public void setEdShortLeadTime(final int edShortLeadTime) {
        this.edShortLeadTime = edShortLeadTime;
    }

    /**
     * @param bulky1ShortLeadTime
     *            the bulky1ShortLeadTime to set
     */
    public void setBulky1ShortLeadTime(final int bulky1ShortLeadTime) {
        this.bulky1ShortLeadTime = bulky1ShortLeadTime;
    }

    /**
     * @param bulky2ShortLeadTime
     *            the bulky2ShortLeadTime to set
     */
    public void setBulky2ShortLeadTime(final int bulky2ShortLeadTime) {
        this.bulky2ShortLeadTime = bulky2ShortLeadTime;
    }

    /**
     * @return the postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * @param postcode
     *            the postcode to set
     */
    public void setPostcode(final String postcode) {
        this.postcode = postcode;
    }

    /**
     * @return the closed
     */
    public Boolean getClosed() {
        return closed;
    }

    /**
     * @return the acceptCNC
     */
    public Boolean getAcceptCNC() {
        return acceptCNC;
    }

    /**
     * @return the acceptLayBy
     */
    public Boolean getAcceptLayBy() {
        return acceptLayBy;
    }

    /**
     * @param closed
     *            the closed to set
     */
    public void setClosed(final Boolean closed) {
        this.closed = closed;
    }

    /**
     * @param acceptCNC
     *            the acceptCNC to set
     */
    public void setAcceptCNC(final Boolean acceptCNC) {
        this.acceptCNC = acceptCNC;
    }

    /**
     * @param acceptLayBy
     *            the acceptLayBy to set
     */
    public void setAcceptLayBy(final Boolean acceptLayBy) {
        this.acceptLayBy = acceptLayBy;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param url
     *            the url to set
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final String type) {
        this.type = type;
    }

}
