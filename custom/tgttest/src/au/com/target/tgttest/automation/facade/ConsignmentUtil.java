/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgttest.automation.ImpexImporter;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.ConsignmentForStore;
import au.com.target.tgttest.automation.facade.domain.Order;
import au.com.target.tgtwsfacades.instore.dto.consignments.Consignment;
import org.junit.Assert;


/**
 * Utility methods for Consignment.
 * 
 * @author jjayawa1
 *
 */
public class ConsignmentUtil {

    // Geelong
    private static final String NON_FULFILMENT_STORE = "7001";

    private ConsignmentUtil() {

    }

    /**
     * Method to setup test stores.
     */
    public static void setupStores() {
        ImpexImporter.importCsv("/tgttest/automation-impex/store/stores-for-instore-fulfilment.impex");
    }

    public static void setUpFeatureSwitchForSendConsignmentToFastlineTrue() {
        ImpexImporter.importCsv("/tgttest/automation-impex/consignment/order-extract-consignment-id-on.impex");
    }

    public static void setUpFeatureSwitchForSendConsignmentToFastlineFalse() {
        ImpexImporter.importCsv("/tgttest/automation-impex/consignment/order-extract-consignment-id-off.impex");
    }

    /**
     * Remove all consignments.
     */
    public static void removeAllConsignments() {
        ServiceLookup.getConsignmentCreationHelper().removeAllConsignments();
    }

    /**
     * Method to remove test stores.
     */
    public static void teardownStores() {
        ImpexImporter.importCsv("/tgttest/automation-impex/store/stores-for-instore-fulfilment-reset.impex");
    }

    public static TargetConsignmentModel verifyAndGetOrderConsignment(final ConsignmentStatus consignmentStatus) {

        final OrderModel orderModel = Order.getInstance().getOrderModel();
        final Set<ConsignmentModel> consignments = orderModel.getConsignments();

        final TargetConsignmentModel checkedConsignment = getConsignmentIfInState(consignmentStatus);

        if (checkedConsignment != null) {
            ServiceLookup.getModelService().refresh(checkedConsignment);
            return checkedConsignment;
        }

        final TargetConsignmentModel consignment = (TargetConsignmentModel)consignments.iterator().next();
        ServiceLookup.getModelService().refresh(consignment);
        return consignment;
    }

    public static TargetConsignmentModel getCancelledConsignment() {
        return getConsignmentIfInState(ConsignmentStatus.CANCELLED);
    }

    public static TargetConsignmentModel getConsignmentIfInState(final ConsignmentStatus status) {
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        final Set<ConsignmentModel> consignments = orderModel.getConsignments();
        for (final ConsignmentModel consignment : consignments) {
            if (consignment.getStatus().equals(status)) {
                return (TargetConsignmentModel)consignment;
            }
        }
        return null;
    }

    public static List<ConsignmentModel> getNotCancelledConsignments() {
        return getConsignmentsNotInState(ConsignmentStatus.CANCELLED);
    }

    /**
     * This method will check no of Consignments created for the products.
     * 
     * @return consignmentList
     */
    public static Set<ConsignmentModel> getAllConsignmentsForOrder() {
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        final Set<ConsignmentModel> consignments = orderModel.getConsignments();
        return consignments;
    }


    public static List<ConsignmentModel> getConsignmentsNotInState(final ConsignmentStatus status) {
        final List<ConsignmentModel> consignmentList = new ArrayList<>();
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        final Set<ConsignmentModel> consignments = orderModel.getConsignments();
        for (final ConsignmentModel consignment : consignments) {
            if (!consignment.getStatus().equals(status)) {
                consignmentList.add(consignment);
            }
        }
        return consignmentList;
    }

    public static TargetConsignmentModel verifyAndGetSingleOrderConsignment() {
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        final Set<ConsignmentModel> consignments = orderModel.getConsignments();
        assertThat(consignments).isNotNull().isNotEmpty().hasSize(1);
        final TargetConsignmentModel consignment = (TargetConsignmentModel)consignments.iterator().next();
        ServiceLookup.getModelService().refresh(consignment);
        return consignment;
    }

    public static TargetConsignmentModel verifyAndGetSingleActiveInstoreConsignment() {
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        final List<TargetConsignmentModel> consignments = ServiceLookup.getTargetConsignmentService()
                .getActiveConsignmentsForOrder(orderModel);
        assertThat(consignments).isNotNull().isNotEmpty();

        final List<TargetConsignmentModel> instoreConsignments = new ArrayList<>();
        for (final TargetConsignmentModel cons : consignments) {
            if (ServiceLookup.getTargetStoreConsignmentService().isConsignmentAssignedToAnyStore(cons)) {
                instoreConsignments.add(cons);
            }
        }
        assertThat(instoreConsignments).isNotNull().isNotEmpty().hasSize(1);

        final TargetConsignmentModel instoreConsignment = instoreConsignments.iterator().next();
        ServiceLookup.getModelService().refresh(instoreConsignment);
        return instoreConsignment;
    }

    /**
     * Get a consignment given a code
     * 
     * @param consignmentCode
     * @return TargetConsignmentModel
     */
    public static TargetConsignmentModel getConsignmentForCode(final String consignmentCode) {
        final List<TargetConsignmentModel> consignments = ServiceLookup.getTargetStoreConsignmentService()
                .getConsignmentsByCodes(Collections.singletonList(consignmentCode));
        assertThat(consignments).isNotNull();
        assertThat(consignments).isNotEmpty();
        assertThat(consignments).hasSize(1);
        return consignments.get(0);
    }

    public static Consignment getConsignment(final String consignmentCode, final List<Consignment> consignments) {
        for (final Consignment consignment : consignments) {
            if (consignment.getCode().equalsIgnoreCase(consignmentCode)) {
                return consignment;
            }
        }
        return null;
    }


    public static ConsignmentForStore createConsignmentRecordSentToStore(final String code, final String ofcOrderType,
            final TargetPointOfServiceModel tpos) {

        final String status = "CONFIRMED_BY_WAREHOUSE";

        String deliveryMode = "click-and-collect";
        Integer storeNumber = null;
        if (tpos != null) {
            storeNumber = tpos.getStoreNumber();
        }

        switch (ofcOrderType) {
            case "INSTORE_PICKUP":
                break;
            case "INTERSTORE_DELIVERY":
                storeNumber = Integer.valueOf(NON_FULFILMENT_STORE);
                break;
            case "CUSTOMER_DELIVERY":
                deliveryMode = "home-delivery";
                storeNumber = null;
                break;
            default:
                throw new IllegalArgumentException("Order type not recognized: " + ofcOrderType);
        }


        return ConsignmentForStore.createBasicConsignment(code, status, deliveryMode, storeNumber);
    }

    /**
     * Verify there is a single non cancelled consignment and return it
     * 
     * @return TargetConsignmentModel
     */
    public static TargetConsignmentModel verifyAndGetSingleFastlineConsignment() {
        final TargetConsignmentModel consignment = ServiceLookup.getTargetConsignmentService()
                .findConsignmentForDefaultWarehouse(Order.getInstance().getOrderNumber());
        assertThat(consignment).isNotNull();
        ServiceLookup.getModelService().refresh(consignment);
        return consignment;

    }

    public static void verifyConsignmentAfterSplit(final String warehouse, final String products,
            final String carrier) {
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        Assert.assertNotNull(orderModel);
        final Set<ConsignmentModel> consignments = orderModel.getConsignments();

        boolean atleastOneConWithGivenWarehouse = false;
        for (final ConsignmentModel con : consignments) {
            Assert.assertNotNull(con.getWarehouse());

            if (StringUtils.equals(warehouse, con.getWarehouse().getName())) {
                atleastOneConWithGivenWarehouse = true;
                Assert.assertEquals(carrier, ((TargetConsignmentModel)con).getTargetCarrier().getCode());
                final String[] productCodes = products.split(",");
                final Set<ConsignmentEntryModel> conEntries = con.getConsignmentEntries();
                Assert.assertEquals(productCodes.length, conEntries.size());
                final List<String> expectedCodes = Arrays.asList(productCodes);

                for (final ConsignmentEntryModel entry : conEntries) {
                    Assert.assertTrue(expectedCodes.contains(entry.getOrderEntry().getProduct().getCode()));
                }
                break;
            }
        }
        Assert.assertTrue(atleastOneConWithGivenWarehouse);
    }

    public static ConsignmentModel getNotCancelledConsignmentAssignedToWarehouse(final String warehouseName) {
        final List<ConsignmentModel> notCancelledConsignments = getNotCancelledConsignments();
        for (final ConsignmentModel consignment : notCancelledConsignments) {
            final WarehouseModel warehouse = consignment.getWarehouse();
            if (warehouse != null && StringUtils.equalsIgnoreCase(warehouseName, warehouse.getName())) {
                return consignment;
            }
        }
        return null;
    }
}
