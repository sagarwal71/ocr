/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.response.data.CheckoutOptionsResponseData;
import au.com.target.tgttest.automation.ImpexImporter;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.CartEntry;
import au.com.target.tgttest.automation.facade.domain.Customer;


/**
 * Util for setting up carts
 * 
 */
public final class CartUtil {


    private static final String SELECTED_SALES_APPLICATION = "Selected-Sales-Application";
    private static final Logger LOG = Logger.getLogger(CartUtil.class);

    private CartUtil() {
        // utility
    }

    /**
     * import the impex for product under test
     */
    public static void initialise() {
        ImpexImporter.importCsv("/tgttest/automation-impex/product/cartProducts.impex");
        ServiceLookup.getSessionService().setAttribute(SELECTED_SALES_APPLICATION, SalesApplication.WEB);
    }

    public static void clearSessionCartData() {
        final CartModel cart = ServiceLookup.getTargetCheckoutFacade().getCart();
        if (null != cart && null != cart.getEntries() && CollectionUtils.isNotEmpty(cart.getEntries())) {
            ServiceLookup.getModelService().remove(cart);
        }
    }

    public static void updateCartEntries(final List<CartEntry> entries) throws Exception {
        for (final CartEntry entry : entries) {
            if (entry.getPrice() != null) {
                ProductUtil.updateProductPrice(entry.getProduct(), entry.getPrice().doubleValue());
            }

            CartUtil.updateFromCart(entry.getProduct(), entry.getQty());
        }
        recalculateSessionCart();
    }

    public static void addCartEntries(final List<CartEntry> entries) throws Exception {
        for (final CartEntry entry : entries) {
            if (entry.getPrice() != null) {
                ProductUtil.updateProductPrice(entry.getProduct(), entry.getPrice().doubleValue());
            }

            CartUtil.addToCart(entry.getProduct(), entry.getQty());
        }
        recalculateSessionCart();
    }

    /**
     * Return CartData for the given Session
     * 
     * @return TargetCartData
     */
    public static TargetCartData getSessionCartData() {

        final TargetCartData cartData = (TargetCartData)ServiceLookup.getTargetCartFacade().getSessionCart();
        return cartData;
    }


    /**
     * Add given quantity of product to the cart
     * 
     * @param productCode
     * @param qty
     * @throws CommerceCartModificationException
     */
    public static CartModificationData addToCart(final String productCode, final long qty)
            throws CommerceCartModificationException {
        return ServiceLookup.getTargetCartFacade().addToCart(productCode, qty);
    }


    /**
     * Update product from the cart
     * 
     * @param productCode
     * @param qty
     * @return {@link CartModificationData}
     * @throws CommerceCartModificationException
     */
    public static CartModificationData updateFromCart(final String productCode, final long qty)
            throws CommerceCartModificationException {
        return ServiceLookup.getTargetCartFacade().updateCartEntry(productCode, qty);
    }

    /**
     * Recalculate the cart
     * 
     * @throws CalculationException
     */
    public static void recalculateSessionCart() throws CalculationException {

        // Recalculate with promotions
        final CartModel cartModel = getSessionCartModel();
        cartModel.setCalculated(Boolean.FALSE);
        final CommerceCartParameter cartParameter = new CommerceCartParameter();
        cartParameter.setCart(cartModel);
        ServiceLookup.getTargetCommerceCartService().recalculateCart(cartParameter);
        ServiceLookup.getModelService().refresh(cartModel);
    }

    /**
     * set flybuys number
     * 
     * @param number
     */
    public static void setFlybuysNumber(final String number) {
        final CartModel cartModel = getSessionCartModel();
        cartModel.setFlyBuysCode(number);
        ServiceLookup.getModelService().save(cartModel);
        ServiceLookup.getModelService().refresh(cartModel);
    }

    public static void setShipsterMember(final boolean status) {
        getSessionCartModel().setAusPostClubMember(Boolean.valueOf(status));
    }


    /**
     * Delete the session cart
     * 
     */
    public static void teardownCart() {

        try {
            final CartModel cart = getSessionCartModel();
            if (cart != null) {
                ServiceLookup.getModelService().remove(cart);
            }
        }
        catch (final Exception e) {
            LOG.warn("Exception in cart teardown", e);
        }

    }

    /**
     * Get the session cart model
     * 
     * @return CartModel
     */
    public static CartModel getSessionCartModel() {

        return ServiceLookup.getTargetLaybyCartService().getSessionCart();
    }

    /**
     * Get current cart user
     * 
     * @return UserModel
     */
    public static UserModel getCheckoutCartUser() {
        return ServiceLookup.getTargetLaybyCartService().getSessionCart().getUser();
    }

    /**
     * Set up guest or registered user checkout
     * 
     * @throws DuplicateUidException
     * 
     */
    public static void createCheckoutCart(final String userCheckoutMode) throws DuplicateUidException {
        if (userCheckoutMode.equals(CheckoutUtil.GUEST_CHECKOUT)) {
            Customer.setGuestUser();
            CartUtil.createGuestUserCheckoutCart();
            //Todo delivery mode and payment mode are different from the default user checkout which are not get from the userModel.
        }
        else if (userCheckoutMode.equals(CheckoutUtil.REGISTERED_USER_CHECKOUT)) {
            Customer.setStandardCustomer();
            CartUtil.createRegisteredUserCheckoutCart();
        }
    }

    /**
     * Check if the given cart exists in the system
     * 
     * @param code
     * @return true if found
     */
    public static boolean checkCartExists(final String code) {

        return getCartByCode(code) != null;
    }

    /**
     * Get cart from database for code
     * 
     * @param code
     * @return CartModel
     */
    public static CartModel getCartByCode(final String code) {

        final CartModel example = new CartModel();
        example.setCode(code);
        CartModel foundCart = null;
        try {
            foundCart = ServiceLookup.getFlexibleSearchService().getModelByExample(example);
        }
        catch (final Exception e) {
            LOG.warn("Exception: ", e);
        }

        return foundCart;
    }

    /**
     * @return String Cart Code
     */
    public static String getSessionCartCode() {
        final CartModel cartModel = CartUtil.getSessionCartModel();
        return cartModel.getCode();
    }

    public static List<TargetZoneDeliveryModeData> getApplicableDeliveryModesFromResponseFacade() {
        final CheckoutOptionsResponseData data = (CheckoutOptionsResponseData)ServiceLookup
                .getCheckoutResponseFacade().getApplicableDeliveryModes().getData();
        return data.getDeliveryModes();
    }

    public static TargetZoneDeliveryModeData getApplicableDeliveryModesFromResponseFacade(final String code) {
        final CheckoutOptionsResponseData data = (CheckoutOptionsResponseData)ServiceLookup
                .getCheckoutResponseFacade().getApplicableDeliveryModes().getData();
        for (final TargetZoneDeliveryModeData deliveryModeData : data.getDeliveryModes()) {
            if (deliveryModeData.getCode().equals(code)) {
                return deliveryModeData;
            }
        }
        return null;
    }

    /**
     * update Product Out Of Stock
     * 
     * @param productCode
     * @throws CommerceCartModificationException
     */
    public static void updateProductOutOfStock(final String productCode) throws CommerceCartModificationException {
        final ProductModel product = ServiceLookup.getProductService().getProductForCode(productCode);
        final WarehouseModel warehouse = ServiceLookup.getWarehouseService().getDefaultOnlineWarehouse();
        final Collection<WarehouseModel> warehouses = new ArrayList<>();
        warehouses.add(warehouse);
        ServiceLookup.getStockService().setInStockStatus(product, warehouses, InStockStatus.FORCEOUTOFSTOCK);
        final CartModel checkoutCart = CheckoutUtil.getCheckoutCart();
        ServiceLookup.getTargetCommerceCartService().performSOH(checkoutCart);
    }

    private static void createGuestUserCheckoutCart() throws DuplicateUidException {
        final CustomerModel guestCustomer = CustomerUtil.getDefaultGuestUserModel();
        setCurrentUser(ServiceLookup.getUserService().getAnonymousUser());
        createCheckoutCartForUser(guestCustomer);
    }

    private static void createRegisteredUserCheckoutCart() throws DuplicateUidException {
        final TargetCustomerModel customerModel = CustomerUtil.getTargetCustomer(CustomerUtil
                .getStandardTestCustomerUid());
        customerModel.setType(null);
        setCurrentUser(customerModel);
        createCheckoutCartForUser(customerModel);
    }

    private static void createCheckoutCartForUser(final CustomerModel customerModel) {

        final CartModel cartModel = CheckoutUtil.getCheckoutCart();
        cartModel.setUser(customerModel);
        ServiceLookup.getModelService().save(cartModel);
    }

    private static void setCurrentUser(final CustomerModel customerModel) {
        ServiceLookup.getUserService().setCurrentUser(customerModel);
    }

}
