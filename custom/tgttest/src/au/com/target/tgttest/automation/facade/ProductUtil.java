/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetPriceRowModel;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.util.TargetProductDataHelper;
import au.com.target.tgttest.automation.ImpexImporter;
import au.com.target.tgttest.automation.ServiceLookup;


/**
 * Product facade
 * 
 */
public final class ProductUtil {

    private static final int DEFAULT_TAX = 10;

    private ProductUtil() {
        // utility
    }


    /**
     * Pre-suite initialisation
     */
    public static void initialise() {
        ImpexImporter.importCsv("/tgttest/automation-impex/product/stock-levels.impex");
    }

    /**
     * Pre-suite initialisation
     */
    public static void initialiseAutoFetchProduct() {
        ImpexImporter
                .importCsv(
                "/tgtinitialdata/import/productCatalogs/targetProductCatalog/automationTest/automation-fetch-products-setup.impex");
        ImpexImporter
                .importCsv(
                "/tgtinitialdata/import/productCatalogs/targetProductCatalog/automationTest/automation-fetch-products-price-setup.impex");

    }


    /**
     * Pre-suite Looks initialisation
     */
    public static void initialiseLooks() {
        ImpexImporter.importCsv("/tgttest/automation-impex/looks/productLooks.impex");
    }

    /**
     * Update the price of the given product.
     * 
     * @param product
     * @param price
     * @throws Exception
     */
    public static void updateProductPrice(final String product, final double price) throws Exception {

        CatalogUtil.setSessionCatalogVersion();

        // For reference see TargetPosPriceImportIntegrationFacade - we'll just update in online
        final List<CatalogVersionModel> listCatalogs = new ArrayList<>();
        listCatalogs.add(ServiceLookup.getCatalogVersionService().getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.ONLINE_VERSION));

        final List<ProductModel> productToUpdate = ServiceLookup.getTargetProductPriceService()
                .getProductModelForCatalogs(product, listCatalogs, price, price, null, getGstRate(), false);

        ServiceLookup.getTargetProductPriceService().updateProductPrices(productToUpdate);
    }

    private static int getGstRate() {
        return DEFAULT_TAX;
    }


    /**
     * Move the given product to the given category
     * 
     * @param product
     * @param category
     */
    public static void setProductInCategory(final String product, final String category) {

        CatalogUtil.setSessionCatalogVersion();

        final CategoryModel cat = ServiceLookup.getCategoryService().getCategoryForCode(category);
        final ProductModel productModel = ServiceLookup.getProductService().getProductForCode(product);

        productModel.setSupercategories(Collections.singletonList(cat));
        ServiceLookup.getModelService().save(productModel);
    }


    /**
     * Return ProductModel for the code (for users without permission to see STAGED
     * 
     * @param code
     * @return ProductModel
     */
    public static ProductModel getProductModel(final String code) {

        CatalogUtil.setSessionCatalogVersion();

        return ServiceLookup.getProductService().getProductForCode(code);
    }

    /**
     * retrieves the base product for this product passed in
     * 
     * @param product
     * @return BaseProduct
     */
    public static ProductModel getBaseProduct(final ProductModel product) {
        return TargetProductDataHelper.getBaseProduct(product);
    }

    /**
     * @param code
     * @return product from Online catalog
     */
    public static ProductModel getOnlineProductModel(final String code) {

        return getProductModelFromCatalogVersion(code, TgtCoreConstants.Catalog.ONLINE_VERSION);
    }

    /**
     * @param code
     * @return product from Staged catalog
     */
    public static ProductModel getStagedProductModel(final String code) {

        return getProductModelFromCatalogVersion(code, TgtCoreConstants.Catalog.OFFLINE_VERSION);
    }

    /**
     * @param code
     * @return product from Staged catalog
     */
    private static ProductModel getProductModelFromCatalogVersion(final String code, final String version) {

        CatalogUtil.setSessionCatalogVersion();

        final CatalogVersionModel catalogVersion = ServiceLookup.getCatalogVersionService().getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS, version);
        return ServiceLookup.getProductService().getProductForCode(catalogVersion, code);
    }

    /**
     * Get the delivery mode model from the name
     * 
     * @param deliveryMode
     * @return DeliveryModeModel
     */
    public static DeliveryModeModel getDeliveryMode(final String deliveryMode) {
        return ServiceLookup.getZoneDeliveryModeService().getDeliveryModeForCode(deliveryMode);
    }

    /**
     * @param product
     * @param deliveryModeList
     */
    public static void setDeliveryModeForProduct(final String product, final String deliveryModeList) {
        if (StringUtils.isNotEmpty(deliveryModeList)) {
            final Set<DeliveryModeModel> deliveryModeSet = new HashSet<>();
            final String[] deliveryModes = deliveryModeList.trim().split(",");
            for (final String deliveryMode : deliveryModes) {
                deliveryModeSet.add(getDeliveryMode(deliveryMode));
            }
            ProductModel productModel = getOnlineProductModel(product);
            while (!(productModel instanceof TargetProductModel)) {
                productModel = ((AbstractTargetVariantProductModel)productModel).getBaseProduct();
            }
            productModel.setDeliveryModes(deliveryModeSet);
            ServiceLookup.getModelService().save(productModel);
        }


    }

    /**
     * Set some attributes of the current price for the given product (in both online and offline)
     * 
     * @param product
     * @param startDate
     * @param sellPrice
     * @param isPromoEvent
     */
    public static void updateProductPriceRow(final String product, final Date startDate, final double sellPrice,
            final Double permPrice,
            final boolean isPromoEvent) {

        final ProductModel productModelStg = getStagedProductModel(product);
        updateProductPriceRow(productModelStg, startDate, sellPrice, permPrice, isPromoEvent);

        final ProductModel productModelOnline = getOnlineProductModel(product);
        updateProductPriceRow(productModelOnline, startDate, sellPrice, permPrice, isPromoEvent);
    }

    private static void updateProductPriceRow(final ProductModel productModel, final Date startDate,
            final double sellPrice, final Double permPrice,
            final boolean isPromoEvent) {

        final Collection<PriceRowModel> prices = productModel.getEurope1Prices();

        // Should be always one price for test data
        final TargetPriceRowModel targetPriceRow = (TargetPriceRowModel)prices.iterator().next();
        targetPriceRow.setStartTime(startDate);
        targetPriceRow.setPrice(Double.valueOf(sellPrice));
        if (permPrice != null) {
            targetPriceRow.setWasPrice(permPrice);
        }
        targetPriceRow.setPromoEvent(Boolean.valueOf(isPromoEvent));
        ServiceLookup.getModelService().save(targetPriceRow);

    }

    public static TargetProductData populateThePriceIntoProductData(final String product) {
        return (TargetProductData)ServiceLookup.getTargetProductFacade().getBulkyBoardProductForOptions(
                getOnlineProductModel(product),
                Arrays.asList(
                        ProductOption.BASIC,
                        ProductOption.PRICE, ProductOption.STOCK),
                null);
    }

    public static List<ProductData> populateAllSellableVariantsWithStoreStockAndOptions(final String product,
            final Integer storeNumber) {
        return ServiceLookup.getTargetProductFacade().getAllSellableVariantsWithStoreStockAndOptions(
                getOnlineProductModel(product).getCode(),
                storeNumber,
                Arrays.asList(ProductOption.CONSOLIDATED_STOCK));
    }

    public static boolean checkVariantProduct(final String baseProduct, final String variantProduct) {
        final ProductModel baseProductModel = getStagedProductModel(baseProduct);
        final ProductModel variantProductModel = getStagedProductModel(variantProduct);
        if (variantProductModel instanceof VariantProductModel) {
            if (((VariantProductModel)variantProductModel).getBaseProduct() == baseProductModel) {
                return true;
            }
        }
        return false;
    }

    private static TargetProductDimensionsModel setTargetProductDimensionsModel(final String productCode) {
        final TargetProductDimensionsModel productDimentionModel = ServiceLookup.getModelService().create(
                TargetProductDimensionsModel.class);
        productDimentionModel.setCatalogVersion(ServiceLookup.getCatalogVersionService().getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.ONLINE_VERSION));
        productDimentionModel.setDimensionCode(productCode);
        productDimentionModel.setHeight(Double.valueOf(5));
        productDimentionModel.setLength(Double.valueOf(5));
        productDimentionModel.setWidth(Double.valueOf(5));
        return productDimentionModel;
    }

    public static void setProductWeight(final String productCode, final Double weight) {
        TargetProductDimensionsModel productDimentionModel = null;
        final AbstractTargetVariantProductModel productModel = (AbstractTargetVariantProductModel)getProductModel(
                productCode);
        if (null != weight) {
            productDimentionModel = productModel.getProductPackageDimensions();
            if (null == productDimentionModel) {
                productDimentionModel = setTargetProductDimensionsModel(productCode);
            }
            productDimentionModel.setWeight(weight);
            ServiceLookup.getModelService().save(productDimentionModel);
        }
        productModel.setProductPackageDimensions(productDimentionModel);
        ServiceLookup.getModelService().save(productModel);
    }


    /**
     * @param productCode
     * @return TargetProductData
     */
    public static TargetProductData populateProductEndOfLifeLinkData(final String productCode) {
        return (TargetProductData)ServiceLookup.getTargetProductFacade().getProductForCodeAndOptions(
                getStagedProductModel(productCode).getCode(),
                Arrays.asList(ProductOption.PRODUCT_END_OF_LIFE_LINKS));
    }

    /**
     * remove fluentId
     */
    public static void removeFluentId() {
        final List<String> productCodes = Arrays.asList("P11112222");
        for (final String productCode : productCodes) {
            try {
                CatalogUtil.setSessionStagedCatalogVersion();
                final TargetProductModel targetProductModel = (TargetProductModel)ServiceLookup.getProductService()
                        .getProductForCode(productCode);
                targetProductModel.setFluentId(null);
                ServiceLookup.getModelService().save(targetProductModel);
                ServiceLookup.getModelService().refresh(targetProductModel);
            }
            catch (final Exception e) {
                //
            }

        }

    }

}
