/**
 * 
 */
package au.com.target.tgttest.automation.mock;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import au.com.target.tgtfulfilment.interceptor.StoreFulfilmentCapabilitiesValidateInterceptor;


/**
 * @author ajit
 *
 */
public class MockStoreFulfilmentCapabilitiesInterceptor extends StoreFulfilmentCapabilitiesValidateInterceptor {

    private static MockStoreFulfilmentCapabilitiesInterceptor mockInterceptor = new MockStoreFulfilmentCapabilitiesInterceptor();

    public static MockStoreFulfilmentCapabilitiesInterceptor getMockInstance() {

        return mockInterceptor;
    }

    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        // This is getting mocked no need to do.
    }
}
