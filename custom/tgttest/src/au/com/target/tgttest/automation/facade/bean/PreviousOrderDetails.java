/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author ajit
 *
 */
public class PreviousOrderDetails {

    private final String paymentType;
    private final String previousDeliveryMode;
    private final String deliveryAddress;


    /**
     * @param paymentType
     * @param previousDeliveryMode
     * @param deliveryAddress
     */
    public PreviousOrderDetails(final String paymentType, final String previousDeliveryMode,
            final String deliveryAddress) {
        super();
        this.paymentType = paymentType;
        this.previousDeliveryMode = previousDeliveryMode;
        this.deliveryAddress = deliveryAddress;
    }

    /**
     * @return the paymentType
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * @return the previousDeliveryMode
     */
    public String getPreviousDeliveryMode() {
        return previousDeliveryMode;
    }

    /**
     * @return the deliveryAddress
     */
    public String getDeliveryAddress() {
        return deliveryAddress;
    }

}
