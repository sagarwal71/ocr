/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.jalo.JaloConnection;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.jalo.user.Customer;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.jalo.user.UserManager;

import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgttest.automation.ServiceLookup;


/**
 * @author mjanarth
 * 
 */
public final class SessionUtil {

    private static final String CURRENT_SITE = "currentSite";
    private static final String TARGET_SITE = "target";


    private SessionUtil() {
        // util
    }

    public static void userLogin(final String uid) {
        final User user = UserManager.getInstance().getUserByLogin(uid);
        JaloSession.getCurrentSession().setUser(user);
        CatalogUtil.setSessionCatalogVersion();

        // If its a customer on the site then this will restore their cart
        if (user instanceof Customer) {
            ServiceLookup.getTargetCustomerFacade().loginSuccess();
        }

    }

    public static void createGuestSession() throws JaloSecurityException {

        JaloConnection.getInstance().createAnonymousCustomerSession();
    }

    public static void setAdminUserSession() {

        JaloSession.getCurrentSession().getSessionContext()
                .setUser(JaloConnection.getInstance().getUserManager().getAdminEmployee());
    }

    public static void removeSession() {

        final JaloSession jaloSession = JaloSession.getCurrentSession();
        if (jaloSession != null) {
            jaloSession.close();
        }
    }

    public static void setUser(final String uid) {

        // Set current user in session
        // TODO - should combine with login?
        final CustomerModel user = CustomerUtil.getTargetCustomer(uid);
        ServiceLookup.getUserService().setCurrentUser(user);
    }


    /**
     * Add the base site in session
     */
    public static void initializeSite() {
        final BaseSiteModel siteModel = ServiceLookup.getBaseSiteService().getBaseSiteForUID(TARGET_SITE);
        ServiceLookup.getSessionService().setAttribute(CURRENT_SITE, siteModel);
    }

    /**
     * This will set the active purchase option to be buy now. This is due to laybuy purchase option is not being
     * removed yet.
     * 
     * 
     */
    public static void setSessionPurchaseOption() {

        ServiceLookup.getSessionService().setAttribute(TgtFacadesConstants.ACTIVE_PURCHASE_OPTION,
                CheckoutUtil.BUY_NOW_CODE);
    }

    /**
     * Set AUD as the currency in the session context.
     */
    public static void setSessionCurrency() {
        final CurrencyModel currencyModel = ServiceLookup.getCommonI18nService().getCurrency("AUD");
        final Currency currency = ServiceLookup.getModelService().getSource(currencyModel);
        JaloSession.getCurrentSession().getSessionContext().setCurrency(currency);
    }
}
