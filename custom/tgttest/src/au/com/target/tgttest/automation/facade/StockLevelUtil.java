/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.util.Utilities;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.target.tgttest.automation.ImpexImporter;
import au.com.target.tgttest.automation.ServiceLookup;


/**
 * Util for dealing with fastline stock
 *
 */
public class StockLevelUtil {

    private static final Logger LOG = Logger.getLogger(StockLevelUtil.class);
    private static final String CNP_WAREHOUSE = "CnpWarehouse";

    private StockLevelUtil() {
        // utility
    }

    /**
     * Reset the the stock on fastline products
     */
    public static void reset() {
        ImpexImporter.importCsv("/tgttest/automation-impex/product/stock-levels.impex");
    }

    /**
     * Update fastline stock for product
     * 
     * @param productId
     * @param reserved
     * @param available
     * @param preOrder
     *            YTODO
     * @param maxPreOrder
     *            YTODO
     */
    public static void updateFastlineProductStock(final String productId, final int reserved,
            final int available, final int preOrder, final int maxPreOrder) {

        final StockLevelModel stockLevel = getStockLevelModeForWarehouse(
                ServiceLookup.getWarehouseService().getDefaultOnlineWarehouse(), productId, false);
        stockLevel.setAvailable(available);
        stockLevel.setReserved(reserved);
        stockLevel.setPreOrder(preOrder);
        stockLevel.setMaxPreOrder(maxPreOrder);
        ServiceLookup.getModelService().save(stockLevel);
    }

    /**
     * Update CNP stock for product
     * 
     * @param productId
     * @param reserved
     * @param available
     */
    public static void updateCNPProductStock(final String productId, final int reserved,
            final int available) {

        final StockLevelModel stockLevel = getStockLevelModeForWarehouse(
                ServiceLookup.getWarehouseService().getWarehouseForCode("CNPWarehouse"), productId, false);
        stockLevel.setAvailable(available);
        stockLevel.setReserved(reserved);
        ServiceLookup.getModelService().save(stockLevel);
    }


    /**
     * Lookup fastline stock for product
     * 
     * @param productId
     * @return StockLevelModel
     */
    public static StockLevelModel getFastlineStockFor(final String productId) {

        return getFastlineStockFor(productId, false);
    }

    /**
     * Lookup Cnp stock for product
     * 
     * @param productId
     * @return StockLevelModel
     */
    public static StockLevelModel getCnpStockFor(final String productId) {

        return getCnpStockFor(productId, false);
    }

    /**
     * @param productId
     * @param staged
     * @return StockLevelModel
     */
    private static StockLevelModel getCnpStockFor(final String productId, final boolean staged) {

        final StockLevelModel stockLevelModel;

        try {
            stockLevelModel = getCnpStockLevelModel(productId, staged);
        }
        catch (final UnknownIdentifierException e) {
            return null;
        }
        if (stockLevelModel != null) {
            Utilities.invalidateCache(stockLevelModel.getPk());
            ServiceLookup.getModelService().refresh(stockLevelModel);
        }
        return stockLevelModel;
    }

    /**
     * Lookup staged fastline stock for product
     * 
     * @param productId
     * @return StockLevelModel
     */
    public static StockLevelModel getStagedFastlineStockFor(final String productId) {

        return getFastlineStockFor(productId, true);
    }

    public static StockLevelModel getFastlineStockFor(final String productId, final boolean staged) {

        final StockLevelModel stockLevelModel;

        try {
            stockLevelModel = getStockLevelModeForWarehouse(
                    ServiceLookup.getWarehouseService().getDefaultOnlineWarehouse(), productId, staged);
        }
        catch (final UnknownIdentifierException e) {
            return null;
        }
        if (stockLevelModel != null) {
            Utilities.invalidateCache(stockLevelModel.getPk());
            ServiceLookup.getModelService().refresh(stockLevelModel);
        }
        return stockLevelModel;
    }

    private static StockLevelModel getStockLevelModeForWarehouse(final WarehouseModel warehouse,
            final String productId,
            final boolean staged) {

        if (staged) {
            return getStagedStockLevelForWarehouse(productId, warehouse);
        }
        return getStockLevelForWarehouse(productId, warehouse);
    }

    private static StockLevelModel getCnpStockLevelModel(final String productId, final boolean staged) {

        final WarehouseModel warehouseModel = ServiceLookup.getWarehouseService().getWarehouseForCode(CNP_WAREHOUSE);

        if (staged) {
            return getStagedStockLevelForWarehouse(productId, warehouseModel);
        }
        return getStockLevelForWarehouse(productId, warehouseModel);
    }

    private static StockLevelModel getStockLevelForWarehouse(final String productId,
            final WarehouseModel warehouseModel) {

        final ProductModel productModel = ProductUtil.getOnlineProductModel(productId);
        return ServiceLookup.getStockService().getStockLevel(productModel, warehouseModel);
    }

    private static StockLevelModel getStagedStockLevelForWarehouse(final String productId,
            final WarehouseModel warehouseModel) {

        final ProductModel productModel = ProductUtil.getStagedProductModel(productId);
        return ServiceLookup.getStockService().getStockLevel(productModel, warehouseModel);
    }


    /**
     * reserve the stock for the given product
     * 
     * @param product
     * @param amount
     */
    public static void reserveStock(final String product, final int amount) {

        final WarehouseModel warehouseModel = ServiceLookup.getWarehouseService().getDefaultOnlineWarehouse();
        if (StringUtils.isNotEmpty(product) && amount > 0) {
            final ProductModel productModel = ProductUtil.getProductModel(product);
            try {
                ServiceLookup.getTargetStockService().reserve(productModel, warehouseModel, amount,
                        "We have reserved " + amount
                                + " of "
                                + productModel.getName());
            }
            catch (final InsufficientStockLevelException e) {
                LOG.error("insufficient stock to reserve", e);
            }

        }

    }

    /**
     * Lookup NTL stock for product
     * 
     * @param productId
     * @return StockLevelModel
     */
    public static StockLevelModel getStockForWarehouse(final String productId, final String warehouseCode) {

        final StockLevelModel stockLevelModel = getStockLevelModelForWarehouse(productId, warehouseCode);
        Utilities.invalidateCache(stockLevelModel.getPk());
        ServiceLookup.getModelService().refresh(stockLevelModel);
        return stockLevelModel;
    }

    private static StockLevelModel getStockLevelModelForWarehouse(final String productId, final String warehouseCode) {

        final ProductModel productModel = ProductUtil.getProductModel(productId);
        final WarehouseModel warehouseModel = ServiceLookup.getWarehouseService().getWarehouseForCode(warehouseCode);
        return ServiceLookup.getStockService().getStockLevel(productModel, warehouseModel);
    }

    /**
     * Update stock for product. Add if not there.
     * 
     * @param productId
     */
    public static void updateProductStock(final String productId, final int available,
            final String warehouseCode) {

        final WarehouseModel warehouseModel = ServiceLookup.getWarehouseService()
                .getWarehouseForCode(warehouseCode);

        StockLevelModel stockLevel = getStockLevelForWarehouse(productId, warehouseModel);

        if (stockLevel == null) {

            stockLevel = ServiceLookup.getModelService().create(StockLevelModel.class);
            stockLevel.setProductCode(productId);
            stockLevel.setWarehouse(warehouseModel);
        }

        stockLevel.setAvailable(available);
        stockLevel.setReserved(0);

        ServiceLookup.getModelService().save(stockLevel);
    }


    /**
     * Remove given stock level from given warehouse if it exists
     * 
     * @param productId
     * @param warehouseCode
     */
    public static void removeStockLevel(final String productId, final String warehouseCode) {

        final WarehouseModel warehouseModel = ServiceLookup.getWarehouseService()
                .getWarehouseForCode(warehouseCode);

        final StockLevelModel stockLevel = getStockLevelForWarehouse(productId, warehouseModel);
        if (stockLevel != null) {
            ServiceLookup.getModelService().remove(stockLevel);
        }
    }

    /**
     * set max batch size for stock update
     * 
     * @param maxSize
     */
    public static void setMaxBatchSizeForStockUpdate(final int maxSize) {
        ServiceLookup.getStockUpdateIntegrationFacade().setMaxBatchSize(maxSize);
    }

}
