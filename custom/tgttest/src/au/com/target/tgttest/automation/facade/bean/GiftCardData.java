/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author sbryan6
 *
 */
public class GiftCardData {

    private String brandID;
    private Integer maxOrderQuantity;
    private Integer maxOrderValue;
    private String brand;



    /**
     * @return the maxOrderQuantity
     */
    public Integer getMaxOrderQuantity() {
        return maxOrderQuantity;
    }

    /**
     * @param maxOrderQuantity
     *            the maxOrderQuantity to set
     */
    public void setMaxOrderQuantity(final Integer maxOrderQuantity) {
        this.maxOrderQuantity = maxOrderQuantity;
    }

    /**
     * @return the maxOrderValue
     */
    public Integer getMaxOrderValue() {
        return maxOrderValue;
    }

    /**
     * @param maxOrderValue
     *            the maxOrderValue to set
     */
    public void setMaxOrderValue(final Integer maxOrderValue) {
        this.maxOrderValue = maxOrderValue;
    }

    /**
     * @return the brandID
     */
    public String getBrandID() {
        return brandID;
    }

    /**
     * @param brandID
     *            the brandID to set
     */
    public void setBrandID(final String brandID) {
        this.brandID = brandID;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }


}
