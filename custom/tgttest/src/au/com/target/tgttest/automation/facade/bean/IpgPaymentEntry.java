/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * This is to used in automation test.
 * 
 * The format of each payment should be <paymentId>,<amount> e.g. GC1,12.9
 * 
 * @author htan3
 *
 */
public class IpgPaymentEntry {

    private String paymentEntry;
    private boolean declined;

    /**
     * @return the paymentEntry
     */
    public String getPaymentEntry() {
        return paymentEntry;
    }

    /**
     * @param paymentEntry
     *            the paymentEntry to set
     */
    public void setPaymentEntry(final String paymentEntry) {
        this.paymentEntry = paymentEntry;
    }

    /**
     * @return the declined
     */
    public boolean isDeclined() {
        return declined;
    }

    /**
     * @param declined
     *            the declined to set
     */
    public void setDeclined(final boolean declined) {
        this.declined = declined;
    }

}
