/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtfulfilment.dao.TargetGlobalStoreFulfilmentCapabilitiesDao;
import au.com.target.tgtfulfilment.model.CategoryExclusionsModel;
import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.model.ProductExclusionsModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgttest.automation.ImpexImporter;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.ExclusionData;
import au.com.target.tgttest.automation.facade.bean.StoreFulfilmentCapabilitiesInfo;
import org.junit.Assert;


/**
 * Utility for dealing with store fulfilment capabilities
 * 
 * @author jjayawa1
 *
 */
public class StoreFulfilmentCapabilitiesUtil {

    private static final Logger LOG = Logger.getLogger(StoreFulfilmentCapabilitiesUtil.class);

    private StoreFulfilmentCapabilitiesUtil() {
        // util
    }

    public static void reset() {
        ImpexImporter.importCsv("/tgttest/automation-impex/store/store-fulfilment-capabilities.impex");

        // Passing all global routing rules by default
        final GlobalStoreFulfilmentCapabilitiesModel model = ServiceLookup
                .getTargetGlobalStoreFulfilmentCapabilitiesDao().getGlobalStoreFulfilmentCapabilities();
        model.setEnabled(true);
        model.setBlackoutStart(null);
        model.setBlackoutEnd(null);
        model.setMaxItemsPerConsignment(Integer.valueOf(50));
        model.setMaxTimeToPick(Integer.valueOf(180));
        model.setNumberOfRoutingAttempts(Integer.valueOf(1));
        model.setPullBackTolerance(Integer.valueOf(80));
        model.setAllowInterstoreDelivery(true);
        model.setProductExclusions(new HashSet<ProductExclusionsModel>());
        model.setExcludeCategories(new HashSet<CategoryExclusionsModel>());
        model.setDeliveryModes(new HashSet<TargetZoneDeliveryModeModel>());
        setAutomationDeliveryModes(model);

        ServiceLookup.getModelService().save(model);
    }

    /**
     * Method to set all automation delivery mode as enabled for in-store fulfilment
     * 
     * @param model
     */
    private static void setAutomationDeliveryModes(final GlobalStoreFulfilmentCapabilitiesModel model) {
        model.getDeliveryModes().add((TargetZoneDeliveryModeModel)ServiceLookup.getTargetDeliveryService()
                .getDeliveryModeForCode(CheckoutUtil.HOME_DELIVERY_CODE));
        model.getDeliveryModes().add((TargetZoneDeliveryModeModel)ServiceLookup.getTargetDeliveryService()
                .getDeliveryModeForCode(CheckoutUtil.CLICK_AND_COLLECT_CODE));
        model.getDeliveryModes().add((TargetZoneDeliveryModeModel)ServiceLookup.getTargetDeliveryService()
                .getDeliveryModeForCode(CheckoutUtil.EXPRESS_DELIVERY_CODE));
    }

    /**
     * Method to return GlobalStoreFulfilmentCapabilities model
     * 
     * @return GlobalStoreFulfilmentCapabilitiesModel
     */
    public static GlobalStoreFulfilmentCapabilitiesModel getGlobalStoreFulfilmentCapabilitiesModel() {
        final TargetGlobalStoreFulfilmentCapabilitiesDao targetGlobalStoreFulfilmentCapabilitiesDao = ServiceLookup
                .getTargetGlobalStoreFulfilmentCapabilitiesDao();
        final GlobalStoreFulfilmentCapabilitiesModel model = targetGlobalStoreFulfilmentCapabilitiesDao
                .getGlobalStoreFulfilmentCapabilities();
        return model;
    }

    public static void tearDown() {
        reset();
    }

    /**
     * Set the given fulfilment capabilities on the store
     * 
     * @param storeNumber
     * @param storeData
     */
    public static void setFulfilmentCapabilitiesForStore(final int storeNumber,
            final StoreFulfilmentCapabilitiesInfo storeData) {

        // Get the StoreFulfilmentCapabilitiesModel if it exists for the store
        final TargetPointOfServiceModel store = getStore(storeNumber);
        final StoreFulfilmentCapabilitiesModel storeFulfilmentCapabilitiesModel = store.getFulfilmentCapability();

        // If not enabled then the store capabilities might not be there which is ok
        if (storeFulfilmentCapabilitiesModel == null) {
            if (storeData.getInstoreEnabled().equalsIgnoreCase("no")) {

                LOG.info("Store has no fulfilment capabilities capabilities record - ok skipping other fields: "
                        + storeNumber);
                return;
            }
            else {
                throw new IllegalArgumentException(
                        "Cannot set store enabled for fulfilment since it has no fulfilment capabilities record: "
                                + storeNumber);
            }
        }

        storeFulfilmentCapabilitiesModel.setEnabled(Boolean.valueOf("yes".equalsIgnoreCase(storeData
                .getInstoreEnabled())));

        // Set the optional store specific capabilities
        if (StringUtils.isNotEmpty(storeData.getDeliveryModesAllowed())) {
            storeFulfilmentCapabilitiesModel.setDeliveryModes(getDeliveryModesSet(storeData
                    .getDeliveryModesAllowed()));
        }
        if (StringUtils.isNotEmpty(storeData.getAllowDeliveryToSameStore())) {
            storeFulfilmentCapabilitiesModel.setAllowDeliveryToThisStore(Boolean.valueOf("yes"
                    .equalsIgnoreCase(storeData.getAllowDeliveryToSameStore())));
        }
        if (StringUtils.isNotEmpty(storeData.getAllowDeliveryToAnotherStore())) {
            storeFulfilmentCapabilitiesModel.setAllowDeliveryToAnotherStore(Boolean.valueOf("yes"
                    .equalsIgnoreCase(storeData.getAllowDeliveryToAnotherStore())));
        }

        if (StringUtils.isNotEmpty(storeData.getMerchantLocationId())) {
            storeFulfilmentCapabilitiesModel.setMerchantLocationId(storeData.getMerchantLocationId());
        }

        if (StringUtils.isNotEmpty(storeData.getChargeCode())) {
            storeFulfilmentCapabilitiesModel.setChargeCode(storeData.getChargeCode());
        }
        if (null != storeData.getChargeAccountNumber()) {
            storeFulfilmentCapabilitiesModel.setChargeAccountNumber(storeData.getChargeAccountNumber());
        }
        if (StringUtils.isNotEmpty(storeData.getUsername())) {
            storeFulfilmentCapabilitiesModel.setUsername(storeData.getUsername());
        }
        if (null != storeData.getProductCode()) {
            storeFulfilmentCapabilitiesModel.setProductCode(storeData.getProductCode());
        }

        if (null != storeData.getServiceCode()) {
            storeFulfilmentCapabilitiesModel.setServiceCode(storeData.getServiceCode());
        }

        if (storeData.isClearProductExclusions()) {
            for (final ProductExclusionsModel productExclusionsModel : storeFulfilmentCapabilitiesModel
                    .getProductExclusions()) {
                ServiceLookup.getModelService().remove(productExclusionsModel);
            }
        }

        if (storeData.isClearCategoryExclusions()) {
            for (final CategoryExclusionsModel categoryExclusionsModel : storeFulfilmentCapabilitiesModel
                    .getCategoryExclusions()) {
                ServiceLookup.getModelService().remove(categoryExclusionsModel);
            }
        }

        saveStoreFulfilmentCapabilitiesModel(storeFulfilmentCapabilitiesModel);

    }


    /**
     * Set the given max allowed orders on the store
     * 
     * @param storeNumber
     * @param maxAllowedOrdersPerDay
     */
    public static void setStoreWithMaxOrderLimit(final int storeNumber, final String maxAllowedOrdersPerDay) {
        final TargetPointOfServiceModel store = getStore(storeNumber);
        final StoreFulfilmentCapabilitiesModel storeFulfilmentCapabilitiesModel = store.getFulfilmentCapability();
        if (!maxAllowedOrdersPerDay.equalsIgnoreCase("null")) {
            storeFulfilmentCapabilitiesModel.setMaxConsignmentsForDeliveryPerDay(Integer
                    .valueOf(maxAllowedOrdersPerDay));
        }
        else {
            storeFulfilmentCapabilitiesModel.setMaxConsignmentsForDeliveryPerDay(null);
        }
        saveStoreFulfilmentCapabilitiesModel(storeFulfilmentCapabilitiesModel);
    }

    /**
     * Set the global start time for order limits
     * 
     * @param startTime
     */
    public static void setGlobalStartTimeForOrderLimit(final String startTime) {

        final GlobalStoreFulfilmentCapabilitiesModel model = StoreFulfilmentCapabilitiesUtil
                .getGlobalStoreFulfilmentCapabilitiesModel();
        model.setMaxOrderPeriodStartTime(startTime);
        StoreFulfilmentCapabilitiesUtil.saveGlobalStoreFulfilmentCapabilitiesModel(model);
    }

    /**
     * clear both category and product exclusion list
     * 
     */
    public static void clearGlobalExclusionData() {

        final GlobalStoreFulfilmentCapabilitiesModel model = StoreFulfilmentCapabilitiesUtil
                .getGlobalStoreFulfilmentCapabilitiesModel();
        model.setProductExclusions(new HashSet<ProductExclusionsModel>());
        model.setExcludeCategories(new HashSet<CategoryExclusionsModel>());
        StoreFulfilmentCapabilitiesUtil.saveGlobalStoreFulfilmentCapabilitiesModel(model);
    }

    /**
     * Set product weight limit on a store
     * 
     * @param storeNumber
     * @param weight
     */
    public static void setProductWeightLimitForStore(final int storeNumber, final String weight) {

        Double allowedWeight = null;
        final TargetPointOfServiceModel storeModel = getStore(storeNumber);
        final StoreFulfilmentCapabilitiesModel storeFulfilmentCapabilitiesModel = storeModel.getFulfilmentCapability();
        if (!weight.equalsIgnoreCase("null")) {
            allowedWeight = Double.valueOf(weight);
        }
        storeFulfilmentCapabilitiesModel.setMaxAllowedProductWeightForDelivery(allowedWeight);
        ServiceLookup.getModelService().save(storeFulfilmentCapabilitiesModel);
    }

    /**
     * Set global blackout
     * 
     * @param startDate
     * @param endDate
     */
    public static void setGlobalBlackout(final Date startDate, final Date endDate) {

        final GlobalStoreFulfilmentCapabilitiesModel model = StoreFulfilmentCapabilitiesUtil
                .getGlobalStoreFulfilmentCapabilitiesModel();
        model.setBlackoutEnd(endDate);
        model.setBlackoutStart(startDate);
        StoreFulfilmentCapabilitiesUtil.saveGlobalStoreFulfilmentCapabilitiesModel(model);
    }

    /**
     * Set global max routing attempts
     * 
     * @param routingAttempts
     */
    public static void setGlobalMaxRoutingAttempts(final int routingAttempts) {
        final GlobalStoreFulfilmentCapabilitiesModel model = StoreFulfilmentCapabilitiesUtil
                .getGlobalStoreFulfilmentCapabilitiesModel();
        model.setNumberOfRoutingAttempts(Integer.valueOf(routingAttempts));
        StoreFulfilmentCapabilitiesUtil.saveGlobalStoreFulfilmentCapabilitiesModel(model);
    }

    /**
     * Set store blackout
     * 
     * @param startDate
     * @param endDate
     * @param storeNumber
     */
    public static void setStoreBlackout(final Date startDate, final Date endDate,
            final int storeNumber) {

        final TargetPointOfServiceModel store = getStore(storeNumber);
        final StoreFulfilmentCapabilitiesModel storeFulfilmentCapabilitiesModel = store.getFulfilmentCapability();
        storeFulfilmentCapabilitiesModel.setBlackoutEnd(endDate);
        storeFulfilmentCapabilitiesModel.setBlackoutStart(startDate);
        saveStoreFulfilmentCapabilitiesModel(storeFulfilmentCapabilitiesModel);
    }

    /**
     * Set whether to enable instore fulfilment globally
     * 
     * @param enabled
     */
    public static void setGlobalInstoreFulfilmentEnabled(final boolean enabled) {

        final GlobalStoreFulfilmentCapabilitiesModel model = StoreFulfilmentCapabilitiesUtil
                .getGlobalStoreFulfilmentCapabilitiesModel();
        model.setEnabled(enabled);
        StoreFulfilmentCapabilitiesUtil.saveGlobalStoreFulfilmentCapabilitiesModel(model);
    }

    /**
     * Set global allowed delivery modes
     * 
     * @param deliveryModesAllowed
     */
    public static void setGlobalDeliveryModesAllowed(final String deliveryModesAllowed) {
        final GlobalStoreFulfilmentCapabilitiesModel model = StoreFulfilmentCapabilitiesUtil
                .getGlobalStoreFulfilmentCapabilitiesModel();
        model.setDeliveryModes(getDeliveryModesSet(deliveryModesAllowed));
        saveGlobalStoreFulfilmentCapabilitiesModel(model);
    }

    public static void setGlobalExclusionProducts(final Date startDate, final Date endDate,
            final String productCode) {
        final GlobalStoreFulfilmentCapabilitiesModel model = StoreFulfilmentCapabilitiesUtil
                .getGlobalStoreFulfilmentCapabilitiesModel();

        model.setProductExclusions(getProductExclusionsList(startDate, endDate, productCode));
        saveGlobalStoreFulfilmentCapabilitiesModel(model);
    }

    private static Set<ProductExclusionsModel> getProductExclusionsList(final Date startDate, final Date endDate,
            final String productCode) {
        final Set<ProductExclusionsModel> exclusionList = new HashSet<>();
        final ProductExclusionsModel productExclusion = ServiceLookup.getModelService().create(
                ProductExclusionsModel.class);
        final ProductModel product = getBaseProduct(productCode);
        productExclusion.setProduct(product);
        productExclusion.setExclusionStartDate(startDate);
        productExclusion.setExclusionEndDate(endDate);
        exclusionList.add(productExclusion);
        return exclusionList;
    }

    public static ProductModel getBaseProduct(final String productCode) {
        ProductModel product = ((VariantProductModel)ProductUtil.getProductModel(productCode)).getBaseProduct();
        while (product instanceof VariantProductModel) {
            product = ((VariantProductModel)product).getBaseProduct();
        }
        return product;
    }

    public static void setGlobalMaxItemsPerConsignment(final int maxItemsPerConsignment) {
        final GlobalStoreFulfilmentCapabilitiesModel model = StoreFulfilmentCapabilitiesUtil
                .getGlobalStoreFulfilmentCapabilitiesModel();
        model.setMaxItemsPerConsignment(Integer.valueOf(maxItemsPerConsignment));
        saveGlobalStoreFulfilmentCapabilitiesModel(model);
    }

    public static void setGlobalInterstoreRoutingCapability(final boolean allowInterstore) {
        final GlobalStoreFulfilmentCapabilitiesModel model = StoreFulfilmentCapabilitiesUtil
                .getGlobalStoreFulfilmentCapabilitiesModel();
        model.setAllowInterstoreDelivery(allowInterstore);
        saveGlobalStoreFulfilmentCapabilitiesModel(model);
    }

    /**
     * set global category exclusion
     * 
     * @param excludeCategoies
     */
    public static void setGlobalCategoryExclusion(final List<ExclusionData> excludeCategoies) {
        final GlobalStoreFulfilmentCapabilitiesModel model = StoreFulfilmentCapabilitiesUtil
                .getGlobalStoreFulfilmentCapabilitiesModel();
        CatalogUtil.setSessionCatalogVersion();
        setExclusionCategoryData(excludeCategoies, model);
        StoreFulfilmentCapabilitiesUtil.saveGlobalStoreFulfilmentCapabilitiesModel(model);
    }

    /**
     * set same store category exclusion
     * 
     * @param pos
     * @param excludeCategoies
     */
    public static void setSameStoreCategoryExclusion(final TargetPointOfServiceModel pos,
            final List<ExclusionData> excludeCategoies) {
        final StoreFulfilmentCapabilitiesModel model = pos.getFulfilmentCapability();
        model.setCategoryExclusions(null);
        StoreFulfilmentCapabilitiesUtil.saveStoreFulfilmentCapabilitiesModel(model);
        CatalogUtil.setSessionCatalogVersion();
        setExclusionCategoryData(excludeCategoies, model);
        StoreFulfilmentCapabilitiesUtil.saveStoreFulfilmentCapabilitiesModel(model);
    }

    /**
     * Set Store Product Exclusion
     * 
     * @param excludedProductsForStore
     * @param tpos
     */
    public static void setExclusionProductData(final List<ExclusionData> excludedProductsForStore,
            final TargetPointOfServiceModel tpos) {
        final StoreFulfilmentCapabilitiesModel model = tpos.getFulfilmentCapability();
        model.setProductExclusions(null);
        final Set<StoreFulfilmentCapabilitiesModel> sfcmModels = new HashSet<>();
        sfcmModels.add(model);
        StoreFulfilmentCapabilitiesUtil.saveStoreFulfilmentCapabilitiesModel(model);
        CatalogUtil.setSessionCatalogVersion();
        for (final ExclusionData storeExcludeProductData : excludedProductsForStore) {
            final ProductModel product = ServiceLookup.getProductService()
                    .getProductForCode(storeExcludeProductData.getProductCode());
            final ProductExclusionsModel excludeProduct = ServiceLookup.getModelService()
                    .create(ProductExclusionsModel.class);
            excludeProduct.setProduct(product);
            excludeProduct.setExclusionStartDate(storeExcludeProductData.getStartDate());
            excludeProduct.setExclusionEndDate(storeExcludeProductData.getEndDate());
            excludeProduct.setStoreFulfilmentCapabilities(sfcmModels);
            ServiceLookup.getModelService().save(excludeProduct);
        }
        StoreFulfilmentCapabilitiesUtil.saveStoreFulfilmentCapabilitiesModel(model);
    }

    public static void setExclusionProductType(final String productType, final TargetPointOfServiceModel storeModel) {
        final StoreFulfilmentCapabilitiesModel capabilityModel = storeModel.getFulfilmentCapability();
        if (StringUtils.isEmpty(productType)) {
            capabilityModel.setProductTypes(null);
        }
        else {
            final ProductTypeModel productTypeModel = ServiceLookup.getProductTypeService().getByCode(productType);
            Assert.assertNotNull(productTypeModel);
            final Set<ProductTypeModel> productTypes = new HashSet<>();
            productTypes.add(productTypeModel);

            capabilityModel.setProductTypes(productTypes);
            ServiceLookup.getModelService().save(capabilityModel);
        }
        ServiceLookup.getModelService().refresh(storeModel);
    }

    private static void setExclusionCategoryData(final List<ExclusionData> excludeCategoies,
            final ItemModel model) {
        for (final ExclusionData excludeCategoryData : excludeCategoies) {
            final CategoryModel cat = ServiceLookup.getCategoryService().getCategoryForCode(
                    excludeCategoryData.getCategoryCode());
            final CategoryExclusionsModel excludeCategory = ServiceLookup.getModelService()
                    .create(CategoryExclusionsModel.class);
            excludeCategory.setCategory(cat);
            excludeCategory.setExclusionStartDate(excludeCategoryData.getStartDate());
            excludeCategory.setExclusionEndDate(excludeCategoryData.getEndDate());
            if (model instanceof GlobalStoreFulfilmentCapabilitiesModel) {
                excludeCategory.setGlobalStoreFulfilmentCapabilities((GlobalStoreFulfilmentCapabilitiesModel)model);
            }
            else if (model instanceof StoreFulfilmentCapabilitiesModel) {
                final Set<StoreFulfilmentCapabilitiesModel> sfcmModels = new HashSet<>();
                sfcmModels.add((StoreFulfilmentCapabilitiesModel)model);
                excludeCategory.setStoreFulfilmentCapabilities(sfcmModels);
            }
            ServiceLookup.getModelService().save(excludeCategory);
        }
    }

    private static TargetPointOfServiceModel getStore(final int storeNumber) {

        try {
            final TargetPointOfServiceService targetPointOfServiceService = ServiceLookup
                    .getTargetPointOfServiceService();
            return targetPointOfServiceService.getPOSByStoreNumber(Integer.valueOf(storeNumber));
        }
        catch (final Exception e) {
            throw new IllegalArgumentException("Can't find store " + storeNumber, e);
        }
    }

    private static Set<TargetZoneDeliveryModeModel> getDeliveryModesSet(final String deliveryModeCodes) {

        // Map the codes to the auto delivery codes
        final String[] deliveryModes = deliveryModeCodes.split(",");
        final Set<TargetZoneDeliveryModeModel> set = new HashSet<>();

        for (int i = 0; i < deliveryModes.length; i++) {
            if (CheckoutUtil.HOME_DELIVERY_CODE.equalsIgnoreCase(deliveryModes[i].trim())) {
                set.add(getDeliveryModeForCode(CheckoutUtil.HOME_DELIVERY_CODE));
            }
            else if (CheckoutUtil.CLICK_AND_COLLECT_CODE.equalsIgnoreCase(deliveryModes[i].trim())) {
                set.add(getDeliveryModeForCode(CheckoutUtil.CLICK_AND_COLLECT_CODE));
            }
            else if (CheckoutUtil.EXPRESS_DELIVERY_CODE.equalsIgnoreCase(deliveryModes[i].trim())) {
                set.add(getDeliveryModeForCode(CheckoutUtil.EXPRESS_DELIVERY_CODE));
            }
            else if (CheckoutUtil.EBAY_HOME_DELIVERY_CODE.equalsIgnoreCase(deliveryModes[i].trim())) {
                set.add(getDeliveryModeForCode(CheckoutUtil.EBAY_HOME_DELIVERY_CODE));
            }
            else if (CheckoutUtil.EBAY_CLICK_AND_COLLECT_CODE.equalsIgnoreCase(deliveryModes[i].trim())) {
                set.add(getDeliveryModeForCode(CheckoutUtil.EBAY_CLICK_AND_COLLECT_CODE));
            }
        }

        return set;
    }

    private static TargetZoneDeliveryModeModel getDeliveryModeForCode(final String deliveryModeCode) {

        final TargetDeliveryService targetDeliveryService = ServiceLookup.getTargetDeliveryService();
        final TargetZoneDeliveryModeModel deliveryModeModel = (TargetZoneDeliveryModeModel)targetDeliveryService
                .getDeliveryModeForCode(deliveryModeCode);
        return deliveryModeModel;
    }

    private static void saveStoreFulfilmentCapabilitiesModel(final StoreFulfilmentCapabilitiesModel model) {
        final ModelService modelService = ServiceLookup.getModelService();
        modelService.save(model);
    }

    public static void saveGlobalStoreFulfilmentCapabilitiesModel(final GlobalStoreFulfilmentCapabilitiesModel model) {
        final ModelService modelService = ServiceLookup.getModelService();
        modelService.save(model);
    }
}
