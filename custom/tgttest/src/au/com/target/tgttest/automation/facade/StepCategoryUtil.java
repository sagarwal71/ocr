/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import java.util.Collections;
import java.util.List;

import au.com.target.tgttest.automation.facade.bean.CategoryImportData;
import au.com.target.tgtwsfacades.integration.dto.IntegrationPrincipalDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationSupercategoryDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationTargetProductCategoryDto;


/**
 * @author bpottass
 *
 */
public class StepCategoryUtil {


    private StepCategoryUtil() {

    }

    public static IntegrationTargetProductCategoryDto getIntegrationTargetProductCategoryDto(
            final CategoryImportData importData) {
        final IntegrationTargetProductCategoryDto dto = new IntegrationTargetProductCategoryDto();
        dto.setCode(importData.getCategoryCode());
        dto.setName(importData.getCategoryName());

        final IntegrationPrincipalDto principalDto = new IntegrationPrincipalDto();
        principalDto.setUid(importData.getPrincipalUid());
        final List<IntegrationPrincipalDto> allowedPrincipals = Collections.singletonList(principalDto);
        dto.setAllowedPrincipals(allowedPrincipals);

        final IntegrationSupercategoryDto superCategoryDto = new IntegrationSupercategoryDto();
        superCategoryDto.setCode(importData.getSupercategoryCode());
        final List<IntegrationSupercategoryDto> supercategories = Collections.singletonList(superCategoryDto);
        dto.setSupercategories(supercategories);

        return dto;

    }

}
