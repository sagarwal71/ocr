/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemResponseDto;
import au.com.target.tgtfluent.data.Attribute;
import au.com.target.tgtfluent.data.AttributeType;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.FluentAttributeData;


/**
 * @author mgazal
 *
 */
public class FluentClientUtil {

    private FluentClientUtil() {
        // utility
    }

    /**
     * @param option
     * @return fulfilmentOptionMap
     */
    public static Map<String, Integer> getFulfilmentOption(final String option) {
        Map<String, Map<String, Integer>> fulfilmentOptionMap = ServiceLookup.getFluentClientMock()
                .getFulfilmentOptionMap();
        if (fulfilmentOptionMap == null) {
            fulfilmentOptionMap = new LinkedHashMap<>();
            ServiceLookup.getFluentClientMock().setFulfilmentOptionMap(fulfilmentOptionMap);
        }
        Map<String, Integer> fulfilmentOption = fulfilmentOptionMap.get(option);
        if (fulfilmentOption == null) {
            fulfilmentOption = new LinkedHashMap<>();
            fulfilmentOptionMap.put(option, fulfilmentOption);
        }
        return fulfilmentOption;
    }

    /**
     * @param stockLevels
     */
    public static void setStoreStockMock(final List<StockVisibilityItemResponseDto> stockLevels) {
        for (final StockVisibilityItemResponseDto stockVisibilityItemResponseDto : stockLevels) {
            getFulfilmentOption(stockVisibilityItemResponseDto.getStoreNumber()).put(
                    stockVisibilityItemResponseDto.getCode(), Integer.valueOf(stockVisibilityItemResponseDto.getSoh()));
        }
    }

    /**
     * 
     * @param fluentAttributeData
     * @return list of fluent Attributes
     */
    public static List<Attribute> createAttributes(final List<FluentAttributeData> fluentAttributeData) {
        final List<Attribute> attributes = new ArrayList();
        for (final FluentAttributeData attributeData : fluentAttributeData) {
            final Attribute attribute = new Attribute();
            attribute.setName(attributeData.getName());
            attribute.setType(AttributeType.valueOf(attributeData.getType()));
            attribute.setValue(attributeData.getValue());
            attributes.add(attribute);
        }
        return attributes;
    }
}
