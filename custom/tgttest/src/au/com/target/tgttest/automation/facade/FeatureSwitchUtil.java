/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetFeatureSwitchModel;
import au.com.target.tgttest.automation.ServiceLookup;


/**
 * @author bhuang3
 *
 */
public class FeatureSwitchUtil {

    private FeatureSwitchUtil() {
        //
    }

    public static void switchOnTheFeature(final String name) {
        final TargetFeatureSwitchModel feature = ServiceLookup.getTargetFeatureSwitchDao().getFeatureByName(name);
        feature.setEnabled(Boolean.TRUE);
        ServiceLookup.getModelService().save(feature);
        ServiceLookup.getModelService().refresh(feature);
    }

    public static void switchOffTheFeature(final String name) {
        final TargetFeatureSwitchModel feature = ServiceLookup.getTargetFeatureSwitchDao().getFeatureByName(name);
        feature.setEnabled(Boolean.FALSE);
        ServiceLookup.getModelService().save(feature);
        ServiceLookup.getModelService().refresh(feature);
    }

    public static void setDefaultFeatureSwitches() {
        FeatureSwitchUtil.switchOnTheFeature(TgtCoreConstants.FeatureSwitch.SHIPSTER);
    }

}
