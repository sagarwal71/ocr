/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

import java.util.Date;


/**
 * @author smishra1
 *
 */
public class ExclusionData {

    private String categoryCode;
    private String productCode;
    private String excludeStartDate;
    private String excludeEndDate;
    private Date startDate;
    private Date endDate;


    /**
     * @return the categoryCode
     */
    public String getCategoryCode() {
        return categoryCode;
    }

    /**
     * @param categoryCode
     *            the categoryCode to set
     */
    public void setCategoryCode(final String categoryCode) {
        this.categoryCode = categoryCode;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate
     *            the startDate to set
     */
    public void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate
     *            the endDate to set
     */
    public void setEndDate(final Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the excludeStartDate
     */
    public String getExcludeStartDate() {
        return excludeStartDate;
    }

    /**
     * @param excludeStartDate
     *            the excludeStartDate to set
     */
    public void setExcludeStartDate(final String excludeStartDate) {
        this.excludeStartDate = excludeStartDate;
    }

    /**
     * @return the excludeEndDate
     */
    public String getExcludeEndDate() {
        return excludeEndDate;
    }

    /**
     * @param excludeEndDate
     *            the excludeEndDate to set
     */
    public void setExcludeEndDate(final String excludeEndDate) {
        this.excludeEndDate = excludeEndDate;
    }


}
