/**
 * 
 */
package au.com.target.tgttest.automation.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgttinker.mock.instore.ProductData;


/**
 * @author sbryan6
 *
 */
public final class ProductQuantityParser {

    private ProductQuantityParser() {
        // util
    }

    /**
     * Parse input string into a list of (product, qty)
     * 
     * @param input
     * @return list
     */
    public static List<ProductData> parseProductList(final String input) {

        final List<ProductData> list = new ArrayList<>();

        if (StringUtils.isNotEmpty(input)) {
            final String[] listOfProducts = input.split(",");
            for (final String productString : listOfProducts) {
                if (StringUtils.isNotEmpty(productString)) {
                    final String[] parts = productString.split("of");
                    final ProductData product = new ProductData();
                    if (parts.length == 2) {
                        product.setCode(StringUtils.trim(parts[1]));
                        product.setQuantity(StringUtils.trim(parts[0]));
                        list.add(product);
                    }
                }
            }
        }

        return list;

    }

}
