/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author Nandini
 *
 */
public class WarehouseEntry {

    private String warehouseCode;
    private Integer storeNumber;


    /**
     * @return the warehouseCode
     */
    public String getWarehouseCode() {
        return warehouseCode;
    }

    /**
     * @param warehouseCode
     *            the warehouseCode to set
     */
    public void setWarehouseCode(final String warehouseCode) {
        this.warehouseCode = warehouseCode;
    }

    /**
     * @return the storeNumber
     */
    public Integer getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final Integer storeNumber) {
        this.storeNumber = storeNumber;
    }

}
