/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author Vivek
 *
 */
public class ConsignmentForStore {

    private final String items;
    private final String consignmentCode;
    private final String createdDate;
    private final String waveDate;
    private final String pickDate;
    private final String shipDate;
    private final String packDate;
    private final String cancelDate;
    private final String customer;
    private final String rejectReason;
    private final String consignmentStatus;
    private final String deliveryMethod;
    private final Integer cncStoreNumber;
    private final String manifestID;
    private final Integer boxes;
    private final String destination;
    private final String warehouse;
    private final String minutesSinceWaved;
    private final String order;
    private final String consignmentOneStatus;
    private final String consignmentOneReason;
    private final String consignmentTwoStatus;
    private final String consignmentTwoWarehouse;
    private final int minutesSinceCreated;
    private String orderType;
    private String comment;
    private String trackingId;
    private String products;
    private boolean clearance;
    private boolean bulky;
    private Integer parcelCount;
    private String carrier;
    private String email;
    private String phone;
    private String sendNotifications;

    /**
     * @param items
     * @param consignmentCode
     * @param createdDate
     * @param waveDate
     * @param pickDate
     * @param shipDate
     * @param cancelDate
     * @param customer
     * @param rejectReason
     * @param consignmentStatus
     * @param deliveryMethod
     * @param cncStoreNumber
     * @param manifestID
     * @param boxes
     * @param warehouse
     * @param minutesSinceWaved
     * @param order
     * @param consignmentOneStatus
     * @param consignmentOneReason
     * @param consignmentTwoStatus
     * @param consignmentTwoWarehouse
     * @param destination
     */
    public ConsignmentForStore(final String items, final String consignmentCode, final String createdDate,
            final String waveDate,
            final String pickDate, final String packDate, final String shipDate, final String cancelDate,
            final String customer,
            final String rejectReason,
            final String consignmentStatus, final String deliveryMethod, final Integer cncStoreNumber,
            final String manifestID,
            final Integer boxes,
            final String destination, final String warehouse, final String minutesSinceWaved,
            final String order, final String consignmentOneStatus, final String consignmentOneReason,
            final String consignmentTwoStatus, final String consignmentTwoWarehouse, final int minutesSinceCreated) {
        super();
        this.items = items;
        this.consignmentCode = consignmentCode;
        this.createdDate = createdDate;
        this.waveDate = waveDate;
        this.pickDate = pickDate;
        this.packDate = packDate;
        this.shipDate = shipDate;
        this.cancelDate = cancelDate;
        this.customer = customer;
        this.rejectReason = rejectReason;
        this.consignmentStatus = consignmentStatus;
        this.deliveryMethod = deliveryMethod;
        this.cncStoreNumber = cncStoreNumber;
        this.manifestID = manifestID;
        this.boxes = boxes;
        this.destination = destination;
        this.warehouse = warehouse;
        this.minutesSinceWaved = minutesSinceWaved;
        this.order = order;
        this.consignmentOneStatus = consignmentOneStatus;
        this.consignmentOneReason = consignmentOneReason;
        this.consignmentTwoStatus = consignmentTwoStatus;
        this.consignmentTwoWarehouse = consignmentTwoWarehouse;
        this.minutesSinceCreated = minutesSinceCreated;
    }


    /**
     * Convenience method to create a consignment with basic details
     * 
     * @param code
     * @param status
     * @param deliveryMethod
     * @param cncStoreNumber
     * @return ConsignmentForStore
     */
    public static ConsignmentForStore createBasicConsignment(final String code, final String status,
            final String deliveryMethod, final Integer cncStoreNumber) {

        final ConsignmentForStore cons = new ConsignmentForStore(null, code,
                null, null, null, null, null, null, null, null,
                status, deliveryMethod, cncStoreNumber,
                null, null,
                null, null, null, null, null, null, null, null, 0);
        return cons;
    }

    /**
     * @return the items
     */
    public String getItems() {
        return items;
    }

    /**
     * @return the consignmentCode
     */
    public String getConsignmentCode() {
        return consignmentCode;
    }

    /**
     * @return the createdDate
     */
    public String getCreatedDate() {
        return createdDate;
    }

    /**
     * @return the waveDate
     */
    public String getWaveDate() {
        return waveDate;
    }

    /**
     * @return the pickDate
     */
    public String getPickDate() {
        return pickDate;
    }

    /**
     * @return the shipDate
     */
    public String getShipDate() {
        return shipDate;
    }

    /**
     * @return the cancelDate
     */
    public String getCancelDate() {
        return cancelDate;
    }

    /**
     * @return the customer
     */
    public String getCustomer() {
        return customer;
    }

    /**
     * @return the rejectReason
     */
    public String getRejectReason() {
        return rejectReason;
    }

    /**
     * @return the consignmentStatus
     */
    public String getConsignmentStatus() {
        return consignmentStatus;
    }

    /**
     * @return the deliveryMethod
     */
    public String getDeliveryMethod() {
        return deliveryMethod;
    }

    /**
     * @return the storeNo
     */
    public Integer getCncStoreNumber() {
        return cncStoreNumber;
    }

    /**
     * @return the manifestID
     */
    public String getManifestID() {
        return manifestID;
    }

    /**
     * @return the boxes
     */
    public Integer getBoxes() {
        return boxes;
    }

    /**
     * @return the destination
     */
    public String getDestination() {
        return destination;
    }

    /**
     * @return the order
     */
    public String getOrder() {
        return order;
    }

    /**
     * @return the consignmentOneStatus
     */
    public String getConsignmentOneStatus() {
        return consignmentOneStatus;
    }

    /**
     * @return the consignmentOneReason
     */
    public String getConsignmentOneReason() {
        return consignmentOneReason;
    }

    /**
     * @return the consignmentTwoStatus
     */
    public String getConsignmentTwoStatus() {
        return consignmentTwoStatus;
    }

    /**
     * @return the consignmentTwoWarehouse
     */
    public String getConsignmentTwoWarehouse() {
        return consignmentTwoWarehouse;
    }

    /**
     * @return the warehouse
     */
    public String getWarehouse() {
        return warehouse;
    }

    /**
     * @return the minutesSinceWaved
     */
    public String getMinutesSinceWaved() {
        return minutesSinceWaved;
    }

    /**
     * @return the minutesSinceCreated
     */
    public int getMinutesSinceCreated() {
        return minutesSinceCreated;
    }


    /**
     * @return the packDate
     */
    public String getPackDate() {
        return packDate;
    }


    /**
     * @return the orderType
     */
    public String getOrderType() {
        return orderType;
    }


    /**
     * @param orderType
     *            the orderType to set
     */
    public void setOrderType(final String orderType) {
        this.orderType = orderType;
    }


    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }


    /**
     * @param comment
     *            the comment to set
     */
    public void setComment(final String comment) {
        this.comment = comment;
    }


    /**
     * @return the trackingId
     */
    public String getTrackingId() {
        return trackingId;
    }


    /**
     * @param trackingId
     *            the trackingId to set
     */
    public void setTrackingId(final String trackingId) {
        this.trackingId = trackingId;
    }


    /**
     * @return the products
     */
    public String getProducts() {
        return products;
    }


    /**
     * @param products
     *            the products to set
     */
    public void setProducts(final String products) {
        this.products = products;
    }


    /**
     * @return the clearance
     */
    public boolean isClearance() {
        return clearance;
    }


    /**
     * @param clearance
     *            the clearance to set
     */
    public void setClearance(final boolean clearance) {
        this.clearance = clearance;
    }


    /**
     * @return the bulky
     */
    public boolean isBulky() {
        return bulky;
    }


    /**
     * @param bulky
     *            the bulky to set
     */
    public void setBulky(final boolean bulky) {
        this.bulky = bulky;
    }


    /**
     * @return the parcelCount
     */
    public Integer getParcelCount() {
        return parcelCount;
    }


    /**
     * @param parcelCount
     *            the parcelCount to set
     */
    public void setParcelCount(final Integer parcelCount) {
        this.parcelCount = parcelCount;
    }


    /**
     * @return the carrier
     */
    public String getCarrier() {
        return carrier;
    }


    /**
     * @param carrier
     *            the carrier to set
     */
    public void setCarrier(final String carrier) {
        this.carrier = carrier;
    }


    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }


    /**
     * @param email
     *            the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }


    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }


    /**
     * @param phone
     *            the phone to set
     */
    public void setPhone(final String phone) {
        this.phone = phone;
    }


    /**
     * @return the sendNotifications
     */
    public String getSendNotifications() {
        return sendNotifications;
    }


    /**
     * @param sendNotifications
     *            the sendNotifications to set
     */
    public void setSendNotifications(final String sendNotifications) {
        this.sendNotifications = sendNotifications;
    }

}
