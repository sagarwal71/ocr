/**
 * 
 */
package au.com.target.tgttest.automation.util;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgttest.automation.ServiceLookup;


/**
 * @author sbryan6
 *
 */
public final class StoreNumberResolver {

    private StoreNumberResolver() {
        // util
    }

    /**
     * Resolve the input name or store number to Integer store number
     * 
     * @param store
     * @return store number
     */
    public static Integer getStoreNumber(final String store) {

        return getStore(store).getStoreNumber();
    }

    /**
     * Get store given string which is store number or name
     * 
     * @param storeNo
     * @return TargetPointOfServiceModel indicating the storeNumber
     */
    public static TargetPointOfServiceModel getStore(final String storeNo) {

        try {

            final TargetPointOfServiceService targetPointOfServiceService = ServiceLookup
                    .getTargetPointOfServiceService();
            TargetPointOfServiceModel pointOfService = null;
            if (org.apache.commons.lang.math.NumberUtils.isNumber(storeNo)) {
                pointOfService = targetPointOfServiceService.getPOSByStoreNumber(Integer.valueOf(storeNo));
            }
            else {
                pointOfService = (TargetPointOfServiceModel)targetPointOfServiceService
                        .getPointOfServiceForName(storeNo);
            }
            return pointOfService;
        }
        catch (final Exception e) {
            throw new IllegalArgumentException("Exception getting store for: " + storeNo, e);
        }

    }

}
