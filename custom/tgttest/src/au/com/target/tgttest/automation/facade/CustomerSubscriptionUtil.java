/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import org.apache.log4j.Logger;

import au.com.target.tgtmail.enums.TargetCustomerSubscriptionResponseType;
import au.com.target.tgttest.automation.ServiceLookup;


/**
 * @author bhuang3
 * 
 */
public final class CustomerSubscriptionUtil {


    private static final Logger LOG = Logger.getLogger(CustomerSubscriptionUtil.class);

    private CustomerSubscriptionUtil() {
        //
    }

    /**
     * set mock client response
     * 
     * @param responseType
     * @param message
     * @param code
     */
    public static void setResponse(final TargetCustomerSubscriptionResponseType responseType, final String message,
            final int code) {
        ServiceLookup.getTargetCustomerSubscriptionClientMock().setActive(true);
        ServiceLookup.getTargetCustomerSubscriptionClientMock().setResponseType(responseType);
        ServiceLookup.getTargetCustomerSubscriptionClientMock().setResponseMessage(message);
        ServiceLookup.getTargetCustomerSubscriptionClientMock().setResponseCode(Integer.valueOf(code));
    }

    /**
     * set webMethodes offline for seconds
     * 
     * @param seconds
     */
    public static void setWebmethodsOffline(final int seconds) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                setResponse(TargetCustomerSubscriptionResponseType.UNAVAILABLE, "unavailable", -2);
                try {
                    Thread.sleep(seconds * 1000L);
                }
                catch (final InterruptedException e) {
                    LOG.error(e.getMessage());
                    Thread.currentThread().interrupt();
                }
                setResponse(TargetCustomerSubscriptionResponseType.SUCCESS, "123", 0);
            }
        }).start();
    }
}
