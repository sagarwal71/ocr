/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author rsamuel3
 *
 */
public class ProductImportData {
    private String productName;
    private String productCode;
    private String variantCode;
    private String primaryCategory;
    private String secondaryCategory;
    private String brand;
    private String department;
    private String productDescription;
    private String approvalStatus;
    private String showWhenOutOfStock;
    private String bulky;
    private String productType;
    private String availableExpressDelivery;
    private String availableForLayby;
    private String availableLongTermLayby;
    private String availableHomeDelivery;
    private String availableCnc;
    private boolean sizeOnly;
    private String sizeType;
    private String size;
    private String onlineDate;
    private String offlineDate;
    private String primaryImage;
    private String[] secondaryImages;
    private String materials;
    private String careInstructions;
    private String webExtraInfo;
    private String webFeatures;
    private String youTube;
    private String license;
    private String[] genders;
    private String ageFrom;
    private String originalCategory;
    private String warehouse;
    private String giftcardBrandId;
    private Double denominaton;
    private String excludeForAfterpay;
    private String availableEbayExpressDelivery;
    private String availableOnEbay;
    private String ean;
    private String fluentId;
    private String merchProductStatus;
    private String preOrderStartDate;
    private String preOrderEndDate;
    private Integer preOrderOnlineQuantity;
    private String preOrderEmbargoReleaseDate;
    private String excludePaymentMethods;

    /**
     * @return the originalCategory
     */
    public String getOriginalCategory() {
        return originalCategory;
    }

    /**
     * @param originalCategory
     *            the originalCategory to set
     */
    public void setOriginalCategory(final String originalCategory) {
        this.originalCategory = originalCategory;
    }

    /**
     * @return the availableHomeDelivery
     */
    public String getAvailableHomeDelivery() {
        return availableHomeDelivery;
    }

    /**
     * @param availableHomeDelivery
     *            the availableHomeDelivery to set
     */
    public void setAvailableHomeDelivery(final String availableHomeDelivery) {
        this.availableHomeDelivery = availableHomeDelivery;
    }

    /**
     * @return the primaryCategory
     */
    public String getPrimaryCategory() {
        return primaryCategory;
    }

    /**
     * @param primaryCategory
     *            the primaryCategory to set
     */
    public void setPrimaryCategory(final String primaryCategory) {
        this.primaryCategory = primaryCategory;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName
     *            the productName to set
     */
    public void setProductName(final String productName) {
        this.productName = productName;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }


    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand
     *            the brand to set
     */
    public void setBrand(final String brand) {
        this.brand = brand;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department
     *            the department to set
     */
    public void setDepartment(final String department) {
        this.department = department;
    }

    /**
     * @return the productDescription
     */
    public String getProductDescription() {
        return productDescription;
    }

    /**
     * @param productDescription
     *            the productDescription to set
     */
    public void setProductDescription(final String productDescription) {
        this.productDescription = productDescription;
    }

    /**
     * @return the approvalStatus
     */
    public String getApprovalStatus() {
        return approvalStatus;
    }

    /**
     * @param approvalStatus
     *            the approvalStatus to set
     */
    public void setApprovalStatus(final String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    /**
     * @return the showWhenOutOfStock
     */
    public String getShowWhenOutOfStock() {
        return showWhenOutOfStock;
    }

    /**
     * @param showWhenOutOfStock
     *            the showWhenOutOfStock to set
     */
    public void setShowWhenOutOfStock(final String showWhenOutOfStock) {
        this.showWhenOutOfStock = showWhenOutOfStock;
    }

    /**
     * @return the productType
     */
    public String getProductType() {
        return productType;
    }

    /**
     * @param productType
     *            the productType to set
     */
    public void setProductType(final String productType) {
        this.productType = productType;
    }

    /**
     * @return the bulky
     */
    public String getBulky() {
        return bulky;
    }

    /**
     * @param bulky
     *            the bulky to set
     */
    public void setBulky(final String bulky) {
        this.bulky = bulky;
    }

    /**
     * @return the availableExpressDelivery
     */
    public String getAvailableExpressDelivery() {
        return availableExpressDelivery;
    }

    /**
     * @param availableExpressDelivery
     *            the availableExpressDelivery to set
     */
    public void setAvailableExpressDelivery(final String availableExpressDelivery) {
        this.availableExpressDelivery = availableExpressDelivery;
    }

    /**
     * @return the availableForLayby
     */
    public String getAvailableForLayby() {
        return availableForLayby;
    }

    /**
     * @param availableForLayby
     *            the availableForLayby to set
     */
    public void setAvailableForLayby(final String availableForLayby) {
        this.availableForLayby = availableForLayby;
    }

    /**
     * @return the availableLongTermLayby
     */
    public String getAvailableLongTermLayby() {
        return availableLongTermLayby;
    }

    /**
     * @param availableLongTermLayby
     *            the availableLongTermLayby to set
     */
    public void setAvailableLongTermLayby(final String availableLongTermLayby) {
        this.availableLongTermLayby = availableLongTermLayby;
    }

    /**
     * @return the sizeOnly
     */
    public boolean isSizeOnly() {
        return sizeOnly;
    }

    /**
     * @param sizeOnly
     *            the sizeOnly to set
     */
    public void setSizeOnly(final boolean sizeOnly) {
        this.sizeOnly = sizeOnly;
    }

    /**
     * @return the sizeType
     */
    public String getSizeType() {
        return sizeType;
    }

    /**
     * @param sizeType
     *            the sizeType to set
     */
    public void setSizeType(final String sizeType) {
        this.sizeType = sizeType;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size
     *            the size to set
     */
    public void setSize(final String size) {
        this.size = size;
    }

    /**
     * @return the onlineDate
     */
    public String getOnlineDate() {
        return onlineDate;
    }

    /**
     * @param onlineDate
     *            the onlineDate to set
     */
    public void setOnlineDate(final String onlineDate) {
        this.onlineDate = onlineDate;
    }

    /**
     * @return the offlineDate
     */
    public String getOfflineDate() {
        return offlineDate;
    }

    /**
     * @param offlineDate
     *            the offlineDate to set
     */
    public void setOfflineDate(final String offlineDate) {
        this.offlineDate = offlineDate;
    }


    /**
     * @return the secondaryImages
     */
    public String[] getSecondaryImages() {
        return secondaryImages;
    }

    /**
     * @param secondaryImages
     *            the secondaryImages to set
     */
    public void setSecondaryImages(final String[] secondaryImages) {
        this.secondaryImages = secondaryImages;
    }

    /**
     * @return the primaryImage
     */
    public String getPrimaryImage() {
        return primaryImage;
    }

    /**
     * @param primaryImage
     *            the primaryImage to set
     */
    public void setPrimaryImage(final String primaryImage) {
        this.primaryImage = primaryImage;
    }

    /**
     * @return the variantCode
     */
    public String getVariantCode() {
        return variantCode;
    }

    /**
     * @param variantCode
     *            the variantCode to set
     */
    public void setVariantCode(final String variantCode) {
        this.variantCode = variantCode;
    }

    /**
     * @return the secondaryCategory
     */
    public String getSecondaryCategory() {
        return secondaryCategory;
    }

    /**
     * @param secondaryCategory
     *            the secondaryCategory to set
     */
    public void setSecondaryCategory(final String secondaryCategory) {
        this.secondaryCategory = secondaryCategory;
    }

    /**
     * @return the materials
     */
    public String getMaterials() {
        return materials;
    }

    /**
     * @param materials
     *            the materials to set
     */
    public void setMaterials(final String materials) {
        this.materials = materials;
    }

    /**
     * @return the careInstructions
     */
    public String getCareInstructions() {
        return careInstructions;
    }

    /**
     * @param careInstructions
     *            the careInstructions to set
     */
    public void setCareInstructions(final String careInstructions) {
        this.careInstructions = careInstructions;
    }

    /**
     * @return the webExtraInfo
     */
    public String getWebExtraInfo() {
        return webExtraInfo;
    }

    /**
     * @param webExtraInfo
     *            the webExtraInfo to set
     */
    public void setWebExtraInfo(final String webExtraInfo) {
        this.webExtraInfo = webExtraInfo;
    }

    /**
     * @return the webFeatures
     */
    public String getWebFeatures() {
        return webFeatures;
    }

    /**
     * @param webFeatures
     *            the webFeatures to set
     */
    public void setWebFeatures(final String webFeatures) {
        this.webFeatures = webFeatures;
    }

    /**
     * @return the youTube
     */
    public String getYouTube() {
        return youTube;
    }

    /**
     * @param youTube
     *            the youTube to set
     */
    public void setYouTube(final String youTube) {
        this.youTube = youTube;
    }

    /**
     * @return the license
     */
    public String getLicense() {
        return license;
    }

    /**
     * @param license
     *            the license to set
     */
    public void setLicense(final String license) {
        this.license = license;
    }

    /**
     * @return the genders
     */
    public String[] getGenders() {
        return genders;
    }

    /**
     * @param genders
     *            the genders to set
     */
    public void setGenders(final String[] genders) {
        this.genders = genders;
    }

    /**
     * @return the ageFrom
     */
    public String getAgeFrom() {
        return ageFrom;
    }

    /**
     * @param ageFrom
     *            the ageFrom to set
     */
    public void setAgeFrom(final String ageFrom) {
        this.ageFrom = ageFrom;
    }

    /**
     * @return the availableCnc
     */
    public String getAvailableCnc() {
        return availableCnc;
    }

    /**
     * @param availableCnc
     *            the availableCnc to set
     */
    public void setAvailableCnc(final String availableCnc) {
        this.availableCnc = availableCnc;
    }

    /**
     * @return the warehouse
     */
    public String getWarehouse() {
        return warehouse;
    }

    /**
     * @param warehouse
     *            the warehouse to set
     */
    public void setWarehouse(final String warehouse) {
        this.warehouse = warehouse;
    }

    /**
     * @return the giftcardBrandId
     */
    public String getGiftcardBrandId() {
        return giftcardBrandId;
    }

    /**
     * @param giftcardBrandId
     *            the giftcardBrandId to set
     */
    public void setGiftcardBrandId(final String giftcardBrandId) {
        this.giftcardBrandId = giftcardBrandId;
    }

    /**
     * @return the denominaton
     */
    public Double getDenominaton() {
        return denominaton;
    }

    /**
     * @param denominaton
     *            the denominaton to set
     */
    public void setDenominaton(final Double denominaton) {
        this.denominaton = denominaton;
    }

    /**
     * @return the excludeForAfterpay
     */
    public String getExcludeForAfterpay() {
        return excludeForAfterpay;
    }

    /**
     * @param excludeForAfterpay
     *            the excludeForAfterpay to set
     */
    public void setExcludeForAfterpay(final String excludeForAfterpay) {
        this.excludeForAfterpay = excludeForAfterpay;
    }

    /**
     * @return the availableEbayExpressDelivery
     */
    public String getAvailableEbayExpressDelivery() {
        return availableEbayExpressDelivery;
    }

    /**
     * @param availableEbayExpressDelivery
     *            the availableEbayExpressDelivery to set
     */
    public void setAvailableEbayExpressDelivery(final String availableEbayExpressDelivery) {
        this.availableEbayExpressDelivery = availableEbayExpressDelivery;
    }

    /**
     * @return the availableOnEbay
     */
    public String getAvailableOnEbay() {
        return availableOnEbay;
    }

    /**
     * @param availableOnEbay
     *            the availableOnEbay to set
     */
    public void setAvailableOnEbay(final String availableOnEbay) {
        this.availableOnEbay = availableOnEbay;
    }

    /**
     * @return the ean
     */
    public String getEan() {
        return ean;
    }

    /**
     * @param ean
     *            the ean to set
     */
    public void setEan(final String ean) {
        this.ean = ean;
    }

    /**
     * @return the fluentId
     */
    public String getFluentId() {
        return fluentId;
    }

    /**
     * @param fluentId
     *            the fluentId to set
     */
    public void setFluentId(final String fluentId) {
        this.fluentId = fluentId;
    }

    /**
     * @return the merchProductStatus
     */
    public String getMerchProductStatus() {
        return merchProductStatus;
    }

    /**
     * @param merchProductStatus
     *            the merchProductStatus to set
     */
    public void setMerchProductStatus(final String merchProductStatus) {
        this.merchProductStatus = merchProductStatus;
    }

    /**
     * @return the preOrderStartDate
     */
    public String getPreOrderStartDate() {
        return preOrderStartDate;
    }

    /**
     * @param preOrderStartDate
     *            the preOrderStartDate to set
     */
    public void setPreOrderStartDate(final String preOrderStartDate) {
        this.preOrderStartDate = preOrderStartDate;
    }

    /**
     * @return the preOrderEndDate
     */
    public String getPreOrderEndDate() {
        return preOrderEndDate;
    }

    /**
     * @param preOrderEndDate
     *            the preOrderEndDate to set
     */
    public void setPreOrderEndDate(final String preOrderEndDate) {
        this.preOrderEndDate = preOrderEndDate;
    }

    /**
     * @return the preOrderOnlineQuantity
     */
    public Integer getPreOrderOnlineQuantity() {
        return preOrderOnlineQuantity;
    }

    /**
     * @param preOrderOnlineQuantity
     *            the preOrderOnlineQuantity to set
     */
    public void setPreOrderOnlineQuantity(final Integer preOrderOnlineQuantity) {
        this.preOrderOnlineQuantity = preOrderOnlineQuantity;
    }

    /**
     * @return the preOrderEmbargoReleaseDate
     */
    public String getPreOrderEmbargoReleaseDate() {
        return preOrderEmbargoReleaseDate;
    }

    /**
     * @param preOrderEmbargoReleaseDate
     *            the preOrderEmbargoReleaseDate to set
     */
    public void setPreOrderEmbargoReleaseDate(final String preOrderEmbargoReleaseDate) {
        this.preOrderEmbargoReleaseDate = preOrderEmbargoReleaseDate;
    }

    /**
     * @return the excludePaymentMethods
     */
    public String getExcludePaymentMethods() {
        return excludePaymentMethods;
    }

    /**
     * @param excludePaymentMethods
     *            the excludePaymentMethods to set
     */
    public void setExcludePaymentMethods(final String excludePaymentMethods) {
        this.excludePaymentMethods = excludePaymentMethods;
    }

}
