/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author sbryan6
 *
 */
public class ConsignmentToWebmethodsInfo {

    private static final String NEW_CONSIGNMENT_ID = "NEW_CONSIGNMENT_ID";
    private static final String NEW_ORDER_ID = "NEW_ORDER_ID";

    private String consignmentId;
    private String warehouseId;
    private String shippingMethod;
    private String orderId;
    private String customerFirstName;
    private String customerLastName;

    public boolean checkCreatedConsignmentId() {

        return NEW_CONSIGNMENT_ID.equalsIgnoreCase(consignmentId);
    }

    public boolean checkCreatedOrderId() {

        return NEW_ORDER_ID.equalsIgnoreCase(orderId);
    }


    /**
     * @return the consignmentId
     */
    public String getConsignmentId() {
        return consignmentId;
    }

    /**
     * @param consignmentId
     *            the consignmentId to set
     */
    public void setConsignmentId(final String consignmentId) {
        this.consignmentId = consignmentId;
    }

    /**
     * @return the warehouseId
     */
    public String getWarehouseId() {
        return warehouseId;
    }

    /**
     * @param warehouseId
     *            the warehouseId to set
     */
    public void setWarehouseId(final String warehouseId) {
        this.warehouseId = warehouseId;
    }

    /**
     * @return the orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * @param orderId
     *            the orderId to set
     */
    public void setOrderId(final String orderId) {
        this.orderId = orderId;
    }

    /**
     * @return the customerFirstName
     */
    public String getCustomerFirstName() {
        return customerFirstName;
    }

    /**
     * @param customerFirstName
     *            the customerFirstName to set
     */
    public void setCustomerFirstName(final String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    /**
     * @return the customerLastName
     */
    public String getCustomerLastName() {
        return customerLastName;
    }

    /**
     * @param customerLastName
     *            the customerLastName to set
     */
    public void setCustomerLastName(final String customerLastName) {
        this.customerLastName = customerLastName;
    }

    /**
     * @return the newConsignmentId
     */
    public static String getNewConsignmentId() {
        return NEW_CONSIGNMENT_ID;
    }

    /**
     * @return the newOrderId
     */
    public static String getNewOrderId() {
        return NEW_ORDER_ID;
    }

    /**
     * @return the shippingMethod
     */
    public String getShippingMethod() {
        return shippingMethod;
    }

    /**
     * @param shippingMethod
     *            the shippingMethod to set
     */
    public void setShippingMethod(final String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }


}
