/**
 *
 */
package au.com.target.tgttest.automation.facade;

import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeValueModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;

import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.TargetMaxDimensionZDMVRestrictionModel;
import au.com.target.tgtcore.model.TargetMaxTotalDeadWeightZDMVRestrictionModel;
import au.com.target.tgtcore.model.TargetMaxVolumeZDMVRestrictionModel;
import au.com.target.tgttest.automation.ImpexImporter;
import au.com.target.tgttest.automation.ServiceLookup;


/**
 * Utility class for delivery specific operations
 *
 * @author jjayawa1
 *
 */
public final class DeliveryUtil {

    private static final int DELIVERY_FEE_PRIORITY = 9999999;

    private DeliveryUtil() {

    }

    public static void initialise() {
        //delete existing delivery mode value records
        final FlexibleSearchQuery paramFlexibleSearchQuery = new FlexibleSearchQuery("SELECT {"
                + AbstractTargetZoneDeliveryModeValueModel.PK + "} FROM {"
                + AbstractTargetZoneDeliveryModeValueModel._TYPECODE
                + "}");
        final SearchResult searchResult = ServiceLookup.getFlexibleSearchService().search(paramFlexibleSearchQuery);
        ServiceLookup.getModelService().removeAll(searchResult.getResult());


        // Initialise delivery modes
        ImpexImporter.importCsv("/tgttest/automation-impex/product/delivery-modes.impex");
    }

    /**
     * Check whether post code and delivery mode combination is valid
     *
     * @param deliveryModeCode
     * @param postCode
     * @return Boolean
     */
    public static Boolean isDeliveryModePostCodeCombinationValid(final String deliveryModeCode, final String postCode) {

        return Boolean.valueOf(ServiceLookup.getTargetDeliveryFacade().isDeliveryModePostCodeCombinationValid(
                deliveryModeCode, postCode));
    }

    public static void enableDeliveryModes() {
        final String[] modes = { "home-delivery", "click-and-collect", "express-delivery" };
        for (final String deliveryModeCode : modes) {
            final DeliveryModeModel deliveryMode = ServiceLookup.getTargetDeliveryService()
                    .getDeliveryModeForCode(deliveryModeCode);
            if (BooleanUtils.isNotTrue(deliveryMode.getActive())) {
                deliveryMode.setActive(Boolean.TRUE);
                ServiceLookup.getModelService().save(deliveryMode);
            }
        }
    }

    /**
     *
     */
    public static void tearDown() {
        // reset delivery modes
        ImpexImporter.importCsv("/tgttest/automation-impex/product/delivery-modes-reset.impex");
    }

    /**
     * @param applicableDeliveryModeValues
     */
    public static RestrictableTargetZoneDeliveryModeValueModel getHighestPriorityRestriction(
            final List<AbstractTargetZoneDeliveryModeValueModel> applicableDeliveryModeValues) {
        int priority = 0;
        final Map<Integer, AbstractTargetZoneDeliveryModeValueModel> tempMap = new HashMap<Integer, AbstractTargetZoneDeliveryModeValueModel>();
        for (final AbstractTargetZoneDeliveryModeValueModel restrictionModel : applicableDeliveryModeValues) {
            if (restrictionModel instanceof RestrictableTargetZoneDeliveryModeValueModel
                    && restrictionModel.getPriority().intValue() > priority) {
                priority = restrictionModel.getPriority().intValue();
                tempMap.put(restrictionModel.getPriority(), restrictionModel);
            }
        }
        return (RestrictableTargetZoneDeliveryModeValueModel)tempMap.get(Integer.valueOf(priority));
    }

    public static void initializePostCodeData() {
        ImpexImporter.importCsv("/tgttest/automation-impex/product/postcode-groups.impex");
    }

    /**
     * InitializeShipsterConfig
     * 
     */

    public static void initializeShipsterConfig() {
        ImpexImporter.importCsv("/tgttest/automation-impex/product/shipster-config.impex");
    }

    /**
     * initialise preOrder delivery fee
     */
    public static void initializePreOrderDeliveryFeeConfig() {
        ImpexImporter.importCsv("/tgttest/automation-impex/product/preOrder-delivery-fee.impex");
    }


    /**
     * Method to set TargetMaxTotalDeadWeightRestriction.
     * 
     * @param maxWeight
     */
    public static void setMaxWeightRestriction(final int maxWeight) {
        final RestrictableTargetZoneDeliveryModeValueModel restrictableTargetZoneDeliveryModeValueModel = setUpModeValue();
        createAndSetMaxWeightRestriction(maxWeight, restrictableTargetZoneDeliveryModeValueModel);
    }

    /**
     * Method to set TargetMaxVolumeZDMVRestriction
     * 
     * @param maxVolume
     */
    public static void setMaxVolumeRestriction(final int maxVolume) {
        final RestrictableTargetZoneDeliveryModeValueModel restrictableTargetZoneDeliveryModeValueModel = setUpModeValue();
        createAndSetMaxVolumeRestriction(maxVolume, restrictableTargetZoneDeliveryModeValueModel);
    }

    /**
     * Method to set TargetMaxDimensionZDMVRestriction
     * 
     * @param maxDimension
     */
    public static void setMaxDimensionRestriction(final int maxDimension) {
        final RestrictableTargetZoneDeliveryModeValueModel restrictableTargetZoneDeliveryModeValueModel = setUpModeValue();
        createAndSetMaxDimensionRestriction(maxDimension, restrictableTargetZoneDeliveryModeValueModel);
    }

    /**
     * Method to create and set max weight, dimension and volume restriction in restrictable delivery value
     * 
     * @param maxWeight
     * @param maxDimension
     * @param maxVolume
     */
    public static void setMaxWeightDimensionAndVolumeRestriction(final int maxWeight, final int maxDimension,
            final int maxVolume) {
        final RestrictableTargetZoneDeliveryModeValueModel restrictableTargetZoneDeliveryModeValueModel = setUpModeValue();
        createAndSetMaxWeightRestriction(maxWeight, restrictableTargetZoneDeliveryModeValueModel);
        createAndSetMaxDimensionRestriction(maxDimension, restrictableTargetZoneDeliveryModeValueModel);
        createAndSetMaxVolumeRestriction(maxVolume, restrictableTargetZoneDeliveryModeValueModel);
    }

    /**
     * Method to delete RestrictableTargetZoneDeliveryModeValue
     */
    public static void deleteRestrictionCreated() {
        final TargetZoneDeliveryModeModel zoneModel = (TargetZoneDeliveryModeModel)ServiceLookup
                .getTargetDeliveryService().getDeliveryModeForCode(CheckoutUtil.EXPRESS_DELIVERY_CODE);
        final Set<ZoneDeliveryModeValueModel> deliveryValues = zoneModel.getValues();
        if (CollectionUtils.isNotEmpty(deliveryValues)) {
            for (final ZoneDeliveryModeValueModel deliveryValue : deliveryValues) {
                if (deliveryValue instanceof RestrictableTargetZoneDeliveryModeValueModel
                        && ((RestrictableTargetZoneDeliveryModeValueModel)deliveryValue).getPriority()
                                .intValue() == DELIVERY_FEE_PRIORITY) {
                    ServiceLookup.getModelService().remove(deliveryValue);
                    break;
                }
            }
        }
    }

    /**
     * Method to set RestrictableTargetZoneDeliveryModeValue
     * 
     * @return RestrictableTargetZoneDeliveryModeValue
     */
    private static RestrictableTargetZoneDeliveryModeValueModel setUpModeValue() {
        final RestrictableTargetZoneDeliveryModeValueModel restrictableTargetZoneDeliveryModeValueModel = ServiceLookup
                .getModelService().create(RestrictableTargetZoneDeliveryModeValueModel.class);
        final ZoneDeliveryModeModel deliverMode = (ZoneDeliveryModeModel)ServiceLookup
                .getTargetDeliveryService().getDeliveryModeForCode(CheckoutUtil.EXPRESS_DELIVERY_CODE);
        deliverMode.setValues(null);
        ServiceLookup.getModelService().save(deliverMode);
        ServiceLookup.getModelService().refresh(deliverMode);
        restrictableTargetZoneDeliveryModeValueModel.setDeliveryMode((ZoneDeliveryModeModel)ServiceLookup
                .getTargetDeliveryService().getDeliveryModeForCode(CheckoutUtil.EXPRESS_DELIVERY_CODE));

        restrictableTargetZoneDeliveryModeValueModel.setPriority(Integer.valueOf(DELIVERY_FEE_PRIORITY));
        restrictableTargetZoneDeliveryModeValueModel.setZone(ServiceLookup.getZoneDeliveryModeService().getZoneForCode(
                "express"));
        restrictableTargetZoneDeliveryModeValueModel.setCurrency(ServiceLookup.getCommonI18nService()
                .getCurrency("AUD"));
        restrictableTargetZoneDeliveryModeValueModel.setMinimum(Double.valueOf(1));
        restrictableTargetZoneDeliveryModeValueModel.setValue(Double.valueOf(15));
        ServiceLookup.getModelService().save(restrictableTargetZoneDeliveryModeValueModel);
        ServiceLookup.getModelService().refresh(restrictableTargetZoneDeliveryModeValueModel);
        return restrictableTargetZoneDeliveryModeValueModel;
    }

    /**
     * Method to create max dimension restriction and set it in the restrictable delivery value
     * 
     * @param maxDimension
     * @param restrictableTargetZoneDeliveryModeValueModel
     */
    private static void createAndSetMaxDimensionRestriction(final int maxDimension,
            final RestrictableTargetZoneDeliveryModeValueModel restrictableTargetZoneDeliveryModeValueModel) {
        final TargetMaxDimensionZDMVRestrictionModel targetMaxDimensionZDMVRestrictionModel = ServiceLookup
                .getModelService().create(TargetMaxDimensionZDMVRestrictionModel.class);
        targetMaxDimensionZDMVRestrictionModel.setMaxDimension(Double.valueOf(maxDimension));
        targetMaxDimensionZDMVRestrictionModel
                .setRestrictableDeliveryModeValue(restrictableTargetZoneDeliveryModeValueModel);
        ServiceLookup.getModelService().save(targetMaxDimensionZDMVRestrictionModel);
    }

    /**
     * Method to create max volume restriction and set it in restrictable delivery value
     * 
     * @param maxVolume
     * @param restrictableTargetZoneDeliveryModeValueModel
     */
    private static void createAndSetMaxVolumeRestriction(final int maxVolume,
            final RestrictableTargetZoneDeliveryModeValueModel restrictableTargetZoneDeliveryModeValueModel) {
        final TargetMaxVolumeZDMVRestrictionModel targetMaxVolumeZDMVRestrictionModel = ServiceLookup.getModelService()
                .create(TargetMaxVolumeZDMVRestrictionModel.class);
        targetMaxVolumeZDMVRestrictionModel.setMaxVolume(Double.valueOf(maxVolume));
        targetMaxVolumeZDMVRestrictionModel
                .setRestrictableDeliveryModeValue(restrictableTargetZoneDeliveryModeValueModel);
        ServiceLookup.getModelService().save(targetMaxVolumeZDMVRestrictionModel);
    }

    /**
     * Method to create max weight restriction and set it in restrictable delivery value
     * 
     * @param maxWeight
     * @param restrictableTargetZoneDeliveryModeValueModel
     */
    private static void createAndSetMaxWeightRestriction(final int maxWeight,
            final RestrictableTargetZoneDeliveryModeValueModel restrictableTargetZoneDeliveryModeValueModel) {
        final TargetMaxTotalDeadWeightZDMVRestrictionModel targetMaxTotalDeadWeightZDMVRestriction = ServiceLookup
                .getModelService().create(TargetMaxTotalDeadWeightZDMVRestrictionModel.class);
        targetMaxTotalDeadWeightZDMVRestriction.setMaxTotalDeadWeight(Double.valueOf(maxWeight));
        targetMaxTotalDeadWeightZDMVRestriction
                .setRestrictableDeliveryModeValue(restrictableTargetZoneDeliveryModeValueModel);
        ServiceLookup.getModelService().save(targetMaxTotalDeadWeightZDMVRestriction);
    }

}
