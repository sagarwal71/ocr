@storeStockData @adminUser
Feature: Stock levels at stores should be kept up to date

  Scenario: Getting stock levels of products from a store
    Given store stock levels are:
      | store | product | soh | message | success |
      | 7098  | CP7002  | 2   |         | true    |
      | 7098  | CP7003  | 10  |         | true    |
      | 7098  | CP7004  | 1   |         | true    |
      | 7098  | CP7005  | 21  |         | true    |
      | 7098  | CP7006  | 1   |         | true    |
      | 7086  | CP7002  | 0   |         | true    |
      | 7086  | CP7007  | 4   |         | true    |
      | 7086  | CP7010  | 1   |         | true    |
    When update hybris stocklevels
    Then hybris stock levels are:
      | store | product | soh |
      | 7098  | CP7002  | 2   |
      | 7098  | CP7003  | 10  |
      | 7098  | CP7004  | 1   |
      | 7098  | CP7005  | 21  |
      | 7098  | CP7006  | 1   |
      | 7086  | CP7002  | 0   |
      | 7086  | CP7007  | 4   |
      | 7086  | CP7010  | 1   |
