@deliveryModeData
Feature: Different delivery modes are available on orders
  Depending on the products in their cart, and their postcode, 
  Target can offer different delivery options to customers.

  # Use standard test user test.user1@target.com.au
  # Test products are defined in test-delivery-modes-colour-variant-products.impex

  Scenario: Delivery modes available with product having all modes
    Given cart entries:
      | product  | qty | price |
      | 10001001 | 1   | 10    |
    When get cart data
    Then cart delivery modes are:
      | name              | available |
      | home-delivery     | true      |
      | click-and-collect | true      |
      | express-delivery  | true      |

Scenario: Delivery modes available with product having home delivery and cnc
    Given cart entries:
      | product  | qty | price |
      | 10001002 | 1   | 10    |
    When get cart data
    Then cart delivery modes are:
      | name              | available |
      | home-delivery     | true      |
      | click-and-collect | true      |
      | express-delivery  | false     |

Scenario: Delivery modes available with product having home delivery and express
    Given cart entries:
      | product  | qty | price |
      | 10001003 | 1   | 10    |
    When get cart data
    Then cart delivery modes are:
      | name              | available |
      | home-delivery     | true      |
      | click-and-collect | false     |
      | express-delivery  | true      |

Scenario: Delivery modes available with product having home delivery only
    Given cart entries:
      | product  | qty | price |
      | 10001004 | 1   | 10    |
    When get cart data
    Then cart delivery modes are:
      | name              | available |
      | home-delivery     | true      |
      | click-and-collect | false     |
      | express-delivery  | false     |
      
Scenario: Delivery modes available with product having cnc only
    Given cart entries:
      | product  | qty | price |
      | 10001005 | 1   | 10    |
    When get cart data
    Then cart delivery modes are:
      | name              | available |
      | home-delivery     | false     |
      | click-and-collect | true      |
      | express-delivery  | false     |

Scenario: Delivery modes available with product having express only
    Given cart entries:
      | product  | qty | price |
      | 10001006 | 1   | 10    |
    When get cart data
    Then cart delivery modes are:
      | name              | available |
      | home-delivery     | false     |
      | click-and-collect | false     |
      | express-delivery  | true      |

Scenario: Delivery modes available with multiple products having mix of home delivery only and all
    Given cart entries:
      | product  | qty | price |
      | 10001004 | 1   | 10    |
      | 10001001 | 1   | 10    |
    When get cart data
    Then cart delivery modes are:
      | name              | available |
      | home-delivery     | true      |
      | click-and-collect | false     |
      | express-delivery  | false     |

Scenario: Delivery modes available with multiple products having mix such there are no delivery modes
    Given cart entries:
      | product  | qty | price |
      | 10001004 | 1   | 10    |
      | 10001005 | 1   | 10    |
      | 10001006 | 1   | 10    |
    When get cart data
    Then cart delivery modes are:
      | name              | available |
      | home-delivery     | false     |
      | click-and-collect | false     |
      | express-delivery  | false     |
