@wip
Feature: 
  As an Online store we want to display the promotional messages for the customers

  Background: 
    Given Target online has the following deals
      | name              | rewardType       | rewardValue | rewardMaxQty | Qualifier         | Reward   |
      | buygetsamelist101 | PercentOffEach   | 50          | 1            | 10000011,10000012 | N/A      |
      | buyget102         | PercentOffEach   | 100         | 1            | 10000021          | 10000322 |
      | buyget103         | dollaroff        | 5           | 2            | 10000221          | 10000212 |
      | spendandsave104   | dollaroffeach    | 15          | 1            | 10000022          | N/A      |
      | spendandget105    | PercentOffEach   | 100         | 2            | 10000122          | 10000321 |
      | valuebundle106    | fixeddollartotal | 50          | 1            | 10000311,10000222 | N/A      |
    And the deal messages are
      | name              | dealDescription                            | couldhavefiredmessage                 | couldhavemorerewardsmessage         | firedmessage                              |
      | buygetsamelist101 | Buy a Jean and get Tshirt 50% off          | Add a tshirt to get it for half price | N/A                                 | You have received a tshirt for half price |
      | buyget102         | Buy a Jean and get Tshirt free             | Add a tshirt to get it free           | Add one more tshirt to get for free | You have received a tshirt for free       |
      | buyget103         | Buy a Jean and get two Tshirts $5 off      | Add two tshirt to get them $5 off     | Add one more tshirt to get $5 off   | You have saved $10                        |
      | spendandsave104   | Spend $100 on Jeans and save $15           | Spend $100 to save $15                | N/A                                 | You have saved $15                        |
      | spendandget105    | Spend $50 on Jeans and get two shirts free | Add Tshirts to get them for free      | Add one more tshirt to get it free  | You have received two tshirts for free    |
      | valuebundle106    | Buy Tshirt and a jean for $50              | Add Jean to buy for $50               | N/A                                 | You have saved $10                        |

  Scenario Outline: 
    Given A cart with entries <Products in cart>
    When the customer retrieves the <Product>
    Then the message is displayed as <Message>
      | Products in Cart           | Product  | Message                                    |
      | N/A                        | 10000012 | Buy a Jean and get Tshirt 50% off          |
      | 10000011                   | 10000012 | Add a tshirt to get it for half price      |
      | 10000011,10000012          | 10000011 | Buy a Jean and get Tshirt 50% off          |
      | N/A                        | 10000021 | Buy a Jean and get Tshirt free             |
      | 10000322                   | 10000021 | Add a tshirt to get it free                |
      | 10000021,10000322          | 10000322 | Buy a Jean and get Tshirt free             |
      | N/A                        | 10000221 | Buy a Jean and get two Tshirts $5 off      |
      | 10000212                   | 10000221 | Add two tshirt to get them $5 off          |
      | 10000212,10000221          | 10000212 | Add one more tshirt to get $5 off          |
      | 10000212,10000221          | 10000221 | Buy a Jean and get two Tshirts $5 off      |
      | 10000212,10000221,10000212 | 10000221 | Buy a Jean and get two Tshirts $5 off      |
      | N/A                        | 10000022 | Spend $100 on Jeans and save $15           |
      | 10000022                   | 10000022 | Spend $100 to save $15                     |
      | 10000022,10000022          | 10000022 | spend $100 on Jeans and save $15           |
      | N/A                        | 10000122 | Spend $50 on Jeans and get two shirts free |
      | 10000122                   | 10000122 | Add Tshirts to get them for free           |
      | 10000122,10000122          | 10000122 | Add one more tshirt to get it free         |
      | 10000122,10000122,10000122 | 10000122 | Spend $50 on Jeans and get two shirts free |
      | N/A                        | 10000311 | Buy Tshirt and a jean for    $50           |
      | 10000222                   | 10000311 | Add Jean to buy together for $50           |
      | 10000222,10000311          | 10000311 | Buy Tshirt and a jean for $50              |
