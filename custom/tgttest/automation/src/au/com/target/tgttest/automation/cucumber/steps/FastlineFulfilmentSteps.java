/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static java.lang.Boolean.TRUE;
import static java.lang.Boolean.valueOf;
import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtjms.jms.model.ConsignmentExtract;
import au.com.target.tgttest.automation.facade.BusinessProcessUtil;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.ConsignmentUtil;
import au.com.target.tgttest.automation.facade.FastlineFacade;
import au.com.target.tgttest.automation.facade.FulfilmentFacade;
import au.com.target.tgttest.automation.facade.bean.AddressEntry;
import au.com.target.tgttest.automation.facade.bean.ConsignmentToFastline;
import au.com.target.tgttest.automation.facade.bean.PickEntry;
import au.com.target.tgttest.automation.facade.domain.Order;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author rsamuel3
 * 
 */
public class FastlineFulfilmentSteps {

    @When("^order ack is received from fastline$")
    public void fastlineAcknowledgeOrder() {

        final OrderModel orderModel = Order.getInstance().getOrderModel();

        FulfilmentFacade.processAck(orderModel.getCode());
    }


    @When("^the order is zero picked$")
    public void fastlineZeroPicked() throws Exception {
        final List<PickEntry> pickEntries = new ArrayList<>();
        final PickEntry entry = new PickEntry(CheckoutUtil.ANY_PRD_CODE, 0);
        pickEntries.add(entry);

        fastlinePickEntries(pickEntries);
    }

    @When("^fastline returns pick entry data:$")
    public void fastlinePickEntries(final List<PickEntry> pickEntries) throws Exception {

        final OrderModel orderModel = Order.getInstance().getOrderModel();
        final TargetConsignmentModel consignment = ConsignmentUtil.verifyAndGetSingleFastlineConsignment();
        FulfilmentFacade.processPickFile(orderModel.getCode(), consignment.getCode(), consignment.getTargetCarrier()
                .getWarehouseCode(), "123456", Integer.valueOf(1), pickEntries);
        BusinessProcessUtil.waitForAllOrderProcessesToFinish(orderModel);
    }

    @When("^the consignment is full picked by fastline$")
    public void fastlineFullPicked() throws Exception {

        final TargetConsignmentModel consignment = ConsignmentUtil.verifyAndGetSingleFastlineConsignment();

        final List<PickEntry> pickEntries = createPickEntriesForConsignment(consignment.getConsignmentEntries());

        fastlinePickEntries(pickEntries);
    }

    @When("^the consignment is '(full picked|partially picked)' by fastline$")
    public void fastlinePicked(final String fulfilmentType) throws Exception {

        final TargetConsignmentModel consignment = ConsignmentUtil.verifyAndGetSingleFastlineConsignment();
        List<PickEntry> pickEntries = null;
        if (StringUtils.equalsIgnoreCase(fulfilmentType, "partially picked")) {
            pickEntries = createPartialPickEntriesForConsignment(consignment.getConsignmentEntries());
        }
        else {
            pickEntries = createPickEntriesForConsignment(consignment.getConsignmentEntries());
        }
        fastlinePickEntries(pickEntries);
    }

    @When("^ship conf is received from fastline$")
    public void fastlineShipConf() throws Exception {

        final OrderModel orderModel = Order.getInstance().getOrderModel();

        processFastlineShipConf(orderModel.getCode());
    }

    @Then("(?:fastline pick|auto pick) process completes")
    public void verifyConsignmentPickedProcessCompletes() throws InterruptedException {
        BusinessProcessUtil.waitForExpectedProcessToFinishAndCheckSuccess("consignmentPickedProcess");
    }

    @Then("order complete process completes")
    public void verifyOrderCompleteProcessCompletes() throws InterruptedException {
        BusinessProcessUtil.waitForExpectedProcessToFinishAndCheckSuccess("orderCompletionProcess");
    }

    @Then("order cancel process completes")
    public void verifyOrderCancelProcessCompletes() throws InterruptedException {
        BusinessProcessUtil.waitForExpectedProcessToFinishAndCheckSuccess("orderCancelProcess");
    }

    @Then("order complete process does not run")
    public void verifyOrderCompleteProcessDoesNotRun() throws InterruptedException {
        assertThat(BusinessProcessUtil.getOrderProcess("orderCompletionProcess")).isNull();
    }

    @When("^order ack is received from fastline with consignmentID as the identifier$")
    public void whenOrderAckIsReceivedFromFastlineWithConsgIdAsIdentifier() {
        FulfilmentFacade.processAck(ConsignmentUtil.verifyAndGetSingleFastlineConsignment().getCode());
    }

    @When("^the consignment is full picked by fastline with consignmentID as the identifier$")
    public void whenOrderIsPickedConfirmationIsReceivedFromFastlineWithConsgIdAsIdentifier() throws Exception {

        final TargetConsignmentModel consignment = ConsignmentUtil.verifyAndGetSingleFastlineConsignment();

        final List<PickEntry> pickEntries = createPickEntriesForConsignment(consignment.getConsignmentEntries());

        FulfilmentFacade.processPickFile(consignment.getCode(), consignment.getCode(),
                consignment.getTargetCarrier().getWarehouseCode(), "123456", Integer.valueOf(1), pickEntries);
        verifyConsignmentPickedProcessCompletes();
    }

    @When("^ship conf is received from fastline with consignmentID as the identifier$")
    public void whenOrderShipConfirmationIsReceivedFromFastlineWithConsgIdAsIdentifier() throws Exception {
        processFastlineShipConf(ConsignmentUtil.verifyAndGetSingleFastlineConsignment().getCode());
    }

    @Then("^order extract address details are:$")
    public void verifyOrderExtractAddress(final List<AddressEntry> addressEntries) {
        final AddressEntry addressEntry = addressEntries.get(0);
        for (final ConsignmentExtract consignmentExtract : FastlineFacade.getLastConsignmentExtracts()) {
            Assert.assertEquals(TRUE,
                    valueOf(consignmentExtract.getShipToLocAddr1()
                            .equalsIgnoreCase(addressEntry.getAddressLine1())));
            Assert.assertEquals(TRUE,
                    valueOf(consignmentExtract.getShipToCity().equalsIgnoreCase(addressEntry.getTown())));
            Assert.assertEquals(TRUE,
                    valueOf(consignmentExtract.getShipToPostCode().equalsIgnoreCase(addressEntry.getPostalCode())));
            Assert.assertEquals(TRUE,
                    valueOf(consignmentExtract.getShipToState().equalsIgnoreCase(addressEntry.getRegion())));
            Assert.assertEquals(TRUE,
                    valueOf(consignmentExtract.getShipToCountry().equalsIgnoreCase(addressEntry.getCountry())));
        }
    }

    @Then("^the fastline extract contains the records$")
    public void verifyItemOrderAmounts(final List<ConsignmentToFastline> consignmentEntries) {
        final List<ConsignmentExtract> actualExtracts = FastlineFacade.getLastConsignmentExtracts();
        Assert.assertEquals(actualExtracts.size(), consignmentEntries.size());
        final Map<String, ConsignmentExtract> actualMap = new HashMap();
        for (final ConsignmentExtract actualExtract : actualExtracts) {
            actualMap.put(actualExtract.getItemNumber(), actualExtract);
        }

        for (final ConsignmentToFastline expectedEntry : consignmentEntries) {
            final ConsignmentExtract actualExtract = actualMap.get(expectedEntry.getItemCode());
            Assert.assertNotNull(actualExtract);
            Assert.assertEquals(expectedEntry.getQuantityOrdered(), actualExtract.getQuantityOrdered());
            Assert.assertEquals(expectedEntry.getItemAmount(), actualExtract.getItemAmount());
            Assert.assertEquals(expectedEntry.getOrderGoodsAmount(), actualExtract.getOrderGoodsAmount());
            Assert.assertEquals(expectedEntry.getItemWeight(), actualExtract.getItemWeight());
            if (expectedEntry.getDeliveryFee() != null) {
                Assert.assertEquals(expectedEntry.getDeliveryFee(), actualExtract.getDeliveryFee());
            }
        }
    }

    /**
     * Method to create pick entries for consignment entries
     * 
     * @param consignmentEntries
     * @return pickEntries
     */
    private List<PickEntry> createPickEntriesForConsignment(final Set<ConsignmentEntryModel> consignmentEntries) {
        final List<PickEntry> pickEntries = new ArrayList<>();
        for (final ConsignmentEntryModel entry : consignmentEntries) {
            final String product = entry.getOrderEntry().getProduct().getCode();
            final PickEntry pickEntry = new PickEntry(product, entry.getQuantity().intValue());
            pickEntries.add(pickEntry);
        }
        return pickEntries;
    }

    /**
     * Method to create partial pick entries for consignment entries
     * 
     * @param consignmentEntries
     * @return pickEntries
     */
    private List<PickEntry> createPartialPickEntriesForConsignment(
            final Set<ConsignmentEntryModel> consignmentEntries) {
        final List<PickEntry> pickEntries = new ArrayList<>();
        for (final ConsignmentEntryModel entry : consignmentEntries) {
            final String product = entry.getOrderEntry().getProduct().getCode();
            final PickEntry pickEntry = new PickEntry(product, entry.getQuantity().intValue() - 1);
            pickEntries.add(pickEntry);
        }
        return pickEntries;
    }

    /**
     * Method to process ship conf using order code or consignment code
     * 
     * @param orderOrConsignmentCode
     * @throws FulfilmentException
     * @throws InterruptedException
     */
    private void processFastlineShipConf(final String orderOrConsignmentCode)
            throws FulfilmentException, InterruptedException {
        FulfilmentFacade.processShipConfirm(orderOrConsignmentCode, new Date());
        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("orderCompletionProcess");
    }

}
