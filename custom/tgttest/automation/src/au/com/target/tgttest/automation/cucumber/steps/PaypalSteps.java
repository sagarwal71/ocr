/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import au.com.target.tgttest.automation.ServiceLookup;
import cucumber.api.java.en.Given;


/**
 * @author htan3
 *
 */
public class PaypalSteps {

    @Given("^paypal rejects the payment$")
    public void paymentFails() {
        ServiceLookup.getPaypalServiceMock().setFailedToCapture(true);
    }

    @Given("^paypal is down during payment capture$")
    public void paypalDownWhileCapture() {
        ServiceLookup.getPaypalServiceMock().setDownWhileCapture(true);
    }

    @Given("^paypal is down$")
    public void paypalDown() {
        ServiceLookup.getPaypalServiceMock().setDownWhileCapture(true);
        ServiceLookup.getPaypalServiceMock().setDownWhileRetrieve(true);
    }
}
