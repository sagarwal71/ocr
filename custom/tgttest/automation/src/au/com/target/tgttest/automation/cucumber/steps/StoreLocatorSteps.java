/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.junit.Assert;

import au.com.target.tgtfacades.storelocator.data.TargetOpeningScheduleData;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.StoresData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;


/**
 * @author pthoma20
 *
 */
public class StoreLocatorSteps {


    private Map<String, List<TargetPointOfServiceData>> targetPointOfServicesData;

    @Given("^The following target Stores are present in Hybris:$")
    public void givenTheFollowingTargetStoresExist(final List<String> storesList) {

        for (final String storeName : storesList) {
            final PointOfServiceModel targetPointOfServiceModel = ServiceLookup
                    .getTargetPointOfServiceService().getPointOfServiceForName(storeName);
            assertThat(targetPointOfServiceModel).isNotNull();
        }
    }

    @Given("^all the stores are fetched from Hybris$")
    public void whenTargetStoresAreBeingFetched() {
        targetPointOfServicesData = ServiceLookup
                .getTargetStoreLocatorFacade().getAllStateAndStores();
    }

    @Then("^response should contain the following stores and details:$")
    public void thenFollowingStoresAndDetailsExist(final List<StoresData> storesDataList) {

        assertThat(targetPointOfServicesData).isNotNull();
        final Collection<List<TargetPointOfServiceData>> targetPointOfServiceStores = targetPointOfServicesData
                .values();
        final List<Integer> storesFound = new ArrayList<>();
        for (final List<TargetPointOfServiceData> targetPointOfServiceList : targetPointOfServiceStores) {
            for (final TargetPointOfServiceData targetPointOfServiceData : targetPointOfServiceList) {
                for (final StoresData storesData : storesDataList) {
                    assertThat(targetPointOfServiceData).isNotNull();
                    if (storesData.getStoreNumber().compareTo(targetPointOfServiceData.getStoreNumber()) == 0) {
                        storesFound.add(targetPointOfServiceData.getStoreNumber());
                        assertThat(storesData.getStoreName()).isEqualTo(targetPointOfServiceData.getName());
                        final GeoPoint geoPoint = targetPointOfServiceData.getGeoPoint();
                        assertThat(geoPoint).isNotNull();
                        assertThat(storesData.getLatitude()).isEqualTo(geoPoint.getLatitude());
                        assertThat(storesData.getLongitude()).isEqualTo(geoPoint.getLongitude());
                        assertThat(storesData.getTimeZone()).isEqualTo(targetPointOfServiceData.getTimeZone());
                        assertThat(targetPointOfServiceData.getAddress()).isInstanceOf(TargetAddressData.class);
                        final TargetAddressData addressData = (TargetAddressData)targetPointOfServiceData.getAddress();
                        assertThat(addressData).isNotNull();
                        assertThat(storesData.getState().equals(addressData.getState()));
                        assertThat(storesData.getFormattedAddress().equals(addressData.getFormattedAddress()));
                        assertThat(storesData.getPhone().equals(addressData.getPhone()));
                        assertThat(targetPointOfServiceData.getOpeningHours()).isInstanceOf(
                                TargetOpeningScheduleData.class);
                        final TargetOpeningScheduleData targetOpeningScheduleData = (TargetOpeningScheduleData)targetPointOfServiceData
                                .getOpeningHours();
                        assertThat(targetOpeningScheduleData.getCode()).isNotNull();
                        assertThat(storesData.getTargetOpeningHours()).isEqualTo(targetOpeningScheduleData.getCode());
                        assertThat(targetOpeningScheduleData.getTargetOpeningWeeks()).isNotNull();
                        if ("target-no-hours".equals(storesData.getTargetOpeningHours())) {
                            assertThat(targetOpeningScheduleData.getTargetOpeningWeeks()).isEmpty();
                        }
                    }
                }
            }
        }

        //this is to ensure that all the stores are found.
        for (final StoresData storesData : storesDataList) {
            assertThat(storesFound).contains(storesData.getStoreNumber());
        }

    }

    @Then("^the following closed stores will not be present:$")
    public void thenfollowingStoresWillNotBePresent(final List<String> storesList) {
        final Collection<List<TargetPointOfServiceData>> targetPointOfServiceStores = targetPointOfServicesData
                .values();
        assertThat(targetPointOfServicesData).isNotNull();
        for (final List<TargetPointOfServiceData> targetPointOfServiceList : targetPointOfServiceStores) {
            for (final TargetPointOfServiceData targetPointOfServiceData : targetPointOfServiceList) {
                for (final String storeName : storesList) {
                    assertThat(targetPointOfServiceData).isNotNull();
                    if (storeName.equals(targetPointOfServiceData.getName())) {
                        Assert.fail("Closed Store found in the response" + storeName);
                    }
                }
            }
        }
    }
}
