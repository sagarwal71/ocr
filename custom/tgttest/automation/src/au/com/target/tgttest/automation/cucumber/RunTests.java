package au.com.target.tgttest.automation.cucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty", "html:cuc-report/cucumber-html-report", "json:cuc-report/cucumber.json",
        "junit:cuc-report/cucumber-junit.xml" },
        tags = { "~@wip", "~@toFix", "~@notAutomated" })
public class RunTests {
    // Cucumber test runner class
}
