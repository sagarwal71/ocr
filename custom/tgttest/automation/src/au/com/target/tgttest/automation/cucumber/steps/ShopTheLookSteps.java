/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.fest.assertions.Assertions;

import au.com.target.tgtfacades.looks.data.ImageContainer;
import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtfacades.looks.data.LooksCollectionData;
import au.com.target.tgtfacades.looks.data.ShopTheLookPageData;
import au.com.target.tgtfacades.stl.TargetShopTheLookFacade;
import au.com.target.tgttest.automation.ImpexImporter;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.ShopTheLookCollectionsData;
import au.com.target.tgttest.automation.facade.bean.ShopTheLookData;
import au.com.target.tgttest.automation.facade.bean.ShopTheLookLooksData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author pthoma20
 *
 */
public class ShopTheLookSteps {


    private final TargetShopTheLookFacade targetShopTheLookFacade = ServiceLookup.getTargetShopTheLookFacade();

    private ShopTheLookPageData shopTheLookPageData;

    private LookDetailsData lookDetailsData;

    private List<String> productCodeList;

    @Given("^the shop the look are setup in the system running the mentioned impex$")
    public void setupLooks() {
        ImpexImporter
                .importCsv("/tgttest/automation-impex/shopthelook/test-looks.impex");
    }

    @When("^the valid looks are being retrieved for shop the look having id '(.*)'$")
    public void retrieveShopTheLookData(final String shopTheLookCode) {
        shopTheLookPageData = targetShopTheLookFacade.populateShopTheLookPageData(shopTheLookCode);
    }

    @Then("^the shop the look is retrieved with the following details:$")
    public void verifyShopTheLookDetails(final List<ShopTheLookData> shopTheLookDataList) {
        Assertions.assertThat(shopTheLookPageData).isNotNull();
        final ShopTheLookData shopTheLookDataExpected = shopTheLookDataList.get(0);
        Assertions.assertThat(shopTheLookPageData.getTitle()).isEqualTo(shopTheLookDataExpected.getTitle());
        Assertions.assertThat(shopTheLookPageData.getCountOfLooks())
                .isEqualTo(shopTheLookDataExpected.getCountOfLooks());
    }

    @Then("^the retrieved shop the look will have the following collections:$")
    public void verifyShopTheLookCollectionDetails(
            final List<ShopTheLookCollectionsData> shopTheLookCollectionsDataList) {
        Assertions.assertThat(shopTheLookPageData).isNotNull();
        Assertions.assertThat(shopTheLookPageData.getCollections()).isNotNull();
        Assertions.assertThat(shopTheLookPageData.getCollections()).hasSize(shopTheLookCollectionsDataList.size());
        final Map<String, ShopTheLookCollectionsData> expectedCollectionsMap = createExpectedCollectionsMap(
                shopTheLookCollectionsDataList);
        for (final LooksCollectionData resultCollection : shopTheLookPageData.getCollections()) {
            Assertions.assertThat(expectedCollectionsMap.containsKey(resultCollection.getId()));
            final ShopTheLookCollectionsData expectedCollection = expectedCollectionsMap.get(resultCollection.getId());
            Assertions.assertThat(expectedCollection.getCollectionName()).isEqualTo(resultCollection.getName());
            Assertions.assertThat(expectedCollection.getCountOfLooks()).isEqualTo(resultCollection.getCountOfLooks());
        }
    }

    private Map<String, ShopTheLookCollectionsData> createExpectedCollectionsMap(
            final List<ShopTheLookCollectionsData> shopTheLookCollectionsDataList) {
        final Map<String, ShopTheLookCollectionsData> expectedCollectionsMap = new HashMap<>();
        for (final ShopTheLookCollectionsData expectedCollection : shopTheLookCollectionsDataList) {
            expectedCollectionsMap.put(expectedCollection.getColletionId(), expectedCollection);
        }
        return expectedCollectionsMap;
    }

    @Then("^the retrieved shop the look will have the following looks:$")
    public void verifyShopTheLooksLookDetailsPresent(final List<ShopTheLookLooksData> shopTheLookLooksDataList) {
        Assertions.assertThat(shopTheLookPageData).isNotNull();
        Assertions.assertThat(shopTheLookPageData.getLooks()).isNotNull();
        Assertions.assertThat(shopTheLookPageData.getLooks()).hasSize(shopTheLookLooksDataList.size());
        final Map<String, ShopTheLookLooksData> expectedLooksMap = createExpectedLooksMap(shopTheLookLooksDataList);
        for (final LookDetailsData lookDetailsResult : shopTheLookPageData.getLooks()) {
            Assertions.assertThat(expectedLooksMap.containsKey(lookDetailsResult.getId()));
            final ShopTheLookLooksData expectedLook = expectedLooksMap.get(lookDetailsResult.getId());
            Assertions.assertThat(lookDetailsResult.getUrl()).isEqualTo(expectedLook.getUrl());
            Assertions.assertThat(lookDetailsResult.getName()).isEqualTo(expectedLook.getLookName());
            Assertions.assertThat(lookDetailsResult.getCollectionName()).isEqualTo(expectedLook.getCollectionName());
            Assertions.assertThat(lookDetailsResult.getCollectionId()).isEqualTo(expectedLook.getCollectionId());
        }
    }

    private Map<String, ShopTheLookLooksData> createExpectedLooksMap(
            final List<ShopTheLookLooksData> shopTheLookLooksDataList) {
        final Map<String, ShopTheLookLooksData> expectedLooksMap = new HashMap<>();
        for (final ShopTheLookLooksData expectedLook : shopTheLookLooksDataList) {
            expectedLooksMap.put(expectedLook.getLookId(), expectedLook);
        }
        return expectedLooksMap;
    }

    @Then("^will not contain the following looks:$")
    public void verifyShopTheLooksLookDetailsNotPresent(final List<String> lookIdsList) {
        Assertions.assertThat(shopTheLookPageData).isNotNull();
        Assertions.assertThat(shopTheLookPageData.getLooks()).isNotNull();
        for (final LookDetailsData lookDetailsResult : shopTheLookPageData.getLooks()) {
            Assertions.assertThat(lookIdsList.contains(lookDetailsResult.getId())).isFalse();
        }
    }

    @When("^the valid looks are being retrieved for id '(.*)'$")
    public void populateLookDetailsData(final String lookCode) {
        lookDetailsData = targetShopTheLookFacade.populateLookDetailsData(lookCode);
        productCodeList = targetShopTheLookFacade.getProductCodeListByLook(lookCode);
    }

    @Then("^the look is retrieved with the following details:$")
    public void verifyLookDetails(final List<LookDetailsData> lookDatas) {
        Assertions.assertThat(lookDatas.get(0).getId()).isEqualTo(lookDetailsData.getId());
        Assertions.assertThat(lookDatas.get(0).getName()).isEqualTo(lookDetailsData.getName());
        Assertions.assertThat(lookDatas.get(0).getDescription()).isEqualTo(lookDetailsData.getDescription());
        Assertions.assertThat(lookDatas.get(0).getCollectionId()).isEqualTo(lookDetailsData.getCollectionId());
        Assertions.assertThat(lookDatas.get(0).getCollectionName()).isEqualTo(lookDetailsData.getCollectionName());
    }

    @When("^the look is retrieved with the following images:$")
    public void verifyLookMedias(final List<ImageContainer> imageContainer) {
        Assertions.assertThat(
                lookDetailsData.getImages().getGridImageUrl() == null ? StringUtils.EMPTY : lookDetailsData.getImages()
                        .getGridImageUrl())
                .isEqualTo(imageContainer.get(0).getGridImageUrl());
        Assertions.assertThat(
                lookDetailsData.getImages().getHeroImageUrl() == null ? StringUtils.EMPTY : lookDetailsData.getImages()
                        .getHeroImageUrl())
                .isEqualTo(imageContainer.get(0).getHeroImageUrl());
        Assertions.assertThat(
                lookDetailsData.getImages().getLargeImageUrl() == null ? StringUtils.EMPTY : lookDetailsData
                        .getImages().getLargeImageUrl())
                .isEqualTo(imageContainer.get(0).getLargeImageUrl());
        Assertions.assertThat(
                lookDetailsData.getImages().getListImageUrl() == null ? StringUtils.EMPTY : lookDetailsData.getImages()
                        .getListImageUrl())
                .isEqualTo(imageContainer.get(0).getListImageUrl());
        Assertions.assertThat(
                lookDetailsData.getImages().getSwatchImageUrl() == null ? StringUtils.EMPTY : lookDetailsData
                        .getImages().getSwatchImageUrl())
                .isEqualTo(imageContainer.get(0).getSwatchImageUrl());
        Assertions.assertThat(
                lookDetailsData.getImages().getThumbImageUrl() == null ? StringUtils.EMPTY : lookDetailsData
                        .getImages().getThumbImageUrl())
                .isEqualTo(imageContainer.get(0).getThumbImageUrl());
    }

    @Then("^the look contains the products:$")
    public void verifyProductsInLook(final List<String> products) {
        Assertions.assertThat(productCodeList.size()).isEqualTo(products.size());
        for (int i = 0; i < productCodeList.size(); i++) {
            Assertions.assertThat(productCodeList.get(i)).isEqualTo(products.get(i));
        }
    }
}
