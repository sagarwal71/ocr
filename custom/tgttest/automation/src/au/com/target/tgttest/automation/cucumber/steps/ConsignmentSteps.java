/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.jms.JMSException;
import javax.xml.bind.JAXBException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.junit.Assert;
import org.springframework.http.HttpStatus;
import org.springframework.jms.support.converter.MessageConversionException;

import au.com.target.tgtbusproc.constants.TgtbusprocConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfluent.data.Address;
import au.com.target.tgtfluent.data.Fulfilment;
import au.com.target.tgtfluent.data.FulfilmentItem;
import au.com.target.tgtfluent.exception.FluentFulfilmentException;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectState;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;
import au.com.target.tgtfulfilment.model.ConsignmentExtractRecordModel;
import au.com.target.tgtfulfilment.model.ConsignmentParcelModel;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;
import au.com.target.tgtjms.jms.model.ConsignmentExtract;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.BusinessProcessUtil;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.CncExtractFacade;
import au.com.target.tgttest.automation.facade.CncExtractFacade.CncExtractDto;
import au.com.target.tgttest.automation.facade.ConsignmentUtil;
import au.com.target.tgttest.automation.facade.FastlineFacade;
import au.com.target.tgttest.automation.facade.FulfilmentFacade;
import au.com.target.tgttest.automation.facade.OrderUtil;
import au.com.target.tgttest.automation.facade.ProductUtil;
import au.com.target.tgttest.automation.facade.bean.CartEntry;
import au.com.target.tgttest.automation.facade.bean.ConsignmentForStore;
import au.com.target.tgttest.automation.facade.bean.OrderEntry;
import au.com.target.tgttest.automation.facade.bean.OrderExtractData;
import au.com.target.tgttest.automation.facade.bean.ParcelData;
import au.com.target.tgttest.automation.facade.bean.PickEntry;
import au.com.target.tgttest.automation.facade.bean.ProductPartialData;
import au.com.target.tgttest.automation.facade.bean.StockOnHand;
import au.com.target.tgttest.automation.facade.domain.Checkout;
import au.com.target.tgttest.automation.facade.domain.Order;
import au.com.target.tgttest.automation.mock.MockInventoryAdjustmentClientService;
import au.com.target.tgttest.automation.mock.MockTargetCaShippingService;
import au.com.target.tgttest.automation.util.StoreNumberResolver;
import au.com.target.tgttinker.mock.instore.ProductData;
import au.com.target.tgttinker.mock.instore.StoreStockData;
import au.com.target.tgtwebmethods.ca.data.request.SubmitOrderShipmentRequest;
import au.com.target.tgtwebmethods.constants.TgtwebmethodsConstants;
import au.com.target.tgtwebmethods.inventory.dto.request.InventoryAdjustmentDto;
import au.com.target.tgtwebmethods.inventory.dto.request.InventoryAdjustmentRequestDto;
import au.com.target.tgtwsfacades.instore.dto.consignments.Consignment;
import au.com.target.tgtwsfacades.instore.dto.consignments.ConsignmentsResponseData;
import au.com.target.tgtwsfacades.instore.dto.consignments.Customer;
import au.com.target.tgtwsfacades.instore.dto.consignments.Parcel;
import au.com.target.tgtwsfacades.instore.dto.consignments.Parcels;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;



/**
 * @author rsamuel3
 * 
 */
public class ConsignmentSteps {

    // Camberwell
    private static final String DEFAULT_FULFIL_STORE = "7032";

    // Geelong
    private static final String NON_FULFILMENT_STORE = "7001";

    private static final int DEFAULT_OFFSET = 0;

    private static final int DEFAULT_RECS_PER_PAGE = 0;

    private static final int DEFAULT_LAST_X_FETCH_CONSINGMENT_DAYS = 60;

    private static final String CNC_MESSAGE_SUCCESS = "created";

    private static final String DEFAULT_PARCEL_COUNT = "1";

    private static final String DEFAULT_CONSIGNMENT_CODE = "auto11000001";

    private static final String ORDER_EXTRACT_CONSIGNMENT_OFF = "off";

    private static final String ORDER_EXTRACT_CONSIGNMENT_ON = "on";

    private static final String ORDER_NUMBER = "ORDER_NUMBER";

    private static final String CONSIGNMENT_NUMBER = "CONSIGNMENT_NUMBER";


    private static final String NO_REASON = "none";

    private List<CartEntry> cartEntries;

    private Response response;

    private Integer storeNumber = Integer.valueOf(DEFAULT_FULFIL_STORE);

    private String setupConsignmentCode = null;

    private byte[] labelPdfData;

    private List<ConsignmentExtract> extracts = null;

    private TargetConsignmentModel currentConsignment;

    private Exception exception;

    @Given("^empty list of consignments for store '(\\d+)'$")
    public void givenEmptyConsignmentsForStore(final Integer storeNo)
            throws NumberFormatException, TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        storeNumber = storeNo;
    }

    @Given("^list of consignments for store '(.*)' are:$")
    public void givenListOfConsignmentsForStore(final String store, final List<ConsignmentForStore> entries)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, DuplicateUidException {

        final Integer storeNo = StoreNumberResolver.getStoreNumber(store);

        // This will set up consignments rather than actually placing orders
        ServiceLookup.getConsignmentCreationHelper()
                .createConsignmentsForStore(entries, storeNo, "WEB");
        storeNumber = storeNo;
    }

    @Given("^consignments from fluent for store '(.*)' and sales channel '(.*)'$")
    public void eBayConsignmentsFromFluentForStore(final String store, final String salesChannel,
            final List<ConsignmentForStore> entries) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException, DuplicateUidException {

        final Integer storeNo = StoreNumberResolver.getStoreNumber(store);

        // This will set up consignments rather than actually placing orders
        ServiceLookup.getConsignmentCreationHelper()
                .createConsignmentsFromFluentForStore(entries, storeNo, salesChannel);
        storeNumber = storeNo;
    }


    @Given("^list of consignments for store '(.*)' created from fluent fulfiments are:$")
    public void givenListOfConsignmentsForStoreCreatedFromFluentFulfilments(final String store,
            final List<ConsignmentForStore> entries)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, DuplicateUidException {

        final Integer storeNo = StoreNumberResolver.getStoreNumber(store);

        // This will set up consignments rather than actually placing orders
        ServiceLookup.getConsignmentCreationHelper()
                .createConsignmentsForStoreFromFluentFulfilments(entries, storeNo);
        storeNumber = storeNo;
    }


    @Given("^a consignment is set up for store '(.*)':$")
    public void givenSingleConsignmentsForStore(final String store, final List<ConsignmentForStore> entries)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, DuplicateUidException

    {

        assertThat(entries).hasSize(1);
        givenListOfConsignmentsForStore(store, entries);

        setupConsignmentCode = entries.get(0).getConsignmentCode();
    }

    @Given("^a consignment of type '(.*)' is set up for store '(.*)'$")
    public void givenConsignmentSetUpForStore(final String ofcOrderType, final String storeNo)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, DuplicateUidException {
        final TargetPointOfServiceModel tpos = StoreNumberResolver.getStore(storeNo);
        // This will set up a consignment rather than actually placing order
        final List<ConsignmentForStore> entries = Collections.singletonList(ConsignmentUtil
                .createConsignmentRecordSentToStore(
                        DEFAULT_CONSIGNMENT_CODE, ofcOrderType, tpos));
        givenListOfConsignmentsForStore(storeNo, entries);

        // Now set the order domain model
        OrderUtil.setOrderDomainModel(DEFAULT_CONSIGNMENT_CODE);
    }

    @Given("^a consignment is sent to fastline$")
    public void createOrderForFastline() throws Exception,
            DuplicateUidException {
        cartEntries = new ArrayList<>();
        final CartEntry entry = new CartEntry(CheckoutUtil.ANY_PRD_CODE, CheckoutUtil.ANY_PRD_QTY,
                Double.valueOf(CheckoutUtil.ANY_PRD_PRICE));
        cartEntries.add(entry);

        consignmentSentToFastline(cartEntries);
    }

    @Given("^feature 'fastline.orderextract.consignmentid' is switched '(on|off)'$")
    public void updateFeatureSwitchForOrderExtract(final String status) {
        if (ORDER_EXTRACT_CONSIGNMENT_OFF.equalsIgnoreCase(status)) {
            ConsignmentUtil.setUpFeatureSwitchForSendConsignmentToFastlineFalse();
        }
        else if (ORDER_EXTRACT_CONSIGNMENT_ON.equalsIgnoreCase(status)) {
            ConsignmentUtil.setUpFeatureSwitchForSendConsignmentToFastlineTrue();
        }
        else {
            throw new IllegalArgumentException("Unexpected value for status parameter: " + status);
        }
    }


    @Then("^ConsignmentExtractRecord has consignmentId used '(true|false)'$")
    public void checkConsignmentExtractRecord(final String consignmentIdUsed) {
        final TargetConsignmentModel consignmentModel = ConsignmentUtil.verifyAndGetSingleOrderConsignment();
        final ConsignmentExtractRecordModel consignmentExtractmodel = ServiceLookup.getTargetFeatureServices()
                .getConsExtractRecordByConsignment(consignmentModel);
        assertThat(consignmentExtractmodel).isNotNull();
        assertThat(consignmentExtractmodel.getConsignmentAsId()).isEqualTo(Boolean.valueOf(consignmentIdUsed));
    }

    @Then("^the following fastline extracts are sent:$")
    public void expectedExtracts(final List<OrderExtractData> expectedOrderExtracts) {
        final List<List<ConsignmentExtract>> actualExtracts = FastlineFacade.getAllExtractMessages();
        int i = 0;
        assertThat(actualExtracts.size()).isEqualTo(expectedOrderExtracts.size());

        for (final List<ConsignmentExtract> list : actualExtracts) {
            final ConsignmentExtract actualExtract = list.get(0);
            final OrderExtractData expectedExtract = expectedOrderExtracts.get(i);
            if (expectedExtract.getConsignmentId().equalsIgnoreCase(ORDER_NUMBER)) {
                assertThat(actualExtract.getConsignmentId()).isEqualTo(Order.getInstance().getOrderNumber());
            }
            else if (expectedExtract.getConsignmentId().equalsIgnoreCase(CONSIGNMENT_NUMBER)) {
                assertThat(actualExtract.getConsignmentId())
                        .isEqualTo(ConsignmentUtil.verifyAndGetSingleOrderConsignment().getCode());
            }
            else {
                throw new IllegalArgumentException(
                        "Unexpected value for consignment id type: " + expectedExtract.getConsignmentId());
            }
            assertThat(actualExtract.getOrderPurpose()).isEqualTo(expectedExtract.getOrderPurpose().substring(0, 1));
            i++;
        }

    }

    @Given("^a consignment sent to fastline with (?:product|products):$")
    public void consignmentSentToFastline(final List<CartEntry> cartEntryData) throws Exception {
        cartEntries = cartEntryData;

        // TODO: this assumes that interstore is disabled 
        // would be better to explicitly disable instore fulfilment in global rules
        placeOrderForStore(cartEntryData, NON_FULFILMENT_STORE);
    }

    @Given("^a consignment sent to a store '(.*)' with (?:product|products):$")
    public void consignmentFulfilledByStore(final String storeNo, final List<CartEntry> cartEntryData)
            throws Exception {
        cartEntries = cartEntryData;
        final String storeNum = StoreNumberResolver.getStoreNumber(storeNo).toString();

        setupStockForStore(storeNum, cartEntryData);

        placeOrderForStore(cartEntryData, storeNum);
    }

    @Given("^a consignment sent to store '(.*)' with existing stocks and (?:product|products):$")
    public void consignmentFulfilledByStoreWithStock(final String storeNo, final List<CartEntry> cartEntryData)
            throws Exception {
        final String storeNum = StoreNumberResolver.getStoreNumber(storeNo).toString();

        placeOrderForStore(cartEntryData, storeNum);
    }

    @Given("^a consignment sent to a store '(.*)'$")
    public void consignmentFulfilledByStoreAnyProduct(final String storeNo)
            throws Exception {

        // Default to one product
        final CartEntry entry = new CartEntry(CheckoutUtil.ANY_PRD_CODE, CheckoutUtil.ANY_PRD_QTY,
                Double.valueOf(CheckoutUtil.ANY_PRD_PRICE));
        final List<CartEntry> cartEntryData = new ArrayList<>();
        cartEntryData.add(entry);

        consignmentFulfilledByStore(storeNo, cartEntryData);
    }

    @Given("^a consignment sent to a store with (?:product|products):$")
    public void consignmentFulfilledByDefaultStore(final List<CartEntry> cartEntryData) throws Exception {
        consignmentFulfilledByStore(DEFAULT_FULFIL_STORE, cartEntryData);
    }

    @Given("^a consignment sent to a store$")
    public void createOrderForInstoreFulfilment() throws Exception {
        consignmentFulfilledByStoreAnyProduct(DEFAULT_FULFIL_STORE);
    }

    @Given("the consignment is in status '(.*)'$")
    public void setConsignmentStatusOnOrder(final String consignmentStatus) {
        ConsignmentModel consignment = ConsignmentUtil.verifyAndGetOrderConsignment(ConsignmentStatus
                .valueOf(consignmentStatus));
        if (consignment == null) {
            consignment = ConsignmentUtil.verifyAndGetSingleOrderConsignment();
            consignment.setStatus(ConsignmentStatus.valueOf(consignmentStatus));
            ServiceLookup.getModelService().save(consignment);
        }
    }

    @Given("consignments have parcels:$")
    public void getConsignmentWithParcels(final List<ParcelData> parcels) {
        String consignmentCode = "";
        TargetConsignmentModel consignment = null;
        int numberOfParcels = 1;
        for (final ParcelData parcel : parcels) {
            if (!consignmentCode.equalsIgnoreCase(parcel.getConsignmentCode())) {
                consignmentCode = parcel.getConsignmentCode();
                consignment = ServiceLookup.getTargetStoreConsignmentService()
                        .getConsignmentByCode(consignmentCode);
                numberOfParcels = 1;
            }
            final ConsignmentParcelModel model = ServiceLookup.getModelService().create(ConsignmentParcelModel.class);
            model.setActualWeight(parcel.getWeight());
            model.setLength(parcel.getLength());
            model.setWidth(parcel.getWidth());
            model.setHeight(parcel.getHeight());
            if (consignment != null) {
                consignment.setParcelCount(Integer.valueOf(numberOfParcels++));
                model.setConsignment(consignment);
            }
            ServiceLookup.getModelService().save(model);
        }
    }

    @Given("^the consignment is accepted$")
    public void theConsignmentIsAcceptedInStore() {
        final ConsignmentModel consignment = ConsignmentUtil.verifyAndGetSingleOrderConsignment();
        ServiceLookup.getTargetInStoreIntegrationFacade().waveConsignmentForInstore(
                consignment.getCode(), storeNumber);
    }

    @Given("^consignments for warehouse '(.*)' are:$")
    public void listOfConsignmentsForWarehouse(final String warehouseCode, final List<ConsignmentForStore> consignments)
            throws Exception {

        ServiceLookup.getConsignmentCreationHelper()
                .createConsignmentForWarehouseCode(consignments, warehouseCode);
    }

    @When("^cancel is recieved from warehouse for the consignment '(.*)'$")
    public void cancelConsignmentRecieved(final String consignmentCode) throws FulfilmentException {
        FulfilmentFacade.processCancelConsignment(consignmentCode);
    }

    @When("^complete is recieved from warehouse for the consignment:'$")
    public void completeConsignmentRecieved(final List<ConsignmentForStore> consignments) throws FulfilmentException {
        FulfilmentFacade.processCompleteConsignment(consignments);
    }

    @When("^the (?:instore |)consignment is rejected$")
    public void consignmentCancelled() throws InterruptedException {
        consignmentCancelledWithReason(NO_REASON);
    }

    @When("^the consignment is rejected with instore reject reason '([^']*)'$")
    public void consignmentCancelledWithReason(final String instoreRejectReasonCode)
            throws InterruptedException {
        consignmentCancelledWithReasonAndStockFlag(instoreRejectReasonCode, null);
    }

    @When("^the consignment is rejected with instore reject reason '([^']*)' and reject state is '([^']*)'$")
    public void consignmentCancelledWithReasonAndStockFlag(String instoreRejectReasonCode,
            final ConsignmentRejectState rejectState)
            throws InterruptedException {
        if (instoreRejectReasonCode.equals(NO_REASON)) {
            instoreRejectReasonCode = null;
        }

        final ConsignmentModel consignment = ConsignmentUtil.verifyAndGetSingleActiveInstoreConsignment();
        ServiceLookup.getTargetInStoreIntegrationFacade().rejectConsignmentForInstore(consignment.getCode(),
                storeNumber, instoreRejectReasonCode, rejectState);
        BusinessProcessUtil.waitForExpectedProcessToFinishAndCheckSuccess("rerouteConsignmentProcess");
        ServiceLookup.getModelService().refresh(consignment);
    }

    @When("^the consignment re-routed to warehouse '(.*)' is waved and picked instore")
    public void consignmentrejectedAndRerouted(final String warehouse) throws InterruptedException {
        final TargetConsignmentModel consignment = verifyWarehouseForNonCancelledConsignment(warehouse);
        waveAndPickConsignment(consignment);
    }

    @When("^run ofc list consignments for store '(.*)' with offset as (\\d+) and records per page as (\\d+)")
    public void ofcListStores(final String storeNo, final int offset, final int recsPerPage)
            throws InterruptedException {

        response = ServiceLookup.getTargetInStoreIntegrationFacade().getConsignmentsForStore(
                Integer.valueOf(storeNo), offset, recsPerPage, DEFAULT_LAST_X_FETCH_CONSINGMENT_DAYS);
        assertThat(response).isNotNull();
    }

    @When("^the consignment is completed instore$")
    public void processConsignmentCompletedInstore() throws InterruptedException {
        processConsignmentCompletedInstoreWithParcelCount(DEFAULT_PARCEL_COUNT);
    }

    @When("^the consignment is completed instore after being picked$")
    public void processConsignmentCompletedInstoreAfterPicked() throws InterruptedException {
        final ConsignmentModel consignment = getNonCancelledConsignment();
        response = ServiceLookup.getTargetInStoreIntegrationFacade()
                .completeConsignmentInstore(consignment.getCode(), storeNumber, Integer.valueOf(DEFAULT_PARCEL_COUNT),
                        null);
        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("orderCompletionProcess");
    }

    @When("^the consignment is completed instore with parcel count as (.*)$")
    public void processConsignmentCompletedInstoreWithParcelCount(final String parcelCount)
            throws InterruptedException {

        processConsignmentPickedInstore();
        final ConsignmentModel consignment = ConsignmentUtil.verifyAndGetSingleOrderConsignment();
        response = ServiceLookup.getTargetInStoreIntegrationFacade()
                .completeConsignmentInstore(consignment.getCode(), storeNumber, Integer.valueOf(parcelCount), null);
        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("orderCompletionProcess");
    }

    @When("^the consignment is completed instore with parcel list:$")
    public void processConsignmentCompletedInstoreWithParcelList(final List<Parcel> parcelList)
            throws InterruptedException {

        processConsignmentPickedInstore();

        final ConsignmentModel consignment = ConsignmentUtil.verifyAndGetSingleOrderConsignment();
        final Parcels parcels = new Parcels();
        parcels.setParcels(parcelList);
        response = ServiceLookup.getTargetInStoreIntegrationFacade()
                .completeConsignmentInstore(consignment.getCode(), storeNumber, null, parcels);
        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("orderCompletionProcess");
    }

    @Given("^the set up consignment has parcel list:$")
    public void setupConsignmentHasParcelList(final List<Parcel> parcelList) {

        ServiceLookup.getConsignmentCreationHelper().createParcels(setupConsignmentCode, parcelList);
    }

    @When("^consignment dispatch label is retrieved by ofc$")
    public void retrieveDispatchLabel() {

        labelPdfData = ServiceLookup.getTargetInStoreIntegrationFacade().getConsignmentDispatchLabel(
                setupConsignmentCode, storeNumber, null, null);
    }

    @Then("^label pdf is (returned|not returned)$")
    public void verifyPdfReturned(final String returned) {

        if (returned.equalsIgnoreCase("returned")) {
            assertThat(labelPdfData).isNotNull();
        }
        else {
            assertThat(labelPdfData).isNull();
        }
    }

    @When("^the consignment is waved and picked instore$")
    public void processConsignmentPickedInstore() throws InterruptedException {
        final ConsignmentModel consignment = ConsignmentUtil.verifyAndGetSingleOrderConsignment();
        waveAndPickConsignment(consignment);
    }

    /**
     * @param consignment
     * @throws InterruptedException
     */
    protected void waveAndPickConsignment(final ConsignmentModel consignment) throws InterruptedException {
        ServiceLookup.getTargetInStoreIntegrationFacade().waveConsignmentForInstore(consignment.getCode(), storeNumber);

        ServiceLookup.getTargetInStoreIntegrationFacade().pickForPackConsignmentInstore(consignment.getCode(),
                storeNumber);
    }

    @When("^the consignment '(.*)' is completed$")
    public void completeConsignmentInStore(final String consignmentCode) throws InterruptedException {
        ServiceLookup.getTargetInStoreIntegrationFacade().completeConsignmentInstore(consignmentCode,
                storeNumber, Integer.valueOf(DEFAULT_PARCEL_COUNT), null);
        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("orderCompletionProcess", consignmentCode);
    }

    @When("^the consignment '(.*)' is rejected$")
    public void rejectConsignmentInStore(final String consignmentCode) throws InterruptedException {
        ServiceLookup.getTargetInStoreIntegrationFacade().rejectConsignmentForInstore(consignmentCode, storeNumber,
                null, null);
        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("rerouteConsignmentProcess",
                consignmentCode);
    }

    @When("^the consignment '(.*)' is picked$")
    public void packConsignmentInStore(final String consignmentCode) throws InterruptedException {
        ServiceLookup.getTargetInStoreIntegrationFacade().pickForPackConsignmentInstore(consignmentCode, storeNumber);
        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("consignmentInstorePickedProcess", consignmentCode);
    }

    @When("^consignment is assigned to '(.*)' and rejected by the store")
    public void consignmentAssignedAndRejectedByStore(final String store) throws InterruptedException {
        assignedWarehouseWillbe(store);
        consignmentCancelled();
    }

    @When("^the consignment is full picked by fastline store$")
    public void fullPickedByFastline() throws Exception {
        final List<PickEntry> pickEntries = new ArrayList<>();
        for (final CartEntry entry : cartEntries) {
            final PickEntry pickEntry = new PickEntry(entry.getProduct(), entry.getQty());
            pickEntries.add(pickEntry);
        }
        final FastlineFulfilmentSteps fastlineSteps = new FastlineFulfilmentSteps();
        fastlineSteps.fastlinePickEntries(pickEntries);
    }

    @When("^the consignment '(.*)' is accepted$")
    public void consignmentAccepted(final String consignmentCode) {
        ServiceLookup.getTargetInStoreIntegrationFacade().waveConsignmentForInstore(consignmentCode, storeNumber);
    }

    @Then("^a single consignment is created$")
    public void verifySingleConsignmentCreated() {
        ConsignmentUtil.verifyAndGetSingleOrderConsignment();
    }

    @Then("^(\\d+) consignments are created$")
    public void verifyMultipleConsignmentsCreated(final Integer num) {
        final List<ConsignmentModel> consignments = ConsignmentUtil.getNotCancelledConsignments();
        assertThat(consignments).isNotNull().hasSize(num.intValue());
    }

    @Then("^no consignments are created$")
    public void verifyNoConsignmentsAreCreated() {
        assertThat(ConsignmentUtil.getAllConsignmentsForOrder()).isEmpty();
    }

    @Then("^consignment status is '(.*)'$")
    public void verifyConsignmentStatus(final String status) {
        final TargetConsignmentModel consignment = ConsignmentUtil
                .verifyAndGetOrderConsignment(ConsignmentStatus.valueOf(status));
        verifyConsignmentStatus(status, consignment);
    }

    @Then("^consignment reject state is '(.*)'$")
    public void verifyConsignmentRejectState(final ConsignmentRejectState rejectState) {
        final TargetConsignmentModel consignment = ConsignmentUtil.getCancelledConsignment();
        assertThat(consignment.getRejectState()).isEqualTo(rejectState);
    }

    @Then("^this consignments status is '(.*)'$")
    public void verifyConsignmentStatusOfCurrentConsignment(final String status) {
        verifyConsignmentStatus(status, currentConsignment);
    }

    @Then("^consignment ship conf flag is set$")
    public void verifyConsignmentShipConfFlag() {
        final TargetConsignmentModel consignment = ConsignmentUtil.verifyAndGetSingleOrderConsignment();
        assertThat(consignment.getShipConfReceived());
    }

    @Then("^consignment parcel count is (.*)$")
    public void verifyConsignmentParcelCount(final String expectedCount) {
        final TargetConsignmentModel consignment = ConsignmentUtil.verifyAndGetSingleOrderConsignment();
        final Integer parcelCount = consignment.getParcelCount();

        if (StringUtils.isEmpty(expectedCount) || expectedCount.equalsIgnoreCase("null")) {
            assertThat(parcelCount).isNull();
        }
        else {
            assertThat(parcelCount).isNotNull().isEqualTo(Integer.parseInt(expectedCount));
        }
    }

    @Then("^consignment parcels are:$")
    public void verifyConsignmentParcelList(final List<Parcel> expectedParcels) {
        final TargetConsignmentModel consignment = ConsignmentUtil.verifyAndGetSingleOrderConsignment();

        final Set<ConsignmentParcelModel> consignmentParcels = consignment.getParcelsDetails();

        if (CollectionUtils.isEmpty(expectedParcels)) {
            assertThat(consignmentParcels).isEmpty();
        }
        else {
            assertThat(consignmentParcels).isNotEmpty().hasSize(expectedParcels.size());

            for (final Parcel expected : expectedParcels) {
                verifyParcelModelExists(consignmentParcels, expected);
            }
        }
    }

    @Then("^the consignment has no tracking id")
    public void verifyNoTrackingIdOnConsignment() {
        final TargetConsignmentModel consignment = getNonCancelledConsignment();
        assertThat(consignment.getTrackingID()).isNull();
    }

    @Then("^the consignment has a valid tracking id that starts with '(.*)'")
    public void verifyTrackingIdOnConsignment(final String startOfTrackingId) {
        final TargetConsignmentModel consignment = getNonCancelledConsignment();
        final String trackingID = consignment.getTrackingID();
        assertThat(trackingID).isNotNull().isNotEmpty().startsWith(startOfTrackingId);
        final String generatedCode = trackingID.replaceFirst(startOfTrackingId, "");
        assertThat(generatedCode).isNotEmpty().hasSize(7);
        assertThat(NumberUtils.isDigits(generatedCode));
    }

    private void verifyParcelModelExists(final Set<ConsignmentParcelModel> consignmentParcels, final Parcel expected) {

        boolean found = false;
        for (final ConsignmentParcelModel model : consignmentParcels) {
            if (doublesAreClose(model.getHeight(), expected.getHeight())
                    && doublesAreClose(model.getLength(), expected.getLength())
                    && doublesAreClose(model.getWidth(), expected.getWidth())
                    && doublesAreClose(model.getActualWeight(), expected.getWeight())) {
                found = true;
                break;
            }
        }

        assertThat(found).as("parcel found").isTrue();
    }

    private boolean doublesAreClose(final Double d1, final Double d2) {

        if (d1 == null && d2 == null) {
            return true;
        }
        return (d1 != null && d2 != null && (Math.abs(d1.doubleValue() - d2.doubleValue()) < 0.1));
    }

    @Then("^the CnC extract is '(.*)' with parcel count (.*)$")
    public void validateCncExtract(final String cncOutcome,
            final String parcelCount) throws MessageConversionException, JMSException {
        final CncExtractDto extractedMessageObj = CncExtractFacade.getCncMessageExtract();
        if (cncOutcome.equals(CNC_MESSAGE_SUCCESS)) {
            assertThat(extractedMessageObj).isNotNull();
            assertThat(extractedMessageObj.getParcelCount()).isEqualTo(parcelCount);
        }
        else {
            assertThat(extractedMessageObj).isNull();
        }
    }

    @Then("^the CnC extract is not created$")
    public void validateCncExtractNotCreated() throws MessageConversionException, JMSException {
        final CncExtractDto extractedMessageObj = CncExtractFacade.getCncMessageExtract();

        assertThat(extractedMessageObj).isNull();
    }

    @Then("^the ofc error message is '(.*)'$")
    public void processConsignmentCompletedInstoreErrorCodeValidation(final String errorMessage) {
        if (!errorMessage.isEmpty()) {
            assertThat(response.getData().getError()).isNotNull();
            assertThat(response.getData().getError().getCode()).isEqualTo(errorMessage);
        }
        else {
            assertThat(response.getData()).isNull();
        }
    }

    @Then("^re-routing reason is '(.*)'$")
    public void verifyConsignmentReRoutingReason(final String reroutingReason) throws InterruptedException {
        verifyRejectReason(reroutingReason);
        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("rerouteConsignmentProcess");
    }

    @Then("^reject reason is '(.*)'$")
    public void verifyRejectReason(final String rejectReason) {
        final TargetConsignmentModel consignment = ConsignmentUtil
                .getCancelledConsignment();
        assertThat(consignment.getCancelDate()).isNotNull();
        final ConsignmentRejectReason consignmentRejectReason = consignment.getRejectReason();
        assertThat(consignmentRejectReason).isNotNull();
        assertThat(consignmentRejectReason.getCode()).isEqualToIgnoringCase(rejectReason.toString());
    }

    @Then("^instore reject reason is '(.*)'$")
    public void verifyInstoreRejectReason(final String instoreRejectReason) {
        final TargetConsignmentModel consignment = ConsignmentUtil.getCancelledConsignment();
        assertThat(consignment.getCancelDate()).isNotNull();
        assertThat(consignment.getInstoreRejectReason()).isNotNull();
        assertThat(consignment.getInstoreRejectReason()).isEqualToIgnoringCase(instoreRejectReason);
    }

    @Then("^all items in order extract will have the Allows short pick flag set to '(.*)'$")
    public void verifyShortPickFlag(final String expected) throws JAXBException {
        extracts = FastlineFacade.getLastConsignmentExtracts();
        for (final ConsignmentExtract consignmentExtract : extracts) {
            assertThat(consignmentExtract.getAllowShortPick()).as("Allows short pick").isEqualTo(expected);
        }
    }

    @Then("^carrier id in order extract is '(.*)' and carrierService in order extract is '(.*)'$")
    public void carrierInOrderExtract(final String carrier, final String carrierService) {
        for (final ConsignmentExtract consignmentExtract : extracts) {
            assertThat(carrier).isEqualTo(consignmentExtract.getCarrier());
            assertThat(carrierService).isEqualTo(consignmentExtract.getCarrierService());
        }
    }

    @Then("^consignment is (sent|not sent) to Fastline$")
    public void verifyConsignmentSentToFastline(final String sentOrNot) {
        if (sentOrNot.equalsIgnoreCase("sent")) {
            verifyFastlineSend(Order.getInstance().getOrderNumber());
        }
        else {
            verifyNoFastlineSend();
        }
    }

    @Then("^consignment is picked by Fastline without order extract$")
    public void verifyConsignmentSentToFastlineWithoutOrderExtract() {
        ConsignmentUtil.verifyAndGetSingleFastlineConsignment();
        verifyNoFastlineSend();
    }

    @Then("^a new consignment created with the same consignment details")
    public void anotherConsignmentCreated() {
        final TargetConsignmentModel cancelledConsignment = ConsignmentUtil
                .getCancelledConsignment();
        assertThat(cancelledConsignment).isNotNull();
        final TargetConsignmentModel newConsignment = getNonCancelledConsignment();
        assertThat(cancelledConsignment.getParcelCount()).isEqualTo(newConsignment.getParcelCount());
    }

    @Then("^the assigned warehouse will be '(.*)'")
    public void assignedWarehouseWillbe(final String warehouseName) {
        verifyWarehouseForNonCancelledConsignment(warehouseName);
    }

    @Then("^a consignment is assigned to warehouse '(.*)'")
    public void getConsignmentAssignedToWarehouse(final String warehouseName) {
        currentConsignment = (TargetConsignmentModel)ConsignmentUtil
                .getNotCancelledConsignmentAssignedToWarehouse(warehouseName);
        assertThat(currentConsignment).isNotNull();
    }

    @Then("^consignment carrier is '(.*)'$")
    public void verifySingleConsignmentCarrier(final String carrierCode) {
        verifyConsignmentCarrier(carrierCode, getTargetConsignment());
    }

    @Then("^consignment deliveryMode is '(.*)'$")
    public void verifyDeliveryMode(final String deliveryMode) {
        verifyConsignmentDeliveryMode(deliveryMode, currentConsignment);
    }

    @Then("^consignment deliveryAddress is '(.*)'$")
    public void verifyDeliveryAddress(final String deliveryAddress) {
        verifyConsignmentAddress(deliveryAddress, currentConsignment);
    }

    @Then("^the consignment OFC order type is '(.*)'$")
    public void verifyConsignmentOFCOrderType(final String orderType) {
        final TargetConsignmentModel consignment = ConsignmentUtil.verifyAndGetSingleOrderConsignment();
        if (StringUtils.isEmpty(orderType) || orderType.equalsIgnoreCase("null")) {
            assertThat(consignment.getOfcOrderType()).isNull();
        }
        else {
            assertThat(consignment.getOfcOrderType().name()).isNotNull().isEqualTo(orderType);
        }
    }

    @Then("^the OFC order type in portal for store '(.*)' is '(.*)'")
    public void getConsignmentForStore(final String store, final String ofcOrderType) {

        final TargetConsignmentModel consignment = ConsignmentUtil.verifyAndGetSingleOrderConsignment();

        // Get the full list of consignments
        response = ServiceLookup.getTargetInStoreIntegrationFacade().getConsignmentsForStore(
                Integer.valueOf(store), 0, 20, DEFAULT_LAST_X_FETCH_CONSINGMENT_DAYS);
        final ConsignmentsResponseData consignmentResponseData = (ConsignmentsResponseData)response.getData();
        final List<Consignment> consignmentResponses = consignmentResponseData.getConsignments();
        final Consignment consignmentData = ConsignmentUtil.getConsignment(consignment.getCode(), consignmentResponses);
        assertThat(consignmentData).isNotNull();

        if (StringUtils.isEmpty(ofcOrderType) || ofcOrderType.equalsIgnoreCase("null")) {
            assertThat(consignmentData.getDeliveryType()).isNull();
        }
        else {
            assertThat(consignmentData.getDeliveryType().toString()).isEqualTo(ofcOrderType);
        }
    }


    @Then("^ofc list of consignments is:$")
    public void verifyOfcConsignmentsList(final List<ConsignmentForStore> consignments) {
        assertThat(response).isNotNull();
        assertThat(response.getData()).isInstanceOf(ConsignmentsResponseData.class);

        final ConsignmentsResponseData reponseData = (ConsignmentsResponseData)response.getData();
        for (final ConsignmentForStore consignmentForStore : consignments) {
            assertThat(reponseData.getConsignments()).isNotEmpty();
            final Consignment consignment = ConsignmentUtil.getConsignment("a" + Order.getInstance().getOrderNumber(),
                    reponseData.getConsignments());
            assertThat(consignment).isNotNull();
            final Customer customer = consignment.getCustomer();
            assertThat(customer).isNotNull();
            final String firstName = customer.getFirstName();
            assertThat(firstName).isNotNull();
            assertThat(firstName).isEqualTo(consignmentForStore.getCustomer());
            assertThat(consignment.getTotalItems()).isEqualTo(
                    Long.valueOf(consignmentForStore.getItems()));
            assertThat(consignment.getStatus()).isEqualToIgnoringCase(
                    consignmentForStore.getConsignmentStatus());
        }
    }

    @Then("^consignment status for '(.*)' is '(.*)'$")
    public void consignmentStatusForGivenConsignmentIs(final String consignmentCode, final String consignmentStatus) {
        verifyConsignmentStatus(consignmentStatus, ConsignmentUtil.getConsignmentForCode(consignmentCode));
    }

    @Then("^consignment '(.*)' has a status of '(.*)' and order status of '(.*)'$")
    public void orderStatusForGivenConsignmentIs(final String consignmentCode, final String consignmentStatus,
            final String orderStatus) {
        final TargetConsignmentModel consignment = getTargetConsignmentModelForCode(consignmentCode);
        verifyConsignmentStatus(consignmentStatus, consignment);
        OrderSteps.verifyOrderInStatus(consignment.getOrder().getCode(), orderStatus);
    }

    @Then("^consignment list is ordered as (.*)$")
    public void verifyConsignmentResult(final String consignmentCodes) {
        final List<String> expectedCodes = getConsignmentCodes(consignmentCodes);

        assertThat(response).isNotNull();
        assertThat(response.getData()).isInstanceOf(ConsignmentsResponseData.class);

        final ConsignmentsResponseData reponseData = (ConsignmentsResponseData)response.getData();

        // Check both size and order
        assertThat(reponseData.getConsignments()).isNotEmpty();
        assertThat(expectedCodes.size()).isEqualTo(reponseData.getConsignments().size());

        for (int i = 0; i < reponseData.getConsignments().size(); i++) {
            assertThat(reponseData.getConsignments().get(i).getCode()).as("result consignment code").isEqualTo(
                    expectedCodes.get(i));
        }
    }

    @Then("^consignment list contains:$")
    public void verifyConsignmentWithClearanceProduct(final List<ConsignmentForStore> expectedCons) {
        assertThat(response).isNotNull();
        assertThat(response.getData()).isInstanceOf(ConsignmentsResponseData.class);
        final ConsignmentsResponseData reponseData = (ConsignmentsResponseData)response.getData();
        final List<Consignment> consignments = reponseData.getConsignments();
        assertThat(consignments).isNotEmpty();
        int numberOfConsignmentsMatched = 0;
        for (final ConsignmentForStore expectedCon : expectedCons) {
            for (final Consignment resultCon : consignments) {
                if (StringUtils.equals(expectedCon.getConsignmentCode(), resultCon.getCode())) {
                    assertThat(expectedCon.isClearance()).isEqualTo(resultCon.isClearance());
                    numberOfConsignmentsMatched++;
                }
            }
        }
        assertThat(expectedCons).hasSize(numberOfConsignmentsMatched);
    }

    @Then("^consignment '(.*)' contains products:$")
    public void verifyConsignmentWithProductDetails(final String consignmentCode,
            final List<ProductPartialData> product) {
        assertThat(response).isNotNull();
        assertThat(response.getData()).isInstanceOf(ConsignmentsResponseData.class);
        final ConsignmentsResponseData reponseData = (ConsignmentsResponseData)response.getData();
        final List<Consignment> consignments = reponseData.getConsignments();
        assertThat(consignments).isNotEmpty();
        TargetConsignmentModel consignment = null;
        for (final Consignment resultCon : consignments) {
            if (resultCon.getCode().equals(consignmentCode)) {
                consignment = ServiceLookup.getTargetStoreConsignmentService()
                        .getConsignmentByCode(consignmentCode);
                break;
            }
        }
        assertThat(consignment).isNotNull();
        final Set<ConsignmentEntryModel> conEntry = consignment.getConsignmentEntries();
        int numberOfProductsMatched = 0;
        for (final ConsignmentEntryModel entry : conEntry) {
            final String prodCode = entry.getOrderEntry().getProduct().getCode();
            final TargetProductModel baseProduct = (TargetProductModel)ProductUtil
                    .getBaseProduct(ProductUtil
                            .getProductModel(prodCode));
            for (final ProductPartialData prod : product) {
                if (StringUtils.equals(prodCode, prod.getProductCode())) {
                    assertThat(baseProduct.getClearanceProduct().booleanValue()).isEqualTo(prod.isClearance());
                    numberOfProductsMatched++;
                }
            }
        }
        assertThat(conEntry).hasSize(numberOfProductsMatched);
    }

    @When("^run ofc list consignments for store$")
    public void ofcListStoreConsignments() throws InterruptedException {

        response = ServiceLookup.getTargetInStoreIntegrationFacade().getConsignmentsForStore(
                Integer.valueOf(DEFAULT_FULFIL_STORE), DEFAULT_OFFSET, DEFAULT_RECS_PER_PAGE,
                DEFAULT_LAST_X_FETCH_CONSINGMENT_DAYS);
        assertThat(response).isNotNull();
    }



    private List<String> getConsignmentCodes(final String consignmentCodes) {
        return new ArrayList<String>(Arrays.asList(consignmentCodes.split("\\s*,\\s*")));
    }

    /**
     * @param consignmentStatus
     * @param targetConsignmentModel
     */
    private void verifyConsignmentStatus(final String consignmentStatus,
            final TargetConsignmentModel targetConsignmentModel) {
        assertThat(targetConsignmentModel.getStatus().getCode()).isEqualToIgnoringCase(consignmentStatus);
    }

    private void verifyConsignmentCarrierCode(final String consignmentCarrierCode,
            final TargetCarrierModel targetCarrierModel) {
        assertThat(targetCarrierModel.getCode()).isEqualToIgnoringCase(consignmentCarrierCode);
    }

    private void verifyFastlineSend(final String orderNumber) {
        final List<ConsignmentExtract> consignmentExtracts = FastlineFacade.getLastConsignmentExtracts();
        assertThat(consignmentExtracts.get(0).getOrderNumber()).as("Extract order number").isEqualTo(orderNumber);
    }

    private void verifyNoFastlineSend() {
        final boolean noFastlineSent = FastlineFacade.noConsignmentsWereSentToFastline();
        assertThat(noFastlineSent).as("No fastline sent").isEqualTo(true);
    }


    private void setupStockForStore(final String storeNo, final List<CartEntry> cartEntryData) {
        final List<StoreStockData> listStoreData = new ArrayList<>();
        final List<ProductData> listProductData = new ArrayList<>();

        final StoreStockData storeData = new StoreStockData();
        storeData.setStore(storeNo);
        for (final CartEntry cartEntry : cartEntryData) {
            final ProductData productDTO = new ProductData();
            productDTO.setCode(cartEntry.getProduct());
            productDTO.setQuantity(String.valueOf(cartEntry.getQty()));
            listProductData.add(productDTO);
        }

        storeData.setProducts(listProductData);
        listStoreData.add(storeData);
        ServiceLookup.getTargetStockUpdateClient().populateStoreStock(listStoreData);
    }

    private void placeOrderForStore(final List<CartEntry> cartEntryData, final String storeNo)
            throws Exception {

        final CartSteps cartSteps = new CartSteps();
        cartSteps.givenCartEntries(cartEntryData);

        Checkout.getInstance().setDeliveryMode("click-and-collect");

        Checkout.getInstance().setStoreNumber(Integer.parseInt(storeNo));

        final OrderSteps orderSteps = new OrderSteps();
        orderSteps.placeOrder();
    }

    /**
     * @param warehouseName
     */
    private TargetConsignmentModel verifyWarehouseForNonCancelledConsignment(final String warehouseName) {
        final TargetConsignmentModel consignment = getNonCancelledConsignment();
        verifyConsignmentWarehouse(consignment, warehouseName);
        return consignment;
    }

    private void verifyConsignmentWarehouse(final TargetConsignmentModel consignment, final String warehouseName) {

        final WarehouseModel warehouse = consignment.getWarehouse();
        assertThat(warehouse).isNotNull();
        assertThat(warehouse.getName()).isEqualTo(warehouseName);
    }

    private void verifyConsignmentCarrier(final String carrierCode, final TargetConsignmentModel consignment) {

        assertThat(consignment.getTargetCarrier()).isNotNull();
        assertThat(consignment.getTargetCarrier().getCode()).isNotNull().isEqualTo(carrierCode);
    }

    private void verifyConsignmentDeliveryMode(final String deliveryMode, final TargetConsignmentModel consignment) {

        assertThat(consignment.getDeliveryMode()).isNotNull();
        assertThat(consignment.getDeliveryMode().getCode()).isNotNull().contains(deliveryMode);
    }

    private void verifyConsignmentAddress(final String deliveryAddress, final TargetConsignmentModel consignment) {
        final AddressModel address = consignment.getShippingAddress();
        if (deliveryAddress.equalsIgnoreCase("ORDER-DELIVERY-ADDRESS")) {
            assertThat(address).isNotNull().isEqualTo(consignment.getOrder().getDeliveryAddress());
        }
        else {
            assertThat(address).isNull();
        }
    }

    /**
     * @return TargetConsignmentModel
     */
    private TargetConsignmentModel getNonCancelledConsignment() {
        final List<ConsignmentModel> consignments = ConsignmentUtil
                .getNotCancelledConsignments();
        assertThat(consignments).hasSize(1);
        final TargetConsignmentModel consignment = (TargetConsignmentModel)consignments.get(0);
        return consignment;
    }

    @Then("^consignmentOne assigned warehouse will be '(.*)' containing product '(.*)' and assigned carrier '(.*)'$")
    public void verifyFirstConsignmentAfterSplit(final String warehouse, final String products, final String carrier) {
        ConsignmentUtil.verifyConsignmentAfterSplit(warehouse, products, carrier);
    }

    @Then("^consignmentTwo assigned warehouse will be '(.*)' containing product '(.*)' and assigned carrier '(.*)'$")
    public void verifySecondConsignmentAfterSplit(final String warehouse, final String products, final String carrier) {
        if (!StringUtils.equals(warehouse.trim(), "N/A")) {
            ConsignmentUtil.verifyConsignmentAfterSplit(warehouse, products, carrier);
        }
    }

    @Then("^consignment contains products:$")
    public void verifySingleConsignmentContainsProducts(final List<OrderEntry> expectedEntries) {

        verifyConsignmentContainsProducts(expectedEntries, currentConsignment);
    }

    private void verifyConsignmentContainsProducts(final List<OrderEntry> expectedEntries,
            final TargetConsignmentModel consignment) {

        final Set<ConsignmentEntryModel> actualEntries = consignment.getConsignmentEntries();

        if (CollectionUtils.isEmpty(expectedEntries)) {
            assertThat(actualEntries).isEmpty();
            return;
        }

        assertThat(actualEntries).isNotEmpty().hasSize(expectedEntries.size());

        for (final OrderEntry expectedEntry : expectedEntries) {

            boolean found = false;
            final String prodCode = expectedEntry.getProduct();

            for (final ConsignmentEntryModel conEntry : actualEntries) {

                assertThat(conEntry.getOrderEntry()).isNotNull();
                assertThat(conEntry.getOrderEntry().getProduct()).isNotNull();
                assertThat(conEntry.getOrderEntry().getProduct().getCode()).isNotNull();

                if (conEntry.getOrderEntry().getProduct().getCode().equals(prodCode)) {

                    found = true;
                    assertThat(conEntry.getQuantity()).isEqualTo(expectedEntry.getQty());
                }
            }
            assertThat(found).as("Found product " + prodCode).isTrue();
        }

    }

    private TargetConsignmentModel getTargetConsignment() {
        if (currentConsignment != null) {
            return currentConsignment;
        }
        else {
            return getNonCancelledConsignment();
        }
    }

    @Given("^cronjob resendNotAcknowledgedOrderExtractsJob is trigged$")
    public void triggerResendNotAcknowledgedOrderExtractsJob() throws InterruptedException {
        final CronJobModel cronJob = ServiceLookup.getModelService().create(CronJobModel.class);
        ServiceLookup.getResendNotAcknowledgedOrderExtractsJob().perform(cronJob);
        BusinessProcessUtil.waitForProcessToFinish(TgtbusprocConstants.BusinessProcess.RESEND_CONSIGNMENT_TO_WAREHOUSE);
    }

    @Then("^extract is not sent to warehouse$")
    public void extractNotSentToWarehouse() {
        assertThat(ServiceLookup.getSendToWarehouseRestClient().getLastRequest()).isNull();
    }


    @Then("^inventory adjustment is sent to webmethods with the following entries:$")
    public void verifyInventoryAdjustmentSentToWebmethods(final List<StockOnHand> stockOnHandRequestList) {
        assertThat(ServiceLookup.getSendToWarehouseRestClient().getLastRequest()).isNull();
        final InventoryAdjustmentRequestDto request = MockInventoryAdjustmentClientService.getLastRequest();
        assertThat(request.getInventoryAdjustments()).isNotEmpty();
        assertThat(request.getInventoryAdjustments()).hasSize(stockOnHandRequestList.size());
        for (final StockOnHand stockOnHandRequest : stockOnHandRequestList) {
            for (final InventoryAdjustmentDto inventoryAdjustmentDto : request.getInventoryAdjustments()) {
                if (stockOnHandRequest.getProductCode().equalsIgnoreCase(inventoryAdjustmentDto.getCode())
                        && stockOnHandRequest.getStore().equals(inventoryAdjustmentDto.getStoreNum())) {
                    assertThat(inventoryAdjustmentDto.getAdjustmentQty())
                            .isEqualTo(stockOnHandRequest.getAdjustmentQuantity());
                }
            }
        }

    }

    @Then("^inventory adjustment is success for fluent fulfilment '(.*)'$")
    public void verifyInventoryAdjustmentPassed(final String consignmentCode) throws NotFoundException {

        assertThat(ServiceLookup.getSendToWarehouseRestClient().getLastRequest()).isNull();
        final InventoryAdjustmentRequestDto request = MockInventoryAdjustmentClientService.getLastRequest();
        assertThat(request.getInventoryAdjustments()).isNotEmpty();

        final TargetConsignmentModel consignmentModel = ServiceLookup.getTargetConsignmentService()
                .getConsignmentForCode(consignmentCode);
        assertThat(consignmentModel).isNotNull();
        assertThat(consignmentModel.getConsignmentEntries()).isNotEmpty();
    }


    @Then("^'(.*)' consignment status is '(.*)'$")
    public void verifyWarehouseConsignmentStatus(final String warehouse, final String consignmentStatus) {
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        Assert.assertNotNull(orderModel);
        final Set<ConsignmentModel> consignments = orderModel.getConsignments();

        for (final ConsignmentModel con : consignments) {
            Assert.assertNotNull(con.getWarehouse());

            if (StringUtils.equalsIgnoreCase(warehouse, con.getWarehouse().getName())) {
                ServiceLookup.getModelService().refresh(con);
                assertThat(con.getStatus()).isEqualTo(ConsignmentStatus.valueOf(consignmentStatus));
                return;
            }
        }
        fail();
    }

    @When("^complete consignment is recieved for '(.*)'$")
    public void updateConsignmentStatusForWarehouse(final String warehouseCode) throws FulfilmentException {
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        Assert.assertNotNull(orderModel);
        final Set<ConsignmentModel> consignments = orderModel.getConsignments();

        for (final ConsignmentModel con : consignments) {
            Assert.assertNotNull(con.getWarehouse());

            if (StringUtils.equals(warehouseCode, con.getWarehouse().getName())) {
                FulfilmentFacade.processCompleteConsignmentWithDefaultData(con.getCode());
                break;
            }
        }
    }

    @When("^the consignment is accepted by store '(.*)'$")
    public void consignmentIsAcceptedBySpecificStore(final String storeCode) {

        final Integer storeNo = StoreNumberResolver.getStoreNumber(storeCode);
        ServiceLookup.getTargetInStoreIntegrationFacade().waveConsignmentForInstore(
                currentConsignment.getCode(), storeNo);
    }

    @When("^the consignment is packed by store '(.*)'$")
    public void processConsignmentPackedBySpecificStore(final String storeCode) throws InterruptedException {

        final Integer storeNo = StoreNumberResolver.getStoreNumber(storeCode);
        response = ServiceLookup.getTargetInStoreIntegrationFacade()
                .completeConsignmentInstore(currentConsignment.getCode(), storeNo,
                        Integer.valueOf(DEFAULT_PARCEL_COUNT),
                        null);
        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("consignmentInstorePickedProcess");
    }

    @When("^the consignment is picked by store '(.*)'$")
    public void consignmentIsPickedBySpecificStore(final String storeCode) throws InterruptedException {

        final OrderModel orderModel = Order.getInstance().getOrderModel();
        final Integer storeNo = StoreNumberResolver.getStoreNumber(storeCode);
        ServiceLookup.getTargetInStoreIntegrationFacade().pickForPackConsignmentInstore(
                currentConsignment.getCode(), storeNo);
        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("consignmentInstorePickedProcess",
                orderModel.getCode());
    }

    @When("platform receives fulfilment update from fluent for consignment '(.*)'")
    public void updateFulfilment(final String consignmentCode, final List<Fulfilment> fulfilments)
            throws NotFoundException {

        final Fulfilment fulfilment = getFluentFulfilment(consignmentCode, fulfilments, null);
        try {
            ServiceLookup.getFluentFulfilmentService().upsertFulfilment(fulfilment);
            exception = null;
        }
        catch (final Exception e) {
            exception = e;
        }
    }

    /**
     * @param consignmentCode
     * @param fulfilments
     * @return fulfilment
     */
    private Fulfilment getFluentFulfilment(final String consignmentCode, final List<Fulfilment> fulfilments,
            final String shippedQty) {
        final TargetConsignmentModel consignment = ServiceLookup.getTargetConsignmentService()
                .getOrCreateConsignmentForCode(consignmentCode);

        final Fulfilment fulfilment = fulfilments.get(0);
        fulfilment.setFulfilmentId(consignmentCode);
        final Address fromAddress = new Address();
        fromAddress.setLocationRef(consignment.getWarehouse().getCode());
        fulfilment.setFromAddress(fromAddress);
        fulfilment.setOrderId(consignment.getOrder().getFluentId());

        final List<FulfilmentItem> items = new ArrayList<>();
        FulfilmentItem item;
        for (final String productCode : ServiceLookup.getTargetOrderEntryService()
                .getAllProductCodeFromOrderEntries(consignment.getOrder())) {
            item = new FulfilmentItem();
            item.setOrderItemRef(productCode);
            item.setFilledQty(StringUtils.isNotEmpty(shippedQty) ? Integer.valueOf(shippedQty) : Integer.valueOf(0));
            items.add(item);
        }
        fulfilment.setItems(items);
        return fulfilment;
    }

    @When("hybris receives fulfilment update from fluent for consignment '(.*)' and shipped qty '(.*)'")
    public void pickFulfilment(final String consignmentCode, final String shippedQty,
            final List<Fulfilment> fulfilments)
            throws NotFoundException {

        final Fulfilment fulfilment = getFluentFulfilment(consignmentCode, fulfilments, shippedQty);
        try {
            ServiceLookup.getFluentFulfilmentService().upsertFulfilment(fulfilment);
            exception = null;
        }
        catch (final Exception e) {
            exception = e;
        }
    }

    @Then("^consignment request sent to EBay$")
    public void consignmentRequestSentToEBay(final List<ConsignmentForStore> consignments) throws InterruptedException {
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("fluentConsignmentShippedProcess",
                orderModel.getCode());
        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("updateShipmentToPartnerProcess",
                orderModel.getCode());
        final ConsignmentForStore ConsignmentForStore = consignments.get(0);

        final SubmitOrderShipmentRequest request = MockTargetCaShippingService.getShipmentRequest();

        assertThat(
                request.getShipmentList().get(0).getShipmentType()
                        .equalsIgnoreCase(TgtwebmethodsConstants.PartnerFulFillmentConstants.SHIPMENT_PARTIAL))
                                .isTrue();

        assertThat(
                request.getShipmentList().get(0).getTrackingNumber()
                        .equalsIgnoreCase(ConsignmentForStore.getTrackingId()))
                                .isTrue();

    }

    @Then("consignment update status is '(.*)'")
    public void verifyConsignmentUpdate(final HttpStatus status) {
        if (HttpStatus.OK.equals(status)) {
            assertThat(exception).isNull();
        }
        else if (HttpStatus.FAILED_DEPENDENCY.equals(status)) {
            assertThat(exception).isInstanceOf(FluentFulfilmentException.class);
        }
        else if (HttpStatus.BAD_REQUEST.equals(status)) {
            assertThat(exception).isInstanceOf(IllegalArgumentException.class);
        }
        else {
            fail();
        }
    }

    @Then("^consignment carrier code for '(.*)' is '(.*)'$")
    public void verifyConsignmentCarrierCode(final String consignmentCode, final String carrierCode) {
        final TargetConsignmentModel targetConsignmentModel = getTargetConsignmentModelForCode(consignmentCode);
        verifyConsignmentCarrierCode(carrierCode, targetConsignmentModel.getTargetCarrier());
    }

    /**
     * @param consignmentCode
     * @return target consignment model
     */
    private TargetConsignmentModel getTargetConsignmentModelForCode(final String consignmentCode) {
        return ConsignmentUtil.getConsignmentForCode(consignmentCode);
    }

    @Then("^consignment trackingId for '(.*)' is '(.*)'$")
    public void verifyConsignmentCarrierTrackingId(final String consignmentCode, final String trackingId) {
        if (trackingId != null) {
            assertThat(getTargetConsignmentModelForCode(consignmentCode).getTrackingID()).isEqualTo(trackingId);
        }
    }

    @Then("^consignment parcel count for '(.*)' is '(.*)'$")
    public void verifyConsignmentCarrierParcelCount(final String consignmentCode, final int parcelCount) {
        assertThat(getTargetConsignmentModelForCode(consignmentCode).getParcelCount()).isEqualTo(parcelCount);
    }

}
