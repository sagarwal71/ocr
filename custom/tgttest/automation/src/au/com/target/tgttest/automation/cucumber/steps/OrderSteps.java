/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;

import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.basecommerce.enums.RefundReason;
import de.hybris.platform.basecommerce.enums.ReturnAction;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.ticket.model.CsTicketModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.order.FluentErrorException;
import au.com.target.tgtcore.ordercancel.data.IpgNewRefundInfoDTO;
import au.com.target.tgtcore.ordercancel.enums.RequestOrigin;
import au.com.target.tgtfacades.order.enums.TargetPlaceOrderResultEnum;
import au.com.target.tgtpaymentprovider.ipg.data.CardDetails;
import au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSingleRefundRequest;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.BusinessProcessUtil;
import au.com.target.tgttest.automation.facade.CancelUtil;
import au.com.target.tgttest.automation.facade.CartUtil;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.OrderUtil;
import au.com.target.tgttest.automation.facade.bean.CartEntry;
import au.com.target.tgttest.automation.facade.bean.ConsignmentForStore;
import au.com.target.tgttest.automation.facade.bean.CreditCardData;
import au.com.target.tgttest.automation.facade.bean.OrderEntry;
import au.com.target.tgttest.automation.facade.bean.OrderFields;
import au.com.target.tgttest.automation.facade.domain.Checkout;
import au.com.target.tgttest.automation.facade.domain.Order;
import au.com.target.tgttest.automation.facade.domain.StoreEmployee;
import au.com.target.tgttest.automation.mock.MockJmsMessageDispatcher;
import au.com.target.tgtutility.util.TargetDateUtil;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Steps for checkout, cancel and return
 * 
 */
public class OrderSteps {

    private static final Logger LOG = Logger.getLogger(OrderSteps.class.getName());
    private static int orderNo = 1050;
    private String cartNumber = null;
    private CartModel cartModel = null;
    private IpgNewRefundInfoDTO ipgNewRefundInfoDTO = null;
    private Exception exception;

    @Given("^something is wrong with order creation$")
    public void somethingWrongWithOrderCreation() {

        Checkout.getInstance().setSomethingWrongWithOrderCreation(true);
    }

    @Given("^order payment goes into REVIEW state$")
    public void orderPaymentGoesIntoReviewState() {

        // Will return PENDING ONCE THEN APPROVED
        ServiceLookup.getTargetMockCcpayment().setMockResponse("PENDING", 1, "APPROVED");
    }

    @When("^order is placed$")
    public void placeOrder() throws Exception {
        //setting it for successful payment
        submitOrder();
        BusinessProcessUtil.waitForOrderBusinessProcesses();
    }

    @When("^order is placed with errors$")
    public void placeOrderError() throws Exception {
        //setting it for successful payment
        submitOrder();
        BusinessProcessUtil.waitForOrderBusinessProcessesWithError();
    }

    @When("^order is placed which might fail$")
    public void placeOrderFailed() throws Exception {
        submitOrder();
        for (int i=0;i<2;i++) {//2 passes to wait for new processes
          BusinessProcessUtil.waitForAllOrderProcessesToFinish(Order.getInstance().getOrderModel());
        }
    }


    @When("^order is placed with (.*) creditcard$")
    public void placeOrderWithIPGCard(final String cardStatus, final List<CreditCardData> creditCards)
            throws Exception {
        if ("invalid".equalsIgnoreCase(cardStatus)) {
            ServiceLookup.getIpgClientMock().setSinglePayResponseCode("1");
        }
        else {
            ServiceLookup.getIpgClientMock().setSinglePayResponseCode("0");
        }
        customerPaysWithCard(creditCards);

        placeOrder();
    }

    @Given("^mobile number in the order is (.*)$")
    public void setMobileNumber(final String mobileNumber) {
        final AddressModel address = Order.getInstance().getOrderModel().getDeliveryAddress();
        address.setPhone1(mobileNumber);
        ServiceLookup.getModelService().save(address);
    }

    @Given("^order was placed$")
    public void givenOrderIsPlaced() throws Exception {
        placeOrder();
        MockJmsMessageDispatcher.clean();
    }

    @Given("^any order$")
    public void givenAnyOrder() throws Exception {
        CheckoutUtil.createAnyCart();

        final OrderSteps orderSteps = new OrderSteps();
        orderSteps.placeOrder();
    }

    @When("^order extract is resent$")
    public void resendOrder() throws InterruptedException {
        OrderProcessModel processModel;
        for (final ConsignmentModel consignmentModel : Order.getInstance().getOrderModel().getConsignments()) {
            processModel = ServiceLookup.getTargetBusinessProcessService().resendConsignmentProcess(consignmentModel);
            BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess(processModel);
        }
    }

    @When("^the customer tries to confirm the (?:cart|cart again)$")
    public void customerConfirmsCart() throws Exception {

        // Remember cart we start with
        cartModel = CheckoutUtil.getCheckoutCart();
        cartNumber = CheckoutUtil.getCheckoutCart().getCode();

        // Similar to placeOrder but we expect the order creation to fail
        submitOrder();

        // Emulate what the checkout controller does
        if (Checkout.getInstance().getPlaceOrderResult().equals(TargetPlaceOrderResultEnum.UNKNOWN)) {
            ServiceLookup.getTargetOrderErrorHandlerFacade().handleOrderCreationFailed(
                    Checkout.getInstance().getPlaceOrderResult(), cartNumber);

            ServiceLookup.getTargetCustomerFacade().invalidateUserSession();
        }

    }

    @When("^order is submitted$")
    public void submitOrder() throws Exception {

        // Place order depending on mode

        final OrderModel orderModel;
        if (Checkout.getInstance().isKioskMethod()) {

            CheckoutUtil.startPlaceOrder();
            orderModel = CheckoutUtil.finishOrder();
        }
        else {
            orderModel = CheckoutUtil.placeOrder();
        }

        Order.setOrder(orderModel);
    }

    @When("^cancel entries$")
    public void cancelEntries(final List<CartEntry> entries) throws InterruptedException {

        // reference OMS PartialCancellationController
        final CancelReason cancelReason = CancelReason.CUSTOMERREQUEST;
        final String notes = "notes";
        final Double shippingAmountToRefund = Double.valueOf(0d);

        try {
            final String status = CancelUtil.partialCancelRequest(entries, Order.getInstance().getOrderModel(),
                    cancelReason, notes,
                    shippingAmountToRefund, ipgNewRefundInfoDTO);
            assertThat(status).isEqualTo("PARTIAL");
        }
        catch (final OrderCancelException e) {
            LOG.info("OrderCancelException", e);
        }

        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("orderCancelProcess");
    }

    @When("^the order is fully cancelled$")
    public void fullCancel() throws InterruptedException {
        fullCancelByCsAgent("CUSTOMERREQUEST");
    }

    @When("^the order is fully cancelled with the reason '(.*)'$")
    public void fullCancelByCsAgent(final String reason) throws InterruptedException {

        // reference CancelOrderButtonEventListener
        final CancelReason cancelReason = CancelReason.valueOf(reason);
        final RequestOrigin requestOrigin = RequestOrigin.CSCOCKPIT;
        final String notes = "notes";
        final Double shippingAmountToRefund = Double.valueOf(0d);
        try {
            final String status = CancelUtil.fullCancelRequest(Order.getInstance().getOrderModel(), cancelReason,
                    notes,
                    shippingAmountToRefund, ipgNewRefundInfoDTO, requestOrigin);
            assertThat(status).isEqualTo("FULL");
        }
        catch (final OrderCancelException e) {
            LOG.info("order cancel exception", e);
        }

        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("orderCancelProcess");
    }

    @When("^return entries$")
    public void returnEntries(final List<CartEntry> entries) throws OrderCancelException, InterruptedException {

        // reference TargetReturnsController
        final RefundReason refundReason = RefundReason.CHANGEOFMINDRETURN;
        final ReturnAction returnAction = ReturnAction.IMMEDIATE;
        final String notes = "notes";
        final Double shippingAmountToRefund = Double.valueOf(0d);

        CancelUtil.createRefundRequest(entries, Order.getInstance().getOrderModel(), refundReason,
                returnAction, notes, shippingAmountToRefund, ipgNewRefundInfoDTO);

        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("orderRefundProcess");
    }


    @Then("^order total is (.*)$")
    public void verifyOrderTotal(final Double value) {

        assertThat(Order.getInstance().getTotal()).isEqualTo(value);
    }

    @Then("^order subtotal is (.*)$")
    public void verifyOrderSubtotal(final Double value) {

        assertThat(Order.getInstance().getSubtotal()).isEqualTo(value);
    }

    @Then("^order delivery cost is (.*)$")
    public void verifyOrderDeliveryCost(final Double value) {

        assertThat(Order.getInstance().getDeliveryCost()).isEqualTo(value);
    }

    @Then("^order total tax is (.*)$")
    public void verifyOrderTotalTax(final Double value) {
        assertThat(Order.getInstance().getTotalTax()).isEqualTo(value);
    }

    @Then("^order discounts is (.*)$")
    public void verifyOrderTotalDiscounts(final Double value) {

        assertThat(Order.getInstance().getTotalDiscounts()).isEqualTo(value);
    }

    @Then("^the order status is (.*)$")
    public void verifyOrderStatus(final String value) {
        assertThat(Order.getInstance().getOrderStatus()).isEqualToIgnoringCase(value);
    }

    @Then("^assigned cockpit group is '(.*)'$")
    public void verifyAssignedCockpitGroup(final String group) {
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        final List<CsTicketModel> csTicketModels = ServiceLookup.getTicketService().getTicketsForOrder(orderModel);
        assertThat(csTicketModels.size()).isEqualTo(1);
        for (final CsTicketModel csTicketModel : csTicketModels) {
            assertThat(csTicketModel.getAssignedGroup()).isNotNull();
            assertThat(csTicketModel.getAssignedGroup().getUid()).isEqualToIgnoringCase(group);
        }
    }

    @Then("^no cockpit group is assigned$")
    public void verifyAssignedCockpitGroupForWebOrders() {
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        final List<CsTicketModel> csTicketModels = ServiceLookup.getTicketService().getTicketsForOrder(orderModel);
        if (CollectionUtils.isNotEmpty(csTicketModels)) {
            assertThat(csTicketModels).onProperty("assignedGroup").isNull();
        }
    }



    @Then("^order is:$")
    public void verifyOrderFields(final List<OrderFields> orderFields) {

        assertThat(orderFields.size()).isEqualTo(1);

        final OrderFields fields = orderFields.get(0);

        if (StringUtils.isNotEmpty(fields.getTotal())) {
            verifyOrderTotal(Double.valueOf(fields.getTotal()));
        }

        if (StringUtils.isNotEmpty(fields.getSubtotal())) {
            verifyOrderSubtotal(Double.valueOf(fields.getSubtotal()));
        }

        if (StringUtils.isNotEmpty(fields.getDeliveryCost())) {
            verifyOrderDeliveryCost(Double.valueOf(fields.getDeliveryCost()));
        }

        if (StringUtils.isNotEmpty(fields.getTotalTax())) {
            verifyOrderTotalTax(Double.valueOf(fields.getTotalTax()));
        }

        if (StringUtils.isNotEmpty(fields.getTotalDiscounts())) {
            verifyOrderTotalDiscounts(Double.valueOf(fields.getTotalDiscounts()));
        }

    }

    @Then("^order has fluent id:$")
    public void verifyFluentIdInOrder(final String id) {
        final String fluentId = Order.getInstance().getOrderModel().getFluentId();
        assertThat(fluentId).as("fluent id").isEqualTo(id);

    }

    @Then("^order user is '(.*)'$")
    public void verifyOrderUser(final String expected) {

        final String uid = Order.getInstance().getOrderModel().getUser().getUid();
        assertThat(uid).as("Order user").isEqualTo(expected);
    }


    @Then("^cart payment is in status '(.*)'$")
    public void verifyCartPaymentInStatus(final String expected) {

        final String paymentStatus = CheckoutUtil.getCheckoutCart()
                .getPaymentTransactions().get(0)
                .getEntries().get(0).getTransactionStatus();
        assertThat(paymentStatus).as("Cart payment status").isEqualTo(expected);
    }

    @Then("^order payment is in status '(.*)'$")
    public void verifyOrderPaymentInStatus(final String expected) {

        final String paymentStatus = Order.getInstance().getOrderModel()
                .getPaymentTransactions().get(0)
                .getEntries().get(0).getTransactionStatus();
        assertThat(paymentStatus).as("Order payment status").isEqualTo(expected);
    }

    @Then("^order is in status '(.*)'$")
    public void verifyOrderInStatus(final String expected) {

        final OrderModel orderModel = Order.getInstance().getOrderModel();
        assertThat(orderModel.getStatus().toString()).as("Order status").isEqualTo(expected);
    }

    public static void verifyOrderInStatus(final String orderCode, final String expected) {

        final OrderModel orderModel = ServiceLookup.getTargetOrderService().findOrderModelForOrderId(orderCode);
        assertThat(orderModel.getStatus().toString()).as("Order status").isEqualTo(expected);
    }

    @Then("^place order result is '(.*)'$")
    public void finishOrder(final String expectedResult) throws InterruptedException {

        assertThat(Checkout.getInstance().getPlaceOrderResult().toString()).as("Place order result").isEqualTo(
                expectedResult);
    }

    @Then("^place preorder result is '(.*)'$")
    public void finishPreOrder(final String expectedResult) throws InterruptedException {
        assertThat(Checkout.getInstance().getPlaceOrderResult().toString()).as("Place preorder result").isEqualTo(
                expectedResult);
    }

    @Then("^place order result is not '(.*)'$")
    public void finishOrderReverseResult(final String expectedResult) {

        assertThat(Checkout.getInstance().getPlaceOrderResult().toString()).as("Place order result").isNotEqualTo(
                expectedResult);
    }

    @Then("business process completes")
    public void verifyBusinessProcessCompletes() throws InterruptedException {

        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("placeOrderProcess");
        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("acceptOrderProcess");
    }

    @Then("fluent business process completes")
    public void verifyFluentBusinessProcessCompletes() throws InterruptedException {
        BusinessProcessUtil.waitForFluentOrderBusinessProcesses();
    }

    @Then("fluent picked business process completes")
    public void verifyFluentPickedBusinessProcessCompletes() throws InterruptedException {
        BusinessProcessUtil.waitForFluentPickedOrderBusinessProcesses();
    }

    @Then("fluent shipped business process completes")
    public void verifyFluentShippedBusinessProcessCompletes() throws InterruptedException {
        BusinessProcessUtil.waitForFluentOrderShippedBusinessProcesses();
    }

    @Then("payment failed business process completes")
    public void verifyPaymentFailedBusinessProcessCompletes() throws InterruptedException {

        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("placeOrderProcess");
        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("paymentFailedProcess");
    }

    @Then("^order employee is '(.*)'$")
    public void verifyOrderEmployee(final String uid) {
        StoreEmployee.getInstance().getUid().equals(uid);
    }

    @Then("^refund value should be '(.*)'$")
    public void verifyRefundValue(final Double amount) {
        assertThat(OrderUtil.isRefundValueMatch(Order.getInstance(), amount)).as("Refund value match").isTrue();
    }

    @Then("^the customer is directed to a failed transaction information page$")
    public void customerGetsFailedTransactionPage() {

        // At the facade level we just check for result of type UNKNOWN
        // Controllers should do the redirect based on this
        assertThat(Checkout.getInstance().getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.UNKNOWN);
    }

    @Then("^the checkout cart is no longer in session$")
    public void verifyCheckoutCartNotInSession() {

        // We will get a new cart which should have a different code
        final CartModel cart = CheckoutUtil.getCheckoutCart();
        assertThat(cart.getCode()).as("New checkout cart code").isNotEqualTo(cartNumber);
    }

    @Then("^the checkout cart still exists in the system$")
    public void verifyCheckoutCartStillExists() {

        assertThat(CartUtil.checkCartExists(cartNumber)).as("cart exists for code: " + cartNumber).isTrue();
    }

    @Then("^the payment is stored on the cart$")
    public void verifyCartPaymentExists() {

        final PaymentInfoModel paymentInfo = cartModel.getPaymentInfo();
        assertThat(paymentInfo).as("cart payment info").isNotNull();
        // TODO: could compare with payment details added based on Checkout payment mode
    }

    @Then("^a second payment is not stored on the cart$")
    public void verifyCartPaymentExistsOnceOnly() {

        final List<PaymentTransactionModel> paymentTransactions = cartModel.getPaymentTransactions();
        assertThat(paymentTransactions).as("cart payment info").isNotEmpty().hasSize(1);

        final List<PaymentTransactionEntryModel> paymentTransactionEntries = paymentTransactions.get(0).getEntries();
        assertThat(paymentTransactionEntries).as("cart payment info").isNotEmpty().hasSize(1);
    }

    @Then("^the order contains the following entries:$")
    public void theOrderContainsTheFollowingEntries(final List<OrderEntry> expectedOrderEntries) {
        final OrderModel order = Order.getInstance().getOrderModel();

        final List<AbstractOrderEntryModel> orderEntries = order.getEntries();

        final List<OrderEntry> actualOrderEntries = new ArrayList<>();
        for (final AbstractOrderEntryModel orderEntry : orderEntries) {
            actualOrderEntries.add(new OrderEntry(orderEntry.getProduct().getCode(), orderEntry.getQuantity()
                    .intValue()));
        }
        assertThat(expectedOrderEntries).isEqualTo(actualOrderEntries);
    }

    @Then("^the IPG refund has been trigerred for the order with amount '(.*)'$")
    public void verifyRefundforIPG(final double amount) {
        final SubmitSingleRefundRequest refundRequest = ServiceLookup.getIpgClientMock().getRefundRequest();
        assertThat(refundRequest).isNotNull();
        //verifying refund request triggered for IPG
        assertThat(Double.valueOf(refundRequest.getRefundInfo().getAmount())).isEqualTo(amount * 100);
        //verify the receiptnumber in request
        assertThat(isReceiptNumberMatch(refundRequest.getRefundInfo().getReceiptNumber())).as(
                "Receipt Number match").isTrue();
        //verify the actual refunded value in hybris
        assertThat(OrderUtil.isRefundValueMatch(Order.getInstance(), Double.valueOf(amount))).as("Refund value match")
                .isTrue();

    }

    @Then("^the IPG refund is not triggered for the order")
    public void verifyRefundNotTrigerredforIPG() {
        final SubmitSingleRefundRequest refundRequest = ServiceLookup.getIpgClientMock().getRefundRequest();
        assertThat(refundRequest).isNull();
    }

    @Given("^customer pays with credit card$")
    public void customerPaysWithCard(final List<CreditCardData> creditCard) {
        final CardDetails cardDetails = new CardDetails();
        cardDetails.setCardNumber(creditCard.get(0).getCardNumber());
        cardDetails.setCardType(creditCard.get(0).getCardType());
        cardDetails.setCardExpiry(creditCard.get(0).getCardExpiry());
        cardDetails.setToken(CheckoutUtil.DEFAULT_CARD_TOKEN);
        ServiceLookup.getIpgClientMock().setCustomerCardData(cardDetails);
    }

    @And("^order has been completed$")
    public void completeTheOrder() {
        final OrderModel order = Order.getInstance().getOrderModel();
        final Set<ConsignmentModel> consignments = order.getConsignments();
        for (final ConsignmentModel consignment : consignments) {
            consignment.setStatus(ConsignmentStatus.SHIPPED);
        }
        order.setStatus(OrderStatus.COMPLETED);

        Order.setOrder(order);
        ServiceLookup.getModelService().save(order);
    }

    @And("^a manual refund informaion is (.*)$")
    public void manualRefundInformaionProvided(final String info) {
        final String[] infoList = info.split(",");
        ipgNewRefundInfoDTO = new IpgNewRefundInfoDTO();
        ipgNewRefundInfoDTO.setReceiptNo(infoList[0]);
        ipgNewRefundInfoDTO.setAmount(Double.valueOf(infoList[1]));
    }

    @Given("^all products with delivery fee amount (.*) are (.*)$")
    public void allProductsAreRefundedWithDeliveryFee(final int deliveryFee, final String refunded)
            throws OrderCancelException, InterruptedException {
        if ("refunded".equalsIgnoreCase(refunded)) {
            final RefundReason refundReason = RefundReason.CHANGEOFMINDRETURN;
            final ReturnAction returnAction = ReturnAction.IMMEDIATE;
            final String notes = "notes";

            final List<CartEntry> orderEntries = new ArrayList<>();
            final CartEntry entry1 = new CartEntry(CheckoutUtil.ANY_PRD_CODE, 2,
                    Double.valueOf(CheckoutUtil.ANY_PRD_PRICE));
            orderEntries.add(entry1);
            try {
                CancelUtil.createRefundRequest(orderEntries, Order.getInstance().getOrderModel(), refundReason,
                        returnAction, notes, Double.valueOf(deliveryFee), ipgNewRefundInfoDTO);
            }
            catch (final OrderCancelException e) {
                LOG.info("OrderCancelException", e);
            }

            BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("orderRefundProcess");
        }
    }

    @And("^reverse giftcard process is completed$")
    public void reverseGiftcardProcessIsCompleted() throws InterruptedException {
        BusinessProcessUtil.waitForBusinessProcessWithNameLikeToFinish("reverseGiftCardProcess-");
    }

    private boolean isReceiptNumberMatch(final String receiptNumber) {
        final String transEntryReceiptNUmber = CheckoutUtil.getReceiptNumberForSuccessfulTrans();
        if (transEntryReceiptNUmber.equals(receiptNumber)) {
            return true;
        }
        return false;
    }

    @Given("^an order with (\\d+) as delivery fee$")
    public void givenAnOrderWithADeliveryFee(final int deliveryFee) throws Exception {
        final OrderModel order = createOrder(deliveryFee, "click-and-collect");
        createSampleShippedConsignment(order);
        saveAndIncrementOrderNumber(order);
    }

    /**
     * Method to verify normal sale start date of the order
     *
     * @param normalSaleStartDate
     */
    @Then("^the normal sale start date is '(.*)'$")
    public void verifyNormalSaleStartDate(final String normalSaleStartDate) {

        final OrderModel orderModel = Order.getInstance().getOrderModel();

        final Date expectedNormalSaleStartDate = TargetDateUtil.getStringAsDate(normalSaleStartDate);

        assertThat(TargetDateUtil.getDateStringForComparison(expectedNormalSaleStartDate)).isEqualTo(
                TargetDateUtil.getDateStringForComparison(orderModel.getNormalSaleStartDateTime()));
    }

    @Then("^started fluent order shipped process with '(.*)'$")
    public void startedFluentOrderShippedProcess(final String status) {
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        final au.com.target.tgtfluent.data.Order order = new au.com.target.tgtfluent.data.Order();
        order.setOrderId(orderModel.getFluentId());
        order.setOrderRef(orderModel.getCode());
        order.setStatus(status);

        try {
            ServiceLookup.getFluentOrderService().updateOrder(order);
            exception = null;
        }
        catch (final Exception e) {
            exception = e;
        }

    }

    @Then("the order update return '(.*)'")
    public void verifyConsignmentUpdate(final HttpStatus status) {
        if (HttpStatus.OK.equals(status)) {
            assertThat(exception).isNull();
        }
        else if (HttpStatus.FAILED_DEPENDENCY.equals(status)) {
            assertThat(exception).isInstanceOf(FluentErrorException.class);
        }
        else {
            fail();
        }
    }

    /**
     * Method to create sample shipped consignment and order is in completed state
     * 
     * @param order
     */
    private void createSampleShippedConsignment(final OrderModel order) {
        final WarehouseModel warehouse = ServiceLookup.getWarehouseService().getWarehouseForCode(
                "Fastline" + "Warehouse");
        ServiceLookup
                .getConsignmentCreationHelper()
                .createConsignment("a" + orderNo, StringUtils.EMPTY,
                        ConsignmentStatus.valueOf("SHIPPED"),
                        warehouse, ConsignmentReRouteSteps.getShippingAddress(order.getDeliveryAddress()), new Date(),
                        null, null, null, null, null, null, null, null, null, order);
        order.setStatus(OrderStatus.COMPLETED);
    }

    @Given("^an order with delivery fee \\$(.*) with consignments:$")
    public void givenAnOrderWithDeliveryFeeAndConsignments(final double deliveryFee,
            final List<ConsignmentForStore> consignments)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, DuplicateUidException {
        final OrderModel order = createOrder(deliveryFee, "home-delivery");
        ServiceLookup.getConsignmentCreationHelper().createConsignments(consignments, order,
                ConsignmentReRouteSteps.getShippingAddress(order.getDeliveryAddress()));
        saveAndIncrementOrderNumber(order);
    }

    @When("^pre order is placed$")
    public void placePreOrder() throws Exception {
        //setting it for successful payment
        submitOrder();
        BusinessProcessUtil.waitForPreOrderBusinessProcesses();
    }

    @When("^fluent preorder is placed$")
    public void placeFluentPreOrder() throws Exception {
        submitOrder();
        BusinessProcessUtil.waitForFluentPreOrderBusinessProcesses();
    }

    @When("^fluent order is placed$")
    public void fluentPlaceOrder() throws Exception {
        //setting it for successful payment
        submitOrder();
        BusinessProcessUtil.waitForFluentOrderBusinessProcesses();
    }

    @When("^fluent order is placed with errors$")
    public void fluentPlaceOrderError() throws Exception {
        //setting it for successful payment
        submitOrder();
        BusinessProcessUtil.waitForFluentOrderBusinessProcessesWithError();
    }

    @Then("^reject order process finishes with status '(.*)'")
    public void verifyRejectOrder(final String status) throws InterruptedException {
        if ("SUCCEEDED".equalsIgnoreCase(status)) {
            BusinessProcessUtil.waitForRejectBusinessProcesses();
        }
        else if ("ERROR".equalsIgnoreCase(status)) {
            BusinessProcessUtil.waitForRejectBusinessProcessesWithError();
        }
    }

    /**
     * Method to create Order with delivery fee and delivery mode
     * 
     * @param deliveryFee
     * @return OrderModel
     * @throws DuplicateUidException
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    private OrderModel createOrder(final double deliveryFee, final String deliveryMode)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, DuplicateUidException {
        final OrderModel order = ServiceLookup.getOrderCreationHelper().createOrder(deliveryMode,
                "auto" + String.valueOf(orderNo), true,
                Integer.valueOf(00), Double.valueOf(deliveryFee), "WEB");
        return order;
    }

    /**
     * Method to save order and increment test order number
     * 
     * @param order
     */
    private void saveAndIncrementOrderNumber(final OrderModel order) {
        Order.setOrder(order);
        ServiceLookup.getModelService().save(order);
        orderNo++;
    }

}
