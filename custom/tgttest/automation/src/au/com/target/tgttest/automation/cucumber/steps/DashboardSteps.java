/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import java.util.List;

import org.junit.Assert;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.DashboardStats;
import au.com.target.tgtwsfacades.instore.dto.dashboard.ConsignmentStats;
import au.com.target.tgtwsfacades.instore.dto.dashboard.DashboardResponseData;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Steps definitions class for Dashboard feature.
 * 
 * @author Vivek
 *
 */
public class DashboardSteps {

    private static final String OPEN = "open";
    private static final String IN_PROGRESS = "in progress";
    private static final String PICKED = "picked";
    private static final String PACKED = "packed";
    private static final String COMPLETED = "completed";
    private static final String REJECTED = "rejected";
    private static final String TOTAL = "total";

    private Response response;

    @When("^todays dashboard function is run for store '(\\d+)'$")
    public void whenRunTodaysDashboardFunction(final Integer storeNo) {
        response = ServiceLookup.getTargetInStoreIntegrationFacade().getConsignmentsByStatusForToday(storeNo);
    }

    @When("^yesterdays dashboard function is run for store '(\\d+)'$")
    public void whenRunYesterdaysDashboardFunction(final Integer storeNo) {
        response = ServiceLookup.getTargetInStoreIntegrationFacade().getConsignmentsByStatusForYesterday(storeNo);
    }

    @Then("^todays dashboard stats are:$")
    public void thenTodaysDashboardStats(final List<DashboardStats> todayStats) {
        final ConsignmentStats consignmentStats = ((DashboardResponseData)response.getData()).getConsignmentStats();
        int totalCount = 0;
        for (final DashboardStats dashboardStats : todayStats) {
            if (StringUtils.equalsIgnoreCase(dashboardStats.getDashboardStatus(), OPEN)) {
                Assert.assertEquals(dashboardStats.getTotal(), consignmentStats.getOpen().intValue());
                totalCount += consignmentStats.getOpen().intValue();
            }
            else if (StringUtils.equalsIgnoreCase(dashboardStats.getDashboardStatus(), IN_PROGRESS)) {
                Assert.assertEquals(dashboardStats.getTotal(), consignmentStats.getInProgress().intValue());
                totalCount += consignmentStats.getInProgress().intValue();
            }
            else if (StringUtils.equalsIgnoreCase(dashboardStats.getDashboardStatus(), PICKED)) {
                Assert.assertEquals(dashboardStats.getTotal(), consignmentStats.getPicked().intValue());
                totalCount += consignmentStats.getPicked().intValue();
            }
            else if (StringUtils.equalsIgnoreCase(dashboardStats.getDashboardStatus(), COMPLETED)) {
                Assert.assertEquals(dashboardStats.getTotal(), consignmentStats.getCompleted().intValue());
                totalCount += consignmentStats.getCompleted().intValue();
            }
            else if (StringUtils.equalsIgnoreCase(dashboardStats.getDashboardStatus(), REJECTED)) {
                Assert.assertEquals(dashboardStats.getTotal(), consignmentStats.getRejected().intValue());
                totalCount += consignmentStats.getRejected().intValue();
            }
            else if (StringUtils.equalsIgnoreCase(dashboardStats.getDashboardStatus(), PACKED)) {
                Assert.assertEquals(dashboardStats.getTotal(), consignmentStats.getPacked().intValue());
                totalCount += consignmentStats.getPacked().intValue();
            }
            else if (StringUtils.equalsIgnoreCase(dashboardStats.getDashboardStatus(), TOTAL)) {
                Assert.assertEquals(dashboardStats.getTotal(), totalCount);
            }
        }
    }

    @Then("^yesterdays dashboard stats are:$")
    public void thenYesterdaysDashboardStats(final List<DashboardStats> yesterdayStats) {
        final ConsignmentStats consignmentStats = ((DashboardResponseData)response.getData()).getConsignmentStats();
        int totalCount = 0;
        for (final DashboardStats dashboardStats : yesterdayStats) {
            if (StringUtils.equalsIgnoreCase(dashboardStats.getDashboardStatus(), OPEN)) {
                Assert.assertNull(consignmentStats.getOpen());
            }
            else if (StringUtils.equalsIgnoreCase(dashboardStats.getDashboardStatus(), IN_PROGRESS)) {
                Assert.assertNull(consignmentStats.getInProgress());
            }
            else if (StringUtils.equalsIgnoreCase(dashboardStats.getDashboardStatus(), PICKED)) {
                Assert.assertNull(consignmentStats.getPicked());
            }
            else if (StringUtils.equalsIgnoreCase(dashboardStats.getDashboardStatus(), COMPLETED)) {
                Assert.assertEquals(dashboardStats.getTotal(), consignmentStats.getCompleted().intValue());
                totalCount += consignmentStats.getCompleted().intValue();
            }
            else if (StringUtils.equalsIgnoreCase(dashboardStats.getDashboardStatus(), REJECTED)) {
                Assert.assertEquals(dashboardStats.getTotal(), consignmentStats.getRejected().intValue());
                totalCount += consignmentStats.getRejected().intValue();
            }
            else if (StringUtils.equalsIgnoreCase(dashboardStats.getDashboardStatus(), TOTAL)) {
                Assert.assertEquals(dashboardStats.getTotal(), totalCount);
            }
        }
    }

}
