/**
 *
 */
package au.com.target.tgttest.automation;

import de.hybris.platform.core.Registry;
import de.hybris.platform.jalo.JaloConnection;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.JaloSystemNotInitializedException;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.util.Utilities;

import org.apache.log4j.Logger;

import au.com.target.tgttest.automation.facade.DealUtil;
import au.com.target.tgttest.automation.facade.FeatureSwitchUtil;
import au.com.target.tgttest.automation.facade.ProductUtil;
import au.com.target.tgttest.automation.facade.SalesApplicationUtil;
import au.com.target.tgttest.automation.facade.SessionUtil;


/**
 * Created with reference to PlatformRunListener. <br/>
 * Adds capability to activate master or junit tenant in standalone mode.
 * 
 * Using global state allows for global hooks - not very nice but see
 * http://zsoltfabok.com/blog/2012/09/cucumber-jvm-hooks/
 * 
 */
public class AutomationPlatformRunner {

    // Platform and logger should be started on first test
    private static Logger log;
    private static boolean isRunning = false;

    // Session user is ANONYMOUS by default
    public enum SessionUser {
        ANONYMOUS, ADMIN, ESBPRICEUPDATE, ESB, ESBONLINE, DEV, DEALSAUTHOR, ESBSTLUPDATE
    }


    /**
     * Call at start of each scenario.<br/>
     * Check for suiteInitialisation which is run just the first time.
     * 
     * @throws Exception
     */
    public void testStarted(final SessionUser userType) throws Exception {
        // On first test in suite start the platform with master tenant
        if (!isRunning) {
            suiteInitialisation();
        }

        scenarioInitialisation(userType);
    }


    /**
     * Initialisation per scenario
     * 
     * @throws JaloSecurityException
     * 
     */
    private void scenarioInitialisation(final SessionUser userType) throws JaloSecurityException {

        SessionUtil.createGuestSession();

        if (userType == SessionUser.ADMIN) {
            SessionUtil.setAdminUserSession();
        }
        else if (userType == SessionUser.ESBPRICEUPDATE) {
            SessionUtil.userLogin("esbpriceupdate");
        }
        else if (userType == SessionUser.ESB) {
            SessionUtil.userLogin("esb");
        }
        else if (userType == SessionUser.ESBONLINE) {
            SessionUtil.userLogin("esbonline");
        }
        else if (userType == SessionUser.DEV) {
            JaloSession.deactivate();
            SessionUtil.userLogin("dev1");
        }
        else if (userType == SessionUser.DEALSAUTHOR) {
            SessionUtil.userLogin("dealsauthor");
        }
        else if (userType == SessionUser.ESBSTLUPDATE) {
            SessionUtil.userLogin("esbstlupdate");
        }

        TestInitialiser.resetDomainObjects();
        TestInitialiser.resetMocks();

        SessionUtil.initializeSite();
        SessionUtil.setSessionPurchaseOption();
        SessionUtil.setSessionCurrency();
        SalesApplicationUtil.resetSalesApplicationToDefault();
        FeatureSwitchUtil.setDefaultFeatureSwitches();
    }


    /**
     * Pre-suite initialisation (run just once at start of suite)
     */
    private void suiteInitialisation() {

        try {
            startStandaloneHybris(false);

            log = Logger.getLogger(AutomationPlatformRunner.class.getName());

            setupSuiteTestData();

            // These mocks will be in place for the duration of the suite!
            TestInitialiser.setMocks();
        }
        catch (final Exception e) {

            // If there is an exception in the suite setup we should log and abort all
            logError("Exception in suiteInitialisation, aborting.", e);
            System.exit(1);
        }
    }


    /**
     * Start Hybris standalone context
     * 
     * @param junit
     *            - true to activate JUnit tenant, false to activate Master tenant
     */
    private void startStandaloneHybris(final boolean junit) {
        Registry.activateStandaloneMode();
        if (junit) {
            Utilities.setJUnitTenant();
        }
        else {
            Registry.activateMasterTenant();
        }

        logInfo("Setting Cluster and Tenant, mode=" + (junit ? "junit" : "master"));

        // This check copied from PlatformRunListener
        final JaloConnection con = JaloConnection.getInstance();
        final boolean sysIni = con.isSystemInitialized();
        if (!sysIni) {
            final JaloSystemNotInitializedException e = new JaloSystemNotInitializedException(null,
                    "Test system is not initialized", -1);
            final StackTraceElement[] trimmedStack = new StackTraceElement[Math.min(e.getStackTrace().length, 3)];
            System.arraycopy(e.getStackTrace(), 0, trimmedStack, 0, trimmedStack.length);
            e.setStackTrace(trimmedStack);
            throw e;
        }

        isRunning = true;

    }


    /**
     * Call after each test to close the jaloSession
     * 
     */
    public void testFinished() {
        SessionUtil.removeSession();
    }


    /**
     * Set up test data for the entire suite
     */
    private void setupSuiteTestData() {

        ProductUtil.initialise();
        DealUtil.initialise();
    }


    /**
     * Log an info message
     * 
     * @param message
     */
    public void logInfo(final String message) {
        // ystem.out.println(message);
        if (log != null) {
            log.info(message);
        }
    }

    /**
     * Log an error message
     * 
     * @param message
     */
    public void logError(final String message, final Exception e) {
        // ystem.out.println("ERROR: " + message);
        if (log != null) {
            log.error(message, e);
        }
    }

}
