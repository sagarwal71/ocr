/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import au.com.target.tgtfacades.deals.data.TargetVouchersData;
import au.com.target.tgtfacades.offers.data.TargetMobileOfferHeadingData;
import au.com.target.tgtfacades.voucher.data.TargetPromotionVoucherData;
import au.com.target.tgttest.automation.ImpexImporter;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.MobileVoucherParameters;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Steps for setting up Mobile deals
 * 
 */
public class MobileVoucherSteps {

    private TargetVouchersData targetVouchersData;


    @Given("^the vouchers are setup in the system running the mentioned impex$")
    public void givenTheVouchersAreSetup() {
        ImpexImporter.importCsv("/tgttest/automation-impex/voucher/vouchers-mobile-auto-test.impex");
        ImpexImporter.importCsv("/tgttest/automation-impex/voucher/vouchers-mobile-auto-test-enable.impex");
    }

    @When("^the vouchers applicable for mobile are being fetched$")
    public void fetchVouchersForMobile() {
        targetVouchersData = ServiceLookup.getTargetVoucherFacade()
                .getAllMobileActivePromotionVouchersWithOfferHeadings();
    }

    @Then("^the retrieved data will contain the following vouchers:$")
    public void fetchedVouchersForMobileWillContainThePassedVouchers(
            final List<MobileVoucherParameters> mobileVoucherParametersList) {
        assertThat(targetVouchersData).isNotNull();
        final List<TargetPromotionVoucherData> targetPromotionVoucherDataList = targetVouchersData
                .getTargetPromotionVouchers();
        assertThat(targetPromotionVoucherDataList).isNotNull();
        final List<String> voucherIdsFound = new ArrayList<>();
        for (final TargetPromotionVoucherData targetPromotionVoucherData : targetPromotionVoucherDataList) {
            for (final MobileVoucherParameters mobileVoucherParameters : mobileVoucherParametersList) {
                assertThat(targetPromotionVoucherData).isNotNull();
                assertThat(targetPromotionVoucherData.getCode()).isNotNull();
                if (targetPromotionVoucherData.getCode().equals(mobileVoucherParameters.getVoucherId())) {
                    voucherIdsFound.add(targetPromotionVoucherData.getCode());
                    assertThat(mobileVoucherParameters.getAvailableForMobile()).isEqualTo(
                            targetPromotionVoucherData.getAvailableForMobile());
                    assertThat(mobileVoucherParameters.getLongTitle()).isEqualTo(
                            targetPromotionVoucherData.getDescription());
                    assertThat(mobileVoucherParameters.getEnabledOnline()).isEqualTo(
                            targetPromotionVoucherData.getEnabledOnline());
                    assertThat(mobileVoucherParameters.getPageTitle()).isEqualTo(targetPromotionVoucherData.getName());
                    assertThat(mobileVoucherParameters.getBarcode()).isEqualTo(targetPromotionVoucherData.getBarcode());
                    final List<String> mobileOfferHeadingCodeExpected = mobileVoucherParameters
                            .getOfferHeadingsAsList();
                    final List<TargetMobileOfferHeadingData> targetMobileOfferHeadingsResultList = targetPromotionVoucherData
                            .getTargetMobileOfferHeadings();
                    assertThat(targetMobileOfferHeadingsResultList).isNotEmpty();
                    for (final TargetMobileOfferHeadingData targetMobileOfferHeading : targetMobileOfferHeadingsResultList) {
                        assertThat(mobileOfferHeadingCodeExpected).contains(targetMobileOfferHeading.getCode());
                        assertThat(targetMobileOfferHeading.getCode()).isNotNull();
                        assertThat(targetMobileOfferHeading.getColour()).isNotNull();
                        assertThat(targetMobileOfferHeading.getName()).isNotNull();
                    }
                    assertThat(targetPromotionVoucherData.getStartTimeFormatted()).isNotNull();
                    assertThat(targetPromotionVoucherData.getCreatedTimeFormatted()).isNotNull();
                }
            }
        }
        for (final MobileVoucherParameters mobileVoucherParameters : mobileVoucherParametersList) {
            assertThat(voucherIdsFound).contains(mobileVoucherParameters.getVoucherId());
        }
    }

    @Then("^will not contain the following vouchers with id:$")
    public void fetchedDealsForMobileWillNotContainThePassedDeals(
            final List<String> mobileVoucherIdList) {
        assertThat(targetVouchersData).isNotNull();
        final List<TargetPromotionVoucherData> targetPromotionVoucherDataList = targetVouchersData
                .getTargetPromotionVouchers();
        assertThat(targetPromotionVoucherDataList).isNotNull();
        for (final TargetPromotionVoucherData targetPromotionVoucherData : targetPromotionVoucherDataList) {
            for (final String voucherId : mobileVoucherIdList) {
                assertThat(targetPromotionVoucherData).isNotNull();
                assertThat(targetPromotionVoucherData.getCode()).isNotNull();
                if (targetPromotionVoucherData.getCode().equals(voucherId)) {
                    Assert.fail("Voucher which should not get retrieved found in response"
                            + targetPromotionVoucherData.getCode());
                }
            }
        }
    }

}
