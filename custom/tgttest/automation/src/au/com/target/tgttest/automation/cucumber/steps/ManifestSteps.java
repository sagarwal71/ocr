/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static java.lang.Boolean.valueOf;
import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.fest.assertions.Assertions;
import org.junit.Assert;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;
import au.com.target.tgtfulfilment.model.TargetManifestModel;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.BusinessProcessUtil;
import au.com.target.tgttest.automation.facade.bean.ConsignmentForStore;
import au.com.target.tgttest.automation.facade.bean.HardCopyData;
import au.com.target.tgttest.automation.facade.bean.HardCopyManifestData;
import au.com.target.tgttest.automation.facade.bean.ManifestForStore;
import au.com.target.tgttest.automation.util.ComplexDateExpressionParser;
import au.com.target.tgtwsfacades.instore.dto.auspost.Address;
import au.com.target.tgtwsfacades.instore.dto.auspost.ConsignmentSummary;
import au.com.target.tgtwsfacades.instore.dto.consignments.Consignment;
import au.com.target.tgtwsfacades.instore.dto.manifests.Manifest;
import au.com.target.tgtwsfacades.instore.dto.manifests.ManifestHardCopyResponseData;
import au.com.target.tgtwsfacades.instore.dto.manifests.ManifestsResponseData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author Vivek
 *
 */
public class ManifestSteps {

    private int storeNumber;

    private Response transmitResponse;

    private Response response;




    @Given("^store number for ofc portal is '(\\d+)'$")
    public void givenStoreForOFCPortal(final int storeNo) {
        storeNumber = storeNo;
    }

    @Given("^empty list of manifests for the store$")
    public void givenEmptyManifestsList() {
        ServiceLookup.getManifestCreationHelper().removeExistingManifests();
    }

    @Given("^list of manifests for the store is:$")
    public void givenManifestsList(final List<ManifestForStore> manifests) {
        ServiceLookup.getManifestCreationHelper().createManifestsForStore(manifests);
        setupResponsesForTransmission(manifests);
    }

    /**
     * @param manifests
     * 
     */
    private void setupResponsesForTransmission(final List<ManifestForStore> manifests) {
        final Map<String, String> expectedResponsesForManifest = new HashMap<String, String>();
        for (final ManifestForStore manifest : manifests) {
            if (StringUtils.isNotEmpty(manifest.getWebmethodsResponse())) {
                expectedResponsesForManifest.put(manifest.getManifestID(), manifest.getWebmethodsResponse());
            }
        }
        ServiceLookup.getTransmitManifestClient().setExpectedResponsesForManifest(expectedResponsesForManifest);
    }

    @Given("^list of consignments for the store is:$")
    public void givenListOfConsignmentsForStores(final List<ConsignmentForStore> entries)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, DuplicateUidException {
        ServiceLookup.getConsignmentCreationHelper().createConsignmentsForStore(entries, Integer.valueOf(storeNumber),
                "WEB");
    }

    @Given("^manifest history days is set to '(\\d+)'$")
    public void givenManifestHistoryDays(final int historyDays) {
        ServiceLookup.getTargetManifestDao().setManifestHistoryDays(historyDays);
    }

    @When("^ofc transmit function is run$")
    public void whenTransmitManifest() {

        // Transmit response has basic manifest details only
        transmitResponse = ServiceLookup.getTargetInStoreIntegrationFacade().transmitManifestForStore(storeNumber);

        // Retrieve the manifest to get the full manifest in the response
        response = ServiceLookup.getTargetInStoreIntegrationFacade().getManifestByCode(
                getManifestFromTransmitResponse().getCode(),
                Integer.valueOf(storeNumber));
    }

    @When("^ofc un-manifested consignments function is run$")
    public void whenRunUnmanifestedConsignmentsFunctionForStore() {
        response = ServiceLookup.getTargetInStoreIntegrationFacade()
                .getConsignmentsNotManifestedForStore(storeNumber);
    }

    @When("^ofc manifest history function is run$")
    public void whenManifestHistoryRun() {
        response = ServiceLookup.getTargetInStoreIntegrationFacade().getManifestHistoryForStore(storeNumber);
    }

    @When("^user selects manifest '(.*)' to view$")
    public void whenManifestViewRun(final String manifestCode) {
        response = ServiceLookup.getTargetInStoreIntegrationFacade().getManifestByCode(manifestCode,
                Integer.valueOf(storeNumber));
    }

    @When("^retransmit of the manifests is initiated$")
    public void retransmitManifest() {
        ServiceLookup.getRetransmitManifestsJob().perform(null);
    }

    @Then("^the manifests after retransmit are:$")
    public void verifyManifestTransmitted(final List<ManifestForStore> manifests) throws NotFoundException {
        for (final ManifestForStore manifestData : manifests) {
            final TargetManifestModel manifestModel = ServiceLookup.getTargetManifestService().getManifestByCode(
                    manifestData.getManifestID());
            Assertions.assertThat(manifestModel).isNotNull();
            Assertions.assertThat(manifestModel.isSent()).isEqualTo(manifestData.isTransmitted());
        }
    }

    @When("^ofc retrieves hard copy manifest '(.*)' to view$")
    public void whenHardcopyManifestIsRequested(final String manifestCode) {
        response = ServiceLookup.getTargetInStoreIntegrationFacade().getHardCopyManifestData(manifestCode,
                Integer.valueOf(storeNumber));
    }

    @Then("^hard copy manifest has core details:$")
    public void hardCopyManifestCoreDetails(final List<HardCopyManifestData> hardCopyDataList) {
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getData());
        final ManifestHardCopyResponseData data = (ManifestHardCopyResponseData)response.getData();
        final HardCopyManifestData hardCopyData = hardCopyDataList.get(0);
        Assert.assertEquals(hardCopyData.getMlid(), data.getManifest().getMerchantLocationId());
        Assert.assertEquals(new ComplexDateExpressionParser().interpretStringAsDate(hardCopyData.getManifestDate())
                .toString(),
                new Date(Long.valueOf(data.getManifest().getManifestCreatedDate()).longValue()).toString());
        Assert.assertEquals(hardCopyData.getStoreAddress().trim(), getFormattedAddressAsString(data.getManifest()
                .getStoreAddress()));
        Assert.assertEquals(hardCopyData.getManifestId(), data.getManifest().getManifestNumber());
        Assert.assertEquals(Integer.valueOf(hardCopyData.getTotalArticles()), data.getManifest().getTotalArticles());
        Assert.assertEquals(Double.valueOf(hardCopyData.getTotalWeight()), data.getManifest().getTotalWeight());
    }

    @Then("^hard copy manifest has consignments:$")
    public void hardCopyManifestConsignments(final List<HardCopyData> hardCopyDataList) {
        final ManifestHardCopyResponseData data = (ManifestHardCopyResponseData)response.getData();
        Assert.assertNotNull(data);
        Assert.assertNotNull(data.getManifest());
        final List<ConsignmentSummary> consignmentSummaries = data.getManifest().getConsignmentSummaries();
        verifyConsignmentSummary(consignmentSummaries, hardCopyDataList);
    }

    @Then("^list of manifest consignments for the store is:$")
    public void verifyManifestedConsignmentsBasedOnManifestResponseDto(final List<ConsignmentForStore> expectedCons) {

        final List<Consignment> consignments = getConsignmentsFromResponse();
        Assert.assertEquals(expectedCons.size(), consignments.size());

        int correctEntries = 0;
        for (final ConsignmentForStore expectedCon : expectedCons) {
            for (final Consignment resultCon : consignments) {
                if (StringUtils.equals(expectedCon.getConsignmentCode(), resultCon.getCode())) {
                    Assert.assertEquals(expectedCon.getBoxes().intValue(), resultCon.getParcels().intValue());
                    Assert.assertEquals(expectedCon.getDestination(), resultCon.getDestination().getCity() + ", "
                            + resultCon.getDestination().getState());
                    Assert.assertEquals(expectedCon.getCustomer(), resultCon.getCustomer().getFirstName() + " "
                            + resultCon.getCustomer().getLastName());
                    assertPickDate(expectedCon, resultCon);
                    correctEntries++;
                }
            }
        }
        Assert.assertEquals(expectedCons.size(), correctEntries);
    }

    @Then("^list of consignments in generated manifest is:$")
    public void verifyManifestedConsignmentsInGeneratedManifest(final List<ConsignmentForStore> expectedCons)
            throws NotFoundException {

        final List<Consignment> manifestedConsignments = getConsignmentsFromResponse();
        Assert.assertEquals(expectedCons.size(), manifestedConsignments.size());

        int correctEntries = 0;
        for (final ConsignmentForStore expectedCon : expectedCons) {
            for (final Consignment resultCon : manifestedConsignments) {
                if (StringUtils.equals(expectedCon.getConsignmentCode(), resultCon.getCode())) {

                    Assert.assertEquals(expectedCon.getBoxes().intValue(), resultCon.getParcels().intValue());
                    Assert.assertEquals(expectedCon.getDestination(), resultCon.getDestination().getCity() + ", "
                            + resultCon.getDestination().getState());
                    Assert.assertEquals(expectedCon.getCustomer(), resultCon.getCustomer().getFirstName() + " "
                            + resultCon.getCustomer().getLastName());
                    assertPackDate(expectedCon, resultCon);

                    Assert.assertEquals(expectedCon.getOrderType(), resultCon.getDeliveryType());

                    correctEntries++;
                }
            }
        }
        Assert.assertEquals(expectedCons.size(), correctEntries);
    }

    @Then("^manifest history for the store is empty$")
    public void thenManifestHistoryEmpty() {

        Assert.assertTrue(CollectionUtils.isEmpty(getManifestsFromFullResponse()));
    }

    @Then("^manifest history for the store is:$")
    public void thenManifestHistoryContains(final List<ManifestForStore> expectedManifests) {

        final List<Manifest> actualManifests = getManifestsFromFullResponse();
        Assert.assertEquals(expectedManifests.size(), actualManifests.size());

        for (final ManifestForStore expManifest : expectedManifests) {
            for (final Manifest actManifest : actualManifests) {
                if (StringUtils.equalsIgnoreCase(expManifest.getManifestID(), actManifest.getCode())) {
                    final Calendar actualDate = Calendar.getInstance();
                    actualDate.setTimeInMillis(Long.valueOf(actManifest.getDate()).longValue());
                    Assert.assertTrue(DateUtils.isSameDay(ServiceLookup.getManifestCreationHelper()
                            .getDateObject(expManifest.getManifestDate(),
                                    expManifest.getManifestTime()),
                            actualDate.getTime()));
                }
            }
        }
    }

    @Then("^unique manifestId is generated$")
    public void verifyManifestId() throws NotFoundException {

        final Manifest basicManifest = getManifestFromTransmitResponse();

        // Verification on code - storenumber plus six digit number
        final String manifestCode = basicManifest.getCode();
        final String strStoreNumber = Integer.toString(storeNumber);
        Assert.assertNotNull(manifestCode);
        Assert.assertTrue(manifestCode.startsWith(strStoreNumber));
        Assert.assertTrue(manifestCode.length() == (strStoreNumber.length() + 6));

        final String stem = manifestCode.substring(strStoreNumber.length());
        Assert.assertTrue(StringUtils.isNumeric(stem));
    }

    @Then("^manifest dateTime stamp is current time$")
    public void verifyDateTimeStampOfManifest() {

        final Manifest basicManifest = getManifestFromTransmitResponse();
        Assert.assertTrue(DateUtils.isSameDay(Calendar.getInstance().getTime(), getManifestDate(basicManifest)));
    }

    @Then("^manifest date is '(.*)'$")
    public void verifyDateTimeStampOfManifest(final String dateString) {
        final String[] dateTime = dateString.split("at", 2);
        final Date expDate = ServiceLookup.getManifestCreationHelper().getDateObject(dateTime[0], dateTime[1]);

        DateUtils.isSameInstant(expDate, getManifestDate(getManifestFromFullResponse()));
    }

    @Then("^number of boxes in manifest is '(\\d+)'$")
    public void verifyNumberOfBoxesBasedOnManifestDto(final int boxes) {
        final Integer resultTotalParcels = getManifestFromFullResponse().getParcels();
        Assert.assertNotNull(resultTotalParcels);
        Assert.assertEquals(boxes, resultTotalParcels.intValue());
    }

    @Then("^number of parcels in generated manifest is '(\\d+)'$")
    public void verifyNumberOfBoxesInGeneratedManifest(final int boxes) throws NotFoundException {

        verifyNumberOfBoxesBasedOnManifestDto(boxes);
    }

    @Then("^the manifest sent flag is set to '(true|false)'$")
    public void verifyManifestSentFlag(final boolean expectedStatus) throws NotFoundException {

        // check the manifest in transmit response
        final Manifest basicManifest = getManifestFromTransmitResponse();
        Assert.assertEquals(valueOf(expectedStatus), valueOf(basicManifest.isSent()));

        // Check model for date transmitted
        final TargetManifestModel manifestModel = getGeneratedManifestModel();
        Assert.assertEquals(valueOf(expectedStatus), valueOf(manifestModel.isSent()));
        if (expectedStatus) {
            Assert.assertNotNull(manifestModel.getDateTransmitted());
        }
        else {
            Assert.assertNull(manifestModel.getDateTransmitted());
        }
    }

    @Then("^status of consignments in generated manifest is shipped$")
    public void verifyStatusOfManifestedConsignmentsIsShipped() throws NotFoundException, InterruptedException {

        final TargetManifestModel manifestModel = getGeneratedManifestModel();
        final Set<TargetConsignmentModel> consignments = manifestModel.getConsignments();
        for (final TargetConsignmentModel conModel : consignments) {
            BusinessProcessUtil.waitForAllOrderProcessesToFinish((OrderModel)conModel.getOrder());
            Assert.assertEquals(ConsignmentStatus.SHIPPED, conModel.getStatus());
        }
    }

    @Then("^status of orders in generated manifest is '(.*)'$")
    public void verifyStatusOfManifestedConsignmentOrders(final String orderStatus) throws NotFoundException,
            InterruptedException {

        final TargetManifestModel manifestModel = getGeneratedManifestModel();
        final Set<TargetConsignmentModel> consignments = manifestModel.getConsignments();
        for (final TargetConsignmentModel conModel : consignments) {
            final AbstractOrderModel order = conModel.getOrder();
            Assert.assertNotNull(order);
            BusinessProcessUtil.waitForAllOrderProcessesToFinish((OrderModel)order);
            assertThat(order.getStatus().toString()).isEqualToIgnoringCase(orderStatus);
        }
    }

    @Then("^list of un-manifested consignments for the store is:$")
    public void thenListOfunmanifestedConsignments(final List<ConsignmentForStore> expectedCons) {

        final List<Consignment> consignments = getConsignmentsFromResponse();
        Assert.assertEquals(expectedCons.size(), consignments.size());
        int correctEntries = 0;
        for (final ConsignmentForStore expectedCon : expectedCons) {
            for (final Consignment resultCon : consignments) {
                if (StringUtils.equals(expectedCon.getConsignmentCode(), resultCon.getCode())) {
                    Assert.assertEquals(resultCon.getDeliveryType(), expectedCon.getOrderType());
                    correctEntries++;
                }
            }
        }
        Assert.assertEquals(expectedCons.size(), correctEntries);
    }

    @Then("^list of un-manifested consignments for the store is empty$")
    public void thenUnmanifestedConsignmentsIsEmpty() {
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getData());
        final ManifestsResponseData manifestsResponseData = (ManifestsResponseData)response.getData();
        manifestsResponseData.getManifests();
        Assert.assertNotNull(manifestsResponseData);
        Assert.assertEquals(0, manifestsResponseData.getManifests().size());
    }


    private void assertPickDate(final ConsignmentForStore expectedCon, final Consignment resultCon) {

        compareDates(expectedCon.getPickDate(), resultCon.getPickDate());
    }

    private void assertPackDate(final ConsignmentForStore expectedCon, final Consignment resultCon) {

        compareDates(expectedCon.getPackDate(), resultCon.getPackDate());
    }

    private void compareDates(final String expectedExpression, final String actualMillis) {

        final ComplexDateExpressionParser dateUtil = new ComplexDateExpressionParser();
        final Date expectedDate = dateUtil.interpretStringAsDate(expectedExpression);
        final Calendar actualDate = Calendar.getInstance();
        actualDate.setTimeInMillis(Long.valueOf(actualMillis).longValue());

        Assert.assertTrue(DateUtils.isSameDay(actualDate.getTime(), expectedDate));
    }

    private Date getManifestDate(final Manifest man) {
        final Calendar calendarActual = Calendar.getInstance();
        calendarActual.setTimeInMillis(Long.valueOf(man.getDate()).longValue());
        return calendarActual.getTime();
    }

    private Manifest getManifestFromFullResponse() {

        return getManifestFromResponse(response);
    }

    private Manifest getManifestFromTransmitResponse() {

        return getManifestFromResponse(transmitResponse);
    }

    private List<Manifest> getManifestsFromFullResponse() {
        Assert.assertNotNull(response);
        Assert.assertTrue(response.getData() instanceof ManifestsResponseData);

        final ManifestsResponseData manifestsResponseData = (ManifestsResponseData)response.getData();
        Assert.assertNotNull(manifestsResponseData);

        return manifestsResponseData.getManifests();
    }

    private Manifest getManifestFromResponse(final Response aResponse) {

        final ManifestsResponseData manifestsResponseData = (ManifestsResponseData)aResponse.getData();
        Assert.assertNotNull(manifestsResponseData);

        final List<Manifest> manifests = manifestsResponseData.getManifests();
        Assert.assertEquals(1, manifests.size());

        return manifests.get(0);
    }

    private List<Consignment> getConsignmentsFromResponse() {

        final Manifest manifest = getManifestFromFullResponse();
        Assert.assertNotNull(manifest);

        return manifest.getConsignments();
    }

    private TargetManifestModel getGeneratedManifestModel() throws NotFoundException {

        final Manifest manifest = getManifestFromTransmitResponse();
        Assert.assertNotNull(manifest);

        final TargetManifestModel manifestModel = ServiceLookup.getTargetManifestService().getManifestByCode(
                manifest.getCode());
        Assert.assertNotNull(manifestModel);
        return manifestModel;
    }

    private String getFormattedAddressAsString(final Address storeAddress) {
        Assert.assertNotNull(storeAddress);
        final StringBuilder addressString = new StringBuilder();
        addressString.append(storeAddress.getAddressLine1()).append(", ");
        if (storeAddress.getAddressLine2() != null) {
            addressString.append(storeAddress.getAddressLine2()).append(", ");
        }
        addressString.append(storeAddress.getSuburb()).append(", ");
        addressString.append(storeAddress.getState()).append(", ");
        addressString.append(storeAddress.getPostcode());
        return addressString.toString();
    }

    private void verifyConsignmentSummary(final List<ConsignmentSummary> consignmentSummaries,
            final List<HardCopyData> hardCopyDataList) {
        Assert.assertNotNull(consignmentSummaries);
        Assert.assertEquals(consignmentSummaries.size(), hardCopyDataList.size());
        for (int index = 0; index < consignmentSummaries.size(); index++) {
            final ConsignmentSummary summary = consignmentSummaries.get(index);
            final HardCopyData hardCopyData = hardCopyDataList.get(index);
            Assert.assertEquals(summary.getChargeCode(), hardCopyData.getChargeCode());
            Assert.assertEquals(summary.getConsignmentId(), hardCopyData.getConsignmentId());
            Assert.assertEquals(summary.getTotalArticles().toString(), hardCopyData.getArticles());
            Assert.assertEquals(summary.getTotalWeight().toString(), hardCopyData.getWeight());
            Assert.assertEquals(summary.getChargeZone().getCode(), hardCopyData.getChargeZone());
            Assert.assertEquals(summary.getChargeZone().getDescription(), hardCopyData.getChargeZoneDescription());
            Assert.assertEquals(summary.getDeliveryAddress().getName(), hardCopyData.getDeliveryName());
        }
    }

}
