/**
 *
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemResponseDto;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.InStoreStockUtil;
import au.com.target.tgttest.automation.facade.ProductUtil;
import au.com.target.tgttest.automation.facade.StoreFulfilmentCapabilitiesUtil;
import au.com.target.tgttest.automation.facade.bean.ProductPartialData;
import au.com.target.tgttest.automation.facade.domain.Order;
import au.com.target.tgttest.automation.util.ComplexDateExpressionParser;
import au.com.target.tgttest.automation.util.ProductQuantityParser;
import au.com.target.tgttest.automation.util.StoreNumberResolver;
import au.com.target.tgttinker.mock.instore.ProductData;
import au.com.target.tgttinker.mock.instore.StoreStockData;
import au.com.target.tgtwsfacades.instore.dto.consignments.ProductStockLevelInstore;
import au.com.target.tgtwsfacades.instore.dto.response.data.ProductSohResponseData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;


/**
 * @author smudumba
 *
 */
public class StoreStockSteps {

    private boolean stockStatus;

    private Date committedProductCountStartTime;

    private int committedProductCountForStoreResult;

    private Response response;

    private String productCode;

    private List<String> colorVariants;

    private List<String> sizeVariants;

    private Integer storeId;

    @Given("^stores with stock:$")
    public void givenStoresStock(final List<StoreStockData> ssdtos) {

        for (final StoreStockData ssdto : ssdtos) {
            setProductsFromStock(ssdto);
        }
        ServiceLookup.getTargetStockUpdateClient().populateStoreStock(ssdtos);

        ServiceLookup.getStockVisibilityClientMock()
                .setStockVisibilityItemResponseDtoList(setupStockInCache(ssdtos));
    }

    private List<StockVisibilityItemResponseDto> setupStockInCache(final List<StoreStockData> ssdtos) {
        final List<StockVisibilityItemResponseDto> stockVisibilityItemResponseDtoList = new ArrayList<>();

        for (final StoreStockData ssdto : ssdtos) {
            for (final ProductData pdtData : ssdto.getProducts()) {
                final StockVisibilityItemResponseDto responseItemDto = new StockVisibilityItemResponseDto();
                responseItemDto.setCode(pdtData.getCode());
                responseItemDto.setSoh(pdtData.getQuantity());
                responseItemDto.setStoreNumber(ssdto.getStore());
                stockVisibilityItemResponseDtoList.add(responseItemDto);
            }
        }
        return stockVisibilityItemResponseDtoList;
    }

    @Given("^store SOH buffer is reset for '(\\d+)'")
    public void setStoreStockBufferToNull(final Integer storeNumber) {
        final TargetPointOfServiceModel targetPointOfServiceModel;
        try {
            targetPointOfServiceModel = ServiceLookup.getTargetPointOfServiceService()
                    .getPOSByStoreNumber(storeNumber);
            final StoreFulfilmentCapabilitiesModel storeFulfilmentCapabilitiesModel = targetPointOfServiceModel
                    .getFulfilmentCapability();
            if (null != storeFulfilmentCapabilitiesModel) {
                storeFulfilmentCapabilitiesModel.setBufferStock(null);
                ServiceLookup.getModelService().save(storeFulfilmentCapabilitiesModel);
            }
        }
        catch (TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
            // its ok if the targetPointOfServiceModel doesn't exist for a given storeNo
        }
    }

    @Given("^store SOH buffer is set to '(\\d+)' for '(\\d+)'$")
    public void setStoreStockBufferWithValue(final Long globalBuffer, final Integer storeNumber) {
        final TargetPointOfServiceModel targetPointOfServiceModel;
        try {
            targetPointOfServiceModel = ServiceLookup.getTargetPointOfServiceService()
                    .getPOSByStoreNumber(storeNumber);
            final StoreFulfilmentCapabilitiesModel storeFulfilmentCapabilitiesModel = targetPointOfServiceModel
                    .getFulfilmentCapability();
            if (null != storeFulfilmentCapabilitiesModel) {
                storeFulfilmentCapabilitiesModel.setBufferStock(globalBuffer);
                ServiceLookup.getModelService().save(storeFulfilmentCapabilitiesModel);
            }
        }
        catch (TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
            // its ok if the targetPointOfServiceModel doesn't exist for a given storeNo
        }
    }

    @Given("^committed product count start time is '(.*)'$")
    public void setCommittedProductCountStartTime(final String startTime) {

        committedProductCountStartTime = new ComplexDateExpressionParser().interpretStringAsDate(startTime);
    }

    @Given("^product type '(.*)' restricted for store '(.*)'$")
    public void setProductTypeRestrictionForStore(final String productType, final String store) throws Exception {
        final Integer storeNumber = StoreNumberResolver.getStoreNumber(store);
        final TargetPointOfServiceModel storeModel = ServiceLookup.getTargetPointOfServiceService()
                .getPOSByStoreNumber(storeNumber);
        StoreFulfilmentCapabilitiesUtil.setExclusionProductType(productType, storeModel);
    }

    @When("^SOH check is performed for (\\d+)$")
    public void whenSOHIsPerformed(final Integer store) throws Exception {
        ServiceLookup.getTargetBusinessProcessService().setActive(true);
        submitOrderForSOH();
        final OrderModel order = Order.getInstance().getOrderModel();
        stockStatus = ServiceLookup.getTargetStoreStockService().isOrderEntriesInStockAtStore(order.getEntries(),
                store, order.getCode());
    }

    @When("^the stock levels for the consignment is checked for store '(.*)'$")
    public void whenSOHIsPerformedForConsignment(final Integer store) {
        final OrderModel order = Order.getInstance().getOrderModel();
        final Set<ConsignmentModel> cons = order.getConsignments();
        Assert.assertTrue(cons.size() == 1);
        response = ServiceLookup.getTargetInStoreIntegrationFacade()
                .getStockLevelsForProductsInConsignment(cons.iterator().next().getCode(), store);
    }

    @When("^get committed product count for store '(.*)' for product '(.*)'$")
    public void getCommittedProductCountForStore(final String store, final String product) {

        final Integer storeNumber = StoreNumberResolver.getStoreNumber(store);
        final ProductModel productModel = ProductUtil.getOnlineProductModel(product);
        assertThat(productModel).isInstanceOf(AbstractTargetVariantProductModel.class);
        committedProductCountForStoreResult = ServiceLookup.getTargetConsignmentDao()
                .getQuantityAssignedToStoreForProduct(storeNumber, (AbstractTargetVariantProductModel)productModel,
                        committedProductCountStartTime);
    }

    @Then("^the result of the store SOH check is (.*)$")
    public void verifySOHResult(final Boolean value) {
        assertThat(stockStatus).isEqualTo(value.booleanValue());
    }

    @Then("^committed product count is (\\d+)$")
    public void verifyCommittedProductCount(final int count) {
        assertThat(committedProductCountForStoreResult).isNotNull().isEqualTo(count);
    }

    @Then("^the stock levels at the store is:$")
    public void verifyStoreStockLevelsForConsignment(final List<ProductPartialData> productsExpected) {
        final BaseResponseData responseData = response.getData();
        Assert.assertTrue(responseData instanceof ProductSohResponseData);
        final List<ProductStockLevelInstore> products = ((ProductSohResponseData)responseData).getProducts();
        int productsMatched = 0;
        for (final ProductStockLevelInstore productResponse : products) {
            for (final ProductPartialData productExpected : productsExpected) {
                if (productExpected.getProductCode().equals(productResponse.getProductCode())) {
                    productsMatched++;
                    assertThat(productResponse.getStockOnHand()).isEqualTo(productExpected.getStockLevel());
                    assertThat(productResponse.getStockLevel()).isEqualTo(productExpected.getStockStatus());
                    break;
                }
            }
        }
        Assert.assertEquals(productsExpected.size(), productsMatched);
    }

    private void submitOrderForSOH() throws Exception {
        final OrderModel orderModel;
        orderModel = CheckoutUtil.placeOrder();
        Order.setOrder(orderModel);
    }

    private void setProductsFromStock(final StoreStockData storeData) {

        final String stockEntry = storeData.getStock();
        storeData.setProducts(ProductQuantityParser.parseProductList(stockEntry));
    }

    @Given("^that Product '(.*)' has color variants as '(.*)' and sizes as '(.*)'$")
    public void theFollowingProductsAreConfigured(final String product, final List<String> colorVariantsCode,
            final List<String> sizeVariantsCode) {
        this.productCode = product;
        this.colorVariants = colorVariantsCode;
        this.sizeVariants = sizeVariantsCode;
    }

    @Given("^customer has a preferred store set (.*)$")
    public void thePreferredStore(final Integer store) {
        this.storeId = store;
    }

    @When("^preferred store stock of the sellable variant in cache db are:$")
    public void theFollowingStoreInEachStore(final List<StockVisibilityItemResponseDto> stockLevels) {
        InStoreStockUtil.enableMocks(stockLevels);
    }

    @When("^stock threshold config are set to limitedStockThreshold:(\\d+),noStockThreshold:(\\d+)$")
    public void theFollowingSharedConfigurations(final int limitedStockThreshold, final int noStockThreshold) {
        final Map<String, String> configEntries = new HashMap<>();
        configEntries.put("inStoreStockVisibility.limitedStockThreshold", String.valueOf(limitedStockThreshold));
        configEntries.put("inStoreStockVisibility.noStockThreshold", String.valueOf(noStockThreshold));
        ServiceLookup.getTargetSharedConfigService().setSharedConfigs(configEntries);
    }

    @Then("^the stock indication on store tab in PDP should be '(.*)'$")
    public void theStockMessageWillBe(final List<String> storeMessage) {
        final List<de.hybris.platform.commercefacades.product.data.ProductData> responseProductData = ProductUtil
                .populateAllSellableVariantsWithStoreStockAndOptions(productCode, storeId);
        assertThat(responseProductData).isNotEmpty();
        int index = 0;
        for (final de.hybris.platform.commercefacades.product.data.ProductData productData : responseProductData) {
            final TargetProductData targetProductData = (TargetProductData)productData;
            assertThat(targetProductData.getBaseProductCode()).isEqualTo(productCode);
            assertThat(colorVariants).contains(targetProductData.getColourVariantCode());
            assertThat(targetProductData.getCode()).isEqualTo(sizeVariants.get(index));
            assertThat(targetProductData.getStock()).isNotNull();
            assertThat(targetProductData.getStock().getStockLevelStatus()).isNotNull();
            assertThat(targetProductData.getStock().getStockLevelStatus().getCode()).isEqualTo(storeMessage.get(index));
            index++;
        }
    }

}
