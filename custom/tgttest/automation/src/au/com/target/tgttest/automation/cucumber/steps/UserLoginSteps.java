/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;

import org.fest.assertions.Assertions;

import au.com.target.tgtcore.customer.ReusePasswordException;
import au.com.target.tgtcore.customer.TargetBcryptPasswordEncoder;
import au.com.target.tgtcore.customer.TargetMD5PasswordEncoder;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.user.data.TargetRegisterData;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.SessionUtil;
import au.com.target.tgttest.automation.facade.bean.UserLoginData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author rmcalave
 *
 */
public class UserLoginSteps {
    private static final String BAD_PASSWORD = "password";

    private final UserService userService = ServiceLookup.getUserService();
    private final TargetCustomerFacade targetCustomerFacade = ServiceLookup.getTargetCustomerFacade();
    private final TargetBcryptPasswordEncoder targetBcryptPasswordEncoder = ServiceLookup
            .getTargetBcryptPasswordEncoder();
    private final TargetMD5PasswordEncoder targetMd5PasswordEncoder = ServiceLookup.getTargetMd5PasswordEncoder();
    private final ModelService modelService = ServiceLookup.getModelService();

    private UserLoginData userLoginData;
    private UserModel userModel;
    private boolean loginSuccess;
    private String resetToken;

    private ReusePasswordException reusePasswordException;

    @Given("^a user with uid '(.*)' does not exist$")
    public void aUserWithUidDoesNotExist(final String username) {
        try {
            userService.getUserForUID(username);
            fail("Expected no users with uid " + username + " to exist.");
        }
        catch (final UnknownIdentifierException ex) {
            // Do nothing
        }
    }

    @When("^the user registers with the following details:$")
    public void theUserRegistersWithTheFollowingDetails(final List<UserLoginData> data) throws DuplicateUidException {
        assertThat(data).hasSize(1);
        userLoginData = data.get(0);

        final TargetRegisterData registerData = new TargetRegisterData();
        registerData.setLogin(userLoginData.getUsername());
        registerData.setPassword(userLoginData.getPassword());
        registerData.setFirstName(userLoginData.getFirstName());
        registerData.setLastName(userLoginData.getLastName());

        targetCustomerFacade.register(registerData);
    }

    @Then("^the user password is encrypted with bcrypt$")
    public void theUserPasswordIsEncryptedWithBcrypt() {
        final UserModel user = userService.getUserForUID(userLoginData.getUsername());
        assertThat(user.getPasswordEncoding()).isEqualTo("bcrypt");
    }

    @Given("^a user exists with the following details:$")
    public void aUserExistsWithTheFollowingDetails(final List<UserLoginData> data) {
        assertThat(data).hasSize(1);
        userLoginData = data.get(0);

        userModel = userService.getUserForUID(userLoginData.getUsername());

        assertThat(userModel).isNotNull();
        assertThat(userModel.getUid()).isEqualTo(userLoginData.getUsername());

        assertThat(userLoginData.getPasswordEncoding()).isIn("bcrypt", "targetmd5");
        assertThat(userModel.getPasswordEncoding()).isEqualTo(userLoginData.getPasswordEncoding());

        if ("bcrypt".equals(userLoginData.getPasswordEncoding())) {
            assertThat(targetBcryptPasswordEncoder.check(userModel.getUid(),
                    userModel.getEncodedPassword(),
                    userLoginData.getPassword())).isTrue();
        }
        else if ("targetmd5".equals(userLoginData.getPasswordEncoding())) {
            assertThat(
                    targetMd5PasswordEncoder.encode(userModel.getUid(), userLoginData.getPassword()))
                    .isEqualTo(userModel.getEncodedPassword());
        }
    }

    @SuppressWarnings("deprecation")
    @When("^the user signs in with the correct password$")
    public void theUserEntersCorrectPasswordAtSignIn() {
        final User user = modelService.getSource(userModel);
        loginSuccess = user.checkPassword(userLoginData.getPassword());
    }

    @Then("^the user is allowed to sign in$")
    public void theUserIsAllowedToSignIn() {
        assertThat(loginSuccess).isTrue();
    }

    @Then("^no attempt is made to update password encryption$")
    public void noAttemptIsMadeToUpdatePasswordEncryption() {
        final UserModel freshUserModel = userService.getUserForUID(userLoginData.getUsername());
        assertThat(freshUserModel.getPasswordEncoding()).isEqualTo(userModel.getPasswordEncoding());
        assertThat(freshUserModel.getEncodedPassword()).isEqualTo(userModel.getEncodedPassword());
    }

    @SuppressWarnings("deprecation")
    @When("^the user attempts to sign in with an incorrect password$")
    public void theUserEntersIncorrectPasswordAtSignIn() {
        final User user = modelService.getSource(userModel);
        loginSuccess = user.checkPassword(BAD_PASSWORD);
    }

    @Then("^the user is not allowed to sign in$")
    public void theUserIsNotAllowedToSignIn() {
        assertThat(loginSuccess).isFalse();
    }

    @When("^the user resets their password to '(.*)'$")
    public void theUserResetsTheirPasswordTo(final String newPassword) throws TokenInvalidatedException {
        reusePasswordException = null;
        try {
            targetCustomerFacade.updatePassword(resetToken, newPassword);
        }
        catch (final ReusePasswordException ex) {
            reusePasswordException = ex;
        }

    }

    @When("^the user changes their password to '(.*)'$")
    public void theUserChangesTheirPasswordTo(final String newPassword) {
        userService.setCurrentUser(userService.getUserForUID(userLoginData.getUsername()));
        targetCustomerFacade.changePassword(userLoginData.getPassword(), newPassword);
    }

    @Given("^the user submits a password reset request$")
    public void theUserSubmitsAPasswordResetRequest() {
        targetCustomerFacade.forgottenPassword(userLoginData.getUsername());

        final TargetCustomerModel user = userService.getUserForUID(userLoginData.getUsername(),
                TargetCustomerModel.class);
        resetToken = user.getToken();
    }

    @Then("^the user is presented with a re-use same password error$")
    public void userRecievesReusePasswordError() {
        Assertions.assertThat(reusePasswordException).isNotNull();
    }

    @Then("^Switch the user to be admin$")
    public void switchToAdminUser() {
        SessionUtil.setAdminUserSession();
    }

}
