/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.ordersplitting.model.StockLevelModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.endeca.navigation.MERec;
import com.endeca.navigation.MERecList;
import com.endeca.navigation.MProperty;
import com.endeca.navigation.MPropertyMap;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.tgtfacades.product.stock.StockAvailabilitiesData;
import au.com.target.tgtfacades.product.stock.StockAvailabilityData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.response.data.StockResponseData;
import au.com.target.tgtfluent.data.StockDatum;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.FluentClientUtil;
import au.com.target.tgttest.automation.facade.StockLevelUtil;
import au.com.target.tgttest.automation.facade.bean.ProductsStockDetails;
import au.com.target.tgttest.automation.facade.bean.StockLevelData;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Set or lookup stock
 *
 */
public class StockLevelSteps {

    private StockAvailabilitiesData stockAvailabilitiesData;

    private Response response;

    @Given("^set fastline stock:$")
    public void setFastlineStockForProduct(final List<StockLevelData> stockLevel) {

        for (final StockLevelData stockData : stockLevel) {
            StockLevelUtil.updateFastlineProductStock(stockData.getProduct(),
                    Integer.valueOf(stockData.getReserved()).intValue(),
                    Integer.valueOf(stockData.getAvailable()).intValue(),
                    stockData.getPreOrder() == null ? 0 : Integer.parseInt(stockData.getPreOrder()),
                    stockData.getMaxPreOrder() == null ? 0 : Integer.parseInt(stockData.getMaxPreOrder()));
        }
    }

    @Given("^set CNP stock:$")
    public void setCNPStockForProduct(final List<StockLevelData> stockLevel) {

        for (final StockLevelData stockData : stockLevel) {
            StockLevelUtil.updateCNPProductStock(stockData.getProduct(),
                    Integer.valueOf(stockData.getReserved()).intValue(),
                    Integer.valueOf(stockData.getAvailable()).intValue());
        }
    }

    @Given("^sellable Variants stock availablity as below:$")
    public void setProductStockDetails(final List<ProductsStockDetails> productStockDetails) {
        for (final ProductsStockDetails eachProductStockDetails : productStockDetails) {
            final String[] productCodes = eachProductStockDetails.getSellableVariants().trim().split(",");
            final String[] stockAvailability = eachProductStockDetails.getStockAvailability().trim().split(",");
            final List<StockLevelData> stockLevelDataList = new ArrayList<>();
            int productCount = 0;
            for (final String eachProductCode : productCodes) {
                final StockLevelData stockLevelData = new StockLevelData();
                stockLevelData.setProduct(eachProductCode);
                if (stockAvailability[productCount++].equals("y")) {
                    stockLevelData.setAvailable("100");
                    stockLevelData.setReserved("0");
                }
                else {
                    stockLevelData.setAvailable("0");
                    stockLevelData.setReserved("0");
                }
                stockLevelData.setProduct(eachProductCode);
                stockLevelDataList.add(stockLevelData);
            }

            setFastlineStockForProduct(stockLevelDataList);
        }

    }

    @Given("^set Incomm gift cards in stock:$")
    public void setIncommStockForProducts(final List<StockLevelData> stockLevels) {

        // for incomm we set them to FORCE_INSTOCK
        for (final StockLevelData stockData : stockLevels) {
            if ("NONE".equals(stockData.getAvailable())) {

                StockLevelUtil.removeStockLevel(stockData.getProduct(), "IncommWarehouse");
            }
            else {
                StockLevelUtil.updateProductStock(stockData.getProduct(),
                        Integer.valueOf(stockData.getAvailable()).intValue(),
                        "IncommWarehouse");
            }
        }

    }

    @Then("^fastline stock is:$")
    public void validateFastlineStockForProduct(final List<StockLevelData> stockLevel) {
        for (final StockLevelData stockData : stockLevel) {
            final StockLevelModel stockLevelModel = StockLevelUtil.getFastlineStockFor(stockData.getProduct());

            assertThat(stockLevelModel.getReserved()).isEqualTo(Integer.parseInt(stockData.getReserved()));
            assertThat(stockLevelModel.getAvailable()).isEqualTo(Integer.parseInt(stockData.getAvailable()));
            if (StringUtils.isNotEmpty(stockData.getMaxPreOrder())) {
                assertThat(stockLevelModel.getMaxPreOrder()).isEqualTo(Integer.parseInt(stockData.getMaxPreOrder()));
            }
            if (StringUtils.isNotEmpty(stockData.getPreOrder())) {
                assertThat(stockLevelModel.getPreOrder()).isEqualTo(Integer.parseInt(stockData.getPreOrder()));
            }
        }
    }

    @Then("^CNP stock is:$")
    public void validateCnpStockForProduct(final List<StockLevelData> stockLevel) {
        for (final StockLevelData stockData : stockLevel) {
            final StockLevelModel stockLevelModel = StockLevelUtil.getCnpStockFor(stockData.getProduct());
            assertThat(stockLevelModel.getReserved()).isEqualTo(Integer.parseInt(stockData.getReserved()));
            assertThat(stockLevelModel.getAvailable()).isEqualTo(Integer.parseInt(stockData.getAvailable()));
        }
    }


    @Given("^products with the following stock exists against fastline warehouse:$")
    public void setupFastlineStock(final List<StockLevelData> stockLevel) {
        setFastlineStockForProduct(stockLevel);
    }

    @When("^the stock level is looked up:$")
    public void stockLevelIsLookedUp(final List<StockAvailabilityData> stockAvailabilities) {
        stockAvailabilitiesData = new StockAvailabilitiesData();
        stockAvailabilitiesData.setAvailabilities(stockAvailabilities);
        stockAvailabilitiesData = ServiceLookup.getTargetStockLookupFacade()
                .getStockFromWarehouse(stockAvailabilitiesData, "ebay", null);
    }

    @Then("^the retrieved stock response will contain the following details:$")
    public void stockLevelResponseContains(final List<StockAvailabilityData> stockAvailabilitiesExpected) {
        assertThat(stockAvailabilitiesData.getAvailabilities()).hasSize(stockAvailabilitiesExpected.size());
        for (final StockAvailabilityData stockAvailabilityRecieved : stockAvailabilitiesData.getAvailabilities()) {
            for (final StockAvailabilityData stockAvailabilityExpected : stockAvailabilitiesExpected) {
                if (StringUtils.equalsIgnoreCase(stockAvailabilityRecieved.getItemCode(),
                        stockAvailabilityExpected.getItemCode())) {
                    assertThat(stockAvailabilityRecieved.isIsAvailable())
                            .isEqualTo(stockAvailabilityExpected.isIsAvailable());
                    assertThat(stockAvailabilityRecieved.getTotalAvailableQuantity())
                            .isEqualTo(stockAvailabilityExpected.getTotalAvailableQuantity());
                }
            }
        }
    }

    @Then("^(?:NTL|incomm) stock is:$")
    public void validateNTLStockForProduct(final List<StockLevelData> stockLevel) {

        for (final StockLevelData stockData : stockLevel) {
            final StockLevelModel stockLevelModel = StockLevelUtil.getStockForWarehouse(stockData.getProduct(),
                    stockData.getWarehouseCode());

            assertThat(stockLevelModel.getReserved()).isEqualTo(Integer.parseInt(stockData.getReserved()));
            assertThat(stockLevelModel.getAvailable()).isEqualTo(Integer.parseInt(stockData.getAvailable()));
        }
    }

    @Given("^products with the following ats values in fluent:$")
    public void setupFluentStock(final DataTable dataTable) {
        for (final Map<String, String> row : dataTable.asMaps(String.class,String.class)) {
            for (final String fulfilmentType : Arrays.asList("CC", "HD", "ED", "PO")) {
                final int stock = row.get(fulfilmentType) != null ? Integer.parseInt(row.get(fulfilmentType))
                        : 0;
                FluentClientUtil.getFulfilmentOption(fulfilmentType).put(row.get("product"),
                        Integer.valueOf(stock));
            }
        }
    }

    @Given("^products with the following consolidated stock exists:$")
    public void setupConsolidatedStock(final DataTable dataTable) {
        final MERecList eRecs = new MERecList();
        MERec eRec;
        MPropertyMap pProperties;
        for (final Map<String, String> row : dataTable.asMaps(String.class,String.class)) {
            FluentClientUtil.getFulfilmentOption("CONSOLIDATED_STORES_SOH").put(row.get("product"),
                    Integer.valueOf(row.get("stock")));

            eRec = new MERec();
            pProperties = new MPropertyMap();
            pProperties.add(new MProperty(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE,
                    row.get("product")));
            pProperties.add(new MProperty(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY_IN_STORE,
                    row.get("stock")));
            pProperties.add(
                    new MProperty(EndecaConstants.EndecaRecordSpecificFields.ENDECA_ACTUAL_CONSOLIDATED_STORE_STOCK,
                            row.get("stock")));
            eRec.setProperties(pProperties);
            eRecs.add(eRec);
        }

        ServiceLookup.getEndecaProductQueryBuilder().seteRecs(eRecs);
    }

    @When("^stock level is looked up with:$")
    public void lookupStock(final DataTable dataTable) {
        final Map<String, String> row = dataTable.asMaps(String.class,String.class).iterator().next();
        final String variants = extractData(row, "variants");
        final String deliveryTypes = extractData(row, "deliveryTypes");
        final String locations = extractData(row, "locations");

        response = ServiceLookup.getTargetStockLookupFacade().lookupStock(
                StringUtils.isBlank(variants) ? Collections.<String> emptyList() : Arrays.asList(variants.split(",")),
                StringUtils.isBlank(deliveryTypes) ? Collections.<String> emptyList()
                        : Arrays.asList(deliveryTypes.split(",")),
                StringUtils.isBlank(locations) ? Collections.<String> emptyList()
                        : Arrays.asList(locations.split(",")));
    }

    @Then("^stock response has:$")
    public void verifyResponse(final DataTable dataTable) {
        final Map<String, String> row = dataTable.asMaps(String.class,String.class).iterator().next();
        final String errorCode = extractData(row, "error");
        if (StringUtils.isNotBlank(errorCode)) {
            assertThat(response.isSuccess()).isFalse();
            assertThat(response.getData().getError()).isNotNull();
            assertThat(response.getData().getError().getCode()).isEqualTo(errorCode);
        }
        else {
            assertThat(response.isSuccess()).isTrue();
            assertThat(response.getData()).isInstanceOf(StockResponseData.class);
            final StockResponseData stockResponseData = (StockResponseData)response.getData();
            assertThat(stockResponseData.getAvailabilityTime()).isInstanceOf(Date.class);
            assertThat(stockResponseData.getStockData()).onProperty("variantCode")
                    .containsExactly((Object[])extractData(row, "variants").split(","));
            verifyAts(row, stockResponseData);
            verifyStoreSoh(row, stockResponseData);
        }
    }

    /**
     * @param row
     * @param stockResponseData
     */
    private void verifyAts(final Map<String, String> row, final StockResponseData stockResponseData) {
        List<StockLevelStatus> stockLevelStatusList;
        for (final String atsKey : Arrays.asList("atsCc", "atsHd", "atsEd", "atsPo", "consolidatedStoreStock")) {
            stockLevelStatusList = new ArrayList<>();
            final String atsValueString = extractData(row, atsKey);
            if (StringUtils.isNotBlank(atsValueString)) {
                for (final String stockLevelString : atsValueString.split(",")) {
                    stockLevelStatusList.add(StockLevelStatus.valueOf(stockLevelString));
                }
            }
            assertThat(stockResponseData.getStockData()).onProperty(atsKey)
                    .containsExactly(
                            CollectionUtils.isNotEmpty(stockLevelStatusList) ? stockLevelStatusList.toArray()
                                    : new Object[stockResponseData.getStockData().size()]);
        }
    }

    private void verifyStoreSoh(final Map<String, String> row, final StockResponseData stockResponseData) {
        final String storeSohString = extractData(row, "storeSoh");
        if (StringUtils.isNotBlank(storeSohString)) {
            Map<String, StockLevelStatus> storeSoh;
            int i = 0;
            for (final String storeSohPerVariant : storeSohString.split(",")) {
                storeSoh = new LinkedHashMap<>();
                for (final String storeSohPerLocation : storeSohPerVariant.split(" ")) {
                    final String[] split = storeSohPerLocation.split(":");
                    storeSoh.put(split[0], StockLevelStatus.valueOf(split[1]));
                }
                assertThat(stockResponseData.getStockData().get(i).getStoreSoh()).isEqualTo(storeSoh);
                i += 1;
            }
        }
        else {
            for (final StockDatum stockDatum : stockResponseData.getStockData()) {
                assertThat(stockDatum.getStoreSoh()).isNull();
            }
        }
    }

    private String extractData(final Map<String, String> row, final String key) {
        final String value = row.get(key);
        return value == null || "none".equalsIgnoreCase(value) ? StringUtils.EMPTY : value;
    }

    @Given("^set product stock:$")
    public void setStockForProducts(final List<StockLevelData> stockLevels) {
        for (final StockLevelData stockData : stockLevels) {
            StockLevelUtil.updateProductStock(stockData.getProduct(),
                    Integer.parseInt(stockData.getAvailable()),
                    stockData.getWarehouseCode());
        }
    }
}
