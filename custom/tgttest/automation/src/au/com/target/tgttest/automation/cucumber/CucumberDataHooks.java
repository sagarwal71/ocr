/**
 *  
 */
package au.com.target.tgttest.automation.cucumber;

import de.hybris.platform.core.Registry;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfulfilment.ordersplitting.strategy.impl.StoreSelectorStrategyImpl;
import au.com.target.tgtfulfilment.ordersplitting.strategy.impl.TargetSplitWithRoutingStrategy;
import au.com.target.tgttest.automation.ImpexImporter;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.BusinessProcessCustomerUtil;
import au.com.target.tgttest.automation.facade.BusinessProcessUtil;
import au.com.target.tgttest.automation.facade.CartUtil;
import au.com.target.tgttest.automation.facade.CatalogUtil;
import au.com.target.tgttest.automation.facade.ConsignmentUtil;
import au.com.target.tgttest.automation.facade.CustomerUtil;
import au.com.target.tgttest.automation.facade.DeliveryUtil;
import au.com.target.tgttest.automation.facade.FeatureSwitchUtil;
import au.com.target.tgttest.automation.facade.ManifestUtil;
import au.com.target.tgttest.automation.facade.OrderUtil;
import au.com.target.tgttest.automation.facade.PinPadUtil;
import au.com.target.tgttest.automation.facade.ProductUtil;
import au.com.target.tgttest.automation.facade.StepProductUtil;
import au.com.target.tgttest.automation.facade.StockLevelUtil;
import au.com.target.tgttest.automation.facade.StoreFulfilmentCapabilitiesUtil;
import au.com.target.tgttest.automation.facade.StoreStockUtil;
import au.com.target.tgttest.automation.facade.VoucherUtil;
import au.com.target.tgttest.automation.facade.domain.BusinessProcess;
import au.com.target.tgttinker.mock.exception.MockExceptionThrowingTargetStockService;
import au.com.target.tgttinker.mock.ordersplitting.MockStoreSelectorStrategyImpl;
import cucumber.api.java.After;
import cucumber.api.java.Before;


/**
 * Hooks for setup and teardown of data per feature/scenario.<br/>
 * Before order should start at 200 since main before hook is priority 100 <br/>
 * After order should be in range 200-300 since main after hook is priority 1000
 * 
 */
public class CucumberDataHooks {

    @Before(value = "@voucherData", order = 250)
    public void beforeScenarioVoucherData() {

        VoucherUtil.initialise();
    }

    @Before(value = "@voucherRestrictionData", order = 250)
    public void beforeScenarioVoucherRestrictionData() {
        VoucherUtil.initialiseRestrictions();
    }

    @Before(value = "@storeStockData", order = 250)
    public void beforeScenarioStoreStockData() {
        StoreStockUtil.initialise();
    }

    @Before(value = "@pinpadData", order = 250)
    public void beforeScenarioPinPadData() {
        PinPadUtil.initialise();
    }

    @Before(value = "@storeFulfilmentCapabilitiesData", order = 260)
    public void beforeScenarioStoreFulfilmentData() {
        StoreFulfilmentCapabilitiesUtil.reset();
    }

    @Before(value = "@deliveryModeData", order = 240)
    public void beforeScenarioDeliveryModeData() {
        // We need to initialise delivery modes before each scenario to ensure they are all active
        DeliveryUtil.initialise();
    }

    @Before(value = "@stockLevelExceptionMock", order = 250)
    public void beforeScenarioStockLevelata() {
        final MockExceptionThrowingTargetStockService mockStockService = ServiceLookup.getMockTargetService();
        mockStockService.setActive(true);
    }

    @Before(value = "@dummyStoreWarehouseData", order = 250)
    public void beforeScenarioStoreConsignmentData() {
        ConsignmentUtil.setupStores();
    }

    @Before(value = "@cleanConsignmentData", order = 250)
    public void beforeScenarioClearConsignmentData() {
        ConsignmentUtil.removeAllConsignments();
        ManifestUtil.removeAllManifests();
    }

    @Before(value = "@cleanOrderData", order = 250)
    public void beforeScenarioClearOrderData() {
        OrderUtil.removeAllOrders();
    }

    @Before(value = "@cartProductData", order = 250)
    public void beforeScenarioCartProductData() {
        CartUtil.initialise();
    }

    @Before(value = "@shipsterConfigData", order = 250)
    public void beforeScenarioShipsterConfigData() {
        DeliveryUtil.initializeShipsterConfig();
    }

    @Before(value = "@preOrderDeliveryFee", order = 250)
    public void preOrderDeliveryFee() {
        DeliveryUtil.initializePreOrderDeliveryFeeConfig();
    }

    @Before(value = "@autoFetchProductData", order = 250)
    public void beforeScenarioAutoFetchProductData() {
        ProductUtil.initialiseAutoFetchProduct();
    }

    @Before(value = "@initFluentData", order = 250)
    public void beforeScenarioInitFluentData() {
        ImpexImporter.importCsv("/tgttest/automation-impex/product/fluent-product.impex");
    }

    @Before(value = "@initFluentCategoriesData", order = 250)
    public void beforeScenarioInitFluentCategoriesData() {
        ImpexImporter.importCsv("/tgttest/automation-impex/fluent/categories-fluent.impex");
    }

    @Before(value = "@clearSessionCartData", order = 250)
    public void beforeScenarioclearSessionCartData() {
        CartUtil.clearSessionCartData();
    }

    @Before(value = "@lookDetailsData", order = 250)
    public void beforeScenarioLookDetailsData() {
        ProductUtil.initialiseLooks();
    }

    @Before(value = "@customerData", order = 250)
    public void beforeScenarioUser() {
        CustomerUtil.resetUserData();
        BusinessProcessCustomerUtil.teardownCustomerProcesses();
    }

    @Before(value = "@cleanSmsBusinessProcessData", order = 250)
    public void beforeScenarioBusinessProcessData() {
        CustomerUtil.resetUserData();
        BusinessProcessUtil.removeAllSmsBusinessProcesses();
    }

    @Before(value = "@cleanReverseGiftcardProcessData", order = 250)
    public void beforeScenarioCleanReverseGiftcardProcessData() {
        BusinessProcessUtil.removeAllReverseGiftcardProcesses();
    }

    @Before(value = "@sessionCatalogVersion", order = 250)
    public void beforeScenarioSessionCatalogVersion() {
        CatalogUtil.setSessionCatalogVersion();
    }

    @Before(value = "@sessionStagedCatalogVersion", order = 250)
    public void beforeScenarioSessionStagedCatalogVersion() {
        CatalogUtil.setSessionStagedCatalogVersion();
    }


    @Before(value = "@postCodeGroupData", order = 250)
    public void beforeScenarioPostCodeGroupData() {
        DeliveryUtil.initializePostCodeData();
    }

    @Before(value = "@stepImportData", order = 250)
    public void beforeScenarioStepImportData() {
        StepProductUtil.initializeStepImportData();
    }

    @Before(value = "@fastlinestockData", order = 250)
    public void beforerScenarioStockReset() {
        StockLevelUtil.reset();
    }

    @Before(value = "@ntlFulfilmentData", order = 250)
    public void beforeScenarioRoutingRefactoringOff() {
        ImpexImporter.importCsv("/tgttest/automation-impex/ntl/ntl-capabilities.impex");
        ImpexImporter.importCsv("/tgttest/automation-impex/ntl/ntl-stock.impex");
    }

    @Before(value = "@currencyConversionFactorsData", order = 250)
    public void beforeScenarioCurrencyConversionFactorsData() {
        ImpexImporter.importCsv("/tgttest/automation-impex/currency-conversion/currency-conversion.impex");
    }

    @Before(value = "@forceDefaultWarehouseToOnlinePOS", order = 250)
    public void beforeScenarioForceDefaultWarehouseToOnlinePOS() {
        FeatureSwitchUtil.switchOnTheFeature(TgtCoreConstants.FeatureSwitch.FORCE_DEFAULT_WAREHOUSE_TO_ONLINE_POS);
    }

    @Before(value = "@fluentFeatureSwitch", order = 250)
    public void beforeScenarioFluentFeatureSwitch() {
        FeatureSwitchUtil.switchOnTheFeature(TgtCoreConstants.FeatureSwitch.FLUENT);
    }

    @After(value = "@deliveryModeData", order = 240)
    public void afterScenarioDeliveryModeData() {
        // We need to initialise delivery modes before each scenario to ensure they are all active
        DeliveryUtil.tearDown();
    }

    @After(value = "@stockLevelExceptionMock", order = 250)
    public void afterScenarioStockLevelata() {
        final MockExceptionThrowingTargetStockService mockStockService = ServiceLookup.getMockTargetService();
        mockStockService.setActive(false);
    }

    @Before(value = "@initPreOrderProducts", order = 250)
    public void beforeScenarioInitPreOrderProducts() {
        ImpexImporter.importCsv("/tgttest/automation-impex/product/preOrder-products.impex");
    }

    @After(value = "@storeFulfilmentCapabilitiesData", order = 260)
    public void afterScenarioStoreFulfilmentData() {
        StoreFulfilmentCapabilitiesUtil.reset();
    }

    @Before(value = "@zipFeatureSwitch", order = 250)
    public void beforeScenarioZipFeatureSwitch() {
        FeatureSwitchUtil.switchOnTheFeature(TgtCoreConstants.FeatureSwitch.ZIP);
    }

    @After(order = 240)
    public void afterScenarioOrderData() {

        // Tear down order and cart if there is one
        BusinessProcessUtil.teardownOrderProcesses();
        CartUtil.teardownCart();
        BusinessProcessUtil.teardownBusinessProcess(BusinessProcess.getInstance());
    }

    @After(value = "@voucherData", order = 250)
    public void afterScenarioVoucherData() {
        VoucherUtil.teardown();
    }

    @After(value = "@voucherRestrictionData", order = 250)
    public void afterScenarioVoucherRestrictionData() {
        VoucherUtil.teardownRestrictions();
    }

    @After(value = "@dummyStoreWarehouseData", order = 250)
    public void afterScenarioStoreConsignmentData() {
        ConsignmentUtil.teardownStores();
    }

    @After(value = "@fastlinestockData", order = 250)
    public void afterScenarioStockReset() {
        StockLevelUtil.reset();
    }

    @Before(value = "@registeredCustomers", order = 250)
    public void beforeScenarioRegisteredCustomers() {
        CustomerUtil.setupRegisteredCustomers();
    }

    @After(value = "@forceDefaultWarehouseToOnlinePOS", order = 250)
    public void afterScenarioForceDefaultWarehouseToOnlinePOS() {
        FeatureSwitchUtil.switchOffTheFeature(TgtCoreConstants.FeatureSwitch.FORCE_DEFAULT_WAREHOUSE_TO_ONLINE_POS);
    }

    @After(value = "@fluentFeatureSwitch", order = 250)
    public void afterScenarioFluentFeatureSwitch() {
        FeatureSwitchUtil.switchOffTheFeature(TgtCoreConstants.FeatureSwitch.FLUENT);
    }


    @Before(value = "@mockStoreSelectorStrategy", order = 250)
    public void beforeScenarioMockStoreSelector() {
        final TargetSplitWithRoutingStrategy routingStrategy = (TargetSplitWithRoutingStrategy)Registry
                .getApplicationContext().getBean("targetSplitWithRoutingStrategy");
        final MockStoreSelectorStrategyImpl mockStoreSelector = (MockStoreSelectorStrategyImpl)Registry
                .getApplicationContext().getBean("mockStoreSelectorStrategy");
        routingStrategy.setStoreSelectorStrategy(mockStoreSelector);
    }

    @After(value = "@mockStoreSelectorStrategy", order = 250)
    public void afterScenarioMockStoreSelector() {
        final TargetSplitWithRoutingStrategy routingStrategy = (TargetSplitWithRoutingStrategy)Registry
                .getApplicationContext().getBean("targetSplitWithRoutingStrategy");
        final StoreSelectorStrategyImpl storeSelectorStrategy = (StoreSelectorStrategyImpl)Registry
                .getApplicationContext().getBean("storeSelectorStrategy");
        routingStrategy.setStoreSelectorStrategy(storeSelectorStrategy);
    }

    @Before(value = "@cncOrderExtractOnConsignment", order = 250)
    public void beforeScenarioCncOrderExtractOnConsignmentFeatureSwitch() {
        FeatureSwitchUtil.switchOnTheFeature("cncOrderExtractOnConsignment");
    }

    @After(value = "@zipFeatureSwitch", order = 250)
    public void afterScenarioZipFeatureSwitch() {
        FeatureSwitchUtil.switchOffTheFeature(TgtCoreConstants.FeatureSwitch.ZIP);
    }

    @Before(value = "@falconFeatureSwitch", order = 250)
    public void beforeScenarioFalconFeatureSwitch() {
        FeatureSwitchUtil.switchOnTheFeature(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON);
    }

    @After(value = "@falconFeatureSwitch", order = 250)
    public void afterScenarioFalconFeatureSwitch() {
        FeatureSwitchUtil.switchOffTheFeature(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON);
    }

    @Before(value = "@initShortLeadTime", order = 250)
    public void beforeScenarioShortLeadTime() {
        ImpexImporter.importCsv("/tgttest/automation-impex/store/delivery-short-lead-time.impex");
    }

    @Before(value = "@initCncLocationData", order = 250)
    public void beforeScenarioCncLocationData() {
        ImpexImporter.importCsv("/tgttest/automation-impex/store/cnc-store-location-data.impex");
    }
}
