/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.math.BigDecimal;
import java.util.List;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetFeatureSwitchModel;
import au.com.target.tgtfacades.cart.data.TargetCheckoutCartData;
import au.com.target.tgtfacades.config.CheckoutConfigurationFacade;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.login.TargetAuthenticationFacade;
import au.com.target.tgtfacades.login.TargetCheckoutLoginResponseFacade;
import au.com.target.tgtfacades.order.data.SessionData;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.CustomerRegisteredResponseData;
import au.com.target.tgtfacades.response.data.Error;
import au.com.target.tgtfacades.response.data.LoginSuccessResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.user.data.TargetRegisterData;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.CartUtil;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.bean.CustomerRegistrationData;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author rmcalave
 *
 */
public class CheckoutLoginSteps {
    private final UserService userService = ServiceLookup.getUserService();
    private final CheckoutConfigurationFacade checkoutConfigurationFacade = ServiceLookup
            .getCheckoutConfigurationFacade();
    private final TargetCheckoutLoginResponseFacade targetCheckoutLoginResponseFacade = ServiceLookup
            .getTargetCheckoutLoginResponseFacade();
    private final TargetAuthenticationFacade targetAuthenticationFacade = ServiceLookup.getTargetAuthenticationFacade();
    private final SessionService sessionService = ServiceLookup.getSessionService();
    private final TargetLaybyCartService targetLaybyCartService = ServiceLookup.getTargetLaybyCartService();
    private final ModelService modelService = ServiceLookup.getModelService();
    private final FlexibleSearchService flexibleSearchService = ServiceLookup.getFlexibleSearchService();

    private final String redirectUrl = "/checkout";

    private String providedUsername;
    private CustomerRegistrationData providedRegistrationData;

    private Response isCustomerRegisteredResponse;
    private Response processGuestCheckoutRequestResponse;
    private Response processCustomerRegistrationRequestResponse;
    private UserModel preCustomerModel;

    @Given("^a customer exists with the user name '(.*)'$")
    public void aCustomerExistsWithTheUserName(final String username) {
        final UserModel user = userService.getUserForUID(username);

        assertThat(user).isInstanceOf(TargetCustomerModel.class);
        providedUsername = username;
    }

    @When("^the customer provides their email address during checkout login$")
    public void theCustomerProvidesTheirEmailAddressDuringCheckoutLogin() {
        isCustomerRegisteredResponse = targetCheckoutLoginResponseFacade.isCustomerRegistered(providedUsername);
    }

    @When("^the customer changes the email to '(.*)'$")
    public void changeEmailTo(final String email) {
        providedUsername = email;
    }

    @Then("^the customer will be asked to enter their password$")
    public void theCustomerWillBeAskedToEnterTheirPassword() {
        assertThat(isCustomerRegisteredResponse.isSuccess()).isTrue();
        assertThat(isCustomerRegisteredResponse.getData()).isInstanceOf(CustomerRegisteredResponseData.class);

        final CustomerRegisteredResponseData data = (CustomerRegisteredResponseData)isCustomerRegisteredResponse
                .getData();
        assertThat(data.isRegistered()).isTrue();
        assertThat(data.isAccountLocked()).isFalse();
    }

    @Given("^a customer does not exist with the user name '(.*)'$")
    public void aCustomerDoesNotExistWithTheUserName(final String username) {
        UserModel user = null;
        try {
            user = userService.getUserForUID(username);
        }
        catch (final UnknownIdentifierException ex) {
            // Do nothing
        }

        assertThat(user).isNull();
        providedUsername = username;
    }

    @Then("^the customer will not be asked to enter their password$")
    public void theCustomerWillNotBeAskedToEnterTheirPassword() {
        assertThat(isCustomerRegisteredResponse.isSuccess()).isTrue();
        assertThat(isCustomerRegisteredResponse.getData()).isInstanceOf(CustomerRegisteredResponseData.class);

        final CustomerRegisteredResponseData data = (CustomerRegisteredResponseData)isCustomerRegisteredResponse
                .getData();
        assertThat(data.isRegistered()).isFalse();
    }

    @Given("^that customer has not exceeded the maximum number of failed login attempts$")
    public void thatCustomerHasNotExceededTheMaximumNumberOfFailedLoginAttempts() {
        assertThat(targetAuthenticationFacade.isAccountLocked(providedUsername)).isFalse();
    }

    @Given("^that customer has exceeded the maximum number of failed login attempts$")
    public void thatCustomerHasExceededTheMaximumNumberOfFailedLoginAttempts() {
        assertThat(targetAuthenticationFacade.isAccountLocked(providedUsername)).isTrue();
    }

    @Then("^the customer will be told that they are locked out of their account$")
    public void theCustomerWillBeToldThatTheyAreLockedOutOfTheirAccount() {
        assertThat(isCustomerRegisteredResponse.isSuccess()).isTrue();
        assertThat(isCustomerRegisteredResponse.getData()).isInstanceOf(CustomerRegisteredResponseData.class);

        final CustomerRegisteredResponseData data = (CustomerRegisteredResponseData)isCustomerRegisteredResponse
                .getData();
        assertThat(data.isRegistered()).isTrue();
        assertThat(data.isAccountLocked()).isTrue();
    }

    @When("^the customer begins checkout as guest$")
    public void theCustomerBeginsCheckoutAsGuest() {
        processGuestCheckoutRequestResponse = targetCheckoutLoginResponseFacade
                .processGuestCheckoutRequest(providedUsername, redirectUrl);
    }

    @Then("^the customer will enter the checkout flow as an anonymous guest$")
    public void theCustomerWillEnterTheCheckoutFlowAsAnAnonymousGuest() {
        assertThat(processGuestCheckoutRequestResponse.isSuccess()).isTrue();
        assertThat(processGuestCheckoutRequestResponse.getData()).isInstanceOf(LoginSuccessResponseData.class);

        final LoginSuccessResponseData data = (LoginSuccessResponseData)processGuestCheckoutRequestResponse.getData();
        assertThat(data.getRedirectUrl()).isEqualTo(redirectUrl);

        //TODO confirm this works http://joel-costigliola.github.io/assertj/assertj-core.html#ambiguous-compilation-error
        assertThat((Boolean) sessionService.getAttribute(TgtFacadesConstants.ANONYMOUS_CHECKOUT)).isEqualTo(Boolean.TRUE);

        final CartModel cart = targetLaybyCartService.getSessionCart();
        assertThat(cart).isNotNull();
        assertThat(cart.getUser()).isInstanceOf(TargetCustomerModel.class);

        final TargetCustomerModel cartCustomer = (TargetCustomerModel)cart.getUser();
        assertThat(cartCustomer.getContactEmail()).isEqualTo(providedUsername);
        assertThat(cartCustomer.getType()).isEqualTo(CustomerType.GUEST);
    }

    @Then("^the customer will enter the checkout flow as a registered guest$")
    public void theCustomerWillEnterTheCheckoutFlowAsARegisteredGuest() {
        assertThat(processGuestCheckoutRequestResponse.isSuccess()).isTrue();
        assertThat(processGuestCheckoutRequestResponse.getData()).isInstanceOf(LoginSuccessResponseData.class);

        final LoginSuccessResponseData data = (LoginSuccessResponseData)processGuestCheckoutRequestResponse.getData();
        assertThat(data.getRedirectUrl()).isEqualTo(redirectUrl);

        //TODO confirm this works http://joel-costigliola.github.io/assertj/assertj-core.html#ambiguous-compilation-error
        assertThat((Boolean) sessionService.getAttribute(TgtFacadesConstants.ANONYMOUS_CHECKOUT)).isEqualTo(Boolean.TRUE);

        final CartModel cart = targetLaybyCartService.getSessionCart();
        assertThat(cart).isNotNull();
        assertThat(cart.getUser()).isInstanceOf(TargetCustomerModel.class);

        final TargetCustomerModel cartCustomer = (TargetCustomerModel)cart.getUser();
        assertThat(cartCustomer.getContactEmail()).isEqualTo(providedUsername);
        assertThat(cartCustomer.getUid()).endsWith(providedUsername);
        assertThat(cartCustomer.getType()).isEqualTo(CustomerType.GUEST);
    }

    @Given("^guest checkout is (enabled|disabled)$")
    public void guestCheckoutIsEnabled(final String state) {
        final TargetFeatureSwitchModel example = new TargetFeatureSwitchModel();
        example.setName(TgtCoreConstants.FeatureSwitch.CHECKOUT_ALLOW_GUEST);

        final TargetFeatureSwitchModel checkoutAllowGuestFeatureSwitch = flexibleSearchService
                .getModelByExample(example);

        if ("enabled".equals(state)) {
            checkoutAllowGuestFeatureSwitch.setEnabled(Boolean.TRUE);
        }
        else if ("disabled".equals(state)) {
            checkoutAllowGuestFeatureSwitch.setEnabled(Boolean.FALSE);
        }

        modelService.save(checkoutAllowGuestFeatureSwitch);
    }

    @Then("^the customer will be told that guest checkout is not available$")
    public void theCustomerWillBeToldThatGuestCheckoutIsNotAvailable() {
        validateGuestCheckout(TgtFacadesConstants.WebServiceError.ERR_LOGIN_GUEST_NOT_ALLOWED);
    }

    @Then("^the customer will be told that guest checkout cannot be used to purchase gift cards$")
    public void theCustomerWillBeToldThatGuestCheckoutCannotBeUsedToPurchaseGiftCards() {
        validateGuestCheckout(TgtFacadesConstants.WebServiceError.ERR_LOGIN_GUEST_NO_GIFT_CARDS);
    }

    @Then("^the customer will be told that guest checkout cannot be used to purchase pre-order item$")
    public void theCustomerWillBeToldThatGuestCheckoutCannotBeUsedToPurchasePreOrderItem() {
        validateGuestCheckout(TgtFacadesConstants.WebServiceError.ERR_LOGIN_GUEST_NO_PREORDERS);
    }

    private void validateGuestCheckout(final String errorCode) {
        assertThat(processGuestCheckoutRequestResponse.isSuccess()).isFalse();

        final BaseResponseData data = processGuestCheckoutRequestResponse.getData();
        final Error error = data.getError();

        assertThat(error).isNotNull();
        assertThat(error.getCode()).isEqualTo(errorCode);

        final BigDecimal sessionAttrAnonyCheckout = sessionService.getAttribute(TgtFacadesConstants.ANONYMOUS_CHECKOUT);
        assertThat(sessionAttrAnonyCheckout).isNull();

        final CartModel cart = targetLaybyCartService.getSessionCart();
        assertThat(cart).isNotNull();
        assertThat(cart.getUser()).isInstanceOf(CustomerModel.class);

        final CustomerModel cartCustomer = (CustomerModel)cart.getUser();
        assertThat(cartCustomer.getUid()).isEqualTo("anonymous");
    }

    @Then("^the customer will be given the option of registering or continue as guest$")
    public void theCustomerWillBeGivenTheOptionOfRegisteringOrContinueAsGuest() {
        assertThat(isCustomerRegisteredResponse.isSuccess()).isTrue();
        assertThat(isCustomerRegisteredResponse.getData()).isInstanceOf(CustomerRegisteredResponseData.class);

        final CustomerRegisteredResponseData customerRegisteredResponseData = (CustomerRegisteredResponseData)isCustomerRegisteredResponse
                .getData();
        assertThat(customerRegisteredResponseData.isRegistered()).isFalse();

        final SessionData sessionData = checkoutConfigurationFacade.getSessionInformation(null);
        final TargetCheckoutCartData cartData = sessionData.getCart();

        assertThat(cartData.isIsGuestCheckoutEnabled()).isTrue();
        assertThat(cartData.isIsGiftCardInCart()).isFalse();
    }

    @When("^the customer provides the following details:$")
    public void theCustomerProvidesTheFollowingDetails(final List<CustomerRegistrationData> dataList) {
        assertThat(dataList).hasSize(1);

        providedRegistrationData = dataList.get(0);

        final TargetRegisterData registerData = new TargetRegisterData();
        registerData.setLogin(providedUsername);
        registerData.setFirstName(providedRegistrationData.getFirstName());
        registerData.setLastName(providedRegistrationData.getLastName());
        registerData.setPassword(providedRegistrationData.getPassword());

        processCustomerRegistrationRequestResponse = targetCheckoutLoginResponseFacade
                .processCustomerRegistrationRequest(registerData, redirectUrl);
    }

    @Then("^an account will be created with the provided details$")
    public void anAccountWillBeCreatedWithTheProvidedDetails() {
        assertThat(processCustomerRegistrationRequestResponse.isSuccess()).isTrue();

        final UserModel user = userService.getUserForUID(providedUsername);

        assertThat(user).isInstanceOf(TargetCustomerModel.class);

        final TargetCustomerModel customer = (TargetCustomerModel)user;
        assertThat(customer.getFirstname()).isEqualTo(providedRegistrationData.getFirstName());
        assertThat(customer.getLastname()).isEqualTo(providedRegistrationData.getLastName());

    }

    @Then("^the customer will enter the checkout flow as them$")
    public void theCustomerWillEnterTheCheckoutFlowAsThem() {
        assertThat(processCustomerRegistrationRequestResponse.isSuccess()).isTrue();
        assertThat(processCustomerRegistrationRequestResponse.getData()).isInstanceOf(LoginSuccessResponseData.class);

        final LoginSuccessResponseData data = (LoginSuccessResponseData)processCustomerRegistrationRequestResponse
                .getData();
        assertThat(data.getRedirectUrl()).isEqualTo(redirectUrl);

        final CartModel cart = targetLaybyCartService.getSessionCart();
        assertThat(cart).isNotNull();
        assertThat(cart.getUser()).isInstanceOf(TargetCustomerModel.class);

        final TargetCustomerModel cartCustomer = (TargetCustomerModel)cart.getUser();
        assertThat(cartCustomer.getContactEmail()).isEqualTo(providedUsername);
        assertThat(cartCustomer.getUid()).endsWith(providedUsername);
    }

    @Then("^the customer will be signed up to receive marketing material$")
    public void theCustomerWillBeSignedUpToReceiveMarketingMaterial() {
        // Express the Regexp above with the code you wish you had
        throw new PendingException();
    }

    @Then("^the customer will be told that their password is insecure$")
    public void theCustomerWillBeToldThatTheirPasswordIsInsecure() {
        assertThat(processCustomerRegistrationRequestResponse.isSuccess()).isFalse();

        final BaseResponseData data = processCustomerRegistrationRequestResponse.getData();
        final Error error = data.getError();

        assertThat(error).isNotNull();
        assertThat(error.getCode()).isEqualTo(TgtFacadesConstants.WebServiceError.ERR_LOGIN_REGISTER_WEAK_PASSWORD);
    }

    @Given("^an anonymous user is doing checkout as guest$")
    public void anonymousGuestCheckout() throws DuplicateUidException {
        CartUtil.createCheckoutCart(CheckoutUtil.GUEST_CHECKOUT);
        preCustomerModel = CartUtil.getCheckoutCartUser();
    }

    @Then("^the customer holds the same user details as before in checkout process$")
    public void verifySameUserModel() {
        assertThat(CartUtil.getCheckoutCartUser()).isEqualTo(preCustomerModel);
    }
}
