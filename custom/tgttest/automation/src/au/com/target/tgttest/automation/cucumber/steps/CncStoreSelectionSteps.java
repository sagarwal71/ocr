/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static java.lang.Boolean.valueOf;

import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import au.com.target.tgtcore.storelocator.impl.TargetGPS;
import au.com.target.tgtfacades.response.data.StoreSearchResultResponseData;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgttest.automation.ImpexImporter;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.CartUtil;
import au.com.target.tgttest.automation.facade.bean.CartEntry;
import au.com.target.tgttest.automation.facade.bean.CncStoreEntry;
import au.com.target.tgttest.automation.facade.bean.GeolocationData;
import au.com.target.tgttest.automation.mock.MockGeoWebServiceWrapper;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author bhuang3
 *
 */
@SuppressWarnings("deprecation")
public class CncStoreSelectionSteps {

    private CartSteps cartSteps;
    private GeoPoint customerGeoPoint;
    private PageableData pageableData;
    private List<PointOfServiceData> pointOfServiceData;

    @Given("^the customer has the cart with non bulky product$")
    public void prepareDefaultCartData() throws Exception {
        final CartEntry entry = new CartEntry("10000011", 2, Double.valueOf(15));
        final List<CartEntry> cartEntries = new ArrayList<>();
        cartEntries.add(entry);
        cartSteps = new CartSteps();
        cartSteps.givenCartEntries(cartEntries);
    }

    @Given("^the customer has the cart with bulky product$")
    public void prepareDefaultCartDataWithBulkyProduct() throws Exception {
        prepareDefaultCartData();
        final CartEntry entry1 = new CartEntry("10000011", 2, Double.valueOf(15));
        final CartEntry entry2 = new CartEntry("10000411", 2, Double.valueOf(15));
        final List<CartEntry> cartEntries = new ArrayList<>();
        cartEntries.add(entry1);
        cartEntries.add(entry2);
        cartSteps = new CartSteps();
        cartSteps.givenCartEntries(cartEntries);
    }

    @Given("^a customer is located with coordinate data of latitude '(.*)' and longitude '(.*)'$")
    public void prepareCustomerCoordinate(final String latitude, final String longitude) {
        try {
            customerGeoPoint = new GeoPoint();
            customerGeoPoint.setLatitude(Double.parseDouble(latitude));
            customerGeoPoint.setLongitude(Double.parseDouble(longitude));
        }
        catch (final NumberFormatException e) {
            throw new RuntimeException("NumberFormatException when setting customer location", e);
        }

    }

    @Given("^the max number of stores shown is (\\d+)$")
    public void setMaxNumberOfStoresShown(final int number) {
        pageableData = new PageableData();
        pageableData.setCurrentPage(0);
        pageableData.setPageSize(number);
    }

    @Given("^the Top 10 closest stores to the coordinate of latitude '-38.1083132' and longitude '144.3357845':$")
    public void setDefaultPointOfServiceData(final List<CncStoreEntry> storeEntry) {
        Assert.assertEquals(10, storeEntry.size());
        ImpexImporter.importCsv("/tgttest/automation-impex/store/cnc-store-selection-data.impex");
    }

    @When("^the customer opts to use his/her location$")
    public void trigerCncStoreFinder() {
        pointOfServiceData = ServiceLookup.getTargetStoreLocatorFacade().getCncStoresByPositionSearch(
                CartUtil.getSessionCartModel(), customerGeoPoint, pageableData).getResults();
    }

    @Then("^the customer should be presented with the following (\\d+) stores:$")
    public void verifyCncStoresList(final int number, final List<CncStoreEntry> storeEntry) {
        Assert.assertEquals(number, pointOfServiceData.size());
        Assert.assertEquals(storeEntry.size(), pointOfServiceData.size());
        for (int i = 0; i < pointOfServiceData.size(); i++) {
            final TargetPointOfServiceData posData = (TargetPointOfServiceData)pointOfServiceData.get(i);
            Assert.assertEquals(storeEntry.get(i).getStoreName(), posData.getName());
            if (posData.getAvailableForPickup() != null) {
                Assert.assertEquals(valueOf(storeEntry.get(i).isAvailableForPickup()), posData.getAvailableForPickup());
            }
        }
    }

    @When("^the customer submits a postcode (\\d+)$")
    public void submitsPostcode(final int postcode) {
        pointOfServiceData = ServiceLookup.getTargetStoreLocatorFacade().getCncStoresByPostCode(
                String.valueOf(postcode),
                CartUtil.getSessionCartModel(), pageableData).getResults();
    }

    @Given("^geolocation setup for certain postcode/suburb are:$")
    public void setGpsForLocation(final List<GeolocationData> geoLocations) {
        for (final GeolocationData geolocation : geoLocations) {
            final TargetGPS gps = new TargetGPS(geolocation.getLatitude(), geolocation.getLongitude());

            gps.setLocality(geolocation.getLocality());
            gps.setPostcode(geolocation.getPostcode());
            gps.setState(geolocation.getState());

            MockGeoWebServiceWrapper.setGps(geolocation.getLocationText(), gps);
        }
    }

    @When("^the customer search stores with '(.*)' via spc$")
    public void submitsPostcodeViaSPC(final String locationText) {
        final StoreSearchResultResponseData data = (StoreSearchResultResponseData)ServiceLookup
                .getTargetCheckoutResponseFacade().searchCncStores(locationText, pageableData).getData();
        pointOfServiceData = data.getStores();
    }

    @Given("^a customer makes a CNC store selection with the named location cannot be geocoded$")
    public void namedLocationCannotBeGeocoded() {
        MockGeoWebServiceWrapper.resetGpsMap();
    }

    @When("^the customer submits a named location$")
    public void submitsNamedLocation() {
        pointOfServiceData = ServiceLookup.getTargetStoreLocatorFacade().getCncStoresByPostCode("invalid locaton",
                CartUtil.getSessionCartModel(), pageableData).getResults();
    }

    @Then("^the customer will not see any stores$")
    public void emptyResultList() {
        Assert.assertEquals(0, pointOfServiceData.size());
    }
}
