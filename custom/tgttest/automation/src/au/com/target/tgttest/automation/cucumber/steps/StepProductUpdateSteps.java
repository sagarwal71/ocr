/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.GiftCardModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.GiftCardUtil;
import au.com.target.tgttest.automation.facade.ProductUtil;
import au.com.target.tgttest.automation.facade.SessionUtil;
import au.com.target.tgttest.automation.facade.StepProductUtil;
import au.com.target.tgttest.automation.facade.StockLevelUtil;
import au.com.target.tgttest.automation.facade.bean.GiftCardData;
import au.com.target.tgttest.automation.facade.bean.ImportGiftCard;
import au.com.target.tgttest.automation.facade.bean.ProductImportData;
import au.com.target.tgttest.automation.facade.bean.ProductImportVariantData;
import au.com.target.tgtutility.util.TargetProductUtils;
import au.com.target.tgtwsfacades.integration.dto.IntegrationActivationDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationVariantDto;
import au.com.target.tgtwsfacades.productimport.TargetProductImportConstants;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author knemalik
 * 
 */
public class StepProductUpdateSteps {

    private static final String EMPTY_VALUE = "Empty";
    private static final String NOT_AVAILABLE = "N/A";

    private IntegrationActivationDto integrationActivationDto;

    private IntegrationProductDto integrationProductDto;

    private IntegrationResponseDto response;

    private String lastProductCode;
    private String lastProductName;
    private Double lastWidth;
    private Double lastHeight;
    private Double lastLength;
    private Double lastWeight;


    @Given("^product '(.*)' is set to '(.*)' in STEP$")
    public void productIsSetToInactiveInSTEP(final String productCode, final String approvalStatus) {
        integrationActivationDto = StepProductUtil.getIntegrationActivationData(productCode, approvalStatus);
    }

    @Given("^a new colour variant product in STEP$")
    public void newColourVariantProductInSTEP() {

        integrationProductDto = StepProductUtil.getColourVariantOnlyData(getNewProductCode());
    }

    @Given("^a product in STEP with code '(\\d+)'$")
    public void givenBaseProductInStepWithCode(final String productCode) {
        lastProductCode = productCode;
        integrationProductDto = StepProductUtil.getColourVariantOnlyData(productCode);
    }

    @Given("^a sellable variant product in STEP with code 'P(\\d+)' and with name '(.*)'$")
    public void sellableVariantProductInSTEPWithCode(final String productCode, final String productName) {
        lastProductCode = productCode;
        lastProductName = productName;
        integrationProductDto = StepProductUtil.getColourVariantOnlyData(lastProductCode);
        integrationProductDto.setName(lastProductName);
    }

    @Given("^a product:$")
    public void productInSTEP(final List<ProductImportData> product)
            throws Exception {
        integrationProductDto = StepProductUtil.getColourVariantOnlyData(product.get(0));
    }

    @Given("^variants:$")
    public void andItsVariantsInSTEP(final List<ProductImportVariantData> variants)
            throws Exception {
        if (integrationProductDto == null) {
            integrationProductDto = StepProductUtil.getColourVariantOnlyData("P11112222");
        }
        integrationProductDto = StepProductUtil.populateVariants(integrationProductDto, variants.get(0));
    }

    @Given("^product type in STEP is '(.*)'$")
    public void givenProductTypeInStep(final String productType) {
        integrationProductDto.setProductType(productType);

        // marking non-normal products as bulky in dto
        if (!"normal".equalsIgnoreCase(productType)) {
            integrationProductDto.setBulky("Y");
        }

        // marking cnc as "N" for mhd products
        if ("mhd".equalsIgnoreCase(productType)) {
            integrationProductDto.setAvailableCnc("N");
        }
    }

    @Given("^available on ebay flag in STEP is '(Y|N)'$")
    public void givenAvailableOnEbayFlagInStep(final String availableOnEbayFlag) {
        integrationProductDto.setAvailableOnEbay(availableOnEbayFlag);
    }

    @Given("^ebay express delivery flag in STEP is '(Y|N)'$")
    public void givenEbayExpressDeliveryFlagInStep(final String ebayExpDelFlag) {
        integrationProductDto.setAvailableEbayExpressDelivery(ebayExpDelFlag);
    }

    @Given("^available for cnc flag in STEP is '(Y|N)'$")
    public void givenAvaliableForCncFlagInStep(final String availableForCncFlag) {
        integrationProductDto.setAvailableCnc(availableForCncFlag);
    }

    @Given("^express delivery flag in STEP is '(.*)'$")
    public void givenExpressDeliveryFlagInStep(final String expDelFlag) {
        integrationProductDto.setAvailableExpressDelivery(getExpressDeliveryFlagFromString(expDelFlag));
    }

    @Given("^EAN of product is given as '(.*)'$")
    public void productEANgivenAs(final String productEAN) {
        if (EMPTY_VALUE.equalsIgnoreCase(productEAN)) {
            integrationProductDto.setEan(StringUtils.EMPTY);
        }
        else {
            integrationProductDto.setEan(productEAN);
        }
    }

    @Given("^clearance flag in STEP is '(Y|N)'$")
    public void givenClearanceFlagInStep(final String clearanceFlag) {
        integrationProductDto.setClearance(getClearanceFlagFromString(clearanceFlag));
    }


    @Given("^an existing colour variant product in STEP$")
    public void existingColourVariantProductInSTEP() {

        final String code = getNewProductCode();
        integrationProductDto = StepProductUtil.getColourVariantOnlyData(code);
        stepImportProcessRuns();
        integrationProductDto = StepProductUtil.getColourVariantOnlyData(code);
        integrationProductDto.setDescription("updated");
    }

    @Given("^a new size variant product in STEP$")
    public void newSizeVariantProductInSTEP() {

        integrationProductDto = StepProductUtil.getSizeVariantOnlyData(getNewProductCode());
    }

    @Given("^a new colour variant with size variant in STEP$")
    public void newColourVariantWithSizeVariantInSTEP() {

        integrationProductDto = StepProductUtil.getColourVariantWithVariantsData(getNewProductCode());
    }

    @Given("^a new gift card product in STEP$")
    public void newGiftCardProductInSTEP() {

        integrationProductDto = StepProductUtil.getGiftCardData(getNewProductCode());
    }

    @Given("^an existing gift card product '(.*)' in STEP$")
    public void existingGiftCardProductInSTEP(final String code) {

        integrationProductDto = StepProductUtil.getGiftCardData(code);
    }


    @Given("^dimension data \\((\\d*), (\\d*), (\\d*), (\\d*)\\)$")
    public void dimensionData(final Double height, final Double width, final Double length,
            final Double weight) {

        if (CollectionUtils.isNotEmpty(integrationProductDto.getVariants())) {
            final IntegrationVariantDto sizeDto = integrationProductDto.getVariants().get(0);
            sizeDto.setPkgHeight(height);
            sizeDto.setPkgWidth(width);
            sizeDto.setPkgLength(length);
            sizeDto.setPkgWeight(weight);
        }
        else {
            integrationProductDto.setPkgHeight(height);
            integrationProductDto.setPkgWidth(width);
            integrationProductDto.setPkgLength(length);
            integrationProductDto.setPkgWeight(weight);
        }

        // Remember what we set for verification later
        lastWidth = width;
        lastHeight = height;
        lastLength = length;
        lastWeight = weight;
    }

    @When("^STEP product import runs and updates approval status$")
    public void stepActivationProcessRuns() throws Exception {

        // We need to run this step as the esb user since this is used by esb to call the STEP import
        SessionUtil.userLogin("esb");

        ServiceLookup.getTargetProductActivationIntegrationFacade().updateApprovalStatus(integrationActivationDto);
    }

    @When("^run STEP product import process$")
    @And("^it is imported into Hybris through ESB$")
    public void stepImportProcessRuns() {

        // We need to run this step as the esb user since this is used by esb to call the STEP import
        SessionUtil.userLogin("esb");

        response = ServiceLookup.getTargetProductImportFacade().persistTargetProduct(integrationProductDto);
    }

    @Given("^a product contains original category:$")
    public void productInSTEPForOriginalCategory(final List<ProductImportData> product) {
        integrationProductDto = StepProductUtil.getColourVariantOnlyData(product.get(0));
    }


    @Then("^EAN of product imported is '(.*)'$")
    public void productEANImportedIs(final String updatedEAN) {
        final String actualImportedEan = ProductUtil.getStagedProductModel(lastProductCode).getEan();

        if (EMPTY_VALUE.equalsIgnoreCase(updatedEAN)) {
            assertThat(actualImportedEan).as("EAN of product imported")
                    .isEqualTo(StringUtils.EMPTY);
        }
        else {
            assertThat(actualImportedEan).as("EAN of product imported")
                    .isEqualTo(updatedEAN);
        }
    }

    @Then("^error message after import is \"(.*)\"$")
    public void errorMessageAfterImport(final String message) {
        final List<String> messages = response.getMessages();
        if (NOT_AVAILABLE.equalsIgnoreCase(message)) {
            assertThat(messages).as("error message after import is").isEmpty();
        }
        else {
            assertThat(messages).as("error message after import is").isNotEmpty().hasSize(1);
            assertThat(messages.get(0).toString()).as("error message after import is").isEqualTo(message);
        }
    }

    @Then("^error messages after import are:$")
    public void errorMessagesAfterImport(final List<String> errors) throws Exception {
        final List<String> messages = response.getMessages();
        assertThat(messages).as("The actual messages contain").isEqualTo(errors);
    }

    @Then("^product import response is successful$")
    public void verifyProductImportResponseSuccess() {

        assertThat(response).as("product import response").isNotNull();
        assertThat(response.isSuccessStatus()).as("product import response status").isTrue();
        assertThat(response.getMessages()).as("product import error messages").isEmpty();
    }

    @Then("^product import response is not successful$")
    public void verifyProductImportResponseNotSuccess() {

        assertThat(response).as("product import response").isNotNull();
        assertThat(response.isSuccessStatus()).as("product import response status").isFalse();

        final List<String> messages = response.getMessages();
        assertThat(messages).as("product import error messages").isNotEmpty();
    }

    @Then("^product import response is '(.*)'$")
    public void verifyProductImportResponse(final String successString) {
        if ("successful".equalsIgnoreCase(successString)) {
            verifyProductImportResponseSuccess();
        }
        else {
            verifyProductImportResponseNotSuccess();
        }
    }

    @Then("^express delivery mode in hybris is '(.*)'$")
    public void verifyExpressDeliveryMode(final String expModStatus) {

        final Set delModeStrings = getProductDeliveryModes(integrationProductDto.getVariantCode());

        if (delModeStrings == null && "N/A".equalsIgnoreCase(expModStatus)) {
            return;
        }

        assertThat(delModeStrings).isNotEmpty();

        if ("added".equalsIgnoreCase(expModStatus)) {
            assertThat(delModeStrings).contains(CheckoutUtil.EXPRESS_DELIVERY_CODE);
        }
        else {
            assertThat(delModeStrings).excludes(CheckoutUtil.EXPRESS_DELIVERY_CODE);
        }
    }

    @Then("^ebay express delivery mode in hybris is '(.*)'$")
    public void verifyEbayExpressDeliveryMode(final String ebayExpModStatus) {

        final Set delModeStrings = getProductDeliveryModes(integrationProductDto.getVariantCode());

        assertThat(delModeStrings).isNotEmpty();

        if ("added".equalsIgnoreCase(ebayExpModStatus)) {
            assertThat(delModeStrings).contains(CheckoutUtil.EBAY_EXPRESS_DELIVERY_CODE);
        }
        else {
            assertThat(delModeStrings).excludes(CheckoutUtil.EBAY_EXPRESS_DELIVERY_CODE);
        }
    }

    @Then("^ebay Home delivery mode in hybris is '(.*)'$")
    public void verifyEbayHomeDeliveryMode(final String ebayExpModStatus) {

        final Set delModeStrings = getProductDeliveryModes(integrationProductDto.getVariantCode());

        assertThat(delModeStrings).isNotEmpty();

        if ("added".equalsIgnoreCase(ebayExpModStatus)) {
            assertThat(delModeStrings).contains(CheckoutUtil.EBAY_HOME_DELIVERY_CODE);
        }
        else {
            assertThat(delModeStrings).excludes(CheckoutUtil.EBAY_HOME_DELIVERY_CODE);
        }
    }


    @Then("^clearance flag in hybris is '(added|not added)'$")
    public void verifyClearance(final String clearanceStatus) {
        final Boolean clearance = getClearanceProduct(integrationProductDto.getVariantCode());
        if ("added".equalsIgnoreCase(clearanceStatus)) {
            assertThat(clearance).isTrue();
        }
        else {
            assertThat(clearance).isFalse();
        }

    }

    @Then("^product has:")
    public void verifyValues(final List<ProductImportData> productDataList) {

        final ProductImportData productData = productDataList.get(0);
        final TargetProductModel product = (TargetProductModel)ProductUtil
                .getStagedProductModel(integrationProductDto.getProductCode());
        final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)ProductUtil
                .getStagedProductModel(integrationProductDto.getVariantCode());
        if (productData.getDepartment() != null) {
            assertThat(StepProductUtil.containsDepartment(product, productData.getDepartment()))
                    .as("The imported department is valid").isTrue();
        }
        if (StringUtils.isNotEmpty(productData.getAvailableExpressDelivery())) {
            verifyExpressDeliveryMode(productData.getAvailableExpressDelivery());
        }
        if (StringUtils.isNotEmpty(productData.getCareInstructions())) {
            assertThat(product.getCareInstructions()).isNotEmpty().isEqualTo(productData.getCareInstructions());
        }
        if (StringUtils.isNotEmpty(productData.getMaterials())) {
            assertThat(product.getMaterials()).isNotEmpty().isEqualTo(productData.getMaterials());
        }
        if (StringUtils.isNotEmpty(productData.getWebExtraInfo())) {
            assertThat(product.getExtraInfo()).isNotEmpty().isEqualTo(productData.getWebExtraInfo());
        }
        if (StringUtils.isNotEmpty(productData.getWebFeatures())) {
            assertThat(product.getProductFeatures()).isNotEmpty().isEqualTo(productData.getWebFeatures());
        }
        if (StringUtils.isNotEmpty(productData.getYouTube())) {
            final MediaModel youtube = product.getVideos().iterator().next();
            assertThat(youtube.getURL()).isEqualTo(productData.getYouTube());
        }
        if (StringUtils.isNotEmpty(productData.getMerchProductStatus())) {
            if (CollectionUtils.isEmpty(colourVariant.getVariants())) {
                assertThat(colourVariant.getMerchProductStatus()).isEqualTo(productData.getMerchProductStatus());
            }
            else {
                assertThat(colourVariant.getMerchProductStatus()).isNull();
                final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)colourVariant
                        .getVariants().iterator().next();
                assertThat(sizeVariant.getMerchProductStatus()).isEqualTo(productData.getMerchProductStatus());
            }

        }

        verifyPhysicalGiftCardValues(productData, product, colourVariant);

        StepProductUtil.verifyProductFeatures(productData, product);
        if (StringUtils.isNotEmpty(productData.getFluentId())) {
            if ("null".equals(productData.getFluentId())) {
                assertThat(product.getFluentId()).isNull();
            }
            else {
                assertThat(product.getFluentId()).isEqualTo(productData.getFluentId());
            }
        }
        verfiyPreOrderAttributes(productData, colourVariant);
    }

    private void verfiyPreOrderAttributes(final ProductImportData productData,
            final TargetColourVariantProductModel colourVariant) {
        if (StringUtils.isNotBlank(productData.getPreOrderStartDate())) {
            assertThat(colourVariant.getPreOrderStartDateTime())
                    .isEqualTo(parseStringToDate(productData.getPreOrderStartDate()));
        }
        if (StringUtils.isNotBlank(productData.getPreOrderEndDate())) {
            assertThat(colourVariant.getPreOrderEndDateTime())
                    .isEqualTo(parseStringToDate(productData.getPreOrderEndDate()));
        }

        if (StringUtils.isNotBlank(productData.getPreOrderEmbargoReleaseDate())) {
            assertThat(colourVariant.getNormalSaleStartDateTime())
                    .isEqualTo(parseStringToDate(productData.getPreOrderEmbargoReleaseDate()));
        }
        if (productData.getPreOrderOnlineQuantity() == null) {
            assertThat(colourVariant.getMaxPreOrderQuantity()).isNull();
        }
        else {
            final StockLevelModel stockLevelModel = StockLevelUtil.getStagedFastlineStockFor(colourVariant.getCode());
            assertThat(stockLevelModel).isNotNull();
            assertThat(stockLevelModel.getMaxPreOrder()).isEqualTo(productData.getPreOrderOnlineQuantity());
            assertThat(colourVariant.getMaxPreOrderQuantity()).isEqualTo(productData.getPreOrderOnlineQuantity());
        }

    }

    /**
     * 
     * @param dateString
     * @return date
     */
    private Date parseStringToDate(final String dateString) {
        Date dateTime = null;
        if (StringUtils.isNotEmpty(dateString)) {
            try {
                dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateString);
            }
            catch (final ParseException e) {
                //if any error happens while parsing the date, will return null
            }
        }
        return dateTime;
    }

    /**
     * @param productData
     * @param product
     * @param colourVariant
     */
    private void verifyPhysicalGiftCardValues(final ProductImportData productData, final TargetProductModel product,
            final TargetColourVariantProductModel colourVariant) {

        if (BooleanUtils.toBoolean(productData.getExcludeForAfterpay())) {
            assertThat(colourVariant.isExcludeForAfterpay()).isTrue();
        }
        else {
            assertThat(colourVariant.isExcludeForAfterpay()).isFalse();
        }

        if (StringUtils.isNotEmpty(productData.getProductType())
                && "giftCard".equals(productData.getProductType())) {
            final ProductTypeModel giftCardProductType = ServiceLookup.getProductTypeService()
                    .getByCode(TargetProductImportConstants.PRODUCT_TYPE_PHYSICAL_GIFTCARD);
            assertThat(product.getProductType().getCode()).isEqualTo(giftCardProductType.getCode());

        }
        if (StringUtils.isNotEmpty(productData.getAvailableHomeDelivery())) {
            verifyHomeDeliveryMode(productData.getAvailableHomeDelivery());
        }
    }

    public void verifyHomeDeliveryMode(final String expModStatus) {

        final Set delModeStrings = getProductDeliveryModes(integrationProductDto.getVariantCode());

        if (delModeStrings == null && "N/A".equalsIgnoreCase(expModStatus)) {
            return;
        }

        assertThat(delModeStrings).isNotEmpty();

        if ("added".equalsIgnoreCase(expModStatus)) {
            assertThat(delModeStrings).contains(CheckoutUtil.HOME_DELIVERY_CODE);
        }
        else {
            assertThat(delModeStrings).excludes(CheckoutUtil.HOME_DELIVERY_CODE);
        }
    }

    @Then("^imported product delivery modes are '(.*)'$")
    public void verifyImportedProductDeliveryModes(final String expectedModes) {

        String code = lastProductCode;
        if (lastProductCode == null) {
            code = integrationProductDto.getVariantCode();
        }

        final Set delModeStrings = getProductDeliveryModes(code);

        final String[] expectedModesArray = expectedModes.split(",");

        assertThat(expectedModesArray).isNotEmpty();

        assertThat(delModeStrings).isNotEmpty().hasSize(expectedModesArray.length);

        for (final String expectedMode : expectedModesArray) {
            assertThat(delModeStrings).contains(expectedMode);
        }
    }

    private Set<String> getProductDeliveryModes(final String code) {

        ProductModel productModel = null;
        try {
            productModel = ServiceLookup.getTargetProductService()
                    .getProductForCode(code);
            assertThat(productModel).isNotNull();
        }
        catch (final UnknownIdentifierException e) {
            // if the product import has failed than no product will be created in hybris which is fine
            return null;

        }

        final Set<DeliveryModeModel> delModes = ServiceLookup.getTargetDeliveryService()
                .getVariantsDeliveryModesFromBaseProduct(productModel);
        assertThat(delModes).isNotEmpty();

        final Set delModeStrings = new HashSet<>();
        for (final DeliveryModeModel delMode : delModes) {
            delModeStrings.add(delMode.getCode());
        }

        return delModeStrings;
    }

    private Boolean getClearanceProduct(final String code) {
        ProductModel productModel = null;
        try {
            productModel = ServiceLookup.getTargetProductService()
                    .getProductForCode(code);
            assertThat(productModel).isNotNull();
        }
        catch (final UnknownIdentifierException e) {
            // if the product import has failed than no product will be created in hybris which is fine
            return null;

        }
        final TargetProductModel baseProduct = (TargetProductModel)ProductUtil
                .getBaseProduct(ProductUtil
                        .getProductModel(code));
        return baseProduct.getClearanceProduct();

    }

    @Then("^product dimensions are updated$")
    public void verifyProductDimensionsUpdated() {

        final AbstractTargetVariantProductModel newProduct = (AbstractTargetVariantProductModel)ProductUtil
                .getStagedProductModel(lastProductCode);
        assertThat(newProduct).as("last product created").isNotNull();

        final TargetProductDimensionsModel dims = newProduct.getProductPackageDimensions();
        assertThat(dims).as("dimensions").isNotNull();
        assertThat(dims.getHeight()).as("height").isEqualTo(lastHeight);
        assertThat(dims.getWeight()).as("weight").isEqualTo(lastWeight);
        assertThat(dims.getLength()).as("length").isEqualTo(lastLength);
        assertThat(dims.getWidth()).as("width").isEqualTo(lastWidth);
    }

    @Then("^fastline stock object is created$")
    public void verifyFastlineStockObjectIsCreated() {

        final StockLevelModel stock = StockLevelUtil.getStagedFastlineStockFor(lastProductCode);
        assertThat(stock).isNotNull();
        assertThat(stock.getAvailable()).isEqualTo(0);
        assertThat(stock.getInStockStatus()).isNotNull().isEqualTo(InStockStatus.NOTSPECIFIED);
    }

    @Then("^no fastline stock object is created$")
    public void verifyNoFastlineStockObjectIsCreated() {

        final StockLevelModel stock = StockLevelUtil.getStagedFastlineStockFor(lastProductCode);
        assertThat(stock).isNull();
    }

    @Given("^a new gift card product in STEP with gift card data:$")
    public void setGiftCardData(final List<ImportGiftCard> giftCards) {
        if (CollectionUtils.isNotEmpty(giftCards)) {
            final ImportGiftCard giftCard = giftCards.get(0);
            integrationProductDto = StepProductUtil.getGiftCardData(getNewProductCode());
            integrationProductDto.setBrand(giftCard.getBrandID());
            integrationProductDto.setProductCode(giftCard.getProductID());
            integrationProductDto.setGiftcardStyle(giftCard.getGiftCardStyle());
            integrationProductDto.setDenominaton(StringUtils.isNotEmpty(giftCard.getDenomination()) ? Double
                    .valueOf(giftCard.getDenomination()) : null);
            integrationProductDto.setGiftcardBrandId(giftCard.getBrandID());
            integrationProductDto.setAvailableDigitalDelivery("Y");
        }
    }

    @Given("^giftcard record exists for '(.*)'$")
    public void setExisingGiftCardBrand(final String brandId) {
        final GiftCardModel giftCard = GiftCardUtil.getGiftCard(brandId);
        assertThat(giftCard).isNotNull();
    }

    @Given("^giftcard record does not exist for '(.*)'$")
    public void checkAndClearExistingGifrCard(final String brandId) {
        GiftCardUtil.removeGiftCardIfExist(brandId);
    }

    @Then("^imported product type is '(Digital)'$")
    public void testProductTypeDigital(final String productType) {
        final AbstractTargetVariantProductModel product = getLatestImportedProduct();
        final TargetProductModel baseProduct = TargetProductUtils.getBaseTargetProduct(product);
        assertThat(productType.equalsIgnoreCase(baseProduct.getProductType().getCode())).isTrue();
    }

    @Then("^imported giftcard denomination value in size variant level is '(.*)'$")
    public void checkGiftCardDenominationValue(final String denominationValue) {
        final AbstractTargetVariantProductModel product = getLatestImportedProduct();
        if (product instanceof TargetSizeVariantProductModel) {
            final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)product;
            assertThat(denominationValue).isEqualTo(sizeVariant.getDenomination().toString());
        }
    }

    @Then("^imported giftcard style value in colour variant level is '(.*)'$")
    public void checkColourVariantLevel(final String colorVariantValue) {
        final AbstractTargetVariantProductModel product = getLatestImportedProduct();
        if (product instanceof TargetSizeVariantProductModel) {
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)product
                    .getBaseProduct();
            assertThat(colorVariantValue).isEqualTo(colourVariant.getGiftCardStyle());
        }
    }

    @Then("^imported giftcard productID value in colour variant level is '(.*)'$")
    public void checkColourVariantCode(final String productId) {
        final AbstractTargetVariantProductModel product = getLatestImportedProduct();
        final TargetProductModel baseProduct = TargetProductUtils.getBaseTargetProduct(product);
        assertThat(productId.equalsIgnoreCase(baseProduct.getCode())).isTrue();
    }

    @Then("^imported giftcard brandID value in base product is '(.*)'$")
    public void checkBaseProductBrandId(final String brandId) {
        final AbstractTargetVariantProductModel product = getLatestImportedProduct();
        final TargetProductModel baseProduct = TargetProductUtils.getBaseTargetProduct(product);
        assertThat(brandId.equalsIgnoreCase(baseProduct.getBrand().getCode())).isTrue();
    }

    @Then("^incomm stock object is created with available (\\d+)$")
    public void checkStockInncommWareHouse(final String stockAmount) {
        final AbstractTargetVariantProductModel product = getLatestImportedProduct();
        if (product instanceof TargetSizeVariantProductModel) {
            final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)product;
            final Collection<StockLevelModel> stockLevel = ServiceLookup.getTargetStockService().getAllStockLevels(
                    sizeVariant);
            assertThat(stockLevel).isNotNull();
            final Iterator<StockLevelModel> itr = stockLevel.iterator();
            while (itr.hasNext()) {
                final StockLevelModel stockModel = itr.next();
                assertThat(stockModel.getWarehouse().getCode()).isEqualTo("IncommWarehouse");
                assertThat(Integer.parseInt(stockAmount)).isEqualTo(Integer.valueOf(stockModel.getAvailable()));
            }

        }
    }

    @Then("^a new giftcard is created for '(.*)' with:$")
    public void checkNewGiftCardData(final String brandId, final List<GiftCardData> giftCardList) {
        final GiftCardModel newGitCard = GiftCardUtil.getGiftCard(brandId);
        if (CollectionUtils.isNotEmpty(giftCardList)) {
            final GiftCardData giftCardData = giftCardList.get(0);
            assertThat(newGitCard.getBrandId()).isEqualTo(giftCardData.getBrandID());
            assertThat(newGitCard.getBrandName()).isEqualTo(giftCardData.getBrand());
            assertThat(newGitCard.getMaxOrderQuantity()).isEqualTo(giftCardData.getMaxOrderQuantity());
            assertThat(newGitCard.getMaxOrderValue()).isEqualTo(giftCardData.getMaxOrderValue());
        }
    }

    @Given("^a product with assorted attribute set to '(.*)' in STEP$")
    public void getProductForAssortment(final String assortedFlag) {
        integrationProductDto = StepProductUtil.getColourVariantOnlyData("ASSTMT01");
        integrationProductDto.setAssorted(assortedFlag);
    }

    @Then("^the assorted flag is set to '(.*)' against the product in Hybris$")
    public void verifyAssortedProduct(final boolean assorted) {
        final ProductModel productModel = ServiceLookup.getTargetProductService()
                .getProductForCode("ASSTMT01");
        assertThat(productModel).isInstanceOfAny(TargetColourVariantProductModel.class);
        assertThat(((TargetColourVariantProductModel)productModel).isAssorted()).isEqualTo(assorted);
    }

    private String getNewProductCode() {
        lastProductCode = String.valueOf("STEP_" + new Date().getTime());
        return lastProductCode;
    }

    private String getExpressDeliveryFlagFromString(final String expDelFlag) {
        if ("Blank".equalsIgnoreCase(expDelFlag)) {
            return null;
        }
        return expDelFlag;
    }

    private AbstractTargetVariantProductModel getLatestImportedProduct() {
        final AbstractTargetVariantProductModel newProduct;
        try {
            newProduct = (AbstractTargetVariantProductModel)ProductUtil
                    .getStagedProductModel(lastProductCode);
            assertThat(newProduct).isNotNull();
        }
        catch (final UnknownIdentifierException e) {
            // if the product import has failed than no product will be created in hybris which is fine
            return null;
        }
        return newProduct;
    }

    private String getClearanceFlagFromString(final String clearanceFlag) {
        if ("Blank".equalsIgnoreCase(clearanceFlag)) {
            return null;
        }
        return clearanceFlag;
    }

    @And("^product has originalCatogry:$")
    public void verifyValuesOC(final List<ProductImportData> products) {
        final ProductImportData productData = products.get(0);
        final TargetColourVariantProductModel s = (TargetColourVariantProductModel)ProductUtil
                .getStagedProductModel(integrationProductDto.getVariantCode());
        final TargetProductModel product = (TargetProductModel)s.getBaseProduct();


        if (product.getOriginalCategory() != null) {
            assertThat(product.getOriginalCategory()).isEqualTo(productData.getOriginalCategory());
        }


        if (productData.getDepartment() != null) {
            assertThat(StepProductUtil.containsDepartment(product, productData.getDepartment()))
                    .as("The imported department is valid").isTrue();
        }

    }

    @And("product '(.*)' excludeForZipPayment is '(.*)'$")
    public void excludeForZipPayment(final String productCode, final boolean expected) {
        final AbstractTargetVariantProductModel product = (AbstractTargetVariantProductModel)ProductUtil
                .getStagedProductModel(productCode);
        final boolean actual = product.isExcludeForZipPayment();
        assertThat(actual).isEqualTo(expected);
    }


}
