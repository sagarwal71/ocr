/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.basecommerce.enums.RefundReason;
import de.hybris.platform.basecommerce.enums.ReturnAction;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.cscockpit.exceptions.ValidationException;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.ordercancel.CancelDecision;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import au.com.target.tgtcore.order.TargetFindDeliveryCostStrategy;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.BusinessProcessUtil;
import au.com.target.tgttest.automation.facade.CancelUtil;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.RefundUtil;
import au.com.target.tgttest.automation.facade.bean.CartEntry;
import au.com.target.tgttest.automation.facade.domain.Order;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Steps to automate CS cockpit
 * 
 * @author jjayawa1
 *
 */
public class CSCockpitSteps {

    private static final Logger LOG = Logger.getLogger(CSCockpitSteps.class.getName());

    private CancelDecision cancelDecision;

    private boolean isDeliveryFeeRefundEnabled;

    private double deliveryFeeRefunded;

    private double totalDeliverFeeToRefund;

    private Map<AbstractOrderEntryModel, Long> allReturnableEntries;

    private double refundableDeliveryFee;

    @When("^CS cockpit checks if the consignment '(.*)' can be cancelled$")
    public void csAgentCanCancelConsignment(final String consignmentCode) {
        final UserModel user = ServiceLookup.getUserService().getCurrentUser();
        final ConsignmentModel exampleConsignment = new ConsignmentModel();
        exampleConsignment.setCode(consignmentCode);
        final List<ConsignmentModel> consignments = ServiceLookup.getFlexibleSearchService().getModelsByExample(
                exampleConsignment);
        assertThat(consignments).hasSize(1);
        final ConsignmentModel consignment = consignments.get(0);
        final OrderModel order = (OrderModel)consignment.getOrder();
        assertThat(order).isNotNull();
        cancelDecision = ServiceLookup.getTargetOrderCancelService().isCancelPossible(order, user, true, true);
    }

    @Then("^the cancelled response is '(.*)'$")
    public void verifyCSCockpitCancelResponse(final String responseOutcome) {
        assertThat(cancelDecision.isAllowed()).isEqualTo(BooleanUtils.toBoolean(responseOutcome));
    }

    @When("^Customer service agent views the order$")
    public void whenCustomerServiceAgentViewsTheOrder() throws InterruptedException {
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        isDeliveryFeeRefundEnabled = RefundUtil.isDeliveryFeeRefundable(orderModel);
        allReturnableEntries = RefundUtil.getAllReturnableEntries(orderModel);
        refundableDeliveryFee = RefundUtil.getRefundableDeliveryFee(orderModel);
    }

    @Then("^the option to refund delivery fee will be (.*)$")
    public void thenTheOptionToRefundDeliveryFeeIsEnabledOrDisabled(final String isEnabled) {
        assertThat(Boolean.valueOf(isDeliveryFeeRefundEnabled))
                .isEqualTo(BooleanUtils.toBoolean(Boolean.valueOf(isEnabled)));
    }

    @When("^delivery fee refund amount is (\\d+)$")
    public void whenADeliveryFeeIsRefunded(final int deliveryFee) throws OrderCancelException, InterruptedException {
        final RefundReason refundReason = RefundReason.CHANGEOFMINDRETURN;
        final ReturnAction returnAction = ReturnAction.IMMEDIATE;
        final String notes = "notes";
        deliveryFeeRefunded = deliveryFee;

        final TargetFindDeliveryCostStrategy targetFindDeliveryCostStrategy = ServiceLookup
                .getTargetFindDeliveryCostStrategy();
        if (targetFindDeliveryCostStrategy
                .deliveryCostOutstandingForOrder(Order.getInstance().getOrderModel()) >= deliveryFee) {
            CancelUtil.createRefundRequest(Collections.EMPTY_LIST, Order.getInstance().getOrderModel(), refundReason,
                    returnAction, notes, Double.valueOf(deliveryFee), null);
            BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("orderRefundProcess");
        }
    }

    @Then("^delivery fee refund request is (.*)$")
    public void thenIsDeliveryFeeRefundRequestASuccess(final String refunded) {
        if ("refunded".equalsIgnoreCase(refunded)) {
            assertThat(
                    ServiceLookup.getFindOrderRefundedShippingStrategy().getRefundedShippingAmount(
                            Order.getInstance().getOrderModel())).isEqualTo(deliveryFeeRefunded);
        }
        if ("not refunded".equalsIgnoreCase(refunded)) {
            if (totalDeliverFeeToRefund > 0) {
                assertThat(
                        ServiceLookup.getFindOrderRefundedShippingStrategy().getRefundedShippingAmount(
                                Order.getInstance().getOrderModel())).isEqualTo(deliveryFeeRefunded);
            }
            else {
                assertThat(
                        ServiceLookup.getFindOrderRefundedShippingStrategy().getRefundedShippingAmount(
                                Order.getInstance().getOrderModel())).isEqualTo(0);
            }
        }
    }

    @When("^multiple refund requests is received for (.*)$")
    public void whenMultipleRefundRequestsAreReceived(final String refundAmounts) throws NumberFormatException,
            OrderCancelException, InterruptedException {

        final String[] amounts = refundAmounts.split(",");
        for (int i = 0; i < amounts.length; i++) {
            totalDeliverFeeToRefund += Integer.parseInt(amounts[i]);
            whenADeliveryFeeIsRefunded(Integer.parseInt(amounts[i]));
        }
        deliveryFeeRefunded = totalDeliverFeeToRefund;
        if (deliveryFeeRefunded > Order.getInstance().getOrderModel().getInitialDeliveryCost().doubleValue()) {
            deliveryFeeRefunded = totalDeliverFeeToRefund - Integer.valueOf(amounts[amounts.length - 1]).intValue();
        }
    }

    @Given("^all products are (.*)$")
    public void allProductsAreRefunded(final String refunded) throws OrderCancelException, InterruptedException {
        if ("refunded".equalsIgnoreCase(refunded)) {
            final RefundReason refundReason = RefundReason.CHANGEOFMINDRETURN;
            final ReturnAction returnAction = ReturnAction.IMMEDIATE;
            final String notes = "notes";

            final List<CartEntry> orderEntries = new ArrayList<>();
            final CartEntry entry1 = new CartEntry(CheckoutUtil.ANY_PRD_CODE, 2,
                    Double.valueOf(CheckoutUtil.ANY_PRD_PRICE));
            orderEntries.add(entry1);
            try {
                CancelUtil.createRefundRequest(orderEntries, Order.getInstance().getOrderModel(), refundReason,
                        returnAction, notes, Double.valueOf(0), null);
            }
            catch (final OrderCancelException e) {
                LOG.info("OrderCancelException", e);
            }

            BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("orderRefundProcess");
        }
    }


    @Given("^delivery fee is not refunded$")
    public void deliveryFeeIsNotRefunded() {
        // Do nothing as nothing needs to be done
    }

    @Then("^the refund option is (.*)$")
    public void thenRefundOptionIsEnabledOrDisabled(final String isEnabled) {
        final boolean isRefundOptionEnabled = isDeliveryFeeRefundEnabled || MapUtils.isNotEmpty(allReturnableEntries);
        if ((isEnabled.equalsIgnoreCase("enabled") || Boolean.valueOf(isEnabled) == Boolean.TRUE)) {
            assertThat(Boolean.valueOf(isRefundOptionEnabled)).isTrue();
        }
        else if ((isEnabled.equalsIgnoreCase("disabled") || Boolean.valueOf(isEnabled) == Boolean.FALSE)) {
            assertThat(isRefundOptionEnabled).isFalse();
        }
    }

    @Then("^refund dialog product list will be empty$")
    public void refundDialogContainingProductListIsEmpty() {
        assertThat(MapUtils.isEmpty(allReturnableEntries)).isEqualTo(Boolean.TRUE);
    }

    @Then("^refund dialog product list allows Delivery cost refund up to \\$(.*)$")
    public void refundableDeliveryCostInRefundDialog(final double refundableDeliveryCost) {
        assertThat(refundableDeliveryFee).isEqualTo(refundableDeliveryCost);
    }

    @Then("^refund dialog product list has '(.*)'$")
    public void refundableProductsInRefundDialog(final String refundableProducts) {
        final String[] products = refundableProducts.split(",");
        assertThat(allReturnableEntries.size()).isEqualTo(products.length);
        for (final AbstractOrderEntryModel entry : allReturnableEntries.keySet()) {
            assertThat(isThisEntryRefundable(entry.getProduct().getCode(), products)).isTrue();
        }
    }

    /**
     * Method to check if entry product can be refunded
     * 
     * @param code
     * @return boolean
     */
    private boolean isThisEntryRefundable(final String code, final String[] products) {
        final List<String> productsInList = CollectionUtils.arrayToList(products);
        return productsInList.contains(code);
    }

    @Then("^refund dialog product list disallows Delivery cost refund$")
    public void deliveryFeeIsNotRefundableInRefundDialog() {
        assertThat(refundableDeliveryFee).isEqualTo(0.0);
    }

    @Given("^B2B Order flag is set to '(true|false)'$")
    public void setDeliveryMode(final Boolean value) {
        CheckoutUtil.setB2BValueInSession(value);
    }

    @When("^order is placed in cscockpit$")
    public void placeOrder() throws CalculationException, DuplicateUidException, ValidationException {
        CheckoutUtil.placeOrderForCsCockpit();
    }

    @Then("^sales application of order is '(B2B|CallCenter)'$")
    public void verifyOrderSalesApplication(final String salesApplication) {
        assertThat(Order.getInstance().getOrderModel().getSalesApplication().toString()).isEqualTo(salesApplication);
    }

}
