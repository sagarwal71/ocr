/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static java.lang.Boolean.TRUE;
import static java.lang.Boolean.valueOf;
import static org.fest.assertions.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Assert;

import au.com.target.tgtsale.tlog.data.TenderTypeEnum;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.PinPadUtil;
import au.com.target.tgttest.automation.facade.bean.AddressEntry;
import au.com.target.tgttest.automation.facade.bean.CustomerAccountData;
import au.com.target.tgttest.automation.facade.bean.TlogCreditCardPaymentInfo;
import au.com.target.tgttest.automation.facade.bean.TlogEntry;
import au.com.target.tgttest.automation.facade.bean.TlogTransDeal;
import au.com.target.tgttest.automation.facade.bean.TlogTransDiscount;
import au.com.target.tgttest.automation.facade.domain.TlogTransaction;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;


/**
 * Steps for TLOG feature
 * 
 */
public class TlogSteps {

    @Then("^tlog entries are:$")
    public void verifyTlogEntries(final List<TlogEntry> expectedEntries) {
        TlogTransaction.getLastTransaction().verifyEntries(expectedEntries);
    }

    @And("^tlog user is '(.*)'$")
    public void verifyTlogUser(final String expectedUsername) {

        assertThat(TlogTransaction.getLastTransaction().getUsername()).isEqualTo(expectedUsername);
    }

    @And("^tlog type is '(.*)'$")
    public void verifyTlogType(final String expectedType) {

        assertThat(TlogTransaction.getLastTransaction().getType()).isEqualTo(expectedType);
    }

    @And("^tlog layby type is '(.*)'$")
    public void verifyLaybyType(final String expectedType) {
        assertThat(TlogTransaction.getLastTransaction().getLaybyType()).isEqualTo(expectedType);
    }


    @And("^tlog layby version is '(.*)'$")
    public void verifyLaybyVersion(final String version) {
        assertThat(TlogTransaction.getLastTransaction().getLaybyVersion()).isEqualTo(version);
    }

    @And("^tlog tender type is '(.*)'$")
    public void verifyTlogTenderType(final String expectedType) {
        TlogTransaction.getLastTransaction().verifyTenderType(TenderTypeEnum.valueOf(expectedType));
    }

    @And("^tlog dueDate is equal to order normalSalesDate$")
    public void verifyTlogDueDate() {
        TlogTransaction.getLastTransaction().verifyTlogDueDate();
    }

    @And("^tlog payment amount is equal to initial deposit$")
    public void verifyPaymentAmount() {
        TlogTransaction.getLastTransaction().verifyPaymentInfo();
    }

    @And("^tlog layby fee is (\\d+)$")
    public void verifyLaybyFee(final int fee) {
        assertThat(TlogTransaction.getLastTransaction().getLaybyFee().getAmount()).isEqualTo(BigDecimal.valueOf(fee));
    }

    @And("^tlog shipping is (-?\\d+)$")
    public void verifyTlogShipping(final int expectedShipping) {

        assertThat(TlogTransaction.getLastTransaction().getShipping()).isEqualTo(expectedShipping);
    }

    @And("^tlog total tender is (-?\\d+)$")
    public void verifyTlogTotalTender(final int expectedTotal) {

        assertThat(TlogTransaction.getLastTransaction().getTotalTender()).isEqualTo(expectedTotal);
    }

    @Then("^transDiscount entries are:$")
    public void verifyTlogTransDiscount(final List<TlogTransDiscount> expected) {

        TlogTransaction.getLastTransaction().verifyTransDiscounts(expected);
    }

    @Then("^transDeal entries are:$")
    public void verifyTlogTransDeal(final List<TlogTransDeal> expected) {

        TlogTransaction.getLastTransaction().verifyTransDeals(expected);
    }

    @Then("^tlog pinpad payment info matches$")
    public void verifyTlogPinPadPaymentInfo() {

        TlogTransaction.getLastTransaction().verifyPinPadPaymentInfo(PinPadUtil.getPinPadTlogInfoExpected());
    }

    @Then("^tlog credit card payment info matches$")
    public void verifyTlogCreditCardPaymentInfo() {

        TlogTransaction.getLastTransaction().verifyCreditCardPaymentInfo(CheckoutUtil.getCreditCardTlogInfoExpected());
    }

    @Then("^tlog multi card payment info matches$")
    public void verifyTlogMultiCardPaymentInfo() {
        TlogTransaction.getLastTransaction().verifyMultiCardPaymentInfo(CheckoutUtil.getMultiCardTlogInfoExpected());
    }

    @Then("^tlog afterpay sale info is valid$")
    public void verifyTlogAfterpaySaleInfo() {
        TlogTransaction.getLastTransaction().verifyAfterpaySaleInfo();
    }

    @Then("^tlog sale info is valid for '(.*)'$")
    public void verifyTlogPaymentPartnerSaleInfo(final String paymentPartner) {
        TlogTransaction.getLastTransaction().verifyPaymentPartnerSaleInfo(paymentPartner);
    }

    @Then("^tlog afterpay refund info is valid$")
    public void verifyTlogAfterpayRefundInfo() {
        TlogTransaction.getLastTransaction().verifyAfterpayRefundInfo();
    }

    @Then("^tlog zip payment refund id is '(.*)'$")
    public void verifyTlogRefundInfo(final String refundId) {
        TlogTransaction.getLastTransaction().verifyRefundInfo(refundId);
    }

    @Then("^flybuys number is '(\\d+)'$")
    public void verifyFlybuysNumber(final String flyBuysNumber) {
        TlogTransaction.getLastTransaction().verifyFlybuys(flyBuysNumber);
    }

    @Then("^tlog tender entries are:$")
    public void verifyTlogTenderEntries(final List<TlogCreditCardPaymentInfo> expectedEntries) {
        assertThat(expectedEntries.size()).isGreaterThanOrEqualTo(1);

        if (expectedEntries.size() > 1) {
            TlogTransaction.getLastTransaction().verifyMultiCardPaymentInfo(expectedEntries);
        }
        else if (expectedEntries.size() == 1) {
            TlogTransaction.getLastTransaction().verifySingleCardPaymentInfo(expectedEntries);
        }
    }

    @Then("^tlog currency is '(.*)'$")
    public void verifyTlogCurrency(final String currency) {
        Assert.assertNotNull(TlogTransaction.getLastTransaction());
        Assert.assertEquals(TRUE, valueOf(currency.equals(TlogTransaction.getLastTransaction().getCurrency())));
    }

    @Then("^tlog sales channel is '(.*)'$")
    public void verifyTlogSalesChannel(final String salesChannel) {
        Assert.assertNotNull(TlogTransaction.getLastTransaction());
        Assert.assertEquals(TRUE, valueOf(salesChannel.equals(TlogTransaction.getLastTransaction().getSalesChannel())));
    }

    @Then("^tlog customer information are:$")
    public void verifuCustomerInfo(final List<CustomerAccountData> expectedCustomerInfos) {
        assertThat(TlogTransaction.getLastTransaction().getCustomerInfo()).isNotNull();
        TlogTransaction.getLastTransaction().verifyCustomerInfo(expectedCustomerInfos.get(0));
    }

    @Then("^tlog customer Addresses are:$")
    public void verifyCustomerAddress(final List<AddressEntry> addressEntries) {
        assertThat(addressEntries.size()).isEqualTo(2);
        TlogTransaction.getLastTransaction().verifyCustomerAddress(addressEntries);
    }

    @Then("^tlog entries reason is '(.*)'$")
    public void verifyReasonInTlogEntries(final String reasonCode) {
        TlogTransaction.getLastTransaction().verifyTlogReason(reasonCode);
    }



}
