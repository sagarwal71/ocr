/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import au.com.target.tgtcore.model.TargetFeatureSwitchModel;
import au.com.target.tgttest.automation.ServiceLookup;
import cucumber.api.java.en.Given;


/**
 * @author rmcalave
 *
 */
public class FeatureSwitchSteps {
    private final ModelService modelService = ServiceLookup.getModelService();
    private final FlexibleSearchService flexibleSearchService = ServiceLookup.getFlexibleSearchService();

    @Given("^the '(.*)' feature switch is (enabled|disabled)$")
    public void theFeatureSwitchIs(final String featureSwitchName, final String state) {
        final TargetFeatureSwitchModel example = new TargetFeatureSwitchModel();
        example.setName(featureSwitchName);

        TargetFeatureSwitchModel featureSwitch = null;
        try {
            featureSwitch = flexibleSearchService.getModelByExample(example);
        }
        catch (final ModelNotFoundException ex) {
            featureSwitch = modelService.create(TargetFeatureSwitchModel.class);
            featureSwitch.setName(featureSwitchName);
            featureSwitch.setAttrName("attrName");
            featureSwitch.setComment("Comment");
        }

        if ("enabled".equals(state)) {
            featureSwitch.setEnabled(Boolean.TRUE);
        }
        else if ("disabled".equals(state)) {
            featureSwitch.setEnabled(Boolean.FALSE);
        }

        modelService.save(featureSwitch);
    }
}
