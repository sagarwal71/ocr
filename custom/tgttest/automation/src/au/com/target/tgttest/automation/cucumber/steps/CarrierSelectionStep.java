/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.Collection;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;
import au.com.target.tgttest.automation.facade.domain.Order;
import cucumber.api.java.en.Then;
import org.junit.Assert;



/**
 * @author vivek
 *
 */
public class CarrierSelectionStep {

    private TargetConsignmentModel consignment;
    private TargetCarrierModel carrier;

    @Then("^consignment sent to '(.*)'$")
    public void consignmentSentTo(final String sentTo) {
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        Assert.assertNotNull(orderModel);
        final Collection<ConsignmentModel> cons = orderModel.getConsignments();
        Assert.assertNotNull(cons);
        Assert.assertEquals(1, cons.size());
        consignment = (TargetConsignmentModel)cons.iterator().next();
        final WarehouseModel warehouse = consignment.getWarehouse();
        Assert.assertNotNull(warehouse);
        Assert.assertEquals(sentTo, warehouse.getCode());
    }

    @Then("^consignment redirected and sent to '(.*)'$")
    public void consignmentRedirectedAndSentTo(final String sentTo) {
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        Assert.assertNotNull(orderModel);
        final Collection<ConsignmentModel> cons = orderModel.getConsignments();
        Assert.assertNotNull(cons);
        Assert.assertEquals(2, cons.size());
        for (final ConsignmentModel con : cons) {
            if (ConsignmentStatus.CANCELLED != con.getStatus()) {
                consignment = (TargetConsignmentModel)con;
                break;
            }
        }
        final WarehouseModel warehouse = consignment.getWarehouse();
        Assert.assertNotNull(warehouse);
        Assert.assertEquals(sentTo, warehouse.getCode());
    }

    @Then("^carrier is '(.*)'$")
    public void validateCarrier(final String carrierCode) {
        carrier = consignment.getTargetCarrier();
        Assert.assertNotNull(carrier);
        Assert.assertEquals(carrierCode, carrier.getCode());
    }

    @Then("^service type is '(.*)'$")
    public void validateServiceType(final String serviceType) {
        Assert.assertEquals(serviceType, carrier.getServiceCode());
    }
}
