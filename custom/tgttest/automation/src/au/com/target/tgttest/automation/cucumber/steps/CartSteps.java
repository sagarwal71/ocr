/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static java.lang.Boolean.TRUE;
import static java.lang.Boolean.valueOf;
import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.ordersplitting.model.StockLevelModel;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.order.TargetCheckoutResponseFacade;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.order.data.TargetOrderEntryData;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;
import au.com.target.tgtfacades.response.data.CheckoutOptionsResponseData;
import au.com.target.tgtfacades.response.data.PaymentMethodData;
import au.com.target.tgtfacades.response.data.PaymentResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.CartUtil;
import au.com.target.tgttest.automation.facade.CatalogUtil;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.ProductUtil;
import au.com.target.tgttest.automation.facade.SessionUtil;
import au.com.target.tgttest.automation.facade.StockLevelUtil;
import au.com.target.tgttest.automation.facade.VoucherUtil;
import au.com.target.tgttest.automation.facade.bean.CartEntry;
import au.com.target.tgttest.automation.facade.bean.DeliveryModeAvailability;
import au.com.target.tgttest.automation.facade.bean.OrderFields;
import au.com.target.tgttest.automation.facade.bean.PaymentModeResponseData;
import au.com.target.tgttest.automation.facade.bean.PriceData;
import au.com.target.tgttest.automation.facade.bean.ShipsterCartData;
import au.com.target.tgttest.automation.facade.bean.StockLevelData;
import au.com.target.tgttest.automation.facade.domain.Checkout;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Steps for cart and checkout setup
 * 
 */
public class CartSteps {

    private static final String ANY_PRD_CODE = "10000011";

    private static final String ZIP_PAYMENT_MODE = "zip";

    private static final String DELIMITER_OF_FOR_ENTRY = "of";

    private TargetCartData cartData;

    private String cartCode;

    private String newCartCode;

    private CartModificationData cartModificationData;

    private List<CommerceCartModification> cartModifications;

    @Given("^a cart with entries:$")
    public void givenCartEntries(final List<CartEntry> entries) throws Exception {
        CartUtil.addCartEntries(entries);
    }

    @Given("^a cart with entries over the afterpay max threshold:$")
    public void givenCartEntriesOverAfterpayMaxThreshold(final List<CartEntry> entries) throws Exception {
        CartUtil.addCartEntries(entries);
    }

    @Given("^cart is updated with entries:$")
    public void updateCart(final List<CartEntry> entries) throws Exception {
        CartUtil.addCartEntries(entries);
    }

    @Given("^the cart entries are updated before place order:$")
    public void updateCartEntriesBeforePlaceOrder(final List<CartEntry> entries) {
        Checkout.getInstance().setBeforPlaceOrderUpdateCartEntries(entries);
    }

    @Given("^the cart entries are added before place order:$")
    public void addCartEntriesBeforePlaceOrder(final List<CartEntry> entries) {
        Checkout.getInstance().setBeforPlaceOrderAddCartEntries(entries);
    }

    @Given("^any cart$")
    public void givenAnyCart() throws Exception {
        CheckoutUtil.createAnyCart();
    }

    @Given("^any home-delivery cart$")
    public void givenAnyHomeDeliveryCart() throws Exception {
        givenAnyCart();
        setDeliveryMode("home-delivery");
    }

    @Given("^the quantity added to cart for the '(.*)' is '(\\d+)'$")
    public void cartWithProductAndGivenQty(final String product, final int qty) throws Exception {
        CartUtil.addToCart(product, qty);
        recalculateSessionCart();
    }

    @Given("^any click-and-collect cart for pickup at '(.*)'$")
    public void givenAnyClickAndCollectCartForPickup(final String store) throws Exception {
        givenAnyCart();
        setDeliveryMode("click-and-collect");
        orderCncStoreIs(store);
    }

    @Given("^registered user checkout$")
    public void createUserForCheckout() {
        Checkout.getInstance().setUserCheckoutMode(CheckoutUtil.REGISTERED_USER_CHECKOUT);
    }

    @Given("^registered user proceed to checkout$")
    public void userProceedToCheckout() throws DuplicateUidException {
        CartUtil.createCheckoutCart(CheckoutUtil.REGISTERED_USER_CHECKOUT);
    }


    @Given("^user checkout via spc$")
    public void spcCheckout() {
        Checkout.getInstance().setSpc(true);
    }

    @Given("^guest checkout$")
    public void createGuestUserForAnonymousCheckout() {
        Checkout.getInstance().setUserCheckoutMode(CheckoutUtil.GUEST_CHECKOUT);
    }

    @Given("^delivery mode is '(.*)'$")
    public void setDeliveryMode(final String deliveryModeCode) {
        Checkout.getInstance().setDeliveryMode(deliveryModeCode);
    }

    @When("^get available payment methods for SPC$")
    public void getAvailablePaymentMethodsForSPC() {
        Checkout.getInstance().setLastResponse(
                ServiceLookup.getTargetCheckoutResponseFacade().getApplicablePaymentMethods());
    }

    @When("^selected delivery mode in spc is '(.*)'$")
    public void selectDeliveryMode(final String deliveryModeCode) {
        ServiceLookup.getTargetCheckoutResponseFacade().setDeliveryMode(deliveryModeCode);
    }

    @When("^selected delivery mode in spc is changed to '(.*)'$")
    public void changeDeliveryMode(final String deliveryModeCode) {
        ServiceLookup.getTargetCheckoutResponseFacade().setDeliveryMode(deliveryModeCode);
    }

    @When("^the order summary is displayed via spc$")
    public void getCartSummary() {
        final Response response = ServiceLookup.getTargetCheckoutResponseFacade().getCartSummary();
        Checkout.getInstance().setLastResponse(response);
    }

    @When("^the order detail is displayed via spc$")
    public void getCartDetail() {
        final Response response = ServiceLookup.getTargetCheckoutResponseFacade().getCartDetail();
        Checkout.getInstance().setLastResponse(response);
    }

    @Then("^billing address displayed is$")
    public void verifyBillingAddress(final List<String> address) {
        for (final String addressValue : address) {
            final String[] addressFragments = addressValue.split(",");
            final TargetAddressData addressData = new TargetAddressData();
            addressData.setLine1(addressFragments[0].trim());
            addressData.setTown(addressFragments[1].trim());
            addressData.setPostalCode(addressFragments[2].trim());
            final CountryData countryData = new CountryData();
            countryData.setIsocode(addressFragments[3].trim());
            countryData.setName(addressFragments[4].trim());
            addressData.setCountry(countryData);

            final Response response = Checkout.getInstance().getLastResponse();
            final CartDetailResponseData responseData = (CartDetailResponseData)response.getData();
            final AddressData billingAddress = responseData.getBillingAddress();

            assertThat(addressData.getLine1()).isEqualTo(billingAddress.getLine1());
            assertThat(addressData.getTown()).isEqualTo(billingAddress.getTown());
            assertThat(addressData.getPostalCode()).isEqualTo(billingAddress.getPostalCode());
            assertThat(addressData.getCountry().getIsocode()).isEqualTo(
                    billingAddress.getCountry().getIsocode());
            assertThat(addressData.getCountry().getName()).isEqualTo(billingAddress.getCountry().getName());
        }
    }

    @When("^customer enters billing address$")
    public void givenCustomerAddsBillingAddress(final List<String> address) {
        for (final String addressValue : address) {
            final String[] addressFragments = addressValue.split(",");
            final TargetAddressData addressData = new TargetAddressData();
            addressData.setLine1(addressFragments[0].trim());
            addressData.setTown(addressFragments[1].trim());
            addressData.setPostalCode(addressFragments[2].trim());
            final CountryData countryData = new CountryData();
            countryData.setIsocode(addressFragments[3].trim());
            addressData.setCountry(countryData);
            addressData.setShippingAddress(false);
            addressData.setVisibleInAddressBook(false);
            addressData.setBillingAddress(true);
            ServiceLookup.getCheckoutResponseFacade().createBillingAddress(addressData);
        }
    }

    @Then("^saved cart delivery address is '(.*)'$")
    public void thenDeliveryAddressIs(final String deliveryAddress) {
        if (StringUtils.isBlank(deliveryAddress)) {
            assertThat((CheckoutUtil.getCheckoutCart().getDeliveryAddress())).isNull();
            return;
        }
        final String[] addressFragments = deliveryAddress.split(",");
        final AddressModel address = CheckoutUtil.getCheckoutCart().getDeliveryAddress();
        assertThat(address).isNotNull();
        assertThat(address.getLine1()).isEqualTo(addressFragments[0]);
        assertThat(address.getTown()).isEqualTo(addressFragments[1]);
        assertThat(address.getPostalcode()).isEqualTo(addressFragments[2]);
    }

    @Then("^available payment method details are:$")
    public void thenPaymentMethodDetails(final List<PaymentModeResponseData> paymentResponses) {
        final Response response = Checkout.getInstance().getLastResponse();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<PaymentMethodData> paymentMethods = responseData.getPaymentMethods();
        for (final PaymentModeResponseData expectedData : paymentResponses) {
            final PaymentMethodData paymentMethod = getPaymentMethodData(expectedData.getName(), paymentMethods);
            if (paymentMethod != null) {
                assertThat(paymentMethod.getName()).as(expectedData.getName()).isEqualTo(expectedData.getName());
                assertThat(paymentMethod.isAvailable()).isEqualTo(expectedData.isAvailable());
                if (expectedData.getReason().equals("null")) {
                    assertThat(paymentMethod.getReason()).isNull();
                }
                else {
                    assertThat(paymentMethod.getReason()).isEqualTo(expectedData.getReason());
                }
            }
        }
    }

    @Given("^the delivery address is '(.*)'$")
    public void setDeliveryAddress(final String deliveryAddress) {
        if (StringUtils.isNotBlank(deliveryAddress)) {
            Checkout.getInstance().setDeliveryAddress(deliveryAddress);
        }
    }

    @Given("^'(.*)' is selected as billing address$")
    public void selectBillingAddress(final String billingAddress) {
        if (StringUtils.isNotBlank(billingAddress)) {
            final Response response = ServiceLookup.getCheckoutResponseFacade().getDeliveryAddresses();
            final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
            final List<AddressData> userAddresses = responseData.getDeliveryAddresses();
            final String[] addressFragments = billingAddress.split(",");
            AddressData selectedAddress = null;
            for (final AddressData address : userAddresses) {
                if (address.getLine1().equals(addressFragments[0]) && address.getTown().equals(addressFragments[1])
                        && address.getPostalCode().equals(addressFragments[2])) {
                    selectedAddress = address;
                    break;
                }
            }
            if (null != selectedAddress) {
                final Response addressChangedResponse = ServiceLookup.getCheckoutResponseFacade()
                        .selectBillingAddress(selectedAddress.getId());
                Checkout.getInstance().setLastResponse(addressChangedResponse);
            }
        }
    }


    @Given("^order cnc store is '(.*)'")
    public void orderCncStoreIs(final String store) {
        if (StringUtils.isNotBlank(store) && !store.equalsIgnoreCase("na")) {
            final StoreFulfilmentCapabilitySteps fulfillmentSteps = new StoreFulfilmentCapabilitySteps();
            final int storeNumber = fulfillmentSteps.getStoreNumber(store, null).intValue();
            Checkout.getInstance().setStoreNumber(storeNumber);
        }
    }

    @Given("^payment method is '(.*)'")
    public void setPaymentMethod(final String paymentMethod) {
        Checkout.getInstance().setPaymentMethod(paymentMethod);
    }

    @Given("^the user chooses payment method '(.*)' via spc(.*)$")
    public void setPaymentMethodViaIPG(final String paymentMethod, final String result) {
        final TargetCheckoutResponseFacade targetCheckoutResponseFacade = ServiceLookup
                .getTargetCheckoutResponseFacade();
        targetCheckoutResponseFacade.removeExistingPayment();
        final Response response;
        if (StringUtils.isEmpty(result)) {
            response = targetCheckoutResponseFacade.setIpgPaymentMode(
                    paymentMethod, "return-spc");
        }
        else {
            //by passing weird string it will return failed response
            response = targetCheckoutResponseFacade
                    .setIpgPaymentMode(
                            result, "return-spc");
        }
        Checkout.getInstance().setPaymentMethod(paymentMethod);
        Checkout.getInstance().setLastResponse(response);
    }

    @Given("^POS price update occurs in checkout$")
    public void updatePrice(final List<PriceData> entries) {
        Checkout.getInstance().setPosPriceUpdateInCheckout(entries);
    }

    @Given("^customer presents tmd '(.*)'$")
    public void setTMD(final String tmdCardType) {
        Checkout.getInstance().setTmdCardType(tmdCardType);
    }

    @Given("^customer apply tmd '(.*)'$")
    public void applyTMD(final String tmdCardType) {
        CheckoutUtil.setTeamMemberDiscount(tmdCardType);
    }

    @Given("^customer presents flybuys card '(.*)'$")
    public void setFlybuysNumber(final String flybuysNumber) {
        Checkout.getInstance().setFlybuysNumber(flybuysNumber);
    }

    @Given("^a customer has provided '(.*)' payment details for a cart$")
    public void createCartForCustomerGivenPaymentTypeAndPlatform(final String paymentMethod) throws Exception {

        Checkout.getInstance().setPaymentMethod(lowercaseAndNospaces(paymentMethod));

        // Create default cart
        givenAnyCart();
    }

    @Given("^customer presents voucher '(.*)'$")
    public void setVoucher(final String voucher) {
        Checkout.getInstance().setVoucher(voucher);
    }

    @Given("^a cart with entries with weight:$")
    public void aCartWithEntriesWithWeight(final List<CartEntry> entries)
            throws CommerceCartModificationException, CalculationException {

        for (final CartEntry entry : entries) {
            ProductUtil.setProductWeight(entry.getProduct(), entry.getWeight());
            final int quantity = entry.getQty() == 0 ? 1 : entry.getQty();
            CartUtil.addToCart(entry.getProduct(), quantity);
        }

        recalculateSessionCart();
    }

    @Given("^Persistent cart exists with successful payment and order conversion failed$")
    public void createPersistentCartWhoseOrderCreationFailed() throws Exception {
        SessionUtil.userLogin("test.user1@target.com.au");
        // Create default cart
        givenAnyCart();
        Checkout.getInstance().setPaymentMethod(lowercaseAndNospaces(CheckoutUtil.CREDITCARD_MODE_CODE));
        Checkout.getInstance().setSomethingWrongWithOrderCreation(true);
        // Similar to placeOrder but we expect the order creation to fail
        new OrderSteps().submitOrder();
        final CartModel cartModel = CartUtil.getSessionCartModel();
        cartCode = cartModel.getCode();
        SessionUtil.removeSession();
    }

    @When("^select paypal payment in spc")
    public void setPaypalPaymentMethod() {
        final String payPalReturnUrl = "https://www.target.com.au/checkout/payment-details/paypal-return";
        final String payPalCancelUrl = "https://www.target.com.au/checkout/payment-details";
        final Response response = ServiceLookup.getCheckoutResponseFacade().setPaypalPaymentMode(payPalReturnUrl,
                payPalCancelUrl);
        Checkout.getInstance().setLastResponse(response);
    }

    @When("^the payment mode is '(.*)'$")
    public void setAfterpayOrZipPaymentMethod(final String paymentMode) {
        Response response;
        if (ZIP_PAYMENT_MODE.equalsIgnoreCase(paymentMode)) {
            response = ServiceLookup.getCheckoutResponseFacade()
                    .setZippayPaymentMode("http://www.target.com.au/payment-details/zippay-return/spc");
        }
        else {
            response = ServiceLookup.getCheckoutResponseFacade().setAfterpayPaymentMode(
                    "https://www.target.com.au/checkout/payment-details/paypal-return",
                    "https://www.target.com.au/spc/order");
        }
        Checkout.getInstance().setLastResponse(response);
    }

    @Then("^the paypal reponse is successful")
    public void validatePaypalReturnURL() {
        final Response response = Checkout.getInstance().getLastResponse();
        assertThat(response.isSuccess()).isTrue();
        final BaseResponseData baseResponseData = response.getData();
        assertThat(baseResponseData).isInstanceOf(PaymentResponseData.class);
        final PaymentResponseData paymentResponseData = (PaymentResponseData)baseResponseData;
        assertThat(paymentResponseData.getPaypalSessionToken()).isNotEmpty();
    }

    @Then("^the Afterpay reponse is successful")
    public void validateAfterpaySessionToken() {
        final Response response = Checkout.getInstance().getLastResponse();
        assertThat(response.isSuccess()).isTrue();
        final BaseResponseData baseResponseData = response.getData();
        assertThat(baseResponseData).isInstanceOf(PaymentResponseData.class);
        final PaymentResponseData paymentResponseData = (PaymentResponseData)baseResponseData;
        assertThat(paymentResponseData.getAfterpaySessionToken()).isNotEmpty();
    }

    @Then("^the Afterpay reponse is unsuccessful")
    public void validateAfterpayFailure() {
        final Response response = Checkout.getInstance().getLastResponse();
        assertThat(response.isSuccess()).isFalse();
        final BaseResponseData baseResponseData = response.getData();
        assertThat(baseResponseData.getError()).isNotNull();
        assertThat(baseResponseData.getError().getCode()).isEqualTo("ERR_AFTERPAY_UNAVAILABLE");
    }

    @When("^customer presents flybuys card '(.*)' in spc$")
    public void setFlybuysNumberSPC(final String flybuysNumber) {
        ServiceLookup.getCheckoutResponseFacade().applyFlybuys(flybuysNumber);
    }

    @Then("^cart does not contain flybuys number$")
    public void getFlybuysNumberSPCInValid() {
        final Response response = ServiceLookup.getCheckoutResponseFacade().getCartDetail();
        final CartDetailResponseData cartDetails = (CartDetailResponseData)response.getData();
        assertThat(cartDetails.getFlybuysData()).isNull();
    }

    @Then("^cart does not contain flybuys discount$")
    public void getFlybuysDiscountSPCInValid() {
        assertThat(VoucherUtil.isFlybuysDiscountInCart()).isFalse();
    }

    @Then("^cart contains flybuys number '(.*)'$")
    public void getFlybuysNumberSPCValid(final String flybuysNumber) {
        final Response response = ServiceLookup.getCheckoutResponseFacade().getCartDetail();
        final CartDetailResponseData cartDetails = (CartDetailResponseData)response.getData();
        assertThat(cartDetails.getFlybuysData().getCode()).isEqualTo(flybuysNumber);
    }

    @Then("^cart constains flybuys canRedeemPoints '(.*)'$")
    public void canRedeemFlybuysPoints(final String canRedeemFlybuysPoints) {
        final Response response = ServiceLookup.getCheckoutResponseFacade().getCartDetail();
        final CartDetailResponseData cartDetails = (CartDetailResponseData)response.getData();
        assertThat(cartDetails.getFlybuysData().isCanRedeemPoints()).isEqualTo(
                canRedeemFlybuysPoints.equals("true"));
    }

    @Then("^new cart is created$")
    public void verifyNewCartLoaded() {
        assertThat(cartCode).isNotEqualTo(newCartCode);
    }

    @Then("^customer goes to basket page$")
    public void getbasketPageData() {
        final Response response = ServiceLookup.getCheckoutResponseFacade().getCartDetail();
        Checkout.getInstance().setLastResponse(response);
    }

    @Given("^Persistent cart exists with no payments$")
    public void createPersistentCartwithNoPayment() throws Exception {
        SessionUtil.userLogin("test.user1@target.com.au");
        // Create default cart
        givenAnyCart();
        //get cart code
        cartCode = CartUtil.getSessionCartCode();
        SessionUtil.removeSession();
    }

    @When("^user logs in to storefront$")
    public void whenUserlogsIn() {
        SessionUtil.userLogin("test.user1@target.com.au");
        //get cart code
        newCartCode = CartUtil.getSessionCartCode();

    }

    @When("^customer checkout is started|a soh check is done on the cart$")
    public void loadYourAddressPage() throws CommerceCartModificationException {
        final CartModel checkoutCart = CheckoutUtil.getCheckoutCart();
        cartModifications = ServiceLookup.getTargetCommerceCartService().performSOH(checkoutCart);
    }

    @Then("^cart modification quantities are:$")
    public void verifyCartModificationQuantities(final List<Long> expectedQuantities) {
        assertThat(cartModifications).isNotEmpty();
        assertThat(cartModifications).onProperty("quantity").containsExactly(expectedQuantities.toArray());
    }

    @Then("^cart modification statuses are:$")
    public void verifyCartModificationStatuses(final List<String> expectedStatuses) {
        assertThat(cartModifications).isNotEmpty();
        assertThat(cartModifications).onProperty("statusCode").containsExactly(expectedStatuses.toArray());
    }

    @When("^the product '(.*)' is out of stock$")
    public void setProductOutOfStock(final String productCode) throws CommerceCartModificationException {
        CartUtil.updateProductOutOfStock(productCode);
    }

    @When("^the product '(.*)' is out of stock before place order$")
    public void setProductOutOfStockBeforePlaceOrder(final String productCode) {
        Checkout.getInstance().setProductOutOfStockBeforePlaceOrder(productCode);
    }

    @When("^set fastline stock before place order:$")
    public void setFastlineStockForProductBeforePlaceOrder(final List<StockLevelData> stockLevel) {
        Checkout.getInstance().setBeforePlaceOrderStockLevel(stockLevel);
    }

    @When("^the cart has product '(.*)'$")
    public void cartContainsProduct(final String product) {
        boolean productExists = false;
        final CartModel cart = CheckoutUtil.getCheckoutCart();
        final List<AbstractOrderEntryModel> orderEntries = cart.getEntries();
        if (CollectionUtils.isNotEmpty(orderEntries)) {
            for (final AbstractOrderEntryModel orderEntry : orderEntries) {
                if (orderEntry.getProduct().getCode().equals(product)) {
                    productExists = true;
                    break;
                }
            }
        }
        org.junit.Assert.assertEquals(TRUE, valueOf(productExists));
    }

    @Then("^persistent cart is loaded$")
    public void verifyPersistanCartLoaded() {
        assertThat(cartCode).isEqualTo(newCartCode);
    }

    @Given("^Persistent cart exists with an unsuccessful payment$")
    public void createPersistantCartWithUnsuccessfulPayment() throws Exception {

        SessionUtil.userLogin("test.user1@target.com.au");
        // Create default cart
        givenAnyCart();
        Checkout.getInstance().setPaymentMethod(lowercaseAndNospaces(CheckoutUtil.CREDITCARD_MODE_CODE));
        // Will return DENIED ONCE THEN REJECTED
        ServiceLookup.getTargetMockCcpayment().setMockResponse("DENIED", 1, "REJECTED");
        //get cart code
        cartCode = CartUtil.getSessionCartCode();
        SessionUtil.removeSession();

    }

    @When("^apply voucher to cart (.*)$")
    public void whenApplyVoucher(final String voucherCode) throws CalculationException {

        // This applies voucher to session cart
        VoucherUtil.applyVoucher(voucherCode, CartUtil.getSessionCartModel());
        recalculateSessionCart();
    }

    @When("^apply voucher (.*) to cart in spc$")
    public void whenApplyVoucherInSPC(final String voucherCode) {
        final Response response = ServiceLookup.getCheckoutResponseFacade().applyVoucher(voucherCode);
        Checkout.getInstance().setLastResponse(response);
    }

    @When("^remove voucher in spc$")
    public void whenRemoveVoucherInSPC() {
        ServiceLookup.getCheckoutResponseFacade().removeVoucher();
    }

    @When("^the customer redeems (\\d+) flybuys points$")
    public void whenCustomerSelectsFlybuysRedeem(final int points) {
        ServiceLookup.getFlybuysclientMockService().setMockResponse("SUCCESS", CheckoutUtil.AVAILABLE_FLYBUYSPOINTS,
                "SUCCESS", "SUCCESS");
        Checkout.getInstance().setFlybuysNumber(CheckoutUtil.FLYBUYS_CODE);
        Checkout.getInstance().setFlyBuysPoints(points);

    }

    /**
     * product need to be set as 10000011 price 5
     * 
     * @param amount
     *            can be 10 or 20
     * @throws CommerceCartModificationException
     */
    @When("^the customer decreases cart value below (\\d+) dollars")
    public void whenCustomerDecreasesCartValue(@SuppressWarnings("unused") final int amount)
            throws CommerceCartModificationException {
        int quantity = 0;
        if (amount == 10) {
            quantity = 1;
        }
        else if (amount == 20) {
            quantity = 3;
        }
        CartUtil.updateFromCart(ANY_PRD_CODE, quantity);
    }

    @When("^customer adds items increasing cart value to (\\d+) dollars")
    public void whenCustomerAddsItemsIncreasingCartValue(@SuppressWarnings("unused") final int dummyAmount)
            throws CommerceCartModificationException {
        CartUtil.addToCart(ANY_PRD_CODE, 5);
    }

    @Then("^cart total is (.*)$")
    public void thenCheckTotal(final Double total) {
        assertThat(getSessionCartData().getTotalPrice().getValue().doubleValue()).isEqualTo(total);
    }

    @Then("^cart delivery cost is (.*)$")
    public void thenCheckCartDelivery(final double del) {
        assertThat(getSessionCartData().getDeliveryCost().getValue()).isEqualTo(BigDecimal.valueOf(del));
    }

    @When("^get cart data$")
    public void getCartData() {

        if (cartData == null) {
            cartData = getSessionCartData();
        }

        assertThat(cartData).isInstanceOf(TargetCartData.class);
    }

    @Then("^physical cart delivery modes are:$")
    public void checkCartDeliveryModes(final List<DeliveryModeAvailability> delModes) {

        getCartData();
        checkDeliveryModes(delModes, cartData.getDeliveryModes());
    }

    @Then("^(:?cart|physical cart) delivery modes are empty$")
    public void checkCartDeliveryModesEmpty() {

        getCartData();
        assertThat(cartData.getDeliveryModes());
    }

    @Then("^digital cart delivery modes are:$")
    public void checkDigitalCartDeliveryModes(final List<DeliveryModeAvailability> delModes) {

        getCartData();
        checkDeliveryModes(delModes, cartData.getDigitalDeliveryModes());
    }

    @Then("^digital cart delivery modes are empty$")
    public void checkDigitalCartDeliveryModesEmpty() {

        getCartData();
        assertThat(cartData.getDigitalDeliveryModes());
    }

    @Then("^only the digital-gift-card delivery mode is available on the cart$")
    public void checkOnlyDigitalDeliveryModeShows() {

        checkCartDeliveryModesEmpty();
        final DeliveryModeAvailability delMode = new DeliveryModeAvailability("digital-gift-card", true);
        checkDigitalCartDeliveryModes(Collections.singletonList(delMode));
    }

    @Then("^cart has subtotals:$")
    public void validateTotals(final List<OrderFields> orderFields) {
        assertThat(orderFields.size()).isEqualTo(1);

        final TargetCartData checkoutCart = (TargetCartData)ServiceLookup.getTargetCheckoutFacade().getCheckoutCart();

        final OrderFields fields = orderFields.get(0);

        if (StringUtils.isNotEmpty(fields.getTotal())) {
            verifyPriceData(checkoutCart.getTotalPrice().getValue(), fields.getTotal());
        }

        if (StringUtils.isNotEmpty(fields.getSubtotal())) {
            verifyPriceData(checkoutCart.getSubTotal().getValue(), fields.getSubtotal());
        }

        final String deliveryCost = fields.getDeliveryCost();
        if (StringUtils.isNotEmpty(deliveryCost) && !StringUtils.equals(deliveryCost, "0.0")) {
            verifyPriceData(checkoutCart.getDeliveryCost().getValue(), deliveryCost);
        }
        else {
            assertThat(checkoutCart.getDeliveryCost()).isNull();
        }

    }

    @And("^adding product '(.*)' with quantity '(.*)' returns '(.*)'$")
    public void mixedBasketStatus(final String productCode, final String qty, final String expectedStatus)
            throws NumberFormatException, CommerceCartModificationException {
        final CartModificationData result = CartUtil.addToCart(productCode, Long.parseLong(qty));

        assertThat(result.getStatusCode()).isEqualTo(expectedStatus);
    }

    @When("^product '(.*)' with quantity '(.*)' is added$")
    public void addToCart(final String productCode, final long qty) throws CommerceCartModificationException {
        cartModificationData = CartUtil.addToCart(productCode, qty);
    }

    @When("^product '(.*)' is updated with quantity '(.*)'$")
    public void updateCart(final String productCode, final long qty) throws CommerceCartModificationException {
        cartModificationData = CartUtil.updateFromCart(productCode, qty);
    }

    @Then("cart modification status is '(.*)'")
    public void verifyCartModificationStatus(final String expectedStatus) {
        assertThat(cartModificationData.getStatusCode()).isEqualTo(expectedStatus);
    }

    @Given("^the cart is empty$")
    public void emptyCart() {
        final CartModel cartModel = CartUtil.getSessionCartModel();
        assertThat(cartModel.getEntries()).isEmpty();
    }

    @Then("^adding product '(.*)' fails$")
    public void preOrderProductNoStartDateAndEndDate(final String productCode) {
        try {
            CartUtil.addToCart(productCode, 1L);
        }
        catch (final CommerceCartModificationException e) {
            assertThat(e.getMessage()).isEqualTo(TgtCoreConstants.CART.ERR_PREORDER_CART);
            return;
        }
        fail();
    }

    @Given("^the product '(.*)' has reached max preOrder quantity$")
    public void maxPreOrderQuantityReached(final String productCode) {

        final StockLevelModel stockLevel = StockLevelUtil.getFastlineStockFor(productCode);
        final int maxPreOrder = stockLevel.getMaxPreOrder();
        final int preOrder = stockLevel.getPreOrder();
        assertThat(maxPreOrder).isEqualTo(preOrder)
                .overridingErrorMessage("Product max preOrder limit reached.");
    }

    @Then("^cart delivery address is (.*)$")
    public void isDeliveryAddressEmpty(final String empty) {
        final TargetCartData checkoutCart = (TargetCartData)ServiceLookup.getTargetCheckoutFacade().getCheckoutCart();
        Assert.notNull(checkoutCart, "Session cart is null");
        if (empty.equals("empty")) {
            org.junit.Assert.assertNull(checkoutCart.getDeliveryAddress());
        }
        else if (empty.equals("not empty")) {
            org.junit.Assert.assertNotNull(checkoutCart.getDeliveryAddress());
        }
    }

    @Then("^cart cnc store is empty$")
    public void isCNCStoreEmpty() {
        final TargetCartData sessionCart = getSessionCartData();
        Assert.notNull(sessionCart, "Session cart is null");
        org.junit.Assert.assertNull(sessionCart.getCncStoreNumber());
    }

    @Then("^cart has empty delivery mode$")
    public void isDeliveryModeEmpty() {
        final TargetCartData sessionCart = getSessionCartData();
        Assert.notNull(sessionCart, "Session cart is null");
        org.junit.Assert.assertNull(sessionCart.getDeliveryMode());
    }

    @Then("^available delivery modes are (.*)$")
    public void checkDeliveryModesAvailable(final List<String> deliveryModesToCompare) {
        Assert.isTrue(!CollectionUtils.isEmpty(deliveryModesToCompare),
                "Delivery modes to compare cannot be empty or null");
        final TargetCartData sessionCart = getSessionCartData();
        Assert.notNull(sessionCart, "Session cart is null");
        final List<TargetZoneDeliveryModeData> deliveryModes = sessionCart.getDeliveryModes();
        for (final String availableValue : deliveryModesToCompare) {
            org.junit.Assert.assertTrue(isDeliveryModeAvailable(deliveryModes, availableValue.replace("'", "")));
        }
    }

    @Then("^available digital delivery modes are (.*)$")
    public void checkDigitalDeliveryModesAvailable(final List<String> deliveryModesToCompare) {
        final TargetCartData sessionCart = getSessionCartData();
        Assert.notNull(sessionCart, "Session cart is null");
        final List<TargetZoneDeliveryModeData> deliveryModes = sessionCart.getDigitalDeliveryModes();
        for (final String availableValue : deliveryModesToCompare) {
            org.junit.Assert.assertTrue(isDeliveryModeAvailable(deliveryModes, availableValue.replace("'", "")));
        }
    }

    @Then("^cart is empty$")
    public void checkIfCartEmpty() {
        final TargetCartData sessionCart = getSessionCartData();
        assertThat(sessionCart.getEntries()).isNullOrEmpty();
    }

    @Then("^the cart contains:$")
    public void verifycartProducts(final List<String> productCode) {
        assertThat(productCode).isNotNull().isNotEmpty();
        final TargetCartData sessionCart = getSessionCartData();
        final List<OrderEntryData> entries = sessionCart.getEntries();
        assertThat(entries).isNotNull().isNotEmpty().hasSize(productCode.size());
        for (final OrderEntryData entryData : entries) {
            assertThat(productCode.contains(entryData.getProduct().getCode())).isTrue();
        }
    }

    private boolean isDeliveryModeAvailable(final List<TargetZoneDeliveryModeData> deliveryModes,
            final String checkDeliveryMode) {
        for (final TargetZoneDeliveryModeData deliveryMode : deliveryModes) {
            if (deliveryMode.getCode().equals(checkDeliveryMode)) {
                return true;
            }
        }
        return false;
    }

    private void checkDeliveryModes(final List<DeliveryModeAvailability> delModes,
            final List<TargetZoneDeliveryModeData> actualModes) {

        for (final DeliveryModeAvailability wantedMode : delModes) {
            boolean found = false;
            for (final TargetZoneDeliveryModeData actualMode : actualModes) {
                if (actualMode.getCode().equals(wantedMode.getName())) {
                    assertThat(wantedMode.isAvailable())
                            .as("Availability incorrect for delivery mode " + wantedMode.getName())
                            .isEqualTo(actualMode.isAvailable());
                    found = true;
                    break;
                }
            }
            assertThat(found).as("Delivery mode " + wantedMode.getName() + " not found").isTrue();
        }
    }

    @Given("^a cart with entries '(.*)'$")
    public void givenCartWithEntriesAndQuantity(final String cartEntries)
            throws CommerceCartModificationException, CalculationException {
        CatalogUtil.setSessionCatalogVersion();
        final String[] splittedCartEntries = cartEntries.trim().split(",");
        for (final String entry : splittedCartEntries) {
            final String[] splittedEntry = entry.trim().split(DELIMITER_OF_FOR_ENTRY);
            final Long quantity = Long.valueOf(splittedEntry[0].trim());
            final String productCode = splittedEntry[1].trim();
            CartUtil.addToCart(productCode, quantity.longValue());
        }
        recalculateSessionCart();
    }

    @Given("^the billing address is '(.*)'$")
    public void setBillingAddress(final String billingAddress) {
        Checkout.getInstance().setBillingAddress(billingAddress);
    }

    @Then("^cart delivery mode is '(.*)'$")
    public void checkCartDeliveryMode(final String deliveryModeCode) {
        if (StringUtils.isEmpty(deliveryModeCode)) {
            assertThat(CheckoutUtil.getCheckoutCart().getDeliveryMode()).isNull();
        }
        else {
            assertThat(CheckoutUtil.getCheckoutCart().getDeliveryMode().getCode()).isEqualToIgnoringCase(
                    deliveryModeCode);
        }
    }

    @Then("^payment method selection is empty on the cart$")
    public void verifyNoPaymentOnCart() {
        assertThat(CheckoutUtil.getCheckoutCart().getPaymentInfo()).isNull();
    }

    @Then("^the items in the order summary are:$")
    public void verifyCartSummary(final List<CartEntry> expectedEntries) {

        final CartDetailResponseData responseData = (CartDetailResponseData)Checkout.getInstance().getLastResponse()
                .getData();
        final List<OrderEntryData> entries = responseData.getEntries();
        for (int i = 0; i < entries.size(); i++) {
            final OrderEntryData entry = entries.get(i);
            final CartEntry expectedEntry = expectedEntries.get(i);
            assertThat(entry.getQuantity()).isEqualTo(expectedEntry.getQty());
            assertThat(entry.getTotalPrice().getValue().doubleValue()).isEqualTo(
                    expectedEntry.getTotalPrice());
            final TargetProductData product = (TargetProductData)entry.getProduct();
            assertThat(product.getName()).isEqualTo(expectedEntry.getProductHeading());
            assertThat(product.getSize() == null ? "" : product.getSize())
                    .isEqualTo(expectedEntry.getSize());
            assertThat(product.getSwatch() == null ? "" : product.getSwatch()).isEqualTo(
                    expectedEntry.getColour());
            assertThat(product.getImageUrl() == null ? "" : product.getImageUrl()).isEqualTo(
                    expectedEntry.getImage());
            assertThat(product.getBrand() == null ? "" : product.getBrand()).isEqualTo(
                    expectedEntry.getBrand());
            assertThat(product.getBaseProductCode() == null ? "" : product.getBaseProductCode()).isEqualTo(
                    expectedEntry.getBaseProductCode());
            assertThat(product.getBaseName() == null ? "" : product.getBaseName()).isEqualTo(
                    expectedEntry.getBaseProductName());
            assertThat(product.getTopLevelCategory() == null ? "" : product.getTopLevelCategory())
                    .isEqualTo(
                            expectedEntry.getTopLevelCategory());
            if (expectedEntry.isAssorted()) {
                assertThat(product.isAssorted()).isTrue();
            }


        }
    }

    /**
     * Method to verify initial deposit amount for the pre-order product in the cart summary page.
     *
     * @param expectedPreOrderEntries
     */
    @Then("^the (?:cart summary|basket page) for (?:pre-order|normal) product includes:$")
    public void verifyPreOrderCartSummary(final List<CartEntry> expectedPreOrderEntries) {
        final CartDetailResponseData responseData = (CartDetailResponseData)Checkout.getInstance().getLastResponse()
                .getData();
        final List<OrderEntryData> orderEntryDatas = responseData.getEntries();
        for (int i = 0; i < orderEntryDatas.size(); i++) {
            final OrderEntryData entry = orderEntryDatas.get(i);
            final CartEntry expectedCartEntry = expectedPreOrderEntries.get(i);
            if (expectedCartEntry.getTotalPrice() != null) {
                assertThat(entry.getTotalPrice().getValue().doubleValue()).isEqualTo(expectedCartEntry.getTotalPrice());
            }
            if (expectedCartEntry.getProduct() != null) {
                final TargetProductData product = (TargetProductData)entry.getProduct();
                assertThat(product.getCode()).isEqualTo(expectedCartEntry.getProduct());
            }
            if (expectedCartEntry.getPreOrderDepositAmount() != null) {
                assertThat(responseData.getPreOrderDepositAmount().getValue())
                        .isEqualTo(expectedCartEntry.getPreOrderDepositAmount());
            }
            if (expectedCartEntry.getPreOrderOutstandingAmount() != null) {
                assertThat(responseData.getPreOrderOutstandingAmount().getValue())
                        .isEqualTo(expectedCartEntry.getPreOrderOutstandingAmount());
            }
        }
    }

    @Then("^the items in the cart are:$")
    public void verifyCartItems(final List<CartEntry> expectedEntries) {

        final List<OrderEntryData> entries = getSessionCartData().getEntries();
        for (int i = 0; i < entries.size(); i++) {
            final OrderEntryData entry = entries.get(i);
            final CartEntry expectedEntry = expectedEntries.get(i);
            assertThat(entry.getQuantity()).isEqualTo(expectedEntry.getQty());
            assertThat(entry.getBasePrice().getValue().doubleValue()).isEqualTo(
                    expectedEntry.getPrice());
            final TargetProductData product = (TargetProductData)entry.getProduct();
            assertThat(product.getCode()).isEqualTo(expectedEntry.getProduct());
        }
    }

    @Then("^price information for items in the order summary are:$")
    public void verifyCartSummaryPrices(final List<CartEntry> expectedEntries) {

        final CartDetailResponseData responseData = (CartDetailResponseData)Checkout.getInstance().getLastResponse()
                .getData();
        final List<OrderEntryData> entries = responseData.getEntries();
        for (int i = 0; i < entries.size(); i++) {
            final TargetOrderEntryData entry = (TargetOrderEntryData)entries.get(i);
            final CartEntry expectedEntry = expectedEntries.get(i);

            assertThat(entry.getBasePrice().getValue().doubleValue()).isEqualTo(expectedEntry.getPrice());
            assertThat(entry.getTotalPrice().getValue().doubleValue()).isEqualTo(expectedEntry.getTotalPrice());
            assertThat(entry.getTotalItemPrice().getValue().doubleValue()).isEqualTo(expectedEntry.getTotalItemPrice());
            if (entry.getDiscountPrice() != null) {
                assertThat(entry.getDiscountPrice().getValue().doubleValue()).isEqualTo(expectedEntry.getDiscount());
            }
            assertThat(entry.isDealsApplied()).isEqualTo(expectedEntry.isDealsApplied());

        }
    }


    @Then("^cart summary is:$")
    public void verifyCartSummaryDetails(final List<OrderFields> orderFields) {
        final CartDetailResponseData responseData = (CartDetailResponseData)ServiceLookup.getCheckoutResponseFacade()
                .getCartSummary().getData();
        final OrderFields orderField = orderFields.get(0);

        if (StringUtils.isNotEmpty(orderField.getTotalTax())) {
            verifyPriceData(responseData.getGst().getValue(), orderField.getTotalTax());
        }
        verifyPriceData(responseData.getTotal().getValue(), orderField.getTotal());
        verifyPriceData(responseData.getOrderDiscounts().getValue(), orderField.getTotalDiscounts());
        verifyPriceData(responseData.getSubtotal().getValue(), orderField.getSubtotal());
        if (orderField.getPointsConsumed() != null) {
            assertThat(responseData.getFlybuysDiscountData().getPointsConsumed()).isEqualTo(
                    orderField.getPointsConsumed());
        }
        if (orderField.getContainsDigitalEntriesOnly() != null) {
            assertThat(responseData.getContainsDigitalEntriesOnly()).isEqualTo(
                    orderField.getContainsDigitalEntriesOnly());
        }
        final String deliveryCost = orderField.getDeliveryCost();
        if (StringUtils.isNotEmpty(deliveryCost) && !StringUtils.equals(deliveryCost, "0.0")) {
            verifyPriceData(responseData.getDeliveryFee().getValue(), deliveryCost);
        }
    }

    @Then("^voucher is '(.*)'$")
    public void verifyCartPrices(final String voucher) {
        final CartDetailResponseData responseData = (CartDetailResponseData)Checkout.getInstance().getLastResponse()
                .getData();
        assertThat(responseData.getVoucher().getCode()).isEqualTo(voucher);
    }

    @Then("^the address '(.*)' is '(.*)' to the cart$")
    public void verifyAddressinCart(final String address, final String status) {
        if (("Saved").equalsIgnoreCase(status)) {
            final CartModel cartModel = ServiceLookup.getTargetCheckoutFacade().getCart();
            Assert.notNull(cartModel.getDeliveryAddress(), "Delivery address must not be null");
            final String[] addressFragments = address.split(",");
            assertThat(addressFragments[0]).isEqualTo(cartModel.getDeliveryAddress().getLine1());
            assertThat(addressFragments[1]).isEqualTo(cartModel.getDeliveryAddress().getTown());
            assertThat(addressFragments[2]).isEqualTo(cartModel.getDeliveryAddress().getPostalcode());
        }
        else if (("Not Saved").equalsIgnoreCase(status)) {
            final CartModel cartModel = ServiceLookup.getTargetCheckoutFacade().getCart();
            Assert.isNull(cartModel.getDeliveryAddress(), "Delivery address must be null");
            final Response response = Checkout.getInstance().getLastResponse();
            assertThat(response.isSuccess()).isEqualTo(Boolean.FALSE);
        }
        else {
            throw new IllegalArgumentException("Status not recognized: " + status);
        }
    }

    @Then("^the error code is '(.*)'$")
    public void verifyErrorMessage(final String errorMessage) {
        final Response response = Checkout.getInstance().getLastResponse();
        if (("N/A").equalsIgnoreCase(errorMessage)) {
            assertThat(response.isSuccess()).isEqualTo(Boolean.TRUE);
        }
        else {
            assertThat(response.isSuccess()).isEqualTo(Boolean.FALSE);
            assertThat(response.getData().getError()).isNotNull();
            assertThat(response.getData().getError().getCode()).isEqualTo(errorMessage);

        }
    }


    @Then("^delivery info on the cart is cleaned$")
    public void verifyNoDeliveryInfoOnCart() {
        final CartModel cartModel = CheckoutUtil.getCheckoutCart();
        assertThat(cartModel.getDeliveryMode()).isNull();
        assertThat(cartModel.getDeliveryAddress()).isNull();
    }

    @Then("^shipster attributes on cart are:$")
    public void verifyShipsterAttributeOnCart(final List<ShipsterCartData> shipsterData) {
        final CartModel cartModel = CheckoutUtil.getCheckoutCart();
        if (shipsterData.get(0).getOriginalDeliveryCost() != null) {
            assertThat(cartModel.getOriginalDeliveryCost()).isEqualTo(
                    Double.parseDouble(shipsterData.get(0).getOriginalDeliveryCost()));
        }
        if (shipsterData.get(0).getAusPostDeliveryClubFreeDelivery() != null) {
            assertThat(cartModel.getAusPostDeliveryClubFreeDelivery())
                    .isEqualTo(shipsterData.get(0).getAusPostDeliveryClubFreeDelivery());
        }
        if (shipsterData.get(0).getAusPostClubMember() != null) {
            assertThat(cartModel.getAusPostClubMember())
                    .isEqualTo(shipsterData.get(0).getAusPostClubMember());
        }
    }

    @When("^customer removes flybuys in spc$")
    public void whenRemoveFlybuysInSPC() {
        ServiceLookup.getCheckoutResponseFacade().removeFlybuys();
    }

    @Then("cart afterpay installment price is '(.*)'")
    public void verifyAfterpayInstallmentPrice(final String expectedPrice) {
        assertThat(cartData.getInstallmentPrice().getFormattedValue()).isEqualTo(expectedPrice);
    }

    @Then("cart excludeForAfterpay is '(.*)'")
    public void verifyAfterpayExcludeFlag(final Boolean expected) {
        assertThat(cartData.isExcludeForAfterpay()).isEqualTo(expected);
    }

    @Then("cart excludeForZipPayment is '(.*)'")
    public void verifyZipPaymentExcludeFlag(final Boolean expected) {
        assertThat(cartData.isExcludeForZipPayment()).isEqualTo(expected);
    }

    @Then("^customer is not allowed to apply voucher$")
    public void verifyErrorWhileApplyingVoucher() {
        final Response lastResponse = Checkout.getInstance().getLastResponse();
        assertThat(lastResponse.getData().getError().getCode()).isEqualTo("ERR_VOUCHER_NOT_AVAILABLE_FOR_PREORDER");
        assertThat(lastResponse.getData().getError().getMessage())
                .isEqualTo("Promo Code not available for pre-orders.");
    }

    @And("^preOrder attributes '(.*)' in cart")
    public void verifyPreOrderAttributes(final String checkAttributes) {
        final CartModel cartModel = CartUtil.getSessionCartModel();

        if ("notpresent".equals(checkAttributes)) {
            //assert all preOrder attributes which are not present 
            assertThat(cartModel.getContainsPreOrderItems()).isFalse();
            assertThat(cartModel.getNormalSaleStartDateTime()).isNull();
            assertThat(cartModel.getPreOrderDepositAmount()).isEqualTo(Double.valueOf(0d));
        }
        else {
            //assert preOrder attributes which are present
            assertThat(cartModel.getContainsPreOrderItems()).isTrue();
            assertThat(cartModel.getNormalSaleStartDateTime()).isNotNull();
            assertThat(cartModel.getPreOrderDepositAmount()).isEqualTo(Double.valueOf(10d));
        }
    }

    private void recalculateSessionCart() throws CalculationException {
        CartUtil.recalculateSessionCart();
    }

    private TargetCartData getSessionCartData() {

        return CartUtil.getSessionCartData();
    }

    private String lowercaseAndNospaces(final String in) {
        return in.toLowerCase().replaceAll(" ", "");
    }

    /**
     * @param actual
     * @param expected
     */
    private void verifyPriceData(final BigDecimal actual, final String expected) {
        assertThat(Math.abs(actual.doubleValue())).isEqualTo(Double.parseDouble(expected));
    }

    private PaymentMethodData getPaymentMethodData(final String paymentMethod,
            final List<PaymentMethodData> paymentMethods) {
        for (final PaymentMethodData paymentMode : paymentMethods) {
            if (paymentMode.getName().equals(paymentMethod)) {
                return paymentMode;
            }
        }
        return null;
    }
}
