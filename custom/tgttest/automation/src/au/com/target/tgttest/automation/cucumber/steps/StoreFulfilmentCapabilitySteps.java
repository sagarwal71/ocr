/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.fest.assertions.Assertions;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.StoreFulfilmentCapabilitiesUtil;
import au.com.target.tgttest.automation.facade.bean.ExclusionData;
import au.com.target.tgttest.automation.facade.bean.StoreFulfilmentCapabilitiesInfo;
import au.com.target.tgttest.automation.mock.MockTargetStoreStockService;
import au.com.target.tgttest.automation.util.ComplexDateExpressionParser;
import cucumber.api.java.en.Given;


/**
 * Fulfillment steps
 * 
 * @author jjayawa1
 *
 */
public class StoreFulfilmentCapabilitySteps {


    @Given("^stores with fulfilment (?:capability|capability and stock):$")
    public void givenStoresWithFulfilmentCapability(final List<StoreFulfilmentCapabilitiesInfo> stores) {

        final MockTargetStoreStockService targetStoreStockService = ServiceLookup.getTargetStoreStockService();

        for (final StoreFulfilmentCapabilitiesInfo storeData : stores) {

            // Demand store and enabled field is supplied
            Assertions.assertThat(storeData.getStore()).as("store number/name").isNotEmpty();
            Assertions.assertThat(storeData.getInstoreEnabled()).as("store enabled").isNotEmpty();

            // Get the store number and validate state if supplied
            final Integer storeNumber = getStoreNumber(storeData.getStore(), storeData.getState());

            // Mock stock response if stock supplied
            if (StringUtils.isNotEmpty(storeData.getInStock())) {
                targetStoreStockService.setActive(true);
                targetStoreStockService.addMockData(storeNumber,
                        Boolean.valueOf("yes".equalsIgnoreCase(storeData.getInStock())));
            }

            StoreFulfilmentCapabilitiesUtil.setFulfilmentCapabilitiesForStore(storeNumber.intValue(), storeData);
        }
    }

    @Given("^the daily maximum consignments for Robina is '(.*)'$")
    public void givenStoresWithOrderLimit(final String maxAllowedOrdersPerDay) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        StoreFulfilmentCapabilitiesUtil.setStoreWithMaxOrderLimit(7126, maxAllowedOrdersPerDay);
    }

    @Given("^the global start time for the order limit is '(.*)'$")
    public void givenGlobalStartTimeForOrderLimit(final String timeWithOffset) {
        final ComplexDateExpressionParser dateUtil = new ComplexDateExpressionParser();
        final Date date = dateUtil.interpretStringAsDate(timeWithOffset);
        final Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        final String globalStartTime = calendar.get(Calendar.HOUR_OF_DAY) + ":"
                + calendar.get(Calendar.MINUTE);

        StoreFulfilmentCapabilitiesUtil.setGlobalStartTimeForOrderLimit(globalStartTime);
    }

    /**
     * Method to get Store number using the store name or return the store number as a integer
     * 
     * @param store
     * @param state
     * @return storeNumber
     */
    public Integer getStoreNumber(final String store, final String state) {
        if (store.matches("^[0-9]+$")) {
            return Integer.valueOf(store);
        }
        else {
            final TargetPointOfServiceService targetPointOfServiceService = ServiceLookup
                    .getTargetPointOfServiceService();
            final TargetPointOfServiceModel pointOfService = (TargetPointOfServiceModel)targetPointOfServiceService
                    .getPointOfServiceForName(store);
            if (StringUtils.isNotBlank(state)) {
                final AddressModel address = pointOfService.getAddress();
                Assertions.assertThat(address).isNotNull();
                Assertions.assertThat(address.getDistrict()).isNotNull().isNotEmpty().isEqualTo(state);
            }
            return pointOfService.getStoreNumber();
        }
    }

    @Given("^product weight limit for store delivery for '(.*)' is (.*) kg$")
    public void productWeightLimitForStore(final String store, final String weight)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        final Integer storeNumber = getStoreNumber(store, null);
        StoreFulfilmentCapabilitiesUtil.setProductWeightLimitForStore(storeNumber.intValue(), weight);
    }

    @Given("^a store '(\\d+)' with fulfilment capability and stock$")
    public void givenAStoreWithFulfilmentCapability(final int storeNumber) {
        final StoreFulfilmentCapabilitiesInfo storeData = new StoreFulfilmentCapabilitiesInfo();
        storeData.setStore(Integer.toString(storeNumber));
        storeData.setInstoreEnabled("yes");
        storeData.setInStock("yes");
        givenStoresWithFulfilmentCapability(Collections.singletonList(storeData));
    }

    @Given("^blackout for store is disabled$")
    public void blackoutForStoreIsDisabled() {

        StoreFulfilmentCapabilitiesUtil.setGlobalBlackout(null, null);
    }

    @Given("^System-wide blackout start time is '(NOT POPULATED|EXACTLY NOW|BEFORE NOW|AFTER NOW)' and end time is '(NOT POPULATED|EXACTLY NOW|BEFORE NOW|AFTER NOW)'$")
    public void systemWideBlackoutStartTimeAndEndTime(final String blackoutStartDate, final String blackoutEndDate) {
        final Date startDate = getDateValue(blackoutStartDate, Calendar.getInstance(), 3, -3);
        final Date endDate = getDateValue(blackoutEndDate, Calendar.getInstance(), 5, -1);
        StoreFulfilmentCapabilitiesUtil.setGlobalBlackout(startDate, endDate);
    }

    @Given("^System-wide max number of routing attempts is '(.*)'$")
    public void systemMaxRoutingAttempts(final int routingAttempts) {

        StoreFulfilmentCapabilitiesUtil.setGlobalMaxRoutingAttempts(routingAttempts);
    }

    /**
     * Method to get the date value depending on the scenario
     * 
     * @param date
     * @return Date
     */
    private Date getDateValue(final String date, final Calendar currentDate, final int daysAhead,
            final int daysBehind) {
        Date dateValue = null;
        switch (date) {
            case "EXACTLY NOW":
                dateValue = currentDate.getTime();
                break;
            case "BEFORE NOW":
                currentDate.add(Calendar.DATE, daysBehind);
                dateValue = currentDate.getTime();
                break;
            case "AFTER NOW":
                currentDate.add(Calendar.DATE, daysAhead);
                dateValue = currentDate.getTime();
                break;
            default:
                // DO NOTHING
        }
        return dateValue;
    }


    @Given("^Store-wide blackout start time is '(NOT POPULATED|EXACTLY NOW|BEFORE NOW|AFTER NOW)' and end time is '(NOT POPULATED|EXACTLY NOW|BEFORE NOW|AFTER NOW)' for store '(\\d+)'$")
    public void storeWideBlackoutStartTimeAndEndTime(final String blackoutStartDate, final String blackoutEndDate,
            final int storeNumber) {

        final Calendar calander = Calendar.getInstance();
        final Date startDate = getDateValue(blackoutStartDate, calander, 3, -3);
        final Date endDate = getDateValue(blackoutEndDate, calander, 5, -1);

        StoreFulfilmentCapabilitiesUtil.setStoreBlackout(startDate, endDate, storeNumber);
    }

    @Given("^global order routing is (Enabled|Disabled)$")
    public void givenGlobalOrderRoutingIsEnabledOrDisabled(final String orderRoutingSetting) {

        StoreFulfilmentCapabilitiesUtil.setGlobalInstoreFulfilmentEnabled("enabled"
                .equalsIgnoreCase(orderRoutingSetting));
    }

    @Given("^the '(NOT POPULATED|EXACTLY NOW|BEFORE NOW|AFTER NOW)' and '(NOT POPULATED|EXACTLY NOW|BEFORE NOW|AFTER NOW)' are set for the '(.*)'$")
    public void createGlobalProductExclusion(final String exclusionStartDate, final String exclusionEndDate,
            final String productCode) {
        final Date startDate = (exclusionStartDate.equalsIgnoreCase("NOT POPULATED") ? null : getDateValue(
                exclusionStartDate, Calendar.getInstance(), 3, -3));
        final Date endDate = (exclusionEndDate.equalsIgnoreCase("NOT POPULATED") ? null : getDateValue(
                exclusionEndDate, Calendar.getInstance(), 5, -1));

        StoreFulfilmentCapabilitiesUtil.setGlobalExclusionProducts(startDate, endDate, productCode);
    }

    @Given("^global blackout period is (Not in Effect|In Effect)$")
    public void givenGlobalBlackoutPeriodIsInEffectOrNot(final String globalBlackoutPeriod) {

        if ("Not in Effect".equalsIgnoreCase(globalBlackoutPeriod)) {
            StoreFulfilmentCapabilitiesUtil.setGlobalBlackout(null, null);
        }
        else {
            final Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, 2);
            StoreFulfilmentCapabilitiesUtil.setGlobalBlackout(null, calendar.getTime());
        }
    }

    @Given("^delivery modes allowed for instore fulfilment are '(.*)'$")
    public void givenDeliveryModesAllowedForInstoreFulfilmentAre(final String deliveryModesAllowed) {
        StoreFulfilmentCapabilitiesUtil.setGlobalDeliveryModesAllowed(deliveryModesAllowed);
    }

    @Given("^global max items per instore fulfillment order is (\\d+)$")
    public void givenGlobalMaxItemsPerInstoreFulfillmentOrder(final int maxItemsPerConsignment) {

        StoreFulfilmentCapabilitiesUtil.setGlobalMaxItemsPerConsignment(maxItemsPerConsignment);
    }

    @Given("^the global interstore routing capability is '(Yes|yes|No|no)'$")
    public void givenGlobalInterstoreRoutingCapability(final String allowInterstore) {

        StoreFulfilmentCapabilitiesUtil.setGlobalInterstoreRoutingCapability(allowInterstore.equalsIgnoreCase("yes"));
    }

    @Given("^the global category exclusion is$")
    public void createGlobalCategoryExclusionList(final List<ExclusionData> excludeCategoies) {
        for (final ExclusionData excludeCategoryData : excludeCategoies) {
            setDates(excludeCategoryData);
        }
        StoreFulfilmentCapabilitiesUtil.setGlobalCategoryExclusion(excludeCategoies);
    }

    @Given("^the global category exclusion is '(.*)' with start date '(NOT POPULATED|EXACTLY NOW|BEFORE NOW|AFTER NOW)' and end date '(NOT POPULATED|EXACTLY NOW|BEFORE NOW|AFTER NOW)'$")
    public void createGlobalCategoryExclusion(final String categoryCode, final String exclusionStartDate,
            final String exclusionEndDate) {
        final ExclusionData excludeCategoryData = new ExclusionData();
        excludeCategoryData.setCategoryCode(categoryCode);
        excludeCategoryData.setExcludeStartDate(exclusionStartDate);
        excludeCategoryData.setExcludeEndDate(exclusionEndDate);
        final List<ExclusionData> excludeCategoies = new ArrayList<>(
                Arrays.asList(excludeCategoryData));
        createGlobalCategoryExclusionList(excludeCategoies);
    }

    @Given("^product exclusion for store '(.*)':$")
    public void createStoreProductExclusionList(final String storeNumber,
            final List<ExclusionData> excludedProducts)
                    throws NumberFormatException, TargetUnknownIdentifierException {

        for (final ExclusionData excludeProductData : excludedProducts) {
            setDates(excludeProductData);
        }

        TargetPointOfServiceModel pos = null;
        try {
            pos = ServiceLookup.getTargetPointOfServiceService()
                    .getPOSByStoreNumber(Integer.valueOf(storeNumber));
        }
        catch (final TargetAmbiguousIdentifierException ambiguousException) {
            throw new AmbiguousIdentifierException("Duplicate store found!", ambiguousException);
        }

        StoreFulfilmentCapabilitiesUtil.setExclusionProductData(excludedProducts, pos);
    }

    @Given("^the store '(.*)' product exclusion is '(.*)' with start date '(NOT POPULATED|EXACTLY NOW|BEFORE NOW|AFTER NOW)' and end date '(NOT POPULATED|EXACTLY NOW|BEFORE NOW|AFTER NOW)'$")
    public void createStoreProductExclusion(final String storeNumber, final String productCode,
            final String exclusionStartDate,
            final String exclusionEndDate)
                    throws NumberFormatException, TargetUnknownIdentifierException {
        final ExclusionData excludeProductData = new ExclusionData();
        excludeProductData.setProductCode(productCode);
        excludeProductData.setExcludeStartDate(exclusionStartDate);
        excludeProductData.setExcludeEndDate(exclusionEndDate);
        final List<ExclusionData> excludeProducts = new ArrayList<>(
                Arrays.asList(excludeProductData));
        createStoreProductExclusionList(storeNumber, excludeProducts);
    }

    @Given("^the store '(.*)' category exclusion is '(.*)' with start date '(NOT POPULATED|EXACTLY NOW|BEFORE NOW|AFTER NOW)' and end date '(NOT POPULATED|EXACTLY NOW|BEFORE NOW|AFTER NOW)'$")
    public void createStoreCategoryExclusion(final String storeNumber, final String categoryCode,
            final String exclusionStartDate,
            final String exclusionEndDate)
                    throws NumberFormatException, TargetUnknownIdentifierException {
        final ExclusionData excludeCategorytData = new ExclusionData();
        excludeCategorytData.setCategoryCode(categoryCode);
        excludeCategorytData.setExcludeStartDate(exclusionStartDate);
        excludeCategorytData.setExcludeEndDate(exclusionEndDate);
        final List<ExclusionData> excludeCategories = new ArrayList<>(Arrays.asList(excludeCategorytData));
        createSameStoreCategoryExclusionList(storeNumber, excludeCategories);
    }

    @Given("^category exclusion for store '(.*)':$")
    public void createSameStoreCategoryExclusionList(final String storeNo,
            final List<ExclusionData> excludeCategoies)
                    throws NumberFormatException, TargetUnknownIdentifierException {

        for (final ExclusionData excludeCategoryData : excludeCategoies) {
            setDates(excludeCategoryData);
        }
        TargetPointOfServiceModel pos = null;
        try {
            pos = ServiceLookup.getTargetPointOfServiceService()
                    .getPOSByStoreNumber(
                            Integer.valueOf(storeNo));
        }
        catch (final TargetAmbiguousIdentifierException ambiguousException) {
            throw new AmbiguousIdentifierException("Duplicate store found!", ambiguousException);
        }

        StoreFulfilmentCapabilitiesUtil.setSameStoreCategoryExclusion(pos, excludeCategoies);
    }

    @Given("^the same store category exclusion is '(.*)' with start date '(NOT POPULATED|EXACTLY NOW|BEFORE NOW|AFTER NOW)' and end date '(NOT POPULATED|EXACTLY NOW|BEFORE NOW|AFTER NOW)' for store '(.*)'$")
    public void createSameStoreCategoryExclusion(final String categoryCode,
            final String exclusionStartDate,
            final String exclusionEndDate, final String storeNo)
                    throws NumberFormatException, TargetUnknownIdentifierException {
        final ExclusionData excludeCategoryData = new ExclusionData();
        excludeCategoryData.setCategoryCode(categoryCode);
        excludeCategoryData.setExcludeStartDate(exclusionStartDate);
        excludeCategoryData.setExcludeEndDate(exclusionEndDate);
        final List<ExclusionData> excludeCategoies = new ArrayList<>(
                Arrays.asList(excludeCategoryData));
        createSameStoreCategoryExclusionList(storeNo, excludeCategoies);
    }

    private void setDates(final ExclusionData data) {
        data.setStartDate(getDateValue(data.getExcludeStartDate(),
                Calendar.getInstance(), 3, -3));
        data.setEndDate(getDateValue(data.getExcludeEndDate(),
                Calendar.getInstance(), 5, -1));
    }
}
