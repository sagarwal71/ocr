/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;

import au.com.target.tgtpaymentprovider.zippay.data.request.CreateRefundRequest;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateCheckoutResponse;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateRefundResponse;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.BusinessProcessUtil;
import au.com.target.tgttest.automation.facade.OrderUtil;
import au.com.target.tgttest.automation.facade.ZipPaymentUtil;
import au.com.target.tgttest.automation.facade.domain.Order;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author salexa10
 *
 */
public class ZippaySteps {

    @Given("^Zippay createCheckout API is unavailable$")
    public void setZippayCreateCheckoutUnavailable() {
        ServiceLookup.getTargetZippayClientMock().setCreateCheckoutException(true);
    }

    @Given("^Zippay createCheckout api returns:$")
    public void setCreateCheckoutResponse(final List<CreateCheckoutResponse> createCheckoutResponses) {
        ServiceLookup.getTargetZippayClientMock().setCreateCheckoutResponse(createCheckoutResponses.get(0));
    }

    @Given("^Zippay ping API is available$")
    public void setZippayAvailability() {
        ServiceLookup.getTargetZippayClientMock().setPingActive(true);
    }

    @Given("^Zippay ping API is unavailable$")
    public void setZippayPingUnavailable() {
        ServiceLookup.getTargetZippayClientMock().setThrowExpection(true);
        ServiceLookup.getTargetZippayClientMock().setPingActive(true);
    }

    @Given("^zippay retrieve checkout api is down$")
    public void setRetrieveCheckoutApiDown() {
        ServiceLookup.getTargetZippayClientMock().setRetrieveCheckoutException(true);
    }

    @Given("^zippay rejects the payment$")
    public void rejectCapturePayment() {
        ZipPaymentUtil.rejectCapturePayment();
    }

    @Given("^zip create refund api returns:$")
    public void setCreateRefundResponse(final List<CreateRefundResponse> createRefundResponse) {
        ServiceLookup.getTargetZippayClientMock().setMockRefundResponse(createRefundResponse.get(0));
    }

    @Given("^the zip refund will '(succeed|fail)' for the order$")
    public void mockZipRefund(final String status) {
        if ("fail".equals(status)) {
            ServiceLookup.getTargetZippayClientMock().setRefundRejectException(true);
        }
    }

    @Given("^the zip refund has been trigerred for the order with amount '(.*)'$")
    public void verifyZipRefundRequest(final double amount) {
        final CreateRefundRequest request = ServiceLookup.getTargetZippayClientMock().getCreateRefundRequestCaptor();
        assertThat(Double.valueOf(request.getAmount())).isEqualTo(Double.valueOf(amount));
        assertThat(OrderUtil.isRefundValueMatch(Order.getInstance(), Double.valueOf(amount))).as("Refund value match")
                .isTrue();
    }

    @Given("^the zip refund has been '(succeeded|failed)'$")
    public void verifyZipRefundEntry(final String status) {
        final CreateRefundResponse createRefundResponse = ServiceLookup.getTargetZippayClientMock()
                .getMockRefundResponse();
        if ("succeeded".equals(status)) {
            assertThat(createRefundResponse.getId()).isNotNull();
            assertThat(OrderUtil.checkRefundSuccss(Order.getInstance())).isTrue();
        }
        else {
            assertThat(createRefundResponse).isNull();
            assertThat(OrderUtil.checkRefundSuccss(Order.getInstance())).isFalse();
        }
    }

    @Given("^zippay capture payment api is down$")
    public void capturePaymentApiDown() {
        ServiceLookup.getTargetZippayClientMock().setCaptureRequestException(true);
    }

    @When("^zippay capture payment api succeeds on retry$")
    public void capturePaymentSucceedsOnRetry() throws InterruptedException {
        ServiceLookup.getTargetZippayClientMock().setCaptureRequestException(false);
        BusinessProcessUtil.waitForAllOrderProcessesToFinish(Order.getInstance().getOrderModel());
    }

    @Then("^the refund for the order is:$")
    public void verifySuccessfulRefund(final List<String> refunds) {
        for (final String refund : refunds) {
            verifyRefund(refund);
        }
    }

    private void verifyRefund(final String amount) {
        boolean findMatch = false;
        if (StringUtils.isNotEmpty(amount)) {
            final List<PaymentTransactionModel> transactions = Order.getInstance().getOrderModel()
                    .getPaymentTransactions();
            assertThat(transactions).isNotNull();
            assertThat(transactions.size()).isEqualTo(1);

            final PaymentTransactionModel paymentTransactionModel = transactions.get(0);
            for (final PaymentTransactionEntryModel entry : paymentTransactionModel.getEntries()) {
                if (PaymentTransactionType.REFUND_FOLLOW_ON.equals(entry.getType())
                        && TransactionStatus.ACCEPTED.toString().equals(entry.getTransactionStatus()) &&
                        entry.getAmount().compareTo(new BigDecimal(amount)) == 0) {
                    findMatch = true;
                    break;
                }
            }
            Assert.assertTrue("Could not find a matching refund amount " + amount, findMatch);
        }
        else {
            Assert.assertFalse("Could not find a matching refund amount " + amount, findMatch);
        }
    }

    @Then("^set zip CaptureTransactionError '(.*)'$")
    public void setCaptureTransactionError(final boolean enable) {
        ServiceLookup.getTargetZippayClientMock().setCaptureTransactionError(enable);
    }

}
