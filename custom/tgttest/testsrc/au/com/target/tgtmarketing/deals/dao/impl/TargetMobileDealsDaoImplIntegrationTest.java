/**
 * 
 */
package au.com.target.tgtmarketing.deals.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.deals.dao.impl.DealsTestHelper;
import au.com.target.tgtmarketing.deals.dao.TargetMobileDealsDao;
import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;
import au.com.target.tgtmarketing.offers.TargetMobileOfferHeadingsTestHelper;


/**
 * @author paul
 *
 */

/**
 * Integration test for {@link TargetMobileDealsDao}
 */
@IntegrationTest
public class TargetMobileDealsDaoImplIntegrationTest extends ServicelayerTransactionalTest {


    @Resource
    private TargetMobileDealsDao targetMobileDealDao;

    private final List<AbstractDealModel> createdAbstractDealsForTest = new ArrayList<>();

    private final List<TargetMobileOfferHeadingModel> targetMobileOfferHeadingModelList = new ArrayList<>();

    @Before
    public void setup() {

        targetMobileOfferHeadingModelList.add(TargetMobileOfferHeadingsTestHelper
                .setupTargetMobileOfferHeadingsData("entertainment", "ENTERTAINMENT", "#4C4D74"));
        targetMobileOfferHeadingModelList.add(TargetMobileOfferHeadingsTestHelper
                .setupTargetMobileOfferHeadingsData("women", "WOMEN", "#EF6790"));
    }

    @Test
    public void testGetAllActiveDeals() {

        final DateTime now = new DateTime();

        // deal with no end date and enabled for mobile --- This should be retrieved.
        final AbstractDealModel dealWithNoEndDateEnableForMobile = MobileDealsTestHelper
                .createValueBundleDealWithAvailableForMobileAttribute("6467", "7009", 150d,
                        null, Boolean.TRUE, "longtitle1", targetMobileOfferHeadingModelList, null);

        createdAbstractDealsForTest.add(dealWithNoEndDateEnableForMobile);

        // deal with no end date and enabled for mobile and with an immutableKeyHash --- This should not be retrieved.
        final AbstractDealModel dealWithNoEndDateEnableForMobileWithImmutableKeyHash = MobileDealsTestHelper
                .createValueBundleDealWithAvailableForMobileAttribute("6467", "7009", 150d,
                        null, Boolean.TRUE, "longtitle1", targetMobileOfferHeadingModelList,
                        "83969646f8e275660137fccdcded27b0");
        createdAbstractDealsForTest.add(dealWithNoEndDateEnableForMobileWithImmutableKeyHash);

        // deal with no end date and disabled for mobile --- This should not be retrieved.
        final AbstractDealModel dealWithNoEndDateDisabledForMobile = MobileDealsTestHelper
                .createValueBundleDealWithAvailableForMobileAttribute("5467", "7009", 150d,
                        null, Boolean.FALSE, null, targetMobileOfferHeadingModelList, null);
        createdAbstractDealsForTest.add(dealWithNoEndDateDisabledForMobile);

        // deal with end date in the past  enabled for mobile -- This should not be retrieved.
        final AbstractDealModel dealWithNoEndDateDateInPast = MobileDealsTestHelper
                .createValueBundleDealWithAvailableForMobileAttribute("6468", "7009", 250d, now
                        .minusDays(1).toDate(), Boolean.TRUE, "longtitle6468",
                        targetMobileOfferHeadingModelList, null);
        createdAbstractDealsForTest.add(dealWithNoEndDateDateInPast);

        // deal with end date in the future enabled for mobile -- This should be retrieved
        final AbstractDealModel dealExpiringTomorrowEnabledForMobile = MobileDealsTestHelper
                .createValueBundleDealWithAvailableForMobileAttribute("6469", "7009", 350d, now.plusDays(1).toDate(),
                        Boolean.TRUE, "longtitle6469", targetMobileOfferHeadingModelList, null);
        createdAbstractDealsForTest.add(dealExpiringTomorrowEnabledForMobile);

        // deal ending today --this should be retrieved for mobile
        final AbstractDealModel dealExpiringTodayEnabledForMobile = MobileDealsTestHelper
                .createValueBundleDealWithAvailableForMobileAttribute("6471", "7009", 450d, now
                        .plusHours(1).toDate(), Boolean.TRUE, "longtitle6470",
                        targetMobileOfferHeadingModelList, null);

        createdAbstractDealsForTest.add(dealExpiringTodayEnabledForMobile);

        final List<AbstractDealModel> activeDeals = targetMobileDealDao.getAllActiveMobileDeals();

        Assert.assertNotNull(activeDeals);
        Assert.assertEquals(3, activeDeals.size());
        Assert.assertTrue(activeDeals.containsAll(Arrays.asList(dealWithNoEndDateEnableForMobile,
                dealExpiringTomorrowEnabledForMobile,
                dealExpiringTodayEnabledForMobile)));
    }

    @After
    public void destroy() {
        DealsTestHelper.removeDeal(createdAbstractDealsForTest);
        TargetMobileOfferHeadingsTestHelper.removeTargetMobileOfferHeadingsData(targetMobileOfferHeadingModelList);
    }
}
