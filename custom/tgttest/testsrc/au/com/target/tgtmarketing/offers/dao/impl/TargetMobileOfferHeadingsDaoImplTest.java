/**
 * 
 */
package au.com.target.tgtmarketing.offers.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;
import au.com.target.tgtmarketing.offers.TargetMobileOfferHeadingsTestHelper;
import au.com.target.tgtmarketing.offers.dao.TargetMobileOfferHeadingsDao;


/**
 * @author paul
 *
 */
@IntegrationTest
public class TargetMobileOfferHeadingsDaoImplTest extends ServicelayerTransactionalTest {


    @Resource
    private TargetMobileOfferHeadingsDao targetMobileOfferHeadingsDao;


    private final List<TargetMobileOfferHeadingModel> targetMobileOfferHeadingModelList = new ArrayList<>();

    @Before
    public void setup() {

        targetMobileOfferHeadingModelList.add(TargetMobileOfferHeadingsTestHelper
                .setupTargetMobileOfferHeadingsData("entertainment", "ENTERTAINMENT", "#4C4D74"));
        targetMobileOfferHeadingModelList.add(TargetMobileOfferHeadingsTestHelper
                .setupTargetMobileOfferHeadingsData("women", "WOMEN", "#EF6790"));

    }

    @Test
    public void testGetAllTargetMobileOfferHeadings() {

        final List<TargetMobileOfferHeadingModel> targetMobileOfferHeadings = targetMobileOfferHeadingsDao
                .getAllTargetMobileOfferHeadings();

        Assert.assertNotNull(targetMobileOfferHeadings);
        Assert.assertEquals(2, targetMobileOfferHeadings.size());
        Assert.assertTrue(targetMobileOfferHeadings.containsAll(Arrays.asList(targetMobileOfferHeadingModelList.get(0),
                targetMobileOfferHeadingModelList.get(1))));
    }

    @After
    public void destroy() {
        TargetMobileOfferHeadingsTestHelper.removeTargetMobileOfferHeadingsData(targetMobileOfferHeadingModelList);
    }
}
