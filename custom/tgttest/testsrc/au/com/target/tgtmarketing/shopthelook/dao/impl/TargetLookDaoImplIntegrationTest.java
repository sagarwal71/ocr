/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.annotation.Resource;

import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.AbstractConcurrentJaloSessionCallable;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.shopthelook.dao.LookDao;


/**
 * 
 * Integration test for {@link LookDao}
 * 
 * @author mgazal
 *
 */
@IntegrationTest
public class TargetLookDaoImplIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private LookDao lookDao;

    @Resource
    private ModelService modelService;

    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtmarketing/test/test-looks.impex", Charsets.UTF_8.name());
    }

    @Test
    public void getExistingLook()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final TargetLookModel lookModel = lookDao.getLookForCode("casualWearLook1");
        Assert.assertNotNull(lookModel);
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testUnknownLook()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        lookDao.getLookForCode("dummy");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLookMissingCode()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        lookDao.getLookForCode(null);
    }

    @Test
    public void testLooksByCollection() throws ParseException {
        final List<TargetLookModel> looks = lookDao.getVisibleLooksForCollection("womenSpringCasual");
        Assert.assertNotNull(looks);
        Assert.assertEquals(3, looks.size());
        Assertions.assertThat(looks).onProperty("id").containsExactly("womenSpringCasualLook6",
                "womenSpringCasualLook1", "womenSpringCasualLook4");
    }

    @Test
    public void testLooksByCollectionEmpty() {
        final List<TargetLookModel> looks = lookDao.getVisibleLooksForCollection("dummy");
        Assert.assertNotNull(looks);
        Assert.assertEquals(0, looks.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLooksByCollectionNull() {
        lookDao.getVisibleLooksForCollection(null);
    }

    @Test
    public void testLooksByProductCodes() {
        final List<TargetLookModel> looks = lookDao
                .getVisibleLooksForProductCodes(Arrays.asList("P8069_red", "P8080_red", "P4000_black"));
        Assert.assertNotNull(looks);
        Assert.assertEquals(3, looks.size());

        Assertions.assertThat(looks).onProperty("id").containsExactly("womenSpringCasualLook6",
                "womenSpringCasualLook1", "womenSpringCasualLook4");
    }

    @Test
    public void testFetchLooksByProductCodesWithLimit() {
        final List<TargetLookModel> looks = lookDao
                .getVisibleLooksForProductCodes(Arrays.asList("P8069_red", "P8080_red", "P4000_black"), 1);
        Assert.assertNotNull(looks);
        Assert.assertEquals(1, looks.size());
        Assert.assertEquals("womenSpringCasualLook6", looks.iterator().next().getId());
    }

    @Test
    public void testLooksByProductCodesEmpty() {
        final Collection<TargetLookModel> looks = lookDao
                .getVisibleLooksForProductCodes(Arrays.asList("dummy1", "dummy2"));
        Assert.assertNotNull(looks);
        Assert.assertEquals(0, looks.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLooksByProductCodesMissingCodes() {
        lookDao.getVisibleLooksForProductCodes(null);
    }

    @Test
    public void testFetchLooksByProductCodesEmpty() {
        final Collection<TargetLookModel> looks = lookDao
                .getVisibleLooksForProductCodes(Arrays.asList("dummy1", "dummy2"));
        Assert.assertNotNull(looks);
        Assert.assertEquals(0, looks.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFetchLooksByProductCodesMissingCodes() {
        lookDao.getVisibleLooksForProductCodes(null);
    }

    @Test
    public void testFetchAllLooksWithInstagramUrl() {
        final List<TargetLookModel> looks = lookDao.getAllVisibleLooksWithInstagramUrl();
        Assert.assertNotNull(looks);
        Assert.assertEquals(3, looks.size());
        Assertions.assertThat(looks).onProperty("id").containsExactly("womenSpringCasualLook1",
                "womenSpringCasualLook4", "casualWearLook1");
    }

    @Test
    public void testFetchAllLooksLimit() {
        final List<TargetLookModel> looks = lookDao.getAllVisibleLooksWithInstagramUrl(1);
        Assert.assertNotNull(looks);
        Assert.assertEquals(1, looks.size());
        Assert.assertEquals("womenSpringCasualLook1", looks.iterator().next().getId());
    }

    @Test
    public void testConcurrentCreateDoesNotProduceDuplicateLooks()
            throws InterruptedException, ExecutionException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final int noOfThreads = 2;
        // barrier to ensure both threads start at exactly the same time
        final CyclicBarrier barrier = new CyclicBarrier(noOfThreads);
        final String id = "L01";
        final ExecutorService executor = Executors.newFixedThreadPool(noOfThreads);
        final AbstractConcurrentJaloSessionCallable<TargetLookModel> createLook = new AbstractConcurrentJaloSessionCallable() {

            @Override
            public TargetLookModel execute() throws InterruptedException, BrokenBarrierException,
                    TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
                final TargetLookModel look = modelService.create(TargetLookModel.class);
                look.setId(id);
                look.setName("Look 1");

                barrier.await();
                try {
                    modelService.save(look);
                }
                catch (final ModelSavingException e) {
                    //ignore this
                }
                return lookDao.getLookForCode(id);
            }
        };

        final List<Future<TargetLookModel>> futures = new ArrayList<>();
        for (int i = 0; i < noOfThreads; i++) {
            futures.add(executor.submit(createLook));
        }

        for (int i = 1; i < noOfThreads; i++) {
            Assert.assertEquals(futures.get(i).get(), futures.get(i - 1).get());
        }

        // if a concurrency issue does occur, a subsequent invocation would return null coz of ambiguous results
        final TargetLookModel look = lookDao.getLookForCode(id);
        Assert.assertNotNull(look);
        Assert.assertEquals(look, futures.get(0).get());
    }

}
