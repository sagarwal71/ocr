/**
 *
 */
package au.com.target.tgtwishlist.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.daos.impl.DefaultUserDao;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.tgtwishlist.dao.TargetWishListDao;
import au.com.target.tgtwishlist.enums.WishListTypeEnum;
import au.com.target.tgtwishlist.model.TargetWishListEntryModel;
import au.com.target.tgtwishlist.model.TargetWishListModel;
import org.junit.Assert;


/**
 * @author paul
 *
 */
@IntegrationTest
public class TargetWishListDaoIntegrationTest extends ServicelayerTransactionalTest {

    @Resource(name = "targetWishListDao")
    private TargetWishListDao targetWishListDaoImpl;

    @Resource(name = "userDao")
    private DefaultUserDao userDao;

    private UserModel user;

    @Before
    public void setUp() throws ImpExException {
        importCsv("/tgtwishlist/test/basicProductSetup.impex", Charsets.UTF_8.name());
        importCsv("/tgtwishlist/test/testTargetWishList.impex", Charsets.UTF_8.name());
        user = userDao.findUserByUID("test.wishlistuser1@target.com.au");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetWishListByNameUserAndTypeForNullName() throws Exception {
        targetWishListDaoImpl.getWishListByNameUserAndType(null, user, WishListTypeEnum.FAVOURITE);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetWishListByNameUserAndTypeForNullUser() throws Exception {
        targetWishListDaoImpl.getWishListByNameUserAndType("hello", null, WishListTypeEnum.FAVOURITE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetWishListByNameUserAndTypeForNullType() throws Exception {
        targetWishListDaoImpl.getWishListByNameUserAndType("hello", user, null);
    }

    @Test(expected = UnknownIdentifierException.class)
    public void testGetWishListByNameUserAndTypeWhenNoEntryFound() throws Exception {
        targetWishListDaoImpl.getWishListByNameUserAndType("hello", user, WishListTypeEnum.FAVOURITE);
    }

    @Test
    public void testGetWishListByNameUserAndTypeWhenFound() throws Exception {
        final TargetWishListModel targetWishListModel = targetWishListDaoImpl.getWishListByNameUserAndType("Favourites",
                user,
                WishListTypeEnum.FAVOURITE);
        Assert.assertNotNull(targetWishListModel);
    }

    @Test
    public void testGetWishListByUserAndTypeWhenFound() throws Exception {
        user = userDao.findUserByUID("test.wishlistuser2@target.com.au");
        final List<TargetWishListModel> targetWishListModelList = targetWishListDaoImpl.getWishListByUserAndType(user,
                WishListTypeEnum.FAVOURITE);
        Assertions.assertThat(targetWishListModelList).isNotEmpty();
        Assertions.assertThat(targetWishListModelList.get(0).getCode()).isEqualTo("testWishList_002");
    }

    @Test
    public void testFindOldestWishlistEntryForAWishList() throws Exception {
        user = userDao.findUserByUID("test.wishlistuser3@target.com.au");
        final TargetWishListModel targetWishListModel = targetWishListDaoImpl.getWishListByNameUserAndType("Favourites",
                user,
                WishListTypeEnum.FAVOURITE);
        final List<TargetWishListEntryModel> targetWishListEntryModelList = targetWishListDaoImpl
                .findOldestWishlistEntryForAWishList(targetWishListModel, 2);
        Assertions.assertThat(targetWishListEntryModelList).isNotEmpty();

        final List<String> expectedProducts = Arrays.asList("P1000", "P2010");
        for (final TargetWishListEntryModel entry : targetWishListEntryModelList) {
            Assertions.assertThat(expectedProducts).contains(entry.getProduct().getCode());
        }
    }

    @Test
    public void testFindWishListEntriesWithVariant() throws Exception {
        final List<TargetWishListEntryModel> wishListEntryModels = targetWishListDaoImpl
                .findWishListEntriesWithVariant(Arrays.asList("P1000_Black_L", "P2010_Black_L"));
        assertThat(wishListEntryModels).isNotEmpty();
        final List<String> expectedProducts = Arrays.asList("P1000", "P2010");
        for (final TargetWishListEntryModel targetWishListEntryModel : wishListEntryModels) {
            assertThat(expectedProducts).contains(targetWishListEntryModel.getProduct().getCode());
        }

    }

}
