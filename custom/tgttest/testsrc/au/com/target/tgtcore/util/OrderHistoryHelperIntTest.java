/**
 * 
 */
package au.com.target.tgtcore.util;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;


@IntegrationTest
public class OrderHistoryHelperIntTest extends ServicelayerTransactionalTest {

    @Resource
    private ModelService modelService;

    @Resource
    private OrderHistoryHelper orderHistoryHelper;


    @Test
    public void testCreateHistorySnapshot() {
        final OrderModel order = createOrderForCode("testOrder");
        orderHistoryHelper.createHistorySnapshot(order, "Test description");

        // Now update the code
        order.setCode("testOrder2");
        modelService.save(order);
        Assert.assertEquals("testOrder2", order.getCode());

        // Check there is one history entry with the old code
        final List<OrderHistoryEntryModel> historyEntries = order.getHistoryEntries();
        Assert.assertNotNull(historyEntries);
        Assert.assertEquals(1, historyEntries.size());

        Assert.assertEquals("testOrder", historyEntries.get(0).getPreviousOrderVersion().getCode());

    }

    private OrderModel createOrderForCode(final String code) {

        final CurrencyModel currency = modelService.create(CurrencyModel.class);
        currency.setIsocode("TEST");
        currency.setSymbol("TEST");
        modelService.save(currency);
        final UserModel user = modelService.create(UserModel.class);
        user.setUid("user");
        modelService.save(user);

        final OrderModel order = new OrderModel();
        order.setCode(code);
        order.setCurrency(currency);
        order.setUser(user);
        order.setDate(new Date());
        modelService.save(order);

        return order;
    }

}
