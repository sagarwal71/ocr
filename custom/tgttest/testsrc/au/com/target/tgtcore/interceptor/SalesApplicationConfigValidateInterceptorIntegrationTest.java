package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import au.com.target.tgtcore.model.SalesApplicationConfigModel;
import org.junit.Assert;


@IntegrationTest
public class SalesApplicationConfigValidateInterceptorIntegrationTest extends ServicelayerTransactionalTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Resource
    private ModelService modelService;

    @Test
    public void testOnValidateInvalid() {
        final SalesApplicationConfigModel config = modelService.create(SalesApplicationConfigModel.class);
        config.setSalesApplication(SalesApplication.EBAY);
        config.setCncStockBuffer(-1);

        expectedException.expect(ModelSavingException.class);
        expectedException.expectMessage(SalesApplicationConfigValidateInterceptor.INVALID_CNC_STOCK_BUFFER);

        modelService.save(config);
    }

    @Test
    public void testOnValidateValid() {
        final SalesApplicationConfigModel config = modelService.create(SalesApplicationConfigModel.class);
        config.setSalesApplication(SalesApplication.EBAY);
        config.setCncStockBuffer(1);

        try {
            modelService.save(config);
        }
        catch (final Exception e) {
            Assert.fail();
        }
    }

}
