/**
 * 
 */
package au.com.target.tgtcore.flybuys.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.FlybuysRedeemConfigModel;


/**
 * @author Nandini
 * 
 */
@IntegrationTest
public class FlybuysRedeemConfigDaoImplTest extends ServicelayerTransactionalTest {

    @Resource
    private ModelService modelService;

    @Resource
    private FlybuysRedeemConfigDaoImpl flybuysRedeemConfigDao;

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testGetFlyBuyRedeemConfigNull() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        flybuysRedeemConfigDao.getFlybuysRedeemConfig();
        Assert.fail();
    }

    @Test(expected = TargetAmbiguousIdentifierException.class)
    public void testGetFlyBuyRedeemConfigMultiple() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final FlybuysRedeemConfigModel model1 = modelService.create(FlybuysRedeemConfigModel.class);
        model1.setCode("one");
        final FlybuysRedeemConfigModel model2 = modelService.create(FlybuysRedeemConfigModel.class);
        model2.setCode("two");

        modelService.save(model1);
        modelService.save(model2);

        flybuysRedeemConfigDao.getFlybuysRedeemConfig();
        Assert.fail();
    }

    @Test
    public void testGetFlyBuyRedeemConfig() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final FlybuysRedeemConfigModel model = modelService.create(FlybuysRedeemConfigModel.class);
        model.setCode("config");
        model.setMinRedeemable(Double.valueOf(10d));
        model.setMaxRedeemable(Double.valueOf(50d));
        model.setMinCartValue(Double.valueOf(10d));
        model.setMinPointsBalance(Integer.valueOf(0));
        model.setActive(Boolean.FALSE);
        modelService.save(model);

        final FlybuysRedeemConfigModel flybuysConfigModel = flybuysRedeemConfigDao.getFlybuysRedeemConfig();

        Assert.assertNotNull(flybuysConfigModel);
        Assert.assertEquals(Double.valueOf(10d), flybuysConfigModel.getMinRedeemable());
        Assert.assertEquals(Double.valueOf(50d), flybuysConfigModel.getMaxRedeemable());
        Assert.assertEquals(Double.valueOf(10d), flybuysConfigModel.getMinCartValue());
        Assert.assertEquals(Integer.valueOf(0), flybuysConfigModel.getMinPointsBalance());
        Assert.assertEquals(Boolean.FALSE, flybuysConfigModel.getActive());
    }
}
