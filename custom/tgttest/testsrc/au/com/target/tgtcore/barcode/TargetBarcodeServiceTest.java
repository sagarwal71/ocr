/**
 * 
 */
package au.com.target.tgtcore.barcode;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.ByteArrayOutputStream;

import javax.annotation.Resource;

import org.junit.Assert;

import org.junit.Test;

import au.com.target.tgtcore.exception.GenerateBarCodeFailedException;
import au.com.target.tgtcore.util.OrderEncodingTools;
import au.com.target.tgtutility.util.BarcodeTools;


@IntegrationTest
public class TargetBarcodeServiceTest extends ServicelayerTransactionalTest {
    @Resource
    private TargetBarCodeService targetBarCodeService;

    @Resource
    private ConfigurationService configurationService;

    @Test
    public void testGenerateSvgBarcode() throws GenerateBarCodeFailedException {
        final String encoded = OrderEncodingTools.encodeOrderId("12345678");
        final ByteArrayOutputStream bao = targetBarCodeService.generateSvgBarcode(encoded);
        Assert.assertNotNull(bao);
        Assert.assertTrue(bao.toByteArray().length > 1);
    }

    @Test
    public void testGeneratePngBarcode() throws GenerateBarCodeFailedException {
        final String encoded = OrderEncodingTools.encodeOrderId("12345678");
        final ByteArrayOutputStream bao = targetBarCodeService.generatePngBarcode(encoded);
        Assert.assertNotNull(bao);
        Assert.assertTrue(bao.toByteArray().length > 1);
    }

    @Test
    public void testGetPlainBarcode() {
        final String orderId = OrderEncodingTools.encodeOrderId("12345678");

        final String plainBarcode = targetBarCodeService.getPlainBarcode(orderId);

        final String storeNumber = configurationService.getConfiguration().getString("tgtcore.store.number");
        final char checkDigit = BarcodeTools.findEANCheckDigit(storeNumber + "12345678");

        Assert.assertEquals(storeNumber + "12345678" + checkDigit, plainBarcode);
    }
}
