/**
 *
 */
package au.com.target.tgtcore.delivery.dao.impl;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.order.DeliveryModeService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.Collection;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.tgtcore.constants.TgtCoreConstants;


/**
 * @author Pradeep
 *
 */

@IntegrationTest
public class TargetCountryZoneDeliveryModeDaoTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetCountryZoneDeliveryModeDao countryZoneDeliveryModeDao;
    @Resource
    private CommonI18NService commonI18NService;
    @Resource
    private DeliveryModeService deliveryModeService;
    private CurrencyModel aud;
    private CountryModel countryModel;
    private DeliveryModeModel homeDelivery;
    private DeliveryModeModel expressDelivery;
    private DeliveryModeModel clickAndCollect;

    @Before
    public void setUp() throws ImpExException
    {
        importCsv("/tgtcore/test/testTargetCountryZoneDeliveryModeDao.impex",
                Charsets.UTF_8.toString());
        countryModel = commonI18NService.getCountry(TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);
        aud = commonI18NService.getCurrency(TgtCoreConstants.AUSTRALIAN_DOLLARS);
        homeDelivery = deliveryModeService.getDeliveryModeForCode("home-delivery");
        expressDelivery = deliveryModeService.getDeliveryModeForCode("express-delivery");
        clickAndCollect = deliveryModeService.getDeliveryModeForCode("click-and-collect");
    }

    @Test
    public void testFindDeliveryModes()
    {
        final Collection<DeliveryModeModel> deliveryModes = countryZoneDeliveryModeDao
                .findDeliveryModes(countryModel, aud, Boolean.TRUE);
        assertNotNull(deliveryModes);
        assertEquals(2, deliveryModes.size());
        assertThat(deliveryModes, hasItems(homeDelivery, expressDelivery));
    }


    @Test
    public void testFindDeliveryModesFalseNet()
    {
        final Collection<DeliveryModeModel> deliveryModes = countryZoneDeliveryModeDao
                .findDeliveryModes(countryModel, aud, Boolean.FALSE);
        assertNotNull(deliveryModes);
        assertEquals(1, deliveryModes.size());
        assertThat(deliveryModes, hasItem(clickAndCollect));

    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindDeliveryModesNullCurrency()
    {
        countryZoneDeliveryModeDao
                .findDeliveryModes(countryModel, null, Boolean.TRUE);
        Assert.fail();
    }


    @Test(expected = IllegalArgumentException.class)
    public void testFindDeliveryModesNullCountry()
    {
        countryZoneDeliveryModeDao
                .findDeliveryModes(null, aud, Boolean.TRUE);
        Assert.fail();
    }


}
