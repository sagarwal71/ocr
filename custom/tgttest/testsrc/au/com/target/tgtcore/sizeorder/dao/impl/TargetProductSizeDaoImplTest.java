/**
 * 
 */
package au.com.target.tgtcore.sizeorder.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetProductSizeModel;
import au.com.target.tgtcore.model.TargetSizeGroupModel;
import au.com.target.tgtcore.sizeorder.dao.TargetProductSizeDao;
import au.com.target.tgtcore.sizeorder.dao.TargetSizeGroupDao;


/**
 * @author mjanarth
 *
 */
@IntegrationTest
public class TargetProductSizeDaoImplTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetProductSizeDao targetProductSizeDao;

    @Resource
    private TargetSizeGroupDao targetSizeGroupDao;

    @Resource
    private ModelService modelService;

    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtcore/test/testProductSize.impex", "utf-8");
    }


    @Test
    public void testMaxPosition() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final TargetSizeGroupModel sizeGroup = targetSizeGroupDao.findSizeGroupbyCode("mensBottoms");
        final Integer result = targetProductSizeDao.getMaxProductSizePosition(sizeGroup);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(15);
    }

    @Test
    public void testMaxPositionUnknownSize() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetSizeGroupModel sizeGroup = targetSizeGroupDao.findSizeGroupbyCode("babyShoes");
        final Integer result = targetProductSizeDao.getMaxProductSizePosition(sizeGroup);
        assertThat(result).isNull();
    }

    @Test
    public void testFindTargetProductSize() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetSizeGroupModel sizeGroup = targetSizeGroupDao.findSizeGroupbyCode("mensBottoms");
        final TargetProductSizeModel productSize = targetProductSizeDao.findTargetProductSizeBySizeGroup("32",
                sizeGroup);
        assertThat(productSize).isNotNull();
        assertThat(productSize.getSize()).isEqualTo("32");
    }


    @Test(expected = TargetUnknownIdentifierException.class)
    public void testFindTargetProductSizeWithoutSize() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetSizeGroupModel sizeGroup = targetSizeGroupDao.findSizeGroupbyCode("mensBottoms");
        targetProductSizeDao.findTargetProductSizeBySizeGroup("1500", sizeGroup);
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testFindTargetProductSizeWithoutGrp() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetSizeGroupModel sizeGroup = targetSizeGroupDao.findSizeGroupbyCode("babyShoes");
        targetProductSizeDao.findTargetProductSizeBySizeGroup("22", sizeGroup);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindTargetProductSizeNull() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        targetProductSizeDao.findTargetProductSizeBySizeGroup("22", null);
    }
}
