/**
 * 
 */
package au.com.target.tgtcore.price.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.commons.lang.time.DateUtils;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetPreviousPermanentPriceModel;
import au.com.target.tgtcore.price.daos.TargetPreviousPermanentPriceDao;
import org.junit.Assert;


/**
 * @author bhuang3
 * 
 */
@IntegrationTest
public class TargetPreviousPermanentPriceDaoImplIntegationTest extends ServicelayerTransactionalTest {

    private CurrencyModel currencyModel;

    @Resource
    private ModelService modelService;

    @Resource
    private UserService userService;

    @Resource
    private TargetPreviousPermanentPriceDao targetPreviousPermanentPriceDao;

    @Resource
    private CommonI18NService commonI18NService;

    @Before
    public void setUp() throws ImpExException, ParseException {
        userService.setCurrentUser(userService.getAdminUser());
        currencyModel = commonI18NService.getCurrency(TgtCoreConstants.AUSTRALIAN_DOLLARS);
    }

    @Test
    public void testFindValidProductPreviousPricePrevMinTimeMatchCurrentMaxTimeMatch() {
        final Date now = new Date();
        //config: currentMaxAge 14 days prevMinAge=14 days
        //start day 28 days plus 2min before now, end day 14 days minus 2min before now
        //time diff: currentMaxAge=14 days, prevMinAge=14d+2min
        final Date date1Start = new Date(
                now.getTime() - 28l * 24l * 60l * 60l * 1000l - 60l * 60l * 1000l - (2000l * 60l));
        final Date date1End = new Date(
                now.getTime() - 14l * 24l * 60l * 60l * 1000l + (2000l * 60l));

        createNewPreviousPermanentPrice("1000011", Double.valueOf("20"), date1Start, date1End, currencyModel);

        final TargetPreviousPermanentPriceModel targetPreviousPermanentPriceModel = targetPreviousPermanentPriceDao
                .findValidProductPreviousPrice("1000011", "14", "14", currencyModel);

        assertThat(targetPreviousPermanentPriceModel).isNotNull();
        Assert.assertEquals(Double.valueOf("20"), targetPreviousPermanentPriceModel.getPrice());
    }

    @Test
    public void testFindValidProductPreviousPriceWithEndDayNull() {
        final Date now = new Date();
        //end day is null. return null
        final Date date1Start = new Date(now.getTime() - 19l * 24l * 60l * 60l * 1000l - 2000l);
        createNewPreviousPermanentPrice("1000011", Double.valueOf("20"), date1Start, null, currencyModel);

        final TargetPreviousPermanentPriceModel targetPreviousPermanentPriceModel = targetPreviousPermanentPriceDao
                .findValidProductPreviousPrice("1000011", "14", "14", currencyModel);

        assertThat(targetPreviousPermanentPriceModel).isNull();
    }

    @Test
    public void testFindValidProductPreviousPricePrevMinTimeMatchCurrentMaxTimeNotMatch() {
        final Date now = new Date();

        //config: currentMaxAge 14 days prevMinAge=14 days
        //start day 30 days plus 2s before now, end day 5 days before now
        //time diff: currentMinAge=16 days, prevMaxAge=14d+2s
        final Date date1Start = new Date(now.getTime() - 30l * 24l * 60l * 60l * 1000l - 2000l);
        final Date date1End = new Date(now.getTime() - 16l * 24l * 60l * 60l * 1000l);
        createNewPreviousPermanentPrice("1000011", Double.valueOf("20"), date1Start, date1End, currencyModel);

        final TargetPreviousPermanentPriceModel targetPreviousPermanentPriceModel = targetPreviousPermanentPriceDao
                .findValidProductPreviousPrice("1000011", "14", "14", currencyModel);

        assertThat(targetPreviousPermanentPriceModel).isNull();
    }

    @Test
    public void testFindValidProductPreviousPricePrevMinTimeNotMatchCurrentMaxTimeMatch() {
        final Date now = new Date();
        //config: currentMaxAge 14 days prevMinAge=14 days
        //start day 20 days before now, end day 6 days before now
        //time diff: currentMaxAge=6 days, prevMinAge=14d-2s (need to be bigger than 14 for match)
        final Date date1Start = new Date(now.getTime() - 20l * 24l * 60l * 60l * 1000 + 2000l);
        final Date date1End = new Date(now.getTime() - 6l * 24l * 60l * 60l * 1000);
        createNewPreviousPermanentPrice("1000011", Double.valueOf("20"), date1Start, date1End, currencyModel);
        final TargetPreviousPermanentPriceModel targetPreviousPermanentPriceModel = targetPreviousPermanentPriceDao
                .findValidProductPreviousPrice("1000011", "14", "14", currencyModel);

        assertThat(targetPreviousPermanentPriceModel).isNull();
    }


    @Test
    public void testFindValidProductPreviousPricePrevMinTimeNotMatchCurrentMaxTimeNotMatch() {
        final Date now = new Date();

        //config: currentMaxAge 14 days prevMinAge=14 days
        //start day 40 days before now, end day 26 days before now
        //time diff: currentMaxAge=26 days, prevMinAge=14 day (need to be bigger than 14 for match)
        final Date date1Start = new Date(now.getTime() - 40l * 24l * 60l * 60l * 1000);
        final Date date1End = new Date(now.getTime() - 26l * 24l * 60l * 60l * 1000);
        createNewPreviousPermanentPrice("1000011", Double.valueOf("20"), date1Start, date1End, currencyModel);
        final TargetPreviousPermanentPriceModel targetPreviousPermanentPriceModel = targetPreviousPermanentPriceDao
                .findValidProductPreviousPrice("1000011", "14", "14", currencyModel);
        assertThat(targetPreviousPermanentPriceModel).isNull();
    }

    @Test
    public void testFindValidProductPreviousPriceMultplResult() {
        final Date now = new Date();

        //config: currentMaxAge 30 days prevMinAge=10 days

        //start day 37 days before now, end day 26 days before now
        //time diff: currentMinAge=26 days, prevMaxAge=11 day 
        final Date date1Start = new Date(now.getTime() - 37l * 24l * 60l * 60l * 1000l);
        final Date date1End = new Date(now.getTime() - 26l * 24l * 60l * 60l * 1000l);
        createNewPreviousPermanentPrice("1000011", Double.valueOf("20"), date1Start, date1End, currencyModel);

        //start day 26 days before now, end day 15 days before now
        //time diff: currentMinAge=15 days, prevMaxAge=11 day 
        final Date date2Start = new Date(now.getTime() - 26l * 24l * 60l * 60l * 1000);
        final Date date2End = new Date(now.getTime() - 15l * 24l * 60l * 60l * 1000);
        createNewPreviousPermanentPrice("1000011", Double.valueOf("10"), date2Start, date2End, currencyModel);

        final TargetPreviousPermanentPriceModel targetPreviousPermanentPriceModel = targetPreviousPermanentPriceDao
                .findValidProductPreviousPrice("1000011", "30", "10", currencyModel);

        //return the latest end time record
        Assert.assertEquals(Double.valueOf("10"), targetPreviousPermanentPriceModel.getPrice());
    }

    @Test
    public void testFindAllEndedTargetPreviousPermanentPriceRecords() {
        final int currentPermPriceMaxAgeDays = 14;
        final int previousPermPriceMinAgeDays = 14;
        final int batchSize = 100;

        // Ended too long ago
        final DateTime startDate1 = DateTime.now().minusDays(30);
        final DateTime endDate1 = DateTime.now().minusDays(15);
        final TargetPreviousPermanentPriceModel prevPermPrice1 = createNewPreviousPermanentPrice("1000011",
                Double.valueOf(20), startDate1.toDate(), endDate1.toDate(), currencyModel);

        // Hasn't ended
        final DateTime startDate2 = DateTime.now();
        final TargetPreviousPermanentPriceModel prevPermPrice2 = createNewPreviousPermanentPrice("1000011",
                Double.valueOf(20), startDate2.toDate(), null, currencyModel);

        // Didn't exist long enough
        final DateTime startDate3 = DateTime.now().minusDays(15);
        final DateTime endDate3 = DateTime.now().minusDays(10);
        final TargetPreviousPermanentPriceModel prevPermPrice3 = createNewPreviousPermanentPrice("1000011",
                Double.valueOf(20), startDate3.toDate(), endDate3.toDate(), currencyModel);

        // Still valid
        final DateTime startDate4 = DateTime.now().minusDays(31);
        final DateTime endDate4 = DateTime.now().minusDays(2);
        final TargetPreviousPermanentPriceModel prevPermPrice4 = createNewPreviousPermanentPrice("1000011",
                Double.valueOf(20), startDate4.toDate(), endDate4.toDate(), currencyModel);

        final List<TargetPreviousPermanentPriceModel> result = targetPreviousPermanentPriceDao
                .findAllRemovableTargetPreviousPermanentPriceRecords(currentPermPriceMaxAgeDays,
                        previousPermPriceMinAgeDays, batchSize);
        assertThat(result).contains(prevPermPrice1, prevPermPrice3).excludes(prevPermPrice2, prevPermPrice4);
    }

    public TargetPreviousPermanentPriceModel createNewPreviousPermanentPrice(final String variantCode,
            final Double price, final Date startDate, final Date endDate, final CurrencyModel currency) {
        final TargetPreviousPermanentPriceModel targetPreviousPriceModel = modelService.create(
                TargetPreviousPermanentPriceModel.class);
        targetPreviousPriceModel.setVariantCode(variantCode);
        targetPreviousPriceModel.setPrice(price);
        targetPreviousPriceModel.setStartDate(getDateWithDayLightAdjustment(startDate));
        targetPreviousPriceModel.setEndDate(getDateWithDayLightAdjustment(endDate));
        targetPreviousPriceModel.setCurrency(currency);
        modelService.save(targetPreviousPriceModel);
        return targetPreviousPriceModel;
    }

    private Date getDateWithDayLightAdjustment(final Date date) {
        if (null != date && TimeZone.getDefault().inDaylightTime(date)) {
            return DateUtils.addHours(date, -1);
        }
        return date;
    }
}
