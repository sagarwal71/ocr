package au.com.target.tgtcore.price;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.model.TargetPreviousPermanentPriceModel;
import au.com.target.tgtcore.price.daos.TargetPreviousPermanentPriceDao;

import com.google.common.base.Charsets;


/**
 * @author knemalik
 * 
 */

@IntegrationTest
public class TargetPreviousPermanentPriceDaoImplTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetPreviousPermanentPriceDao targetPreviousPermanentPriceDao;

    @Before
    public void setUp() throws ImpExException {

        importCsv("/tgtcore/test/testTargetPreviousPermanantPrice.impex", Charsets.UTF_8.name());
    }


    @Test(expected = IllegalArgumentException.class)
    public void testfindProductWithNullEndDateByEmptyProductCode()
    {
        targetPreviousPermanentPriceDao.findProductWithNullEndDate("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindProductWithNullEndDateNullProductCode()
    {
        targetPreviousPermanentPriceDao.findProductWithNullEndDate(null);
    }

    @Test
    public void testFindProductWithNullEndDateWithNonExistentProductCode()
    {
        final List<TargetPreviousPermanentPriceModel> productWithNUllDate = targetPreviousPermanentPriceDao
                .findProductWithNullEndDate("0189876");
        Assert.assertNotNull(productWithNUllDate);
        Assert.assertTrue(CollectionUtils.isEmpty(productWithNUllDate));

    }

    @Test
    public void testFindProductWithNullEndDateForStartDateAttribute() throws ParseException
    {

        final TargetPreviousPermanentPriceModel productWithNUllDate = targetPreviousPermanentPriceDao
                .findProductWithNullEndDate("10000073").get(0);
        Assert.assertNotNull(productWithNUllDate.getStartDate());
        Assert.assertEquals("03.01.2006 07:56",
                new SimpleDateFormat("dd.MM.yyyy hh:mm").format(productWithNUllDate.getStartDate()));

    }

    @Test
    public void testFindProductWithNullEndDateForPriceAttribute()
    {

        final TargetPreviousPermanentPriceModel productWithNUllDate = targetPreviousPermanentPriceDao
                .findProductWithNullEndDate("10000095").get(0);
        Assert.assertNotNull(productWithNUllDate.getPrice());
        Assert.assertEquals(productWithNUllDate.getPrice().toString(), "25.0");
    }

    @Test
    public void testFindProductWithNullEndDateForNullEndDateAttirbute()
    {

        final TargetPreviousPermanentPriceModel productWithNUllDate = targetPreviousPermanentPriceDao
                .findProductWithNullEndDate("10000095").get(0);
        Assert.assertNull(productWithNUllDate.getEndDate());
    }

}