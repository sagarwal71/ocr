/**
 * 
 */
package au.com.target.tgtcore.price;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import au.com.target.tgtcore.price.data.PriceRangeInformation;

import com.google.common.base.Charsets;


/**
 * Integration test for TargetCommercePriceServiceIntTest.getPriceRangeInfoForProduct method, <br/>
 * for cases of products with and without variants.
 * 
 * This test is in tgttest since the impex references PurchaseOption objects from tgtlayby.
 * 
 */
@IntegrationTest
@Ignore
public class TargetCommercePriceServiceIntTest extends ServicelayerTransactionalTest {

    @Resource
    private UserService userService;

    @Resource
    private ProductService productService;

    @Resource
    private TargetCommercePriceService targetCommercePriceService;

    // Test products
    private ProductModel product;
    private ProductModel productSoldOut;
    private ProductModel productWithDifferentPriceVars;
    private ProductModel productWithSamePriceVars;
    private ProductModel productWithVarsSoldOut;
    private ProductModel productWithSomeVarsSoldOut;
    private ProductModel productWithWas;


    @Before
    public void setUp() throws Exception {
        userService.setCurrentUser(userService.getAdminUser());
        importCsv("/tgtcore/test/testTargetCommercePriceService.impex", Charsets.UTF_8.name());

        // Product with no size variants, price=5
        product = productService.getProductForCode("P1000");

        // Product with size variants, price 10-30
        productWithDifferentPriceVars = productService.getProductForCode("P1001");

        // Product with size variants, all price 40
        productWithSamePriceVars = productService.getProductForCode("P1002");

        // Product with no size variants, sold out
        productSoldOut = productService.getProductForCode("P1003");

        // Product with size variants, all sold out
        productWithVarsSoldOut = productService.getProductForCode("P1004");

        // Product with size variants, one sold out
        productWithSomeVarsSoldOut = productService.getProductForCode("P1005");

        // Product with no size variants, with was price
        productWithWas = productService.getProductForCode("P1006");
    }


    @Test
    public void testWebPrice() {
        final PriceInformation priceInfo = targetCommercePriceService.getWebPriceForProduct(product);

        Assert.assertNotNull("PriceInformation is null", priceInfo);
        Assert.assertNotNull("PriceInformation.priceValue is null", priceInfo.getPriceValue());
        Assert.assertEquals("Web price should be 5", new Double(5), new Double(priceInfo.getPriceValue().getValue()));
    }


    @Test
    public void testPriceRangeInfoForProductWithNoVariants() {

        final PriceRangeInformation rangeInfo = targetCommercePriceService.getPriceRangeInfoForProduct(product);

        // Check there will be both a low and a high and they will be equal to the supplied reference value
        Assert.assertNotNull("PriceRangeInformation is null", rangeInfo);
        Assert.assertNotNull("PriceRangeInformation from price is null", rangeInfo.getFromPrice());
        Assert.assertNotNull("PriceRangeInformation to price is null", rangeInfo.getToPrice());
        Assert.assertEquals("From price should be 5", new Double(5), rangeInfo.getFromPrice());
        Assert.assertEquals("To price should be 5", new Double(5), rangeInfo.getToPrice());
    }

    @Test
    public void testPriceRangeInfoForProductWithVariantsDifferentPrices() {

        final PriceRangeInformation rangeInfo = targetCommercePriceService
                .getPriceRangeInfoForProduct(productWithDifferentPriceVars);

        // Check there will be both a low and a high and they will be equal to the supplied reference value
        Assert.assertNotNull("PriceRangeInformation is null", rangeInfo);
        Assert.assertNotNull("PriceRangeInformation from price is null", rangeInfo.getFromPrice());
        Assert.assertNotNull("PriceRangeInformation to price is null", rangeInfo.getToPrice());
        Assert.assertEquals("From price should be 10", new Double(10), rangeInfo.getFromPrice());
        Assert.assertEquals("To price should be 30", new Double(30), rangeInfo.getToPrice());
    }

    @Test
    public void testPriceRangeInfoForProductWithVariantsSamePrices() {

        final PriceRangeInformation rangeInfo = targetCommercePriceService
                .getPriceRangeInfoForProduct(productWithSamePriceVars);

        // Check there will be both a low and a high and they will be equal to the supplied reference value
        Assert.assertNotNull("PriceRangeInformation is null", rangeInfo);
        Assert.assertNotNull("PriceRangeInformation from price is null", rangeInfo.getFromPrice());
        Assert.assertNotNull("PriceRangeInformation to price is null", rangeInfo.getToPrice());
        Assert.assertEquals("From price should be 40", new Double(40), rangeInfo.getFromPrice());
        Assert.assertEquals("To price should be 40", new Double(40), rangeInfo.getToPrice());
    }

    @Test
    public void testPriceRangeInfoForProductWithNoVariantsSoldOut() {

        final PriceRangeInformation rangeInfo = targetCommercePriceService.getPriceRangeInfoForProduct(productSoldOut);

        Assert.assertNotNull("PriceRangeInformation is null", rangeInfo);
        Assert.assertEquals("From price should be 5", new Double(5), rangeInfo.getFromPrice());
        Assert.assertEquals("To price should be 5", new Double(5), rangeInfo.getToPrice());
    }

    @Test
    public void testPriceRangeInfoForProductWithVariantsSoldOut() {

        final PriceRangeInformation rangeInfo = targetCommercePriceService
                .getPriceRangeInfoForProduct(productWithVarsSoldOut);

        Assert.assertNotNull("PriceRangeInformation is null", rangeInfo);
        Assert.assertEquals("From price should be 40", new Double(40), rangeInfo.getFromPrice());
        Assert.assertEquals("To price should be 40", new Double(40), rangeInfo.getToPrice());
    }

    @Test
    public void testPriceRangeInfoForProductWithVariantsOneSoldOut() {

        final PriceRangeInformation rangeInfo = targetCommercePriceService
                .getPriceRangeInfoForProduct(productWithSomeVarsSoldOut);

        // Check there will be both a low and a high and they will be equal to the supplied reference value
        Assert.assertNotNull("PriceRangeInformation is null", rangeInfo);
        Assert.assertNotNull("PriceRangeInformation from price is null", rangeInfo.getFromPrice());
        Assert.assertNotNull("PriceRangeInformation to price is null", rangeInfo.getToPrice());
        Assert.assertEquals("From price should be 40", new Double(40), rangeInfo.getFromPrice());
        Assert.assertEquals("To price should be 60", new Double(60), rangeInfo.getToPrice());
    }

    @Test
    public void testWasPriceRangeInfoForProductWithNoVariantsAndNoWas() {

        final PriceRangeInformation rangeInfo = targetCommercePriceService.getPriceRangeInfoForProduct(product);

        // Check there is no was price data
        Assert.assertNotNull("PriceRangeInformation is null", rangeInfo);
        Assert.assertNull("PriceRangeInformation from was price should be null", rangeInfo.getFromWasPrice());
        Assert.assertNull("PriceRangeInformation to was price should be null", rangeInfo.getToWasPrice());

    }

    @Test
    public void testWasPriceRangeInfoForProductWithNoVariantsAndWithWas() {

        final PriceRangeInformation rangeInfo = targetCommercePriceService.getPriceRangeInfoForProduct(productWithWas);

        // Check there is a was price, with low and high equal to reference value
        Assert.assertNotNull("PriceRangeInformation is null", rangeInfo);
        Assert.assertNotNull("PriceRangeInformation from was price should not be null", rangeInfo.getFromWasPrice());
        Assert.assertNotNull("PriceRangeInformation to was price should not be null", rangeInfo.getToWasPrice());
        Assert.assertEquals("From was price should be 110", new Double(110), rangeInfo.getFromWasPrice());
        Assert.assertEquals("To was price should be 110", new Double(110), rangeInfo.getToWasPrice());
    }

    @Test
    public void testWasPriceRangeInfoForVariantsWithSamePrice() {

        final PriceRangeInformation rangeInfo = targetCommercePriceService
                .getPriceRangeInfoForProduct(productWithSamePriceVars);

        // Check there will be both a low and a high and they will be equal to the supplied reference value
        Assert.assertNotNull("PriceRangeInformation is null", rangeInfo);
        Assert.assertNotNull("PriceRangeInformation from was price should not be null", rangeInfo.getFromWasPrice());
        Assert.assertNotNull("PriceRangeInformation to was price should not be null", rangeInfo.getToWasPrice());
        Assert.assertEquals("From was price should be 110", new Double(110), rangeInfo.getFromWasPrice());
        Assert.assertEquals("To was price should be 110", new Double(110), rangeInfo.getToWasPrice());
    }

    @Test
    public void testWasPriceRangeInfoForVariantsWithDifferentPrices() {

        final PriceRangeInformation rangeInfo = targetCommercePriceService
                .getPriceRangeInfoForProduct(productWithDifferentPriceVars);

        // Check there will be both a low and a high and they are different and in the right order
        Assert.assertNotNull("PriceRangeInformation is null", rangeInfo);
        Assert.assertNotNull("PriceRangeInformation from was price should not be null", rangeInfo.getFromWasPrice());
        Assert.assertNotNull("PriceRangeInformation to was price should not be null", rangeInfo.getToWasPrice());
        Assert.assertEquals("From was price should be 80", new Double(80), rangeInfo.getFromWasPrice());
        Assert.assertEquals("To was price should be 100", new Double(100), rangeInfo.getToWasPrice());
        Assert.assertTrue("From was should be less than to price",
                rangeInfo.getFromWasPrice().doubleValue() < rangeInfo.getToWasPrice().doubleValue());

    }

}
