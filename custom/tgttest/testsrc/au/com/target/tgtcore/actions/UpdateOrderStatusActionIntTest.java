/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import java.util.Date;

import javax.annotation.Resource;

import org.junit.Test;

import org.junit.Assert;


@IntegrationTest
public class UpdateOrderStatusActionIntTest extends ServicelayerTransactionalTest {

    @Resource
    private UpdateOrderStatusAction updateOrderStatusCreatedAction;

    @Resource
    private UpdateOrderStatusAction updatePreOrderStatusParkedAction;

    @Resource
    private ModelService modelService;



    @Test
    public void testReviewOrderStatusAction() throws RetryLaterException, Exception {

        final OrderStatus newStatus = populateOrderStatus(updateOrderStatusCreatedAction, "testOrder");

        Assert.assertEquals(OrderStatus.CREATED, newStatus);
    }

    /**
     * Test method is used to verify parked order status of the Pre-Order product
     * 
     * @throws RetryLaterException
     * @throws Exception
     */
    @Test
    public void testParkedOrderStatusAction() throws RetryLaterException, Exception {

        final OrderStatus orderStatus = populateOrderStatus(updatePreOrderStatusParkedAction, "testPreOrderParked");

        Assert.assertEquals(OrderStatus.PARKED, orderStatus);
    }


    /**
     * Method to populate order status of pre-order product.
     * 
     * @param updateOrderStatusAction
     * @param orderCode
     * @return orderStatus
     * @throws RetryLaterException
     * @throws Exception
     */
    private OrderStatus populateOrderStatus(final UpdateOrderStatusAction updateOrderStatusAction,
            final String orderCode) throws RetryLaterException, Exception {

        final OrderModel orderModel = createOrderForCode(orderCode);

        final OrderProcessModel orderProcessModel = new OrderProcessModel();
        orderProcessModel.setOrder(orderModel);

        updateOrderStatusAction.executeAction(orderProcessModel);

        final OrderStatus orderStatus = orderModel.getStatus();

        return orderStatus;
    }

    private OrderModel createOrderForCode(final String code) {

        final CurrencyModel currency = modelService.create(CurrencyModel.class);
        currency.setIsocode("TEST");
        currency.setSymbol("TEST");
        modelService.save(currency);
        final UserModel user = modelService.create(UserModel.class);
        user.setUid("user");
        modelService.save(user);

        final OrderModel order = new OrderModel();
        order.setCode(code);
        order.setCurrency(currency);
        order.setUser(user);
        order.setDate(new Date());

        return order;
    }

}
