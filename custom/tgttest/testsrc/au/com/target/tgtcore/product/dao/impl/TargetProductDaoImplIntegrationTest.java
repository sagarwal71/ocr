/**
 * 
 */
package au.com.target.tgtcore.product.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.TargetProductDepartmentService;
import au.com.target.tgtcore.product.TargetProductService;


/**
 * @author rmcalave
 * 
 */
@IntegrationTest
public class TargetProductDaoImplIntegrationTest extends ServicelayerTransactionalTest {

    private static final Logger LOG = Logger.getLogger(TargetProductDaoImplIntegrationTest.class);

    @Resource(name = "targetProductService")
    private TargetProductService targetProductService;

    @Resource(name = "targetProductDao")
    private TargetProductDaoImpl targetProductDaoImpl;

    @Resource(name = "targetProductDepartmentService")
    private TargetProductDepartmentService targetProductDepartmentServiceImpl;

    @Resource
    private CatalogVersionService catalogVersionService;

    @Resource
    private UserService userService;

    @Resource
    private ModelService modelService;

    private CatalogVersionModel stagedCatalogVersion;
    private CatalogVersionModel onlineCatalogVersion;

    @Before
    public void setUp() throws ImpExException {
        importCsv("/tgtcore/test/testSystemSetup.impex", Charsets.UTF_8.name());
        userService.setCurrentUser(userService.getAdminUser());
        stagedCatalogVersion = catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION);
        onlineCatalogVersion = catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.ONLINE_VERSION);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindBulkyBoardProductsWithNoCatalogVersion() {
        targetProductDaoImpl.findBulkyBoardProducts(null);
    }

    @Test
    public void testFindBulkyBoardProductsWithNoProducts() {
        final List<TargetProductModel> result = targetProductDaoImpl.findBulkyBoardProducts(stagedCatalogVersion);

        assertThat(result).hasSize(0);
    }

    @Test
    public void testFindBulkyBoardProductsWithOfflineProducts() throws ImpExException {
        importCsv("/tgtcore/test/testOfflineBulkyBoardProducts.impex", Charsets.UTF_8.name());

        final List<TargetProductModel> result = targetProductDaoImpl.findBulkyBoardProducts(stagedCatalogVersion);

        final ProductModel p1000 = targetProductService.getProductForCode(stagedCatalogVersion, "P1000");
        final ProductModel p1001 = targetProductService.getProductForCode(stagedCatalogVersion, "P1001");

        assertThat(result).isNotNull().hasSize(2).containsOnly(p1000, p1001);
    }

    @Test
    public void testFindBulkyBoardProductsWithOfflineProductsOnlineCatalog() throws ImpExException {
        importCsv("/tgtcore/test/testOfflineBulkyBoardProducts.impex", Charsets.UTF_8.name());

        final List<TargetProductModel> result = targetProductDaoImpl.findBulkyBoardProducts(onlineCatalogVersion);

        assertThat(result).hasSize(0);
    }

    @Test
    public void testFindBulkyBoardProductsWithOfflineProductsNoBulkyBoard() throws ImpExException {
        importCsv("/tgtcore/test/testOfflineNonBulkyBoardProducts.impex", Charsets.UTF_8.name());

        final List<TargetProductModel> result = targetProductDaoImpl.findBulkyBoardProducts(stagedCatalogVersion);

        assertThat(result).hasSize(0);
    }

    @Test
    public void testFindProductByMerchDepartment() throws ImpExException {
        importCsv("/tgtcore/test/testProductsCountInMerchDepartment.impex", Charsets.UTF_8.name());
        final TargetMerchDepartmentModel targetMerchDepartmentModel = targetProductDepartmentServiceImpl
                .getDepartmentCategoryModel(stagedCatalogVersion, Integer.valueOf(2000));

        final List<TargetColourVariantProductModel> result = targetProductDaoImpl.findProductByMerchDepartment(
                targetMerchDepartmentModel, 0, 7);

        assertThat(result).hasSize(7);
    }

    @Test
    public void testFindProductByMerchDepartmentOverTotal() throws ImpExException {
        importCsv("/tgtcore/test/testProductsCountInMerchDepartment.impex", Charsets.UTF_8.name());
        final TargetMerchDepartmentModel targetMerchDepartmentModel = targetProductDepartmentServiceImpl
                .getDepartmentCategoryModel(stagedCatalogVersion, Integer.valueOf(2000));

        final List<TargetColourVariantProductModel> result = targetProductDaoImpl.findProductByMerchDepartment(
                targetMerchDepartmentModel, 0, 10);//total is 8

        assertThat(result).hasSize(8);
    }

    @Test
    public void testFindProductByEanNoProductForGivenEan() throws ImpExException {
        importCsv("/tgtcore/test/testProducts.impex", Charsets.UTF_8.name());
        final List<ProductModel> result = targetProductDaoImpl.findProductByEan("0065541802292"); // No products with this EAN in test data

        assertThat(result).hasSize(0);
    }

    @Test
    public void testFindProductByEanOneProductForGivenEan() throws ImpExException {
        importCsv("/tgtcore/test/testProducts.impex", Charsets.UTF_8.name());
        final List<ProductModel> result = targetProductDaoImpl.findProductByEan("9300633499815");// 1 product with this EAN in test data

        assertThat(result).hasSize(1);
    }

    @Test
    public void testFindProductByEanMultipleProductForGivenEan() throws ImpExException {
        importCsv("/tgtcore/test/testProducts.impex", Charsets.UTF_8.name());
        final List<ProductModel> result = targetProductDaoImpl.findProductByEan("817980010536");// 3 products with this EAN in test data

        assertThat(result).hasSize(3);
    }

    @Test
    public void testFindProductByOriginalCategoryIsNull() throws ImpExException {
        importCsv("/tgtcore/test/testProductsOriginalCategory.impex", Charsets.UTF_8.name());

        final List<ProductModel> totalProducts = targetProductDaoImpl.find();
        assertThat(totalProducts).hasSize(14);

        final List<TargetProductModel> result = targetProductDaoImpl
                .findProductsByOriginalCategoryNull(stagedCatalogVersion, 10, Collections.EMPTY_LIST);

        //out of 12 products only 2 products satisfy criteria 
        assertThat(result).hasSize(4);

        assertThat(result).onProperty("code").containsExactly("P5001", "P5002", "P50013", "P50014");
        assertThat(result).onProperty("originalCategory").containsExactly(null, null, null, null);
        assertThat(result).onProperty("approvalStatus").containsExactly(ArticleApprovalStatus.APPROVED,
                ArticleApprovalStatus.UNAPPROVED, ArticleApprovalStatus.APPROVED,
                ArticleApprovalStatus.UNAPPROVED);
        assertThat(result).onProperty("catalogVersion").containsExactly(stagedCatalogVersion, stagedCatalogVersion,
                stagedCatalogVersion, stagedCatalogVersion);
    }

    @Test
    public void testFindProductByOriginalCategoryBatchSize() throws ImpExException {
        importCsv("/tgtcore/test/testProductsOriginalCategory.impex", Charsets.UTF_8.name());

        final List<TargetProductModel> result = targetProductDaoImpl
                .findProductsByOriginalCategoryNull(stagedCatalogVersion, 1, Collections.EMPTY_LIST);

        //Even though 2 records are eligible to be return but batch size is set to 1 so it return one record
        assertThat(result).hasSize(1);
        assertThat(result).onProperty("code").containsExactly("P5001");
        assertThat(result).onProperty("approvalStatus").containsOnly(ArticleApprovalStatus.APPROVED);
        assertThat(result).onProperty("catalogVersion").containsOnly(stagedCatalogVersion);
    }

    @Test
    public void testFindProductByOriginalCategoryBatchExcludeOneItem() throws ImpExException {
        importCsv("/tgtcore/test/testProductsOriginalCategory.impex", Charsets.UTF_8.name());

        final List<TargetProductModel> result = targetProductDaoImpl
                .findProductsByOriginalCategoryNull(stagedCatalogVersion, 4, Arrays.asList("P50013"));
        //Even though 2 records are eligible to be return but batch size is set to 1 so it return one record
        assertThat(result).hasSize(3);
        LOG.info("ProductCode : " + result.get(0).getCode());
        assertThat(result).onProperty("catalogVersion").containsOnly(stagedCatalogVersion);
    }

    @Test
    public void testFindProductsNotSyncedToFluent() throws ImpExException {
        importCsv("/tgtfluent/test/testProducts.impex", Charsets.UTF_8.name());

        final List<TargetProductModel> productsWithoutFluentId = targetProductDaoImpl
                .findProductsNotSyncedToFluent(stagedCatalogVersion, 100, null);
        assertThat(productsWithoutFluentId).isNotEmpty();
        assertThat(productsWithoutFluentId).hasSize(12);
        assertThat(productsWithoutFluentId).onProperty("code").containsExactly("P5001", "P5002", "P5003", "P5005",
                "P5006", "P5007", "P5008", "P5009", "P5010", "P5011", "P5012", "P50014");
    }

    @Test
    public void testFindProductsNotSyncedToFluentWithPageSize() throws ImpExException {
        importCsv("/tgtfluent/test/testProducts.impex", Charsets.UTF_8.name());

        final List<TargetProductModel> productsWithoutFluentId = targetProductDaoImpl
                .findProductsNotSyncedToFluent(stagedCatalogVersion, 4, null);
        assertThat(productsWithoutFluentId).isNotEmpty();
        assertThat(productsWithoutFluentId).hasSize(4);
    }

    @Test
    public void testFindProductsNotSyncedToFluentWithExlusion() throws ImpExException {
        importCsv("/tgtfluent/test/testProducts.impex", Charsets.UTF_8.name());

        final List<TargetProductModel> productsWithoutFluentId = targetProductDaoImpl
                .findProductsNotSyncedToFluent(stagedCatalogVersion, 100, Arrays.asList("P50014"));
        assertThat(productsWithoutFluentId).isNotEmpty();
        assertThat(productsWithoutFluentId).hasSize(11);
        assertThat(productsWithoutFluentId).onProperty("code").containsExactly("P5001", "P5002", "P5003", "P5005",
                "P5006", "P5007", "P5008", "P5009", "P5010", "P5011", "P5012");
    }
}
