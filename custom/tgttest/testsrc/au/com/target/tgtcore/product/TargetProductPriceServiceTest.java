/**
 * 
 */
package au.com.target.tgtcore.product;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.util.StandardDateRange;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPriceRowModel;
import au.com.target.tgtcore.product.data.TargetPriceRowData;

import com.google.common.base.Charsets;


@IntegrationTest
public class TargetProductPriceServiceTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetProductPriceService targetProductPriceService;

    @Resource
    private ProductService productService;

    @Resource
    private CatalogVersionService catalogVersionService;

    private CatalogVersionModel offline;
    private CatalogVersionModel online;
    private StandardDateRange dateRange = null;

    private List<CatalogVersionModel> catalogs;


    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtcore/test/testSystemSetup.impex", Charsets.UTF_8.name());
        importCsv("/tgtcore/test/testProductPriceService.impex", Charsets.UTF_8.name());

        offline = catalogVersionService.getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION);

        online = catalogVersionService.getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.ONLINE_VERSION);

        catalogs = new ArrayList<>();

        catalogs.add(offline);
        catalogs.add(online);

        final Calendar cal = Calendar.getInstance();
        final Date now = cal.getTime();
        cal.add(Calendar.WEEK_OF_YEAR, 1);
        final Date nextWeek = cal.getTime();

        dateRange = new StandardDateRange(now, nextWeek);

    }

    @Test
    public void testRemoveFromSale()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        // Beforehand product p1001 should be APPROVED
        ProductModel productModelOffline = productService.getProductForCode(offline, "P1001");
        ProductModel productModelOnline = productService.getProductForCode(online, "P1001");
        Assert.assertEquals(ArticleApprovalStatus.APPROVED, productModelOffline.getApprovalStatus());
        Assert.assertEquals(ArticleApprovalStatus.APPROVED, productModelOnline.getApprovalStatus());

        final List<String> products = new ArrayList<>();
        products.add("P1001");

        targetProductPriceService.removeProductFromSale(products, catalogs);

        // Afterwards product p1001 should be in CHECK status
        productModelOffline = productService.getProductForCode(offline, "P1001");
        productModelOnline = productService.getProductForCode(online, "P1001");
        Assert.assertEquals(ArticleApprovalStatus.CHECK, productModelOffline.getApprovalStatus());
        Assert.assertEquals(ArticleApprovalStatus.CHECK, productModelOnline.getApprovalStatus());
    }

    @Test
    public void testUpdatePricesOnProductWithNoPrices()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        // Beforehand no price or tax group
        ProductModel productModelOffline = productService.getProductForCode(offline, "P1001");
        ProductModel productModelOnline = productService.getProductForCode(online, "P1001");
        Assert.assertTrue(CollectionUtils.isEmpty(productModelOffline.getEurope1Prices()));
        Assert.assertTrue(CollectionUtils.isEmpty(productModelOnline.getEurope1Prices()));
        Assert.assertNull(productModelOffline.getEurope1PriceFactory_PTG());
        Assert.assertNull(productModelOnline.getEurope1PriceFactory_PTG());

        final List<ProductModel> products = targetProductPriceService.getProductModelForCatalogs("P1001", catalogs, 6,
                7, null, 1000, true);

        // After has price and tax group
        Assert.assertTrue(CollectionUtils.isNotEmpty(products));
        productModelOffline = productService.getProductForCode(offline, "P1001");
        productModelOnline = productService.getProductForCode(online, "P1001");
        Assert.assertTrue(CollectionUtils.isNotEmpty(productModelOffline.getEurope1Prices()));
        Assert.assertTrue(CollectionUtils.isNotEmpty(productModelOnline.getEurope1Prices()));
        Assert.assertTrue(productModelOffline.getEurope1Prices().size() == 1);
        Assert.assertTrue(productModelOnline.getEurope1Prices().size() == 1);
        Assert.assertEquals(Double.valueOf(6), productModelOffline.getEurope1Prices().iterator().next().getPrice());
        Assert.assertEquals(Double.valueOf(6), productModelOnline.getEurope1Prices().iterator().next().getPrice());
        Assert.assertEquals("AUD", productModelOffline.getEurope1Prices().iterator().next().getCurrency().getIsocode());
        Assert.assertEquals("AUD", productModelOnline.getEurope1Prices().iterator().next().getCurrency().getIsocode());
        Assert.assertEquals("aus-gst-1000", productModelOffline.getEurope1PriceFactory_PTG().getCode());
        Assert.assertEquals("aus-gst-1000", productModelOnline.getEurope1PriceFactory_PTG().getCode());
    }

    @Test
    public void testUpdatePricesOnProductWithDifferentPriceAndTax() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        // Existing price is 5 and tax group 1000
        ProductModel productModelOffline = productService.getProductForCode(offline, "P1002");
        ProductModel productModelOnline = productService.getProductForCode(online, "P1002");
        Assert.assertTrue(productModelOffline.getEurope1Prices().size() == 1);
        Assert.assertTrue(productModelOnline.getEurope1Prices().size() == 1);
        Assert.assertEquals(Double.valueOf(5), productModelOffline.getEurope1Prices().iterator().next().getPrice());
        Assert.assertEquals(Double.valueOf(5), productModelOnline.getEurope1Prices().iterator().next().getPrice());
        Assert.assertEquals("aus-gst-1000", productModelOffline.getEurope1PriceFactory_PTG().getCode());
        Assert.assertEquals("aus-gst-1000", productModelOnline.getEurope1PriceFactory_PTG().getCode());

        final List<ProductModel> updatedProducts = targetProductPriceService.getProductModelForCatalogs("P1002",
                catalogs, 6,
                7, null,
                1005, true);

        // Price updated to price=6, was=7, tax group 1005
        Assert.assertTrue(CollectionUtils.isNotEmpty(updatedProducts));
        productModelOffline = productService.getProductForCode(offline, "P1002");
        productModelOnline = productService.getProductForCode(online, "P1002");
        Assert.assertTrue(productModelOffline.getEurope1Prices().size() == 1);
        Assert.assertTrue(productModelOnline.getEurope1Prices().size() == 1);
        Assert.assertEquals(Double.valueOf(6), productModelOffline.getEurope1Prices().iterator().next().getPrice());
        Assert.assertEquals(Double.valueOf(6), productModelOnline.getEurope1Prices().iterator().next().getPrice());
        Assert.assertEquals(Double.valueOf(7),
                ((TargetPriceRowModel)productModelOffline.getEurope1Prices().iterator().next()).getWasPrice());
        Assert.assertEquals(Double.valueOf(7),
                ((TargetPriceRowModel)productModelOnline.getEurope1Prices().iterator().next()).getWasPrice());
        Assert.assertEquals(Boolean.TRUE,
                ((TargetPriceRowModel)productModelOffline.getEurope1Prices().iterator().next()).getPromoEvent());
        Assert.assertEquals(Boolean.TRUE,
                ((TargetPriceRowModel)productModelOnline.getEurope1Prices().iterator().next()).getPromoEvent());
        Assert.assertEquals("aus-gst-1005", productModelOffline.getEurope1PriceFactory_PTG().getCode());
        Assert.assertEquals("aus-gst-1005", productModelOnline.getEurope1PriceFactory_PTG().getCode());
    }

    @Test
    public void testUpdatePricesOnProductWithSamePriceAndTax()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        // Existing price is 5 and tax group 1000
        final ProductModel productModelOffline = productService.getProductForCode(offline, "P1002");
        final ProductModel productModelOnline = productService.getProductForCode(online, "P1002");
        Assert.assertTrue(productModelOffline.getEurope1Prices().size() == 1);
        Assert.assertTrue(productModelOnline.getEurope1Prices().size() == 1);
        Assert.assertEquals(Double.valueOf(5), productModelOffline.getEurope1Prices().iterator().next().getPrice());
        Assert.assertEquals(Double.valueOf(5), productModelOnline.getEurope1Prices().iterator().next().getPrice());
        Assert.assertEquals("aus-gst-1000", productModelOffline.getEurope1PriceFactory_PTG().getCode());
        Assert.assertEquals("aus-gst-1000", productModelOnline.getEurope1PriceFactory_PTG().getCode());

        final List<ProductModel> updatedProducts = targetProductPriceService.getProductModelForCatalogs("P1002",
                catalogs, 5,
                5, null,
                1000, true);

        // Nothing updated
        Assert.assertTrue(CollectionUtils.isEmpty(updatedProducts));
    }

    @Test
    public void testUpdatePricesOnProductWithAddedFutures()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        // Existing price is 5 only
        ProductModel productModelOffline = productService.getProductForCode(offline, "P1002");
        ProductModel productModelOnline = productService.getProductForCode(online, "P1002");
        Assert.assertTrue(productModelOffline.getEurope1Prices().size() == 1);
        Assert.assertTrue(productModelOnline.getEurope1Prices().size() == 1);
        Assert.assertEquals(Double.valueOf(5), productModelOffline.getEurope1Prices().iterator().next().getPrice());
        Assert.assertEquals(Double.valueOf(5), productModelOnline.getEurope1Prices().iterator().next().getPrice());

        final TargetPriceRowData row1 = new TargetPriceRowData();
        row1.setSellPrice(4);
        row1.setWasPrice(4.5);
        row1.setDateRange(dateRange);
        final TargetPriceRowData row2 = new TargetPriceRowData();
        row2.setSellPrice(3);
        row2.setWasPrice(3.5);
        row2.setDateRange(dateRange);
        final List<TargetPriceRowData> list = new ArrayList<>();
        list.add(row1);
        list.add(row2);

        final List<ProductModel> updated = targetProductPriceService.getProductModelForCatalogs("P1002", catalogs, 5,
                5, list,
                1000, true);

        // Future prices added
        // Note we are not assuming the prices are guaranteed to come in any specific order
        Assert.assertTrue(CollectionUtils.isNotEmpty(updated));
        productModelOffline = productService.getProductForCode(offline, "P1002");
        productModelOnline = productService.getProductForCode(online, "P1002");
        Assert.assertTrue(productModelOffline.getEurope1Prices().size() == 3);
        Assert.assertTrue(productModelOnline.getEurope1Prices().size() == 3);
        final Iterator<PriceRowModel> itOff = productModelOffline.getEurope1Prices().iterator();
        final Iterator<PriceRowModel> itOn = productModelOnline.getEurope1Prices().iterator();

        checkPrices(itOff);
        checkPrices(itOn);
    }

    private void checkPrices(final Iterator<PriceRowModel> it) {

        boolean found5 = false;
        boolean found4 = false;
        boolean found3 = false;

        while (it.hasNext()) {
            final TargetPriceRowModel priceRow = (TargetPriceRowModel)it.next();
            final double price = priceRow.getPrice().doubleValue();

            if (price == 3) {
                found3 = true;
                Assert.assertEquals(Double.valueOf(3.5), priceRow.getWasPrice());
                Assert.assertEquals(dateRange, priceRow.getDateRange());
            }
            if (price == 4) {
                found4 = true;
                Assert.assertEquals(Double.valueOf(4.5), priceRow.getWasPrice());
                Assert.assertEquals(dateRange, priceRow.getDateRange());
            }
            if (price == 5) {
                found5 = true;
                Assert.assertNull(priceRow.getDateRange());
            }
        }

        Assert.assertTrue(found3);
        Assert.assertTrue(found4);
        Assert.assertTrue(found5);

    }


}
