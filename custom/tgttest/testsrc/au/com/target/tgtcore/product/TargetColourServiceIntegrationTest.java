/**
 * 
 */
package au.com.target.tgtcore.product;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.AbstractConcurrentJaloSessionCallable;
import au.com.target.tgtcore.model.ColourModel;
import org.junit.Assert;


@IntegrationTest
public class TargetColourServiceIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private ColourService targetColourService;

    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtcore/test/testTargetColourService.impex", Charsets.UTF_8.name());
    }

    @Test
    public void testGetTargetColorByName() {
        final ColourModel red = targetColourService.getColourForFuzzyName("red", false, true);
        Assert.assertNotNull(red);
        Assert.assertEquals("red", red.getName());
    }

    @Test
    public void testCreateTargetColorByName() {
        final ColourModel fakered = targetColourService.getColourForFuzzyName("fakered", true, true);
        Assert.assertNotNull(fakered);
        Assert.assertEquals("fakered", fakered.getName());
    }

    @Test
    public void testConcurrentCreateDoesNotProduceDuplicateColours() throws InterruptedException, ExecutionException {
        final int noOfThreads = 2;
        // barrier to ensure both threads start at exactly the same time
        final CyclicBarrier barrier = new CyclicBarrier(noOfThreads);
        final String colourName = "tropic";
        final ExecutorService executor = Executors.newFixedThreadPool(noOfThreads);
        final AbstractConcurrentJaloSessionCallable<ColourModel> createColour = new AbstractConcurrentJaloSessionCallable() {

            @Override
            public final ColourModel execute() throws InterruptedException, BrokenBarrierException {
                barrier.await();
                return targetColourService.getColourForFuzzyName(colourName, true, true);
            }
        };

        final List<Future<ColourModel>> futures = new ArrayList<>();
        for (int i = 0; i < noOfThreads; i++) {
            futures.add(executor.submit(createColour));
        }

        for (int i = 1; i < noOfThreads; i++) {
            Assert.assertEquals(futures.get(i).get(), futures.get(i - 1).get());
        }

        // if a concurrency issue does occur, a subsequent invocation would return null coz of ambiguous results
        final ColourModel colour = targetColourService.getColourForFuzzyName(colourName, true, true);
        Assert.assertNotNull(colour);
        Assert.assertEquals(colour, futures.get(0).get());
    }

}
