/**
 * 
 */
package au.com.target.tgtcore.product.dao.impl;


import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.model.SizeTypeModel;

import com.google.common.base.Charsets;


/**
 * The Class TargetSizeTypeDaoImplIntegrationTest.
 * 
 * @author nandini
 */
@IntegrationTest
public class TargetSizeTypeDaoImplIntegrationTest extends ServicelayerTransactionalTest {

    /** The model service. */
    @Resource
    private ModelService modelService;

    /** The size type dao. */
    @Resource
    private TargetSizeTypeDaoImpl sizeTypeDao;

    /**
     * Sets the up.
     * 
     * @throws ImpExException
     *             the imp ex exception
     */
    @Before
    public void setUp() throws ImpExException {
        importCsv("/tgtcore/test/test-size-config.impex", Charsets.UTF_8.name());
    }

    /**
     * Test empty.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetByCodeNull() {
        final String code = null;
        sizeTypeDao.getSizeTypeByCode(code);
    }

    /**
     * Test get by code.
     */
    @Test
    public void testGetByCode() {
        final SizeTypeModel sizeTypeModel = sizeTypeDao.getSizeTypeByCode("samplecode2");

        Assert.assertNotNull(sizeTypeModel);
        Assert.assertEquals("samplecode2", sizeTypeModel.getCode());
        Assert.assertEquals("samplename2", sizeTypeModel.getName());
        Assert.assertFalse(sizeTypeModel.getIsDefault().booleanValue());
        Assert.assertTrue(sizeTypeModel.getIsSizeChartDisplay().booleanValue());
    }

    /**
     * Test get by code not exists.
     */
    @Test
    public void testGetByCodeNotExists() {
        final SizeTypeModel sizeType = sizeTypeDao.getSizeTypeByCode("code");

        Assert.assertNull(sizeType);
    }

    /**
     * Test get size type config record not exists.
     */
    @Test
    public void testGetDefaultSizeType() {
        final SizeTypeModel sizeTypeModel = sizeTypeDao.getDefaultSizeType();

        Assert.assertEquals("samplecode1", sizeTypeModel.getCode());
        Assert.assertEquals("samplename1", sizeTypeModel.getName());
        Assert.assertTrue(sizeTypeModel.getIsDefault().booleanValue());
        Assert.assertTrue(sizeTypeModel.getIsSizeChartDisplay().booleanValue());
    }

}
