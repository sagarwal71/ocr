package au.com.target.tgtcore.product.dao.impl;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.product.dao.ProductTypeDao;


/**
 * Test suite for {@link TargetProductTypeDaoImpl}.
 */
@IntegrationTest
public class TargetProductTypeDaoImplTest extends ServicelayerTransactionalTest {

    public static final String PRODUCT_TYPE_CODE = "type1";

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException exception = ExpectedException.none();
    //CHECKSTYLE:ON

    @Resource
    private ModelService modelService;

    @Resource
    private ProductTypeDao productTypeDao;


    private ProductTypeModel productType;


    /**
     * Initializes test cases before run.
     */
    @Before
    public void setUp() {
        productType = modelService.create(ProductTypeModel.class);
        productType.setCode(PRODUCT_TYPE_CODE);
        productType.setName(PRODUCT_TYPE_CODE);
        productType.setBulky(Boolean.FALSE);
        modelService.save(productType);
    }

    /**
     * Verifies that product type can be found by the code.
     */
    @Test
    public void testGetByCode() {
        assertEquals(productType, productTypeDao.getByCode(PRODUCT_TYPE_CODE));
    }

    /**
     * Verifies that exception is thrown when requested product type does not exist in database.
     */
    @Test
    public void testGetByCodeNoProductType() {
        exception.expect(ModelNotFoundException.class);
        productTypeDao.getByCode("unknown");
    }
}
