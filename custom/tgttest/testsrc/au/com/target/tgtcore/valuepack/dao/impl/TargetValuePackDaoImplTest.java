package au.com.target.tgtcore.valuepack.dao.impl;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import au.com.target.tgtcore.enums.DealTypeEnum;
import au.com.target.tgtcore.model.TargetValuePackModel;
import au.com.target.tgtcore.valuepack.dao.TargetValuePackDao;


/**
 * Test suite for {@link TargetValuePackDaoImpl}.
 */
@IntegrationTest
public class TargetValuePackDaoImplTest extends ServicelayerTransactionalTest {

    private static final String SKU = "12345";

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @Resource
    private TargetValuePackDao valuePackDao;

    @Resource
    private ModelService modelService;

    private TargetValuePackModel valuePack;

    /**
     * Initialises test cases before run.
     */
    @Before
    public void setUp() {
        valuePack = modelService.create(TargetValuePackModel.class);
        valuePack.setChildSKU(SKU);
        valuePack.setChildSKUDescription(SKU);
        valuePack.setPrice(Double.valueOf(BigDecimal.ONE.doubleValue()));
        valuePack.setValuePackDealType(DealTypeEnum.BUYGET);
        modelService.save(valuePack);
    }

    /**
     * Verifies that value pack can be retrieved by child SKU.
     */
    @Test
    public void testGetByChildSku() {
        assertEquals(valuePack, valuePackDao.getByChildSku(SKU));
    }

    /**
     * Verifies that exception will be thrown in case of non-existing child SKU will be provided as an input to
     * {@link TargetValuePackDao#getByChildSku(String)}.
     */
    @Test
    public void testRecordNotFound() {
        expectedException.expect(ModelNotFoundException.class);
        valuePackDao.getByChildSku("foo");
    }
}
