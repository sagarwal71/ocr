/**
 * 
 */
package au.com.target.tgtcore.document.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commons.enums.DocumentTypeEnum;
import de.hybris.platform.commons.model.DocumentModel;
import de.hybris.platform.commons.model.FormatModel;
import de.hybris.platform.commons.model.VelocityFormatterModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Date;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.document.dao.TargetDocumentDao;
import au.com.target.tgtcore.enums.DocumentType;


/**
 * @author asingh78
 * 
 */
@IntegrationTest
public class TargetDocumentDaoImplTest extends ServicelayerTransactionalTest {

    @Resource(name = "userService")
    private UserService userService;
    @Resource(name = "modelService")
    private ModelService modelService;

    @Resource
    private TargetDocumentDao targetDocumentDao;

    private OrderModel orderModel = null;

    private OrderModel order = null;

    private DocumentModel originalDocumentModel = null;

    @Before
    public void setUp() {

        final CurrencyModel currency = modelService.create(CurrencyModel.class);
        currency.setIsocode("TEST");
        currency.setSymbol("TEST");
        modelService.save(currency);
        final VelocityFormatterModel velocityFormatterModel = modelService.create(VelocityFormatterModel.class);
        velocityFormatterModel.setCode("test");
        velocityFormatterModel.setOutputMimeType("test");
        modelService.save(velocityFormatterModel);
        final FormatModel format = modelService.create(FormatModel.class);
        format.setCode("TEST");
        format.setDocumentType(DocumentTypeEnum.PDF);
        format.setInitial(velocityFormatterModel);
        modelService.save(currency);
        // first order
        orderModel = modelService.create(OrderModel.class);
        final UserModel user = userService.getCurrentUser();
        orderModel.setCode("testOrderD");
        orderModel.setUser(user);
        orderModel.setCurrency(currency);
        orderModel.setDate(new Date());
        modelService.save(orderModel);

        //Second order 
        order = modelService.create(OrderModel.class);
        order.setCode("testOrderWD");
        order.setUser(user);
        order.setCurrency(currency);
        order.setDate(new Date());
        modelService.save(order);

        originalDocumentModel = modelService.create(DocumentModel.class);
        originalDocumentModel.setCode("testDocument");
        originalDocumentModel.setFormat(format);
        originalDocumentModel.setSourceItem(orderModel);
        originalDocumentModel.setDocumentType(DocumentType.INVOICE);
        modelService.save(originalDocumentModel);


    }

    @Test
    public void findDocumentModelBySourceItemAndDocumentTypeWithNoDocument() {
        final DocumentModel documentModel = targetDocumentDao.findDocumentModelBySourceItemAndDocumentType(order,
                DocumentType.INVOICE);
        assertThat(documentModel).isNull();
    }

    @Test
    public void findDocumentModelBySourceItemAndDocumentType() {
        final DocumentModel documentModel = targetDocumentDao.findDocumentModelBySourceItemAndDocumentType(orderModel,
                DocumentType.INVOICE);
        assertThat(documentModel).isEqualTo(originalDocumentModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindDocumentModelWithNoOrder() {
        targetDocumentDao.findDocumentModelBySourceItemAndDocumentType(null, DocumentType.INVOICE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindDocumentModelWithNoDocumentType() {
        targetDocumentDao.findDocumentModelBySourceItemAndDocumentType(order, null);
    }

}
