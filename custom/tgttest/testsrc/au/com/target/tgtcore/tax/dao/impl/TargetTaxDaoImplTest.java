/**
 * 
 */
package au.com.target.tgtcore.tax.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.model.TargetTaxModel;
import au.com.target.tgtcore.tax.dao.TargetTaxDao;

import com.google.common.base.Charsets;


@IntegrationTest
public class TargetTaxDaoImplTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetTaxDao targetTaxDao;

    @Before
    public void setUp() throws Exception {
        importCsv("/tgtcore/test/testSystemSetup.impex", Charsets.UTF_8.name());
    }

    @Test
    public void testGetDefaultTax()
    {
        final TargetTaxModel tax = targetTaxDao.getDefaultTax();
        Assert.assertNotNull(tax);
        Assert.assertEquals("aus-gst-1000", tax.getCode());
    }
}
