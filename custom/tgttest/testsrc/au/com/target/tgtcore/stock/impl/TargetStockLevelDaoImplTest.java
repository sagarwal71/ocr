/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtcore.stock.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.VendorModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.util.Utilities;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.stock.TargetStockLevelDao;
import au.com.target.tgtcore.warehouse.dao.TargetWarehouseDao;
import au.com.target.tgtfulfilment.enums.IntegrationMethod;


/**
 *
 */
@IntegrationTest
//TODO need to get these tests working
public class TargetStockLevelDaoImplTest extends ServicelayerTransactionalTest {
    private static final String WAREHOUSE1CODE = "w1";
    private static final String VENDOR1CODE = "v1";
    private static final String PRODUCT1CODE = "P1";
    private static final String PREORDERPRODUCT1CODE = "P0P1";

    @Resource
    private TargetStockLevelDao targetStockLevelDao;

    @Resource
    private ModelService modelService;

    @Resource
    private FlexibleSearchService flexibleSearchService;

    @Resource
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Resource
    private TargetWarehouseDao targetWarehouseDao;

    private WarehouseModel warehouse1;
    private VendorModel vendor1;
    private CatalogModel catalog;
    private CatalogVersionModel catalogVersion;
    private ProductModel product1;
    private ProductModel preOrderproduct1;
    private StockLevelModel stock1;
    private StockLevelModel preOrderStock1;
    private UserModel user;
    private UnitModel unit;

    /**
     *
     */
    @Before
    public void setUp() throws Exception {
        vendor1 = createVendor(VENDOR1CODE);
        warehouse1 = createWarehouse(WAREHOUSE1CODE, vendor1, Boolean.TRUE);

        catalog = modelService.create(CatalogModel.class);
        catalog.setId("id1");
        modelService.save(catalog);
        catalogVersion = modelService.create(CatalogVersionModel.class);
        catalogVersion.setVersion("v1");
        catalogVersion.setCatalog(catalog);
        modelService.save(catalogVersion);

        product1 = createProduct(PRODUCT1CODE, catalogVersion);
        preOrderproduct1 = createProduct(PREORDERPRODUCT1CODE, catalogVersion);
        stock1 = createStockLevel(product1, 10, warehouse1);
        preOrderStock1 = createPreOrderStockLevel(preOrderproduct1, 10, warehouse1);

        user = modelService.create(UserModel.class);
        user.setUid("mr.rondel");
        modelService.save(user);

        unit = modelService.create(UnitModel.class);
        unit.setCode("test");
        unit.setUnitType("pieces");
        modelService.save(unit);

    }

    private StockLevelModel createStockLevel(final ProductModel product, final int quantity,
            final WarehouseModel warehouse) {
        final StockLevelModel res = modelService.create(StockLevelModel.class);
        res.setProductCode(product.getCode());
        res.setAvailable(quantity);
        res.setWarehouse(warehouse);
        modelService.save(res);
        return res;
    }

    private StockLevelModel createPreOrderStockLevel(final ProductModel product, final int quantity,
            final WarehouseModel warehouse) {
        final StockLevelModel res = modelService.create(StockLevelModel.class);
        res.setProductCode(product.getCode());
        res.setMaxPreOrder(quantity);
        res.setWarehouse(warehouse);
        modelService.save(res);
        return res;
    }



    private ProductModel createProduct(final String code, final CatalogVersionModel version) {
        final ProductModel res = modelService.create(ProductModel.class);
        res.setCode(code);
        res.setCatalogVersion(version);
        modelService.save(res);
        return res;
    }

    private WarehouseModel createWarehouse(final String code, final VendorModel vendor, final Boolean def) {
        final WarehouseModel res = modelService.create(WarehouseModel.class);
        res.setCode(code);
        res.setVendor(vendor);
        res.setDefault(def);
        res.setIntegrationMethod(IntegrationMethod.ESB);
        modelService.save(res);
        return res;
    }

    private VendorModel createVendor(final String code) {
        final VendorModel res = modelService.create(VendorModel.class);
        res.setCode(code);
        modelService.save(res);
        return res;
    }

    /**
     * @throws InterruptedException
     */
    @Test
    public void testAdjustActualAmount() throws InterruptedException {
        final Date date = new Date(); // record the time that the test began
        Thread.sleep(50);
        assertThat(stock1.getAvailable()).isEqualTo(10); // confirm initial starting position

        // test positive adjustment and verify timestamp has updated
        targetStockLevelDao.adjustActualAmount(stock1, 5);
        Utilities.invalidateCache(stock1.getPk());
        modelService.refresh(stock1);
        assertThat(stock1.getAvailable()).isEqualTo(15);

        assertThat(date.before(stock1.getModifiedtime())).isTrue();

        // test negative adjustment and verify timestamp has updated
        targetStockLevelDao.adjustActualAmount(stock1, -7);
        Utilities.invalidateCache(stock1.getPk());
        modelService.refresh(stock1);
        assertThat(stock1.getAvailable()).isEqualTo(8);

        assertThat(date.before(stock1.getModifiedtime())).isTrue();

        // test zero adjustment and verify timestamp has updated
        targetStockLevelDao.adjustActualAmount(stock1, 0);
        Utilities.invalidateCache(stock1.getPk());
        modelService.refresh(stock1);
        assertThat(stock1.getAvailable()).isEqualTo(8);

        assertThat(date.before(stock1.getModifiedtime())).isTrue();

        // test adjustment that sends available negative and verify timestamp has updated
        targetStockLevelDao.adjustActualAmount(stock1, -10);
        Utilities.invalidateCache(stock1.getPk());
        modelService.refresh(stock1);
        assertThat(stock1.getAvailable()).isEqualTo(-2);

        assertThat(date.before(stock1.getModifiedtime())).isTrue();
    }

    /**
     * @throws InterruptedException
     */
    @Test
    public void testTransferReserveToActualAmount() throws InterruptedException {
        final Date date = new Date(); // record the time that the test began
        Thread.sleep(50);
        targetStockLevelDao.reserve(stock1, 6);
        Utilities.invalidateCache(stock1.getPk());
        modelService.refresh(stock1);
        // confirm initial starting position 
        assertThat(stock1.getAvailable()).isEqualTo(10);
        assertThat(stock1.getReserved()).isEqualTo(6);

        assertThat(date.before(stock1.getModifiedtime())).isTrue();

        // test positive transfer and verify timestamp has updated
        targetStockLevelDao.transferReserveToActualAmount(stock1, 5);
        Utilities.invalidateCache(stock1.getPk());
        modelService.refresh(stock1);
        assertThat(stock1.getAvailable()).isEqualTo(5);
        assertThat(stock1.getReserved()).isEqualTo(1);

        assertThat(date.before(stock1.getModifiedtime())).isTrue();

        // test negative transfer and verify timestamp has updated
        targetStockLevelDao.transferReserveToActualAmount(stock1, -7);
        Utilities.invalidateCache(stock1.getPk());
        modelService.refresh(stock1);
        assertThat(stock1.getAvailable()).isEqualTo(12);
        assertThat(stock1.getReserved()).isEqualTo(8);

        assertThat(date.before(stock1.getModifiedtime())).isTrue();

        // test zero transfer and verify timestamp has updated
        targetStockLevelDao.transferReserveToActualAmount(stock1, 0);
        Utilities.invalidateCache(stock1.getPk());
        modelService.refresh(stock1);
        assertThat(stock1.getAvailable()).isEqualTo(12);
        assertThat(stock1.getReserved()).isEqualTo(8);

        assertThat(date.before(stock1.getModifiedtime())).isTrue();

        // test transfer that will send values negative and verify timestamp has updated
        targetStockLevelDao.transferReserveToActualAmount(stock1, 50);
        Utilities.invalidateCache(stock1.getPk());
        modelService.refresh(stock1);
        assertThat(stock1.getAvailable()).isEqualTo(-38);
        assertThat(stock1.getReserved()).isEqualTo(0);

        assertThat(date.before(stock1.getModifiedtime())).isTrue();
    }

    /**
     * @throws InterruptedException
     */
    @Test
    public void testUpdateActualAmount() throws InterruptedException {
        final Date date = new Date(); // record the time that the test began
        Thread.sleep(50);
        targetStockLevelDao.reserve(stock1, 6);
        Utilities.invalidateCache(stock1.getPk());
        modelService.refresh(stock1);
        assertThat(stock1.getAvailable()).isEqualTo(10); // confirm initial starting position
        assertThat(stock1.getAvailable()).isEqualTo(10);
        assertThat(stock1.getReserved()).isEqualTo(6);

        assertThat(date.before(stock1.getModifiedtime())).isTrue();

        // test positive value and verify timestamp has updated
        targetStockLevelDao.updateActualAmount(stock1, 5);
        Utilities.invalidateCache(stock1.getPk());
        modelService.refresh(stock1);
        assertThat(stock1.getAvailable()).isEqualTo(5);
        assertThat(stock1.getReserved()).isEqualTo(6);

        assertThat(date.before(stock1.getModifiedtime())).isTrue();

        // test negative value and verify timestamp has updated
        targetStockLevelDao.updateActualAmount(stock1, -7);
        Utilities.invalidateCache(stock1.getPk());
        modelService.refresh(stock1);
        assertThat(stock1.getAvailable()).isEqualTo(-7);
        assertThat(stock1.getReserved()).isEqualTo(6);

        assertThat(date.before(stock1.getModifiedtime())).isTrue();

        // test zero value and verify timestamp has updated
        targetStockLevelDao.updateActualAmount(stock1, 0);
        Utilities.invalidateCache(stock1.getPk());
        modelService.refresh(stock1);
        assertThat(stock1.getAvailable()).isEqualTo(0);
        assertThat(stock1.getReserved()).isEqualTo(6);

        assertThat(date.before(stock1.getModifiedtime())).isTrue();
    }

    @Test
    public void testReserve() throws InterruptedException {
        final Date date = new Date(); // record the time that the test began
        Thread.sleep(50);
        //reserve stock and verify timestamp has updated
        targetStockLevelDao.reserve(stock1, 2);
        Utilities.invalidateCache(stock1.getPk());
        modelService.refresh(stock1);

        assertThat(stock1.getReserved()).isEqualTo(2);

        assertThat(date.before(stock1.getModifiedtime())).isTrue();

        //reserve 0 stock and verify reserved stock has not changed and verify timestamp has updated
        targetStockLevelDao.reserve(stock1, 0);
        Utilities.invalidateCache(stock1.getPk());
        modelService.refresh(stock1);
        assertThat(stock1.getReserved()).isEqualTo(2);

        assertThat(date.before(stock1.getModifiedtime())).isTrue();

    }

    @Test
    public void testPreOrderReserveNoStock() throws InterruptedException {
        final Date date = new Date(); // record the time that the test began
        Thread.sleep(50);
        //reserve stock and verify timestamp has updated
        targetStockLevelDao.reserve(preOrderStock1, 12, true);
        Utilities.invalidateCache(preOrderStock1.getPk());
        modelService.refresh(preOrderStock1);
        assertThat(preOrderStock1.getPreOrder()).isEqualTo(0);
        assertThat(date.after(preOrderStock1.getModifiedtime())).isTrue();
    }

    @Test
    public void testPreOrderReserve() throws InterruptedException {
        final Date date = new Date(); // record the time that the test began
        Thread.sleep(50);
        //reserve stock and verify timestamp has updated
        targetStockLevelDao.reserve(preOrderStock1, 2, true);
        Utilities.invalidateCache(preOrderStock1.getPk());
        modelService.refresh(preOrderStock1);

        assertThat(preOrderStock1.getPreOrder()).isEqualTo(2);
        assertThat(date.before(preOrderStock1.getModifiedtime())).isTrue();
    }

    @Test
    public void testPreOrderReserveWithZero() throws InterruptedException {
        final Date date = new Date(); // record the time that the test began
        Thread.sleep(50);
        //reserve 0 stock and verify reserved stock has not changed and verify timestamp has updated
        targetStockLevelDao.reserve(preOrderStock1, 0, true);
        Utilities.invalidateCache(preOrderStock1.getPk());
        modelService.refresh(preOrderStock1);
        assertThat(preOrderStock1.getPreOrder()).isEqualTo(0);
        assertThat(date.before(preOrderStock1.getModifiedtime())).isTrue();
    }

    @Test
    public void testRelease() throws InterruptedException {
        final Date date = new Date(); // record the time that the test began
        Thread.sleep(50);
        //reserve then release stock and verify timestamp has updated
        targetStockLevelDao.reserve(stock1, 5);
        targetStockLevelDao.release(stock1, 2);
        Utilities.invalidateCache(stock1.getPk());
        modelService.refresh(stock1);
        assertThat(stock1.getReserved()).isEqualTo(3);

        assertThat(date.before(stock1.getModifiedtime())).isTrue();

        //release more stock and verify timestamp has updated         
        targetStockLevelDao.release(stock1, 2);
        Utilities.invalidateCache(stock1.getPk());
        modelService.refresh(stock1);
        assertThat(stock1.getReserved()).isEqualTo(1);

        assertThat(date.before(stock1.getModifiedtime())).isTrue();
    }

    @Test
    public void testReleasePreOrder() throws InterruptedException {
        final Date date = new Date(); // record the time that the test began
        Thread.sleep(50);
        //reserve then release stock and verify timestamp has updated
        targetStockLevelDao.reserve(preOrderStock1, 5, true);
        targetStockLevelDao.release(preOrderStock1, 2, true);
        Utilities.invalidateCache(preOrderStock1.getPk());
        modelService.refresh(preOrderStock1);
        assertThat(preOrderStock1.getPreOrder()).isEqualTo(3);

        assertThat(date.before(preOrderStock1.getModifiedtime())).isTrue();

        //release more stock and verify timestamp has updated         
        targetStockLevelDao.release(preOrderStock1, 2, true);
        Utilities.invalidateCache(preOrderStock1.getPk());
        modelService.refresh(preOrderStock1);
        assertThat(preOrderStock1.getPreOrder()).isEqualTo(1);

        assertThat(date.before(preOrderStock1.getModifiedtime())).isTrue();
    }

    @Test
    public void testReleaseWithNegativeQuantity() throws InterruptedException {
        //reserve then release stock and verify timestamp has updated
        targetStockLevelDao.reserve(stock1, 5);
        targetStockLevelDao.release(stock1, 8);
        Utilities.invalidateCache(stock1.getPk());
        modelService.refresh(stock1);
        assertThat(stock1.getReserved()).isEqualTo(0);
    }

    @Test
    public void testReleasePreOrderWithNegativeQuantity() throws InterruptedException {
        //reserve then release stock and verify timestamp has updated
        targetStockLevelDao.reserve(preOrderStock1, 5);
        targetStockLevelDao.release(preOrderStock1, 8);
        Utilities.invalidateCache(preOrderStock1.getPk());
        modelService.refresh(preOrderStock1);
        assertThat(preOrderStock1.getPreOrder()).isEqualTo(0);
    }


    @Test
    public void testForceReserve() throws InterruptedException {
        final Date date = new Date(); // record the time that the test began
        Thread.sleep(50);
        product1 = createProduct("P5000", catalogVersion);
        final StockLevelModel stock2 = createStockLevel(product1, 5, warehouse1);
        final Integer afterreserve = targetStockLevelDao.forceReserve(stock2, 7);
        Utilities.invalidateCache(stock2.getPk());
        modelService.refresh(stock2);
        assertThat(date.before(stock2.getModifiedtime())).isTrue();
        assertThat(Integer.valueOf(7)).isEqualTo(afterreserve);

    }

    @Test
    public void testGetProductListForWarehouse() throws InterruptedException {
        final ProductModel product11 = createProduct("P11_Black_L", catalogVersion);
        final ProductModel product22 = createProduct("P22_Blue_M", catalogVersion);
        final StockLevelModel stock11 = createStockLevel(product11, 15, warehouse1);
        final StockLevelModel stock22 = createStockLevel(product22, 25, warehouse1);
        modelService.refresh(stock11);
        modelService.refresh(stock22);
        final Map<String, Integer> stockLevelMap = targetStockLevelDao.findStockForProductCodesAndWarehouse(
                Arrays.asList("P11_Black_L", "P22_Blue_M"),
                null);
        assertThat(stockLevelMap).hasSize(2);
        assertThat(stockLevelMap.get(StringUtils.lowerCase("P11_Black_L"))).isEqualTo(15);
        assertThat(stockLevelMap.get(StringUtils.lowerCase("P22_Blue_M"))).isEqualTo(25);

    }

    @Test
    public void testUpdateQuantityFulfilledByFastlineWarehouse() throws InterruptedException {
        Thread.sleep(50);
        targetStockLevelDao.updateQuantityFulfilledByWarehouse(7, warehouse1);
        Utilities.invalidateCache(warehouse1.getPk());
        modelService.refresh(warehouse1);
        assertThat(warehouse1.getTotalQtyFulfilledAtFastlineToday()).isEqualTo(7);
    }

}
