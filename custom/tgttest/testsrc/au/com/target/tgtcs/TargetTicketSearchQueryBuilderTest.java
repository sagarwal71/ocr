/**
 * 
 */
package au.com.target.tgtcs;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.cscockpit.model.data.DataObject;
import de.hybris.platform.cscockpit.services.search.CsSearchResult;
import de.hybris.platform.cscockpit.services.search.SearchException;
import de.hybris.platform.cscockpit.services.search.generic.DefaultCsFlexibleSearchService;
import de.hybris.platform.cscockpit.services.search.generic.query.DefaultTicketSearchQueryBuilder;
import de.hybris.platform.cscockpit.services.search.impl.DefaultCsTextSearchCommand;
import de.hybris.platform.cscockpit.services.search.impl.DefaultPageable;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.ticket.enums.CsTicketCategory;
import de.hybris.platform.ticket.enums.CsTicketPriority;
import de.hybris.platform.ticket.enums.CsTicketState;
import de.hybris.platform.ticket.model.CsAgentGroupModel;
import de.hybris.platform.ticket.model.CsTicketModel;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcs.widgets.controllers.impl.TargetTicketSearchQueryBuilder;


/**
 * Integration Test for {@link TargetTicketSearchQueryBuilder}
 * 
 */
@IntegrationTest
public class TargetTicketSearchQueryBuilderTest extends ServicelayerTransactionalTest {

    private static final String TICKET_ID_FOR_STATE_SEARCH = "00001";
    private static final String TICKET_ID_FOR_PART_STATE_SEARCH = "00002";

    private static final String TICKET_ID_FOR_ASSIGNED_AGENT_UID_SEARCH = "00003";
    private static final String TICKET_ID_FOR_PART_ASSIGNED_AGENT_UID_SEARCH = "00004";

    private static final String TICKET_ID_FOR_ASSIGNED_AGENT_NAME_SEARCH = "00005";
    private static final String TICKET_ID_FOR_PART_ASSIGNED_AGENT_NAME_SEARCH = "00006";

    private static final String TICKET_ID_FOR_ASSIGNED_GROUP_UID_SEARCH = "00007";
    private static final String TICKET_ID_FOR_PART_ASSIGNED_GROUP_UID_SEARCH = "00008";

    private static final String TICKET_ID_FOR_ASSIGNED_GROUP_NAME_SEARCH = "00009";
    private static final String TICKET_ID_FOR_PART_ASSIGNED_GROUP_NAME_SEARCH = "000010";

    private static final String TICKET_ID_FOR_ALL_SEARCH_TEST = "000011";


    private static final String ASSIGNED_AGENT_UID = "csagent@agents.com";
    private static final String ASSIGNED_AGENT_UID_PART_SEARCH = "partUidAgentSearch";

    private static final String ASSIGNED_AGENT_UID_FOR_NAME_SEARCH = "nameSearchUid";
    private static final String ASSIGNED_AGENT_UID_FOR_PART_NAME_SEARCH = "partNameAgentSearch";
    private static final String ASSIGNED_AGENT_NAME = "Michel";
    private static final String ASSIGNED_AGENT_NAME_PART_SEARCH = "Adella";

    private static final String ASSIGNED_GROUP_UID = "csTestAgentGroup";
    private static final String ASSIGNED_GROUP_UID_PART_SEARCH = "exampleGroupUid";

    private static final String ASSIGNED_GROUP_UID_FOR_NAME_SEARCH = "111111";
    private static final String ASSIGNED_GROUP_NAME = "customersGroup";
    private static final String ASSIGNED_GROUP_UID_FOR_PART_NAME_SEARCH = "22222";
    private static final String ASSIGNED_GROUP_NAME_FOR_PART_SEARCH = "directorsGroup";


    private static final String HEADLINE = "headline";

    private static final int NUMBER_CREATED_TEST_TICKETS = 4;

    @Resource
    private FlexibleSearchService flexibleSearchService;

    @Resource
    private SearchRestrictionService searchRestrictionService;

    @Resource
    private ModelService modelService;

    @Resource
    private SessionService sessionService;

    private final DefaultCsTextSearchCommand command = new DefaultCsTextSearchCommand();

    private final DefaultCsFlexibleSearchService<DefaultCsTextSearchCommand, ItemModel> searchService = new DefaultCsFlexibleSearchService<DefaultCsTextSearchCommand, ItemModel>();//NOPMD

    private final DefaultPageable pageable = new DefaultPageable();

    private CsTicketModel searchResultTicket;

    @Before
    public void setUp() throws Exception {

        searchService.setFlexibleSearchService(flexibleSearchService);
        searchService.setSearchRestrictionService(searchRestrictionService);
        searchService.setSessionService(sessionService);
        searchService.setFlexibleSearchQueryBuilder(new TargetTicketSearchQueryBuilder()); // target cscockpit query builder

        pageable.setPageNumber(0); //first page
        pageable.setPageSize(15);
    }

    private void fillCommonTicketFields(final CsTicketModel csTicket) {
        csTicket.setCategory(CsTicketCategory.COMPLAINT);
        csTicket.setPriority(CsTicketPriority.LOW);
        csTicket.setHeadline(HEADLINE);
    }

    private void searchAndCheckResult(final String ticketId) throws SearchException {
        final CsSearchResult<DefaultCsTextSearchCommand, ItemModel> searchResult = searchService.search(command,
                pageable);
        final List<DataObject<ItemModel>> result = searchResult.getResult();

        final ItemModel item = result.get(0).getItem();
        Assert.assertNotNull(item);

        Assert.assertTrue(item instanceof CsTicketModel);

        searchResultTicket = (CsTicketModel)item;
        Assert.assertEquals(ticketId, searchResultTicket.getTicketID());
    }

    @Test
    public void testSearchByTicketStateField() throws SearchException {
        final CsTicketModel csTicket = modelService.create(CsTicketModel.class);
        csTicket.setState(CsTicketState.NEW);
        csTicket.setTicketID(TICKET_ID_FOR_STATE_SEARCH);

        fillCommonTicketFields(csTicket);

        modelService.save(csTicket);

        command.setText(DefaultTicketSearchQueryBuilder.TextField.SearchText, CsTicketState.NEW.getCode());

        searchAndCheckResult(TICKET_ID_FOR_STATE_SEARCH);
    }

    @Test
    public void testSearchByPartStateField() throws SearchException {
        final CsTicketModel csTicket = modelService.create(CsTicketModel.class);
        csTicket.setTicketID(TICKET_ID_FOR_PART_STATE_SEARCH);
        csTicket.setState(CsTicketState.CLOSED);

        fillCommonTicketFields(csTicket);

        modelService.save(csTicket);

        final String partOfState = CsTicketState.CLOSED.getCode().substring(0, 2);
        command.setText(DefaultTicketSearchQueryBuilder.TextField.SearchText, partOfState);

        searchAndCheckResult(TICKET_ID_FOR_PART_STATE_SEARCH);
    }

    @Test
    public void testSearchByAssignedAgentUidField() throws SearchException {
        final CsTicketModel csTicket = modelService.create(CsTicketModel.class);
        csTicket.setTicketID(TICKET_ID_FOR_ASSIGNED_AGENT_UID_SEARCH);

        fillCommonTicketFields(csTicket);

        final EmployeeModel assignedAgent = modelService.create(EmployeeModel.class);
        assignedAgent.setUid(ASSIGNED_AGENT_UID);

        csTicket.setAssignedAgent(assignedAgent);

        modelService.save(csTicket);

        command.setText(DefaultTicketSearchQueryBuilder.TextField.SearchText, ASSIGNED_AGENT_UID);

        searchAndCheckResult(TICKET_ID_FOR_ASSIGNED_AGENT_UID_SEARCH);
    }

    @Test
    public void testSearchByPartAssignedAgentUidField() throws SearchException {
        final CsTicketModel csTicket = modelService.create(CsTicketModel.class);
        csTicket.setTicketID(TICKET_ID_FOR_PART_ASSIGNED_AGENT_UID_SEARCH);

        final EmployeeModel assignedAgent = modelService.create(EmployeeModel.class);
        assignedAgent.setUid(ASSIGNED_AGENT_UID_PART_SEARCH);

        csTicket.setAssignedAgent(assignedAgent);

        fillCommonTicketFields(csTicket);

        modelService.save(csTicket);

        final String partOfAgentUid = ASSIGNED_AGENT_UID_PART_SEARCH.substring(0, 5);
        command.setText(DefaultTicketSearchQueryBuilder.TextField.SearchText, partOfAgentUid);

        searchAndCheckResult(TICKET_ID_FOR_PART_ASSIGNED_AGENT_UID_SEARCH);
    }

    @Test
    public void testSearchByAssignedAgentNameField() throws SearchException {
        final CsTicketModel csTicket = modelService.create(CsTicketModel.class);
        csTicket.setTicketID(TICKET_ID_FOR_ASSIGNED_AGENT_NAME_SEARCH);

        final EmployeeModel assignedAgent = modelService.create(EmployeeModel.class);
        assignedAgent.setUid(ASSIGNED_AGENT_UID_FOR_NAME_SEARCH);
        assignedAgent.setName(ASSIGNED_AGENT_NAME);

        csTicket.setAssignedAgent(assignedAgent);

        fillCommonTicketFields(csTicket);

        modelService.save(csTicket);

        command.setText(DefaultTicketSearchQueryBuilder.TextField.SearchText, ASSIGNED_AGENT_NAME);

        searchAndCheckResult(TICKET_ID_FOR_ASSIGNED_AGENT_NAME_SEARCH);
    }

    @Test
    public void testSearchByPartAssignedAgentNameField() throws SearchException {
        final CsTicketModel csTicket = modelService.create(CsTicketModel.class);
        csTicket.setTicketID(TICKET_ID_FOR_PART_ASSIGNED_AGENT_NAME_SEARCH);

        final EmployeeModel assignedAgent = modelService.create(EmployeeModel.class);
        assignedAgent.setName(ASSIGNED_AGENT_NAME_PART_SEARCH);
        assignedAgent.setUid(ASSIGNED_AGENT_UID_FOR_PART_NAME_SEARCH);

        csTicket.setAssignedAgent(assignedAgent);

        fillCommonTicketFields(csTicket);

        modelService.save(csTicket);

        final String partOfAgentName = ASSIGNED_AGENT_NAME_PART_SEARCH.substring(0, 3);
        command.setText(DefaultTicketSearchQueryBuilder.TextField.SearchText, partOfAgentName);

        searchAndCheckResult(TICKET_ID_FOR_PART_ASSIGNED_AGENT_NAME_SEARCH);
    }

    @Test
    public void testSearchByAssignedGroupUidField() throws SearchException {
        final CsTicketModel csTicket = modelService.create(CsTicketModel.class);
        csTicket.setTicketID(TICKET_ID_FOR_ASSIGNED_GROUP_UID_SEARCH);

        fillCommonTicketFields(csTicket);

        final CsAgentGroupModel csAgentGroup = modelService.create(CsAgentGroupModel.class);
        csAgentGroup.setUid(ASSIGNED_GROUP_UID);

        csTicket.setAssignedGroup(csAgentGroup);

        modelService.save(csTicket);

        command.setText(DefaultTicketSearchQueryBuilder.TextField.SearchText, ASSIGNED_GROUP_UID);

        searchAndCheckResult(TICKET_ID_FOR_ASSIGNED_GROUP_UID_SEARCH);
    }

    @Test
    public void testSearchByPartAssignedGroupUidField() throws SearchException {
        final CsTicketModel csTicket = modelService.create(CsTicketModel.class);
        csTicket.setTicketID(TICKET_ID_FOR_PART_ASSIGNED_GROUP_UID_SEARCH);

        fillCommonTicketFields(csTicket);

        final CsAgentGroupModel csAgentGroup = modelService.create(CsAgentGroupModel.class);
        csAgentGroup.setUid(ASSIGNED_GROUP_UID_PART_SEARCH);

        csTicket.setAssignedGroup(csAgentGroup);

        modelService.save(csTicket);

        final String partOfGroupUid = ASSIGNED_GROUP_UID_PART_SEARCH.substring(0, 5);
        command.setText(DefaultTicketSearchQueryBuilder.TextField.SearchText, partOfGroupUid);

        searchAndCheckResult(TICKET_ID_FOR_PART_ASSIGNED_GROUP_UID_SEARCH);
    }

    @Test
    public void testSearchByAssignedGroupNameField() throws SearchException {
        final CsTicketModel csTicket = modelService.create(CsTicketModel.class);
        csTicket.setTicketID(TICKET_ID_FOR_ASSIGNED_GROUP_NAME_SEARCH);

        fillCommonTicketFields(csTicket);

        final CsAgentGroupModel csAgentGroup = modelService.create(CsAgentGroupModel.class);
        csAgentGroup.setUid(ASSIGNED_GROUP_UID_FOR_NAME_SEARCH);
        csAgentGroup.setLocName(ASSIGNED_GROUP_NAME);

        csTicket.setAssignedGroup(csAgentGroup);

        modelService.save(csTicket);

        command.setText(DefaultTicketSearchQueryBuilder.TextField.SearchText, ASSIGNED_GROUP_NAME);

        searchAndCheckResult(TICKET_ID_FOR_ASSIGNED_GROUP_NAME_SEARCH);
    }

    @Test
    public void testSearchByPartAssignedGroupNameField() throws SearchException {
        final CsTicketModel csTicket = modelService.create(CsTicketModel.class);
        csTicket.setTicketID(TICKET_ID_FOR_PART_ASSIGNED_GROUP_NAME_SEARCH);

        fillCommonTicketFields(csTicket);

        final CsAgentGroupModel csAgentGroup = modelService.create(CsAgentGroupModel.class);
        csAgentGroup.setUid(ASSIGNED_GROUP_UID_FOR_PART_NAME_SEARCH);
        csAgentGroup.setLocName(ASSIGNED_GROUP_NAME_FOR_PART_SEARCH);

        csTicket.setAssignedGroup(csAgentGroup);

        modelService.save(csTicket);

        final String partOfGroupName = ASSIGNED_GROUP_NAME_FOR_PART_SEARCH.substring(0, 5);
        command.setText(DefaultTicketSearchQueryBuilder.TextField.SearchText, partOfGroupName);

        searchAndCheckResult(TICKET_ID_FOR_PART_ASSIGNED_GROUP_NAME_SEARCH);
    }

    @Test
    public void testSearchAll() throws SearchException {
        CsTicketModel csTicket;
        for (int i = 0; i < NUMBER_CREATED_TEST_TICKETS; i++) {
            csTicket = modelService.create(CsTicketModel.class);
            csTicket.setTicketID(TICKET_ID_FOR_ALL_SEARCH_TEST + i);

            fillCommonTicketFields(csTicket);

            modelService.save(csTicket);
        }

        command.setText(DefaultTicketSearchQueryBuilder.TextField.SearchText, StringUtils.EMPTY);
        CsSearchResult<DefaultCsTextSearchCommand, ItemModel> searchResult = searchService.search(command,
                pageable);
        List<DataObject<ItemModel>> result = searchResult.getResult();
        Assert.assertTrue("Search all fail", result.size() >= NUMBER_CREATED_TEST_TICKETS);

        command.setText(DefaultTicketSearchQueryBuilder.TextField.SearchText, "  ");
        searchResult = searchService.search(command,
                pageable);
        result = searchResult.getResult();
        Assert.assertTrue("Search all fail", result.size() >= NUMBER_CREATED_TEST_TICKETS);

        command.setText(DefaultTicketSearchQueryBuilder.TextField.SearchText, null);
        searchResult = searchService.search(command,
                pageable);
        result = searchResult.getResult();
        Assert.assertTrue("Search all fail", result.size() >= NUMBER_CREATED_TEST_TICKETS);


    }

}
