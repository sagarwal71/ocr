package au.com.target.tgtupdate.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


@IntegrationTest
public class TargetUpdateProductDaoImplTest extends ServicelayerTransactionalTest {

    private static final int INITIALIZE_BATCH_SIZE = 5;

    @Resource
    private TargetUpdateProductDaoImpl targetUpdateProductDao;

    @Resource
    private CatalogVersionService catalogVersionService;

    @Resource
    private ModelService modelService;

    @Resource
    private TargetDeliveryService targetDeliveryService;

    @Before
    public void setUp() throws ImpExException {
        importCsv("/tgtcore/test/testSystemSetup.impex", Charsets.UTF_8.name());
        importCsv("/tgtcore/test/testProductsCountInMerchDepartment.impex", Charsets.UTF_8.name());
    }

    @Test
    public void testGetColourVariantsWithNullOnlineFromDate() throws ImpExException {
        targetUpdateProductDao.setInitializeOnlineFromDateBatchSize(INITIALIZE_BATCH_SIZE);

        final List<TargetColourVariantProductModel> result = targetUpdateProductDao
                .getColourVariantsWithNullOnlineFromDate();

        assertThat(result.size()).isEqualTo(INITIALIZE_BATCH_SIZE);
        assertThat(result).onProperty("onlineDate").containsExactly(null, null, null, null, null);
        assertThat(result).onProperty("approvalStatus").containsExactly(ArticleApprovalStatus.APPROVED,
                ArticleApprovalStatus.APPROVED, ArticleApprovalStatus.APPROVED, ArticleApprovalStatus.APPROVED,
                ArticleApprovalStatus.APPROVED);
    }

    @Test
    public void testGetCncAndEbayAvailableProducts() throws ImpExException {
        importCsv("/test/impex/testEbayProductsOne.impex", Charsets.UTF_8.name());
        final List<TargetProductModel> result = targetUpdateProductDao
                .getCncAndEbayAvailableProducts("click-and-collect", "ebay-click-and-collect", 100,
                        catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                                TgtCoreConstants.Catalog.OFFLINE_VERSION));

        assertThat(result.size()).isEqualTo(2);
    }

    @Test
    public void testGetCncAndEbayAvailableProductsAfterUpdate() throws ImpExException {
        importCsv("/test/impex/testEbayProductsOne.impex", Charsets.UTF_8.name());
        final List<TargetProductModel> result = targetUpdateProductDao
                .getCncAndEbayAvailableProducts("click-and-collect", "ebay-click-and-collect", 100,
                        catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                                TgtCoreConstants.Catalog.OFFLINE_VERSION));
        updateDeliveryMode(result);
        final List<TargetProductModel> result2 = targetUpdateProductDao
                .getCncAndEbayAvailableProducts("click-and-collect", "ebay-click-and-collect", 100,
                        catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                                TgtCoreConstants.Catalog.OFFLINE_VERSION));
        assertThat(result2.size()).isEqualTo(1);
    }

    @Test
    public void testFindSizeVariantsWithoutTargetProductSize() throws ImpExException {

        final HashSet<String> codes = new HashSet<>();
        //sizevariantcodes
        codes.add("P5001_blue_L");
        codes.add("P5000_black_L");
        codes.add("P5004_blue_L");
        //colour variant code
        codes.add("P5003_black");

        final List<TargetSizeVariantProductModel> result = targetUpdateProductDao
                .findSizeVariantsWhichDoesNotHaveProductSizeAssociation(codes,
                        catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                                TgtCoreConstants.Catalog.OFFLINE_VERSION));
        assertThat(result.size()).isEqualTo(3);
    }


    /**
     * @param result
     */
    private void updateDeliveryMode(final List<TargetProductModel> result) {
        final Set<DeliveryModeModel> deliverModes = new HashSet<>();
        deliverModes.add(targetDeliveryService.getDeliveryModeForCode("ebay-click-and-collect"));
        for (final DeliveryModeModel delivery : deliverModes) {
            deliverModes.add(delivery);
        }
        result.get(0).setDeliveryModes(null);
        result.get(0).setDeliveryModes(deliverModes);
        modelService.saveAll(result);
    }
}
