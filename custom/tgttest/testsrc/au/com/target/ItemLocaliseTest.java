/**
 * 
 */
package au.com.target;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;

import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;


/**
 * @author dcwillia
 *
 */
@IntegrationTest
public class ItemLocaliseTest extends ServicelayerTransactionalTest {

    // We exclude relationships with enum, hence the check for EnumerationMetaType.  If you try to localise, it is not
    // persisted in the DB
    // We are excluding attributes that in POS because there are used by hybris for their internal purposes
    private static final String ATTRIBUTES_QUERY =
            "select atr.p_extensionname + ' - ' + typ.InternalCode + '.' + atr.QualifierInternal "
                    + "from (junit_composedtypes typ join junit_attributedescriptors atr on typ.PK = atr.OwnerPkString) "
                    + "full outer join junit_attributedescriptorslp lng on atr.PK = lng.ITEMPK "
                    + "where lng.p_name is null "
                    + "and atr.p_extensionname like 'tgt%' "
                    + "and typ.TypePkString not in (select PK from junit_composedtypes where InternalCode = 'EnumerationMetaType') "
                    + "and atr.QualifierInternal not like '%POS' "
                    + "order by atr.p_extensionname, typ.InternalCode, atr.QualifierInternal";

    private static final String TYPES_QUERY =
            "select typ.p_extensionname + ' - ' + typ.InternalCode "
                    + "from junit_composedtypes typ full outer join junit_composedtypeslp lng on typ.PK = lng.ITEMPK "
                    + "where lng.p_name is null "
                    + "and typ.p_sourcetype is null "
                    + "and typ.p_extensionname like 'tgt%' "
                    + "order by typ.p_extensionname, typ.p_sourcetype";

    @Resource
    private JdbcTemplate jdbcTemplate;

    @Test
    public void testTypesLocalised() {
        final List<String> results = jdbcTemplate.queryForList(TYPES_QUERY, String.class);
        if (!results.isEmpty()) {
            Assert.fail("Found attributes that are not localised: " + results);
        }
    }

    @Test
    public void testAttributesLocalised() {
        final List<String> results = jdbcTemplate.queryForList(ATTRIBUTES_QUERY, String.class);
        if (!results.isEmpty()) {
            Assert.fail("Found attributes that are not localised: " + results);
        }
    }
}
