package au.com.target.tgtfulfilment.dao.impl;


import static de.hybris.platform.basecommerce.enums.ConsignmentStatus.CONFIRMED_BY_WAREHOUSE;
import static de.hybris.platform.basecommerce.enums.ConsignmentStatus.CREATED;
import static de.hybris.platform.basecommerce.enums.ConsignmentStatus.SENT_TO_WAREHOUSE;
import static de.hybris.platform.basecommerce.enums.ConsignmentStatus.SHIPPED;
import static java.lang.Double.valueOf;
import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.VendorModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.dao.TargetCarrierDao;
import au.com.target.tgtfulfilment.dao.TargetConsignmentDao;
import au.com.target.tgtfulfilment.dto.TargetConsignmentPageResult;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.enums.IntegrationMethod;
import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;
import au.com.target.tgtfulfilment.model.TargetManifestModel;
import au.com.target.tgtfulfilment.warehouseservices.TargetFulfilmentWarehouseService;


/**
 * IntegrationTest test for (@link {@link TargetConsignmentDaoImpl}
 */
@IntegrationTest
public class TargetConsignmentDaoImplIntegrationTest extends ServicelayerTransactionalTest {

    private static final int RECS_PER_PAGE = 3;

    private static final int OFFSET = 0;

    private static final int LAST_X_CONSIGNMENT_FETCH_DAYS = 60;

    private static final String UID = "1234";

    private static final String UID2 = "1235";

    @Resource
    private TargetConsignmentDao targetConsignmentDao;

    @Resource
    private TargetDeliveryService targetDeliveryService;

    @Resource
    private ModelService modelService;

    @Resource
    private TargetCarrierDao targetCarrierDao;

    @Resource
    private CommonI18NService commonI18NService;

    @Resource
    private TargetFulfilmentWarehouseService targetFulfilmentWarehouseService;

    @Resource
    private TargetFeatureSwitchService targetFeatureSwitchService;

    private WarehouseModel warehouse;

    private WarehouseModel incommWarehouse;

    private WarehouseModel defaultWarehouse;

    private WarehouseModel cnpWarehouse;

    private AddressModel shippingAddress;

    private OrderModel orderModel;

    private UserModel userModel;

    private TargetPointOfServiceModel pointOfServiceModel;

    private BaseStoreModel baseStoreModel;

    private final Map<String, TargetZoneDeliveryModeModel> deliveryModeModelMap = new HashMap<String, TargetZoneDeliveryModeModel>();

    private List<String> consignmentCodes;
    private WarehouseModel manjimupwarehouse;

    @Before
    public void setUp() throws ImpExException {
        final VendorModel vendor = modelService.create(VendorModel.class);
        vendor.setCode("fastline2");
        modelService.save(vendor);
        warehouse = modelService.create(WarehouseModel.class);
        warehouse.setCode("fastline-melbourne");
        warehouse.setVendor(vendor);
        warehouse.setIntegrationMethod(IntegrationMethod.ESB);
        warehouse.setDefault(Boolean.TRUE);
        modelService.save(warehouse);

        final VendorModel incommVendor = modelService.create(VendorModel.class);
        incommVendor.setCode("incommVendor");
        incommWarehouse = modelService.create(WarehouseModel.class);
        incommWarehouse.setCode("IncommWarehouse");
        incommWarehouse.setVendor(incommVendor);
        incommWarehouse.setIntegrationMethod(IntegrationMethod.WEBMETHODS);
        incommWarehouse.setDefault(Boolean.FALSE);
        modelService.save(incommWarehouse);
        shippingAddress = modelService.create(AddressModel.class);
        shippingAddress.setOwner(warehouse);
        modelService.save(shippingAddress);

        final VendorModel manjimupVendor = modelService.create(VendorModel.class);
        manjimupwarehouse = modelService.create(WarehouseModel.class);
        manjimupVendor.setCode("manjimupVendor");
        manjimupwarehouse.setCode("manjimup");
        manjimupwarehouse.setVendor(incommVendor);
        manjimupwarehouse.setIntegrationMethod(IntegrationMethod.WEBMETHODS);
        manjimupwarehouse.setDefault(Boolean.FALSE);
        modelService.save(manjimupwarehouse);
        shippingAddress = modelService.create(AddressModel.class);
        shippingAddress.setOwner(warehouse);
        modelService.save(shippingAddress);

        userModel = modelService.create(UserModel.class);
        userModel.setUid("demouid");
        modelService.save(userModel);

        orderModel = modelService.create(OrderModel.class);
        orderModel.setSubtotal(Double.valueOf(30.0d));
        orderModel.setCurrency(commonI18NService.getCurrentCurrency());
        orderModel.setUser(userModel);
        orderModel.setDate(new Date());
        modelService.save(orderModel);

        importCsv("/tgtcore/test/testSystemSetup.impex", Charsets.UTF_8.name());
        importCsv("/test/test-delivery-modes.impex", Charsets.UTF_8.name());
        importCsv("/test/test-targetCarriers.impex", Charsets.UTF_8.name());

        for (final TargetZoneDeliveryModeModel targetZoneDeliveryModeModel : targetDeliveryService
                .getAllApplicableDeliveryModesForOrder(orderModel, SalesApplication.WEB)) {
            deliveryModeModelMap.put(targetZoneDeliveryModeModel.getCode(), targetZoneDeliveryModeModel);
        }
        defaultWarehouse = targetFulfilmentWarehouseService.getDefaultOnlineWarehouse();
        cnpWarehouse = targetFulfilmentWarehouseService.getWarehouseForCode("CnpWarehouse");
    }

    @Test
    public void testGetConsignmentsWaitingForShipConfirm() {

        final Integer waitLimit = Integer.valueOf(6);

        createConsignment("a1001", ConsignmentStatus.CREATED, null);
        createConsignment("a1002", ConsignmentStatus.SENT_TO_WAREHOUSE, null);
        createConsignment("a1003", ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, null);
        createConsignment("a1004", ConsignmentStatus.PICKED, null); // buy-now or layby-incomplete
        createConsignment("a1005", ConsignmentStatus.PICKED, DateTime.now().minusHours(waitLimit.intValue() + 1)
                .toDate()); // layby fully paid beyond wait threshold
        createConsignment("a1006", ConsignmentStatus.PICKED, DateTime.now().minusHours(waitLimit.intValue() - 1)
                .toDate()); // layby fully paid within wait threshold
        createConsignment("a1007", ConsignmentStatus.SHIPPED, null); // buy-now shipped
        createConsignment("a1008", ConsignmentStatus.SHIPPED, DateTime.now().minusHours(waitLimit.intValue())
                .toDate()); // lay-by shipped
        createConsignment("a1009", ConsignmentStatus.CANCELLED, null);

        final List<TargetConsignmentModel> consignments = targetConsignmentDao
                .getConsignmentsWaitingForShipConfirm(waitLimit);

        assertThat(consignments).hasSize(1);
        assertThat(consignments.get(0).getCode()).isEqualTo("a1005");
    }

    @Test
    public void testGetAllConsignmentsNotAcknowlegedWithinTimeLimitPositive() {

        //Time sent to warehouse is outsideTimeout configured
        final List<String> targetConsignmentModelListExpected = new ArrayList<>();
        TargetCarrierModel targetCarrierModel = targetCarrierDao.getApplicableCarriers(
                deliveryModeModelMap.get("express-delivery"), false, false).get(0);
        Date sentToWarehouse = getAdjustedDate(new Date(), -(targetCarrierModel.getResendOrdExtractTimeOut()
                .intValue() + 1));
        createConsignment("b1002", ConsignmentStatus.SENT_TO_WAREHOUSE, null, targetCarrierModel, sentToWarehouse,
                defaultWarehouse);
        targetConsignmentModelListExpected.add("b1002");

        targetCarrierModel = targetCarrierDao.getApplicableCarriers(
                deliveryModeModelMap.get("home-delivery"), true, false).get(0);
        sentToWarehouse = getAdjustedDate(new Date(), -(targetCarrierModel.getResendOrdExtractTimeOut()
                .intValue() + 1));
        createConsignment("b1003", ConsignmentStatus.SENT_TO_WAREHOUSE, null, targetCarrierModel, sentToWarehouse,
                defaultWarehouse);
        targetConsignmentModelListExpected.add("b1003");

        targetCarrierModel = targetCarrierDao.getApplicableCarriers(
                deliveryModeModelMap.get("click-and-collect"), true, false).get(0);
        sentToWarehouse = getAdjustedDate(new Date(), -(targetCarrierModel.getResendOrdExtractTimeOut()
                .intValue() + 1));
        createConsignment("b1004", ConsignmentStatus.SENT_TO_WAREHOUSE, null, targetCarrierModel, sentToWarehouse,
                defaultWarehouse);
        targetConsignmentModelListExpected.add("b1004");

        final List<TargetConsignmentModel> consignments = targetConsignmentDao
                .getAllConsignmentsNotAcknowlegedWithinTimeLimit();
        assertThat(consignments).hasSize(3);
        verifyConsigments(targetConsignmentModelListExpected, consignments);
    }

    @Test
    public void testGetAllConsignmentsNotAcknowlegedWithinTimeLimitNegative() {

        //Time sent to warehouse is outsideTimeout configured
        TargetCarrierModel targetCarrierModel = targetCarrierDao.getApplicableCarriers(
                deliveryModeModelMap.get("express-delivery"), false, false).get(0);
        Date sentToWarehouse = getAdjustedDate(new Date(), -(targetCarrierModel.getResendOrdExtractTimeOut()
                .intValue() - 2));
        createConsignment("c1002", ConsignmentStatus.SENT_TO_WAREHOUSE, null, targetCarrierModel, sentToWarehouse,
                defaultWarehouse);

        targetCarrierModel = targetCarrierDao.getApplicableCarriers(
                deliveryModeModelMap.get("home-delivery"), true, false).get(0);
        sentToWarehouse = getAdjustedDate(new Date(), -(targetCarrierModel.getResendOrdExtractTimeOut()
                .intValue() - 2));
        createConsignment("c1003", ConsignmentStatus.SENT_TO_WAREHOUSE, null, targetCarrierModel, sentToWarehouse,
                defaultWarehouse);

        targetCarrierModel = targetCarrierDao.getApplicableCarriers(
                deliveryModeModelMap.get("click-and-collect"), true, false).get(0);
        sentToWarehouse = getAdjustedDate(new Date(), -(targetCarrierModel.getResendOrdExtractTimeOut()
                .intValue() - 2));
        createConsignment("c1004", ConsignmentStatus.SENT_TO_WAREHOUSE, null, targetCarrierModel, sentToWarehouse,
                defaultWarehouse);

        final List<TargetConsignmentModel> consignments = targetConsignmentDao
                .getAllConsignmentsNotAcknowlegedWithinTimeLimit();

        assertThat(CollectionUtils.isEmpty(consignments)).isTrue();
    }

    @Test
    public void testGetExternalConsignmentsNotAcknowlegedWithinTimeLimitPositive() {

        final WarehouseModel tempWarehouse = this.warehouse;
        this.warehouse = this.cnpWarehouse;

        //Time sent to warehouse is outsideTimeout configured
        final List<String> targetConsignmentModelListExpected = new ArrayList<>();
        TargetCarrierModel targetCarrierModel = targetCarrierDao.getApplicableCarriers(
                deliveryModeModelMap.get("express-delivery"), false, false).get(0);
        Date sentToWarehouse = getAdjustedDate(new Date(), -(targetCarrierModel.getResendOrdExtractTimeOut()
                .intValue() + 1));
        createConsignment("b1002", sentToWarehouse, ConsignmentStatus.CREATED, targetCarrierModel);

        targetConsignmentModelListExpected.add("b1002");

        targetCarrierModel = targetCarrierDao.getApplicableCarriers(
                deliveryModeModelMap.get("home-delivery"), true, false).get(0);
        sentToWarehouse = getAdjustedDate(new Date(), -(targetCarrierModel.getResendOrdExtractTimeOut()
                .intValue() + 1));
        createConsignment("b1003", sentToWarehouse, ConsignmentStatus.CREATED, targetCarrierModel);
        targetConsignmentModelListExpected.add("b1003");

        targetCarrierModel = targetCarrierDao.getApplicableCarriers(
                deliveryModeModelMap.get("click-and-collect"), true, false).get(0);
        sentToWarehouse = getAdjustedDate(new Date(), -(targetCarrierModel.getResendOrdExtractTimeOut()
                .intValue() + 1));
        createConsignment("b1004", sentToWarehouse, ConsignmentStatus.CREATED, targetCarrierModel);
        targetConsignmentModelListExpected.add("b1004");

        final List<TargetConsignmentModel> consignments = targetConsignmentDao
                .getAllExternalConsignmentsInCreatedStateWithinTimeLimit();
        assertThat(consignments).hasSize(3);
        verifyConsigments(targetConsignmentModelListExpected, consignments);
        this.warehouse = tempWarehouse;
    }

    @Test
    public void testGetExternalConsignmentsNotAcknowlegedWithinTimeLimitNegative() {

        final WarehouseModel tempWarehouse = this.warehouse;
        this.warehouse = this.cnpWarehouse;

        //Time sent to warehouse is outsideTimeout configured
        final List<String> targetConsignmentModelListExpected = new ArrayList<>();
        TargetCarrierModel targetCarrierModel = targetCarrierDao.getApplicableCarriers(
                deliveryModeModelMap.get("express-delivery"), false, false).get(0);
        Date sentToWarehouse = getAdjustedDate(new Date(), -(targetCarrierModel.getResendOrdExtractTimeOut()
                .intValue() - 2));
        createConsignment("b1002", sentToWarehouse, ConsignmentStatus.CREATED, targetCarrierModel);

        targetConsignmentModelListExpected.add("b1002");

        targetCarrierModel = targetCarrierDao.getApplicableCarriers(
                deliveryModeModelMap.get("home-delivery"), true, false).get(0);
        sentToWarehouse = getAdjustedDate(new Date(), -(targetCarrierModel.getResendOrdExtractTimeOut()
                .intValue() - 2));
        createConsignment("b1003", sentToWarehouse, ConsignmentStatus.CREATED, targetCarrierModel);
        targetConsignmentModelListExpected.add("b1003");

        targetCarrierModel = targetCarrierDao.getApplicableCarriers(
                deliveryModeModelMap.get("click-and-collect"), true, false).get(0);
        sentToWarehouse = getAdjustedDate(new Date(), -(targetCarrierModel.getResendOrdExtractTimeOut()
                .intValue() - 2));
        createConsignment("b1004", sentToWarehouse, ConsignmentStatus.CREATED, targetCarrierModel);
        targetConsignmentModelListExpected.add("b1004");

        final List<TargetConsignmentModel> consignments = targetConsignmentDao
                .getAllExternalConsignmentsInCreatedStateWithinTimeLimit();
        assertThat(consignments).isEmpty();
        this.warehouse = tempWarehouse;
    }

    @Test
    public void testGetConsignmentBycode() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {


        createConsignment("a1002", ConsignmentStatus.SENT_TO_WAREHOUSE, null);
        final TargetConsignmentModel targetConsignmentModel = targetConsignmentDao.getConsignmentBycode("a1002");
        assertThat(targetConsignmentModel.getCode()).isEqualTo("a1002");
    }

    @Test
    public void testGetAllExternalConsignmentsInCreatedStateEmptyForDitialGiftCard()
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final List<TargetConsignmentModel> consignments = targetConsignmentDao
                .getAllExternalConsignmentsInCreatedStateWithinTimeLimit();

        assertThat(consignments).isEmpty();
    }

    @Test
    public void testGetAllExternalConsignmentsInCreatedStateForDitialGiftCard()
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        // fastline consignments

        createConsignment("a1001", ConsignmentStatus.CREATED, null);
        createConsignment("a1002", ConsignmentStatus.SENT_TO_WAREHOUSE, null);
        createConsignment("a1003", ConsignmentStatus.CREATED, null);
        createConsignment("a1004", ConsignmentStatus.PICKED, null);

        final WarehouseModel tempModel = warehouse;
        warehouse = targetFulfilmentWarehouseService.getGiftcardWarehouse();
        //Time sent to warehouse is outsideTimeout configured
        final TargetCarrierModel targetCarrierModel = targetCarrierDao
                .getNullCarrier((TargetZoneDeliveryModeModel)targetDeliveryService
                        .getDeliveryModeForCode("digital-gift-card"));
        final Calendar cal = Calendar.getInstance();

        // digital consignments
        createConsignment("b1001", cal.getTime(), ConsignmentStatus.CREATED, targetCarrierModel);
        cal.add(Calendar.MINUTE, -5);
        createConsignment("b1002", cal.getTime(), ConsignmentStatus.CREATED, targetCarrierModel);
        createConsignment("b1003", cal.getTime(), ConsignmentStatus.CREATED, targetCarrierModel);
        createConsignment("b1004", ConsignmentStatus.SENT_TO_WAREHOUSE, null);
        createConsignment("b1005", ConsignmentStatus.CREATED, null);
        createConsignment("b1005", ConsignmentStatus.PICKED, null);


        final List<TargetConsignmentModel> consignments = targetConsignmentDao
                .getAllExternalConsignmentsInCreatedStateWithinTimeLimit();
        warehouse = tempModel;

        assertThat(consignments).hasSize(2);

        final List<String> resultCodes = new ArrayList<>();
        resultCodes.add(consignments.get(0).getCode());
        resultCodes.add(consignments.get(1).getCode());
        assertThat(resultCodes).contains("b1002");
        assertThat(resultCodes).contains("b1003");
    }

    @Test
    public void testGetConsignmentBycodes()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        createConsignment("a1002", ConsignmentStatus.SENT_TO_WAREHOUSE, null);
        createConsignment("a1003", ConsignmentStatus.SENT_TO_WAREHOUSE, null);

        setupConsignmentCodes("a1002", "a1003");
        final List<TargetConsignmentModel> targetConsignmentModel = targetConsignmentDao
                .getConsignmentsByCodes(consignmentCodes);
        assertThat(targetConsignmentModel).hasSize(2);
        final List<String> resultCodes = new ArrayList<>();
        resultCodes.add(targetConsignmentModel.get(0).getCode());
        resultCodes.add(targetConsignmentModel.get(1).getCode());
        assertThat(resultCodes).contains("a1002");
        assertThat(resultCodes).contains("a1003");
    }

    @Test
    public void testGetConsignmentBycodesNoCodes() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        setupConsignmentCodes("a1002", "a1003");
        final List<TargetConsignmentModel> targetConsignmentModel = targetConsignmentDao
                .getConsignmentsByCodes(consignmentCodes);

        assertThat(targetConsignmentModel).isEmpty();
    }

    @Test
    public void testGetConsignmentByStoreNumber() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        createConsignment("a1002", warehouse, ConsignmentStatus.SENT_TO_WAREHOUSE, null);
        final List<TargetConsignmentModel> targetConsignmentModel = targetConsignmentDao
                .getConsignmentsForStore(Integer.valueOf(2126));

        assertThat(targetConsignmentModel).isNotNull();
        assertThat(targetConsignmentModel.get(0).getCode()).isEqualTo("a1002");

    }

    @Test
    public void testGetConsignmentByStoreNumberWithPagination() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        createConsignment("a1002", warehouse, ConsignmentStatus.SENT_TO_WAREHOUSE, null);
        final TargetConsignmentPageResult targetConsignmentPageResult = targetConsignmentDao
                .getConsignmentsForStore(Integer.valueOf(2126), OFFSET, RECS_PER_PAGE, LAST_X_CONSIGNMENT_FETCH_DAYS);
        final List<TargetConsignmentModel> targetConsignmentModel = targetConsignmentPageResult.getConsignments();
        assertThat(targetConsignmentModel).isNotNull();
        assertThat(targetConsignmentModel.get(0).getCode()).isEqualTo("a1002");
    }

    @Test
    public void testGetConsignmentByStoreNumberWhichDoesNotExist() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        createConsignment("a1002", warehouse, ConsignmentStatus.SENT_TO_WAREHOUSE, null);
        final List<TargetConsignmentModel> targetConsignmentModel = targetConsignmentDao
                .getConsignmentsForStore(Integer.valueOf(499999));
        assertThat(targetConsignmentModel).isNotNull();
        assertThat(targetConsignmentModel).isEmpty();
    }

    @Test
    public void testGetConsignmentByStoreNumberWhichDoesNotExistWithPagination()
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        createConsignment("a1002", warehouse, ConsignmentStatus.SENT_TO_WAREHOUSE, null);
        final TargetConsignmentPageResult targetConsignmentPageResult = targetConsignmentDao
                .getConsignmentsForStore(Integer.valueOf(499999), OFFSET, RECS_PER_PAGE, LAST_X_CONSIGNMENT_FETCH_DAYS);
        final List<TargetConsignmentModel> targetConsignmentModel = targetConsignmentPageResult.getConsignments();
        assertThat(targetConsignmentModel).isNotNull();
        assertThat(targetConsignmentModel).isEmpty();
    }

    @Test
    public void testGetConsignmentByStoreNumberWithCancelledByCustomer() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        createConsignment("a1002", warehouse, ConsignmentStatus.SENT_TO_WAREHOUSE,
                ConsignmentRejectReason.CANCELLED_BY_CUSTOMER);
        final List<TargetConsignmentModel> targetConsignmentModel = targetConsignmentDao
                .getConsignmentsForStore(Integer.valueOf(2126));
        assertThat(targetConsignmentModel).isNotNull();
        assertThat(targetConsignmentModel).isEmpty();
    }

    @Test
    public void testGetConsignmentByStoreNumberWithCancelledByCustomerWithPagination()
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        createConsignment("a1002", warehouse, ConsignmentStatus.SENT_TO_WAREHOUSE,
                ConsignmentRejectReason.CANCELLED_BY_CUSTOMER);
        final TargetConsignmentPageResult targetConsignmentPageResult = targetConsignmentDao
                .getConsignmentsForStore(Integer.valueOf(2126), OFFSET, RECS_PER_PAGE, LAST_X_CONSIGNMENT_FETCH_DAYS);
        final List<TargetConsignmentModel> targetConsignmentModel = targetConsignmentPageResult.getConsignments();
        assertThat(targetConsignmentModel).isNotNull();
        assertThat(targetConsignmentModel).isEmpty();
    }

    @Test
    public void testGetConsignmentByStoreNumberWithOtherReason() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        createConsignment("a1002", warehouse, ConsignmentStatus.SENT_TO_WAREHOUSE,
                ConsignmentRejectReason.ACCEPT_TIMEOUT);
        final List<TargetConsignmentModel> targetConsignmentModel = targetConsignmentDao
                .getConsignmentsForStore(Integer.valueOf(2126));
        assertThat(targetConsignmentModel).isNotNull();
        assertThat(targetConsignmentModel).hasSize(1);
    }

    @Test
    public void testGetConsignmentByStoreNumberWithOtherReasonWithPagination() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        createConsignment("a1002", warehouse, ConsignmentStatus.SENT_TO_WAREHOUSE,
                ConsignmentRejectReason.ACCEPT_TIMEOUT);
        final TargetConsignmentPageResult targetConsignmentPageResult = targetConsignmentDao
                .getConsignmentsForStore(Integer.valueOf(2126), OFFSET, RECS_PER_PAGE, LAST_X_CONSIGNMENT_FETCH_DAYS);
        final List<TargetConsignmentModel> targetConsignmentModel = targetConsignmentPageResult.getConsignments();
        assertThat(targetConsignmentModel).isNotNull();
        assertThat(targetConsignmentModel).hasSize(1);
    }

    @Test
    public void testGetConsignmentByStoreNumberWithoutConsignments() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);

        final List<TargetConsignmentModel> targetConsignmentModel = targetConsignmentDao
                .getConsignmentsForStore(Integer.valueOf(2126));
        assertThat(targetConsignmentModel).isNotNull();
        assertThat(targetConsignmentModel).isEmpty();
    }

    @Test
    public void testGetInstoreConsignments() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final String consCode1 = "a1002";
        final String consCode2 = "a1003";
        final String consCode3 = "a1004";
        final WarehouseModel robinaWarehouse = setupForInStoreWithFulFilmentCapabilities(Integer.valueOf(2126),
                "Robina1", "RobinaWarehouse1",
                UID, true, true, true);
        final WarehouseModel rockhamptonWarehouse = setupForInStore(Integer.valueOf(2127), "Rockhampton",
                "RockhamptonWarehouse1", "1235");
        createConsignment(consCode1, robinaWarehouse, ConsignmentStatus.PICKED, null);
        createConsignment(consCode2, robinaWarehouse, ConsignmentStatus.PACKED,
                null);
        createConsignment(consCode3, rockhamptonWarehouse, ConsignmentStatus.PACKED, null);
        orderModel.setCncStoreNumber(Integer.valueOf(2128));
        orderModel.setDeliveryMode(deliveryModeModelMap.get("click-and-collect"));
        modelService.save(orderModel);

        final List<TargetConsignmentModel> targetConsignments = targetConsignmentDao
                .getConsignmentsUnmanifestedInStore(ConsignmentStatus.PACKED,
                        2126);
        assertThat(targetConsignments).hasSize(1);
        assertThat(targetConsignments.get(0).getCode()).isEqualTo(consCode2);
    }

    @Test
    public void testGetInstoreConsignmentsNoConsignments() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final String consCode = "a1002";
        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        createConsignment(consCode, ConsignmentStatus.PICKED, null, null, null, null);
        orderModel.setCncStoreNumber(Integer.valueOf(2126));
        modelService.save(orderModel);

        final List<TargetConsignmentModel> targetConsignments = targetConsignmentDao
                .getConsignmentsUnmanifestedInStore(ConsignmentStatus.PACKED,
                        2126);
        assertThat(targetConsignments).isEmpty();
    }

    @Test
    public void testGetInstoreConsignmentsForNonExistingStore()
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final String consCode = "a1002";
        setupForInStore(Integer.valueOf(2126), "Fastline", "FastlineWarehouse", UID);
        createConsignment(consCode, ConsignmentStatus.PICKED, null, null, null, null);
        final List<TargetConsignmentModel> targetConsignments = targetConsignmentDao
                .getConsignmentsUnmanifestedInStore(ConsignmentStatus.PACKED,
                        1212);
        assertThat(targetConsignments).isEmpty();
    }

    @Test
    public void testGetInstoreConsignmentsAssociatedWithManifest()
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final String consCode = "a1002";
        final TargetManifestModel manifestModel = modelService.create(TargetManifestModel.class);
        setupForInStore(Integer.valueOf(2126), "Fastline", "FastlineWarehouse", UID);
        manifestModel.setCode("manifesttest");
        modelService.save(manifestModel);
        createConsignmentWithManifest(consCode, ConsignmentStatus.SHIPPED, manifestModel);
        final List<TargetConsignmentModel> targetConsignments = targetConsignmentDao
                .getConsignmentsUnmanifestedInStore(ConsignmentStatus.PACKED,
                        1212);
        assertThat(targetConsignments).isEmpty();
    }

    @Test
    public void testGetConsignmentByStoreNumberWithoutConsignmentsWithPagination()
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);

        final TargetConsignmentPageResult targetConsignmentPageResult = targetConsignmentDao
                .getConsignmentsForStore(Integer.valueOf(2126), OFFSET, RECS_PER_PAGE, LAST_X_CONSIGNMENT_FETCH_DAYS);

        final List<TargetConsignmentModel> targetConsignmentModel = targetConsignmentPageResult.getConsignments();
        assertThat(targetConsignmentModel).isNotNull();
        assertThat(targetConsignmentModel).isEmpty();
    }

    @Test
    public void testGetConsignmentByStoreNumberWithRecsPerPage() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException, InterruptedException {
        final int perPage = 1;
        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        createConsignment("a1002", warehouse, ConsignmentStatus.SENT_TO_WAREHOUSE, null);
        Thread.sleep(1000);
        createConsignment("a1003", warehouse, ConsignmentStatus.SENT_TO_WAREHOUSE, null);
        final TargetConsignmentPageResult targetConsignmentPageResult = targetConsignmentDao
                .getConsignmentsForStore(Integer.valueOf(2126), OFFSET, perPage, LAST_X_CONSIGNMENT_FETCH_DAYS);
        final List<TargetConsignmentModel> targetConsignmentModel = targetConsignmentPageResult.getConsignments();
        assertThat(targetConsignmentModel).isNotNull();
        assertThat(targetConsignmentModel).hasSize(1);
        assertThat(targetConsignmentPageResult.getTotal()).isEqualTo(2);
        assertThat(targetConsignmentModel.get(0).getCode()).isEqualTo("a1003");
    }

    @Test
    public void testGetConsignmentByStoreNumberWithOffset() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException, InterruptedException {
        final int perPage = 1;
        final int offset = 1;
        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        createConsignment("a1002", warehouse, ConsignmentStatus.SENT_TO_WAREHOUSE, null);
        Thread.sleep(1000);
        createConsignment("a1003", warehouse, ConsignmentStatus.SENT_TO_WAREHOUSE, null);
        final TargetConsignmentPageResult targetConsignmentPageResult = targetConsignmentDao
                .getConsignmentsForStore(Integer.valueOf(2126), offset, perPage, LAST_X_CONSIGNMENT_FETCH_DAYS);
        final List<TargetConsignmentModel> targetConsignmentModel = targetConsignmentPageResult.getConsignments();
        assertThat(targetConsignmentModel).isNotNull();
        assertThat(targetConsignmentModel).hasSize(1);
        assertThat(targetConsignmentPageResult.getTotal()).isEqualTo(2);

        // Sort order is a1003, a1002 but offset is 1
        assertThat(targetConsignmentModel.get(0).getCode()).isEqualTo("a1002");
    }

    @Test
    public void testGetConsignmentByStoreNumberWithDefaultPageNumber() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        createConsignment("a1002", warehouse, ConsignmentStatus.SENT_TO_WAREHOUSE, null);
        createConsignment("a1003", warehouse, ConsignmentStatus.SENT_TO_WAREHOUSE, null);
        final TargetConsignmentPageResult targetConsignmentPageResult = targetConsignmentDao
                .getConsignmentsForStore(Integer.valueOf(2126), 0, 0, LAST_X_CONSIGNMENT_FETCH_DAYS);
        final List<TargetConsignmentModel> targetConsignmentModel = targetConsignmentPageResult.getConsignments();
        assertThat(targetConsignmentModel).isNotNull();
        assertThat(targetConsignmentModel).hasSize(2);
        assertThat(targetConsignmentPageResult.getTotal()).isEqualTo(2);
    }

    @Test
    public void getConsignmentsByStatusForAllStore() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final String consCode = "a1002";
        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        setUpForConsignment(consCode, "click-and-collect",
                "Robina1",
                null, ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        final TargetConsignmentModel targetConsignmentModel = targetConsignmentDao.getConsignmentBycode(consCode);
        final List<TargetConsignmentModel> targetConsignments = targetConsignmentDao
                .getConsignmentsByStatusForAllStore(targetConsignmentModel.getStatus());
        assertThat(targetConsignments).hasSize(1);
        assertThat(targetConsignments.get(0).getCode()).isEqualTo(consCode);
    }

    @Test
    public void getConsignmentsByStatusForStoreInState() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final WarehouseModel warehouse1 = setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        final PointOfServiceModel pointOfService1 = warehouse1.getPointsOfService().iterator().next();
        final AddressModel address1 = modelService.create(AddressModel.class);
        address1.setOwner(pointOfService1);
        address1.setDistrict("QLD");
        pointOfService1.setAddress(address1);
        modelService.save(pointOfService1);

        final WarehouseModel warehouse2 = createForInStore(Integer.valueOf(2127), "Perth1", "PerthWarehouse1", UID2);
        final PointOfServiceModel pointOfService2 = warehouse2.getPointsOfService().iterator().next();
        final AddressModel address2 = modelService.create(AddressModel.class);
        address2.setOwner(pointOfService2);
        address2.setDistrict("WA");
        pointOfService2.setAddress(address2);
        modelService.save(pointOfService2);

        setUpForConsignment("a1001", "click-and-collect",
                "Robina1",
                warehouse1, ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        final String consCode = "a1002";
        setUpForConsignment(consCode, "click-and-collect",
                "Perth1",
                warehouse2, ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        setUpForConsignment("a1003", "click-and-collect", "Perth1", warehouse2, ConsignmentStatus.CANCELLED);
        setUpForConsignment("a1004", "click-and-collect", "Perth1", warehouse2, ConsignmentStatus.SENT_TO_WAREHOUSE);

        final List<TargetConsignmentModel> targetConsignments = targetConsignmentDao
                .getConsignmentsByStatusForStoreInState(
                        Arrays.asList(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, ConsignmentStatus.SENT_TO_WAREHOUSE),
                        Arrays.asList("WA"));

        assertThat(targetConsignments).as("Expcted number of consignments in result").hasSize(2);
        for (final TargetConsignmentModel consignment : targetConsignments) {
            assertThat(consignment.getCode()).as("Consignment Code").isIn("a1002", "a1004");
            assertThat(consignment.getStatus()).as("Consignment Status").isIn(ConsignmentStatus.SENT_TO_WAREHOUSE,
                    ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
            assertThat(consignment.getWarehouse().getPointsOfService().iterator().next().getName()).isEqualTo("Perth1");
        }
    }

    @Test
    public void getConsignmentsByStatusForStoreNotInState() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final WarehouseModel warehouse1 = setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        final PointOfServiceModel pointOfService1 = warehouse1.getPointsOfService().iterator().next();
        final AddressModel address1 = modelService.create(AddressModel.class);
        address1.setOwner(pointOfService1);
        address1.setDistrict("QLD");
        pointOfService1.setAddress(address1);
        modelService.save(pointOfService1);

        final WarehouseModel warehouse2 = createForInStore(Integer.valueOf(2127), "Perth1", "PerthWarehouse1", UID2);
        final PointOfServiceModel pointOfService2 = warehouse2.getPointsOfService().iterator().next();
        final AddressModel address2 = modelService.create(AddressModel.class);
        address2.setOwner(pointOfService2);
        address2.setDistrict("WA");
        pointOfService2.setAddress(address2);
        modelService.save(pointOfService2);

        final String consCode = "a1001";
        setUpForConsignment(consCode, "click-and-collect",
                "Robina1",
                warehouse1, ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        setUpForConsignment("a1005", "click-and-collect", "Robina1", warehouse1, ConsignmentStatus.SENT_TO_WAREHOUSE);
        setUpForConsignment("a1009", "click-and-collect", "Robina1", warehouse1, ConsignmentStatus.WAVED);
        setUpForConsignment("a1002", "click-and-collect",
                "Perth1",
                warehouse2, ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);

        final List<TargetConsignmentModel> targetConsignments = targetConsignmentDao
                .getConsignmentsByStatusForStoreNotInState(
                        Arrays.asList(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, ConsignmentStatus.SENT_TO_WAREHOUSE),
                        Arrays.asList("WA"));

        assertThat(targetConsignments).as("Expcted number of consignments in result").hasSize(2);
        for (final TargetConsignmentModel consignment : targetConsignments) {
            assertThat(consignment.getCode()).as("Consignment Code").isIn("a1001", "a1005");
            assertThat(consignment.getStatus()).as("Consignment Status").isIn(ConsignmentStatus.SENT_TO_WAREHOUSE,
                    ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
            assertThat(consignment.getWarehouse().getPointsOfService().iterator().next().getName())
                    .isEqualTo("Robina1");
        }
    }

    @Test
    public void testGetInstoreConsignmentsForHomeDelivery()
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final String consCode = "a1002";
        setupForOnlineWareHouse("FastlineWarehouse");
        setUpForConsignment("a1002", "express-delivery", "Fastine",
                null, ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        final TargetConsignmentModel targetConsignmentModel = targetConsignmentDao.getConsignmentBycode(consCode);
        final List<TargetConsignmentModel> targetConsignments = targetConsignmentDao
                .getConsignmentsByStatusForAllStore(targetConsignmentModel.getStatus());
        assertThat(targetConsignments).isEmpty();
    }

    @Test
    public void testGetInstoreConsignmentsForNonExistingStatus()
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        setUpForConsignment("a1002", "express-delivery", "Fastine",
                null, ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        final List<TargetConsignmentModel> targetConsignments = targetConsignmentDao
                .getConsignmentsByStatusForAllStore(ConsignmentStatus.PICKED);
        assertThat(targetConsignments).isEmpty();
    }

    @Test
    public void testGetConsignmentsNotProcessedForStore() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        createConsignmentsWithAllStatusesForStore();

        final List<TargetConsignmentModel> consignments = targetConsignmentDao
                .getConsignmentsNotProcessedForStore(pointOfServiceModel);
        assertThat(consignments).isNotEmpty();
        assertThat(consignments).hasSize(5);
    }

    @Test
    public void testGetCountOfConsignmentsNotProcessedForStoreByStatus() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        createConsignmentsWithAllStatusesForStore();

        final Map<ConsignmentStatus, Integer> consignments = targetConsignmentDao
                .getCountOfConsignmentsNotProcessedForStoreByStatus(pointOfServiceModel);
        assertThat(consignments).isNotEmpty();
        assertThat(consignments).hasSize(5);
        assertThat(consignments.containsKey(ConsignmentStatus.CREATED)).isFalse();

    }

    @Test
    public void testGetConsignmentsCompletedByDayForStore() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        createConsignmentsWithAllStatusesForStore();

        final List<TargetConsignmentModel> consignments = targetConsignmentDao
                .getConsignmentsCompletedByDayForStore(pointOfServiceModel, 0);
        assertThat(consignments).isNotEmpty();
        assertThat(consignments).hasSize(1);
    }

    @Test
    public void testGetCountOfConsignmentsCompletedByDayForStoreByStatus() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        createConsignmentsWithAllStatusesForStore();

        final Map<ConsignmentStatus, Integer> consignmentStatusMap = targetConsignmentDao
                .getCountOfConsignmentsCompletedByDayForStoreByStatus(pointOfServiceModel, 0);
        assertThat(consignmentStatusMap).isNotEmpty();
        assertThat(consignmentStatusMap).hasSize(1);
    }

    @Test
    public void testGetConsignmentsRejectedByDayForStore() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        createConsignmentsWithAllStatusesForStore();

        final List<TargetConsignmentModel> consignments = targetConsignmentDao
                .getConsignmentsRejectedByDayForStore(pointOfServiceModel, 0);
        assertThat(consignments).isNotEmpty();
        assertThat(consignments).hasSize(1);
    }

    @Test
    public void testGetCountOfConsignmentsRejectedByDayForStoreByStatus() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        createConsignmentsWithAllStatusesForStore();

        final Map<ConsignmentStatus, Integer> consignments = targetConsignmentDao
                .getCountOfConsignmentsRejectedByDayForStoreByStatus(pointOfServiceModel, 0);
        assertThat(consignments).isNotEmpty();
        assertThat(consignments).hasSize(1);
    }

    @Test
    public void testGetOpenConsignmentsForStoree() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        createConsignmentsWithAllStatusesForStore();

        final List<TargetConsignmentModel> consignments = targetConsignmentDao
                .getOpenConsignmentsForStore(pointOfServiceModel);
        assertThat(consignments).isNotEmpty();
        assertThat(consignments).hasSize(1);
    }

    @Test
    public void testConsignmentCountWithHDAndInterStoreForGivenDate() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException, InterruptedException {
        final WarehouseModel robinaWarehouse = setUpInStoreFulFilmentWithGlobalSettings("00:00", Integer.valueOf(2126),
                "Robina1", "RobinaWarehouse1",
                UID, true, true, true);
        setUpOrdersWithDifferentDeliveryModes("testcode001", ConsignmentStatus.CREATED, "home-delivery",
                robinaWarehouse, Integer.valueOf(2126));
        setUpOrdersWithDifferentDeliveryModes("testcode002", ConsignmentStatus.CREATED, "click-and-collect",
                robinaWarehouse, Integer.valueOf(2126));
        setUpOrdersWithDifferentDeliveryModes("testcode003", ConsignmentStatus.CREATED, "inter-store",
                robinaWarehouse, Integer.valueOf(2126));
        setUpOrdersWithDifferentDeliveryModes("testcode004", ConsignmentStatus.CREATED, "home-delivery",
                robinaWarehouse, Integer.valueOf(2126));
        setUpOrdersWithDifferentDeliveryModes("testcode005", ConsignmentStatus.CREATED, "click-and-collect",
                robinaWarehouse, Integer.valueOf(2126));
        setUpOrdersWithDifferentDeliveryModes("testcode006", ConsignmentStatus.CREATED, "inter-store",
                robinaWarehouse, Integer.valueOf(2126));
        setUpOrdersWithDifferentDeliveryModes("testcode007", ConsignmentStatus.CREATED, "home-delivery",
                robinaWarehouse, Integer.valueOf(2126));
        final int resultOrderCount = targetConsignmentDao
                .getConsignmentCountForStoreByMaxCutoffPeriod(Integer.valueOf(2126), new DateTime().minusHours(1)
                        .toDate());
        assertThat(Integer.valueOf(resultOrderCount)).isNotNull();
        assertThat(resultOrderCount).isEqualTo(5);
    }

    @Test
    public void testConsignmentCountWithDateFilter() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException, InterruptedException {
        final WarehouseModel robinaWarehouse = setUpInStoreFulFilmentWithGlobalSettings("00:00", Integer.valueOf(2126),
                "Robina1", "RobinaWarehouse1",
                UID, true, true, true);
        setUpOrdersWithDifferentDeliveryModes("testcode001", ConsignmentStatus.CREATED, "home-delivery",
                robinaWarehouse, Integer.valueOf(2126));
        setUpOrdersWithDifferentDeliveryModes("testcode002", ConsignmentStatus.CREATED, "click-and-collect",
                robinaWarehouse, Integer.valueOf(2126));
        setUpOrdersWithDifferentDeliveryModes("testcode003", ConsignmentStatus.CREATED, "inter-store",
                robinaWarehouse, Integer.valueOf(2126));
        Thread.sleep(10000);
        final Date date = new Date();
        setUpOrdersWithDifferentDeliveryModes("testcode004", ConsignmentStatus.CREATED, "home-delivery",
                robinaWarehouse, Integer.valueOf(2126));
        setUpOrdersWithDifferentDeliveryModes("testcode005", ConsignmentStatus.CREATED, "click-and-collect",
                robinaWarehouse, Integer.valueOf(2126));
        setUpOrdersWithDifferentDeliveryModes("testcode006", ConsignmentStatus.CREATED, "inter-store",
                robinaWarehouse, Integer.valueOf(2126));
        setUpOrdersWithDifferentDeliveryModes("testcode007", ConsignmentStatus.CREATED, "home-delivery",
                robinaWarehouse, Integer.valueOf(2126));
        final int resultOrderCount = targetConsignmentDao
                .getConsignmentCountForStoreByMaxCutoffPeriod(Integer.valueOf(2126), date);
        assertThat(Integer.valueOf(resultOrderCount)).isNotNull();
        assertThat(resultOrderCount).isEqualTo(3);
    }

    @Test
    public void testGetConsignmentsForAutoUpadatePartnerPickedUp() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException, ParseException {

        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);

        createOrderForAutoUpadatePartnerPickedUp("1000001", ConsignmentStatus.READY_FOR_PICKUP,
                deliveryModeModelMap.get("click-and-collect"), -1, 0);
        createOrderForAutoUpadatePartnerPickedUp("1000002", ConsignmentStatus.READY_FOR_PICKUP,
                deliveryModeModelMap.get("home-delivery"), 0,
                0);
        createOrderForAutoUpadatePartnerPickedUp("1000003", ConsignmentStatus.READY_FOR_PICKUP,
                deliveryModeModelMap.get("express-delivery"), 0, 0);
        createOrderForAutoUpadatePartnerPickedUp("1000004", ConsignmentStatus.READY_FOR_PICKUP,
                deliveryModeModelMap.get("home-delivery"), 0, 0);
        createOrderForAutoUpadatePartnerPickedUp("1000005", ConsignmentStatus.CANCELLED,
                deliveryModeModelMap.get("click-and-collect"), -1,
                -1);
        createOrderForAutoUpadatePartnerPickedUp("1000006", ConsignmentStatus.READY_FOR_PICKUP,
                deliveryModeModelMap.get("click-and-collect"), -10, 0);
        createOrderForAutoUpadatePartnerPickedUp("1000007", ConsignmentStatus.READY_FOR_PICKUP,
                deliveryModeModelMap.get("click-and-collect"), -2, -1);
        createOrderForAutoUpadatePartnerPickedUp("1000008", ConsignmentStatus.READY_FOR_PICKUP,
                deliveryModeModelMap.get("click-and-collect"), -8, 0);
        final List<TargetConsignmentModel> consignments = targetConsignmentDao
                .getConsignmentsForAutoUpdatePartnerPickedup("click-and-collect", 7);
        assertThat(consignments).isNotEmpty();
        assertThat(consignments).hasSize(2);
    }

    @Test
    public void getAllConsignmentsSentToWarehouse() throws TargetUnknownIdentifierException {
        createConsignment("a1001", incommWarehouse, CREATED, null);
        createConsignment("a1002", incommWarehouse, SENT_TO_WAREHOUSE, null);
        createConsignment("a1003", incommWarehouse, CONFIRMED_BY_WAREHOUSE, null);
        createConsignment("a1004", incommWarehouse, SHIPPED, null);

        final OrderModel order = modelService.create(OrderModel.class);
        order.setSubtotal(valueOf(30.0d));
        order.setCurrency(commonI18NService.getCurrentCurrency());
        order.setUser(userModel);
        order.setDate(new Date());
        order.setFluentId("252");
        createConsignmentWithOrder("a1005", incommWarehouse, SENT_TO_WAREHOUSE, null, order);

        final List<TargetConsignmentModel> consignmentsList = targetConsignmentDao
                .getAllConsignmentsByStatusAndWarehouse(incommWarehouse, SENT_TO_WAREHOUSE);

        assertThat(consignmentsList).isNotEmpty();
        assertThat(consignmentsList).hasSize(1);
        assertThat(consignmentsList.get(0).getCode()).isEqualTo("a1002");
        assertThat(consignmentsList.get(0).getStatus()).isEqualTo(SENT_TO_WAREHOUSE);
        assertThat(consignmentsList.get(0).getOrder().getFluentId()).isNull();
    }

    @Test
    public void getAllConsignmentsConfirmedByWarehouse() throws TargetUnknownIdentifierException {
        createConsignment("a1004", incommWarehouse, SENT_TO_WAREHOUSE, null);
        createConsignment("a1005", incommWarehouse, CONFIRMED_BY_WAREHOUSE, null);
        final List<TargetConsignmentModel> consignments = targetConsignmentDao
                .getAllConsignmentsByStatusAndWarehouse(incommWarehouse, CONFIRMED_BY_WAREHOUSE);
        assertThat(consignments).isNotEmpty();
        assertThat(consignments).hasSize(1);
        assertThat(consignments.get(0).getCode()).isEqualTo("a1005");
        assertThat(consignments.get(0).getStatus()).isEqualTo(CONFIRMED_BY_WAREHOUSE);
    }

    @Test
    public void getAllConsignmentsConfirmedByWarehouseNoMatchingwarehouse() throws TargetUnknownIdentifierException {
        createConsignment("a1006", incommWarehouse, ConsignmentStatus.SENT_TO_WAREHOUSE, null);
        createConsignment("a1007", incommWarehouse, ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, null);
        final List<TargetConsignmentModel> consignments = targetConsignmentDao
                .getAllConsignmentsByStatusAndWarehouse(manjimupwarehouse, ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        assertThat(consignments).isEmpty();
    }

    @Test
    public void getAllConsignmentsConfirmedByWarehouseNoMatchingConsignment() throws TargetUnknownIdentifierException {
        createConsignment("a1008", incommWarehouse, ConsignmentStatus.SENT_TO_WAREHOUSE, null);
        createConsignment("a1009", incommWarehouse, ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, null);
        final List<TargetConsignmentModel> consignments = targetConsignmentDao
                .getAllConsignmentsByStatusAndWarehouse(incommWarehouse, ConsignmentStatus.PICKED);
        assertThat(consignments).isEmpty();
    }

    @Test
    public void testGetConsignmentsOfMultiplesStatusesForAllStore() throws Exception {

        setupForInStore(Integer.valueOf(2126), "Robina1", "RobinaWarehouse1", UID);
        setUpForConsignment("a1002", "click-and-collect",
                "Robina1",
                null, ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        setUpForConsignment("a1003", "click-and-collect",
                "Robina1",
                null, ConsignmentStatus.CANCELLED);
        setUpForConsignment("a1004", "click-and-collect",
                "Robina1",
                null, ConsignmentStatus.SENT_TO_WAREHOUSE);

        final List<TargetConsignmentModel> consignments = targetConsignmentDao
                .getConsignmentsOfMultiplesStatusesForAllStores(
                        Arrays.asList(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, ConsignmentStatus.SENT_TO_WAREHOUSE));

        assertThat(consignments).isNotEmpty().hasSize(2);
        for (final TargetConsignmentModel consignment : consignments) {
            assertThat(consignment.getCode()).isIn("a1002", "a1004");
            assertThat(consignment.getStatus()).isIn(ConsignmentStatus.SENT_TO_WAREHOUSE,
                    ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        }

    }

    private void createOrderForAutoUpadatePartnerPickedUp(final String code,
            final ConsignmentStatus status, final TargetZoneDeliveryModeModel deliveryModeModel,
            final int readyForPickupDate,
            final int pickedupAutoDate)
                    throws ParseException {
        final Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        final OrderModel order = modelService.create(OrderModel.class);
        order.setSubtotal(Double.valueOf(30.0d));
        order.setCurrency(commonI18NService.getCurrentCurrency());
        order.setUser(userModel);
        order.setDate(new Date());
        modelService.save(order);
        final TargetConsignmentModel consignment = modelService.create(TargetConsignmentModel.class);
        consignment.setCode(code);
        consignment.setStatus(status);
        consignment.setWarehouse(warehouse);
        consignment.setShippingAddress(shippingAddress);
        consignment.setDeliveryMode(deliveryModeModel);
        if (readyForPickupDate != 0) {
            c.add(Calendar.DATE, readyForPickupDate);
            consignment.setReadyForPickUpDate(c.getTime());
        }
        if (pickedupAutoDate != 0) {
            c.add(Calendar.DATE, pickedupAutoDate);
            consignment.setPickedUpAutoDate(c.getTime());
        }
        consignment.setOrder(order);
        modelService.save(consignment);
        modelService.save(order);

    }

    private void setUpOrdersWithDifferentDeliveryModes(final String code,
            final ConsignmentStatus status, final String deliveryMode, final WarehouseModel warehouseModel,
            final Integer storeNumber) {
        final OrderModel order = modelService.create(OrderModel.class);
        order.setSubtotal(Double.valueOf(30.0d));
        order.setCurrency(commonI18NService.getCurrentCurrency());
        order.setUser(userModel);
        order.setDate(new Date());
        modelService.save(order);
        final TargetConsignmentModel consignment = modelService.create(TargetConsignmentModel.class);
        consignment.setCode(code);
        consignment.setStatus(status);
        consignment.setWarehouse(warehouseModel);
        consignment.setShippingAddress(shippingAddress);
        consignment.setOrder(order);
        modelService.save(consignment);
        if (deliveryMode.equals("click-and-collect")) {
            order.setCncStoreNumber(storeNumber);
        }
        else if (deliveryMode.equals("inter-store")) {
            order.setCncStoreNumber(Integer.valueOf(9999));
        }
        else if (deliveryMode.equalsIgnoreCase("home-delivery")) {
            order.setCncStoreNumber(null);
        }
        modelService.save(order);

    }

    private WarehouseModel setUpInStoreFulFilmentWithGlobalSettings(final String maxOrderPeriodStartTime,
            final Integer storeNumber,
            final String storeName,
            final String warehouseCode, final String uid, final boolean isEnabled, final boolean isSameStoreEnabled,
            final boolean isInterStoreEnabled) {
        final GlobalStoreFulfilmentCapabilitiesModel globalFulfilmentModel = modelService
                .create(GlobalStoreFulfilmentCapabilitiesModel.class);
        globalFulfilmentModel.setMaxOrderPeriodStartTime(maxOrderPeriodStartTime);
        globalFulfilmentModel.setMaxTimeToPick(Integer.valueOf(180));
        globalFulfilmentModel.setCode("dummycode");
        globalFulfilmentModel.setMaxItemsPerConsignment(Integer.valueOf(10));
        modelService.save(globalFulfilmentModel);
        return setupForInStoreWithFulFilmentCapabilities(storeNumber, storeName, warehouseCode,
                uid, isEnabled, isSameStoreEnabled, isInterStoreEnabled);
    }

    private void createConsignmentsWithAllStatusesForStore() {
        createConsignment("a1001", warehouse, ConsignmentStatus.CREATED, null);
        createConsignment("a1002", warehouse, ConsignmentStatus.SENT_TO_WAREHOUSE, null);
        createConsignment("a1003", warehouse, ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, null);
        createConsignment("a1004", warehouse, ConsignmentStatus.WAVED, null);
        createConsignment("a1005", warehouse, ConsignmentStatus.PICKED, null);
        createConsignment("a1006", warehouse, ConsignmentStatus.SHIPPED, null);
        createConsignment("a1007", warehouse, ConsignmentStatus.CANCELLED, null);
        createConsignment("a1007", warehouse, ConsignmentStatus.CANCELLED,
                ConsignmentRejectReason.CANCELLED_BY_CUSTOMER);
        createConsignment("a1008", warehouse, ConsignmentStatus.PACKED, null);
    }

    private void createConsignment(final String code, final ConsignmentStatus status, final Date shipAdviceSentDate) {
        createConsignment(code, status, shipAdviceSentDate, null, null, null);
    }

    private void createConsignmentWithManifest(final String code, final ConsignmentStatus status,
            final TargetManifestModel manifestModel) {
        final TargetConsignmentModel consignment = createConsignmentWithBasicAttributes(code, status);
        if (manifestModel != null) {
            consignment.setManifest(manifestModel);
        }
        consignment.setWarehouse(warehouse);
        modelService.save(consignment);
    }

    private void createConsignment(final String code, final ConsignmentStatus status, final Date shipAdviceSentDate,
            final TargetCarrierModel targetCarrierModel, final Date sentToWareHouseDate,
            final WarehouseModel externalWarehouse) {
        final TargetConsignmentModel consignment = modelService.create(TargetConsignmentModel.class);
        consignment.setCode(code);
        consignment.setStatus(status);
        consignment.setShipAdviceSentDate(shipAdviceSentDate);
        if (sentToWareHouseDate != null) {
            consignment.setSentToWarehouseDate(sentToWareHouseDate);
        }
        consignment.setWarehouse(warehouse);
        if (targetCarrierModel != null) {
            consignment.setTargetCarrier(targetCarrierModel);
        }
        if (externalWarehouse != null) {
            consignment.setWarehouse(externalWarehouse);
        }
        consignment.setShippingAddress(shippingAddress);
        consignment.setOrder(orderModel);
        modelService.save(consignment);
    }

    private TargetConsignmentModel createConsignment(final String code, final WarehouseModel warehouseModel,
            final ConsignmentStatus status,
            final ConsignmentRejectReason consignmentRejectReason) {
        final TargetConsignmentModel consignment = createConsignmentWithBasicAttributes(code, status);
        consignment.setWarehouse(warehouseModel);
        consignment.setCreationtime(new Date());
        if (consignmentRejectReason != null) {
            consignment.setRejectReason(consignmentRejectReason);
        }
        modelService.save(consignment);
        return consignment;
    }

    private void createConsignment(final String code, final Date creationDate, final ConsignmentStatus status,
            final TargetCarrierModel targetCarrierModel) {
        final TargetConsignmentModel consignment = createConsignmentWithBasicAttributes(code, status);
        if (targetCarrierModel != null) {
            consignment.setTargetCarrier(targetCarrierModel);
        }
        consignment.setWarehouse(warehouse);
        consignment.setCreationtime(creationDate);
        modelService.save(consignment);

    }

    private TargetConsignmentModel createConsignmentWithBasicAttributes(final String code,
            final ConsignmentStatus status) {
        final TargetConsignmentModel consignment = modelService.create(TargetConsignmentModel.class);
        consignment.setCode(code);
        consignment.setStatus(status);
        consignment.setShippingAddress(shippingAddress);
        consignment.setOrder(orderModel);
        return consignment;
    }

    private Date getAdjustedDate(final Date date, final int minutes) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, minutes);
        final Date adjustedDate = cal.getTime();
        return adjustedDate;
    }

    private void verifyConsigments(final List<String> targetConsignmentModelListExpected,
            final List<TargetConsignmentModel> targetConsignmentModelListResult) {

        for (final TargetConsignmentModel targetConsignmentModel : targetConsignmentModelListResult) {
            assertThat(targetConsignmentModelListExpected).contains(targetConsignmentModel.getCode());
        }
    }

    private WarehouseModel setupForInStore(final Integer storeNumber, final String storeName,
            final String warehouseCode, final String uid) {
        final WarehouseModel storeWarehouse = createForInStore(storeNumber, storeName, warehouseCode, uid);
        this.warehouse = storeWarehouse;
        this.pointOfServiceModel = (TargetPointOfServiceModel)storeWarehouse.getPointsOfService().iterator().next();
        this.baseStoreModel = this.pointOfServiceModel.getBaseStore();
        return storeWarehouse;
    }

    private WarehouseModel createForInStore(final Integer storeNumber, final String storeName,
            final String warehouseCode, final String uid) {

        final VendorModel vendor = modelService.create(VendorModel.class);
        vendor.setCode("target");
        modelService.save(vendor);
        //setup store warehouse
        final WarehouseModel storeWarehouse = modelService.create(WarehouseModel.class);
        storeWarehouse.setCode(warehouseCode);
        storeWarehouse.setVendor(vendor);
        storeWarehouse.setIntegrationMethod(IntegrationMethod.ESB);
        final BaseStoreModel storeForWarehouse = modelService.create(BaseStoreModel.class);
        storeForWarehouse.setName(storeName);
        storeForWarehouse.setUid(uid);
        modelService.save(storeForWarehouse);
        final TargetPointOfServiceModel pointOfServiceForWarehouse = modelService
                .create(TargetPointOfServiceModel.class);
        pointOfServiceForWarehouse.setName(storeName);
        pointOfServiceForWarehouse.setType(PointOfServiceTypeEnum.TARGET);
        pointOfServiceForWarehouse.setBaseStore(storeForWarehouse);
        pointOfServiceForWarehouse.setStoreNumber(storeNumber);
        modelService.save(pointOfServiceForWarehouse);
        final Set<PointOfServiceModel> pointOfServiceModels = new HashSet<>();
        pointOfServiceModels.add(pointOfServiceForWarehouse);
        storeWarehouse.setPointsOfService(pointOfServiceModels);
        modelService.save(storeWarehouse);
        return storeWarehouse;
    }

    private WarehouseModel setupForInStoreWithFulFilmentCapabilities(final Integer storeNumber, final String storeName,
            final String warehouseCode, final String uid, final boolean isEnabled, final boolean isSameStoreEnabled,
            final boolean isInterStoreEnabled) {

        final VendorModel vendor = modelService.create(VendorModel.class);
        vendor.setCode("target");
        modelService.save(vendor);
        //setup store warehouse
        warehouse = modelService.create(WarehouseModel.class);
        warehouse.setCode(warehouseCode);
        warehouse.setVendor(vendor);
        warehouse.setIntegrationMethod(IntegrationMethod.ESB);
        baseStoreModel = modelService.create(BaseStoreModel.class);
        baseStoreModel.setName(storeName);
        baseStoreModel.setUid(uid);
        modelService.save(baseStoreModel);
        pointOfServiceModel = modelService.create(TargetPointOfServiceModel.class);
        pointOfServiceModel.setName(storeName);
        pointOfServiceModel.setType(PointOfServiceTypeEnum.TARGET);
        pointOfServiceModel.setBaseStore(baseStoreModel);
        pointOfServiceModel.setStoreNumber(storeNumber);
        final StoreFulfilmentCapabilitiesModel fulfilmentCapability = setUpStoreFulfilmentCapabilities(isEnabled,
                isSameStoreEnabled, isInterStoreEnabled);
        pointOfServiceModel.setFulfilmentCapability(fulfilmentCapability);
        modelService.save(pointOfServiceModel);
        final Set<PointOfServiceModel> pointOfServiceModels = new HashSet<>();
        pointOfServiceModels.add(pointOfServiceModel);
        warehouse.setPointsOfService(pointOfServiceModels);
        modelService.save(warehouse);
        return warehouse;
    }

    /**
     * @return StoreFulfilmentCapabilities
     */
    private StoreFulfilmentCapabilitiesModel setUpStoreFulfilmentCapabilities(final boolean isEnabled,
            final boolean isSameStoreEnabled, final boolean isInterStoreEnabled) {
        final StoreFulfilmentCapabilitiesModel fulfilmentCapabilities = modelService
                .create(StoreFulfilmentCapabilitiesModel.class);
        fulfilmentCapabilities.setEnabled(Boolean.valueOf(isEnabled));
        fulfilmentCapabilities.setAllowDeliveryToThisStore(Boolean.valueOf(isSameStoreEnabled));
        fulfilmentCapabilities.setAllowDeliveryToAnotherStore(Boolean.valueOf(isInterStoreEnabled));
        if (isInterStoreEnabled) {
            fulfilmentCapabilities.setMaxConsignmentsForDeliveryPerDay(Integer.valueOf(0));
            fulfilmentCapabilities.setDeliveryModes(Collections.singleton(deliveryModeModelMap
                    .get("click-and-collect")));
            fulfilmentCapabilities.setMerchantLocationId("JDQ1");
            fulfilmentCapabilities.setChargeCode("S1");
            fulfilmentCapabilities.setChargeAccountNumber(Integer.valueOf(6616510));
            fulfilmentCapabilities.setUsername("eParcelDemoSoap");
            fulfilmentCapabilities.setServiceCode(Integer.valueOf(60));
            fulfilmentCapabilities.setProductCode(Integer.valueOf(2));
            fulfilmentCapabilities.setMerchantId("123456789");
            fulfilmentCapabilities.setLodgementFacility("351310 - MPF BULK PARCELS (PROFIT CTR)");
        }
        return fulfilmentCapabilities;
    }

    private void setupConsignmentCodes(final String... consignmentLabels) {
        consignmentCodes = new ArrayList<>();
        for (final String consignmentLabel : consignmentLabels) {
            consignmentCodes.add(consignmentLabel);
        }
    }

    /**
     * @param consCode
     * @param deliveryMode
     * @param storeName
     * @param warehouseForConsignment
     * @return DeliveryModeModel
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    private DeliveryModeModel setUpForConsignment(final String consCode, final String deliveryMode,
            final String storeName,
            final WarehouseModel warehouseForConsignment, final ConsignmentStatus consignmentStatus)
                    throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        createConsignment(consCode, consignmentStatus, null, null, null,
                warehouseForConsignment);
        final TargetConsignmentModel targetConsignmentModel = targetConsignmentDao.getConsignmentBycode(consCode);
        final DeliveryModeModel deliveryModeModel = deliveryModeModelMap.get(deliveryMode);

        final OrderModel order = modelService.create(OrderModel.class);
        order.setSubtotal(Double.valueOf(30.0d));
        order.setDeliveryMode(deliveryModeModel);
        order.setCurrency(commonI18NService.getCurrentCurrency());
        order.setUser(userModel);
        order.setDate(new Date());

        final Set<ConsignmentModel> consignmentSet = new HashSet<>();
        consignmentSet.add(targetConsignmentModel);
        order.setConsignments(consignmentSet);
        modelService.save(order);
        return deliveryModeModel;
    }

    private void setupForOnlineWareHouse(final String warehouseCode) {

        final VendorModel vendor = modelService.create(VendorModel.class);
        vendor.setCode("target");
        modelService.save(vendor);
        //setup store warehouse
        warehouse = modelService.create(WarehouseModel.class);
        warehouse.setCode(warehouseCode);
        warehouse.setVendor(vendor);
        warehouse.setIntegrationMethod(IntegrationMethod.ESB);
        baseStoreModel = modelService.create(BaseStoreModel.class);
        baseStoreModel.setName("Base");
        baseStoreModel.setUid("1234");
        modelService.save(baseStoreModel);
        modelService.save(warehouse);
    }

    private TargetConsignmentModel createConsignmentWithOrder(final String code, final WarehouseModel warehouseModel,
            final ConsignmentStatus status,
            final ConsignmentRejectReason consignmentRejectReason, final AbstractOrderModel order) {
        final TargetConsignmentModel consignment = createConsignmentWithBasicAttributes(code, status);
        consignment.setWarehouse(warehouseModel);
        consignment.setCreationtime(new Date());
        if (consignmentRejectReason != null) {
            consignment.setRejectReason(consignmentRejectReason);
        }

        consignment.setOrder(order);
        modelService.save(consignment);
        return consignment;
    }
}
