/**
 * 
 */
package au.com.target.tgtwsfacades.storelocator.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.storelocator.model.OpeningDayModel;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.model.TargetOpeningDayModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.TargetOpeningDayService;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationAddressDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationOpeningDayDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationOpeningScheduleDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationPointOfServiceDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;

import com.google.common.base.Charsets;


/**
 * @author fkratoch
 * 
 */
@IntegrationTest
public class TargetStoreLocatorIntegrationFacadeTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetStoreLocatorIntegrationFacadeImpl storeLocatorIntegrationFacade;

    @Resource
    private TargetPointOfServiceService targetPointOfServiceService;

    @Resource
    private TargetOpeningDayService targetOpeningDayService;

    @Resource
    private UserService userService;

    @Resource
    private ModelService modelService;

    private OpeningScheduleModel schedule;

    @Before
    public void initialise() throws ImpExException {
        setContextUser();
        importCsv("/tgtwsfacades/test/testTargetStoreLocatorIntegrationFacade.impex", Charsets.UTF_8.name());
    }

    @Test
    public void testCanCreateNewPointOfService() throws Exception {

        final Integer storeNumber = Integer.valueOf(1111);
        final IntegrationPointOfServiceDto dto = getBasePointOfServiceDto(storeNumber, true);

        final IntegrationResponseDto response = storeLocatorIntegrationFacade.persistPointOfService(dto);
        TargetPointOfServiceModel tpos = null;

        tpos = targetPointOfServiceService.getPOSByStoreNumber(storeNumber);

        assertTrue("New PointOfService created", response.isSuccessStatus());
        assertTrue("PointOfService has correct storeNumber", tpos.getStoreNumber().equals(dto.getStoreNumber()));
        assertTrue("PointOfService has correct storeName", tpos.getName().equalsIgnoreCase(dto.getStoreName()));
    }

    @Test
    public void testRejectNewPointOfServiceWithoutAddress() throws Exception {

        final Integer storeNumber = Integer.valueOf(1111);
        final IntegrationPointOfServiceDto dto = getBasePointOfServiceDto(storeNumber, false);

        final IntegrationResponseDto response = storeLocatorIntegrationFacade.persistPointOfService(dto);
        assertFalse("New PointOfService rejected", response.isSuccessStatus());
        assertEquals("PointOfService correct error message",
                "Cannot create/update PointOfService without a valid address!",
                response.getMessages().get(0));
    }

    @Test
    public void testCanUpdateExistingPointOfService() throws Exception {

        final Integer storeNumber = Integer.valueOf(2222);

        // create new store
        final IntegrationPointOfServiceDto dto = getBasePointOfServiceDto(storeNumber, true);

        storeLocatorIntegrationFacade.persistPointOfService(dto);

        // update the store
        final IntegrationPointOfServiceDto dto2 = getBasePointOfServiceDto(storeNumber, true);
        dto2.setAcceptCNC(Boolean.FALSE);
        dto2.setAcceptLayBy(Boolean.FALSE);
        dto2.setLatitude(Double.valueOf(-36.987));
        dto2.setLongitude(Double.valueOf(144.123));

        storeLocatorIntegrationFacade.persistPointOfService(dto2);

        final TargetPointOfServiceModel tpos = targetPointOfServiceService.getPOSByStoreNumber(storeNumber);

        assertFalse("PointOfService does not accept CNC", tpos.getAcceptCNC().booleanValue());
        assertFalse("PointOfService does not accept Layby", tpos.getAcceptLayBy().booleanValue());
        assertTrue("PointOfService has correct latitude", tpos.getLatitude().equals(dto2.getLatitude()));
        assertTrue("PointOfService has correct longitude", tpos.getLongitude().equals(dto2.getLongitude()));
    }

    @Test
    public void testRejectUpdateExistingPointOfServiceWithoutAddress() throws Exception {

        final Integer storeNumber = Integer.valueOf(2222);

        // create new store
        final IntegrationPointOfServiceDto dto = getBasePointOfServiceDto(storeNumber, true);
        storeLocatorIntegrationFacade.persistPointOfService(dto);

        // update the store
        final IntegrationPointOfServiceDto dto2 = getBasePointOfServiceDto(storeNumber, false);
        final IntegrationResponseDto response = storeLocatorIntegrationFacade.persistPointOfService(dto2);

        assertFalse("Update PointOfService rejected", response.isSuccessStatus());
        assertEquals("PointOfService correct error message",
                "Cannot create/update PointOfService without a valid address!",
                response.getMessages().get(0));
    }

    @Test
    public void testExceptionWhenStatusNotSet() {

        final Integer storeNumber = Integer.valueOf(3333);

        final IntegrationPointOfServiceDto dto = new IntegrationPointOfServiceDto();
        dto.setStoreNumber(storeNumber);
        dto.setStoreName("Point Of Service " + storeNumber);
        dto.setType("Target");
        dto.setBaseStore("target");
        dto.setAcceptCNC(Boolean.TRUE);
        dto.setAcceptLayBy(Boolean.TRUE);

        final IntegrationResponseDto response = storeLocatorIntegrationFacade.persistPointOfService(dto);

        assertFalse("PointOfService not saved, ModelSavingException", response.isSuccessStatus());
    }

    @Test
    public void testCanSaveAddress() throws Exception {
        final Integer storeNumber = Integer.valueOf(5555);
        final IntegrationPointOfServiceDto dto = getBasePointOfServiceDto(storeNumber, true);

        final IntegrationAddressDto address = new IntegrationAddressDto();
        address.setStreetname("123 Retail Ave");
        address.setDistrict("VIC");
        address.setTown("Geelong");
        address.setCountry("Australia");
        dto.setAddress(address);

        storeLocatorIntegrationFacade.persistPointOfService(dto);

        final TargetPointOfServiceModel tpos = targetPointOfServiceService.getPOSByStoreNumber(storeNumber);

        assertTrue("Can save store address", tpos.getAddress().getStreetname().equalsIgnoreCase("123 Retail Ave"));
    }

    @Test
    public void testCanHaveTargetCountryType() throws Exception {
        final Integer storeNumber = Integer.valueOf(6666);
        final IntegrationPointOfServiceDto dto = getBasePointOfServiceDto(storeNumber, true);

        dto.setType("TargetCountry");
        storeLocatorIntegrationFacade.persistPointOfService(dto);

        final TargetPointOfServiceModel tpos = targetPointOfServiceService.getPOSByStoreNumber(storeNumber);

        assertTrue("Can create Target Country stores", tpos.getType().getCode().equalsIgnoreCase("TargetCountry"));
    }

    @Test
    public void testCanRetrieveStoreByStoreNumber() {
        final Integer storeNumber = Integer.valueOf(6666);
        final IntegrationPointOfServiceDto dto = getBasePointOfServiceDto(storeNumber, true);
        storeLocatorIntegrationFacade.persistPointOfService(dto);

        final IntegrationPointOfServiceDto dto2 = storeLocatorIntegrationFacade.getPointOfService(storeNumber);

        assertTrue("Can retrieve PointOfService by storeNumber", dto.getStoreNumber().equals(dto2.getStoreNumber()));
    }

    @Test
    public void testCannotCreateOpeningScheduleForNonExistingStore() {
        final Calendar cal = Calendar.getInstance();

        final IntegrationOpeningScheduleDto openingSchedule = new IntegrationOpeningScheduleDto();
        final List<IntegrationOpeningDayDto> days = new ArrayList<>();
        // 7 Jan 2013 = Monday
        for (int i = 7; i < 14; i++) {
            final IntegrationOpeningDayDto openingDay = new IntegrationOpeningDayDto();
            openingDay.setClosed(Boolean.FALSE);
            cal.set(2013, Calendar.JANUARY, i, 8, 0);
            openingDay.setOpeningTime(cal.getTime());
            cal.set(2013, Calendar.JANUARY, i, 20, 0);
            openingDay.setClosingTime(cal.getTime());
            days.add(openingDay);
        }

        final IntegrationOpeningDayDto sod = new IntegrationOpeningDayDto();
        sod.setClosed(Boolean.TRUE);
        cal.set(2013, Calendar.JANUARY, 14);
        sod.setOpeningTime(cal.getTime());
        cal.set(2013, Calendar.JANUARY, 14);
        sod.setClosingTime(cal.getTime());
        days.add(sod);

        openingSchedule.setOpeningDays(days);

        final Integer storeNumber = Integer.valueOf(8888);
        final IntegrationPointOfServiceDto dto = new IntegrationPointOfServiceDto();
        dto.setHasOnlySchedule(true);
        dto.setStoreNumber(storeNumber);
        dto.setStoreName("Point Of Service " + storeNumber);

        dto.setOpeningSchedule(openingSchedule);

        final IntegrationResponseDto response = storeLocatorIntegrationFacade.persistPointOfService(dto);

        assertFalse("New PointOfService not created", response.isSuccessStatus());
        assertSame("Cannot create opening schedule for non-existent store",
                "Cannot create an OpeningSchedule for PointOfService that does not exist", response.getMessages()
                        .get(0));
    }

    @Test
    public void testPopulateOpeningSchedule() throws TargetAmbiguousIdentifierException {

        final Integer storeNumber = Integer.valueOf(7001);

        schedule = modelService.create(OpeningScheduleModel.class);
        schedule.setCode("testSchedule");
        schedule.setCreationtime(Calendar.getInstance().getTime());
        schedule.setName("Test Schedule");
        modelService.save(schedule);

        // Creating 3 TargetOpeningDay Models attached to the OpeningSchedule

        final Calendar cal = Calendar.getInstance();
        cal.set(2015, Calendar.JANUARY, 9, 8, 0, 0);
        final Date openingDay1OpeningTime = cal.getTime();
        cal.set(2015, Calendar.JANUARY, 9, 18, 0, 0);
        createOpeningDayModel(storeNumber, openingDay1OpeningTime, cal.getTime());
        cal.set(2015, Calendar.FEBRUARY, 9, 8, 0, 0);
        final Date openingDay2OpeningTime = cal.getTime();
        cal.set(2015, Calendar.FEBRUARY, 9, 18, 0, 0);
        createOpeningDayModel(storeNumber, openingDay2OpeningTime, cal.getTime());

        cal.set(2015, Calendar.MARCH, 9, 8, 0, 0);
        final Date openingDay3OpeningTime = cal.getTime();
        cal.set(2015, Calendar.MARCH, 9, 18, 0, 0);
        createOpeningDayModel(storeNumber, openingDay3OpeningTime, cal.getTime());

        // Creating OpeningScheduleDto
        final IntegrationOpeningScheduleDto scheduleDto = new IntegrationOpeningScheduleDto();
        scheduleDto.setCode("testSchedule");
        scheduleDto.setStoreNumber(storeNumber.toString());
        scheduleDto.setStoreName("Test Schedule");
        scheduleDto.setStoreNumber(storeNumber.toString());

        //Creating 2 OpeningDay Dtos - 1st Same as openingDay1, 2nd Updating openingDay2
        final List<IntegrationOpeningDayDto> openingDayDtos = new ArrayList<>();

        final IntegrationOpeningDayDto openingDayDto1 = new IntegrationOpeningDayDto();
        cal.set(2015, Calendar.JANUARY, 9, 8, 0, 0);
        openingDayDto1.setOpeningTime(cal.getTime());
        cal.set(2015, Calendar.JANUARY, 9, 18, 0, 0);
        openingDayDto1.setClosingTime(cal.getTime());
        openingDayDto1.setClosed(Boolean.FALSE);
        openingDayDtos.add(openingDayDto1);

        final IntegrationOpeningDayDto openingDayDto2 = new IntegrationOpeningDayDto();
        cal.set(2015, Calendar.FEBRUARY, 9, 8, 0, 0);
        openingDayDto2.setOpeningTime(cal.getTime());
        cal.set(2015, Calendar.FEBRUARY, 9, 19, 0, 0);
        openingDayDto2.setClosingTime(cal.getTime());
        openingDayDto2.setClosed(Boolean.FALSE);
        openingDayDtos.add(openingDayDto2);

        scheduleDto.setOpeningDays(openingDayDtos);

        storeLocatorIntegrationFacade.populateOpeningSchedule(schedule, scheduleDto);

        // 1st and 2nd OpeningDay Should be updated and 3rd should be removed
        TargetOpeningDayModel dayModel = getOpeningDayModelForOpeningTime(storeNumber, openingDay1OpeningTime);
        Assert.assertNotNull(dayModel);
        Assert.assertEquals(openingDayDto1.getOpeningTime(), dayModel.getOpeningTime());
        Assert.assertEquals(openingDayDto1.getClosingTime(), dayModel.getClosingTime());

        dayModel = getOpeningDayModelForOpeningTime(storeNumber, openingDay2OpeningTime);
        Assert.assertNotNull(dayModel);
        Assert.assertEquals(openingDayDto2.getOpeningTime(), dayModel.getOpeningTime());
        Assert.assertEquals(openingDayDto2.getClosingTime(), dayModel.getClosingTime());

        dayModel = getOpeningDayModelForOpeningTime(storeNumber, openingDay3OpeningTime);
        Assert.assertNull(dayModel);
    }

    /**
     * @param storeNumber
     * @param openingTime
     * @param closingTime
     */
    private void createOpeningDayModel(final Integer storeNumber, final Date openingTime,
            final Date closingTime) {
        final TargetOpeningDayModel openingDay = modelService.create(TargetOpeningDayModel.class);
        openingDay.setOpeningSchedule(schedule);
        openingDay.setOwner(schedule);

        openingDay.setOpeningTime(openingTime);
        openingDay.setTargetOpeningDayId(targetOpeningDayService.getTargetOpeningDayId(storeNumber.toString(),
                openingTime));
        openingDay.setClosingTime(closingTime);
        openingDay.setOpeningSchedule(schedule);
        final List<OpeningDayModel> openingDatList = (List<OpeningDayModel>)schedule.getOpeningDays();
        final List<OpeningDayModel> clonedOpeningDayList = new ArrayList<>();
        for (final OpeningDayModel model : openingDatList) {
            clonedOpeningDayList.add(model);
        }
        clonedOpeningDayList.add(openingDay);
        schedule.setOpeningDays(clonedOpeningDayList);

        modelService.save(openingDay);
        modelService.save(schedule);
    }

    /**
     * @param storeNumber
     * @param openingTime
     * @return TargetOpeningDayModel
     * @throws TargetAmbiguousIdentifierException
     */
    private TargetOpeningDayModel getOpeningDayModelForOpeningTime(final Integer storeNumber,
            final Date openingTime) throws TargetAmbiguousIdentifierException {
        final String targetOpeningDayId = targetOpeningDayService.getTargetOpeningDayId(storeNumber.toString(),
                openingTime);

        TargetOpeningDayModel dayModel = null;
        dayModel = targetOpeningDayService
                .getTargetOpeningDay(schedule, targetOpeningDayId);
        return dayModel;
    }

    private IntegrationPointOfServiceDto getBasePointOfServiceDto(final Integer storeNumber, final boolean addAddress) {

        final IntegrationPointOfServiceDto dto = new IntegrationPointOfServiceDto();

        dto.setStoreNumber(storeNumber);
        dto.setStoreName("Point Of Service " + storeNumber);
        dto.setDescription("Point Of Service " + storeNumber);
        dto.setType("Target");
        dto.setBaseStore("target");
        dto.setAcceptCNC(Boolean.TRUE);
        dto.setAcceptLayBy(Boolean.TRUE);
        dto.setClosed(Boolean.FALSE);
        dto.setLatitude(Double.valueOf(-35.123));
        dto.setLongitude(Double.valueOf(145.987));

        if (addAddress) {
            final IntegrationAddressDto address = new IntegrationAddressDto();
            address.setStreetname("123 Retail Ave");
            address.setDistrict("VIC");
            address.setTown("Geelong");
            address.setCountry("Australia");
            dto.setAddress(address);
        }

        return dto;
    }

    private void setContextUser() {
        userService.setCurrentUser(userService.getUserForUID("admin"));
    }
}
