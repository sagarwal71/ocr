/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.fest.assertions.Assertions;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtwsfacades.integration.dto.IntegrationCrossReferenceProduct;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductMediaDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductReferenceDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductReferencesDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationSecondaryImagesDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationVariantDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationWideImagesDto;
import au.com.target.tgtwsfacades.productimport.TargetProductImportConstants;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;


/**
 * @author fkratoch
 * 
 */
@IntegrationTest
public class TargetProductImportIntegrationFacadeTest extends ServicelayerTransactionalTest {

    private static final String BASE_PRODUCT_CODE = "P123456789";
    private static final String NEW_BASE_PRODUCT_CODE = "P987654321";
    private static final String BASE_PRODUCT_VARIANT_CODE = "V987654321";
    private static final String BASE_PRODUCT_NAME = "Test Product Name";
    private static final String SIZE_VARIANT_CODE_1 = "S11223344";
    private static final String SIZE_VARIANT_CODE_2 = "S55667788";

    private static final FastDateFormat FAST_DATE_FORMAT = FastDateFormat.getInstance("yyyy-MM-dd hh:mm:ss");
    @Resource
    protected TargetProductImportIntegrationFacade productImportIntegrationFacade;

    @Resource
    protected UserService userService;

    @Resource
    protected ProductService productService;

    @Resource
    protected ModelService modelService;

    @Resource
    protected ClassificationService classificationService;

    @Resource
    protected CatalogVersionService catalogVersionService;

    @Resource
    protected ConfigurationService configurationService;

    @Resource
    protected TargetStockService targetStockService;

    @Resource
    protected MediaService mediaService;

    @Resource
    protected TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Resource
    private TargetWarehouseService warehouseService;

    private final String homeDeliveryCode = "home-delivery";
    private final String clickAndCollectCode = "click-and-collect";
    private final String expressDeliveryCode = "express-delivery";
    private final String ebayExpressDeliveryCode = "ebay-express-delivery";
    private final String ebayHomeDeliveryCode = "eBay-delivery";
    private static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    @Before
    public void setupBefore() throws ImpExException {
        importCsv("/tgtcore/test/testSystemSetup.impex", Charsets.UTF_8.name());
        importCsv("/tgtwsfacades/test/testStepProductImport.impex", Charsets.UTF_8.name());
        importCsv("/tgtwsfacades/test/testDepartmentImport.impex", Charsets.UTF_8.name());
        importCsv("/tgtcore/test/testSizeOrderFeatureOff.impex", Charsets.UTF_8.name());
        importCsv("/tgtcore/test/testProductSize.impex", Charsets.UTF_8.name());
        userService.setCurrentUser(userService.getUserForUID("esb"));

        // For these tests then set the associate-media flag to true so we can test the media functions
        productImportIntegrationFacade.setAssociateMedia(true);
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectProductWithNoProductCode() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setProductCode(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create product without product code", response.isSuccessStatus());
        assertEquals("Cannot create product without product code",
                "One of required values is missing [Base Product Code, Variation Code, Product Name]", response
                        .getMessages()
                        .get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectProductWithNoName() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setName(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create product without product name", response.isSuccessStatus());
        assertEquals("Cannot create product without product name",
                "One of required values is missing [Base Product Code, Variation Code, Product Name]", response
                        .getMessages()
                        .get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectProductWithNoCategory() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setPrimaryCategory(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create product without primary category", response.isSuccessStatus());
        assertEquals(
                "Cannot create product without primary categore",
                "Missing PRIMARY CATEGORY (web storefront classification) for product, aborting product/variant import for: V987654321",
                response
                        .getMessages()
                        .get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectProductWithNonExistentCategory() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setPrimaryCategory("XYZ123");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Product category must exist", response.isSuccessStatus());
        assertEquals("Product category must exist",
                "Primary category XYZ123 does not exist in Hybris, aborting product/variant import for: V987654321",
                response.getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectProductWithNonExistentSecondaryCategory() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setSecondaryCategory(Collections.singletonList("XYZ123"));
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Product category must exist", response.isSuccessStatus());
        assertEquals(
                "Product category must exist",
                "Secondary category [XYZ123] does not exist in Hybris, aborting product/variant import for: V987654321",
                response.getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectProductWithNoBrand() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setBrand(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create product without brand", response.isSuccessStatus());
        assertEquals("Cannot create product without brand",
                "Missing product/variant BRAND (49135), aborting product/variant import for: V987654321", response
                        .getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectProductWithNoDepartment() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setDepartment(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create product without department", response.isSuccessStatus());
        assertEquals("Cannot create product without department",
                "Missing DEPARTMENT NUMBER (Attr_308459), aborting product/variant import for: V987654321", response
                        .getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectProductWithInvalidDepartment() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setDepartment(Integer.valueOf(777));
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create product without department", response.isSuccessStatus());
        assertEquals("Department 777 does not exists in hybris, aborting product/variant import for V987654321",
                response.getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectProductWithNoDescription() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setDescription(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create product without description", response.isSuccessStatus());
        assertEquals("Cannot create product without description",
                "Missing PRODUCT DESCRIPTION (49142), aborting product/variant import for: V987654321", response
                        .getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectProductWithNoApprovalStatus() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setApprovalStatus(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create product without approval status", response.isSuccessStatus());
        assertEquals("Cannot create product without approval status",
                "Missing product/variant APPROVAL STATUS (94267), aborting product/variant import for: V987654321",
                response.getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectVariationsWithNoApprovalStatus() {
        final IntegrationProductDto dto = createTestDto(true);
        dto.getVariants().get(0).setApprovalStatus(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create variant without approval status", response.isSuccessStatus());
        assertEquals("Cannot create variant without approval status",
                "Missing variant APPROVAL STATUS (94267), aborting product/variant import for: S11223344", response
                        .getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testCreateProductWithNoShowIfOutOfStockFlag() {
        final IntegrationProductDto dto = createTestDto(true);
        dto.setShowIfOutOfStock(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Create product without showIfOutOfStock flag", response.isSuccessStatus());
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectBulkyProductWithIncorrectProductType() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setProductType("normal");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create bulky product with incorrect product type", response.isSuccessStatus());
        assertEquals("Cannot create bulky product with incorrect product type",
                "Bulky product with incorrect product type, aborting product/variant import for: V987654321", response
                        .getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectBulkyProductWithIncorrectBulkyFlag() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setBulky("N");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create bulky product with incorrect product type", response.isSuccessStatus());
        assertEquals(
                "Cannot create bulky product with incorrect product type",
                "Bulky product type set for a bulky flag that is not Y, aborting product/variant import for: V987654321",
                response
                        .getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectBulkyProductWithEmptyBulkyFlag() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setBulky(StringUtils.EMPTY);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create bulky product with incorrect product type", response.isSuccessStatus());
        assertEquals(
                "Cannot create bulky product with incorrect product type",
                "Bulky product type set for a bulky flag that is not Y, aborting product/variant import for: V987654321",
                response
                        .getMessages().get(0));
    }

    @Test
    public void testRejectBulkyProductWithNullBulkyFlag() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setBulky(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create bulky product with incorrect product type", response.isSuccessStatus());
        assertEquals(
                "Cannot create bulky product with incorrect product type",
                "Bulky product type set for a bulky flag that is not Y, aborting product/variant import for: V987654321",
                response
                        .getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectBulkyProductWithUnknownProductType() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setProductType("abcdef");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create bulky product with incorrect product type", response.isSuccessStatus());
        assertEquals(
                "Cannot create bulky product with incorrect product type",
                "ProductType specified is not defined in hybris, aborting product/variant import for: V987654321",
                response
                        .getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectBulkyProductWithYBulkyFlagEmptyProductTypeNull() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setProductType(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create bulky product with incorrect product type", response.isSuccessStatus());
        assertEquals(
                "Cannot create bulky product with incorrect product type",
                "Bulky product with incorrect product type, aborting product/variant import for: V987654321",
                response
                        .getMessages().get(0));
    }

    @Test
    public void testRejectBulkyProductWithYBulkyFlagProductTypeEmpty() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setProductType(StringUtils.EMPTY);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create bulky product with incorrect product type", response.isSuccessStatus());
        assertEquals(
                "Cannot create bulky product with incorrect product type",
                "Bulky product with incorrect product type, aborting product/variant import for: V987654321",
                response
                        .getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectBulkyProductWithExpressDeliveryFlagSet() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setAvailableExpressDelivery("Y");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create bulky product with express-delivery enabled", response.isSuccessStatus());
        assertEquals("Cannot create bulky product with express-delivery enabled",
                "It is a bulky product with express-delivery enabled, aborting product/variant import for: "
                        + BASE_PRODUCT_VARIANT_CODE,
                response.getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectProductWithMissingLaybyFlag() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setAvailableLayby(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create product without layby flag", response.isSuccessStatus());
        assertEquals(
                "Cannot create product without layby flag",
                "Missing AVAILABLE FOR LAYBY flag (138956 - Toysale 7), aborting product/variant import for: V987654321",
                response.getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectProductWithMissingLongtermLaybyFlag() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setAvailableLongtermLayby(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create product without longterm layby flag", response.isSuccessStatus());
        assertEquals(
                "Cannot create product without longterm layby flag",
                "Missing AVAILABLE FOR LONGTERM LAYBY flag (Attr_311962), aborting product/variant import for: V987654321",
                response.getMessages().get(0));
    }

    @Test
    public void testRejectProductWithMissingMhdFlag() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setAvailableHomeDelivery(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create product without MHD flag", response.isSuccessStatus());
        assertEquals("Cannot create product without MHD flag",
                "Missing AVAILABLE FOR HOME DELIVERY flag (279381), aborting product/variant import for: V987654321",
                response.getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectSizeOnlyProductWithMissingSizeType() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setIsSizeOnly(Boolean.TRUE);
        dto.setSizeType(null);
        dto.setSize("10");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create product without size type when size variants exist", response.isSuccessStatus());
        assertEquals("Cannot create product without size type when size variants exist",
                "Missing SIZE TYPE definition (49129), aborting product/variant import for: V987654321", response
                        .getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectProductWithVariantsWithMissingSizeType() {
        final IntegrationProductDto dto = createTestDto(true);
        dto.setSizeType(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create product without size type when size variants exist", response.isSuccessStatus());
        assertEquals("Cannot create product without size type when size variants exist",
                "Missing SIZE TYPE definition (49129), aborting product/variant import for: V987654321", response
                        .getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectSizeOnlyProductWithoutSize() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setIsSizeOnly(Boolean.TRUE);

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create size only product without size", response.isSuccessStatus());
        assertEquals("Cannot create size only product without size",
                "Missing SIZE value (49128), aborting product/variant import for: V987654321", response
                        .getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectProductWithSingleSizeVariantWithoutSize() {
        final IntegrationProductDto dto = createTestDto(true);
        dto.getVariants().get(0).setSize(null);
        dto.getVariants().remove(1);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create product with size variant without size", response.isSuccessStatus());
        assertEquals("Cannot create product with size variant without size",
                "Missing SIZE value (49128) for size variant, aborting product/variant import for: S11223344",
                response.getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectNewProductWithApprovalStatusNotSourced() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setApprovalStatus("NOT SOURCED");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        // base product will always be approved, the real approval status will be set at variant level !

        assertFalse("Cannot create new product with 'NOT SOURCED' approval status", response.isSuccessStatus());
        assertEquals("Cannot create new product with 'NOT SOURCED' approval status",
                "'NOT SOURCED' product/variant status found, aborting product/variant import for: V987654321", response
                        .getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectProductWithOffileDateBeforeOnlineDate() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setProductOnlineDate("2000-10-10 08:00:00");
        dto.setProductOfflineDate("2000-10-01 08:00:00");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create product with off date before on date", response.isSuccessStatus());
        assertEquals("Cannot create product with off date before on date",
                "Product offline date is before online date, aborting product/variant import for: V987654321", response
                        .getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectProductWithOnlineOffileDateInWrongFormat() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setProductOnlineDate("incorrect format");
        dto.setProductOfflineDate("incorrect format");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create product with on / off date in wrong format", response.isSuccessStatus());
        assertEquals("Cannot create product with on / off date in wrong format",
                "Product online date has incorrect format, aborting product/variant import for: V987654321",
                response.getMessages().get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testCanRemoveLeadingZerosInProductCode() {
        final IntegrationProductDto dto = createTestDto(true);
        dto.setProductCode("00011223300");
        dto.setVariantCode("0099887700");
        dto.getVariants().get(0).setVariantCode("0033445500");
        dto.getVariants().get(0).setBaseProduct("00011223300");
        dto.getVariants().get(1).setVariantCode("0066778800");
        dto.getVariants().get(1).setBaseProduct("00011223300");

        assertEquals("Can remove leading zeros", "11223300", dto.getProductCode());
        assertEquals("Can remove leading zeros", "99887700", dto.getVariantCode());
        assertEquals("Can remove leading zeros", "33445500", dto.getVariants().get(0).getVariantCode());
        assertEquals("Can remove leading zeros", "11223300", dto.getVariants().get(0).getBaseProduct());
        assertEquals("Can remove leading zeros", "66778800", dto.getVariants().get(1).getVariantCode());
        assertEquals("Can remove leading zeros", "11223300", dto.getVariants().get(1).getBaseProduct());
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectNewProductWithTooManyMediaMissing() {
        final IntegrationProductDto dto = createTestDto(false);

        final IntegrationProductMediaDto media = new IntegrationProductMediaDto();
        media.setPrimaryImage("IMG999");
        final IntegrationSecondaryImagesDto secImgs = new IntegrationSecondaryImagesDto();
        secImgs.addSecondaryImage("IMG888");
        media.setSecondaryImages(secImgs);
        dto.setProductMedia(media);

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create new product with no media", response.isSuccessStatus());
        assertEquals("Cannot create new product with no medias", 8, response.getMessages().size());
        assertEquals("Cannot create new product with no medias",
                "ERR-PRODUCTMEDIA-MEDIANOTFOUND : The media with the qualifier /hero/IMG999 not found in hybris",
                response
                        .getMessages()
                        .get(0));
        assertEquals("Cannot create new product with no medias",
                "ERR-PRODUCTMEDIA-MEDIANOTFOUND : The media with the qualifier /thumb/IMG999 not found in hybris",
                response
                        .getMessages()
                        .get(1));
        assertEquals("Cannot create new product with no medias",
                "ERR-PRODUCTMEDIA-MEDIANOTFOUND : The media with the qualifier /large/IMG999 not found in hybris",
                response
                        .getMessages()
                        .get(2));
        assertEquals("Cannot create new product with no medias",
                "ERR-PRODUCTMEDIA-MEDIANOTFOUND : The media with the qualifier /list/IMG999 not found in hybris",
                response
                        .getMessages()
                        .get(3));
        assertEquals("Cannot create new product with no medias",
                "ERR-PRODUCTMEDIA-MEDIANOTFOUND : The media with the qualifier /grid/IMG999 not found in hybris",
                response
                        .getMessages()
                        .get(4));
        assertEquals("Cannot create new product with no medias",
                "ERR-PRODUCTMEDIA-MEDIANOTFOUND : The media with the qualifier /full/IMG999 not found in hybris",
                response
                        .getMessages()
                        .get(5));
        assertEquals(
                "Cannot create new product with no medias",
                "ERR-PRODUCTMEDIA-NOMEDIACONTAINER: No media container was found in hybris with the qualifier IMG999",
                response
                        .getMessages()
                        .get(6));
        assertEquals(
                "Cannot create new product with no medias",
                "ERR-PRODUCTMEDIA-NOMEDIACONTAINER: No media container was found in hybris with the qualifier IMG888",
                response
                        .getMessages()
                        .get(7));
    }

    /*
     * @Automated
     */
    @Test
    public void testRejectNewProductWithNoMedia() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setProductMedia(null);

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertFalse("Cannot create new product with no media", response.isSuccessStatus());
        assertEquals("Cannot create new product with no medias", 1, response.getMessages().size());
        assertEquals("Cannot create new product with no medias",
                "ERR-PRODUCTIMPORT-MEDIAMISSING : There was no media found on hybris for product with ID V987654321",
                response
                        .getMessages()
                        .get(0));
    }

    /*
     * @Automated
     */
    @Test
    public void testCanAssociateDepartment() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setSecondaryCategory(null);
        dto.setProductType("normal");
        dto.setBulky("N");
        dto.setDepartment(new Integer(160));
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());

            assertEquals("Can create new base product - product code", dto.getProductCode(), baseProduct.getCode());
            assertTrue(containsDepartment(baseProduct, "160"));

        }
    }

    /*
     * @Automated
     */
    @Test
    public void testCanAssignExpressDelivery() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setBulky("N");
        dto.setProductType("normal");
        dto.setDepartment(new Integer(160));

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());
        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());

            assertEquals("Can create new base product - product code", dto.getProductCode(), baseProduct.getCode());

            assertTrue(containsDepartment(baseProduct, "160"));
            // Use helper for delivery modes
            assertTrue("Can create new base product - available for HD",
                    containsDeliveryMode(baseProduct, expressDeliveryCode));
        }
    }

    /*
     * @Automated
     */
    @Test
    public void testCanAssignExpressDeliveryViaDtoWithFeatureOn() throws ImpExException {
        importCsv("/tgtwsfacades/test/testProductimportExpressbyflagFeatureOn.impex", Charsets.UTF_8.name());

        final IntegrationProductDto dto = createTestDto(false);
        dto.setBulky("N");
        dto.setProductType("normal");
        dto.setAvailableExpressDelivery("Y");

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());
        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());

            assertEquals("Can create new base product - product code", dto.getProductCode(), baseProduct.getCode());

            // Use helper for delivery modes
            assertTrue("Can create new base product - available for Express Delivery",
                    containsDeliveryMode(baseProduct, expressDeliveryCode));
        }
    }

    @Test
    public void testCanAssignExpressDeliveryWithEmptyBulkyAndProductType() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setBulky(StringUtils.EMPTY);
        dto.setProductType(StringUtils.EMPTY);
        dto.setDepartment(new Integer(160));

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());
        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());

            assertEquals("Can create new base product - product code", dto.getProductCode(), baseProduct.getCode());

            assertTrue(containsDepartment(baseProduct, "160"));
            // Use helper for delivery modes
            assertTrue("Can create new base product - available for HD",
                    containsDeliveryMode(baseProduct, expressDeliveryCode));
        }
    }

    @Test
    public void testCanAssignExpressDeliveryWithNullBulkyAndProductType() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setBulky(null);
        dto.setProductType(null);
        dto.setDepartment(new Integer(160));

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());
        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());

            assertEquals("Can create new base product - product code", dto.getProductCode(), baseProduct.getCode());

            assertTrue(containsDepartment(baseProduct, "160"));
            // Use helper for delivery modes
            assertTrue("Can create new base product - available for HD",
                    containsDeliveryMode(baseProduct, expressDeliveryCode));
        }
    }

    /*
     * @Automated
     */
    @Test
    public void testNotAssignExpressDeliveryBulky() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setBulky("Y");
        dto.setProductType("bulky2");
        dto.setDepartment(new Integer(160));

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());
        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());

            assertEquals("Can create new base product - product code", dto.getProductCode(), baseProduct.getCode());
            assertTrue(containsDepartment(baseProduct, "160"));
            // Use helper for delivery modes
            assertTrue("Can create new base product - available for Express Delivery",
                    !containsDeliveryMode(baseProduct, expressDeliveryCode));
        }
    }

    /*
     * @Automated
     */
    @Test
    public void testNotAssignExpDeliveryIneligibleDepartment() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setBulky("N");
        dto.setProductType("normal");
        dto.setDepartment(new Integer(300));

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());
        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            assertEquals("Can create new base product - product code", dto.getProductCode(), baseProduct.getCode());
            assertTrue(containsDepartment(baseProduct, "300"));
            // Use helper for delivery modes
            assertTrue("Can create new base product - available for HD",
                    !containsDeliveryMode(baseProduct, expressDeliveryCode));
        }
    }

    /*
     * @Automated
     */
    @Test
    public void testCanCreateNewBaseProduct() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setSecondaryCategory(null);
        dto.setProductType("bulky2");
        dto.setMaterials("Sample product materials");
        dto.setCareInstructions("Sample care instructions for product");
        dto.setWebExtraInfo("Sample web extra info for product");
        dto.setWebFeatures("Sample web web features for product");
        dto.setYoutube("http://youtu.be/ABC123");
        dto.setLicense("Disney");
        dto.getGenders().add("Boy");
        dto.setAgeFrom(Double.valueOf(1));

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());

            assertEquals("Can create new base product - product code", dto.getProductCode(), baseProduct.getCode());
            assertEquals("Can create new base product - product name", dto.getName(), baseProduct.getName());
            assertEquals("Can create new base product - approval status", ArticleApprovalStatus.APPROVED,
                    baseProduct.getApprovalStatus());
            assertEquals("Can create new base product - brand", dto.getBrand().toLowerCase(), baseProduct.getBrand()
                    .getCode());
            assertEquals("Can create new base product - product type", dto.getProductType(), baseProduct
                    .getProductType()
                    .getCode());
            assertEquals("Can create new base product - primary category", dto.getPrimaryCategory(), baseProduct
                    .getPrimarySuperCategory().getCode());
            assertEquals("Can create new base product - description", dto.getDescription(),
                    baseProduct.getDescription());
            assertEquals("Can create new base product - materials", dto.getMaterials(), baseProduct.getMaterials());
            assertEquals("Can create new base product - care instructions", dto.getCareInstructions(),
                    baseProduct.getCareInstructions());
            assertEquals("Can create new base product - product features", dto.getWebFeatures(),
                    baseProduct.getProductFeatures());

            // Use helper for delivery modes
            assertTrue("Can create new base product - available for CNC",
                    containsDeliveryMode(baseProduct, clickAndCollectCode));
            assertTrue("Can create new base product - available for HD",
                    containsDeliveryMode(baseProduct, homeDeliveryCode));

            //to-do: extra info
            //assertEquals("Can create new base product - extra info", dto.getWebExtraInfo(), baseProduct.getProductFeatures());

            final MediaModel youtube = baseProduct.getVideos().iterator().next();
            assertEquals("Can create new base product - youtube video", "http://www.youtube.com/embed/ABC123?rel=0",
                    youtube.getURL());

            verifyProductFeatures(dto, baseProduct);
        }
    }

    /*
     * @Automated
     */
    @Test
    public void testAssignDeliveryModeHDOnly() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setAvailableHomeDelivery("Y");
        dto.setAvailableCnc("N");
        dto.setDepartment(Integer.valueOf(221)); // Dept not eligible for Express Delivery

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Cannot create new base product", response.isSuccessStatus());

        final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                dto.getProductCode());
        assertEquals(1, baseProduct.getDeliveryModes().size());
        assertTrue("Does not have the required delivery mode -Home delivery ",
                containsDeliveryMode(baseProduct, homeDeliveryCode));
    }

    /*
     * @Automated
     */
    @Test
    public void testAssignDeliveryModeEBayExpressDeliveryOnly() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setAvailableHomeDelivery("N");
        dto.setAvailableCnc("N");
        dto.setAvailableOnEbay("Y");
        dto.setAvailableEbayExpressDelivery("Y");
        dto.setDepartment(Integer.valueOf(221)); // Dept not eligible for Express Delivery

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Cannot create new base product", response.isSuccessStatus());

        final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                dto.getProductCode());
        assertEquals(2, baseProduct.getDeliveryModes().size());
        assertTrue("Can create new base product - available for ebay Home Delivery",
                containsDeliveryMode(baseProduct, ebayHomeDeliveryCode));
        assertTrue("Can create new base product - available for ebay Express Delivery",
                containsDeliveryMode(baseProduct, ebayExpressDeliveryCode));
    }

    /*
     * @Automated
     */
    @Test
    public void testAssignDeliveryModeEBayHomeDeliveryOnly() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setAvailableHomeDelivery("N");
        dto.setAvailableCnc("N");
        dto.setAvailableOnEbay("Y");
        dto.setAvailableEbayExpressDelivery("N");
        dto.setDepartment(Integer.valueOf(221)); // Dept not eligible for Express Delivery

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Cannot create new base product", response.isSuccessStatus());

        final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                dto.getProductCode());
        assertEquals(1, baseProduct.getDeliveryModes().size());
        assertTrue("Can create new base product - available for ebay Home Delivery",
                containsDeliveryMode(baseProduct, ebayHomeDeliveryCode));
    }

    /*
     * @Automated
     */
    @Test
    public void testAssignDeliveryModeHomeDeliveryOnlyNotAvailableOnEbay() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setAvailableHomeDelivery("Y");
        dto.setAvailableCnc("N");
        dto.setAvailableOnEbay("N");
        dto.setAvailableEbayExpressDelivery("Y");
        dto.setDepartment(Integer.valueOf(221)); // Dept not eligible for Express Delivery

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Cannot create new base product", response.isSuccessStatus());

        final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                dto.getProductCode());
        assertEquals(1, baseProduct.getDeliveryModes().size());
        assertFalse("Can create new base product - available for ebay Home Delivery",
                containsDeliveryMode(baseProduct, ebayHomeDeliveryCode));
        assertFalse("Can create new base product - available for ebay Express Delivery",
                containsDeliveryMode(baseProduct, ebayExpressDeliveryCode));
    }

    /*
     * @Automated
     */
    @Test
    public void testAssignDeliveryModeHDAndCNCDeliveryOnlyNotAvailableOnEbay() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setAvailableHomeDelivery("Y");
        dto.setAvailableCnc("Y");
        dto.setAvailableOnEbay("N");
        dto.setAvailableEbayExpressDelivery("Y");
        dto.setDepartment(Integer.valueOf(221)); // Dept not eligible for Express Delivery

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Cannot create new base product", response.isSuccessStatus());

        final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                dto.getProductCode());
        assertEquals(2, baseProduct.getDeliveryModes().size());
        assertFalse("Can create new base product - available for ebay Home Delivery",
                containsDeliveryMode(baseProduct, ebayHomeDeliveryCode));
        assertFalse("Can create new base product - available for ebay Express Delivery",
                containsDeliveryMode(baseProduct, ebayExpressDeliveryCode));
    }

    @Test
    public void testAssignDeliveryModeEBayHomeDeliveryOnlyNotAvailableOnEbay() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setAvailableHomeDelivery("Y");
        dto.setAvailableCnc("N");
        dto.setAvailableOnEbay("N");
        dto.setAvailableEbayExpressDelivery("N");
        dto.setDepartment(Integer.valueOf(221)); // Dept not eligible for Express Delivery

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Cannot create new base product", response.isSuccessStatus());

        final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                dto.getProductCode());
        assertEquals(1, baseProduct.getDeliveryModes().size());
        assertFalse("Can create new base product - available for ebay Home Delivery",
                containsDeliveryMode(baseProduct, ebayHomeDeliveryCode));
    }

    @Test
    public void testAssignDeliveryModeHDAndCNC() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setAvailableHomeDelivery("Y");
        dto.setAvailableCnc("Y");
        dto.setDepartment(Integer.valueOf(221)); // Dept not eligible for Express Delivery

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Cannot create new base product", response.isSuccessStatus());

        final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                dto.getProductCode());
        assertEquals(2, baseProduct.getDeliveryModes().size());
        assertTrue("Does not have the required delivery mode - Home delivery ",
                containsDeliveryMode(baseProduct, homeDeliveryCode));
        assertTrue("Does not have the required delivery mode - Click N Collect ",
                containsDeliveryMode(baseProduct, clickAndCollectCode));
    }

    private boolean containsDeliveryMode(final TargetProductModel targetProductModel, final String deliveryModecode) {

        final Set<DeliveryModeModel> deliveryModes = targetProductModel.getDeliveryModes();
        if (CollectionUtils.isEmpty(deliveryModes)) {
            return false;
        }

        for (final DeliveryModeModel deliveryMode : deliveryModes) {
            if (deliveryModecode.equals(deliveryMode.getCode())) {
                return true;
            }
        }

        return false;
    }


    public boolean containsDepartment(final TargetProductModel targetProductModel, final String department) {
        TargetMerchDepartmentModel targetMerchDepartmentModel = null;
        for (final CategoryModel categoryModel : targetProductModel.getSupercategories()) {
            if (categoryModel instanceof TargetMerchDepartmentModel) {
                targetMerchDepartmentModel = (TargetMerchDepartmentModel)categoryModel;
                if (department.equals(targetMerchDepartmentModel.getCode())) {
                    return true;
                }
            }
        }
        return false;
    }

    @Test
    public void testCanCreateNewColourVariant() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setColour("Blue");
        dto.setSwatch("Sky Blue");
        dto.setArticleStatus("targetexclusive");

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new colour variant", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());

            assertEquals("Can create new colour variant - base product", dto.getProductCode(),
                    colourVariant.getBaseProduct().getCode());
            assertEquals("Can create new colour variant - variant code", dto.getVariantCode(), colourVariant.getCode());
            assertEquals("Can create new colour variant - variant name", dto.getName(), colourVariant.getName());
            assertEquals("Can create new colour variant - variant colour", dto.getColour().toLowerCase(),
                    colourVariant.getColourName().toLowerCase());
            assertEquals("Can create new colour variant - variant swatch", dto.getSwatch().toLowerCase(),
                    colourVariant.getSwatchName().toLowerCase());
            final Collection<StockLevelModel> stockLevels = targetStockService.getAllStockLevels(colourVariant);
            Assert.assertTrue(CollectionUtils.isNotEmpty(stockLevels));
            Assert.assertTrue(stockLevels.size() == 1);
        }
    }

    /**
     * test teh different article status.
     */
    @Test
    public void testArticleStatusTargetExNChangeToNull() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setColour("Blue");
        dto.setSwatch("Sky Blue");
        dto.setArticleStatus(TargetProductImportConstants.ARTICLE_STATUS_TARGETEXCLUSIVE);

        IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new colour variant", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());

            assertTrue("Assert that article status is targetExclusive", colourVariant.getTargetExclusive()
                    .booleanValue());
            assertFalse(colourVariant.getHotProduct().booleanValue());
            assertFalse(colourVariant.getClearance().booleanValue());
            assertFalse(colourVariant.getEssential().booleanValue());
            assertFalse(colourVariant.getOnlineExclusive().booleanValue());
        }

        dto.setArticleStatus(null);
        response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new colour variant", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());

            assertFalse("Assert that article status is targetExclusive", colourVariant.getTargetExclusive()
                    .booleanValue());
            assertFalse(colourVariant.getHotProduct().booleanValue());
            assertFalse(colourVariant.getClearance().booleanValue());
            assertFalse(colourVariant.getEssential().booleanValue());
            assertFalse(colourVariant.getOnlineExclusive().booleanValue());
        }

        dto.setArticleStatus(TargetProductImportConstants.ARTICLE_STATUS_CLEARANCE);
        response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new colour variant", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());

            assertFalse("Assert that article status is targetExclusive", colourVariant.getTargetExclusive()
                    .booleanValue());
            assertFalse(colourVariant.getHotProduct().booleanValue());
            assertTrue(colourVariant.getClearance().booleanValue());
            assertFalse(colourVariant.getEssential().booleanValue());
            assertFalse(colourVariant.getOnlineExclusive().booleanValue());
        }
    }

    /**
     * test teh different article status.
     */
    @Test
    public void testArticleStatusTargetExNChangeToEmptyString() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setColour("Blue");
        dto.setSwatch("Sky Blue");
        dto.setArticleStatus(TargetProductImportConstants.ARTICLE_STATUS_ESSENTIALS);

        IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new colour variant", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());

            assertFalse("Assert that article status is targetExclusive", colourVariant.getTargetExclusive()
                    .booleanValue());
            assertFalse(colourVariant.getHotProduct().booleanValue());
            assertFalse(colourVariant.getClearance().booleanValue());
            assertTrue(colourVariant.getEssential().booleanValue());
            assertFalse(colourVariant.getOnlineExclusive().booleanValue());
        }

        dto.setArticleStatus(StringUtils.EMPTY);
        response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new colour variant", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());

            assertFalse("Assert that article status is targetExclusive", colourVariant.getTargetExclusive()
                    .booleanValue());
            assertFalse(colourVariant.getHotProduct().booleanValue());
            assertFalse(colourVariant.getClearance().booleanValue());
            assertFalse(colourVariant.getEssential().booleanValue());
            assertFalse(colourVariant.getOnlineExclusive().booleanValue());
        }

        dto.setArticleStatus(TargetProductImportConstants.ARTICLE_STATUS_HOTPRODUCT);
        response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new colour variant", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());

            assertFalse("Assert that article status is targetExclusive", colourVariant.getTargetExclusive()
                    .booleanValue());
            assertTrue(colourVariant.getHotProduct().booleanValue());
            assertFalse(colourVariant.getClearance().booleanValue());
            assertFalse(colourVariant.getEssential().booleanValue());
            assertFalse(colourVariant.getOnlineExclusive().booleanValue());
        }
    }

    /**
     * test teh different article status.
     */
    @Test
    public void testArticleStatusOnlineExNChangeToNone() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setColour("Blue");
        dto.setSwatch("Sky Blue");
        dto.setArticleStatus(TargetProductImportConstants.ARTICLE_STATUS_ONLINEEXCLUSIVE);

        IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new colour variant", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());

            assertFalse("Assert that article status is targetExclusive", colourVariant.getTargetExclusive()
                    .booleanValue());
            assertFalse(colourVariant.getHotProduct().booleanValue());
            assertFalse(colourVariant.getClearance().booleanValue());
            assertFalse(colourVariant.getEssential().booleanValue());
            assertTrue(colourVariant.getOnlineExclusive().booleanValue());
        }

        dto.setArticleStatus("none");
        response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new colour variant", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());

            assertFalse("Assert that article status is targetExclusive", colourVariant.getTargetExclusive()
                    .booleanValue());
            assertFalse(colourVariant.getHotProduct().booleanValue());
            assertFalse(colourVariant.getClearance().booleanValue());
            assertFalse(colourVariant.getEssential().booleanValue());
            assertFalse(colourVariant.getOnlineExclusive().booleanValue());
        }

        dto.setArticleStatus(TargetProductImportConstants.ARTICLE_STATUS_CLEARANCE);
        response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new colour variant", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());

            assertFalse("Assert that article status is targetExclusive", colourVariant.getTargetExclusive()
                    .booleanValue());
            assertFalse(colourVariant.getHotProduct().booleanValue());
            assertTrue(colourVariant.getClearance().booleanValue());
            assertFalse(colourVariant.getEssential().booleanValue());
            assertFalse(colourVariant.getOnlineExclusive().booleanValue());
        }
    }

    @Test
    public void testCanCreateNewSizeVariant() {
        final IntegrationProductDto dto = createTestDto(true);

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new size variant", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());
            final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)productService
                    .getProductForCode(dto.getVariants().get(0).getVariantCode());

            // colour variant is the parent of size variants
            assertEquals("Can create new size variant - base product", dto.getVariantCode(), sizeVariant
                    .getBaseProduct()
                    .getCode());
            assertEquals("Can create new size variant - no color", "nocolour", colourVariant.getColour().getCode());
            assertEquals("Can create new size variant - no color", ArticleApprovalStatus.APPROVED,
                    colourVariant.getApprovalStatus());
            assertEquals("Can create new size variant - size type", dto.getSizeType().toLowerCase(), sizeVariant
                    .getSizeType().toLowerCase());
            assertEquals("Can create new size variant - size", dto.getSizeType().toLowerCase(), sizeVariant
                    .getSizeType()
                    .toLowerCase());
            final Collection<StockLevelModel> stockLevels = targetStockService.getAllStockLevels(sizeVariant);
            Assert.assertTrue(CollectionUtils.isNotEmpty(stockLevels));
            Assert.assertTrue(stockLevels.size() == 1);
        }
    }

    @Test
    public void testCanCreateNewSizeOnlyVariant() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setIsSizeOnly(Boolean.TRUE);
        dto.setSize("10");

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new size variant", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final String colourVariantCode = dto.getProductCode().substring(1) + "_nocolour";
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                    .getProductForCode(colourVariantCode);
            final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());

            assertEquals("Can create new size variant - no color", "nocolour", colourVariant.getColour().getCode());
            assertEquals("Can create new size variant - no color", ArticleApprovalStatus.APPROVED,
                    colourVariant.getApprovalStatus());
            assertEquals("Can create new size variant - size type", dto.getSizeType().toLowerCase(), sizeVariant
                    .getSizeType().toLowerCase());
            assertEquals("Can create new size variant - size", dto.getSizeType().toLowerCase(), sizeVariant
                    .getSizeType()
                    .toLowerCase());
            final Collection<StockLevelModel> stockLevels = targetStockService.getAllStockLevels(sizeVariant);
            Assert.assertTrue(CollectionUtils.isNotEmpty(stockLevels));
            Assert.assertTrue(stockLevels.size() == 1);
        }
    }

    @Test
    public void testCanSetCorrectNewSizeVariantName() {
        final IntegrationProductDto dto = createTestDto(true);
        dto.getVariants().get(0).setName("Size variant one");

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can set correct size variant name", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetSizeVariantProductModel sizeVariant1 = (TargetSizeVariantProductModel)productService
                    .getProductForCode(dto.getVariants().get(0).getVariantCode());
            final TargetSizeVariantProductModel sizeVariant2 = (TargetSizeVariantProductModel)productService
                    .getProductForCode(dto.getVariants().get(1).getVariantCode());

            assertEquals("Can set correct size variant name - overriden",
                    dto.getVariants().get(0).getName(),
                    sizeVariant1.getName());
            assertEquals("Can set correct size variant name - inherited",
                    dto.getName(),
                    sizeVariant2.getName());
        }
    }

    @Test
    public void testCanSetCorrectNewSizeVariantNameForSizeOnly() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setIsSizeOnly(Boolean.TRUE);
        dto.setSizeType("Size Type");
        dto.setSize("10");

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can set correct size variant name for size only product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());

            assertEquals("Can set correct size variant name for size only product - overriden",
                    dto.getName(),
                    sizeVariant.getName());
        }
    }

    @Test
    public void testCanCreateNewProductWithJustPrimaryImage() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.getProductMedia().setSecondaryImages(null);

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new product with just primary image", response.isSuccessStatus());
        assertTrue("Can create new product with just primary image - no error messages",
                response.getMessages().size() == 0);
    }

    @Test
    public void testCanCreateNewProductWithWideImages() {
        final IntegrationProductDto dto = createTestDto(false);

        final IntegrationWideImagesDto wideImgs = new IntegrationWideImagesDto();
        wideImgs.addWideImage("IMG-WIDE-001");
        wideImgs.addWideImage("IMG-WIDE-002");
        dto.getProductMedia().setWideImages(wideImgs);

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertThat(response.isSuccessStatus()).as("Can create new product with wide images").isTrue();
        assertThat(response.getMessages()).as("Can create new product with wide images - no error messages")
                .hasSize(0);

    }

    @Test
    public void testCanUpdateExistingBaseProduct() {
        final IntegrationProductDto dto = createTestDto(false);
        final IntegrationResponseDto response1 = productImportIntegrationFacade.persistTargetProduct(dto);
        final TargetProductModel product1 = (TargetProductModel)productService.getProductForCode(dto.getProductCode());

        // Check new base product
        assertTrue("Can update existing base product", response1.isSuccessStatus());
        assertEquals("Can update existing base product - approval status", ArticleApprovalStatus.APPROVED,
                product1.getApprovalStatus());
        assertEquals("Can update existing base product - product name", dto.getName(), product1.getName());
        assertEquals("Can update existing base product - product description", dto.getDescription(),
                product1.getDescription());
        assertEquals("Cotton", product1.getMaterial());
        assertEquals("Trendy", product1.getTrend());
        assertEquals("Him", product1.getGiftsFor());
        assertEquals("Online", product1.getPlatform());
        assertEquals("Retro", product1.getGenre());
        assertEquals("Spring", product1.getSeason());
        assertEquals("Type", product1.getType());
        assertEquals("Christmas", product1.getOccassion());

        // Update product
        dto.setName("Updated test Product Name");
        dto.setDescription("Updated test product description");
        dto.setApprovalStatus("Inactive");
        final IntegrationResponseDto response2 = productImportIntegrationFacade.persistTargetProduct(dto);
        final TargetProductModel product2 = (TargetProductModel)productService.getProductForCode(dto.getProductCode());

        // Check updated base product
        assertTrue("Can update existing base product", response2.isSuccessStatus());
        assertEquals("Can update existing base product - updated product status", ArticleApprovalStatus.APPROVED,
                product2.getApprovalStatus());
        assertEquals("Can update existing base product - updated product name", dto.getName(), product2.getName());
        assertEquals("Can update existing base product - updated product description", dto.getDescription(),
                product2.getDescription());
    }

    @Test
    public void testCanUpdateExistingColourVariant() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setColour("Red Colour");
        dto.setSwatch("Red Swatch");
        final IntegrationResponseDto response1 = productImportIntegrationFacade.persistTargetProduct(dto);
        final TargetColourVariantProductModel variant1 = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        // check new colour variant
        assertTrue("Can update existing colour variant", response1.isSuccessStatus());
        assertEquals("Can update existing colour variant - variant status", ArticleApprovalStatus.APPROVED,
                variant1.getApprovalStatus());
        assertEquals("Can update existing colour variant - variant colour", "Red Colour", variant1.getColourName());
        assertEquals("Can update existing colour variant - variant swatch", "Red Swatch", variant1.getSwatchName());

        // Update variant - updating colour will create new Colour Variant !!!
        dto.setApprovalStatus("Inactive");
        final IntegrationResponseDto response2 = productImportIntegrationFacade.persistTargetProduct(dto);
        final TargetColourVariantProductModel variant2 = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        // Check updated variant
        assertTrue("Can update existing colour variant", response2.isSuccessStatus());
        assertEquals("Can update existing colour variant - updated variant status", ArticleApprovalStatus.UNAPPROVED,
                variant2.getApprovalStatus());
        assertEquals("Can update existing colour variant - variant colour", "Red Colour", variant1.getColourName());
        assertEquals("Can update existing colour variant - variant swatch", "Red Swatch", variant1.getSwatchName());
    }

    @Test
    public void testCanUpdateExistingColourVariantInitiallyInStyleGroupButNowWithNewBaseProductNotUnderStylegroup()
            throws ParseException {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setColour("Red Colour");
        dto.setSwatch("Red Swatch");
        final IntegrationResponseDto response1 = productImportIntegrationFacade.persistTargetProduct(dto);
        final TargetColourVariantProductModel variant1 = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        // check new colour variant
        assertTrue("Updated to new base product", BASE_PRODUCT_CODE.equals(variant1.getBaseProduct().getCode()));
        assertTrue("Can update existing colour variant", response1.isSuccessStatus());
        assertEquals("Can update existing colour variant - variant status", ArticleApprovalStatus.APPROVED,
                variant1.getApprovalStatus());
        assertEquals("Can update existing colour variant - variant colour", "Red Colour", variant1.getColourName());
        assertEquals("Can update existing colour variant - variant swatch", "Red Swatch", variant1.getSwatchName());

        // Update variant - updating colour will create new Colour Variant !!!
        dto.setApprovalStatus("Inactive");
        dto.setIsStylegroup(Boolean.FALSE);
        dto.setProductCode(NEW_BASE_PRODUCT_CODE);
        final IntegrationResponseDto response2 = productImportIntegrationFacade.persistTargetProduct(dto);
        final TargetColourVariantProductModel variant2 = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        // Check updated variant
        assertTrue("Updated to new base product", NEW_BASE_PRODUCT_CODE.equals(variant2.getBaseProduct().getCode()));
        assertTrue("Can update existing colour variant", response2.isSuccessStatus());
        assertEquals("Can update existing colour variant - updated variant status", ArticleApprovalStatus.UNAPPROVED,
                variant2.getApprovalStatus());
        assertEquals("Can update existing colour variant - variant colour", "Red Colour", variant1.getColourName());
        assertEquals("Can update existing colour variant - variant swatch", "Red Swatch", variant1.getSwatchName());
    }

    @Test
    public void testCanUpdateExistingColourVariantWithPreOrder() throws ParseException {
        final IntegrationProductDto dto = createTestDto(false, true);
        final IntegrationResponseDto response1 = productImportIntegrationFacade.persistTargetProduct(dto);
        final TargetColourVariantProductModel variant1 = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        assertThat(response1.isSuccessStatus()).isTrue();
        // check new colour variant
        final StockLevelModel stockLevelModel = targetStockService.checkAndGetStockLevel(variant1,
                warehouseService.getDefaultOnlineWarehouse());
        assertThat(stockLevelModel.getMaxPreOrder()).isEqualTo(dto.getPreOrderOnlineQuantity().intValue());
        assertThat(variant1.getPreOrderEndDateTime())
                .isEqualTo(new SimpleDateFormat(DATETIME_FORMAT).parse(dto.getPreOrderEndDate()));
        assertThat(variant1.getPreOrderStartDateTime())
                .isEqualTo(new SimpleDateFormat(DATETIME_FORMAT).parse(dto.getPreOrderStartDate()));
        assertThat(variant1.getNormalSaleStartDateTime())
                .isEqualTo(new SimpleDateFormat(DATETIME_FORMAT).parse(dto.getPreOrderEmbargoReleaseDate()));
    }

    @Test
    public void testCanUpdateProductStatusToChecked() {
        final IntegrationProductDto dto = createTestDto(false);
        final IntegrationResponseDto response1 = productImportIntegrationFacade.persistTargetProduct(dto);
        final TargetColourVariantProductModel variant1 = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        // Check new base product
        assertTrue("Can update existing base product", response1.isSuccessStatus());
        assertEquals("Can update existing base product - approval status", ArticleApprovalStatus.APPROVED,
                variant1.getApprovalStatus());

        // update status
        dto.setApprovalStatus("Not Sourced");
        final IntegrationResponseDto response2 = productImportIntegrationFacade.persistTargetProduct(dto);
        final TargetColourVariantProductModel variant2 = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        assertTrue("Can update existing base product", response2.isSuccessStatus());
        assertEquals("Can update existing base product - approval status", ArticleApprovalStatus.CHECK,
                variant2.getApprovalStatus());
    }

    @Test
    public void testCanCreateProductReferences() {
        // cross references are attached to colour variants
        final IntegrationProductDto dto1 = createTestDto(false); // V987654321
        final IntegrationResponseDto response1 = productImportIntegrationFacade.persistTargetProduct(dto1);

        final IntegrationProductDto dto2 = createTestDto(false);
        // second product must have either another base product or another colour, otherwise they get merged.
        dto2.setProductCode("P123456788");
        dto2.setVariantCode("V998877665");
        final IntegrationResponseDto response2 = productImportIntegrationFacade.persistTargetProduct(dto2);

        assertTrue("Can create cross references - create product1", response1.isSuccessStatus());
        assertTrue("Can create cross references - create product2", response2.isSuccessStatus());

        final IntegrationCrossReferenceProduct crossRef = new IntegrationCrossReferenceProduct();
        crossRef.addRefProduct("V998877665");

        final IntegrationProductReferenceDto ref = new IntegrationProductReferenceDto();
        ref.setCode(BASE_PRODUCT_VARIANT_CODE);
        ref.setRefProducts(crossRef);

        final IntegrationProductReferencesDto dto = new IntegrationProductReferencesDto();
        dto.getStepProducts().add(ref);

        final IntegrationResponseDto response3 = productImportIntegrationFacade.generateProductCrossReferences(dto);
        final TargetColourVariantProductModel variant = (TargetColourVariantProductModel)productService
                .getProductForCode(dto1.getVariantCode());

        assertTrue("Can create cross references - create reference", response3.isSuccessStatus());
        assertTrue("Can create cross references - reference exists", variant.getProductReferences().size() == 1);

        final ProductReferenceModel[] refs = variant.getProductReferences().toArray(
                new ProductReferenceModel[variant.getProductReferences().size()]);
        assertEquals("Can create cross references - source code", dto1.getVariantCode(), refs[0].getSource().getCode());
        assertEquals("Can create cross references - target code", dto2.getVariantCode(), refs[0].getTarget().getCode());
    }

    @Test
    public void testCanMergeSameColourVariant() {
        final IntegrationProductDto dto1 = createTestDto(true);// dto1 contains sizes 10 and 11
        dto1.setColour("Red");
        dto1.setSwatch("Crimson Red");

        final IntegrationResponseDto response1 = productImportIntegrationFacade.persistTargetProduct(dto1);
        final TargetColourVariantProductModel variant1 = (TargetColourVariantProductModel)productService
                .getProductForCode(dto1.getVariantCode());
        assertTrue("Can merge colour variants - created variant", response1.isSuccessStatus());
        assertEquals("Can merge size variants - correct colour variant code", dto1.getVariantCode(),
                variant1.getCode());

        final IntegrationProductDto dto2 = createTestDto(false);
        dto2.setVariantCode("V987654322"); // different code
        dto2.setColour("Red");
        dto2.setSwatch("Crimson Red");// same swatch colour

        final IntegrationVariantDto varDto1 = new IntegrationVariantDto();
        varDto1.setVariantCode("S44332211");
        varDto1.setBaseProduct(BASE_PRODUCT_CODE);
        varDto1.setApprovalStatus("Active");
        varDto1.setSize("12");

        final IntegrationVariantDto varDto2 = new IntegrationVariantDto();
        varDto2.setVariantCode("S88775566");
        varDto2.setBaseProduct(BASE_PRODUCT_CODE);
        varDto2.setApprovalStatus("Active");
        varDto2.setSize("13");

        dto2.getVariants().add(varDto1);
        dto2.getVariants().add(varDto2);

        final IntegrationResponseDto response2 = productImportIntegrationFacade.persistTargetProduct(dto2);
        final TargetColourVariantProductModel variant2 = (TargetColourVariantProductModel)productService
                .getProductForCode(dto1.getVariantCode());

        assertTrue("Can merge colour variants  - merged variant", response2.isSuccessStatus());
        assertEquals("Can merge colour variants  - correct variant code", dto1.getVariantCode(), variant2.getCode());
        assertEquals("Can merge colour variants - correct number of size variants", 4, variant2.getVariants().size());
    }

    @Test(expected = UnknownIdentifierException.class)
    public void testOnlyOneColourVariantCreatedForSameBaseProductSameSwatchColourCombination() {
        final IntegrationProductDto dto1 = createTestDto(true); // dto1 contains sizes 10 and 11
        dto1.setColour("Red");
        dto1.setSwatch("Crimson Red");
        final IntegrationResponseDto response1 = productImportIntegrationFacade.persistTargetProduct(dto1);

        final IntegrationProductDto dto2 = createTestDto(false);
        dto2.setVariantCode("V987654322"); // different variant code
        dto2.setColour("Red");
        dto2.setSwatch("Crimson Red");// same swatch colour

        final IntegrationVariantDto varDto1 = new IntegrationVariantDto();
        varDto1.setVariantCode("S44332211");
        varDto1.setBaseProduct(BASE_PRODUCT_CODE);
        varDto1.setApprovalStatus("Active");
        varDto1.setSize("12");

        final IntegrationVariantDto varDto2 = new IntegrationVariantDto();
        varDto2.setVariantCode("S88775566");
        varDto2.setBaseProduct(BASE_PRODUCT_CODE);
        varDto2.setApprovalStatus("Active");
        varDto2.setSize("13");

        dto2.getVariants().add(varDto1);
        dto2.getVariants().add(varDto2);

        final IntegrationResponseDto response2 = productImportIntegrationFacade.persistTargetProduct(dto2);

        assertTrue("Only one colour variant created  - first insert", response1.isSuccessStatus());
        assertTrue("Only one colour variant created  - second insert", response2.isSuccessStatus());

        // try to retrieve second variant by code => expecting exception: UnknownIdentifierException: Product with code 'V987654322' not found!
        productService.getProductForCode(dto2.getVariantCode());
    }


    @Test
    public void testCanCreateIndividualColourVariantsForSeparateSwatchColours() {
        final IntegrationProductDto dto1 = createTestDto(true);
        dto1.setColour("Red");
        dto1.setSwatch("Crimson Red");
        final IntegrationResponseDto response1 = productImportIntegrationFacade.persistTargetProduct(dto1);
        final TargetColourVariantProductModel variant1 = (TargetColourVariantProductModel)productService
                .getProductForCode(dto1.getVariantCode());
        assertTrue("Can merge size variants", response1.isSuccessStatus());
        assertEquals("Can merge size variants", dto1.getVariantCode(), variant1.getCode());
        assertEquals("Can merge size variants", 2, variant1.getVariants().size());

        final IntegrationProductDto dto2 = createTestDto(false);
        dto2.setVariantCode("V987654322");
        dto2.setColour("Red");
        dto2.setSwatch("Imperial Red");

        final IntegrationVariantDto varDto1 = new IntegrationVariantDto();
        varDto1.setVariantCode("S44332211");
        varDto1.setBaseProduct(BASE_PRODUCT_CODE);
        varDto1.setApprovalStatus("Active");
        varDto1.setSize("12");

        final IntegrationVariantDto varDto2 = new IntegrationVariantDto();
        varDto2.setVariantCode("S88775566");
        varDto2.setBaseProduct(BASE_PRODUCT_CODE);
        varDto2.setApprovalStatus("Active");
        varDto2.setSize("13");

        dto2.getVariants().add(varDto1);
        dto2.getVariants().add(varDto2);

        final IntegrationResponseDto response2 = productImportIntegrationFacade.persistTargetProduct(dto2);
        final TargetColourVariantProductModel variant2 = (TargetColourVariantProductModel)productService
                .getProductForCode(dto2.getVariantCode());

        assertTrue("Can merge size variants", response2.isSuccessStatus());
        assertEquals("Can merge size variants", dto2.getVariantCode(), variant2.getCode());
        assertEquals("Can merge size variants", 2, variant2.getVariants().size());
    }

    @Test
    public void testCanCreateAssortmentProduct() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setIsAssortment(Boolean.TRUE);
        dto.setIsStylegroup(Boolean.FALSE);
        dto.setColour("Green");
        dto.setSwatch("Green Swatch");

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new assortment product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(dto
                    .getProductCode());
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());

            assertEquals("Can create new assortment product - base product", baseProduct.getCode(), colourVariant
                    .getBaseProduct().getCode());
            assertEquals("Can create new assortment product - variant code", dto.getVariantCode(),
                    colourVariant.getCode());
            assertEquals("Can create new assortment product - variant name", dto.getName(), colourVariant.getName());
            assertEquals("Can create new assortment product - variant colour", "green", colourVariant
                    .getColourName().toLowerCase());
            assertEquals("Can create new assortment product - variant swatch", "green swatch", colourVariant
                    .getSwatchName().toLowerCase());
        }
    }

    @Test
    public void createNewMediaWithoutModelSavingException() {
        MediaModel testMediaModel = null;
        final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION);
        final String code = "testMedia";
        try {
            testMediaModel = mediaService.getMedia(catalogVersionModel, code);
        }
        //CHECKSTYLE:OFF want an empty block here
        catch (final UnknownIdentifierException ex) {
            // do nothing 
        }
        //CHECKSTYLE:ON
        if (null != testMediaModel) {
            modelService.remove(testMediaModel);
        }
        testMediaModel = productImportIntegrationFacade.createNewMedia(code, "youtube");
        assertEquals(code, testMediaModel.getCode());
    }

    @Test
    public void testCanCreateProductWithGender() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setColour("Blue");
        dto.setSwatch("Sky Blue");
        dto.setArticleStatus("targetexclusive");
        dto.setGenders(Collections.singletonList("girl"));

        IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new colour variant", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());

            assertEquals("Can create new colour variant - base product", dto.getProductCode(),
                    colourVariant.getBaseProduct().getCode());
            assertEquals("Can create new colour variant - variant code", dto.getVariantCode(), colourVariant.getCode());
            assertEquals("Can create new colour variant - variant name", dto.getName(), colourVariant.getName());
            assertEquals("Can create new colour variant - variant colour", dto.getColour().toLowerCase(),
                    colourVariant.getColourName().toLowerCase());
            assertEquals("Can create new colour variant - variant swatch", dto.getSwatch().toLowerCase(),
                    colourVariant.getSwatchName().toLowerCase());
            final TargetProductModel baseProduct = (TargetProductModel)colourVariant.getBaseProduct();
            verifyProductFeatures(dto, baseProduct);
        }

        // change the gender to boy
        dto.setGenders(Collections.singletonList("boy"));

        response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("updated the colour variant", response.isSuccessStatus());

        TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        TargetProductModel baseProduct = (TargetProductModel)colourVariant.getBaseProduct();
        verifyProductFeatures(dto, baseProduct);

        //create multiple values for the gender
        dto.setGenders(Lists.asList("boy", new String[] { "girl" }));

        response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("updated the colour variant", response.isSuccessStatus());

        colourVariant = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        baseProduct = (TargetProductModel)colourVariant.getBaseProduct();
        final FeatureList features = classificationService.getFeatures(baseProduct);
        for (final Feature feature : features.getFeatures()) {
            final String code = feature.getClassAttributeAssignment().getClassificationAttribute().getCode();

            if (code.equalsIgnoreCase(TargetProductImportConstants.FEATURE_GENDER)) {
                final List<FeatureValue> genders = feature.getValues();
                final List<String> updatedGenders = new ArrayList<>();
                for (final FeatureValue val : genders) {
                    updatedGenders.add(((ClassificationAttributeValueModel)val.getValue()).getCode());
                }
                Assertions.assertThat(updatedGenders).isNotEmpty().hasSize(2);
                Assertions.assertThat(updatedGenders).contains("girl", "boy");
            }
        }
    }

    @Test
    public void testCanCreateProductWithLicenseNAgeNOnlineToFrom() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setColour("Blue");
        dto.setSwatch("Sky Blue");
        dto.setArticleStatus("targetexclusive");
        dto.setLicense("licensing");
        dto.setAgeFrom(Double.valueOf(3.0d));
        dto.setAgeTo(Double.valueOf(7.0d));
        String onlineDate = getDate(2);
        String offlineDate = getDate(4);
        dto.setProductOnlineDate(onlineDate);
        dto.setProductOfflineDate(offlineDate);

        IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        Assertions.assertThat(response.isSuccessStatus()).isTrue();

        TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        TargetProductModel baseProduct = (TargetProductModel)colourVariant.getBaseProduct();

        verifyProductFeatures(dto, baseProduct);

        Date actualOnlineDate = colourVariant.getOnlineDate();
        Assertions.assertThat(actualOnlineDate).isNotNull();
        Assertions.assertThat(FAST_DATE_FORMAT.format(actualOnlineDate)).isEqualToIgnoringCase(onlineDate);

        Date actualOfflineDate = colourVariant.getOfflineDate();
        Assertions.assertThat(actualOfflineDate).isNotNull();
        Assertions.assertThat(FAST_DATE_FORMAT.format(actualOfflineDate)).isEqualToIgnoringCase(offlineDate);

        // change the features
        dto.setLicense("license");
        dto.setAgeFrom(Double.valueOf(5.0d));
        dto.setAgeTo(Double.valueOf(10.0d));
        onlineDate = getDate(1);
        offlineDate = getDate(5);
        dto.setProductOnlineDate(onlineDate);
        dto.setProductOfflineDate(offlineDate);

        response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("updated the colour variant", response.isSuccessStatus());

        colourVariant = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        baseProduct = (TargetProductModel)colourVariant.getBaseProduct();

        verifyProductFeatures(dto, baseProduct);

        actualOnlineDate = colourVariant.getOnlineDate();
        Assertions.assertThat(actualOnlineDate).isNotNull();
        Assertions.assertThat(FAST_DATE_FORMAT.format(actualOnlineDate)).isEqualToIgnoringCase(onlineDate);

        actualOfflineDate = colourVariant.getOfflineDate();
        Assertions.assertThat(actualOfflineDate).isNotNull();
        Assertions.assertThat(FAST_DATE_FORMAT.format(actualOfflineDate)).isEqualToIgnoringCase(offlineDate);

        //update the values
        dto.setLicense(null);
        dto.setAgeFrom(null);
        dto.setAgeTo(null);
        dto.setProductOnlineDate(null);
        dto.setProductOfflineDate(null);

        response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("updated the colour variant", response.isSuccessStatus());

        colourVariant = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        baseProduct = (TargetProductModel)colourVariant.getBaseProduct();
        final FeatureList features = classificationService.getFeatures(baseProduct);
        for (final Feature feature : features.getFeatures()) {
            final String code = feature.getClassAttributeAssignment().getClassificationAttribute().getCode();

            if (code.equalsIgnoreCase(TargetProductImportConstants.FEATURE_AGE_RANGE)) {
                final List<FeatureValue> genders = feature.getValues();
                Assertions.assertThat(genders).isEmpty();
            }
            else if (code.equalsIgnoreCase(TargetProductImportConstants.FEATURE_LICENSE)) {
                final FeatureValue featureValue = feature.getValue();
                Assertions.assertThat(featureValue).isNull();
            }
        }
    }

    @Test
    public void testCanCreateProductWithNoAgeFrom() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setColour("Blue");
        dto.setSwatch("Sky Blue");
        dto.setArticleStatus("targetexclusive");
        // change the features
        dto.setLicense("license");
        dto.setAgeTo(Double.valueOf(10.0d));
        final String onlineDate = getDate(1);
        final String offlineDate = getDate(5);
        dto.setProductOnlineDate(onlineDate);
        dto.setProductOfflineDate(offlineDate);

        IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("updated the colour variant", response.isSuccessStatus());

        TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        TargetProductModel baseProduct = (TargetProductModel)colourVariant.getBaseProduct();

        final FeatureList features = classificationService.getFeatures(baseProduct);
        for (final Feature feature : features.getFeatures()) {
            final String code = feature.getClassAttributeAssignment().getClassificationAttribute().getCode();

            if (code.equalsIgnoreCase(TargetProductImportConstants.FEATURE_AGE_RANGE)) {
                final List<FeatureValue> featureValues = feature.getValues();
                Assertions.assertThat(featureValues).isNotNull().isNotEmpty().hasSize(2);
                for (int i = 0; i < featureValues.size(); i++) {
                    final Object objValue = featureValues.get(i);
                    Assertions.assertThat(objValue).isNotNull().isInstanceOf(FeatureValue.class);
                    final FeatureValue featureValue = (FeatureValue)objValue;
                    final Object value = featureValue.getValue();
                    Assertions.assertThat(value).isNotNull().isInstanceOf(Double.class);
                    final Double age = (Double)value;
                    if (i == 0) {
                        Assertions.assertThat(age).isEqualTo(0d);
                    }
                    else {
                        Assertions.assertThat(age).isEqualTo(10d);
                    }
                }

            }
        }

        final Date actualOnlineDate = colourVariant.getOnlineDate();
        Assertions.assertThat(actualOnlineDate).isNotNull();
        Assertions.assertThat(FAST_DATE_FORMAT.format(actualOnlineDate)).isEqualToIgnoringCase(onlineDate);

        final Date actualOfflineDate = colourVariant.getOfflineDate();
        Assertions.assertThat(actualOfflineDate).isNotNull();
        Assertions.assertThat(FAST_DATE_FORMAT.format(actualOfflineDate)).isEqualToIgnoringCase(offlineDate);

        //update the values
        dto.setLicense(null);
        dto.setAgeFrom(null);
        dto.setAgeTo(null);
        dto.setProductOnlineDate(null);
        dto.setProductOfflineDate(null);

        response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("updated the colour variant", response.isSuccessStatus());

        colourVariant = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        baseProduct = (TargetProductModel)colourVariant.getBaseProduct();
        final FeatureList featuresList = classificationService.getFeatures(baseProduct);
        for (final Feature feature : featuresList.getFeatures()) {
            final String code = feature.getClassAttributeAssignment().getClassificationAttribute().getCode();

            if (code.equalsIgnoreCase(TargetProductImportConstants.FEATURE_AGE_RANGE)) {
                final List<FeatureValue> genders = feature.getValues();
                Assertions.assertThat(genders).isEmpty();
            }
            else if (code.equalsIgnoreCase(TargetProductImportConstants.FEATURE_LICENSE)) {
                final FeatureValue featureValue = feature.getValue();
                Assertions.assertThat(featureValue).isNull();
            }
        }
    }

    @Test
    public void testValidateClassificationAgeRangeWithOnlyNegativeFromAge() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setProductCode("W552447");
        dto.setAgeFrom(Double.valueOf(-5d));

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            final List<ProductFeatureModel> features = baseProduct.getFeatures();
            Assertions.assertThat(features).isEmpty();
        }
    }

    @Test
    public void testValidateClassificationAgeRangeWithOnlyNegativeToAge() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setProductCode("W552447");
        dto.setAgeTo(Double.valueOf(-5d));

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            final List<ProductFeatureModel> features = baseProduct.getFeatures();
            Assertions.assertThat(features).isEmpty();
        }
    }

    @Test
    public void testValidateClassificationAgeRangeWithAllNegatives() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setProductCode("W552447");
        dto.setAgeFrom(Double.valueOf(-5d));
        dto.setAgeTo(Double.valueOf(-10d));
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            final List<ProductFeatureModel> features = baseProduct.getFeatures();
            Assertions.assertThat(features).isEmpty();
        }
    }

    @Test
    public void testValidateClassificationAgeRangeWithNegativeFromAge() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setProductCode("W552447");
        dto.setAgeFrom(Double.valueOf(-5d));
        dto.setAgeTo(Double.valueOf(10d));

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            final List<ProductFeatureModel> features = baseProduct.getFeatures();
            Assertions.assertThat(features).isEmpty();
        }
    }

    @Test
    public void testValidateClassificationAgeRangeWithNegativeToAge() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setProductCode("W552447");
        dto.setAgeFrom(Double.valueOf(5d));
        dto.setAgeTo(Double.valueOf(-10d));

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            final List<ProductFeatureModel> features = baseProduct.getFeatures();
            Assertions.assertThat(features).isEmpty();
        }
    }

    @Test
    public void testEbayFlagSetToNullForProductsWithNoSizeVariants() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setAvailableOnEbay("N");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            final Collection<VariantProductModel> variants = baseProduct.getVariants();
            Assertions.assertThat(variants).isNotEmpty().hasSize(1);
            final VariantProductModel variant = baseProduct.getVariants().iterator().next();
            Assertions.assertThat(variant).isInstanceOf(TargetColourVariantProductModel.class);
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)variant;
            Assertions.assertThat(colourVariant.getAvailableOnEbay()).isFalse();
        }
    }

    @Test
    public void testEbayFlagSetTrueForProductsWithNoSizeVariants() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setAvailableOnEbay("Y");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            final Collection<VariantProductModel> variants = baseProduct.getVariants();
            Assertions.assertThat(variants).isNotEmpty().hasSize(1);
            final VariantProductModel variant = baseProduct.getVariants().iterator().next();
            Assertions.assertThat(variant).isInstanceOf(TargetColourVariantProductModel.class);
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)variant;
            Assertions.assertThat(colourVariant.getAvailableOnEbay()).isTrue();
        }
    }

    @Test
    public void testEbayFlagSetNForProductsWithNoSizeVariants() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setAvailableOnEbay("N");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            final Collection<VariantProductModel> variants = baseProduct.getVariants();
            Assertions.assertThat(variants).isNotEmpty().hasSize(1);
            final VariantProductModel variant = baseProduct.getVariants().iterator().next();
            Assertions.assertThat(variant).isInstanceOf(TargetColourVariantProductModel.class);
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)variant;
            Assertions.assertThat(colourVariant.getAvailableOnEbay()).isFalse();
        }
    }

    @Test
    public void testEbayFlagSetToEmptyForProductsWithNoSizeVariants() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setAvailableOnEbay("");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            final Collection<VariantProductModel> variants = baseProduct.getVariants();
            Assertions.assertThat(variants).isNotEmpty().hasSize(1);
            final VariantProductModel variant = baseProduct.getVariants().iterator().next();
            Assertions.assertThat(variant).isInstanceOf(TargetColourVariantProductModel.class);
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)variant;
            Assertions.assertThat(colourVariant.getAvailableOnEbay()).isFalse();
        }
    }

    @Test
    public void testEbayFlagSetTrueForCVForProductsWithSizeVariantsOneSizeVariantFlagSetToFalse() {
        final IntegrationProductDto dto = createTestDto(true);
        dto.setAvailableOnEbay("Y");
        dto.getVariants().get(0).setAvailableOnEbay("N");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            final Collection<VariantProductModel> variants = baseProduct.getVariants();
            Assertions.assertThat(variants).isNotEmpty().hasSize(1);
            final VariantProductModel variant = baseProduct.getVariants().iterator().next();
            Assertions.assertThat(variant).isInstanceOf(TargetColourVariantProductModel.class);
            final VariantProductModel variantModel = baseProduct.getVariants().iterator().next();
            Assertions.assertThat(variantModel).isInstanceOf(TargetColourVariantProductModel.class);
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)variantModel;
            Assertions.assertThat(colourVariant.getAvailableOnEbay()).isFalse();
            final Collection<VariantProductModel> sizeVariants = variant.getVariants();
            Assertions.assertThat(sizeVariants).isNotNull().isNotEmpty().hasSize(2);
            int countOfSizeVariantsHavingEbayFlag = 0;
            int countOfSizeVariantsNotHavingEbayFlag = 0;
            for (final VariantProductModel sizeVariant : sizeVariants) {
                Assertions.assertThat(sizeVariant).isInstanceOf(TargetSizeVariantProductModel.class);
                final TargetSizeVariantProductModel sizeVariantModel = (TargetSizeVariantProductModel)sizeVariant;
                if (sizeVariantModel.getAvailableOnEbay().booleanValue()) {
                    countOfSizeVariantsHavingEbayFlag++;
                }
                else {
                    countOfSizeVariantsNotHavingEbayFlag++;
                }

            }

            assertEquals(countOfSizeVariantsHavingEbayFlag, sizeVariants.size());
            assertEquals(countOfSizeVariantsNotHavingEbayFlag, 0);
        }
    }

    @Test
    public void testEbayFlagSetToNForProductsWithSizeVariants() {
        final IntegrationProductDto dto = createTestDto(true);
        dto.setAvailableOnEbay("N");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            final Collection<VariantProductModel> variants = baseProduct.getVariants();
            Assertions.assertThat(variants).isNotEmpty().hasSize(1);
            final VariantProductModel variant = baseProduct.getVariants().iterator().next();
            Assertions.assertThat(variant).isInstanceOf(TargetColourVariantProductModel.class);
            final VariantProductModel variantModel = baseProduct.getVariants().iterator().next();
            Assertions.assertThat(variantModel).isInstanceOf(TargetColourVariantProductModel.class);
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)variantModel;
            Assertions.assertThat(colourVariant.getAvailableOnEbay()).isFalse();
            final Collection<VariantProductModel> sizeVariants = variant.getVariants();
            Assertions.assertThat(sizeVariants).isNotNull().isNotEmpty().hasSize(2);
            for (final VariantProductModel sizeVariant : sizeVariants) {
                Assertions.assertThat(sizeVariant).isInstanceOf(TargetSizeVariantProductModel.class);
                final TargetSizeVariantProductModel sizeVariantModel = (TargetSizeVariantProductModel)sizeVariant;
                Assertions.assertThat(sizeVariantModel.getAvailableOnEbay()).isFalse();
            }
        }
    }

    @Test
    public void testEbayFlagSetToYForProductsWithSizeVariants() {
        final IntegrationProductDto dto = createTestDto(true);
        dto.setAvailableOnEbay("Y");
        for (final IntegrationVariantDto vdto : dto.getVariants()) {
            vdto.setAvailableOnEbay("Y");
        }

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            final Collection<VariantProductModel> variants = baseProduct.getVariants();
            Assertions.assertThat(variants).isNotEmpty().hasSize(1);
            final VariantProductModel variant = baseProduct.getVariants().iterator().next();
            Assertions.assertThat(variant).isInstanceOf(TargetColourVariantProductModel.class);
            final VariantProductModel variantModel = baseProduct.getVariants().iterator().next();
            Assertions.assertThat(variantModel).isInstanceOf(TargetColourVariantProductModel.class);
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)variantModel;
            Assertions.assertThat(colourVariant.getAvailableOnEbay()).isFalse();
            final Collection<VariantProductModel> sizeVariants = variantModel.getVariants();
            Assertions.assertThat(sizeVariants).isNotNull().isNotEmpty().hasSize(2);
            for (final VariantProductModel sizeVariant : sizeVariants) {
                Assertions.assertThat(sizeVariant).isInstanceOf(TargetSizeVariantProductModel.class);
                final TargetSizeVariantProductModel sizeVariantModel = (TargetSizeVariantProductModel)sizeVariant;
                Assertions.assertThat(sizeVariantModel.getAvailableOnEbay()).isTrue();
            }
        }
    }

    @Test
    public void testEanForSizeVariantsWithColourVariants() {
        final String expectedEan = "9300675001427";
        final IntegrationProductDto dto = createTestDto(true);
        dto.setEan("   93675001427  ");
        for (final IntegrationVariantDto vdto : dto.getVariants()) {
            vdto.setEan(expectedEan);
        }

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            final Collection<VariantProductModel> variants = baseProduct.getVariants();
            Assertions.assertThat(variants).isNotEmpty().hasSize(1);
            final VariantProductModel variant = baseProduct.getVariants().iterator().next();
            Assertions.assertThat(variant).isInstanceOf(TargetColourVariantProductModel.class);
            final VariantProductModel variantModel = baseProduct.getVariants().iterator().next();
            Assertions.assertThat(variantModel).isInstanceOf(TargetColourVariantProductModel.class);
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)variantModel;
            Assertions.assertThat(colourVariant.getAvailableOnEbay()).isFalse();
            Assertions.assertThat(colourVariant.getEan()).isEqualTo("0093675001427");
            final Collection<VariantProductModel> sizeVariants = variantModel.getVariants();
            Assertions.assertThat(sizeVariants).isNotNull().isNotEmpty().hasSize(2);
            for (final VariantProductModel sizeVariant : sizeVariants) {
                Assertions.assertThat(sizeVariant).isInstanceOf(TargetSizeVariantProductModel.class);
                final TargetSizeVariantProductModel sizeVariantModel = (TargetSizeVariantProductModel)sizeVariant;
                Assertions.assertThat(sizeVariantModel.getEan()).isEqualTo(expectedEan);
            }
        }
    }

    @Test
    public void testReponseMessageForInvalidEanForSizeVariantsWithColourVariants() {
        final String ean = "936750 01427";
        final IntegrationProductDto dto = createTestDto(true);

        for (final IntegrationVariantDto vdto : dto.getVariants()) {
            vdto.setEan(ean);
        }

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue(response.isSuccessStatus());

        final List<String> responseMsgs = response.getMessages();
        assertTrue(CollectionUtils.isNotEmpty(responseMsgs));
        assertEquals(2, responseMsgs.size());
        assertEquals(MessageFormat.format(TargetProductImportConstants.LogMessages.EAN_VALIDATION_FAILURE,
                SIZE_VARIANT_CODE_1, BASE_PRODUCT_NAME, ean), responseMsgs.get(0));
        assertEquals(MessageFormat.format(TargetProductImportConstants.LogMessages.EAN_VALIDATION_FAILURE,
                SIZE_VARIANT_CODE_2, BASE_PRODUCT_NAME, ean), responseMsgs.get(1));
    }

    @Test
    public void testEanForOnlySizeVariantsNoColourVariant() {
        final IntegrationProductDto dto = createTestDto(true);
        dto.setIsSizeOnly(Boolean.TRUE);
        dto.setSize("S");
        dto.setEan("   93675001427  ");
        for (final IntegrationVariantDto vdto : dto.getVariants()) {
            vdto.setEan("testVariantEan");
        }

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            final Collection<VariantProductModel> variants = baseProduct.getVariants();
            Assertions.assertThat(variants).isNotEmpty().hasSize(1);
            final VariantProductModel variant = baseProduct.getVariants().iterator().next();
            Assertions.assertThat(variant).isInstanceOf(TargetColourVariantProductModel.class);
            final VariantProductModel variantModel = baseProduct.getVariants().iterator().next();
            Assertions.assertThat(variantModel).isInstanceOf(TargetColourVariantProductModel.class);
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)variantModel;
            Assertions.assertThat(colourVariant.getAvailableOnEbay()).isFalse();
            Assertions.assertThat(colourVariant.getEan()).isNullOrEmpty();
            final Collection<VariantProductModel> sizeVariants = variantModel.getVariants();
            Assertions.assertThat(sizeVariants).isNotNull().isNotEmpty().hasSize(1);
            for (final VariantProductModel sizeVariant : sizeVariants) {
                Assertions.assertThat(sizeVariant).isInstanceOf(TargetSizeVariantProductModel.class);
                final TargetSizeVariantProductModel sizeVariantModel = (TargetSizeVariantProductModel)sizeVariant;
                Assertions.assertThat(sizeVariantModel.getEan()).isEqualTo("0093675001427");
            }
        }
    }

    @Test
    public void testReponseMessageForInvalidEanForOnlySizeVariantsNoColourVariant() {
        final String ean = "936750 01427";
        final IntegrationProductDto dto = createTestDto(true);
        dto.setIsSizeOnly(Boolean.TRUE);
        dto.setSize("S");
        dto.setEan(ean);

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue(response.isSuccessStatus());

        final List<String> responseMsgs = response.getMessages();
        assertTrue(CollectionUtils.isNotEmpty(responseMsgs));
        assertEquals(1, responseMsgs.size());
        assertEquals(MessageFormat.format(TargetProductImportConstants.LogMessages.EAN_VALIDATION_FAILURE,
                BASE_PRODUCT_VARIANT_CODE, BASE_PRODUCT_NAME, ean), responseMsgs.get(0));
    }

    @Test
    public void testEbayFlagDifferentVersions() {
        final IntegrationProductDto dto = createTestDto(true);
        dto.setAvailableOnEbay("Y");
        int count = 0;
        final Map<String, Boolean> setupTestData = new HashMap<String, Boolean>();
        for (final IntegrationVariantDto vdto : dto.getVariants()) {
            if (count == 0) {
                vdto.setAvailableOnEbay("Y");
            }
            else if (count == 1) {
                vdto.setAvailableOnEbay("N");
            }
            else if (count == 2) {
                //if sizevariant ebay flag is null take the colour variant ebay flag.
                vdto.setAvailableOnEbay(null);
            }
            setupTestData.put(vdto.getVariantCode(), Boolean.TRUE);

            count++;
        }

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            final Collection<VariantProductModel> variants = baseProduct.getVariants();
            Assertions.assertThat(variants).isNotEmpty().hasSize(1);
            final VariantProductModel variant = baseProduct.getVariants().iterator().next();
            Assertions.assertThat(variant).isInstanceOf(TargetColourVariantProductModel.class);
            final VariantProductModel variantModel = baseProduct.getVariants().iterator().next();
            Assertions.assertThat(variantModel).isInstanceOf(TargetColourVariantProductModel.class);
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)variantModel;
            Assertions.assertThat(colourVariant.getAvailableOnEbay()).isFalse();
            final Collection<VariantProductModel> sizeVariants = variantModel.getVariants();
            Assertions.assertThat(sizeVariants).isNotNull().isNotEmpty().hasSize(2);
            for (final VariantProductModel sizeVariant : sizeVariants) {
                Assertions.assertThat(sizeVariant).isInstanceOf(TargetSizeVariantProductModel.class);
                final TargetSizeVariantProductModel sizeVariantModel = (TargetSizeVariantProductModel)sizeVariant;
                final Boolean actualEbayFlag = sizeVariantModel.getAvailableOnEbay();
                final Boolean expectedBooleanFlag = setupTestData.get(sizeVariantModel.getCode());
                assertEquals(actualEbayFlag, expectedBooleanFlag);
            }
        }
    }


    @Test
    public void testCreateVariantProductWithNewDimension() throws ImpExException {

        final IntegrationProductDto dto = createTestDto(true);
        int count = 0;
        final Map<String, Double> setupTestData = new HashMap<String, Double>();
        for (final IntegrationVariantDto vdto : dto.getVariants()) {
            vdto.setPkgHeight(Double.valueOf(count + 0.5));
            setupTestData.put(vdto.getVariantCode() + "_Height", vdto.getPkgHeight());
            vdto.setPkgWeight(Double.valueOf(count + 1.5));
            setupTestData.put(vdto.getVariantCode() + "_Weight", vdto.getPkgWeight());
            vdto.setPkgLength(Double.valueOf(count + 2.5));
            setupTestData.put(vdto.getVariantCode() + "_Length", vdto.getPkgLength());
            vdto.setPkgWidth(Double.valueOf(count + 3.5));
            setupTestData.put(vdto.getVariantCode() + "_Width", vdto.getPkgWidth());
            count++;
        }

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());
        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(dto
                    .getProductCode());
            final Collection<VariantProductModel> variants = baseProduct.getVariants();
            Assertions.assertThat(variants).isNotEmpty().hasSize(1);
            final VariantProductModel variant = baseProduct.getVariants().iterator().next();
            Assertions.assertThat(variant).isInstanceOf(TargetColourVariantProductModel.class);
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)variant;
            final Collection<VariantProductModel> sizeVariants = colourVariant.getVariants();
            Assertions.assertThat(sizeVariants).isNotNull().isNotEmpty().hasSize(2);
            for (final VariantProductModel sizeVariant : sizeVariants) {
                Assertions.assertThat(sizeVariant).isInstanceOf(TargetSizeVariantProductModel.class);
                final TargetSizeVariantProductModel sizeVariantModel = (TargetSizeVariantProductModel)sizeVariant;
                final TargetProductDimensionsModel productPackageDimensions = sizeVariantModel
                        .getProductPackageDimensions();
                assertEquals(setupTestData.get(sizeVariantModel.getCode() + "_Height"),
                        productPackageDimensions.getHeight());
                assertEquals(setupTestData.get(sizeVariantModel.getCode() + "_Weight"),
                        productPackageDimensions.getWeight());
                assertEquals(setupTestData.get(sizeVariantModel.getCode() + "_Length"),
                        productPackageDimensions.getLength());
                assertEquals(setupTestData.get(sizeVariantModel.getCode() + "_Width"),
                        productPackageDimensions.getWidth());
            }
        }

    }

    @Test
    public void testCreateBaseProductWithNewDimension() throws ImpExException {

        final IntegrationProductDto dto = createTestDto(false);

        dto.setPkgHeight(Double.valueOf(2.5));
        dto.setPkgWeight(Double.valueOf(3.5));
        dto.setPkgLength(Double.valueOf(4.5));
        dto.setPkgWidth(Double.valueOf(5.5));

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new base product", response.isSuccessStatus());
        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(dto
                    .getProductCode());
            final Collection<VariantProductModel> variants = baseProduct.getVariants();
            Assertions.assertThat(variants).isNotEmpty().hasSize(1);
            final VariantProductModel variant = baseProduct.getVariants().iterator().next();
            Assertions.assertThat(variant).isInstanceOf(TargetColourVariantProductModel.class);
            final TargetColourVariantProductModel targetColourVariantProductModel = (TargetColourVariantProductModel)variant;
            assertEquals(dto.getVariantCode(), targetColourVariantProductModel.getCode());
            final TargetProductDimensionsModel targetProductPackageModel = targetColourVariantProductModel
                    .getProductPackageDimensions();
            assertEquals("PKG_" + dto.getVariantCode(), targetProductPackageModel.getDimensionCode());
            assertEquals(Double.valueOf(2.5), targetProductPackageModel.getHeight());
            assertEquals(Double.valueOf(3.5), targetProductPackageModel.getWeight());
            assertEquals(Double.valueOf(4.5), targetProductPackageModel.getLength());
            assertEquals(Double.valueOf(5.5), targetProductPackageModel.getWidth());
        }
    }

    @Test
    public void testValidateClassificationAgeRangeWithInvalidRange() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setProductCode("W552447");
        dto.setAgeFrom(Double.valueOf(15d));
        dto.setAgeTo(Double.valueOf(10d));

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            final List<ProductFeatureModel> features = baseProduct.getFeatures();
            Assertions.assertThat(features).isEmpty();
        }
    }

    @Test
    public void testValidateClassificationAgeRangeWithValidRange() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setProductCode("W552447");
        dto.setAgeFrom(Double.valueOf(5d));
        dto.setAgeTo(Double.valueOf(10d));

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            final List<ProductFeatureModel> features = baseProduct.getFeatures();
            Assertions.assertThat(features).isNotEmpty();
            assertEquals("Should create a range entries if given range values are valid", features.size(), 2);
        }
    }

    @Test
    public void testClearanceFieldTrue() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setClearance("Y");

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            Assertions.assertThat(baseProduct.getClearanceProduct()).isTrue();
        }
    }

    @Test
    public void testClearanceFieldEmpty() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setClearance("");

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            Assertions.assertThat(baseProduct.getClearanceProduct()).isFalse();
        }
    }

    @Test
    public void testClearanceFieldFalse() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setClearance("N");

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new base product", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            Assertions.assertThat(baseProduct.getClearanceProduct()).isFalse();
        }
    }

    @Test
    public void testUpdateCVWithNewOnlineDate() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setColour("Red Colour");
        dto.setSwatch("Red Swatch");
        dto.setProductOnlineDate("2015-08-26 00:00:00");
        dto.setProductOfflineDate("2015-09-30 00:00:00");
        final IntegrationResponseDto response1 = productImportIntegrationFacade.persistTargetProduct(dto);
        final TargetColourVariantProductModel variant1 = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        final Calendar fromDate = new GregorianCalendar(2015, Calendar.AUGUST, 26);
        final Calendar toDate = new GregorianCalendar(2015, Calendar.SEPTEMBER, 30);

        assertTrue("Can update existing colour variant", response1.isSuccessStatus());
        Assert.assertNotNull(variant1.getOnlineDate());
        Assert.assertNotNull(variant1.getOfflineDate());
        Assert.assertEquals(fromDate.getTime(), variant1.getOnlineDate());
        Assert.assertEquals(toDate.getTime(), variant1.getOfflineDate());
    }

    @Test
    public void testCanCreatGiftCard() {
        final IntegrationProductDto dto = createTestDto(true, "PGC_iTunes", "PGC_iTunes_10", "iTunes gift card", true,
                false);
        dto.setColour("Blue");
        dto.setSwatch("iTunes");
        dto.setArticleStatus("targetexclusive");

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new colour variant", response.isSuccessStatus());

        if (response.isSuccessStatus()) {
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());

            final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)productService
                    .getProductForCode(dto.getVariants().get(0).getVariantCode());

            final TargetProductModel baseProduct = (TargetProductModel)colourVariant.getBaseProduct();
            assertEquals("Can create new colour variant - base product", dto.getProductCode(),
                    baseProduct.getCode());
            assertEquals("Can create new colour variant - variant code", dto.getVariantCode(), colourVariant.getCode());
            assertEquals("Can create new colour variant - variant name", dto.getName(), colourVariant.getName());
            assertEquals("Can create new colour variant - variant colour", dto.getColour().toLowerCase(),
                    colourVariant.getColourName().toLowerCase());
            assertEquals("Can create new colour variant - variant swatch", dto.getSwatch().toLowerCase(),
                    colourVariant.getSwatchName().toLowerCase());

            Assert.assertNotNull(baseProduct.getGiftCard());
            assertEquals("Can create new colour variant - gift card brand id", dto.getGiftcardBrandId(),
                    baseProduct.getGiftCard().getBrandId());

            final Collection<StockLevelModel> stockLevels = targetStockService.getAllStockLevels(sizeVariant);
            Assert.assertTrue(CollectionUtils.isNotEmpty(stockLevels));
            Assert.assertEquals(1, stockLevels.size());

            final Set<DeliveryModeModel> deliveryModes = baseProduct.getDeliveryModes();
            Assert.assertTrue(CollectionUtils.isNotEmpty(deliveryModes));
            Assert.assertEquals(1, deliveryModes.size());
        }
    }

    @Test
    public void testPersisTargetProductWithSizeVariantAndSizeOrderFeatureOff() {
        final IntegrationProductDto dto = createTestDto(true);
        dto.setSizeGroup("womens");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());
    }

    @Test
    public void testPersisTargetProductFeatureOffAndSizeGrpNull() {
        final IntegrationProductDto dto = createTestDto(true);
        dto.setSizeGroup(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());
    }

    @Test
    public void testPersisTargetProductWithNewSizeGroupAndFeatureSwitchOn() throws ImpExException {
        importCsv("/tgtcore/test/testSizeOrderFeature.impex", Charsets.UTF_8.name());
        final IntegrationProductDto dto = createTestDto(true);
        dto.setSizeGroup("womens");
        dto.getVariants().get(0).setSize("xs");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());
        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            Assert.assertNotNull(baseProduct);
            final TargetColourVariantProductModel colorVariant = (TargetColourVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());
            Assert.assertNotNull(colorVariant);
            assertEquals(colorVariant.getSizeGroup().getCode(), "womens");
            assertEquals(colorVariant.getSizeGroup().getPosition(), Integer.valueOf(3));
            final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)productService
                    .getProductForCode(dto.getVariants().get(0).getVariantCode());
            Assert.assertNotNull(sizeVariant);
            Assert.assertNotNull(sizeVariant.getProductSize());
            Assert.assertEquals(sizeVariant.getProductSize().getSize(), "xs");
            Assert.assertEquals(sizeVariant.getProductSize().getPosition(), Integer.valueOf(1));

        }
    }

    @Test
    public void testPersisTargetProductWithExistingGroupAndFeatureSwitchOn() throws ImpExException {
        importCsv("/tgtcore/test/testSizeOrderFeature.impex", Charsets.UTF_8.name());
        final IntegrationProductDto dto = createTestDto(true);
        dto.setSizeGroup("mensBottoms");
        dto.getVariants().get(0).setSize("52");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());
        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            Assert.assertNotNull(baseProduct);
            final TargetColourVariantProductModel colorVariant = (TargetColourVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());
            Assert.assertNotNull(colorVariant);
            assertEquals(colorVariant.getSizeGroup().getCode(), "mensBottoms");
            assertEquals(colorVariant.getSizeGroup().getPosition(), Integer.valueOf(1));
            final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)productService
                    .getProductForCode(dto.getVariants().get(0).getVariantCode());
            Assert.assertNotNull(sizeVariant);
            Assert.assertNotNull(sizeVariant.getProductSize());
            Assert.assertEquals(sizeVariant.getProductSize().getSize(), "52");
            Assert.assertEquals(sizeVariant.getProductSize().getPosition(), Integer.valueOf(13));

        }
    }

    @Test
    public void testPersisTargetProductWithExistingGroupAndNonExistPrdSizeFeatureSwitchOn() throws ImpExException {
        importCsv("/tgtcore/test/testSizeOrderFeature.impex", Charsets.UTF_8.name());
        final IntegrationProductDto dto = createTestDto(true);
        dto.setSizeGroup("mensBottoms");
        dto.getVariants().get(0).setSize("150");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());
        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            Assert.assertNotNull(baseProduct);
            final TargetColourVariantProductModel colorVariant = (TargetColourVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());
            Assert.assertNotNull(colorVariant);
            assertEquals(colorVariant.getSizeGroup().getCode(), "mensBottoms");
            assertEquals(colorVariant.getSizeGroup().getPosition(), Integer.valueOf(1));
            final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)productService
                    .getProductForCode(dto.getVariants().get(0).getVariantCode());
            Assert.assertNotNull(sizeVariant);
            Assert.assertNotNull(sizeVariant.getProductSize());
            Assert.assertEquals(sizeVariant.getProductSize().getSize(), "150");
            Assert.assertEquals(sizeVariant.getProductSize().getPosition(), Integer.valueOf(16));

        }
    }


    @Test
    public void testPersistProductSizeOnlySizeOrderFeatureOn() throws ImpExException {
        importCsv("/tgtcore/test/testSizeOrderFeature.impex", Charsets.UTF_8.name());
        final IntegrationProductDto dto = createTestDto(false);
        dto.setIsSizeOnly(Boolean.TRUE);
        dto.setSizeGroup("bedding");
        dto.setSizeType("bedding");
        dto.setSize("55");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());
        if (response.isSuccessStatus()) {
            final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                    dto.getProductCode());
            final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)productService
                    .getProductForCode(dto.getVariantCode());
            final TargetColourVariantProductModel colorVariant = (TargetColourVariantProductModel)sizeVariant
                    .getBaseProduct();
            Assert.assertNotNull(baseProduct);
            Assert.assertEquals(dto.getProductCode(), baseProduct.getCode());
            Assert.assertNotNull(colorVariant);
            Assert.assertNotNull(colorVariant.getSizeGroup());
            Assert.assertEquals("bedding", colorVariant.getSizeGroup().getCode());
            Assert.assertEquals("bedding", colorVariant.getSizeGroup().getName());
            Assert.assertNotNull(sizeVariant);
            assertEquals(sizeVariant.getProductSize().getSize(), "55");
            assertEquals(sizeVariant.getProductSize().getPosition(), Integer.valueOf(1));
        }
    }

    private String getDate(final int plusDays) {
        DateTime date = new DateTime();
        if (plusDays > 0) {
            date = date.plusDays(plusDays);
        }
        return FAST_DATE_FORMAT.format(date.toDate());
    }

    private void verifyProductFeatures(final IntegrationProductDto dto, final TargetProductModel baseProduct) {
        final FeatureList features = classificationService.getFeatures(baseProduct);
        for (final Feature feature : features.getFeatures()) {
            final String code = feature.getClassAttributeAssignment().getClassificationAttribute().getCode();

            if (code.equalsIgnoreCase(TargetProductImportConstants.FEATURE_LICENSE)) {
                final FeatureValue featureValue = feature.getValue();
                final Object value = featureValue != null ? featureValue.getValue() : null;
                assertEquals("Can create new base product - feature / licence", dto.getLicense(),
                        value);
            }

            if (code.equalsIgnoreCase(TargetProductImportConstants.FEATURE_GENDER)) {
                final List<FeatureValue> genders = feature.getValues();
                for (final FeatureValue val : genders) {
                    assertEquals("Can create new base product - feature / licence", dto.getGenders().get(0),
                            val.getDescription());
                    assertEquals("Can create new base product - feature / licence", dto.getGenders().get(0)
                            .toLowerCase(),
                            ((ClassificationAttributeValueModel)val.getValue()).getCode());
                }
            }

            if (code.equalsIgnoreCase(TargetProductImportConstants.FEATURE_AGE_RANGE)) {
                final FeatureValue featureValue = feature.getValue();
                final Object value = featureValue != null ? featureValue.getValue() : null;
                assertEquals("Can create new base product - feature / age", dto.getAgeFrom(),
                        value);
            }
        }
    }

    protected IntegrationProductDto createTestDto(final boolean addSizeVariant) {
        return createTestDto(addSizeVariant, BASE_PRODUCT_CODE, BASE_PRODUCT_VARIANT_CODE, BASE_PRODUCT_NAME, false,
                false);
    }

    protected IntegrationProductDto createTestDto(final boolean addSizeVariant, final boolean preOrder) {
        return createTestDto(addSizeVariant, BASE_PRODUCT_CODE, BASE_PRODUCT_VARIANT_CODE, BASE_PRODUCT_NAME, false,
                preOrder);
    }

    protected IntegrationProductDto createTestDto(final boolean addSizeVariant, final String baseProductCode,
            final String baseVariantCode, final String productName, final boolean isDigital, final boolean preOrder) {
        final IntegrationProductDto dto = new IntegrationProductDto();
        dto.setIsAssortment(Boolean.FALSE);
        dto.setIsSizeOnly(Boolean.FALSE);
        dto.setIsStylegroup(Boolean.TRUE);

        dto.setProductCode(baseProductCode);
        dto.setVariantCode(baseVariantCode);
        dto.setName(productName);
        dto.setPrimaryCategory("CAT111");
        dto.setSecondaryCategory(Collections.singletonList("CAT222"));
        dto.setApprovalStatus("Active");
        dto.setDepartment(Integer.valueOf(123));
        dto.setDescription("Test product description");
        dto.setAvailableLayby("Y");
        dto.setAvailableLongtermLayby("Y");
        dto.setFit("Slim fit");
        dto.setMaterial("Cotton");
        dto.setTrend("Trendy");
        dto.setGiftsFor("Him");
        dto.setPlatform("Online");
        dto.setGenre("Retro");
        dto.setSeason("Spring");
        dto.setType("Type");
        dto.setOccassion("Christmas");
        if (!isDigital) {
            dto.setBulky("Y");
            dto.setProductType("bulky1");
            dto.setAvailableCnc("Y");
            dto.setAvailableHomeDelivery("Y");
            dto.setAvailableEbayExpressDelivery("Y");
        }
        else {
            dto.setProductType("digital");
            dto.setBulky("N");
            dto.setAvailableCnc("N");
            dto.setAvailableHomeDelivery("N");
            dto.setAvailableEbayExpressDelivery("N");
            dto.setGiftcardBrandId("test-gc-brand");
            dto.setDenominaton(Double.valueOf(10d));
        }
        dto.setSizeType("babysize");
        dto.setBrand("Target");
        dto.setShowIfOutOfStock("N");

        final IntegrationProductMediaDto media = new IntegrationProductMediaDto();
        media.setPrimaryImage("IMG123");

        final IntegrationSecondaryImagesDto secImgs = new IntegrationSecondaryImagesDto();
        secImgs.addSecondaryImage("IMG222");
        media.setSecondaryImages(secImgs);

        dto.setProductMedia(media);

        if (addSizeVariant) {
            final IntegrationVariantDto varDto1 = new IntegrationVariantDto();
            varDto1.setVariantCode(SIZE_VARIANT_CODE_1);
            varDto1.setBaseProduct(BASE_PRODUCT_CODE);
            varDto1.setApprovalStatus("Active");
            varDto1.setSize("10");

            final IntegrationVariantDto varDto2 = new IntegrationVariantDto();
            varDto2.setVariantCode(SIZE_VARIANT_CODE_2);
            varDto2.setBaseProduct(BASE_PRODUCT_CODE);
            varDto2.setApprovalStatus("Active");
            varDto2.setSize("11");

            dto.getVariants().add(varDto1);
            dto.getVariants().add(varDto2);
        }

        if (preOrder) {
            dto.setPreOrderEmbargoReleaseDate("2018-02-04 09:30:00");
            dto.setPreOrderStartDate("2018-01-02 09:30:00");
            dto.setPreOrderEndDate("2018-02-02 09:30:00");
            dto.setPreOrderOnlineQuantity(Integer.valueOf(20));
        }

        return dto;
    }

    @Test
    public void testCreateNewColourVariantWithStockForCnpWarehouse() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setColour("Blue");
        dto.setSwatch("Sky Blue");
        dto.setArticleStatus("targetexclusive");

        setWarehousesInRequestDto(dto, "cnpwarehouse");

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new colour variant", response.isSuccessStatus());

        final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        Assert.assertNotNull(colourVariant);

        verifyStockLevelForWarehouse(colourVariant, "cnpwarehouse");
    }

    @Test
    public void testCreateNewColourVariantWithStockForFastlineWarehouse() {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setColour("Blue");
        dto.setSwatch("Sky Blue");
        dto.setArticleStatus("targetexclusive");

        setWarehousesInRequestDto(dto, "fastlinewarehouse");

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new colour variant", response.isSuccessStatus());

        final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        Assert.assertNotNull(colourVariant);

        verifyStockLevelForWarehouse(colourVariant, "fastlinewarehouse");
    }

    @Test
    public void testCanCreateNewSizeVariantForCnpWarehouse() {
        final IntegrationProductDto dto = createTestDto(true);

        setWarehousesInRequestDto(dto, "cnpwarehouse");

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new size variant", response.isSuccessStatus());
        final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)productService
                .getProductForCode(dto.getVariants().get(0).getVariantCode());
        // colour variant is the parent of size variants
        assertEquals("Can create new size variant - base product", dto.getVariantCode(), sizeVariant
                .getBaseProduct().getCode());

        verifyStockLevelForWarehouse(sizeVariant, "cnpwarehouse");
    }

    @Test
    public void testCanCreateNewSizeVariantForDefaultWarehouse() {
        final IntegrationProductDto dto = createTestDto(true);

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);

        assertTrue("Can create new size variant", response.isSuccessStatus());
        final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)productService
                .getProductForCode(dto.getVariants().get(0).getVariantCode());
        // colour variant is the parent of size variants
        assertEquals("Can create new size variant - base product", dto.getVariantCode(), sizeVariant
                .getBaseProduct().getCode());

        verifyStockLevelForWarehouse(sizeVariant, "fastlinewarehouse");
    }

    @Test
    public void testCreateBaseProductWithStoreStockFlagAndPreviewNull() throws ImpExException {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setShowInStoreStock(null);
        dto.setPreview(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue("Can create new base product", response.isSuccessStatus());
        final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                dto.getProductCode());
        assertTrue(response.isSuccessStatus());
        assertFalse(baseProduct.getShowStoreStock().booleanValue());
        assertFalse(baseProduct.getPreview().booleanValue());
    }

    @Test
    public void testCreateBaseProductWithPreviewAndShowStoreStock() throws ImpExException {
        final IntegrationProductDto dto = createTestDto(false);
        dto.setPreview("Y");
        dto.setShowInStoreStock("N");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue(response.isSuccessStatus());
        final TargetProductModel baseProduct = (TargetProductModel)productService.getProductForCode(
                dto.getProductCode());
        assertFalse(baseProduct.getShowStoreStock().booleanValue());
        assertTrue(baseProduct.getPreview().booleanValue());
    }

    @Test
    public void testPreOrderEmbargoReleaseDateWithNoHoursAndMinutes() throws ParseException {
        final IntegrationProductDto dto = createTestDto(false, true);
        dto.setPreOrderEmbargoReleaseDate("2018-02-05 00:00:00");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue(response.isSuccessStatus());
        final TargetColourVariantProductModel product = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        final Date expectedDate = new SimpleDateFormat(DATETIME_FORMAT).parse("2018-02-05 03:00:00");
        assertThat(product.getNormalSaleStartDateTime()).isEqualTo(expectedDate);
    }

    @Test
    public void testPreOrderEmbargoReleaseDateWithNoHoursButMinutesPopulated() throws ParseException {
        final IntegrationProductDto dto = createTestDto(false, true);
        dto.setPreOrderEmbargoReleaseDate("2018-02-05 00:29:00");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue(response.isSuccessStatus());
        final TargetColourVariantProductModel product = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        assertThat(product.getNormalSaleStartDateTime())
                .isEqualTo(new SimpleDateFormat(DATETIME_FORMAT).parse(dto.getPreOrderEmbargoReleaseDate()));
    }

    @Test
    public void testPreOrderEmbargoReleaseDateWithNoMinutesButHourssPopulated() throws ParseException {
        final IntegrationProductDto dto = createTestDto(false, true);
        dto.setPreOrderEmbargoReleaseDate("2018-02-05 01:00:00");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertTrue(response.isSuccessStatus());
        final TargetColourVariantProductModel product = (TargetColourVariantProductModel)productService
                .getProductForCode(dto.getVariantCode());

        assertThat(product.getNormalSaleStartDateTime())
                .isEqualTo(new SimpleDateFormat(DATETIME_FORMAT).parse(dto.getPreOrderEmbargoReleaseDate()));
    }

    private void verifyStockLevelForWarehouse(final AbstractTargetVariantProductModel variant,
            final String warehouse) {
        final Collection<StockLevelModel> stockLevels = targetStockService.getAllStockLevels(variant);
        Assert.assertTrue(CollectionUtils.isNotEmpty(stockLevels));
        Assert.assertTrue(stockLevels.size() == 1);
        Assert.assertTrue(stockLevels.iterator().next().getWarehouse().getCode().equalsIgnoreCase(warehouse));
    }

    private void setWarehousesInRequestDto(final IntegrationProductDto dto, final String warehouseCode) {
        dto.setWarehouses(Collections.singletonList(warehouseCode));
    }

}
