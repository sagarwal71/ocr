/**
 * 
 */
package au.com.target.tgtwsfacades.activation.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtwsfacades.activation.ActivationIntegrationFacade;
import au.com.target.tgtwsfacades.integration.dto.IntegrationActivationDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;


/**
 * @author mmaki
 * 
 */
@IntegrationTest
public class TargetActivationIntegrationFacadeTest extends ServicelayerTransactionalTest {

    @Resource
    private ActivationIntegrationFacade activationIntegrationFacade;

    @Resource
    private ProductService productService;

    @Resource
    private UserService userService;

    @Resource
    private CommonI18NService commonI18NService;

    @Resource
    private ModelService modelService;

    @Before
    public void initialise() throws ImpExException {
        userService.setCurrentUser(userService.getAdminUser());
        final CurrencyModel currencyModel = commonI18NService.getCurrency("AUD");
        final Currency currency = modelService.getSource(currencyModel);
        JaloSession.getCurrentSession().getSessionContext().setCurrency(currency);
        importCsv("/tgtwsfacades/test/testActivationIntegrationFacadeOnline.impex", Charsets.UTF_8.name());
    }

    /**
     * Test method for {@link au.com.target.tgtwsfacades.activation.impl.TargetProductActivationIntegrationFacade} .
     * 
     * @throws InstantiationException
     */
    @Test
    public void testPersistActivationStatusActiveSuccessfullyForSizeVariant() throws InstantiationException {
        final String variantCode = "TEST1001_1_1";

        final IntegrationActivationDto dto = new IntegrationActivationDto();
        dto.setProductCode(null);
        dto.setVariantCode(variantCode);
        dto.setApprovalStatus("Active");

        final IntegrationResponseDto response = activationIntegrationFacade.updateApprovalStatus(dto);
        assertThat(response.getCode()).isEqualTo(variantCode).as("Code incorrect, ");
        assertThat(response.isSuccessStatus()).as("Variant approvalStatus not updated").isTrue();

        final TargetSizeVariantProductModel product = (TargetSizeVariantProductModel)productService
                .getProductForCode(variantCode);
        assertThat(product.getApprovalStatus()).isEqualTo(ArticleApprovalStatus.APPROVED)
                .as("Approval status incorrect, ");
    }

    /**
     * Test method for {@link au.com.target.tgtwsfacades.activation.impl.TargetProductActivationIntegrationFacade}.
     * 
     * @throws InstantiationException
     */
    @Test
    public void testPersistActivationStatusInactiveSuccessfullyForSizeVariant() throws InstantiationException {
        final String variantCode = "TEST1001_1_1";
        final IntegrationActivationDto dto = new IntegrationActivationDto();
        dto.setProductCode(null);
        dto.setVariantCode(variantCode);
        dto.setApprovalStatus("Inactive");

        final IntegrationResponseDto response = activationIntegrationFacade.updateApprovalStatus(dto);
        assertThat(response.getCode()).isEqualTo(variantCode).as("Code incorrect, ");
        assertThat(response.isSuccessStatus()).as("Product approvalStatus not updated").isTrue();

        final TargetSizeVariantProductModel product = (TargetSizeVariantProductModel)productService
                .getProductForCode(variantCode);
        assertThat(product.getApprovalStatus()).isEqualTo(ArticleApprovalStatus.UNAPPROVED)
                .as("Approval status incorrect, ");
    }

    /**
     * Test method for {@link au.com.target.tgtwsfacades.activation.impl.TargetProductActivationIntegrationFacade}.
     * 
     * @throws InstantiationException
     */
    @Test
    public void testPersistActivationStatusActiveSuccessfullyForColourVariant() throws InstantiationException {
        final String productCode = "TEST1001";
        final String variantCode = "TEST1001_1";

        final IntegrationActivationDto dto = new IntegrationActivationDto();
        dto.setProductCode(productCode);
        dto.setVariantCode(variantCode);
        dto.setApprovalStatus("Active");

        final IntegrationResponseDto response = activationIntegrationFacade.updateApprovalStatus(dto);
        assertThat(response.getCode()).isEqualTo(variantCode).as("Code incorrect, ");
        assertThat(response.isSuccessStatus()).as("Variant approvalStatus not updated").isTrue();

        // default status of size variant set in IMPEX file is CHECK, hence colour variant will be UNAPPROVED
        final TargetColourVariantProductModel product = (TargetColourVariantProductModel)productService
                .getProductForCode(variantCode);
        assertThat(product.getApprovalStatus()).isEqualTo(ArticleApprovalStatus.UNAPPROVED)
                .as("Approval status incorrect, ");
    }

    /**
     * Test method for {@link au.com.target.tgtwsfacades.activation.impl.TargetProductActivationIntegrationFacade}.
     * 
     * @throws InstantiationException
     */
    @Test
    public void testPersistActivationStatusInactiveSuccessfullyForColourVariant() throws InstantiationException {
        final String productCode = "TEST1001";
        final String variantCode = "TEST1001_1";

        final IntegrationActivationDto dto = new IntegrationActivationDto();
        dto.setProductCode(productCode);
        dto.setVariantCode(variantCode);
        dto.setApprovalStatus("Inactive");

        final IntegrationResponseDto response = activationIntegrationFacade.updateApprovalStatus(dto);
        assertThat(response.getCode()).isEqualTo(variantCode).as("Code incorrect, ");
        assertThat(response.isSuccessStatus()).as("Product approvalStatus not updated").isTrue();

        final TargetColourVariantProductModel product = (TargetColourVariantProductModel)productService
                .getProductForCode(variantCode);
        assertThat(product.getApprovalStatus()).isEqualTo(ArticleApprovalStatus.UNAPPROVED)
                .as("Approval status incorrect, ");
    }

    /**
     * Test method for {@link au.com.target.tgtwsfacades.activation.impl.TargetProductActivationIntegrationFacade}.
     * 
     * @throws InstantiationException
     */
    @Test
    public void testPersistActivationStatusActiveForStyleGroupSizesOnlyProduct() throws InstantiationException {
        final String productCode = "WTEST1002";
        final String colourVariantCode = "TEST1002";
        final String sizeVariantCode = "TEST1002_1_1";

        final IntegrationActivationDto dto = new IntegrationActivationDto();
        dto.setProductCode(productCode);
        dto.setVariantCode(sizeVariantCode);
        dto.setApprovalStatus("Active");

        final IntegrationResponseDto response = activationIntegrationFacade.updateApprovalStatus(dto);
        assertThat(response.getCode()).isEqualTo(sizeVariantCode).as("Code incorrect, ");
        assertThat(response.isSuccessStatus()).as("Variant approvalStatus not updated").isTrue();

        final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)productService
                .getProductForCode(sizeVariantCode);
        final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productService
                .getProductForCode(colourVariantCode);
        assertThat(sizeVariant.getApprovalStatus()).isEqualTo(ArticleApprovalStatus.APPROVED)
                .as("Approval status incorrect, ");
        assertThat(colourVariant.getApprovalStatus()).isEqualTo(ArticleApprovalStatus.APPROVED)
                .as("Approval status incorrect, ");
    }

    /**
     * Test method for {@link au.com.target.tgtwsfacades.activation.impl.TargetProductActivationIntegrationFacade}.
     * 
     * @throws InstantiationException
     */
    @Test
    public void testPersistActivationStatusForStyleGroupSizesOnlyProductWhereColourVariantDoesNotExist()
            throws InstantiationException {
        final String productCode = "TEST1001";
        final String sizeVariantCode = "TEST1001_1_1";
        //this test will try to look for the colour variant with the code "EST1001" derived from the productCode, which does not exist

        final IntegrationActivationDto dto = new IntegrationActivationDto();
        dto.setProductCode(productCode);
        dto.setVariantCode(sizeVariantCode);
        dto.setApprovalStatus("Active");

        final IntegrationResponseDto response = activationIntegrationFacade.updateApprovalStatus(dto);
        assertThat(response.getCode()).isEqualTo(sizeVariantCode).as("TEST1001_1_1");
        assertThat(response.isSuccessStatus()).as("Colour variant approvalStatus updated").isTrue();

        final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)productService
                .getProductForCode(sizeVariantCode);
        assertThat(sizeVariant.getApprovalStatus()).isEqualTo(ArticleApprovalStatus.APPROVED)
                .as("Approval status incorrect, ");
    }

    /**
     * Test method for {@link au.com.target.tgtwsfacades.activation.impl.TargetProductActivationIntegrationFacade}.
     * 
     * @throws InstantiationException
     */
    @Test
    public void testPersistActivationStatusNotSourcedForSizeVariantThatExists() throws InstantiationException {
        final String variantCode = "TEST1001_1_1";

        final IntegrationActivationDto dto = new IntegrationActivationDto();
        dto.setVariantCode(variantCode);
        dto.setApprovalStatus("NOT SOURCED");

        final IntegrationResponseDto response = activationIntegrationFacade.updateApprovalStatus(dto);
        assertThat(response.getCode()).isEqualTo(variantCode).as("Code incorrect, ");
        assertThat(response.isSuccessStatus()).as("Product approvalStatus not updated").isTrue();

        final TargetSizeVariantProductModel product = (TargetSizeVariantProductModel)productService
                .getProductForCode(variantCode);
        assertThat(product.getApprovalStatus()).isEqualTo(ArticleApprovalStatus.CHECK)
                .as("Approval status incorrect, ");
    }

    /**
     * Test method for {@link au.com.target.tgtwsfacades.activation.impl.TargetProductActivationIntegrationFacade}.
     * 
     * @throws InstantiationException
     */
    @Test
    public void testPersistActivationStatusNotSourcedForColourVariantThatExists() throws InstantiationException {
        final String productCode = "TEST1001";
        final String variantCode = "TEST1001_1";

        final IntegrationActivationDto dto = new IntegrationActivationDto();
        dto.setProductCode(productCode);
        dto.setVariantCode(variantCode);
        dto.setApprovalStatus("NOT SOURCED");

        final IntegrationResponseDto response = activationIntegrationFacade.updateApprovalStatus(dto);
        assertThat(response.getCode()).isEqualTo(variantCode).as("Code incorrect, ");
        assertThat(response.isSuccessStatus()).as("Product approvalStatus not updated").isTrue();

        final TargetColourVariantProductModel product = (TargetColourVariantProductModel)productService
                .getProductForCode(variantCode);
        assertThat(product.getApprovalStatus()).isEqualTo(ArticleApprovalStatus.UNAPPROVED)
                .as("Approval status incorrect, ");
    }

    /**
     * Test method for {@link au.com.target.tgtwsfacades.activation.impl.TargetProductActivationIntegrationFacade}.
     * 
     * @throws InstantiationException
     */
    @Test
    public void testPersistActivationStatusNotSourcedForSizeVariantThatDoesNotExist() throws InstantiationException {
        final String variantCode = "TEST0XYZ_1_1";

        final IntegrationActivationDto dto = new IntegrationActivationDto();
        dto.setVariantCode(variantCode);
        dto.setApprovalStatus("NOT SOURCED");

        final IntegrationResponseDto response = activationIntegrationFacade.updateApprovalStatus(dto);
        assertThat(response.getCode()).isEqualTo(variantCode).as("Code incorrect, ");
        assertThat(response.isSuccessStatus()).as("Product approvalStatus updated").isFalse();
        assertThat(response.getMessages().get(0)).isEqualTo("'NOT SOURCED' product status found")
                .as("Message incorrect, ");
        assertThat(response.getMessages().get(1)).isEqualTo("Size variant does not exist").as("Message incorrect, ");
    }

    /**
     * Test method for {@link au.com.target.tgtwsfacades.activation.impl.TargetProductActivationIntegrationFacade}.
     * 
     * @throws InstantiationException
     */
    @Test
    public void testPersistActivationStatusNotSourcedForColourVariantThatDoesNotExist() throws InstantiationException {
        final String productCode = "TEST0XYZ";
        final String variantCode = "TEST0XYZ_1";

        final IntegrationActivationDto dto = new IntegrationActivationDto();
        dto.setProductCode(productCode);
        dto.setVariantCode(variantCode);
        dto.setApprovalStatus("NOT SOURCED");

        final IntegrationResponseDto response = activationIntegrationFacade.updateApprovalStatus(dto);
        assertThat(response.getCode()).isEqualTo(variantCode).as("Code incorrect, ");
        assertThat(response.isSuccessStatus()).as("Product approvalStatus updated").isFalse();
        assertThat(response.getMessages().get(0)).isEqualTo("'NOT SOURCED' product status found")
                .as("Message incorrect, ");
        assertThat(response.getMessages().get(1)).isEqualTo("Colour variant does not exist").as("Message incorrect, ");
    }

    /**
     * Test method for {@link au.com.target.tgtwsfacades.activation.impl.TargetProductActivationIntegrationFacade}.
     * 
     * @throws InstantiationException
     */
    @Test
    public void testPersistActivationStatusWhereColourVariantDoesNotExist() throws InstantiationException {
        final String productCode = "TEST0XYZ";
        final String variantCode = "TEST0XYZ_1";

        final IntegrationActivationDto dto = new IntegrationActivationDto();
        dto.setProductCode(productCode);
        dto.setVariantCode(variantCode);
        dto.setApprovalStatus("Active");

        final IntegrationResponseDto response = activationIntegrationFacade.updateApprovalStatus(dto);
        assertThat(response.getCode()).isEqualTo(variantCode).as("Code incorrect, ");
        assertThat(response.isSuccessStatus()).as("Product approvalStatus updated").isFalse();
        assertThat(response.getMessages().get(0)).isEqualTo("Colour variant does not exist").as("Message incorrect, ");
    }

    /**
     * Test method for {@link au.com.target.tgtwsfacades.activation.impl.TargetProductActivationIntegrationFacade}.
     * 
     * @throws InstantiationException
     */
    @Test
    public void testPersistActivationStatusWhereSizeVariantDoesNotExist() throws InstantiationException {
        final String variantCode = "TEST0XYZ_1_1";

        final IntegrationActivationDto dto = new IntegrationActivationDto();
        dto.setProductCode(null);
        dto.setVariantCode(variantCode);
        dto.setApprovalStatus("Active");

        final IntegrationResponseDto response = activationIntegrationFacade.updateApprovalStatus(dto);
        assertThat(response.getCode()).isEqualTo(variantCode).as("Code incorrect, ");
        assertThat(response.isSuccessStatus()).as("Product approvalStatus updated").isFalse();
        assertThat(response.getMessages().get(0)).isEqualTo("Size variant does not exist").as("Message incorrect, ");
    }

}
