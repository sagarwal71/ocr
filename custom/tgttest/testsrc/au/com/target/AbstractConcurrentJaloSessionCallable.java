package au.com.target;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.Tenant;
import de.hybris.platform.jalo.JaloSession;

import java.util.concurrent.Callable;


/**
 * An abstraction for a callable from which an operation that needs a JaloSession can be executed. Can be used for tests
 * that need to verify concurrency.
 * 
 * @see <a href=
 *      "https://wiki.hybris.com/pages/viewpage.action?pageId=39158263&focusedCommentId=53838311#comment-53838311">Hybris
 *      Wiki Reference</a>
 * 
 * @author mgazal
 *
 */
public abstract class AbstractConcurrentJaloSessionCallable<T> implements Callable<T> {


    protected final JaloSession session = JaloSession.getCurrentSession();

    protected final Tenant tenant = Registry.getCurrentTenant();

    protected void pre() {
        Registry.setCurrentTenant(tenant);
        this.session.activate();
    }

    @Override
    public T call() {
        try {
            pre();
            return execute();
        }
        catch (final Exception e) {
            throw new RuntimeException(e);
        }
        finally {
            post();
        }
    }

    protected abstract T execute() throws Exception;

    protected void post() {
        JaloSession.deactivate();
        Registry.unsetCurrentTenant();
    }

}
