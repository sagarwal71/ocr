/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtfacades.suggestion.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;

import org.apache.commons.lang.math.NumberUtils;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtfacades.suggestion.SimpleSuggestionFacade;

import com.google.common.base.Charsets;


/**
 * Integration test suite for {@link DefaultSimpleSuggestionFacade}.
 */
@IntegrationTest
public class DefaultSimpleSuggestionFacadeIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private SimpleSuggestionFacade simpleSuggestionFacade;

    @Resource
    private UserService userService;

    @Before
    public void setUp() throws ImpExException {
        importCsv("/tgtcore/test/testSystemSetup.impex", Charsets.UTF_8.name());
        importCsv("/tgtcore/test/testSimpleSuggestions.impex", Charsets.UTF_8.name());
    }

    @Test
    public void testReferencesForPurchasedInCategory() {
        final CustomerModel user = (CustomerModel)userService.getUserForUID("testuser");
        userService.setCurrentUser(user);

        List<ProductData> result = simpleSuggestionFacade.getReferencesForPurchasedInCategory("cameras", null, false,
                null);
        Assert.assertEquals(4, result.size());

        result = simpleSuggestionFacade.getReferencesForPurchasedInCategory("cameras", null, false,
                NumberUtils.INTEGER_ONE);
        Assert.assertEquals(1, result.size());

        result = simpleSuggestionFacade.getReferencesForPurchasedInCategory("cameras",
                ProductReferenceTypeEnum.SIMILAR, false,
                null);
        Assert.assertEquals(1, result.size());

        result = simpleSuggestionFacade.getReferencesForPurchasedInCategory("cameras",
                ProductReferenceTypeEnum.ACCESSORIES, false,
                null);
        Assert.assertEquals(2, result.size());

        result = simpleSuggestionFacade.getReferencesForPurchasedInCategory("cameras",
                ProductReferenceTypeEnum.ACCESSORIES, true,
                null);
        Assert.assertEquals(1, result.size());

        final ProductData product = result.get(0);
        Assert.assertEquals("adapterDC", product.getCode());
        Assert.assertEquals("adapter", product.getName());
    }
}
