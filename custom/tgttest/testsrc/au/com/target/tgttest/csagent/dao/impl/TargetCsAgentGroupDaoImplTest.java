/**
 * 
 */
package au.com.target.tgttest.csagent.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.ticket.model.CsAgentGroupModel;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.tgtcs.csagent.dao.TargetCsAgentGroupDao;


/**
 * @author mjanarth
 *
 */
@IntegrationTest
public class TargetCsAgentGroupDaoImplTest extends ServicelayerTransactionalTest {
    @Resource
    private TargetCsAgentGroupDao targetCsAgentGroupDao;

    @Before
    public void setUp() throws ImpExException {
        importCsv("/tgtcs/test/testCsAgentGroup.impex", Charsets.UTF_8.name());
    }

    @Test
    public void testGetAllCsAgentGroups() {
        final List<CsAgentGroupModel> csAgentGrps = targetCsAgentGroupDao.findAllCsAgentGroup();
        assertThat(csAgentGrps).isNotEmpty();
        assertThat(csAgentGrps).onProperty("uid").contains("fraudAgentGroup", "trademe-csagentgroup",
                "ebay-csagentgroup",
                "refundfailures-csagentgroup");

    }

}
