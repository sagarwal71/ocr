/**
 * 
 */
package au.com.target.tgttest.automation.util;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;

import au.com.target.tgttest.automation.util.ComplexDateExpressionParser.DateInterpreterResult;


/**
 * @author sbryan6
 *
 */
@UnitTest
public class ComplexDateExpressionParserTest {

    private final ComplexDateExpressionParser dateUtil = new ComplexDateExpressionParser();

    @Test
    public void testParseHistoricalDateString() {

        DateInterpreterResult result = dateUtil.parseHistoricalDateString("yesterday at 12:15");
        assertThat(result.isHasTime()).as("has time").isTrue();
        assertThat(result.getDaysAgo()).as("days ago").isEqualTo(1);
        assertThat(result.getHour()).as("hour").isEqualTo(12);
        assertThat(result.getMin()).as("min").isEqualTo(15);

        result = dateUtil.parseHistoricalDateString("3 days ago");
        assertThat(result.isHasTime()).as("has time").isFalse();
        assertThat(result.getDaysAgo()).as("days ago").isEqualTo(3);
        assertThat(result.getHour()).as("hour").isEqualTo(0);
        assertThat(result.getMin()).as("min").isEqualTo(0);

        result = dateUtil.parseHistoricalDateString("3 days ago at 12:15");
        assertThat(result.isHasTime()).as("has time").isTrue();
        assertThat(result.getDaysAgo()).as("days ago").isEqualTo(3);
        assertThat(result.getHour()).as("hour").isEqualTo(12);
        assertThat(result.getMin()).as("min").isEqualTo(15);

        result = dateUtil.parseHistoricalDateString("3 days ago plus 7 seconds");
        assertThat(result.isHasTime()).as("has time").isFalse();
        assertThat(result.getDaysAgo()).as("days ago").isEqualTo(3);
        assertThat(result.getHour()).as("hour").isEqualTo(0);
        assertThat(result.getMin()).as("min").isEqualTo(0);
        assertThat(result.getSecsDifference()).as("secsDifference").isEqualTo(7);

        result = dateUtil.parseHistoricalDateString("3 days ago minus 8 seconds");
        assertThat(result.isHasTime()).as("has time").isFalse();
        assertThat(result.getDaysAgo()).as("days ago").isEqualTo(3);
        assertThat(result.getHour()).as("hour").isEqualTo(0);
        assertThat(result.getMin()).as("min").isEqualTo(0);
        assertThat(result.getSecsDifference()).as("secsDifference").isEqualTo(-8);

        result = dateUtil.parseHistoricalDateString("3 days ago minus 1 second");
        assertThat(result.isHasTime()).as("has time").isFalse();
        assertThat(result.getDaysAgo()).as("days ago").isEqualTo(3);
        assertThat(result.getHour()).as("hour").isEqualTo(0);
        assertThat(result.getMin()).as("min").isEqualTo(0);
        assertThat(result.getSecsDifference()).as("secsDifference").isEqualTo(-1);

        result = dateUtil.parseHistoricalDateString("3 days ago plus 1 second");
        assertThat(result.isHasTime()).as("has time").isFalse();
        assertThat(result.getDaysAgo()).as("days ago").isEqualTo(3);
        assertThat(result.getHour()).as("hour").isEqualTo(0);
        assertThat(result.getMin()).as("min").isEqualTo(0);
        assertThat(result.getSecsDifference()).as("secsDifference").isEqualTo(1);

    }

    @Test
    public void testInterpretStringAsDateNull() {

        assertThat(dateUtil.interpretStringAsDate("null")).isNull();
        assertThat(dateUtil.interpretStringAsDate("null ")).isNull();
        assertThat(dateUtil.interpretStringAsDate(null)).isNull();
        assertThat(dateUtil.interpretStringAsDate("")).isNull();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseDayAndTimeToDate() {
        dateUtil.parseDayAndTimeToDate("some", "rubbish");
    }

    @Test
    public void testParseDayAndTimeToDateToday() {
        final Date currDate = getDateByHourAndMinutes(10, 30);
        final Date parsedDate = dateUtil.parseDayAndTimeToDate("today", "10:30AM");

        assertThat(DateUtils.isSameDay(currDate, parsedDate)).isTrue();
        assertThat(DateUtils.toCalendar(currDate).get(Calendar.HOUR_OF_DAY)).isEqualTo(
                DateUtils.toCalendar(parsedDate).get(Calendar.HOUR_OF_DAY));
        assertThat(DateUtils.toCalendar(currDate).get(Calendar.MINUTE)).isEqualTo(
                DateUtils.toCalendar(parsedDate).get(Calendar.MINUTE));
    }

    @Test
    public void testParseDayAndTimeToDateTomorrow() {
        final Date currDate = getDateByHourAndMinutes(9, 20);
        final Date parsedDate = dateUtil.parseDayAndTimeToDate("tomorrow", "09:20AM");

        assertThat(DateUtils.isSameDay(currDate, parsedDate)).isFalse();
        assertThat(DateUtils.toCalendar(currDate).get(Calendar.HOUR_OF_DAY)).isEqualTo(
                DateUtils.toCalendar(parsedDate).get(Calendar.HOUR_OF_DAY));
        assertThat(DateUtils.toCalendar(currDate).get(Calendar.MINUTE)).isEqualTo(
                DateUtils.toCalendar(parsedDate).get(Calendar.MINUTE));
    }

    @Test
    public void testParseDayAndTimeToDateYesterday() {
        final Date currDate = getDateByHourAndMinutes(20, 10);
        final Date parsedDate = dateUtil.parseDayAndTimeToDate("yesterday", "08:10PM");

        assertThat(DateUtils.isSameDay(currDate, parsedDate)).isFalse();
        assertThat(DateUtils.toCalendar(currDate).get(Calendar.HOUR_OF_DAY)).isEqualTo(
                DateUtils.toCalendar(parsedDate).get(Calendar.HOUR_OF_DAY));
        assertThat(DateUtils.toCalendar(currDate).get(Calendar.MINUTE)).isEqualTo(
                DateUtils.toCalendar(parsedDate).get(Calendar.MINUTE));
    }

    private Date getDateByHourAndMinutes(final int hour, final int minute) {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        final Date currDate = calendar.getTime();
        return currDate;
    }

}
