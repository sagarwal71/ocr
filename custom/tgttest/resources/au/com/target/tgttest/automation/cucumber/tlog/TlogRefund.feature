@tlog @voucherData @cartProductData @deliveryModeData @initPreOrderProducts 
Feature: TLOG - Cancelling or refunding orders with flybuys creates tlog refund entries
    In order to integrate with POS As online operations I want REFUND TLOG entries to be created for cancellations and refunds with flybuys
  
  Note: some of these scenarios are currently failing pending fixing in OCR-8756
  Note: Some of these scenarios related to deals are pulled back from the current sprint.i.e., sprint 25. So those scenarios are marked as @notAutomated

  Background: 
    Given giftcard/creditcard details:
      | paymentId | cardType | cardNumber       | cardExpiry | token     | tokenExpiry | bin |
      | CC1       | VISA     | 4987654321010012 | 01/20      | 123456789 | 01/01/2020  |  35 |

  @notAutomated
  Scenario Outline: Partial cancel on Reward item on order with buy get deal items ,TMD and flybuys redemption.
    Given set deal parameters:
      | name   | rewardType     | rewardValue | rewardMaxQty |
      | buyget | PercentOffEach |          25 |            2 |
    And cart entries with deals:
      | product  | qty | price | dealName | dealRole |
      | 10000011 |   2 |    25 | buyget   | Q        |
      | 10000012 |   2 |    25 | buyget   | R        |
    And registered user checkout
    And customer presents tmd 'valid'
    And delivery mode is 'home-delivery'
    And the customer redeems 2000 flybuys points
    And payment method is '<payment_method>'
    When order is placed
    And cancel entries
      | product  | qty |
      | 10000012 |   1 |
    Then tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -1594
    And flybuys number is '6008943218616910'
    And tlog entries are:
      | code     | qty | price | filePrice |
      | 10000012 |   1 | -1594 |      2500 |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  @notAutomated
  Scenario Outline: Partial cancel on qualifier and reward on  order with buy get deal items ,TMD and flybuys redemption.
    Given set deal parameters:
      | name   | rewardType     | rewardValue | rewardMaxQty |
      | buyget | PercentOffEach |          25 |            2 |
    And cart entries with deals:
      | product  | qty | price | dealName | dealRole |
      | 10000011 |   2 |    25 | buyget   | Q        |
      | 10000012 |   2 |    25 | buyget   | R        |
    And registered user checkout
    And customer presents tmd 'valid'
    And delivery mode is 'home-delivery'
    And the customer redeems 2000 flybuys points
    And payment method is '<payment_method>'
    When order is placed
    And cancel entries
      | product  | qty |
      | 10000012 |   1 |
      | 10000011 |   1 |
    Then tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -3719
    And flybuys number is '6008943218616910'
    And tlog entries are:
      | code     | qty | price | filePrice |
      | 10000011 |   1 | -1859 |      2500 |
      | 10000012 |   1 | -1860 |      2500 |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  @notAutomated
  Scenario Outline: Full cancel on order  with buy get deal items ,TMD and flybuys redemption.
    Given set deal parameters:
      | name   | rewardType     | rewardValue | rewardMaxQty |
      | buyget | PercentOffEach |          25 |            2 |
    And cart entries with deals:
      | product  | qty | price | dealName | dealRole |
      | 10000012 |   2 |    25 | buyget   | R        |
      | 10000011 |   2 |    25 | buyget   | Q        |
    And registered user checkout
    And customer presents tmd 'valid'
    And delivery mode is 'home-delivery'
    And the customer redeems 2000 flybuys points
    And payment method is '<payment_method>'
    When order is placed
    And the order is fully cancelled
    Then tlog type is 'refund'
    And tlog shipping is 000
    And transDiscount entries are:
      | type    | code             | points | amount |
      | flybuys | 6008943218616910 |   2000 |   1000 |
    And tlog total tender is -6438
    And flybuys number is '6008943218616910'
    And tlog entries are:
      | code     | qty | price | filePrice |
      | 10000012 |   2 | -1859 |      2500 |
      | 10000011 |   1 | -1860 |      2500 |
      | 10000011 |   1 | -1861 |      2500 |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Partial cancel on qualifier on an order with buy get deal items and flybuys redemption,
    Given set deal parameters:
      | name   | rewardType     | rewardValue | rewardMaxQty |
      | buyget | PercentOffEach |          25 |            2 |
    And cart entries with deals:
      | product  | qty | price | dealName | dealRole |
      | 10000012 |   2 |    25 | buyget   | R        |
      | 10000011 |   2 |    25 | buyget   | Q        |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And the customer redeems 2000 flybuys points
    And payment method is '<payment_method>'
    When order is placed
    And cancel entries
      | product  | qty |
      | 10000011 |   1 |
    Then tlog entries are:
      | code     | qty | price | filePrice |
      | 10000011 |   1 | -2500 |      2500 |
    And tlog type is 'refund'
    And tlog shipping is 000
    And flybuys number is '6008943218616910'
    And tlog total tender is -2500

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  @notAutomated
  Scenario Outline: Partial cancel on Reward and qualifier on an order with buy get deal items and flybuys redemption
    Given set deal parameters:
      | name   | rewardType     | rewardValue | rewardMaxQty |
      | buyget | PercentOffEach |          25 |            2 |
    And cart entries with deals:
      | product  | qty | price | dealName | dealRole |
      | 10000012 |   2 |    25 | buyget   | R        |
      | 10000011 |   2 |    25 | buyget   | Q        |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And the customer redeems 2000 flybuys points
    And payment method is '<payment_method>'
    When order is placed
    And cancel entries
      | product  | qty |
      | 10000011 |   1 |
      | 10000012 |   1 |
    Then tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -4375
    And flybuys number is '6008943218616910'
    And tlog entries are:
      | code     | qty | price | filePrice |
      | 10000012 |   1 | -2187 |      2500 |
      | 10000011 |   1 | -2188 |      2500 |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  @notAutomated
  Scenario Outline: Full cancel on order with buy get deal items and flybuys redemption
    Given set deal parameters:
      | name   | rewardType     | rewardValue | rewardMaxQty |
      | buyget | PercentOffEach |          25 |            2 |
    And cart entries with deals:
      | product  | qty | price | dealName | dealRole |
      | 10000012 |   2 |    25 | buyget   | R        |
      | 10000011 |   2 |    25 | buyget   | Q        |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And the customer redeems 2000 flybuys points
    And payment method is '<payment_method>'
    When order is placed
    And the order is fully cancelled
    Then transDiscount entries are:
      | type    | code             | points | amount |
      | flybuys | 6008943218616910 |   2000 |   1000 |
    And tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -7750
    And flybuys number is '6008943218616910'
    And tlog entries are:
      | code     | qty | price | filePrice |
      | 10000012 |   2 | -2187 |      2500 |
      | 10000011 |   1 | -2188 |      2500 |
      | 10000011 |   1 | -2189 |      2500 |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  @notAutomated
  Scenario Outline: Partial cancel on an order with TMD discount and flybuys redemption
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 |   2 |    25 |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And customer presents tmd 'valid'
    And the customer redeems 2000 flybuys points
    And payment method is '<payment_method>'
    When order is placed
    And cancel entries
      | product  | qty |
      | 10000011 |   1 |
    Then tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -2125
    And flybuys number is '6008943218616910'
    And tlog entries are:
      | code     | qty | price | filePrice |
      | 10000011 |   1 | -2125 |      2500 |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Full cancel on an order with TMD discount and flybuys redemption
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 |   2 |    50 |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And customer presents tmd 'valid'
    And the customer redeems 2000 flybuys points
    And payment method is '<payment_method>'
    When order is placed
    And the order is fully cancelled
    Then tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -7000
    And flybuys number is '6008943218616910'
    And transDiscount entries are:
      | type    | code             | points | amount |
      | flybuys | 6008943218616910 |   2000 |   1000 |
    And tlog entries are:
      | code     | qty | price | filePrice | itemDiscountAmount |
      | 10000011 |   2 | -5000 |      5000 |               1500 |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  @toFix
  Scenario Outline: Full cancel on an order with only TMD discount
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 |   5 |    25 |
      | 10000012 |   3 |    50 |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And customer presents tmd 'valid'
    And payment method is '<payment_method>'
    When order is placed
    And the order is fully cancelled
    Then tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -23375
    And tlog entries are:
      | code     | qty | price  | filePrice | itemDiscount |
      | 10000011 |   5 | -10625 |      2500 |         1875 |
      | 10000012 |   2 |  -8500 |      5000 |         1500 |
      | 10000012 |   1 |  -4250 |      5000 |          750 |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  @toFix
  Scenario Outline: Partial cancel on an order with only TMD discount
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 |   5 |    25 |
      | 10000012 |   3 |    50 |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And customer presents tmd 'valid'
    And payment method is '<payment_method>'
    When order is placed
    And cancel entries
      | product  | qty |
      | 10000011 |   3 |
      | 10000012 |   2 |
    Then tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -14875
    And tlog entries are:
      | code     | qty | price | filePrice | itemDiscount |
      | 10000011 |   3 | -6375 |      2500 |         1125 |
      | 10000012 |   1 | -4250 |      5000 |          750 |
      | 10000012 |   1 | -4250 |      5000 |          750 |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Partial cancel on order  with flybuys redemption
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 |   2 |    25 |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And the customer redeems 2000 flybuys points
    And payment method is '<payment_method>'
    When order is placed
    And cancel entries
      | product  | qty |
      | 10000011 |   1 |
    Then tlog entries are:
      | code     | qty | price | filePrice |
      | 10000011 |   1 | -2500 |      2500 |
    And flybuys number is '6008943218616910'
    And tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -2500

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Full cancel on order Placing an order with flybuys redeem
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 |   2 |    50 |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And the customer redeems 2000 flybuys points
    And payment method is '<payment_method>'
    When order is placed
    And the order is fully cancelled
    Then tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -9000
    And flybuys number is '6008943218616910'
    And transDiscount entries are:
      | type    | code             | points | amount |
      | flybuys | 6008943218616910 |   2000 |   1000 |
    And tlog entries are:
      | code     | qty | price | filePrice |
      | 10000011 |   2 | -5000 |      5000 |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario: Short pick on order with single card payment, where one refund to credit card fails
    Given customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | CC1,30       |
    And a consignment sent to fastline with products:
      | product  | qty | price |
      | 10000011 |   2 |    15 |
    And refund for credit card '4987654321010012' is declined
    When fastline returns pick entry data:
      | product  | qty |
      | 10000011 |   1 |
    Then tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -1500
    And tlog tender entries are:
      | cardNumber | approved | amount | type |
      | null       | true     |    -15 | cash |

  Scenario: Full cancel on placing an order with afterpay
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 |   2 |    50 |
    And registered user checkout
    And user checkout via spc
    And delivery mode is 'home-delivery'
    And payment method is 'afterpay'
    When order is placed
    And the afterpay refund will 'succeed' for the order
    And the order is fully cancelled
    Then tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -10000
    And tlog tender type is 'AFTERPAY'
    And tlog afterpay refund info is valid

  Scenario: Full cancel on placing an order with zip payment
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 |   2 |    50 |
    And registered user checkout
    And user checkout via spc
    And delivery mode is 'home-delivery'
    And payment method is 'zippay'
    When order is placed
    And zip create refund api returns:
      | id                        |
      | rf_H5J24ZnqpKMByItvF2Jh85 |
    And the order is fully cancelled
    And the zip refund has been trigerred for the order with amount '100'
    And the zip refund has been 'succeeded'
    Then tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -10000
    And tlog tender type is 'ZIP'
    And tlog zip payment refund id is 'rf_H5J24ZnqpKMByItvF2Jh85'

  Scenario: Full cancel on order with preOrder items
    Given a cart with entries:
      | product          | qty | price |
      | V5555_preOrder_5 |   2 |    36 |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And payment method is 'ipg'
    When pre order is placed
    And the order is fully cancelled
    And tlog type is 'laybyCancel'
    And tlog total tender is -1000
    And tlog tender entries are:
      | cardNumber       | approved | amount | type |
      | 5123450000000346 | true     |    -10 | eft  |

    
   Scenario Outline: Full cancel on order to check tlog reason
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 50    |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And payment method is '<payment_method>'
    When order is placed
    And the order is fully cancelled with the reason 'FRAUDCHECKREJECTED'
    Then tlog type is 'refund'
    And tlog entries reason is '20' 
    
    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |
    
   Scenario Outline: Full cancel on order to check tlog reason
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 50    |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And payment method is '<payment_method>'
    When order is placed
    And the order is fully cancelled with the reason 'CUSTOMERREQUEST'
    Then tlog type is 'refund'
    And tlog entries reason is '40' 
    
    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |
    
    Scenario Outline: Full cancel on order to check tlog reason
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 50    |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And payment method is '<payment_method>'
    When order is placed
    And the order is fully cancelled with the reason 'RECALL'
    Then tlog type is 'refund'
    And tlog entries reason is '90' 
    
    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |
  
    Scenario Outline: Return an order to check tlog refund reason
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 10    |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And payment method is '<payment_method>'
    When order is placed
    And return entries
      | product       | qty | returnReason       |
      | 10000011      | 1   | CHANGEOFMINDRETURN |
   
    Then tlog type is 'refund'
    And tlog entries reason is '40' 
    
    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

 Scenario Outline: Return an order to check tlog refund reason
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 10    |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And payment method is '<payment_method>'
    When order is placed
    And return entries
      | product       | qty | returnReason       |
      | 10000011      | 1   | PRICEMATCH         |
   
    Then tlog type is 'refund'
    And tlog entries reason is '20' 
    
    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

