@notAutomated
Feature: IPG - Payment information in my account order details page
  As a registered online customer
  I want to know details of payments I used on the my account order details page
  So that I can be certain on payments I made

  Scenario Outline: Display PayPal payment details on my account order details page
    Given order with total <totalAmount>
    And customer paid with PayPal payment method <paypalAccount>
    And paypal receipt number is <receiptNumber>
    When customer opens order details in my account page
    Then <paypalAccount> is displayed in Payment Details section
    And <totalAmount> is displayed in Payment Details section
    And <receiptNumber> is displayed in Payment Details section

    Examples: 
      | totalAmount | paypalAccount     | receiptNumber |
      | $10.00      | paypal@paypal.com | PP1346789     |

  Scenario Outline: Display credit card payment details on my account order details page
    Given order with total <totalAmount>
    And customer paid with credit card; <cardType>, <cardNumber>, <cardExpiry>
    And payment receipt number is <receiptNumber>
    When customer opens order details in my account page
    Then <cardType> is displayed in Payment Details section
    And <maskedCreditCardNumber> is displayed in Payment Details section
    And <cardExpiry> is displayed in Payment Details section
    And <totalAmount> is displayed in Payment Details section
    And <receiptNumber> is displayed in Payment Details section

    Examples: 
      | totalAmount | cardType    | cardNumber       | maskedCardNumber | cardExpiry | receiptNumber |
      | $10.00      | Diners Club | 3676541234560036 | 367654******0036 | 02/17      | 123465789     |

  Scenario Outline: Display gift card payment details on my account order details page
    Given order with total <totalAmount>
    And customer paid with gift card <cardNumber>
    And payment receipt number is <receiptNumber>
    When customer opens order details in my account page
    Then <maskedCardNumber> is displayed in Payment Details section
    And <totalAmount> is displayed in Payment Details section
    And <receiptNumber> is displayed in Payment Details section

    Examples: 
      | totalAmount | cardNumber       | maskedCardNumber | receiptNumber |
      | $10.00      | 6273451234568796 | 627345******8796 | 123465789     |

  Scenario Outline: Display split payment details on my account order details page
    Given customer paid with split payments
      | payment1   | payment2   |
      | <payment1> | <payment2> |
    When customer opens order details in my account page
    Then payment information is displayed in Payment Details section
      | payment1   | payment2   |
      | <payment1> | <payment2> |

    Examples: 
      | payment1                             | payment2                             |
      | GC1,627345******8796,$10.00,receipt1 | CC1,424242******4242,$15.00,receipt2 |
