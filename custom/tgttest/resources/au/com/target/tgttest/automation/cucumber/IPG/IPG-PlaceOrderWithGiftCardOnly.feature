@cartProductData
Feature: IPG - Use ipg to place order with gift card only
  In order to purchase products
  As an online customer
  I want to use my gift card to make payment via ipg, and the order will be placed according to payment result and stock level.

  Background: 
    Given set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |

  Scenario: Placing an order successfully with gift
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And guest checkout
    And payment method is 'ipg'
    And payment type is giftcard
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And order is in status 'INPROGRESS'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 2        | 2         |