@cartProductData
Feature: IPG - IPG Refund for Fraud check
  Inorder to have better shopping experience
  As an Online Store
  I want a refund as quickly as possible if my order is cancelled by a fraud check

  Background: 
    Given payment method is 'ipg'
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And giftcard/creditcard details:
      | paymentId | cardType   | cardNumber        | cardExpiry | token     | tokenExpiry | bin |
      | GC1       | Giftcard   | 62734500000000002 | 01/20      | 123456789 |             | 31  |
      | GC2       | Giftcard   | 62779575000000000 | 01/20      | 123456789 |             | 32  |
      | GC3       | Giftcard   | 62779575000000001 | 01/20      | 123456789 |             | 32  |
      | GC4       | Giftcard   | 62733500000000003 | 01/20      | 123456789 |             | 33  |
      | CC1       | VISA       | 4987654321010012  | 01/20      | 123456789 | 01/01/2020  | 35  |
      | CC2       | MASTERCARD | 5587654321010013  | 02/20      | 123456789 | 01/02/2020  | 36  |
      | CC3       | AMEX       | 349876543210010   | 03/20      | 123456789 | 01/03/2020  | 37  |
      | CC4       | DINERS     | 36765432100028    | 04/20      | 123456789 | 01/04/2020  | 38  |

  Scenario: Refund for order rejected of fraud
    Given the order would trigger an Accertify response of 'REJECT'
    And the IPG refund will 'succeed' for the order
    When order is placed
    Then order is in status 'REJECTED'
    And the IPG refund has been trigerred for the order with amount '39'

  Scenario: CS ticket raised for order when refund failed
    Given the order would trigger an Accertify response of 'REJECT'
    And the IPG refund will 'fail' for the order
    When order is placed
    Then a CS ticket is created for the order

  Scenario: Refund not triggered for Accertify Review status
    Given the order would trigger an Accertify response of 'REVIEW'
    And the IPG refund will 'succeed' for the order
    When order is placed
    Then order is in status 'REVIEW'
    And the IPG refund is not triggered for the order

  Scenario: Order in progress state when Accertify status is accept
    Given the order would trigger an Accertify response of 'ACCEPT'
    And the IPG refund will 'succeed' for the order
    When order is placed
    Then order is in status 'INPROGRESS'
    And the IPG refund is not triggered for the order

  Scenario Outline: Refund for mixed payment order rejected of fraud
    Given the order would trigger an Accertify response of 'REJECT'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    And refund for credit card 'CC2' is declined
    And refund for gift card 'GC2' is declined
    When order is placed
    Then order is in status 'REJECTED'
    And order will have successful refund:
      | PaymentEntry |
      | <refund1>    |
      | <refund2>    |
      | <refund3>    |
    And a CS ticket is <ticket_status> for the order

    Examples: 
      | payment1 | payment2 | payment3 | payment4 | refund1 | refund2 | refund3 | refund4 | ticket_status |
      | GC1,39   |          |          |          | GC1,39  |         |         |         | not created   |
      | CC1,39   |          |          |          | CC1,39  |         |         |         | not created   |
      | GC1,20   | CC1,19   |          |          | GC1,20  | CC1,19  |         |         | not created   |
      | GC2,10   | CC2,29   |          |          |         |         |         |         | created       |
      | GC2,39   |          |          |          |         |         |         |         | created       |
      | CC2,39   |          |          |          |         |         |         |         | created       |
      | GC1,10   | GC2,10   | GC3,10   | GC4,9    | GC1,10  | GC3,10  | GC4,9   |         | created       |
      | GC2,10   | GC3,10   | GC4,19   |          | GC3,10  | GC4,19  |         |         | created       |
      | GC2,10   | CC1,10   | CC2,19   |          | CC1,10  |         |         |         | created       |
