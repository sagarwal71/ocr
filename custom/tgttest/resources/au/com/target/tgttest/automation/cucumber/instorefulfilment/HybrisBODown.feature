@notAutomated
Feature: Instore Fulfilment - Error message when Hybris Back Office is down

  Scenario: Error message shown to store user when Hybris Backoffice  is down
    Given store user 'Store5032Employee'
    And the Hybris Back Office server is down
    When the store user logs in to store portal
    Then store user is presented with a message to try again later
