@notAutomated
Feature: Product Detail Page: Allow PDP to switch between Online and Instore modes

  Scenario Outline: Display tabs on PDP
    Given product <prodId> is being displayed on PDP
    And either the product is a one level product or has sellable variants associated with the product
    When the user lands on the PDP
    And for products with variants, atleast one of the sellable variants has online stock as <instockOnline>
    And product with one level has online stock as <instockOnline>
    And for products with variants, atleast one of the sellable variants has in store stock as <instockInStore>
    And for product with one level has in store stock as <instockInStore>
    And the users preferred store is <preferredStore>
    Then the text for the online tab is <onlineTabText> and the instore tab is <instoreTabText>
    And active tab, whichever is selected is indicated as <isActive>

    Examples: 
      | prodId | instockOnline | instockInStore | preferredStore | onlineTabText      | instoreTabText | isActive         |
      | SKU01  | true          | true           | Not selected   | Available online   | Find in store  | Available online |
      | SKU01  | true          | true           | Geelong        | Available online   | Geelong        | Available online |
      | SKU01  | false         | true           | Not selected   | Unavailable online | Find in store  | Find in store    |
      | SKU01  | false         | true           | Geelong        | Unavailable online | Geelong        | Geelong          |
      | SKU01  | true          | false          | Geelong        | Available online   | Not in stores  | Available online |

  Scenario: User must be able to select tabs and switch between the tabs
    Given that user is in online tab
    When user selects in store tab, if in store tab is available and not greyed out/disabled
    Then the in store tab should become active
    And online tab should inactive
