@notAutomated
Feature: Display the total count number of products on mobile view of product listing page
  As a target online customer 
  I want to view the total count number of products on the product listing page
  So that customer can know the total number of the items they selected from the facets clearly.

  Background: 
    Given user is on product list page for a Category or Brand Page or Search term
    And user views the web page from mobile device

  Scenario: Display Red Rondel in the Refine Menu
    Given user navigates into the category page Furniture
    When user clicks the "Refine" button
    Then a Red Rondel is displayed in the middle of the top Refine row
    And the Red Rondel has a total count number of the refinement results with "View Items >" underneath

  Scenario: Collapse the Refine Menu
    Given user navigates into the category page Furniture with Refine button clicked
    When user clicks anywhere on the Refine Menu
    Then the refine Menu slides to the left
    And user is presented with all listings on the selections made
    And user has the same number of items on the page, what was displayed on the Red Rondel

  Scenario: Display Category title in the Refine Menu
    Given user navigates into the category page Furniture
    When user clicks the "Refine" button
    Then a category title underneath the Refine bar is displayed
