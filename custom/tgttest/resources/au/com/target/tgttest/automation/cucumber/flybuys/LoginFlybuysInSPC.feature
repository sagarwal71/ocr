@rollback @cartProductData
Feature: Flybuys - Authenticate flybuys members
  In order to see my redeem options, as a customer I need to authenticate with flybuys.
  Note that the message codes are translated in the UI layer to messages, eg
  INVALID = The details you have supplied are invalid. Please provide the DOB and residential postcode of the primary card holder.
  UNAVAILABLE = Oops thats not supposed to happen. Please try again.
  FLYBUYS_OTHER_ERROR = There was a problem with the details you entered. Please contact flybuys directly for further explanation on 131116.

  Background: 
    Given a registered customer with a cart
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 35    |

  Scenario: Authentication details dont match
    Given the user entered flybuys details are 'valid'
    When the customer logins flybuys in spc
    Then flybuys login successfully

  Scenario: Authentication details dont match
    Given the user entered flybuys details are 'invalid'
    When the customer logins flybuys in spc
    Then a flybuys login error message is displayed to the customer 'ERR_FLYBUYS_INVALID'

  Scenario: Flybuys Offline
    Given that flybuys is offline
    And the user entered flybuys details are 'valid'
    When the customer logins flybuys in spc
    Then a flybuys login error message is displayed to the customer 'ERR_FLYBUYS_UNAVAILABLE'

  Scenario: WebMethods Offline
    Given that Webmethods is offline
    And the user entered flybuys details are 'valid'
    When the customer logins flybuys in spc
    Then a flybuys login error message is displayed to the customer 'ERR_FLYBUYS_UNAVAILABLE'

  Scenario: Lost/Stolen flybuys card
    Given that the customer enters stolen flybuys account details
    And the user entered flybuys details are 'valid'
    And the customer logins flybuys in spc
    Then a flybuys login error message is displayed to the customer 'ERR_FLYBUYS_FLYBUYSOTHERERROR'
