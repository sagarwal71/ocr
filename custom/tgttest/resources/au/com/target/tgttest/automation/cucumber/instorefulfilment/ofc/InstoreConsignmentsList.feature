@cartProductData @dummyStoreWarehouseData @cleanOrderData @cleanConsignmentData
Feature: Instore Fulfilment - Instore Consignment list
  As a store user, I want to see the orders filtered out for reject reason as cancelled by customer 
  Pagination of results.
  Default sorting by date descending
  
  Note: Assuming product 10000022 has clearance=true, 10000121 has clearance=false and 10000111 has clearance=false. Need to set the values while implementing Step definition.

  Background: 
    Given list of consignments for store '2000' are:
      | consignmentCode | consignmentStatus      | createdDate          | cancelDate           | rejectReason          |
      | autox11000011   | CANCELLED              | 7 days ago at 23:30  | 7 days ago at 23:35  | PICK_FAILED           |
      | autox11000012   | CANCELLED              | 8 days ago at 13:30  | 8 days ago at 13:35  | PICK_TIMEOUT          |
      | autox11000013   | CANCELLED              | 20 days ago at 14:26 | 20 days ago at 14:28 | REJECTED_BY_STORE     |
      | autox11000014   | CONFIRMED_BY_WAREHOUSE | yesterday at 08:00   | null                 | null                  |
      | autox11000015   | CANCELLED              | yesterday at 23:00   | yesterday at 23:15   | CANCELLED_BY_CUSTOMER |
      | autox11000016   | WAVED                  | today                | null                 | null                  |

  Scenario Outline: Show consignments for store '2000'
    When run ofc list consignments for store '2000' with offset as <Offset> and records per page as <PerPage>
    Then consignment list is ordered as <Result>

    Examples: 
      | Offset | PerPage | Result                                                                | Comment                                                                      |
      | 0      | 0       | autox11000016,autox11000014,autox11000011,autox11000012,autox11000013 | Will use default page size for retrieving records, and exclude autox11000015 |
      | 0      | 3       | autox11000016,autox11000014,autox11000011                             | From offset value 0 3 records will be retrieved                              |
      | 1      | 2       | autox11000014,autox11000011                                           | From offset value 1 2 records will be retrieved                              |

  Scenario: consignments with clearance products
    Given list of consignments for store '2000' are:
      | consignmentCode | consignmentStatus      | products                     |
      | autox11000101   | CONFIRMED_BY_WAREHOUSE | 1 of 10000022, 1 of 10000121 |
      | autox11000102   | CONFIRMED_BY_WAREHOUSE | 1 of 10000121                |
      | autox11000104   | CONFIRMED_BY_WAREHOUSE | 1 of 10000121, 1 of 10000111 |
    When run ofc list consignments for store '2000' with offset as 0 and records per page as 0
    Then consignment list contains:
      | consignmentCode | clearance |
      | autox11000101   | true      |
      | autox11000102   | false     |
      | autox11000104   | false     |

  Scenario: A single consignment with product details including clearance status
    Given list of consignments for store '2000' are:
      | consignmentCode | consignmentStatus      | products                     |
      | autox11000105   | CONFIRMED_BY_WAREHOUSE | 1 of 10000022, 1 of 10000111 |
    When run ofc list consignments for store '2000' with offset as 0 and records per page as 0
    Then consignment list contains:
      | consignmentCode | clearance |
      | autox11000105   | true      |
    And consignment 'autox11000105' contains products:
      | productCode | clearance |
      | 10000022    | true      |
      | 10000111    | false     |
