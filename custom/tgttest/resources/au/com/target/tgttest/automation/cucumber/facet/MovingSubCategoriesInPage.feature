@notAutomated
Feature: Moving of the Sub-Categories to in Page

  Scenario: Displaying of the sub-categories in the Category Page (Mobile only)
    Given I am browsing the Target website via Mobile
    When I get into the Category Page (e.g. Menu>Women>Tops>All Tops)
    And I click the Refine button
    Then I see all the sub-categories listed there
    And I only see the maximum of 6 sub-categories at first
    And I see "More" button under the listed 6 sub-categories

  Scenario: Tapping of the "More" button (Mobile only)
    Given I have executed the scenario-1 above
    When I tap on the "More" button
    Then I see the rest of the sub-categories
    And I see the "Less" button underneath all the listed sub-categories

  Scenario: Tapping of the "Less" button (Mobile only)
    Given I have executed the scenario-2 above
    When I tap on the "Less" button
    Then All the listed sub-categories shrink to the maximum of only 6 sub-categories

  Scenario: No sub-categories in the Refine Menu (Mobile only)
    Given I have executed the scenario-1 above
    When I tap on the "Refine" button
    Then I DO NOT see any sub-categories listed
