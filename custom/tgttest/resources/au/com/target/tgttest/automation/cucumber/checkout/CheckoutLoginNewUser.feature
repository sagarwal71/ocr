@cartProductData @sessionCatalogVersion @customerData @initPreOrderProducts
Feature: As a new Target customer
  I want to be guided through the relevant checkout path
  So that I can checkout faster

  Scenario: New Target customer begins checkout as guest
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 |   2 |    15 |
    And a customer does not exist with the user name 'idontexist@target.com.au'
    And guest checkout is enabled
    When the customer provides their email address during checkout login
    Then the customer will not be asked to enter their password
    When the customer begins checkout as guest
    Then the customer will enter the checkout flow as an anonymous guest

  Scenario: New Target customer tries to begin checkout as guest but guest is not enabled
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 |   2 |    15 |
    And a customer does not exist with the user name 'idontexist@target.com.au'
    And guest checkout is disabled
    When the customer provides their email address during checkout login
    Then the customer will not be asked to enter their password
    When the customer begins checkout as guest
    Then the customer will be told that guest checkout is not available

  Scenario: New Target customer tries to begin checkout as guest with a gift card in the cart
    When gift cards are added to cart with recipient details:
      | productCode       | firstName | lastName | email                    | messageText            |
      | PGC1000_iTunes_10 | No        | User     | idontexist@target.com.au | This is a test message |
    And a customer does not exist with the user name 'idontexist@target.com.au'
    And guest checkout is enabled
    When the customer provides their email address during checkout login
    Then the customer will not be asked to enter their password
    When the customer begins checkout as guest
    Then the customer will be told that guest checkout cannot be used to purchase gift cards

  Scenario: New Target customer tries to register during checkout with a bad password
    Given a customer does not exist with the user name 'idontexist@target.com.au'
    When the customer provides their email address during checkout login
    Then the customer will be given the option of registering or continue as guest
    When the customer provides the following details:
      | firstName | lastName | password | optIntoMarketing |
      | John      | Smith    | password | true             |
    Then the customer will be told that their password is insecure
  
  Scenario: New Target customer tries to begin checkout as guest with a preOrder item in the cart
    Given a cart with entries:
      | product          | qty | price |
      | V1111_preOrder_1 | 1   | 15    |
    And a customer does not exist with the user name 'idontexist@target.com.au'
    And guest checkout is enabled
    When the customer provides their email address during checkout login
    Then the customer will not be asked to enter their password
    When the customer begins checkout as guest
    Then the customer will be told that guest checkout cannot be used to purchase pre-order item

  @wip
  Scenario: New Target customer begins checkout after registering
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 |   2 |    15 |
    And a customer does not exist with the user name 'idontexist@target.com.au'
    When the customer provides their email address during checkout login
    Then the customer will be given the option of registering or continue as guest
    When the customer provides the following details:
      | firstName | lastName | password | optIntoMarketing |
      | John      | Smith    | asdfasdf | true             |
    Then an account will be created with the provided details
    And the customer will enter the checkout flow as them
    And the customer will be signed up to receive marketing material
