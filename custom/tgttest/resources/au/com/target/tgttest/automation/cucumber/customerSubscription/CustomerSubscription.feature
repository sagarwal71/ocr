@wip
Feature: customer subscription
  In order to provide better customer experience 
  As marketing
  I would like to capture customer information

  Background: 
    Given customer subscription retry interval is set to 2 times every 15000 milliseconds

  Scenario: customer subscribes to enews
    Given customer with subscription details:
      | title | First Name | Email          |
      | Mr    | John       | John@gmail.com |
    When customer subscribes to newsletter
    Then customer gets message for subscription 'success'

  Scenario: customer subscribes to enews only with email address
    Given customer with subscription details:
      | Email          |
      | John@gmail.com |
    When customer subscribes to newsletter
    Then customer gets message for subscription 'success'

  Scenario: customer subscribes to enews when webmethod is offline
    Given webmethods is offline for 20 seconds
    And customer with subscription details:
      | title | First Name | Email          |
      | Mr    | John       | John@gmail.com |
    When customer subscribes to newsletter
    Then customer gets message for subscription 'success'
     And customer subscription process outcome is 'success'

  Scenario: customer subscribes to enews when provider is offline
    Given provider is offline for 20 seconds
    And customer with subscription details:
      | title | First Name | Email          |
      | Mr    | John       | John@gmail.com |
    When customer subscribes to newsletter
    Then customer gets message for subscription 'success'
     And customer subscription process outcome is 'success'

  Scenario: existing subscriber tries to subsribe to enews
    Given customer is already subsribed
    When customer subscribes to newsletter
    Then customer gets message for subscription 'success'

  Scenario: hybris retry entries exceeded
    Given webmethods is offline for 100 seconds
    When customer subscribes to newsletter
    Then customer gets message for subscription 'success'
     And customer subscription business process fails with retry exceeded
