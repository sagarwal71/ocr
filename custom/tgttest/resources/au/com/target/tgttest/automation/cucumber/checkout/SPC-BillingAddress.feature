Feature: Single Page Checkout - Billing address
  As a mobile user who is in the Single Page Checkout
  I want to enter my billing address
  So that I receive the invoice

  Background: 
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 10    |

  Scenario: Create Australian billing address
    Given customer enters billing address
      | 12 Thompson Road,North Geelong,3002, AU |
    When the order detail is displayed via spc
    Then billing address displayed is
      | 12 Thompson Road,North Geelong,3002, AU, Australia |

  Scenario: Create New Zealand billing address
    Given customer enters billing address
      | 1 Northshire street,London,1000, NZ |
    When the order detail is displayed via spc
    Then billing address displayed is
      | 1 Northshire street,London,1000, NZ, New Zealand |

  Scenario: Create UK billing address
    Given customer enters billing address
      | 28 Royal Worcester Drive,Lancashire,LS11 1BA, GB |
    When the order detail is displayed via spc
    Then billing address displayed is
      | 28 Royal Worcester Drive,Lancashire,LS11 1BA, GB, United Kingdom |

  Scenario: Select saved address as a billing address
    Given customer has saved addresses
      | 12 Thompson Road,North Geelong,3002 |
      | 13 Thompson Road,North Geelong,3003 |
      | 14 Thompson Road,North Geelong,3222 |
    And '12 Thompson Road,North Geelong,3002' is selected as billing address
    Then billing address displayed is
      | 12 Thompson Road,North Geelong,3002, AU, Australia |
