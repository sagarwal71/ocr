@cartProductData
Feature: Display both valid and invalid open stores for cnc order by post code or city in order of distance.
  As a Target online customer
  I want to see both valid/invalid stores according to my search
  So I can select the best store to pickup my order

  Background: 
    Given a customer is located with coordinate data of latitude '-38.1083132' and longitude '144.3357845'
    And the max number of stores shown is 5
    And the Top 10 closest stores to the coordinate of latitude '-38.1083132' and longitude '144.3357845':
      | storeName        | storeNumber | productTypes             | acceptCNC | closed |
      | Geelong          | 7001        | bulky1,bulky2,normal,mhd | true      | false  |
      | Waurn Ponds UAT  | 7103        | bulky1,bulky2,normal,mhd | true      | true   |
      | Ocean Grove      | 8593        | bulky1,bulky2,normal,mhd | false     | false  |
      | Hoppers Crossing | 7064        | bulky1,bulky2,normal,mhd | true      | false  |
      | Point Cook       | 7269        | normal                   | true      | false  |
      | Bacchus Marsh    | 8558        | bulky1,bulky2,normal,mhd | true      | false  |
      | Caroline Springs | 7120        | normal                   | true      | false  |
      | Brimbank Central | 7158        | bulky1,bulky2,normal,mhd | false     | false  |
      | Rosebud          | 7274        | bulky1,bulky2,normal,mhd | true      | false  |
      | Watergardens     | 7160        | bulky1,bulky2,normal,mhd | true      | false  |
    And geolocation setup for certain postcode/suburb are:
      | locationText | latitude    | longitude   |
      | 3220         | -38.1083132 | 144.3357845 |
      | Geelong      | -38.1083132 | 144.3357845 |

  Scenario: Customer searches stores by postcode - normal cart
    Given the customer has the cart with non bulky product
    When the customer search stores with '3220' via spc
    Then the customer should be presented with the following 5 stores:
      | storeName        | storeNumber | availableForPickup |
      | Geelong          | 7001        | true               |
      | Waurn Ponds UAT  | 7103        | true               |
      | Ocean Grove      | 8593        | false              |
      | Hoppers Crossing | 7064        | true               |
      | Point Cook       | 7269        | true               |

  Scenario: Customer searches stores by postcode - cart with bulky product
    Given the customer has the cart with bulky product
    When the customer search stores with '3220' via spc
    Then the customer should be presented with the following 5 stores:
      | storeName        | storeNumber | availableForPickup |
      | Geelong          | 7001        | true               |
      | Waurn Ponds UAT  | 7103        | true               |
      | Ocean Grove      | 8593        | false              |
      | Hoppers Crossing | 7064        | true               |
      | Point Cook       | 7269        | false              |

  Scenario: Customer searches stores by suburb - cart with bulky product
    Given the customer has the cart with bulky product
    When the customer search stores with 'Geelong' via spc
    Then the customer should be presented with the following 5 stores:
      | storeName        | storeNumber | availableForPickup |
      | Geelong          | 7001        | true               |
      | Waurn Ponds UAT  | 7103        | true               |
      | Ocean Grove      | 8593        | false              |
      | Hoppers Crossing | 7064        | true               |
      | Point Cook       | 7269        | false              |
