@cartProductData @storeFulfilmentCapabilitiesData @forceDefaultWarehouseToOnlinePOS
Feature: Instore Fulfilment - Same Store and PPD Routing Rules for product exclusion

  Background: 
    Given stores with fulfilment capability and stock:
      | store | inStock | state | instoreEnabled | allowDeliveryToSameStore | allowDeliveryToAnotherStore |
      | 7126  | Yes     | QLD   | Yes            | Yes                      | Yes                         |
      | 7049  | No      | QLD   | Yes            | Yes                      | Yes                         |

  Scenario: Robina store product exclusion rule exist and consignment is routing to the store.
    Given product exclusion for store '7126':
      | productCode | excludeStartDate | excludeEndDate |
      | W100002     | NOT POPULATED    | NOT POPULATED  |
      | W100003     | NOT POPULATED    | NOT POPULATED  |
    When a consignment sent to a store '7126' with product:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
      | 10000111 | 2   | 15    |
    Then the assigned warehouse will be 'Robina'

  Scenario: Store product exclusion deny routing to store with base product W100001 of 10000011 and W100000 of 10000111 in the product exclusion list.
    Given product exclusion for store '7126':
      | productCode | excludeStartDate | excludeEndDate |
      | W100001     | NOT POPULATED    | NOT POPULATED  |
      | W100000     | NOT POPULATED    | NOT POPULATED  |
    When a consignment sent to a store '7126' with product:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
      | 10000111 | 2   | 15    |
    Then the assigned warehouse will be 'Fastline'

  Scenario: Store product exclusion deny routing to store 7049 (no stock) - Rerouted to 7126 - Fulfilled by store
    Given product exclusion for store '7126':
      | productCode | excludeStartDate | excludeEndDate |
      | W100002     | NOT POPULATED    | NOT POPULATED  |
      | W100003     | NOT POPULATED    | NOT POPULATED  |
    When a consignment sent to a store '7049' with product:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
      | 10000111 | 2   | 15    |
    Then the assigned warehouse will be 'Robina'

  Scenario: Store product exclusion deny routing to store 7049 (no stock) - Rerouted to 7126 - Products in Exclusion list - Routed to Fastline
    Given product exclusion for store '7126':
      | productCode | excludeStartDate | excludeEndDate |
      | W100001     | NOT POPULATED    | NOT POPULATED  |
      | W100000     | NOT POPULATED    | NOT POPULATED  |
    When a consignment sent to a store '7049' with product:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
      | 10000111 | 2   | 15    |
    Then the assigned warehouse will be 'Fastline'

  Scenario Outline: Store product exclusion for date range
    Given the store '7126' product exclusion is '<productCode>' with start date '<exclusionStartDate>' and end date '<exclusionEndDate>'
    When a consignment sent to a store '7126' with product:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    Then the assigned warehouse will be '<warehouse>'

    Examples: 
      | productCode | exclusionStartDate | exclusionEndDate | warehouse | comment                                                                                  |
      | W100000     | NOT POPULATED      | NOT POPULATED    | Fastline  | store product exclusion is active without start date and end date                        |
      | W100000     | NOT POPULATED      | BEFORE NOW       | Robina    | store product exclusion is not active with no start date and end date before now         |
      | W100000     | NOT POPULATED      | AFTER NOW        | Fastline  | store product exclusion is active with no start date and end date after now              |
      | W100000     | BEFORE NOW         | NOT POPULATED    | Fastline  | store product exclusion is active with start date before now and no end date             |
      | W100000     | BEFORE NOW         | AFTER NOW        | Fastline  | store product exclusion is active with start date before now and end date after now      |
      | W100000     | BEFORE NOW         | BEFORE NOW       | Robina    | store product exclusion is not active with start date before now and end date before now |
      | W100000     | AFTER NOW          | NOT POPULATED    | Robina    | store product exclusion is not active with start date after now and no end date          |
      | W100000     | AFTER NOW          | AFTER NOW        | Robina    | store product exclusion is not active with start date after now and end date after now   |
