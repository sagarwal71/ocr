@notAutomated
Feature: Display Customer Experience Center (CEC) number in checkout flow
  As a Target online customer
  I want to know CEC number on checkout pages
  So that I can contact the Customer Experience Centre if I am unable to pay and finish my order

  Scenario Outline: Display CEC number in Delivery page
    Given the customer has entered the checkout flow
    When the customer lands on checkout <step> page
    Then the customer experience centre information will be displayed in the footer

    Examples: 
      | step        |
      | 'Delivery'  |
      | 'Payment'   |
      | 'Review'    |
      | 'Thank you' |
