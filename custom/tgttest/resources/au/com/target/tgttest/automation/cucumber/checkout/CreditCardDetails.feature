@notAutomated
Feature: Capture credit card details for future checkouts
In order to checkout faster
As a online customer 
I want credit card details saved in system 

Scenario: Customer with no saved credit cards is making credit card payment then checkbox is selected by default. 
Given a customer is providing payment details
And does NOT have any existing saved credit cards
When the customer chooses to pay by credit card
Then "Remember this card for future use" is selected by default

Scenario: Customer with at least one saved credit card is making credit card payment with new card
Given a customer is providing payment details
And does have one or more existing saved credit cards
When the customer chooses to pay by a new credit card
Then "Remember this card for future use" is selected by default

Scenario: Customer with multiple saved credit cards is able to remove card
Given a customer has multiple saved credit cards in payment details:
|name         |card number|
|guestCheckout|5123456789012346|
|guestCheckout|5123456789012347|
|guestCheckout|5123456789012348|
When the customer removes the first credit cards record in payment details 
Then saved credit cards records in payment details are:
|name         |card number|
|guestCheckout|5123456789012347|
|guestCheckout|5123456789012348|

Scenario: Customer with multiple saved credit cards is able to select credit card for payment.
Given a customer has multiple saved credit cards in payment details:
|name         |card number|
|guestCheckout|5123456789012346|
|guestCheckout|5123456789012347|
When Customer selects credit card: 
|name         |card number|
|guestCheckout|5123456789012346|
Then credit card in review order page are: 
|name         |card number|
|guestCheckout|5123456789012346|

Scenario: Multiple credit cards that are saved in the customer account are displayed correctly in payment details page.  
Given a customer has multiple saved credit cards in my account:
|name         |card number|
|guestCheckout|5123456789012346|
|guestCheckout|5123456789012347|
|guestCheckout|5123456789012348|
When the customer navigates to payment details page
Then credit card in payment details page are: 
|name         |card number|
|guestCheckout|5123456789012346|
|guestCheckout|5123456789012347|
|guestCheckout|5123456789012348|

