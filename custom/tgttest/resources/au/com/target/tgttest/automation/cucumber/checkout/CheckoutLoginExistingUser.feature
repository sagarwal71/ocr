@cartProductData @sessionCatalogVersion @initPreOrderProducts
Feature: As a Target customer
  I want to be guided through the relevant checkout path
  So that I can checkout faster

  Scenario: Existing Target customer begins checkout
    Given a customer exists with the user name 'therealdeal@target.com.au'
    And that customer has not exceeded the maximum number of failed login attempts
    When the customer provides their email address during checkout login
    Then the customer will be asked to enter their password

  Scenario: Existing Target customer who is locked out begins checkout
    Given a customer exists with the user name 'custy@mccustomer.com'
    And that customer has exceeded the maximum number of failed login attempts
    When the customer provides their email address during checkout login
    Then the customer will be told that they are locked out of their account

  Scenario: Existing Target customer begins checkout as guest
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 |   2 |    15 |
    And a customer exists with the user name 'therealdeal@target.com.au'
    And guest checkout is enabled
    When the customer provides their email address during checkout login
    Then the customer will be asked to enter their password
    When the customer begins checkout as guest
    Then the customer will enter the checkout flow as a registered guest

  Scenario: Existing Target customer tries to begin checkout as guest but guest is not enabled
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 |   2 |    15 |
    And a customer exists with the user name 'therealdeal@target.com.au'
    And guest checkout is disabled
    When the customer provides their email address during checkout login
    Then the customer will be asked to enter their password
    When the customer begins checkout as guest
    Then the customer will be told that guest checkout is not available

  Scenario: Existing Target customer tries to begin checkout as guest with a gift card in the cart
    When gift cards are added to cart with recipient details:
      | productCode       | firstName | lastName | email                    | messageText            |
      | PGC1000_iTunes_10 | No        | User     | idontexist@target.com.au | This is a test message |
    And a customer exists with the user name 'therealdeal@target.com.au'
    And guest checkout is enabled
    When the customer provides their email address during checkout login
    Then the customer will be asked to enter their password
    When the customer begins checkout as guest
    Then the customer will be told that guest checkout cannot be used to purchase gift cards
    
  Scenario: Existing Target customer tries to begin checkout as guest with a preOrder item in the cart
    Given a cart with entries:
      | product          | qty | price |
      | V1111_preOrder_1 | 1   | 15    |
    And a customer exists with the user name 'therealdeal@target.com.au'
    And guest checkout is enabled
    When the customer provides their email address during checkout login
    And the customer will be asked to enter their password
    And the customer begins checkout as guest
    Then the customer will be told that guest checkout cannot be used to purchase pre-order item
