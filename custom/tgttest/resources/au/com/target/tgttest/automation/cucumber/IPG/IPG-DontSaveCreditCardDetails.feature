@notAutomated
Feature: IPG - Remove ablility to save the creditcard on payment page
  In order to provide better customer experience
  As target online
  I want to remove the option to save the credit card when the IPG is turned off

  Scenario: User try to enter creditcard details when the IPG is turned off
     Given IPG feature is turned off
     And user is registered
     And user request payment details page
     When payment method selected is 'creditcard'
     Then save creditcard details should not be allowed

   Scenario: User try to enter creditcard details when the IPG is turned off
     Given IPG feature is turned off
     And user is guest
     And user request payment details page
     When payment method selected is 'creditcard'
     Then save creditcard details should not be allowed

 