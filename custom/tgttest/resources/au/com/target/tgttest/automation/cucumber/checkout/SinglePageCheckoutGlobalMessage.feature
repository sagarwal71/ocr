@notAutomated
Feature: Display the information as global message during expedite checkout process
  While expedite checkout 
  As a online customer I want a global information should be shown on order review page

  Scenario: Registered customer with saved credit card details and delivery address initiating a checkout.
    Given a registered customer with a cart
    When customer initiates checkout
    Then customer navigates to Order Review Page through expedite checkout process
    And global message should display

  Scenario: Registered customer with saved credit card details and delivery address has initiated checkout and navigates back to payment page
    Given a registered customer is on Order review page through expedite checkout process
    And Customer navigates back to payment details page
    When Customer navigates to Order review page again
    Then global message should not display

  Scenario: Registered customer with saved credit card details and no delivery address initiating a checkout
    Given a registered customer with a cart
    When customer initiates checkout
    Then Customer navigates to Order review page through standard checkout process
    And global message should not display

  Scenario: Guest Customer without saved credit card details and no delivery address
    Given a guest user with cart
    When customer initiates checkout
    Then Customer navigates to Order review page through standard checkout process
    And global message should not display
