@notAutomated
Feature: Securing Thank You Page
  In order to keep the thank you page secure
  As a online shop
  I want to display the current finished order only for current user

  Background: 
    Given feature 'spc.thankyou' is enabled

  Scenario: Updating of the Thank You Page
    Given the user has already completed the purchase of the order
    When the user updates the URL "spc/order/thankyou/ordernumber" with other users Order Number
    Then the user gets to the Basket Page

  Scenario: Session Time out on the Thank You Page
    Given the user has already completed the purchase of the order
    When the user has a session time-out on the Thank You Page
    Then the user gets to the Basket Page

  Scenario: Refreshing the Thank You Page before Session Time out
    Given the user has already completed the purchase of order
    When the user refreshs the Thank You Page
    And The session time-out has not happened
    Then the user remains on the Thank You Page

  Scenario: Refreshing the Thank You Page after Session Time Out
    Given the user has already completed the purchase of the order
    When the user refreshs the Thank You Page
    And The session time-out has occurred (>30 mins)
    Then the user gets to the Basket Page
