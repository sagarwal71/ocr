@cartProductData
Feature: Fastline Fulfilment - Regression suite

  Scenario: order sent to fastline is not auto acknowledged
    Given a consignment is sent to fastline
    Then consignment status is 'SENT_TO_WAREHOUSE'

  Scenario: fastline acknowleges a new order
    Given a consignment is sent to fastline
    When order ack is received from fastline
    Then consignment status is 'CONFIRMED_BY_WAREHOUSE'

  Scenario: fastline full pick
    Given set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 10        |
    And a consignment sent to fastline with products:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    When the consignment is full picked by fastline
    Then fastline pick process completes
    And consignment status is 'PICKED'
    And order is in status 'INVOICED'
    And a CS ticket is not created for the order
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 8         |

  Scenario: fastline ship conf
    Given a consignment is sent to fastline
    When the consignment is full picked by fastline
    And fastline pick process completes
    And ship conf is received from fastline
    Then order complete process completes
    And consignment status is 'SHIPPED'
    And a CS ticket is not created for the order

  Scenario: ship before pick conf from fastline
    Given a consignment is sent to fastline
    When order ack is received from fastline
    And ship conf is received from fastline
    Then order complete process does not run
    And consignment status is 'CONFIRMED_BY_WAREHOUSE'
    And consignment ship conf flag is set
    And a CS ticket is not created for the order

  Scenario: ship then pick conf
    Given a consignment is sent to fastline
    When order ack is received from fastline
    And ship conf is received from fastline
    And the consignment is full picked by fastline
    Then order complete process completes
    And consignment status is 'SHIPPED'
    And a CS ticket is not created for the order

  Scenario: fastline short pick
    Given set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 10        |
    And a consignment sent to fastline with products:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    When fastline returns pick entry data:
      | product  | qty |
      | 10000011 | 1   |
    Then fastline pick process completes
    And consignment status is 'PICKED'
    And order is in status 'INVOICED'
    And a CS ticket is not created for the order
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 9         |

  Scenario: Process order acknowledgement file based on consignmentID
    Given a consignment is sent to fastline
    When order ack is received from fastline with consignmentID as the identifier
    Then consignment status is 'CONFIRMED_BY_WAREHOUSE'

  Scenario: Process pick confirmation file based on consignmentID
    Given a consignment is sent to fastline
    When the consignment is full picked by fastline with consignmentID as the identifier
    Then consignment status is 'PICKED'

  Scenario: Process ship confirmation file based on consignmentID
    Given a consignment is sent to fastline
    When the consignment is full picked by fastline
    And fastline pick process completes
    And ship conf is received from fastline with consignmentID as the identifier
    Then order complete process completes
    And consignment status is 'SHIPPED'
