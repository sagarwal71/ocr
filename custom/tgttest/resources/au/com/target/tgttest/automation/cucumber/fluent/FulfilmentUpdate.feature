@dummyStoreWarehouseData @cleanOrderData @cleanConsignmentData
Feature: As the Target Online Store
  I want to update consignment statuses based on fulfilment status updates sent by web hook from Fluent to the API Gateway when an order is resumed after fraud check
  so that hybris has the up to date status and data for fulfilments and is able to take action where required

  Scenario Outline: Invalid fulfilment updates get rejected
    Given list of consignments for store '2000' created from fluent fulfiments are:
      | consignmentCode | consignmentStatus | createdDate | shipDate | cancelDate |
      | auto11000001    | CREATED           | today       | null     | null       |
    When platform receives fulfilment update from fluent for consignment '<consignmentCode>'
      | fulfilmentType | deliveryType | status      |
      | CC_PFS         | STANDARD     | <newStatus> |
    Then consignment update status is '<updateStatus>'

    Examples: 
      | consignmentCode | newStatus | updateStatus      |
      | auto11000001    | ASSIGNED  | FAILED_DEPENDENCY |
      | auto11000001    | FULFILLED | FAILED_DEPENDENCY |
      | auto11000001    | SHIPPED   | FAILED_DEPENDENCY |
      | auto11000001    | UNKOWN    | BAD_REQUEST       |

  Scenario Outline: Outdated fulfilment updated is dropped
    Given list of consignments for store '2000' created from fluent fulfiments are:
      | consignmentCode | consignmentStatus | createdDate | shipDate | cancelDate |
      | auto11000001    | SHIPPED           | today       | null     | null       |
    When platform receives fulfilment update from fluent for consignment '<consignmentCode>'
      | fulfilmentType | deliveryType | status      |
      | CC_PFS         | STANDARD     | <newStatus> |
    Then consignment update status is 'OK'
    And consignment status for '<consignmentCode>' is 'SHIPPED'

    Examples: 
      | consignmentCode | newStatus     |
      | auto11000001    | CREATED       |
      | auto11000001    | AWAITING_WAVE |
      | auto11000001    | ASSIGNED      |
      | auto11000001    | FULFILLED     |
      | auto11000001    | SHIPPED       |

  Scenario Outline: Valid fulfilment update is successful
    Given list of consignments for store '2000' created from fluent fulfiments are:
      | consignmentCode | consignmentStatus | createdDate | shipDate | cancelDate |
      | auto11000001    | <oldStatus>       | today       | null     | null       |
    When platform receives fulfilment update from fluent for consignment '<consignmentCode>'
      | fulfilmentType | deliveryType | status      |
      | CC_PFS         | STANDARD     | <newStatus> |
    Then consignment update status is 'OK'
    And consignment status for '<consignmentCode>' is '<newConsignmentStatus>'

    Examples: 
      | consignmentCode | oldStatus         | newStatus     | newConsignmentStatus |
      | auto11000001    | CREATED           | AWAITING_WAVE | SENT_TO_WAREHOUSE    |
      | auto11000001    | SENT_TO_WAREHOUSE | ASSIGNED      | WAVED                |
      | auto11000001    | WAVED             | FULFILLED     | PICKED               |
      | auto11000001    | PICKED            | SHIPPED       | SHIPPED              |

  Scenario: Inventory adjustment for fulfilled fulfilment
    Given list of consignments for store '2000' created from fluent fulfiments are:
      | consignmentCode | consignmentStatus | createdDate | shipDate | cancelDate |
      | auto11000001    | WAVED             | today       | null     | null       |
    When hybris receives fulfilment update from fluent for consignment 'auto11000001' and shipped qty '1'
      | fulfilmentType | deliveryType | status    |
      | CC_PFS         | STANDARD     | FULFILLED |
    Then consignment update status is 'OK'
    And consignment status is 'PICKED'
    And the 'inventoryAdjustmentUsingWM' feature switch is enabled
    And fluent picked business process completes
    Then inventory adjustment is success for fluent fulfilment 'auto11000001'

  @fluentFeatureSwitch
  Scenario Outline: Valid fulfilment update EBay is successful
    Given consignments from fluent for store '2000' and sales channel '<salesChannel>'
      | consignmentCode | consignmentStatus | createdDate | shipDate | cancelDate | trackingId | deliveryMethod |
      | auto11000001    | <oldStatus>       | today       | null     | null       | 123456     | home-delivery  |
    When platform receives fulfilment update from fluent for consignment '<consignmentCode>'
      | fulfilmentType | deliveryType | status      |carrierTrackingId |
      | CC_PFS         | STANDARD     | <newStatus> |123456            |
    Then consignment update status is 'OK'
    And consignment status for '<consignmentCode>' is '<newConsignmentStatus>'
    And consignment request sent to EBay
      | trackingId |
      | 123456     |

    Examples: 
      | consignmentCode | oldStatus | newStatus | newConsignmentStatus | salesChannel |
      | auto11000001    | PICKED    | SHIPPED   | SHIPPED              | eBay         |

  Scenario Outline: Valid fulfilment update state transitions sets the appropriate carrier model
    Given list of consignments for store '2000' created from fluent fulfiments are:
      | consignmentCode | consignmentStatus | createdDate | shipDate | cancelDate |
      | auto11100001    | <oldStatus>       | today       | null     | null       |
    When platform receives fulfilment update from fluent for consignment '<consignmentCode>'
      | fulfilmentType | deliveryType | status      | carrierName    | carrierTrackingId | parcelCount   |
      | CC_PFS         | STANDARD     | <newStatus> | AUSTRALIA_POST | <trackingId>      | <parcelCount> |
    Then consignment update status is 'OK'
    And consignment status for '<consignmentCode>' is '<newConsignmentStatus>'
    And consignment carrier code for '<consignmentCode>' is 'AustraliaPostInstoreCNC'
    And consignment trackingId for '<consignmentCode>' is '<trackingId>'
    And consignment parcel count for '<consignmentCode>' is '<parcelCount>'

    Examples: 
      | consignmentCode | oldStatus         | newStatus     | newConsignmentStatus | trackingId | parcelCount |
      | auto11100001    | CREATED           | AWAITING_WAVE | SENT_TO_WAREHOUSE    | null       | 0           | 
      | auto11100001    | SENT_TO_WAREHOUSE | ASSIGNED      | WAVED                | null       | 0           | 
      | auto11100001    | WAVED             | FULFILLED     | PICKED               | null       | 0           | 
      | auto11100001    | PICKED            | SHIPPED       | SHIPPED              | trackingId | 2           | 

  Scenario Outline: Valid fulfilment update sets the appropriate carrier
    Given list of consignments for store '2000' created from fluent fulfiments are:
      | consignmentCode   | consignmentStatus | createdDate | shipDate | cancelDate |
      | <consignmentCode> | CREATED           | today       | null     | null       |
    When platform receives fulfilment update from fluent for consignment '<consignmentCode>'
      | fulfilmentType | deliveryType | status        | carrierName   |
      | CC_PFS         | STANDARD     | AWAITING_WAVE | <carrierName> |
    Then consignment update status is 'OK'
    And consignment carrier code for '<consignmentCode>' is '<targetCarrierName>'

    Examples: 
      | consignmentCode | carrierName    | targetCarrierName       |
      | auto11110001    | AUSTRALIA_POST | AustraliaPostInstoreCNC |
      | auto11110001    | NA             | NullCnC                 |

  @fluentFeatureSwitch @cncOrderExtractOnConsignment
  Scenario: Start update fluent order shippped processes
    Given list of consignments for store '2000' created from fluent fulfiments are:
      | consignmentCode | consignmentStatus | createdDate | shipDate | cancelDate |
      | auto11000001    | PICKED            | today       | today    | null       |
    When hybris receives fulfilment update from fluent for consignment 'auto11000001' and shipped qty '1'
      | fulfilmentType | deliveryType | status  |
      | CC_PFS         | STANDARD     | SHIPPED |
    Then consignment update status is 'OK'
    And consignment status is 'SHIPPED'
    And started fluent order shipped process with 'SHIPPED'
    And fluent shipped business process completes
    Then the order status is COMPLETED

  @fluentFeatureSwitch @cncOrderExtractOnConsignment
  Scenario: Start update fluent order shipped process with failed dependency
    Given list of consignments for store '2000' created from fluent fulfiments are:
      | consignmentCode | consignmentStatus | createdDate | shipDate | cancelDate |
      | auto11000002    | WAVED             | today       | today    | null       |
    When hybris receives fulfilment update from fluent for consignment 'auto11000002' and shipped qty '1'
      | fulfilmentType | deliveryType | status    |
      | CC_PFS         | STANDARD     | FULFILLED |
    Then consignment update status is 'OK'
    And consignment status is 'PICKED'
    And started fluent order shipped process with 'SHIPPED'
    Then the order update return 'FAILED_DEPENDENCY'
