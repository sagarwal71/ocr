@cartProductData
Feature: Search by Post Code or Suburb for nearest stores for in store stock availability check

  Scenario: An assorted product receives the flag on the data as true
    Given an assorted product with code '10000010'
    When the product is retrieved
    Then the product data returned contains the assorted flag as true

  Scenario: A non-assorted product receives the flag on the data as false
    Given an non-assorted product with code '10000010'
    When the product is retrieved
    Then the product data returned contains the assorted flag as false

  Scenario: A null assorted product receives the flag on the data as false
    Given an null assorted product with code '10000010'
    When the product is retrieved
    Then the product data returned contains the assorted flag as false

@notAutomated 
  Scenario: An assorted product has an orange Add to Basket button
   Given A target customer is viewing an assorted product with code '10000010'
   When The customer views the "Add to Basket" button
   Then The button is an orange color and has a warning message
   
   
@notAutomated 
  Scenario: A non-assorted product has a green Add to Basket button
   Given A target customer is viewing a non-assorted product with code '10000011'
   When The customer views the "Add to Basket" button
   Then The button is a green color and has no warning message

@notAutomated
  Scenario: An assorted product has a warning message on the PDP
	  Given: A target customer is viewing an assorted product with code '10000010'
	  When The customer views the PDP
	  Then The customer will see a warning message in orange
  
@notAutomated
  Scenario: A non-assorted product has no warning message on the PDP
	  Given: A target customer is viewing a non-assorted product with code '10000011'
	  When The customer views the PDP
	  Then The customer will not see a warning message in orange

@notAutomated
  Scenario Outline: Clicking on the warning button on PDP will use find in store if possible
	  Given A target customer is viewing an assorted product with code '10000010'
		And the IISV FeatureSwitch is <switchStatus>
		And ISSV is <enabledStatus> for the product
		When I click on the link "Target Store"
		Then I <expectedAction>

		Examples:
		| switchStatus | enabledStatus | expectedAction |
		| Enabled | Enabled | Scroll to Find in Store and open Accordion |
		| Enabled | Disabled | Am taken to store-finder |
		| Disabled | Disabled | Am taken to store-finder |
		| Disabled | Enabled | Am taken to store-finder |
			
