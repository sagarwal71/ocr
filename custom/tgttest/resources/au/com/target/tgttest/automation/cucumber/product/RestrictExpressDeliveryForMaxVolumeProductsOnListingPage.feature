@notAutomated
Feature: Restrict express delivery sticker on listing page for products above the maximum cubic volume.
  
  
  In order to restrict express delivery 
  As the online store
  I want to prevent express delivery sticker for products on listing page whose volume is configured above the max.

  Background: 
    Given products with volume:
      | product | Volume(cm3) |
      | W100000 | 150         |
      | W100001 | 140         |
      | W100002 | 100         |
      | W100003 | 160         |
      | W100004 | 50          |
      | W100005 | 500         |
      | W100006 | 130         |
      | W100007 | 110         |
      | W100008 | N/A         |
      | W100009 | 0           |
      | W100010 | -100        |
    And Maximum volume restriction for the express delivery is set to 150cm3


  Scenario Outline: Delivery promotion sticker on product list page based on volume of the product
    Given product delivery modes:
      | productCode | deliveryModesAllowed                             |
      | W100000     | express-delivery,home-delivery,click-and-collect |
      | W100001     | express-delivery,home-delivery,click-and-collect |
      | W100002     | express-delivery,home-delivery,click-and-collect |
      | W100003     | express-delivery,home-delivery                   |
      | W100004     | home-delivery,click-and-collect                  |
      | W100005     | home-delivery                                    |
      | W100006     | express-delivery                                 |
      | W100007     | click-and-collect                                |
      | W100008     | express-delivery,click-and-collect               |
      | W100009     | express-delivery,home-delivery                   |
      | W100010     | express-delivery,home-delivery                   |
    And delivery promotion ranks order is '<deliveryPromotionRank>'
    And express delivery promo sticker settings are '<expressDeliveryPromo>'
    And home delivery promo sticker settings are '<homeDeliveryPromo>'
    And cnc delivery promo sticker settings are '<CnCPromo>'
    When product is displayed on the product list page
    Then delivery promotion sticker is '<deliveryPromotionSticker>'

    Examples: 
      | productCode | deliveryPromotionRank | expressDeliveryPromo | homeDeliveryPromo   | CnCPromo            | deliveryPromotionSticker |
      | W100000     | express,home,cnc      | STICKER PRESENT      | STICKER NOT-PRESENT | STICKER PRESENT     | express                  |
      | W100001     | express,home,cnc      | STICKER PRESENT      | STICKER NOT-PRESENT | STICKER NOT-PRESENT | express                  |
      | W100002     | home,express,cnc      | STICKER PRESENT      | STICKER PRESENT     | STICKER NOT-PRESENT | home                     |
      | W100003     | express,home          | STICKER PRESENT      | STICKER PRESENT     | N/A                 | home                     |
      | W100004     | home,cnc              | STICKER PRESENT      | N/A                 | STICKER PRESENT     | cnc                      |
      | W100005     | home                  | N/A                  | STICKER PRESENT     | N/A                 | home                     |
      | W100006     | express               | STICKER PRESENT      | N/A                 | N/A                 | express                  |
      | W100007     | cnc                   | N/A                  | N/A                 | STICKER NOT-PRESENT | NONE                     |
      | W100008     | express,cnc           | STICKER PRESENT      | STICKER PRESENT     | STICKER NOT-PRESENT | NONE                     |
      | W100009     | express,home          | STICKER PRESENT      | STICKER PRESENT     | STICKER NOT-PRESENT | home                     |
      | W100010     | express,home          | STICKER PRESENT      | STICKER NOT-PRESENT | STICKER NOT-PRESENT | NONE                     |
