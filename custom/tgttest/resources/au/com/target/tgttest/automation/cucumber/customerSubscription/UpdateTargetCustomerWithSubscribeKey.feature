Feature: If an email address is registered in Target Online (Hybris) and signed up for eNews or Mumshub,
We want to assign the subscribe key to the customer record in hybris db.

Scenario: Customer signup for enews
Given an email address 'marry@marry.com.au' is present in hybris
  And customer with subscription details:
      | title | First Name   | Email              |
      | Mrs    | Marry       | marry@marry.com.au |
 When a new customer record with the email is created in ExactTarget consolidated customer database
 Then the same unique subscriber key will be assigned to the record in hybris

@notAutomated 
Scenario: Customer exists in ET and later creates a Hybris account
Given that customer with email address 'harry@barry.com.au' exists in ExactTarget consolidated customer database
 When this customer creates an account on www.target.com.au for the very first time
 Then a record will be created in Hybris for this customer 
  And same unique subscriber key as ExactTarget will be appended to the newly created record in Hybris