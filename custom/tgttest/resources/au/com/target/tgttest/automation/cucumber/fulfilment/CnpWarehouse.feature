@cartProductData @sessionCatalogVersion @storeFulfilmentCapabilitiesData
Feature: Reserve stock against DropShip warehouse within place order for all CNP products
  As the Trading team
  I want to reserve stock against DropShip warehouse within place order for all CNP products 
  So that the SOH is reserved at the correct location

  Scenario: Stock available for cart with Home Delivery at fulfilment in CNP warehouse
    Given a cart with entries:
      | product  | qty | price |
      | 10000323 | 2   | 15    |
    And registered user checkout
    And customer checkout is started
    And delivery mode is 'home-delivery'
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39.0  | 30.0     | 9.0          |          |
    And CNP stock is:
      | product  | reserved | available |
      | 10000323 | 2        | 100       |
