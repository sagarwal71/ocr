Feature: Product Detail Page: variant display logic for instore tab in PDP

@notAutomated
  Scenario Outline: Preferred store is not selected and user is in store tab{color}*
    Given user likes a product and has navigated to PDP
    And has no preferred store selected
    When user selects the in store tab
    Then display Find in store icon
    And the price/price range for the product

@notAutomated
  Scenario Outline: Fetch store stock data from backend - Product is available for both online and instore or only in store
    Given a base product with <baseProduct> has colour variants as the <sellableVariants> which is sold both online and in store or only in store
    When preferred store is set by the user as <preferredStore>
    And when the sellable  variants has different stock levels <storeStock> in the selected preferred store
    And consolidated store stock in other stores <consolidatedOtherStores>
    Then retrieve the stock levels <stockRetrieved> as per the below table.

    Examples: 
      | baseProduct | sellableVariants | preferredStore | storeStock | consolidatedOtherStores | stockRetrieved   |
      | P1000       | CP1,CP2          | Geelong        | 20, 8      | 600, 600                | High, Low        |
      | P1000       | CP1,CP2          | Geelong        | 6, 2       | 6, 20                   | Low, Low         |
      | P1000       | CP1,CP2          | Geelong        | 15, 10     | 60, 60                  | High, High       |
      | P1000       | CP1,CP2          | Geelong        | 0, 16      | 60, 60                  | Network, High    |
      | P1000       | CP1,CP2          | Geelong        | 0, 0       | 60, 60                  | Network, Network |
      | P1000       | CP1,CP2          | Geelong        | 0, 0       | 0, 0                    | No, No           |

@notAutomated
  Scenario Outline: Preferred store set, user is in store tab, display the stock levels of the selected variant
    Given user is in PDP, has a preferred store set for a base product <baseProduct> that has sellable variants as <sellableVariants>
    When user selects a sellable variant of the product as <selectedVariant>
    And has a preferred store set as <preferredStore>
    And has store Stock as <storeStock>
    And has consolidated store stock as <consolidatedOtherStores>
    And has stock data for the selected variant retrieved <stockRetrieved>
    And has online stock if applicable as <onlineStock>
    Then display the stock level as per the <UIMessage>

    Examples: 
      | baseProduct | sellableVariants | selectedVariant | preferredStore | storeStock      | consolidatedOtherStores | onlineStock | stockRetrieved | UIMessage                                            |
      | P1000       | CP1,CP2          | CP1             | Geelong        | >=9             | Any                     | Any         | High           | In stock                                             |
      | P1000       | CP1,CP2          | CP1             | Geelong        | between 1 and 8 | Any                     | Any         | Low            | Limited Stock                                        |
      | P1000       | CP1,CP2          | CP1             | Geelong        |               0 |                       0 |           0 | No             | Sold out online and in store                         |
      | P1000       | CP1,CP2          | CP1             | Geelong        |               0 | >0                      | >0          | Network        | Find in other store icon and Available online option |
      | P1000       | CP1,CP2          | CP1             | Geelong        |               0 | >0                      |           0 | Network        | Find in other store icon                             |
      | P1000       | CP1,CP2          | CP1             | Geelong        |               0 |                       0 | >0          | No             | Available online option                              |

@notAutomated
  Scenario Outline: UI Message - icons vs actions
    Given that user is in PDP and in store tab
    When user selects icons, <UIMessage> on page
    Then redirect user to <nextState>

    Examples: 
      | UIMessage                | nextState                                                             |
      | Find in other store icon | navigate to in store tab and open the Find  in other store pop up     |
      | Available online icon    | navigate to online  tab and display variants, price etc as applicable |

  
  Scenario Outline: Update store thresholds for Sold out, No stock and Limited stock in PDP using the shared configuration.
    Given that Product '<product>' has color variants as '<colorVariants>' and sizes as '<sizeVariants>'
    And customer has a preferred store set <store>
    When preferred store stock of the sellable variant in cache db are:
    |   code     | storeNumber | soh |
    |  10000011  | 7001        |  0  |
    |  10000012  | 7001        |  9  |
    |  10000021  | 7001        |  4  |
    |  10000022  | 7001        |  1  |
    |  10000023  | 7001        |  8  |
    |  10000111  | 7001        |  0  |
    |  10000112  | 7001        |  8  |
    |  10000121  | 7001        |  1  |
    |  10000122  | 7001        |  2  |
    And stock threshold config are set to <sharedConfig>
    Then the stock indication on store tab in PDP should be '<stockMessage>'
    
    Examples: 
      | product |   colorVariants   |          sizeVariants               | store  |   sharedConfig                               |            stockMessage                 | 
      | W100000 | 10000010,10000020 | 10000011,10000012,10000021,10000022,10000023 | 7001   |  limitedStockThreshold:8,noStockThreshold:1  | outOfStock,inStock,lowStock,outOfStock,lowStock|  
      | W100001 | 10000110,10000120 | 10000111,10000112,10000121,10000122 | 7001   |  limitedStockThreshold:7,noStockThreshold:0  | outOfStock,inStock,lowStock,lowStock    |
 