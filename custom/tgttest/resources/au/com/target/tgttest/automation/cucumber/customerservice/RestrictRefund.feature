@cleanOrderData @cleanConsignmentData
Feature: Restrict refund if refund capability not supported by fulfilling warehouse
  
  As Target Online
  I want to restrict refunds for items fulfilled from specific warehouses
  So that I can comply with terms and conditions for digital gift card purchase (disallowing refunds) and allow for future initiatives to use this feature
  
  Notes: 
  * Warehouse with refund capability are setup as:
      | Warehouse | Refunds |
      | Incomm    | Denied  |
      | Fastline  | Allowed |
      | Robina    | Allowed |

  Scenario: Shipped consignment without delivery fee and warehouse denies refund
    Given an order with delivery fee $0.0 with consignments:
      | products               | warehouse | consignmentStatus |
      | 1 of PGC1000_iTunes_10 | Incomm    | Shipped           |
    When Customer service agent views the order
    Then the refund option is disabled

  Scenario: Shipped consignment with delivery fee and warehouse denies refund
    Given an order with delivery fee $9.0 with consignments:
      | products               | warehouse | consignmentStatus |
      | 1 of PGC1000_iTunes_10 | Incomm    | Shipped           |
    When Customer service agent views the order
    Then the refund option is enabled
    And refund dialog product list will be empty
    And refund dialog product list allows Delivery cost refund up to $9.00

  Scenario: Shipped consignment with no delivery fee and warehouse allows refund
    Given an order with delivery fee $0.0 with consignments:
      | products      | warehouse | consignmentStatus |
      | 2 of 10000111 | Fastline  | Shipped           |
    When Customer service agent views the order
    Then the refund option is enabled
    And refund dialog product list has '10000111'
    And refund dialog product list disallows Delivery cost refund

  Scenario: Shipped consignment with delivery fee and warehouse allows refund
    Given an order with delivery fee $9.0 with consignments:
      | products      | warehouse | consignmentStatus |
      | 2 of 10000111 | Fastline  | Shipped           |
    When Customer service agent views the order
    Then the refund option is enabled
    And refund dialog product list has '10000111'
    And refund dialog product list allows Delivery cost refund up to $9.00

  Scenario: Consignment confirmend by warehouse and no delivery fee, warehouse allows refund
    Given an order with delivery fee $0.0 with consignments:
      | products      | warehouse | consignmentStatus      |
      | 2 of 10000111 | Fastline  | Confirmed by Warehouse |
    When Customer service agent views the order
    Then the refund option is disabled

  Scenario: Consignment picked, with no delivery fee and warehouse allows refund
    Given an order with delivery fee $0.0 with consignments:
      | products      | warehouse | consignmentStatus |
      | 2 of 10000111 | Robina    | Picked            |
    When Customer service agent views the order
    Then the refund option is disabled
    And the option to refund delivery fee will be disabled

  Scenario: Consignment packed, with delivery fee and warehouse allows refund
    Given an order with delivery fee $9.0 with consignments:
      | products      | warehouse | consignmentStatus |
      | 2 of 10000111 | Robina    | Packed            |
    When Customer service agent views the order
    Then the refund option is enabled
    And refund dialog product list will be empty
    And refund dialog product list allows Delivery cost refund up to $9.00

  Scenario: Consignment not yet shipped, with delivery fee and warehouse allows refund
    Given an order with delivery fee $9.0 with consignments:
      | products      | warehouse | consignmentStatus |
      | 2 of 10000111 | Fastline  | Sent to warehouse |
    When Customer service agent views the order
    Then the refund option is enabled
    And refund dialog product list will be empty
    And refund dialog product list allows Delivery cost refund up to $9.00

  Scenario: All consignments shipped, no delivery fee for order
    Given an order with delivery fee $0.0 with consignments:
      | products               | warehouse | consignmentStatus |
      | 1 of PGC1000_iTunes_10 | Incomm    | Shipped           |
      | 2 of 10000111          | Fastline  | Shipped           |
    When Customer service agent views the order
    Then the refund option is enabled
    And refund dialog product list has '10000111'
    And refund dialog product list disallows Delivery cost refund

  Scenario: All consignments shipped, delivery fee exists for order
    Given an order with delivery fee $9.0 with consignments:
      | products               | warehouse | consignmentStatus |
      | 1 of PGC1000_iTunes_10 | Incomm    | Shipped           |
      | 2 of 10000111          | Fastline  | Shipped           |
    When Customer service agent views the order
    Then the refund option is enabled
    And refund dialog product list has '10000111'
    And refund dialog product list allows Delivery cost refund up to $9.00

  Scenario: Mixed consignment status with no delivery fee and none of the consignments are available for refund
    Given an order with delivery fee $9.0 with consignments:
      | products               | warehouse | consignmentStatus      |
      | 1 of PGC1000_iTunes_10 | Incomm    | Shipped                |
      | 2 of 10000111          | Fastline  | Confirmed By Warehouse |
    When Customer service agent views the order
    Then the refund option is enabled
    And refund dialog product list allows Delivery cost refund up to $9.00

  Scenario: Mixed consignment status with delivery fee and none of the consignments are available for refund
    Given an order with delivery fee $9.0 with consignments:
      | products               | warehouse | consignmentStatus      |
      | 1 of PGC1000_iTunes_10 | Incomm    | Shipped                |
      | 2 of 10000111          | Fastline  | Confirmed By Warehouse |
    When Customer service agent views the order
    Then the refund option is enabled
    And refund dialog product list will be empty
    And refund dialog product list allows Delivery cost refund up to $9.00

  Scenario: Mixed consignment status with no delivery fee and one of the consignment is available for refund
    Given an order with delivery fee $0.0 with consignments:
      | products               | warehouse | consignmentStatus |
      | 1 of PGC1000_iTunes_10 | Incomm    | Created           |
      | 2 of 10000111          | Fastline  | Shipped           |
    When Customer service agent views the order
    Then the refund option is disabled
    And refund dialog product list will be empty
    And refund dialog product list disallows Delivery cost refund

  Scenario: Mixed consignment status with delivery fee and one of the consignment is available for refund
    Given an order with delivery fee $9.0 with consignments:
      | products               | warehouse | consignmentStatus |
      | 1 of PGC1000_iTunes_10 | Incomm    | Created           |
      | 2 of 10000111          | Fastline  | Shipped           |
    When Customer service agent views the order
    Then the refund option is enabled
   And refund dialog product list will be empty
    And refund dialog product list allows Delivery cost refund up to $9.00

  Scenario: No consignments shipped and no delivery fee for order
    Given an order with delivery fee $0.0 with consignments:
      | products               | warehouse | consignmentStatus      |
      | 1 of PGC1000_iTunes_10 | Incomm    | Created                |
      | 2 of 10000111          | Fastline  | Confirmed By Warehouse |
    When Customer service agent views the order
    Then the refund option is disabled

  Scenario: No consignments shipped and delivery fee exists for order
    Given an order with delivery fee $9.0 with consignments:
      | products               | warehouse | consignmentStatus      |
      | 1 of PGC1000_iTunes_10 | Incomm    | Created                |
      | 2 of 10000111          | Fastline  | Confirmed By Warehouse |
    When Customer service agent views the order
    Then the refund option is enabled
    And refund dialog product list will be empty
    And refund dialog product list allows Delivery cost refund up to $9.00
