Feature: Automatic delivery fee refund on cancellation of all physical items
  
  As a Target Online
  I want to automatically refund the deliveey fee
  So that I do not charge any delivery fee when all physical items are cancelled
  
  Notes: Home delivery fee is $9 for orders below $75.

  Scenario: Full cancel of physical order, full delivery fee refunded.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
      | 10000012 | 1   | 15    |
    And delivery mode is 'home-delivery'
    And order is placed
    And order is:
      | total | subtotal | deliveryCost |
      | 39    | 30       | 9            |
    When the order is fully cancelled
    Then refund value should be '39'
    And order is:
      | total | subtotal | deliveryCost |
      | 0     | 0        | 0            |

  Scenario: Full cancel of physical items in mixed order, full delivery fee refunded. Digital consignment is fulfilled.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
      | 10000012 | 1   | 15    |
    And a gift card is added to cart with recipient details:
      | productCode       | firstName  | lastName  | email                            | messageText   |
      | PGC1000_iTunes_10 | Test First | Test Last | TestFirst.TestLast@target.com.au | Test Checkout |
    And delivery mode is 'home-delivery'
    And order is placed
    And order is:
      | total | subtotal | deliveryCost |
      | 49    | 40       | 9            |
    When cancel entries
      | product  | qty |
      | 10000011 | 1   |
      | 10000012 | 1   |
    Then refund value should be '39'
    And order is:
      | total | subtotal | deliveryCost |
      | 10    | 10       | 0            |

  Scenario: Full cancel of physical items in mixed order, full delivery fee refunded. Digital consignment is unfulfilled.
    Given webmethods returns 'error' for send to warehouse
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
      | 10000012 | 1   | 15    |
    And a gift card is added to cart with recipient details:
      | productCode       | firstName  | lastName  | email                            | messageText   |
      | PGC1000_iTunes_10 | Test First | Test Last | TestFirst.TestLast@target.com.au | Test Checkout |
    And delivery mode is 'home-delivery'
    And order is placed
    And order is:
      | total | subtotal | deliveryCost |
      | 49    | 40       | 9            |
    When cancel entries
      | product  | qty |
      | 10000011 | 1   |
      | 10000012 | 1   |
    Then refund value should be '39'
    And order is:
      | total | subtotal | deliveryCost |
      | 10    | 10       | 0            |

  Scenario: Full cancel of physical items in mixed order re-routed to Fastline, full delivery fee refunded.
    Given stores with fulfilment capability and stock:
      | store  | instoreEnabled | inStock | deliveryModesAllowed | allowDeliveryToSameStore | allowDeliveryToAnotherStore |
      | Robina | Yes            | Yes     | home-delivery        | Yes                      | Yes                         |
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
      | 10000012 | 1   | 15    |
    And a gift card is added to cart with recipient details:
      | productCode       | firstName  | lastName  | email                            | messageText   |
      | PGC1000_iTunes_10 | Test First | Test Last | TestFirst.TestLast@target.com.au | Test Checkout |
    And delivery mode is 'home-delivery'
    And the delivery address is '19,Robina Town Centre Drive,Robina,QLD,4226'
    And order is placed
    And order is:
      | total | subtotal | deliveryCost |
      | 49    | 40       | 9            |
    And the instore consignment is rejected
    When cancel entries
      | product  | qty |
      | 10000011 | 1   |
      | 10000012 | 1   |
    Then refund value should be '39'
    And order is:
      | total | subtotal | deliveryCost |
      | 10    | 10       | 0            |

  Scenario: Partial cancel of physical order, no delivery fee refunded.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
      | 10000012 | 1   | 15    |
    And delivery mode is 'home-delivery'
    And order is placed
    And order is:
      | total | subtotal | deliveryCost |
      | 39    | 30       | 9            |
    When cancel entries
      | product  | qty |
      | 10000011 | 1   |
    Then refund value should be '15'
    And order is:
      | total | subtotal | deliveryCost |
      | 24    | 15       | 9            |

  Scenario: Partial cancel of physical items in mixed order, no delivery fee refunded.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
      | 10000012 | 1   | 15    |
    And a gift card is added to cart with recipient details:
      | productCode       | firstName  | lastName  | email                            | messageText   |
      | PGC1000_iTunes_10 | Test First | Test Last | TestFirst.TestLast@target.com.au | Test Checkout |
    And delivery mode is 'home-delivery'
    And order is placed
    And order is:
      | total | subtotal | deliveryCost |
      | 49    | 40       | 9            |
    When cancel entries
      | product  | qty |
      | 10000011 | 1   |
    Then refund value should be '15'
    And order is:
      | total | subtotal | deliveryCost |
      | 34    | 25       | 9            |

  Scenario: Partial cancel of physical items in mixed order re-routed to Fastline, no delivery fee refunded.
    Given stores with fulfilment capability and stock:
      | store  | instoreEnabled | inStock | deliveryModesAllowed | allowDeliveryToSameStore | allowDeliveryToAnotherStore |
      | Robina | Yes            | Yes     | home-delivery        | Yes                      | Yes                         |
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
      | 10000012 | 1   | 15    |
    And a gift card is added to cart with recipient details:
      | productCode       | firstName  | lastName  | email                            | messageText   |
      | PGC1000_iTunes_10 | Test First | Test Last | TestFirst.TestLast@target.com.au | Test Checkout |
    And delivery mode is 'home-delivery'
    And the delivery address is '19,Robina Town Centre Drive,Robina,QLD,4226'
    And order is placed
    And order is:
      | total | subtotal | deliveryCost |
      | 49    | 40       | 9            |
    And the instore consignment is rejected
    When cancel entries
      | product  | qty |
      | 10000011 | 1   |
    Then refund value should be '15'
    And order is:
      | total | subtotal | deliveryCost |
      | 34    | 25       | 9            |
