@voucherData
Feature: Vouchers can be applied to carts
  In order to drive sales, vouchers can be set up and given to users
  who can apply them to carts to get a discount.

  # Use standard test user test.user1@target.com.au
  # Test Voucher TV1 has value $10 and restriction on order value at least $11
 
  Scenario: A valid voucher is applied to a cart the total is reduced by the voucher value.
    Given a cart with entries:
      | product       | qty | price |
      | 10000011 | 2   | 10    |
    When apply voucher to cart VOUCHER-TV1
    Then cart total is 10

  Scenario: Apply an invalid voucher to a cart.
    Given a cart with entries:
      | product       | qty | price |
      | 10000011 | 2   | 10    |
    When apply voucher to cart ZZZZ
    Then cart total is 20

  Scenario: Apply a voucher but not enough in cart.
    Given a cart with entries:
      | product       | qty | price |
      | 10000011 | 1   | 10    |
    When apply voucher to cart VOUCHER-TV1
    Then cart total is 10

  Scenario Outline: Redeeming vouchers on baskets with varying quantity of item
    Given a cart with entries:
      | product       | qty | price |
      | 10000011 | <qty>   | 10    |
    When apply voucher to cart VOUCHER-TV1
    Then cart total is <total>

    Examples: 
      | qty | total | notes       |
      | 1   | 10    | no discount |
      | 2   | 10    | 10 discount |
      | 3   | 20    | 10 discount |
      | 4   | 30    | 10 discount |
