@initPreOrderProducts
Feature: Single Page Checkout - Show basket items and item totals in Order Summary
  As a mobile user who is in the Single Page Checkout
  I want to see the items in my basket listed in the Order Summary
  So that I know what I am purchasing
  
  Note: product used is setup as initiation
  
  Use 'valid' or 'invalid' for valid/invalid tmd numbers

  Scenario: Display basket items
    Given a cart with entries:
      | product       | qty | price |
      | P1001_black_M | 2   | 10    |
      | 80100100      | 1   | 20    |
      | CP2006        | 1   | 10    |
    When the order summary is displayed via spc
    Then the items in the order summary are:
      | baseProductCode | baseProductName             | topLevelCategory | Brand  | image                                | productHeading              | size | colour | qty | totalPrice |
      | P1001           | Baby Girls product          | Baby             | Target | /medias/product/51140021_product.jpg | Baby Girls product - Black  | M    | Black  | 2   | 20         |
      | P80100100       | Spider Man Action Figure    | Toys             | Target | /medias/product/80100100_product.jpg | Spider Man Action Figure    |      | Blue   | 1   | 20         |
      | P2006           | Body + Beauty Women product | Body + Beauty    | Target |                                      | Body + Beauty Women product |      |        | 1   | 10         |

  Scenario: Display basket items without deal items.
    Given a cart with entries:
      | product       | qty | price |
      | P1001_black_M | 2   | 10    |
      | 80100100      | 1   | 20    |
    When the order summary is displayed via spc
    Then price information for items in the order summary are:
      | price | totalPrice | totalItemPrice | discount | dealsApplied |
      | 10    | 20         | 20             | 0        | false        |
      | 20    | 20         | 20             | 0        | false        |

  Scenario: Display basket items with buy get deal items.
    Given set deal parameters:
      | name   | rewardType     | rewardValue | rewardMaxQty |
      | buyget | PercentOffEach | 50          | 1            |
    And cart entries with deals:
      | product  | qty | price | dealName | dealRole |
      | 10000011 | 2   | 50    | buyget   | Q        |
      | 10000012 | 2   | 20    | buyget   | R        |
    When the order summary is displayed via spc
    Then price information for items in the order summary are:
      | price | totalPrice | totalItemPrice | discount | dealsApplied |
      | 50    | 100        | 100            | 0        | true         |
      | 20    | 30         | 40             | 10       | true         |

  Scenario: Display basket items with tmd.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And customer apply tmd 'valid'
    When the order summary is displayed via spc
    Then price information for items in the order summary are:
      | price | totalPrice | totalItemPrice | discount | dealsApplied |
      | 15    | 24         | 30             | 6        | false        |

  Scenario: Display basket items with buy get deal items and tmd.
    Given set deal parameters:
      | name   | rewardType     | rewardValue | rewardMaxQty |
      | buyget | PercentOffEach | 50          | 1            |
    And cart entries with deals:
      | product  | qty | price | dealName | dealRole |
      | 10000011 | 2   | 50    | buyget   | Q        |
      | 10000012 | 2   | 20    | buyget   | R        |
    And customer apply tmd 'valid'
    When the order summary is displayed via spc
    Then price information for items in the order summary are:
      | price | totalPrice | totalItemPrice | discount | dealsApplied |
      | 50    | 80         | 100            | 20       | true         |
      | 20    | 24         | 40             | 16       | true         |

  Scenario: SPC Checkout with an Assorted product in the Cart
    Given an assorted product with code 'CP2006'
    And a cart with entries:
      | product | qty | price |
      | CP2006  | 1   | 10    |
    When the order summary is displayed via spc
    Then the items in the order summary are:
      | baseProductCode | baseProductName             | topLevelCategory | Brand  | image | productHeading              | size | colour | qty | totalPrice | assorted |
      | P2006           | Body + Beauty Women product | Body + Beauty    | Target |       | Body + Beauty Women product |      |        | 1   | 10         | true     |

  Scenario: Display initial deposit amount in cart summary SPC page for the pre-order product
    Given a cart with entries:
      | product          | qty | price |
      | V1111_preOrder_1 | 1   | 350   |
    When the order summary is displayed via spc
    Then the cart summary for pre-order product includes:
      | product          | totalPrice | preOrderDepositAmount |
      | V1111_preOrder_1 | 350.0      | 10.0                  |
