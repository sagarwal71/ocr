Feature: Product import
  Test normal imports
  As Target Online
  I want to do import of products from STEP

   Scenario: product Import and check exclude payment ZIP method flag as true
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | availableExpressDelivery | excludePaymentMethods |
      | P11112222   | Test Product | N     | normal      | V987654321  | W93746          |                   | target | Active         | N                 | N                      |        160 | test               | Y                     | women    | Y                        | zip			|
    When run STEP product import process
    Then product import response is successful
    And product 'V987654321' excludeForZipPayment is 'true'
 
   Scenario: product Import and check exclude payment ZIP method flag as false
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | availableExpressDelivery | excludePaymentMethods |
      | P11112222   | Test Product | N     | normal      | V987654321  | W93746          |                   | target | Active         | N                 | N                      |        160 | test               | Y                     | women    | Y                        | 			|
    When run STEP product import process
    Then product import response is successful
    And product 'V987654321' excludeForZipPayment is 'false'
    
   Scenario: product Import and check exclude payment ZIP method with multiple values flag as true
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | availableExpressDelivery | excludePaymentMethods |
      | P11112222   | Test Product | N     | normal      | V987654321  | W93746          |                   | target | Active         | N                 | N                      |        160 | test               | Y                     | women    | Y                        | zip,afterpay			|
    When run STEP product import process
    Then product import response is successful
    And product 'V987654321' excludeForZipPayment is 'true'