@cartProductData @deliveryModeData @initPreOrderProducts
Feature: IPG - Use ipg to place order using single credit card
  In order to purchase products
  As an online customer
  I want to use my credit card to make payment via ipg, and the order will be placed according to payment result and stock level.

  Background: 
    Given set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |

  Scenario: Placing an order with saved credit card.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    And payment method is 'ipg'
    And ipg returns saved cards
      | cardType | cardNumber       | cardExpiry | token     | tokenExpiry | bin | defaultCard |
      | VISA     | 4242424242424242 | 01/20      | 123456789 | 01/01/2020  | 34  | true        |
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And order is in status 'INPROGRESS'

  Scenario: Placing an order with successful payment and product in stock.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And guest checkout
    And payment method is 'ipg'
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And order is in status 'INPROGRESS'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 2        | 2         |

  Scenario Outline: IPG credit card check fails
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And guest checkout
    And payment method is 'ipg'
    And ipg check response is '<ipg_check_response_code>'
    When order is placed
    Then place order result is 'PAYMENT_FAILURE'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |

    Examples: 
      | ipg_check_response_code |
      | 0                       |
      | 2                       |
      | 3                       |
      | 4                       |

  Scenario: Place order with partial stock
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 4   | 15    |
    And guest checkout
    And payment method is 'ipg'
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And order is in status 'INPROGRESS'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 2        | 2         |

  Scenario: Place order with no stock
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
    And guest checkout
    And payment method is 'ipg'
    And set fastline stock:
      | product  | reserved | available |
      | 10000011 | 2        | 2         |
    When order is placed
    Then place order result is 'INSUFFICIENT_STOCK'

  Scenario: IPG credit card payment is unsuccessful
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And guest checkout
    And payment method is 'ipg'
    And ipg submit payment response is '1'
    When order is placed
    Then place order result is 'PAYMENT_FAILURE'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |

  Scenario: Placing an Order with IPG Credit Card as Registered User
    Given a cart with entries:
      | product  | qty | price |
      | 10000911 | 1   | 10    |
      | 10001030 | 2   | 15    |
      | 10000710 | 3   | 8     |
    And registered user checkout
    And payment method is 'ipg'
    And ipg returns saved cards
      | cardType | cardNumber       | cardExpiry | token     | tokenExpiry | bin | defaultCard |
      | VISA     | 5123450000000346 | 01/20      | 123456789 | 01/01/2020  | 34  | true        |
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 73.0  | 64.0     | 9.0          |          |
    And tlog entries are:
      | code     | qty | price | filePrice |
      | 10000911 | 1   | 1000  | 1000      |
      | 10001030 | 2   | 1500  | 1500      |
      | 10000710 | 3   | 800   | 800       |
    And tlog type is 'sale'
    And tlog shipping is 900
    And tlog total tender is 7300
    And tlog credit card payment info matches

  Scenario: Placing an Order with Pre-order product
    Given a cart with entries:
      | product               | qty | price  |
      | V5555_preOrder_5      | 1   | 400    |
    And registered user checkout
    And payment method is 'ipg'
    When pre order is placed
    And order is in status 'PARKED'

  @notAutomated
  Scenario: Placing an Order with Pre Order product with payment difference less than 0.005 from InitialDeposit and amount paid
    Given a cart with entries:
      | product               | qty | price  |
      | V5555_preOrder_5      | 1   | 400    |
    And registered user checkout
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | CC1, 9.996   |
      When pre order is placed
    And order is in status 'PARKED'

  @notAutomated
  Scenario: Placing an Order with Pre Order product with payment difference greater than 0.005 from InitialDeposit and amount paid
    Given a cart with entries:
      | product               | qty | price  |
      | V5555_preOrder_5      | 1   | 400    |
    And registered user checkout
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | CC1, 9.995   |
    When pre order is placed
    And order is in status 'REJECTED'

  Scenario: Placing an Order with Pre Order product with ACCEPT response from Fraud Check
    Given a cart with entries:
      | product               | qty | price  |
      | V5555_preOrder_5      | 1   | 400    |
    And registered user checkout
    And payment method is 'ipg'
    When pre order is placed
    Then the order would trigger an Accertify response of 'ACCEPT'
    And order is in status 'PARKED'

  Scenario: Placing an Order with Pre Order product with RETRYEXCEEDED response from Fraud Check
    Given a cart with entries:
      | product               | qty | price  |
      | V5555_preOrder_5      | 1   | 400    |
    And registered user checkout
    And payment method is 'ipg'
    When pre order is placed
    Then the order would trigger an Accertify response of 'RETRYEXCEEDED'
    And order is in status 'PARKED'

  @notAutomated
  Scenario: Placing an Order with Pre Order product with RETRYEXCEEDED response from Fraud Check and successful email sent to Accertify
    Given a cart with entries:
      | product               | qty | price  |
      | V5555_preOrder_5      | 1   | 400    |
    And registered user checkout
    And payment method is 'ipg'
    When pre order is placed
    And the order would trigger an Accertify response of 'RETRYEXCEEDED'
    Then order is in status 'PARKED'
    And the asset protection email has been sent

  @fluentFeatureSwitch
  Scenario: Placing an Order using fluent
    Given products with the following ats values in fluent:
      | product  | CC | HD | ED |
      | 10000911 | 10 | 10 | 10 |
      | 10001030 | 10 | 10 | 10 |
      | 10000710 | 10 | 10 | 10 |
    And a cart with entries:
      | product  | qty | price |
      | 10000911 | 1   | 10    |
      | 10001030 | 2   | 15    |
      | 10000710 | 3   | 8     |
    And registered user checkout
    And payment method is 'ipg'
    And ipg returns saved cards
      | cardType | cardNumber       | cardExpiry | token     | tokenExpiry | bin | defaultCard |
      | VISA     | 5123450000000346 | 01/20      | 123456789 | 01/01/2020  | 34  | true        |
    And fluent 'createPreOrder' api returns fluent id '456'
    And the fluent fulfilment reponse is:
      | orderId | fulfilmentId | locationRef | fulfilmentType | deliveryType | status  |
      | 456     | auto234      | 7032        | HD_PFS         | STANDARD     | CREATED |
    
    When fluent order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 73.0  | 64.0     | 9.0          |          |
    And tlog entries are:
      | code     | qty | price | filePrice |
      | 10000911 | 1   | 1000  | 1000      |
      | 10001030 | 2   | 1500  | 1500      |
      | 10000710 | 3   | 800   | 800       |
    And tlog type is 'sale'
    And tlog shipping is 900
    And tlog total tender is 7300
    And tlog credit card payment info matches

  @fluentFeatureSwitch
  Scenario: Placing an Order using fluent that fails
    Given products with the following ats values in fluent:
      | product  | CC | HD | ED |
      | 10000911 | 10 | 10 | 10 |
      | 10001030 | 10 | 10 | 10 |
      | 10000710 | 10 | 10 | 10 |
    And fluent event response is:
      | eventId | entityId | eventStatus | errorMessage |
      | 1       | 1        | SUCCESS     |              |
    And a cart with entries:
      | product  | qty | price |
      | 10000911 | 1   | 10    |
      | 10001030 | 2   | 15    |
      | 10000710 | 3   | 8     |
    And registered user checkout
    And payment method is 'ipg'
    And the order would trigger an Accertify response of 'REJECT'
    And the IPG refund will 'succeed' for the order
    And fluent 'createPreOrder' api returns fluent id '456'
    When fluent order is placed with errors
    And reject order process finishes with status 'SUCCEEDED'
    Then event sent to fluent is:
      | name         | entityType |
      | CANCEL_ORDER | ORDER      |
    And the IPG refund has been trigerred for the order with amount '73'

  @fluentFeatureSwitch
  Scenario: Cancelling order at fluent fails for a cancelled order
    Given products with the following ats values in fluent:
      | product  | CC | HD | ED |
      | 10000911 | 10 | 10 | 10 |
      | 10001030 | 10 | 10 | 10 |
      | 10000710 | 10 | 10 | 10 |
    And fluent event response is:
      | eventId | entityId | eventStatus | errorMessage     |
      | 1       | 1        | FAILED      | Event has Failed |
    And a cart with entries:
      | product  | qty | price |
      | 10000911 | 1   | 10    |
      | 10001030 | 2   | 15    |
      | 10000710 | 3   | 8     |
    And registered user checkout
    And payment method is 'ipg'
    And the order would trigger an Accertify response of 'REJECT'
    And fluent 'createPreOrder' api returns fluent id '456'
    When fluent order is placed with errors
    And reject order process finishes with status 'ERROR'
    Then event sent to fluent is:
      | name         | entityType |
      | CANCEL_ORDER | ORDER      |
    And the IPG refund is not triggered for the order
