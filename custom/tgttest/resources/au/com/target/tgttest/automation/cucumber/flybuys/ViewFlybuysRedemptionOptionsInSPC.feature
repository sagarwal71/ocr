@rollback @cartProductData @initPreOrderProducts
Feature: Flybuys - View flybuys redemption options
  In order to control the flybuys redemption options, 
  as Target online store we can set configurable max redeemable amount
  and options are limited by order value and points balance.

  Background: 
    Given flybuys config max redeemable amount is 50
    And a registered customer goes into the checkout process
    And customer has valid flybuys number presented
    And the user entered flybuys details are 'valid'

  Scenario: show all redemption options where limited from the maximum configured.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 150   |
    And Customer has available points '120000'
    When the customer logins flybuys in spc
    Then the customer will be presented with the redemption options in spc
      | dollarAmt | points | code             |
      | 50        | 10000  | DUMMYREDEEMCODE5 |
      | 40        | 8000   | DUMMYREDEEMCODE4 |
      | 30        | 6000   | DUMMYREDEEMCODE3 |
      | 20        | 4000   | DUMMYREDEEMCODE2 |
      | 10        | 2000   | DUMMYREDEEMCODE1 |

  Scenario: show all redemption options up to order value
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 21    |
    And Customer has available points '11000'
    When the customer logins flybuys in spc
    Then the customer will be presented with the redemption options in spc
      | dollarAmt | points | code             |
      | 40        | 8000   | DUMMYREDEEMCODE4 |
      | 30        | 6000   | DUMMYREDEEMCODE3 |
      | 20        | 4000   | DUMMYREDEEMCODE2 |
      | 10        | 2000   | DUMMYREDEEMCODE1 |

  Scenario: show all redemption options up to points balance
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 200   |
    And Customer has available points '2500'
    When the customer logins flybuys in spc
    Then the customer will be presented with the redemption options in spc
      | dollarAmt | points | code             |
      | 10        | 2000   | DUMMYREDEEMCODE1 |

  Scenario: Valid card details but not enough points to redeem
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 200   |
    And Customer has available points '500'
    When the customer logins flybuys in spc
    Then the customer will be presented with the redemption options in spc
      | dollarAmt | points | code |

   Scenario: Customer redeeming flybuys points when pre-order item in cart
    Given a cart with entries:
      | product          | qty |
      | V1111_preOrder_1 | 1   |
    When the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE1'
    Then customer is not allowed to redeem flybuys
  