@cartProductData @cleanOrderData @cleanConsignmentData @storeFulfilmentCapabilitiesData @forceDefaultWarehouseToOnlinePOS
Feature: Instore Fulfilment - Verify a picked and packed consignment contains a tracking id.

  Background: 
    Given stores with fulfilment capability and stock:
      | store       | storeNumber | merchantLocationId | state | instoreEnabled | allowDeliveryToSameStore | allowDeliveryToAnotherStore | inStock |
      | Robina      | 7126        | MRO                | QLD   | Yes            | Yes                      | Yes                         | Yes     |
      | Rockhampton | 7049        | MROCK              | QLD   | Yes            | Yes                      | Yes                         | Yes     |

  Scenario: Order is placed to a store that is enabled for instore fulfilment. Consignment is picked instore. Picked consignment should not have a tracking id. OfcOrder Type is InStore
    Given any cart
    And delivery mode is 'click-and-collect'
    And order cnc store is '7126'
    When order is placed
    And the consignment is completed instore
    Then the consignment has no tracking id

  Scenario: Order is placed to a store that is not enabled for instore fulfilment. Consignment is picked for interstore delivery. Picked consignment should have a tracking id.
    Given any cart
    And delivery mode is 'click-and-collect'
    And order cnc store is '7038'
    When order is placed
    And the consignment is completed instore
    Then the consignment has a valid tracking id that starts with 'MRO'

  Scenario: Order is placed with delivery type home delivery. Consignment is picked instore by a store enabled for fulfilment.
    Picked consignment should have a tracking id.

    Given any home-delivery cart
    And the delivery address is '446,Ann Street,Brisbane City,QLD,4000'
    When order is placed
    And the consignment is completed instore
    Then the consignment has a valid tracking id that starts with 'MRO'
    And consignment is not sent to Fastline

  Scenario: Order is placed to a store that is enabled for instore fulfilment. Consignment is rejected and sent to another store for fulfilment.
    Consignment is picked in second store. Picked consignment should have a tracking id.

    Given System-wide max number of routing attempts is '2'
    Given any cart
    And delivery mode is 'click-and-collect'
    And order cnc store is '7126'
    When order is placed
    And the consignment is rejected
    And the consignment re-routed to warehouse 'Rockhampton' is waved and picked instore
    And the consignment is completed instore after being picked
    Then the consignment has a valid tracking id that starts with 'MROCK'
