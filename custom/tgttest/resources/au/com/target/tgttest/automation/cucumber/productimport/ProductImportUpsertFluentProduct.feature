@fluentFeatureSwitch @initFluentData
Feature: Product import with fluent data feed
  Test normal imports
  As Target Online
  I want to do import of products from STEP and upsert the product feed to fluent

  Background: 
    Given category 'W93746' has fluentId '93746'
    And the 'productimport.expressbyflag' feature switch is disabled

  Scenario: product import creates product in fluent for product without fluentId
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | ean           | merchProductStatus |
      | 11112226    | Test Product | N     | normal      | V987654325  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    | 9317669671603 | Active             |
    And fluent 'createProduct' api returns fluent id '11112226'
    And fluent 'createSku' api returns fluent id '987654325'
    When run STEP product import process
    Then product import response is successful
    And fluent product api gets called with basic data:
      | productRef | name         | status |
      | P11112226  | Test Product | ACTIVE |
    And fluent product api gets called with category id:
      | 93746 |
    And fluent product api gets called with customized attribute data:
      | name              | value  | type   |
      | PRODUCT_TYPE      | normal | STRING |
      | DISPLAY_ONLY      | FALSE  | STRING |
      | PREVIEW           | FALSE  | STRING |
      | CLICK_AND_COLLECT | TRUE   | STRING |
      | HOME_DELIVERY     | TRUE   | STRING |
      | EXPRESS_DELIVERY  | TRUE   | STRING |
      | EMAIL_DELIVERY    | FALSE  | STRING |
    And product has:
      | fluentId |
      | 11112226 |
    And fluent 'product' update status is:
      | code      | fluentId | lastRunSuccessful |
      | P11112226 | 11112226 | true              |
    And fluent sku api gets called with:
      | skuRef     | productRef | name         | status |
      | V987654325 | P11112226  | Test Product | ACTIVE |
    And fluent sku api gets called with references:
      | type | value         |
      | EAN  | 9317669671603 |
    And fluent sku api gets called with attributes:
      | name                 | type    | value                      |
      | SWATCH               | STRING  | No Colour                  |
      | DEPARTMENT           | STRING  | LADIES FASHION ACCESSORIES |
      | ASSORTED             | STRING  | FALSE                      |
      | MERCH_PRODUCT_STATUS | STRING  | Active                     |
      | EBAY                 | STRING  | FALSE                      |
    And fluent 'sku' update status is:
      | code       | fluentId  | lastRunSuccessful |
      | V987654325 | 987654325 | true              |

  Scenario: product import creates product in fluent for product without fluentId (preorder attributes)
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | ean           | merchProductStatus | preOrderStartDate   | preOrderEndDate     | preOrderEmbargoReleaseDate | preOrderOnlineQuantity |
      | 11112229    | Test Product | N     | normal      | V987654333  | W93746          |                   | target | Active         | N                 | N                      |        160 | test               | Y                     | women    | 9317669671603 | Active             | 2018-01-21 09:30:00 | 2018-02-02 11:30:00 | 2099-03-03 11:30:00        |  100                   |
    And fluent 'createProduct' api returns fluent id '11112229'
    And fluent 'createSku' api returns fluent id '987654326'
  	And fluent event response is:
      | eventId | eventStatus | errorMessage |
      |       1 | SUCESS      |              |
    When run STEP product import process
    Then product import response is successful
    And fluent product api gets called with basic data:
      | productRef | name         | status |
      | P11112229  | Test Product | ACTIVE |
    And fluent product api gets called with category id:
      | 93746 |
    And fluent product api gets called with customized attribute data:
      | name              | value  | type   |
      | PRODUCT_TYPE      | normal | STRING |
      | DISPLAY_ONLY      | FALSE  | STRING |
      | PREVIEW           | FALSE  | STRING |
      | CLICK_AND_COLLECT | TRUE   | STRING |
      | HOME_DELIVERY     | TRUE   | STRING |
      | EXPRESS_DELIVERY  | TRUE   | STRING |
      | EMAIL_DELIVERY    | FALSE  | STRING |
    And product has:
      | fluentId |preOrderOnlineQuantity|
      | 11112229 | 100                  |
    And fluent 'product' update status is:
      | code      | fluentId | lastRunSuccessful |
      | P11112229 | 11112229 | true              |
    And fluent sku api gets called with:
      | skuRef     | productRef | name         | status |
      | V987654333 | P11112229  | Test Product | ACTIVE |
    And fluent sku api gets called with references:
      | type | value         |
      | EAN  | 9317669671603 |
    And fluent sku api gets called with attributes:
      | name                 | type    | value                      |
      | SWATCH               | STRING  | No Colour                  |
      | DEPARTMENT           | STRING  | LADIES FASHION ACCESSORIES |
      | ASSORTED             | STRING  | FALSE                      |
      | MERCH_PRODUCT_STATUS | STRING  | Active                     |
      | EBAY                 | STRING  | FALSE                      |
      | PREORDER_START_DATE  | STRING  | 2018-01-20T22:30:00.000Z   |
      | PREORDER_END_DATE    | STRING  | 2018-02-02T00:30:00.000Z   |
      | SALE_START_DATE      | STRING  | 2099-03-03T00:30:00.000Z   |
    And fluent 'sku' update status is:
      | code       | fluentId  | lastRunSuccessful |
      | V987654333 | 987654326 | true              |      
    And preorder inventory event sent to fluent is:
      | name                  | entityType         | entitySubtype      | rootEntityType      |
      | PREORDER_LOAD_MAX_QTY | INVENTORY_CATALOGUE| DEFAULT            | INVENTORY_CATALOGUE |
    And preorder inventory data is:
      | skuRef         | quantity |
      | V987654333     |     100  |

  Scenario: product import updates product in fluent for product without fluentId when product is already in fluent after finding its fluentId
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | ean           | merchProductStatus |
      | 11112226    | Test Product | N     | normal      | V987654325  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    | 9317669671603 | Active             |
    And fluent 'createProduct' api returns error code '409'
    And fluent 'searchProduct' api returns fluent id '11112226'
    And fluent 'updateProduct' api returns fluent id '11112226'
    And fluent 'createSku' api returns error code '400'
    And fluent 'searchSku' api returns fluent id '987654325'
    And fluent 'updateSku' api returns fluent id '987654325'
    When run STEP product import process
    Then product import response is successful
    And fluent product api gets called with basic data:
      | productRef | name         | status |
      | P11112226  | Test Product | ACTIVE |
    And fluent product api gets called with category id:
      | 93746 |
    And fluent product api gets called with customized attribute data:
      | name              | value  | type   |
      | PRODUCT_TYPE      | normal | STRING |
      | DISPLAY_ONLY      | FALSE  | STRING |
      | PREVIEW           | FALSE  | STRING |
      | CLICK_AND_COLLECT | TRUE   | STRING |
      | HOME_DELIVERY     | TRUE   | STRING |
      | EXPRESS_DELIVERY  | TRUE   | STRING |
      | EMAIL_DELIVERY    | FALSE  | STRING |
    And product has:
      | fluentId |
      | 11112226 |
    And fluent 'product' update status is:
      | code      | fluentId | lastRunSuccessful |
      | P11112226 | 11112226 | true              |
    And fluent sku api gets called with:
      | skuRef     | productRef | name         | status |
      | V987654325 | P11112226  | Test Product | ACTIVE |
    And fluent sku api gets called with references:
      | type | value         |
      | EAN  | 9317669671603 |
    And fluent sku api gets called with attributes:
      | name                 | type    | value                      |
      | SWATCH               | STRING  | No Colour                  |
      | DEPARTMENT           | STRING  | LADIES FASHION ACCESSORIES |
      | ASSORTED             | STRING  | FALSE                      |
      | MERCH_PRODUCT_STATUS | STRING  | Active                     |
      | EBAY                 | STRING  | FALSE                      |
    And fluent 'sku' update status is:
      | code       | fluentId  | lastRunSuccessful |
      | V987654325 | 987654325 | true              |

  Scenario: product import updates product in fluent for product with fluentId(preorder attributes) without maxpreOrderqty
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | ean           | merchProductStatus | preOrderStartDate   | preOrderEndDate     | preOrderEmbargoReleaseDate |
      |    11112228 | Test Product | N     | normal      | V987654326  | W93746          |                   | target | Active         | N                 | N                      |        160 | test               | Y                     | women    | 9317669671603 | Active             | 2019-01-21 09:30:00 | 2019-02-02 11:30:00 | 2099-03-03 11:30:00        |
    And product 'P11112228' has fluentId '11112228'
    And productVariant 'V987654326' has fluentId '987654326'
    And fluent 'updateProduct' api returns fluent id '11112228'
    And fluent 'updateSku' api returns fluent id '987654326'
    And fluent event response is:
      | eventId | eventStatus | errorMessage |
      |       1 | SUCCESS      |             |
    When run STEP product import process
    Then product import response is successful
    And fluent product api gets called with basic data:
      | productRef | name         | status |
      | P11112228  | Test Product | ACTIVE |
    And fluent product api gets called with category id:
      | 93746 |
    And fluent product api gets called with customized attribute data:
      | name              | value  | type   |
      | PRODUCT_TYPE      | normal | STRING |
      | DISPLAY_ONLY      | FALSE  | STRING |
      | PREVIEW           | FALSE  | STRING |
      | CLICK_AND_COLLECT | TRUE   | STRING |
      | HOME_DELIVERY     | TRUE   | STRING |
      | EXPRESS_DELIVERY  | TRUE   | STRING |
      | EMAIL_DELIVERY    | FALSE  | STRING |
    And product has:
      | fluentId |
      | 11112228 |
    And fluent 'product' update status is:
      | code      | fluentId | lastRunSuccessful |
      | P11112228 | 11112228 | true              |
    And fluent sku api gets called with:
      | skuRef     | productRef | name         | status |
      | V987654326 | P11112228  | Test Product | ACTIVE |
    And fluent sku api gets called with references:
      | type | value         |
      | EAN  | 9317669671603 |
    And fluent sku api gets called with attributes:
      | name                 | type    | value                      |
      | SWATCH               | STRING  | No Colour                  |
      | DEPARTMENT           | STRING  | LADIES FASHION ACCESSORIES |
      | ASSORTED             | STRING  | FALSE                      |
      | MERCH_PRODUCT_STATUS | STRING  | Active                     |
      | EBAY                 | STRING  | FALSE                      |
      | PREORDER_START_DATE  | STRING  | 2019-01-20T22:30:00.000Z   |
      | PREORDER_END_DATE    | STRING  | 2019-02-02T00:30:00.000Z   |
      | SALE_START_DATE      | STRING  | 2099-03-03T00:30:00.000Z   |
    And fluent 'sku' update status is:
      | code       | fluentId  | lastRunSuccessful |
      | V987654326 | 987654326 | true              |
    And event is not sent to fluent


  Scenario: product import updates product in fluent for product with fluentId
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | ean           | merchProductStatus |
      | 11112226    | Test Product | N     | normal      | V987654325  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    | 9317669671603 | Active             |
    And product 'P11112226' has fluentId '11112226'
    And productVariant 'V987654325' has fluentId '987654325'
    And fluent 'updateProduct' api returns fluent id '11112226'
    And fluent 'updateSku' api returns fluent id '987654325'
    When run STEP product import process
    Then product import response is successful
    And fluent product api gets called with basic data:
      | productRef | name         | status |
      | P11112226  | Test Product | ACTIVE |
    And fluent product api gets called with category id:
      | 93746 |
    And fluent product api gets called with customized attribute data:
      | name              | value  | type   |
      | PRODUCT_TYPE      | normal | STRING |
      | DISPLAY_ONLY      | FALSE  | STRING |
      | PREVIEW           | FALSE  | STRING |
      | CLICK_AND_COLLECT | TRUE   | STRING |
      | HOME_DELIVERY     | TRUE   | STRING |
      | EXPRESS_DELIVERY  | TRUE   | STRING |
      | EMAIL_DELIVERY    | FALSE  | STRING |
    And product has:
      | fluentId |
      | 11112226 |
    And fluent 'product' update status is:
      | code      | fluentId | lastRunSuccessful |
      | P11112226 | 11112226 | true              |
    And fluent sku api gets called with:
      | skuRef     | productRef | name         | status |
      | V987654325 | P11112226  | Test Product | ACTIVE |
    And fluent sku api gets called with references:
      | type | value         |
      | EAN  | 9317669671603 |
    And fluent sku api gets called with attributes:
      | name                 | type    | value                      |
      | SWATCH               | STRING  | No Colour                  |
      | DEPARTMENT           | STRING  | LADIES FASHION ACCESSORIES |
      | ASSORTED             | STRING  | FALSE                      |
      | MERCH_PRODUCT_STATUS | STRING  | Active                     |
      | EBAY                 | STRING  | FALSE                      |
    And fluent 'sku' update status is:
      | code       | fluentId  | lastRunSuccessful |
      | V987654325 | 987654325 | true              |

  Scenario: product import creates product in fluent for product with fluentId when it is not really present in fluent
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | ean           | merchProductStatus |
      | 11112226    | Test Product | N     | normal      | V987654325  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    | 9317669671603 | Active             |
    And product 'P11112226' has fluentId '11112226'
    And productVariant 'V987654325' has fluentId '987654325'
    And fluent 'updateProduct' api returns error code '404'
    And fluent 'createProduct' api returns fluent id '11112226'
    And fluent 'updateSku' api returns error code '404'
    And fluent 'createSku' api returns fluent id '987654325'
    When run STEP product import process
    Then product import response is successful
    And fluent product api gets called with basic data:
      | productRef | name         | status |
      | P11112226  | Test Product | ACTIVE |
    And fluent product api gets called with category id:
      | 93746 |
    And fluent product api gets called with customized attribute data:
      | name              | value  | type   |
      | PRODUCT_TYPE      | normal | STRING |
      | DISPLAY_ONLY      | FALSE  | STRING |
      | PREVIEW           | FALSE  | STRING |
      | CLICK_AND_COLLECT | TRUE   | STRING |
      | HOME_DELIVERY     | TRUE   | STRING |
      | EXPRESS_DELIVERY  | TRUE   | STRING |
      | EMAIL_DELIVERY    | FALSE  | STRING |
    And product has:
      | fluentId |
      | 11112226 |
    And fluent 'product' update status is:
      | code      | fluentId | lastRunSuccessful |
      | P11112226 | 11112226 | true              |
    And fluent sku api gets called with:
      | skuRef     | productRef | name         | status |
      | V987654325 | P11112226  | Test Product | ACTIVE |
    And fluent sku api gets called with references:
      | type | value         |
      | EAN  | 9317669671603 |
    And fluent sku api gets called with attributes:
      | name                 | type    | value                      |
      | SWATCH               | STRING  | No Colour                  |
      | DEPARTMENT           | STRING  | LADIES FASHION ACCESSORIES |
      | ASSORTED             | STRING  | FALSE                      |
      | MERCH_PRODUCT_STATUS | STRING  | Active                     |
      | EBAY                 | STRING  | FALSE                      |
    And fluent 'sku' update status is:
      | code       | fluentId  | lastRunSuccessful |
      | V987654325 | 987654325 | true              |

  Scenario: product Import and create product in fluent - fluent service 503 error
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | 11112225    | Test Product | N     | normal      | V987654330  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    |
    And fluent 'createProduct' api returns error code '503'
    And fluent 'createSku' api returns error code '503'
    When run STEP product import process
    Then product import response is successful
    And product has:
      | fluentId |
      | null     |
    And fluent 'product' update status is:
      | code      | fluentId | lastRunSuccessful | errorCode |
      | P11112225 | null     | false             | 503       |

  Scenario: product Import and create product in fluent - fluent service 503 error for sku
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | 11112227    | Test Product | N     | normal      | V987654322  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    |
    And fluent 'createProduct' api returns fluent id '11112227'
    And fluent 'createSku' api returns error code '503'
    When run STEP product import process
    Then product import response is successful
    And product has:
      | fluentId |
      | 11112227 |
    And fluent 'product' update status is:
      | code      | fluentId | lastRunSuccessful |
      | P11112227 | 11112227 | true              |
    And fluent 'sku' update status is:
      | code       | fluentId | lastRunSuccessful | errorCode |
      | V987654322 | null     | false             | 503       |

  Scenario: product Import and update product in fluent - fluent service 503 error
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | 11112226    | Test Product | N     | normal      | V987654325  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    |
    And product 'P11112226' has fluentId '11112226'
    And productVariant 'V987654325' has fluentId '987654325'
    And fluent 'updateProduct' api returns error code '503'
    And fluent 'updateSku' api returns error code '503'
    When run STEP product import process
    Then product import response is successful
    And product has:
      | fluentId |
      | 11112226 |
    And fluent 'product' update status is:
      | code      | fluentId | lastRunSuccessful | errorCode |
      | P11112226 | 11112226 | false             | 503       |
    And fluent 'sku' update status is:
      | code       | fluentId  | lastRunSuccessful | errorCode |
      | V987654325 | 987654325 | false             | 503       |
