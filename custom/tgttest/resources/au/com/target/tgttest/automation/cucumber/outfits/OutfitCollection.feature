@notAutomated
Feature: Outfit Collection page - storefront
    In order to view, customise collection of products as a whole outfit
    As an online customer 
    I want to see Collection information

  Scenario Outline: Online customer are able to navigate to collections page
    Given collection is 'APPROVED'
    And online user loads top level category <topLevelCategory>
    And collection <collection> is listed in the collections section
    And choose to navigate to collection <collection> page
    Then collection page loads
    And breadcrumb is visible as <collection> under <topLevelCategory>
    And collection hero image <heroImage> is visible
    And description <description> is available
    And looks <looksInCollection> can be seen
    And each look has a hero image

    Examples: 
      | collection        | topLevelCategory | title              | description                  | heroImage        | looksInCollection              |
      | SummerCollection  | Women            | Summer Collection  | Summer Collection Trends     | summerHeroImage  | Look11, Look12, Look13, Look14 |
      | AutumnCollection  | Women            | Autumb Collection  |                              | AutumnHeroImage  | Look21, Look22                 |
      | WinterCollection  | Women            |                    | Winter Collection Trends     |                  | Look31                         |
      | KitchenCollection | Home             | Kitchen Collection | Essential Kitchen Collection | KitchenHeroImage |                                |

  Scenario Outline: Online customers can see from price of collection
    Given collection page is loaded
    When look <look> is listed in collection <collection> page
    Then from price <fromPrice> is available for look <look>

    Examples: 
      | collection       | look       | fromPrice |
      | SummerCollection | OfficeLook | 75        |
      | SummerCollection | PartyLook  |           |

  Scenario: Unapproved collections are not visible to online customers
    Given the collection is not 'APPROVED'
    When online customer opens the top level category <topLevelCategory>
    Then online customer cannot see collection information in collections section
