@customerData
Feature: Customer Account Activities
  In order to checkout more quickly and view past orders, as a customer I want to register.

  Scenario: Customer doesnt have target account and register for one
    Given that the user doesn't have a Target account
    When the user creates a new account
    Then the customer account is created

  Scenario: Customer has a Target customer account and register again
    Given that the customer has a Target account
    When the user creates a new account
    Then the customer account is not created

  Scenario: Modify customer firstName
    Given that the customer has a Target account
    When the customer modifies first name
    Then the customer account is updated successfully

  Scenario: Modify customer emailId
    Given that the customer has a Target account
    When the customer modifies emailAddress
    Then the customer email Address is updated successfully

  Scenario: Modify customer emailId with invalid password
    Given that the customer has a Target account
    When the customer modifies emailAddress with invalid password
    Then the customer email Address is not updated

  Scenario: Modify Customer Account existing address
    Given that the customer has a Target account
    When the user modifies the saved address
    Then the customer Address is updated successfully

  Scenario: Remove Customer Account saved address
    Given that the customer has a Target account
    When the customer removes the address
    Then the Address is removed from Customer Account

  Scenario: Add an address to a Customer Account
    Given that the customer has a Target account
    When the customer adds a new address
    Then the new Address gets added to customer account

  @notAutomated
  Scenario: Saving first card in IPG saves the masked card details in My Account
    Given the customer has no saved card
    And the customer have chosen to save a card during payment in IPG
    When the customer looks at payment details in My Account
    Then the customer will see the masked card details of the saved card marked as primary

  @notAutomated
  Scenario: Saving a card in IPG saves the masked card details in My Account
    Given the customer has saved cards
    And the customer have chosen to save a new card during payment in IPG
    When the customer looks at payment details in My Account
    Then the customer will see the masked card details of the newly saved card
    And the card will not be marked as primary

  @notAutomated
  Scenario: Marking a card as a primary card will make it my primary card in My Account
    Given the customer have chosen to save a card as primary during payment in IPG
    When the customer looks at payment details in My Account
    Then the customer will see the saved card marked as primary
    And other saved cards will not be marked as primary

  @notAutomated
  Scenario: Customer requests to see order details when order fulfilled by Fastline
    Given that the customer has a Target account
    And the customer has placed an order in past
    And the consignment has been allocated to 'Fastline'
    When the customer requests to see the order details
    Then the order details contains the consignment details from 'Fastline'

  @notAutomated
  Scenario: Customer requests to see order details when order fulfilled by store
    Given that the customer has a Target account
    And the customer has placed an order in past
    And the consignment has been allocated to 'Store'
    When the customer requests to see the order details
    Then the order details contains the consignment details	from 'Store'

  @notAutomated
  Scenario: Customer requests to see order details when order rejected by store and routed to Fastline
    Given that the customer has a Target account
    And the customer has placed an order in past
    And the consignment has been rejected by the store
    When the customer requests to see the order details
    Then the order details contains the consignment details from 'Fastline' only

  @notAutomated
   Scenario: Customer orders a preOrder with non saved card, and that card is not saved/visible in my account section.
    Given the customer has saved cards
    And the customer has used a new card  while making a preOrder
    When the customer looks at payment details in My Account
    Then the customer will not see the new card
    
  @notAutomated   
   Scenario: Pre-order item available info is displayed successfully in Order Details Summary 
	Given that the customer has a Target account
    And the customer has placed an order in the past
    When the customer requests to see the order details
    Then the order details summary contains the pre-order details with productDisplayType and normalSaleStartDateTime