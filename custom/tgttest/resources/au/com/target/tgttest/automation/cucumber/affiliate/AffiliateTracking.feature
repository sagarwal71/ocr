@cartProductData @voucherData @deliveryModeData
Feature: Affiliates - Tracking tag data
  In order to increase site traffic, target needs to send commission data to affililates 
  so that they can bill us accurately.

  Scenario Outline: Placing an order with no deals or discounts in same affiliate category.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 20    |
      | 10000012 | 3   | 10    |
    And delivery mode is 'home-delivery'
    And payment method is '<payment_method>'
    And order is placed
    When thankyou page data is retrieved
    Then affiliate data is:
      | codes   | quantities | prices |
      | Apparel | 5          | 7000   |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Placing an order with no deals or discounts in different affiliate category.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 20    |
      | 10003001 | 3   | 30    |
    And delivery mode is 'home-delivery'
    And payment method is '<payment_method>'
    And order is placed
    When thankyou page data is retrieved
    Then affiliate data is:
      | codes             | quantities | prices     |
      | Apparel\|Homeware | 2\|3       | 4000\|9000 |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Placing an order with TMD
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 20    |
      | 10000012 | 1   | 10    |
    And delivery mode is 'home-delivery'
    And customer presents tmd 'valid'
    And payment method is '<payment_method>'
    And order is placed
    When thankyou page data is retrieved
    Then order total is 49.0
    And affiliate data is:
      | codes   | quantities | prices |
      | Apparel | 3          | 4000   |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Placing an order with Voucher
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 20    |
      | 10000012 | 1   | 10    |
    And delivery mode is 'home-delivery'
    And customer presents voucher 'VOUCHER-TV1'
    And payment method is '<payment_method>'
    And order is placed
    When thankyou page data is retrieved
    Then order total is 49.0
    And affiliate data is:
      | codes             | quantities | prices      |
      | Apparel\|Discount | 3\|0       | 5000\|-1000 |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Placing an order with Fly Buys
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 20    |
      | 10000012 | 1   | 10    |
    And delivery mode is 'home-delivery'
    And customer presents flybuys card '6008943218616910'
    And payment method is '<payment_method>'
    And order is placed
    When thankyou page data is retrieved
    Then order total is 59.0
    And affiliate data is:
      | codes   | quantities | prices |
      | Apparel | 3          | 5000   |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Placing an order with buy get deal items.
    Given set deal parameters:
      | name   | rewardType     | rewardValue | rewardMaxQty |
      | buyget | PercentOffEach | 50          | 1            |
    And cart entries with deals:
      | product  | qty | price | dealName | dealRole |
      | 10000011 | 2   | 50    | buyget   | Q        |
      | 10000012 | 2   | 20    | buyget   | R        |
    And delivery mode is 'home-delivery'
    And payment method is '<payment_method>'
    And order is placed
    When thankyou page data is retrieved
    Then order total is 130.0
    And affiliate data is:
      | codes   | quantities | prices |
      | Apparel | 4          | 13000  |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |
