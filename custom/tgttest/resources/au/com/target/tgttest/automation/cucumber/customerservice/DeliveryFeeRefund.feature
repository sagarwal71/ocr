@cartProductData @cleanOrderData @cleanConsignmentData
Feature: Refund Delivery Fee
  
  In order to refund delivery fee
  As a CS Agent I want to be able to refund delivery fee only without any product refunds

  Scenario Outline: Delivery fee refund option is available
    Given an order with <deliveryAmount> as delivery fee
    When Customer service agent views the order
    Then the option to refund delivery fee will be <available>

    Examples: 
      | deliveryAmount | available |
      | 9              | True      |
      | 0              | False     |

  Scenario Outline: Delivery fee is available and refunded once
    Given an order with <deliveryAmount> as delivery fee
    When delivery fee refund amount is <refundAmount>
    Then delivery fee refund request is <refundStatus>

    Examples: 
      | deliveryAmount | refundAmount | refundStatus |
      | 9              | 8            | refunded     |
      | 9              | 10           | not refunded |
      | 9              | 9            | refunded     |

  Scenario Outline: Delivery fee is available and refunded multiple times
    Given an order with <deliveryAmount> as delivery fee
    When multiple refund requests is received for <amounts>
    Then delivery fee refund request is <refundStatus>

    Examples: 
      | deliveryAmount | amounts | refundStatus |
      | 9              | 2,3,4   | refunded     |
      | 10             | 4,4,3   | not refunded |
      | 10             | 8,2     | refunded     |

  @wip
  Scenario Outline: Delivery fee refund request after refunding all or no products
    Given an order with <deliveryAmount> as delivery fee
    And all products are <productRefundStatus>
    And delivery fee is not refunded
    When delivery fee refund amount is <refundAmount>
    Then delivery fee refund request is <refundStatus>

    Examples: 
      | deliveryAmount | productRefundStatus | refundAmount | refundStatus |
      | 9              | refunded            | 9            | refunded     |
      | 9              | refunded            | 10           | not refunded |
      | 9              | refunded            | 8            | refunded     |
      | 9              | not refunded        | 8            | refunded     |
