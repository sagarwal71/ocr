@cartProductData @initPreOrderProducts
Feature: As a target online customer
    I need to be shown Afterpay and Zip payment information on Basket Page
    so that I know if I can pay using Afterpay or Zip payment and how much the installment price is.

  Scenario Outline: view Afterpay information on Basket
    Given a cart with entries:
      | product   | qty | price |
      | 10000011  | 1   | 15    |
      | <product> | 2   | 15    |
    When get cart data
    Then cart afterpay installment price is '<installmentPrice>'
    And cart excludeForAfterpay is '<excludeForAfterpay>'

    Examples:
      | product  | installmentPrice | excludeForAfterpay |
      | 10000111 | $11.25           | false              |
      | 10000911 | $11.25           | true               |

  Scenario: View Afterpay information on Basket for pre-order product
    Given a cart with entries:
      | product          | qty | price |
      | V5555_preOrder_5 | 1   | 400   |
    When get cart data
    Then cart excludeForAfterpay is 'true'

  Scenario Outline: view Zip payment information on Basket
    Given a cart with entries:
      | product   | qty | price |
      | 10000011  | 1   | 15    |
      | <product> | 2   | 15    |
    When get cart data
    Then cart excludeForZipPayment is '<excludeForZipPayment>'

    Examples:
      | product  | excludeForZipPayment |
      | 10000111 | false                |
      | 10000911 | true                 |

  Scenario: View Zip payment information on Basket for pre-order product
    Given a cart with entries:
      | product          | qty | price |
      | V5555_preOrder_5 | 1   | 400   |
    When get cart data
    Then cart excludeForZipPayment is 'true'