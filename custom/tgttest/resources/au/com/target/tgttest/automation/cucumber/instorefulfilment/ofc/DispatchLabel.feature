@cleanOrderData @cleanConsignmentData
Feature: Instore Fulfilment - Request Dispatch Label
  
  In order to prepare consignments for AusPost as Store Agent I want to print a dispatch label
  
  Notes: 
  * Currently OFC does not pass layout and branding params, so they should go to AusPost as defaults.
  * Hoppers store is number 7064, at 50 Old Geelong Road, Hoppers Crossing, VIC, 3030

  @notAutomated
  Scenario Outline: Label buttons present according to screen and order type and status
    Given consignment status is '<consignmentStatus>'
    And ofc order type is '<ofcOrderType>'
    When store user is on screen 'Order Details'
    Then CnC label is '<cncActive>' and Dispatch label is '<dlActive>'

    Examples: 
      | consignmentStatus | ofcOrderType  | screen        | cncActive | dlActive |
      | PICKED            | Interstore    | Order Details | N         | N        |
      | PACKED            | Interstore    | Order Details | Y         | Y        |
      | COMPLETED         | Interstore    | Order Details | Y         | Y        |
      | PICKED            | Delivery      | Order Details | N         | N        |
      | PACKED            | Delivery      | Order Details | N         | Y        |
      | COMPLETED         | Delivery      | Order Details | N         | Y        |
      | PICKED            | Samestore CNC | Order Details | N         | N        |
      | COMPLETED         | Samestore CNC | Order Details | Y         | N        |

    Examples: 
      | consignmentStatus | ofcOrderType | screen        | cncActive | dlActive |
      | PICKED            | Interstore   | Order Details | Y         | Y        |

  Scenario Outline: Request dispatch label for home delivery, where AusPost returns varied responses.
    Given webmethods returns '<webmethodsStatus>' for auspost dispatch label
    And a consignment is set up for store 'Camberwell':
      | consignmentCode | deliveryMethod | cncStoreNumber | consignmentStatus | customer   | destination                           | trackingId |
      | autox0000100    | home-delivery  |                | Packed            | John Smith | 19 Holroyd Street, Seaford, VIC, 3198 | 123456     |
    And the set up consignment has parcel list:
      | height | width | length | weight |
      | 1      | 2     | 3      | 4      |
      | 5      | 6     | 7      | 8      |
    When consignment dispatch label is retrieved by ofc
    Then label request is sent to AusPost with:
      | store      | merchantLocationId | storeAddress                          | layout | branding | trackingId | deliveryName | deliveryAddress                       |
      | Camberwell | JDQ                | Station Street, Camberwell, VIC, 3124 | A4-1PP | true     | 123456     | John Smith   | 19 Holroyd Street, Seaford, VIC, 3198 |
    And label request has parcels:
      | height | width | length | weight |
      | 1      | 2     | 3      | 4      |
      | 5      | 6     | 7      | 8      |
    And label pdf is <pdfResponse>

    Examples: 
      | webmethodsStatus | pdfResponse  |
      | SUCCESS          | returned     |
      | UNAVAILABLE      | not returned |
      | ERROR            | not returned |

  Scenario: Request dispatch label but consignment is in picked state should disallow.
    Given webmethods returns 'SUCCESS' for auspost dispatch label
    And a consignment is set up for store 'Camberwell':
      | consignmentCode | deliveryMethod | cncStoreNumber | consignmentStatus | customer   | destination                           | trackingId |
      | autox0000100    | home-delivery  |                | Picked            | John Smith | 19 Holroyd Street, Seaford, VIC, 3198 | 123456     |
    When consignment dispatch label is retrieved by ofc
    Then label pdf is not returned

  Scenario: Request dispatch label for interstore consignment, AusPost returns success.
    Given webmethods returns 'SUCCESS' for auspost dispatch label
    And a consignment is set up for store 'Camberwell':
      | consignmentCode | deliveryMethod    | cncStoreNumber | consignmentStatus | customer | destination                                      | trackingId |
      | autox0000101    | click-and-collect | 7064           | Packed            | Jane Doe | 50 Old Geelong Road, Hoppers Crossing, VIC, 3030 | 123457     |
    And the set up consignment has parcel list:
      | height | width | length | weight |
      | 1      | 2     | 3      | 4      |
      | 5      | 6     | 7      | 8      |
    When consignment dispatch label is retrieved by ofc
    Then label request is sent to AusPost with:
      | store      | merchantLocationId | storeAddress                          | layout | branding | trackingId | deliveryName              | deliveryAddress                                  |
      | Camberwell | JDQ                | Station Street, Camberwell, VIC, 3124 | A4-1PP | true     | 123457     | Target - Hoppers Crossing | 50 Old Geelong Road, Hoppers Crossing, VIC, 3030 |
    And label request has parcels:
      | height | width | length | weight |
      | 1      | 2     | 3      | 4      |
      | 5      | 6     | 7      | 8      |
    And label pdf is returned
