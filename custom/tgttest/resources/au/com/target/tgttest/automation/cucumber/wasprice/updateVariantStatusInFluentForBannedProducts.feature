@esbPriceUpdate @cartProductData @fluentFeatureSwitch
Feature: Banned variant product status to be updated in Fluent
  As Online Operations
  I want to mark the sku as INACTIVE in fluent when POS updates hybris for an item removed from sale

  Background: 
    Given Hybris variantCode 'PGC1005_skypeblue_25' set to approved

  Scenario Outline: If it is not a banned product, the status will not change.
    Given the 'fluent' feature switch is disabled
    And POS updates price for product '<variantCode>':
      | Date | SellPrice | PermPrice | promoEvent |
      | null | 10        | 10        | true       |
    And POS sets the product as '<banned>'
    When Hybris POS price update runs
    Then the status for '<variantCode>' is '<hybrisStatus>'

    Examples: 
      | variantCode          | variantName             | productCode | banned | hybrisStatus |
      | PGC1005_skypeblue_25 | $25 e-Gift Card - Skype | PGC1005     | false  | approved     |

  Scenario Outline: Updating Fluent Sku Status to Inactive when banned product price update occurs
    Given the 'fluent' feature switch is enabled
    And POS updates price for product '<variantCode>':
      | Date | SellPrice | PermPrice | promoEvent |
      | null | 10        | 10        | true       |
    And POS sets the product as '<banned>'
    And fluent 'createSku' api returns fluent id '633056'
    And fluent 'searchSku' api returns fluent id '633056'
    And fluent 'updateSku' api returns fluent id '633056'
    When Hybris POS price update runs
    Then the status for '<variantCode>' is '<hybrisStatus>'
    And fluent sku api gets called with:
      | skuRef        | productRef    | name          | status         |
      | <variantCode> | <productCode> | <variantName> | <fluentStatus> |
    And Hybris variantCode '<variantCode>' set to approved

    Examples: 
      | variantCode          | variantName             | productCode | banned | hybrisStatus | fluentStatus |
      | PGC1005_skypeblue_25 | $25 e-Gift Card - Skype | PGC1005     | banned | check        | INACTIVE     |
