@cartProductData @deliveryModeData
Feature: Refund deal items that are short picked

  Background: 
    Given set deal parameters:
      | name   | rewardType     | rewardValue | rewardMaxQty |
      | buyget | PercentOffEach | 25          | 2            |
    And cart entries with deals:
      | product  | qty | price | dealName | dealRole |
      | 10000011 | 2   | 25    | buyget   | Q        |
      | 10000012 | 2   | 25    | buyget   | R        |

  Scenario: A deal item is short picked
    Given order is placed
    When fastline returns pick entry data:
      | product  | qty |
      | 10000011 | 2   |
      | 10000012 | 1   |
    Then fastline pick process completes
    And the order contains the following entries:
      | product  | qty |
      | 10000011 | 2   |
      | 10000012 | 1   |
    And refund value should be '18.75'
    And a CS ticket is not created for the order

  Scenario: An order containing deal items is zero picked
    Given order is placed
    When fastline returns pick entry data:
      | product  | qty |
      | 10000011 | 0   |
      | 10000012 | 0   |
    Then order cancel process completes
    And the order contains the following entries:
      | product  | qty |
      | 10000011 | 0   |
      | 10000012 | 0   |
    And refund value should be '87.50'
    And a CS ticket is not created for the order
