@wip
Feature: Product Import - imports products from STEP with displayOnly attribute
  
  In order to import the display only  product from step
  As Target Online
  I want to specify in STEP whether a product is displayOnly or not

  Scenario Outline: Export In Store Only attribute from STEP to hybris
    Given a product is being exported from STEP to hybris for the first time
    And the product being exported is at level <level>
    And the value of displayOnly flag <displayOnlySTEP>
    When the product is successfully exported
    Then the 'Display Only' attribute in hybris is <setDisplayOnly>
    And if set, the value set is <hybrisValue>
    And whether a Fastline stock level record is created is <createStockLevel>
    And whether a warehouse is set is <setWarehouse>

    Examples: 
      | level                   | displayOnlySTEP | setDisplayOnly | hybrisValue | createStockLevel | setWarehouse |
      | sellable colour         | Y               | set            | true        | no               | no           |
      | sellable colour         | N               | set            | false       | yes              | yes          |
      | sellable colour         | blank           | set            | false       | yes              | yes          |
      | sellable assortment     | Y               | set            | true        | no               | no           |
      | sellable assortment     | N               | set            | false       | yes              | yes          |
      | sellable assortment     | blank           | set            | false       | yes              | yes          |
      | size                    | Y               | set            | true        | no               | no           |
      | size                    | N               | set            | false       | yes              | yes          |
      | size                    | blank           | set            | false       | yes              | yes          |
      | non-sellable colour     | Y               | not set        | n/a         | n/a              | n/a          |
      | non-sellable colour     | N               | not set        | n/a         | n/a              | n/a          |
      | non-sellable colour     | blank           | not set        | n/a         | n/a              | n/a          |
      | non-sellable assortment | Y               | not set        | n/a         | n/a              | n/a          |
      | non-sellable assortment | N               | not set        | n/a         | n/a              | n/a          |
      | non-sellable assortment | blank           | not set        | n/a         | n/a              | n/a          |

  Scenario Outline: Export In Store Only attribute from STEP to hybris for EXISTING product
    Given a product that is already in hybris is being re-exported from STEP to hybris
    And the product has an existing 'Display Only' attribute value in hybris of <existingValue>
    And the product being exported is at level <level>
    And the product has STEP 'Display Only' attribute value of <displayOnlySTEP>
    When the product is successfully exported
    Then the 'Display Only' attribute in hybris is <setDisplayOnly>
    And if set, the value set is <new_hybrisValue>
    And the action taken on the Fastline stock level record is <stockLevelAction>
    And the action taken on the warehouse is set is <warehouseAction>

    Examples: 
      | level               | existingValue | displayOnlySTEP | setDisplayOnly | new_hybrisValue | stockLevelAction | warehouseAction |
      | sellable colour     | true          | Y               | not changed    | true            | no action        | no action       |
      | sellable colour     | false         | N               | not changed    | false           | no action        | no action       |
      | sellable colour     | false         | blank           | not changed    | false           | no action        | no action       |
      | sellable colour     | false         | Y               | updated        | true            | unchanged        | unchanged       |
      | sellable colour     | true          | N               | updated        | false           | created          | created         |
      | sellable colour     | true          | blank           | updated        | false           | created          | created         |
      | sellable assortment | true          | Y               | not changed    | true            | no action        | no action       |
      | sellable assortment | false         | N               | not changed    | false           | no action        | no action       |
      | sellable assortment | false         | blank           | not changed    | false           | no action        | no action       |
      | sellable assortment | false         | Y               | updated        | true            | unchanged        | unchanged       |
      | sellable assortment | true          | N               | updated        | false           | created          | created         |
      | sellable assortment | true          | blank           | updated        | false           | created          | created         |
      | size                | true          | Y               | not changed    | true            | no action        | no action       |
      | size                | false         | N               | not changed    | false           | no action        | no action       |
      | size                | false         | blank           | not changed    | false           | no action        | no action       |
      | size                | false         | Y               | updated        | true            | unchanged        | unchanged       |

  Scenario Outline: Export In Store Only attribute from STEP to hybris validation
    Given a product is being considered for export from STEP to hybris
    And the product being exported is at level <level>
    And the product has STEP 'Display Only' attribute value of <displayOnlySTEP>
    And whether the product is Online Exclusive is <onlineExclusive>
    And whether the product type is Digital (eg. e-Gift Cards) is <digital>
    And whether the product is a Drop Ship product (eg. CNP) is <dropShip>
    And whether the product support sales via Channel Adviser (ie. eBay, TradeMe) is <eBay>
    And whether the product fails any existing validations (used for products sold online) is <failExisting>
    When the export of the product is attempted
    Then the export validations for the product are <validations>

    Examples: 
      | displayOnlySTEP | onlineExclusive | digital | dropShip | eBay | failExisting | validations  |
      | Y               | no              | no      | no       | no   | no           | successful   |
      | Y               | yes             | no      | no       | no   | no           | unsuccessful |
      | Y               | no              | yes     | no       | no   | no           | unsuccessful |
      | Y               | no              | no      | yes      | no   | no           | unsuccessful |
      | Y               | no              | no      | no       | yes  | no           | unsuccessful |
      | Y               | no              | no      | no       | no   | yes          | unsuccessful |
      | N               | no              | no      | no       | no   | no           | successful   |
      | N               | yes             | no      | no       | no   | no           | successful   |
      | N               | no              | yes     | no       | no   | no           | successful   |
      | N               | no              | no      | yes      | no   | no           | successful   |
      | N               | no              | no      | no       | yes  | no           | successful   |
      | N               | no              | no      | no       | no   | yes          | unsuccessful |
