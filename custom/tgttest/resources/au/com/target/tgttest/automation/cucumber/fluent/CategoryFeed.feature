@fluentFeatureSwitch @initFluentCategoriesData
Feature: Category feed to fluent
  As Target Online I want to upsert category in fluent
  when category is imported from step into hybris

  Background: 
    Given category 'AP01' has fluentId '11'
    And category 'FluentC2' has fluentId '2'

  Scenario: top level category import and create Catgory in fluent
    Given a category:
      | categoryCode | categoryName  | principalUid  | supercategoryCode |
      | FluentC1     | TestCategory1 | customergroup | AP01              |
    And fluent 'createCategory' api returns fluent id '1'
    When run STEP category import process
    Then category import response is successful
    And fluent category api gets called with data:
      | categoryRef | name          | parentCategoryId |
      | FluentC1    | TestCategory1 |               11 |
    And category has:
      | fluentId |
      |        1 |
    And fluent 'category' update status is:
      | code     | fluentId | lastRunSuccessful |
      | FluentC1 |        1 | true              |

  Scenario: category Import and create Catgory in fluent
    Given a category:
      | categoryCode | categoryName  | principalUid  | supercategoryCode |
      | FluentC3     | TestCategory3 | customergroup | FluentC2          |
    And fluent 'createCategory' api returns fluent id '3'
    When run STEP category import process
    Then category import response is successful
    And fluent category api gets called with data:
      | categoryRef | name          | parentCategoryId |
      | FluentC3    | TestCategory3 |                2 |
    And category has:
      | fluentId |
      |        3 |
    And fluent 'category' update status is:
      | code     | fluentId | lastRunSuccessful |
      | FluentC3 |        3 | true              |

  Scenario: category Import and create Catgory in fluent when category is already in fluent
    Given a category:
      | categoryCode | categoryName  | principalUid  | supercategoryCode |
      | FluentC3     | TestCategory3 | customergroup | FluentC2          |
    And fluent 'createCategory' api returns error code '409'
    And fluent 'searchCategory' api returns fluent id '3'
    And fluent 'updateCategory' api returns fluent id '3'
    When run STEP category import process
    Then category import response is successful
    And fluent category api gets called with data:
      | categoryRef | name          | parentCategoryId |
      | FluentC3    | TestCategory3 |                2 |
    And category has:
      | fluentId |
      |        3 |
    And fluent 'category' update status is:
      | code     | fluentId | lastRunSuccessful |
      | FluentC3 |        3 | true              |

  Scenario: category Import and update category in fluent
    Given a category:
      | categoryCode | categoryName  | principalUid  | supercategoryCode |
      | FluentC3     | TestCategory3 | customergroup | FluentC2          |
    And category 'FluentC3' has fluentId '3'
    And fluent 'updateCategory' api returns fluent id '3'
    When run STEP category import process
    Then category import response is successful
    And fluent category api gets called with data:
      | categoryRef | name          | parentCategoryId |
      | FluentC3    | TestCategory3 |                2 |
    And category has:
      | fluentId |
      |        3 |
    And fluent 'category' update status is:
      | code     | fluentId | lastRunSuccessful |
      | FluentC3 |        3 | true              |

  Scenario: category Import and update category in fluent when category is not in fluent
    Given a category:
      | categoryCode | categoryName  | principalUid  | supercategoryCode |
      | FluentC3     | TestCategory3 | customergroup | FluentC2          |
    And fluent 'updateCategory' api returns error code '404'
    And fluent 'createCategory' api returns fluent id '3'
    When run STEP category import process
    Then category import response is successful
    And fluent category api gets called with data:
      | categoryRef | name          | parentCategoryId |
      | FluentC3    | TestCategory3 |                2 |
    And category has:
      | fluentId |
      |        3 |
    And fluent 'category' update status is:
      | code     | fluentId | lastRunSuccessful |
      | FluentC3 |        3 | true              |

  Scenario: category Import and create category in fluent - fluent service 503 error
    Given category 'FluentC3' has fluentId '3'
    And a category:
      | categoryCode | categoryName  | principalUid  | supercategoryCode |
      | FluentC4     | TestCategory4 | customergroup | FluentC3          |
    And fluent 'createCategory' api returns error code '503'
    When run STEP category import process
    And category has:
      | fluentId |
      | null     |
    And fluent 'category' update status is:
      | code     | fluentId | lastRunSuccessful | errorCode |
      | FluentC4 | null     | false             |       503 |

  Scenario: category Import and update category in fluent - fluent service 503 error
    Given a category:
      | categoryCode | categoryName  | principalUid  | supercategoryCode |
      | FluentC1     | TestCategory1 | customergroup | AP01              |
    And category 'FluentC1' has fluentId '1'
    And fluent 'updateCategory' api returns error code '503'
    When run STEP category import process
    And category has:
      | fluentId |
      |        1 |
    And fluent 'category' update status is:
      | code     | fluentId | lastRunSuccessful | errorCode |
      | FluentC1 |        1 | false             |       503 |
