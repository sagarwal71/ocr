@rollback @cartProductData
Feature: Express delivery
  
  In order to offer customers faster delivery options, as Target online store
  we offer express delivery based on a set of post codes and valid products.
  Post code 3000 is a valid post code and 1405 is an invalid post code for express delivery.

  Background: 
    Given product delivery modes:
      | product  | deliveryMode                                     |
      | 10000011 | home-delivery,click-and-collect,express-delivery |

  Scenario: For a valid postcode express delivery is avaliable.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
    When post code is '3000'
    Then express delivery available 'true'

  Scenario: For an invalid postcode express delivery is not avaliable.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
    When post code is '1405'
    Then express delivery available 'false'
