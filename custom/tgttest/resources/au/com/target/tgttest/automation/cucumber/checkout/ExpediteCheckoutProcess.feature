@notAutomated
Feature: Pre-populate the user preferences during checkout to expedite checkout process
  In order to checkout faster  
  As a online customer I want user preferences should be pre-populated in order review page

  Scenario Outline: Customer with/with out saved credit credit card details and delivery address
    Given a registered User with <creditCard> and <deliveryAddress>
    And having a cart
    And customer is on <salesApplication>
    And had <previousOrderStatus> a previous order
    When customer navigates to checkout
    Then the customer will <redirectStatus> to the order review page
    And the order review page has <creditCardInfo> and <deliveryAddressInfo>

    Examples: 
      | creditCard         | deliveryAddress         | previousOrderStatus | salesApplication | redirectStatus | creditCardInfo | deliveryAddressInfo |
      | SavedCreditcard    | SavedDeliveryAddress    | Placed              | KIOSK            | Not Redirect   | N/A            | N/A                 |
      | NotSavedCreditcard | NotSavedDeliveryAddress | Not Placed          | KIOSK            | Not Redirect   | N/A            | N/A                 |
      | NotSavedCreditcard | NotSavedDeliveryAddress | Placed              | WEB              | Not Redirect   | N/A            | N/A                 |
      | SavedCreditcard    | SavedDeliveryAddress    | Not Placed          | WEB              | Redirect       | Saveddetails   | Saveddetails        |
      | SavedCreditcard    | NotSavedDeliveryAddress | Placed              | WEB              | Redirect       | Saveddetails   | PreviouOrderdetails |
      | SavedCreditcard    | NotSavedDeliveryAddress | Not Placed          | WEB              | Not Redirect   | N/A            | N/A                 |
      | NotSavedCreditcard | NotSavedDeliveryAddress | Placed              | WEB              | Not Redirect   | N/A            | N/A                 |
      | NotSavedCreditcard | SavedDeliveryAddress    | Placed              | WEB              | Not Redirect   | N/A            | N/A                 |
      | NotSavedCreditcard | SavedDeliveryAddress    | Not Placed          | WEB              | Not Redirect   | N/A            | N/A                 |
      | SavedCreditcard    | SavedDeliveryAddress    | Placed              | WEB              | Redirect       | Saveddetails   | Saveddetails        |

  Scenario Outline: Customer with/with out saved credit credit card details, delivery address and has a previous order with payment type as Credit Card/Pay Pal
    Given a registered User with <creditCard> and <deliveryAddress>
    And having a cart
    And customer is on <salesApplication>
    And had placed a previous order with <paymentType>
    When customer navigates to checkout
    Then the customer will <redirectStatus> to the order review page
    And the order review page has <paymentModeInfo> and <deliveryAddressInfo>

    Examples: 
      | creditCard         | deliveryAddress         | paymentType | salesApplication | redirectStatus | paymentModeInfo | deliveryAddressInfo |
      | SavedCreditcard    | SavedDeliveryAddress    | PayPal      | KIOSK            | Not Redirect   | N/A             | N/A                 |
      | SavedCreditcard    | SavedDeliveryAddress    | CreditCard  | KIOSK            | Not Redirect   | N/A             | N/A                 |
      | NotSavedCreditcard | NotSavedDeliveryAddress | CreditCard  | WEB              | Not Redirect   | N/A             | N/A                 |
      | SavedCreditcard    | NotSavedDeliveryAddress | CreditCard  | WEB              | Redirect       | CreditCard      | PreviouOrderdetails |
      | SavedCreditcard    | SavedDeliveryAddress    | CreditCard  | WEB              | Redirect       | CreditCard      | Saveddetails        |
      | SavedCreditcard    | SavedDeliveryAddress    | PayPal      | WEB              | Redirect       | CreditCard      | Saveddetails        |
      | SavedCreditcard    | NotSavedDeliveryAddress | PayPal      | WEB              | Redirect       | CreditCard      | PreviouOrderdetails |

  Scenario Outline: Customer with/with out saved credit credit card details, delivery address and cart contains products with/without conflicting delivery modes
    Given a registered User with <creditCard> and <deliveryAddress>
    And having a cart with delivery mode as <preferredDeliveryMode>
    And Delivery modes enabled for a cart are <deliveryModesAllowed>
    And customer is on <salesApplication>
    When customer navigates to checkout
    Then the customer will <redirectStatus> to the order review page
    And the order review page has <creditCardInfo>, <deliveryAddressInfo> and <deliveryMode>

    Examples: 
      | creditCard         | deliveryAddress         | preferredDeliveryMode | deliveryModesAllowed                             | salesApplication | redirectStatus | creditCardInfo | deliveryAddressInfo  | deliveryMode      |
      | SavedCreditcard    | SavedDeliveryAddress    | home-delivery         | express-delivery,home-delivery,click-and-collect | KIOSK            | Not Redirect   | N/A            | N/A                  | N/A               |
      | SavedCreditcard    | SavedDeliveryAddress    | click-and-collect     | express-delivery,home-delivery,click-and-collect | KIOSK            | Not Redirect   | N/A            | N/A                  | N/A               |
      | SavedCreditcard    | NotSavedDeliveryAddress | express-delivery      | express-delivery,home-delivery,click-and-collect | KIOSK            | Not Redirect   | N/A            | N/A                  | N/A               |
      | SavedCreditcard    | SavedDeliveryAddress    | home-delivery         | express-delivery,home-delivery,click-and-collect | WEB              | Redirect       | CreditCard     | SavedDeliveryAddress | home-delivery     |
      | SavedCreditcard    | SavedDeliveryAddress    | click-and-collect     | express-delivery,home-delivery,click-and-collect | WEB              | Redirect       | CreditCard     | SavedDeliveryAddress | click-and-collect |
      | SavedCreditcard    | SavedDeliveryAddress    | click-and-collect     | express-delivery,home-delivery                   | WEB              | Not Redirect   | N/A            | N/A                  | N/A               |
      | SavedCreditcard    | SavedDeliveryAddress    | home-delivery         | express-delivery,click-and-collect               | WEB              | Not Redirect   | N/A            | N/A                  | N/A               |
      | SavedCreditcard    | SavedDeliveryAddress    | express-delivery      | express-delivery,home-delivery,click-and-collect | WEB              | Not Redirect   | N/A            | N/A                  | N/A               |
      | NotSavedCreditcard | NotSavedDeliveryAddress | express-delivery      | home-delivery,click-and-collect                  | WEB              | Not Redirect   | N/A            | N/A                  | N/A               |
      | SavedCreditcard    | SavedDeliveryAddress    | home-delivery         | home-delivery                                    | WEB              | Redirect       | CreditCard     | SavedDeliveryAddress | home-delivery     |
      | SavedCreditcard    | SavedDeliveryAddress    | click-and-collect     | click-and-collect                                | WEB              | Redirect       | CreditCard     | SavedDeliveryAddress | click-and-collect |
      | NotSavedCreditcard | NotSavedDeliveryAddress | home-delivery         | express-delivery,home-delivery,click-and-collect | WEB              | Not Redirect   | N/A            | N/A                  | N/A               |
