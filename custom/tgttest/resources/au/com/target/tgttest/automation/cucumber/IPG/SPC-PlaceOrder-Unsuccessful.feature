@cartProductData @deliveryModeData
Feature: SPC - Handle problems placing order with IPG payment, where the order can still be continued with

  Background: 
    Given set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |
    And a registered customer goes into the checkout process
    And user checkout via spc
    And delivery mode is 'home-delivery'
    And giftcard/creditcard details:
      | paymentId | cardType   | cardNumber        | cardExpiry | token     | tokenExpiry | bin |
      | GC1       | Giftcard   | 62734500000000002 | 01/20      | 123456789 |             | 31  |
      | GC2       | Giftcard   | 62779575000000000 | 01/20      | 123456789 |             | 32  |
      | GC3       | Giftcard   | 62779575000000001 | 01/20      | 123456789 |             | 32  |
      | GC4       | Giftcard   | 62733500000000003 | 01/20      | 123456789 |             | 33  |
      | CC1       | VISA       | 4987654321010012  | 01/20      | 123456789 | 01/01/2020  | 35  |
      | CC2       | MASTERCARD | 5587654321010013  | 02/20      | 123456789 | 01/02/2020  | 36  |
      | CC3       | AMEX       | 349876543210010   | 03/20      | 123456789 | 01/03/2020  | 37  |
      | CC4       | DINERS     | 36765432100028    | 04/20      | 123456789 | 01/04/2020  | 38  |
    And flybuys config max redeemable amount is 50
    And customer has valid flybuys number presented
    And the user entered flybuys details are 'valid'
    And Customer has available points '11000'

  Scenario: Credit Card declined - single CC payment
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And guest checkout
    And payment method is 'ipg'
    And ipg submit payment response is '1'
    When order is placed
    Then place order result is 'PAYMENT_FAILURE'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |

  Scenario Outline: Credit Card declined - split payment
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    And credit card 'CC1' is declined
    When order is placed
    Then place order result is 'PAYMENT_FAILURE'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |
    And all successful payment should be reversed

    Examples: 
      | payment1 | payment2 | payment3 | payment4 |
      | CC1,39   |          |          |          |
      | GC1,10   | CC1,29   |          |          |

  Scenario Outline: Some stock not available when pay with Gift Card
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    And payment method is 'giftcard'
    And set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 1         |
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    When order is placed
    Then place order result is 'INSUFFICIENT_STOCK'
    And the items in the cart are:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 1         |
    And all successful payment should be reversed

    Examples: 
      | payment1 | payment2 | payment3 | payment4 |
      | GC1,39   |          |          |          |
      | GC1,10   | CC1,29   |          |          |
      | GC1,10   | GC2,10   | CC1,19   |          |
      | GC1,10   | GC2,10   | GC3,10   | CC1,9    |

  Scenario: Place order with e-Gift Card and use split payment
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName | email                         | messageText   |
      | PGC1000_iTunes_10 | Shreeram  | Mishra   | Shreeram.Mishra@target.com.au | Test Checkout |
    And registered user checkout
    And payment method is 'giftcard'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | GC1,49       |
    When order is placed
    Then place order result is 'GIFTCARD_PAYMENT_NOT_ALLOWED'
    And all successful payment should be reversed

  Scenario: Order value does not match paid value
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
    And registered user checkout
    And payment method is 'giftcard'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | GC1, 24      |
    And cart is updated with entries:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
    When order is placed
    Then place order result is 'PAYMENT_FAILURE_DUETO_MISSMATCH_TOTALS'
    And all successful payment should be reversed

  Scenario: IPG not available when attempt to place order
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
    And registered user checkout
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | CC1, 24      |
    And ipg is down
    When order is placed
    Then place order result is 'SERVICE_UNAVAILABLE'

  Scenario: Order not complete / fully formed - Incomplete delivery info
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
    And registered user checkout
    And delivery mode is 'express-delivery'
    And the delivery address is '1 Aberdeen Street, Newtown, VIC, 1405'
    And payment method is 'giftcard'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | GC1, 24      |
    When order is placed
    Then place order result is 'INCOMPLETE_DELIVERY_INFO'
    And delivery info on the cart is cleaned
    And all successful payment should be reversed

  Scenario: Order not complete / fully formed - Incomplete delivery info
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
    And registered user checkout
    And the delivery address is 'na'
    And payment method is 'giftcard'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | GC1, 24      |
    When order is placed
    Then place order result is 'INCOMPLETE_DELIVERY_INFO'
    And all successful payment should be reversed

  Scenario: Order not complete / fully formed - No billing address
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
    And delivery mode is 'click-and-collect'
    And the billing address is 'na'
    And payment method is 'giftcard'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | GC1, 24      |
    When order is placed
    Then place order result is 'INCOMPLETE_BILLING_ADDRESS'
    And all successful payment should be reversed

  Scenario: Place order with e-Gift Card with no billing address
    Given a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName | email                         | messageText               |
      | PGC1000_iTunes_10 | Shreeram  | Mishra   | Shreeram.Mishra@target.com.au | Testing the checkout page |
    And registered user checkout
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | CC1,10       |
    When order is placed
    Then place order result is 'INCOMPLETE_BILLING_ADDRESS'
    And all successful payment should be reversed

  Scenario: All stock not available when pay with Gift Card
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    And payment method is 'giftcard'
    And set fastline stock:
      | product  | reserved | available |
      | 10000011 | 2        | 2         |
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | GC1,39       |
    When order is placed
    Then place order result is 'INVALID_CART'
    And all successful payment should be reversed

  Scenario: Product becomes unapproved
    Given a cart with entries:
      | product  | qty |
      | 10000011 | 2   |
    And registered user checkout
    And payment method is 'giftcard'
    And product '10000011' is 'unapproved'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | GC1,39       |
    When order is placed
    Then place order result is 'INVALID_PRODUCT_IN_CART'
    And all successful payment should be reversed

  Scenario Outline: Some stock not available when pay with Gift Card with flybuys redemption remove
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 20    |
    And registered user checkout
    And the customer logins flybuys in spc
    And the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE3'
    And payment method is 'giftcard'
    And set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 1         |
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    When order is placed
    Then place order result is 'INSUFFICIENT_STOCK_FLYBUYS'
    And the items in the cart are:
      | product  | qty | price |
      | 10000011 | 1   | 20    |
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 1         |
    And all successful payment should be reversed
    And cart does not contain flybuys discount

    Examples: 
      | payment1 | payment2 | payment3 | payment4 |
      | GC1,19   |          |          |          |
      | GC1,10   | CC1,9    |          |          |
      | GC1,5    | GC2,10   | CC1,4    |          |
      | GC1,5    | GC2,5    | GC3,5    | CC1,4    |

  Scenario: All stock not available when pay with Gift Card with flybuys redemption remove
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    And the customer logins flybuys in spc
    And the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE2'
    And payment method is 'giftcard'
    And set fastline stock:
      | product  | reserved | available |
      | 10000011 | 2        | 2         |
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | GC1,39       |
    When order is placed
    Then place order result is 'INVALID_CART'
    And all successful payment should be reversed
    And cart does not contain flybuys discount

  Scenario: Product becomes unapproved with flybuys redemption remove
    Given a cart with entries:
      | product  | qty |
      | 10000011 | 2   |
    And registered user checkout
    And the customer logins flybuys in spc
    And the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE2'
    And payment method is 'giftcard'
    And product '10000011' is 'unapproved'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | GC1,19       |
    When order is placed
    Then place order result is 'INVALID_PRODUCT_IN_CART'
    And all successful payment should be reversed
    And cart does not contain flybuys discount

  Scenario: Flybuys redemption becomes invalid because of the SOH
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 20    |
    And registered user checkout
    And the customer logins flybuys in spc
    And the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE3'
    And payment method is 'ipg'
    And set fastline stock:
      | product  | reserved | available |
      | 10000011 | 1        | 2         |
    When order is placed
    Then place order result is 'INVALID_FLYBUYS_REDEMPTION'
    And cart does not contain flybuys discount

  Scenario: All stock not available when pay with paypal with flybuys redemption remove
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    And the customer logins flybuys in spc
    And the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE2'
    And payment method is 'paypal'
    And set fastline stock:
      | product  | reserved | available |
      | 10000011 | 2        | 2         |
    When order is placed
    Then place order result is 'INVALID_CART'
    And all successful payment should be reversed
    And cart does not contain flybuys discount

  Scenario: Product becomes unapproved when pay with paypal with flybuys redemption remove
    Given a cart with entries:
      | product  | qty |
      | 10000011 | 2   |
    And registered user checkout
    And the customer logins flybuys in spc
    And the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE2'
    And payment method is 'paypal'
    And product '10000011' is 'unapproved'
    When order is placed
    Then place order result is 'INVALID_PRODUCT_IN_CART'
    And all successful payment should be reversed
    And cart does not contain flybuys discount

  Scenario: Flybuys redemption becomes invalid because of the SOH when pay with paypal
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 20    |
    And registered user checkout
    And the customer logins flybuys in spc
    And the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE3'
    And payment method is 'paypal'
    And set fastline stock:
      | product  | reserved | available |
      | 10000011 | 1        | 2         |
    When order is placed
    Then place order result is 'INVALID_FLYBUYS_REDEMPTION'
    And cart does not contain flybuys discount
