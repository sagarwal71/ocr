@notAutomated
Feature: Outfits - WCMS Look
  In order to provide better shopping experience for customers As a CMS user I want to create and maintain a Look as a set of Colour Variant Products

  Scenario Outline: Adding Colour Variant Product to Look
    Given a look with name 'newlook' in the collection with name 'SpringCollection'
    And the current ColourVariant Products in the Look are <currentProducts>
    When the new colorVariant <addedProduct>  is added in the position <productPosition>
    Then the products in the look are <updatedProducts>

    Examples: 
      | currentProduct          | addedProduct | productPosition | updatedProducts                 |
      | N/A                     | 1000001      | 1               | 1000001                         |
      | 1000001                 | 1000002      | 2               | 1000001,1000002                 |
      | 1000001,1000002         | 1000003      | 1               | 1000003,1000001,1000002         |
      | 1000003,1000001,1000002 | 1000004      | 3               | 1000003,1000001,1000004,1000002 |

  Scenario Outline: Change order of Products in Look
    Given a look with name 'newlook' in the collection with name 'SpringCollection'
    And the current ColourVariant Products in the Look are <currentProducts>
    When the colorVariant <addedProduct>   position is updated to <productPosition>
    Then the products in the look are <productsinLook>

    Examples: 
      | currentProducts         | updatedProduct | newPosition | productsinLook          |
      | 1000001,1000002         | 1000001        | 2           | 1000002,1000001         |
      | 1000003,1000001,1000002 | 1000003        | 3           | 1000001,1000002,1000003 |

  Scenario Outline: Remove Product from Look
    Given a look with name 'newlook' in the collection with name 'SpringCollection'
    And the current Products in the Look are <currentProducts>
    When Product <removedProduct> is removed from the Look
    Then the Products in the Look are now <updatedProducts>

    Examples: 
      | currentProducts         | removedProducts | productsinLook  |
      | 1000003,1000001,1000002 | 1000002         | 1000003,1000001 |
      | 1000003,1000001         | 1000001         | 1000003         |

  Scenario: Adding a Product already in a Look to a different Look
    Given a look with name 'newlook' in the collection with name 'SpringCollection'
    And the colorvariant product  '1000001' is added to look
    When the product '1000001' is added again to new look with name 'Springlook'
    Then the new look should have product '1000001'

  Scenario: Adding a product at the Size Variant level
    Given a look with name 'newlook' in the collection with name 'SpringCollection'
    When sizevariant product  '1000011' is added to look
    Then the product is not added to the look

  Scenario: Adding a base product to the look
    Given a look with name 'newlook' in the collection with name 'SpringCollection'
    When base product  'P1000011' is added to look
    Then the product is not added to the look
