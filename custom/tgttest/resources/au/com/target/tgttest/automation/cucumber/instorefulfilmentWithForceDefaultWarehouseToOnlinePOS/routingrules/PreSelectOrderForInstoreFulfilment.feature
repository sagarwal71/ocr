@cartProductData @storeFulfilmentCapabilitiesData @forceDefaultWarehouseToOnlinePOS
Feature: Instore Fulfilment - Global Routing Rules for in-store fulfilment
  
  As online store, I want to assess every online order against a set of global rules so that I can determine whether an order should be considered 
  for routing to stores, or routed to Fastline immediately without further assessment
  
  
  There are four rules to determine  whether an order should be considered for routing to stores.
  a) The System wide InStore Fulfilment is Enabled/disabled
  b) System level blackout period is Enabled/disabled
  c) The order type should be allowed
  d) Value for the maximum number of items per order
    
  If an order does not satisfy all criteria, it will be assigned to Fastline.
  
  By default global rules allow all orders
  Robina is set to allow all orders in store specific fulfilment config.

  Background: 
    Given stores with fulfilment capability and stock:
      | store  | inStock | state | instoreEnabled | allowDeliveryToSameStore | allowDeliveryToAnotherStore | deliveryModesAllowed                                                 |
      | Robina | Yes     | QLD   | Yes            | Yes                      | Yes                         | click-and-collect,home-delivery,ebay-click-and-collect,eBay-delivery |

  Scenario Outline: Preselecting an order for in-store fulfilment
    Given a cart with entries with weight:
      | product  | qty | weight |
      | 10000011 | 4   | 1      |
      | 10000012 | 5   | 1      |
    And global order routing is <orderRoutingSetting>
    And global blackout period is <globalBlackoutPeriod>
    And delivery modes allowed for instore fulfilment are '<deliveryModesAllowed>'
    And global max items per instore fulfillment order is <maxItemsPerOrder>
    And delivery mode is '<deliverymode>'
    And the delivery address is '19,Robina Town Centre Drive,Robina,QLD,4226'
    And order cnc store is 'Robina'
    When order is placed
    Then a single consignment is created
    And the assigned warehouse will be '<warehouse>'

    Examples: 
      | orderRoutingSetting | globalBlackoutPeriod | deliverymode           | deliveryModesAllowed                                   | maxItemsPerOrder | warehouse | comment                                 |
      | Enabled             | Not in Effect        | click-and-collect      | click-and-collect,home-delivery,ebay-click-and-collect | 10               | Robina    | Store can fulfil cnc                    |
      | Enabled             | In Effect            | click-and-collect      | click-and-collect,home-delivery                        | 10               | Fastline  | Blackout in effect                      |
      | Enabled             | Not in Effect        | click-and-collect      | click-and-collect,home-delivery,ebay-click-and-collect | 8                | Fastline  | More items in order                     |
      | Enabled             | Not in Effect        | click-and-collect      | home-delivery,ebay-click-and-collect                   | 10               | Fastline  | Delivery mode not supported globally    |
      | Disabled            | Not in Effect        | click-and-collect      | click-and-collect,home-delivery,eBay-delivery          | 10               | Fastline  | Order routing disabled                  |
      | Enabled             | Not in Effect        | home-delivery          | click-and-collect,home-delivery                        | 10               | Robina    | Store can fulfil home-delivery          |
      | Enabled             | Not in Effect        | home-delivery          | click-and-collect,express-delivery                     | 10               | Fastline  | Delivery mode not supported globally    |
      | Enabled             | Not in Effect        | ebay-click-and-collect | ebay-click-and-collect,click-and-collect               | 10               | Robina    | Store can fulfil ebay cnc               |
      | Enabled             | Not in Effect        | eBay-delivery          | ebay-click-and-collect,click-and-collect,home-delivery | 10               | Fastline  | Delivery mode not supported globally    |
      | Enabled             | Not in Effect        | ebay-click-and-collect | ebay-click-and-collect,click-and-collect,eBay-delivery | 8                | Fastline  | More items in order                     |
      | Enabled             | Not in Effect        | eBay-delivery          | eBay-delivery                                          | 10               | Robina    | Store can fulfil eBay-delivery          |
      | Enabled             | Not in Effect        | eBay-delivery          | click-and-collect,home-delivery                        | 10               | Fastline  | Delivery mode not supported globally    |
      | Enabled             | Not in Effect        | eBay-delivery          | ebay-click-and-collect,eBay-delivery                   | 8                | Fastline  | More items in order                     |

  Scenario Outline: Fulfilment of consignment depending on system wide blackout config.
    Given System-wide blackout start time is '<systemBlackoutStart>' and end time is '<systemBlackoutEnd>'
    When a consignment sent to a store '7126'
    Then the assigned warehouse will be '<warehouse>'

    Examples: 
      | systemBlackoutStart | systemBlackoutEnd | warehouse | comment                        |
      | NOT POPULATED       | AFTER NOW         | Fastline  | System blackout current        |
      | EXACTLY NOW         | AFTER NOW         | Fastline  | System blackout current        |
      | AFTER NOW           | AFTER NOW         | Robina    | system blackout in future      |
      | AFTER NOW           | NOT POPULATED     | Robina    | System blackout in future      |
      | NOT POPULATED       | NOT POPULATED     | Robina    | System blackout not configured |
      | BEFORE NOW          | BEFORE NOW        | Robina    | System blackout in past        |
      | BEFORE NOW          | NOT POPULATED     | Fastline  | System blackout current        |
      | NOT POPULATED       | BEFORE NOW        | Robina    | System Blackout in past        |
