@cartProductData
Feature: As a target online customer
    I need to be shown Afterpay payment information on Product Page
    so that I know if a product can be purchased using Afterpay and how much the installment price is.

    Scenario Outline: view Afterpay information on PDP
        When product details are requested for '<product>'
        Then afterpay installment price is '<installmentPrice>'
        And excludeForAfterpay is '<excludeForAfterpay>'

        Examples:
        |product       |installmentPrice |excludeForAfterpay |
        |P1060_green   |$2.50            |false              |
        |P1060_green_L |$2.50            |false              |
        |10000910      |$3.75            |true               |
        |10000911      |$3.75            |true               |
        |PGC1010       |null             |false              |
