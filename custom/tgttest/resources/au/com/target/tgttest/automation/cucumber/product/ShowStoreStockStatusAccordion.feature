@notAutomated
Feature: Display Find In Store accordion item on the Product Details Page for products available in stores

  Scenario Outline: Display Find In Store accordion item on the PDP
    Given the 'inStoreStockVisibility' feature switch is <featureSwitch>
    And the show store stock flag for the product is <showStoreStockFlag>
    When the customer navigate to the product description page
    Then the Find In Store accordion item is <displayFindInStore>

    Examples: 
      | featureSwitch | productCode        | showStoreStockFlag | description       | displayFindInStore |
      | enabled       | PGC1003_jetboat_50 | false              | Digital product   | not displayed      |
      | disabled      | PGC1003_jetboat_50 | false              | Digital product   | not displayed      |
      | enabled       | P1010_blue         | false              | Online Exclusive  | not displayed      |
      | disabled      | P1010_blue         | false              | Online Exclusive  | not displayed      |
      | enabled       | P1026_blue_L       | true               | Normal product    | displayed          |
      | disabled      | P1026_blue_L       | false              | Normal product    | not displayed      |
      | enabled       | 10000324           | false              | Drop ship product | not displayed      |
      | disabled      | 10000324           | false              | Drop ship product | not displayed      |
      | enabled       | 10000324           | false              | Preview product   | not displayed      |

  Scenario Outline: Find In Store accordion is open when product is out of stock
    Given the 'inStoreStockVisibility' feature switch is enabled
    And the show store stock flag for the product is true
    And the stock level for the product is <stockLevel>
    When the customer navigate to the product description page
    Then the Find In Store accordion item is displayed
    And the Find In Store accordion is <openStatus>

    Examples:
      | stockLevel | openStatus |
      |          1 | closed     |
      |          2 | closed     |
      |          0 | open       |

  Scenario Outline: Find In Store accordion opens when selecting an out of stock variant
    Given the 'inStoreStockVisibility' feature switch is enabled
    And the show store stock flag for the product is true
    And the product has size variants <sizeVariant>
    And the size variants have stock levels <stockLevel>
    And the customer navigate to the product description page
    When the customer selects a size variant
    Then the Find In Store accordion is <openStatus>

    Examples:
      | sizeVariant | stockLevel | openStatus |
      |          10 |         10 | unchanged  |
      |          12 |          5 | unchanged  |
      |          14 |          1 | unchanged  |
      |          16 |          0 | opened     |
