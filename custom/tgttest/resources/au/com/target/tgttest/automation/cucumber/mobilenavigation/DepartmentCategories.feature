@notAutomated
Feature: Sub category navigation on department landing page
  In order to get better conversion As a Marketing team I want sub category navigation on department landing page  in mobile devices

  Scenario Outline: Configure subcategory navigation
    Given Women department has <subcategory>
    And Women department is configure to have a landing page
    When the department navigation component is configured
    Then component should display <subcategory>

    Examples: 
      | subcategory |
      | tops        |
      | bottom      |
      | activewear  |
      | dress       |
