@cartProductData
Feature: Restrict express delivery for products above the maximum cubic volume.
  
  
  In order to restrict express delivery 
  As the online store
  I want to prevent express delivery for products whose volume is configured above the max.
  
  Notes:
  * Products are setup in impex as:
      | product  | length(cms)  | width(cms) | height(cms) | volume(cm3) |
      | W1000001 | 25           | 30         | 49          | 36750       |
      | W1000011 | 40           | 50         | 35          | 70000       |
      | W1000021 | 45           | 10         | 25          | 11250       |
      | W1000031 | 55           | 45         | 20          | 49500       |
      | W1000041 | 30           | 25         | 15          | 11250       |
      | W1000051 | 40           | 60         | 30          | 72000       |
      | W1000061 | 10           | 50         | 40          | 20000       |
      | W1000071 | 40           | 45         | 100         | 180000      |
      | W1000081 |  N/A         | N/A        | N/A         | N/A         |
      | W1000091 | 30           | 0          | 45          | 0           |
      | W1000101 | 30           | 40         | -5          | N/A         |
   
   * All the Delivery modes are Enabled through configuration

  Background: 
    Given Maximum volume restriction for the express delivery is set to 70000 cubic cms.

  Scenario Outline: Available delivery modes on product details page and quick view Page based on cubic volume of the product
    Given a product with '<productCode>'
    When product details are retrieved
    Then delivery modes available for the product are '<availableDeliveryModesForProduct>'

    Examples: 
      | productCode | availableDeliveryModesForProduct                 |
      | W1000001    | express-delivery,home-delivery,click-and-collect |
      | W1000011    | express-delivery,home-delivery,click-and-collect |
      | W1000021    | express-delivery,home-delivery,click-and-collect |
      | W1000031    | express-delivery,home-delivery,click-and-collect |
      | W1000041    | express-delivery,home-delivery,click-and-collect |
      | W1000051    | home-delivery, click-and-collect                 |
      | W1000061    | express-delivery,home-delivery,click-and-collect |
      | W1000071    | home-delivery, click-and-collect                 |
      | W1000081    | home-delivery,click-and-collect                  |
      | W1000091    | express-delivery,home-delivery,click-and-collect |
      | W1000101    | home-delivery,click-and-collect                  |

  Scenario Outline: delivery promotion text on product details page and quick view Page based on cubic volume of the product
    Given a product with '<productCode>'
    And delivery promotion ranks order is '<deliveryPromotionRank>'
    And express delivery promo display settings are '<expressDeliveryPromo>'
    And home delivery promo display settings are '<homeDeliveryPromo>'
    And cnc delivery promo display settings are '<CnCPromo>'
    When product details are retrieved
    Then delivery promotion sticker is '<deliveryPromotionSticker>'
    And delivery promotion text is '<deliveryPromotionText>'

    Examples: 
      | productCode | deliveryPromotionRank          | expressDeliveryPromo                 | homeDeliveryPromo                    | CnCPromo                             | deliveryPromotionSticker | deliveryPromotionText  |
      | W1000001    | click-and-collect,home,express | STICKER PRESENT,TEXT PRESENT         | STICKER NOT-PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT PRESENT         | cnc sticker              | cnc or home or express |
      | W1000011    | express,home,click-and-collect | STICKER PRESENT,TEXT PRESENT         | STICKER NOT-PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT PRESENT         | express sticker          | express or home or cnc |
      | W1000021    | express,home,click-and-collect | STICKER NOT-PRESENT,TEXT NOT-PRESENT | STICKER NOT-PRESENT,TEXT NOT-PRESENT | STICKER NOT-PRESENT,TEXT NOT-PRESENT | NONE                     | NONE                   |
      | W1000031    | home,express,click-and-collect | STICKER NOT-PRESENT,TEXT PRESENT     | STICKER NOT-PRESENT,TEXT PRESENT     | STICKER NOT-PRESENT,TEXT NOT-PRESENT | NONE                     | home or express        |
      | W1000041    | home,click-and-collect,express | STICKER NOT-PRESENT,TEXT NOT-PRESENT | STICKER NOT-PRESENT,TEXT NOT-PRESENT | STICKER PRESENT,TEXT PRESENT         | cnc sticker              | cnc                    |
      | W1000051    | express,click-and-collect,home | STICKER PRESENT,TEXT PRESENT         | STICKER PRESENT,TEXT PRESENT         | STICKER NOT-PRESENT,TEXT NOT-PRESENT | home sticker             | home                   |
      | W1000061    | home,express,click-and-collect | STICKER PRESENT,TEXT NOT-PRESENT     | STICKER PRESENT,TEXT NOT-PRESENT     | STICKER NOT-PRESENT,TEXT NOT-PRESENT | home sticker             | NONE                   |
      | W1000071    | click-and-collect,express,home | STICKER PRESENT,TEXT PRESENT         | STICKER NOT-PRESENT,TEXT NOT-PRESENT | STICKER NOT-PRESENT,TEXT NOT-PRESENT | NONE                     | NONE                   |
      | W1000081    | click-and-collect,express,home | STICKER PRESENT,TEXT PRESENT         | STICKER NOT-PRESENT,TEXT NOT-PRESENT | STICKER PRESENT,TEXT NOT-PRESENT     | cnc sticker              | NONE                   |
      | W1000091    | express,home,click-and-collect | STICKER PRESENT,TEXT PRESENT         | STICKER PRESENT,TEXT PRESENT         | STICKER NOT-PRESENT,TEXT NOT-PRESENT | express sticker          | express or home        |
      | W1000101    | express,home,click-and-collect | STICKER PRESENT,TEXT PRESENT         | STICKER NOT-PRESENT,TEXT NOT-PRESENT | STICKER NOT-PRESENT,TEXT NOT-PRESENT | NONE                     | NONE                   |
