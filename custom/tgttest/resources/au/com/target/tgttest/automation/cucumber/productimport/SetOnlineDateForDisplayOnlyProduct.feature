@esbPriceUpdate @cartProductData
Feature: Show New Sticker for display only products on PDP and listing page
    In order to show new sticker on display only products
    As target online
    I want to set onlineDate for display only products properly during step update and price update

  Background: 
    Given the 'newin.onlinefrom' feature switch is enabled
    And products with the following ats values in fluent:
      | product   | CC | HD | ED |
      | 100010901 | 1  | 0  | 0  |

  Scenario Outline: Receive price update for display only products
    Given a product in hybris:
      | productCode | displayOnly | approvedStatus   | onlineDate         |
      | 100010901   | Y           | <approvedStatus> | <onlineDateBefore> |
    And POS updates price for product '100010901':
      | Date | SellPrice | PermPrice | promoEvent |
      | null | 10        | 10        | true       |
    And the 'fluent' feature switch is <fluentEnabled>
    When Hybris POS price update runs
    Then its onlineDate will be updated to <onlineDateAfter>

    Examples: 
      | fluentEnabled | displayOnly | approvedStatus | onlineDateBefore    | onlineDateAfter     |
      | enabled       | Y           | approved       | empty               | CURRENT_DATE        |
      | enabled       | Y           | approved       | 2017-01-01 00:00:00 | 2017-01-01 00:00:00 |
      | enabled       | Y           | unapproved     | empty               | empty               |
      | disabled      | Y           | approved       | empty               | CURRENT_DATE        |
      | disabled      | Y           | approved       | 2017-01-01 00:00:00 | 2017-01-01 00:00:00 |
      | disabled      | Y           | unapproved     | empty               | empty               |

  @notAutomated
  Scenario Outline: update onlineDate from STEP to Hybris
    Given a product in hybris:
      | productCode | onlineDate         |
      | 100010901   | <onlineDateBefore> |
    When it receives an update from STEP with onlineDate as <onlineDateStep>
    Then its onlineDate will be updated to <onlineDateAfter>

    Examples: 
      | onlineDateBefore    | onlineDateStep      | onlineDateAfter     |
      | empty               | empty               | empty               |
      | 2017-01-01 00:00:00 | 2017-03-01 00:00:00 | 2017-03-01 00:00:00 |
      | 2017-03-01 00:00:00 | 2017-01-01 00:00:00 | 2017-01-01 00:00:00 |
      | 2017-01-01 00:00:00 | empty               | 2017-01-01 00:00:00 |

  Scenario Outline: Activate display only product from step
    Given a product in hybris:
      | productCode | price   | displayOnly   | approvedStatus | onlineDate         |
      | 100010901   | <price> | <displayOnly> | unapproved     | <onlineDateBefore> |
    And product '100010901' is set to 'active' in STEP
    When STEP product import runs and updates approval status
    Then its onlineDate will be updated to <onlineDateAfter>

    Examples: 
      | price | displayOnly | onlineDateBefore    | onlineDateAfter     |
      | 10    | Y           | 2017-01-01 00:00:00 | 2017-01-01 00:00:00 |
      | 10    | Y           | empty               | CURRENT_DATE        |
