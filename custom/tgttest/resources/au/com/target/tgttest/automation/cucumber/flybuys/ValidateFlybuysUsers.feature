@rollback @cartProductData
Feature: Flybuys - Authenticate flybuys members
In order to see my redeem options, as a customer I need to authenticate with flybuys.
  Note that the message codes are translated in the UI layer to messages, eg
  INVALID = The details you have supplied are invalid. Please provide the DOB and residential postcode of the primary card holder.
  UNAVAILABLE = Oops thats not supposed to happen. Please try again.
  FLYBUYS_OTHER_ERROR = There was a problem with the details you entered. Please contact flybuys directly for further explanation on 131116.

  Scenario: Authentication details dont match
    Given the user entered flybuys details are 'invalid'
    When the customer selects Redeem
    Then a message is displayed to the customer 'INVALID'

  Scenario: Flybuys Offline
    Given that flybuys is offline
    And the user entered flybuys details are 'valid'
    When the customer selects Redeem
    Then a message is displayed to the customer 'UNAVAILABLE'

  Scenario: WebMethods Offline
    Given that Webmethods is offline
    And the user entered flybuys details are 'valid'
    When the customer selects Redeem
    Then a message is displayed to the customer 'UNAVAILABLE'

  Scenario: Lost/Stolen flybuys card
    Given that the customer enters stolen flybuys account details
    And the user entered flybuys details are 'valid'
    And the customer selects Redeem
    Then a message is displayed to the customer 'FLYBUYS_OTHER_ERROR'
