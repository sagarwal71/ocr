@cartProductData @sessionCatalogVersion @fastlinestockData
Feature: Gift Cards - checkout
  
  As an Online customer
  I want to checkout the cart with different carts having digital gift cards
  so that I can purchase gift cards individually as well as with different physical items

  Scenario: Single gift card in cart
    Delivery mode is auto set to digital-gift-card

    Given a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName | email                         | messageText               |
      | PGC1000_iTunes_10 | Shreeram  | Mishra   | Shreeram.Mishra@target.com.au | Testing the checkout page |
    And registered user checkout
    When customer checkout is started
    Then delivery mode is 'digital-gift-card'
    And cart delivery address is empty
    And cart cnc store is empty

  Scenario: Mixed cart with giftcard and physical items
    Both digital and physical delivery modes are available

    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName | email                         | messageText   |
      | PGC1000_iTunes_10 | Shreeram  | Mishra   | Shreeram.Mishra@target.com.au | Test Checkout |
    And registered user checkout
    When customer checkout is started
    Then cart has empty delivery mode
    And available delivery modes are 'home-delivery', 'click-and-collect', 'express-delivery'
    And available digital delivery modes are 'digital-gift-card'

  Scenario: SOH on mixed basket leaves just digital items in basket
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName | email                         | messageText   |
      | PGC1000_iTunes_10 | Shreeram  | Mishra   | Shreeram.Mishra@target.com.au | Test Checkout |
    And registered user checkout
    And delivery mode is 'home-delivery'
    When the product '10000011' is out of stock
    And a soh check is done on the cart
    Then the cart has product 'PGC1000_iTunes_10'
    And delivery mode is 'digital-gift-card'
    And cart delivery address is empty
    And cart cnc store is empty

  Scenario: SOH on mixed basket with two items non-digital leaves one physical item in basket
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
      | 10000012 | 3   | 15    |
    When a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName | email                         | messageText   |
      | PGC1000_iTunes_10 | Shreeram  | Mishra   | Shreeram.Mishra@target.com.au | Test Checkout |
    And registered user checkout
    And delivery mode is 'home-delivery'
    When the product '10000011' is out of stock
    And a soh check is done on the cart
    Then the cart has product 'PGC1000_iTunes_10'
    And the cart has product '10000012'
    And delivery mode is 'home-delivery'
