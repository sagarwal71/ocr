@notAutomated
Feature: IPG - Display the ablility to view the total amount on the Review page
  In order to provide better customer experience
  As target online customer
  I want to see the option for total amount on the review page

  Scenario Outline: User has the ability to view total amount on the Review page
     Given IPG is turned on
     And payment method selected is credit card
     And user is on review page 
     When the total value in the order summary section is $<summary>
     Then the total in the iframe is $<iframe_total>

      Examples: 
      | summary  | iframe_total  |
      | 10       | 10            |
      | 100      | 100           |
      | 22.22    | 22.22         |
      | 1000.99  | 1000.99       |
      


  