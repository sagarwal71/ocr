@cleanOrderData @cleanConsignmentData @cartProductData @storeFulfilmentCapabilitiesData
Feature: Instore Fulfilment - Update SOH check to include SKU count

  Background: 
    Given committed product count start time is '4 hours ago'
    And list of consignments for store 'Robina' are:
      | consignmentCode | products      | consignmentStatus      | createdDate  | waveDate     | pickDate     | packDate    | shipDate    |
      | autox11000121   | 1 of 10000011 | shipped                | 6 hours ago  | 6 hours ago  | 6 hours ago  | 6 hours ago | 6 hours ago |
      | autox11000122   | 1 of 10000011 | shipped                | 4 hours ago  | 4 hours ago  | 4 hours ago  | 4 hours ago | 4 hours ago |
      | autox11000123   | 1 of 10000011 | shipped                | 6 hours ago  | 6 hours ago  | 6 hours ago  | 6 hours ago | 4 hours ago |
      | autox11000124   | 1 of 10000011 | packed                 | 6 hours ago  | 6 hours ago  | 6 hours ago  | 6 hours ago | n/a         |
      | autox11000125   | 1 of 10000011 | packed                 | 6 hours ago  | 6 hours ago  | 6 hours ago  | 4 hours ago | n/a         |
      | autox11000126   | 1 of 10000011 | picked                 | 12 hours ago | 12 hours ago | 12 hours ago | n/a         | n/a         |
      | autox11000127   | 1 of 10000011 | waved                  | 2 hours ago  | 2 hours ago  | n/a          | n/a         | n/a         |
      | autox11000128   | 1 of 10000011 | confirmed_by_warehouse | 2 hours ago  | n/a          | n/a          | n/a         | n/a         |
      | autox11000129   | 1 of 10000011 | cancelled              | 2 hours ago  | n/a          | n/a          | n/a         | n/a         |

  Scenario Outline: SOH check to include SKU count
    Given stores with stock:
      | store | stock          |
      | 7126  | 10 of 10000011 |
    And store SOH buffer is set to '1' for '7126'
    And the quantity added to cart for the '<product>' is '<qty>'
    And the delivery address is '19,Robina Town Centre Drive,Robina,QLD,4226'
    When order is placed
    Then the assigned warehouse will be '<warehouse>'

    Examples: 
      | qty | warehouse | product  |
      | 2   | Robina    | 10000011 |
      | 8   | Fastline  | 10000011 |
      | 3   | Robina    | 10000011 |
