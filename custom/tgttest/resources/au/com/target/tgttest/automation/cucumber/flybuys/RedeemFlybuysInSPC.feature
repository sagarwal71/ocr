@rollback @cartProductData
Feature: Flybuys - Redeem flybuys points

  Background: 
    Given flybuys config max redeemable amount is 50
    And a registered customer goes into the checkout process
    And customer has valid flybuys number presented
    And the user entered flybuys details are 'valid'
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 8   | 6     |
    And Customer has available points '11000'
    And the customer logins flybuys in spc
    And the customer will be presented with the redemption options in spc
      | dollarAmt | points | code             |
      | 40        | 8000   | DUMMYREDEEMCODE4 |
      | 30        | 6000   | DUMMYREDEEMCODE3 |
      | 20        | 4000   | DUMMYREDEEMCODE2 |
      | 10        | 2000   | DUMMYREDEEMCODE1 |

  Scenario: select redemption options
    When the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE1'
    Then the customer will be presented with the redemption summary in spc
      | flybuysCardNumber | value | pointsConsumed | availablePoints |
      | 6008943218616910  | 10    | 2000           | 11000           |

  Scenario: redeem 40 dollar with 48 total in the cart
    When the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE4'
    Then the customer will be presented with the redemption summary in spc
      | flybuysCardNumber | value | pointsConsumed | availablePoints |
      | 6008943218616910  | 40    | 8000           | 11000           |

  Scenario: redeem 10 dollar after decreases cart value below 10 dollars
    Given the customer decreases cart value below 10 dollars
    When the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE1'
    And cart constains flybuys canRedeemPoints 'false'

  Scenario: redeem 20 dollar after decreases cart value below 20 dollars
    Given the customer decreases cart value below 20 dollars
    When the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE2'
    And cart constains flybuys canRedeemPoints 'true'
    And the customer will be presented with the redemption error message in spc

  Scenario: redeem 0 dollar
    When the customer redeems flybuys points with redeem code 'remove'
    Then the customer will be presented with the redemption summary in spc
      | flybuysCardNumber | value | pointsConsumed | availablePoints |
      | 6008943218616910  | 0     | 0              | 11000           |

  Scenario: Order summary with flybuys
    Given the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE1'
    And the customer will be presented with the redemption summary in spc
      | flybuysCardNumber | value | pointsConsumed | availablePoints |
      | 6008943218616910  | 10    | 2000           | 11000           |
    When the order summary is displayed via spc
    Then cart summary is:
      | total | subtotal | totalTax | totalDiscounts | pointsConsumed |
      | 38.0  | 48.0     |          | 10.0           | 2000           |

  Scenario: Remove the 40 dollar flybuys discount when the cart value below 40 dollars after apply the TMD
    Given the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE4'
    And the customer will be presented with the redemption summary in spc
      | flybuysCardNumber | value | pointsConsumed | availablePoints |
      | 6008943218616910  | 40    | 8000           | 11000           |
    When customer apply tmd 'valid'
    Then cart summary is:
      | total | subtotal | totalTax | totalDiscounts |
      | 38.4  | 38.4     |          | 0              |

  Scenario: keep the 30 dollar flybuys discount when the cart value is still above 30 dollars after apply the TMD
    Given the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE3'
    And the customer will be presented with the redemption summary in spc
      | flybuysCardNumber | value | pointsConsumed | availablePoints |
      | 6008943218616910  | 30    | 6000           | 11000           |
    When customer apply tmd 'valid'
    Then cart summary is:
      | total | subtotal | totalTax | totalDiscounts | pointsConsumed |
      | 8.4   | 38.4     |          | 30             | 6000           |
