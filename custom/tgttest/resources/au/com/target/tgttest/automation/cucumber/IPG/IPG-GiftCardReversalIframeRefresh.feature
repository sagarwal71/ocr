@notAutomated
Feature: IPG - Gift Cards - Reversal
  As an online customer
  I want funds returned to my gift card if I alter navigation
  So that I can use my gift card for continuing payment again

  Scenario: Perform gift card reversal on iframe refresh
    Given a cart with total:
      | total  |
      | 100.00 |
    And customer chooses gift card payment method
    And customer is in review page
    And customer uses gift card for paying amount
      | amount |
      | 60.00  |
    When customer refreshes the review page
    Then amount paid with gift card is reversed
      | amount |
      | 60.00  |
    And new iframe is displayed
    And outstanding amount to pay is
      | amount |
      | 100.00 |

  Scenario: Perform gift card reversal when customer paid partially with gift card, navigates away and comes back to complete payment
    Given a cart with total:
      | total  |
      | 100.00 |
    And customer chose gift card payment method
    And customer is in review page
    And customer uses gift card for paying amount
      | amount |
      | 60.00  |
    And customer navigates away from review page
    When customer comes back to review page for payment again
    Then amount paid with gift card is reversed
      | amount |
      | 60.00  |
    And new iframe is displayed
    And outstanding amount
      | amount |
      | 100.00 |

  Scenario: Perform gift card reversal and handle unsuccessful reversal on iframe refresh
    Given a cart with total:
      | total  |
      | 100.00 |
    And customer chooses gift card payment method
    And customer is in review page
    And customer uses gift card for paying amount
      | amount |
      | 60.00  |
    When customer refreshes the review page
    Then amount paid with gift card reversal is failed
      | amount |
      | 60.00  |
    And a reverse gift card process is created to retry.
    And the reverse gift card process retry exceeds the max attempt
    And a CS ticket is created
    And a gift card reversal failed notification email is sent to customer
    And new iframe is displayed
    And outstanding amount to pay is
      | amount |
      | 100.00 |
