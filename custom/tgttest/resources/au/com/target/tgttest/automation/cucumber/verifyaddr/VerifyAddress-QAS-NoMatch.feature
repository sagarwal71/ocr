@notAutomated
Feature: Handle no address matches found by QAS

  Background: 
    Given single line address lookup feature is enabled
    And the "Delivery Address" page is displayed with no pre-selected address
    And the single-line address is displayed as a blank field and Placeholder text: "eg. 10 Thompsons Road, North Geelong, VIC 3215"

  Scenario: Leave single line address field when it has >=4 characters
    Given the single line address field is displayed
    And the field has the focus
    And the field has >=4 characters
    When the field loses the focus
    Then make a pretend QAS call that returns no matches
    And if the address selection box is already displayed, leave it displayed.
    And if the address selection box is NOT displayed, display it.
    And display the message "Unable to verify address"
    And include a "Manually enter my address" link below the message. (Label to be confirmed.)

  Scenario: Stop typing in single line address field when it has >=4 characters
    Given the single line address field is displayed
    And the field has the focus
    And the field has >=4 characters
    When the user stops entering or removing characters for 0.n seconds (n to be determined)
    Then make a pretend QAS call that returns no matches
    And if the address selection box is already displayed, leave it displayed.
    And if the address selection box is NOT displayed, display it.
    And display the message "Unable to verify address"
    And include a "Manually enter my address" link below the message. (Label to be confirmed.)

  Scenario: Choose the "Manually enter my address" link
    Given the single line address field is displayed
    And the address selection box has been displayed with the "No matching addresses could be found!" message
    When the user chooses the "Manually enter my address" link
    Then hide the single line address field
    And hide the address selection box
    And display the separate fields for Address line 1, Address line 2, Suburb, State and Postcode
    And display all of those fields as blank
    And provide a "Return to address lookup" link
    And display the "Save address" button.

  Scenario: Choose the "Return to address lookup" link
    Given the separate fields for Address line 1, Address line 2, Suburb, State and Postcode are displayed
    And the "Return to address lookup" link is displayed
    When the user chooses the "Return to address lookup" link
    Then hide the separate fields for Address line 1, Address line 2, Suburb, State and Postcode.
    And hide the "Return to address lookup" link.
    And hide the "Save address" button.
    And display the single line address field.
    And display that field as blank with Placeholder text.
