@sessionStagedCatalogVersion @adminUser
Feature: Checks product end of life links for shop by original category.
  
  Description: The products used for testing the scenarios has been setup during 
  initialization process using the following impex-
  /tgttest/automation-impex/product/product-endoflife-links.impex

  Scenario Outline: Populate product end of life link shop by category
    When requested original category for the product '<productCode>'
    Then link shop by original category is '<url>'

    Examples: 
      | productCode | originalCategory | approvedStatus | url                     |
      | N50141      | W95135           | approved       | /c/women/dresses/W95135 |
      | N50131      |                  | approved       |                         |

  Scenario Outline: Populate product end of life link shop by inspiration
    When requested inspiration link for the product '<productCode>'
    Then link shop by inspiration is '<url>'

    Examples: 
      | productCode | originalCategory | topLevelCategory | shopTheLook | approvedStatus | url                                    |
      | N50141      | W95135           | W93743           | STL02       | approved       | /inspiration/women-s-inspiration/STL02 |
      | N50151      | W418111          | W152974          |             | approved       |                                        |

  @notAutomated
  Scenario Outline: Populate product end of life link shop by new arrivals
    When requested new arrivals link for the product '<productCode>'
    Then link shop by new arrivals is '<url>'
    And name of new arrivals is '<name>'

    Examples: 
      | productCode | originalCategory | topLevelCategory | name         | approvedStatus | url                    |
      | N50141      | W95135           | W93743           | New Arrivals | approved       | /c/women/W93743?N=27pk |
      | N50151      | W418111          | W152974          |              | approved       |                        |
