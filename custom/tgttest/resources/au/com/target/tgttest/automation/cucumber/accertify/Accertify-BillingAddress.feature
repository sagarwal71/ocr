@cartProductData  @deliveryModeData
Feature: Accertify - Do not send billing address to accertify for home delivery orders (including express delivery)
  As a target online security team member
  I want the data feed for each transaction sent to Acertify including valid address
  So that I check whether there is a fraud risk for each transaction

  Background: 
    Given payment method is 'ipg'

  Scenario Outline: Exclude billing address to accertify only for home delivery orders
    Given feature 'accertify.include.billingAddress' is switched '<featureIncludeBillingAddress>'
    And any cart
    And delivery mode is '<deliveryMode>'
    And the billing address is '14 Emert street,Wentworthville,NSW,2145'
    When order is placed
    Then the accertify billing information is <billingAddressOnFeed>

    Examples: 
      | featureIncludeBillingAddress | deliveryMode      | billingAddressOnFeed |
      | on                           | home-delivery     | included             |
      | on                           | express-delivery  | included             |
      | on                           | click-and-collect | included             |
      | off                          | home-delivery     | excluded             |
      | off                          | express-delivery  | excluded             |
      | off                          | click-and-collect | included             |

  Scenario Outline: Exclude billing address for failed credit card transaction
    Given feature 'accertify.include.billingAddress' is switched '<featureIncludeBillingAddress>'
    And any cart
    And delivery mode is '<deliveryMode>'
    And the billing address is '14 Emert street,Wentworthville,NSW,2145'
    When order is placed with invalid creditcard
      | cardType | cardNumber       | cardExpiry |
      | MASTER   | 4242424242424242 | 01/2022    |
    And order is placed with valid creditcard
      | cardType | cardNumber       | cardExpiry |
      | VISA     | 5896356978965423 | 01/2020    |
    Then the failed credit card attempt is 1
    And the accertify billing information is <billingAddressOnFeed>
    And the billing information for the failed payment is <billingAddressOnFeed>

    Examples: 
      | featureIncludeBillingAddress | deliveryMode      | billingAddressOnFeed |
      | on                           | home-delivery     | included             |
      | on                           | express-delivery  | included             |
      | on                           | click-and-collect | included             |
      | off                          | home-delivery     | excluded             |
      | off                          | express-delivery  | excluded             |
      | off                          | click-and-collect | included             |
