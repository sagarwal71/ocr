/**
 * 
 */
package au.com.target.tgttest.automation.rest;

import de.hybris.platform.core.model.order.OrderModel;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import au.com.target.tgttest.automation.facade.BusinessProcessUtil;
import au.com.target.tgttest.automation.facade.CartUtil;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.ProductUtil;
import au.com.target.tgttest.automation.facade.domain.Order;


/**
 * Web Service to create an order
 * 
 */
@Path("/createorder")
public class CreateOrderResource {

    private static final String ANY_PRD_CODE = "10000011";
    private static final int ANY_PRD_QTY = 2;
    private static final double ANY_PRD_PRICE = 15.00;

    @GET
    @Produces("text/plain")
    @Path("anyorder")
    public String anyOrder() throws Exception {

        ProductUtil.updateProductPrice(ANY_PRD_CODE, ANY_PRD_PRICE);
        CartUtil.addToCart(ANY_PRD_CODE, ANY_PRD_QTY);
        CartUtil.recalculateSessionCart();

        final OrderModel orderModel = CheckoutUtil.placeOrder();
        Order.setOrder(orderModel);

        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("placeOrderProcess");
        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("acceptOrderProcess");

        final String code = Order.getInstance().getOrderNumber();
        return code;
    }
}
