/**
 * 
 */
package au.com.target.tgtfulfilment.integration.dto.sendtowarehouse;

import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * Product DTO
 * 
 * @author jjayawa1
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductDTO {

    private String id;
    private String ean;
    private String name;
    private String denomination;
    private String brandId;
    private String giftCardStyle;
    private String productId;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * @return the ean
     */
    public String getEan() {
        return ean;
    }

    /**
     * @param ean
     *            the ean to set
     */
    public void setEan(final String ean) {
        this.ean = ean;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the denomination
     */
    public String getDenomination() {
        return denomination;
    }

    /**
     * @param denomination
     *            the denomination to set
     */
    public void setDenomination(final String denomination) {
        this.denomination = denomination;
    }

    /**
     * @return the brandId
     */
    public String getBrandId() {
        return brandId;
    }

    /**
     * @param brandId
     *            the brandId to set
     */
    public void setBrandId(final String brandId) {
        this.brandId = brandId;
    }

    /**
     * @return the giftCardStyle
     */
    public String getGiftCardStyle() {
        return giftCardStyle;
    }

    /**
     * @param giftCardStyle
     *            the giftCardStyle to set
     */
    public void setGiftCardStyle(final String giftCardStyle) {
        this.giftCardStyle = giftCardStyle;
    }

    /**
     * @return the productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId
     *            the productId to set
     */
    public void setProductId(final String productId) {
        this.productId = productId;
    }

}
