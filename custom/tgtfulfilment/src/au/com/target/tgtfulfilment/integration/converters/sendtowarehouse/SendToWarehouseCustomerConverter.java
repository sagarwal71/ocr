/**
 * 
 */
package au.com.target.tgtfulfilment.integration.converters.sendtowarehouse;

import org.springframework.util.Assert;

import au.com.target.tgtcore.converters.TargetConverter;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.CustomerDTO;
import au.com.target.tgtfulfilment.util.EscapeSpecialCharactersUtil;


/**
 * Convert TargetCustomerModel to CustomerDTO
 * 
 * @author jjayawa1
 *
 */
public class SendToWarehouseCustomerConverter implements TargetConverter<TargetCustomerModel, CustomerDTO> {

    /**
     * {@inheritDoc}
     */
    @Override
    public CustomerDTO convert(final TargetCustomerModel source) {
        Assert.notNull(source, "TargetCustomerModel cannot be null");

        final CustomerDTO customer = new CustomerDTO();
        customer.setFirstName(EscapeSpecialCharactersUtil.escapeSpecialXMLCharacters(source.getFirstname()));
        customer.setLastName(EscapeSpecialCharactersUtil.escapeSpecialXMLCharacters(source.getLastname()));
        customer.setEmailAddress(source.getContactEmail());
        customer.setCountry("AU");
        return customer;
    }

}
