/**
 * 
 */
package au.com.target.tgtfulfilment.integration.dto.pollwarehouse;

import java.util.List;


/**
 * Poll warehouse request DTO.
 * 
 * @author gsing236
 *
 */
public class PollWarehouseRequest {

    private List<String> consignmentRequestNumbers;

    /**
     * @return the consignmentRequestNumbers
     */
    public List<String> getConsignmentRequestNumbers() {
        return consignmentRequestNumbers;
    }

    /**
     * @param consignmentRequestNumbers
     *            the consignmentRequestNumbers to set
     */
    public void setConsignmentRequestNumbers(final List<String> consignmentRequestNumbers) {
        this.consignmentRequestNumbers = consignmentRequestNumbers;
    }
}
