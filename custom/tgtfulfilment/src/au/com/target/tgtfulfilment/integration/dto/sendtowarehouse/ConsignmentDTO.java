/**
 * 
 */
package au.com.target.tgtfulfilment.integration.dto.sendtowarehouse;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * Consignment DTO
 * 
 * @author jjayawa1
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConsignmentDTO {
    private String id;
    private String warehouseId;
    private String shippingMethod;
    private OrderDTO order;
    private List<ConsignmentEntryDTO> consignmentEntries;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * @return the warehouseId
     */
    public String getWarehouseId() {
        return warehouseId;
    }

    /**
     * @param warehouseId
     *            the warehouseId to set
     */
    public void setWarehouseId(final String warehouseId) {
        this.warehouseId = warehouseId;
    }

    /**
     * @return the order
     */
    public OrderDTO getOrder() {
        return order;
    }

    /**
     * @param order
     *            the order to set
     */
    public void setOrder(final OrderDTO order) {
        this.order = order;
    }

    /**
     * @return the consignmentEntries
     */
    public List<ConsignmentEntryDTO> getConsignmentEntries() {
        return consignmentEntries;
    }

    /**
     * @param consignmentEntries
     *            the consignmentEntries to set
     */
    public void setConsignmentEntries(final List<ConsignmentEntryDTO> consignmentEntries) {
        this.consignmentEntries = consignmentEntries;
    }

    /**
     * @return the shippingMethod
     */
    public String getShippingMethod() {
        return shippingMethod;
    }

    /**
     * @param shippingMethod
     *            the shippingMethod to set
     */
    public void setShippingMethod(final String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

}
