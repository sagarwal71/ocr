/**
 * 
 */
package au.com.target.tgtfulfilment.integration.dto.sendtowarehouse;

import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * Order DTO
 * 
 * @author jjayawa1
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderDTO {
    private String id;
    private CustomerDTO customer;
    private AddressDTO shippingAddress;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * @return the customer
     */
    public CustomerDTO getCustomer() {
        return customer;
    }

    /**
     * @param customer
     *            the customer to set
     */
    public void setCustomer(final CustomerDTO customer) {
        this.customer = customer;
    }

    /**
     * @return the shippingAddress
     */
    public AddressDTO getShippingAddress() {
        return shippingAddress;
    }

    /**
     * @param shippingAddress
     *            the shippingAddress to set
     */
    public void setShippingAddress(final AddressDTO shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

}
