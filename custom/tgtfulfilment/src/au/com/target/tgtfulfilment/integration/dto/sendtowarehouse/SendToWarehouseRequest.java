/**
 * 
 */
package au.com.target.tgtfulfilment.integration.dto.sendtowarehouse;

import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * SendToWarehouseRequest DTO
 * 
 * @author jjayawa1
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SendToWarehouseRequest {
    private ConsignmentDTO consignment;

    /**
     * @return the consignment
     */
    public ConsignmentDTO getConsignment() {
        return consignment;
    }

    /**
     * @param consignment
     *            the consignment to set
     */
    public void setConsignment(final ConsignmentDTO consignment) {
        this.consignment = consignment;
    }

}
