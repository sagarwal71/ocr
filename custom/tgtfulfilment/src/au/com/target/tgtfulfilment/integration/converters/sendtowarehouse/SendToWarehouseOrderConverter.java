/**
 * 
 */
package au.com.target.tgtfulfilment.integration.converters.sendtowarehouse;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.converters.TargetConverter;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.OrderDTO;


/**
 * Convert OrderModel to DTO.
 * 
 * @author jjayawa1
 *
 */
public class SendToWarehouseOrderConverter implements TargetConverter<OrderModel, OrderDTO> {

    private SendToWarehouseCustomerConverter sendToWarehouseCustomerConverter;
    private SendToWarehouseAddressConverter sendToWarehouseAddressConverter;

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderDTO convert(final OrderModel source) {
        Assert.notNull(source, "OrderModel cannot be null");

        final OrderDTO order = new OrderDTO();
        order.setId(source.getCode());

        final UserModel customer = source.getUser();
        if (customer != null) {
            if (customer instanceof TargetCustomerModel) {
                final TargetCustomerModel targetCustomerModel = (TargetCustomerModel)customer;
                order.setCustomer(sendToWarehouseCustomerConverter.convert(targetCustomerModel));
            }
        }

        final AddressModel shippingAddress = source.getDeliveryAddress();
        if (null != shippingAddress) {
            order.setShippingAddress(sendToWarehouseAddressConverter.convert(shippingAddress));
        }

        return order;
    }

    /**
     * @param sendToWarehouseCustomerConverter
     *            the sendToWarehouseCustomerConverter to set
     */
    @Required
    public void setSendToWarehouseCustomerConverter(
            final SendToWarehouseCustomerConverter sendToWarehouseCustomerConverter) {
        this.sendToWarehouseCustomerConverter = sendToWarehouseCustomerConverter;
    }

    /**
     * @param sendToWarehouseAddressConverter
     *            the sendToWarehouseAddressConverter to set
     */
    @Required
    public void setSendToWarehouseAddressConverter(
            final SendToWarehouseAddressConverter sendToWarehouseAddressConverter) {
        this.sendToWarehouseAddressConverter = sendToWarehouseAddressConverter;
    }

}
