/**
 * 
 */
package au.com.target.tgtfulfilment.integration.dto;

/**
 * Enum for errors during integration.
 * 
 * @author jjayawa1
 *
 */
public enum IntegrationError {
    CONNECTION_ERROR,
    COMMUNICATION_ERROR,
    REMOTE_SERVICE_ERROR,
    MIDDLEWARE_ERROR;
}
