/**
 * 
 */
package au.com.target.tgtfulfilment.integration.dto;

/**
 * Response from carrier for transmitted manifest
 * 
 * @author jjayawa1
 *
 */
public class ManifestResponseDTO extends BaseResponseDTO {
    // Empty class to complete the hierarchy
}
