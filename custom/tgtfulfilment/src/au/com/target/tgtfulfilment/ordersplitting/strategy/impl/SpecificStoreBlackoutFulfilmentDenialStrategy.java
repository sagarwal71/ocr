/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * Check if store has a blackout in operation
 * 
 * @author sbryan6
 *
 */
public class SpecificStoreBlackoutFulfilmentDenialStrategy extends BaseSpecificStoreFulfilmentDenialStrategy {

    @Override
    public DenialResponse isDenied(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos) {

        if (getStoreFulfilmentCapabilitiesService().isTargetStoreBlackoutExist(tpos)) {

            return DenialResponse.createDenied("StoreBlackOutPeriod");
        }

        return DenialResponse.createNotDenied();
    }

}
