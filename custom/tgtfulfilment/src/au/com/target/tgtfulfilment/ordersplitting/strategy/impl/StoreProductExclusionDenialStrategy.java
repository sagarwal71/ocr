/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.Set;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.model.ProductExclusionsModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.FilterExclusionListService;


/**
 * @author ayushman
 *
 */
public class StoreProductExclusionDenialStrategy extends BaseSpecificStoreFulfilmentDenialStrategy {

    private FilterExclusionListService filterExclusionListService;

    @Override
    public DenialResponse isDenied(final OrderEntryGroup oeg, final TargetPointOfServiceModel targetPointOfService) {
        Assert.notNull(oeg, "orderEntryGroup cannot be null");
        Assert.notNull(targetPointOfService, "targetPointOfService cannot be null");
        final StoreFulfilmentCapabilitiesModel storeFulfilmentCapabilitiesModel = targetPointOfService
                .getFulfilmentCapability();
        final Set<ProductExclusionsModel> productExclusionList = filterExclusionListService
                .filterActiveProductExclusions(storeFulfilmentCapabilitiesModel.getProductExclusions());
        if (getStoreFulfilmentCapabilitiesService().isProductInExclusionList(oeg, productExclusionList)) {
            return DenialResponse.createDenied("StoreProductExclusionDenial");
        }
        return DenialResponse.createNotDenied();
    }



    /**
     * @param filterExclusionListService
     *            the filterExclusionListService to set
     */
    @Required
    public void setFilterExclusionListService(final FilterExclusionListService filterExclusionListService) {
        this.filterExclusionListService = filterExclusionListService;
    }


}
