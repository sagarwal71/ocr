/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Utilities;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.logger.RoutingLogger;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.ConsignmentToWarehouseAssigner;
import au.com.target.tgtfulfilment.service.TargetCarrierSelectionService;


/**
 * @author sbryan6
 *
 */
public class ConsignmentToWarehouseAssignerImpl implements ConsignmentToWarehouseAssigner {

    private static final RoutingLogger LOG = new RoutingLogger(ConsignmentToWarehouseAssignerImpl.class);

    private TargetCarrierSelectionService targetCarrierSelectionService;

    private TargetWarehouseService targetWarehouseService;

    private TargetDeliveryService targetDeliveryService;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private TargetStockService targetStockService;

    private ModelService modelService;


    @Override
    public void assignConsignmentToStore(final TargetConsignmentModel consignment,
            final WarehouseModel warehouse,
            final OfcOrderType orderType,
            final TargetZoneDeliveryModeModel deliveryMode,
            final String orderCode) {

        assignWarehouseAndStatus(consignment, warehouse);

        if (null == orderType) {
            LOG.logWarnMessage("Null orderType, carrier not set: order=" + orderCode);
            return;
        }


        if (orderType.equals(OfcOrderType.CUSTOMER_DELIVERY) || orderType.equals(OfcOrderType.INTERSTORE_DELIVERY)) {
            assignPreferredCarrierForStore(consignment);
        }
        else {
            assignNullCarrier(consignment, deliveryMode);
        }
    }


    @Override
    public void assignConsignmentToNTL(final TargetConsignmentModel consignment,
            final WarehouseModel warehouse) {

        assignWarehouseAndStatus(consignment, warehouse);
        assignPreferredCarrierForNonStore(consignment);
    }


    @Override
    public void assignConsignmentToExternalWarehouse(final TargetZoneDeliveryModeModel deliveryMode,
            final TargetConsignmentModel consignment,
            final WarehouseModel warehouse) {

        assignWarehouseAndStatus(consignment, warehouse);

        if (deliveryMode != null && BooleanUtils.isTrue(deliveryMode.getIsDigital())) {
            assignNullCarrier(consignment, deliveryMode);
        }
        else {
            LOG.logWarnMessage("Assigning the products to external warehouse, consignment=" +
                    consignment.getCode() + ", warehouse=" + warehouse.getCode());
            assignPreferredCarrierForNonStore(consignment);

        }
    }


    @Override
    public void assignDefaultWarehouse(final TargetConsignmentModel consignment) {
        assignWarehouseAndStatus(consignment, targetWarehouseService.getDefaultOnlineWarehouse());
        if (targetFeatureSwitchService
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FORCE_DEFAULT_WAREHOUSE_TO_ONLINE_POS)) {
            assignPreferredCarrierForStore(consignment);

        }
        else {
            assignPreferredCarrierForNonStore(consignment);
        }

        //added for Fastline Falcon
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)) {
            updateTotalQtyFulfilledByFastlinePerDay(consignment);
        }

    }


    @Override
    public void assignCancellationWarehouseAndUpdateConsignmentStatus(final TargetConsignmentModel consignment) {
        consignment.setStatus(ConsignmentStatus.CANCELLED);
        final WarehouseModel warehouseModel = targetWarehouseService
                .getWarehouseForCode(TgtCoreConstants.CANCELLATION_WAREHOUSE);
        consignment.setRejectReason(ConsignmentRejectReason.SYSTEM_REJECT);
        consignment.setWarehouse(warehouseModel);
    }

    /**
     * @param consignment
     */
    private void updateTotalQtyFulfilledByFastlinePerDay(final TargetConsignmentModel consignment) {
        final int currentQuantity = getTotalConsignmentQuantity(consignment);
        targetStockService.updateQuantityFulfilledByWarehouse(currentQuantity, consignment.getWarehouse());
        final WarehouseModel warehouseModel = targetWarehouseService
                .getWarehouseForCode(TgtCoreConstants.FASTLINE_WAREHOUSE);
        Utilities.invalidateCache(warehouseModel.getPk());
        modelService.refresh(warehouseModel);

    }


    /**
     * @param consignment
     * @return total quantity
     */
    private int getTotalConsignmentQuantity(final TargetConsignmentModel consignment) {
        int totalQuantity = 0;
        if (CollectionUtils.isNotEmpty(consignment.getConsignmentEntries())) {
            for (final ConsignmentEntryModel conEntryModel : consignment.getConsignmentEntries()) {
                totalQuantity += conEntryModel.getQuantity().intValue();
            }

        }
        return totalQuantity;
    }


    private void assignWarehouseAndStatus(final TargetConsignmentModel consignment, final WarehouseModel warehouse) {
        consignment.setWarehouse(warehouse);
        consignment.setStatus(ConsignmentStatus.CREATED);
    }


    /**
     * Assign null carrier.
     *
     * @param consignment
     *            the consignment
     * @param deliveryMode
     *            the delivery mode
     */
    private void assignNullCarrier(final TargetConsignmentModel consignment,
            final TargetZoneDeliveryModeModel deliveryMode) {
        final TargetCarrierModel nullCarrier = targetCarrierSelectionService.getNullCarrier(deliveryMode);
        consignment.setTargetCarrier(nullCarrier);
    }

    /**
     * Assign preferred carrier for instore fulfilment based on carrier selection logic.
     *
     * @param consignment
     *            the consignment
     */
    private void assignPreferredCarrierForStore(final TargetConsignmentModel consignment) {

        // Currently stores don't handle bulky products but if they did we could pass the bulky parameter here
        final TargetCarrierModel carrier = targetCarrierSelectionService.getPreferredCarrier(consignment,
                false, true);
        consignment.setTargetCarrier(carrier);
    }

    /**
     * Assign preferred carrier for non-store warehouse
     * 
     * @param consignment
     */
    private void assignPreferredCarrierForNonStore(final TargetConsignmentModel consignment) {

        // Assign carrier based on bulkiness
        final boolean bulky = targetDeliveryService.consignmentContainsBulkyProducts(consignment
                .getConsignmentEntries());

        final TargetCarrierModel carrier = targetCarrierSelectionService.getPreferredCarrier(consignment,
                bulky, false);
        consignment.setTargetCarrier(carrier);
    }


    /**
     * @param targetCarrierSelectionService
     *            the targetCarrierSelectionService to set
     */
    @Required
    public void setTargetCarrierSelectionService(final TargetCarrierSelectionService targetCarrierSelectionService) {
        this.targetCarrierSelectionService = targetCarrierSelectionService;
    }

    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */
    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }

    /**
     * @param targetDeliveryService
     *            the targetDeliveryService to set
     */
    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        this.targetDeliveryService = targetDeliveryService;
    }


    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }


    /**
     * @param targetStockService
     *            the targetStockService to set
     */
    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }

    /**
     * @param modelService
     *            the new model service
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }


}
