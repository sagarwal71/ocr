/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy;

import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.List;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;


/**
 * @author smishra1
 *
 */
public interface NearestStoreSelectorStrategy {

    /**
     * Sort stores by distance from given address
     * 
     * @param targetPointOfServiceList
     * @param deliveryAddress
     * @return list PointOfServiceDistanceData
     */
    List<PointOfServiceDistanceData> getSortedStoreListByDistance(
            final List<TargetPointOfServiceModel> targetPointOfServiceList,
            final AddressModel deliveryAddress);

    /**
     * Sort stores by distance from given address and filter out the store out of distance of radius
     * 
     * @param targetPointOfServiceList
     * @param deliveryAddress
     * @return list PointOfServiceDistanceData
     */
    List<PointOfServiceDistanceData> getSortedStoreListByDistanceWithDistanceRestriction(
            final List<TargetPointOfServiceModel> targetPointOfServiceList,
            final AddressModel deliveryAddress, double distanceKm);

    /**
     * sort stores by distance
     * 
     * @param storeListWithDistance
     */
    void sortByDistance(final List<PointOfServiceDistanceData> storeListWithDistance);
}
