/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtsale.stock.service.TargetStoreStockService;


/**
 * @author sbryan6
 *
 */
public class SpecificStoreStockFulfilmentDenialStrategy extends BaseSpecificStoreFulfilmentDenialStrategy {

    private TargetStoreStockService targetStoreStockService;

    @Override
    public DenialResponse isDenied(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos) {
        Assert.notNull(oeg, "oeg must not be null");

        if (tpos == null
                || !targetStoreStockService.isOrderEntriesInStockAtStore(oeg, tpos.getStoreNumber(),
                        getOegParameterHelper().getOrderCode(oeg))) {

            return DenialResponse.createDenied("OrderNotInStockAtStore");
        }

        return DenialResponse.createNotDenied();
    }

    @Required
    public void setTargetStoreStockService(final TargetStoreStockService targetStoreStockService) {
        this.targetStoreStockService = targetStoreStockService;
    }

}
