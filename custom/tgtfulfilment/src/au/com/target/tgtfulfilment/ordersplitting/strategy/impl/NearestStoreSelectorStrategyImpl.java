package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.storelocator.GPS;
import de.hybris.platform.storelocator.GeoWebServiceWrapper;
import de.hybris.platform.storelocator.data.AddressData;
import de.hybris.platform.storelocator.exception.GeoLocatorException;
import de.hybris.platform.storelocator.exception.GeoServiceWrapperException;
import de.hybris.platform.storelocator.exception.LocationServiceException;
import de.hybris.platform.storelocator.impl.DefaultGPS;
import de.hybris.platform.storelocator.impl.GeometryUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.NearestStoreSelectorStrategy;


/**
 * @author smishra1
 *
 */
public class NearestStoreSelectorStrategyImpl implements NearestStoreSelectorStrategy {
    private static final Logger LOG = Logger.getLogger(NearestStoreSelectorStrategyImpl.class);

    private GeoWebServiceWrapper geoWebServiceWrapper;

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.ordersplitting.strategy.NearestStoreSelectorStrategy#findClosestFulfillmentStore
     * (au.com.target.tgtcore.model.TargetPointOfServiceModel, au.com.target.tgtcore.model.TargetAddressModel)
     */
    @Override
    public List<PointOfServiceDistanceData> getSortedStoreListByDistance(
            final List<TargetPointOfServiceModel> targetPointOfServiceList,
            final AddressModel deliveryAddress) {

        if (CollectionUtils.isEmpty(targetPointOfServiceList) || null == deliveryAddress) {
            return null;
        }

        final GPS baseLocation = this.getGPSByAddress(deliveryAddress);

        if (null == baseLocation) {
            return null;
        }

        final List<PointOfServiceDistanceData> storeListWithDistance = getStoreListWithDistance(baseLocation,
                targetPointOfServiceList);

        this.sortByDistance(storeListWithDistance);

        return storeListWithDistance;
    }

    @Override
    public List<PointOfServiceDistanceData> getSortedStoreListByDistanceWithDistanceRestriction(
            final List<TargetPointOfServiceModel> targetPointOfServiceList, final AddressModel deliveryAddress,
            final double distanceKm) {
        List<PointOfServiceDistanceData> storeListWithDistance = new ArrayList<>();
        if (CollectionUtils.isEmpty(targetPointOfServiceList) || null == deliveryAddress || distanceKm <= 0) {
            return storeListWithDistance;
        }

        final GPS baseLocation = this.getGPSByAddress(deliveryAddress);

        if (null == baseLocation) {
            return storeListWithDistance;
        }
        storeListWithDistance = getStoreListWithDistance(baseLocation,
                targetPointOfServiceList);

        filterOutStoresByDistance(storeListWithDistance, distanceKm);

        this.sortByDistance(storeListWithDistance);

        return storeListWithDistance;
    }

    @Override
    public void sortByDistance(final List<PointOfServiceDistanceData> storeListWithDistance) {
        Collections.sort(storeListWithDistance,
                new Comparator<PointOfServiceDistanceData>() {
                    @Override
                    public int compare(final PointOfServiceDistanceData store1,
                            final PointOfServiceDistanceData store2) {
                        return Double.compare(store1.getDistanceKm(), store2.getDistanceKm());
                    }
                });
    }

    private void filterOutStoresByDistance(final List<PointOfServiceDistanceData> storeListWithDistance,
            final double distanceKm) {
        CollectionUtils.filter(storeListWithDistance, new Predicate() {
            @Override
            public boolean evaluate(final Object obj) {
                final double dis = ((PointOfServiceDistanceData)obj).getDistanceKm();
                // only return the store in distanceKm radius
                return dis < distanceKm;
            }

        });
    }

    private GPS getGPSByAddress(final AddressModel deliveryAddress) {
        final AddressData address = new AddressData(deliveryAddress);
        GPS baseLocation;
        try {
            baseLocation = geoWebServiceWrapper.geocodeAddress(address);
        }
        catch (final GeoServiceWrapperException e) {
            LOG.warn(e + " for=" + address + ". Retrying with postCode & countryCode");
            final AddressData partialAddress = new AddressData(deliveryAddress);
            partialAddress.setBuilding(null);
            partialAddress.setStreet(null);
            partialAddress.setCity(null);
            baseLocation = geoWebServiceWrapper.geocodeAddress(partialAddress);
        }
        return baseLocation;
    }

    /**
     * Method to get the List of PointOfServiceDistanceData with the calculated distance between delivery address and
     * the store
     * 
     * @param deliveryLocation
     *            delivery location GPS
     * @param pointOfServiceList
     *            list of TargetPointOfServiceModel
     * 
     * @return List of PointOfServiceDistanceData
     */
    private List<PointOfServiceDistanceData> getStoreListWithDistance(final GPS deliveryLocation,
            final List<TargetPointOfServiceModel> pointOfServiceList) {
        final List<PointOfServiceDistanceData> resultList = new ArrayList<>();

        for (final TargetPointOfServiceModel pointOfService : pointOfServiceList) {
            final PointOfServiceDistanceData storeFinderResultData = new PointOfServiceDistanceData();
            storeFinderResultData.setPointOfService(pointOfService);

            try {
                storeFinderResultData.setDistanceKm(getDistanceForStore(deliveryLocation,
                        pointOfService));
                resultList.add(storeFinderResultData);
            }
            catch (final LocationServiceException exception) {
                LOG.debug(exception.getMessage());
            }
        }
        return resultList;
    }

    /**
     * Method to get the distance between the DeliveryAddress and TargetStore
     * 
     * @param referenceGps
     *            The delivery address location/gps
     * @param posModel
     *            The PointOfServiceModel
     * 
     * @throws GeoLocatorException
     *             throws geoLocationException
     * @throws LocationServiceException
     *             throws LocationServiceException
     * 
     * @return distance
     */
    private double getDistanceForStore(final GPS referenceGps, final TargetPointOfServiceModel posModel)
            throws GeoLocatorException, LocationServiceException {
        if ((null != posModel.getLatitude()) && (null != posModel.getLongitude())) {
            final GPS positionGPS = new DefaultGPS(posModel.getLatitude().doubleValue(),
                    posModel.getLongitude().doubleValue());
            return GeometryUtils.getElipticalDistanceKM(referenceGps, positionGPS);
        }
        throw new LocationServiceException(
                "Unable to calculate a distance for PointOfService(" + posModel.getDisplayName()
                        + ") due to missing  latitude, longitude value");
    }

    /**
     * @param geoWebServiceWrapper
     *            the geoWebServiceWrapper to set
     */
    @Required
    public void setGeoWebServiceWrapper(final GeoWebServiceWrapper geoWebServiceWrapper) {
        this.geoWebServiceWrapper = geoWebServiceWrapper;
    }

}
