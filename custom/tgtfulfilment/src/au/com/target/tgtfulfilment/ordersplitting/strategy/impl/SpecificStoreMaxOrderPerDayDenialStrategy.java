/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * @author smishra1
 *
 */
public class SpecificStoreMaxOrderPerDayDenialStrategy extends BaseSpecificStoreFulfilmentDenialStrategy {

    @Override
    public DenialResponse isDenied(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos) {
        Assert.notNull(oeg, "oeg must not be null");

        if (getStoreFulfilmentCapabilitiesService().isMaxNumberOfOrdersExceeded(tpos)) {
            return DenialResponse.createDenied("MaxNumberOfOrdersForTheDayExceededForTheStore");
        }

        return DenialResponse.createNotDenied();
    }

}
