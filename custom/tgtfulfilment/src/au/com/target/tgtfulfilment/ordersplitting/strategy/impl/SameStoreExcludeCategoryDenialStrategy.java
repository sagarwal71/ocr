/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.FilterExclusionListService;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;


/**
 * @author Vivek
 *
 */
public class SameStoreExcludeCategoryDenialStrategy extends BaseSpecificStoreFulfilmentDenialStrategy {

    private TargetStoreFulfilmentCapabilitiesService storeFulfilmentCapabilitiesService;

    private FilterExclusionListService filterExclusionListService;

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.ordersplitting.strategy.SpecificStoreFulfilmentDenialStrategy#isDenied(de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup, au.com.target.tgtcore.model.TargetPointOfServiceModel)
     */
    @Override
    public DenialResponse isDenied(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos) {

        final String orderCode = getOegParameterHelper().getOrderCode(oeg);
        Assert.notEmpty(oeg, "orderEntryGroup cannot be empty " + orderCode);
        Assert.notNull(tpos, "targetPointOfService cannot be null " + orderCode);
        final List<CategoryModel> excludeCategories = getExcludedCategories(tpos);

        if (CollectionUtils.isNotEmpty(excludeCategories) && storeFulfilmentCapabilitiesService
                .isOrderContainingProductsFromExcludedCategories(oeg, excludeCategories)) {
            return DenialResponse.createDenied("SameStoreExcludeCategoryDenialStrategy");
        }
        return DenialResponse.createNotDenied();
    }

    /**
     * @return list of excluded categories
     */
    private List<CategoryModel> getExcludedCategories(final TargetPointOfServiceModel tpos) {
        final StoreFulfilmentCapabilitiesModel capabilitiesModel = tpos.getFulfilmentCapability();

        List<CategoryModel> excludeCategories = null;
        if (capabilitiesModel != null && capabilitiesModel.getCategoryExclusions() != null) {
            excludeCategories = filterExclusionListService.filterActiveCategoryModelsByDateRange(capabilitiesModel
                    .getCategoryExclusions());
        }
        return excludeCategories;
    }

    /**
     * @param storeFulfilmentCapabilitiesService
     *            the storeFulfilmentCapabilitiesService to set
     */
    @Override
    @Required
    public void setStoreFulfilmentCapabilitiesService(
            final TargetStoreFulfilmentCapabilitiesService storeFulfilmentCapabilitiesService) {
        this.storeFulfilmentCapabilitiesService = storeFulfilmentCapabilitiesService;
    }

    /**
     * @param filterExclusionListService
     *            the filterExclusionListService to set
     */
    @Required
    public void setFilterExclusionListService(final FilterExclusionListService filterExclusionListService) {
        this.filterExclusionListService = filterExclusionListService;
    }

}
