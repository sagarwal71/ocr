/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.junit.Assert;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * @author bhuang3
 *
 */
public class CncStoreDenialStrategy extends BaseSpecificStoreFulfilmentDenialStrategy {

    private static final Logger LOG = Logger.getLogger(CncStoreDenialStrategy.class);

    @Override
    public DenialResponse isDenied(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos) {
        AbstractOrderModel order = null;
        if (CollectionUtils.isNotEmpty(oeg)) {
            order = oeg.get(0).getOrder();
        }
        Assert.assertNotNull("Order cannot be null!", order);
        if (tpos == null || tpos.getStoreNumber() == null) {
            LOG.warn("Cant get store number : " + order.getCode());
            return DenialResponse.createDenied("CncStoreDenial: can't read store");
        }

        final Integer storeNumber = tpos.getStoreNumber();
        // Deny for same store Cnc since we should have already tried it in SameStoreCncSelectorStrategy
        final Integer cncStoreNumber = order.getCncStoreNumber();
        if (cncStoreNumber != null && (cncStoreNumber.compareTo(storeNumber) == 0)) {
            return DenialResponse.createDenied("AlreadyTriedForInstorePickup");
        }
        return DenialResponse.createNotDenied();
    }

}
