/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * Check if store is enabled for fulfilment
 * 
 * @author sbryan6
 *
 */
public class SpecificStoreEnabledFulfilmentDenialStrategy extends BaseSpecificStoreFulfilmentDenialStrategy {

    @Override
    public DenialResponse isDenied(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos) {

        if (getStoreFulfilmentCapabilitiesService().isStoreEnabledForFulfilment(tpos)) {
            return DenialResponse.createNotDenied();
        }

        return DenialResponse.createDenied("StoreNotCapableToFulfillOrder");
    }

}
