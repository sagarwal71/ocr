/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Collection;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * Filters stores that rejected to fulfill order.
 * 
 * @author jjayawa1
 *
 */
public class AlreadyTriedStoreDenialStrategy extends BaseSpecificStoreFulfilmentDenialStrategy {

    private static final Logger LOG = Logger.getLogger(AlreadyTriedStoreDenialStrategy.class);

    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Override
    public DenialResponse isDenied(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos) {

        AbstractOrderModel order = null;
        if (CollectionUtils.isNotEmpty(oeg)) {
            order = oeg.get(0).getOrder();
        }

        Assert.assertNotNull("Order cannot be null!", order);

        if (tpos == null || tpos.getStoreNumber() == null) {
            LOG.warn("Cant get store number : " + order.getCode());
            return DenialResponse.createDenied("AlreadyTriedStore: can't read store");
        }

        final Integer storeNumber = tpos.getStoreNumber();

        final Set<ConsignmentModel> consignments = order.getConsignments();
        if (consignments == null) {
            return DenialResponse.createNotDenied();
        }

        for (final ConsignmentModel consignment : consignments) {
            if (consignment.getStatus() == null) {
                LOG.warn("Consignment status for consignment : " + consignment.getCode() + " is null ");
                continue;
            }
            final WarehouseModel warehouse = consignment.getWarehouse();
            if (warehouse == null) {
                LOG.warn(" Consignment doesn't have a  warehouse ");
                continue;
            }
            final Collection<PointOfServiceModel> poses = warehouse.getPointsOfService();

            if (poses == null) {
                LOG.warn(" Warehouse doesn't have point of services ");
                continue;
            }

            // Ignore the consignment status check for falcon because we support short pick from store
            if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)) {
                if (this.checkTriedTops(poses, storeNumber)) {
                    return DenialResponse.createDenied("AlreadyTriedStore");
                }
            }
            else {
                if (consignment.getStatus().equals(ConsignmentStatus.CANCELLED)
                        && this.checkTriedTops(poses, storeNumber)) {
                    return DenialResponse.createDenied("AlreadyTriedStore");
                }
            }

        }
        return DenialResponse.createNotDenied();
    }

    private boolean checkTriedTops(final Collection<PointOfServiceModel> poses, final Integer storeNumber) {
        boolean denialResponse = false;
        for (final PointOfServiceModel pos : poses) {
            if (comparePOS(pos, storeNumber)) {
                denialResponse = true;
                break;
            }
        }
        return denialResponse;
    }

    private final boolean comparePOS(final PointOfServiceModel pos1, final Integer storeNumber) {

        if (pos1 != null && (pos1 instanceof TargetPointOfServiceModel)) {

            final TargetPointOfServiceModel tpos1 = (TargetPointOfServiceModel)pos1;

            if (tpos1.getStoreNumber() != null && (tpos1.getStoreNumber().compareTo(storeNumber) == 0)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }


}
