/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.logger.RoutingLogger;
import au.com.target.tgtfulfilment.ordersplitting.strategy.SpecificStoreFulfilmentDenialStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.WarehouseSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.SplitOegResult;
import au.com.target.tgtsale.stock.service.TargetStoreStockService;


/**
 * 
 * @author bhuang3
 *
 */
public class SameStoreCncSplitConsignmentStrategy implements WarehouseSelectorStrategy {

    private static final RoutingLogger ROUTING_LOG = new RoutingLogger(SameStoreCncSplitConsignmentStrategy.class);

    private OEGParameterHelper oegParameterHelper;

    private TargetPointOfServiceService targetPointOfServiceService;

    private List<SpecificStoreFulfilmentDenialStrategy> specificStoreFulfilmentDenialStrategies;

    private TargetStoreStockService targetStoreStockService;

    @Override
    public SplitOegResult split(final OrderEntryGroup oeg) {
        Assert.notEmpty(oeg, "OEG cannot be empty");
        final String orderCode = oegParameterHelper.getOrderCode(oeg);
        final Integer cncStoreNumber = oegParameterHelper.getCncStoreNumber(oeg);
        TargetPointOfServiceModel tpos = null;
        final SplitOegResult result = new SplitOegResult();
        final List<OrderEntryGroup> assignedOEGs = new ArrayList<>();
        final OrderEntryGroup assignedToStore = new OrderEntryGroup();
        oegParameterHelper.setAssignedQty(assignedToStore, new HashMap<String, Long>());
        OrderEntryGroup notAssignedToStore = new OrderEntryGroup();
        oegParameterHelper.setRequiredQty(notAssignedToStore, new HashMap<String, Long>());
        if (cncStoreNumber == null) {
            ROUTING_LOG.logSameStoreCncRulesSkipMessage(orderCode, "NotCncOrder");
            notAssignedToStore = oeg;
        }
        else {
            tpos = lookupTargetPointOfService(cncStoreNumber);
            if (null == tpos) {
                ROUTING_LOG.logSameStoreCncRulesSkipMessage(orderCode, "TPOSNotFound");
                notAssignedToStore = oeg;
            }
            else {
                final OrderEntryGroup restOfOeg = this.splitOegByDenialStrategies(tpos, oeg,
                        notAssignedToStore);
                if (CollectionUtils.isNotEmpty(restOfOeg)) {
                    this.splitOegByStoreStock(tpos, restOfOeg, assignedToStore, notAssignedToStore);
                }

            }

        }
        if (CollectionUtils.isNotEmpty(assignedToStore)) {
            oegParameterHelper.assignTposToOEG(assignedToStore, tpos);
            oegParameterHelper.populateParamsFromOrder(assignedToStore);
            assignedOEGs.add(assignedToStore);
        }
        oegParameterHelper.populateParamsFromOrder(notAssignedToStore);

        result.setAssignedOEGsToWarehouse(assignedOEGs);
        result.setNotAssignedToWarehouse(notAssignedToStore);
        final String deliveryModeCode = oegParameterHelper.getDeliveryMode(oeg) != null ? oegParameterHelper
                .getDeliveryMode(oeg).getCode() : StringUtils.EMPTY;
        final String cncStoreNumberCode = cncStoreNumber != null ? oegParameterHelper
                .getCncStoreNumber(oeg).toString() : StringUtils.EMPTY;
        ROUTING_LOG.logSplitOegResult(orderCode, deliveryModeCode, cncStoreNumberCode,
                "SameStoreCncSplitConsignmentStrategy",
                result.getAssignedOEGsToWarehouse(), result.getNotAssignedToWarehouse());
        return result;
    }

    /**
     * split the OEG by store stock
     * 
     * @param tpos
     * @param oeg
     * @param assignedToStore
     * @param notAssignedToStore
     */
    protected void splitOegByStoreStock(final TargetPointOfServiceModel tpos,
            final OrderEntryGroup oeg,
            final OrderEntryGroup assignedToStore, final OrderEntryGroup notAssignedToStore) {
        final Map<String, Long> requiredQtyMap = oegParameterHelper.getRequiredQty(oeg);
        final Map<String, Long> sohMap = targetStoreStockService.fetchProductsSohInStore(tpos,
                getProductsFromOrderEntries(oeg));
        final Map<String, Long> requiredQtyForNonAssignedOegMap = oegParameterHelper.getRequiredQty(notAssignedToStore);
        final Map<String, Long> assignedQtyMap = oegParameterHelper.getAssignedQty(assignedToStore);
        for (final AbstractOrderEntryModel entry : oeg) {
            if (entry.getProduct() != null) {
                final String productCode = entry.getProduct().getCode();
                final Long requiredQty = requiredQtyMap.get(productCode);
                final Long sohQty = sohMap.get(productCode);
                if (requiredQty == null || requiredQty.longValue() <= 0 || sohQty == null || sohQty.longValue() <= 0) {
                    notAssignedToStore.add(entry);
                    requiredQtyForNonAssignedOegMap.put(productCode, requiredQty);
                }
                else {
                    final long diff = requiredQty.longValue() - sohQty.longValue();
                    if (diff <= 0) {
                        assignedToStore.add(entry);
                        assignedQtyMap.put(productCode, requiredQty);
                    }
                    else {
                        // split the order entry into 2, partial assigned and partial not
                        notAssignedToStore.add(entry);
                        requiredQtyForNonAssignedOegMap.put(productCode, Long.valueOf(diff));
                        assignedToStore.add(entry);
                        assignedQtyMap.put(productCode, sohQty);
                    }
                }
            }

        }

    }

    private List<String> getProductsFromOrderEntries(final OrderEntryGroup oeg) {
        final List<String> products = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(oeg)) {
            for (final AbstractOrderEntryModel entry : oeg) {
                if (entry.getProduct() != null) {
                    products.add(entry.getProduct().getCode());
                }
            }
        }
        return products;
    }

    private OrderEntryGroup createOrderEntryGroup(final AbstractOrderEntryModel entry) {
        final OrderEntryGroup orderEntryGroup = new OrderEntryGroup();
        orderEntryGroup.add(entry);
        oegParameterHelper.populateParamsFromOrder(orderEntryGroup);
        return orderEntryGroup;
    }

    /**
     * filter out the entries not satisfied deny strategies.
     * 
     * @param tpos
     * @param oeg
     * @param notAssignedToStore
     * @return restOfOeg
     */
    protected OrderEntryGroup splitOegByDenialStrategies(final TargetPointOfServiceModel tpos,
            final OrderEntryGroup oeg,
            final OrderEntryGroup notAssignedToStore) {
        final OrderEntryGroup validOeg = new OrderEntryGroup();
        final Map<String, Long> requiredQtyForRestOfOeg = new HashMap<>();
        oegParameterHelper.setRequiredQty(validOeg, requiredQtyForRestOfOeg);
        final Map<String, Long> requiredQtyForNotAssignedMap = oegParameterHelper.getRequiredQty(notAssignedToStore);
        final Map<String, Long> requiredQtyMap = oegParameterHelper.getRequiredQty(oeg);
        for (final AbstractOrderEntryModel entry : oeg) {
            final String product = entry.getProduct().getCode();
            final OrderEntryGroup singleEntry = createOrderEntryGroup(entry);
            if (!this.checkByDenialStrategies(singleEntry, tpos)) {
                notAssignedToStore.add(entry);
                requiredQtyForNotAssignedMap.put(product, requiredQtyMap.get(product));
            }
            else {
                validOeg.add(entry);
                requiredQtyForRestOfOeg.put(product, requiredQtyMap.get(product));
            }
        }
        return validOeg;
    }

    /**
     * check all the denial strategies except stock check
     * 
     * @param oeg
     * @param tpos
     * @return true if pass all checks
     */
    protected boolean checkByDenialStrategies(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos) {
        return checkBySpecificStoreFulfilmentDenialStrategy(oeg, tpos, specificStoreFulfilmentDenialStrategies,
                "SameStoreCNCRulesExit");
    }

    protected boolean checkBySpecificStoreFulfilmentDenialStrategy(final OrderEntryGroup oeg,
            final TargetPointOfServiceModel tpos, final List<SpecificStoreFulfilmentDenialStrategy> strategies,
            final String action) {
        if (CollectionUtils.isNotEmpty(strategies)) {
            final String orderCode = getOegParameterHelper().getOrderCode(oeg);
            for (final SpecificStoreFulfilmentDenialStrategy denialStrategy : strategies) {
                final DenialResponse denialResponse = denialStrategy.isDenied(oeg, tpos);
                if (denialResponse.isDenied()) {
                    ROUTING_LOG.logStoreRulesExcludedMessage(action, orderCode,
                            denialResponse.getReason(), getStoreNumberForLogging(tpos), getStoreStateForLogging(tpos));
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Lookup target point of service.
     *
     * @param storeNumber
     *            the store number
     * @return the target point of service model
     */
    private TargetPointOfServiceModel lookupTargetPointOfService(final Integer storeNumber) {
        try {
            return targetPointOfServiceService.getPOSByStoreNumber(storeNumber);
        }
        catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException ex) {
            return null;
        }
    }

    /**
     * get store number from tpos
     * 
     * @param tpos
     * @return store number
     */
    protected String getStoreNumberForLogging(final TargetPointOfServiceModel tpos) {
        String storeNum = StringUtils.EMPTY;
        if (tpos != null && tpos.getStoreNumber() != null) {
            storeNum = tpos.getStoreNumber().toString();
        }
        return storeNum;
    }

    /**
     * get store state from tpos
     * 
     * @param tpos
     * @return store number
     */
    protected String getStoreStateForLogging(final TargetPointOfServiceModel tpos) {
        String storeState = StringUtils.EMPTY;
        if (tpos != null && tpos.getAddress() != null) {
            storeState = tpos.getAddress().getDistrict();
        }
        return storeState;
    }

    /**
     * @param oegParameterHelper
     *            the oegParameterHelper to set
     */
    @Required
    public void setOegParameterHelper(final OEGParameterHelper oegParameterHelper) {
        this.oegParameterHelper = oegParameterHelper;
    }

    /**
     * @param targetPointOfServiceService
     *            the targetPointOfServiceService to set
     */
    @Required
    public void setTargetPointOfServiceService(final TargetPointOfServiceService targetPointOfServiceService) {
        this.targetPointOfServiceService = targetPointOfServiceService;
    }


    /**
     * @param specificStoreFulfilmentDenialStrategies
     *            the specificStoreFulfilmentDenialStrategies to set
     */
    @Required
    public void setSpecificStoreFulfilmentDenialStrategies(
            final List<SpecificStoreFulfilmentDenialStrategy> specificStoreFulfilmentDenialStrategies) {
        this.specificStoreFulfilmentDenialStrategies = specificStoreFulfilmentDenialStrategies;
    }


    @Required
    public void setTargetStoreStockService(final TargetStoreStockService targetStoreStockService) {
        this.targetStoreStockService = targetStoreStockService;
    }

    protected OEGParameterHelper getOegParameterHelper() {
        return oegParameterHelper;
    }

    protected TargetPointOfServiceService getTargetPointOfServiceService() {
        return targetPointOfServiceService;
    }

    protected TargetStoreStockService getTargetStoreStockService() {
        return targetStoreStockService;
    }

}
