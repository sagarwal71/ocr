/**
 * 
 */
package au.com.target.tgtfulfilment.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfulfilment.manifest.service.TransmitManifestService;


/**
 * This cron job will determine the manifests that have failed transmission and attempt to retransmit
 * 
 * @author rsamuel3
 *
 */
public class RetransmitManifestsJob extends AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(RetransmitManifestsJob.class);


    private TransmitManifestService transmitManifestService;

    /**
     * (non-Javadoc)
     * 
     * @see de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.CronJobModel)
     */
    @Override
    public PerformResult perform(final CronJobModel paramT) {
        LOG.info("Starting the cron job to retrieve manifests not transmitted");
        transmitManifestService.reTransmitManifests();
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    /**
     * @param transmitManifestService
     *            the transmitManifestService to set
     */
    @Required
    public void setTransmitManifestService(final TransmitManifestService transmitManifestService) {
        this.transmitManifestService = transmitManifestService;
    }



}
