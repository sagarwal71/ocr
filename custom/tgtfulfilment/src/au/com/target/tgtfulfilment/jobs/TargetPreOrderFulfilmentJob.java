/**
 * 
 */
package au.com.target.tgtfulfilment.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.text.MessageFormat;

import org.apache.log4j.Logger;

import au.com.target.tgtfulfilment.service.TargetPreOrderFulfilmentService;


/**
 * @author jhermoso
 *
 */
public class TargetPreOrderFulfilmentJob extends AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(TargetPreOrderFulfilmentJob.class);

    private static final String TARGET_PREORDER_FULFILMENT_JOB_ERROR = "TARGET_PREORDER_FULFILMENT_JOB_ERROR_MESSAGE: message={0}, result={1}, details={2}";

    private TargetPreOrderFulfilmentService targetPreOrderFulfilmentService;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.CronJobModel)
     */
    @Override
    public PerformResult perform(final CronJobModel cronJob) {
        LOG.info("Initializing the Cronjob for Pre-order Fulfilment");
        try {
            // trigger business process for orders by calling a service
            targetPreOrderFulfilmentService.processPreOrderFulfilment();
        }
        catch (final Exception e) {
            LOG.error(MessageFormat.format(TARGET_PREORDER_FULFILMENT_JOB_ERROR,
                    "Failed to perform TargetPreOrderFulfilmentJob job", CronJobResult.ERROR, e.getMessage()), e);

            return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
        }

        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    /**
     * @param targetPreOrderFulfilmentService
     *            the targetPreOrderFulfilmentService to set
     */
    public void setTargetPreOrderFulfilmentService(
            final TargetPreOrderFulfilmentService targetPreOrderFulfilmentService) {
        this.targetPreOrderFulfilmentService = targetPreOrderFulfilmentService;
    }


}
