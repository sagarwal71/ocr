/**
 * 
 */
package au.com.target.tgtfulfilment.jobs;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.dao.TargetConsignmentDao;


/**
 * @author rmcalave
 * 
 */
public class ResendNotAcknowledgedOrderExtractsJob extends AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(ResendNotAcknowledgedOrderExtractsJob.class);

    private static final String LOG_PREFIX = "ResendConsignmentsJob: ";

    private TargetBusinessProcessService targetBusinessProcessService;

    private TargetConsignmentDao targetConsignmentDao;

    @Override
    public PerformResult perform(final CronJobModel cronJob) {
        LOG.info(LOG_PREFIX + "Starting job");

        boolean successFastline = false;
        boolean successExternal = false;

        try {
            // Ones which have been sent but no acknowledgement received (eg fastline)
            successFastline = resendConsignments(
                    targetConsignmentDao.getAllConsignmentsNotAcknowlegedWithinTimeLimit());

            // Ones which failed to send at all (eg Incomm)
            successExternal = resendConsignments(targetConsignmentDao
                    .getAllExternalConsignmentsInCreatedStateWithinTimeLimit());
        }
        catch (final Exception e) {
            LOG.error(LOG_PREFIX + " Unhandled exception occurred, aborting", e);
            return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
        }

        CronJobResult result = CronJobResult.SUCCESS;
        if (!successFastline) {
            LOG.error(LOG_PREFIX + " Errors found in resend consignments to fastline");
            result = CronJobResult.ERROR;
        }
        if (!successExternal) {
            LOG.error(LOG_PREFIX + " Errors found in resend consignments to external warehouse");
            result = CronJobResult.ERROR;
        }

        return new PerformResult(result, CronJobStatus.FINISHED);
    }


    private boolean resendConsignments(final List<TargetConsignmentModel> consignmentsToResend) {
        boolean success = true;

        if (CollectionUtils.isNotEmpty(consignmentsToResend)) {

            for (final TargetConsignmentModel consignment : consignmentsToResend) {

                if (null == consignment.getOrder()) {
                    LOG.error(LOG_PREFIX + " Skipping resending consignment=" + consignment.getCode()
                            + ", as order is missing.");
                    continue;
                }

                if (OrderStatus.CANCELLED.equals(consignment.getOrder().getStatus())) {
                    LOG.warn(LOG_PREFIX + " Skipping resending consignment=" + consignment.getCode()
                            + ", as order is cancelled.");
                    continue;
                }

                if (StringUtils.isNotBlank(consignment.getOrder().getFluentId())) {
                    LOG.warn(LOG_PREFIX + " Skipping resending consignment=" + consignment.getCode()
                            + ", as order has fluentId.");
                    continue;
                }

                try {
                    targetBusinessProcessService.resendConsignmentProcess(consignment);
                }
                catch (final Exception e) {
                    LOG.error(LOG_PREFIX + " Error in resend, consignment=" + consignment.getCode(), e);
                    success = false;
                }
            }
        }

        return success;
    }

    /**
     * @param targetBusinessProcessService
     *            the targetBusinessProcessService to set
     */
    @Required
    public void setTargetBusinessProcessService(final TargetBusinessProcessService targetBusinessProcessService) {
        this.targetBusinessProcessService = targetBusinessProcessService;
    }

    /**
     * @param targetConsignmentDao
     *            the targetConsignmentDao to set
     */
    @Required
    public void setTargetConsignmentDao(final TargetConsignmentDao targetConsignmentDao) {
        this.targetConsignmentDao = targetConsignmentDao;
    }
}
