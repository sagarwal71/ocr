/**
 * 
 */
package au.com.target.tgtfulfilment.jobs;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseResponse;
import au.com.target.tgtfulfilment.service.SendToWarehouseService;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;
import au.com.target.tgtutility.logging.JsonLogger;


/**
 * @author mjanarth
 *
 */
public class ExtractUnShippedConsignmentsJob extends AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(ExtractUnShippedConsignmentsJob.class);
    private static final String LOG_PREFIX = "ExtractUnShippedConsignmentsJob: ";
    private static final String EXTRACT_UNSHIPPED_CONSIGNMENT_CRON_JOB_FAILED = "EXTRACT_UNSHIPPED_CONSIGNMENT_CRON_JOB_FAILED";

    private TargetConsignmentService targetConsignmentService;
    private SendToWarehouseService sendToWarehouseService;
    private List<String> warehouseCodes;


    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.CronJobModel)
     */
    @Override
    public PerformResult perform(final CronJobModel cronJob) {
        LOG.info(LOG_PREFIX + "Start executing the job");
        if (CollectionUtils.isEmpty(warehouseCodes)) {
            LOG.warn(LOG_PREFIX + "Warehouse code can't be empty");
            return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
        }
        for (final String warehouseCode : warehouseCodes) {
            try {
                final PerformResult result = sendUnshippedConsignments(warehouseCode);
                if (result != null) {
                    LOG.info(LOG_PREFIX + "Finished executing the job");
                    return result;
                }
            }
            catch (final NotFoundException e) {
                LOG.info(LOG_PREFIX + "No Consignments found for wareshouse:" + warehouseCodes);
            }
        }
        LOG.info(LOG_PREFIX + "Finished executing the job");
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    /**
     * @param warehouseCode
     * @throws NotFoundException
     */
    private PerformResult sendUnshippedConsignments(final String warehouseCode) throws NotFoundException {
        final List<TargetConsignmentModel> consignmentList = targetConsignmentService
                .getAllConsignmentByStatusAndWarehouse(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, warehouseCode);
        if (CollectionUtils.isEmpty(consignmentList)) {
            LOG.info(LOG_PREFIX + "No Consignments found for wareshouse:" + warehouseCodes);
            return null;
        }
        else {
            final PollWarehouseResponse pollWarehouseResponse = sendToWarehouseService
                    .sendUnshippedConsignemnts(consignmentList);
            if (!pollWarehouseResponse.isSuccess()) {
                LOG.info(LOG_PREFIX + EXTRACT_UNSHIPPED_CONSIGNMENT_CRON_JOB_FAILED);
                JsonLogger.logJson(LOG, pollWarehouseResponse);
                // if any error/exception while calling consignment rest call.
                return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
            }
        }
        return null;
    }

    /**
     * @param targetConsignmentService
     *            the targetConsignmentService to set
     */
    @Required
    public void setTargetConsignmentService(final TargetConsignmentService targetConsignmentService) {
        this.targetConsignmentService = targetConsignmentService;
    }

    /**
     * @param warehouseCodes
     *            the warehouseCodes to set
     */
    public void setWarehouseCodes(final List<String> warehouseCodes) {
        this.warehouseCodes = warehouseCodes;
    }

    /**
     * @param sendToWarehouseService
     *            the sendToWarehouseService to set
     */
    public void setSendToWarehouseService(final SendToWarehouseService sendToWarehouseService) {
        this.sendToWarehouseService = sendToWarehouseService;
    }

}
