/**
 * 
 */
package au.com.target.tgtfulfilment.stock;

import de.hybris.platform.ordersplitting.model.WarehouseModel;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;

/**
 * @author Rahul
 *
 */
public interface TargetFulfilmentStockService {

    /**
     * Reconciles the stock level's reserved quantity with the given Product Variant model.
     * 
     * @param productVariant
     * @param warehouseModel
     */
    void reconcileStockLevelReservedQuantity(final AbstractTargetVariantProductModel productVariant,
            final WarehouseModel warehouseModel);

}
