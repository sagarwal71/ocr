/**
 * 
 */
package au.com.target.tgtfulfilment.stock.impl;

import de.hybris.platform.basecommerce.enums.StockLevelUpdateType;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.stock.impl.TargetStockServiceImpl;
import au.com.target.tgtfulfilment.stock.TargetFulfilmentStockService;
import au.com.target.tgtfulfilment.stock.dao.TargetFulfilmentStockLevelDao;


/**
 * @author Rahul
 * 
 */
public class TargetFulfilmentStockServiceImpl extends TargetStockServiceImpl implements
        TargetFulfilmentStockService {

    private static final Logger LOG = Logger.getLogger(TargetFulfilmentStockServiceImpl.class);

    private TargetFulfilmentStockLevelDao targetFulfilmentStockLevelDao;

    @Override
    public void reconcileStockLevelReservedQuantity(final AbstractTargetVariantProductModel productVariant,
            final WarehouseModel warehouseModel) {

        if (productVariant == null || warehouseModel == null) {
            return;
        }

        final StockLevelModel slm = getStockLevel(productVariant, warehouseModel);
        if (slm == null) {
            return;
        }

        final int reconciledQty = (int)targetFulfilmentStockLevelDao.calculateReservedStockAdjustment(productVariant,
                warehouseModel);

        if (reconciledQty < 0) {
            final int absQty = Math.abs(reconciledQty);
            LOG.warn("Reserve stock level for " + productVariant.getCode() + " is OVER by " + absQty);
            targetFulfilmentStockLevelDao.release(slm, absQty);
            clearCacheForItem(slm);
            createStockLevelHistoryEntry(slm, StockLevelUpdateType.CUSTOMER_RELEASE, slm.getReserved(),
                    "reconcils stock job released " + absQty + " in reserved stock");
        }
        else if (reconciledQty > 0) {
            LOG.warn("Reserve stock level for " + productVariant.getCode() + " is UNDER by " + reconciledQty);
            targetFulfilmentStockLevelDao.reserve(slm, reconciledQty);
            clearCacheForItem(slm);
            createStockLevelHistoryEntry(slm, StockLevelUpdateType.CUSTOMER_RESERVE, slm.getReserved(),
                    "reconcils stock job reserved " + reconciledQty + " in reserved stock");
        }

    }

    /**
     * @param targetFulfilmentStockLevelDao
     *            the targetFulfilmentStockLevelDao to set
     */
    @Required
    public void setTargetFulfilmentStockLevelDao(final TargetFulfilmentStockLevelDao targetFulfilmentStockLevelDao) {
        this.targetFulfilmentStockLevelDao = targetFulfilmentStockLevelDao;
    }

}
