/**
 * 
 */
package au.com.target.tgtfulfilment.dao;

import de.hybris.platform.servicelayer.internal.dao.GenericDao;

import java.util.List;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.model.TargetManifestModel;


/**
 * The Interface TargetManifestDao.
 *
 * @author Rahul
 */
public interface TargetManifestDao extends GenericDao<TargetManifestModel> {

    /**
     * Gets the manifest by code.
     *
     * @param manifestCode
     *            the manifest code
     * @return the manifest by code
     */
    TargetManifestModel getManifestByCode(final String manifestCode)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;

    /**
     * Gets the manifests history for a given store.
     *
     * @param store
     *            the store
     * @return the manifests history by date
     */
    List<TargetManifestModel> getManifestsHistoryForStore(final TargetPointOfServiceModel store);

    /**
     * @return the list of not transmitted manifest
     */
    List<TargetManifestModel> getNotTransmittedManifests();
}
