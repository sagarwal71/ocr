/**
 * 
 */
package au.com.target.tgtfulfilment.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfulfilment.dao.TargetStandardParcelDao;
import au.com.target.tgtfulfilment.model.TargetStandardParcelModel;


/**
 * @author smishra1
 *
 */
public class TargetStandardParcelDaoImpl implements TargetStandardParcelDao {

    private static final String GET_STANDARD_PARCELS = "SELECT {" + TargetStandardParcelModel.PK + "} FROM {"
            + TargetStandardParcelModel._TYPECODE + "} " + "WHERE {" + TargetStandardParcelModel.ENABLED
            + "} = ?enabled" + " ORDER BY {" + TargetStandardParcelModel.ORDER + "} ASC";

    private FlexibleSearchService flexibleSearchService;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TargetStandardParcelModel> getEnabledTargetStandardParcels() {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_STANDARD_PARCELS);
        query.addQueryParameter("enabled", Boolean.TRUE);
        final SearchResult<TargetStandardParcelModel> standardParcelModelList = flexibleSearchService.search(query);
        if (null != standardParcelModelList && CollectionUtils.isNotEmpty(standardParcelModelList.getResult())) {
            return standardParcelModelList.getResult();
        }
        return null;
    }

    /**
     * Set the FlexibleSearchService The Flexible service to set
     * 
     * @param flexibleSearchService
     *            The flexibleSearchService
     */
    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }
}
