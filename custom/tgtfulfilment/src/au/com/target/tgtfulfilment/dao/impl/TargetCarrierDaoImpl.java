/**
 * 
 */
package au.com.target.tgtfulfilment.dao.impl;

import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.util.TargetServicesUtil;
import au.com.target.tgtfulfilment.dao.TargetCarrierDao;
import au.com.target.tgtfulfilment.dao.util.TargetFulfilmentDaoUtil;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;


/**
 * @author dcwillia
 * 
 */
public class TargetCarrierDaoImpl extends DefaultGenericDao<TargetCarrierModel>implements TargetCarrierDao {


    private static final String FIND_APPLICABLE_CARRIERS = "SELECT {" + TargetCarrierModel.PK + "} FROM {"
            + TargetCarrierModel._TYPECODE + "} WHERE {" + TargetCarrierModel.ENABLED + "} = ?enabled and {"
            + TargetCarrierModel.DELIVERYMODE + "} = ?deliverymode";

    private static final String BULKY_CONDITION = " and {" + TargetCarrierModel.ALLOWBULKY + "} = ?allowbulky";

    private static final String NULL_CARRIER_CONDITION = " and {" + TargetCarrierModel.NULLCARRIER + "} = ?nullCarrier";

    private static final String INSTORE_CARRIER_CONDITION = " and {" + TargetCarrierModel.ENABLEDFORINSTORE
            + "} = ?enabledInStore";

    private static final String ORDER_BY_CLAUSE = " order by {" + TargetCarrierModel.PRIORITY + "} asc";


    private FlexibleSearchService flexibleSearchService;

    private TargetFulfilmentDaoUtil targetFulfilmentDaoUtil;

    public TargetCarrierDaoImpl() {
        super(TargetCarrierModel._TYPECODE);
    }

    @Override
    public List<TargetCarrierModel> getApplicableCarriers(final TargetZoneDeliveryModeModel deliveryMode,
            final boolean requireBulky, final boolean enabledInStore) {
        Assert.notNull(deliveryMode, "delivery mode must not be null");

        final FlexibleSearchQuery query;

        if (requireBulky) {
            query = new FlexibleSearchQuery(FIND_APPLICABLE_CARRIERS + BULKY_CONDITION + NULL_CARRIER_CONDITION
                    + INSTORE_CARRIER_CONDITION
                    + ORDER_BY_CLAUSE);
            query.addQueryParameter("allowbulky", Boolean.TRUE);
        }
        else {
            query = new FlexibleSearchQuery(FIND_APPLICABLE_CARRIERS + NULL_CARRIER_CONDITION
                    + INSTORE_CARRIER_CONDITION
                    + ORDER_BY_CLAUSE);
        }

        query.addQueryParameter("enabled", Boolean.TRUE);
        query.addQueryParameter("deliverymode", deliveryMode);
        query.addQueryParameter("nullCarrier", Boolean.FALSE);
        query.addQueryParameter("enabledInStore", Boolean.valueOf(enabledInStore));

        final SearchResult<TargetCarrierModel> searchResult = flexibleSearchService.search(query);
        final List result = searchResult.getResult();

        if (result.isEmpty()) {
            throw new ModelNotFoundException(MessageFormat.format(
                    "Couldn't find any valid Carrier for deliveryMode: {0}, requireBulky: {1} and enabledInStore: {2}",
                    deliveryMode.getCode(), Boolean.valueOf(requireBulky), Boolean.valueOf(enabledInStore)));

        }
        return result;
    }

    @Override
    public TargetCarrierModel getNullCarrier(final TargetZoneDeliveryModeModel deliveryMode) {

        Assert.notNull(deliveryMode, "delivery mode must not be null");

        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_APPLICABLE_CARRIERS + NULL_CARRIER_CONDITION
                + ORDER_BY_CLAUSE);

        query.addQueryParameter("enabled", Boolean.TRUE);
        query.addQueryParameter("deliverymode", deliveryMode);
        query.addQueryParameter("nullCarrier", Boolean.TRUE);
        query.setCount(1);

        return flexibleSearchService.searchUnique(query);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfluent.dao.FluentCarrierDao#getTargetCarrierByCode(java.lang.String)
     */
    @Override
    public TargetCarrierModel getTargetCarrierByCode(final String carrierCode)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        final List<TargetCarrierModel> models = find(getParametersMap(carrierCode));
        TargetServicesUtil.validateIfSingleResult(models, TargetCarrierModel.class,
                TargetCarrierModel.CODE, carrierCode);
        final TargetCarrierModel targetCarrierModel = models.get(0);

        targetFulfilmentDaoUtil.refreshModel(targetCarrierModel);
        return targetCarrierModel;
    }

    /**
     * @param carrierCode
     * @return Map with parameters carrier code and enabled to be true
     */
    private Map<String, Object> getParametersMap(final String carrierCode) {
        final Map<String, Object> parametersMap = new HashMap();
        parametersMap.put(TargetCarrierModel.CODE, carrierCode);
        parametersMap.put(TargetCarrierModel.ENABLED, Boolean.TRUE);
        return parametersMap;
    }

    @Override
    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    /**
     * @param targetFulfilmentDaoUtil
     *            the targetFulfilmentDaoUtil to set
     */
    @Required
    public void setTargetFulfilmentDaoUtil(final TargetFulfilmentDaoUtil targetFulfilmentDaoUtil) {
        this.targetFulfilmentDaoUtil = targetFulfilmentDaoUtil;
    }

}
