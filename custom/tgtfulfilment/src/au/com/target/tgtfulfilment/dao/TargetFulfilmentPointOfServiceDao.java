/**
 * 
 */
package au.com.target.tgtfulfilment.dao;

import de.hybris.platform.core.model.c2l.RegionModel;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.dao.TargetPointOfServiceDao;


/**
 * @author Nandini
 *
 */
public interface TargetFulfilmentPointOfServiceDao extends TargetPointOfServiceDao {

    /**
     * Return a single TargetPointOfServiceModel identified by region.
     * 
     * @param region
     *            the Region
     * 
     * @return TargetPointOfServiceModel
     */
    TargetPointOfServiceModel getNTLByRegion(RegionModel region);

}
