/**
 * 
 */
package au.com.target.tgtfulfilment.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.dbformatter.TargetDBSpecificQueryFactory;
import au.com.target.tgtcore.dbformatter.TargetDBSpecificQueryFormatter;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.util.TargetServicesUtil;
import au.com.target.tgtfulfilment.dao.TargetManifestDao;
import au.com.target.tgtfulfilment.dao.util.TargetFulfilmentDaoUtil;
import au.com.target.tgtfulfilment.model.TargetManifestModel;


/**
 * The Class TargetManifestDaoImpl.
 *
 * @author Rahul
 */
public class TargetManifestDaoImpl extends DefaultGenericDao<TargetManifestModel> implements TargetManifestDao {

    /** Query to fetch distinct manifests from consignments fulfilled by given store */
    private static final String FIND_MANIFESTS_HISTORY_FOR_STORE_QUERY = "SELECT DISTINCT {mf."
            + TargetManifestModel.PK + "}, {" + TargetManifestModel.DATE + "} FROM {" + TargetManifestModel._TYPECODE
            + " AS mf JOIN " + TargetConsignmentModel._TYPECODE + " AS con ON {con." + TargetConsignmentModel.MANIFEST
            + "} = {mf." + TargetManifestModel.PK + "} JOIN PoS2WarehouseRel AS pw ON {pw.target} = {con."
            + TargetConsignmentModel.WAREHOUSE + "} JOIN " + TargetPointOfServiceModel._TYPECODE + " AS tpos ON {tpos."
            + TargetPointOfServiceModel.PK + "} = {pw.source}} WHERE {tpos." + TargetPointOfServiceModel.PK
            + "} = ?store AND @@DATEDIFFQUERY@@ <= ?manifestHistoryDays ORDER BY {" + TargetManifestModel.DATE
            + "} DESC";

    private static final String FIND_NOT_SENT_MANIFESTS = "SELECT DISTINCT {mf." + TargetManifestModel.PK + "} FROM {"
            + TargetManifestModel._TYPECODE + " AS mf}  WHERE {mf." + TargetManifestModel.SENT + "} = 0";

    private int manifestHistoryDays;

    private TargetDBSpecificQueryFactory targetDBSpecificQueryFactory;

    private TargetFulfilmentDaoUtil targetFulfilmentDaoUtil;

    public TargetManifestDaoImpl() {
        super(TargetManifestModel._TYPECODE);
    }

    @Override
    public TargetManifestModel getManifestByCode(final String manifestCode) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final List<TargetManifestModel> models = find(Collections.singletonMap(TargetManifestModel.CODE, manifestCode));

        TargetServicesUtil.validateIfSingleResult(models, TargetManifestModel.class,
                TargetManifestModel.CODE, manifestCode);

        final TargetManifestModel targetManifestModel = models.get(0);
        targetFulfilmentDaoUtil.refreshModel(targetManifestModel);
        return targetManifestModel;
    }

    @Override
    public List<TargetManifestModel> getManifestsHistoryForStore(final TargetPointOfServiceModel store) {

        final String timeFormatDateDiffQuery = targetDBSpecificQueryFactory.getDBSpecificQueryFormatter()
                .formatDateDiff(TargetDBSpecificQueryFormatter.DAYS, "{" + TargetManifestModel.DATE + "}",
                        "?currentDate");

        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_MANIFESTS_HISTORY_FOR_STORE_QUERY.replace(
                "@@DATEDIFFQUERY@@", timeFormatDateDiffQuery));

        query.addQueryParameter("store", store);
        query.addQueryParameter("currentDate", Calendar.getInstance().getTime());
        query.addQueryParameter("manifestHistoryDays", Integer.valueOf(manifestHistoryDays));

        final SearchResult<TargetManifestModel> searchResult = getFlexibleSearchService().search(query);
        final List<TargetManifestModel> manifestList = searchResult.getResult();
        if (CollectionUtils.isNotEmpty(manifestList)) {
            targetFulfilmentDaoUtil.refreshModelList(manifestList);
        }
        return manifestList;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.dao.TargetManifestDao#getNotTransmittedManifests(au.com.target.tgtcore.model.TargetPointOfServiceModel)
     */
    @Override
    public List<TargetManifestModel> getNotTransmittedManifests() {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_NOT_SENT_MANIFESTS);
        final SearchResult<TargetManifestModel> searchResult = getFlexibleSearchService().search(query);
        return searchResult.getResult();
    }

    /**
     * @param manifestHistoryDays
     *            the manifestHistoryDays to set
     */
    @Required
    public void setManifestHistoryDays(final int manifestHistoryDays) {
        this.manifestHistoryDays = manifestHistoryDays;
    }

    /**
     * @param targetDBSpecificQueryFactory
     *            the targetDBSpecificQueryFactory to set
     */
    @Required
    public void setTargetDBSpecificQueryFactory(final TargetDBSpecificQueryFactory targetDBSpecificQueryFactory) {
        this.targetDBSpecificQueryFactory = targetDBSpecificQueryFactory;
    }

    /**
     * @param targetFulfilmentDaoUtil
     *            the targetFulfilmentDaoUtil to set
     */
    @Required
    public void setTargetFulfilmentDaoUtil(final TargetFulfilmentDaoUtil targetFulfilmentDaoUtil) {
        this.targetFulfilmentDaoUtil = targetFulfilmentDaoUtil;
    }

}
