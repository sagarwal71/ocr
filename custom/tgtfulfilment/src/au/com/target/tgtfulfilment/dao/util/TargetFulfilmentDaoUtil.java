/**
 * 
 */
package au.com.target.tgtfulfilment.dao.util;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author rsamuel3
 *
 */
public class TargetFulfilmentDaoUtil<T extends ItemModel> {

    private ModelService modelService;

    /**
     * When models are retrieved an update on the model is not invalidating the hybris cache for unknown reason. <br/>
     * Hence we are explicitly refreshing the list so the the model changes are reflected.
     *
     * @param models
     */
    public List<T> refreshModelList(final List<T> models) {

        if (CollectionUtils.isNotEmpty(models)) {
            for (final T model : models) {
                refreshModel(model);
            }
        }

        return models;
    }

    /**
     * Refreshing the model to invalidate the cache
     * 
     * @param model
     * @return model
     */
    public T refreshModel(final T model) {

        modelService.refresh(model);

        return model;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }
}
