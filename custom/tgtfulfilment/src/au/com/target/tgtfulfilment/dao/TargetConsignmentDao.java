package au.com.target.tgtfulfilment.dao;


import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.Date;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.dto.TargetConsignmentPageResult;



public interface TargetConsignmentDao {

    /**
     * Returns a list of consignments waiting for ship confirmation from the warehouse after sending ship advice beyond
     * the specified wait limit
     * 
     * @param shipConfirmWaitLimit
     *            - wait limit in hours
     * @return consignments
     */
    List<TargetConsignmentModel> getConsignmentsWaitingForShipConfirm(Integer shipConfirmWaitLimit);

    /**
     * Gets all the {@link TargetConsignmentModel}s that have the status {@link ConsignmentStatus#SENT_TO_WAREHOUSE} and
     * where the sentToWarehouseDate is more than minutes to wait configured as per the carrier.
     * 
     * @return A list of {@link TargetConsignmentModel}s that match the above criteria
     */
    List<TargetConsignmentModel> getAllConsignmentsNotAcknowlegedWithinTimeLimit();

    /**
     * Gets all External consignments in Created status - eg send to CNP failed
     * 
     * @return list of consignments
     */
    List<TargetConsignmentModel> getAllExternalConsignmentsInCreatedStateWithinTimeLimit();

    /**
     * @param consignmentCode
     *            consignment code
     * @return ConsignmentModel Consignment Model
     * @throws TargetUnknownIdentifierException
     *             TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     *             TargetAmbiguousIdentifierException
     */
    TargetConsignmentModel getConsignmentBycode(String consignmentCode) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException;

    /**
     * @param storeNumber
     *            Store number
     * @param offset
     *            offset value
     * @param recsPerPage
     *            records per page
     * @return targetConsignmentPageResult
     */
    TargetConsignmentPageResult getConsignmentsForStore(Integer storeNumber, final int offset,
            final int recsPerPage, int lastXDays);

    /**
     * @param consignmentCodes
     *            List of consignment codes
     * @return ConsignmentModels Consignment Models
     */
    List<TargetConsignmentModel> getConsignmentsByCodes(List<String> consignmentCodes);

    /**
     * Returns the list of Consignments based on store and consignment status
     * 
     * @param status
     * @return list of ConsignmentModels
     */
    List<TargetConsignmentModel> getConsignmentsByStatusForAllStore(ConsignmentStatus status);

    /**
     * Returns the list of Consignments with matching consignment status and within the given list of states.
     * 
     * @param status
     * @param includedStates
     * @return list of ConsignmentModels
     */
    List<TargetConsignmentModel> getConsignmentsByStatusForStoreInState(List<ConsignmentStatus> status,
            List<String> includedStates);

    /**
     * Returns the list of Consignments with matching consignment status and not within the given list of states.
     * 
     * @param status
     * @param excludedStates
     * @return list of ConsignmentModels
     */
    List<TargetConsignmentModel> getConsignmentsByStatusForStoreNotInState(List<ConsignmentStatus> status,
            List<String> excludedStates);

    /**
     * Method to get consignments by status for store sorted by pick date which are not to be collected from given
     * store.
     * 
     * @param status
     * @param storeNumber
     * @return ConsignmentModels Consignment Models
     */
    List<TargetConsignmentModel> getConsignmentsUnmanifestedInStore(ConsignmentStatus status,
            int storeNumber);

    /**
     * Gets the consignments not processed for store.
     *
     * @param store
     *            the store the no of days
     * @return the consignments not processed for store
     */
    List<TargetConsignmentModel> getConsignmentsNotProcessedForStore(final TargetPointOfServiceModel store);

    /**
     * Gets the consignments completed instore by days. Days will be calculated from current day i.e. 0 means today, 1
     * means yesterday and so on.
     *
     * @param store
     *            the store
     * @return the consignments completed by date
     */
    List<TargetConsignmentModel> getConsignmentsCompletedByDayForStore(final TargetPointOfServiceModel store,
            final int noOfDays);

    /**
     * Gets the consignments rejected instore by days. Days will be calculated from current day i.e. 0 means today, 1
     * means yesterday and so on.
     *
     * @param store
     *            the store
     * @return the consignments rejected by date
     */
    List<TargetConsignmentModel> getConsignmentsRejectedByDayForStore(final TargetPointOfServiceModel store,
            final int noOfDays);

    /**
     * Gets the consignments opened for store.
     *
     * @param store
     *            the store
     * @return the consignments opened for a store
     */
    List<TargetConsignmentModel> getOpenConsignmentsForStore(final TargetPointOfServiceModel store);

    /**
     * @param storeNumber
     *            Store number
     * @return ConsignmentModel Consigment model
     */
    List<TargetConsignmentModel> getConsignmentsForStore(Integer storeNumber);

    /**
     * @param storeNumber
     *            Store number
     * @param filterDate
     *            Date to fetch the later consignments
     * @return Count of all the filtered consignements
     */
    int getConsignmentCountForStoreByMaxCutoffPeriod(final Integer storeNumber,
            final Date filterDate);

    /**
     * Get a sum of quantities of this product that have been assigned to the store since the date
     * 
     * @param product
     * @param storeNumber
     * @param filterDate
     * @return int indicating the sum total of the SKU that is already sent to the store
     */
    int getQuantityAssignedToStoreForProduct(final Integer storeNumber,
            final AbstractTargetVariantProductModel product,
            final Date filterDate);


    /**
     * Get the consignments which are not processed to picked up for the given period and delivery mode.
     *
     * @param deliveryModeCode
     * @param noOfDays
     * @return the consignments for auto upadate partner picked up
     */
    List<TargetConsignmentModel> getConsignmentsForAutoUpdatePartnerPickedup(final String deliveryModeCode,
            final int noOfDays);

    /**
     * Get the count of consignments not processed for Store By Status
     * 
     * @param store
     * @return countPerConsignmentStatusMap
     */
    Map<ConsignmentStatus, Integer> getCountOfConsignmentsNotProcessedForStoreByStatus(
            final TargetPointOfServiceModel store);

    /**
     * Get the count of consignments completed for Store By Status
     * 
     * @param store
     * @param noOfDays
     * @return countPerConsignmentStatusMap
     */
    Map<ConsignmentStatus, Integer> getCountOfConsignmentsCompletedByDayForStoreByStatus(
            final TargetPointOfServiceModel store,
            final int noOfDays);

    /**
     * Get the count of consignments rejected for Store By Status
     * 
     * @param store
     * @param noOfDays
     * @return countPerConsignmentStatusMap
     */
    Map<ConsignmentStatus, Integer> getCountOfConsignmentsRejectedByDayForStoreByStatus(
            final TargetPointOfServiceModel store,
            final int noOfDays);


    /**
     * Gets all consignments by warehouse status and warehouse
     * 
     * @return list of consignments
     */
    List<TargetConsignmentModel> getAllConsignmentsByStatusAndWarehouse(final WarehouseModel warehouse,
            final ConsignmentStatus status);

    /**
     * Get all the consignments by multiple statuses.
     *
     * @return the consignments ack by warehouse
     */
    List<TargetConsignmentModel> getConsignmentsOfMultiplesStatusesForAllStores(List<ConsignmentStatus> statuses);
}
