/**
 * 
 */
package au.com.target.tgtfulfilment.ordercancel.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.basecommerce.enums.OrderCancelState;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.impl.DefaultOrderCancelStateMappingStrategy;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;


/**
 * @author Rahul
 *
 */
public class TargetOrderCancelStateMappingStrategy extends DefaultOrderCancelStateMappingStrategy {

    private static final Logger LOG = Logger.getLogger(TargetOrderCancelStateMappingStrategy.class.getName());

    private TargetConsignmentService targetConsignmentService;
    private List<OrderStatus> cancelDeniedOrderStates;
    private List<ConsignmentStatus> cancelDeniedConsignmentStates;
    private List<ConsignmentStatus> cancelRequiredConsignmentStates;

    private List<ConsignmentStatus> newCancelDeniedConsignmentStates;
    private List<ConsignmentStatus> newCancelRequiredConsignmentStates;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Override
    public OrderCancelState getOrderCancelState(final OrderModel order) {
        // first check the order state
        if (cancelDeniedOrderStates.contains(order.getStatus())) {
            logMessage(order, "order in disallowed status=" + order.getStatus());
            return OrderCancelState.CANCELIMPOSSIBLE;
        }

        // then the active consignments
        final List<TargetConsignmentModel> consignments = targetConsignmentService
                .getActiveConsignmentsForOrder(order);
        if (CollectionUtils.isEmpty(consignments)) {
            return OrderCancelState.PENDINGORHOLDINGAREA;
        }

        return checkConsignments(CollectionUtils.typedCollection(consignments, ConsignmentModel.class));
    }

    @Override
    protected OrderCancelState checkConsignments(final Collection<ConsignmentModel> consignments) {
        final List<ConsignmentModel> cancelDeniedConsignmentList = new ArrayList<>();
        boolean seenRequiredState = false;
        AbstractOrderModel order = null;

        if (CollectionUtils.isNotEmpty(consignments)) {
            if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.CSCOCKPIT_CANCEL_BUTTON)) {
                for (final ConsignmentModel consignment : consignments) {

                    // Just get the order reference
                    if (order == null) {
                        order = consignment.getOrder();
                    }

                    if (newCancelDeniedConsignmentStates.contains(consignment.getStatus())) {
                        logMessage(order, "consignment in disallowed status=" + consignment.getStatus());
                        cancelDeniedConsignmentList.add(consignment);
                    }
                    if (newCancelRequiredConsignmentStates.contains(consignment.getStatus())) {
                        seenRequiredState = true;
                    }
                }
            }
            else {
                for (final ConsignmentModel consignment : consignments) {

                    // Just get the order reference
                    if (order == null) {
                        order = consignment.getOrder();
                    }
                    if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)
                            && ConsignmentStatus.WAVED.equals(consignment.getStatus())) {
                        continue;
                    }
                    else if (cancelDeniedConsignmentStates.contains(consignment.getStatus())) {

                        logMessage(order, "consignment in disallowed status=" + consignment.getStatus());
                        cancelDeniedConsignmentList.add(consignment);
                    }
                    if (cancelRequiredConsignmentStates.contains(consignment.getStatus())) {
                        seenRequiredState = true;
                    }
                }
            }

        }

        if (!seenRequiredState && cancelDeniedConsignmentList.size() == consignments.size()) {
            logMessage(order, "no consignments in required status");
            return OrderCancelState.CANCELIMPOSSIBLE;
        }

        return OrderCancelState.PENDINGORHOLDINGAREA;
    }

    private void logMessage(final AbstractOrderModel order, final String message) {
        if (LOG.isDebugEnabled()) {
            String sMessage = "";
            if (order != null) {
                sMessage += "Order:" + order.getCode() + ", ";
            }
            sMessage += "Order cancel denied since " + message;
            LOG.debug(sMessage);
        }
    }

    /**
     * @param targetConsignmentService
     *            the targetConsignmentService to set
     */
    @Required
    public void setTargetConsignmentService(final TargetConsignmentService targetConsignmentService) {
        this.targetConsignmentService = targetConsignmentService;
    }

    /**
     * @param cancelDeniedOrderStates
     *            the cancelDeniedOrderStates to set
     */
    @Required
    public void setCancelDeniedOrderStates(final List<OrderStatus> cancelDeniedOrderStates) {
        this.cancelDeniedOrderStates = cancelDeniedOrderStates;
    }

    /**
     * @param cancelDeniedConsignmentStates
     *            the cancelDeniedConsignmentStates to set
     */
    @Required
    public void setCancelDeniedConsignmentStates(final List<ConsignmentStatus> cancelDeniedConsignmentStates) {
        this.cancelDeniedConsignmentStates = cancelDeniedConsignmentStates;
    }

    /**
     * @param cancelRequiredConsignmentStates
     *            the cancelRequiredConsignmentStates to set
     */
    @Required
    public void setCancelRequiredConsignmentStates(final List<ConsignmentStatus> cancelRequiredConsignmentStates) {
        this.cancelRequiredConsignmentStates = cancelRequiredConsignmentStates;
    }

    /**
     * @param newCancelDeniedConsignmentStates
     *            the newCancelDeniedConsignmentStates to set
     */
    @Required
    public void setNewCancelDeniedConsignmentStates(final List<ConsignmentStatus> newCancelDeniedConsignmentStates) {
        this.newCancelDeniedConsignmentStates = newCancelDeniedConsignmentStates;
    }

    /**
     * @param newCancelRequiredConsignmentStates
     *            the newCancelRequiredConsignmentStates to set
     */
    @Required
    public void setNewCancelRequiredConsignmentStates(
            final List<ConsignmentStatus> newCancelRequiredConsignmentStates) {
        this.newCancelRequiredConsignmentStates = newCancelRequiredConsignmentStates;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

}
