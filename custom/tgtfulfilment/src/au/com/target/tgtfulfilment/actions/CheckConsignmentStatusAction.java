/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractAction;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.google.common.base.Functions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import au.com.target.tgtbusproc.exceptions.BusinessProcessException;
import au.com.target.tgtutility.constants.TgtutilityConstants;


public class CheckConsignmentStatusAction extends AbstractAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(CheckConsignmentStatusAction.class);

    private List<String> warehousesList;

    public enum Transition {
        ALL_CANCELLED, ALL_DONE, PENDING
    }

    @Override
    public Set<String> getTransitions() {
        return ImmutableSet.copyOf(Iterables.transform(EnumSet.allOf(Transition.class), Functions.toStringFunction()));
    }

    @Override
    public String execute(final OrderProcessModel process) throws Exception {
        Assert.notNull(process, "OrderProcessModel must not be null");
        Assert.notNull(process.getOrder(), "Order must not be null");

        // NOTE on concurrency: it is possible but very unlikely that two consignments are picked at the same time, 
        // two PickProcesses could have this action running simultaneously, and both could return ALL_DONE:
        // in which case the invoicing process would be started twice.

        return getCombinedConsignmentState(process);
    }

    /**
     * Check all consignments to see if we have finished.<br/>
     * If there is an overall zero pick then set order status to cancelled
     * 
     * @param process
     *            the order process context
     * @return a combined consignment state
     */
    protected String getCombinedConsignmentState(final OrderProcessModel process) {
        Assert.notNull(process, "OrderProcessModel must not be null");
        Assert.notNull(process.getOrder(), "Order must not be null");

        final OrderModel order = process.getOrder();

        if (order.getPurchaseOptionConfig() == null) {
            throw new BusinessProcessException(TgtutilityConstants.ErrorCode.ERR_TGTFULLFILLMENT,
                    "Order has no purchase option config");
        }

        if (CollectionUtils.isEmpty(order.getConsignments())) {
            throw new BusinessProcessException(TgtutilityConstants.ErrorCode.ERR_TGTFULLFILLMENT,
                    "Order has no consignments");
        }

        boolean allDone = true;
        boolean allCancelled = true;
        for (final ConsignmentModel consignment : order.getConsignments()) {
            if (consignment == null) {
                LOG.warn("Consignment shouldn't be null at this point. " + process.getCode());
                continue;
            }
            if (!ConsignmentStatus.CANCELLED.equals(consignment.getStatus())) {
                allCancelled = false;
            }

            // The pick is 'DONE' if all consignments are either SHIPPED, PICKED or CANCELLED, PACKED (in-store fulfillment)
            // PICKED status for instore fullfilment should not be considered for DONE
            if (warehousesList.contains(consignment.getWarehouse().getCode())) {
                if (!ImmutableList.of(ConsignmentStatus.SHIPPED, ConsignmentStatus.CANCELLED,
                        ConsignmentStatus.PICKED)
                        .contains(consignment.getStatus())) {
                    allDone = false;
                }
            }
            else {
                if (!ImmutableList.of(ConsignmentStatus.PACKED, ConsignmentStatus.SHIPPED, ConsignmentStatus.CANCELLED)
                        .contains(consignment.getStatus())) {
                    allDone = false;
                }
            }


        }
        if (allCancelled) {
            return Transition.ALL_CANCELLED.name();
        }
        else if (allDone) {
            return Transition.ALL_DONE.name();
        }
        else {
            return Transition.PENDING.name();
        }
    }

    /**
     * @param warehousesList
     *            the warehousesList to set
     */
    @Required
    public void setWarehousesList(final List<String> warehousesList) {
        this.warehousesList = warehousesList;
    }

}
