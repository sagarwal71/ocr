/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.CancelTypeEnum;
import au.com.target.tgtfulfilment.service.SendToWarehouseService;


/**
 * Send cancel or part cancel order to warehouse
 * 
 */
public class ExtractCancelOrderToWarehouseAction extends AbstractProceduralAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(ExtractCancelOrderToWarehouseAction.class);

    private SendToWarehouseService sendToWarehouseService;
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {

        Assert.notNull(process, "OrderProcessModel must not be null");

        final CancelTypeEnum orderCancelType = orderProcessParameterHelper.getCancelType(process);
        Assert.notNull(orderCancelType, "cancelType must not be null");
        switch (orderCancelType) {
            case SHORT_PICK:
            case ZERO_PICK:
                LOG.debug("Short/Zero pick does not need to be sent to warehouse, because that is where it came from");
                break;
            default:
                doExtract(process);
        }
    }


    private void doExtract(final OrderProcessModel process) {

        final OrderModel order = process.getOrder();
        Assert.notNull(order, "OrderProcessModel must not be null");

        // Check whether to do the extract based on the process
        if (orderProcessParameterHelper.doCancelExtract(process)) {

            sendToWarehouseService.sendOrderCancellation(order);
        }
    }


    /**
     * @param sendToWarehouseService
     *            the sendToWarehouseService to set
     */
    @Required
    public void setSendToWarehouseService(final SendToWarehouseService sendToWarehouseService) {
        this.sendToWarehouseService = sendToWarehouseService;
    }


    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }


}
