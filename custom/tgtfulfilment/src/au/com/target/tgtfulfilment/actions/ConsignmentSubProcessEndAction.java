package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import au.com.target.tgtbusproc.actions.SubProcessEndAction;

/**
 * Triggers an event that indicates a consignment related sub-process end. The event name
 * is composed of consignment code appended with {@link #getEventSuffix()}.
 */
public class ConsignmentSubProcessEndAction extends SubProcessEndAction {

    private String eventSuffix;

    @Override
    public String getEvent(final OrderProcessModel process) {
        final ConsignmentModel consignment = getProcessParameterHelper().getConsignment(process);
        return consignment.getCode() + getEventSuffix();
    }

    /**
     * Returns the event suffix, a string constant indicating a nature of the process, e.g.
     * {@link au.com.target.tgtfulfilment.constants.TgtfulfilmentConstants#EVENT_INVOICING_FINISHED_SUFFIX}.
     *
     * @return the event suffix
     */
    public String getEventSuffix() {
        return eventSuffix;
    }

    /**
     * Sets the event suffix.
     *
     * @param eventSuffix the event suffix
     */
    public void setEventSuffix(final String eventSuffix) {
        this.eventSuffix = eventSuffix;
    }
}
