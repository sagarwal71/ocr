/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;


/**
 * @author ayushman
 *
 */
public class AdjustNTLStockAction extends AbstractProceduralOrderAction {

    private TargetStockService targetStockService;

    private TargetStoreConsignmentService targetStoreConsignmentService;

    private TargetWarehouseService targetWarehouseService;

    /* (nonJavadoc)
     * @see de.hybris.platform.processengine.action.AbstractProceduralAction#executeAction(de.hybris.platform.processengine.model.BusinessProcessModel)
     */
    @Override
    public void executeAction(final OrderProcessModel process) throws Exception {
        Assert.notNull(process, "OrderProcess cannot be null");

        final OrderModel order = process.getOrder();
        Assert.notNull(order, "Order cannot be null");

        if (CollectionUtils.isNotEmpty(order.getConsignments())) {

            for (final ConsignmentModel consignment : order.getConsignments()) {

                if (targetStoreConsignmentService.isConsignmentAssignedToAnyNTL(consignment)) {

                    if (CollectionUtils.isNotEmpty(consignment.getConsignmentEntries())) {

                        for (final ConsignmentEntryModel conEntModel : consignment.getConsignmentEntries()) {

                            if (null != conEntModel.getOrderEntry()
                                    && null != conEntModel.getOrderEntry().getProduct()
                                    && null != conEntModel.getOrderEntry()
                                            .getQuantity()) {

                                targetStockService.transferReserveAmount(conEntModel.getOrderEntry().getProduct(),
                                        conEntModel.getOrderEntry().getQuantity().intValue(),
                                        targetWarehouseService.getDefaultOnlineWarehouse(),
                                        consignment.getWarehouse(), consignment.getCode());
                            }
                        }
                    }
                }
            }

        }
    }

    /**
     * @param targetStockService
     *            the targetStockService to set
     */
    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }

    /**
     * @param targetStoreConsignmentService
     *            the targetStoreConsignmentService to set
     */
    @Required
    public void setTargetStoreConsignmentService(final TargetStoreConsignmentService targetStoreConsignmentService) {
        this.targetStoreConsignmentService = targetStoreConsignmentService;
    }

    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */
    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }

}