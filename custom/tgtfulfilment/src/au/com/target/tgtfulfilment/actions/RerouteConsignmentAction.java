package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtfulfilment.service.SendToWarehouseService;
import au.com.target.tgtfulfilment.service.impl.ConsignmentReroutingService;


/**
 * Re-route consignment if store rejects the order.
 * 
 */
public class RerouteConsignmentAction extends AbstractProceduralAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(RerouteConsignmentAction.class);

    private static final String ERR_WHILE_REROUTING_CONS = "ERROR-CONSIGNMENT-REROUTING: Error while re-routing consignment, consignmentCode={0}, orderCode={1}";

    private static final String ERR_CONS_TO_REROUTE_MISSING = "ERROR-CONSIGNMENT-MISSING-TO-REROUTE: No consignments found to reroute for orderCode={0}";

    private static final String INFO_SKIP_REROUTE = "INFO-CONSIGNMENT-REROUTING: Skip rerouting consignment, because of all items fulfulled,  consignmentCode={0}, orderCode={1}";

    private OrderProcessParameterHelper orderProcessParameterHelper;

    private ConsignmentReroutingService consignmentReroutingService;

    private SendToWarehouseService sendToWarehouseService;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    /**
     * {@inheritDoc}
     */
    @Override
    public void executeAction(final OrderProcessModel process) throws Exception {
        Assert.notNull(process, "OrderProcessModel cannot be null");
        // Get the consignment and update object out of the process parameters
        final ConsignmentModel consignment = orderProcessParameterHelper.getConsignment(process);
        Assert.notNull(consignment, "Consignment cannot be null");
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)) {
            if (consignmentReroutingService.isRequireRerouting(consignment)) {
                final List<ConsignmentModel> consignments = consignmentReroutingService.rerouteConsignment(consignment);
                sendToWarehouse(consignments);
            }
            else {
                LOG.info(MessageFormat.format(INFO_SKIP_REROUTE, consignment.getCode(),
                        consignment.getOrder() != null ? consignment.getOrder()
                                .getCode() : StringUtils.EMPTY));
            }
        }
        else {
            final List<ConsignmentModel> consignments = consignmentReroutingService.rerouteConsignment(consignment);

            if (CollectionUtils.isEmpty(consignments)) {
                throw new IllegalStateException(
                        MessageFormat.format(ERR_CONS_TO_REROUTE_MISSING, consignment.getOrder().getCode()));
            }
            sendToWarehouse(consignments);
        }

    }

    private void sendToWarehouse(final List<ConsignmentModel> consignments) {
        for (final ConsignmentModel cons : consignments) {
            try {
                sendToWarehouseService.sendConsignment(cons);
            }
            catch (final Exception e) {
                LOG.error(MessageFormat.format(ERR_WHILE_REROUTING_CONS, cons.getCode(), cons.getOrder().getCode()), e);
            }
        }
    }

    /**
     * @param sendToWarehouseService
     *            the sendToWarehouseService to set
     */
    @Required
    public void setSendToWarehouseService(final SendToWarehouseService sendToWarehouseService) {
        this.sendToWarehouseService = sendToWarehouseService;
    }

    /**
     * @param consignmentReroutingService
     *            the consignmentReroutingService to set
     */
    @Required
    public void setConsignmentReroutingService(final ConsignmentReroutingService consignmentReroutingService) {
        this.consignmentReroutingService = consignmentReroutingService;
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

}
