package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.ConsignmentCreationException;
import de.hybris.platform.ordersplitting.OrderSplittingService;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;


/**
 * Split the order/order entries into consignments
 */
public class SplitOrderAction extends AbstractProceduralOrderAction
{

    private static final Logger LOG = Logger.getLogger(SplitOrderAction.class);

    private OrderSplittingService orderSplittingService;

    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Override
    public void executeAction(final OrderProcessModel process) throws ConsignmentCreationException
    {
        Assert.notNull(process, "OrderProcess cannot be null");

        final OrderModel orderModel = process.getOrder();
        Assert.notNull(orderModel, "Order cannot be null");
        assignDeliveryModesToOrderEntries(orderModel);

        final List<ConsignmentModel> consignments = orderSplittingService.splitOrderForConsignment(
                orderModel, orderModel.getEntries());

        if (LOG.isDebugEnabled())
        {
            LOG.debug("Splitting order into " + consignments.size() + " consignments.");
        }
        setOrderStatus(process.getOrder(), OrderStatus.INPROGRESS);
    }

    /**
     * Assign delivery modes to order entries based on the product.
     * 
     * @param orderModel
     */
    private void assignDeliveryModesToOrderEntries(final OrderModel orderModel) {
        final List<AbstractOrderEntryModel> orderEntries = orderModel.getEntries();
        if (CollectionUtils.isNotEmpty(orderEntries)) {
            for (final AbstractOrderEntryModel orderEntry : orderEntries) {
                final ProductModel product = orderEntry.getProduct();
                if (product != null) {
                    if (targetDeliveryModeHelper.hasOnlyDigitalDeliveryMode(product)) {
                        final TargetZoneDeliveryModeModel digitalDeliveryMode = targetDeliveryModeHelper
                                .getDigitalDeliveryMode(product);
                        orderEntry.setDeliveryMode(digitalDeliveryMode);
                        orderEntry.setDeliveryAddress(null);
                    }
                    else {
                        orderEntry.setDeliveryMode(orderModel.getDeliveryMode());
                        orderEntry.setDeliveryAddress(orderModel.getDeliveryAddress());
                    }
                    getModelService().save(orderEntry);
                }
            }
        }
    }

    @Required
    public void setOrderSplittingService(final OrderSplittingService orderSplittingService)
    {
        this.orderSplittingService = orderSplittingService;
    }

    /**
     * @param targetDeliveryModeHelper
     *            the targetDeliveryModeHelper to set
     */
    @Required
    public void setTargetDeliveryModeHelper(final TargetDeliveryModeHelper targetDeliveryModeHelper) {
        this.targetDeliveryModeHelper = targetDeliveryModeHelper;
    }

}
