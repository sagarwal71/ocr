/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.actions.SendEmailAction;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;


/**
 * Send shipping notice for all consignments in the order
 * 
 */
public class SendAllOrderShippingNoticesEmailAction extends SendEmailAction {

    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Override
    public Transition executeAction(final BusinessProcessModel process) throws RetryLaterException, Exception {

        Assert.notNull(process, "Process cannot be null");

        if (process instanceof OrderProcessModel) {
            final OrderModel order = ((OrderProcessModel)process).getOrder();

            final Set<ConsignmentModel> consignments = order.getConsignments();
            if (CollectionUtils.isNotEmpty(consignments)) {
                for (final ConsignmentModel consignment : consignments) {

                    // Set the consignment in the process param then execute the base email action
                    orderProcessParameterHelper.setConsignment(((OrderProcessModel)process), consignment);
                    super.executeAction(process);
                }
            }
        }

        return Transition.OK;
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }


}
