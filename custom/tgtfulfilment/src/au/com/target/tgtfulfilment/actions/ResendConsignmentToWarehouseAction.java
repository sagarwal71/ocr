package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtfulfilment.service.SendToWarehouseService;


/**
 * This action is called to resend the consignment to warehouse when the status of the order is not changed for a
 * pre-defined period of time
 */
public class ResendConsignmentToWarehouseAction extends AbstractProceduralAction<OrderProcessModel> {

    private SendToWarehouseService sendToWarehouseService;

    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process, "OrderProcess cannot be null");
        final OrderModel order = process.getOrder();
        Assert.notNull(order, "Order cannot be null");

        final ConsignmentModel consignment = orderProcessParameterHelper.getConsignment(process);

        Assert.notNull(consignment, "Consignment cannot be null");
        sendToWarehouseService.sendConsignment(consignment);
    }

    /**
     * @param sendToWarehouseService
     *            the sendToWarehouseService to set
     */
    @Required
    public void setSendToWarehouseService(final SendToWarehouseService sendToWarehouseService) {
        this.sendToWarehouseService = sendToWarehouseService;
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }

}
