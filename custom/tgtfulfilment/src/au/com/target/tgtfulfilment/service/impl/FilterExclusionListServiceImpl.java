/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import de.hybris.platform.category.model.CategoryModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import au.com.target.tgtfulfilment.model.CategoryExclusionsModel;
import au.com.target.tgtfulfilment.model.ProductExclusionsModel;
import au.com.target.tgtfulfilment.service.FilterExclusionListService;


/**
 * @author smishra1
 *
 */
public class FilterExclusionListServiceImpl implements FilterExclusionListService {

    /**
     * {@inheritDoc}
     * */
    @Override
    public Set<ProductExclusionsModel> filterActiveProductExclusions(
            final Set<ProductExclusionsModel> productExclusionList) {
        final Set<ProductExclusionsModel> filteredProductExclusions = new HashSet<>();
        filteredProductExclusions.addAll(productExclusionList);
        CollectionUtils.filter(filteredProductExclusions, new Predicate() {
            @Override
            public boolean evaluate(final Object input) {
                final Date exclusionEndDate = ((ProductExclusionsModel)input).getExclusionEndDate();
                final Date exclusionStartDate = ((ProductExclusionsModel)input).getExclusionStartDate();
                return compareDates(exclusionStartDate, exclusionEndDate);
            }
        });
        return filteredProductExclusions;
    }

    @Override
    public List<CategoryModel> filterActiveCategoryModelsByDateRange(
            final Set<CategoryExclusionsModel> excludeCategoryList) {

        final List<CategoryModel> excludeCategories = new ArrayList<>();
        if (excludeCategoryList != null) {
            for (final CategoryExclusionsModel excludeCategory : excludeCategoryList) {
                if (compareDates(excludeCategory.getExclusionStartDate(), excludeCategory.getExclusionEndDate())) {
                    excludeCategories.add(excludeCategory.getCategory());
                }
            }
        }
        return excludeCategories;
    }

    protected boolean compareDates(final Date exclusionStartDate, final Date exclusionEndDate) {
        if (null == exclusionStartDate) {
            return (null == exclusionEndDate || (exclusionEndDate.compareTo(new Date()) > 0));
        }
        else if ((exclusionStartDate.compareTo(new Date()) <= 0)) {
            return (null == exclusionEndDate || (exclusionEndDate.compareTo(new Date()) > 0));
        }
        return false;
    }
}
