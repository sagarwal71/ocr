/**
 * 
 */
package au.com.target.tgtfulfilment.service;

import java.util.List;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;


/**
 * @author rsamuel3
 *
 */
public interface TargetStoreLocatorService {
    /**
     * get the stores in the state
     * 
     * @param state
     * @return List of stores in a state
     */
    List<TargetPointOfServiceModel> getAllFulfilmentStoresInState(String state);

    /**
     * get the enabled stores
     * 
     * @return List of stores
     */
    List<TargetPointOfServiceModel> getAllFulfilmentStores();
}
