package au.com.target.tgtfulfilment.service.impl;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfulfilment.dao.TargetGlobalStoreFulfilmentCapabilitiesDao;
import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;


/**
 * The Class TargetGlobalStoreFulfilmentServiceImpl.
 * 
 * @author ajit
 */
public class TargetGlobalStoreFulfilmentServiceImpl implements TargetGlobalStoreFulfilmentService {

    private TargetGlobalStoreFulfilmentCapabilitiesDao targetGlobalStoreFulfilmentCapabilitiesDao;

    /**
     * Method to get GlobalStoreFulfilmentCapabilities model
     * 
     * @return GlobalStoreFulfilmentCapabilitiesModel
     */
    @Override
    public GlobalStoreFulfilmentCapabilitiesModel getGlobalStoreFulfilmentCapabilites() {
        return targetGlobalStoreFulfilmentCapabilitiesDao.getGlobalStoreFulfilmentCapabilities();
    }

    /**
     *
     * @param targetGlobalStoreFulfilmentCapabilitiesDao
     *            the new target global store fulfilment capabilities dao
     */
    @Required
    public void setTargetGlobalStoreFulfilmentCapabilitiesDao(
            final TargetGlobalStoreFulfilmentCapabilitiesDao targetGlobalStoreFulfilmentCapabilitiesDao) {
        this.targetGlobalStoreFulfilmentCapabilitiesDao = targetGlobalStoreFulfilmentCapabilitiesDao;
    }

}
