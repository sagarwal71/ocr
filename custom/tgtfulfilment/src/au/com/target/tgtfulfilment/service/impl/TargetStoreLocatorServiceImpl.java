/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.dao.TargetPointOfServiceDao;
import au.com.target.tgtfulfilment.service.TargetStoreLocatorService;


/**
 * @author rsamuel3
 *
 */
public class TargetStoreLocatorServiceImpl implements TargetStoreLocatorService {

    private TargetPointOfServiceDao targetPointOfServiceDao;

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.service.TargetStoreLocatorService#getAllStores(java.lang.String)
     */
    @Override
    public List<TargetPointOfServiceModel> getAllFulfilmentStoresInState(final String state) {
        Assert.notNull(state, "A value needs to be provided for the state");
        return targetPointOfServiceDao.getAllFulfilmentEnabledPOSInState(state);
    }

    @Override
    public List<TargetPointOfServiceModel> getAllFulfilmentStores() {
        return targetPointOfServiceDao.getAllFulfilmentEnabledPOS();
    }

    /**
     * @param targetPointOfServiceDao
     *            the targetPointOfServiceDao to set
     */
    @Required
    public void setTargetPointOfServiceDao(final TargetPointOfServiceDao targetPointOfServiceDao) {
        this.targetPointOfServiceDao = targetPointOfServiceDao;
    }

}
