/**
 * 
 */
package au.com.target.tgtfulfilment.service;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.dto.TargetConsignmentPageResult;


/**
 * Provides services for retrieving consignments for stores.
 *
 */
public interface TargetStoreConsignmentService {

    /**
     * Get all consignments for given store
     * 
     * @param storeNumber
     * @return list
     */
    List<TargetConsignmentModel> getConsignmentsForStore(Integer storeNumber);

    /**
     * Get all consignments for given store
     * 
     * @param storeNumber
     * @param offset
     *            offset position
     * @param recsPerPage
     *            records per page
     * @return targetConsignmentPageResult
     */
    TargetConsignmentPageResult getConsignmentsForStore(Integer storeNumber, final int offset,
            final int recsPerPage, int lastXDays);

    /**
     * Get all consignments for given list of codes
     * 
     * @param consignmentCodes
     *            list of consignment codes
     * @return list of Consignments
     */
    List<TargetConsignmentModel> getConsignmentsByCodes(List<String> consignmentCodes);

    /**
     * Get all the consignments for store with all the statuses by day. Day will be calculated from current day i.e. 0
     * means today, 1 means yesterday and so on.
     *
     * @param store
     *            the store
     * @param noOfDays
     *            the no of days
     * @return the consignments for store by date
     */
    List<TargetConsignmentModel> getConsignmentsByDayForStore(final TargetPointOfServiceModel store,
            final int noOfDays);

    /**
     * Is the consignment passed in associated with a store warehouse
     * 
     * @param consignment
     * @return true if the warehouse assigned to the consignment is a store warehouse
     */
    boolean isConsignmentAssignedToAnyStore(ConsignmentModel consignment);

    /**
     * Is the consignment passed in associated with a store warehouse which belongs to NTL
     * 
     * @param consignment
     * @return true if the warehouse assigned to the consignment is From Non Trading Location
     */
    boolean isConsignmentAssignedToAnyNTL(ConsignmentModel consignment);

    /**
     * Is the consignment assigned to the given store
     * 
     * @param consignment
     * @param store
     * @return true if consignment is assigned to given store
     */
    boolean isConsignmentAssignedToStore(ConsignmentModel consignment, int store);

    /**
     * Get all the not manifested consignments
     * 
     * @param storeNumber
     * @return consignments
     */
    List<TargetConsignmentModel> getConsignmentsNotManifestedForStore(int storeNumber);

    /**
     * Get all the consignments by status
     *
     * @return the consignments ack by warehouse
     */
    List<TargetConsignmentModel> getConsignmentsForAllStore(ConsignmentStatus status);

    /**
     * Get all consignments by status and within the given list of states.
     * 
     * @param status
     * @param includedStates
     * @return the consignments ack by warehouse
     */
    List<TargetConsignmentModel> getConsignmentsForStoreInState(List<ConsignmentStatus> status,
            List<String> includedStates);

    /**
     * Get all consignments by status and not within the given list of states.
     * 
     * @param status
     * @param excludedStates
     * @return the consignments ack by warehouse
     */
    List<TargetConsignmentModel> getConsignmentsForStoreNotInState(List<ConsignmentStatus> status,
            List<String> excludedStates);

    /**
     * Return the store that the consignment is assigned to
     * 
     * @param consignment
     * @return TargetPointOfSale or null if not assigned to a store
     */
    TargetPointOfServiceModel getAssignedStoreForConsignment(ConsignmentModel consignment);

    /**
     * Return the store number if consignment is assigned to a store
     * 
     * @param consignment
     * @return int store number or null if not assigned to a store
     */
    Integer getAssignedStoreForConsignmentAsInteger(ConsignmentModel consignment);

    /**
     * Get all stores which currently have open orders to be fulfilled
     * 
     * @return Map of Pos and Number of open orders
     */
    Map<TargetPointOfServiceModel, Integer> getPosAndOpenOrdersForStores();

    /**
     * Get consignment for given code
     * 
     * @param consignmentCode
     * @return TargetConsignmentModel or null if not found
     */
    TargetConsignmentModel getConsignmentByCode(String consignmentCode);

    /**
     * Return assigned store for given warehouse, if there is one
     * 
     * @param warehouse
     * @return tpos or null
     */
    TargetPointOfServiceModel getAssignedStoreForWarehouse(WarehouseModel warehouse);

    /**
     * Method will return the count of consignments by status
     * 
     * @param store
     * @param noOfDays
     * @return countOfConsignmentsByStatus
     */
    Map<ConsignmentStatus, Integer> getConsignmentCountByStatus(final TargetPointOfServiceModel store,
            final int noOfDays);

    /**
     * Get delivery to state from consignment
     * 
     * @param targetConsignmentModel
     * @return delivery to state
     */
    String getDeliverToStateFromConsignment(final TargetConsignmentModel targetConsignmentModel);

    /**
     * Get delivery to store number from consignment
     * 
     * @param targetConsignmentModel
     * @return delivery to store number
     */
    String getDeliverToStoreNumberFromConsignment(final TargetConsignmentModel targetConsignmentModel);

    /**
     * Get delivery from state from consignment
     * 
     * @param targetConsignmentModel
     * @return delivery from state
     */
    String getDeliverFromStateFromConsignment(final TargetConsignmentModel targetConsignmentModel);

    /**
     * Get order code from consignment
     * 
     * @param targetConsignmentModel
     * @return order code
     */
    String getOrderCodeFromConsignment(final TargetConsignmentModel targetConsignmentModel);

    /**
     * Get all the consignments by multiple statuses.
     *
     * @return the consignments ack by warehouse
     */
    List<TargetConsignmentModel> getConsignmentsOfMultiplesStatusesForAllStores(List<ConsignmentStatus> status);
}
