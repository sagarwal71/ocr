/**
 * 
 */
package au.com.target.tgtfulfilment.warehouseservices;

import de.hybris.platform.ordersplitting.model.WarehouseModel;

import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;


/**
 * The Interface TargetFulfilmentWarehouseService.
 *
 * @author ajit
 */
public interface TargetFulfilmentWarehouseService extends TargetWarehouseService {


    /**
     * Gets the digital or physical giftcard warehouse. Both refer to "Incomm warehouse"
     *
     * @return the digital giftcard warehouse
     */
    WarehouseModel getGiftcardWarehouse();
}
