/**
 * 
 */
package au.com.target.tgtfulfilment.dispatchlabel.service.impl;

import org.apache.log4j.Logger;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.dispatchlabel.service.DispatchLabelService;
import au.com.target.tgtfulfilment.integration.dto.LabelLayout;
import au.com.target.tgtfulfilment.integration.dto.LabelResponseDTO;


/**
 * @author sbryan6
 *
 */
public class DefaultDispatchLabelServiceImpl implements DispatchLabelService {

    private static final Logger LOG = Logger.getLogger(DefaultDispatchLabelServiceImpl.class);

    @Override
    public LabelResponseDTO getDispatchLabel(final TargetPointOfServiceModel pos,
            final TargetConsignmentModel consignment,
            final LabelLayout layout, final boolean branding) {

        LOG.info("In default dispatch label service");
        return null;
    }

}
