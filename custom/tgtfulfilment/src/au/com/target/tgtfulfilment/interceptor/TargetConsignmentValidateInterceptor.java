/**
 * 
 */
package au.com.target.tgtfulfilment.interceptor;

import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.lang.BooleanUtils;

import au.com.target.tgtcore.model.TargetConsignmentModel;


/**
 * @author sbryan6
 *
 */
public class TargetConsignmentValidateInterceptor implements ValidateInterceptor<TargetConsignmentModel> {

    @Override
    public void onValidate(final TargetConsignmentModel consignment, final InterceptorContext ctx)
            throws InterceptorException {

        if (consignment.getShippingAddress() != null) {
            return;
        }

        // If shipping address is not present then there must be a digital delivery mode
        final DeliveryModeModel delMode = consignment.getDeliveryMode();

        if (!(delMode instanceof TargetZoneDeliveryModeModel)) {

            throw new InterceptorException(
                    "Consignment has no shippingAddress and delivery mode is missing or not an instance of TargetZoneDeliveryModeModel, consignment="
                            + consignment.getCode());
        }

        final TargetZoneDeliveryModeModel targetDelMode = (TargetZoneDeliveryModeModel)delMode;
        if (BooleanUtils.isNotTrue(targetDelMode.getIsDigital())) {

            throw new InterceptorException(
                    "Consignment has no shippingAddress and consignment delivery mode is not digital, consignment="
                            + consignment.getCode());
        }
    }


}
