/**
 * 
 */
package au.com.target.tgtfulfilment.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfulfilment.model.TargetCarrierModel;


/**
 * @author mjanarth
 * 
 */
public class TargetCarrierValidateInterceptor implements ValidateInterceptor {

    private int orderInstructionsLength;

    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof TargetCarrierModel) {
            final TargetCarrierModel targetCarrier = (TargetCarrierModel)model;
            final String orderInstructions1 = targetCarrier.getOrderInstructions1();
            final String orderInstructions2 = targetCarrier.getOrderInstructions2();

            if (StringUtils.isNotBlank(orderInstructions1) && (orderInstructions1.length() > orderInstructionsLength)) {
                throw new InterceptorException("Maximum length allowed for Order Instructions1 is  "
                        + orderInstructionsLength);
            }
            if (StringUtils.isNotBlank(orderInstructions2) && (orderInstructions2.length() > orderInstructionsLength)) {
                throw new InterceptorException("Maximum length allowed for  Order Instructions2  is  "
                        + orderInstructionsLength);
            }

        }
    }

    /**
     * 
     * @param orderInstructionsLength
     */
    @Required
    public void setOrderInstructionsLength(final int orderInstructionsLength) {
        this.orderInstructionsLength = orderInstructionsLength;
    }

}
