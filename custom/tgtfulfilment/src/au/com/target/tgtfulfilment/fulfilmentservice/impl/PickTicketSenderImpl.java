/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;
import au.com.target.tgtfulfilment.dto.PickConfirmEntry;
import au.com.target.tgtfulfilment.fulfilmentservice.PickTicketSender;


/**
 * Helper for sending pick CS tickets.
 * 
 */
public class PickTicketSenderImpl implements PickTicketSender {

    protected static final String SHORT_ZERO_TICKET_HEADLINE = "Short or Zero pick received, but was unexpected";
    protected static final String SHORT_ZERO_TICKET_SUBJECT = "Short or Zero pick received, but was unexpected";

    protected static final String SHORT_ZERO_PINPAD_TICKET_HEADLINE = "Short or Zero pick received for an order from kiosk, but was unexpected";
    protected static final String SHORT_ZERO_PINPAD_TICKET_SUBJECT = "Short or Zero pick received for an order from kiosk, but was unexpected";

    protected static final String ZERO_EBAY_TICKET_HEADLINE = "Zero pick received for an order from eBay, but was unexpected";
    protected static final String ZERO_EBAY_TICKET_SUBJECT = "Zero pick received for an order from eBay, but was unexpected";

    protected static final String ZERO_TRADEME_TICKET_HEADLINE = "Zero pick received for an order from TradeMe, but was unexpected";
    protected static final String ZERO_TRADEME_TICKET_SUBJECT = "Zero pick received for an order from TradeMe, but was unexpected";

    private static final Logger LOG = Logger.getLogger(PickTicketSenderImpl.class);

    private static final String INVALID_TICKET_HEADLINE = "Invalid pick received, but was unexpected";
    private static final String INVALID_TICKET_SUBJECT = "Invalid pick received, but was unexpected";


    private static final String OVER_TICKET_HEADLINE = "Over pick received, but was unexpected";
    private static final String OVER_TICKET_SUBJECT = "Over pick received, but was unexpected";

    private static final String FAILED_REFUND_HEADLINE = "Short or Zero pick refund failed";
    private static final String FAILED_REFUND_SUBJECT = "Short or Zero pick refund failed";

    private TargetTicketBusinessService targetTicketBusinessService;


    @Override
    public void sendTicketInvalidPick(final OrderModel orderModel, final String errorMessage) {

        sendTicketToCsAgent(orderModel, INVALID_TICKET_HEADLINE, INVALID_TICKET_SUBJECT, errorMessage);
    }

    @Override
    public void sendTicketFailedRefund(final OrderModel orderModel, final String errorMessage) {

        sendTicketToCsAgent(orderModel, FAILED_REFUND_HEADLINE, FAILED_REFUND_SUBJECT, errorMessage);
    }

    @Override
    public void sendTicketShortZeroPick(final OrderModel order, final ConsignmentModel consignment,
            final List<PickConfirmEntry> pickConfirmEntries) {

        final String body = "Short or Zero pick received for an order that does not allow them.";


        final StringBuilder bodyText = new StringBuilder();
        bodyText.append(body);

        final Set<ConsignmentEntryModel> consignmentEntries = consignment.getConsignmentEntries();
        if (CollectionUtils.isNotEmpty(consignmentEntries)) {
            bodyText.append("\n Consignment Code: ").append(consignment.getCode());
            bodyText.append("\n Consignment ordered quantities: ");
            appendOrderedConsignmentEntries(bodyText, consignmentEntries);
            bodyText.append("\n Picked quantities: ");
            appendPickDetails(bodyText, pickConfirmEntries);
        }
        if (isAKioskOrder(order)) {
            sendTicketToCsAgent(order, SHORT_ZERO_PINPAD_TICKET_HEADLINE, SHORT_ZERO_PINPAD_TICKET_SUBJECT,
                    bodyText.toString());
        }
        else if (isEbayOrder(order)) {
            sendTicketToCsAgent(order, ZERO_EBAY_TICKET_HEADLINE, ZERO_EBAY_TICKET_SUBJECT,
                    bodyText.toString());
        }
        else if (isTradeMeOrder(order)) {
            sendTicketToCsAgent(order, ZERO_TRADEME_TICKET_HEADLINE, ZERO_TRADEME_TICKET_SUBJECT,
                    bodyText.toString());
        }
        else {
            sendTicketToCsAgent(order, SHORT_ZERO_TICKET_HEADLINE, SHORT_ZERO_TICKET_SUBJECT, bodyText.toString());
        }
    }

    /**
     * @param order
     * @return true if order is a sales application is kiosk and false otherwise
     */
    @Override
    public boolean isAKioskOrder(final OrderModel order) {
        final SalesApplication salesApplication = order.getSalesApplication();
        return salesApplication != null
                && salesApplication.equals(SalesApplication.KIOSK);
    }

    @Override
    public boolean isEbayOrder(final OrderModel order) {
        final SalesApplication salesApplication = order.getSalesApplication();
        return salesApplication != null
                && salesApplication.equals(SalesApplication.EBAY);
    }

    @Override
    public boolean isTradeMeOrder(final OrderModel order) {
        final SalesApplication salesApplication = order.getSalesApplication();
        return salesApplication != null
                && salesApplication.equals(SalesApplication.TRADEME);
    }

    @Override
    public void sendTicketOverPick(final OrderModel order, final ConsignmentModel consignment,
            final List<PickConfirmEntry> pickConfirmEntries) {

        final StringBuilder bodyText = new StringBuilder();
        bodyText.append("Over pick received for an order that does not allow them.");

        final Set<ConsignmentEntryModel> consignmentEntries = consignment.getConsignmentEntries();
        if (CollectionUtils.isNotEmpty(consignmentEntries)) {
            bodyText.append("\n Consignment Code: ").append(consignment.getCode());
            bodyText.append("\n Consignment ordered quantities: ");
            appendOrderedConsignmentEntries(bodyText, consignmentEntries);
            bodyText.append("\n Picked quantities: ");
            appendPickDetails(bodyText, pickConfirmEntries);
        }

        sendTicketToCsAgent(order, OVER_TICKET_HEADLINE, OVER_TICKET_SUBJECT, bodyText.toString());
    }

    private StringBuilder appendOrderedConsignmentEntries(final StringBuilder builder,
            final Set<ConsignmentEntryModel> consignmentEntries) {

        for (final ConsignmentEntryModel cem : consignmentEntries) {
            builder.append("(Item=").append(cem.getOrderEntry().getProduct().getCode())
                    .append(", OrderedQuantity=").append(cem.getQuantity()).append(") \n");
        }
        return builder;
    }

    private StringBuilder appendPickDetails(final StringBuilder builder,
            final List<PickConfirmEntry> pickConfirmEntries) {

        for (final PickConfirmEntry pickEntry : pickConfirmEntries) {
            builder.append("(Item=").append(pickEntry.getItemCode())
                    .append(", PickedQuantity=").append(pickEntry.getQuantityShipped()).append(") \n");
        }
        return builder;
    }

    private void sendTicketToCsAgent(final OrderModel orderModel, final String headline, final String subject,
            final String errorMessage) {

        targetTicketBusinessService.createCsAgentTicket(headline, subject, errorMessage, orderModel);

        final StringBuilder logMsg = new StringBuilder();
        logMsg.append("Successfully raised ticket for cs agent");
        if (orderModel != null) {
            logMsg.append(" for order:").append(orderModel.getCode());
        }
        LOG.info(logMsg);

    }


    /**
     * @param targetTicketBusinessService
     *            the targetTicketBusinessService to set
     */
    @Required
    public void setTargetTicketBusinessService(final TargetTicketBusinessService targetTicketBusinessService) {
        this.targetTicketBusinessService = targetTicketBusinessService;
    }


}
