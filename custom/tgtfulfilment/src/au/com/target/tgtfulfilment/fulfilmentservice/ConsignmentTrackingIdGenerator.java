/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice;

import au.com.target.tgtfulfilment.exceptions.TrackingIdGenerationException;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;


/**
 * @author smishra1
 *
 */
public interface ConsignmentTrackingIdGenerator {
    /**
     * @param storeFulfilmentModel
     *            the storeFulfilmentModel
     * @return trackingId for AusPost
     */
    String generateTrackingIdForConsignment(final StoreFulfilmentCapabilitiesModel storeFulfilmentModel)
            throws TrackingIdGenerationException;
}
