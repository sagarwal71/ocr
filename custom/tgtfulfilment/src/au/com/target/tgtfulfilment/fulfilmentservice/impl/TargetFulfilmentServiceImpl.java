package au.com.target.tgtfulfilment.fulfilmentservice.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.runtime.parser.node.MathUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.google.common.collect.ImmutableSet;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.constants.TgtbusprocConstants;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.PickTypeEnum;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.jalo.ProductType;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.order.dao.TargetOrderDao;
import au.com.target.tgtcore.util.ProductUtil;
import au.com.target.tgtfluent.constants.TgtFluentConstants;
import au.com.target.tgtfluent.exception.FluentBaseException;
import au.com.target.tgtfluent.service.FluentFulfilmentService;
import au.com.target.tgtfulfilment.dto.ConsignmentParcelDTO;
import au.com.target.tgtfulfilment.dto.PickConfirmEntry;
import au.com.target.tgtfulfilment.dto.PickConsignmentUpdateData;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectState;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.enums.WarehouseAutoTrigger;
import au.com.target.tgtfulfilment.exceptions.ConsignmentStatusValidationException;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;
import au.com.target.tgtfulfilment.fulfilmentservice.ConsignmentParcelUpdater;
import au.com.target.tgtfulfilment.fulfilmentservice.ConsignmentStatusValidator;
import au.com.target.tgtfulfilment.fulfilmentservice.ConsignmentTrackingIdGenerator;
import au.com.target.tgtfulfilment.fulfilmentservice.PickConsignmentUpdater;
import au.com.target.tgtfulfilment.fulfilmentservice.PickTicketSender;
import au.com.target.tgtfulfilment.fulfilmentservice.PickTypeAscertainerStrategy;
import au.com.target.tgtfulfilment.fulfilmentservice.PickValidator;
import au.com.target.tgtfulfilment.fulfilmentservice.TargetFulfilmentService;
import au.com.target.tgtfulfilment.logger.InstoreFulfilmentLogger;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;
import au.com.target.tgtfulfilment.ordercancel.PickCancelService;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntry;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntryList;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;
import au.com.target.tgtfulfilment.util.LoggingContext;





/**
 * Default implementation of {@link TargetFulfilmentService}
 * 
 */
public class TargetFulfilmentServiceImpl extends AbstractBusinessService implements TargetFulfilmentService {
    // legacy carrier code logic can be removed first opportunity in 2014
    private static final String LEGACY_CARRIER_CODE = "XL";

    private static final Integer AUTO_PARCEL_COUNT = Integer.valueOf(0);

    private static final Set<OrderStatus> ORDER_STATUS_ALLOWED_FOR_ACK = new ImmutableSet.Builder<OrderStatus>()
            .add(OrderStatus.INPROGRESS)
            .build();

    private static final Set<OrderStatus> ORDER_STATUS_ALLOWED_FOR_PICK_CONF = new ImmutableSet.Builder<OrderStatus>()
            .addAll(ORDER_STATUS_ALLOWED_FOR_ACK)
            .add(OrderStatus.INVOICED) // Handling for second layby picks
            .build();

    private static final Set<OrderStatus> ORDER_STATUS_ALLOWED_FOR_SHIP_CONF = new ImmutableSet.Builder<OrderStatus>()
            .addAll(ORDER_STATUS_ALLOWED_FOR_PICK_CONF)
            .build();


    private static final Logger LOG = Logger.getLogger(TargetFulfilmentServiceImpl.class);
    private static final InstoreFulfilmentLogger INSTORE_LOGGER = new InstoreFulfilmentLogger(
            TargetFulfilmentServiceImpl.class);

    private TargetOrderDao targetOrderDao;

    private TargetBusinessProcessService targetBusinessProcessService;

    private PickConsignmentUpdater pickConsignmentUpdater;

    private ConsignmentParcelUpdater consignmentParcelUpdater;

    private PickValidator pickValidator;

    private PickTicketSender pickTicketSender;

    private PickTypeAscertainerStrategy pickTypeAscertainerStrategy;

    private PickCancelService pickCancelService;

    private TargetConsignmentService targetConsignmentService;

    private TargetStoreConsignmentService targetStoreConsignmentService;

    private ConsignmentTrackingIdGenerator consignmentTrackingIdGenerator;

    private ConsignmentStatusValidator instoreFulfilmentConsignmentStatusValidator;

    private ConsignmentStatusValidator fastlineFulfilmentConsignmentStatusValidator;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private FluentFulfilmentService fluentFulfilmentService;

    @Override
    public void processAckForConsignment(final String orderCode, final TargetConsignmentModel consignment,
            final LoggingContext loggingCtx)
                    throws FulfilmentException,
                    ConsignmentStatusValidationException {

        Assert.notNull(orderCode, "orderCode cannot be null");
        Assert.notNull(consignment, "consignment cannot be null");
        Assert.isTrue(StringUtils.isBlank(consignment.getOrder().getFluentId()),
                "order:" + consignment.getOrder().getCode() + " is a fluent order");
        final WarehouseModel warehouse = consignment.getWarehouse();
        Assert.notNull(warehouse, "warehouse cannot be null");
        validateOrderStatus(orderCode, ORDER_STATUS_ALLOWED_FOR_ACK);

        fastlineFulfilmentConsignmentStatusValidator.validateConsignmentStatus(consignment.getStatus(),
                ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);

        setConsignmentStatusConfirmed(consignment);

        LOG.info(LoggingContext.getSelectedContext(loggingCtx) + "Warehouse=" + warehouse.getName()
                + " action=Ack  order=" + orderCode
                + " consignment="
                + consignment.getCode());
    }

    @Override
    public void processShipConfForConsignment(final String orderCode, final TargetConsignmentModel consignment,
            final Date shippedDate, final LoggingContext loggingContext)
                    throws FulfilmentException, ConsignmentStatusValidationException {

        if (consignment == null) {
            throw new FulfilmentException("Ship conf consignment is null");
        }

        if (StringUtils.isNotBlank(consignment.getOrder().getFluentId())) {
            throw new FulfilmentException("order:" + orderCode + " is a fluent order");
        }

        if (orderCode == null) {
            throw new FulfilmentException("Ship conf has null order code");
        }

        if (shippedDate == null) {
            throw new FulfilmentException("Ship conf has null ship date");
        }

        final WarehouseModel warehouse = consignment.getWarehouse();
        if (warehouse == null) {
            throw new FulfilmentException("Warehouse cannot be null");
        }


        final OrderModel orderModel = validateOrderStatus(orderCode, ORDER_STATUS_ALLOWED_FOR_SHIP_CONF);

        // Some purchase options don't allow a ship unless the order is invoiced
        if (!OrderStatus.INVOICED.equals(orderModel.getStatus())
                && BooleanUtils.isFalse(orderModel.getPurchaseOptionConfig().getAllowShipBeforeInvoiced())) {
            throw new FulfilmentException("For order of type " + orderModel.getPurchaseOptionConfig().getCode()
                    + " ship conf not allowed for order in status " + orderModel.getStatus());
        }

        fastlineFulfilmentConsignmentStatusValidator.validateConsignmentStatus(consignment.getStatus(),
                ConsignmentStatus.SHIPPED);

        // If not picked (ship before pick), then just flag that ship conf received
        if (!ConsignmentStatus.PICKED.equals(consignment.getStatus())) {
            LOG.info(LoggingContext.getSelectedContext(loggingContext) + "Warehouse=" + warehouse.getName()
                    + " action=ShipBeforePick order=" + orderCode
                    + " consignment="
                    + consignment.getCode());
            setConsignmentShipReceived(consignment, shippedDate);
            return;
        }

        // Otherwise do the full shipped process
        consignment.setShippingDate(shippedDate);
        setConsignmentStatusShipped(consignment);

        targetBusinessProcessService.startOrderConsignmentProcess(orderModel, consignment,
                TgtbusprocConstants.BusinessProcess.ORDER_COMPLETION_PROCESS);

        LOG.info(LoggingContext.getSelectedContext(loggingContext) + "Warehouse=" + warehouse.getName()
                + " action=ShipConf order=" + orderCode + " consignment=" + consignment.getCode());
    }

    @Override
    public void processPickConfForConsignment(final String orderCode, final TargetConsignmentModel consignment,
            final String carrier,
            final String trackingNumber, final Integer parcelCount, final List<PickConfirmEntry> pickConfirmEntries,
            final LoggingContext loggingContext)
                    throws FulfilmentException {

        Assert.notNull(orderCode, "OrderCode cannot be null");
        Assert.notNull(consignment, "Consignment cannot be null");
        Assert.isTrue(StringUtils.isBlank(consignment.getOrder().getFluentId()),
                "order:" + consignment.getOrder().getCode() + " is a fluent order");
        final WarehouseModel warehouse = consignment.getWarehouse();
        Assert.notNull(warehouse, "Warehouse cannot be null");

        LOG.info("Processing Pick Conf for order:" + orderCode);

        LOG.info(LoggingContext.getSelectedContext(loggingContext) + "Warehouse=" + warehouse.getName()
                + " action=PickConf order=" + orderCode
                + " consignment="
                + consignment.getCode());

        // Validate parameters and order status
        final AbstractOrderModel consignmentOrder = consignment.getOrder();

        if (consignmentOrder != null && consignmentOrder instanceof OrderModel) {
            final OrderModel order = (OrderModel)consignmentOrder;
            validatePickConfParameters(order, parcelCount);

            if (!validateOrderStatusForPick(order)) {
                return;
            }

            // Retrieve and validate the consignment
            try {
                fastlineFulfilmentConsignmentStatusValidator.validateConsignmentStatus(consignment.getStatus(),
                        ConsignmentStatus.PICKED);
            }
            catch (final ConsignmentStatusValidationException exp) {
                pickTicketSender.sendTicketInvalidPick(order, exp.getMessage());
                return;
            }

            final String pickConsignmentCode = consignment.getCode();

            if (!validateCarrier(consignment, carrier)) {
                throw new FulfilmentException(
                        "Carrier ID in Hybris does not match Warehouse Pick File.  Recieved carrier ID:"
                                + carrier);
            }

            // Handle second pick
            if (isSecondPick(consignment)) {
                processSecondPick(order, consignment, parcelCount);
                return;
            }


            // Invalid pick has higher precedence than Over pick
            final String invalidPickMsg = pickValidator.isInvalidPick(consignment, pickConfirmEntries);
            if (invalidPickMsg != null) {
                pickTicketSender.sendTicketInvalidPick(order, invalidPickMsg);
                LOG.info("Invalid pick ticket created for order:" + orderCode + " consignment:" + pickConsignmentCode);
                return;
            }

            if (pickValidator.isOverPick(consignment, pickConfirmEntries)) {
                pickTicketSender.sendTicketOverPick(order, consignment, pickConfirmEntries);
                LOG.info("Over pick ticket created for for order:" + orderCode + " consignment:" + pickConsignmentCode);
                return;
            }


            // Create POJO to hold updates
            final PickConsignmentUpdateData updateData = createPickConsignmentUpdateData(pickConfirmEntries,
                    parcelCount,
                    trackingNumber, carrier);


            // Proceed based on pick type
            final PickTypeEnum pickType = pickTypeAscertainerStrategy.ascertainPickType(consignment, updateData);

            switch (pickType) {
                case ZERO:
                    if (pickTicketSender.isEbayOrder(order) || pickTicketSender.isTradeMeOrder(order)) {
                        pickTicketSender.sendTicketShortZeroPick(order, consignment, pickConfirmEntries);
                    }
                    // no break intentionally
                case SHORT:
                    if (BooleanUtils.isFalse(order.getPurchaseOptionConfig().getAllowShortPick())
                            || pickTicketSender.isAKioskOrder(order)) {
                        pickTicketSender.sendTicketShortZeroPick(order, consignment, pickConfirmEntries);
                        return;
                    }
                    try {
                        pickCancelService.startPickCancelProcess(order, consignment, updateData, pickType);
                    }
                    catch (final OrderCancelException e) {
                        LOG.info("Failed Refund occurred for order:" + orderCode);
                        pickTicketSender.sendTicketFailedRefund(order, e.getMessage());
                        return;
                    }
                    break;
                case FULL:
                default:
                    targetBusinessProcessService.startOrderConsignmentPickedProcess(order, consignment, updateData,
                            TgtbusprocConstants.BusinessProcess.CONSIGNMENT_PICKED_PROCESS);
                    LOG.info("Pick process started successfully for order:" + orderCode + " consignment:"
                            + pickConsignmentCode);
                    break;
            }
        }
    }

    @Override
    public void processCompleteForInstoreFulfilment(final String consignmentCode, final int parcelCount,
            final List<ConsignmentParcelDTO> consignmentParcels)
                    throws NotFoundException, ConsignmentStatusValidationException, FulfilmentException,
                    FluentBaseException {
        Assert.notNull(consignmentCode, "Consignment Code cannot be null");
        final TargetConsignmentModel consignment = targetConsignmentService.getConsignmentForCode(consignmentCode);
        Assert.notNull(consignment, "Consignment cannot be null for code : " + consignmentCode);
        final OrderModel order = (OrderModel)consignment.getOrder();
        Assert.notNull(order, "Order cannot be null for consignment with code : " + consignmentCode);
        // We need the order type to continue - should not be null
        final OfcOrderType type = consignment.getOfcOrderType();
        if (type == null) {
            throw new IllegalStateException("Null order type for consignment : " + consignmentCode);
        }
        // Validate based on moving to the PACKED status (but skips to SHIPPED for INSTORE_PICKUP)
        instoreFulfilmentConsignmentStatusValidator.validateConsignmentStatus(consignment.getStatus(),
                ConsignmentStatus.PACKED);

        final PickConsignmentUpdateData updateData = createPickConsignmentUpdateDataForInstore(consignment,
                parcelCount);
        updateConsignmentPickDetails(consignment, updateData, consignmentParcels);

        if (targetConsignmentService.isConsignmentToDefaultWarehouse(consignment)) {
            processCancelOrderItemInConsignmentForShortPick(consignment, order);
            LOG.info(MessageFormat
                    .format(
                            "Cancelled the order items with consignment warehouse same as the default online warehouse for order={0}",
                            order.getCode()));
        }
        if (type.equals(OfcOrderType.INSTORE_PICKUP)) {

            // we set the ship received here and when we get the consignment for ofc, we treat the ship received consignment same as shipped consignment
            // consignment status will be updated to shipped in the order completion process
            setConsignmentShipReceived(consignment, new Date());

            targetBusinessProcessService.startOrderConsignmentProcess(order, consignment,
                    TgtbusprocConstants.BusinessProcess.ORDER_COMPLETION_PROCESS);
        }
        else {
            //Inter store or Home delivery
            if (targetConsignmentService.isAFluentConsignment(consignment)) {
                fluentFulfilmentService.sendConsignmentEventToFluent(consignment,
                        TgtFluentConstants.TargetOfcEvent.OFC_PICK_PACK, true);
            }

            setConsignmentStatusPacked(consignment);

            targetBusinessProcessService.startOrderConsignmentPickedProcess(order, consignment,
                    updateData, TgtbusprocConstants.BusinessProcess.CONSIGNMENT_INSTORE_PICKED_PROCESS);
        }

    }

    @Override
    public void processPickForInstoreFulfilment(final String consignmentCode)
            throws NotFoundException, ConsignmentStatusValidationException {

        Assert.notNull(consignmentCode, "Consignment Code cannot be null");
        final TargetConsignmentModel consignment = targetConsignmentService.getConsignmentForCode(consignmentCode);
        Assert.notNull(consignment, "Consignment cannot be null for code : " + consignmentCode);

        instoreFulfilmentConsignmentStatusValidator.validateConsignmentStatus(consignment.getStatus(),
                ConsignmentStatus.PICKED);
        setConsignmentStatusPicked(consignment);
    }

    @Override
    public void processRejectInstoreFulfilment(final String consignmentCode, final String instoreRejectReason,
            final ConsignmentRejectState rejectState)
                    throws NotFoundException,
                    ConsignmentStatusValidationException, FulfilmentException, FluentBaseException {

        Assert.notNull(consignmentCode, "Consignment Code cannot be null");
        final TargetConsignmentModel consignment = targetConsignmentService.getConsignmentForCode(consignmentCode);
        Assert.notNull(consignment, "Consignment cannot be null for code : " + consignmentCode);

        instoreFulfilmentConsignmentStatusValidator.validateConsignmentStatus(consignment.getStatus(),
                ConsignmentStatus.CANCELLED);

        final ConsignmentRejectReason rejectReason = determineConsignmentRejectReason(consignment.getStatus());
        INSTORE_LOGGER.logStoreConsignmentInstoreRejectReason(rejectReason,
                targetStoreConsignmentService.getAssignedStoreForConsignmentAsInteger(consignment),
                targetStoreConsignmentService.getOrderCodeFromConsignment(consignment),
                consignment.getCode(), instoreRejectReason, rejectState,
                targetStoreConsignmentService.getDeliverFromStateFromConsignment(consignment),
                targetStoreConsignmentService.getDeliverToStoreNumberFromConsignment(consignment),
                targetStoreConsignmentService.getDeliverToStateFromConsignment(consignment));

        consignment.setInstoreRejectReason(instoreRejectReason);
        consignment.setRejectState(rejectState);

        udpateShippedQtyInConsignmentEntryForZeroPick(consignment);

        if (targetConsignmentService.isAFluentConsignment(consignment)) {
            fluentFulfilmentService.sendConsignmentEventToFluent(consignment,
                    getFluentOfcEventType(consignment),
                    true);
            setConsignmentStatusCancelled(consignment, rejectReason);
        }
        else {
            setConsignmentStatusCancelled(consignment, rejectReason);
            final OrderModel orderModel = (OrderModel)consignment.getOrder();
            Assert.notNull(orderModel, "Order cannot be null for consignment with code : " + consignmentCode);

            if (targetConsignmentService.isConsignmentToDefaultWarehouse(consignment)) {
                processCancelOrderItemInConsignmentForShortPick(consignment, orderModel);
                LOG.info(MessageFormat
                        .format(
                                "Reject the entire order items with consignment warehouse same as the default online warehouse for order={0}",
                                orderModel.getCode()));
            }
            else {
                targetBusinessProcessService.startConsignmentRejectionProcess(orderModel, consignment, rejectReason,
                        TgtbusprocConstants.BusinessProcess.REROUTE_CONSIGNMENT);
                LOG.info("Re-routing consignment process started successfully for order:" + orderModel.getCode()
                        + " consignment:"
                        + consignmentCode);
            }
        }
    }

    private String getFluentOfcEventType(final TargetConsignmentModel consignment) {
        String ofcEvent = TgtFluentConstants.TargetOfcEvent.OFC_PICK_PACK;
        if (consignment.getOfcOrderType() == OfcOrderType.INSTORE_PICKUP) {
            ofcEvent = TgtFluentConstants.TargetOfcEvent.OFC_SS_CNC_SHIPPED;
        }
        return ofcEvent;
    }

    @Override
    public void processCancelOrderItemInConsignmentForShortPick(final TargetConsignmentModel consignment,
            final OrderModel order) throws FulfilmentException {
        final TargetOrderCancelEntryList targetOrderCancelEntryList = getOrderCancelEntriesForShortPick(consignment);
        if (CollectionUtils.isNotEmpty(targetOrderCancelEntryList.getEntriesToCancel())) {
            targetBusinessProcessService.startOrderItemCancelProcess(order, targetOrderCancelEntryList);
        }
    }

    @Override
    public TargetOrderCancelEntryList getOrderCancelEntriesForShortPick(final TargetConsignmentModel consignment)
            throws FulfilmentException {
        final Set<ConsignmentEntryModel> consignmentEntries = consignment.getConsignmentEntries();
        if (CollectionUtils.isEmpty(consignmentEntries)) {
            throw new FulfilmentException("Consignment Entries not present for consignment=" + consignment.getCode());
        }
        final TargetOrderCancelEntryList targetOrderCancelEntryList = new TargetOrderCancelEntryList();
        final List<TargetOrderCancelEntry> targetOrderCancelEntries = new ArrayList<>();

        for (final ConsignmentEntryModel consignmentEntry : consignmentEntries) {
            if (consignmentEntry == null || consignmentEntry.getOrderEntry() == null
                    || consignmentEntry.getOrderEntry().getProduct() == null) {
                throw new FulfilmentException("Consignment Entry invalid for consignment=" + consignment.getCode());
            }
            final TargetOrderCancelEntry entry = new TargetOrderCancelEntry();
            final Long totalQuantity = consignmentEntry.getQuantity();
            final Long shippedQuantity = consignmentEntry.getShippedQuantity();
            final Long cancelQuantity = (Long)MathUtils.subtract(totalQuantity, shippedQuantity);
            if (cancelQuantity.longValue() > 0) {
                entry.setCancelQuantity(cancelQuantity);
                entry.setOrderEntry(consignmentEntry.getOrderEntry());
                targetOrderCancelEntries.add(entry);
            }
            targetOrderCancelEntryList.setEntriesToCancel(targetOrderCancelEntries);
        }
        return targetOrderCancelEntryList;
    }

    private void udpateShippedQtyInConsignmentEntryForZeroPick(final TargetConsignmentModel consignment) {
        try {
            final List<PickConfirmEntry> pickConfirmEntries = mockPickConfirmEntriesForZeroPick(consignment);
            pickConsignmentUpdater.updateConsignmentShipQuantities(consignment, pickConfirmEntries);
        }
        catch (final FulfilmentException e) {
            LOG.error("FulfilmentException", e);
        }
    }

    @Override
    public void processWavedForInstoreFulfilment(final String consignmentCode) throws NotFoundException,
            ConsignmentStatusValidationException, FluentBaseException {

        Assert.notNull(consignmentCode, "Consignment Code cannot be null");
        final TargetConsignmentModel consignment = targetConsignmentService.getConsignmentForCode(consignmentCode);
        Assert.notNull(consignment, "Consignment cannot be null for code : " + consignmentCode);

        instoreFulfilmentConsignmentStatusValidator.validateConsignmentStatus(consignment.getStatus(),
                ConsignmentStatus.WAVED);

        if (targetConsignmentService.isAFluentConsignment(consignment)) {
            fluentFulfilmentService.sendConsignmentEventToFluent(consignment,
                    TgtFluentConstants.TargetOfcEvent.OFC_WAVED, true);
        }
        setConsignmentStatusWaved(consignment);
    }

    @Override
    public void processRerouteAutoTimeoutConsignment(final TargetConsignmentModel consignmentModel,
            final ConsignmentRejectReason reason)
                    throws NotFoundException, ConsignmentStatusValidationException, FluentBaseException {

        Assert.notNull(consignmentModel, "Consignment Model can not be null");
        Assert.notNull(reason, "Rejection reason can not be null");

        final String consignmentCode = consignmentModel.getCode();
        final ConsignmentStatus status = consignmentModel.getStatus();

        instoreFulfilmentConsignmentStatusValidator.validateConsignmentStatus(status,
                ConsignmentStatus.CANCELLED);


        INSTORE_LOGGER.logStoreConsignmentRejectReason(reason,
                targetStoreConsignmentService.getAssignedStoreForConsignmentAsInteger(consignmentModel),
                targetStoreConsignmentService.getOrderCodeFromConsignment(consignmentModel),
                consignmentModel.getCode(),
                targetStoreConsignmentService.getDeliverFromStateFromConsignment(consignmentModel),
                targetStoreConsignmentService.getDeliverToStoreNumberFromConsignment(consignmentModel),
                targetStoreConsignmentService.getDeliverToStateFromConsignment(consignmentModel));

        udpateShippedQtyInConsignmentEntryForZeroPick(consignmentModel);

        if (targetConsignmentService.isAFluentConsignment(consignmentModel)) {
            fluentFulfilmentService.sendConsignmentEventToFluent(consignmentModel,
                    getFluentOfcEventType(consignmentModel),
                    true);
            setConsignmentStatusCancelled(consignmentModel, reason);
        }
        else {
            setConsignmentStatusCancelled(consignmentModel, reason);
            final OrderModel orderModel = (OrderModel)consignmentModel.getOrder();
            Assert.notNull(orderModel, "Order cannot be null for consignment with code : " + consignmentCode);

            targetBusinessProcessService.startConsignmentRejectionProcess(orderModel, consignmentModel,
                    reason, TgtbusprocConstants.BusinessProcess.REROUTE_CONSIGNMENT);
            LOG.info("Re-routing consignment process started successfully for order:" + orderModel.getCode()
                    + " consignment:"
                    + consignmentCode);
        }
    }


    private PickConsignmentUpdateData createPickConsignmentUpdateData(final List<PickConfirmEntry> pickConfirmEntries,
            final Integer parcelCount,
            final String trackingNumber, final String carrier) {

        final PickConsignmentUpdateData data = new PickConsignmentUpdateData();

        data.setPickEntries(pickConfirmEntries);
        data.setParcelCount(parcelCount);
        data.setCarrier(carrier);
        data.setTrackingNumber(trackingNumber);

        return data;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void processConsignmentAutoActions(final TargetConsignmentModel targetConsignment)
            throws FulfilmentException,
            ConsignmentStatusValidationException {
        Assert.notNull(targetConsignment, "Consignment cannot be null");
        final AbstractOrderModel order = targetConsignment.getOrder();
        Assert.notNull(order, "order cannot be null");
        final String orderCode = order.getCode();
        final String consignmentCode = targetConsignment.getCode();

        final WarehouseModel warehouse = targetConsignment.getWarehouse();
        Assert.notNull(warehouse, "warehouse should be associated to the consignemnt");
        final WarehouseAutoTrigger warehouseAutoTrigger = warehouse.getWarehouseAutoTrigger();
        if (null == warehouseAutoTrigger) {
            return;
        }
        if (warehouseAutoTrigger.equals(WarehouseAutoTrigger.NONE)) {
            LOG.info(LoggingContext.getSelectedContext(LoggingContext.AUTO_ACK) + "action=None consignment="
                    + consignmentCode
                    + " order=" + orderCode);
            return;
        }
        if (null != targetConsignment.getStatus()
                && targetConsignment.getStatus().equals(ConsignmentStatus.SENT_TO_WAREHOUSE)) {
            try {
                if (warehouseAutoTrigger.equals(WarehouseAutoTrigger.SHIP)
                        || (warehouseAutoTrigger.equals(WarehouseAutoTrigger.ACK)
                                && isDigitalProduct(targetConsignment))) {
                    processAckForConsignment(orderCode, targetConsignment, LoggingContext.AUTO_ACK);
                    setConsignmentShipReceived(targetConsignment, new Date());
                    processPickConfForConsignment(orderCode, targetConsignment, getCarrier(targetConsignment),
                            null, AUTO_PARCEL_COUNT, getPickConfirmEntries(targetConsignment), LoggingContext.AUTO_ACK);
                }
                else if (warehouseAutoTrigger.equals(WarehouseAutoTrigger.ACK)) {
                    processAckForConsignment(orderCode, targetConsignment, LoggingContext.AUTO_ACK);
                }
                else if (warehouseAutoTrigger.equals(WarehouseAutoTrigger.PICK)) {
                    processAckForConsignment(orderCode, targetConsignment, LoggingContext.AUTO_ACK);
                    processPickConfForConsignment(orderCode, targetConsignment, getCarrier(targetConsignment),
                            null, AUTO_PARCEL_COUNT, getPickConfirmEntries(targetConsignment), LoggingContext.AUTO_ACK);
                }
            }
            catch (final Exception exception) {
                LOG.error(
                        LoggingContext.getSelectedContext(LoggingContext.AUTO_ACK) + "Warehouse=" + warehouse.getName()
                                + "action=Error consignment="
                                + consignmentCode + " order=" + orderCode);
                throw new IllegalStateException("Exception while performing auto ack actions", exception);
            }
        }

    }


    /**
     * Verify whether the consignment has digital product,just check in the first entry
     * 
     * @param consignment
     * @return boolean
     */
    private boolean isDigitalProduct(final TargetConsignmentModel consignment) {
        final Set<ConsignmentEntryModel> consignmentEntries = consignment.getConsignmentEntries();
        final Iterator<ConsignmentEntryModel> iterator = consignmentEntries.iterator();
        if (null != iterator && iterator.hasNext()) {
            final ConsignmentEntryModel entry = iterator.next();
            final ProductModel productModel = entry.getOrderEntry().getProduct();
            final String prdType = ProductUtil.getProductTypeCode(productModel);
            return ProductType.DIGITAL.equalsIgnoreCase(prdType);
        }
        return false;
    }



    /**
     * 
     * @param consignment
     * @return PickConsignmentUpdateData
     */
    protected PickConsignmentUpdateData createPickConsignmentUpdateDataForInstore(
            final TargetConsignmentModel consignment, final int parcelCount) {

        final PickConsignmentUpdateData data = new PickConsignmentUpdateData();
        // The TargetCarrier should have been set during creation of consignment in the split order strategy
        // Since the carrier is not passed we will set it to match.
        data.setCarrier(getCarrier(consignment));

        data.setParcelCount(Integer.valueOf(parcelCount));

        final OfcOrderType ofcOrderType = consignment.getOfcOrderType();
        if (null != ofcOrderType) {
            if (ofcOrderType.equals(OfcOrderType.INTERSTORE_DELIVERY)
                    || ofcOrderType.equals(OfcOrderType.CUSTOMER_DELIVERY)) {
                final TargetPointOfServiceModel tpos = getPointOfServiceForConsignment(consignment);
                if (null != tpos && null != tpos.getFulfilmentCapability()) {
                    data.setTrackingNumber(consignmentTrackingIdGenerator.generateTrackingIdForConsignment(tpos
                            .getFulfilmentCapability()));
                }
            }
        }
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)) {
            data.setPickEntries(getPickConfirmEntriesFromShippedQty(consignment));
        }
        else if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)) {
            data.setPickEntries(getPickConfirmEntriesFromShippedQty(consignment));
        }
        else {
            data.setPickEntries(getPickConfirmEntries(consignment));
        }

        return data;
    }

    private String getCarrier(final TargetConsignmentModel consignment) {
        if (null != consignment.getTargetCarrier()) {
            return consignment.getTargetCarrier().getWarehouseCode();
        }
        return null;
    }

    protected List<PickConfirmEntry> getPickConfirmEntries(final TargetConsignmentModel consignment) {
        final List<PickConfirmEntry> pickConfirmEntries = new ArrayList<>();
        final Iterator<ConsignmentEntryModel> iter = consignment.getConsignmentEntries().iterator();
        if (null != iter) {
            while (iter.hasNext()) {
                final ConsignmentEntryModel cem = iter.next();
                if (null != cem && null != cem.getOrderEntry()) {
                    final PickConfirmEntry entry = new PickConfirmEntry();
                    if (null != cem.getOrderEntry().getProduct()
                            && null != cem.getOrderEntry().getProduct().getCode()) {
                        entry.setItemCode(cem.getOrderEntry().getProduct().getCode());
                    }
                    if (null != cem.getOrderEntry().getQuantity()) {
                        entry.setQuantityShipped(Integer.valueOf(cem.getOrderEntry().getQuantity().intValue()));
                    }
                    pickConfirmEntries.add(entry);
                }
            }
        }
        return pickConfirmEntries;
    }

    protected List<PickConfirmEntry> getPickConfirmEntriesFromShippedQty(final TargetConsignmentModel consignment) {
        final List<PickConfirmEntry> pickConfirmEntries = new ArrayList<>();
        final Iterator<ConsignmentEntryModel> iter = consignment.getConsignmentEntries().iterator();
        if (null != iter) {
            while (iter.hasNext()) {
                final ConsignmentEntryModel cem = iter.next();
                if (null != cem && null != cem.getOrderEntry()) {
                    final PickConfirmEntry entry = new PickConfirmEntry();
                    if (null != cem.getOrderEntry().getProduct()
                            && null != cem.getOrderEntry().getProduct().getCode()) {
                        entry.setItemCode(cem.getOrderEntry().getProduct().getCode());
                    }
                    final int shippedQty = cem.getShippedQuantity() != null ? cem.getShippedQuantity().intValue() : 0;
                    entry.setQuantityShipped(Integer.valueOf(shippedQty));
                    pickConfirmEntries.add(entry);
                }
            }
        }
        return pickConfirmEntries;
    }

    /**
     * Method to get TargetPointOfServiceModel from consignment
     * 
     * @param consignment
     *            the targetConsignmentModel
     * @return TargetPointOfServiceModel
     */
    private TargetPointOfServiceModel getPointOfServiceForConsignment(final TargetConsignmentModel consignment) {

        return targetStoreConsignmentService.getAssignedStoreForConsignment(consignment);
    }

    private void validatePickConfParameters(final OrderModel order,
            final Integer parcelCount)
                    throws FulfilmentException {
        if (order == null) {
            throw new FulfilmentException("Cannot find order");
        }

        if (parcelCount == null) {
            throw new FulfilmentException("Parcel count is null");
        }
    }

    /**
     * Test if the carrier is expected given the details already in the consignment.
     * 
     * @param consignment
     * @param carrier
     * @return true if the the carrier is correct
     */
    protected boolean validateCarrier(final TargetConsignmentModel consignment, final String carrier) {
        final TargetCarrierModel carrierModel = consignment.getTargetCarrier();
        return LEGACY_CARRIER_CODE.equals(carrier)
                || ((carrierModel != null) && carrierModel.getWarehouseCode().equals(carrier));
    }

    private boolean isSecondPick(final TargetConsignmentModel consignment) {
        return ConsignmentStatus.PICKED.equals(consignment.getStatus());
    }


    /**
     * Validate if second picks are allowed, and if not send ticket.<br/>
     * Otherwise process the second pick.
     * 
     */
    private void processSecondPick(final OrderModel order, final TargetConsignmentModel consignment,
            final Integer parcelCount) {

        if (BooleanUtils.isFalse(order.getPurchaseOptionConfig().getAllowMultiplePicks())) {
            pickTicketSender.sendTicketInvalidPick(order,
                    "Second pick received but multiple picks are not allowed for purchase option "
                            + order.getPurchaseOptionConfig().getCode());
            return;
        }

        consignmentParcelUpdater.updateParcelCount(consignment, parcelCount);

        // If already invoiced and a second pick received, then start a business process (sends cnc extract to pos)
        if (OrderStatus.INVOICED.equals(order.getStatus())) {

            targetBusinessProcessService.startOrderProcess(order,
                    TgtbusprocConstants.BusinessProcess.LAYBY_SECOND_PICK_INVOICED);
        }
    }


    private OrderModel validateOrderStatus(final String orderNumber, final Set<OrderStatus> orderStatusList)
            throws FulfilmentException {

        final OrderModel orderModel = targetOrderDao.findOrderModelForOrderId(orderNumber);

        if (orderModel == null) {
            throw new FulfilmentException("No order found with code:" + orderNumber);
        }

        validateOrderModelStatus(orderModel, orderStatusList);
        return orderModel;
    }

    private void validateOrderModelStatus(final OrderModel orderModel, final Set<OrderStatus> orderStatusList)
            throws FulfilmentException {

        if (!orderStatusList.contains(orderModel.getStatus())) {
            throw new FulfilmentException("Order is not in correct status. Expected status list:"
                    + orderStatusList + " Current status:" + orderModel.getStatus());
        }
    }

    private boolean validateOrderStatusForPick(final OrderModel order) {

        try {
            validateOrderModelStatus(order, ORDER_STATUS_ALLOWED_FOR_PICK_CONF);
        }
        catch (final FulfilmentException exp) {
            pickTicketSender.sendTicketInvalidPick(order, exp.getMessage());
            return false;
        }

        // Pick in invoiced state is only allowed if the poc allows multiple picks
        if (OrderStatus.INVOICED.equals(order.getStatus())
                && BooleanUtils.isFalse(order.getPurchaseOptionConfig().getAllowMultiplePicks())) {

            pickTicketSender.sendTicketInvalidPick(order,
                    "Pick received for INVOICED order but multiple picks are not allowed for purchase option "
                            + order.getPurchaseOptionConfig().getCode());
            return false;
        }

        return true;
    }

    /**
     * Determines the consignment reject reason based the status of the consignment
     * 
     * @param status
     * @return the correct reject reason for the consignment
     */
    protected ConsignmentRejectReason determineConsignmentRejectReason(final ConsignmentStatus status) {
        if (status != null && (ConsignmentStatus.WAVED == status || ConsignmentStatus.PICKED == status)) {
            return ConsignmentRejectReason.PICK_FAILED;
        }

        return ConsignmentRejectReason.REJECTED_BY_STORE;
    }

    /**
     * Find the right consignment for fulfilment action, by the given code if supplied. If not we return the first
     * active one.
     * 
     * @param orderModel
     * @param consignmentCode
     * @return ConsignmentModel
     * @throws FulfilmentException
     */
    protected TargetConsignmentModel getOrderConsignment(final OrderModel orderModel, final String consignmentCode)
            throws FulfilmentException {
        Assert.notNull(orderModel, "orderModel cannot be null");

        if (CollectionUtils.isEmpty(orderModel.getConsignments())) {
            throw new FulfilmentException("No consignments found for the order:" + orderModel.getCode());
        }

        if (null != consignmentCode) {

            return getConsignmentByCode(orderModel.getConsignments(), consignmentCode);
        }

        // In future fastline will pass the code in, but for now need to handle just the order code
        // and assuming just one active consignment.
        final List<TargetConsignmentModel> activeConsignments = targetConsignmentService
                .getActiveConsignmentsForOrder(orderModel);

        if (CollectionUtils.isEmpty(activeConsignments)) {
            throw new FulfilmentException("No active consignment found for the order:"
                    + orderModel.getCode());
        }

        return activeConsignments.get(0);

    }

    private TargetConsignmentModel getConsignmentByCode(final Set<ConsignmentModel> consignments, final String code)
            throws FulfilmentException {
        Assert.notNull(consignments, "consignments cannot be null");
        Assert.notNull(code, "code cannot be null");

        for (final ConsignmentModel consignmentModel : consignments) {

            if (code.equals(consignmentModel.getCode()) && consignmentModel instanceof TargetConsignmentModel) {
                return (TargetConsignmentModel)consignmentModel;
            }
        }

        throw new FulfilmentException("No consignment found in order for code " + code);
    }


    private void setConsignmentStatusConfirmed(final TargetConsignmentModel consignment) {
        consignment.setStatus(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        getModelService().save(consignment);

    }

    /**
     * Set ship received flag and date only (for case of ship before pick)
     * 
     * @param consignment
     * @param shippedDate
     */
    private void setConsignmentShipReceived(final TargetConsignmentModel consignment, final Date shippedDate) {

        consignment.setShippingDate(shippedDate);
        consignment.setShipConfReceived(Boolean.TRUE);
        getModelService().save(consignment);

    }

    private void setConsignmentStatusShipped(final TargetConsignmentModel consignment) {
        consignment.setStatus(ConsignmentStatus.SHIPPED);
        getModelService().save(consignment);

    }

    private void setConsignmentStatusPacked(final TargetConsignmentModel consignment) {
        consignment.setStatus(ConsignmentStatus.PACKED);
        getModelService().save(consignment);
    }

    private void setConsignmentStatusPicked(final TargetConsignmentModel consignment) {
        consignment.setStatus(ConsignmentStatus.PICKED);
        getModelService().save(consignment);
    }

    private void setConsignmentStatusWaved(final TargetConsignmentModel consignment) {
        consignment.setStatus(ConsignmentStatus.WAVED);
        getModelService().save(consignment);
    }

    private void setConsignmentStatusCancelled(final TargetConsignmentModel consignment,
            final ConsignmentRejectReason reason) {
        consignment.setStatus(ConsignmentStatus.CANCELLED);
        consignment.setRejectReason(reason);
        getModelService().save(consignment);
    }

    private void updateConsignmentPickDetails(final TargetConsignmentModel consignment,
            final PickConsignmentUpdateData consignmentUpdateData,
            final List<ConsignmentParcelDTO> consignmentParcels) {
        consignmentParcelUpdater.updateParcelDetails(consignment, consignmentParcels);
        // we don't need to update the shipped qty for falcon because it is already saved in ofc
        if (!targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)) {
            pickConsignmentUpdater.updateConsignmentShipQuantities(consignment, consignmentUpdateData.getPickEntries());
        }
        pickConsignmentUpdater.updateConsignmentDetails(consignment, consignmentUpdateData.getParcelCount(),
                consignmentUpdateData.getTrackingNumber(), consignmentUpdateData.getCarrier());
    }

    @Override
    public void processCancelForConsignment(final String consignmentCode, final LoggingContext loggingCtx)
            throws FulfilmentException, ConsignmentStatusValidationException, NotFoundException {
        Assert.notNull(consignmentCode, "consignment code must not be null");
        final TargetConsignmentModel consignment = targetConsignmentService.getConsignmentForCode(consignmentCode);
        fastlineFulfilmentConsignmentStatusValidator.validateConsignmentStatus(consignment.getStatus(),
                ConsignmentStatus.CANCELLED);
        if (consignment.getOrder() == null) {
            throw new FulfilmentException("Order association missing for consignment=" + consignment.getCode());
        }
        final TargetCarrierModel targetCarrier = consignment.getTargetCarrier();
        if (targetCarrier == null) {
            throw new FulfilmentException("Carrier Missing for consignment=" + consignment.getCode());
        }
        final String orderCode = consignment.getOrder().getCode();
        final List<PickConfirmEntry> pickConfirmEntries = mockPickConfirmEntriesForZeroPick(consignment);

        processPickConfForConsignment(orderCode, consignment, targetCarrier.getWarehouseCode(), null,
                Integer.valueOf(0), pickConfirmEntries,
                loggingCtx);
    }

    private List<PickConfirmEntry> mockPickConfirmEntriesForZeroPick(final TargetConsignmentModel consignment)
            throws FulfilmentException {
        final List<PickConfirmEntry> pickConfirmEntries = new ArrayList<>();
        final Set<ConsignmentEntryModel> consignmentEntries = consignment.getConsignmentEntries();
        if (CollectionUtils.isEmpty(consignmentEntries)) {
            throw new FulfilmentException("Consignment Entries not present for consignment=" + consignment.getCode());
        }
        for (final ConsignmentEntryModel consignmentEntry : consignmentEntries) {
            final PickConfirmEntry pickConfirmEntry = new PickConfirmEntry();
            if (consignmentEntry == null || consignmentEntry.getOrderEntry() == null
                    || consignmentEntry.getOrderEntry().getProduct() == null) {
                throw new FulfilmentException("Consignment Entry invalid for consignment=" + consignment.getCode());
            }
            pickConfirmEntry.setItemCode(consignmentEntry.getOrderEntry().getProduct().getCode());
            pickConfirmEntry.setQuantityShipped(Integer.valueOf(0));
            pickConfirmEntries.add(pickConfirmEntry);
        }
        return pickConfirmEntries;
    }

    private List<PickConfirmEntry> mockPickConfirmEntriesForFullPick(final TargetConsignmentModel consignment)
            throws FulfilmentException {
        final List<PickConfirmEntry> pickConfirmEntries = new ArrayList<>();
        final Set<ConsignmentEntryModel> consignmentEntries = consignment.getConsignmentEntries();
        if (CollectionUtils.isEmpty(consignmentEntries)) {
            throw new FulfilmentException("Consignment Entries not present for consignment=" + consignment.getCode());
        }
        for (final ConsignmentEntryModel consignmentEntry : consignmentEntries) {
            final PickConfirmEntry pickConfirmEntry = new PickConfirmEntry();
            if (consignmentEntry == null || consignmentEntry.getOrderEntry() == null
                    || consignmentEntry.getOrderEntry().getProduct() == null
                    || consignmentEntry.getOrderEntry().getQuantity() == null) {
                throw new FulfilmentException("Consignment Entry invalid for consignment=" + consignment.getCode());
            }
            pickConfirmEntry.setItemCode(consignmentEntry.getOrderEntry().getProduct().getCode());
            pickConfirmEntry
                    .setQuantityShipped(Integer.valueOf(consignmentEntry.getOrderEntry().getQuantity().toString()));
            pickConfirmEntries.add(pickConfirmEntry);
        }
        return pickConfirmEntries;
    }

    @Override
    public void processCompleteConsignment(final String consignmentId,
            final Date shippedDate,
            final String carrier, final String trackingNumber, final Integer parcelCount,
            final LoggingContext loggingCtx)
                    throws FulfilmentException, ConsignmentStatusValidationException, NotFoundException {
        final TargetConsignmentModel consignment = targetConsignmentService
                .getConsignmentForCode(consignmentId);
        final AbstractOrderModel order = consignment.getOrder();
        Assert.notNull(order, "Consignment does not have an order associated");
        final String orderCode = order.getCode();
        //process ship first as it guarantees that it will do ship as well as pick 
        //as the ship will a flag to indicate that the ship has been received and pick will trigger the processing of the ship
        processShipConfForConsignment(orderCode, consignment, shippedDate, loggingCtx);
        processPickConfForConsignment(orderCode, consignment, carrier, trackingNumber, parcelCount,
                mockPickConfirmEntriesForFullPick(consignment),
                loggingCtx);
    }

    /**
     * @param targetBusinessProcessService
     *            the targetBusinessProcessService to set
     */
    @Required
    public void setTargetBusinessProcessService(
            final TargetBusinessProcessService targetBusinessProcessService) {
        this.targetBusinessProcessService = targetBusinessProcessService;
    }

    /**
     * @param consignmentParcelUpdater
     *            the consignmentParcelUpdater to set
     */
    @Required
    public void setConsignmentParcelUpdater(final ConsignmentParcelUpdater consignmentParcelUpdater) {
        this.consignmentParcelUpdater = consignmentParcelUpdater;
    }

    /**
     * @param targetOrderDao
     *            the targetOrderDao to set
     */
    @Required
    public void setTargetOrderDao(final TargetOrderDao targetOrderDao) {
        this.targetOrderDao = targetOrderDao;
    }

    /**
     * @param pickValidator
     *            the pickValidator to set
     */
    @Required
    public void setPickValidator(final PickValidator pickValidator) {
        this.pickValidator = pickValidator;
    }

    /**
     * @param pickTicketSender
     *            the pickTicketSender to set
     */
    @Required
    public void setPickTicketSender(final PickTicketSender pickTicketSender) {
        this.pickTicketSender = pickTicketSender;
    }

    /**
     * @param pickTypeAscertainerStrategy
     *            the pickTypeAscertainerStrategy to set
     */
    @Required
    public void setPickTypeAscertainerStrategy(final PickTypeAscertainerStrategy pickTypeAscertainerStrategy) {
        this.pickTypeAscertainerStrategy = pickTypeAscertainerStrategy;
    }

    /**
     * @param pickCancelService
     *            the pickCancelService to set
     */
    @Required
    public void setPickCancelService(final PickCancelService pickCancelService) {
        this.pickCancelService = pickCancelService;
    }

    /**
     * @param targetConsignmentService
     *            the targetConsignmentService to set
     */
    @Required
    public void setTargetConsignmentService(final TargetConsignmentService targetConsignmentService) {
        this.targetConsignmentService = targetConsignmentService;
    }

    /**
     * @param targetStoreConsignmentService
     *            the targetStoreConsignmentService to set
     */
    @Required
    public void setTargetStoreConsignmentService(final TargetStoreConsignmentService targetStoreConsignmentService) {
        this.targetStoreConsignmentService = targetStoreConsignmentService;
    }

    /**
     * @param consignmentTrackingIdGenerator
     *            the consignmentTrackingIdGenerator to set
     */
    @Required
    public void setConsignmentTrackingIdGenerator(final ConsignmentTrackingIdGenerator consignmentTrackingIdGenerator) {
        this.consignmentTrackingIdGenerator = consignmentTrackingIdGenerator;
    }

    /**
     * @param pickConsignmentUpdater
     *            the pickConsignmentUpdater to set
     */
    @Required
    public void setPickConsignmentUpdater(final PickConsignmentUpdater pickConsignmentUpdater) {
        this.pickConsignmentUpdater = pickConsignmentUpdater;
    }

    /**
     * @param instoreFulfilmentConsignmentStatusValidator
     *            the instoreFulfilmentConsignmentStatusValidator to set
     */
    @Required
    public void setInstoreFulfilmentConsignmentStatusValidator(
            final ConsignmentStatusValidator instoreFulfilmentConsignmentStatusValidator) {
        this.instoreFulfilmentConsignmentStatusValidator = instoreFulfilmentConsignmentStatusValidator;
    }

    /**
     * @param fastlineFulfilmentConsignmentStatusValidator
     *            the fastlineFulfilmentConsignmentStatusValidator to set
     */
    @Required
    public void setFastlineFulfilmentConsignmentStatusValidator(
            final ConsignmentStatusValidator fastlineFulfilmentConsignmentStatusValidator) {
        this.fastlineFulfilmentConsignmentStatusValidator = fastlineFulfilmentConsignmentStatusValidator;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * @param fluentFulfilmentService
     *            the fluentFulfilmentService to set
     */
    @Required
    public void setFluentFulfilmentService(final FluentFulfilmentService fluentFulfilmentService) {
        this.fluentFulfilmentService = fluentFulfilmentService;
    }
}
