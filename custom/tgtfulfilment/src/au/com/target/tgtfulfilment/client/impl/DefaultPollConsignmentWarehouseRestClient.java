/**
 * 
 */
package au.com.target.tgtfulfilment.client.impl;

import au.com.target.tgtfulfilment.client.PollConsignmentWarehouseRestClient;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseRequest;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseResponse;


/**
 * default implementation of {@link PollConsignmentWarehouseRestClient}
 * 
 * @author gsing236
 *
 */
public class DefaultPollConsignmentWarehouseRestClient implements PollConsignmentWarehouseRestClient {

    @Override
    public PollWarehouseResponse pollConsignments(final PollWarehouseRequest request) {
        return PollWarehouseResponse.getSuccessResponse();
    }
}
