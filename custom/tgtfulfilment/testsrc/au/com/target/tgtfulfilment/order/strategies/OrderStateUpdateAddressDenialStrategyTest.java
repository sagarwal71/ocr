/**
 * 
 */
package au.com.target.tgtfulfilment.order.strategies;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderStateUpdateAddressDenialStrategyTest {
    private OrderStateUpdateAddressDenialStrategy strategy;

    @Before
    public void setup() {
        strategy = new OrderStateUpdateAddressDenialStrategy();
        final List<OrderStatus> updateDeniedOrderStates = new ArrayList<>();
        updateDeniedOrderStates.add(OrderStatus.CANCELLED);
        updateDeniedOrderStates.add(OrderStatus.COMPLETED);
        strategy.setUpdateDeniedOrderStates(updateDeniedOrderStates);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsDeniedNullOrder() {
        strategy.isDenied(null);
    }

    @Test
    public void testIsDeniedInList() {
        final OrderModel order = Mockito.mock(OrderModel.class);
        BDDMockito.given(order.getStatus()).willReturn(OrderStatus.CANCELLED);
        Assert.assertTrue(strategy.isDenied(order));
        BDDMockito.given(order.getStatus()).willReturn(OrderStatus.COMPLETED);
        Assert.assertTrue(strategy.isDenied(order));
    }

    @Test
    public void testIsDeniedNotInList() {
        final OrderModel order = Mockito.mock(OrderModel.class);
        BDDMockito.given(order.getStatus()).willReturn(OrderStatus.INPROGRESS);
        Assert.assertFalse(strategy.isDenied(order));
    }

}
