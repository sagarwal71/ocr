/**
 * 
 */
package au.com.target.tgtfulfilment.helper;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;


/**
 * Test cases for WarehouseHelper.
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WarehouseHelperTest {

    @Mock
    private WarehouseModel warehouseModel;

    @InjectMocks
    private final WarehouseHelper warehouseHelper = new WarehouseHelper();

    @Test
    public void testNullWarehouse() {
        Assert.assertNull(warehouseHelper.getAssignedStoreForWarehouse(null));
    }

    @Test
    public void testWhenNoTargetPointOfService() {
        final PointOfServiceModel posModel = new PointOfServiceModel();
        final Collection<PointOfServiceModel> collection = new ArrayList<>();
        collection.add(posModel);
        BDDMockito.given(warehouseModel.getPointsOfService()).willReturn(collection);
        Assert.assertNull(warehouseHelper.getAssignedStoreForWarehouse(warehouseModel));
    }

    @Test
    public void testWhenTargetPointOfService() {
        final TargetPointOfServiceModel posModel = new TargetPointOfServiceModel();
        final Collection<PointOfServiceModel> collection = new ArrayList<>();
        collection.add(posModel);
        BDDMockito.given(warehouseModel.getPointsOfService()).willReturn(collection);
        Assert.assertNotNull(warehouseHelper.getAssignedStoreForWarehouse(warehouseModel));
    }
}
