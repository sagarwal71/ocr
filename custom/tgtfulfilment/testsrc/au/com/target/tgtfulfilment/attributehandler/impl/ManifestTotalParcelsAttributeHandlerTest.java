/**
 * 
 */
package au.com.target.tgtfulfilment.attributehandler.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.model.TargetManifestModel;


/**
 * @author Vivek
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ManifestTotalParcelsAttributeHandlerTest {

    @InjectMocks
    private final ManifestTotalParcelsAttributeHandler handler = new ManifestTotalParcelsAttributeHandler();

    @Mock
    private TargetManifestModel manifest;

    @Before
    public void setUp() {

        final TargetConsignmentModel consignment1 = new TargetConsignmentModel();
        final TargetConsignmentModel consignment2 = new TargetConsignmentModel();
        final TargetConsignmentModel consignment3 = new TargetConsignmentModel();
        consignment1.setParcelCount(Integer.valueOf(3));
        consignment2.setParcelCount(null);
        consignment3.setParcelCount(Integer.valueOf(2));
        final Set<TargetConsignmentModel> consignments = new HashSet<>();
        consignments.add(consignment1);
        consignments.add(consignment2);
        consignments.add(consignment3);

        Mockito.when(manifest.getConsignments()).thenReturn(consignments);
    }

    @Test
    public void testNullManifest() {
        final Integer parcels = handler.get(null);
        Assert.assertEquals(0, parcels.intValue());
    }

    @Test
    public void testForManifestWithNullConsignments() {
        Mockito.when(manifest.getConsignments()).thenReturn(null);
        final Integer parcels = handler.get(manifest);
        Assert.assertEquals(0, parcels.intValue());
    }

    @Test
    public void testForManifestWithEmptyListOfConsignments() {
        Mockito.when(manifest.getConsignments()).thenReturn(new HashSet<TargetConsignmentModel>());
        final Integer parcels = handler.get(manifest);
        Assert.assertEquals(0, parcels.intValue());
    }

    @Test
    public void testForManifestWithConsignments() {
        final Integer parcels = handler.get(manifest);
        Assert.assertEquals(5, parcels.intValue());
    }
}
