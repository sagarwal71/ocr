/**
 * 
 */
package au.com.target.tgtfulfilment.interceptor;



import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtfulfilment.model.TargetCarrierModel;


@UnitTest
public class TargetCarrierValidateInterceptorTest {

    private final TargetCarrierValidateInterceptor interceptor = new TargetCarrierValidateInterceptor();

    @Mock
    private InterceptorContext interceptorContext;
    @Mock
    private TargetCarrierModel targetCarrier;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

    }

    @Test(expected = InterceptorException.class)
    public void testValidateLengthOrderInstructions1() throws InterceptorException {

        interceptor.setOrderInstructionsLength(40);
        BDDMockito.given(targetCarrier.getOrderInstructions1()).willReturn(
                "This is a test Message more than 40 characters length");
        interceptor.onValidate(targetCarrier, interceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testValidateLengthOrderInstructions2() throws InterceptorException {

        interceptor.setOrderInstructionsLength(40);
        BDDMockito.given(targetCarrier.getOrderInstructions2()).willReturn(
                "This is a test Message more than 40 characters length");
        interceptor.onValidate(targetCarrier, interceptorContext);
    }
}
