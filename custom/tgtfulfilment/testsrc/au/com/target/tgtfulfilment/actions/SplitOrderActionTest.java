package au.com.target.tgtfulfilment.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.ConsignmentCreationException;
import de.hybris.platform.ordersplitting.OrderSplittingService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;


/**
 * Unit test for {@link SplitOrderAction}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SplitOrderActionTest {

    @InjectMocks
    private final SplitOrderAction splitOrderAction = new SplitOrderAction();

    @Mock
    private OrderSplittingService orderSplittingService;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Test
    public void testExecuteActionWithNullProcess() throws ConsignmentCreationException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("OrderProcess cannot be null");
        splitOrderAction.executeAction(null);
    }

    @Test
    public void testExecuteActionWithNullOrder() throws ConsignmentCreationException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Order cannot be null");

        final OrderProcessModel orderProcessModel = Mockito.mock(OrderProcessModel.class);
        BDDMockito.given(orderProcessModel.getOrder()).willReturn(null);

        splitOrderAction.executeAction(orderProcessModel);
    }

    @Test
    public void testExecuteAction() throws ConsignmentCreationException {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final OrderProcessModel orderProcessModel = Mockito.mock(OrderProcessModel.class);
        BDDMockito.given(orderProcessModel.getOrder()).willReturn(orderModel);

        splitOrderAction.executeAction(orderProcessModel);

        BDDMockito.verify(orderSplittingService).splitOrderForConsignment(orderModel, orderModel.getEntries());
        BDDMockito.verify(orderModel).setStatus(OrderStatus.INPROGRESS);
        BDDMockito.verify(modelService).save(orderModel);
    }

    @Test
    public void testVerifyWhenOrderHasOnlyDigitalProduct() throws ConsignmentCreationException {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final OrderProcessModel orderProcessModel = Mockito.mock(OrderProcessModel.class);
        final ProductModel digitalProduct = Mockito.mock(ProductModel.class);
        final AbstractOrderEntryModel digitalEntry = Mockito.mock(AbstractOrderEntryModel.class);
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries.add(digitalEntry);
        BDDMockito.given(orderProcessModel.getOrder()).willReturn(orderModel);
        BDDMockito.given(orderModel.getEntries()).willReturn(orderEntries);
        BDDMockito.given(digitalEntry.getProduct()).willReturn(digitalProduct);
        BDDMockito.given(Boolean.valueOf(targetDeliveryModeHelper.hasOnlyDigitalDeliveryMode(digitalProduct)))
                .willReturn(Boolean.TRUE);
        splitOrderAction.executeAction(orderProcessModel);
        Mockito.verify(targetDeliveryModeHelper, Mockito.times(1)).getDigitalDeliveryMode(
                Mockito.any(AbstractTargetVariantProductModel.class));
    }

    @Test
    public void testVerifyWhenOrderHasMixedProducts() throws ConsignmentCreationException {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final OrderProcessModel orderProcessModel = Mockito.mock(OrderProcessModel.class);
        final AbstractTargetVariantProductModel digitalProduct = Mockito.mock(AbstractTargetVariantProductModel.class);
        final AbstractTargetVariantProductModel nonDigitalProduct = Mockito
                .mock(AbstractTargetVariantProductModel.class);
        final AbstractOrderEntryModel digitalEntry = Mockito.mock(AbstractOrderEntryModel.class);
        final AbstractOrderEntryModel nonDigitalEntry = Mockito.mock(AbstractOrderEntryModel.class);
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries.add(digitalEntry);
        orderEntries.add(nonDigitalEntry);
        BDDMockito.given(orderModel.getEntries()).willReturn(orderEntries);
        BDDMockito.given(digitalEntry.getProduct()).willReturn(digitalProduct);
        BDDMockito.given(nonDigitalEntry.getProduct()).willReturn(nonDigitalProduct);
        BDDMockito.given(Boolean.valueOf(targetDeliveryModeHelper.hasOnlyDigitalDeliveryMode(digitalProduct)))
                .willReturn(Boolean.TRUE);
        BDDMockito.given(Boolean.valueOf(targetDeliveryModeHelper.hasOnlyDigitalDeliveryMode(nonDigitalProduct)))
                .willReturn(Boolean.FALSE);
        BDDMockito.given(orderProcessModel.getOrder()).willReturn(orderModel);
        splitOrderAction.executeAction(orderProcessModel);
        Mockito.verify(targetDeliveryModeHelper, Mockito.times(1)).getDigitalDeliveryMode(
                Mockito.any(AbstractTargetVariantProductModel.class));
    }

    @Test
    public void testVerifyWhenOrderHasOnlyNonDigitalProduct() throws ConsignmentCreationException {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final OrderProcessModel orderProcessModel = Mockito.mock(OrderProcessModel.class);
        final ProductModel nonDigitalProduct = Mockito.mock(ProductModel.class);
        final AbstractOrderEntryModel nonDigitalEntry = Mockito.mock(AbstractOrderEntryModel.class);
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries.add(nonDigitalEntry);
        BDDMockito.given(orderModel.getEntries()).willReturn(orderEntries);
        BDDMockito.given(nonDigitalEntry.getProduct()).willReturn(nonDigitalProduct);
        BDDMockito.given(Boolean.valueOf(targetDeliveryModeHelper.hasOnlyDigitalDeliveryMode(nonDigitalProduct)))
                .willReturn(Boolean.FALSE);
        BDDMockito.given(orderProcessModel.getOrder()).willReturn(orderModel);
        splitOrderAction.executeAction(orderProcessModel);
        Mockito.verify(targetDeliveryModeHelper, Mockito.times(0)).getDigitalDeliveryMode(
                Mockito.any(AbstractTargetVariantProductModel.class));
    }

    @Test
    public void testVerifyWhenOrderHasProductWithMixedDelModes() throws ConsignmentCreationException {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final OrderProcessModel orderProcessModel = Mockito.mock(OrderProcessModel.class);
        final AbstractTargetVariantProductModel digitalProduct = Mockito.mock(AbstractTargetVariantProductModel.class);
        final AbstractTargetVariantProductModel nonDigitalProduct = Mockito
                .mock(AbstractTargetVariantProductModel.class);
        final AbstractOrderEntryModel digitalEntry = Mockito.mock(AbstractOrderEntryModel.class);
        final AbstractOrderEntryModel nonDigitalEntry = Mockito.mock(AbstractOrderEntryModel.class);
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries.add(digitalEntry);
        orderEntries.add(nonDigitalEntry);
        BDDMockito.given(orderModel.getEntries()).willReturn(orderEntries);
        BDDMockito.given(digitalEntry.getProduct()).willReturn(digitalProduct);
        BDDMockito.given(nonDigitalEntry.getProduct()).willReturn(nonDigitalProduct);
        BDDMockito.given(Boolean.valueOf(targetDeliveryModeHelper.hasOnlyDigitalDeliveryMode(digitalProduct)))
                .willReturn(Boolean.FALSE);
        BDDMockito.given(Boolean.valueOf(targetDeliveryModeHelper.hasOnlyDigitalDeliveryMode(nonDigitalProduct)))
                .willReturn(Boolean.FALSE);
        BDDMockito.given(orderProcessModel.getOrder()).willReturn(orderModel);
        splitOrderAction.executeAction(orderProcessModel);
        Mockito.verify(targetDeliveryModeHelper, Mockito.times(0)).getDigitalDeliveryMode(
                Mockito.any(AbstractTargetVariantProductModel.class));
    }
}
