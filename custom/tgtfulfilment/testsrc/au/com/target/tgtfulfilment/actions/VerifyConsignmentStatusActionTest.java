package au.com.target.tgtfulfilment.actions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;

/**
 * Test suite for {@link VerifyConsignmentStatusAction}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VerifyConsignmentStatusActionTest {

    private static final ConsignmentStatus STATUS_TO_CHECK_AGAINST = ConsignmentStatus.PICKED;

    @Mock
    private OrderProcessParameterHelper processParameterHelper;

    @Spy
    @InjectMocks
    private final VerifyConsignmentStatusAction action = new VerifyConsignmentStatusAction();

    @Mock
    private OrderProcessModel process;
    @Mock
    private ConsignmentModel consignment;

    /**
     * Initializes test case before run.
     */
    @Before
    public void setUp() {
        doReturn(processParameterHelper).when(action).getOrderProcessParameterHelper();
        action.setConsignmentStatus(STATUS_TO_CHECK_AGAINST);
    }

    /**
     * Verifies that execution of action will fail if not enough information was provided.
     *
     * @throws Exception if any exception occurs
     */
    @Test
    public void testIllegalArgument() throws Exception {
        when(processParameterHelper.getConsignment(process)).thenReturn(null);
        try {
            action.executeAction(process);
            fail();
        }
        catch (final IllegalArgumentException e) {
            assertEquals("Consignment must not be null", e.getMessage());
        }
    }

    /**
     * Verifies the case of consignment statuses match.
     *
     * @throws Exception if any exception occurs
     */
    @Test
    public void testStatusNotMatches() throws Exception {
        when(processParameterHelper.getConsignment(process)).thenReturn(consignment);
        when(consignment.getStatus()).thenReturn(ConsignmentStatus.CREATED);
        assertEquals(Transition.NOK, action.executeAction(process));
    }

    /**
     * Verifies the case of consignment statues do not match.
     *
     * @throws Exception if any exception occurs
     */
    @Test
    public void testStatusMatches() throws Exception {
        when(processParameterHelper.getConsignment(process)).thenReturn(consignment);
        when(consignment.getStatus()).thenReturn(STATUS_TO_CHECK_AGAINST);
        assertEquals(Transition.OK, action.executeAction(process));
    }
}
