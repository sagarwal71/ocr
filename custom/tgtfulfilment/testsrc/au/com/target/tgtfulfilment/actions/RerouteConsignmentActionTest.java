package au.com.target.tgtfulfilment.actions;

/**
 * 
 */



import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtfulfilment.service.SendToWarehouseService;
import au.com.target.tgtfulfilment.service.impl.ConsignmentReroutingService;


/**
 * Unit test for {@link RerouteConsignmentAction}
 * 
 * @author jjayawa1
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class RerouteConsignmentActionTest {

    @InjectMocks
    private final RerouteConsignmentAction reRouteConsignmentAction = new RerouteConsignmentAction();

    @Mock
    private ConsignmentReroutingService consignmentReroutingService;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private SendToWarehouseService sendToWarehouseService;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Test
    public void testExecuteActionWithNullProcess() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("OrderProcessModel cannot be null");
        reRouteConsignmentAction.executeAction(null);
    }

    @Test
    public void testExecuteActionWithNullOrder() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Consignment cannot be null");

        final OrderProcessModel orderProcessModel = Mockito.mock(OrderProcessModel.class);
        reRouteConsignmentAction.executeAction(orderProcessModel);
    }

    @Test
    public void testExecuteAction() throws Exception {
        final ConsignmentModel consignmentModel = Mockito.mock(ConsignmentModel.class);
        final OrderProcessModel orderProcessModel = Mockito.mock(OrderProcessModel.class);
        final ConsignmentModel consignmentOne = Mockito.mock(ConsignmentModel.class);
        final ConsignmentModel consignmentTwo = Mockito.mock(ConsignmentModel.class);
        final List<ConsignmentModel> ordersConsignments = new ArrayList<>();
        ordersConsignments.add(consignmentOne);
        ordersConsignments.add(consignmentTwo);

        given(consignmentReroutingService.rerouteConsignment(consignmentModel))
                .willReturn(ordersConsignments);
        given(orderProcessParameterHelper.getConsignment(orderProcessModel)).willReturn(consignmentModel);

        reRouteConsignmentAction.executeAction(orderProcessModel);
        verify(consignmentReroutingService).rerouteConsignment(consignmentModel);
        verify(sendToWarehouseService).sendConsignment(consignmentOne);
        verify(sendToWarehouseService).sendConsignment(consignmentTwo);
    }

    @Test
    public void testExecuteActionWhenNoConsignments() throws Exception {
        expectedException.expect(RuntimeException.class);
        final ConsignmentModel consignmentModel = Mockito.mock(ConsignmentModel.class);
        final OrderProcessModel orderProcessModel = Mockito.mock(OrderProcessModel.class);
        final OrderModel orderModel = Mockito.mock(OrderModel.class);

        given(consignmentReroutingService.rerouteConsignment(consignmentModel))
                .willReturn(Collections.EMPTY_LIST);
        given(orderProcessParameterHelper.getConsignment(orderProcessModel)).willReturn(consignmentModel);
        given(consignmentModel.getOrder()).willReturn(orderModel);
        given(orderModel.getCode()).willReturn("11");

        reRouteConsignmentAction.executeAction(orderProcessModel);
    }

    @Test
    public void testExecuteActionWhenReroutingThrowsAnError() throws Exception {
        final ConsignmentModel consignmentModel = Mockito.mock(ConsignmentModel.class);
        final OrderProcessModel orderProcessModel = Mockito.mock(OrderProcessModel.class);
        final ConsignmentModel consignmentOne = Mockito.mock(ConsignmentModel.class);
        final ConsignmentModel consignmentTwo = Mockito.mock(ConsignmentModel.class);
        final List<ConsignmentModel> ordersConsignments = new ArrayList<>();
        ordersConsignments.add(consignmentOne);
        ordersConsignments.add(consignmentTwo);
        final OrderModel orderModel = Mockito.mock(OrderModel.class);

        given(consignmentReroutingService.rerouteConsignment(consignmentModel))
                .willReturn(ordersConsignments);
        given(orderProcessParameterHelper.getConsignment(orderProcessModel)).willReturn(consignmentModel);
        BDDMockito.willThrow(new RuntimeException("Test Exception")).given(sendToWarehouseService)
                .sendConsignment(consignmentOne);
        given(consignmentOne.getOrder()).willReturn(orderModel);
        given(orderModel.getCode()).willReturn("11");
        given(consignmentOne.getCode()).willReturn("a11");

        reRouteConsignmentAction.executeAction(orderProcessModel);
        verify(consignmentReroutingService).rerouteConsignment(consignmentModel);
        verify(sendToWarehouseService).sendConsignment(consignmentOne);
        verify(sendToWarehouseService).sendConsignment(consignmentTwo);
    }

    @Test
    public void testExecuteActionWithFalconON() throws Exception {
        given(
                Boolean.valueOf(targetFeatureSwitchService
                        .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON))).willReturn(Boolean.TRUE);
        final ConsignmentModel consignmentModel = Mockito.mock(ConsignmentModel.class);
        final OrderProcessModel orderProcessModel = Mockito.mock(OrderProcessModel.class);
        final ConsignmentModel consignmentOne = Mockito.mock(ConsignmentModel.class);
        final ConsignmentModel consignmentTwo = Mockito.mock(ConsignmentModel.class);
        final List<ConsignmentModel> ordersConsignments = new ArrayList<>();
        ordersConsignments.add(consignmentOne);
        ordersConsignments.add(consignmentTwo);
        given(Boolean.valueOf(consignmentReroutingService.isRequireRerouting(consignmentModel))).willReturn(
                Boolean.TRUE);
        given(consignmentReroutingService.rerouteConsignment(consignmentModel))
                .willReturn(ordersConsignments);
        given(orderProcessParameterHelper.getConsignment(orderProcessModel)).willReturn(consignmentModel);

        reRouteConsignmentAction.executeAction(orderProcessModel);
        verify(consignmentReroutingService).rerouteConsignment(consignmentModel);
        verify(sendToWarehouseService).sendConsignment(consignmentOne);
        verify(sendToWarehouseService).sendConsignment(consignmentTwo);
    }

    @Test
    public void testExecuteActionWithFalconONWitnSkipRerouting() throws Exception {
        given(
                Boolean.valueOf(targetFeatureSwitchService
                        .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON))).willReturn(Boolean.TRUE);
        final ConsignmentModel consignmentModel = Mockito.mock(ConsignmentModel.class);
        final OrderProcessModel orderProcessModel = Mockito.mock(OrderProcessModel.class);
        final ConsignmentModel consignmentOne = Mockito.mock(ConsignmentModel.class);
        final ConsignmentModel consignmentTwo = Mockito.mock(ConsignmentModel.class);
        final List<ConsignmentModel> ordersConsignments = new ArrayList<>();
        ordersConsignments.add(consignmentOne);
        ordersConsignments.add(consignmentTwo);
        final OrderModel order = mock(OrderModel.class);
        given(order.getCode()).willReturn("test order code");
        given(consignmentModel.getOrder()).willReturn(order);
        given(Boolean.valueOf(consignmentReroutingService.isRequireRerouting(consignmentModel))).willReturn(
                Boolean.FALSE);
        given(consignmentReroutingService.rerouteConsignment(consignmentModel))
                .willReturn(ordersConsignments);
        given(orderProcessParameterHelper.getConsignment(orderProcessModel)).willReturn(consignmentModel);

        reRouteConsignmentAction.executeAction(orderProcessModel);
        verify(consignmentReroutingService, never()).rerouteConsignment(consignmentModel);
    }
}
