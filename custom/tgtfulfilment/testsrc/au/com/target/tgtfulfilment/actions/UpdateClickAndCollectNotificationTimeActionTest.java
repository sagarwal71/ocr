/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author bhuang3
 * 
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UpdateClickAndCollectNotificationTimeActionTest {

    @Mock
    private OrderProcessModel processModel;

    @Mock
    private OrderModel orderModel;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private final UpdateClickAndCollectNotificationTimeAction action = new UpdateClickAndCollectNotificationTimeAction();

    @Test
    public void testUpdateClickAndCollectNotificationTime() throws RetryLaterException, Exception {
        BDDMockito.given(processModel.getOrder()).willReturn(orderModel);
        action.executeAction(processModel);
        Mockito.verify(processModel).getOrder();
        Mockito.verify(orderModel).setCncLastNotification(Mockito.any(Date.class));
        Mockito.verify(modelService).save(orderModel);
    }


}
