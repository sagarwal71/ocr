/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.actions.ReleaseStockForOrderAction;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.ordercancel.TargetCancelService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ReleaseStockForOrderActionTest {


    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private OrderProcessModel orderProcessMock;

    @Mock
    private ConsignmentModel consignment;

    @Mock
    private WarehouseModel consolidatedWarehouseModel;

    @Mock
    private TargetPointOfServiceModel targetPointOfServiceModel;

    @Mock
    private WarehouseModel reservationWarehouseModel;

    @Mock
    private StoreFulfilmentCapabilitiesModel storeFulfilmentCapabilitiesModel;

    @Mock
    private TargetCancelService targetCancelService;

    @Mock
    private TargetStockService targetStockService;

    private Set<ConsignmentEntryModel> consignmentEntryList;


    @InjectMocks
    private final ReleaseStockForOrderAction releaseStockForOrderAction = new ReleaseStockForOrderAction();

    @Before
    public void setup() {

        willReturn(Boolean.TRUE).given(targetFeatureSwitchService).isFeatureEnabled(TgtCoreConstants.FASTLINE_FALCON);
        given(orderProcessParameterHelper.getConsignment(orderProcessMock)).willReturn(consignment);
        given(targetPointOfServiceModel.getStoreNumber()).willReturn(Integer.valueOf(1));
        given(targetWarehouseService.getWarehouseForCode(TgtCoreConstants.CONSOLIDATED_STORE_WAREHOUSE))
                .willReturn(consolidatedWarehouseModel);
        given(targetWarehouseService.getWarehouseForCode(TgtCoreConstants.STOCK_RESERVATION_WAREHOUSE))
                .willReturn(consolidatedWarehouseModel);
        mockConsignmentEntry();
        given(consignment.getConsignmentEntries()).willReturn(consignmentEntryList);
    }

    private void mockConsignmentEntry() {
        consignmentEntryList = new HashSet<ConsignmentEntryModel>();
        consignmentEntryList.add(mockConsignmentEntry("1223", 10));
    }

    private ConsignmentEntryModel mockConsignmentEntry(final String itemCode, final long quanity) {
        final ConsignmentEntryModel entryModel = mock(ConsignmentEntryModel.class);
        given(entryModel.getShippedQuantity()).willReturn(Long.valueOf(quanity));
        final OrderEntryModel orderEntryModel = mock(OrderEntryModel.class);
        given(entryModel.getOrderEntry()).willReturn(orderEntryModel);
        final TargetSizeVariantProductModel sizeVariantModel = mock(TargetSizeVariantProductModel.class);
        given(orderEntryModel.getProduct()).willReturn(sizeVariantModel);
        given(sizeVariantModel.getCode()).willReturn(itemCode);
        return entryModel;
    }

    @Test
    public void testReleaseStockWhenFeatureIsDisabled() throws RetryLaterException, Exception {
        willReturn(Boolean.FALSE).given(targetFeatureSwitchService).isFeatureEnabled(TgtCoreConstants.FASTLINE_FALCON);
        releaseStockForOrderAction.executeAction(orderProcessMock);
        verify(orderProcessParameterHelper, never()).getConsignment(orderProcessMock);
    }

    @Test
    public void testReleaseStockWhenFeatureIsEnabled() throws RetryLaterException, Exception {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService).isFeatureEnabled(TgtCoreConstants.FASTLINE_FALCON);
        given(consignment.getWarehouse()).willReturn(consolidatedWarehouseModel);
        given(storeFulfilmentCapabilitiesModel.getPickPackDispatchStore()).willReturn(Boolean.TRUE);
        given(targetPointOfServiceModel.getFulfilmentCapability()).willReturn(storeFulfilmentCapabilitiesModel);
        given(consignment.getWarehouse().getPointsOfService())
                .willReturn((Collection)Arrays.asList(targetPointOfServiceModel));
        releaseStockForOrderAction.executeAction(orderProcessMock);
        verify(orderProcessParameterHelper, times(1)).getConsignment(orderProcessMock);
    }

}
