/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.process.strategies.ProcessContextResolutionStrategy;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.email.BusinessProcessEmailStrategy;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendAllOrderShippingNoticesEmailActionTest {

    @Mock
    private BusinessProcessEmailStrategy businessProcessEmailStrategy;

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private ConsignmentModel consignment;

    @Mock
    private ModelService modelService;

    @Mock
    private ProcessContextResolutionStrategy contextResolutionStrategy;


    @InjectMocks
    @Spy
    private final SendAllOrderShippingNoticesEmailAction action = new SendAllOrderShippingNoticesEmailAction();

    @Before
    public void setup() {
        action.setFrontendTemplateName("TEMPLATE");
        BDDMockito.willReturn(businessProcessEmailStrategy).given(action).getBusinessProcessEmailStrategy();
        BDDMockito.given(process.getOrder()).willReturn(order);
    }

    @Test
    public void testExecuteNoConsignments() throws RetryLaterException, Exception {
        BDDMockito.given(order.getConsignments()).willReturn(null);

        final Transition trans = action.executeAction(process);
        Mockito.verifyZeroInteractions(orderProcessParameterHelper);
        Mockito.verifyZeroInteractions(businessProcessEmailStrategy);
        Assert.assertEquals(Transition.OK, trans);
    }

    @Test
    public void testExecuteWithConsignments() throws RetryLaterException, Exception {
        BDDMockito.given(order.getConsignments()).willReturn(Collections.singleton(consignment));

        final Transition trans = action.executeAction(process);
        Mockito.verify(orderProcessParameterHelper).setConsignment(process, consignment);
        Mockito.verify(businessProcessEmailStrategy).sendEmail(process, "TEMPLATE");
        Assert.assertEquals(Transition.OK, trans);
    }

}
