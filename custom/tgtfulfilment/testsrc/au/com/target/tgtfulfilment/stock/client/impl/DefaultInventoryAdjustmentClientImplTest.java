/**
 * 
 */
package au.com.target.tgtfulfilment.stock.client.impl;


import static org.fest.assertions.Assertions.assertThat;

import java.util.ArrayList;

import org.junit.Test;

import au.com.target.tgtwebcore.exception.TargetIntegrationException;


/**
 * @author pthoma20
 *
 */
public class DefaultInventoryAdjustmentClientImplTest {

    private final DefaultInventoryAdjustmentClientImpl defaultInventoryAdjustmentClientImplTest = new DefaultInventoryAdjustmentClientImpl();

    @Test
    public void testDefaultSendInventoryAdjustmentAlwaysReturnsFalse() throws TargetIntegrationException {
        assertThat(defaultInventoryAdjustmentClientImplTest.sendInventoryAdjustment(new ArrayList()))
                .isFalse();
    }

}
