/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;


/**
 * Unit tests for SpecificStoreEnabledFulfilmentDenialStrategy.
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SpecificStoreEnabledFulfilmentDenialStrategyTest {

    private static final Integer STORE_NUMBER = Integer.valueOf(7001);

    @Mock
    private TargetStoreFulfilmentCapabilitiesService targetStoreFulfilmentCapabilitiesService;

    @InjectMocks
    private final SpecificStoreEnabledFulfilmentDenialStrategy strategy = new SpecificStoreEnabledFulfilmentDenialStrategy();

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private TargetPointOfServiceModel tpos;

    @Before
    public void setup() {

        Mockito.when(tpos.getStoreNumber()).thenReturn(STORE_NUMBER);
    }

    @Test
    public void testWithNullTPOS() {

        final DenialResponse response = strategy.isDenied(oeg, null);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
    }

    @Test
    public void testWithNullStoreNumber() {

        Mockito.when(tpos.getStoreNumber()).thenReturn(null);
        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
    }

    @Test
    public void testWithDenied() {

        Mockito.when(
                Boolean.valueOf(targetStoreFulfilmentCapabilitiesService.isStoreEnabledForFulfilment(tpos)))
                .thenReturn(Boolean.FALSE);
        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
        assertThat(response.getReason()).isEqualTo("StoreNotCapableToFulfillOrder");
    }

    @Test
    public void testWithAllowed() {

        Mockito.when(
                Boolean.valueOf(targetStoreFulfilmentCapabilitiesService.isStoreEnabledForFulfilment(tpos)))
                .thenReturn(Boolean.TRUE);
        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isFalse();
        assertThat(response.getReason()).isNull();
    }

}
