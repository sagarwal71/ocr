/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;


/**
 * @author smishra1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SpecificStoreMaxOrderPerDayDenialStrategyTest {

    @Mock
    private TargetStoreFulfilmentCapabilitiesService targetStoreFulfilmentCapabilitiesService;

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private TargetPointOfServiceModel tpos;

    @InjectMocks
    private final SpecificStoreMaxOrderPerDayDenialStrategy strategy = new SpecificStoreMaxOrderPerDayDenialStrategy();

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    //CHECKSTYLE:ON
    @Test
    public void testWithNullOrder() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("oeg must not be null");
        strategy.isDenied(null, tpos);
    }

    @Test
    public void testDeniedWithMaxNumberOfOrdersForTheDayExceededForTheStore() {
        Mockito.when(Boolean.valueOf(targetStoreFulfilmentCapabilitiesService.isMaxNumberOfOrdersExceeded(tpos)))
                .thenReturn(Boolean.valueOf(true));
        final DenialResponse response = strategy.isDenied(oeg, tpos);
        assertThat(response).isNotNull();
        assertThat(response.getReason()).isEqualTo("MaxNumberOfOrdersForTheDayExceededForTheStore");
        assertThat(response.isDenied()).isTrue();
    }

    @Test
    public void testNotDeniedMaxNumberOfOrdersForTheDayExceededForTheStore() {
        Mockito.when(Boolean.valueOf(targetStoreFulfilmentCapabilitiesService.isMaxNumberOfOrdersExceeded(tpos)))
                .thenReturn(Boolean.valueOf(false));
        final DenialResponse response = strategy.isDenied(oeg, tpos);
        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isFalse();
    }
}
