/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.model.ProductExclusionsModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.FilterExclusionListService;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;


/**
 * @author ayushman
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StoreProductExclusionDenialStrategyTest {


    @Mock
    private TargetStoreFulfilmentCapabilitiesService storeFulfilmentCapabilitiesService;

    @Mock
    private StoreFulfilmentCapabilitiesModel capabilitiesModel;

    @Mock
    private FilterExclusionListService filterExclusionListService;

    @Mock
    private Set<ProductExclusionsModel> productExclusionList;

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private TargetPointOfServiceModel targetPointOfService;


    @InjectMocks
    private final StoreProductExclusionDenialStrategy storeProductExclusionDenialStrategy = new StoreProductExclusionDenialStrategy();



    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        BDDMockito.given(targetPointOfService
                .getFulfilmentCapability()).willReturn(
                        capabilitiesModel);
        BDDMockito.given(filterExclusionListService
                .filterActiveProductExclusions(capabilitiesModel.getProductExclusions())).willReturn(
                        productExclusionList);
        storeProductExclusionDenialStrategy.setStoreFulfilmentCapabilitiesService(storeFulfilmentCapabilitiesService);
    }

    @Test
    public void testDeniedForProductInExcluded() {
        BDDMockito
                .given(Boolean.valueOf(
                        storeFulfilmentCapabilitiesService.isProductInExclusionList(oeg, productExclusionList)))
                .willReturn(Boolean.TRUE);
        final DenialResponse denialResponse = storeProductExclusionDenialStrategy.isDenied(oeg, targetPointOfService);
        Assert.assertTrue("Create Denied", denialResponse.isDenied());
    }

    @Test
    public void testDeniedForProductNotExcluded() {
        BDDMockito
                .given(Boolean.valueOf(
                        storeFulfilmentCapabilitiesService.isProductInExclusionList(oeg, productExclusionList)))
                .willReturn(Boolean.FALSE);
        final DenialResponse denialResponse = storeProductExclusionDenialStrategy.isDenied(oeg, targetPointOfService);
        Assert.assertFalse("Create Not Denied", denialResponse.isDenied());
    }


}
