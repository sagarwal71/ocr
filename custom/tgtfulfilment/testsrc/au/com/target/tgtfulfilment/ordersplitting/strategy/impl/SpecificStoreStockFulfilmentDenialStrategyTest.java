/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;
import au.com.target.tgtsale.stock.service.TargetStoreStockService;


/**
 * Unit tests for SpecificStoreStockFulfilmentDenialStrategy.
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SpecificStoreStockFulfilmentDenialStrategyTest {

    private static final Integer STORE_NUMBER = Integer.valueOf(7001);

    private static final String ORDER_CODE = "11001100";

    @Mock
    private TargetStoreFulfilmentCapabilitiesService targetStoreFulfilmentCapabilitiesService;

    @Mock
    private TargetStoreStockService targetStoreStockService;

    @InjectMocks
    private final SpecificStoreStockFulfilmentDenialStrategy strategy = new SpecificStoreStockFulfilmentDenialStrategy();

    @Mock
    private OrderModel order;

    @Mock
    private TargetPointOfServiceModel tpos;

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private OEGParameterHelper oegParameterHelper;

    @Before
    public void setup() {

        Mockito.when(tpos.getStoreNumber()).thenReturn(STORE_NUMBER);
    }

    @Test
    public void testWithNullTPOS() {

        final DenialResponse response = strategy.isDenied(oeg, null);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
    }

    @Test
    public void testWithNullStoreNumber() {

        Mockito.when(tpos.getStoreNumber()).thenReturn(null);
        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
    }

    @Test
    public void testWithDenied() {

        Mockito.when(oegParameterHelper.getOrderCode(oeg)).thenReturn(ORDER_CODE);
        Mockito.when(
                Boolean.valueOf(targetStoreStockService.isOrderEntriesInStockAtStore(oeg, STORE_NUMBER,
                        ORDER_CODE)))
                .thenReturn(Boolean.FALSE);

        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
        assertThat(response.getReason()).isEqualTo("OrderNotInStockAtStore");
    }

    @Test
    public void testWithAllowed() {

        Mockito.when(oegParameterHelper.getOrderCode(oeg)).thenReturn(ORDER_CODE);
        Mockito.when(
                Boolean.valueOf(targetStoreStockService.isOrderEntriesInStockAtStore(oeg, STORE_NUMBER,
                        ORDER_CODE)))
                .thenReturn(Boolean.TRUE);
        final DenialResponse response = strategy.isDenied(oeg, tpos);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isFalse();
        assertThat(response.getReason()).isNull();
    }


}
