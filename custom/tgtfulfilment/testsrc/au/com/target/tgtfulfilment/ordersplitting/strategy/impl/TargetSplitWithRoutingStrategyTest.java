/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;
import de.hybris.platform.stock.exception.StockLevelNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.exceptions.TargetSplitOrderException;
import au.com.target.tgtfulfilment.fulfilmentservice.TicketSender;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.helper.WarehouseHelper;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntryList;
import au.com.target.tgtfulfilment.ordersplitting.strategy.ConsignmentToWarehouseAssigner;
import au.com.target.tgtfulfilment.ordersplitting.strategy.InstoreFulfilmentGlobalRulesChecker;
import au.com.target.tgtfulfilment.ordersplitting.strategy.SameStoreCncSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.StoreSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.WarehouseSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.SplitOegResult;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetSplitWithRoutingStrategyTest {

    private static final Integer CNC_STORE_NUMBER = Integer.valueOf(1234);

    private static final Integer FULFILMENT_STORE_NUMBER = Integer.valueOf(4321);

    private static final String ORDER_CODE = "testOrder";

    @Spy
    @InjectMocks
    private final TargetSplitWithRoutingStrategy tgtSplitWithRoutingStrategy = new TargetSplitWithRoutingStrategy();

    @Mock
    private OEGParameterHelper oegParameterHelper;

    @Mock
    private SameStoreCncSelectorStrategy sameStoreCncSelectorStrategy;

    @Mock
    private StoreSelectorStrategy storeSelectorStrategy;

    @Mock
    private WarehouseHelper consignmentReroutingHelper;

    @Mock
    private ConsignmentToWarehouseAssigner consignmentToWarehouseAssigner;

    @Mock
    private AddressModel deliveryAddress;

    @Mock
    private TargetZoneDeliveryModeModel deliveryMode;

    @Mock
    private TargetPointOfServiceModel cncStore;

    @Mock
    private TargetPointOfServiceModel fulfilmentStore;

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private WarehouseModel warehouse;

    @Mock
    private WarehouseSelectorStrategy ntlSelectorStrategy;

    @Mock
    private WarehouseSelectorStrategy externalWarehouseSelectorStrategy;

    @Mock
    private InstoreFulfilmentGlobalRulesChecker instoreFulfilmentGlobalRulesChecker;

    @Mock
    private TargetStockService targetStockService;

    @Mock
    private AbstractOrderEntryModel abstractOrderEntry;

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private SplitOegResult externalWarehouseSplitResult;

    @Mock
    private SplitOegResult ntlSplitresult;

    @Mock
    private StockLevelModel stockLevel;

    @Mock
    private OrderEntryModel entry;

    @Mock
    private TargetTicketBusinessService targetTicketBusinessService;

    @Mock
    private TicketSender ticketSender;

    @Mock
    private ProductModel product;

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private SameStoreCncSplitConsignmentStrategy sameStoreCncSplitConsignmentStrategy;

    @Mock
    private FastlineSplitConsignmentStrategy fastlineSplitConsignmentStrategy;

    @Mock
    private StoreSplitConsignmentStrategy storeSplitConsignmentStrategy;

    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;

    private final OrderModel order = new OrderModel();

    private final OrderEntryGroup orderEntryGroup = new OrderEntryGroup();

    private final List<OrderEntryGroup> orderEntryGroups = new ArrayList<>();

    private final OrderEntryModel orderEntry = new OrderEntryModel();

    private final ProductModel productModel = new ProductModel();



    @Before
    public void setUp() {
        orderEntry.setProduct(productModel);
        productModel.setCode("testProduct");
        orderEntryGroup.add(orderEntry);
        orderEntryGroups.add(orderEntryGroup);
        Mockito.doNothing().when(oegParameterHelper).populateParamsFromOrder(orderEntryGroup);
        Mockito.doReturn(ORDER_CODE).when(oegParameterHelper).getOrderCode(orderEntryGroup);
        Mockito.doReturn(CNC_STORE_NUMBER).when(oegParameterHelper).getCncStoreNumber(orderEntryGroup);
        Mockito.doReturn(deliveryAddress).when(oegParameterHelper).getDeliveryAddress(orderEntryGroup);
    }

    @Test
    public void testPerformNullGroups() {
        Assert.assertNull(tgtSplitWithRoutingStrategy.perform(null));
    }

    @Test
    public void testPerformEmptyGroups() {
        Assert.assertTrue(CollectionUtils.isEmpty(tgtSplitWithRoutingStrategy.perform(ListUtils.EMPTY_LIST)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPerformMultipleGroups() {
        orderEntryGroups.add(new OrderEntryGroup());
        tgtSplitWithRoutingStrategy.perform(orderEntryGroups);
    }

    @Test
    public void testPerformAssignCncToSameStore() throws TargetSplitOrderException {
        order.setDeliveryAddress(deliveryAddress);
        order.setDeliveryMode(deliveryMode);
        order.setCncStoreNumber(CNC_STORE_NUMBER);
        order.setCode(ORDER_CODE);

        Mockito.doReturn(Boolean.TRUE).when(instoreFulfilmentGlobalRulesChecker)
                .areAllGlobalRulesPassed(orderEntryGroup);
        Mockito.doReturn(externalWarehouseSplitResult).when(externalWarehouseSelectorStrategy).split(orderEntryGroup);
        Mockito.doReturn(orderEntryGroup).when(externalWarehouseSplitResult).getNotAssignedToWarehouse();
        setupGlobalInstoreFulfilmentRulesPassed(true);
        Mockito.doReturn(cncStore).when(sameStoreCncSelectorStrategy).isValidForSameStoreCnc(orderEntryGroup,
                CNC_STORE_NUMBER);

        tgtSplitWithRoutingStrategy.perform(orderEntryGroups);

        Mockito.verify(oegParameterHelper).assignTposToOEG(orderEntryGroup, cncStore);
    }

    @Test
    public void testPerformAssignCncToAnotherStore() throws TargetSplitOrderException {
        order.setDeliveryAddress(deliveryAddress);
        order.setDeliveryMode(deliveryMode);
        order.setCncStoreNumber(CNC_STORE_NUMBER);
        order.setCode(ORDER_CODE);

        setupGlobalInstoreFulfilmentRulesPassed(true);
        setupDispatchInstoreFulfilmentRulesPassed(true);

        Mockito.doReturn(externalWarehouseSplitResult).when(externalWarehouseSelectorStrategy).split(orderEntryGroup);
        Mockito.doReturn(orderEntryGroup).when(externalWarehouseSplitResult).getNotAssignedToWarehouse();
        Mockito.doReturn(null).when(sameStoreCncSelectorStrategy).isValidForSameStoreCnc(orderEntryGroup,
                CNC_STORE_NUMBER);
        Mockito.doReturn(fulfilmentStore).when(storeSelectorStrategy).selectStore(orderEntryGroup);
        Mockito.doReturn(OfcOrderType.INTERSTORE_DELIVERY).when(oegParameterHelper)
                .getOfcOrderType(orderEntryGroup, FULFILMENT_STORE_NUMBER);

        tgtSplitWithRoutingStrategy.perform(orderEntryGroups);

        Mockito.verify(oegParameterHelper).assignTposToOEG(orderEntryGroup, fulfilmentStore);
    }

    @Test
    public void testPerformNTLNoSplitOneNTLOeg() throws TargetSplitOrderException {
        final List<OrderEntryGroup> oegs = new ArrayList<>();
        oegs.add(orderEntryGroup);
        setupNtlTestData();
        Mockito.doReturn(externalWarehouseSplitResult).when(externalWarehouseSelectorStrategy).split(orderEntryGroup);
        Mockito.doReturn(orderEntryGroup).when(externalWarehouseSplitResult).getNotAssignedToWarehouse();
        Mockito.when(ntlSelectorStrategy.split(orderEntryGroup)).thenReturn(ntlSplitresult);
        Mockito.doReturn(oegs).when(ntlSplitresult).getAssignedOEGsToWarehouse();

        final List<OrderEntryGroup> resultOegs = tgtSplitWithRoutingStrategy.perform(oegs);

        Assert.assertNotNull(resultOegs);
        Assert.assertEquals(1, resultOegs.size());
        Assert.assertEquals(orderEntryGroup, resultOegs.get(0));
    }

    @Test
    public void testPerformExternalSplitException() throws TargetSplitOrderException {
        final List<OrderEntryGroup> oegs = new ArrayList<>();
        oegs.add(orderEntryGroup);

        Mockito.doThrow(new TargetSplitOrderException("Exception while splitting"))
                .when(externalWarehouseSelectorStrategy).split(orderEntryGroup);
        Mockito.doReturn(orderEntryGroup).when(externalWarehouseSplitResult).getNotAssignedToWarehouse();

        final List<OrderEntryGroup> resultOegs = tgtSplitWithRoutingStrategy.perform(oegs);

        Assert.assertNotNull(resultOegs);
        Assert.assertEquals(1, resultOegs.size());
        Assert.assertEquals(orderEntryGroup, resultOegs.get(0));
    }


    @Test
    public void testPerformNTLNoSplitOneNonNTLOeg() throws TargetSplitOrderException {
        final ArrayList<OrderEntryGroup> oegs1 = new ArrayList<>();
        oegs1.add(orderEntryGroup);
        setupNtlTestData();
        Mockito.doReturn(externalWarehouseSplitResult).when(externalWarehouseSelectorStrategy).split(orderEntryGroup);
        Mockito.doReturn(orderEntryGroup).when(externalWarehouseSplitResult).getNotAssignedToWarehouse();
        Mockito.when(ntlSelectorStrategy.split(orderEntryGroup)).thenReturn(ntlSplitresult);
        Mockito.doReturn(oegs1).when(ntlSplitresult).getAssignedOEGsToWarehouse();
        Mockito.when(oegParameterHelper.getFulfilmentWarehouse(orderEntryGroup)).thenReturn(null);

        final List<OrderEntryGroup> oegs = tgtSplitWithRoutingStrategy.perform(orderEntryGroups);

        Assert.assertNotNull(oegs);
        Assert.assertEquals(1, oegs.size());
        Assert.assertEquals(orderEntryGroup, oegs.get(0));
    }


    @Test
    public void testPerformFastlineSplitNoStockOeg() throws TargetSplitOrderException {

        final List<AbstractOrderEntryModel> refundEntries = new ArrayList<>();
        refundEntries.add(abstractOrderEntry);
        final List<OrderEntryGroup> oegs = new ArrayList<>();
        oegs.add(orderEntryGroup);
        setupNtlTestData();
        Mockito.doReturn(externalWarehouseSplitResult).when(externalWarehouseSelectorStrategy).split(orderEntryGroup);
        Mockito.doReturn(orderEntryGroup).when(externalWarehouseSplitResult).getNotAssignedToWarehouse();
        Mockito.when(ntlSelectorStrategy.split(orderEntryGroup)).thenReturn(ntlSplitresult);
        Mockito.doReturn(orderEntryGroup).when(ntlSplitresult).getNotAssignedToWarehouse();
        Mockito.doReturn(warehouse).when(targetWarehouseService).getDefaultOnlineWarehouse();
        Mockito.doThrow(new StockLevelNotFoundException("Stock not found for product in warehouse"))
                .when(targetStockService).checkAndGetStockLevel(orderEntryGroup.get(0).getProduct(), warehouse);
        Assert.assertNotNull(tgtSplitWithRoutingStrategy.perform(oegs));
        Mockito.verify(ticketSender).sendStockNotInDefaultWarehouse(orderEntryGroup);
        Assert.assertEquals(1, oegs.size());
        Assert.assertEquals(orderEntryGroup, oegs.get(0));
    }

    @Test
    public void testPerformNTLSplitOeg() throws TargetSplitOrderException {
        final List<OrderEntryGroup> orderEntryGroupList = new ArrayList<>();
        final OrderEntryGroup originalOeg = new OrderEntryGroup();
        final OrderEntryGroup oeg1 = new OrderEntryGroup();
        final OrderEntryGroup oeg2 = new OrderEntryGroup();
        final OrderEntryModel giftCardEntry = new OrderEntryModel();
        final OrderEntryModel cncEntry = new OrderEntryModel();
        final OrderEntryModel ntlEntry = new OrderEntryModel();
        originalOeg.add(giftCardEntry);
        originalOeg.add(cncEntry);
        originalOeg.add(ntlEntry);
        oeg1.add(giftCardEntry);
        oeg2.add(cncEntry);
        oeg2.add(ntlEntry);
        final ArrayList<OrderEntryGroup> oegs = new ArrayList<>();
        oegs.add(oeg1);
        orderEntryGroupList.add(originalOeg);

        Mockito.doReturn(externalWarehouseSplitResult).when(externalWarehouseSelectorStrategy).split(originalOeg);
        Mockito.doReturn(oegs).when(externalWarehouseSplitResult).getAssignedOEGsToWarehouse();
        Mockito.doReturn(oeg2).when(externalWarehouseSplitResult).getNotAssignedToWarehouse();
        Mockito.doReturn(Boolean.TRUE).when(instoreFulfilmentGlobalRulesChecker)
                .areAllGlobalRulesPassed(oeg2);
        Mockito.doReturn(Boolean.TRUE).when(instoreFulfilmentGlobalRulesChecker)
                .areAllDispatchRulesPassed(oeg2);
        Mockito.doReturn(cncStore).when(sameStoreCncSelectorStrategy).isValidForSameStoreCnc(oeg2,
                CNC_STORE_NUMBER);
        Mockito.doReturn(cncStore).when(storeSelectorStrategy).selectStore(oeg2);

        final List<OrderEntryGroup> resultOegs = tgtSplitWithRoutingStrategy.perform(orderEntryGroupList);
        Mockito.verify(oegParameterHelper).assignTposToOEG(oeg2, cncStore);

        Assert.assertNotNull(resultOegs);
        Assert.assertEquals(2, resultOegs.size());
        Assert.assertEquals(oeg1, resultOegs.get(0));
        Assert.assertEquals(oeg2, resultOegs.get(1));
        Assert.assertEquals(giftCardEntry, resultOegs.get(0).get(0));
        Assert.assertEquals(cncEntry, resultOegs.get(1).get(0));
        Assert.assertEquals(ntlEntry, resultOegs.get(1).get(1));
    }

    private void setupNtlTestData() {
        order.setDeliveryAddress(deliveryAddress);
        order.setDeliveryMode(deliveryMode);
        order.setCode(ORDER_CODE);

        setupGlobalInstoreFulfilmentRulesPassed(false);

        Mockito.doReturn(null).when(oegParameterHelper).getCncStoreNumber(orderEntryGroup);
    }

    @Test
    public void testPerformAssignDeliveryToStore() throws TargetSplitOrderException {
        order.setDeliveryAddress(deliveryAddress);
        order.setDeliveryMode(deliveryMode);
        order.setCode(ORDER_CODE);

        setupGlobalInstoreFulfilmentRulesPassed(true);
        setupDispatchInstoreFulfilmentRulesPassed(true);

        Mockito.doReturn(externalWarehouseSplitResult).when(externalWarehouseSelectorStrategy).split(orderEntryGroup);
        Mockito.doReturn(orderEntryGroup).when(externalWarehouseSplitResult).getNotAssignedToWarehouse();
        Mockito.doReturn(cncStore).when(storeSelectorStrategy).selectStore(orderEntryGroup);
        Mockito.doReturn(OfcOrderType.CUSTOMER_DELIVERY).when(oegParameterHelper)
                .getOfcOrderType(orderEntryGroup, FULFILMENT_STORE_NUMBER);

        tgtSplitWithRoutingStrategy.perform(orderEntryGroups);

        Mockito.verify(oegParameterHelper).assignTposToOEG(orderEntryGroup, cncStore);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAfterSplittingNotTargetConsignment() {
        tgtSplitWithRoutingStrategy.afterSplitting(Mockito.any(OrderEntryGroup.class),
                Mockito.any(ConsignmentModel.class));
    }

    @Test
    public void testAfterSplittingForStore() {

        Mockito.doReturn(cncStore).when(consignmentReroutingHelper).getAssignedStoreForWarehouse(warehouse);
        Mockito.doReturn(warehouse).when(oegParameterHelper).getFulfilmentWarehouse(orderEntryGroup);
        Mockito.doReturn(deliveryMode).when(consignment).getDeliveryMode();
        Mockito.doReturn(CNC_STORE_NUMBER).when(cncStore).getStoreNumber();
        Mockito.doReturn(OfcOrderType.INSTORE_PICKUP).when(oegParameterHelper)
                .getOfcOrderType(orderEntryGroup, CNC_STORE_NUMBER);

        tgtSplitWithRoutingStrategy.afterSplitting(orderEntryGroup, consignment);

        Mockito.verify(consignmentToWarehouseAssigner).assignConsignmentToStore(consignment, warehouse,
                OfcOrderType.INSTORE_PICKUP, deliveryMode, ORDER_CODE);
    }

    @Test
    public void testAfterSplittingNTL() {

        // Warehouse is linked to fulfilmentStore of type WAREHOUSE
        Mockito.doReturn(deliveryMode).when(consignment).getDeliveryMode();
        Mockito.doReturn(fulfilmentStore).when(consignmentReroutingHelper).getAssignedStoreForWarehouse(warehouse);
        Mockito.doReturn(warehouse).when(oegParameterHelper).getFulfilmentWarehouse(orderEntryGroup);
        Mockito.doReturn(PointOfServiceTypeEnum.WAREHOUSE).when(fulfilmentStore).getType();

        tgtSplitWithRoutingStrategy.afterSplitting(orderEntryGroup, consignment);

        Mockito.verify(consignmentToWarehouseAssigner).assignConsignmentToNTL(consignment, warehouse);
    }

    @Test
    public void testAfterSplittingExternalWarehouse() {

        // No store on the warehouse
        Mockito.doReturn(warehouse).when(oegParameterHelper).getFulfilmentWarehouse(orderEntryGroup);
        Mockito.doReturn(null).when(consignmentReroutingHelper).getAssignedStoreForWarehouse(warehouse);
        Mockito.doReturn(Boolean.TRUE).when(warehouse).isExternalWarehouse();
        Mockito.doReturn(deliveryMode).when(consignment).getDeliveryMode();
        tgtSplitWithRoutingStrategy.afterSplitting(orderEntryGroup, consignment);

        Mockito.verify(consignmentToWarehouseAssigner).assignConsignmentToExternalWarehouse(deliveryMode, consignment,
                warehouse);
    }

    @Test
    public void testAfterSplittingDefaultWarehouse() {

        // Warehouse is not set in oeg
        Mockito.doReturn(null).when(oegParameterHelper).getFulfilmentWarehouse(orderEntryGroup);
        Mockito.doReturn(deliveryMode).when(consignment).getDeliveryMode();
        tgtSplitWithRoutingStrategy.afterSplitting(orderEntryGroup, consignment);

        Mockito.verify(consignmentToWarehouseAssigner).assignDefaultWarehouse(consignment);
    }

    @Test
    public void testAfterSplittingCancellationWarehouse() {
        doReturn(Boolean.TRUE).when(oegParameterHelper).getNotAssignedOegFlag(orderEntryGroup);
        final Map<String, Long> assignedQtyMap = new HashMap<>();
        doReturn(assignedQtyMap).when(oegParameterHelper).getAssignedQty(orderEntryGroup);
        tgtSplitWithRoutingStrategy.afterSplitting(orderEntryGroup, consignment);
        verify(consignmentToWarehouseAssigner).assignCancellationWarehouseAndUpdateConsignmentStatus(
                consignment);
        verify(targetBusinessProcessService).startOrderItemCancelProcess(any(OrderModel.class),
                any(TargetOrderCancelEntryList.class));
    }

    private void setupGlobalInstoreFulfilmentRulesPassed(final boolean success) {

        Mockito.doReturn(Boolean.valueOf(success)).when(instoreFulfilmentGlobalRulesChecker)
                .areAllGlobalRulesPassed(orderEntryGroup);
    }

    private void setupDispatchInstoreFulfilmentRulesPassed(final boolean success) {

        Mockito.doReturn(Boolean.valueOf(success)).when(instoreFulfilmentGlobalRulesChecker)
                .areAllDispatchRulesPassed(orderEntryGroup);
    }

}
