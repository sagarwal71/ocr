/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;
import de.hybris.platform.stock.exception.StockLevelNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.warehouse.dao.TargetWarehouseDao;
import au.com.target.tgtfulfilment.exceptions.TargetSplitOrderException;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.SplitOegResult;
import au.com.target.tgtfulfilment.warehouseservices.TargetFulfilmentWarehouseService;


/**
 * @author vivek
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExternalWarehouseSelectorStrategyTest {

    @InjectMocks
    private final ExternalWarehouseSelectorStrategy exteralWarehouseStrategy = new ExternalWarehouseSelectorStrategy();

    @Mock
    private OrderModel order;

    @Mock
    private OrderEntryGroup oeg;

    @Spy
    private final OEGParameterHelper oegParameterHelper = new OEGParameterHelper();

    @Mock
    private TargetFulfilmentWarehouseService targetFulfilmentWarehouseService;

    @Mock
    private TargetStockService targetStockService;

    @Mock
    private TargetWarehouseDao targetWarehouseDao;

    @Mock
    private StockLevelModel stockLevel;

    @Mock
    private TargetZoneDeliveryModeModel deliveryMode;

    @Before
    public void before() {
        given(order.getDeliveryMode()).willReturn(deliveryMode);
    }

    @Test
    public void testSplitForNoExternalWarehouse() throws TargetSplitOrderException {

        final OrderEntryGroup newOeg = new OrderEntryGroup();
        final AbstractOrderEntryModel entry = mock(AbstractOrderEntryModel.class);
        given(entry.getOrder()).willReturn(order);
        newOeg.add(entry);
        doReturn("NoWarehouse").when(oegParameterHelper).getOrderCode(newOeg);
        given(targetWarehouseDao.getExternalWarehouses(true)).willReturn(null);
        final SplitOegResult result = exteralWarehouseStrategy.split(newOeg);
        assertThat(result.getAssignedOEGsToWarehouse()).isEmpty();
        assertThat(result.getNotAssignedToWarehouse()).hasSize(1);
    }

    @Test
    public void testSplitForProductsOfExternalWarehouse() throws TargetSplitOrderException {

        final WarehouseModel warehouse1 = mock(WarehouseModel.class);
        final WarehouseModel warehouse2 = mock(WarehouseModel.class);
        final List<WarehouseModel> warehouses = new ArrayList<>();
        final OrderEntryGroup newOeg = new OrderEntryGroup();


        final Map<String, Long> requiredQtyMap = new HashMap<>();
        requiredQtyMap.put("product1", Long.valueOf(10));
        requiredQtyMap.put("product2", Long.valueOf(3));
        requiredQtyMap.put("product3", Long.valueOf(1));
        newOeg.setParameter("REQUIRED_QTY", requiredQtyMap);

        warehouses.add(warehouse1);
        warehouses.add(warehouse2);

        final ArrayList<OrderEntryGroup> oegs = new ArrayList<>();
        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);
        final AbstractOrderEntryModel entry4 = mock(AbstractOrderEntryModel.class);

        final TargetProductModel product1 = mock(TargetProductModel.class);
        final TargetProductModel product2 = mock(TargetProductModel.class);
        final TargetProductModel product3 = mock(TargetProductModel.class);
        final TargetProductModel product4 = mock(TargetProductModel.class);

        given(entry1.getOrder()).willReturn(order);
        given(entry2.getOrder()).willReturn(order);
        given(entry3.getOrder()).willReturn(order);
        given(entry4.getOrder()).willReturn(order);

        given(entry1.getProduct()).willReturn(product1);
        given(entry2.getProduct()).willReturn(product2);
        given(entry3.getProduct()).willReturn(product3);
        given(entry4.getProduct()).willReturn(product4);

        given(product1.getCode()).willReturn("product1");
        given(product2.getCode()).willReturn("product2");
        given(product3.getCode()).willReturn("product3");
        given(product4.getCode()).willReturn("product4");

        newOeg.add(entry1);
        newOeg.add(entry2);
        newOeg.add(entry3);
        newOeg.add(entry4);
        oegs.add(newOeg);

        doReturn("DigitalSplitOrder").when(oegParameterHelper).getOrderCode(newOeg);
        given(targetStockService.checkAndGetStockLevel(entry1.getProduct(), warehouse1)).willReturn(stockLevel);
        given(targetStockService.checkAndGetStockLevel(entry2.getProduct(), warehouse2)).willReturn(stockLevel);
        given(targetStockService.checkAndGetStockLevel(entry3.getProduct(), warehouse2)).willReturn(stockLevel);
        given(targetStockService.checkAndGetStockLevel(entry4.getProduct(), warehouse2))
                .willThrow(new StockLevelNotFoundException("StockNotFound"));
        given(targetWarehouseDao.getExternalWarehouses(true)).willReturn(warehouses);

        final SplitOegResult result = exteralWarehouseStrategy.split(newOeg);

        assertThat(result.getAssignedOEGsToWarehouse()).isNotEmpty();
        assertThat(result.getNotAssignedToWarehouse()).isNotEmpty();
        assertThat(result.getAssignedOEGsToWarehouse()).hasSize(2);
        assertThat(result.getAssignedOEGsToWarehouse().get(0)).hasSize(1);
        assertThat(result.getAssignedOEGsToWarehouse().get(1)).hasSize(2);
        assertThat(result.getAssignedOEGsToWarehouse().get(0).get(0)).isEqualTo(entry1);
        assertThat(result.getAssignedOEGsToWarehouse().get(1).get(0)).isEqualTo(entry2);
        assertThat(result.getAssignedOEGsToWarehouse().get(1).get(1)).isEqualTo(entry3);
        assertThat(result.getNotAssignedToWarehouse()).hasSize(1);
        assertThat(result.getNotAssignedToWarehouse().get(0)).isEqualTo(entry4);
    }

    @Test
    public void testSplitForProductsWhenMixedBasketWithNormalProductDigitalGiftCardAndPhysicalGiftcard()
            throws TargetSplitOrderException {

        final WarehouseModel warehouse1 = mock(WarehouseModel.class);
        final WarehouseModel warehouse2 = mock(WarehouseModel.class);
        final List<WarehouseModel> warehouses = new ArrayList<>();
        final OrderEntryGroup newOeg = new OrderEntryGroup();

        final Map<String, Long> requiredQtyMap = new HashMap<>();
        requiredQtyMap.put("product1", Long.valueOf(10));
        requiredQtyMap.put("product2", Long.valueOf(3));
        requiredQtyMap.put("product3", Long.valueOf(1));
        newOeg.setParameter("REQUIRED_QTY", requiredQtyMap);

        warehouses.add(warehouse1);
        warehouses.add(warehouse2);

        final ArrayList<OrderEntryGroup> oegs = new ArrayList<>();
        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);

        final TargetProductModel product1 = mock(TargetProductModel.class);

        final TargetProductModel product2 = mock(TargetProductModel.class);
        final ProductTypeModel digital = new ProductTypeModel();
        digital.setCode("digital");
        given(product2.getProductType()).willReturn(digital);

        final TargetProductModel product3 = mock(TargetProductModel.class);
        final ProductTypeModel giftCard = new ProductTypeModel();
        giftCard.setCode("giftCard");
        given(product3.getProductType()).willReturn(giftCard);

        given(entry1.getProduct()).willReturn(product1);
        given(entry2.getProduct()).willReturn(product2);
        given(entry3.getProduct()).willReturn(product3);

        given(product1.getCode()).willReturn("product1");
        given(product2.getCode()).willReturn("product2");
        given(product3.getCode()).willReturn("product3");

        given(entry1.getOrder()).willReturn(order);

        newOeg.add(entry1);
        newOeg.add(entry2);
        newOeg.add(entry3);
        oegs.add(newOeg);

        doReturn("mixedBasket").when(oegParameterHelper).getOrderCode(newOeg);
        given(targetStockService.checkAndGetStockLevel(entry1.getProduct(), warehouse1)).willReturn(null);
        given(targetStockService.checkAndGetStockLevel(entry2.getProduct(), warehouse2)).willReturn(stockLevel);
        given(targetStockService.checkAndGetStockLevel(entry3.getProduct(), warehouse2)).willReturn(stockLevel);
        given(targetWarehouseDao.getExternalWarehouses(true)).willReturn(warehouses);

        final SplitOegResult result = exteralWarehouseStrategy.split(newOeg);

        assertThat(result.getAssignedOEGsToWarehouse()).isNotEmpty();
        assertThat(result.getNotAssignedToWarehouse()).isNotEmpty();
        assertThat(result.getAssignedOEGsToWarehouse()).hasSize(2);
        assertThat(result.getAssignedOEGsToWarehouse().get(0)).hasSize(1);
        assertThat(result.getAssignedOEGsToWarehouse().get(1)).hasSize(1);
        assertThat(result.getNotAssignedToWarehouse()).hasSize(1);
        assertThat(result.getNotAssignedToWarehouse().get(0)).isEqualTo(entry1);
        assertThat(result.getAssignedOEGsToWarehouse().get(0).get(0)).isEqualTo(entry2);
        assertThat(result.getAssignedOEGsToWarehouse().get(1).get(0)).isEqualTo(entry3);

    }
}