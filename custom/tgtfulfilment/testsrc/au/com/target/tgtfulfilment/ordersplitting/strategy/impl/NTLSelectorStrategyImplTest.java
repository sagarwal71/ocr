/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.SplitOegResult;
import au.com.target.tgtfulfilment.service.TargetFulfilmentPointOfServiceService;


/**
 * @author Nandini
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class NTLSelectorStrategyImplTest {

    @InjectMocks
    private final NTLSelectorStrategyImpl ntlSelectorStrategy = new NTLSelectorStrategyImpl();

    @Mock
    private AbstractOrderModel order;

    @Mock
    private AbstractOrderEntryModel entry;

    @Mock
    private ProductModel productModel;

    private final String productCode = "testcode";

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private AddressModel addressModel;

    @Mock
    private RegionModel regionModel;

    @Mock
    private TargetPointOfServiceModel posModel;

    @Mock
    private TargetFulfilmentPointOfServiceService targetFulfilmentPOSService;

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private WarehouseModel warehouse;

    @Mock
    private TargetStockService targetStockService;

    private List<WarehouseModel> warehouses;

    @Before
    public void setup() {
        final OEGParameterHelper helper = new OEGParameterHelper();
        helper.setTargetWarehouseService(targetWarehouseService);
        ntlSelectorStrategy.setOegParameterHelper(helper);
        oeg.setParameter("DELIVERY_ADDRESS", addressModel);
        oeg.setParameter("CNC_STORE_NUMBER", null);
        given(targetWarehouseService.getWarehouseForPointOfService(posModel)).willReturn(warehouse);
        Mockito.when(addressModel.getRegion()).thenReturn(regionModel);
        Mockito.when(targetFulfilmentPOSService.getNTLByAddress(addressModel)).thenReturn(posModel);
        Mockito.doReturn(warehouse).when(targetWarehouseService).getWarehouseForPointOfService(posModel);
        when(entry.getProduct()).thenReturn(productModel);
        when(productModel.getCode()).thenReturn(productCode);
        warehouses = new ArrayList<>();
        warehouses.add(warehouse);
    }

    @Test
    public void testSplitNoNTLNullPos() {
        Mockito.when(targetFulfilmentPOSService.getNTLByAddress(addressModel)).thenReturn(null);

        final SplitOegResult result = ntlSelectorStrategy.split(oeg);
        Assert.assertNotNull(result.getNotAssignedToWarehouse());
        Assert.assertNull(result.getAssignedOEGsToWarehouse());
        Assert.assertEquals(oeg, result.getNotAssignedToWarehouse());
    }

    @Test
    public void testSplitNoNTLNullWarehouse() {
        Mockito.doReturn(null).when(targetWarehouseService).getWarehouseForPointOfService(posModel);

        final SplitOegResult result = ntlSelectorStrategy.split(oeg);
        Assert.assertNotNull(result.getNotAssignedToWarehouse());
        Assert.assertNull(result.getAssignedOEGsToWarehouse());
        Assert.assertEquals(oeg, result.getNotAssignedToWarehouse());

    }

    @Test
    public void testSplitFulfilWholeConsByNTL() {
        final OrderEntryGroup newOeg = new OrderEntryGroup();
        newOeg.add(entry);
        final Map<String, Long> requiredQty = new HashMap<>();
        requiredQty.put(productCode, Long.valueOf(3));
        newOeg.setParameter("REQUIRED_QTY", requiredQty);
        final ArrayList<OrderEntryGroup> oegs = new ArrayList<>();
        oegs.add(newOeg);
        newOeg.setParameter("DELIVERY_ADDRESS", addressModel);
        Mockito.when(
                Boolean.valueOf(targetStockService.isOrderEntryInStockAtWarehouse(entry, warehouse, Long.valueOf(3))))
                .thenReturn(
                        Boolean.TRUE);
        final SplitOegResult result = ntlSelectorStrategy.split(newOeg);
        Assert.assertNotNull(result.getAssignedOEGsToWarehouse());
        Assert.assertNull(result.getNotAssignedToWarehouse());
        Assert.assertEquals(oegs.size(), result.getAssignedOEGsToWarehouse().size());
        Assert.assertEquals(entry, result.getAssignedOEGsToWarehouse().get(0).get(0));
    }

    @Test
    public void testSplitIntoTwoOEG() {
        final String p1code = "p1code";
        final String p2code = "p2code";
        final ProductModel p1 = mock(ProductModel.class);
        given(p1.getCode()).willReturn(p1code);
        final ProductModel p2 = mock(ProductModel.class);
        given(p2.getCode()).willReturn(p2code);
        final AbstractOrderEntryModel ntlEntry = Mockito.mock(AbstractOrderEntryModel.class);
        given(ntlEntry.getProduct()).willReturn(p1);
        final AbstractOrderEntryModel nonNtlEntry = Mockito.mock(AbstractOrderEntryModel.class);
        given(nonNtlEntry.getProduct()).willReturn(p2);
        final OrderEntryGroup newOeg = new OrderEntryGroup();
        newOeg.add(ntlEntry);
        newOeg.add(nonNtlEntry);

        newOeg.setParameter("DELIVERY_ADDRESS", addressModel);
        newOeg.setParameter("CNC_STORE_NUMBER", null);
        final Map<String, Long> requiredQty = new HashMap<>();
        requiredQty.put(p1code, Long.valueOf(3));
        requiredQty.put(p2code, Long.valueOf(4));
        newOeg.setParameter("REQUIRED_QTY", requiredQty);

        Mockito.doReturn(Boolean.TRUE).when(targetStockService)
                .isOrderEntryInStockAtWarehouse(ntlEntry, warehouse, Long.valueOf(3));
        Mockito.doReturn(Boolean.FALSE).when(targetStockService)
                .isOrderEntryInStockAtWarehouse(nonNtlEntry, warehouse, Long.valueOf(4));

        final SplitOegResult result = ntlSelectorStrategy.split(newOeg);
        Assert.assertNotNull(result.getAssignedOEGsToWarehouse());
        Assert.assertNotNull(result.getNotAssignedToWarehouse());
        Assert.assertEquals(ntlEntry, result.getAssignedOEGsToWarehouse().get(0).get(0));
        Assert.assertEquals(nonNtlEntry, result.getNotAssignedToWarehouse().get(0));

        // assert the assigned Oeg
        final Map<String, Long> assignedQtyMap = (Map<String, Long>)result.getAssignedOEGsToWarehouse().get(0)
                .getParameter("ASSIGNED_QTY");
        assertThat(assignedQtyMap).hasSize(1);
        final Map<String, Long> expectedAssignedQtyMap = new HashMap<>();
        expectedAssignedQtyMap.put(p1code, Long.valueOf(3));
        assertThat(assignedQtyMap).isIn(expectedAssignedQtyMap);


        // assert the notAssigned oeg
        final OrderEntryGroup notAssigned = result.getNotAssignedToWarehouse();
        final List<AbstractOrderEntryModel> notAssignedEntries = notAssigned;
        final Map<String, Long> notAssignedQtyMap = (Map<String, Long>)notAssigned.getParameter("REQUIRED_QTY");
        final Map<String, Long> expectednotAssignedQtyMap = new HashMap<>();
        expectednotAssignedQtyMap.put(p2code, Long.valueOf(4));
        assertThat(notAssignedEntries).hasSize(1);
        assertThat(notAssignedEntries.get(0)).isEqualTo(nonNtlEntry);
        assertThat(notAssignedQtyMap).hasSize(1);
        assertThat(notAssignedQtyMap).isIn(expectednotAssignedQtyMap);
    }
}
