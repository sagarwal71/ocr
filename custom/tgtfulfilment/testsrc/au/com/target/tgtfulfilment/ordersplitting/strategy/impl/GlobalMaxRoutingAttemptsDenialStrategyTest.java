/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;


/**
 * @author smishra1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GlobalMaxRoutingAttemptsDenialStrategyTest {

    @Mock
    private TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService;

    @Mock
    private TargetStoreConsignmentService targetStoreConsignmentService;

    @Mock
    private GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel;

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private TargetConsignmentModel consignment1;

    @Mock
    private OrderModel order;

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private AbstractOrderEntryModel orderEntry;

    @InjectMocks
    private final GlobalMaxRoutingAttemptsDenialStrategy routingAttemptStrategy = new GlobalMaxRoutingAttemptsDenialStrategy();

    @Before
    public void setUp() {
        BDDMockito.given(targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites()).willReturn(
                capabilitiesModel);
        BDDMockito.given(oeg.get(0)).willReturn(orderEntry);
        BDDMockito.given(orderEntry.getOrder()).willReturn(order);
    }

    /**
     * TestCase : Maximum number of allowed attempts is 0 and rejected by store is false.
     * 
     * Result : Response should be denied(isDenied = true)
     *
     */
    @Test
    public void testReroutingMaxAttemptsWithMax0AndReject0() {
        BDDMockito.given(capabilitiesModel.getNumberOfRoutingAttempts()).willReturn(Integer.valueOf(0));
        final Set<ConsignmentModel> consignmentSet = new HashSet<>();
        consignmentSet.add(consignment);
        BDDMockito.given(order.getConsignments()).willReturn(consignmentSet);
        BDDMockito.given(consignment.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        BDDMockito.given(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyStore(consignment)))
                .willReturn(Boolean.valueOf(false));
        final DenialResponse denialResponse = routingAttemptStrategy.isDenied(oeg);
        assertThat(denialResponse.isDenied()).isTrue();
    }

    @Test
    public void testReroutingMaxAttemptsWithMax0AndNoConsignments() {
        BDDMockito.given(capabilitiesModel.getNumberOfRoutingAttempts()).willReturn(Integer.valueOf(0));
        final Set<ConsignmentModel> consignmentSet = new HashSet<>();
        BDDMockito.given(order.getConsignments()).willReturn(consignmentSet);
        final DenialResponse denialResponse = routingAttemptStrategy.isDenied(oeg);
        assertThat(denialResponse.isDenied()).isTrue();
    }

    @Test
    public void testReroutingMaxAttemptsWithMaxMinus1AndNoConsignments() {
        BDDMockito.given(capabilitiesModel.getNumberOfRoutingAttempts()).willReturn(Integer.valueOf(-1));
        final Set<ConsignmentModel> consignmentSet = new HashSet<>();
        BDDMockito.given(order.getConsignments()).willReturn(consignmentSet);
        final DenialResponse denialResponse = routingAttemptStrategy.isDenied(oeg);
        assertThat(denialResponse.isDenied()).isTrue();
    }

    @Test
    public void testReroutingMaxAttemptsWithMax0CapabilityModelNull() {
        BDDMockito.given(targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites()).willReturn(null);
        final Set<ConsignmentModel> consignmentSet = new HashSet<>();
        BDDMockito.given(order.getConsignments()).willReturn(consignmentSet);
        final DenialResponse denialResponse = routingAttemptStrategy.isDenied(oeg);
        assertThat(denialResponse.isDenied()).isTrue();
    }


    /**
     * TestCase : Maximum number of allowed attempts is 0 and rejected by store is true.
     * 
     * Result : Response should be denied(isDenied = true)
     * 
     *
     */
    @Test
    public void testReroutingMaxAttemptsWithMax0AndReject1() {
        BDDMockito.given(capabilitiesModel.getNumberOfRoutingAttempts()).willReturn(Integer.valueOf(0));
        final Set<ConsignmentModel> consignmentSet = new HashSet<>();
        consignmentSet.add(consignment);
        BDDMockito.given(order.getConsignments()).willReturn(consignmentSet);
        BDDMockito.given(consignment.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        BDDMockito.given(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyStore(consignment)))
                .willReturn(Boolean.valueOf(true));
        final DenialResponse denialResponse = routingAttemptStrategy.isDenied(oeg);
        assertThat(denialResponse.isDenied()).isTrue();
    }

    @Test
    public void testReroutingMaxAttemptsWithMaxMinus1AndReject1() {
        BDDMockito.given(capabilitiesModel.getNumberOfRoutingAttempts()).willReturn(Integer.valueOf(-1));
        final Set<ConsignmentModel> consignmentSet = new HashSet<>();
        consignmentSet.add(consignment);
        BDDMockito.given(order.getConsignments()).willReturn(consignmentSet);
        BDDMockito.given(consignment.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        BDDMockito.given(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyStore(consignment)))
                .willReturn(Boolean.valueOf(true));
        final DenialResponse denialResponse = routingAttemptStrategy.isDenied(oeg);
        assertThat(denialResponse.isDenied()).isTrue();
    }

    @Test
    public void testReroutingMaxAttemptsWithMax1AndReject0NoConsignments() {
        BDDMockito.given(capabilitiesModel.getNumberOfRoutingAttempts()).willReturn(Integer.valueOf(1));
        final Set<ConsignmentModel> consignmentSet = new HashSet<>();
        BDDMockito.given(order.getConsignments()).willReturn(consignmentSet);
        final DenialResponse denialResponse = routingAttemptStrategy.isDenied(oeg);
        assertThat(denialResponse.isDenied()).isFalse();
    }

    /**
     * TestCase : Maximum number of allowed attempts is 1 and rejected by store is false. Result :
     * 
     * Response should be allow(isDenied = false)
     *
     */
    @Test
    public void testReroutingMaxAttemptsWithMax1AndReject0() {
        BDDMockito.given(capabilitiesModel.getNumberOfRoutingAttempts()).willReturn(Integer.valueOf(1));
        final Set<ConsignmentModel> consignmentSet = new HashSet<>();
        consignmentSet.add(consignment);
        BDDMockito.given(order.getConsignments()).willReturn(consignmentSet);
        BDDMockito.given(consignment.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        BDDMockito.given(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyStore(consignment)))
                .willReturn(Boolean.valueOf(false));
        final DenialResponse denialResponse = routingAttemptStrategy.isDenied(oeg);
        assertThat(denialResponse.isDenied()).isFalse();
    }

    /**
     * TestCase : Maximum number of allowed attempts is 1 and rejected by store is true.
     * 
     * Result : Response should be denied(isDenied = true)
     *
     */
    @Test
    public void testReroutingMaxAttemptsWithMax1AndReject1() {
        BDDMockito.given(capabilitiesModel.getNumberOfRoutingAttempts()).willReturn(Integer.valueOf(1));
        final Set<ConsignmentModel> consignmentSet = new HashSet<>();
        consignmentSet.add(consignment);
        BDDMockito.given(order.getConsignments()).willReturn(consignmentSet);
        BDDMockito.given(consignment.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        BDDMockito.given(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyStore(consignment)))
                .willReturn(Boolean.valueOf(true));
        final DenialResponse denialResponse = routingAttemptStrategy.isDenied(oeg);
        assertThat(denialResponse.isDenied()).isTrue();
    }

    /**
     * TestCase : Maximum number of allowed attempts is 2 and rejected by store is 0.
     * 
     * Result : Response should be allow(isDenied = false)
     *
     */
    @Test
    public void testReroutingMaxAttemptsWithMax2AndReject0() {
        BDDMockito.given(capabilitiesModel.getNumberOfRoutingAttempts()).willReturn(Integer.valueOf(2));
        final Set<ConsignmentModel> consignmentSet = new HashSet<>();
        consignmentSet.add(consignment);
        consignmentSet.add(consignment1);
        BDDMockito.given(order.getConsignments()).willReturn(consignmentSet);
        BDDMockito.given(consignment.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        BDDMockito.given(consignment1.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        BDDMockito.given(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyStore(consignment)))
                .willReturn(Boolean.valueOf(false));
        BDDMockito.given(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyStore(consignment1)))
                .willReturn(Boolean.valueOf(false));
        final DenialResponse denialResponse = routingAttemptStrategy.isDenied(oeg);
        assertThat(denialResponse.isDenied()).isFalse();
    }

    /**
     * TestCase : Maximum number of allowed attempts is 2 and rejected by store is 1.
     * 
     * Result : Response should be allow(isDenied = false)
     *
     */
    @Test
    public void testReroutingMaxAttemptsWithMax2AndReject1() {
        BDDMockito.given(capabilitiesModel.getNumberOfRoutingAttempts()).willReturn(Integer.valueOf(2));
        final Set<ConsignmentModel> consignmentSet = new HashSet<>();
        consignmentSet.add(consignment);
        consignmentSet.add(consignment1);
        BDDMockito.given(order.getConsignments()).willReturn(consignmentSet);
        BDDMockito.given(consignment.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        BDDMockito.given(consignment1.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        BDDMockito.given(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyStore(consignment)))
                .willReturn(Boolean.valueOf(false));
        BDDMockito.given(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyStore(consignment1)))
                .willReturn(Boolean.valueOf(true));
        final DenialResponse denialResponse = routingAttemptStrategy.isDenied(oeg);
        assertThat(denialResponse.isDenied()).isFalse();
    }

    /**
     * TestCase : Maximum number of allowed attempts is 2 and rejected by store is 2.
     * 
     * Result : Response should be allow(isDenied = false)
     *
     */
    @Test
    public void testReroutingMaxAttemptsWithMax2AndReject2() {
        BDDMockito.given(capabilitiesModel.getNumberOfRoutingAttempts()).willReturn(Integer.valueOf(2));
        final Set<ConsignmentModel> consignmentSet = new HashSet<>();
        consignmentSet.add(consignment);
        consignmentSet.add(consignment1);
        BDDMockito.given(order.getConsignments()).willReturn(consignmentSet);
        BDDMockito.given(consignment.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        BDDMockito.given(consignment1.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        BDDMockito.given(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyStore(consignment)))
                .willReturn(Boolean.valueOf(true));
        BDDMockito.given(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyStore(consignment1)))
                .willReturn(Boolean.valueOf(true));
        final DenialResponse denialResponse = routingAttemptStrategy.isDenied(oeg);
        assertThat(denialResponse.isDenied()).isTrue();
    }

    /**
     * TestCase : Maximum number of allowed attempts is 2 and rejected by store is 1 and the status of one of the
     * consignment is picked.
     * 
     * Result : Response should be allow(isDenied = false)
     *
     */
    @Test
    public void testReroutingMaxAttemptsWithOnlyOneRejectAndOneCreated() {
        BDDMockito.given(capabilitiesModel.getNumberOfRoutingAttempts()).willReturn(Integer.valueOf(2));
        final Set<ConsignmentModel> consignmentSet = new HashSet<>();
        consignmentSet.add(consignment);
        consignmentSet.add(consignment1);
        BDDMockito.given(order.getConsignments()).willReturn(consignmentSet);
        BDDMockito.given(consignment.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        BDDMockito.given(consignment1.getStatus()).willReturn(ConsignmentStatus.CREATED);
        BDDMockito.given(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyStore(consignment)))
                .willReturn(Boolean.valueOf(true));
        BDDMockito.given(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyStore(consignment1)))
                .willReturn(Boolean.valueOf(true));
        final DenialResponse denialResponse = routingAttemptStrategy.isDenied(oeg);
        assertThat(denialResponse.isDenied()).isFalse();
    }

    /**
     * TestCase : no consignments in the order.
     * 
     * Result : Response should be Allow(isDenied = false)
     *
     */
    @Test
    public void testReroutingMaxAttemptsWithNoConsignments() {
        BDDMockito.given(capabilitiesModel.getNumberOfRoutingAttempts()).willReturn(Integer.valueOf(2));
        BDDMockito.given(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyStore(consignment)))
                .willReturn(Boolean.valueOf(true));
        final DenialResponse denialResponse = routingAttemptStrategy.isDenied(oeg);
        assertThat(denialResponse.isDenied()).isFalse();
    }

    /**
     * TestCase : value of maximum attempts not set
     * 
     * Result : Response should be Allow(isDenied = false)
     *
     */
    @SuppressWarnings("boxing")
    @Test
    public void testReroutingMaxAttemptsWithMaxAttemptsNull() {
        BDDMockito.given(capabilitiesModel.getNumberOfRoutingAttempts()).willReturn(null);
        final DenialResponse denialResponse = routingAttemptStrategy.isDenied(oeg);
        assertThat(denialResponse.isDenied()).isTrue();
    }

}
