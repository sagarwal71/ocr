/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;
import au.com.target.tgtfulfilment.service.TargetCarrierSelectionService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ConsignmentToWarehouseAssignerTest {

    private static final String ORDER_CODE = "testOrder";

    @Mock
    private TargetCarrierSelectionService targetCarrierSelectionService;

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private TargetDeliveryService targetDeliveryService;

    @InjectMocks
    private final ConsignmentToWarehouseAssignerImpl assigner = new ConsignmentToWarehouseAssignerImpl();


    @Mock
    private TargetCarrierModel nullCarrier;

    @Mock
    private TargetCarrierModel storePreferredCarrier;

    @Mock
    private TargetCarrierModel bulkyCarrier;

    @Mock
    private TargetCarrierModel nonbulkyCarrier;

    @Mock
    private TargetCarrierModel storeCarrier;

    @Mock
    private WarehouseModel warehouse;

    @Mock
    private TargetZoneDeliveryModeModel deliveryMode;

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private Set<ConsignmentEntryModel> conEntries;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Before
    public void setup() {

        doReturn(storePreferredCarrier).when(targetCarrierSelectionService)
                .getPreferredCarrier(consignment, false,
                        true);
        doReturn(nullCarrier).when(targetCarrierSelectionService).getNullCarrier(deliveryMode);

        doReturn(conEntries).when(consignment).getConsignmentEntries();

        doReturn(bulkyCarrier).when(targetCarrierSelectionService).getPreferredCarrier(consignment,
                true, false);

        doReturn(nonbulkyCarrier).when(targetCarrierSelectionService).getPreferredCarrier(consignment,
                false, false);


    }

    @Test
    public void testAssignToStoreSameStore() {

        assigner.assignConsignmentToStore(consignment, warehouse, OfcOrderType.INSTORE_PICKUP, deliveryMode,
                ORDER_CODE);

        verify(consignment).setWarehouse(warehouse);
        verify(consignment).setStatus(ConsignmentStatus.CREATED);
        verify(consignment).setTargetCarrier(nullCarrier);
    }

    @Test
    public void testAssignToStoreInterStore() {

        assigner.assignConsignmentToStore(consignment, warehouse, OfcOrderType.INTERSTORE_DELIVERY, deliveryMode,
                ORDER_CODE);

        verify(consignment).setWarehouse(warehouse);
        verify(consignment).setStatus(ConsignmentStatus.CREATED);
        verify(consignment).setTargetCarrier(storePreferredCarrier);
    }

    @Test
    public void testAssignToStoreCustomerDelivery() {

        assigner.assignConsignmentToStore(consignment, warehouse, OfcOrderType.CUSTOMER_DELIVERY, deliveryMode,
                ORDER_CODE);

        verify(consignment).setWarehouse(warehouse);
        verify(consignment).setStatus(ConsignmentStatus.CREATED);
        verify(consignment).setTargetCarrier(storePreferredCarrier);
    }

    @Test
    public void testAssignToNTLWithBulky() {

        doReturn(Boolean.TRUE).when(targetDeliveryService).consignmentContainsBulkyProducts(conEntries);

        assigner.assignConsignmentToNTL(consignment, warehouse);

        verify(consignment).setWarehouse(warehouse);
        verify(consignment).setStatus(ConsignmentStatus.CREATED);
        verify(consignment).setTargetCarrier(bulkyCarrier);
    }

    @Test
    public void testAssignToNTLWithNonBulky() {

        doReturn(Boolean.FALSE).when(targetDeliveryService).consignmentContainsBulkyProducts(conEntries);

        assigner.assignConsignmentToNTL(consignment, warehouse);

        verify(consignment).setWarehouse(warehouse);
        verify(consignment).setStatus(ConsignmentStatus.CREATED);
        verify(consignment).setTargetCarrier(nonbulkyCarrier);
    }

    @Test
    public void testAssignToWarehouseDigital() {

        doReturn(Boolean.TRUE).when(deliveryMode).getIsDigital();

        assigner.assignConsignmentToExternalWarehouse(deliveryMode, consignment, warehouse);

        verify(consignment).setWarehouse(warehouse);
        verify(consignment).setStatus(ConsignmentStatus.CREATED);
        verify(consignment).setTargetCarrier(nullCarrier);
    }

    @Test
    public void testAssignToWarehouseNonDigital() {

        doReturn(Boolean.FALSE).when(deliveryMode).getIsDigital();

        doReturn(Boolean.TRUE).when(targetDeliveryService).consignmentContainsBulkyProducts(conEntries);
        assigner.assignConsignmentToExternalWarehouse(deliveryMode, consignment, warehouse);

        verify(consignment).setWarehouse(warehouse);
        verify(consignment).setStatus(ConsignmentStatus.CREATED);
        verify(consignment).setTargetCarrier(bulkyCarrier);
    }

    @Test
    public void testAssignToExternalWarehouseBulkyProducts() {

        doReturn(null).when(deliveryMode).getIsDigital();

        doReturn(Boolean.TRUE).when(targetDeliveryService).consignmentContainsBulkyProducts(conEntries);
        assigner.assignConsignmentToExternalWarehouse(deliveryMode, consignment, warehouse);

        verify(consignment).setWarehouse(warehouse);
        verify(consignment).setStatus(ConsignmentStatus.CREATED);

        // Sets to bulky carrier for bulky product in consignment
        verify(consignment).setTargetCarrier(bulkyCarrier);
    }

    @Test
    public void testAssignToExternalWarehouseNormalProducts() {

        doReturn(null).when(deliveryMode).getIsDigital();

        doReturn(Boolean.FALSE).when(targetDeliveryService).consignmentContainsBulkyProducts(conEntries);
        assigner.assignConsignmentToExternalWarehouse(deliveryMode, consignment, warehouse);

        verify(consignment).setWarehouse(warehouse);
        verify(consignment).setStatus(ConsignmentStatus.CREATED);

        // Sets to Non bulky carrier for normal product in consignment
        verify(consignment).setTargetCarrier(nonbulkyCarrier);
    }

    @Test
    public void testAssignToDefaultWarehouseWithBulky() {

        doReturn(Boolean.TRUE).when(targetDeliveryService).consignmentContainsBulkyProducts(conEntries);
        doReturn(warehouse).when(targetWarehouseService).getDefaultOnlineWarehouse();

        assigner.assignDefaultWarehouse(consignment);

        verify(consignment).setWarehouse(warehouse);
        verify(consignment).setStatus(ConsignmentStatus.CREATED);
        verify(consignment).setTargetCarrier(bulkyCarrier);
    }

    @Test
    public void testAssignToDefaultWarehouseWithNonBulky() {

        doReturn(Boolean.FALSE).when(targetDeliveryService).consignmentContainsBulkyProducts(conEntries);
        doReturn(warehouse).when(targetWarehouseService).getDefaultOnlineWarehouse();

        assigner.assignDefaultWarehouse(consignment);

        verify(consignment).setWarehouse(warehouse);
        verify(consignment).setStatus(ConsignmentStatus.CREATED);
        verify(consignment).setTargetCarrier(nonbulkyCarrier);
    }

    @Test
    public void testAssignToDefaultWarehouseWithForceDefaultWarehouseToOnlinePOS() {

        doReturn(Boolean.FALSE).when(targetDeliveryService).consignmentContainsBulkyProducts(conEntries);
        doReturn(warehouse).when(targetWarehouseService).getDefaultOnlineWarehouse();
        doReturn(Boolean.TRUE).when(targetFeatureSwitchService).isFeatureEnabled(
                TgtCoreConstants.FeatureSwitch.FORCE_DEFAULT_WAREHOUSE_TO_ONLINE_POS);
        assigner.assignDefaultWarehouse(consignment);

        verify(consignment).setWarehouse(warehouse);
        verify(consignment).setStatus(ConsignmentStatus.CREATED);
        verify(consignment).setTargetCarrier(storePreferredCarrier);
    }
}
