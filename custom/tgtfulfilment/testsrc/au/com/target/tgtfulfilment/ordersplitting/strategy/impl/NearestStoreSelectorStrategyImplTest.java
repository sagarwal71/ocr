/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.storelocator.GPS;
import de.hybris.platform.storelocator.GeoWebServiceWrapper;
import de.hybris.platform.storelocator.data.AddressData;
import de.hybris.platform.storelocator.exception.GeoServiceWrapperException;
import de.hybris.platform.storelocator.impl.DefaultGPS;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.dao.TargetPointOfServiceDao;
import au.com.target.tgtcore.user.impl.TargetAddressService;
import au.com.target.tgtfulfilment.ordersplitting.strategy.NearestStoreSelectorStrategy;
import au.com.target.tgtfulfilment.service.TargetStoreLocatorService;


/**
 * @author smishra1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class NearestStoreSelectorStrategyImplTest {

    private static final String STORE_PREFIX = "99";

    @InjectMocks
    private final NearestStoreSelectorStrategy nearestStoreSelectorStrategyObject = new NearestStoreSelectorStrategyImpl();

    @Mock
    private AddressModel addressModel;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetAddressService addressService;

    @Mock
    private GeoWebServiceWrapper geoWebServiceWrapper;

    @Mock
    private TargetPointOfServiceDao targetPointOfServiceDao;

    @Mock
    private TargetStoreLocatorService targetStoreLocatorService;

    /**
     * Test method for finding the closed store with the following description
     * 
     * Delivery Address Co-ordinates (1.0,1.1)
     * 
     * StoreNumber ~~~ DistanceFromDeliveryAddress
     * =============================================================================================
     * 9900~~~164.98344807822235 9901~~~996.8450989192723 9910~~~1002.7010852500905 9911~~~1400.4056605696978
     */
    @Test
    public void testWithBasicAddressCoordinates() {
        given(geoWebServiceWrapper.geocodeAddress(Mockito.any(AddressData.class))).willReturn(
                new DefaultGPS(1, 1.1));
        final List<PointOfServiceDistanceData> sortedList = nearestStoreSelectorStrategyObject
                .getSortedStoreListByDistance(getPointOfServiceList(),
                        addressModel);
        assertThat(sortedList).isNotNull();
        assertThat(sortedList).isNotEmpty();
        assertThat(getStoreNumberForIndex(sortedList, 0)).isEqualTo(9900);
        assertThat(getStoreNumberForIndex(sortedList, 1)).isEqualTo(9901);
        assertThat(getStoreNumberForIndex(sortedList, 2)).isEqualTo(9910);
        assertThat(getStoreNumberForIndex(sortedList, 3)).isEqualTo(9911);
    }

    /**
     * Test method for finding the closed store with the following description
     * 
     * Delivery Address Co-ordinates (10.0,-1.1)
     * 
     * StoreNumber ~~~ DistanceFromDeliveryAddress
     * =============================================================================================
     * 9910~~~120.60324462249034 9900~~~1112.545594976819 9911~~~1216.9393431696133 9901~~~1653.5613117175794
     */
    @Test
    public void testWithDifferentCoordinates() {
        given(geoWebServiceWrapper.geocodeAddress(Mockito.any(AddressData.class))).willReturn(
                new DefaultGPS(10.0, -1.1));
        final List<PointOfServiceDistanceData> sortedList = nearestStoreSelectorStrategyObject
                .getSortedStoreListByDistance(getPointOfServiceList(),
                        addressModel);
        assertThat(sortedList).isNotNull();
        assertThat(sortedList).isNotEmpty();
        assertThat(getStoreNumberForIndex(sortedList, 0)).isEqualTo(9910);
        assertThat(getStoreNumberForIndex(sortedList, 1)).isEqualTo(9900);
        assertThat(getStoreNumberForIndex(sortedList, 2)).isEqualTo(9911);
        assertThat(getStoreNumberForIndex(sortedList, 3)).isEqualTo(9901);
    }

    /**
     * Test method for finding the closed store with the following description
     * 
     * Delivery Address Co-ordinates (5.0,5.0)
     * 
     * Closest store would be 9910 and 9911. The first one in the list would be 9910
     * 
     * StoreNumber ~~~ DistanceFromDeliveryAddress
     * =============================================================================================
     * 9910~~~781.1062793859637 9911~~~781.1062793859637 9900~~~784.0287398090262 9901~~~784.0287398090262
     */
    @Test
    public void testReturningMoreThanOneStoreWithSameDistance() {
        given(geoWebServiceWrapper.geocodeAddress(Mockito.any(AddressData.class))).willReturn(
                new DefaultGPS(5.0, 5.0));
        final List<PointOfServiceDistanceData> sortedList = nearestStoreSelectorStrategyObject
                .getSortedStoreListByDistance(getPointOfServiceList(),
                        addressModel);
        assertThat(sortedList).isNotNull();
        assertThat(sortedList).isNotEmpty();
        assertThat(getStoreNumberForIndex(sortedList, 0)).isEqualTo(9910);
        assertThat(getStoreNumberForIndex(sortedList, 1)).isEqualTo(9911);
        assertThat(getStoreNumberForIndex(sortedList, 2)).isEqualTo(9900);
        assertThat(getStoreNumberForIndex(sortedList, 3)).isEqualTo(9901);
    }

    /**
     * Test for the empty list of stores
     */
    @Test
    public void testWithEmptyList() {
        final List<PointOfServiceDistanceData> sortedList = nearestStoreSelectorStrategyObject
                .getSortedStoreListByDistance(getPointOfServiceList(),
                        addressModel);
        assertThat(sortedList).isNull();
    }

    /**
     * Test for the null list of stores
     */
    @Test
    public void testWithNullList() {
        final List<PointOfServiceDistanceData> sortedList = nearestStoreSelectorStrategyObject
                .getSortedStoreListByDistance(getPointOfServiceList(),
                        addressModel);
        assertThat(sortedList).isNull();
    }

    @Test
    public void testRetryGeocodeAddressOnError() {
        given(addressModel.getStreetname()).willReturn("Street that does not exist");
        given(addressModel.getStreetnumber()).willReturn("Building that does not exist");
        given(addressModel.getPostalcode()).willReturn("3215");
        given(addressModel.getTown()).willReturn("North Geelong");
        final CountryModel country = mock(CountryModel.class);
        given(addressModel.getCountry()).willReturn(country);
        given(country.getIsocode()).willReturn("AU");
        given(geoWebServiceWrapper.geocodeAddress(Mockito.any(AddressData.class))).willAnswer(new Answer<GPS>() {

            @Override
            public GPS answer(final InvocationOnMock invocation) throws Throwable {
                final AddressData addressData = (AddressData)invocation.getArguments()[0];
                if (StringUtils.isNotBlank(addressData.getBuilding())
                        || StringUtils.isNotBlank(addressData.getStreet())) {
                    throw new GeoServiceWrapperException("can't find address");
                }
                return new DefaultGPS(5.0, 5.0);
            }
        });

        final List<PointOfServiceDistanceData> sortedList = nearestStoreSelectorStrategyObject
                .getSortedStoreListByDistance(getPointOfServiceList(),
                        addressModel);
        assertThat(sortedList).isNotNull();
        assertThat(sortedList).isNotEmpty();
        assertThat(getStoreNumberForIndex(sortedList, 0)).isEqualTo(9910);
        assertThat(getStoreNumberForIndex(sortedList, 1)).isEqualTo(9911);
        assertThat(getStoreNumberForIndex(sortedList, 2)).isEqualTo(9900);
        assertThat(getStoreNumberForIndex(sortedList, 3)).isEqualTo(9901);
        final ArgumentCaptor<AddressData> addressDataCaptor = ArgumentCaptor.forClass(AddressData.class);
        verify(geoWebServiceWrapper, times(2)).geocodeAddress(addressDataCaptor.capture());
        assertThat(addressDataCaptor.getAllValues().get(0).getStreet()).isEqualTo("Street that does not exist");
        assertThat(addressDataCaptor.getAllValues().get(0).getBuilding()).isEqualTo("Building that does not exist");
        assertThat(addressDataCaptor.getAllValues().get(0).getZip()).isEqualTo("3215");
        assertThat(addressDataCaptor.getAllValues().get(0).getCity()).isEqualTo("North Geelong");
        assertThat(addressDataCaptor.getAllValues().get(0).getCountryCode()).isEqualTo("AU");
        assertThat(addressDataCaptor.getAllValues().get(1).getStreet()).isNull();
        assertThat(addressDataCaptor.getAllValues().get(1).getBuilding()).isNull();
        assertThat(addressDataCaptor.getAllValues().get(1).getZip()).isEqualTo("3215");
        assertThat(addressDataCaptor.getAllValues().get(1).getCity()).isNull();
        assertThat(addressDataCaptor.getAllValues().get(1).getCountryCode()).isEqualTo("AU");
    }

    /**
     * Test method for finding the closed store with the following description
     * 
     * Delivery Address Co-ordinates (1.0,1.1)
     * 
     * StoreNumber ~~~ DistanceFromDeliveryAddress
     * =============================================================================================
     * 9900~~~164.98344807822235 9901~~~996.8450989192723 9910~~~1002.7010852500905 9911~~~1400.4056605696978
     */
    @Test
    public void testWithBasicAddressCoordinatesWithDistanceRestriction() {
        given(geoWebServiceWrapper.geocodeAddress(Mockito.any(AddressData.class))).willReturn(
                new DefaultGPS(1, 1.1));
        final List<PointOfServiceDistanceData> sortedList = nearestStoreSelectorStrategyObject
                .getSortedStoreListByDistanceWithDistanceRestriction(getPointOfServiceList(),
                        addressModel, 1000.123d);
        assertThat(sortedList).isNotNull();
        assertThat(sortedList).hasSize(2);
        assertThat(getStoreNumberForIndex(sortedList, 0)).isEqualTo(9900);
        assertThat(getStoreNumberForIndex(sortedList, 1)).isEqualTo(9901);
    }

    /**
     * Method to setup the List of TargetPointOfServiceModel
     * 
     * @return list of TargetPointOfServiceModel with following stores and co-ordinates
     * 
     *         StoreNumber :: 9900 and coordinates :: (0.0, 0.0)
     * 
     *         StoreNumber :: 9901 and coordinates :: (0.0, 10.0)
     * 
     *         StoreNumber :: 9910 and coordinates :: (10.0, 0.0)
     * 
     *         StoreNumber :: 9911 and coordinates :: (10.0, 10.0)
     * 
     */
    private List<TargetPointOfServiceModel> getPointOfServiceList() {
        final List<TargetPointOfServiceModel> targetPointOfServiceList = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                final TargetPointOfServiceModel model = new TargetPointOfServiceModel();
                model.setLatitude(Double.valueOf(i * 10));
                model.setLongitude(Double.valueOf(j * 10));
                model.setStoreNumber(Integer.valueOf(STORE_PREFIX + String.valueOf(i) + String.valueOf(j)));
                targetPointOfServiceList.add(model);
            }
        }
        return targetPointOfServiceList;
    }

    private Integer getStoreNumberForIndex(final List<PointOfServiceDistanceData> sortedList, final int index) {
        final TargetPointOfServiceModel data = (TargetPointOfServiceModel)sortedList.get(index).getPointOfService();
        return data.getStoreNumber();
    }
}
