/**
 * 
 */
package au.com.target.tgtfulfilment.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.order.strategies.TaxInvoiceGenerationDenialStrategy;


/**
 * Unit test for {@link TaxInvoiceGenerationDenialStrategy}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderStatusTaxInvoiceGenerationDenialStrategyTest {
    private final TaxInvoiceGenerationDenialStrategy strategy = new OrderStatusTaxInvoiceGenerationDenialStrategy();


    @Test
    public void testIsDenied() {

        final OrderModel order = Mockito.mock(OrderModel.class);

        BDDMockito.given(order.getStatus()).willReturn(OrderStatus.INVOICED);
        Assert.assertFalse(strategy.isDenied(order));

        BDDMockito.given(order.getStatus()).willReturn(OrderStatus.COMPLETED);
        Assert.assertFalse(strategy.isDenied(order));

        BDDMockito.given(order.getStatus()).willReturn(OrderStatus.CREATED);
        Assert.assertTrue(strategy.isDenied(order));
    }
}
