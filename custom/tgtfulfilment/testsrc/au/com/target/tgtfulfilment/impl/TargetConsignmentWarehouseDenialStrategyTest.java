/**
 * 
 */
package au.com.target.tgtfulfilment.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;



/**
 * @author pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetConsignmentWarehouseDenialStrategyTest {

    @InjectMocks
    private final TargetConsignmentWarehouseDenialStrategy strategy = new TargetConsignmentWarehouseDenialStrategy();

    @Mock
    private OrderModel order;

    @Mock
    private OrderEntryModel orderEntry;

    @Mock
    private WarehouseModel warehouse;

    @Mock
    private ConsignmentModel consignment;

    @Mock
    private ConsignmentEntryModel consignmentEntryOne;

    @Mock
    private ConsignmentEntryModel consignmentEntryTwo;

    private Set<ConsignmentEntryModel> consignmentEntries;

    @Before
    public void setUp() {
        consignmentEntries = new HashSet<>();
    }


    @Test(expected = IllegalArgumentException.class)
    public void testPerformWhenOrderIsNull() {
        strategy.perform(null, orderEntry, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPerformWhenOrderEntryIsNull() {
        strategy.perform(order, null, 1);
    }

    @Test
    public void testPerformWhenOrderEntryIsAssignedToWarehouseWhichAllowsRefund() {
        Mockito.when(orderEntry.getConsignmentEntries()).thenReturn(consignmentEntries);
        consignmentEntries.add(consignmentEntryOne);
        Mockito.when(consignmentEntryOne.getConsignment()).thenReturn(consignment);
        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.SHIPPED);
        Mockito.when(consignment.getWarehouse()).thenReturn(warehouse);
        Mockito.when(Boolean.valueOf(warehouse.isDenyRefund())).thenReturn(Boolean.FALSE);
        Assert.assertTrue(strategy.perform(order, orderEntry, 1));
    }

    @Test
    public void testPerformWhenOrderEntryIsAssignedToWarehouseWhichDeniesRefund() {
        Mockito.when(orderEntry.getConsignmentEntries()).thenReturn(consignmentEntries);
        consignmentEntries.add(consignmentEntryOne);
        Mockito.when(consignmentEntryOne.getConsignment()).thenReturn(consignment);
        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.SHIPPED);
        Mockito.when(consignment.getWarehouse()).thenReturn(warehouse);
        Mockito.when(Boolean.valueOf(warehouse.isDenyRefund())).thenReturn(Boolean.TRUE);
        Assert.assertFalse(strategy.perform(order, orderEntry, 1));
    }

    @Test
    public void testPerformWhenOrderEntryIsAssignedToNullWarehouse() {
        Mockito.when(orderEntry.getConsignmentEntries()).thenReturn(consignmentEntries);
        consignmentEntries.add(consignmentEntryOne);
        Mockito.when(consignmentEntryOne.getConsignment()).thenReturn(consignment);
        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.SHIPPED);
        Mockito.when(consignment.getWarehouse()).thenReturn(null);
        Mockito.when(Boolean.valueOf(warehouse.isDenyRefund())).thenReturn(Boolean.TRUE);
        Assert.assertFalse(strategy.perform(order, orderEntry, 1));
    }

    @Test
    public void testPerformWhenOrderEntryHasMultipleConsignmentEntries() {
        final ConsignmentModel consignmentOne = Mockito.mock(ConsignmentModel.class);
        final ConsignmentModel consignmentTwo = Mockito.mock(ConsignmentModel.class);
        final WarehouseModel warehouseOne = Mockito.mock(WarehouseModel.class);
        final WarehouseModel warehouseTwo = Mockito.mock(WarehouseModel.class);
        Mockito.when(orderEntry.getConsignmentEntries()).thenReturn(consignmentEntries);
        consignmentEntries.add(consignmentEntryOne);
        consignmentEntries.add(consignmentEntryTwo);
        Mockito.when(consignmentEntryOne.getConsignment()).thenReturn(consignmentOne);
        Mockito.when(consignmentEntryTwo.getConsignment()).thenReturn(consignmentTwo);
        Mockito.when(consignmentOne.getStatus()).thenReturn(ConsignmentStatus.CANCELLED);
        Mockito.when(consignmentOne.getWarehouse()).thenReturn(warehouseOne);
        Mockito.when(consignmentTwo.getStatus()).thenReturn(ConsignmentStatus.SHIPPED);
        Mockito.when(consignmentTwo.getWarehouse()).thenReturn(warehouseTwo);
        Mockito.when(Boolean.valueOf(warehouseOne.isDenyRefund())).thenReturn(Boolean.TRUE);
        Mockito.when(Boolean.valueOf(warehouseTwo.isDenyRefund())).thenReturn(Boolean.TRUE);
        Assert.assertFalse(strategy.perform(order, orderEntry, 1));
    }

    @Test
    public void testPerformWhenConsignmentEntriesIsEmptyAndWarehouseIsEmpty() {
        Mockito.when(orderEntry.getConsignmentEntries()).thenReturn(consignmentEntries);
        Assert.assertFalse(strategy.perform(order, orderEntry, 1));
    }

    @Test
    public void testPerformWhenConsignmentEntryDoesntContainConsignment() {
        Mockito.when(orderEntry.getConsignmentEntries()).thenReturn(consignmentEntries);
        consignmentEntries.add(consignmentEntryOne);
        Mockito.when(consignmentEntryOne.getConsignment()).thenReturn(null);
        Assert.assertFalse(strategy.perform(order, orderEntry, 1));
    }

}
