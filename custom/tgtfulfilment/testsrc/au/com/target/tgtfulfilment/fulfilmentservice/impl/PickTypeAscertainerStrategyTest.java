/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.model.BuyGetDealModel;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.model.SpendAndSaveDealModel;
import de.hybris.platform.promotions.model.ValueBundleDealModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.PickTypeEnum;
import au.com.target.tgtcore.model.TMDiscountPromotionModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.dto.PickConfirmEntry;
import au.com.target.tgtfulfilment.dto.PickConsignmentUpdateData;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.helper.TargetFulfilmentHelper;


/**
 * Unit test for {@link PickTypeAscertainerStrategyImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PickTypeAscertainerStrategyTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Mock
    private TargetFulfilmentHelper targetFulfilmentHelper;

    @InjectMocks
    private final PickTypeAscertainerStrategyImpl pickTypeAscertainer = new PickTypeAscertainerStrategyImpl();

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private OrderModel orderModel;

    @Mock
    private PickConsignmentUpdateData data;

    private List<PickConfirmEntry> pickConfirmEntries;
    private Set<ConsignmentEntryModel> consignmentEntries;

    private Set<PromotionResultModel> promotionResults;


    @Before
    public void setup() {

        pickConfirmEntries = new ArrayList<>();
        consignmentEntries = new HashSet<>();
        promotionResults = new HashSet<>();

        Mockito.when(consignment.getConsignmentEntries()).thenReturn(consignmentEntries);
        given(consignment.getOrder()).willReturn(orderModel);
        Mockito.when(orderModel.getAllPromotionResults()).thenReturn(promotionResults);
        given(orderModel.getCode()).willReturn("12345678");

        data = new PickConsignmentUpdateData();
        data.setPickEntries(pickConfirmEntries);
    }

    private void addPickEntry(final String itemCode, final int quantity) {
        final PickConfirmEntry pce = new PickConfirmEntry();
        pce.setItemCode(itemCode);
        pce.setQuantityShipped(Integer.valueOf(quantity));

        pickConfirmEntries.add(pce);
    }

    private void addConsignmentEntry(final OrderEntryModel orderEntry, final long quantity) {
        final ConsignmentEntryModel consEntry = Mockito.mock(ConsignmentEntryModel.class);

        final String itemCode = orderEntry.getProduct().getCode();

        Mockito.when(consEntry.getQuantity()).thenReturn(Long.valueOf(quantity));
        Mockito.when(consEntry.getOrderEntry()).thenReturn(orderEntry);
        Mockito.when(targetFulfilmentHelper.getConsignmentEntryItemCode(consEntry)).thenReturn(itemCode);

        consignmentEntries.add(consEntry);
    }

    private OrderEntryModel mockOrderEntry(final String itemCode, final long quantity,
            final Class clazz, final Float certainty) {

        final OrderEntryModel orderEntry = Mockito.mock(OrderEntryModel.class);
        final ProductModel product = Mockito.mock(ProductModel.class);
        Mockito.when(product.getCode()).thenReturn(itemCode);

        Mockito.when(orderEntry.getProduct()).thenReturn(product);
        Mockito.when(orderEntry.getQuantity()).thenReturn(Long.valueOf(quantity));
        Mockito.when(orderEntry.getOrder()).thenReturn(orderModel);

        if (clazz != null) {
            final PromotionResultModel promoResult = Mockito.mock(PromotionResultModel.class);
            final AbstractPromotionModel promotion = (AbstractPromotionModel)Mockito.mock(clazz);

            Mockito.when(promoResult.getPromotion()).thenReturn(promotion);
            Mockito.when(promoResult.getCertainty()).thenReturn(certainty);

            final PromotionOrderEntryConsumedModel promotionOrderEntryConsumed = Mockito
                    .mock(PromotionOrderEntryConsumedModel.class);
            Mockito.when(promotionOrderEntryConsumed.getOrderEntry()).thenReturn(orderEntry);
            Mockito.when(promoResult.getConsumedEntries()).thenReturn(
                    Collections.singletonList(promotionOrderEntryConsumed));

            promotionResults.add(promoResult);
        }
        return orderEntry;
    }

    @Test
    public void givenNoConsignmentEntries() throws RetryLaterException, Exception {

        expectedException.expect(FulfilmentException.class);

        addPickEntry("item1", 10);

        final PickTypeEnum result = pickTypeAscertainer.ascertainPickType(consignment, data);

        Assert.assertNull(result);
    }

    @Test
    public void givenNullConsignmentEntryItemCode() throws RetryLaterException, Exception {

        expectedException.expect(FulfilmentException.class);

        final OrderEntryModel orderEntry = mockOrderEntry("item1", 10, null, null);

        addConsignmentEntry(orderEntry, 10);
        addPickEntry("item1", 10);

        Mockito.when(targetFulfilmentHelper.getConsignmentEntryItemCode(Mockito.any(ConsignmentEntryModel.class)))
                .thenReturn(null);

        final PickTypeEnum result = pickTypeAscertainer.ascertainPickType(consignment, data);

        Assert.assertNull(result);
    }

    @Test
    public void givenFullPick() throws RetryLaterException, Exception {

        final OrderEntryModel orderEntry1 = mockOrderEntry("item1", 10, null, null);
        final OrderEntryModel orderEntry2 = mockOrderEntry("item2", 99, null, null);

        addConsignmentEntry(orderEntry1, 10);
        addConsignmentEntry(orderEntry2, 99);

        addPickEntry("item1", 10);
        addPickEntry("item2", 99);

        final PickTypeEnum result = pickTypeAscertainer.ascertainPickType(consignment, data);

        Assert.assertEquals(PickTypeEnum.FULL, result);
    }

    @Test
    public void givenShortPickShortItem() throws RetryLaterException, Exception {

        final OrderEntryModel orderEntry1 = mockOrderEntry("item1", 10, null, null);
        final OrderEntryModel orderEntry2 = mockOrderEntry("item2", 99, null, null);
        final OrderEntryModel orderEntry3 = mockOrderEntry("item3", 5, null, null);

        addConsignmentEntry(orderEntry1, 10);
        addConsignmentEntry(orderEntry2, 99);
        addConsignmentEntry(orderEntry3, 5);

        addPickEntry("item1", 10);
        addPickEntry("item2", 99);
        addPickEntry("item3", 1);

        final PickTypeEnum result = pickTypeAscertainer.ascertainPickType(consignment, data);

        Assert.assertEquals(PickTypeEnum.SHORT, result);
    }

    @Test
    public void givenShortPickMissingItem() throws RetryLaterException, Exception {

        final OrderEntryModel orderEntry1 = mockOrderEntry("item1", 10, null, null);
        final OrderEntryModel orderEntry2 = mockOrderEntry("item2", 99, null, null);
        final OrderEntryModel orderEntry3 = mockOrderEntry("item3", 5, null, null);

        addConsignmentEntry(orderEntry1, 10);
        addConsignmentEntry(orderEntry2, 99);
        addConsignmentEntry(orderEntry3, 5);

        addPickEntry("item1", 10);
        addPickEntry("item2", 99);

        final PickTypeEnum result = pickTypeAscertainer.ascertainPickType(consignment, data);

        Assert.assertEquals(PickTypeEnum.SHORT, result);
    }

    @Test
    public void givenZeroPickNoEntries() throws RetryLaterException, Exception {

        final OrderEntryModel orderEntry1 = mockOrderEntry("item1", 10, null, null);
        final OrderEntryModel orderEntry2 = mockOrderEntry("item2", 99, null, null);
        final OrderEntryModel orderEntry3 = mockOrderEntry("item3", 5, null, null);

        addConsignmentEntry(orderEntry1, 10);
        addConsignmentEntry(orderEntry2, 99);
        addConsignmentEntry(orderEntry3, 5);

        final PickTypeEnum result = pickTypeAscertainer.ascertainPickType(consignment, data);

        Assert.assertEquals(PickTypeEnum.ZERO, result);
    }

    @Test
    public void givenZeroPickNullEntries() throws RetryLaterException, Exception {

        final OrderEntryModel orderEntry1 = mockOrderEntry("item1", 10, null, null);
        final OrderEntryModel orderEntry2 = mockOrderEntry("item2", 99, null, null);
        final OrderEntryModel orderEntry3 = mockOrderEntry("item3", 5, null, null);

        addConsignmentEntry(orderEntry1, 10);
        addConsignmentEntry(orderEntry2, 99);
        addConsignmentEntry(orderEntry3, 5);

        data.setPickEntries(null);

        final PickTypeEnum result = pickTypeAscertainer.ascertainPickType(consignment, data);

        Assert.assertEquals(PickTypeEnum.ZERO, result);
    }

    @Test
    public void givenZeroPickZeroEntries() throws RetryLaterException, Exception {

        final OrderEntryModel orderEntry1 = mockOrderEntry("item1", 10, null, null);
        final OrderEntryModel orderEntry2 = mockOrderEntry("item2", 99, null, null);
        final OrderEntryModel orderEntry3 = mockOrderEntry("item3", 5, null, null);

        addConsignmentEntry(orderEntry1, 10);
        addConsignmentEntry(orderEntry2, 99);
        addConsignmentEntry(orderEntry3, 5);

        addPickEntry("item1", 0);
        addPickEntry("item2", 0);
        addPickEntry("item3", 0);

        final PickTypeEnum result = pickTypeAscertainer.ascertainPickType(consignment, data);

        Assert.assertEquals(PickTypeEnum.ZERO, result);
    }

    @Test
    public void givenZeroPickOnDealItem() throws RetryLaterException, Exception {

        final OrderEntryModel orderEntry1 = mockOrderEntry("item1", 10, ValueBundleDealModel.class, Float.valueOf(1));

        addConsignmentEntry(orderEntry1, 10);
        addPickEntry("item1", 0);

        final PickTypeEnum result = pickTypeAscertainer.ascertainPickType(consignment, data);

        Assert.assertEquals(PickTypeEnum.ZERO, result);
    }

    @Test
    public void givenZeroPickOnDealItemNullEntries() throws RetryLaterException, Exception {

        final OrderEntryModel orderEntry1 = mockOrderEntry("item1", 10, ValueBundleDealModel.class, Float.valueOf(1));
        final OrderEntryModel orderEntry2 = mockOrderEntry("item2", 99, BuyGetDealModel.class, Float.valueOf(1));
        final OrderEntryModel orderEntry3 = mockOrderEntry("item3", 5, SpendAndSaveDealModel.class, Float.valueOf(1));

        addConsignmentEntry(orderEntry1, 10);
        addConsignmentEntry(orderEntry2, 99);
        addConsignmentEntry(orderEntry3, 5);

        data.setPickEntries(null);

        final PickTypeEnum result = pickTypeAscertainer.ascertainPickType(consignment, data);

        Assert.assertEquals(PickTypeEnum.ZERO, result);
    }

    @Test
    public void givenShortPickOnDealItemOnly() throws RetryLaterException, Exception {

        final OrderEntryModel orderEntry1 = mockOrderEntry("item1", 10, ValueBundleDealModel.class, Float.valueOf(1));
        final OrderEntryModel orderEntry2 = mockOrderEntry("item2", 99, null, null);
        final OrderEntryModel orderEntry3 = mockOrderEntry("item3", 5, null, null);

        addConsignmentEntry(orderEntry1, 10);
        addConsignmentEntry(orderEntry2, 99);
        addConsignmentEntry(orderEntry3, 5);

        addPickEntry("item1", 9); // short pick on deal item
        addPickEntry("item2", 99);
        addPickEntry("item3", 5);

        final PickTypeEnum result = pickTypeAscertainer.ascertainPickType(consignment, data);

        Assert.assertEquals(PickTypeEnum.SHORT, result);
    }

    @Test
    public void givenShortPickOnDealItemOnlyWithoutPickEntry() throws RetryLaterException, Exception {

        final OrderEntryModel orderEntry1 = mockOrderEntry("item1", 10, ValueBundleDealModel.class, Float.valueOf(1));
        final OrderEntryModel orderEntry2 = mockOrderEntry("item2", 99, null, null);
        final OrderEntryModel orderEntry3 = mockOrderEntry("item3", 5, null, null);

        addConsignmentEntry(orderEntry1, 10);
        addConsignmentEntry(orderEntry2, 99);
        addConsignmentEntry(orderEntry3, 5);

        addPickEntry("item2", 99);
        addPickEntry("item3", 5);

        final PickTypeEnum result = pickTypeAscertainer.ascertainPickType(consignment, data);

        Assert.assertEquals(PickTypeEnum.SHORT, result);
    }

    @Test
    public void givenShortPickOnNonDealItems() throws RetryLaterException, Exception {

        final OrderEntryModel orderEntry1 = mockOrderEntry("item1", 10, ValueBundleDealModel.class, Float.valueOf(1));
        final OrderEntryModel orderEntry2 = mockOrderEntry("item2", 99, null, null);
        final OrderEntryModel orderEntry3 = mockOrderEntry("item3", 5, null, null);

        addConsignmentEntry(orderEntry1, 10);
        addConsignmentEntry(orderEntry2, 99);
        addConsignmentEntry(orderEntry3, 5);

        addPickEntry("item1", 10);
        addPickEntry("item2", 95); // short pick on non-deal item
        addPickEntry("item3", 5);

        final PickTypeEnum result = pickTypeAscertainer.ascertainPickType(consignment, data);

        Assert.assertEquals(PickTypeEnum.SHORT, result);
    }

    @Test
    public void givenShortPickOnDealAndNonDealItems() throws RetryLaterException, Exception {

        final OrderEntryModel orderEntry1 = mockOrderEntry("item1", 10, ValueBundleDealModel.class, Float.valueOf(1));
        final OrderEntryModel orderEntry2 = mockOrderEntry("item2", 99, null, null);
        final OrderEntryModel orderEntry3 = mockOrderEntry("item3", 5, null, null);

        addConsignmentEntry(orderEntry1, 10);
        addConsignmentEntry(orderEntry2, 99);
        addConsignmentEntry(orderEntry3, 5);

        addPickEntry("item1", 9); // short pick on deal item
        addPickEntry("item2", 95); // short pick on non-deal item
        addPickEntry("item3", 5);

        final PickTypeEnum result = pickTypeAscertainer.ascertainPickType(consignment, data);

        Assert.assertEquals(PickTypeEnum.SHORT, result);
    }

    @Test
    public void givenShortPickOnTMDItem() throws RetryLaterException, Exception {

        final OrderEntryModel orderEntry1 = mockOrderEntry("item1", 10, ValueBundleDealModel.class, Float.valueOf(1));
        final OrderEntryModel orderEntry2 = mockOrderEntry("item2", 99, TMDiscountPromotionModel.class,
                Float.valueOf(1));
        final OrderEntryModel orderEntry3 = mockOrderEntry("item3", 5, null, null);

        addConsignmentEntry(orderEntry1, 10);
        addConsignmentEntry(orderEntry2, 99);
        addConsignmentEntry(orderEntry3, 5);

        addPickEntry("item1", 10);
        addPickEntry("item2", 95); // short pick on TMD item
        addPickEntry("item3", 5);

        final PickTypeEnum result = pickTypeAscertainer.ascertainPickType(consignment, data);

        Assert.assertEquals(PickTypeEnum.SHORT, result);
    }

    @Test
    public void givenShortPickOnTMDAndDealItem() throws RetryLaterException, Exception {

        final OrderEntryModel orderEntry1 = mockOrderEntry("item1", 10, ValueBundleDealModel.class, Float.valueOf(1));
        final OrderEntryModel orderEntry2 = mockOrderEntry("item2", 99, TMDiscountPromotionModel.class,
                Float.valueOf(1));
        final OrderEntryModel orderEntry3 = mockOrderEntry("item3", 5, null, null);

        addConsignmentEntry(orderEntry1, 10);
        addConsignmentEntry(orderEntry2, 99);
        addConsignmentEntry(orderEntry3, 5);

        addPickEntry("item1", 7); // short pick on deal item
        addPickEntry("item2", 95); // short pick on TMD item
        addPickEntry("item3", 5);

        final PickTypeEnum result = pickTypeAscertainer.ascertainPickType(consignment, data);

        Assert.assertEquals(PickTypeEnum.SHORT, result);
    }

    @Test
    public void givenShortPickOnTMDAndPotentialDealItem() throws RetryLaterException, Exception {

        final OrderEntryModel orderEntry1 = mockOrderEntry("item1", 10, ValueBundleDealModel.class,
                Float.valueOf(0.89f));
        final OrderEntryModel orderEntry2 = mockOrderEntry("item2", 99, TMDiscountPromotionModel.class,
                Float.valueOf(1));
        final OrderEntryModel orderEntry3 = mockOrderEntry("item3", 5, null, null);

        addConsignmentEntry(orderEntry1, 10);
        addConsignmentEntry(orderEntry2, 99);
        addConsignmentEntry(orderEntry3, 5);

        addPickEntry("item1", 7); // short pick on potential deal item
        addPickEntry("item2", 95); // short pick on TMD item
        addPickEntry("item3", 5);

        final PickTypeEnum result = pickTypeAscertainer.ascertainPickType(consignment, data);

        Assert.assertEquals(PickTypeEnum.SHORT, result);
    }

    @Test
    public void givenShortPickOnFiredAndPotentialDealItems() throws RetryLaterException, Exception {

        final OrderEntryModel orderEntry1 = mockOrderEntry("item1", 10, ValueBundleDealModel.class,
                Float.valueOf(0.89f));
        final OrderEntryModel orderEntry2 = mockOrderEntry("item2", 99, SpendAndSaveDealModel.class,
                Float.valueOf(1));
        final OrderEntryModel orderEntry3 = mockOrderEntry("item3", 5, null, null);

        addConsignmentEntry(orderEntry1, 10);
        addConsignmentEntry(orderEntry2, 99);
        addConsignmentEntry(orderEntry3, 5);

        addPickEntry("item1", 7); // short pick on potential deal item
        addPickEntry("item2", 95); // short pick on fired deal item
        addPickEntry("item3", 5);

        final PickTypeEnum result = pickTypeAscertainer.ascertainPickType(consignment, data);

        Assert.assertEquals(PickTypeEnum.SHORT, result);
    }
}
