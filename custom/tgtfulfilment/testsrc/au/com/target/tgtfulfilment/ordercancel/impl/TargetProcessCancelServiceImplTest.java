/**
 * 
 */
package au.com.target.tgtfulfilment.ordercancel.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRecordsHandler;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.transaction.support.TransactionTemplate;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.CancelTypeEnum;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.flybuys.service.FlybuysDiscountService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.order.impl.TargetFindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtcore.ordercancel.TargetCancelService;
import au.com.target.tgtcore.ordercancel.TargetRefundPaymentService;
import au.com.target.tgtcore.ordercancel.data.CancelRequestResponse;
import au.com.target.tgtcore.ordercancel.data.PaymentTransactionEntryDTO;
import au.com.target.tgtcore.ordercancel.data.RefundInfoDTO;
import au.com.target.tgtcore.ordercancel.discountstrategy.VoucherCorrectionStrategy;
import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;
import au.com.target.tgtfulfilment.dto.PickConsignmentUpdateData;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntry;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntryList;
import au.com.target.tgtfulfilment.salesapplication.TargetFulfilmentSalesApplicationConfigService;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProcessCancelServiceImplTest {

    @Mock
    protected TargetFulfilmentSalesApplicationConfigService salesApplicationConfigService;

    @Mock
    protected TargetRefundPaymentService targetRefundPaymentService;

    @Mock
    protected TargetTicketBusinessService targetTicketBusinessService;

    @InjectMocks
    private final TargetProcessCancelServiceImpl service = new TargetProcessCancelServiceImpl();

    @Mock
    private TransactionTemplate transactionTemplate;

    @Mock
    private FlybuysDiscountService flybuysDiscountService;

    @Mock
    private VoucherCorrectionStrategy voucherCorrectionStrategy;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetFindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    @Mock
    private TargetCancelService targetCancelService;

    @Mock
    private OrderCancelRecordsHandler orderCancelRecordsHandler;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private OrderModel order;

    @Mock
    private AbstractOrderEntryModel orderEntry;

    @Mock
    private ConsignmentModel consignment;

    @Mock
    private TargetConsignmentModel targetConsignment;

    @Mock
    private ConsignmentEntryModel consignmentEntry;

    @Mock
    private PickConsignmentUpdateData data;

    @Mock
    private CancelRequestResponse response;

    @Mock
    private PaymentTransactionEntryDTO paymentEntry;

    @Mock
    private PaymentTransactionEntryModel entryModel;

    @Mock
    private AbstractOrderEntryModel orderEntry1;

    @Mock
    private AbstractOrderEntryModel orderEntry2;

    @Mock
    private ProductModel product1;

    @Mock
    private ProductModel product2;

    private final TargetOrderCancelEntryList targetOrderCancelEntryList = new TargetOrderCancelEntryList();

    @Before
    public void setup() {
        given(order.getEntries()).willReturn(Arrays.asList(orderEntry1, orderEntry2));
        given(order.getCode()).willReturn("testcode");
        given(orderEntry1.getQuantity()).willReturn(Long.valueOf(2l));
        given(orderEntry1.getProduct()).willReturn(product1);
        given(orderEntry1.getOrder()).willReturn(order);
        given(orderEntry1.getEntryNumber()).willReturn(Integer.valueOf(1));
        given(orderEntry2.getQuantity()).willReturn(Long.valueOf(2l));
        given(orderEntry2.getProduct()).willReturn(product2);
        given(orderEntry2.getOrder()).willReturn(order);
        given(orderEntry2.getEntryNumber()).willReturn(Integer.valueOf(2));
    }

    @Test
    public void testProcessCancelWithShortPick() throws FulfilmentException, OrderCancelException {
        final TargetOrderCancelEntry cancelEntry1 = new TargetOrderCancelEntry();
        cancelEntry1.setOrderEntry(orderEntry1);
        cancelEntry1.setCancelQuantity(Long.valueOf(1l));
        final TargetOrderCancelEntry cancelEntry2 = new TargetOrderCancelEntry();
        cancelEntry2.setOrderEntry(orderEntry2);
        cancelEntry2.setCancelQuantity(Long.valueOf(2l));
        targetOrderCancelEntryList.setEntriesToCancel(Arrays.asList(cancelEntry1, cancelEntry2));
        final RefundInfoDTO refundedInfo = new RefundInfoDTO();
        given(targetRefundPaymentService.createRefundedInfo(anyList(), any(BigDecimal.class))).willReturn(refundedInfo);
        final CancelRequestResponse result = service.processCancel(order, targetOrderCancelEntryList);
        assertThat(result.getCancelType()).isEqualTo(CancelTypeEnum.SHORT_PICK);
    }

    @Test
    public void testProcessCancelWithZeroPick() throws FulfilmentException, OrderCancelException {
        final TargetOrderCancelEntry cancelEntry1 = new TargetOrderCancelEntry();
        cancelEntry1.setOrderEntry(orderEntry1);
        cancelEntry1.setCancelQuantity(Long.valueOf(2l));
        final TargetOrderCancelEntry cancelEntry2 = new TargetOrderCancelEntry();
        cancelEntry2.setOrderEntry(orderEntry2);
        cancelEntry2.setCancelQuantity(Long.valueOf(2l));
        targetOrderCancelEntryList.setEntriesToCancel(Arrays.asList(cancelEntry1, cancelEntry2));
        final RefundInfoDTO refundedInfo = new RefundInfoDTO();
        given(targetRefundPaymentService.createRefundedInfo(anyList(), any(BigDecimal.class))).willReturn(refundedInfo);
        final CancelRequestResponse result = service.processCancel(order, targetOrderCancelEntryList);
        assertThat(result.getCancelType()).isEqualTo(CancelTypeEnum.ZERO_PICK);
    }

}
