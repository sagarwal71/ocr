/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.dao.TargetPointOfServiceDao;
import au.com.target.tgtfulfilment.dao.TargetFulfilmentPointOfServiceDao;


/**
 * @author Nandini
 *
 */
@UnitTest
public class TargetFulfilmentPointOfServiceServiceImplTest {

    @Mock
    private TargetFulfilmentPointOfServiceDao fulfilmentDao;

    @Mock
    private AddressModel addressModel;

    @Mock
    private RegionModel region;

    @Mock
    private TargetPointOfServiceModel targetPointOfServiceModel;

    @Mock
    private TargetPointOfServiceDao pos;

    @InjectMocks
    private final TargetFulfilmentPointOfServiceServiceImpl fulfilmentPOSSevice = new TargetFulfilmentPointOfServiceServiceImpl();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        fulfilmentPOSSevice.setTargetPointOfServiceDao(pos);
        fulfilmentPOSSevice.setTargetFulfilmentPointOfServiceDao(fulfilmentDao);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetNTLByAddressNullAddress() {
        fulfilmentPOSSevice.getNTLByAddress(null);
    }

    @Test
    public void testGetNTLByAddressWithAddress() {
        given(addressModel.getRegion()).willReturn(region);
        given(fulfilmentDao.getNTLByRegion(region)).willReturn(targetPointOfServiceModel);
        final TargetPointOfServiceModel returnModel = fulfilmentPOSSevice.getNTLByAddress(addressModel);

        Assert.assertNotNull(returnModel);
        Assert.assertEquals(targetPointOfServiceModel, returnModel);
    }

    @Test
    public void testGetNTLByAddressWithNullDistrict() {
        given(addressModel.getRegion()).willReturn(null);
        given(addressModel.getDistrict()).willReturn(null);
        given(pos.getRegionByAbbreviation(Mockito.anyString())).willReturn(region);
        given(fulfilmentDao.getNTLByRegion(region)).willReturn(targetPointOfServiceModel);
        final TargetPointOfServiceModel returnModel = fulfilmentPOSSevice.getNTLByAddress(addressModel);

        Assert.assertNull(returnModel);
        Mockito.verifyZeroInteractions(fulfilmentDao);
    }

    @Test
    public void testGetNTLByAddressWithNullRegion() {
        given(addressModel.getRegion()).willReturn(null);
        final TargetPointOfServiceModel returnModel = fulfilmentPOSSevice.getNTLByAddress(addressModel);

        Assert.assertNull(returnModel);
    }

    @Test
    public void testGetNTLByAddressWithDistrict() {
        given(addressModel.getRegion()).willReturn(null);
        given(addressModel.getDistrict()).willReturn("NSW");
        given(pos.getRegionByAbbreviation("NSW")).willReturn(region);
        given(fulfilmentDao.getNTLByRegion(region)).willReturn(targetPointOfServiceModel);
        final TargetPointOfServiceModel returnModel = fulfilmentPOSSevice.getNTLByAddress(addressModel);

        Assert.assertNotNull(returnModel);
        Assert.assertEquals(returnModel, targetPointOfServiceModel);
    }

}
