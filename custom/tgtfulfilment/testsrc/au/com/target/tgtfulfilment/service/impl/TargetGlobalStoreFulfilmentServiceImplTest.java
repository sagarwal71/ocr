/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.dao.TargetGlobalStoreFulfilmentCapabilitiesDao;
import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;


/**
 * To test TargetStoreFulfilmentCapabilitiesServiceImpl service to check the GlobalStoreFulfilmentCapabilities model
 * 
 * @author ajit
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetGlobalStoreFulfilmentServiceImplTest {

    @InjectMocks
    private final TargetGlobalStoreFulfilmentServiceImpl targetGlobalStoreFulfilmentService = new TargetGlobalStoreFulfilmentServiceImpl();

    @Mock
    private TargetGlobalStoreFulfilmentCapabilitiesDao targetGlobalStoreFulfilmentCapabilitiesDao;

    private final GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel = new GlobalStoreFulfilmentCapabilitiesModel();

    /**
     * Method to test getGlobalStoreFulfilmentCapabilities method when a model exists
     */
    @Test
    public void testGetGlobalStoreFulfilmentModelWhenItExists() {
        Mockito.when(targetGlobalStoreFulfilmentCapabilitiesDao.getGlobalStoreFulfilmentCapabilities()).thenReturn(
                capabilitiesModel);
        Assert.assertEquals(capabilitiesModel, targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites());
    }

    /**
     * Method to test getGlobalStoreFulfilmentCapabilities method when model doesn't exists
     */
    @Test
    public void testGetGlobalStoreFulfilmentModelWhenItDoesntExists() {
        Mockito.when(targetGlobalStoreFulfilmentCapabilitiesDao.getGlobalStoreFulfilmentCapabilities()).thenReturn(
                null);
        Assert.assertEquals(null, targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites());
    }

}
