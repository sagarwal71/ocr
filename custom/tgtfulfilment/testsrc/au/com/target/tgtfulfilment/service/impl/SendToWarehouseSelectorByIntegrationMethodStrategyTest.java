/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.enums.IntegrationMethod;
import au.com.target.tgtfulfilment.service.SendToWarehouseProtocol;


/**
 * @author sbryan6
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendToWarehouseSelectorByIntegrationMethodStrategyTest {

    private final SendToWarehouseSelectorByIntegrationMethodStrategy strategy = new SendToWarehouseSelectorByIntegrationMethodStrategy();

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private WarehouseModel warehouse;

    @Mock
    private SendToWarehouseProtocol serviceEsb;

    @Mock
    private SendToWarehouseProtocol serviceWM;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;


    @Before
    public void setup() {

        strategy.setDefaultIntegrationMethod("ESB");
        strategy.setTargetFeatureSwitchService(targetFeatureSwitchService);
        final Map<String, SendToWarehouseProtocol> fulfilmentProcessServices = new HashMap<String, SendToWarehouseProtocol>();
        fulfilmentProcessServices.put("ESB", serviceEsb);
        fulfilmentProcessServices.put("WEBMETHODS", serviceWM);
        strategy.setSendToWarehouseProtocols(fulfilmentProcessServices);

        when(consignment.getWarehouse()).thenReturn(warehouse);
        when(warehouse.getIntegrationMethod()).thenReturn(IntegrationMethod.WEBMETHODS);
    }

    @Test
    public void testGetIntegrationMethodNullWarehouse() {

        Mockito.when(consignment.getWarehouse()).thenReturn(null);

        final IntegrationMethod method = strategy.getIntegrationMethod(consignment);

        assertThat(method).as("integration method").isNotNull().isEqualTo(IntegrationMethod.ESB);
    }

    @Test
    public void testGetIntegrationMethodNullIntegrationMethod() {

        when(warehouse.getIntegrationMethod()).thenReturn(null);

        final IntegrationMethod method = strategy.getIntegrationMethod(consignment);

        assertThat(method).as("integration method").isNotNull().isEqualTo(IntegrationMethod.ESB);
    }

    @Test
    public void testGetIntegrationMethod() {

        final IntegrationMethod method = strategy.getIntegrationMethod(consignment);

        assertThat(method).as("integration method").isNotNull().isEqualTo(IntegrationMethod.WEBMETHODS);
    }

    @Test
    public void testGetFulfilmentProcessForConsignment() {

        final SendToWarehouseProtocol service = strategy.getSendToWarehouseProtocolForConsignment(consignment);
        assertThat(service).as("fulfilment process service").isNotNull().isEqualTo(serviceWM);
    }

    @Test
    public void testGetIntegrationMethodWithForceDefaultWarehouseToOnlinePOSForDefaultWarehouse() {

        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FORCE_DEFAULT_WAREHOUSE_TO_ONLINE_POS);

        when(warehouse.getDefault()).thenReturn(Boolean.TRUE);

        final IntegrationMethod method = strategy.getIntegrationMethod(consignment);

        assertThat(method).as("integration method").isNotNull().isEqualTo(IntegrationMethod.NONE);
    }

    @Test
    public void testGetIntegrationMethodWithForceDefaultWarehouseToOnlinePOSForNonDefaultWarehouse() {

        when(
                Boolean.valueOf(targetFeatureSwitchService
                        .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FORCE_DEFAULT_WAREHOUSE_TO_ONLINE_POS)))
                .thenReturn(Boolean.TRUE);

        when(warehouse.getDefault()).thenReturn(Boolean.FALSE);

        final IntegrationMethod method = strategy.getIntegrationMethod(consignment);

        assertThat(method).as("integration method").isNotNull().isEqualTo(IntegrationMethod.WEBMETHODS);
    }

}
