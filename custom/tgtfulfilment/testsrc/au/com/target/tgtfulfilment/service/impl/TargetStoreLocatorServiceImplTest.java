/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Collections;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.dao.TargetPointOfServiceDao;
import au.com.target.tgtfulfilment.service.TargetStoreLocatorService;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetStoreLocatorServiceImplTest {

    @Mock
    private TargetPointOfServiceDao targetPointOfServiceDao;

    @InjectMocks
    private final TargetStoreLocatorService targetStoreLocatorService = new TargetStoreLocatorServiceImpl();


    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Test
    public void testGetAllFulfilmentStoresInStateNullState() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("A value needs to be provided for the state");
        targetStoreLocatorService.getAllFulfilmentStoresInState(null);
        Mockito.verifyZeroInteractions(targetPointOfServiceDao);
    }

    @Test
    public void testGetAllFulfilmentStoresInStateEmptyState() {
        Mockito.when(targetPointOfServiceDao.getAllFulfilmentEnabledPOSInState(Mockito.anyString())).thenReturn(
                Collections.EMPTY_LIST);
        final List<TargetPointOfServiceModel> pos = targetStoreLocatorService.getAllFulfilmentStoresInState("");
        Assertions.assertThat(pos).isNotNull().isEmpty();
        Mockito.verify(targetPointOfServiceDao).getAllFulfilmentEnabledPOSInState(Mockito.anyString());
        Mockito.verifyNoMoreInteractions(targetPointOfServiceDao);
    }

    @Test
    public void testGetAllFulfilmentStoresInState() {
        Mockito.when(targetPointOfServiceDao.getAllFulfilmentEnabledPOSInState(Mockito.anyString())).thenReturn(
                Collections.singletonList(new TargetPointOfServiceModel()));
        final List<TargetPointOfServiceModel> pos = targetStoreLocatorService.getAllFulfilmentStoresInState("VIC");
        Assertions.assertThat(pos).isNotNull().isNotEmpty().hasSize(1);
        Mockito.verify(targetPointOfServiceDao).getAllFulfilmentEnabledPOSInState(Mockito.anyString());
        Mockito.verifyNoMoreInteractions(targetPointOfServiceDao);
    }
}
