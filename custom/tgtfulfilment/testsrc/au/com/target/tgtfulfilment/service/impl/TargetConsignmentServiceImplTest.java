/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import static au.com.target.tgtcore.constants.TgtCoreConstants.FASTLINE_WAREHOUSE;
import static au.com.target.tgtfulfilment.enums.OfcOrderType.CUSTOMER_DELIVERY;
import static au.com.target.tgtfulfilment.enums.OfcOrderType.INSTORE_PICKUP;
import static au.com.target.tgtfulfilment.enums.OfcOrderType.INTERSTORE_DELIVERY;
import static au.com.target.tgtfulfilment.enums.StorePortal.OFC;
import static au.com.target.tgtfulfilment.enums.StorePortal.SERVICE_POINT;
import static de.hybris.platform.basecommerce.enums.ConsignmentStatus.CANCELLED;
import static de.hybris.platform.basecommerce.enums.ConsignmentStatus.CONFIRMED_BY_WAREHOUSE;
import static de.hybris.platform.basecommerce.enums.ConsignmentStatus.CREATED;
import static de.hybris.platform.basecommerce.enums.ConsignmentStatus.PACKED;
import static de.hybris.platform.basecommerce.enums.ConsignmentStatus.PICKED;
import static de.hybris.platform.basecommerce.enums.ConsignmentStatus.SENT_TO_WAREHOUSE;
import static de.hybris.platform.basecommerce.enums.ConsignmentStatus.WAVED;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Collections.EMPTY_LIST;
import static java.util.Collections.EMPTY_SET;
import static java.util.Collections.addAll;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.ConsignmentCreationException;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.collections.SetUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Sets;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.dao.TargetConsignmentDao;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.service.TargetCarrierSelectionService;
import au.com.target.tgtfulfilment.warehouseservices.TargetFulfilmentWarehouseService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetConsignmentServiceImplTest {

    @InjectMocks
    @Spy
    private final TargetConsignmentServiceImpl targetConsignmentServiceImpl = new TargetConsignmentServiceImpl();

    @Mock
    private ModelService modelService;

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private AbstractOrderModel order;

    @Mock
    private AbstractOrderEntryModel orderEntry;

    @Mock
    private ConsignmentEntryModel consignmentEntry;

    @Mock
    private TargetCarrierSelectionService targetCarrierSelectionService;

    @Mock
    private TargetOrderService targetOrderService;

    @Mock
    private DeliveryModeModel orderEntryDelMode;

    @Mock
    private DeliveryModeModel orderDelMode;

    @Mock
    private AddressModel orderEntryAddress;

    @Mock
    private AddressModel orderAddress;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    private OrderEntryGroup orderEntries;

    @Mock
    private TargetConsignmentDao targetConsignmentDao;

    @Mock
    private TargetConsignmentModel mockedConsignment;

    @Mock
    private WarehouseModel warehouse;

    @Mock
    private TargetPointOfServiceModel posModel;

    @Mock
    private TargetFulfilmentWarehouseService targetFulfilmentWarehouseService;

    private final OEGParameterHelper oegParameterHelper = new OEGParameterHelper();

    private final String productCode = "testCode1";

    @Before
    public void setup() {
        targetConsignmentServiceImpl.setOegParameterHelper(oegParameterHelper);
        orderEntries = new OrderEntryGroup();
        orderEntries.add(orderEntry);
        given(modelService.create(ConsignmentEntryModel.class)).willReturn(consignmentEntry);

        final AbstractTargetVariantProductModel product = mock(AbstractTargetVariantProductModel.class);
        final ProductTypeModel type = mock(ProductTypeModel.class);
        given(orderEntry.getProduct()).willReturn(product);
        given(product.getProductType()).willReturn(type);
        given(product.getCode()).willReturn(productCode);

        given(type.getBulky()).willReturn(Boolean.valueOf(false));
        willReturn(orderDelMode).given(order).getDeliveryMode();
        willReturn(orderEntryDelMode).given(orderEntry).getDeliveryMode();
        willReturn(orderAddress).given(order).getDeliveryAddress();
        willReturn(orderEntryAddress).given(orderEntry).getDeliveryAddress();

        final Map<String, Long> assignedQtyMap = new HashMap<>();
        assignedQtyMap.put(productCode, Long.valueOf(3));
        orderEntries.setParameter("ASSIGNED_QTY", assignedQtyMap);
        willReturn(TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
    }

    @Test
    public void testCreateConsignmentWithOrderNull() throws ConsignmentCreationException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Order cannot be null");
        targetConsignmentServiceImpl
                .createConsignment(null, "test", orderEntries);
        verifyZeroInteractions(order);
    }

    @Test
    public void testCreateConsignmentWithNullOrderEntriesList() throws ConsignmentCreationException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("OrderEntries cannot be null or Empty!");
        targetConsignmentServiceImpl
                .createConsignment(order, "test", null);
        verifyZeroInteractions(order);
    }

    @Test
    public void testCreateConsignmentWithEmptyOrderEntries() throws ConsignmentCreationException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("OrderEntries cannot be null or Empty!");
        targetConsignmentServiceImpl
                .createConsignment(order, "test", new ArrayList<AbstractOrderEntryModel>());
        verifyZeroInteractions(order);
    }

    @Test
    public void testCreateConsignmentForNoDelModeOnOrderEntries() throws ConsignmentCreationException {
        setupExpectedCallsForUniqueCodeTest();

        given(orderEntry.getQuantity()).willReturn(Long.valueOf(3));
        willReturn(null).given(orderEntry).getDeliveryMode();
        final ConsignmentModel consignmentModel = targetConsignmentServiceImpl.createConsignment(order, "test",
                orderEntries);
        assertThat(consignmentModel.getCode()).isEqualTo("test");
        assertThat(consignmentModel instanceof TargetConsignmentModel).isTrue();
        assertThat(consignmentModel.getDeliveryMode()).isEqualTo(orderDelMode);
        assertThat(consignmentModel.getShippingAddress()).isEqualTo(orderAddress);
        assertThat(consignmentModel.getConsignmentEntries()).hasSize(1);
    }

    @Test
    public void testCreateConsignmentForDelModeOnOrderEntries() throws ConsignmentCreationException {
        setupExpectedCallsForUniqueCodeTest();

        given(orderEntry.getQuantity()).willReturn(Long.valueOf(3));
        final ConsignmentModel consignmentModel = targetConsignmentServiceImpl.createConsignment(order, "test",
                orderEntries);
        assertThat(consignmentModel.getCode()).isEqualTo("test");
        assertThat(consignmentModel instanceof TargetConsignmentModel).isTrue();
        assertThat(consignmentModel.getDeliveryMode()).isEqualTo(orderEntryDelMode);
        assertThat(consignmentModel.getShippingAddress()).isEqualTo(orderEntryAddress);
        assertThat(consignmentModel.getConsignmentEntries()).hasSize(1);
    }

    @Test
    public void testCreateConsignment() throws ConsignmentCreationException {
        setupExpectedCallsForUniqueCodeTest();

        given(orderEntry.getQuantity()).willReturn(Long.valueOf(3));
        final ConsignmentModel consignmentModel = targetConsignmentServiceImpl.createConsignment(order, "test",
                orderEntries);
        assertThat(consignmentModel.getCode()).isEqualTo("test");
        assertThat(consignmentModel instanceof TargetConsignmentModel).isTrue();
        assertThat(consignmentModel.getConsignmentEntries()).hasSize(1);
    }

    @Test
    public void testCreateConsignmentOrderEntryZeroQuantity() throws ConsignmentCreationException {
        setupExpectedCallsForUniqueCodeTest();

        final Map<String, Long> assignedQtyMap = new HashMap<>();
        assignedQtyMap.put("other", Long.valueOf(3));
        orderEntries.setParameter("ASSIGNED_QTY", assignedQtyMap);

        final ConsignmentModel consignmentModel = targetConsignmentServiceImpl.createConsignment(order, "test",
                orderEntries);

        assertThat(consignmentModel.getCode()).isEqualTo("test");
        assertThat(consignmentModel instanceof TargetConsignmentModel).isTrue();

        // Expect entry not to be added unless positive quantity
        assertThat(consignmentModel.getConsignmentEntries()).isEmpty();
    }

    @Test(expected = ConsignmentCreationException.class)
    public void testCreateConsignmentNoWareHouseException() throws ConsignmentCreationException {
        final TargetConsignmentModel targetConsignmentModel = new TargetConsignmentModel();
        given(modelService.create(TargetConsignmentModel.class)).willReturn(targetConsignmentModel);

        targetConsignmentServiceImpl.createConsignment(order, "test", orderEntries);
    }

    @Test
    public void testCreateConsignmentTestUniqueCodeNullConsignments() throws ConsignmentCreationException {
        setupExpectedCallsForUniqueCodeTest();
        given(order.getConsignments()).willReturn(null);

        final ConsignmentModel consignmentModel = targetConsignmentServiceImpl.createConsignment(order, "test",
                orderEntries);

        assertThat(consignmentModel.getCode()).isEqualTo("test");
        assertThat(consignmentModel instanceof TargetConsignmentModel);
    }

    @Test
    public void testCreateConsignmentTestUniqueCodeEmptyConsignments() throws ConsignmentCreationException {
        setupExpectedCallsForUniqueCodeTest();
        given(order.getConsignments()).willReturn(SetUtils.EMPTY_SET);

        final ConsignmentModel consignmentModel = targetConsignmentServiceImpl.createConsignment(order, "test",
                orderEntries);
        assertThat(consignmentModel.getCode()).isEqualTo("test");
        assertThat(consignmentModel instanceof TargetConsignmentModel).isTrue();
    }

    @Test
    public void testCreateConsignmentTestUniqueCodeButHasOneConsignment() throws ConsignmentCreationException {
        final String orderCode = "1234";
        setupExpectedCallsForUniqueCodeTest();
        given(order.getCode()).willReturn(orderCode);
        final String consignmentCode = "a" + orderCode;
        given(order.getConsignments()).willReturn(setupConsignments(consignmentCode));

        final ConsignmentModel consignmentModel = targetConsignmentServiceImpl.createConsignment(order,
                consignmentCode,
                orderEntries);
        assertThat(consignmentModel.getCode()).isEqualTo("b" + orderCode);
        assertThat(consignmentModel instanceof TargetConsignmentModel).isTrue();
    }

    @Test
    public void testCreateConsignmentTestUniqueCodeButHasThreeConsignment() throws ConsignmentCreationException {
        final String orderCode = "1234";
        setupExpectedCallsForUniqueCodeTest();
        given(order.getCode()).willReturn(orderCode);
        final String consignmentCode = "a" + orderCode;
        given(order.getConsignments()).willReturn(
                setupConsignments(consignmentCode, "b" + orderCode, "c" + orderCode));

        final ConsignmentModel consignmentModel = targetConsignmentServiceImpl.createConsignment(order,
                consignmentCode,
                orderEntries);
        assertThat(consignmentModel.getCode()).isEqualTo("d" + orderCode);
        assertThat(consignmentModel instanceof TargetConsignmentModel).isTrue();
    }

    @Test
    public void testFindConsignmentFromOrderWithStatusWithOrderNull() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Order cannot be null");
        targetConsignmentServiceImpl.findConsignmentFromOrderWithStatus(null, ListUtils.EMPTY_LIST);
        verifyZeroInteractions(order);
    }

    @Test
    public void testFindConsignmentFromOrderWithStatusWithStatusEmpty() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Consignment status needs to be set");
        targetConsignmentServiceImpl.findConsignmentFromOrderWithStatus(order, ListUtils.EMPTY_LIST);
        verifyZeroInteractions(order);
    }

    @Test
    public void testFindConsignmentFromOrderWithStatusWithStatusNull() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Consignment status needs to be set");
        targetConsignmentServiceImpl.findConsignmentFromOrderWithStatus(order, null);
        verifyZeroInteractions(order);
    }

    @Test
    public void testFindConsignmentFromOrderWithStatusWithOneConsignmentWrongStatus() {
        given(order.getConsignments()).willReturn(setupConsignmentsWithStatus("1234", ConsignmentStatus.PICKED));
        final ConsignmentModel consignment = targetConsignmentServiceImpl.findConsignmentFromOrderWithStatus(order,
                singletonList(CONFIRMED_BY_WAREHOUSE));
        assertThat(consignment).isNull();
        verify(order).getConsignments();
        verifyNoMoreInteractions(order);
    }

    @Test
    public void testFindConsignmentFromOrderWithStatusWithOneConsignmentRightStatus() {
        given(order.getConsignments()).willReturn(
                setupConsignmentsWithStatus("1234", CONFIRMED_BY_WAREHOUSE));
        final ConsignmentModel consignment = targetConsignmentServiceImpl.findConsignmentFromOrderWithStatus(order,
                singletonList(CONFIRMED_BY_WAREHOUSE));
        assertThat(consignment).isNotNull();
        assertThat(consignment.getStatus()).isNotNull().isSameAs(CONFIRMED_BY_WAREHOUSE);
        verify(order).getConsignments();
        verifyNoMoreInteractions(order);
    }

    @Test
    public void testFindConsignmentFromOrderWithStatusWithOneConsignmentInOrderTwoStatusPassedIn() {
        given(order.getConsignments()).willReturn(
                setupConsignmentsWithStatus("1234", CONFIRMED_BY_WAREHOUSE));
        final List<ConsignmentStatus> statuses = new ArrayList<>();
        addAll(statuses, ConsignmentStatus.CREATED, CONFIRMED_BY_WAREHOUSE);
        final ConsignmentModel consignment = targetConsignmentServiceImpl.findConsignmentFromOrderWithStatus(order,
                statuses);
        assertThat(consignment).isNotNull();
        assertThat(consignment.getStatus()).isNotNull().isSameAs(CONFIRMED_BY_WAREHOUSE);
        verify(order).getConsignments();
        verifyNoMoreInteractions(order);
    }

    @Test
    public void testFindConsignmentFromOrderWithStatusWithTwoConsignmentInOrderTwoStatusPassedIn() {
        given(order.getConsignments())
                .willReturn(
                        setupConsignmentsWithStatus("1234", CONFIRMED_BY_WAREHOUSE,
                                CREATED));
        final List<ConsignmentStatus> statuses = new ArrayList<>();
        addAll(statuses, CREATED, CONFIRMED_BY_WAREHOUSE);
        final ConsignmentModel consignment = targetConsignmentServiceImpl.findConsignmentFromOrderWithStatus(order,
                statuses);
        assertThat(consignment).isNotNull();
        assertThat(consignment.getStatus()).isNotNull().isSameAs(CREATED);
        verify(order).getConsignments();
        verifyNoMoreInteractions(order);
    }

    @Test
    public void testFindConsignmentFromOrderWithStatusWithActualScenario() {
        given(order.getConsignments())
                .willReturn(
                        setupConsignmentsWithStatus("1234", ConsignmentStatus.CANCELLED,
                                ConsignmentStatus.CREATED));
        final List<ConsignmentStatus> statuses = new ArrayList<>();
        addAll(statuses, CREATED, CONFIRMED_BY_WAREHOUSE);
        final ConsignmentModel consignment = targetConsignmentServiceImpl.findConsignmentFromOrderWithStatus(order,
                statuses);
        assertThat(consignment).isNotNull();
        assertThat(consignment.getStatus()).isNotNull().isSameAs(CREATED);
        verify(order).getConsignments();
        verifyNoMoreInteractions(order);
    }

    @Test
    public void testGetActiveConsignmentsForOrderNoConsignments() {

        given(order.getConsignments()).willReturn(EMPTY_SET);

        final List<TargetConsignmentModel> consList = targetConsignmentServiceImpl.getActiveConsignmentsForOrder(order);
        assertThat(consList).isNotNull().isEmpty();
    }

    @Test
    public void testGetActiveConsignmentsForOrder() {

        final Set<ConsignmentModel> set = new HashSet<>();

        final ConsignmentModel conNonTarget = createNonTargetConsignment("nontarget");
        conNonTarget.setStatus(CONFIRMED_BY_WAREHOUSE);
        set.add(conNonTarget);

        final ConsignmentModel conCancelled = createConsignment("cancelled");
        conCancelled.setStatus(CANCELLED);
        set.add(conCancelled);

        final ConsignmentModel conConfirmed = createConsignment("confirmed");
        conConfirmed.setStatus(CONFIRMED_BY_WAREHOUSE);
        set.add(conConfirmed);

        final ConsignmentModel conSent = createConsignment("sent");
        conSent.setStatus(SENT_TO_WAREHOUSE);
        set.add(conSent);

        given(order.getConsignments()).willReturn(set);

        final List<TargetConsignmentModel> consList = targetConsignmentServiceImpl.getActiveConsignmentsForOrder(order);
        assertThat(consList).isNotNull().isNotEmpty().hasSize(2).contains(conConfirmed, conSent);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindConsignmentForDefaultWarehouseWhenOrderNotPresent() {
        targetConsignmentServiceImpl
                .findConsignmentForDefaultWarehouse("ORDER");
    }

    @Test
    public void testFindConsignmentForDefaultWarehouseWhenOrderHasNoConsignments() {
        final OrderModel orderModel = mock(OrderModel.class);
        given(targetOrderService.findOrderModelForOrderId("ORDER")).willReturn(
                orderModel);
        assertThat(targetConsignmentServiceImpl
                .findConsignmentForDefaultWarehouse("ORDER"));
    }

    @Test
    public void testFindConsignmentForDefaultWarehouseWhenAllConsignmentHasNoWarehouse() {
        final OrderModel orderModel = mock(OrderModel.class);
        given(targetOrderService.findOrderModelForOrderId("ORDER")).willReturn(
                orderModel);
        final Set<ConsignmentModel> consignments = new HashSet<>();
        consignments.add(new TargetConsignmentModel());
        given(orderModel.getConsignments()).willReturn(consignments);
        assertThat(targetConsignmentServiceImpl
                .findConsignmentForDefaultWarehouse("ORDER"));
    }

    @Test
    public void testFindConsignmentForDefaultWarehouseWhenAllConsignmentHasNonDefaultWarehouse() {
        final OrderModel orderModel = mock(OrderModel.class);
        final TargetConsignmentModel consignmentModel = mock(TargetConsignmentModel.class);
        final WarehouseModel warehouseModel = mock(WarehouseModel.class);
        given(targetOrderService.findOrderModelForOrderId("ORDER")).willReturn(
                orderModel);
        given(warehouseModel.getDefault()).willReturn(FALSE);
        given(consignmentModel.getStatus()).willReturn(SENT_TO_WAREHOUSE);
        given(consignmentModel.getWarehouse()).willReturn(warehouseModel);
        final Set<ConsignmentModel> consignments = new HashSet<>();
        consignments.add(consignmentModel);
        given(orderModel.getConsignments()).willReturn(consignments);
        assertThat(targetConsignmentServiceImpl
                .findConsignmentForDefaultWarehouse("ORDER")).isNull();
    }

    @Test
    public void testFindConsignmentForDefaultWarehouseWhenAllConsignmentHasDefaultWarehouse() {
        final OrderModel orderModel = mock(OrderModel.class);
        final TargetConsignmentModel consignmentModel = mock(TargetConsignmentModel.class);
        final WarehouseModel warehouseModel = mock(WarehouseModel.class);
        given(targetOrderService.findOrderModelForOrderId("ORDER")).willReturn(
                orderModel);
        given(warehouseModel.getDefault()).willReturn(TRUE);
        given(consignmentModel.getStatus()).willReturn(SENT_TO_WAREHOUSE);
        given(consignmentModel.getWarehouse()).willReturn(warehouseModel);
        final Set<ConsignmentModel> consignments = new HashSet<>();
        consignments.add(consignmentModel);
        given(orderModel.getConsignments()).willReturn(consignments);
        assertThat(targetConsignmentServiceImpl
                .findConsignmentForDefaultWarehouse("ORDER")).isNotNull();
    }

    @Test
    public void testGetActiveConsignmentsForOrderWithNoConsignmentStatus() {
        final OrderModel orderModel = mock(OrderModel.class);
        final TargetConsignmentModel consignmentModel = mock(TargetConsignmentModel.class);
        final Set<ConsignmentModel> consignments = new HashSet<>();
        consignments.add(consignmentModel);
        given(orderModel.getConsignments()).willReturn(consignments);
        assertThat(targetConsignmentServiceImpl.getActiveConsignmentsForOrder(orderModel)).isEmpty();
    }

    @Test
    public void testGetActiveDeliverToStoreConsignmentsForOrder() {

        final Set<ConsignmentModel> set = new HashSet<>();

        final ConsignmentModel conCnc = createConsignment("cnc");
        conCnc.setStatus(SENT_TO_WAREHOUSE);
        final TargetZoneDeliveryModeModel delMode = mock(TargetZoneDeliveryModeModel.class);
        given(delMode.getIsDeliveryToStore()).willReturn(TRUE);
        conCnc.setDeliveryMode(delMode);
        set.add(conCnc);

        final ConsignmentModel conDig = createConsignment("dig");
        conDig.setStatus(SENT_TO_WAREHOUSE);
        final TargetZoneDeliveryModeModel delMode2 = mock(TargetZoneDeliveryModeModel.class);
        given(delMode2.getIsDeliveryToStore()).willReturn(FALSE);
        conDig.setDeliveryMode(delMode2);
        set.add(conDig);

        final ConsignmentModel conWrongType = createConsignment("bad");
        conWrongType.setStatus(SENT_TO_WAREHOUSE);
        final DeliveryModeModel delMode3 = mock(DeliveryModeModel.class);
        conWrongType.setDeliveryMode(delMode3);
        set.add(conWrongType);

        given(order.getConsignments()).willReturn(set);

        final List<TargetConsignmentModel> consList = targetConsignmentServiceImpl
                .getActiveDeliverToStoreConsignmentsForOrder(order);
        assertThat(consList).isNotNull().isNotEmpty().hasSize(1).contains(conCnc);
    }


    /**
     * 
     */
    private void setupExpectedCallsForUniqueCodeTest() {
        final TargetConsignmentModel targetConsignmentModel = new TargetConsignmentModel();
        given(modelService.create(TargetConsignmentModel.class)).willReturn(targetConsignmentModel);

        final WarehouseModel warehouseMock = mock(WarehouseModel.class);
        final List<WarehouseModel> warehouses = new ArrayList<>();
        warehouses.add(warehouseMock);

        given(targetWarehouseService.getWarehouses(orderEntries)).willReturn(warehouses);
    }

    private Set<ConsignmentModel> setupConsignments(final String... consignmentCodes) {
        final Set<ConsignmentModel> consignments = new HashSet<>();
        for (final String code : consignmentCodes) {
            final ConsignmentModel consignment = createConsignment(code);
            consignments.add(consignment);
        }
        return consignments;
    }

    /**
     * @param code
     * @return ConsignmentModel
     */
    private ConsignmentModel createNonTargetConsignment(final String code) {
        final ConsignmentModel consignment = new ConsignmentModel();
        consignment.setCode(code);
        return consignment;
    }

    private ConsignmentModel createConsignment(final String code) {
        final TargetConsignmentModel consignment = new TargetConsignmentModel();
        consignment.setCode(code);
        return consignment;
    }

    private Set<ConsignmentModel> setupConsignmentsWithStatus(final String orderCode,
            final ConsignmentStatus... consignmentStatus) {
        final Set<ConsignmentModel> consignments = new HashSet<>();
        char prefix = 'a';
        for (final ConsignmentStatus status : consignmentStatus) {
            final ConsignmentModel consignment = createConsignment(prefix + orderCode);
            consignment.setStatus(status);
            consignments.add(consignment);
            prefix++;
        }
        return consignments;
    }

    @Test
    public void testGetConsignmentByCode() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException,
            NotFoundException {
        final String code = "consCode";
        final TargetConsignmentModel consignment = mock(TargetConsignmentModel.class);
        given(targetConsignmentDao.getConsignmentBycode(code)).willReturn(consignment);
        assertThat(targetConsignmentServiceImpl.getConsignmentForCode(code).equals(consignment));
    }

    @Test
    public void testGetConsignmentByCodeForAmbiguity() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException, NotFoundException {
        expectedException.expect(RuntimeException.class);
        final String code = "consCode";
        given(targetConsignmentDao.getConsignmentBycode(code)).willThrow(
                new TargetAmbiguousIdentifierException("More Records"));
        targetConsignmentServiceImpl.getConsignmentForCode(code);
    }

    @Test
    public void testGetConsignmentByCodeForNonAvailability() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException, NotFoundException {
        expectedException.expect(NotFoundException.class);
        final String code = "consCode";
        given(targetConsignmentDao.getConsignmentBycode(code)).willThrow(
                new TargetUnknownIdentifierException("No Records"));
        targetConsignmentServiceImpl.getConsignmentForCode(code);
    }

    @Test
    public void testUpdateReadyForPickUpDate() {
        final TargetConsignmentModel consignmentModel = new TargetConsignmentModel();
        assertThat(targetConsignmentServiceImpl.updateReadyForPickupDate(consignmentModel)).isTrue();
        verify(modelService).save(consignmentModel);
    }

    @Test
    public void testUpdateReadyForPickUpDateForNotNullDate() {
        final TargetConsignmentModel consignmentModel = new TargetConsignmentModel();
        consignmentModel.setReadyForPickUpDate(new Date());
        assertThat(targetConsignmentServiceImpl.updateReadyForPickupDate(consignmentModel)).isFalse();
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testUpdateReadyForPickUpDateForNullConsignment() {
        assertThat(targetConsignmentServiceImpl.updateReadyForPickupDate(null)).isFalse();
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testUpdatePickedupDateWhenConsignmentIsNotNullAndPickedDateIsNull() {
        final TargetConsignmentModel consignment = new TargetConsignmentModel();
        assertThat(targetConsignmentServiceImpl.updatePickedupDate(consignment)).isTrue();
        assertThat(consignment.getPickedUpDate()).isNotNull();
        verify(modelService).save(consignment);
    }

    @Test
    public void testUpdatePickedupDateWhenConsignmentIsNotNullAndPickedDateIsNotNull() {
        final TargetConsignmentModel consignment = new TargetConsignmentModel();
        consignment.setPickedUpDate(new Date());
        assertThat(targetConsignmentServiceImpl.updatePickedupDate(consignment)).isFalse();
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testGetActiveDeliverToStoreConsignmentForOrderWhenActiveConsignmentFound() {
        final OrderModel orderModel = mock(OrderModel.class);
        final List<TargetConsignmentModel> consignments = Collections
                .singletonList(mockedConsignment);
        willReturn(consignments).given(targetConsignmentServiceImpl)
                .getActiveDeliverToStoreConsignmentsForOrder(orderModel);
        assertThat(targetConsignmentServiceImpl.getActiveDeliverToStoreConsignmentForOrder(orderModel))
                .isEqualTo(mockedConsignment);
    }

    @Test
    public void testGetActiveDeliverToStoreConsignmentForOrderWhenNoActiveConsignmentFound() {
        final OrderModel orderModel = mock(OrderModel.class);
        willReturn(null).given(targetConsignmentServiceImpl)
                .getActiveDeliverToStoreConsignmentsForOrder(orderModel);
        assertThat(targetConsignmentServiceImpl.getActiveDeliverToStoreConsignmentForOrder(orderModel)).isNull();
    }

    @Test
    public void testUpdateReturnedToFloorDateWhenConsgIsNotNullAndRetToFloorDateIsNull() {
        final TargetConsignmentModel consignment = new TargetConsignmentModel();
        targetConsignmentServiceImpl.updateReturnedToFloorDate(consignment);
        assertThat(consignment.getReturnedToFloorDate()).isNotNull();
        verify(modelService).save(consignment);
    }

    @Test
    public void testUpdateReturnedToFloorDateWhenConsgIsNotNullAndRetToFloorDateIsNotNull() {
        final TargetConsignmentModel consignment = new TargetConsignmentModel();
        consignment.setReturnedToFloorDate(new Date());
        targetConsignmentServiceImpl.updateReturnedToFloorDate(consignment);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testUpdateReturnedToFloorDateWhenConsgIsNull() {
        targetConsignmentServiceImpl.updateReturnedToFloorDate(null);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testGetAllConsignmentByStatusAndWarehouse() throws NotFoundException {

        given(
                targetFulfilmentWarehouseService.getWarehouseForCode("Incomm")).willReturn(warehouse);
        given(
                targetConsignmentDao.getAllConsignmentsByStatusAndWarehouse(warehouse,
                        CONFIRMED_BY_WAREHOUSE)).willReturn(
                                singletonList(mockedConsignment));
        given(mockedConsignment.getCode()).willReturn("a10002");
        given(mockedConsignment.getOrder()).willReturn(order);
        given(order.getFluentId()).willReturn("");
        final List<TargetConsignmentModel> consignmentsList = targetConsignmentServiceImpl
                .getAllConsignmentByStatusAndWarehouse(CONFIRMED_BY_WAREHOUSE,
                        "Incomm");
        assertThat(consignmentsList).hasSize(1);
        assertThat(consignmentsList.get(0).getCode()).isEqualTo("a10002");
        assertThat(consignmentsList.get(0).getOrder().getFluentId()).isEmpty();
    }

    @Test(expected = NotFoundException.class)
    public void testGetAllConsignmentByStatusAndWarehouseNotFound() throws NotFoundException {
        given(
                targetFulfilmentWarehouseService.getWarehouseForCode("Incomm")).willReturn(warehouse);
        given(
                targetConsignmentDao.getAllConsignmentsByStatusAndWarehouse(warehouse,
                        CONFIRMED_BY_WAREHOUSE)).willThrow(
                                new UnknownIdentifierException("exception"));
        targetConsignmentServiceImpl
                .getAllConsignmentByStatusAndWarehouse(CONFIRMED_BY_WAREHOUSE,
                        "Incomm");
    }

    @Test
    public void testGetAllConsignmentByStatusAndWarehouseNoConsignment() throws NotFoundException {
        given(
                targetFulfilmentWarehouseService.getWarehouseForCode("Incomm")).willReturn(warehouse);
        given(
                targetConsignmentDao.getAllConsignmentsByStatusAndWarehouse(warehouse,
                        CONFIRMED_BY_WAREHOUSE)).willReturn(new ArrayList());
        given(mockedConsignment.getCode()).willReturn("a10002");
        final List<TargetConsignmentModel> consignmentsList = targetConsignmentServiceImpl
                .getAllConsignmentByStatusAndWarehouse(CONFIRMED_BY_WAREHOUSE,
                        "Incomm");
        assertThat(consignmentsList).hasSize(0);

    }

    @Test(expected = NotFoundException.class)
    public void testGetAllConsignmentByStatusAndWarehouseNoWarehouse() throws NotFoundException {
        given(
                targetFulfilmentWarehouseService.getWarehouseForCode("Incomm")).willThrow(
                        new UnknownIdentifierException("exception"));
        given(
                targetConsignmentDao.getAllConsignmentsByStatusAndWarehouse(warehouse,
                        CONFIRMED_BY_WAREHOUSE)).willReturn(new ArrayList());
        given(mockedConsignment.getCode()).willReturn("a10002");
        targetConsignmentServiceImpl
                .getAllConsignmentByStatusAndWarehouse(CONFIRMED_BY_WAREHOUSE,
                        "Incomm");
    }

    /**
     * test scenarios: total 6 consignments, 1. cancel, 2. fastline 3.incomm 4. store.(inter store delivery) 5.
     * store(instore pickup) 6. store.(home delivery) expected return: 4. store.(inter store delivery) 6. store.(home
     * delivery)
     */
    @Test
    public void testGetActivePPDStoreConsignments() {
        final TargetConsignmentModel consignment1 = mock(TargetConsignmentModel.class);
        given(consignment1.getStatus()).willReturn(CANCELLED);
        final WarehouseModel warehouse1 = mock(WarehouseModel.class);
        given(warehouse1.getCode()).willReturn(FASTLINE_WAREHOUSE);
        given(consignment1.getWarehouse()).willReturn(warehouse1);
        given(consignment1.getOfcOrderType()).willReturn(CUSTOMER_DELIVERY);

        final TargetConsignmentModel consignment2 = mock(TargetConsignmentModel.class);
        given(consignment2.getStatus()).willReturn(SENT_TO_WAREHOUSE);
        final WarehouseModel warehouse2 = mock(WarehouseModel.class);
        given(warehouse2.getCode()).willReturn(FASTLINE_WAREHOUSE);
        given(consignment2.getWarehouse()).willReturn(warehouse2);
        given(consignment2.getOfcOrderType()).willReturn(CUSTOMER_DELIVERY);

        final TargetConsignmentModel consignment3 = mock(TargetConsignmentModel.class);
        given(consignment3.getStatus()).willReturn(CONFIRMED_BY_WAREHOUSE);
        final WarehouseModel warehouse3 = mock(WarehouseModel.class);
        given(warehouse3.getCode()).willReturn("incommWarehouse");
        given(Boolean.valueOf(warehouse3.isExternalWarehouse())).willReturn(Boolean.TRUE);
        given(consignment3.getWarehouse()).willReturn(warehouse3);
        given(consignment3.getOfcOrderType()).willReturn(CUSTOMER_DELIVERY);


        final TargetConsignmentModel consignment4 = mock(TargetConsignmentModel.class);
        given(consignment4.getStatus()).willReturn(WAVED);
        final WarehouseModel warehouse4 = mock(WarehouseModel.class);
        given(warehouse4.getCode()).willReturn("Store4Warehouse");
        given(consignment4.getWarehouse()).willReturn(warehouse4);
        given(consignment4.getOfcOrderType()).willReturn(INTERSTORE_DELIVERY);

        final TargetConsignmentModel consignment5 = mock(TargetConsignmentModel.class);
        given(consignment5.getStatus()).willReturn(PICKED);
        final WarehouseModel warehouse5 = mock(WarehouseModel.class);
        given(warehouse5.getCode()).willReturn("Store5Warehouse");
        given(consignment5.getWarehouse()).willReturn(warehouse5);
        given(consignment5.getOfcOrderType()).willReturn(INSTORE_PICKUP);

        final TargetConsignmentModel consignment6 = mock(TargetConsignmentModel.class);
        given(consignment6.getStatus()).willReturn(PACKED);
        final WarehouseModel warehouse6 = mock(WarehouseModel.class);
        given(warehouse6.getCode()).willReturn("Store6Warehouse");
        given(consignment6.getWarehouse()).willReturn(warehouse6);
        given(consignment6.getOfcOrderType()).willReturn(CUSTOMER_DELIVERY);

        final Set consignments = Sets.newHashSet(consignment1, consignment2, consignment3, consignment4, consignment5,
                consignment6);
        given(order.getConsignments()).willReturn(consignments);
        final List<TargetConsignmentModel> activeCons = targetConsignmentServiceImpl
                .getActivePPDStoreConsignments(order);
        assertThat(activeCons).hasSize(2);
        assertThat(activeCons).contains(consignment4, consignment6);
    }

    @Test
    public void testGetOrCreateConsignmentForCodeExisting() throws NotFoundException {
        final TargetConsignmentModel expectedConsignment = mock(TargetConsignmentModel.class);
        willReturn(expectedConsignment).given(targetConsignmentServiceImpl)
                .getConsignmentForCode("existingConsignment");

        final TargetConsignmentModel consignment = targetConsignmentServiceImpl
                .getOrCreateConsignmentForCode("existingConsignment");
        assertThat(consignment).isEqualTo(expectedConsignment);
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testGetOrCreateConsignmentForCodeNew() throws NotFoundException {
        final TargetConsignmentModel expectedConsignment = mock(TargetConsignmentModel.class);
        willThrow(new NotFoundException("")).given(targetConsignmentServiceImpl)
                .getConsignmentForCode("newConsignment");
        willReturn(expectedConsignment).given(modelService).create(TargetConsignmentModel.class);

        final TargetConsignmentModel consignment = targetConsignmentServiceImpl
                .getOrCreateConsignmentForCode("newConsignment");
        assertThat(consignment).isEqualTo(expectedConsignment);
        verify(consignment).setCode("newConsignment");
    }

    @Test
    public void testCreateConsignmentForCodeNew() {
        final TargetConsignmentModel expectedConsignment = mock(TargetConsignmentModel.class);
        willReturn(expectedConsignment).given(modelService).create(TargetConsignmentModel.class);

        final TargetConsignmentModel consignment = targetConsignmentServiceImpl
                .createConsignmentForCode("newConsignment");
        assertThat(consignment).isEqualTo(expectedConsignment);
        verify(consignment).setCode("newConsignment");
    }

    @Test
    public void testIsAFluentConsignmentTrue() {
        given(mockedConsignment.getOrder()).willReturn(order);
        given(order.getFluentId()).willReturn("123");

        assertThat(targetConsignmentServiceImpl.isAFluentConsignment(mockedConsignment)).isTrue();
    }

    @Test
    public void testIsAFluentConsignmentFalse() {
        given(mockedConsignment.getOrder()).willReturn(order);
        given(order.getFluentId()).willReturn(null);

        assertThat(targetConsignmentServiceImpl.isAFluentConsignment(mockedConsignment)).isFalse();
    }

    @Test
    public void testIsAFluentConsignmentFalseWhenFluentSwitchOff() {
        given(mockedConsignment.getOrder()).willReturn(order);
        given(order.getFluentId()).willReturn("234");

        willReturn(FALSE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);

        assertThat(targetConsignmentServiceImpl.isAFluentConsignment(mockedConsignment)).isFalse();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsAFluentConsignmentWhenConsignmentNull() {

        try {
            targetConsignmentServiceImpl.isAFluentConsignment(null);
        }
        catch (final IllegalArgumentException e) {
            assertThat(e.getMessage()).isEqualTo("consignment cannot be null");
            throw e;
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsAFluentConsignmentWhenOrderNull() {

        given(mockedConsignment.getOrder()).willReturn(null);
        try {
            targetConsignmentServiceImpl.isAFluentConsignment(mockedConsignment);
        }
        catch (final IllegalArgumentException e) {
            assertThat(e.getMessage()).isEqualTo("order cannot be null");
            throw e;
        }
    }

    public void testIsConsignmentToDefaultWarehouseWithNullConsignment() {
        assertThat(targetConsignmentServiceImpl.isConsignmentToDefaultWarehouse(null)).isFalse();
    }

    @Test
    public void testIsConsignmentToDefaultWarehouseWithNullConsignmentWarehouse() {
        given(mockedConsignment.getWarehouse()).willReturn(null);
        assertThat(targetConsignmentServiceImpl.isConsignmentToDefaultWarehouse(mockedConsignment)).isFalse();
    }

    @Test
    public void testIsConsignmentToDefaultWarehouseWithConsignmentStoreWarehouse() {
        final WarehouseModel storeWarehouse = mock(WarehouseModel.class);
        given(mockedConsignment.getWarehouse()).willReturn(storeWarehouse);
        assertThat(targetConsignmentServiceImpl.isConsignmentToDefaultWarehouse(mockedConsignment)).isFalse();
    }

    @Test
    public void testIsConsignmentToDefaultWarehouseWithConsignmentOnlineWarehouse() {
        final WarehouseModel onlineWarehouse = mock(WarehouseModel.class);
        given(targetWarehouseService.getDefaultOnlineWarehouse()).willReturn(onlineWarehouse);
        given(mockedConsignment.getWarehouse()).willReturn(onlineWarehouse);
        assertThat(targetConsignmentServiceImpl.isConsignmentToDefaultWarehouse(mockedConsignment)).isTrue();
    }

    @Test
    public void testIsConsignmentServicedByFluentServicePoint() {
        given(mockedConsignment.getWarehouse()).willReturn(warehouse);
        final Collection<PointOfServiceModel> collection = new ArrayList<>();
        collection.add(posModel);
        given(warehouse.getPointsOfService()).willReturn(collection);
        given(posModel.getStorePortal()).willReturn(SERVICE_POINT);

        assertThat(targetConsignmentServiceImpl.isConsignmentServicedByFluentServicePoint(mockedConsignment)).isTrue();
    }

    @Test
    public void testIsConsignmentServicedByFluentServicePointWhenStorePortalIsOFC() {
        given(mockedConsignment.getWarehouse()).willReturn(warehouse);
        final Collection<PointOfServiceModel> collection = new ArrayList<>();
        collection.add(posModel);
        given(warehouse.getPointsOfService()).willReturn(collection);
        given(posModel.getStorePortal()).willReturn(OFC);

        assertThat(targetConsignmentServiceImpl.isConsignmentServicedByFluentServicePoint(mockedConsignment)).isFalse();
    }

    @Test
    public void testIsConsignmentServicedByFluentServicePointWhenPosEmpty() {
        given(mockedConsignment.getWarehouse()).willReturn(warehouse);
        given(warehouse.getPointsOfService()).willReturn(EMPTY_LIST);
        given(posModel.getStorePortal()).willReturn(SERVICE_POINT);

        assertThat(targetConsignmentServiceImpl.isConsignmentServicedByFluentServicePoint(mockedConsignment)).isFalse();
    }

    @Test
    public void testIsConsignmentServicedByFluentServicePointWhenPosGreaterThanOne() {
        given(mockedConsignment.getWarehouse()).willReturn(warehouse);
        final TargetPointOfServiceModel posModel2 = mock(TargetPointOfServiceModel.class);
        final Collection<PointOfServiceModel> collection = new ArrayList<>();
        collection.add(posModel);
        collection.add(posModel2);
        given(warehouse.getPointsOfService()).willReturn(collection);

        assertThat(targetConsignmentServiceImpl.isConsignmentServicedByFluentServicePoint(mockedConsignment)).isFalse();
    }

    @Test
    public void testIsConsignmentServicedByFluentServicePointFluentSwitchOff() {
        willReturn(Boolean.FALSE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);

        assertThat(targetConsignmentServiceImpl.isConsignmentServicedByFluentServicePoint(mockedConsignment)).isFalse();
    }
}
