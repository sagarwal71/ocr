/**
 * 
 */
package au.com.target.tgtfulfilment.integration.converters.sendtowarehouse;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Test;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.CustomerDTO;


/**
 * Tests for SendToWarehouseCustomerConverter
 * 
 * @author jjayawa1
 *
 */
@UnitTest
public class SendToWarehouseCustomerConverterTest {

    private final SendToWarehouseCustomerConverter sendToWarehouseCustomerConverter = new SendToWarehouseCustomerConverter();

    @Test(expected = IllegalArgumentException.class)
    public void testNullCustomer() {
        sendToWarehouseCustomerConverter.convert(null);
    }

    @Test
    public void testEmptyCustomerModel() {
        final CustomerDTO customer = sendToWarehouseCustomerConverter.convert(new TargetCustomerModel());
        Assert.assertNotNull(customer);
        Assert.assertNull(customer.getEmailAddress());
        Assert.assertNull(customer.getFirstName());
        Assert.assertNull(customer.getLastName());
        Assert.assertEquals("AU", customer.getCountry());
    }
}
