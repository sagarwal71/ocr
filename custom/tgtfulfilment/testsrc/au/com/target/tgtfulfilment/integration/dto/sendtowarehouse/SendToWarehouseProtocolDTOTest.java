/**
 * 
 */
package au.com.target.tgtfulfilment.integration.dto.sendtowarehouse;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Test for conversion of SendToWarehouseProtocolDTO to JSON
 * 
 * @author jjayawa1
 *
 */
@UnitTest
public class SendToWarehouseProtocolDTOTest {

    private SendToWarehouseRequest setupDataSingleConsignmentEntrySingleRecipient() {
        final SendToWarehouseRequest request = new SendToWarehouseRequest();

        final ConsignmentDTO consignmentDTO = new ConsignmentDTO();
        consignmentDTO.setId("a123456");
        consignmentDTO.setWarehouseId("Incomm");

        final OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId("123456");
        final CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setFirstName("Jane");
        customerDTO.setLastName("Doe");
        customerDTO.setEmailAddress("jane.doe@test.com");
        customerDTO.setCountry("AU");
        orderDTO.setCustomer(customerDTO);
        consignmentDTO.setOrder(orderDTO);

        final ProductDTO productDTO = new ProductDTO();
        productDTO.setId("PGC1000_iTunes_10");
        productDTO.setName("iTunes gift card");
        productDTO.setDenomination("$10");
        productDTO.setBrandId("iTunes");
        productDTO.setGiftCardStyle("cars");
        final RecipientDTO recipient1 = new RecipientDTO();
        recipient1.setFirstName("first");
        recipient1.setLastName("last");
        recipient1.setEmailAddress("first.last@test.com");
        recipient1.setMessage("Happy b'day");
        final List<RecipientDTO> recipients = new ArrayList<>();
        recipients.add(recipient1);
        final ConsignmentEntryDTO consignmentEntry1 = new ConsignmentEntryDTO();
        consignmentEntry1.setProduct(productDTO);
        consignmentEntry1.setRecipients(recipients);
        final List<ConsignmentEntryDTO> consignmentEntries = new ArrayList<>();
        consignmentEntries.add(consignmentEntry1);
        consignmentDTO.setConsignmentEntries(consignmentEntries);
        request.setConsignment(consignmentDTO);

        return request;
    }

    private SendToWarehouseRequest setupDataSingleConsignmentEntryMultipleRecipients() {
        final SendToWarehouseRequest request = new SendToWarehouseRequest();

        final ConsignmentDTO consignmentDTO = new ConsignmentDTO();
        consignmentDTO.setId("a123456");
        consignmentDTO.setWarehouseId("Incomm");

        final OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId("123456");
        final CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setFirstName("Jane");
        customerDTO.setLastName("Doe");
        customerDTO.setEmailAddress("jane.doe@test.com");
        customerDTO.setCountry("AU");
        orderDTO.setCustomer(customerDTO);
        consignmentDTO.setOrder(orderDTO);

        final ProductDTO productDTO = new ProductDTO();
        productDTO.setId("PGC1000_iTunes_10");
        productDTO.setName("iTunes gift card");
        productDTO.setDenomination("$10");
        productDTO.setBrandId("iTunes");
        productDTO.setGiftCardStyle("cars");
        final RecipientDTO recipient1 = new RecipientDTO();
        recipient1.setFirstName("first");
        recipient1.setLastName("last");
        recipient1.setEmailAddress("first.last@test.com");
        recipient1.setMessage("Happy b'day");
        final RecipientDTO recipient2 = new RecipientDTO();
        recipient2.setFirstName("first1");
        recipient2.setLastName("last2");
        recipient2.setEmailAddress("first1.last2@test.com");
        recipient2.setMessage("Happy d'day");
        final List<RecipientDTO> recipients = new ArrayList<>();
        recipients.add(recipient1);
        recipients.add(recipient2);
        final ConsignmentEntryDTO consignmentEntry1 = new ConsignmentEntryDTO();
        consignmentEntry1.setProduct(productDTO);
        consignmentEntry1.setRecipients(recipients);
        final List<ConsignmentEntryDTO> consignmentEntries = new ArrayList<>();
        consignmentEntries.add(consignmentEntry1);
        consignmentDTO.setConsignmentEntries(consignmentEntries);
        request.setConsignment(consignmentDTO);

        return request;
    }

    private SendToWarehouseRequest setupDataMultipleConsignmentEntriesSingleRecipient() {
        final SendToWarehouseRequest request = new SendToWarehouseRequest();

        final ConsignmentDTO consignmentDTO = new ConsignmentDTO();
        consignmentDTO.setId("a123456");
        consignmentDTO.setWarehouseId("Incomm");

        final OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId("123456");
        final CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setFirstName("Jane");
        customerDTO.setLastName("Doe");
        customerDTO.setEmailAddress("jane.doe@test.com");
        customerDTO.setCountry("AU");
        orderDTO.setCustomer(customerDTO);
        consignmentDTO.setOrder(orderDTO);

        final ProductDTO productDTO = new ProductDTO();
        productDTO.setId("PGC1000_iTunes_10");
        productDTO.setName("iTunes gift card");
        productDTO.setDenomination("$10");
        productDTO.setBrandId("iTunes");
        productDTO.setGiftCardStyle("cars");
        final RecipientDTO recipient1 = new RecipientDTO();
        recipient1.setFirstName("first");
        recipient1.setLastName("last");
        recipient1.setEmailAddress("first.last@test.com");
        recipient1.setMessage("Happy b'day");
        final List<RecipientDTO> recipients = new ArrayList<>();
        recipients.add(recipient1);

        final ConsignmentEntryDTO consignmentEntry1 = new ConsignmentEntryDTO();
        consignmentEntry1.setProduct(productDTO);
        consignmentEntry1.setRecipients(recipients);

        final ConsignmentEntryDTO consignmentEntry2 = new ConsignmentEntryDTO();
        consignmentEntry2.setProduct(productDTO);
        consignmentEntry2.setRecipients(recipients);

        final List<ConsignmentEntryDTO> consignmentEntries = new ArrayList<>();
        consignmentEntries.add(consignmentEntry1);
        consignmentEntries.add(consignmentEntry2);
        consignmentDTO.setConsignmentEntries(consignmentEntries);
        request.setConsignment(consignmentDTO);

        return request;
    }

    private SendToWarehouseRequest setupDataMultipleConsignmentEntriesMultipleRecipients() {
        final SendToWarehouseRequest request = new SendToWarehouseRequest();

        final ConsignmentDTO consignmentDTO = new ConsignmentDTO();
        consignmentDTO.setId("a123456");
        consignmentDTO.setWarehouseId("Incomm");

        final OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId("123456");
        final CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setFirstName("Jane");
        customerDTO.setLastName("Doe");
        customerDTO.setEmailAddress("jane.doe@test.com");
        customerDTO.setCountry("AU");
        orderDTO.setCustomer(customerDTO);
        consignmentDTO.setOrder(orderDTO);

        final ProductDTO product1DTO1 = new ProductDTO();
        product1DTO1.setId("PGC1000_iTunes_10");
        product1DTO1.setName("iTunes gift card");
        product1DTO1.setDenomination("$10");
        product1DTO1.setBrandId("iTunes");
        product1DTO1.setGiftCardStyle("cars");

        final ProductDTO productDTO2 = new ProductDTO();
        productDTO2.setId("PGC1000_iTunes_20");
        productDTO2.setName("iTunes gift card");
        productDTO2.setDenomination("$20");
        productDTO2.setBrandId("iTunes");
        productDTO2.setGiftCardStyle("cars");

        final RecipientDTO recipient1 = new RecipientDTO();
        recipient1.setFirstName("first");
        recipient1.setLastName("last");
        recipient1.setEmailAddress("first.last@test.com");
        recipient1.setMessage("Happy b'day");
        final RecipientDTO recipient2 = new RecipientDTO();
        recipient2.setFirstName("first1");
        recipient2.setLastName("last2");
        recipient2.setEmailAddress("first1.last2@test.com");
        recipient2.setMessage("Happy d'day");
        final List<RecipientDTO> recipients = new ArrayList<>();
        recipients.add(recipient1);
        recipients.add(recipient2);

        final ConsignmentEntryDTO consignmentEntry1 = new ConsignmentEntryDTO();
        consignmentEntry1.setProduct(product1DTO1);
        consignmentEntry1.setRecipients(recipients);

        final ConsignmentEntryDTO consignmentEntry2 = new ConsignmentEntryDTO();
        consignmentEntry2.setProduct(productDTO2);
        consignmentEntry2.setRecipients(recipients);

        final List<ConsignmentEntryDTO> consignmentEntries = new ArrayList<>();
        consignmentEntries.add(consignmentEntry1);
        consignmentEntries.add(consignmentEntry2);
        consignmentDTO.setConsignmentEntries(consignmentEntries);
        request.setConsignment(consignmentDTO);

        return request;
    }

    @Test
    public void testSingleConsignmentEntrySingleRecipient() throws JsonGenerationException, JsonMappingException,
            IOException {
        final SendToWarehouseRequest request = setupDataSingleConsignmentEntrySingleRecipient();
        final ObjectMapper mapper = new ObjectMapper();
        final String consignmentDTOJSON = mapper.writeValueAsString(request);
        final String expectedJSON = "{\"consignment\":{\"id\":\"a123456\",\"warehouseId\":"
                + "\"Incomm\",\"order\":{\"id\":\"123456\",\"customer\":{\"firstName\":"
                + "\"Jane\",\"lastName\":\"Doe\",\"emailAddress\":\"jane.doe@test.com\",\"country\":"
                + "\"AU\"}},\"consignmentEntries\":[{\"product\":{\"id\":\"PGC1000_iTunes_10\",\"name\":"
                + "\"iTunes gift card\",\"denomination\":\"$10\",\"brandId\":\"iTunes\",\"giftCardStyle\":"
                + "\"cars\"},\"recipients\":[{\"firstName\":\"first\",\"lastName\":\"last\",\"emailAddress\":"
                + "\"first.last@test.com\",\"message\":\"Happy b'day\"}]}]}}";
        Assert.assertEquals(expectedJSON, consignmentDTOJSON);
    }

    @Test
    public void testSingleConsignmentEntryMultipleRecipients() throws JsonGenerationException, JsonMappingException,
            IOException {
        final SendToWarehouseRequest request = setupDataSingleConsignmentEntryMultipleRecipients();
        final ObjectMapper mapper = new ObjectMapper();
        final String consignmentDTOJSON = mapper.writeValueAsString(request);
        final String expectedJSON = "{\"consignment\":{\"id\":\"a123456\",\"warehouseId\":"
                + "\"Incomm\",\"order\":{\"id\":\"123456\",\"customer\":{\"firstName\":"
                + "\"Jane\",\"lastName\":\"Doe\",\"emailAddress\":\"jane.doe@test.com\",\"country\":"
                + "\"AU\"}},\"consignmentEntries\":[{\"product\":{\"id\":\"PGC1000_iTunes_10\",\"name\":"
                + "\"iTunes gift card\",\"denomination\":\"$10\",\"brandId\":\"iTunes\",\"giftCardStyle\":"
                + "\"cars\"},\"recipients\":[{\"firstName\":\"first\",\"lastName\":\"last\",\"emailAddress\":"
                + "\"first.last@test.com\",\"message\":\"Happy b'day\"},{\"firstName\":\"first1\",\"lastName\":"
                + "\"last2\",\"emailAddress\":\"first1.last2@test.com\",\"message\":\"Happy d'day\"}]}]}}";
        Assert.assertEquals(expectedJSON, consignmentDTOJSON);
    }

    @Test
    public void testMultipleConsignmentEntriesSingleRecipient() throws JsonGenerationException, JsonMappingException,
            IOException {
        final SendToWarehouseRequest request = setupDataMultipleConsignmentEntriesSingleRecipient();
        final ObjectMapper mapper = new ObjectMapper();
        final String consignmentDTOJSON = mapper.writeValueAsString(request);
        final String expectedJSON = "{\"consignment\":{\"id\":\"a123456\",\"warehouseId\":"
                + "\"Incomm\",\"order\":{\"id\":\"123456\",\"customer\":{\"firstName\":"
                + "\"Jane\",\"lastName\":\"Doe\",\"emailAddress\":\"jane.doe@test.com\",\"country\":"
                + "\"AU\"}},\"consignmentEntries\":[{\"product\":{\"id\":\"PGC1000_iTunes_10\",\"name\":"
                + "\"iTunes gift card\",\"denomination\":\"$10\",\"brandId\":\"iTunes\",\"giftCardStyle\":"
                + "\"cars\"},\"recipients\":[{\"firstName\":\"first\",\"lastName\":\"last\",\"emailAddress\":"
                + "\"first.last@test.com\",\"message\":\"Happy b'day\"}]},{\"product\":{\"id\":"
                + "\"PGC1000_iTunes_10\",\"name\":\"iTunes gift card\",\"denomination\":\"$10\",\"brandId\":"
                + "\"iTunes\",\"giftCardStyle\":\"cars\"},\"recipients\":[{\"firstName\":\"first\",\"lastName\":"
                + "\"last\",\"emailAddress\":\"first.last@test.com\",\"message\":\"Happy b'day\"}]}]}}";
        Assert.assertEquals(expectedJSON, consignmentDTOJSON);
    }

    @Test
    public void testMultipleConsignmentEntriesMultipleRecipients() throws JsonGenerationException,
            JsonMappingException,
            IOException {
        final SendToWarehouseRequest request = setupDataMultipleConsignmentEntriesMultipleRecipients();
        final ObjectMapper mapper = new ObjectMapper();
        final String consignmentDTOJSON = mapper.writeValueAsString(request);
        final String expectedJSON = "{\"consignment\":{\"id\":\"a123456\",\"warehouseId\":"
                + "\"Incomm\",\"order\":{\"id\":\"123456\",\"customer\":{\"firstName\":"
                + "\"Jane\",\"lastName\":\"Doe\",\"emailAddress\":\"jane.doe@test.com\",\"country\":"
                + "\"AU\"}},\"consignmentEntries\":[{\"product\":{\"id\":\"PGC1000_iTunes_10\",\"name\":"
                + "\"iTunes gift card\",\"denomination\":\"$10\",\"brandId\":\"iTunes\",\"giftCardStyle\":"
                + "\"cars\"},\"recipients\":[{\"firstName\":\"first\",\"lastName\":\"last\",\"emailAddress\":"
                + "\"first.last@test.com\",\"message\":\"Happy b'day\"},{\"firstName\":\"first1\",\"lastName\":"
                + "\"last2\",\"emailAddress\":\"first1.last2@test.com\",\"message\":\"Happy d'day\"}]},{\"product\":"
                + "{\"id\":\"PGC1000_iTunes_20\",\"name\":\"iTunes gift card\",\"denomination\":\"$20\",\"brandId\":"
                + "\"iTunes\",\"giftCardStyle\":\"cars\"},\"recipients\":[{\"firstName\":\"first\",\"lastName\":"
                + "\"last\",\"emailAddress\":\"first.last@test.com\",\"message\":\"Happy b'day\"},{\"firstName\":"
                + "\"first1\",\"lastName\":\"last2\",\"emailAddress\":\"first1.last2@test.com\",\"message\""
                + ":\"Happy d'day\"}]}]}}";
        Assert.assertEquals(expectedJSON, consignmentDTOJSON);
    }
}
