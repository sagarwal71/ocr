/**
 * 
 */
package au.com.target.tgtfulfilment.integration.converters.sendtowarehouse;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.GiftCardModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfulfilment.constants.TgtfulfilmentConstants;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ProductDTO;


/**
 * Tests for SendToWarehouseProductConverter
 * 
 * @author jjayawa1
 *
 */
@UnitTest
public class SendToWarehouseProductConverterTest {

    private static final String SIZE_NAME = "$10 iTunes Gift Card";
    private static final String COLOUR_NAME = "colour variant product";
    private final SendToWarehouseProductConverter productModelToDTOConverter = new SendToWarehouseProductConverter();

    private TargetSizeVariantProductModel sizeVariant;
    private TargetColourVariantProductModel colourVariant;
    private TargetColourVariantProductModel colourVariant1;
    private TargetProductModel baseProduct;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        sizeVariant = Mockito.mock(TargetSizeVariantProductModel.class);
        colourVariant = Mockito.mock(TargetColourVariantProductModel.class);
        baseProduct = Mockito.mock(TargetProductModel.class);
        colourVariant1 = Mockito.mock(TargetColourVariantProductModel.class);

        Mockito.when(sizeVariant.getCode()).thenReturn("PGC1000_iTunes_10");
        Mockito.when(sizeVariant.getEan()).thenReturn("123456789012");
        Mockito.when(sizeVariant.getDenomination()).thenReturn(Double.valueOf(10));
        Mockito.when(sizeVariant.getName()).thenReturn(SIZE_NAME);

        Mockito.when(Boolean.valueOf(targetFeatureSwitchService
                .isFeatureEnabled(TgtfulfilmentConstants.FEATURE_WEBMETHOD_ORDEREXTRACT_PRODUCT_ID)))
                .thenReturn(Boolean.FALSE);
        Mockito.when(colourVariant.getGiftCardStyle()).thenReturn("iTunes");
        Mockito.when(colourVariant1.getCode()).thenReturn("12345678");
        Mockito.when(colourVariant1.getEan()).thenReturn("9876543210123");
        Mockito.when(colourVariant1.getName()).thenReturn(COLOUR_NAME);
        productModelToDTOConverter.setTargetFeatureSwitchService(targetFeatureSwitchService);
    }

    private void linkSizeToColour() {

        Mockito.when(sizeVariant.getBaseProduct()).thenReturn(colourVariant);
    }

    private void linkColourToBase() {

        Mockito.when(colourVariant.getBaseProduct()).thenReturn(baseProduct);
    }

    private void linkBaseToGiftCard() {

        final GiftCardModel giftCard = new GiftCardModel();
        giftCard.setBrandId("apple1234");
        giftCard.setBrandName("iTunes Gift Card");

        Mockito.when(baseProduct.getGiftCard()).thenReturn(giftCard);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testNullVariantProductModel() {
        productModelToDTOConverter.convert(null);
    }

    @Test
    public void testIfSizeVariantWithNoColourProduct() {

        final ProductDTO product = productModelToDTOConverter.convert(sizeVariant);
        Assert.assertNotNull(product);
        Assert.assertNull(product.getBrandId());
        Assert.assertEquals("10.0", product.getDenomination());
        Assert.assertEquals("PGC1000_iTunes_10", product.getId());
        Assert.assertEquals("123456789012", product.getEan());
        Assert.assertEquals("$10 iTunes Gift Card", product.getName());
        Assert.assertNull(product.getGiftCardStyle());
    }

    @Test
    public void testIfSizeVariantWithNoBaseProduct() {

        linkSizeToColour();

        final ProductDTO product = productModelToDTOConverter.convert(sizeVariant);
        Assert.assertNotNull(product);
        Assert.assertNull(product.getBrandId());
        Assert.assertEquals("10.0", product.getDenomination());
        Assert.assertEquals("iTunes", product.getGiftCardStyle());
        Assert.assertEquals("PGC1000_iTunes_10", product.getId());
        Assert.assertEquals("123456789012", product.getEan());
        Assert.assertEquals("$10 iTunes Gift Card", product.getName());
    }


    @Test
    public void testIfSizeVariantNotGiftCard() {

        linkSizeToColour();
        linkColourToBase();

        final ProductDTO product = productModelToDTOConverter.convert(sizeVariant);
        Assert.assertNotNull(product);
        Assert.assertNull(product.getBrandId());
        Assert.assertEquals("10.0", product.getDenomination());
        Assert.assertEquals("iTunes", product.getGiftCardStyle());
        Assert.assertEquals("PGC1000_iTunes_10", product.getId());
        Assert.assertEquals("123456789012", product.getEan());
        Assert.assertEquals("$10 iTunes Gift Card", product.getName());
    }

    @Test
    public void testIfSizeVariantWhichIsAGiftCard() {

        linkSizeToColour();
        linkColourToBase();
        linkBaseToGiftCard();

        final ProductDTO product = productModelToDTOConverter.convert(sizeVariant);
        Assert.assertNotNull(product);
        Assert.assertEquals("apple1234", product.getBrandId());
        Assert.assertEquals("10.0", product.getDenomination());
        Assert.assertEquals("iTunes", product.getGiftCardStyle());
        Assert.assertEquals("PGC1000_iTunes_10", product.getId());
        Assert.assertEquals("123456789012", product.getEan());
        Assert.assertEquals("$10 iTunes Gift Card", product.getName());
    }

    @Test
    public void testIfSizeVariantWhichIsAGiftCardWithProductIdAsNull() {

        Mockito.when(Boolean.valueOf(targetFeatureSwitchService
                .isFeatureEnabled(TgtfulfilmentConstants.FEATURE_WEBMETHOD_ORDEREXTRACT_PRODUCT_ID)))
                .thenReturn(Boolean.TRUE);
        linkSizeToColour();
        linkColourToBase();
        linkBaseToGiftCard();
        Mockito.when(sizeVariant.getGiftCardProductId()).thenReturn(null);

        final ProductDTO product = productModelToDTOConverter.convert(sizeVariant);
        Assert.assertNotNull(product);
        Assert.assertEquals("apple1234", product.getBrandId());
        Assert.assertEquals("10.0", product.getDenomination());
        Assert.assertEquals("iTunes", product.getGiftCardStyle());
        Assert.assertEquals("PGC1000_iTunes_10", product.getId());
        Assert.assertEquals("123456789012", product.getEan());
        Assert.assertEquals("$10 iTunes Gift Card", product.getName());
        Assert.assertNull(product.getProductId());
    }

    @Test
    public void testIfSizeVariantWhichIsAGiftCardWithProductIdNotNull() {

        Mockito.when(Boolean.valueOf(targetFeatureSwitchService
                .isFeatureEnabled(TgtfulfilmentConstants.FEATURE_WEBMETHOD_ORDEREXTRACT_PRODUCT_ID)))
                .thenReturn(Boolean.TRUE);
        linkSizeToColour();
        linkColourToBase();
        linkBaseToGiftCard();
        Mockito.when(sizeVariant.getGiftCardProductId()).thenReturn("123456");

        final ProductDTO product = productModelToDTOConverter.convert(sizeVariant);
        Assert.assertNotNull(product);
        Assert.assertEquals("apple1234", product.getBrandId());
        Assert.assertEquals("10.0", product.getDenomination());
        Assert.assertEquals("iTunes", product.getGiftCardStyle());
        Assert.assertEquals("PGC1000_iTunes_10", product.getId());
        Assert.assertEquals("123456789012", product.getEan());
        Assert.assertEquals("$10 iTunes Gift Card", product.getName());
        Assert.assertEquals("123456", product.getProductId());
    }

    @Test
    public void testIfColourVariantNotGiftCard() {
        linkColourToBase();
        final ProductDTO product = productModelToDTOConverter.convert(colourVariant1);
        Assert.assertNotNull(product);
        Assert.assertNull(product.getBrandId());
        Assert.assertNull(product.getDenomination());
        Assert.assertNull(product.getGiftCardStyle());
        Assert.assertEquals("12345678", product.getId());
        Assert.assertEquals("9876543210123", product.getEan());
        Assert.assertEquals(COLOUR_NAME, product.getName());
    }
}
