/**
 * 
 */
package au.com.target.tgtfulfilment.integration.converters.sendtowarehouse;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Test;

import au.com.target.tgtcore.model.GiftRecipientModel;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.RecipientDTO;


/**
 * Tests for SendToWarehouseRecipientConverter.
 * 
 * @author jjayawa1
 *
 */
@UnitTest
public class SendToWarehouseRecipientConverterTest {

    private final SendToWarehouseRecipientConverter recipientToDTOConverter = new SendToWarehouseRecipientConverter();

    @Test(expected = IllegalArgumentException.class)
    public void testNullRecipientModel() {
        recipientToDTOConverter.convert(null);
    }

    @Test
    public void testEmptyRecipientModel() {
        final RecipientDTO recipient = recipientToDTOConverter.convert(new GiftRecipientModel());
        Assert.assertNotNull(recipient);
        Assert.assertNull(recipient.getEmailAddress());
        Assert.assertNull(recipient.getFirstName());
        Assert.assertNull(recipient.getLastName());
        Assert.assertNull(recipient.getMessage());
    }
}
