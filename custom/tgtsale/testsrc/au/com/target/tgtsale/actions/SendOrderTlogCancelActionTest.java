package au.com.target.tgtsale.actions;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtsale.salesapplication.impl.TargetSalesSalesApplicationConfigServiceImpl;
import au.com.target.tgtsale.tlog.TargetTransactionLogService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendOrderTlogCancelActionTest {
    @Mock
    private OrderProcessParameterHelper mockOrderProcessParameterHelper;

    @Mock
    private TargetTransactionLogService mockTargetTransactionLogService;

    @InjectMocks
    private final SendOrderTlogCancelAction sendOrderTlogCancelAction = new SendOrderTlogCancelAction();

    @Mock
    private OrderProcessModel mockOrderProcessModel;

    @Mock
    private OrderModel order;

    @Mock
    private OrderCancelRecordEntryModel mockOrderCancelRecordEntry;

    @Mock
    private TargetSalesSalesApplicationConfigServiceImpl salesApplicationConfigService;

    private final BigDecimal amountRemaining = new BigDecimal(10);

    final List<PaymentTransactionEntryModel> refundPaymentTransactionEntries = new ArrayList<>();


    @Before
    public void setup() {
        given(mockOrderProcessParameterHelper.getRefundAmountRemaining(mockOrderProcessModel)).willReturn(
                amountRemaining);
        given(mockOrderProcessModel.getOrder()).willReturn(order);
        given(mockOrderProcessParameterHelper.getRefundPaymentEntryList(mockOrderProcessModel))
                .willReturn(refundPaymentTransactionEntries);
        given(mockOrderProcessParameterHelper.getOrderCancelRequest(mockOrderProcessModel))
                .willReturn(mockOrderCancelRecordEntry);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPublishTlogWithNullOrderCancelRequest() throws Exception {
        given(mockOrderProcessParameterHelper.getOrderCancelRequest(mockOrderProcessModel)).willReturn(null);
        sendOrderTlogCancelAction.publishTlog(mockOrderProcessModel);
    }

    @Test
    public void testPublishTlog() {
        final ArgumentCaptor<BigDecimal> amountRemainingCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        sendOrderTlogCancelAction.publishTlog(mockOrderProcessModel);
        verify(mockTargetTransactionLogService, never()).publishTlogForPreOrderCancel(order,
                refundPaymentTransactionEntries);
        verify(mockTargetTransactionLogService).publishTlogForOrderCancel(eq(mockOrderCancelRecordEntry),
                eq(refundPaymentTransactionEntries), amountRemainingCaptor.capture());
        assertThat(amountRemainingCaptor.getValue()).isEqualByComparingTo(amountRemaining);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPublishTlogWithNullOrder() {
        given(mockOrderProcessModel.getOrder()).willReturn(null);
        sendOrderTlogCancelAction.publishTlog(mockOrderProcessModel);
    }

    @Test
    public void testPublishTlogWithSkipTolgFalse() {
        final ArgumentCaptor<BigDecimal> amountRemainingCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        given(order.getSalesApplication()).willReturn(SalesApplication.TRADEME);
        given(Boolean.valueOf(salesApplicationConfigService.isSkipTlogs(SalesApplication.TRADEME))).willReturn(
                Boolean.FALSE);
        sendOrderTlogCancelAction.publishTlog(mockOrderProcessModel);
        verify(mockTargetTransactionLogService, times(1)).publishTlogForOrderCancel(
                eq(mockOrderCancelRecordEntry), eq(refundPaymentTransactionEntries), amountRemainingCaptor.capture());
        verify(mockTargetTransactionLogService, never()).publishTlogForPreOrderCancel(order,
                refundPaymentTransactionEntries);
        assertThat(amountRemainingCaptor.getValue()).isEqualTo(amountRemaining);

    }

    @Test
    public void testPublishTlogWithSkipTolgTrue() {
        final ArgumentCaptor<BigDecimal> amountRemainingCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        given(order.getSalesApplication()).willReturn(SalesApplication.TRADEME);
        given(Boolean.valueOf(salesApplicationConfigService.isSkipTlogs(SalesApplication.TRADEME))).willReturn(
                Boolean.TRUE);
        sendOrderTlogCancelAction.publishTlog(mockOrderProcessModel);
        verify(mockTargetTransactionLogService, never()).publishTlogForPreOrderCancel(order,
                refundPaymentTransactionEntries);
        verify(mockTargetTransactionLogService, times(0)).publishTlogForOrderCancel(
                eq(mockOrderCancelRecordEntry), eq(refundPaymentTransactionEntries), amountRemainingCaptor.capture());
    }

    @Test
    public void testPublishTlogPreOrder() {
        given(order.getContainsPreOrderItems()).willReturn(Boolean.TRUE);
        sendOrderTlogCancelAction.publishTlog(mockOrderProcessModel);
        verify(mockTargetTransactionLogService).publishTlogForPreOrderCancel(order, refundPaymentTransactionEntries);
    }
}
