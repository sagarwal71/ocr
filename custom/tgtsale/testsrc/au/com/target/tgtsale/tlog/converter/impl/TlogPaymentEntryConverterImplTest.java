/**
 * 
 */
package au.com.target.tgtsale.tlog.converter.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.IpgGiftCardPaymentInfoModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.constants.TgtpaymentConstants;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;
import au.com.target.tgtpayment.model.PaynowPaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgtpayment.model.ZippayPaymentInfoModel;
import au.com.target.tgtsale.tlog.data.TenderInfo;
import au.com.target.tgtsale.tlog.data.TenderTypeEnum;


/**
 * @author mjanarth
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TlogPaymentEntryConverterImplTest {

    /**
     * 
     */
    private static final String RECEIPT_NO = "123456";
    public static final String REFUND_ID = "87654321";
    private final TlogPaymentEntryConverterImpl tlogPaymentEntryConverter = new TlogPaymentEntryConverterImpl();

    @Test
    public void testGetTenderInfoFromPaymentEntryForCreditCard() {
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);

        final Date now = new Date();
        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(paymentTransactionModel.getPaymentProvider()).willReturn("tns");
        given(paymentTransactionEntryModel.getAmount()).willReturn(BigDecimal.valueOf(29.99d));
        given(paymentTransactionEntryModel.getReceiptNo()).willReturn(RECEIPT_NO);

        final CreditCardPaymentInfoModel creditCardInfoModel = new CreditCardPaymentInfoModel();
        creditCardInfoModel.setNumber("5123XXXXXXXXX346");
        given(paymentTransactionEntryModel.getPaymentTransaction().getInfo()).willReturn(
                creditCardInfoModel);
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(
                TransactionStatus.ACCEPTED.toString());
        given(paymentTransactionEntryModel.getCreationtime()).willReturn(now);

        final TenderInfo tender = tlogPaymentEntryConverter.getTenderInfoFromPaymentEntry(paymentTransactionEntryModel,
                false);

        assertThat(tender.getType()).isEqualTo(TenderTypeEnum.EFT);
        assertThat(tender.getAmount()).isEqualTo(BigDecimal.valueOf(29.99d));
        assertThat(tender.getCardNumber()).isEqualTo("5123XXXXXXXXX346");
        assertThat(tender.getCreditCardRRN()).isEqualTo(RECEIPT_NO);
        assertThat(tender.getProcessedTime()).isEqualTo(now);
        assertThat(tender.getApproved()).isEqualTo("Y");
    }

    @Test
    public void testGetTenderInfoFromPaymentEntryForPaypal() {
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        final PaypalPaymentInfoModel paypalPaymentInfo = mock(PaypalPaymentInfoModel.class);

        final Date now = new Date();
        given(paypalPaymentInfo.getPayerId()).willReturn("PUMQ7JGXYMHV4");
        given(paymentTransactionModel.getPaymentProvider()).willReturn(TgtpaymentConstants.PAY_PAL_PAYMENT_TYPE);
        given(paymentTransactionModel.getInfo()).willReturn(paypalPaymentInfo);

        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(paymentTransactionEntryModel.getAmount()).willReturn(BigDecimal.valueOf(29.99d));
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(
                TransactionStatus.ACCEPTED.toString());
        given(paymentTransactionEntryModel.getCreationtime()).willReturn(now);
        given(paymentTransactionEntryModel.getReceiptNo()).willReturn(RECEIPT_NO);

        final TenderInfo tender = tlogPaymentEntryConverter.getTenderInfoFromPaymentEntry(paymentTransactionEntryModel,
                false);

        assertThat(tender.getType()).isEqualTo(TenderTypeEnum.PAYPAL);
        assertThat(tender.getAmount()).isEqualTo(BigDecimal.valueOf(29.99d));
        assertThat(tender.getPaypalPayerId()).isEqualTo("PUMQ7JGXYMHV4");
        assertThat(tender.getPaypalRRN()).isEqualTo(RECEIPT_NO);
        assertThat(tender.getProcessedTime()).isEqualTo(now);
        assertThat(tender.getApproved()).isEqualTo("Y");
    }

    @Test
    public void testGetTenderInfoFromPaymentEntryForAfterpay() {
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        final AfterpayPaymentInfoModel afterpayPaymentInfo = mock(AfterpayPaymentInfoModel.class);

        final Date now = new Date();
        given(afterpayPaymentInfo.getAfterpayOrderId()).willReturn("12345678");
        given(paymentTransactionModel.getPaymentProvider()).willReturn(TgtpaymentConstants.AFTERPAY_PAYMENT_TYPE);
        given(paymentTransactionModel.getInfo()).willReturn(afterpayPaymentInfo);

        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(paymentTransactionEntryModel.getAmount()).willReturn(BigDecimal.valueOf(29.99d));
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(
                TransactionStatus.ACCEPTED.toString());
        given(paymentTransactionEntryModel.getCreationtime()).willReturn(now);
        given(paymentTransactionEntryModel.getReceiptNo()).willReturn(RECEIPT_NO);

        final TenderInfo tender = tlogPaymentEntryConverter.getTenderInfoFromPaymentEntry(paymentTransactionEntryModel,
                false);

        assertThat(tender.getType()).isEqualTo(TenderTypeEnum.AFTERPAY);
        assertThat(tender.getAmount()).isEqualTo(BigDecimal.valueOf(29.99d));
        assertThat(tender.getAfterPayOrderId()).isEqualTo("12345678");
        assertThat(tender.getAfterPayRefundId()).isEqualTo("");
        assertThat(tender.getProcessedTime()).isEqualTo(now);
        assertThat(tender.getApproved()).isEqualTo("Y");
    }

    @Test
    public void testGetTenderInfoFromPaymentEntryForAfterpayRefund() {
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        final AfterpayPaymentInfoModel afterpayPaymentInfo = mock(AfterpayPaymentInfoModel.class);

        final Date now = new Date();
        given(afterpayPaymentInfo.getAfterpayOrderId()).willReturn("12345678");
        given(afterpayPaymentInfo.getAfterpayRefundId()).willReturn("87654321");
        given(paymentTransactionModel.getPaymentProvider()).willReturn(TgtpaymentConstants.AFTERPAY_PAYMENT_TYPE);
        given(paymentTransactionModel.getInfo()).willReturn(afterpayPaymentInfo);

        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(paymentTransactionEntryModel.getAmount()).willReturn(BigDecimal.valueOf(29.99d));
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(
                TransactionStatus.ACCEPTED.toString());
        given(paymentTransactionEntryModel.getCreationtime()).willReturn(now);
        given(paymentTransactionEntryModel.getReceiptNo()).willReturn(RECEIPT_NO);

        final TenderInfo tender = tlogPaymentEntryConverter.getTenderInfoFromPaymentEntry(paymentTransactionEntryModel,
                false);

        assertThat(tender.getType()).isEqualTo(TenderTypeEnum.AFTERPAY);
        assertThat(tender.getAmount()).isEqualTo(BigDecimal.valueOf(29.99d));
        assertThat(tender.getAfterPayOrderId()).isEqualTo("12345678");
        assertThat(tender.getAfterPayRefundId()).isEqualTo("87654321");
        assertThat(tender.getProcessedTime()).isEqualTo(now);
        assertThat(tender.getApproved()).isEqualTo("Y");
    }

    @Test
    public void testGetTenderInfoFromPaymentEntryForZipPayment() {
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        final ZippayPaymentInfoModel zipPaymentInfo = mock(ZippayPaymentInfoModel.class);

       final Date now = new Date();
        given(zipPaymentInfo.getReceiptNo()).willReturn("12345678");
        given(paymentTransactionModel.getPaymentProvider()).willReturn(TgtpaymentConstants.ZIPPAY_PAYMENT_TYPE);
        given(paymentTransactionModel.getInfo()).willReturn(zipPaymentInfo);

        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(paymentTransactionEntryModel.getAmount()).willReturn(BigDecimal.valueOf(29.99d));
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        given(paymentTransactionEntryModel.getCreationtime()).willReturn(now);
        given(paymentTransactionEntryModel.getReceiptNo()).willReturn("12345678");

        final TenderInfo tender = tlogPaymentEntryConverter.getTenderInfoFromPaymentEntry(paymentTransactionEntryModel,
                false);

        assertThat(tender.getType()).isEqualTo(TenderTypeEnum.ZIP);
        assertThat(tender.getAmount()).isEqualTo(BigDecimal.valueOf(29.99d));
        assertThat(tender.getOrderId()).isEqualTo("12345678");
        assertThat(tender.getRefundId()).isEqualTo("");
        assertThat(tender.getProcessedTime()).isEqualTo(now);
        assertThat(tender.getApproved()).isEqualTo("Y");
    }

    @Test
    public void testGetTenderInfoFromPaymentEntryForZipPaymentRefund() {
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        final ZippayPaymentInfoModel zipPaymentInfo = mock(ZippayPaymentInfoModel.class);

        final Date now = new Date();

        given(zipPaymentInfo.getReceiptNo()).willReturn("12345678");
        given(paymentTransactionModel.getPaymentProvider()).willReturn(TgtpaymentConstants.ZIPPAY_PAYMENT_TYPE);
        given(paymentTransactionModel.getInfo()).willReturn(zipPaymentInfo);

        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(paymentTransactionEntryModel.getAmount()).willReturn(BigDecimal.valueOf(29.99d));
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        given(paymentTransactionEntryModel.getCreationtime()).willReturn(now);
        given(paymentTransactionEntryModel.getReceiptNo()).willReturn(REFUND_ID);

        final TenderInfo tender = tlogPaymentEntryConverter.getTenderInfoFromPaymentEntry(paymentTransactionEntryModel,
            true);

        assertThat(tender.getType()).isEqualTo(TenderTypeEnum.ZIP);
        assertThat(tender.getAmount()).isEqualTo(BigDecimal.valueOf(-29.99d));
        assertThat(tender.getOrderId()).isEqualTo("12345678");
        assertThat(tender.getRefundId()).isEqualTo(REFUND_ID);
        assertThat(tender.getProcessedTime()).isEqualTo(now);
        assertThat(tender.getApproved()).isEqualTo("Y");
    }
    @Test
    public void testGetTenderInfoFromPaymentEntryForPayNow() {
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        final PaynowPaymentInfoModel paynowPaymentInfo = mock(PaynowPaymentInfoModel.class);
        final Date now = new Date();
        given(paymentTransactionModel.getPaymentProvider()).willReturn(TgtpaymentConstants.PAY_NOW_PAYMENT_TYPE);
        given(paymentTransactionModel.getInfo()).willReturn(paynowPaymentInfo);

        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(paymentTransactionEntryModel.getAmount()).willReturn(BigDecimal.valueOf(29.99d));
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(
                TransactionStatus.ACCEPTED.toString());
        given(paymentTransactionEntryModel.getCreationtime()).willReturn(now);
        given(paymentTransactionEntryModel.getReceiptNo()).willReturn(RECEIPT_NO);

        final TenderInfo tender = tlogPaymentEntryConverter.getTenderInfoFromPaymentEntry(paymentTransactionEntryModel,
                false);

        assertThat(tender.getType()).isEqualTo(TenderTypeEnum.CASH);
        assertThat(tender.getAmount()).isEqualTo(BigDecimal.valueOf(29.99d));
        assertThat(tender.getProcessedTime()).isEqualTo(null);
        assertThat(tender.getApproved()).isEqualTo("Y");
    }

    @Test
    public void testGetTenderInfoFromPaymentEntryForIpgCreditCard() {
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);

        final Date now = new Date();
        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(paymentTransactionModel.getPaymentProvider()).willReturn("ipg");
        given(paymentTransactionEntryModel.getAmount()).willReturn(BigDecimal.valueOf(29.99d));
        given(paymentTransactionEntryModel.getReceiptNo()).willReturn(RECEIPT_NO);

        final IpgCreditCardPaymentInfoModel creditCardInfoModel = new IpgCreditCardPaymentInfoModel();
        creditCardInfoModel.setNumber("5123********346");
        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(
                creditCardInfoModel);
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(
                TransactionStatus.ACCEPTED.toString());
        given(paymentTransactionEntryModel.getCreationtime()).willReturn(now);

        final TenderInfo tender = tlogPaymentEntryConverter.getTenderInfoFromPaymentEntry(paymentTransactionEntryModel,
                false);

        assertThat(tender.getType()).isEqualTo(TenderTypeEnum.EFT);
        assertThat(tender.getAmount()).isEqualTo(BigDecimal.valueOf(29.99d));
        assertThat(tender.getCardNumber()).isEqualTo("512300000000346");
        assertThat(tender.getCreditCardRRN()).isEqualTo(RECEIPT_NO);
        assertThat(tender.getProcessedTime()).isEqualTo(now);
        assertThat(tender.getApproved()).isEqualTo("Y");
    }

    @Test
    public void testGetTenderInfoFromPaymentEntryForIpgGiftCard() {
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);

        final Date now = new Date();
        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(paymentTransactionModel.getPaymentProvider()).willReturn("ipg");
        given(paymentTransactionEntryModel.getAmount()).willReturn(BigDecimal.valueOf(29.99d));
        given(paymentTransactionEntryModel.getReceiptNo()).willReturn(RECEIPT_NO);

        final IpgGiftCardPaymentInfoModel giftCardInfoModel = new IpgGiftCardPaymentInfoModel();
        giftCardInfoModel.setMaskedNumber("5123********346");
        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(
                giftCardInfoModel);
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(
                TransactionStatus.ACCEPTED.toString());
        given(paymentTransactionEntryModel.getCreationtime()).willReturn(now);

        final TenderInfo tender = tlogPaymentEntryConverter.getTenderInfoFromPaymentEntry(paymentTransactionEntryModel,
                false);

        assertThat(tender.getType()).isEqualTo(TenderTypeEnum.EFT);
        assertThat(tender.getAmount()).isEqualTo(BigDecimal.valueOf(29.99d));
        assertThat(tender.getCardNumber()).isEqualTo("512300000000346");
        assertThat(tender.getCreditCardRRN()).isEqualTo(RECEIPT_NO);
        assertThat(tender.getProcessedTime()).isEqualTo(now);
        assertThat(tender.getApproved()).isEqualTo("Y");
    }

    @Test
    public void testGetTenderInfoFromPaymentEntryForIpgManualRefund() {
        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        given(paymentTransactionModel.getPaymentProvider()).willReturn("ipg");

        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(paymentTransactionEntryModel.getAmount()).willReturn(BigDecimal.valueOf(29.99d));
        given(paymentTransactionEntryModel.getReceiptNo()).willReturn(RECEIPT_NO);
        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(null);
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(
                TransactionStatus.ACCEPTED.toString());

        final Date now = new Date();
        given(paymentTransactionEntryModel.getCreationtime()).willReturn(now);

        final TenderInfo tender = tlogPaymentEntryConverter.getTenderInfoFromPaymentEntry(paymentTransactionEntryModel,
                false);

        assertThat(tender.getType()).isEqualTo(TenderTypeEnum.CASH);
        assertThat(tender.getAmount()).isEqualTo(BigDecimal.valueOf(29.99d));
        assertThat(tender.getProcessedTime()).isEqualTo(null);
        assertThat(tender.getApproved()).isEqualTo("Y");
    }

    @Test
    public void testGetTenderInfoFromPaymentEntryForIpgCreditCardGetTimeNotNull() {
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);

        final Date now = new Date();
        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(paymentTransactionModel.getPaymentProvider()).willReturn("ipg");
        given(paymentTransactionEntryModel.getAmount()).willReturn(BigDecimal.valueOf(29.99d));
        given(paymentTransactionEntryModel.getReceiptNo()).willReturn(RECEIPT_NO);

        final IpgCreditCardPaymentInfoModel creditCardInfoModel = new IpgCreditCardPaymentInfoModel();
        creditCardInfoModel.setNumber("5123********346");
        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(
                creditCardInfoModel);
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(
                TransactionStatus.ACCEPTED.toString());
        given(paymentTransactionEntryModel.getCreationtime()).willReturn(null);
        given(paymentTransactionEntryModel.getTime()).willReturn(now);

        final TenderInfo tender = tlogPaymentEntryConverter.getTenderInfoFromPaymentEntry(paymentTransactionEntryModel,
                false);

        assertThat(tender.getType()).isEqualTo(TenderTypeEnum.EFT);
        assertThat(tender.getAmount()).isEqualTo(BigDecimal.valueOf(29.99d));
        assertThat(tender.getCardNumber()).isEqualTo("512300000000346");
        assertThat(tender.getCreditCardRRN()).isEqualTo(RECEIPT_NO);
        assertThat(tender.getProcessedTime()).isEqualTo(now);
        assertThat(tender.getApproved()).isEqualTo("Y");
    }

    @Test
    public void testGetTenderInfoFromPaymentEntryForIpgManualRefundWithSetTransactionTime() {
        final PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
        given(paymentTransactionModel.getPaymentProvider()).willReturn("ipg");

        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransactionModel);
        given(paymentTransactionEntryModel.getAmount()).willReturn(BigDecimal.valueOf(29.99d));
        given(paymentTransactionEntryModel.getReceiptNo()).willReturn(RECEIPT_NO);
        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(null);
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(
                TransactionStatus.ACCEPTED.toString());

        final Date now = new Date();
        given(paymentTransactionEntryModel.getCreationtime()).willReturn(null);
        given(paymentTransactionEntryModel.getTime()).willReturn(now);

        final TenderInfo tender = tlogPaymentEntryConverter.getTenderInfoFromPaymentEntry(paymentTransactionEntryModel,
                false);

        assertThat(tender.getType()).isEqualTo(TenderTypeEnum.CASH);
        assertThat(tender.getAmount()).isEqualTo(BigDecimal.valueOf(29.99d));
        assertThat(tender.getProcessedTime()).isEqualTo(null);
        assertThat(tender.getApproved()).isEqualTo("Y");
    }

}
