package au.com.target.tgtsale.tlog.converter.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.when;
import static org.mockito.Mockito.mock;
import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.model.TMDiscountPromotionModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtsale.tlog.data.ItemInfo;
import au.com.target.tgtsale.tlog.data.ReasonTypeEnum;
import au.com.target.tgtsale.tlog.data.Transaction;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.basecommerce.enums.RefundReason;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordModel;
import de.hybris.platform.returns.model.OrderEntryReturnRecordEntryModel;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TlogCancelConverterImplTest {



    private static final String PROMOTION_CODE = "12345";
    private static final String PRODUCT_P12345 = "P12345";
    private static final String PRODUCT_P12346 = "P12346";

    @Mock
    private OrderModel mockOrder;
    @Mock
    private FlybuysDiscountModel mockFlybuysDiscount;

    private final TlogCancelConverterImpl tlogCancelConverter = new TlogCancelConverterImpl();

    @Test
    public void testGetFlyBuysRedeemCodeWithNullOrder() {
        final String output = tlogCancelConverter.getFlybuysRedeemCode(null);
       assertThat(output).isNullOrEmpty();
    }

    @Test
    public void testGetFlyBuysRedeemCodeWithOrderAndEmptyDiscounts() {
        final String output = tlogCancelConverter.getFlybuysRedeemCode(mockOrder);
        assertThat(output).isNullOrEmpty();
    }

    @Test
    public void testGetFlyBuysRedeemCodeWithOrderAndDiscounts() {
        final String redeemCode = "AXD007";
        final ArrayList<DiscountModel> discountList = new ArrayList<>();
        discountList.add(mockFlybuysDiscount);
        given(mockOrder.getDiscounts()).willReturn(discountList);
        given(mockFlybuysDiscount.getRedeemCode()).willReturn(redeemCode);
        final String output = tlogCancelConverter.getFlybuysRedeemCode(mockOrder);
        assertThat(output).isEqualTo(redeemCode);
    }

    @Test
    public void testAddFlybuysDiscountInfo() {
        final String flybuysCardNumber = "1234567890123456";
        final OrderModel order = new OrderModel();
        final String redeemCode = "AXD007";
        final ArrayList<DiscountModel> discountList = new ArrayList<>();
        final FlybuysDiscountModel flybuysDiscount = new FlybuysDiscountModel();
        final OrderModificationRecordEntryModel orderModificationRecordEntry = new OrderModificationRecordEntryModel();
        final OrderModificationRecordModel orderModificationRecodModel = new OrderModificationRecordModel();
        flybuysDiscount.setRedeemCode(redeemCode);
        discountList.add(flybuysDiscount);
        final Transaction transaction = new Transaction();
        orderModificationRecordEntry.setFlybuysPointsRefundConfirmationCode("CONF");
        orderModificationRecordEntry.setRefundedAmount(Double.valueOf(10.00));
        orderModificationRecordEntry.setRefundedFlybuysAmount(Double.valueOf(10.00));
        orderModificationRecordEntry.setRefundedFlybuysPoints(Integer.valueOf(2000));
        order.setDiscounts(discountList);
        orderModificationRecodModel.setOrder(order);
        orderModificationRecordEntry.setModificationRecord(orderModificationRecodModel);
        tlogCancelConverter.addFlybuysDiscountInfo(transaction, orderModificationRecordEntry, flybuysCardNumber);
        assertThat(transaction.getDiscounts()).isNotEmpty().hasSize(1);

        assertThat(transaction.getDiscounts().get(0).getRedeemCode()).isEqualTo(redeemCode);
        assertThat(transaction.getDiscounts().get(0).getConfirmationCode()).isEqualTo("CONF");

    }

    @Test
    public void testAddFlybuysDiscountInfoWithNullPoints() {
        final String flybuysCardNumber = "1234567890123456";
        final OrderModel order = new OrderModel();
        final String redeemCode = "AXD007";
        final ArrayList<DiscountModel> discountList = new ArrayList<>();
        final FlybuysDiscountModel flybuysDiscount = new FlybuysDiscountModel();
        final OrderModificationRecordEntryModel orderModificationRecordEntry = new OrderModificationRecordEntryModel();
        final OrderModificationRecordModel orderModificationRecodModel = new OrderModificationRecordModel();
        flybuysDiscount.setRedeemCode(redeemCode);
        discountList.add(flybuysDiscount);
        final Transaction transaction = new Transaction();
        orderModificationRecordEntry.setFlybuysPointsRefundConfirmationCode("CONF");
        orderModificationRecordEntry.setRefundedFlybuysAmount(Double.valueOf(10.00));
        orderModificationRecordEntry.setRefundedFlybuysPoints(null);
        order.setDiscounts(discountList);
        orderModificationRecodModel.setOrder(order);
        orderModificationRecordEntry.setModificationRecord(orderModificationRecodModel);
        tlogCancelConverter.addFlybuysDiscountInfo(transaction, orderModificationRecordEntry, flybuysCardNumber);
        assertThat(transaction.getDiscounts()).isNull();

    }

    @Test
    public void testAddFlybuysDiscountInfoWithNullAmount() {
        final String flybuysCardNumber = "1234567890123456";
        final OrderModel order = new OrderModel();
        final String redeemCode = "AXD007";
        final ArrayList<DiscountModel> discountList = new ArrayList<>();
        final FlybuysDiscountModel flybuysDiscount = new FlybuysDiscountModel();
        final OrderModificationRecordEntryModel orderModificationRecordEntry = new OrderModificationRecordEntryModel();
        final OrderModificationRecordModel orderModificationRecodModel = new OrderModificationRecordModel();
        flybuysDiscount.setRedeemCode(redeemCode);
        discountList.add(flybuysDiscount);
        final Transaction transaction = new Transaction();
        orderModificationRecordEntry.setFlybuysPointsRefundConfirmationCode("CONF");
        orderModificationRecordEntry.setRefundedFlybuysAmount(null);
        orderModificationRecordEntry.setRefundedFlybuysPoints(null);
        order.setDiscounts(discountList);
        orderModificationRecodModel.setOrder(order);
        orderModificationRecordEntry.setModificationRecord(orderModificationRecodModel);
        tlogCancelConverter.addFlybuysDiscountInfo(transaction, orderModificationRecordEntry, flybuysCardNumber);
        assertThat(transaction.getDiscounts()).isNull();

    }


    @Test
    public void testGetItemListForModifiedEntriesTenderAmountEqToEntries() {
        final List<OrderEntryModificationRecordEntryModel> orderEntries = new ArrayList<>();
        final TargetProductModel targetProduct = new TargetProductModel();
        targetProduct.setCode(PROMOTION_CODE);
        final OrderEntryReturnRecordEntryModel modRecordEntry1 = new OrderEntryReturnRecordEntryModel();
        modRecordEntry1.setReturnedQuantity(Long.valueOf(2L));
        final OrderEntryModel entry1 = new OrderEntryModel();
        entry1.setBasePrice(Double.valueOf(10.0d));
        entry1.setQuantity(Long.valueOf(2L));
        entry1.setProduct(targetProduct);
        modRecordEntry1.setOrderEntry(entry1);
        final OrderEntryReturnRecordEntryModel modRecordEntry2 = new OrderEntryReturnRecordEntryModel();
        modRecordEntry2.setReturnedQuantity(Long.valueOf(1L));
        final OrderEntryModel entry2 = new OrderEntryModel();
        entry2.setBasePrice(Double.valueOf(10.0d));
        entry2.setQuantity(Long.valueOf(1L));
        entry2.setProduct(targetProduct);
        modRecordEntry2.setOrderEntry(entry2);

        orderEntries.add(modRecordEntry1);
        orderEntries.add(modRecordEntry2);

        final List<ItemInfo> items = tlogCancelConverter.getItemListForModifiedEntries(orderEntries,
                BigDecimal.valueOf(20.0d)
                        .negate(),
                false);

        assertThat(items).isNotNull().isNotEmpty().hasSize(2);

        for (final ItemInfo item : items) {
            assertThat(item.getPrice()).isNotNull().isEqualByComparingTo(BigDecimal.valueOf(10.0d).negate());
        }
    }

    @Test
    public void testGetItemListForModifiedEntriesTenderAmountOfZero() {
        final List<OrderEntryModificationRecordEntryModel> orderEntries = new ArrayList<>();
        final TargetProductModel targetProduct = new TargetProductModel();
        targetProduct.setCode(PROMOTION_CODE);
        final OrderEntryReturnRecordEntryModel modRecordEntry1 = new OrderEntryReturnRecordEntryModel();
        modRecordEntry1.setReturnedQuantity(Long.valueOf(2L));
        final OrderEntryModel entry1 = new OrderEntryModel();
        entry1.setBasePrice(Double.valueOf(10.0d));
        entry1.setQuantity(Long.valueOf(2L));
        entry1.setProduct(targetProduct);
        modRecordEntry1.setOrderEntry(entry1);
        final OrderEntryReturnRecordEntryModel modRecordEntry2 = new OrderEntryReturnRecordEntryModel();
        modRecordEntry2.setReturnedQuantity(Long.valueOf(1L));
        final OrderEntryModel entry2 = new OrderEntryModel();
        entry2.setBasePrice(Double.valueOf(10.0d));
        entry2.setQuantity(Long.valueOf(1L));
        entry2.setProduct(targetProduct);
        modRecordEntry2.setOrderEntry(entry2);

        orderEntries.add(modRecordEntry1);
        orderEntries.add(modRecordEntry2);

        final List<ItemInfo> items = tlogCancelConverter.getItemListForModifiedEntries(orderEntries,
                BigDecimal.valueOf(0.0d)
                        .negate(),
                false);

        assertThat(items).isNotNull().isNotEmpty().hasSize(2);

        for (final ItemInfo item : items) {
            assertThat(item.getPrice()).isNotNull().isEqualByComparingTo(BigDecimal.valueOf(0.0d).negate());
        }
    }

    @Test
    public void testGetItemListForModifiedEntriesWithShippingDiscount() {
        final List<OrderEntryModificationRecordEntryModel> orderEntries = new ArrayList<>();
        final TargetProductModel targetProduct = new TargetProductModel();
        targetProduct.setCode(PROMOTION_CODE);
        final OrderEntryReturnRecordEntryModel modRecordEntry1 = new OrderEntryReturnRecordEntryModel();
        modRecordEntry1.setReturnedQuantity(Long.valueOf(2L));
        final OrderEntryModel entry1 = new OrderEntryModel();
        entry1.setBasePrice(Double.valueOf(10.0d));
        entry1.setQuantity(Long.valueOf(2L));
        entry1.setProduct(targetProduct);
        modRecordEntry1.setOrderEntry(entry1);
        final OrderEntryReturnRecordEntryModel modRecordEntry2 = new OrderEntryReturnRecordEntryModel();
        modRecordEntry2.setReturnedQuantity(Long.valueOf(1L));
        final OrderEntryModel entry2 = new OrderEntryModel();
        entry2.setBasePrice(Double.valueOf(10.0d));
        entry2.setQuantity(Long.valueOf(1L));
        entry2.setProduct(targetProduct);
        modRecordEntry2.setOrderEntry(entry2);

        orderEntries.add(modRecordEntry1);
        orderEntries.add(modRecordEntry2);

        final List<ItemInfo> items = tlogCancelConverter.getItemListForModifiedEntries(orderEntries,
                BigDecimal.valueOf(20.0d)
                        .negate(),
                false);

        assertThat(items).isNotNull().isNotEmpty().hasSize(2);

        for (final ItemInfo item : items) {
            assertThat(item.getPrice()).isNotNull().isEqualByComparingTo(BigDecimal.valueOf(10.0d).negate());
        }
    }

    @Test
    public void testGetItemListForModifiedEntriesTenderAmountLTToEntries() {
        final List<OrderEntryModificationRecordEntryModel> orderEntries = new ArrayList<>();
        final TargetProductModel targetProduct = new TargetProductModel();
        targetProduct.setCode(PRODUCT_P12345);
        final OrderEntryReturnRecordEntryModel modRecordEntry1 = new OrderEntryReturnRecordEntryModel();
        modRecordEntry1.setReturnedQuantity(Long.valueOf(2L));
        final OrderEntryModel entry1 = new OrderEntryModel();
        entry1.setBasePrice(Double.valueOf(10.0d));
        entry1.setQuantity(Long.valueOf(2L));
        final TargetProductModel targetProduct1 = new TargetProductModel();
        targetProduct1.setCode(PRODUCT_P12346);
        entry1.setProduct(targetProduct);
        modRecordEntry1.setOrderEntry(entry1);
        final OrderEntryReturnRecordEntryModel modRecordEntry2 = new OrderEntryReturnRecordEntryModel();
        modRecordEntry2.setReturnedQuantity(Long.valueOf(1L));
        final OrderEntryModel entry2 = new OrderEntryModel();
        entry2.setBasePrice(Double.valueOf(10.0d));
        entry2.setQuantity(Long.valueOf(1L));
        entry2.setProduct(targetProduct);
        modRecordEntry2.setOrderEntry(entry2);

        orderEntries.add(modRecordEntry1);
        orderEntries.add(modRecordEntry2);

        final List<ItemInfo> items = tlogCancelConverter.getItemListForModifiedEntries(orderEntries,
                BigDecimal.valueOf(15.0d)
                        .negate(),
                false);

        assertThat(items).isNotNull().isNotEmpty().hasSize(2);

        for (final ItemInfo item : items) {
            if (item.getQuantity() == Long.valueOf(2L)) {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(5d).negate());
            }
            else {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(5d).negate());
            }
        }
    }

    @Test
    public void testGetItemListForModifiedEntriesTenderAmountGTToEntries() {
        final List<OrderEntryModificationRecordEntryModel> orderEntries = new ArrayList<>();
        final TargetProductModel targetProduct = new TargetProductModel();
        targetProduct.setCode(PRODUCT_P12345);
        final OrderEntryReturnRecordEntryModel modRecordEntry1 = new OrderEntryReturnRecordEntryModel();
        modRecordEntry1.setReturnedQuantity(Long.valueOf(2L));
        final OrderEntryModel entry1 = new OrderEntryModel();
        entry1.setBasePrice(Double.valueOf(10.0d));
        entry1.setQuantity(Long.valueOf(2L));
        final TargetProductModel targetProduct1 = new TargetProductModel();
        targetProduct1.setCode(PRODUCT_P12346);
        entry1.setProduct(targetProduct);
        modRecordEntry1.setOrderEntry(entry1);
        final OrderEntryReturnRecordEntryModel modRecordEntry2 = new OrderEntryReturnRecordEntryModel();
        modRecordEntry2.setReturnedQuantity(Long.valueOf(1L));
        final OrderEntryModel entry2 = new OrderEntryModel();
        entry2.setBasePrice(Double.valueOf(10.0d));
        entry2.setQuantity(Long.valueOf(1L));
        entry2.setProduct(targetProduct);
        modRecordEntry2.setOrderEntry(entry2);

        orderEntries.add(modRecordEntry1);
        orderEntries.add(modRecordEntry2);

        final List<ItemInfo> items = tlogCancelConverter.getItemListForModifiedEntries(orderEntries,
                BigDecimal.valueOf(30.0d)
                        .negate(),
                false);

        assertThat(items).isNotNull().isNotEmpty().hasSize(2);

        for (final ItemInfo item : items) {
            if (item.getQuantity() == Long.valueOf(2L)) {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(10d).negate());
            }
            else {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(10d).negate());
            }
        }
    }

    @Test
    public void testGetItemListForModifiedEntriesDecimalTenderAndBasePrices() {
        final List<OrderEntryModificationRecordEntryModel> orderEntries = new ArrayList<>();
        final TargetProductModel targetProduct = new TargetProductModel();
        targetProduct.setCode(PRODUCT_P12345);
        final OrderEntryReturnRecordEntryModel modRecordEntry1 = new OrderEntryReturnRecordEntryModel();
        modRecordEntry1.setReturnedQuantity(Long.valueOf(2L));
        final OrderEntryModel entry1 = new OrderEntryModel();
        entry1.setBasePrice(Double.valueOf(10.03d));
        entry1.setQuantity(Long.valueOf(2L));
        final TargetProductModel targetProduct1 = new TargetProductModel();
        targetProduct1.setCode(PRODUCT_P12346);
        entry1.setProduct(targetProduct);
        modRecordEntry1.setOrderEntry(entry1);
        final OrderEntryReturnRecordEntryModel modRecordEntry2 = new OrderEntryReturnRecordEntryModel();
        modRecordEntry2.setReturnedQuantity(Long.valueOf(1L));
        final OrderEntryModel entry2 = new OrderEntryModel();
        entry2.setBasePrice(Double.valueOf(5.0d));
        entry2.setQuantity(Long.valueOf(1L));
        entry2.setProduct(targetProduct);
        modRecordEntry2.setOrderEntry(entry2);

        orderEntries.add(modRecordEntry1);
        orderEntries.add(modRecordEntry2);

        final List<ItemInfo> items = tlogCancelConverter.getItemListForModifiedEntries(orderEntries,
                BigDecimal.valueOf(14.03d)
                        .negate(),
                false);

        assertThat(items).isNotNull().isNotEmpty().hasSize(2);

        for (final ItemInfo item : items) {
            if (item.getQuantity() == Long.valueOf(2L)) {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(5.62d).negate());
            }
            else {
                assertThat(item.getPrice()).isNotNull()
                        .isEqualByComparingTo(BigDecimal.valueOf(2.79).negate());
            }
        }
    }

    @Test
    public void testGetItemInfoFromRefundEntry() {
        final OrderEntryModel orderEntryModel = mock(OrderEntryModel.class);
        final TMDiscountPromotionModel tmd = mock(TMDiscountPromotionModel.class);

        final TargetProductModel targetProduct = new TargetProductModel();
        targetProduct.setCode(PROMOTION_CODE);
        when(orderEntryModel.getProduct()).thenReturn(targetProduct);
        when(orderEntryModel.getQuantity()).thenReturn(Long.valueOf(2L));
        when(orderEntryModel.getBasePrice()).thenReturn(Double.valueOf(23.45d));
        when(tmd.getPercentageDiscount()).thenReturn(Double.valueOf(5.0d));


        final ItemInfo refund = tlogCancelConverter
                .getItemInfoFromOrderEntry(orderEntryModel, orderEntryModel.getQuantity());

        assertThat(PROMOTION_CODE).as("Id ").isEqualTo(refund.getId());
        assertThat(Long.valueOf(2L)).as("Quantity  ").isEqualTo(refund.getQuantity());
        assertThat(BigDecimal.valueOf(23.45d).negate()).as("Price  ").isEqualTo(refund.getPrice());
        assertThat(BigDecimal.valueOf(23.45d)).as("File price  ").isEqualTo(refund.getFilePrice());
        assertThat(refund.getItemDiscount()).isNull();
    }

	@Test
	public void testTlogReasonForPriceMatchRefundEntry() {
		final OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntry = new OrderEntryReturnRecordEntryModel();
		orderEntryReturnRecordEntry.setRefundReason(RefundReason.PRICEMATCH.getCode());
		ReasonTypeEnum reason = tlogCancelConverter.getRefundReasonForEntry(orderEntryReturnRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.PRICE_ISSUE);
	}
	@Test
	public void testTlogReasonForMissedLinkDealRefundEntry() {
		final OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntry = new OrderEntryReturnRecordEntryModel();
		orderEntryReturnRecordEntry.setRefundReason(RefundReason.MISSEDLINKDEAL.getCode());
		ReasonTypeEnum reason = tlogCancelConverter.getRefundReasonForEntry(orderEntryReturnRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.PRICE_ISSUE);
	}

	@Test
	public void testTlogReasonForSiteErrorRefundEntry() {
		final OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntry = new OrderEntryReturnRecordEntryModel();
		orderEntryReturnRecordEntry.setRefundReason(RefundReason.SITEERROR.getCode());
		ReasonTypeEnum reason = tlogCancelConverter.getRefundReasonForEntry(orderEntryReturnRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.PRICE_ISSUE);
	}

	@Test
	public void testTlogReasonForChangeOfMindReturnRefundEntry() {
		final OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntry = new OrderEntryReturnRecordEntryModel();
		orderEntryReturnRecordEntry.setRefundReason(RefundReason.CHANGEOFMINDRETURN.getCode());
		ReasonTypeEnum reason = tlogCancelConverter.getRefundReasonForEntry(orderEntryReturnRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.NOT_FAULTY);
	}
	@Test
	public void testTlogReasonForCustomerIssueRefundEntry() {
		final OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntry = new OrderEntryReturnRecordEntryModel();
		orderEntryReturnRecordEntry.setRefundReason(RefundReason.CUSTOMERMISUSE.getCode());
		ReasonTypeEnum reason = tlogCancelConverter.getRefundReasonForEntry(orderEntryReturnRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.NOT_FAULTY);
	}

	@Test
	public void testTlogReasonForGoddWillRefundEntry() {
		final OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntry = new OrderEntryReturnRecordEntryModel();
		orderEntryReturnRecordEntry.setRefundReason(RefundReason.GOODWILL.getCode());
		ReasonTypeEnum reason = tlogCancelConverter.getRefundReasonForEntry(orderEntryReturnRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.NOT_FAULTY);
	}

	@Test
	public void testTlogReasonForMisPickWrongItemDeliveredRefundEntry() {
		final OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntry = new OrderEntryReturnRecordEntryModel();
		orderEntryReturnRecordEntry.setRefundReason(RefundReason.MISPICKWRONGITEMDELIVERED.getCode());
		ReasonTypeEnum reason = tlogCancelConverter.getRefundReasonForEntry(orderEntryReturnRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.NOT_FAULTY);
	}

	@Test
	public void testTlogReasonForLateDeliveryRefundEntry() {
		final OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntry = new OrderEntryReturnRecordEntryModel();
		orderEntryReturnRecordEntry.setRefundReason(RefundReason.LATEDELIVERY.getCode());
		ReasonTypeEnum reason = tlogCancelConverter.getRefundReasonForEntry(orderEntryReturnRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.NOT_FAULTY);
	}

	@Test
	public void testTlogReasonForMisPickItemMissingRefundEntry() {
		final OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntry = new OrderEntryReturnRecordEntryModel();
		orderEntryReturnRecordEntry.setRefundReason(RefundReason.MISPICKITEMMISSING.getCode());
		ReasonTypeEnum reason = tlogCancelConverter.getRefundReasonForEntry(orderEntryReturnRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.NOT_FAULTY);
	}

	@Test
	public void testTlogReasonForWrongDescriptionRefundEntry() {
		final OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntry = new OrderEntryReturnRecordEntryModel();
		orderEntryReturnRecordEntry.setRefundReason(RefundReason.WRONGDESCRIPTION.getCode());
		ReasonTypeEnum reason = tlogCancelConverter.getRefundReasonForEntry(orderEntryReturnRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.NOT_FAULTY);
	}

	@Test
	public void testTlogReasonForMissingPartsRefundEntry() {
		final OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntry = new OrderEntryReturnRecordEntryModel();
		orderEntryReturnRecordEntry.setRefundReason(RefundReason.MISSINGPARTS.getCode());
		ReasonTypeEnum reason = tlogCancelConverter.getRefundReasonForEntry(orderEntryReturnRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.NOT_FAULTY);

	}

	@Test
	public void testTlogReasonForLostInTransitRefundEntry() {
		final OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntry = new OrderEntryReturnRecordEntryModel();
		orderEntryReturnRecordEntry.setRefundReason(RefundReason.LOSTINTRANSIT.getCode());
		ReasonTypeEnum reason = tlogCancelConverter.getRefundReasonForEntry(orderEntryReturnRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.NOT_FAULTY);
	}

	@Test
	public void testTlogReasonForManufacturingFaultRefundEntry() {
		final OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntry = new OrderEntryReturnRecordEntryModel();
		orderEntryReturnRecordEntry.setRefundReason(RefundReason.MANUFACTURINGFAULT.getCode());
		ReasonTypeEnum reason = tlogCancelConverter.getRefundReasonForEntry(orderEntryReturnRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.FAULTY);
	}

	@Test
	public void testTlogReasonForRepairRefundEntry() {
		final OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntry = new OrderEntryReturnRecordEntryModel();
		orderEntryReturnRecordEntry.setRefundReason(RefundReason.REPAIR.getCode());
		ReasonTypeEnum reason = tlogCancelConverter.getRefundReasonForEntry(orderEntryReturnRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.FAULTY);
	}

	@Test
	public void testTlogReasonForRecallProductRefundEntry() {
		final OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntry = new OrderEntryReturnRecordEntryModel();
		orderEntryReturnRecordEntry.setRefundReason(RefundReason.RECALLPRODUCT.getCode());
		ReasonTypeEnum reason = tlogCancelConverter.getRefundReasonForEntry(orderEntryReturnRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.FAULTY);
	}

	@Test
	public void testTlogReasonForDamagedInTransitRefundEntry() {
		final OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntry = new OrderEntryReturnRecordEntryModel();
		orderEntryReturnRecordEntry.setRefundReason(RefundReason.DAMAGEDINTRANSIT.getCode());
		ReasonTypeEnum reason = tlogCancelConverter.getRefundReasonForEntry(orderEntryReturnRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.FAULTY);
	}

	@Test
	public void testTlogReasonForEmptyRefundEntry() {
		final OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntry = new OrderEntryReturnRecordEntryModel();
		orderEntryReturnRecordEntry.setRefundReason("");
		ReasonTypeEnum reason = tlogCancelConverter.getRefundReasonForEntry(orderEntryReturnRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.FAULTY);

	}

	@Test
	public void testTlogReasonForFraudCheckRejectedCancelEntry() {
		final OrderCancelRecordEntryModel orderCancelRecordEntry = new OrderCancelRecordEntryModel();
		orderCancelRecordEntry.setCancelReason(CancelReason.FRAUDCHECKREJECTED);
		ReasonTypeEnum reason = tlogCancelConverter.getCancelReasonForEntry(orderCancelRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.PRICE_ISSUE);
	}

	@Test
	public void testTlogReasonForCustomerRequestCancelEntry() {
		final OrderCancelRecordEntryModel orderCancelRecordEntry = new OrderCancelRecordEntryModel();
		orderCancelRecordEntry.setCancelReason(CancelReason.CUSTOMERREQUEST);
		ReasonTypeEnum reason = tlogCancelConverter.getCancelReasonForEntry(orderCancelRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.NOT_FAULTY);
	}

	@Test
	public void testTlogReasonForOtherCancelEntry() {
		final OrderCancelRecordEntryModel orderCancelRecordEntry = new OrderCancelRecordEntryModel();
		orderCancelRecordEntry.setCancelReason(CancelReason.OTHER);
		ReasonTypeEnum reason = tlogCancelConverter.getCancelReasonForEntry(orderCancelRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.NOT_FAULTY);
	}

	@Test
	public void testTlogReasonForLateDeliveryCancelEntry() {
		final OrderCancelRecordEntryModel orderCancelRecordEntry = new OrderCancelRecordEntryModel();
		orderCancelRecordEntry.setCancelReason(CancelReason.LATEDELIVERY);
		ReasonTypeEnum reason = tlogCancelConverter.getCancelReasonForEntry(orderCancelRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.NOT_FAULTY);
	}

	@Test
	public void testTlogReasonForWarehouseCancelEntry() {
		final OrderCancelRecordEntryModel orderCancelRecordEntry = new OrderCancelRecordEntryModel();
		orderCancelRecordEntry.setCancelReason(CancelReason.WAREHOUSE);
		ReasonTypeEnum reason = tlogCancelConverter.getCancelReasonForEntry(orderCancelRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.NOT_FAULTY);
	}

	@Test
	public void testTlogReasonForOutOfStockCancelEntry() {
		final OrderCancelRecordEntryModel orderCancelRecordEntry = new OrderCancelRecordEntryModel();
		orderCancelRecordEntry.setCancelReason(CancelReason.OUTOFSTOCK);
		ReasonTypeEnum reason = tlogCancelConverter.getCancelReasonForEntry(orderCancelRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.NOT_FAULTY);
	}

	@Test
	public void testTlogReasonForNACancelEntry() {
		final OrderCancelRecordEntryModel orderCancelRecordEntry = new OrderCancelRecordEntryModel();
		orderCancelRecordEntry.setCancelReason(CancelReason.NA);
		ReasonTypeEnum reason = tlogCancelConverter.getCancelReasonForEntry(orderCancelRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.NOT_FAULTY);
	}

	@Test
	public void testTlogReasonForRecallCancelEntry() {
		final OrderCancelRecordEntryModel orderCancelRecordEntry = new OrderCancelRecordEntryModel();
		orderCancelRecordEntry.setCancelReason(CancelReason.RECALL);
		ReasonTypeEnum reason = tlogCancelConverter.getCancelReasonForEntry(orderCancelRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.FAULTY);
	}

	@Test
	public void testTlogReasonForNullCancelEntry() {
		final OrderCancelRecordEntryModel orderCancelRecordEntry = new OrderCancelRecordEntryModel();
		orderCancelRecordEntry.setCancelReason(null);
		ReasonTypeEnum reason = tlogCancelConverter.getCancelReasonForEntry(orderCancelRecordEntry);
		assertThat(reason).isEqualTo(ReasonTypeEnum.FAULTY);
	}



}
