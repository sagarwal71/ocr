/**
 * 
 */
package au.com.target.tgtsale.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordercancel.OrderCancelRecordsHandler;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.ordercancel.TargetCancelService;
import au.com.target.tgtsale.exception.IllegalPosModificationException;
import au.com.target.tgtsale.integration.dto.TargetLaybyEntries;
import au.com.target.tgtsale.integration.dto.TargetLaybyOrder;
import au.com.target.tgtsale.integration.dto.TargetLaybyOrderEntry;


/**
 * Unit test for {@link PosLaybyUpdaterImpl} modifyLaybyOrderMethod
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("boxing")
public class TestWhenPosLaybyUpdaterModifyLaybyOrder {

    @Mock
    private TargetBusinessProcessService businessProcessService;

    @Mock
    private OrderCancelRecordsHandler orderCancelRecordsHandler;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetCancelService targetCancelService;


    @InjectMocks
    private final PosLaybyUpdaterImpl posLaybyUpdater = new PosLaybyUpdaterImpl();


    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Mock
    private TargetLaybyOrder posLaybyOrder;

    @Mock
    private TargetLaybyOrderEntry targetLaybyOrderEntry;

    @Mock
    private AbstractOrderEntryModel abstractOrderEntryModel;

    @Mock
    private ProductModel productModel;


    private final List<AbstractOrderEntryModel> hybrisEntries = new ArrayList<>();
    private final List<TargetLaybyOrderEntry> posEntries = new ArrayList<>();
    private final TargetLaybyEntries posEntriesObj = new TargetLaybyEntries();

    @Before
    public void setup() {

        // By default the businessProcessService will return process
        BDDMockito.given(businessProcessService.createProcess(Mockito.anyString(), Mockito.anyString())).willReturn(
                process);


        BDDMockito.given(order.getCode()).willReturn("order1");

        posEntries.add(targetLaybyOrderEntry);
        posEntriesObj.setEntry(posEntries);
        BDDMockito.given(posLaybyOrder.getEntries()).willReturn(posEntriesObj);

        hybrisEntries.add(abstractOrderEntryModel);
        BDDMockito.given(order.getEntries()).willReturn(hybrisEntries);
    }

    private void setupMatchingOrders() {

        BDDMockito.given(targetLaybyOrderEntry.getCode()).willReturn("Going to match");
        BDDMockito.given(abstractOrderEntryModel.getProduct()).willReturn(productModel);
        BDDMockito.given(productModel.getCode()).willReturn("Going to match");
        BDDMockito.given(targetLaybyOrderEntry.getQuantity()).willReturn(10L);
        BDDMockito.given(abstractOrderEntryModel.getQuantity()).willReturn(10L);
        BDDMockito.given(targetLaybyOrderEntry.getBasePrice()).willReturn(12.00D);
        BDDMockito.given(abstractOrderEntryModel.getBasePrice()).willReturn(12.00D);

        // Default to order in progress being partially cancelled
        BDDMockito.given(posLaybyOrder.isCancelled()).willReturn(Boolean.FALSE);
        BDDMockito.given(order.getStatus()).willReturn(OrderStatus.INPROGRESS);

        // Default matching delivery fees
        BDDMockito.given(order.getDeliveryCost()).willReturn(10.0);
        BDDMockito.given(posLaybyOrder.getShippingPrice()).willReturn(10.0);
    }

    @Test
    public void testGivenOrderCancelled() throws Exception {

        setupMatchingOrders();

        BDDMockito.given(posLaybyOrder.isCancelled()).willReturn(Boolean.TRUE);
        BDDMockito.given(order.getStatus()).willReturn(OrderStatus.INPROGRESS);

        final PosLaybyUpdaterImpl spy = Mockito.spy(posLaybyUpdater);
        Mockito.doNothing().when(spy).cancelTheseEntries(Mockito.eq(order), Mockito.anyList());

        spy.modifyLaybyOrder(posLaybyOrder, order);
        Mockito.verify(spy).cancelTheseEntries(Mockito.eq(order), Mockito.anyList());
    }

    @Test(expected = IllegalPosModificationException.class)
    public void testGivenOrderUnCancelled() throws Exception {

        setupMatchingOrders();

        BDDMockito.given(posLaybyOrder.isCancelled()).willReturn(Boolean.FALSE);
        BDDMockito.given(order.getStatus()).willReturn(OrderStatus.CANCELLED);
        posLaybyUpdater.modifyLaybyOrder(posLaybyOrder, order);
    }



    @Test
    public void testGivenUpdatedDelivery() throws Exception {

        setupMatchingOrders();

        BDDMockito.given(order.getDeliveryCost()).willReturn(55.0);
        BDDMockito.given(posLaybyOrder.getShippingPrice()).willReturn(10.0);

        posLaybyUpdater.modifyLaybyOrder(posLaybyOrder, order);
        Mockito.verify(modelService).save(order);
    }

    @Test
    public void testGivenProductMissingInPos() throws Exception {

        setupMatchingOrders();
        posEntries.clear();

        final PosLaybyUpdaterImpl spy = Mockito.spy(posLaybyUpdater);
        Mockito.doNothing().when(spy).cancelTheseEntries(Mockito.eq(order), Mockito.anyList());

        spy.modifyLaybyOrder(posLaybyOrder, order);
        Mockito.verify(spy).cancelTheseEntries(Mockito.eq(order), Mockito.anyList());
    }

    @Test(expected = IllegalPosModificationException.class)
    public void testGivenProductsDontMatch() throws Exception {

        setupMatchingOrders();

        BDDMockito.given(targetLaybyOrderEntry.getCode()).willReturn("Not going to match");
        BDDMockito.given(productModel.getCode()).willReturn("Going to fail");
        posLaybyUpdater.modifyLaybyOrder(posLaybyOrder, order);
    }

}
