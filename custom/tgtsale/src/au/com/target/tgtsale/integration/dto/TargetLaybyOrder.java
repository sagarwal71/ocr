/**
 * 
 */
package au.com.target.tgtsale.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author rsamuel3
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TargetLaybyOrder {
    @XmlElement
    private String orderCode;
    @XmlElement
    private double totalPrice;
    @XmlElement
    private double subtotalPrice;
    @XmlElement
    private double shippingPrice;
    @XmlElement
    private double outstandingAmt;
    @XmlElement
    private double totalDiscounts;
    @XmlElement
    private double layByFee;
    @XmlElement
    private double totalTax;
    @XmlElement
    private TargetLaybyEntries entries;
    @XmlElement
    private String modifiedDate;
    @XmlElement
    private String lastActionedDate;
    @XmlElement
    private boolean laybyModified;
    @XmlElement
    private boolean cancelled;

    /**
     * @return the lastActionedDate
     */
    public String getLastActionedDate() {
        return lastActionedDate;
    }

    /**
     * @param lastActionedDate
     *            the lastActionedDate to set
     */
    public void setLastActionedDate(final String lastActionedDate) {
        this.lastActionedDate = lastActionedDate;
    }



    /**
     * @return the isCancelled
     */
    public boolean isCancelled() {
        return cancelled;
    }

    /**
     * @param isCancelled
     *            the isCancelled to set
     */
    public void setCancelled(final boolean isCancelled) {
        this.cancelled = isCancelled;
    }

    /**
     * @return the isLaybyModified
     */
    public boolean isLaybyModified() {
        return laybyModified;
    }

    /**
     * @param isLaybyModified
     *            the isLaybyModified to set
     */
    public void setLaybyModified(final boolean isLaybyModified) {
        this.laybyModified = isLaybyModified;
    }

    /**
     * @return the modifiedDate
     */
    public String getModifiedDate() {
        return modifiedDate;
    }

    /**
     * @param modifiedDate
     *            the modifiedDate to set
     */
    public void setModifiedDate(final String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }



    /**
     * @return the totalPrice
     */
    public double getTotalPrice() {
        return totalPrice;
    }

    /**
     * @param totalPrice
     *            the totalPrice to set
     */
    public void setTotalPrice(final double totalPrice) {
        this.totalPrice = totalPrice;
    }


    /**
     * @return the orderCode
     */
    public String getOrderCode() {
        return orderCode;
    }

    /**
     * @param orderCode
     *            the orderCode to set
     */
    public void setOrderCode(final String orderCode) {
        this.orderCode = orderCode;
    }



    /**
     * @return the subtotalPrice
     */
    public double getSubtotalPrice() {
        return subtotalPrice;
    }

    /**
     * @param subtotalPrice
     *            the subtotalPrice to set
     */
    public void setSubtotalPrice(final double subtotalPrice) {
        this.subtotalPrice = subtotalPrice;
    }

    /**
     * @return the shippingPrice
     */
    public double getShippingPrice() {
        return shippingPrice;
    }

    /**
     * @param shippingPrice
     *            the shippingPrice to set
     */
    public void setShippingPrice(final double shippingPrice) {
        this.shippingPrice = shippingPrice;
    }

    /**
     * @return the outstandingAmt
     */
    public double getOutstandingAmt() {
        return outstandingAmt;
    }

    /**
     * @param outstandingAmt
     *            the outstandingAmt to set
     */
    public void setOutstandingAmt(final double outstandingAmt) {
        this.outstandingAmt = outstandingAmt;
    }

    /**
     * @return the totalDiscounts
     */
    public double getTotalDiscounts() {
        return totalDiscounts;
    }

    /**
     * @param totalDiscounts
     *            the totalDiscounts to set
     */
    public void setTotalDiscounts(final double totalDiscounts) {
        this.totalDiscounts = totalDiscounts;
    }


    /**
     * @return the layByFee
     */
    public double getLayByFee() {
        return layByFee;
    }

    /**
     * @param layByFee
     *            the layByFee to set
     */
    public void setLayByFee(final double layByFee) {
        this.layByFee = layByFee;
    }

    /**
     * @return the totalTax
     */
    public double getTotalTax() {
        return totalTax;
    }

    /**
     * @param totalTax
     *            the totalTax to set
     */
    public void setTotalTax(final double totalTax) {
        this.totalTax = totalTax;
    }

    /**
     * @return the entries
     */
    public TargetLaybyEntries getEntries() {
        return entries;
    }

    /**
     * @param entries
     *            the entries to set
     */
    public void setEntries(final TargetLaybyEntries entries) {
        this.entries = entries;
    }

}
