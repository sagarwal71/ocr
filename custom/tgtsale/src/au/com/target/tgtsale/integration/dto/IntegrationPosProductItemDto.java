package au.com.target.tgtsale.integration.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "product")
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationPosProductItemDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @XmlElement
    private String itemCode;

    @XmlElement
    private int gstRate;

    @XmlElement
    private double currentSalePrice;

    @XmlElement
    private double permanentPrice;

    @XmlElement
    private boolean isRemoveFromSale;

    @XmlElement
    private boolean promoEvent;

    @XmlElementWrapper(name = "prices")
    @XmlElement(name = "price")
    private List<IntegrationPosProductItemPriceDto> prices;

    /**
     * Constructor
     */
    public IntegrationPosProductItemDto() {
        super();
        prices = new ArrayList<>();
    }

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode
     *            the itemCode to set
     */
    public void setItemCode(final String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the gstRate
     */
    public int getGstRate() {
        return gstRate;
    }

    /**
     * @param gstRate
     *            the gstRate to set
     */
    public void setGstRate(final int gstRate) {
        this.gstRate = gstRate;
    }

    /**
     * @return the currentSalePrice
     */
    public double getCurrentSalePrice() {
        return currentSalePrice;
    }

    /**
     * @param currentSalePrice
     *            the currentSalePrice to set
     */
    public void setCurrentSalePrice(final double currentSalePrice) {
        this.currentSalePrice = currentSalePrice;
    }

    /**
     * @return the permanentPrice
     */
    public double getPermanentPrice() {
        return permanentPrice;
    }

    /**
     * @param permanentPrice
     *            the permanentPrice to set
     */
    public void setPermanentPrice(final double permanentPrice) {
        this.permanentPrice = permanentPrice;
    }

    /**
     * @return the isRemoveFromSale
     */
    public boolean isRemoveFromSale() {
        return isRemoveFromSale;
    }

    /**
     * @param removeFromSale
     *            the isRemoveFromSale to set
     */
    public void setRemoveFromSale(final boolean removeFromSale) {
        this.isRemoveFromSale = removeFromSale;
    }

    /**
     * @return the prices
     */
    public List<IntegrationPosProductItemPriceDto> getPrices() {
        return prices;
    }

    /**
     * @param prices
     *            the prices to set
     */
    public void setPrices(final List<IntegrationPosProductItemPriceDto> prices) {
        this.prices = prices;
    }

    /**
     * @return the promoEvent
     */
    public boolean isPromoEvent() {
        return promoEvent;
    }

    /**
     * @param promoEvent
     *            the promoEvent to set
     */
    public void setPromoEvent(final boolean promoEvent) {
        this.promoEvent = promoEvent;
    }






}
