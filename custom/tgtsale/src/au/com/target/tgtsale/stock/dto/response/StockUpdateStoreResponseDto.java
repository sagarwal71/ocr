/**
 * 
 */
package au.com.target.tgtsale.stock.dto.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author rmcalave
 * 
 */
@XmlRootElement(name = "integration-stockUpdateStoreResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class StockUpdateStoreResponseDto {
    @XmlElement(name = "storeNumber")
    private String storeNumber;

    @XmlElement(name = "success")
    private boolean success;

    @XmlElement(name = "message")
    private String message;

    @XmlElementWrapper(name = "products")
    @XmlElement(name = "product")
    private List<StockUpdateProductResponseDto> stockUpdateProductResponseDtos;

    /**
     * @return the storeNumber
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success
     *            the success to set
     */
    public void setSuccess(final boolean success) {
        this.success = success;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(final String message) {
        this.message = message;
    }

    /**
     * @return the stockUpdateProductResponseDtos
     */
    public List<StockUpdateProductResponseDto> getStockUpdateProductResponseDtos() {
        return stockUpdateProductResponseDtos;
    }

    /**
     * @param stockUpdateProductResponseDtos
     *            the stockUpdateProductResponseDtos to set
     */
    public void setStockUpdateProductResponseDtos(
            final List<StockUpdateProductResponseDto> stockUpdateProductResponseDtos) {
        this.stockUpdateProductResponseDtos = stockUpdateProductResponseDtos;
    }
}
