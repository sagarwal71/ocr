/**
 * 
 */
package au.com.target.tgtsale.stock.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author rmcalave
 * 
 */
@XmlRootElement(name = "integration-stockUpdateProductResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class StockUpdateProductResponseDto {
    @XmlElement(name = "itemcode")
    private String itemcode;

    @XmlElement(name = "soh")
    private String soh;

    @XmlElement(name = "success")
    private boolean success;

    @XmlElement(name = "message")
    private String message;

    /**
     * @return the itemcode
     */
    public String getItemcode() {
        return itemcode;
    }

    /**
     * @param itemcode
     *            the itemcode to set
     */
    public void setItemcode(final String itemcode) {
        this.itemcode = itemcode;
    }

    /**
     * @return the soh
     */
    public String getSoh() {
        return soh;
    }

    /**
     * @param soh
     *            the soh to set
     */
    public void setSoh(final String soh) {
        this.soh = soh;
    }

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success
     *            the success to set
     */
    public void setSuccess(final boolean success) {
        this.success = success;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(final String message) {
        this.message = message;
    }
}
