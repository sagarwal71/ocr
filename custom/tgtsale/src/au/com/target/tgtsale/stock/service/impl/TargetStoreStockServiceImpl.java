/**
 * 
 */
package au.com.target.tgtsale.stock.service.impl;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.TransformerUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtcore.stock.TargetStockLevelStatusStrategy;
import au.com.target.tgtcore.stockvisibility.client.StockVisibilityClient;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemResponseDto;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtsale.stock.client.TargetStockUpdateClient;
import au.com.target.tgtsale.stock.dto.request.StockUpdateDto;
import au.com.target.tgtsale.stock.dto.request.StockUpdateProductDto;
import au.com.target.tgtsale.stock.dto.request.StockUpdateStoreDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateProductResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateStoreResponseDto;
import au.com.target.tgtsale.stock.service.TargetStoreStockService;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;
import au.com.target.tgtwebcore.exception.TargetIntegrationException;



/**
 * @author rmcalave
 * 
 */
public class TargetStoreStockServiceImpl extends AbstractBusinessService implements TargetStoreStockService {

    private static final Logger LOG = Logger.getLogger(TargetStoreStockServiceImpl.class);
    private static final String STOCK_LOOKUP_PREFIX = "StoreStockLookup: ";

    protected TargetPointOfServiceService targetPointOfServiceService;
    private StockService stockService;
    private TargetWarehouseService targetWarehouseService;
    private TargetProductService targetProductService;
    private CatalogVersionService catalogVersionService;
    private TargetStockUpdateClient targetStockUpdateClient;
    private StockVisibilityClient stockVisibilityClient;
    private TargetStockLevelStatusStrategy storeStockLevelStatusStrategy;
    private TargetFeatureSwitchService targetFeatureSwitchService;

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.stock.service.TargetStoreStockService#updateStoreStockLevels(java.util.List)
     */
    @Override
    public void updateStoreStockLevels(final List<AbstractTargetVariantProductModel> products)
            throws TargetIntegrationException {
        if (CollectionUtils.isEmpty(products)) {
            return;
        }

        final StockUpdateDto stockUpdateRequest = createStockUpdateRequest(products);
        final StockUpdateResponseDto stockUpdateResponse = targetStockUpdateClient
                .getStockLevelsFromPos(stockUpdateRequest);
        updateStoreStockLevels(stockUpdateResponse);
    }

    /* Check if the order can be fulfilled by nominated store
     *
     * @param store store number
     * @param order order
     * @return Boolean result
     *
     */
    @Override
    public boolean isOrderEntriesInStockAtStore(final List<AbstractOrderEntryModel> orderEntries,
            final Integer storeNumber, final String orderCode) {
        //if Store number is not passed return false this should never happen
        if (null == storeNumber) {
            logInfoMessage("Store Number is null", storeNumber, orderCode);
            return false;
        }

        //Get products from orderEntries
        List<AbstractTargetVariantProductModel> products = null;
        if (CollectionUtils.isNotEmpty(orderEntries)) {
            products = getProdutsFromOrder(orderEntries);
            if (CollectionUtils.isEmpty(products)) {
                logInfoMessage("No products", storeNumber, orderCode);
                return false;
            }
        }
        else {
            return false;
        }

        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.USE_CACHE_STOCK_FOR_FULFIMENT)) {
            return checkStockRequestUsingCache(orderEntries, products, storeNumber, orderCode);
        }

        return checkStockRequestUsingLegacyStockService(orderEntries,
                products, storeNumber, orderCode);

    }

    private boolean checkStockRequestUsingLegacyStockService(final List<AbstractOrderEntryModel> orderEntries,
            final List<AbstractTargetVariantProductModel> products,
            final Integer storeNumber, final String orderCode) {
        //prepare the request
        final StockUpdateDto stockUpdateRequest = createStockUpdateRequestForStore(storeNumber, products);
        if (null == stockUpdateRequest) {
            logInfoMessage("Stock service request is null", storeNumber, orderCode);
            return false;
        }
        StockUpdateResponseDto stockUpdateResponse = null;
        try {
            stockUpdateResponse = targetStockUpdateClient
                    .getStockLevelsFromPos(stockUpdateRequest);
        }
        catch (final TargetIntegrationException e) {
            LOG.warn(SplunkLogFormatter.formatMessage("Error invoking Stock service "
                    + storeNumber + ".", TgtutilityConstants.ErrorCode.ERR_TGTSALE), e);
            //Forcing fulfilment to happen by default fulfilment provide (fastline)
            return false;
        }
        //null is returned by the store
        if (null == stockUpdateResponse) {
            logInfoMessage("Stock service request returned a null response", storeNumber, orderCode);
            return false;
        }
        return isProductInStock(stockUpdateResponse, orderEntries, storeNumber, orderCode);
    }

    private boolean checkStockRequestUsingCache(final List<AbstractOrderEntryModel> orderEntries,
            final List<AbstractTargetVariantProductModel> products,
            final Integer storeNumber, final String orderCode) {
        //prepare the request
        final StockUpdateDto stockUpdateRequest = createStockUpdateRequestForStore(storeNumber, products);
        if (null == stockUpdateRequest) {
            logInfoMessage("Stock service request is null", storeNumber, orderCode);
            return false;
        }
        final List<String> storeNumbers = new ArrayList<>();
        final List<String> itemcodes = new ArrayList<>();
        for (final AbstractOrderEntryModel orderEntry : orderEntries) {
            itemcodes.add(orderEntry.getProduct().getCode());
        }
        storeNumbers.add(String.valueOf(storeNumber));
        final StockVisibilityItemLookupResponseDto stockVisibilityItemLookupResponseDto;
        try {
            stockVisibilityItemLookupResponseDto = lookupStockForItemsInStores(storeNumbers, itemcodes);
        }
        catch (final Exception e) {
            LOG.warn(SplunkLogFormatter.formatMessage("Error invoking Stock service "
                    + storeNumber + ".", TgtutilityConstants.ErrorCode.ERR_TGTSALE), e);
            //Forcing fulfilment to happen by default fulfilment provide (fastline)
            return false;
        }
        //null is returned by the store
        if (null == stockVisibilityItemLookupResponseDto) {
            logInfoMessage("Stock service request returned a null response", storeNumber, orderCode);
            return false;
        }
        return isProductInStock(stockVisibilityItemLookupResponseDto, orderEntries, storeNumber, orderCode);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.stock.service.TargetStoreStockService#getStockForProducts(java.util.List, java.lang.Integer)
     */
    @Override
    public StockUpdateResponseDto getStockForProducts(final List<AbstractTargetVariantProductModel> products,
            final Integer storeNumber) {
        //prepare the request
        final StockUpdateDto stockUpdateRequest = createStockUpdateRequestForStore(storeNumber, products);
        try {
            final StockUpdateResponseDto response = targetStockUpdateClient
                    .getStockLevelsFromPos(stockUpdateRequest);
            return response;
        }
        catch (final TargetIntegrationException e) {
            return null;
        }
    }

    /**
     * lookup the store stock using web-method service from POS. using different clients based on the feature switch.
     */
    @Override
    public StockVisibilityItemLookupResponseDto lookupStockForItemsInStores(final List<String> storeNumbers,
            final List<String> itemcodes) {
        return stockVisibilityClient.lookupStockForItemsInStores(storeNumbers, itemcodes);
    }

    @Override
    public Map<String, StockLevelStatus> getStoreStockLevelStatusMapForProducts(final Integer storeNumber,
            final List<String> itemcodes) {
        final Map<String, StockLevelStatus> stockMap = new HashMap<>();
        if (targetPointOfServiceService.validateStoreNumber(storeNumber)) {
            final StockVisibilityItemLookupResponseDto result = lookupStockForItemsInStores(
                    Collections.singletonList(storeNumber.toString()), itemcodes);
            if (result != null && result.getItems() != null) {
                for (final StockVisibilityItemResponseDto item : result.getItems()) {
                    stockMap.put(item.getCode(), storeStockLevelStatusStrategy.checkStatus(item.getSoh()));
                }
            }
        }
        return stockMap;
    }

    protected void logInfoMessage(final String message, final Integer storeNumber, final String orderCode) {

        final String logMessage = STOCK_LOOKUP_PREFIX + message + ", store=" + storeNumber + ", order=" + orderCode;
        LOG.info(logMessage);
    }

    protected void logSohInfoMessage(final String message, final Integer storeNumber) {
        final String logMessage = STOCK_LOOKUP_PREFIX + message + ", store=" + storeNumber;
        LOG.info(logMessage);
    }

    @SuppressWarnings("unused")
    protected boolean isProductInStock(final StockUpdateResponseDto stockUpdateResponse,
            final List<AbstractOrderEntryModel> orderEntries, final Integer storeNumber, final String orderCode) {
        return false;
    }

    @SuppressWarnings("unused")
    protected boolean isProductInStock(final StockVisibilityItemLookupResponseDto tockVisibilityItemLookupResponseDto,
            final List<AbstractOrderEntryModel> orderEntries, final Integer storeNumber, final String orderCode) {
        return false;
    }

    @SuppressWarnings("unused")
    @Override
    public Map<String, Long> fetchProductsSohInStore(final TargetPointOfServiceModel tpos,
            final List<String> products) {
        return null;
    }

    private StockUpdateDto createStockUpdateRequest(final List<AbstractTargetVariantProductModel> products) {
        final Map<Integer, StockUpdateStoreDto> storeMapping = new HashMap<Integer, StockUpdateStoreDto>();

        for (final AbstractTargetVariantProductModel product : products) {
            final Collection<StockLevelModel> productStockLevels = stockService.getAllStockLevels(product);

            for (final StockLevelModel productStockLevel : productStockLevels) {
                if (InStockStatus.FORCEINSTOCK.equals(productStockLevel.getInStockStatus())) {
                    continue; // Don't bother checking POS for stock levels if the stock level is set to 'Force in Stock'
                }

                final WarehouseModel warehouse = productStockLevel.getWarehouse();
                final Collection<PointOfServiceModel> warehousePointsOfService = warehouse.getPointsOfService();

                for (final PointOfServiceModel pointOfService : warehousePointsOfService) {
                    final TargetPointOfServiceModel targetPointOfService = (TargetPointOfServiceModel)pointOfService;
                    StockUpdateStoreDto stockUpdateStore = storeMapping.get(targetPointOfService.getStoreNumber());
                    if (stockUpdateStore == null) {
                        stockUpdateStore = new StockUpdateStoreDto();
                        stockUpdateStore.setStoreNumber(targetPointOfService.getStoreNumber().toString());
                        stockUpdateStore.setStoreState(targetPointOfService.getAddress().getDistrict());

                        storeMapping.put(targetPointOfService.getStoreNumber(), stockUpdateStore);
                    }

                    final StockUpdateProductDto stockUpdateProduct = new StockUpdateProductDto();
                    stockUpdateProduct.setItemcode(product.getCode());
                    stockUpdateProduct.setEan(product.getEan());

                    stockUpdateStore.addStockUpdateProductDto(stockUpdateProduct);
                }
            }
        }

        final StockUpdateDto stockUpdateDto = new StockUpdateDto();
        stockUpdateDto.setStockUpdateStoreDtos(new ArrayList<StockUpdateStoreDto>(storeMapping.values()));

        return stockUpdateDto;
    }

    private void updateStoreStockLevels(final StockUpdateResponseDto stockUpdateResponse) {
        final CatalogVersionModel stagedCatalog = catalogVersionService.getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS, TgtCoreConstants.Catalog.OFFLINE_VERSION);
        for (final StockUpdateStoreResponseDto stockUpdateStoreResponseDto : stockUpdateResponse
                .getStockUpdateStoreResponseDtos()) {

            if (stockUpdateStoreResponseDto.getStoreNumber() == null) {
                LOG.warn(SplunkLogFormatter.formatMessage("No store number in response",
                        TgtutilityConstants.ErrorCode.ERR_TGTSALE));
                continue;
            }

            if (!stockUpdateStoreResponseDto.isSuccess()) {
                LOG.error(SplunkLogFormatter.formatMessage(
                        "Error updating stock levels for store " + stockUpdateStoreResponseDto.getStoreNumber()
                                + ", reason: " + stockUpdateStoreResponseDto.getMessage(),
                        TgtutilityConstants.ErrorCode.ERR_TGTSALE));
                continue;
            }

            TargetPointOfServiceModel pointOfService = null;
            try {
                pointOfService = targetPointOfServiceService.getPOSByStoreNumber(Integer
                        .valueOf(stockUpdateStoreResponseDto
                                .getStoreNumber()));
            }
            catch (final TargetAmbiguousIdentifierException ex) {
                LOG.warn(SplunkLogFormatter.formatMessage("Too many stores found for store number "
                        + stockUpdateStoreResponseDto.getStoreNumber() + ".",
                        TgtutilityConstants.ErrorCode.ERR_TGTSALE));
                continue;
            }
            catch (final TargetUnknownIdentifierException ex) {
                LOG.warn(SplunkLogFormatter.formatMessage("No store found for store number "
                        + stockUpdateStoreResponseDto.getStoreNumber() + ".",
                        TgtutilityConstants.ErrorCode.ERR_TGTSALE));
                continue;
            }
            catch (final NumberFormatException ex) {
                LOG.warn(SplunkLogFormatter.formatMessage("Invalid store number from response: "
                        + stockUpdateStoreResponseDto.getStoreNumber(), TgtutilityConstants.ErrorCode.ERR_TGTSALE));
                continue;
            }

            final WarehouseModel warehouse = targetWarehouseService.getWarehouseForPointOfService(pointOfService);
            if (warehouse == null) {
                LOG.warn(SplunkLogFormatter.formatMessage(
                        "No warehouse for point of service: " + pointOfService.getName() + "("
                                + pointOfService.getStoreNumber() + ")",
                        TgtutilityConstants.ErrorCode.ERR_TGTSALE));
                continue;
            }

            for (final StockUpdateProductResponseDto stockUpdateProductResponseDto : stockUpdateStoreResponseDto
                    .getStockUpdateProductResponseDtos()) {
                if (StringUtils.isBlank(stockUpdateProductResponseDto.getItemcode())) {
                    LOG.error(SplunkLogFormatter.formatMessage(
                            "No itemcode returned for product in store " + stockUpdateStoreResponseDto.getStoreNumber()
                                    + ": " + stockUpdateProductResponseDto.getMessage(),
                            TgtutilityConstants.ErrorCode.ERR_TGTSALE));
                    continue;
                }

                if (!stockUpdateProductResponseDto.isSuccess()) {
                    LOG.error(
                            SplunkLogFormatter.formatMessage("There was a problem with product "
                                    + stockUpdateProductResponseDto.getItemcode()
                                    + " in store " + stockUpdateStoreResponseDto.getStoreNumber() + ": "
                                    + stockUpdateProductResponseDto.getMessage(),
                                    TgtutilityConstants.ErrorCode.ERR_TGTSALE));
                }

                final ProductModel product = targetProductService.getProductForCode(stagedCatalog,
                        stockUpdateProductResponseDto.getItemcode());

                final StockLevelModel stockLevel = stockService.getStockLevel(product, warehouse);

                try {
                    final int soh = Integer.parseInt(stockUpdateProductResponseDto.getSoh());

                    if (soh < 0) {
                        LOG.warn(SplunkLogFormatter.formatMessage("Received negative soh value of " + soh
                                + " from store "
                                + stockUpdateStoreResponseDto.getStoreNumber() + " for product "
                                + stockUpdateProductResponseDto.getItemcode() + ". Setting soh to 0.",
                                TgtutilityConstants.ErrorCode.ERR_TGTSALE));
                        stockLevel.setAvailable(0);
                    }
                    else {
                        stockLevel.setAvailable(soh);
                    }
                }
                catch (final NumberFormatException ex) {
                    LOG.error(SplunkLogFormatter.formatMessage("SOH value " + stockUpdateProductResponseDto.getSoh()
                            + " from store " + stockUpdateStoreResponseDto.getStoreNumber() + " for product "
                            + stockUpdateProductResponseDto.getItemcode() + " is invalid.",
                            TgtutilityConstants.ErrorCode.ERR_TGTSALE));
                    continue;
                }

                getModelService().save(stockLevel);
            }
        }
    }

    /**
     * Get products from OrderEntryGroup
     *
     * @return products list of products
     */
    private List<AbstractTargetVariantProductModel> getProdutsFromOrder(
            final List<AbstractOrderEntryModel> orderEntries) {
        final List<AbstractTargetVariantProductModel> products = new ArrayList<>();

        for (final AbstractOrderEntryModel oem : orderEntries) {
            products.add((AbstractTargetVariantProductModel)oem.getProduct());
        }
        return products;
    }

    private StockUpdateDto createStockUpdateRequestForStore(final Integer storeNumber,
            final List<AbstractTargetVariantProductModel> products) {
        final Map<Integer, StockUpdateStoreDto> storeMapping = new HashMap<Integer, StockUpdateStoreDto>();

        if (null == storeNumber) {
            LOG.info("Store number is null");
            return null;
        }
        TargetPointOfServiceModel storeModel = null;
        try {
            storeModel = targetPointOfServiceService.getPOSByStoreNumber(storeNumber);
        }
        catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
            LOG.warn(SplunkLogFormatter.formatMessage("Error processing for store with store number "
                    + storeNumber + ".", TgtutilityConstants.ErrorCode.ERR_TGTSALE), e);
        }
        if (null == storeModel) {
            LOG.info("Store model for store number : " + storeNumber + " is null");
            return null;
        }

        if (null == storeModel.getStoreNumber()) {
            LOG.info("Store Model does not have a store number");
            return null;
        }
        final StockUpdateStoreDto stockUpdateStore = new StockUpdateStoreDto();
        stockUpdateStore.setStoreNumber(storeModel.getStoreNumber().toString());

        if (null != storeModel.getAddress()) {
            stockUpdateStore.setStoreState(storeModel.getAddress().getDistrict());
        }
        storeMapping.put(storeModel.getStoreNumber(), stockUpdateStore);

        for (final AbstractTargetVariantProductModel product : products) {
            final StockUpdateProductDto stockUpdateProduct = new StockUpdateProductDto();
            stockUpdateProduct.setItemcode(product.getCode());
            stockUpdateProduct.setEan(product.getEan());
            stockUpdateStore.addStockUpdateProductDto(stockUpdateProduct);
        }

        final StockUpdateDto stockUpdateDto = new StockUpdateDto();
        stockUpdateDto.setStockUpdateStoreDtos(new ArrayList<StockUpdateStoreDto>(storeMapping.values()));

        return stockUpdateDto;
    }

    protected boolean checkProductExistsInStore(final List<AbstractOrderEntryModel> orderEntries,
            final List<StockUpdateProductResponseDto> productStoreStocks) {
        final Collection<String> storeProducts = CollectionUtils.collect(productStoreStocks,
                TransformerUtils.invokerTransformer("getItemcode"));

        //store does not have any products
        if (CollectionUtils.isEmpty(storeProducts)) {
            return false;
        }

        for (final AbstractOrderEntryModel oem : orderEntries) {
            final ProductModel product = oem.getProduct();
            if (null != product && null != product.getCode()) {
                if (!storeProducts.contains(product.getCode())) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param stockService
     *            the stockService to set
     */
    @Required
    public void setStockService(final StockService stockService) {
        this.stockService = stockService;
    }

    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */
    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }

    /**
     * @param targetPointOfServiceService
     *            the targetPointOfServiceService to set
     */
    @Required
    public void setTargetPointOfServiceService(final TargetPointOfServiceService targetPointOfServiceService) {
        this.targetPointOfServiceService = targetPointOfServiceService;
    }

    /**
     * @param targetProductService
     *            the targetProductService to set
     */
    @Required
    public void setTargetProductService(final TargetProductService targetProductService) {
        this.targetProductService = targetProductService;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @param targetStockUpdateClient
     *            the targetStockUpdateClient to set
     */
    @Required
    public void setTargetStockUpdateClient(final TargetStockUpdateClient targetStockUpdateClient) {
        this.targetStockUpdateClient = targetStockUpdateClient;
    }

    /**
     * @param stockVisibilityClient
     *            the stockVisibilityClient to set
     */
    @Required
    public void setStockVisibilityClient(final StockVisibilityClient stockVisibilityClient) {
        this.stockVisibilityClient = stockVisibilityClient;
    }

    /**
     * @param storeStockLevelStatusStrategy
     *            the storeStockLevelStatusStrategy to set
     */
    @Required
    public void setStoreStockLevelStatusStrategy(final TargetStockLevelStatusStrategy storeStockLevelStatusStrategy) {
        this.storeStockLevelStatusStrategy = storeStockLevelStatusStrategy;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }
}
