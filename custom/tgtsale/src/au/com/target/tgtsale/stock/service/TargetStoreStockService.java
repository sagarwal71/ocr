/**
 * 
 */
package au.com.target.tgtsale.stock.service;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateResponseDto;
import au.com.target.tgtwebcore.exception.TargetIntegrationException;


/**
 * @author rmcalave
 * 
 */
public interface TargetStoreStockService {
    /**
     * Updates the store stock levels of the provided products based on the response from a remote source.
     * 
     * @param products
     *            The list of products to update
     */
    void updateStoreStockLevels(final List<AbstractTargetVariantProductModel> products)
            throws TargetIntegrationException;

    /**
     * Checks whether the order can be fulfilled by the store.
     * 
     * @param orderEntries
     * @param storeNumber
     *            store number
     * @param orderCode
     * 
     * @return Boolean True/False
     */
    boolean isOrderEntriesInStockAtStore(List<AbstractOrderEntryModel> orderEntries, final Integer storeNumber,
            String orderCode);


    /**
     * @param products
     * @param storeNumber
     * @return Stock in hand
     */
    StockUpdateResponseDto getStockForProducts(final List<AbstractTargetVariantProductModel> products,
            final Integer storeNumber);

    /**
     * lookup the store stock.
     * 
     * @param storeNumbers
     * @param itemcodes
     * @return stock in hand
     */
    StockVisibilityItemLookupResponseDto lookupStockForItemsInStores(List<String> storeNumbers,
            List<String> itemcodes);

    /**
     * Return Stock Level Status map For Products
     * 
     * @param storeNumber
     * @param itemcodes
     * @return mapOfItemAndStockLevelStatus
     */
    Map<String, StockLevelStatus> getStoreStockLevelStatusMapForProducts(Integer storeNumber, List<String> itemcodes);

    /**
     * fetch products soh in one store, consider the butter and store pending
     * 
     * @param tpos
     * @param products
     * @return map <productcode, actual soh>
     */
    Map<String, Long> fetchProductsSohInStore(final TargetPointOfServiceModel tpos,
            final List<String> products);

}
