/**
 * 
 */
package au.com.target.tgtsale.ist.exception;

/**
 * @author vmuthura
 * 
 */
public class PublishISTFailedException extends RuntimeException {

    public PublishISTFailedException(final String message) {
        super(message);
    }

    public PublishISTFailedException(final Throwable throwable) {
        super(throwable);
    }

    public PublishISTFailedException(final String message, final Throwable throwable) {
        super(message, throwable);
    }
}
