/**
 * 
 */
package au.com.target.tgtsale.ist.service;




/**
 * Service to publish the inter-store transfer information.
 *
 * @author Nandini
 */
public interface TargetInterStoreTransferService {

    /**
     * Publish inter store transfer info.
     *
     * @param consignmentCode
     *            the consignmentCode
     */
    void publishInterStoreTransferInfo(final String consignmentCode);
}
