package au.com.target.tgtsale.storeconfig.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.util.TargetServicesUtil;
import au.com.target.tgtsale.model.TargetStoreConfigModel;
import au.com.target.tgtsale.storeconfig.TargetStoreConfigDao;


public class TargetStoreConfigDaoImpl extends DefaultGenericDao implements TargetStoreConfigDao {

    private static final String QUERY_GET_LIST_STORE_CONFIG =
            "select {" + TargetStoreConfigModel.PK + "} from {" + TargetStoreConfigModel._TYPECODE + "}"
                    + " where {" + TargetStoreConfigModel.SECTION + "}=?section"
                    + " and {" + TargetStoreConfigModel.TAG + "}=?tag";

    private static final String QUERY_GET_STORE_CONFIG =
            "select {" + TargetStoreConfigModel.PK + "} from {" + TargetStoreConfigModel._TYPECODE + "}"
                    + " where {" + TargetStoreConfigModel.SECTION + "}=?section"
                    + " and {" + TargetStoreConfigModel.TAG + "}=?tag"
                    + " and {" + TargetStoreConfigModel.OCCURANCE + "}=?occurance";

    /**
     * constructor
     */
    public TargetStoreConfigDaoImpl() {
        super(TargetStoreConfigModel._TYPECODE);
    }

    @Override
    public List<TargetStoreConfigModel> getListStoreConfigs(final String section, final String tag)
    {
        final Map<String, String> params = new HashMap<String, String>(2, 1);
        params.put("section", section);
        params.put("tag", tag);

        final SearchResult<TargetStoreConfigModel> searchResult =
                getFlexibleSearchService().search(QUERY_GET_LIST_STORE_CONFIG, params);
        return searchResult.getResult();
    }

    @Override
    public TargetStoreConfigModel getStoreConfig(final String section, final String tag, final int occurance)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException
    {
        final Map<String, Object> params = new HashMap<String, Object>(3, 1);
        params.put("section", section);
        params.put("tag", tag);
        params.put("occurance", Integer.valueOf(occurance));

        final SearchResult<TargetStoreConfigModel> searchResult =
                getFlexibleSearchService().search(QUERY_GET_STORE_CONFIG, params);

        final List<TargetStoreConfigModel> models = searchResult.getResult();
        TargetServicesUtil.validateIfSingleResult(models, TargetStoreConfigModel.class,
                TargetStoreConfigModel.SECTION, section);
        return models.get(0);
    }

}