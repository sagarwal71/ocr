/**
 * 
 */
package au.com.target.tgtsale.tlog.data;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import au.com.target.tgtsale.tlog.data.adapter.FlybuysAdapter;
import au.com.target.tgtsale.tlog.data.adapter.LayByDueDateFormatAdapter;
import au.com.target.tgtsale.tlog.data.adapter.TlogTimestampAdapter;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Transaction {
    @XmlAttribute(name = "type")
    private TransactionTypeEnum type;

    @XmlAttribute(name = "currency")
    private String currency;

    @XmlAttribute(name = "salesChannel")
    private String salesChannel;

    @XmlAttribute(name = "laybyVersion")
    private String laybyVersion;

    @XmlAttribute(name = "timestamp")
    @XmlJavaTypeAdapter(TlogTimestampAdapter.class)
    private Date timestamp;

    @XmlAttribute(name = "laybyType")
    private String laybyType;

    @XmlAttribute(name = "dueDate")
    @XmlJavaTypeAdapter(LayByDueDateFormatAdapter.class)
    private Date dueDate;

    @XmlElement(name = "orderid")
    private OrderInfo order;

    @XmlElement(name = "customer")
    private CustomerInfo customerInfo;

    @XmlElement(name = "item")
    private List<ItemInfo> items;

    @XmlElement(name = "transDeal")
    private List<TransactionDeal> transactionDeal;

    @XmlElement(name = "transDiscount")
    private List<DiscountInfo> discounts;

    @XmlElement(name = "flybuys")
    @XmlJavaTypeAdapter(FlybuysAdapter.class)
    private Flybuys flybuys;

    @XmlElement(name = "shipping")
    private ShippingInfo shippingInfo;

    @XmlElement(name = "laybyFee")
    private LayByFee laybyFee;

    @XmlElement(name = "payment")
    private Payment payment;

    @XmlElement(name = "tender")
    private List<TenderInfo> tender;

    /**
     * @return the type
     */
    public TransactionTypeEnum getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final TransactionTypeEnum type) {
        this.type = type;
    }

    /**
     * @return the laybyVersion
     */
    public String getLaybyVersion() {
        return laybyVersion;
    }

    /**
     * @param laybyVersion
     *            the laybyVersion to set
     */
    public void setLaybyVersion(final String laybyVersion) {
        this.laybyVersion = laybyVersion;
    }

    /**
     * @return the timestamp
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp
     *            the timestamp to set
     */
    public void setTimestamp(final Date timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the dueDate
     */
    public Date getDueDate() {
        return dueDate;
    }

    /**
     * @param dueDate
     *            the dueDate to set
     */
    public void setDueDate(final Date dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * @return the order
     */
    public OrderInfo getOrder() {
        return order;
    }

    /**
     * @param order
     *            the order to set
     */
    public void setOrder(final OrderInfo order) {
        this.order = order;
    }

    /**
     * @return the customerInfo
     */
    public CustomerInfo getCustomerInfo() {
        return customerInfo;
    }

    /**
     * @param customerInfo
     *            the customerInfo to set
     */
    public void setCustomerInfo(final CustomerInfo customerInfo) {
        this.customerInfo = customerInfo;
    }

    /**
     * @return the items
     */
    public List<ItemInfo> getItems() {
        return items;
    }

    /**
     * @param items
     *            the items to set
     */
    public void setItems(final List<ItemInfo> items) {
        this.items = items;
    }

    /**
     * @return the flybuys
     */
    public Flybuys getFlybuys() {
        return flybuys;
    }

    /**
     * @param flybuys
     *            the flybuys to set
     */
    public void setFlybuys(final Flybuys flybuys) {
        this.flybuys = flybuys;
    }

    /**
     * @return the laybyFee
     */
    public LayByFee getLaybyFee() {
        return laybyFee;
    }

    /**
     * @param laybyFee
     *            the laybyFee to set
     */
    public void setLaybyFee(final LayByFee laybyFee) {
        this.laybyFee = laybyFee;
    }

    /**
     * @return the payment
     */
    public Payment getPayment() {
        return payment;
    }

    /**
     * @param payment
     *            the payment to set
     */
    public void setPayment(final Payment payment) {
        this.payment = payment;
    }

    /**
     * @return the discounts
     */
    public List<DiscountInfo> getDiscounts() {
        return discounts;
    }

    /**
     * @param discounts
     *            the discounts to set
     */
    public void setDiscounts(final List<DiscountInfo> discounts) {
        this.discounts = discounts;
    }

    /**
     * @return the shippingInfo
     */
    public ShippingInfo getShippingInfo() {
        return shippingInfo;
    }

    /**
     * @param shippingInfo
     *            the shippingInfo to set
     */
    public void setShippingInfo(final ShippingInfo shippingInfo) {
        this.shippingInfo = shippingInfo;
    }

    /**
     * @return the tender
     */
    public List<TenderInfo> getTender() {
        return tender;
    }

    /**
     * @param tender
     *            the tender to set
     */
    public void setTender(final List<TenderInfo> tender) {
        this.tender = tender;
    }

    /**
     * @return the laybyType
     */
    public String getLaybyType() {
        return laybyType;
    }

    /**
     * @param laybyType
     *            the laybyType to set
     */
    public void setLaybyType(final String laybyType) {
        this.laybyType = laybyType;
    }

    /**
     * @return the transactionDeal
     */
    public List<TransactionDeal> getTransactionDeal() {
        return transactionDeal;
    }

    /**
     * @param transactionDeal
     *            the transactionDeal to set
     */
    public void setTransactionDeal(final List<TransactionDeal> transactionDeal) {
        this.transactionDeal = transactionDeal;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency
     *            the currency to set
     */
    public void setCurrency(final String currency) {
        this.currency = currency;
    }

    /**
     * @return the salesChannel
     */
    public String getSalesChannel() {
        return salesChannel;
    }

    /**
     * @param salesChannel
     *            the salesChannel to set
     */
    public void setSalesChannel(final String salesChannel) {
        this.salesChannel = salesChannel;
    }

}
