/**
 * 
 */
package au.com.target.tgtsale.tlog.data.adapter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.lang.StringUtils;


/**
 * JAXB Adapter {@link XmlAdapter} class to format a {@link BigDecimal} (used for amount) to cent value, as required by
 * POS
 * 
 * @author vmuthura
 * 
 */
public class BigDecimalAdapter extends XmlAdapter<String, BigDecimal>
{
    private final DecimalFormat df = new DecimalFormat("#000");

    @Override
    public String marshal(final BigDecimal value) throws Exception
    {
        if (value == null)
        {
            return null;
        }
        value.setScale(2, RoundingMode.HALF_UP);

        //Converting to cents
        return df.format(value.multiply(new BigDecimal(100)));
    }

    @Override
    public BigDecimal unmarshal(final String value) throws Exception
    {
        if (StringUtils.isBlank(value))
        {
            return null;
        }

        return new BigDecimal(value).setScale(2).divide(new BigDecimal(100));
    }

}
