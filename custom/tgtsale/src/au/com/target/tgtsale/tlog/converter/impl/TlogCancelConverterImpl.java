/**
 * 
 */
package au.com.target.tgtsale.tlog.converter.impl;

import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtpayment.enums.PaymentProviderTypeEnum;
import au.com.target.tgtpayment.util.PaymentTransactionHelper;
import au.com.target.tgtsale.tlog.converter.TlogCancelConverter;
import au.com.target.tgtsale.tlog.converter.TlogPaymentEntryConverter;
import au.com.target.tgtsale.tlog.data.DiscountInfo;
import au.com.target.tgtsale.tlog.data.DiscountTypeEnum;
import au.com.target.tgtsale.tlog.data.ItemInfo;
import au.com.target.tgtsale.tlog.data.ReasonTypeEnum;
import au.com.target.tgtsale.tlog.data.TenderInfo;
import au.com.target.tgtsale.tlog.data.Transaction;
import com.google.common.collect.ImmutableList;
import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.ordercancel.model.OrderEntryCancelRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.returns.model.OrderEntryReturnRecordEntryModel;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;



/**
 * @author mjanarth
 *
 */
public class TlogCancelConverterImpl implements TlogCancelConverter {


    private TlogPaymentEntryConverter tlogPaymentEntryConverter;

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogCancelConverter#getFlybuysRedeemCode(de.hybris.platform.core.model.order.OrderModel)
     */
    @Override
    public String getFlybuysRedeemCode(final OrderModel orderModel) {
        if (null != orderModel && CollectionUtils.isNotEmpty(orderModel.getDiscounts())) {
            for (final DiscountModel discountModel : orderModel.getDiscounts()) {
                if (discountModel instanceof FlybuysDiscountModel) {
                    return ((FlybuysDiscountModel)discountModel).getRedeemCode();
                }
            }
        }
        return null;
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogCancelConverter#addFlybuysDiscountInfo(au.com.target.tgtsale.tlog.data.Transaction, de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel, java.lang.String)
     */
    @Override
    public void addFlybuysDiscountInfo(final Transaction transaction,
            final OrderModificationRecordEntryModel orderModRecordEntryModel, final String flybuysCardNumber) {
        if (StringUtils.isNotBlank(flybuysCardNumber) && null != orderModRecordEntryModel
                && null != orderModRecordEntryModel.getRefundedFlybuysAmount()
                && null != orderModRecordEntryModel.getRefundedFlybuysPoints()
                && orderModRecordEntryModel.getRefundedFlybuysPoints().intValue() > 0
                && null != orderModRecordEntryModel.getFlybuysPointsRefundConfirmationCode()) {
            final List<DiscountInfo> discountInfos = new ArrayList<>();
            final DiscountInfo discountInfo = new DiscountInfo();
            discountInfo.setCode(flybuysCardNumber);
            discountInfo.setType(DiscountTypeEnum.FLYBUYS);
            discountInfo.setAmount(BigDecimal
                    .valueOf(orderModRecordEntryModel.getRefundedFlybuysAmount().doubleValue()));
            discountInfo.setPoints(orderModRecordEntryModel.getRefundedFlybuysPoints());
            discountInfo
                    .setRedeemCode(getFlybuysRedeemCode(orderModRecordEntryModel
                            .getModificationRecord().getOrder()));
            discountInfo.setConfirmationCode(orderModRecordEntryModel.getFlybuysPointsRefundConfirmationCode());
            discountInfos.add(discountInfo);
            transaction.setDiscounts(discountInfos);
        }
    }



    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogCancelConverter#getItemListForModifiedEntries(java.util.Collection, java.math.BigDecimal, boolean)
     */
    @Override
    public List<ItemInfo> getItemListForModifiedEntries(
            final Collection<OrderEntryModificationRecordEntryModel> orderEntryModificationRecordEntries,
            final BigDecimal tenderAmount, final boolean isFlybuysApplied) {
        if (orderEntryModificationRecordEntries == null) {
            return null;
        }

        BigDecimal totalUnitPrice = BigDecimal.ZERO;
        //to find the total price based off quantity
        BigDecimal actualTotalPrice = BigDecimal.ZERO;

        final List<ItemInfo> itemList = new LinkedList<>();

        for (final OrderEntryModificationRecordEntryModel orderEntryModificationRecordEntryModel : orderEntryModificationRecordEntries) {
            long quantity = 0l;
            ReasonTypeEnum reason = null;
            if (orderEntryModificationRecordEntryModel instanceof OrderEntryCancelRecordEntryModel) {
                final OrderEntryCancelRecordEntryModel cancelRecordEntryModel = (OrderEntryCancelRecordEntryModel)orderEntryModificationRecordEntryModel;
                quantity = cancelRecordEntryModel.getCancelledQuantity() != null
                        ? cancelRecordEntryModel.getCancelledQuantity().longValue() : 0l;

                reason = getCancelReasonForEntry(
                        (OrderCancelRecordEntryModel)cancelRecordEntryModel.getModificationRecordEntry());
            }
            else if (orderEntryModificationRecordEntryModel instanceof OrderEntryReturnRecordEntryModel) {

                final OrderEntryReturnRecordEntryModel returnRecordEntryModel = (OrderEntryReturnRecordEntryModel)orderEntryModificationRecordEntryModel;

                quantity = returnRecordEntryModel.getReturnedQuantity() != null
                        ? returnRecordEntryModel.getReturnedQuantity().longValue() : 0l;
                reason = getRefundReasonForEntry(returnRecordEntryModel);

            }
            final ItemInfo item = getItemInfoFromOrderEntry(
                    orderEntryModificationRecordEntryModel.getOrderEntry(),
                    Long.valueOf(quantity));
            if (item != null) {
                totalUnitPrice = totalUnitPrice.add(item.getPrice());
                actualTotalPrice = actualTotalPrice.add(item.getPrice().multiply(BigDecimal.valueOf(quantity)));
                item.setReason(reason);
                itemList.add(item);
            }
        }

        if (!isFlybuysApplied && CollectionUtils.isNotEmpty(itemList)
                && !tenderAmount.equals(totalUnitPrice)) {
            BigDecimal totalNotAllocated = tenderAmount;
            ItemInfo item = null;
            int i = 0;
            for (final ListIterator<ItemInfo> iter = itemList.listIterator(); iter.hasNext(); i++) {
                final ItemInfo itemInfo = iter.next();
                BigDecimal quantity = BigDecimal.valueOf(itemInfo.getQuantity().longValue());
                if (i == itemList.size() - 1) {
                    if (quantity.intValue() > 1) {
                        quantity = quantity.subtract(BigDecimal.valueOf(1.0d));
                        final BigDecimal price = getFinalPriceForItem(tenderAmount, actualTotalPrice, itemInfo,
                                quantity);
                        itemInfo.setQuantity(Long.valueOf(quantity.longValue()));
                        totalNotAllocated = totalNotAllocated.subtract(price.multiply(quantity));
                        itemInfo.setPrice(price);
                        item = new ItemInfo();
                        item.setId(itemInfo.getId());
                        item.setQuantity(Long.valueOf(1));
                        item.setPrice(totalNotAllocated.setScale(2, RoundingMode.HALF_UP));
                        item.setFilePrice(itemInfo.getFilePrice());
                        item.setReason(itemInfo.getReason());
                    }
                    else {
                        itemInfo.setPrice(totalNotAllocated.setScale(2, RoundingMode.HALF_UP));
                    }
                }
                else {
                    final BigDecimal finalPriceForItem = getFinalPriceForItem(tenderAmount, actualTotalPrice, itemInfo,
                            quantity).setScale(2, RoundingMode.DOWN);
                    totalNotAllocated = totalNotAllocated.subtract(finalPriceForItem.multiply(quantity));
                    itemInfo.setPrice(finalPriceForItem);
                }
            }
            if (item != null) {
                itemList.add(item);
            }
        }

        return !itemList.isEmpty() ? itemList : null;
    }



    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogCancelConverter#getItemInfoFromOrderEntry(de.hybris.platform.core.model.order.AbstractOrderEntryModel, java.lang.Long)
     */
    @Override
    public ItemInfo getItemInfoFromOrderEntry(final AbstractOrderEntryModel orderEntryModel,
            final Long quantity) {
        if (orderEntryModel == null) {
            return null;
        }
        final ItemInfo item = new ItemInfo();

        item.setId(orderEntryModel.getProduct().getCode());
        item.setQuantity(quantity);
        final BigDecimal price = BigDecimal.valueOf(orderEntryModel.getBasePrice().doubleValue());
        item.setPrice(price.negate());
        item.setFilePrice(price);

        return item;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogCancelConverter#getRefundReasonForEntry(de.hybris.platform.returns.model.OrderEntryReturnRecordEntryModel)
     */
    @Override
    public ReasonTypeEnum getRefundReasonForEntry(
            final OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntryModel) {
        ReasonTypeEnum reasonForEntry = null;

        final String refundReason = orderEntryReturnRecordEntryModel.getRefundReason();
        if (StringUtils.isNotBlank(refundReason)) {
			reasonForEntry = ReasonTypeEnum.find(refundReason);
		}
        return reasonForEntry!=null ? reasonForEntry : ReasonTypeEnum.FAULTY;
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogCancelConverter#getCancelReasonForEntry(de.hybris.platform.ordercancel.model.OrderEntryCancelRecordEntryModel)
     */
    @Override
    public ReasonTypeEnum getCancelReasonForEntry(
            final OrderCancelRecordEntryModel orderCancelRecordEntryModel) {
        ReasonTypeEnum reasonForEntry = null;

        final CancelReason cancelReason = orderCancelRecordEntryModel.getCancelReason();

        if(cancelReason!=null) {
             reasonForEntry = ReasonTypeEnum.find(cancelReason.getCode());
        }
        return reasonForEntry!=null ? reasonForEntry : ReasonTypeEnum.FAULTY;
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogCancelConverter#getFinalPriceForItem(java.math.BigDecimal, java.math.BigDecimal, au.com.target.tgtsale.tlog.data.ItemInfo, java.math.BigDecimal)
     */
    @Override
    public BigDecimal getFinalPriceForItem(final BigDecimal tenderAmount, final BigDecimal actualTotalPrice,
            final ItemInfo itemInfo, final BigDecimal quantity) {
        final BigDecimal portion = itemInfo.getPrice()
                .divide(actualTotalPrice, 5, RoundingMode.HALF_DOWN)
                .multiply(quantity);
        final BigDecimal totalPrice = portion.multiply(tenderAmount);
        if (quantity.intValue() > 1) {
            return totalPrice.divide(quantity, RoundingMode.HALF_DOWN)
                    .setScale(2, RoundingMode.HALF_DOWN);
        }
        return totalPrice;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogCancelConverter#getTenderInfoForRefund(java.util.List, java.util.List)
     */
    @Override
    public BigDecimal getTenderInfoForRefund(final List<TenderInfo> tenderList,
            final List<PaymentTransactionEntryModel> refundTransactionEntries) {
        BigDecimal tenderAmount = BigDecimal.ZERO;
        if (CollectionUtils.isEmpty(refundTransactionEntries)) {
            return tenderAmount;
        }

        boolean isRefundStandalone = false;
        for (final PaymentTransactionEntryModel ptem : refundTransactionEntries) {
            final TenderInfo tenderInfoFromPaymentEntry = tlogPaymentEntryConverter.getTenderInfoFromPaymentEntry(ptem,
                    true);
            if (tenderInfoFromPaymentEntry != null && "Y".equals(tenderInfoFromPaymentEntry.getApproved())) {
                tenderList.add(tenderInfoFromPaymentEntry);
                tenderAmount = tenderAmount.add(tenderInfoFromPaymentEntry.getAmount());
            }

            if (!isRefundStandalone && PaymentTransactionType.REFUND_STANDALONE.equals(ptem.getType())
                    && !PaymentProviderTypeEnum.IPG.getCode()
                            .equals(ptem.getPaymentTransaction().getPaymentProvider())) {
                isRefundStandalone = true;
            }
        }

        if (isRefundStandalone) {
            final PaymentTransactionModel paymentTransaction = refundTransactionEntries.get(0).getPaymentTransaction();
            final TenderInfo tenderInfoFromTransaction = tlogPaymentEntryConverter.getTenderInfoFromPaymentEntry(
                    PaymentTransactionHelper.findCaptureTransaction(ImmutableList.of(paymentTransaction)),
                    false);
            if (tenderInfoFromTransaction != null) {
                tenderAmount = tenderAmount.add(tenderInfoFromTransaction.getAmount());
                tenderList.add(tenderInfoFromTransaction);
            }
        }

        return tenderAmount;
    }

    /**
     * @param tlogPaymentEntryConverter
     *            the tlogPaymentEntryConverter to set
     */
    @Required
    public void setTlogPaymentEntryConverter(final TlogPaymentEntryConverter tlogPaymentEntryConverter) {
        this.tlogPaymentEntryConverter = tlogPaymentEntryConverter;
    }

}
