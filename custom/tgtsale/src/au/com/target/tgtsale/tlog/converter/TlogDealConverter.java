/**
 * 
 */
package au.com.target.tgtsale.tlog.converter;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.promotions.model.TargetPromotionOrderEntryConsumedModel;

import java.util.Map;

import au.com.target.tgtsale.tlog.data.DealResults;
import au.com.target.tgtsale.tlog.data.ItemDeal;


/**
 * @author mjanarth
 *
 */
public interface TlogDealConverter {

    /**
     * This method populates the ItemDeal and also gets the relevant info from the itemdeal for Transaction Deal for
     * that instance
     * 
     * @param orderEntry
     * @param dealResults
     * @param deals
     * @return itemDeal with values populated
     */
    public ItemDeal populateItemDeal(final AbstractOrderEntryModel orderEntry,
            final TargetPromotionOrderEntryConsumedModel entryConsumedModel,
            final DealResults dealResults, final Map<String, String> deals);
}
