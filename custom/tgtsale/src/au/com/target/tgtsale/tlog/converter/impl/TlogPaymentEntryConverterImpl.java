/**
 * 
 */
package au.com.target.tgtsale.tlog.converter.impl;

import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.IpgGiftCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtpayment.constants.TgtpaymentConstants;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;
import au.com.target.tgtpayment.model.ZippayPaymentInfoModel;
import au.com.target.tgtsale.tlog.converter.TlogPaymentEntryConverter;
import au.com.target.tgtsale.tlog.data.TenderInfo;
import au.com.target.tgtsale.tlog.data.TenderTypeEnum;


/**
 * @author mjanarth
 *
 */
public class TlogPaymentEntryConverterImpl implements TlogPaymentEntryConverter {



    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogPaymentEntryConverter#getTenderInfoFromPaymentEntry(de.hybris.platform.payment.model.PaymentTransactionEntryModel, boolean)
     */
    @Override
    public TenderInfo getTenderInfoFromPaymentEntry(
            final PaymentTransactionEntryModel paymentTransactionEntryModel, final boolean refund) {
        if (paymentTransactionEntryModel == null) {
            return null;
        }

        final TenderInfo tender = new TenderInfo();
        final String paymentProvider = paymentTransactionEntryModel.getPaymentTransaction().getPaymentProvider();

        if (TgtpaymentConstants.PAY_NOW_PAYMENT_TYPE.equalsIgnoreCase(paymentProvider)
                || (TgtpaymentConstants.IPG_PAYMENT_TYPE.equalsIgnoreCase(paymentProvider)
                        && paymentTransactionEntryModel.getIpgPaymentInfo() == null)) {
            tender.setType(TenderTypeEnum.CASH);
        }
        else {
            if (TgtpaymentConstants.PAY_PAL_PAYMENT_TYPE.equalsIgnoreCase(paymentProvider)) {
                tender.setType(TenderTypeEnum.PAYPAL);
            }
            else if (TgtpaymentConstants.AFTERPAY_PAYMENT_TYPE.equalsIgnoreCase(paymentProvider)) {
                tender.setType(TenderTypeEnum.AFTERPAY);
            }
            else if (TgtpaymentConstants.ZIPPAY_PAYMENT_TYPE.equalsIgnoreCase(paymentProvider)) {
                tender.setType(TenderTypeEnum.ZIP);
            }
            else {
                tender.setType(TenderTypeEnum.EFT);
            }
            tender.setProcessedTime(null != paymentTransactionEntryModel.getTime() ? paymentTransactionEntryModel
                    .getTime() : paymentTransactionEntryModel.getCreationtime());
        }
        tender.setAmount(refund ? paymentTransactionEntryModel.getAmount().negate() : paymentTransactionEntryModel
                .getAmount());
        tender.setApproved(TransactionStatus.ACCEPTED.toString().equals(
                paymentTransactionEntryModel.getTransactionStatus()) ? "Y" : "N");

        final PaymentInfoModel paymentInfo;
        if (paymentTransactionEntryModel.getIpgPaymentInfo() != null) {
            paymentInfo = paymentTransactionEntryModel.getIpgPaymentInfo();
        }
        else {
            paymentInfo = paymentTransactionEntryModel.getPaymentTransaction().getInfo();
        }

        if (TenderTypeEnum.EFT.equals(tender.getType())) {
            if (paymentInfo instanceof CreditCardPaymentInfoModel) {
                tender.setCardNumber(((CreditCardPaymentInfoModel)paymentInfo).getNumber().replace("x", "0")
                        .replace("*", "0"));
                tender.setCreditCardRRN(paymentTransactionEntryModel.getReceiptNo());
            }
            else if (paymentInfo instanceof PinPadPaymentInfoModel) {
                final PinPadPaymentInfoModel pinPadPaymentInfoModel = (PinPadPaymentInfoModel)paymentInfo;
                tender.setCardNumber(pinPadPaymentInfoModel.getMaskedCardNumber().replace("x", "0"));
                tender.setCreditCardRRN(pinPadPaymentInfoModel.getRrn());
            }
            else if (paymentInfo instanceof IpgGiftCardPaymentInfoModel) {
                final IpgGiftCardPaymentInfoModel giftcardPaymentInfoModel = (IpgGiftCardPaymentInfoModel)paymentInfo;
                tender.setCardNumber(giftcardPaymentInfoModel.getMaskedNumber().replace("*", "0"));
                tender.setCreditCardRRN(paymentTransactionEntryModel.getReceiptNo());
            }
        }

        if (TenderTypeEnum.PAYPAL.equals(tender.getType())) {
            String payerId = null;
            if (paymentInfo instanceof PaypalPaymentInfoModel) {
                payerId = ((PaypalPaymentInfoModel)paymentInfo).getPayerId();
            }
            if (StringUtils.isEmpty(payerId)) {
                // should never happen, but just to play it safe
                payerId = "PAYPAL";
            }

            tender.setPaypalPayerId(payerId);
            tender.setPaypalRRN(paymentTransactionEntryModel.getReceiptNo());
        }

        if (TenderTypeEnum.AFTERPAY.equals(tender.getType()) && paymentInfo instanceof AfterpayPaymentInfoModel) {
            tender.setAfterPayOrderId(((AfterpayPaymentInfoModel)paymentInfo).getAfterpayOrderId());
            tender.setAfterPayRefundId(StringUtils
                    .defaultString(((AfterpayPaymentInfoModel)paymentInfo).getAfterpayRefundId(), StringUtils.EMPTY));
        }
 else if (TenderTypeEnum.ZIP.equals(tender.getType()) && paymentInfo instanceof ZippayPaymentInfoModel) {
            tender.setOrderId(((ZippayPaymentInfoModel) paymentInfo).getReceiptNo());
            tender.setRefundId(
                    refund ? StringUtils.defaultString(paymentTransactionEntryModel.getReceiptNo(), StringUtils.EMPTY)
                            : StringUtils.EMPTY);
        }

        return tender;
    }
}
