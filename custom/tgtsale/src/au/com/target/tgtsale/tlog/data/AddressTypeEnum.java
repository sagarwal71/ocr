/**
 * 
 */
package au.com.target.tgtsale.tlog.data;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * @author vmuthura
 * 
 */
@XmlEnum(String.class)
public enum AddressTypeEnum
{
    @XmlEnumValue("delivery")
    DELIVERY("delivery"),

    @XmlEnumValue("billing")
    BILLING("billing");

    private String type;

    /**
     * @param type
     *            - Delivery type
     */
    private AddressTypeEnum(final String type)
    {
        this.type = type;
    }

    /**
     * @return type - Delivery type
     */
    public String getType()
    {
        return type;
    }
}
