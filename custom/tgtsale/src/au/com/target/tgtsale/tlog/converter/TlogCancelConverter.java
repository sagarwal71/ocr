/**
 * 
 */
package au.com.target.tgtsale.tlog.converter;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.returns.model.OrderEntryReturnRecordEntryModel;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import au.com.target.tgtsale.tlog.data.ItemInfo;
import au.com.target.tgtsale.tlog.data.ReasonTypeEnum;
import au.com.target.tgtsale.tlog.data.TenderInfo;
import au.com.target.tgtsale.tlog.data.Transaction;


/**
 * @author mjanarth
 *
 */
public interface TlogCancelConverter {

    /**
     * Gets the flybuys redeem code.
     * 
     * @param orderModel
     *            the order model
     * @return the flybuys redeem code
     */
    public String getFlybuysRedeemCode(final OrderModel orderModel);

    /**
     * Adds the flybuys discount info.
     * 
     * @param transaction
     *            the transaction
     * @param orderModRecordEntryModel
     *            the order mod record entry model
     * @param flybuysCardNumber
     *            the flybuys card number
     */

    public void addFlybuysDiscountInfo(final Transaction transaction,
            final OrderModificationRecordEntryModel orderModRecordEntryModel, final String flybuysCardNumber);

    /**
     * 
     * @param orderEntryModificationRecordEntries
     * @param tenderAmount
     * @param isFlybuysApplied
     * @return List<ItemInfo>
     */
    public List<ItemInfo> getItemListForModifiedEntries(
            final Collection<OrderEntryModificationRecordEntryModel> orderEntryModificationRecordEntries,
            final BigDecimal tenderAmount, final boolean isFlybuysApplied);


    /**
     * Convert a {@link AbstractOrderEntryModel} to {@link ItemInfo}
     * 
     * @param orderEntryModel
     *            - a {@link AbstractOrderEntryModel}
     * @param quantity
     * @return- {@link ItemInfo}
     */
    public ItemInfo getItemInfoFromOrderEntry(final AbstractOrderEntryModel orderEntryModel,
            final Long quantity);


    /**
     * @param tenderAmount
     * @param actualTotalPrice
     * @param itemInfo
     * @return BigDecimal which is the portion of the price to be set on the item based on the tender values
     */
    public BigDecimal getFinalPriceForItem(final BigDecimal tenderAmount, final BigDecimal actualTotalPrice,
            final ItemInfo itemInfo, final BigDecimal quantity);

    /**
     * Given a list of PaymentTransactionEntryModel, returns a list of TenderInfo. Handles the condition for
     * PaymentTransactionType#REFUND_FOLLOW_ON, where the one dollar capture is also included, as part of the refund
     * tlog. The assumption here is, the capture and the standalone refund are part of a single
     * {@link de.hybris.platform.payment.model.PaymentTransactionModel}.
     * 
     * @param tenderList
     * @return list of TenderInfo
     */
    public BigDecimal getTenderInfoForRefund(final List<TenderInfo> tenderList,
            List<PaymentTransactionEntryModel> refundTransactionEntries);


    /**
     * @param orderCancelRecordEntryModel
     * @return ReasonTypeEnum
     */
    ReasonTypeEnum getCancelReasonForEntry(OrderCancelRecordEntryModel orderCancelRecordEntryModel);

    /**
     * @param orderEntryReturnRecordEntryModel
     * @return ReasonTypeEnum
     */
    ReasonTypeEnum getRefundReasonForEntry(OrderEntryReturnRecordEntryModel orderEntryReturnRecordEntryModel);
}
