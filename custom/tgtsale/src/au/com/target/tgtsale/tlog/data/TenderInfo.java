/**
 * 
 */
package au.com.target.tgtsale.tlog.data;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import au.com.target.tgtsale.tlog.data.adapter.TlogTimestampAdapter;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TenderInfo {

    @XmlAttribute(name = "amt")
    private BigDecimal amount;

    @XmlAttribute(name = "type")
    private TenderTypeEnum type;

    @XmlAttribute(name = "card")
    private String cardNumber;

    @XmlAttribute(name = "rrn")
    private String creditCardRRN;

    @XmlAttribute(name = "paypalPayerId")
    private String paypalPayerId;

    @XmlAttribute(name = "paypalRRN")
    private String paypalRRN;

    @XmlAttribute
    private String afterPayOrderId;

    @XmlAttribute
    private String afterPayRefundId;

    @XmlAttribute
    private String orderId;

    @XmlAttribute
    private String refundId;

    @XmlAttribute(name = "processedTime")
    @XmlJavaTypeAdapter(TlogTimestampAdapter.class)
    private Date processedTime;

    @XmlAttribute(name = "approved")
    private String approved;


    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the type
     */
    public TenderTypeEnum getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final TenderTypeEnum type) {
        this.type = type;
    }

    /**
     * @return the cardNumber
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * @param cardNumber
     *            the cardNumber to set
     */
    public void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * @return the creditCardRRN
     */
    public String getCreditCardRRN() {
        return creditCardRRN;
    }

    /**
     * @param creditCardRRN
     *            the creditCardRRN to set
     */
    public void setCreditCardRRN(final String creditCardRRN) {
        this.creditCardRRN = creditCardRRN;
    }

    /**
     * @return the paypalPayerId
     */
    public String getPaypalPayerId() {
        return paypalPayerId;
    }

    /**
     * @param paypalPayerId
     *            the paypalPayerId to set
     */
    public void setPaypalPayerId(final String paypalPayerId) {
        this.paypalPayerId = paypalPayerId;
    }

    /**
     * @return the paypalRRN
     */
    public String getPaypalRRN() {
        return paypalRRN;
    }

    /**
     * @param paypalRRN
     *            the paypalRRN to set
     */
    public void setPaypalRRN(final String paypalRRN) {
        this.paypalRRN = paypalRRN;
    }

    /**
     * @return the afterPayOrderId
     */
    public String getAfterPayOrderId() {
        return afterPayOrderId;
    }

    /**
     * @param afterPayOrderId
     *            the afterPayOrderId to set
     */
    public void setAfterPayOrderId(final String afterPayOrderId) {
        this.afterPayOrderId = afterPayOrderId;
    }

    /**
     * @return the afterPayRefundId
     */
    public String getAfterPayRefundId() {
        return afterPayRefundId;
    }

    /**
     * @param afterPayRefundId
     *            the afterPayRefundId to set
     */
    public void setAfterPayRefundId(final String afterPayRefundId) {
        this.afterPayRefundId = afterPayRefundId;
    }

    /**
     * @return the processedTime
     */
    public Date getProcessedTime() {
        return processedTime;
    }

    /**
     * @param processedTime
     *            the processedTime to set
     */
    public void setProcessedTime(final Date processedTime) {
        this.processedTime = processedTime;
    }

    /**
     * @return the approved
     */
    public String getApproved() {
        return approved;
    }

    /**
     * @param approved
     *            the approved to set
     */
    public void setApproved(final String approved) {
        this.approved = approved;
    }


    /**
     * @return the orderId
     */
    public String getOrderId() { return orderId; }

    /**
     * @param orderId
     *            the orderId to set
     */
    public void setOrderId(final String orderId) { this.orderId = orderId; }

    /**
     * @return the refundId
     */
    public String getRefundId() { return refundId; }


    /**
     * @param refundId
     *            the refundId to set
     */
    public void setRefundId(final String refundId) { this.refundId = refundId; }
}
