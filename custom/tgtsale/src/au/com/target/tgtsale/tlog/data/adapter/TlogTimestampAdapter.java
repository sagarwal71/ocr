/**
 * 
 */
package au.com.target.tgtsale.tlog.data.adapter;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.lang.StringUtils;


/**
 * @author vmuthura
 * 
 */
public class TlogTimestampAdapter extends XmlAdapter<String, Date>
{

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

    @Override
    public String marshal(final Date date) throws Exception
    {
        if (date == null)
        {
            return null;
        }

        return dateFormat.format(date);
    }

    @Override
    public Date unmarshal(final String date) throws Exception
    {
        if (StringUtils.isEmpty(date))
        {
            return null;
        }

        return dateFormat.parse(date);
    }

}
