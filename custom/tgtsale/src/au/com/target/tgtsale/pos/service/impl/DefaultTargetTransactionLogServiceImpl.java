/**
 * 
 */
package au.com.target.tgtsale.pos.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.returns.model.OrderReturnRecordEntryModel;

import java.math.BigDecimal;
import java.util.List;

import au.com.target.tgtsale.tlog.TargetTransactionLogService;


/**
 * @author rsamuel3
 * 
 */
public class DefaultTargetTransactionLogServiceImpl implements TargetTransactionLogService {

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.TargetTransactionLogService#publishTlogForPayment(de.hybris.platform.payment.model.PaymentTransactionEntryModel)
     */
    @Override
    public void publishTlogForPayment(final PaymentTransactionEntryModel paymentTransactionEntryModel) {
        // YTODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.TargetTransactionLogService#publishTlogForOrderSale(de.hybris.platform.core.model.order.OrderModel)
     */
    @Override
    public void publishTlogForOrderSale(final OrderModel orderModel) {
        // YTODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.TargetTransactionLogService#publishTlogForOrderCancel(de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel)
     */
    @Override
    public void publishTlogForOrderCancel(final OrderCancelRecordEntryModel orderCancelRecordEntryModel,
            final List<PaymentTransactionEntryModel> refundTransactionEntries, final BigDecimal refundAmountRemaining) {
        // YTODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.TargetTransactionLogService#publishTlogForOrderReturn(de.hybris.platform.returns.model.OrderReturnRecordEntryModel)
     */
    @Override
    public void publishTlogForOrderReturn(final OrderReturnRecordEntryModel orderReturnRecordEntryModel,
            final List<PaymentTransactionEntryModel> refundTransactionEntries, final BigDecimal refundAmountRemaining) {
        // YTODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.TargetTransactionLogService#publishTlogForOrderRejected(de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel)
     */
    @Override
    public void publishTlogForOrderRejected(final OrderCancelRecordEntryModel orderCancelRecordEntryModel,
            final List<PaymentTransactionEntryModel> refundTransactionEntries) {
        // YTODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.TargetTransactionLogService#publishTlogForOrderZeroPick(de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel)
     */
    @Override
    public void publishTlogForOrderZeroPick(final OrderCancelRecordEntryModel orderCancelRecordEntryModel,
            final List<PaymentTransactionEntryModel> refundTransactionEntries) {
        // YTODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.TargetTransactionLogService#publishTlogForPreOrderCancel(de.hybris.platform.core.model.order.OrderModel, java.util.List)
     */
    @Override
    public void publishTlogForPreOrderCancel(final OrderModel orderModel,
            final List<PaymentTransactionEntryModel> refundTransactionEntries) {
        // Will not be implemented

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.TargetTransactionLogService#publishTlogForPreOrderFulfilment(de.hybris.platform.core.model.order.OrderModel)
     */
    @Override
    public void publishTlogForPreOrderFulfilment(final OrderProcessModel orderModel) {
        //  Will not be implemented

    }

}
