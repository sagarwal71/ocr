package au.com.target.tgtsale.tmd.impl;


import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.List;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.util.TargetServicesUtil;
import au.com.target.tgtsale.model.TargetTMDHotCardModel;
import au.com.target.tgtsale.tmd.TargetTMDHotCardDao;


public class TargetTMDHotCardDaoImpl extends DefaultGenericDao<TargetTMDHotCardModel> implements TargetTMDHotCardDao {

    private static final String QUERY_GET_TARGET_HOT_CARD_BY_CARDNUM = "SELECT {PK} "
            + " FROM { " + TargetTMDHotCardModel._TYPECODE + " } WHERE {CARDNUM} = ?cardNumber";

    public TargetTMDHotCardDaoImpl() {
        super(TargetTMDHotCardModel._TYPECODE);
    }

    @Override
    public TargetTMDHotCardModel getTargetHotCardModelByCardNumber(final long cardNumber)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException
    {
        final Long cardNumberObj = Long.valueOf(cardNumber);
        final SearchResult<TargetTMDHotCardModel> searchResult = getFlexibleSearchService().search(
                QUERY_GET_TARGET_HOT_CARD_BY_CARDNUM, Collections.singletonMap("cardNumber", cardNumberObj));
        final List<TargetTMDHotCardModel> models = searchResult.getResult();
        TargetServicesUtil.validateIfSingleResult(models, TargetTMDHotCardModel.class,
                TargetTMDHotCardModel.CARDNUM, cardNumberObj);
        return models.get(0);
    }
}