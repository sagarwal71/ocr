/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtsale.constants;

/**
 * Global class for all Tgtsale constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class TgtsaleConstants extends GeneratedTgtsaleConstants
{
    public static final String EXTENSIONNAME = "tgtsale";

    private TgtsaleConstants()
    {
        //empty to avoid instantiating this constant class
    }

    public interface TLOG {
        String PRE_ORDER_LAYBY_TYPE = "P";
        String PRE_ORDER_LAYBY_VERSION = "4";
    }
}
