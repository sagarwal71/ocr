/**
 * 
 */
package au.com.target.tgtsale.order;

import de.hybris.platform.core.model.order.OrderModel;

import au.com.target.tgtsale.exception.IllegalPosModificationException;
import au.com.target.tgtsale.integration.dto.TargetLaybyOrder;



public interface PosLaybyUpdater {

    /**
     * Modify the existing hybris order according to PosLaybyOrder
     * 
     * @param posLaybyOrder
     * @param order
     * @throws IllegalPosModificationException
     */
    void modifyLaybyOrder(TargetLaybyOrder posLaybyOrder, OrderModel order) throws IllegalPosModificationException;

    /**
     * 
     * Check if there is any changes from pos order to existing order
     * 
     * @param posLaybyOrder
     * @param order
     * @return true if there is no difference
     * @throws IllegalPosModificationException
     */
    boolean checkLaybyOrdersMatch(TargetLaybyOrder posLaybyOrder, OrderModel order) throws IllegalPosModificationException;

}
