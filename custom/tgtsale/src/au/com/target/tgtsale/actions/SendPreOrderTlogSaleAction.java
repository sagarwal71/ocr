/**
 * 
 */
package au.com.target.tgtsale.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import org.springframework.util.Assert;


/**
 * @author pvarghe2
 *
 */
public class SendPreOrderTlogSaleAction extends SendOrderTlogAction {
    @Override
    protected void publishTlog(final OrderProcessModel process) {
        Assert.notNull(process, "Order Process Model cannot be null");
        getTargetTransactionLogService().publishTlogForPreOrderFulfilment(process);
    }

}
