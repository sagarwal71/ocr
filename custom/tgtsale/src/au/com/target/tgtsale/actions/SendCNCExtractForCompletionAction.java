package au.com.target.tgtsale.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtsale.pos.service.TargetPOSExtractsService;


/**
 * Action responsible for notifying POS about the all shipments completion of CNC orders
 * 
 */
public class SendCNCExtractForCompletionAction extends AbstractProceduralAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(SendCNCExtractForCompletionAction.class);

    private TargetPOSExtractsService posExtractsService;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        final OrderModel order = process.getOrder();
        if (targetFeatureSwitchService.isFeatureEnabled("cncOrderExtractOnConsignment")) {
            Assert.notNull(order, "OrderProcessModel must not be null");
            getPosExtractsService().sendCncOrderExtractForCompletion(order);
            LOG.info("Shipment confirmation of CNC Order completion: " + order.getCode()
                    + " published to POS successfully");
        }
        else {
            LOG.info("Skip the Shipment confirmation of CNC Order completion: " + order.getCode()
                    + "because the feature switch=cncOrderExtractOnConsignment is off");
        }

    }

    /**
     * @return the posExtractsService
     */

    public TargetPOSExtractsService getPosExtractsService() {
        return posExtractsService;
    }

    /**
     * @param posExtractsService
     *            the posExtractsService to set
     */
    @Required
    public void setPosExtractsService(final TargetPOSExtractsService posExtractsService) {
        this.posExtractsService = posExtractsService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

}
