/**
 *
 */
package au.com.target.tgtwishlist.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.wishlist2.impl.daos.impl.DefaultWishlist2Dao;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.integration.dto.TargetIntegrationErrorDto;
import au.com.target.tgtcore.integration.dto.TargetIntegrationResponseDto;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtwishlist.dao.impl.TargetWishListDaoImpl;
import au.com.target.tgtwishlist.data.TargetWishListEntryData;
import au.com.target.tgtwishlist.data.TargetWishListSendRequestDto;
import au.com.target.tgtwishlist.enums.WishListTypeEnum;
import au.com.target.tgtwishlist.enums.WishListVisibilityEnum;
import au.com.target.tgtwishlist.exception.AddToWishListException;
import au.com.target.tgtwishlist.logger.TgtWishListLogger;
import au.com.target.tgtwishlist.model.TargetWishListEntryModel;
import au.com.target.tgtwishlist.model.TargetWishListModel;
import au.com.target.tgtwishlist.sharewishlist.client.ShareWishListClient;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetWishListServiceImplTest {

    @InjectMocks
    private final TargetWishListServiceImpl targetWishListServiceImpl = new TargetWishListServiceImpl();

    @Mock
    private TargetWishListDaoImpl targetWishListDao;

    @Mock
    private DefaultWishlist2Dao wishlistDao;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetProductService targetProductService;

    @Mock
    private TargetProductModel targetProductModelMock;

    @Mock
    private ProductModel productModelMock;

    @Mock
    private Wishlist2EntryModel wishListEntryModelMock;

    @Mock
    private TargetWishListEntryModel targetWishListEntryModelMock;

    @Mock
    private TargetWishListModel targetWishListModelMock;

    @Mock
    private TargetCustomerModel targetCustomerModel;


    @Mock
    private SalesApplication salesApplication;

    @Mock
    private ShareWishListClient shareWishListClient;

    private ArgumentCaptor<TargetWishListEntryModel> targetWishListEntryModelCaptor;

    @Before
    public void setup() {
        given(targetCustomerModel.getPk()).willReturn(PK.fromLong(1000));
        given(targetWishListModelMock.getUser()).willReturn(targetCustomerModel);
        targetWishListEntryModelCaptor = ArgumentCaptor
                .forClass(TargetWishListEntryModel.class);
    }

    @Test
    public void testAddToListProductIsAlreadyPresentAndAddedTimeIsNewer() {
        given(targetProductService.getProductForCode("P1000")).willReturn(targetProductModelMock);
        given(targetProductModelMock.getCode()).willReturn("P1000");
        final List<Wishlist2EntryModel> entryModelList = new ArrayList<>();
        entryModelList.add(targetWishListEntryModelMock);
        given(wishlistDao.findWishlistEntryByProduct(targetProductModelMock, targetWishListModelMock))
                .willReturn(
                        entryModelList);
        given(targetWishListEntryModelMock.getAddedDate()).willReturn(DateUtils.addDays(new Date(), -1));

        final TargetWishListModel targetWishListModel = targetWishListServiceImpl.addProductToWishList(
                targetWishListModelMock, createWishListEntryData("P1000", "123123"),
                TgtWishListLogger.SYNC_FAV_LIST_ACTION, salesApplication);

        verify(modelService).save(targetWishListEntryModelCaptor.capture());
        assertThat(targetWishListModel).isNotNull();
        assertThat(targetWishListEntryModelCaptor.getValue()).isEqualTo(targetWishListEntryModelMock);
    }

    @Test
    public void testAddToListProductIsAlreadyPresentAndAddedTimeIsOlder() {
        given(targetProductService.getProductForCode("P1000")).willReturn(targetProductModelMock);
        given(targetProductModelMock.getCode()).willReturn("P1000");
        final List<Wishlist2EntryModel> entryModel = new ArrayList<>();
        entryModel.add(targetWishListEntryModelMock);
        given(wishlistDao.findWishlistEntryByProduct(targetProductModelMock, targetWishListModelMock))
                .willReturn(
                        entryModel);
        given(targetWishListEntryModelMock.getAddedDate()).willReturn(DateUtils.addDays(new Date(), 1));
        final TargetWishListModel targetWishListModel = targetWishListServiceImpl.addProductToWishList(
                targetWishListModelMock, createWishListEntryData("P1000", "123123"),
                TgtWishListLogger.ADD_FAV_LIST_ACTION, salesApplication);
        verify(modelService, never()).save(targetWishListEntryModelMock);
        assertThat(targetWishListModel).isNotNull();
    }

    @Test
    public void testAddToListProductIsNotPresentInWishList() {
        given(targetProductService.getProductForCode("P1000")).willReturn(targetProductModelMock);
        given(targetProductModelMock.getCode()).willReturn("P1000");
        given(wishlistDao.findWishlistEntryByProduct(targetProductModelMock, targetWishListModelMock))
                .willThrow(
                        new UnknownIdentifierException("Product Not Found"));

        given(modelService.create(TargetWishListEntryModel.class)).willReturn(targetWishListEntryModelMock);

        final TargetWishListModel targetWishListModel = targetWishListServiceImpl.addProductToWishList(
                targetWishListModelMock, createWishListEntryData("P1000", "123123"),
                TgtWishListLogger.ADD_FAV_LIST_ACTION, salesApplication);

        verify(modelService).create(TargetWishListEntryModel.class);
        verify(modelService).save(targetWishListEntryModelCaptor.capture());

        assertThat(targetWishListModel).isNotNull();
        assertThat(targetWishListEntryModelCaptor.getValue()).isEqualTo(targetWishListEntryModelMock);

    }

    @Test(expected = AddToWishListException.class)
    public void testAddToListProductWhenDuplicateEntryInList() {
        given(targetProductService.getProductForCode("P1000")).willReturn(targetProductModelMock);
        given(targetProductModelMock.getCode()).willReturn("P1000");
        given(wishlistDao.findWishlistEntryByProduct(targetProductModelMock, targetWishListModelMock))
                .willThrow(
                        new AmbiguousIdentifierException("More than one entry found"));

        targetWishListServiceImpl.addProductToWishList(targetWishListModelMock,
                createWishListEntryData("P1000", "123123"), TgtWishListLogger.ADD_FAV_LIST_ACTION, salesApplication);
    }

    @Test(expected = AddToWishListException.class)
    public void testAddToListIfWishListEntryIsNotOfTypeTargetWishListEntry() {
        given(targetProductService.getProductForCode("P1000")).willReturn(targetProductModelMock);
        given(targetProductModelMock.getCode()).willReturn("P1000");
        final List<Wishlist2EntryModel> entryModel = new ArrayList<>();
        entryModel.add(wishListEntryModelMock);
        given(wishlistDao.findWishlistEntryByProduct(targetProductModelMock, targetWishListModelMock))
                .willReturn(
                        entryModel);
        given(targetWishListEntryModelMock.getAddedDate()).willReturn(DateUtils.addDays(new Date(), -1));

        targetWishListServiceImpl.addProductToWishList(targetWishListModelMock,
                createWishListEntryData("P1000", "123123"), TgtWishListLogger.ADD_FAV_LIST_ACTION, salesApplication);
    }

    @Test(expected = AddToWishListException.class)
    public void testAddToListIfBaseProductCodeNotValid() {
        given(targetProductService.getProductForCode("P1000"))
                .willThrow(new UnknownIdentifierException("Product Not Found"));

        targetWishListServiceImpl.addProductToWishList(targetWishListModelMock,
                createWishListEntryData("P1000", "123123"), TgtWishListLogger.ADD_FAV_LIST_ACTION, salesApplication);
    }

    @Test
    public void testAddToListWhenMaxEntriesExceeded() {
        final List<Wishlist2EntryModel> entryModelList = new ArrayList<>();
        final TargetWishListEntryModel targetWishListEntryModelMock1 = mock(TargetWishListEntryModel.class);
        final TargetWishListEntryModel targetWishListEntryModelMock2 = mock(TargetWishListEntryModel.class);
        final TargetWishListEntryModel targetWishListEntryModelMock3 = mock(TargetWishListEntryModel.class);
        final TargetWishListEntryModel targetWishListEntryModelMock4 = mock(TargetWishListEntryModel.class);
        targetWishListServiceImpl.setMaxListSize(Integer.valueOf(4));

        entryModelList.add(targetWishListEntryModelMock1);
        entryModelList.add(targetWishListEntryModelMock2);
        entryModelList.add(targetWishListEntryModelMock3);
        entryModelList.add(targetWishListEntryModelMock4);

        given(targetWishListEntryModelMock1.getAddedDate()).willReturn(DateUtils.addDays(new Date(), -1));
        given(targetWishListEntryModelMock2.getAddedDate()).willReturn(new Date());
        given(targetWishListEntryModelMock3.getAddedDate()).willReturn(new Date());
        given(targetWishListEntryModelMock4.getAddedDate()).willReturn(new Date());

        given(targetProductService.getProductForCode("P1000")).willReturn(targetProductModelMock);

        given(targetWishListDao.findOldestWishlistEntryForAWishList(targetWishListModelMock, 1))
                .willReturn(Arrays.asList(targetWishListEntryModelMock1));

        given(modelService.create(TargetWishListEntryModel.class)).willReturn(targetWishListEntryModelMock);

        given(targetWishListModelMock.getEntries()).willReturn(entryModelList);

        given(wishlistDao.findWishlistEntryByProduct(targetProductModelMock, targetWishListModelMock))
                .willReturn(
                        new ArrayList<Wishlist2EntryModel>());
        given(targetWishListEntryModelMock.getAddedDate()).willReturn(new Date());

        targetWishListServiceImpl.addProductToWishList(targetWishListModelMock,
                createWishListEntryData("P1000", "1234"), TgtWishListLogger.ADD_FAV_LIST_ACTION, salesApplication);

        verify(modelService, times(1)).remove(targetWishListEntryModelMock1);
        verify(modelService, times(1)).save(targetWishListEntryModelMock);
    }

    @Test
    public void testRetrieveWishListWhenNotFound() {
        given(
                targetWishListDao.getWishListByNameUserAndType("Favourites", targetCustomerModel,
                        WishListTypeEnum.FAVOURITE))
                                .willThrow(new UnknownIdentifierException("WishListNotFound"));
        final TargetWishListModel result = targetWishListServiceImpl.retrieveWishList("Favourites",
                targetCustomerModel,
                WishListTypeEnum.FAVOURITE);
        assertThat(result).isNull();

    }

    @Test
    public void testRetrieveWishListWhenFound() {
        given(
                targetWishListDao.getWishListByNameUserAndType("Favourites", targetCustomerModel,
                        WishListTypeEnum.FAVOURITE))
                                .willReturn(targetWishListModelMock);
        final TargetWishListModel result = targetWishListServiceImpl.retrieveWishList("Favourites",
                targetCustomerModel,
                WishListTypeEnum.FAVOURITE);
        assertThat(result).isEqualTo(targetWishListModelMock);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testSyncWishListForUserNullUserModel() {
        targetWishListServiceImpl.syncWishListForUser(null, ListUtils.EMPTY_LIST, salesApplication);
        verifyZeroInteractions(targetWishListDao);
        verifyZeroInteractions(targetProductService);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testSyncWishListForUserEmptyWishlist() {
        final TargetWishListModel targetWishListModel = new TargetWishListModel();
        targetWishListModel.setUser(targetCustomerModel);
        given(modelService.create(TargetWishListModel.class)).willReturn(targetWishListModel);
        given(
                targetWishListDao.getWishListByNameUserAndType(anyString(),
                        any(TargetCustomerModel.class), any(WishListTypeEnum.class)))
                                .willReturn(
                                        targetWishListModel);
        final TargetWishListModel wishListModel = targetWishListServiceImpl.syncWishListForUser(targetCustomerModel,
                ListUtils.EMPTY_LIST, salesApplication);
        assertThat(wishListModel).isNotNull();
        verify(targetWishListDao).getWishListByNameUserAndType(anyString(),
                any(TargetCustomerModel.class), any(WishListTypeEnum.class));
        verifyNoMoreInteractions(targetWishListDao);
        verifyZeroInteractions(targetProductService);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testSyncWishListForUserDAOReturnsNullWishlist() {
        final List<TargetWishListEntryData> wishListData = createWishListData("12345", "567890");
        final ProductModel productModelMock1 = mock(TargetProductModel.class);
        final ProductModel productModelMock2 = mock(TargetProductModel.class);
        given(targetProductService.getProductForCode("P12345")).willReturn(productModelMock1);
        given(targetProductService.getProductForCode("P567890")).willReturn(productModelMock2);
        final TargetWishListModel targetWishListModel = new TargetWishListModel();
        targetWishListModel.setUser(targetCustomerModel);
        given(modelService.create(TargetWishListModel.class)).willReturn(targetWishListModel);
        given(modelService.create(TargetWishListEntryModel.class)).willReturn(targetWishListEntryModelMock);
        final TargetWishListModel wishListModel = targetWishListServiceImpl.syncWishListForUser(targetCustomerModel,
                wishListData, salesApplication);

        verifyWishListModel(targetCustomerModel, wishListModel);

        verify(targetWishListDao).getWishListByNameUserAndType(TargetWishListServiceImpl.FAVOURITE_LIST_NAME,
                targetCustomerModel, WishListTypeEnum.FAVOURITE);
        verifyNoMoreInteractions(targetWishListDao);
        verify(targetProductService, times(2)).getProductForCode(anyString());
        verifyNoMoreInteractions(targetProductService);
        verify(modelService).create(TargetWishListModel.class);
        verify(modelService, times(2)).create(TargetWishListEntryModel.class);
        verify(modelService, times(3)).save(anyObject());
        verify(modelService, times(2)).refresh(any(TargetWishListModel.class));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testSyncWishListForUserDAOReturnsTargetWishlistModel() {
        final List<TargetWishListEntryData> wishListData = createWishListData("12345", "567890");
        final TargetWishListModel targetWishListModel = new TargetWishListModel();
        targetWishListModel.setUser(targetCustomerModel);
        given(targetWishListDao.getWishListByNameUserAndType(TargetWishListServiceImpl.FAVOURITE_LIST_NAME,
                targetCustomerModel, WishListTypeEnum.FAVOURITE))
                        .willReturn(targetWishListModel);
        final ProductModel productModelMock1 = mock(TargetProductModel.class);
        final ProductModel productModelMock2 = mock(TargetProductModel.class);
        given(targetProductService.getProductForCode("P12345")).willReturn(productModelMock1);
        given(targetProductService.getProductForCode("P567890")).willReturn(productModelMock2);
        given(modelService.create(TargetWishListEntryModel.class)).willReturn(targetWishListEntryModelMock);
        final TargetWishListModel wishListModel = targetWishListServiceImpl.syncWishListForUser(targetCustomerModel,
                wishListData, salesApplication);

        assertThat(wishListModel).isNotNull().isSameAs(targetWishListModel);

        verify(targetWishListDao).getWishListByNameUserAndType(TargetWishListServiceImpl.FAVOURITE_LIST_NAME,
                targetCustomerModel, WishListTypeEnum.FAVOURITE);
        verifyNoMoreInteractions(targetWishListDao);
        verify(targetProductService, times(2)).getProductForCode(anyString());
        verifyNoMoreInteractions(targetProductService);
        verify(modelService, times(2)).create(TargetWishListEntryModel.class);
        verify(modelService, times(2)).save(anyObject());
        verify(modelService, times(2)).refresh(any(TargetWishListModel.class));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testSyncWishListForUserDAOReturnsWishlist() {
        final List<TargetWishListEntryData> wishListData = createWishListData("12345", "567890");
        final TargetWishListModel targetWishListModel = new TargetWishListModel();
        targetWishListModel.setUser(targetCustomerModel);
        given(targetWishListDao.getWishListByNameUserAndType(TargetWishListServiceImpl.FAVOURITE_LIST_NAME,
                targetCustomerModel, WishListTypeEnum.FAVOURITE))
                        .willReturn(targetWishListModel);
        final ProductModel productModelMock1 = mock(TargetProductModel.class);
        final ProductModel productModelMock2 = mock(TargetProductModel.class);
        given(targetProductService.getProductForCode("P12345")).willReturn(productModelMock1);
        given(targetProductService.getProductForCode("P567890")).willReturn(productModelMock2);
        given(modelService.create(TargetWishListEntryModel.class)).willReturn(targetWishListEntryModelMock);
        final TargetWishListModel wishListModel = targetWishListServiceImpl.syncWishListForUser(targetCustomerModel,
                wishListData, salesApplication);

        assertThat(wishListModel).isNotNull().isSameAs(targetWishListModel);

        verify(targetWishListDao).getWishListByNameUserAndType(TargetWishListServiceImpl.FAVOURITE_LIST_NAME,
                targetCustomerModel, WishListTypeEnum.FAVOURITE);
        verifyNoMoreInteractions(targetWishListDao);
        verify(targetProductService, times(2)).getProductForCode(anyString());
        verifyNoMoreInteractions(targetProductService);
        verify(modelService, times(2)).create(TargetWishListEntryModel.class);
        verify(modelService, times(2)).save(anyObject());
        verify(modelService, times(2)).refresh(any(TargetWishListModel.class));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testRemoveWishListForUserWithNoWishListForUser() {
        given(
                targetWishListDao.getWishListByNameUserAndType(TargetWishListServiceImpl.FAVOURITE_LIST_NAME,
                        targetCustomerModel, WishListTypeEnum.FAVOURITE))
                                .willReturn(null);
        final TargetWishListModel wishListModel = targetWishListServiceImpl.removeWishListEntriesForUser(
                targetCustomerModel,
                new ArrayList<TargetWishListEntryData>(), SalesApplication.WEB);

        assertThat(wishListModel).isNull();

        verify(targetWishListDao).getWishListByNameUserAndType(TargetWishListServiceImpl.FAVOURITE_LIST_NAME,
                targetCustomerModel, WishListTypeEnum.FAVOURITE);
        verifyNoMoreInteractions(targetWishListDao);
        verifyZeroInteractions(targetProductService, modelService);

    }

    @Test
    public void testRemoveWishListForUserWithWishListEntryDataEmpty() {
        given(
                targetWishListDao.getWishListByNameUserAndType(TargetWishListServiceImpl.FAVOURITE_LIST_NAME,
                        targetCustomerModel, WishListTypeEnum.FAVOURITE))
                                .willReturn(
                                        targetWishListModelMock);
        final TargetWishListModel wishListModel = targetWishListServiceImpl.removeWishListEntriesForUser(
                targetCustomerModel,
                new ArrayList<TargetWishListEntryData>(), SalesApplication.WEB);

        assertThat(wishListModel).isNotNull().isEqualTo(targetWishListModelMock);

        verify(targetWishListDao).getWishListByNameUserAndType(TargetWishListServiceImpl.FAVOURITE_LIST_NAME,
                targetCustomerModel, WishListTypeEnum.FAVOURITE);
        verifyNoMoreInteractions(targetWishListDao);
        verifyZeroInteractions(targetProductService, modelService);

    }

    @Test
    public void testRemoveWishListForUserWithValidWishListEntryData() {
        final List<Wishlist2EntryModel> entryModelList = new ArrayList<>();
        final TargetWishListEntryModel targetWishListEntryModelMock1 = mock(TargetWishListEntryModel.class);
        entryModelList.add(targetWishListEntryModelMock1);

        given(
                targetWishListDao.getWishListByNameUserAndType(TargetWishListServiceImpl.FAVOURITE_LIST_NAME,
                        targetCustomerModel, WishListTypeEnum.FAVOURITE))
                                .willReturn(
                                        targetWishListModelMock);

        given(targetProductService.getProductForCode(anyString())).willReturn(targetProductModelMock);
        given(targetWishListModelMock.getEntries()).willReturn(entryModelList);
        given(wishlistDao.findWishlistEntryByProduct(targetProductModelMock, targetWishListModelMock))
                .willReturn(entryModelList);
        final List<TargetWishListEntryData> wishlistsEntryData = createWishListData("213434");

        final TargetWishListModel wishListModel = targetWishListServiceImpl.removeWishListEntriesForUser(
                targetCustomerModel,
                wishlistsEntryData, SalesApplication.WEB);

        assertThat(wishListModel).isNotNull().isEqualTo(targetWishListModelMock);

        verify(targetWishListDao).getWishListByNameUserAndType(TargetWishListServiceImpl.FAVOURITE_LIST_NAME,
                targetCustomerModel, WishListTypeEnum.FAVOURITE);
        verify(wishlistDao).findWishlistEntryByProduct(targetProductModelMock, targetWishListModelMock);
        verify(targetProductService).getProductForCode(anyString());
        verify(modelService).save(targetWishListModelMock);
        verify(modelService).refresh(targetWishListModelMock);
        verifyNoMoreInteractions(targetWishListDao, targetProductService, modelService);
    }

    @Test
    public void testRemoveWishListForUserWithValidWishListEntryDataButThrowsException() {
        final List<Wishlist2EntryModel> entryModelList = new ArrayList<>();
        final TargetWishListEntryModel targetWishListEntryModelMock1 = mock(TargetWishListEntryModel.class);
        entryModelList.add(targetWishListEntryModelMock1);

        given(
                targetWishListDao.getWishListByNameUserAndType(TargetWishListServiceImpl.FAVOURITE_LIST_NAME,
                        targetCustomerModel, WishListTypeEnum.FAVOURITE))
                                .willReturn(targetWishListModelMock);

        given(targetProductService.getProductForCode(anyString())).willReturn(productModelMock);

        final List<TargetWishListEntryData> wishlistsEntryData = createWishListData("213434");

        final TargetWishListModel wishListModel = targetWishListServiceImpl.removeWishListEntriesForUser(
                targetCustomerModel,
                wishlistsEntryData, SalesApplication.WEB);

        assertThat(wishListModel).isNotNull().isEqualTo(targetWishListModelMock);

        verify(targetWishListDao).getWishListByNameUserAndType(TargetWishListServiceImpl.FAVOURITE_LIST_NAME,
                targetCustomerModel, WishListTypeEnum.FAVOURITE);
        verify(targetProductService).getProductForCode(anyString());
        verify(modelService).refresh(targetWishListModelMock);
        verifyNoMoreInteractions(targetWishListDao, targetProductService, modelService);
    }

    @Test
    public void testShareWishListWhenSucessful() {
        final TargetWishListSendRequestDto request = mock(TargetWishListSendRequestDto.class);
        final TargetIntegrationResponseDto response = mock(TargetIntegrationResponseDto.class);
        given(Boolean.valueOf(response.isSuccess())).willReturn(Boolean.TRUE);
        given(shareWishListClient.shareWishList(request)).willReturn(response);
        assertThat(targetWishListServiceImpl.shareWishList(request)).isTrue();

    }

    @Test
    public void testShareWishListWhenFailed() {
        final TargetWishListSendRequestDto request = mock(TargetWishListSendRequestDto.class);
        final TargetIntegrationResponseDto response = mock(TargetIntegrationResponseDto.class);
        given(Boolean.valueOf(response.isSuccess())).willReturn(Boolean.FALSE);
        final List<TargetIntegrationErrorDto> errorsList = new ArrayList<>();
        given(response.getErrorList()).willReturn(errorsList);
        given(shareWishListClient.shareWishList(request)).willReturn(response);
        assertThat(targetWishListServiceImpl.shareWishList(request)).isFalse();

    }

    @Test
    public void testShareWishListWhenResponseIsNull() {
        final TargetWishListSendRequestDto request = mock(TargetWishListSendRequestDto.class);
        given(shareWishListClient.shareWishList(request)).willReturn(null);
        assertThat(targetWishListServiceImpl.shareWishList(request)).isFalse();

    }

    @Test
    public void testUpdateWishListsWithNewBaseProductCodeWithNullWishList() {
        given(targetWishListDao.findWishListEntriesWithVariant(anyListOf(String.class)))
                .willReturn(null);
        targetWishListServiceImpl.updateWishListsWithNewBaseProductCode("P1000", Arrays.asList("P1000_Black_L"));
        verify(modelService, times(0)).saveAll(Mockito.anyCollection());
    }

    @Test
    public void testUpdateWishListsWithNewBaseProductCodeWithEmptyWishList() {
        given(targetWishListDao.findWishListEntriesWithVariant(anyListOf(String.class)))
                .willReturn(new ArrayList<TargetWishListEntryModel>());
        targetWishListServiceImpl.updateWishListsWithNewBaseProductCode("P1000", Arrays.asList("P1000_Black_L"));
        verify(modelService, times(0)).saveAll(Mockito.anyCollection());
    }

    @Test
    public void testUpdateWishListsWithNewBaseProductCode() {
        given(targetProductService.getProductForCode("P1000")).willReturn(targetProductModelMock);
        given(targetProductModelMock.getCode()).willReturn("P1000");
        final List<TargetWishListEntryModel> entryModelList = new ArrayList<>();
        entryModelList.add(targetWishListEntryModelMock);
        given(targetWishListEntryModelMock.getPk()).willReturn(PK.fromLong(87654321));
        given(targetWishListDao.findWishListEntriesWithVariant(anyListOf(String.class)))
                .willReturn(entryModelList);
        targetWishListServiceImpl.updateWishListsWithNewBaseProductCode("P1000", Arrays.asList("P1000_Black_L"));
        verify(targetWishListEntryModelMock).setProduct(targetProductModelMock);
        verify(modelService).saveAll(entryModelList);
    }

    @Test
    public void testUpdateWishListsWithNewBaseProductCodeWithModelSavingException() {
        given(targetProductService.getProductForCode("P1000")).willReturn(targetProductModelMock);
        given(targetProductModelMock.getCode()).willReturn("P1000");
        final List<TargetWishListEntryModel> entryModelList = new ArrayList<>();
        entryModelList.add(targetWishListEntryModelMock);
        given(targetWishListEntryModelMock.getPk()).willReturn(PK.fromLong(87654321));
        given(targetWishListDao.findWishListEntriesWithVariant(anyListOf(String.class)))
                .willReturn(entryModelList);
        doThrow(new ModelSavingException("save all failed")).when(modelService).saveAll(entryModelList);
        targetWishListServiceImpl.updateWishListsWithNewBaseProductCode("P1000", Arrays.asList("P1000_Black_L"));
    }

    @Test
    public void testReplaceFavouritesWithLatestWithNullWishListModel() {
        targetWishListServiceImpl.replaceFavouritesWithLatest(targetCustomerModel, createWishListData("1000", "1001"),
                salesApplication);
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testReplaceFavouritesWithLatestWithEmptyWishListData() {
        given(targetWishListServiceImpl.retrieveWishList(StringUtils.EMPTY, targetCustomerModel,
                WishListTypeEnum.FAVOURITE)).willReturn(targetWishListModelMock);
        targetWishListServiceImpl.replaceFavouritesWithLatest(targetCustomerModel, createWishListData(),
                salesApplication);
        verify(modelService).removeAll(targetWishListModelMock.getEntries());
    }

    @Test
    public void testReplaceFavouritesWithLatestData() {
        final List<TargetWishListEntryData> targetWishListEntryDatas = createWishListData("12345");
        given(targetWishListServiceImpl.retrieveWishList(StringUtils.EMPTY, targetCustomerModel,
                WishListTypeEnum.FAVOURITE)).willReturn(targetWishListModelMock);
        final ProductModel productModelMock1 = mock(TargetProductModel.class);
        given(targetProductService.getProductForCode("P12345")).willReturn(productModelMock1);
        given(modelService.create(TargetWishListEntryModel.class)).willReturn(targetWishListEntryModelMock);
        final List<TargetWishListEntryModel> targetWishListEntryModels = new ArrayList<>();
        targetWishListEntryModels.add(targetWishListEntryModelMock);
        targetWishListServiceImpl.replaceFavouritesWithLatest(targetCustomerModel, targetWishListEntryDatas,
                salesApplication);
        verify(modelService).removeAll(targetWishListModelMock.getEntries());
        verify(modelService).saveAll(targetWishListEntryModels);
    }

    @Test
    public void testReplaceFavouritesWithLatestThrowsException() {
        final List<TargetWishListEntryData> targetWishListEntryDatas = createWishListData("12345");
        given(targetWishListServiceImpl.retrieveWishList(StringUtils.EMPTY, targetCustomerModel,
                WishListTypeEnum.FAVOURITE)).willReturn(targetWishListModelMock);
        given(targetProductService.getProductForCode("P12345")).willReturn(targetProductModelMock);
        final TargetWishListEntryModel targetWishListEntryModel = mock(TargetWishListEntryModel.class);
        given(modelService.create(TargetWishListEntryModel.class)).willReturn(targetWishListEntryModel);
        doThrow(new ModelSavingException("exception thrown")).when(modelService).saveAll();
        targetWishListServiceImpl.replaceFavouritesWithLatest(targetCustomerModel, targetWishListEntryDatas,
                salesApplication);
        verify(modelService).removeAll(targetWishListModelMock.getEntries());
    }

    /**
     * @param targetCustomer
     * @param wishListModel
     */
    protected void verifyWishListModel(final TargetCustomerModel targetCustomer,
            final TargetWishListModel wishListModel) {
        assertThat(wishListModel).isNotNull();
        assertThat(wishListModel.getName()).isNotNull().isNotEmpty()
                .isEqualTo(TargetWishListServiceImpl.FAVOURITE_LIST_NAME);
        assertThat(wishListModel.getType()).isNotNull().isEqualTo(WishListTypeEnum.FAVOURITE);
        assertThat(wishListModel.getVisibility()).isNotNull().isEqualTo(WishListVisibilityEnum.PRIVATE);
        assertThat(wishListModel.getUser()).isNotNull().isEqualTo(targetCustomer);
    }

    /**
     * @param variantCodes
     * @return list of wishlistData
     */
    private List<TargetWishListEntryData> createWishListData(final String... variantCodes) {
        final List<TargetWishListEntryData> wishListData = new ArrayList<>();
        for (final String variantCode : variantCodes) {
            wishListData.add(createWishListEntryData("P" + variantCode, variantCode));
        }
        return wishListData;
    }

    /**
     * 
     * @param productCode
     * @param variantCode
     * @return target wish list entry data
     */
    private TargetWishListEntryData createWishListEntryData(final String productCode, final String variantCode) {
        final TargetWishListEntryData data = new TargetWishListEntryData();
        data.setBaseProductCode(productCode);
        data.setSelectedVariantCode(variantCode);
        data.setTimeStamp(String.valueOf(new Date().getTime()));
        return data;
    }

}
