/**
 *
 */
package au.com.target.tgtwishlist.generator;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtwishlist.model.TargetWishListModel;


/**
 * @author pthoma20
 *
 */
public class TargetWishListCodeGenerator {

    private static final int LENGTH_OF_NAME_FOR_ID = 20;

    private TargetWishListCodeGenerator() {
        //Prevent instantiation
    }

    /**
     * This method will generate the code for the target wish list based on the user pk , list name and the list type
     *
     * @param targetWishListModel
     * @return generatedcode
     */
    public static String generateKey(final TargetWishListModel targetWishListModel) {
        final StringBuilder code = new StringBuilder();
        if (targetWishListModel.getUser() != null) {
            code.append(targetWishListModel.getUser().getPk());
        }
        if (StringUtils.isNotBlank(targetWishListModel.getName())) {
            String nameForCode = targetWishListModel.getName().replaceAll("\\s+", "-");
            if (nameForCode.length() > LENGTH_OF_NAME_FOR_ID) {
                nameForCode = nameForCode.substring(0, LENGTH_OF_NAME_FOR_ID);
            }
            appendHyphenIfNotBlank(code);
            code.append(nameForCode);
        }
        if (targetWishListModel.getType() != null) {
            appendHyphenIfNotBlank(code);
            code.append(targetWishListModel.getType());
        }
        return code.toString();
    }

    private static void appendHyphenIfNotBlank(final StringBuilder code) {
        if (StringUtils.isNotBlank(code.toString())) {
            code.append("-");
        }
    }

}
