/**
 *
 */
package au.com.target.tgtwishlist.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtwishlist.generator.TargetWishListCodeGenerator;
import au.com.target.tgtwishlist.model.TargetWishListModel;


/**
 * @author paul
 *
 */
public class TargetWishListPrepareInterceptor implements PrepareInterceptor<TargetWishListModel> {

    /**
     * Prepare interceptor for Target Wish List which sets the code for the wish list.
     */
    @Override
    public void onPrepare(final TargetWishListModel targetWishListModel, final InterceptorContext ctx)
            throws InterceptorException {
        if (StringUtils.isBlank(targetWishListModel.getCode())) {
            targetWishListModel.setCode(TargetWishListCodeGenerator.generateKey(targetWishListModel));
        }

    }
}
