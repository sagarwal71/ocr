/**
 * 
 */
package au.com.target.tgtwishlist.data;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author rsamuel3
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TargetCustomerWishListDto {
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String ccme;
    private String notShownCount;
    private String accessUrl;
    private List<TargetRecipientDto> recipients;
    private List<TargetCustomerWishListProductsDto> products;

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * @param emailAddress
     *            the emailAddress to set
     */
    public void setEmailAddress(final String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * @return the ccme
     */
    public String getCcme() {
        return ccme;
    }

    /**
     * @param ccme
     *            the ccme to set
     */
    public void setCcme(final String ccme) {
        this.ccme = ccme;
    }

    /**
     * @return the notShownCount
     */
    public String getNotShownCount() {
        return notShownCount;
    }

    /**
     * @param notShownCount
     *            the notShownCount to set
     */
    public void setNotShownCount(final String notShownCount) {
        this.notShownCount = notShownCount;
    }

    /**
     * @return the accessUrl
     */
    public String getAccessUrl() {
        return accessUrl;
    }

    /**
     * @param accessUrl
     *            the accessUrl to set
     */
    public void setAccessUrl(final String accessUrl) {
        this.accessUrl = accessUrl;
    }

    /**
     * @return the recipients
     */
    public List<TargetRecipientDto> getRecipients() {
        return recipients;
    }

    /**
     * @param recipients
     *            the recipients to set
     */
    public void setRecipients(final List<TargetRecipientDto> recipients) {
        this.recipients = recipients;
    }

    /**
     * @return the products
     */
    public List<TargetCustomerWishListProductsDto> getProducts() {
        return products;
    }

    /**
     * @param products
     *            the products to set
     */
    public void setProducts(final List<TargetCustomerWishListProductsDto> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        final StringBuilder customerInfo = new StringBuilder();
        customerInfo.append("{notShownCount=").append(getNotShownCount()).append(",\n");
        for (final TargetCustomerWishListProductsDto product : getProducts()) {
            customerInfo.append(product.toString()).append("\n");
        }
        customerInfo.append("}");
        return customerInfo.toString();
    }
}
