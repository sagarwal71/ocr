/**
 * 
 */
package au.com.target.tgtwishlist.data;

import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author rsamuel3
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TargetRecipientDto {
    private String emailAddress;
    private String name;
    private String message;

    /**
     * @return the emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * @param emailAddress
     *            the emailAddress to set
     */
    public void setEmailAddress(final String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(final String message) {
        this.message = message;
    }
}
