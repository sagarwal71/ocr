/**
 * 
 */
package au.com.target.tgtfraud.actions;

import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import java.math.BigDecimal;

import org.springframework.util.Assert;

import au.com.target.tgtfraud.exception.FraudCheckServiceUnavailableException;


/**
 * Send to fraud check service eg Accertify for a cancel/reject/zeropick
 * 
 */
public class SendCancelRefundForFraudCheckAction extends AbstractSendDetailsForFraudCheckAction {

    @Override
    protected FraudServiceResponse sendDetails(final OrderProcessModel process)
            throws FraudCheckServiceUnavailableException {

        final OrderCancelRecordEntryModel cancelRequestModel = getOrderProcessParameterHelper()
                .getOrderCancelRequest(process);
        final BigDecimal refundAmount = getOrderProcessParameterHelper().getRefundAmount(process);
        if (refundAmount != null && refundAmount.doubleValue() > 0d) {
            cancelRequestModel.setRefundedAmount(Double.valueOf(refundAmount.doubleValue()));
        }
        Assert.notNull(cancelRequestModel, "cancelRequest may not be null");

        try {
            return targetFraudService.submitRefund(getProviderName(), cancelRequestModel,
                    getOrderProcessParameterHelper().getRefundPaymentEntryList(process));
        }
        catch (final FraudCheckServiceUnavailableException e) {
            throw e;
        }
        //CHECKSTYLE:OFF really don't want fraud check to terminate business process
        catch (final Throwable t) {
            throw new FraudCheckServiceUnavailableException("Unexpected error processing fraud verification", t);
        }
        //CHECKSTYLE:ON
    }
}
