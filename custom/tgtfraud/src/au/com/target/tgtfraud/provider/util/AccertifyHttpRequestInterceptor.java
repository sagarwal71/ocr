/**
 * 
 */
package au.com.target.tgtfraud.provider.util;

import java.io.IOException;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.support.HttpRequestWrapper;


/**
 * Utility class to intercept the request, response for debugging.
 * 
 * @author vmuthura
 * 
 */
public class AccertifyHttpRequestInterceptor implements ClientHttpRequestInterceptor
{

    @Override
    public ClientHttpResponse intercept(final HttpRequest httprequest, final byte[] body,
            final ClientHttpRequestExecution clienthttprequestexecution) throws IOException
    {

        final HttpRequestWrapper requestWrapper = new HttpRequestWrapper(httprequest);
        final ClientHttpResponse response = clienthttprequestexecution.execute(requestWrapper, body);

        return response;
    }

}
