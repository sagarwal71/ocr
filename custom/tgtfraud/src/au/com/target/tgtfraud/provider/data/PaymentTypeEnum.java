/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * @author vmuthura
 * 
 */
@XmlEnum(String.class)
public enum PaymentTypeEnum
{
    @XmlEnumValue("CreditCard")
    CREDITCARD("CreditCard"),

    @XmlEnumValue("PayPal")
    PAYPAL("PayPal"),

    @XmlEnumValue("PinPad")
    PINPAD("PinPad"),

    @XmlEnumValue("GiftCard")
    GIFTCARD("GiftCard"),
    
    @XmlEnumValue("AfterPay")
    AFTERPAY("AfterPay"),

    @XmlEnumValue("Zip")
    ZIPPAY("Zippay");

    private String type;

    /**
     * @param type
     *            - Payment type
     */
    private PaymentTypeEnum(final String type)
    {
        this.type = type;
    }

    /**
     * @return - Payment type
     */
    public String getType()
    {
        return this.type;
    }
}
