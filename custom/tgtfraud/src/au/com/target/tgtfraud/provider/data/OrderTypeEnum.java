/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * @author vmuthura
 * 
 */
@XmlEnum(String.class)
public enum OrderTypeEnum
{
    @XmlEnumValue("sale")
    SALE("sale"),

    @XmlEnumValue("refund")
    REFUND("refund"),

    @XmlEnumValue("preOrder")
    PREORDER("preOrder"),

    @XmlEnumValue("LAYBY-SALE")
    LAYBY_SALE("LAYBY-SALE"),

    @XmlEnumValue("LAYBY-PAYMENT")
    LAYBY_PAYMENT("LAYBY-PAYMENT");

    private String type;

    /**
     * @param type
     *            - The order type
     */
    private OrderTypeEnum(final String type)
    {
        this.type = type;
    }

    /**
     * @return - The order type
     */
    public String getType()
    {
        return this.type;
    }
}
