/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import javax.xml.bind.annotation.XmlEnum;


/**
 * @author vmuthura
 * 
 */
@XmlEnum
public enum RecommendationTypeEnum
{
    ACCEPT, REJECT, REVIEW;
}
