/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement
@XmlType(propOrder = {
        "failedpayPalRequestID",
        "failedpayPalEmail",
        "failedpayPalAmount",
        "failedpayPalStatus",
})
public class PayPalDetail
{
    private String failedpayPalRequestID;
    private String failedpayPalEmail;
    private String failedpayPalAmount;
    private String failedpayPalStatus;

    /**
     * @return the failedpayPalRequestID
     */
    public String getFailedpayPalRequestID()
    {
        return failedpayPalRequestID;
    }

    /**
     * @param failedpayPalRequestID
     *            the failedpayPalRequestID to set
     */
    public void setFailedpayPalRequestID(final String failedpayPalRequestID)
    {
        this.failedpayPalRequestID = failedpayPalRequestID;
    }

    /**
     * @return the failedpayPalEmail
     */
    public String getFailedpayPalEmail()
    {
        return failedpayPalEmail;
    }

    /**
     * @param failedpayPalEmail
     *            the failedpayPalEmail to set
     */
    public void setFailedpayPalEmail(final String failedpayPalEmail)
    {
        this.failedpayPalEmail = failedpayPalEmail;
    }

    /**
     * @return the failedpayPalAmount
     */
    public String getFailedpayPalAmount()
    {
        return failedpayPalAmount;
    }

    /**
     * @param failedpayPalAmount
     *            the failedpayPalAmount to set
     */
    public void setFailedpayPalAmount(final String failedpayPalAmount)
    {
        this.failedpayPalAmount = failedpayPalAmount;
    }

    /**
     * @return the failedpayPalStatus
     */
    public String getFailedpayPalStatus()
    {
        return failedpayPalStatus;
    }

    /**
     * @param failedpayPalStatus
     *            the failedpayPalStatus to set
     */
    public void setFailedpayPalStatus(final String failedpayPalStatus)
    {
        this.failedpayPalStatus = failedpayPalStatus;
    }

}
