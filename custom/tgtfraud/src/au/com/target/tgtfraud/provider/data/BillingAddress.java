/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement
@XmlType(propOrder = {
        "billingFullName",
        "billingFirstName",
        "billingLastName",
        "billingAddressLine1",
        "billingAddressLine2",
        "billingCity",
        "billingRegion",
        "billingPostalCode",
        "billingCountry",
        "billingPhone"
})
public class BillingAddress
{
    private String billingFullName;
    private String billingFirstName;
    private String billingLastName;
    private String billingAddressLine1;
    private String billingAddressLine2;
    private String billingCity;
    private String billingRegion;
    private String billingPostalCode;
    private String billingCountry;
    private String billingPhone;

    /**
     * @return the billingFullName
     */
    public String getBillingFullName()
    {
        return billingFullName;
    }

    /**
     * @param billingFullName
     *            the billingFullName to set
     */
    public void setBillingFullName(final String billingFullName)
    {
        this.billingFullName = billingFullName;
    }

    /**
     * @return the billingFirstName
     */
    public String getBillingFirstName()
    {
        return billingFirstName;
    }

    /**
     * @param billingFirstName
     *            the billingFirstName to set
     */
    public void setBillingFirstName(final String billingFirstName)
    {
        this.billingFirstName = billingFirstName;
    }

    /**
     * @return the billingLastName
     */
    public String getBillingLastName()
    {
        return billingLastName;
    }

    /**
     * @param billingLastName
     *            the billingLastName to set
     */
    public void setBillingLastName(final String billingLastName)
    {
        this.billingLastName = billingLastName;
    }

    /**
     * @return the billingAddressLine1
     */
    public String getBillingAddressLine1()
    {
        return billingAddressLine1;
    }

    /**
     * @param billingAddressLine1
     *            the billingAddressLine1 to set
     */
    public void setBillingAddressLine1(final String billingAddressLine1)
    {
        this.billingAddressLine1 = billingAddressLine1;
    }

    /**
     * @return the billingAddressLine2
     */
    public String getBillingAddressLine2()
    {
        return billingAddressLine2;
    }

    /**
     * @param billingAddressLine2
     *            the billingAddressLine2 to set
     */
    public void setBillingAddressLine2(final String billingAddressLine2)
    {
        this.billingAddressLine2 = billingAddressLine2;
    }

    /**
     * @return the billingCity
     */
    public String getBillingCity()
    {
        return billingCity;
    }

    /**
     * @param billingCity
     *            the billingCity to set
     */
    public void setBillingCity(final String billingCity)
    {
        this.billingCity = billingCity;
    }

    /**
     * @return the billingRegion
     */
    public String getBillingRegion()
    {
        return billingRegion;
    }

    /**
     * @param billingRegion
     *            the billingRegion to set
     */
    public void setBillingRegion(final String billingRegion)
    {
        this.billingRegion = billingRegion;
    }

    /**
     * @return the billingPostalCode
     */
    public String getBillingPostalCode()
    {
        return billingPostalCode;
    }

    /**
     * @param billingPostalCode
     *            the billingPostalCode to set
     */
    public void setBillingPostalCode(final String billingPostalCode)
    {
        this.billingPostalCode = billingPostalCode;
    }

    /**
     * @return the billingCountry
     */
    public String getBillingCountry()
    {
        return billingCountry;
    }

    /**
     * @param billingCountry
     *            the billingCountry to set
     */
    public void setBillingCountry(final String billingCountry)
    {
        this.billingCountry = billingCountry;
    }

    /**
     * @return the billingPhone
     */
    public String getBillingPhone()
    {
        return billingPhone;
    }

    /**
     * @param billingPhone
     *            the billingPhone to set
     */
    public void setBillingPhone(final String billingPhone)
    {
        this.billingPhone = billingPhone;
    }


}
