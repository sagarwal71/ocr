/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement
@XmlType(propOrder = { "totalFailedCCAttempts",
        "totalFailedPPAttempts",
        "totalFailedGCAttempts",
        "totalFailedAPAttempts",
        "totalFailedZIPAttempts",
        "failCCdetails",
        "failPPdetails",
        "failGCdetails",
        "failAPDetails",
        "failZIPDetails"
})
public class FailedPaymentInfo {
    private int totalFailedCCAttempts;
    private int totalFailedPPAttempts;
    private int totalFailedGCAttempts;
    private int totalFailedAPAttempts;
    private int totalFailedZIPAttempts;
    private List<CreditCardDetail> failCCdetails;
    private List<PayPalDetail> failPPdetails;
    private List<FailedGiftCardDetail> failGCdetails;
    private List<AfterpayDetail> failAPDetails;
    private List<ZipPaymentDetail> failZIPDetails;

    /**
     * @return the totalFailedCCAttempts
     */
    public int getTotalFailedCCAttempts() {
        return totalFailedCCAttempts;
    }

    /**
     * @param totalFailedCCAttempts
     *            the totalFailedCCAttempts to set
     */
    public void setTotalFailedCCAttempts(final int totalFailedCCAttempts) {
        this.totalFailedCCAttempts = totalFailedCCAttempts;
    }

    /**
     * @return the totalFailedPPAttempts
     */
    public int getTotalFailedPPAttempts() {
        return totalFailedPPAttempts;
    }

    /**
     * @param totalFailedPPAttempts
     *            the totalFailedPPAttempts to set
     */
    public void setTotalFailedPPAttempts(final int totalFailedPPAttempts) {
        this.totalFailedPPAttempts = totalFailedPPAttempts;
    }

    /**
     * @return the failCCdetails
     */
    public List<CreditCardDetail> getFailCCdetails() {
        return failCCdetails;
    }

    /**
     * @param failCCdetails
     *            the failCCdetails to set
     */
    public void setFailCCdetails(final List<CreditCardDetail> failCCdetails) {
        this.failCCdetails = failCCdetails;
    }

    /**
     * @return the failPPdetails
     */
    public List<PayPalDetail> getFailPPdetails() {
        return failPPdetails;
    }

    /**
     * @param failPPdetails
     *            the failPPdetails to set
     */
    public void setFailPPdetails(final List<PayPalDetail> failPPdetails) {
        this.failPPdetails = failPPdetails;
    }

    /**
     * @return the totalFailedGCAttempts
     */
    public int getTotalFailedGCAttempts() {
        return totalFailedGCAttempts;
    }

    /**
     * @param totalFailedGCAttempts
     *            the totalFailedGCAttempts to set
     */
    public void setTotalFailedGCAttempts(final int totalFailedGCAttempts) {
        this.totalFailedGCAttempts = totalFailedGCAttempts;
    }

    /**
     * @return the failGCdetails
     */
    public List<FailedGiftCardDetail> getFailGCdetails() {
        return failGCdetails;
    }

    /**
     * @param failGCdetails
     *            the failGCdetails to set
     */
    public void setFailGCdetails(final List<FailedGiftCardDetail> failGCdetails) {
        this.failGCdetails = failGCdetails;
    }

    /**
     * @return the totalFailedAPAttempts
     */
    public int getTotalFailedAPAttempts() {
        return totalFailedAPAttempts;
    }

    /**
     * @param totalFailedAPAttempts
     *            the totalFailedAPAttempts to set
     */
    public void setTotalFailedAPAttempts(final int totalFailedAPAttempts) {
        this.totalFailedAPAttempts = totalFailedAPAttempts;
    }

    /**
     * @return the failedAPDetails
     */
    public List<AfterpayDetail> getFailAPDetails() {
        return failAPDetails;
    }

    /**
     * @param failAPDetails
     *            the failedAPDetails to set
     */
    public void setFailAPDetails(final List<AfterpayDetail> failAPDetails) {
        this.failAPDetails = failAPDetails;
    }

    /**
     *
     * @return
     */
    public int getTotalFailedZIPAttempts() {
        return totalFailedZIPAttempts;
    }

    /**
     *
     * @param totalFailedZIPAttempts
     */
    public void setTotalFailedZIPAttempts(final int totalFailedZIPAttempts) {
        this.totalFailedZIPAttempts = totalFailedZIPAttempts;
    }

    /**
     *
     * @return
     */
    public List<ZipPaymentDetail> getFailZIPDetails() {
        return failZIPDetails;
    }

    /**
     *
     * @param failZIPDetails
     */
    public void setFailZIPDetails(final List<ZipPaymentDetail> failZIPDetails) {
        this.failZIPDetails = failZIPDetails;
    }
}
