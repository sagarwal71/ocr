/**
 * 
 */
package au.com.target.tgtfraud.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.fraud.FraudServiceProvider;
import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import java.util.Collection;
import java.util.List;

import au.com.target.tgtfraud.TargetFraudService;
import au.com.target.tgtfraud.TargetFraudServiceResponse;
import au.com.target.tgtfraud.provider.FraudServiceProviderEnum;
import au.com.target.tgtfraud.provider.data.RecommendationTypeEnum;


/**
 * Mock implementation of fraud check service which returns a simple response.
 * 
 */
public class TestFraudCheckService implements TargetFraudService {
    private final TargetFraudServiceResponse response;

    public TestFraudCheckService() {

        response = new TargetFraudServiceResponse(FraudServiceProviderEnum.ACCERTIFY.toString());
        response.setRecommendation(RecommendationTypeEnum.ACCEPT);
    }

    @Override
    public FraudServiceResponse submitPayment(final String provider, final OrderModel orderModel)
    {
        return response;
    }

    @Override
    public FraudServiceResponse submitRefund(final String provider,
            final OrderModificationRecordEntryModel orderModificationRecordEntryModel,
            final List<PaymentTransactionEntryModel> refundEntries)
    {
        return response;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.fraud.FraudService#recognizeOrderSymptoms(java.lang.String, de.hybris.platform.core.model.order.AbstractOrderModel)
     */
    @Override
    public FraudServiceResponse recognizeOrderSymptoms(final String s, final AbstractOrderModel abstractordermodel) {
        return response;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.fraud.FraudService#recognizeActivitySymptoms(java.lang.String, de.hybris.platform.core.model.user.UserModel)
     */
    @Override
    public FraudServiceResponse recognizeActivitySymptoms(final String s, final UserModel usermodel) {
        return response;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.fraud.FraudService#getProvider(java.lang.String)
     */
    @Override
    public FraudServiceProvider getProvider(final String s) {
        return null;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.fraud.FraudService#getProviders()
     */
    @Override
    public Collection<FraudServiceProvider> getProviders() {
        return null;
    }

    @Override
    public boolean isOrderReviewable(final OrderModel order) {
        return false;
    }
}
