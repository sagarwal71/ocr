/**
 * 
 */
package au.com.target.tgtfraud.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.fraud.model.FraudReportModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfraud.TargetFraudServiceResponse;
import au.com.target.tgtfraud.provider.data.RecommendationTypeEnum;
import au.com.target.tgtfraud.strategy.TargetFraudReportCreationStrategy;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FraudReportServiceImplTest {

    @Mock
    private ModelService modelService;

    @InjectMocks
    private final FraudReportServiceImpl fraudReportService = new FraudReportServiceImpl();

    @Mock
    private OrderModel order;

    @Mock
    private FraudReportModel fraudReport;
    @Mock
    private TargetFraudReportCreationStrategy targetFraudReportCreationStrategy;

    @Before
    public void setup() {
        BDDMockito.given(modelService.create(FraudReportModel.class)).willReturn(fraudReport);

    }

    @Test
    public void testCreateFraudReportDefaultFraudServiceResponse() {

        // For the default response we should always default to OK status
        final FraudServiceResponse response = Mockito.mock(FraudServiceResponse.class);
        BDDMockito.given(targetFraudReportCreationStrategy.createFraudReport(response, order, FraudStatus.OK))
                .willReturn(fraudReport);
        final FraudReportModel report = fraudReportService.createFraudReport(response, order);
        Assert.assertNotNull(report);
        Assert.assertEquals(fraudReport, report);
        Mockito.verify(targetFraudReportCreationStrategy).createFraudReport(response, order, FraudStatus.OK);
        Mockito.verify(modelService).save(fraudReport);

    }

    @Test
    public void testCreateFraudReportTargetFraudServiceResponse() {

        // For the target response the status should be derived from the recommendation in the response
        final TargetFraudServiceResponse response = Mockito.mock(TargetFraudServiceResponse.class);
        BDDMockito.given(response.getRecommendation()).willReturn(RecommendationTypeEnum.REVIEW);
        BDDMockito.given(targetFraudReportCreationStrategy.createFraudReport(response, order, FraudStatus.CHECK))
                .willReturn(fraudReport);
        final FraudReportModel report = fraudReportService.createFraudReport(response, order);
        Assert.assertNotNull(report);
        Assert.assertEquals(fraudReport, report);
        Mockito.verify(modelService).save(fraudReport);
        Mockito.verify(targetFraudReportCreationStrategy).createFraudReport(response, order, FraudStatus.CHECK);
    }

    @Test
    public void testGetStatusForRecommendation() {

        FraudStatus status = fraudReportService.getStatusForRecommendation(RecommendationTypeEnum.ACCEPT);
        Assert.assertEquals(FraudStatus.OK, status);

        status = fraudReportService.getStatusForRecommendation(RecommendationTypeEnum.REJECT);
        Assert.assertEquals(FraudStatus.FRAUD, status);

        status = fraudReportService.getStatusForRecommendation(RecommendationTypeEnum.REVIEW);
        Assert.assertEquals(FraudStatus.CHECK, status);

        // Defaults to OK if recommendation is not recognised
        status = fraudReportService.getStatusForRecommendation(null);
        Assert.assertEquals(FraudStatus.OK, status);

    }
}
