/**
 * 
 */
package au.com.target.tgtfraud.ordercancel.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.exceptions.OrderCancelRecordsHandlerException;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.ordercancel.TargetCancelService;
import au.com.target.tgtcore.ordercancel.TargetOrderCancelRequest;
import au.com.target.tgtcore.ordercancel.discountstrategy.VoucherCorrectionStrategy;
import au.com.target.tgtcore.ordercancel.util.OrderCancelHelper;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class RejectCancelServiceImplTest {

    @Mock
    private OrderModel order;

    @Mock
    private OrderCancelHelper orderCancelHelper;

    @Mock
    private TargetCancelService targetCancelService;

    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private final RejectCancelServiceImpl rejectCancelService = new RejectCancelServiceImpl() {

        @Override
        public TargetBusinessProcessService getTargetBusinessProcessService() {
            return targetBusinessProcessService;
        }
    };

    @Mock
    private TargetOrderCancelRequest orderCancelRequest;

    @Mock
    private OrderCancelRecordEntryModel cancelRequestRecordEntry;

    @Mock
    private VoucherCorrectionStrategy mockVoucherCorrectionStrategy;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Before
    public void setup() throws OrderCancelRecordsHandlerException {
        given(orderCancelHelper.createOrderFullCancelRecord(order,
                RejectCancelServiceImpl.CANCEL_REASON,
                CancelReason.FRAUDCHECKREJECTED)).willReturn(orderCancelRequest);
        given(orderCancelHelper.createOrderFullCancelRecordEntry(orderCancelRequest)).willReturn(
                cancelRequestRecordEntry);
    }

    @Test
    public void testStartRejectCancelProcess() throws OrderCancelException {
        rejectCancelService.startRejectCancelProcess(order);
        given(order.getFluentId()).willReturn("");
        verify(targetCancelService).modifyOrderAccordingToRequest(orderCancelRequest);
        verify(targetCancelService).recalculateOrder(order);
        verify(targetCancelService).releaseStock(orderCancelRequest);
        verify(targetCancelService).updateRecordEntry(orderCancelRequest);
        verify(modelService).refresh(order);
        verify(targetBusinessProcessService).startOrderRejectProcess(order, cancelRequestRecordEntry);
        verify(mockVoucherCorrectionStrategy).correctAppliedVouchers(order);
    }

    @Test
    public void testStartRejectCancelProcessWithFluent() throws OrderCancelException {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        given(order.getFluentId()).willReturn("123");
        rejectCancelService.startRejectCancelProcess(order);
        verify(targetCancelService).modifyOrderAccordingToRequest(orderCancelRequest);
        verify(targetCancelService).recalculateOrder(order);
        verify(targetCancelService, never()).releaseStock(orderCancelRequest);
        verify(targetCancelService).updateRecordEntry(orderCancelRequest);
        verify(modelService).refresh(order);
        verify(targetBusinessProcessService).startOrderRejectProcess(order, cancelRequestRecordEntry);
        verify(mockVoucherCorrectionStrategy).correctAppliedVouchers(order);
    }

    @Test
    public void testStartRejectCancelProcessWithFluentOnForOldOrder() throws OrderCancelException {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        given(order.getFluentId()).willReturn("");
        rejectCancelService.startRejectCancelProcess(order);
        verify(targetCancelService).releaseStock(orderCancelRequest);
        verify(targetCancelService).modifyOrderAccordingToRequest(orderCancelRequest);
        verify(targetCancelService).recalculateOrder(order);
        verify(targetCancelService).updateRecordEntry(orderCancelRequest);
        verify(modelService).refresh(order);
        verify(targetBusinessProcessService).startOrderRejectProcess(order, cancelRequestRecordEntry);
        verify(mockVoucherCorrectionStrategy).correctAppliedVouchers(order);
    }
}
