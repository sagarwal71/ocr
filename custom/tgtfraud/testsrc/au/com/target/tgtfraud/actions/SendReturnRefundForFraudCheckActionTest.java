/**
 * 
 */
package au.com.target.tgtfraud.actions;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.returns.model.OrderReturnRecordEntryModel;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtfraud.TargetFraudService;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendReturnRefundForFraudCheckActionTest {

    @InjectMocks
    private final SendReturnRefundForFraudCheckAction action = new SendReturnRefundForFraudCheckAction();

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;
    @Mock
    private OrderProcessModel process;
    @Mock
    private List refundList;
    @Mock
    private TargetFraudService targetFraudService;
    @Mock
    private OrderReturnRecordEntryModel returnRequest;

    @Before
    public void setUp() {
        when(orderProcessParameterHelper.getReturnRequest(process)).thenReturn(returnRequest);
        when(orderProcessParameterHelper.getRefundPaymentEntryList(process)).thenReturn(refundList);
    }

    @Test
    public void testSendDetails() {
        action.sendDetails(process);
        verify(targetFraudService).submitRefund(null, returnRequest, refundList);
    }
}
