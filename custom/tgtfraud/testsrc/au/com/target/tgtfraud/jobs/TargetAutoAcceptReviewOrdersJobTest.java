/**
 * 
 */
package au.com.target.tgtfraud.jobs;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfraud.AutoAcceptService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetAutoAcceptReviewOrdersJobTest {

    @Mock
    private AutoAcceptService autoAcceptService;

    @Mock
    private CronJobModel cronJob;

    @InjectMocks
    private final TargetAutoAcceptReviewOrdersJob job = new TargetAutoAcceptReviewOrdersJob();

    @Test
    public void testSuccessCronjob() {
        final PerformResult result = job.perform(cronJob);
        Assert.assertEquals(CronJobResult.SUCCESS, result.getResult());
    }

    @Test
    public void testFailureCronjob() {
        BDDMockito.given(job.perform(cronJob)).willThrow(new RuntimeException());
        final PerformResult result = job.perform(cronJob);
        Assert.assertEquals(CronJobResult.FAILURE, result.getResult());
    }
}
