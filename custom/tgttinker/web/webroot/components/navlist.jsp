<!--
	Container for a categories_loop widget.  
-->

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

 <div id="categories">
	<c:if test="${mocks != null && fn:length(mocks) > 0}">
	    <c:forEach items="${mocks}" var="mock">
	        <c:url value="${request.contextPath}/${mock.mockCntrlUrl}" var="mockurl"/>
	        <a href="${mockurl}">${mock.serviceName}</a><br />
	    </c:forEach>
	</c:if>
 </div>
   
