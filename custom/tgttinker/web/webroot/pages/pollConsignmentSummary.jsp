<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head><title>Poll consignment Mock status screen</title></head>
<body>
<script type="text/javascript">
    function untickOtherCheckbox(checkboxToReset) {
        var success = document.getElementById('success');
        var serviceUnavailable = document.getElementById('serviceUnavailable');
        if (checkboxToReset == 'resetServiceUnavailable') {
        	serviceUnavailable.checked = false;
        } else if (checkboxToReset == 'resetSuccess') {
        	success.checked = false;
        }
    }

</script>
<div id="status">
    <p>
        <c:url value="${request.contextPath}/pollConsignment.action" var="pollConsignmentUrl"/>
        <c:choose>
            <c:when test="${pollConsignment.mockActive}">
              Active
               <form:form id='enablemock' method="POST" action="?enable=false">
                   <input type="hidden" name="enable" id="enable" value="false" />
                   <button type="submit">Disable</button>
               </form:form>
            </c:when>
            <c:otherwise>
            Inactive
                <form:form id='enablemock' method="POST" action="?enable=true">
                    <button type="submit">Enable</button>
                </form:form>
            </c:otherwise>
        </c:choose>
    </p>
        <c:if test="${pollConsignment.mockActive}">
            <form:form id="setMockValues" method="POST" action="${pollConsignmentUrl}" commandName="pollConsignment">
              <table>
                 <tr>
                    <td style="width:200">Success</td>
                    <td style="width:200"> <form:checkbox id="success" path="success" onchange="untickOtherCheckbox('resetServiceUnavailable')" /> <br/><br/></td>
                 </tr>
                 
                 <tr>
                    <td style="width:200">Service unavailable</td>
                    <td style="width:200"> <form:checkbox id="serviceUnavailable" path="serviceUnavailable" onchange="untickOtherCheckbox('resetSuccess')" /> <br/><br/></td>
                 </tr>
              </table>
                  
               <br/><br/>
               <button type="submit">Update</button>
               
            </form:form>
        </c:if>
</div>