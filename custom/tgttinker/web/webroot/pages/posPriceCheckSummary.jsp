<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>POS Price Check Mock status screen</title>
</head>
<body>

<c:url value="${request.contextPath}/posPriceCheck.action" var="posPriceCheckUrl"/>


<div id="status">
    <p>
        <c:url value="${request.contextPath}/posPriceCheck.action" var="posPriceCheckUrl"/>
        <c:choose>
            <c:when test="${posPriceCheckData.mockActive}">
                Mock is Currently Active
                <form:form id='enablemock' method="POST" action="${posPriceCheckUrl}?enable=false">
                    <input type="hidden" name="enable" id="enable" value="false" />
                    <button type="submit">Disable</button>
                </form:form>
            </c:when>
            <c:otherwise>
                Mock is Currently Inactive
                <c:if test="${posPriceCheckData.mockInUse}">
                    <form:form id='enablemock' method="POST" action="${posPriceCheckUrl}?enable=true">
                        <button type="submit">Enable</button>
                    </form:form>
                </c:if>
            </c:otherwise>
        </c:choose>
    </p>
    <c:if test="${posPriceCheckData.mockActive}">
        <form:form id='enablemock' method="POST" action="${posPriceCheckUrl}?enable=true" commandName="posPriceCheckDataForm">
            Itemcode: <form:input path="itemcode" /><br />
            Description: <form:input path="description" /><br />
            Price: <form:input path="price" /><br />
            Was Price: <form:input path="wasPrice" /><br />
            <br />
            Success: <form:checkbox path="success"/><br />
            Timeout: <form:checkbox path="timeout"/><br />
            Error Source: <form:input path="errorSource" /><br />
            Error Message: <form:input path="errorMessage" />
            <br />
            <button type="submit">Update</button>
        </form:form>
    </c:if>
</div>
</body>
</html>