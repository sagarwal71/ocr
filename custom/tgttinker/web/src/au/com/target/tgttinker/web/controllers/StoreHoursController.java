/**
 * 
 */
package au.com.target.tgttinker.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.tgttinker.mock.facades.TargetStoreHoursGeneratorFacade;


/**
 * @author rmcalave
 *
 */
@Controller
@RequestMapping(value = "/store-hours")
public class StoreHoursController {

    @Autowired
    private TargetStoreHoursGeneratorFacade targetStoreHoursGeneratorFacade;

    @RequestMapping(method = RequestMethod.GET)
    public String get() {
        return "/pages/storeHours";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String regenerateStoreHours() {
        targetStoreHoursGeneratorFacade.regenerateStoreHours();
        return get();
    }
}
