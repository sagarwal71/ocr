/**
 * 
 */
package au.com.target.tgttinker.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.target.tgttinker.mock.facades.ShipsterMockFacade;


/**
 * @author rmcalave
 *
 */
@Controller
@RequestMapping(value = "/shipster")
public class ShipsterMockController {


    private static final String SUMMARY_VIEW = "/pages/shipsterSummary";

    @Autowired
    private ShipsterMockFacade shipsterMockFacade;

    @RequestMapping(method = RequestMethod.GET)
    public String getMockSummary(final ModelMap model) {
        model.addAttribute("shipster", shipsterMockFacade.getSummary());
        return SUMMARY_VIEW;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String enableMock(@RequestParam(value = "enable", required = false) final String enableString,
            final ModelMap model) {
        final boolean enable = Boolean.parseBoolean(enableString);

        shipsterMockFacade.setActive(enable);
        return getMockSummary(model);
    }

    @RequestMapping(value = "/updateVerifyEmail", method = RequestMethod.POST)
    public String updateVerifyEmail(final boolean verifyEmailMockEnable, final boolean verifyEmailServiceAvailable,
            final ModelMap model) {
        shipsterMockFacade.updateVerifyEmail(verifyEmailMockEnable, verifyEmailServiceAvailable);
        return getMockSummary(model);
    }

    @RequestMapping(value = "/updateConfirmOrder", method = RequestMethod.POST)
    public String updateConfirmOrder(final boolean confirmOrderMockEnable, final boolean confirmOrderServiceAvailable,
            final boolean confirmOrderTimeOut,
            final ModelMap model) {
        shipsterMockFacade.updateConfirmOrder(confirmOrderMockEnable, confirmOrderServiceAvailable,
                confirmOrderTimeOut);
        return getMockSummary(model);
    }

    @RequestMapping(value = "/verifyEmail", method = RequestMethod.POST)
    public String verifyEmail(@RequestBody final String verifyEmailResponse,
            final ModelMap model) {
        shipsterMockFacade.verifyEmail(verifyEmailResponse);
        return getMockSummary(model);
    }

    @RequestMapping(value = "/confirmOrder", method = RequestMethod.POST)
    public String confirmOrder(@RequestBody final String confirmOrderResponseDTO,
            final ModelMap model) {
        shipsterMockFacade.confirmOrder(confirmOrderResponseDTO);
        return getMockSummary(model);
    }
}