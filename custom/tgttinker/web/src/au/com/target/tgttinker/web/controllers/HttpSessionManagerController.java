/**
 * 
 */
package au.com.target.tgttinker.web.controllers;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.catalina.Context;
import org.apache.catalina.Host;
import org.apache.catalina.Manager;
import org.apache.catalina.Session;
import org.apache.catalina.core.ApplicationContext;
import org.apache.catalina.core.ApplicationContextFacade;
import org.apache.catalina.core.StandardContext;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * @author bhuang3
 *
 */
@Controller
public class HttpSessionManagerController {

    private static final Logger LOG = Logger.getLogger(HttpSessionManagerController.class);

    private static final String HTTP_SESSION_MANAGER_ACTION = "/targetHttpSessionManager.action";

    private static final String HTTP_SESSION_SEARCH_ACTION = "/targetHttpSessionSearch.action";

    private static final String HTTP_SESSION_EXPIRE_ACTION = "/targetHttpSessionExpire.action";

    private static final String HTTP_SESSION_MANAGER_VIEW = "/pages/targetHttpSessionManager";

    @RequestMapping(value = HTTP_SESSION_MANAGER_ACTION, method = RequestMethod.GET)
    public String getSessionManager()
    {
        return HTTP_SESSION_MANAGER_VIEW;
    }

    @RequestMapping(value = HTTP_SESSION_SEARCH_ACTION, method = RequestMethod.POST)
    public String searchSessionDetails(final HttpServletRequest request, final ModelMap model)
    {
        final String sessionId = request.getParameter("searchSession");
        final Session localSession = findSession(request, "", sessionId);
        model.addAttribute("sessionSearch", Boolean.TRUE);
        if (localSession != null) {
            model.addAttribute("sessionId", localSession.getId());
            model.addAttribute("sessionCreationTime", new Date(localSession.getCreationTime()));
            model.addAttribute("sessionLastAccessedTime", new Date(localSession.getLastAccessedTime()));
            final StringBuilder sessionAttributes = new StringBuilder();
            final HttpSession httpSession = localSession.getSession();
            final Enumeration e = localSession.getSession().getAttributeNames();
            while (e.hasMoreElements()) {
                final String name = (String)e.nextElement();
                sessionAttributes.append(name + ": " + httpSession.getAttribute(name) + "<BR>");
            }
            model.addAttribute("sessionValues", sessionAttributes.toString());
        }
        return HTTP_SESSION_MANAGER_VIEW;
    }

    @RequestMapping(value = HTTP_SESSION_EXPIRE_ACTION, method = RequestMethod.POST)
    public String expireSession(final HttpServletRequest request, final ModelMap model)
    {
        final String sessionId = request.getParameter("expireSession");
        //empty string is the tgtstorefront web context
        final Session localSession = findSession(request, "", sessionId);
        if (localSession != null) {
            expireSession(localSession);
            model.addAttribute("isExpired", Boolean.TRUE);
        }
        return HTTP_SESSION_MANAGER_VIEW;
    }

    private void expireSession(final Session session) {
        if (session != null) {
            session.expire();
            session.setPrincipal(null);
            session.setAuthType(null);
        }
    }

    private ServletContext getServletContext(final HttpServletRequest request, final String name) {
        final Host host = getHostByRequest(request);
        final Context child = (Context)host.findChild(name);
        if (child != null) {
            return child.getServletContext();
        }
        return null;
    }

    private Host getHostByRequest(final HttpServletRequest request) {

        final ApplicationContextFacade appContextFacadeObj = (ApplicationContextFacade)(request.getServletContext());
        Host host = null;
        try
        {
            final Field applicationContextField = appContextFacadeObj.getClass().getDeclaredField("context");
            applicationContextField.setAccessible(true);
            final ApplicationContext appContextObj = (ApplicationContext)applicationContextField
                    .get(appContextFacadeObj);
            final Field standardContextField = appContextObj.getClass().getDeclaredField("context");
            standardContextField.setAccessible(true);
            final StandardContext standardContextObj = (StandardContext)standardContextField.get(appContextObj);
            host = (Host)standardContextObj.getParent();
        }
        catch (final Exception e)
        {
            LOG.error(e);
        }
        return host;

    }

    private Manager getManagerByContext(final ServletContext context) {
        final ApplicationContextFacade appContextFacadeObj = (ApplicationContextFacade)context;
        Manager persistenceManager = null;
        try
        {
            final Field applicationContextField = appContextFacadeObj.getClass().getDeclaredField("context");
            applicationContextField.setAccessible(true);
            final ApplicationContext appContextObj = (ApplicationContext)applicationContextField
                    .get(appContextFacadeObj);
            final Field standardContextField = appContextObj.getClass().getDeclaredField("context");
            standardContextField.setAccessible(true);
            final StandardContext standardContextObj = (StandardContext)standardContextField.get(appContextObj);
            persistenceManager = standardContextObj.getManager();
        }
        catch (final Exception e)
        {
            LOG.error(e);
        }
        return persistenceManager;
    }

    private Session findSession(final HttpServletRequest request, final String extensionName, final String sessionId) {
        final ServletContext servletContext = getServletContext(request, extensionName);
        final Manager manager = getManagerByContext(servletContext);
        Session localSession = null;
        try {
            LOG.info("try to find the session by session id=" + sessionId);
            localSession = manager.findSession(sessionId);

            if ((localSession != null) && (!(localSession.isValid()))) {
                LOG.info("local session is invalid by session id=" + sessionId);
                localSession = null;
            }
        }
        catch (final IOException e) {
            LOG.info("IO exception no local session was found by session id=" + sessionId, e);
        }
        if (localSession != null) {
            LOG.info("local session was found by session id=" + sessionId);
        }
        else {
            LOG.info("No local session was found by session id=" + sessionId);
        }
        return localSession;
    }


}
