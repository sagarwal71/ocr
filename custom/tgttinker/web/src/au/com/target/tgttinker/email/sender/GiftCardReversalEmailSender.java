/**
 * 
 */
package au.com.target.tgttinker.email.sender;

import de.hybris.platform.commerceservices.model.process.StoreFrontProcessModel;
import de.hybris.platform.processengine.enums.ProcessState;

import java.util.Date;

import org.apache.log4j.Logger;

import au.com.target.tgtbusproc.constants.TgtbusprocConstants;
import au.com.target.tgtbusproc.exceptions.BusinessProcessEmailException;
import au.com.target.tgtpayment.dto.GiftCardReversalData;
import au.com.target.tgttinker.web.forms.EmailHarnessForm;


/**
 * @author rmcalave
 * 
 */
public class GiftCardReversalEmailSender extends AbstractEmailSender
{
    private static final Logger LOG = Logger.getLogger(GiftCardReversalEmailSender.class);

    @Override
    public boolean send(final EmailHarnessForm emailHarnessForm)
    {

        final GiftCardReversalData reversalData = new GiftCardReversalData();
        reversalData.setAmount("16.21");
        reversalData.setCustomerEmail(emailHarnessForm.getToAddress());
        reversalData.setFirstName("firstName");
        reversalData.setLastName("LastName");
        reversalData.setMaskedCardNumber("535423432XXXX23232");
        reversalData.setMobile("614258752684344");
        reversalData.setPaymentDate(new Date());
        reversalData.setProvider("ipg");
        reversalData.setReceiptNumber("842541");
        reversalData.setTitle("MRS");

        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.REVERSE_GIFTCARD_PROCESS;
        final String processCode = processDefinitionName + "-" + reversalData.getCustomerEmail()
                + "-" + System.currentTimeMillis();
        final StoreFrontProcessModel processModel = new StoreFrontProcessModel();
        processModel.setSite(getCmsSite());
        processModel.setCode(processCode);
        processModel.setState(ProcessState.CREATED);
        processModel.setProcessDefinitionName(processDefinitionName);
        getModelService().save(processModel);
        getOrderProcessParameterHelper().setGiftCardReversalData(processModel, reversalData);
        getModelService().save(processModel);
        getModelService().refresh(processModel);
        try
        {
            getHybrisEmailStrategy().sendEmail(processModel, "GiftCardReversalFailureEmailTemplate");
        }
        catch (final BusinessProcessEmailException e)
        {
            LOG.error("Problem With BusinessProcessEmail ", e);
            return false;
        }
        return true;
    }

}
