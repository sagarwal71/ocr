/**
 * 
 */
package au.com.target.tgttinker.email.sender;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.email.impl.HybrisEmailStrategy;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgttinker.web.forms.EmailHarnessForm;


/**
 * @author rmcalave
 * 
 */
public abstract class AbstractEmailSender implements EmailSender
{
    private static final Logger LOG = Logger.getLogger(AbstractEmailSender.class);

    private static String websiteURL = null;

    private ConfigurationService configurationService;
    private ModelService modelService;
    private HybrisEmailStrategy hybrisEmailStrategy;
    private HybrisEmailStrategy fileBasedHybrisEmailStrategy;
    private CMSSiteService cmsSiteService;
    private TargetOrderService orderService;
    private OrderProcessParameterHelper orderProcessParameterHelper;
    private TargetBusinessProcessService targetBusinessProcessService;


    protected TargetCustomerModel createCustomerModel(final EmailHarnessForm emailHarnessForm)
    {
        final TargetCustomerModel targetCustomer = modelService.create(TargetCustomerModel.class);
        targetCustomer.setUid(emailHarnessForm.getToAddress());
        return targetCustomer;
    }

    protected String getWebSiteURL()
    {
        if (null == websiteURL)
        {
            websiteURL = configurationService.getConfiguration().getString("tgtstorefront.host.fqdn");
        }
        return websiteURL;
    }

    protected HybrisEmailStrategy getHybrisEmailStrategy()
    {
        if (configurationService.getConfiguration().getBoolean("tgtstorefront.assetmode.develop", false))
        {
            return fileBasedHybrisEmailStrategy;
        }
        else
        {
            return hybrisEmailStrategy;
        }
    }

    protected CMSSiteModel getCmsSite()
    {
        URL url = null;
        try
        {
            url = new URL(getWebSiteURL());
        }
        catch (final MalformedURLException ex)
        {
            LOG.error("Error creating URL for URL string " + getWebSiteURL(), ex);
            return null;
        }

        CMSSiteModel cmsSiteModel = null;
        try
        {
            cmsSiteModel = cmsSiteService.getSiteForURL(url);
        }
        catch (final CMSItemNotFoundException e1)
        {
            LOG.error("There is no site with :", e1);
        }

        return cmsSiteModel;
    }

    /**
     * @param configurationService
     *            the configurationService to set
     */
    @Required
    public void setConfigurationService(final ConfigurationService configurationService)
    {
        this.configurationService = configurationService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService)
    {
        this.modelService = modelService;
    }

    /**
     * @return the modelService
     */
    public ModelService getModelService() {
        return modelService;
    }

    /**
     * @param hybrisEmailStrategy
     *            the hybrisEmailStrategy to set
     */
    @Required
    public void setHybrisEmailStrategy(final HybrisEmailStrategy hybrisEmailStrategy)
    {
        this.hybrisEmailStrategy = hybrisEmailStrategy;
    }

    /**
     * @param fileBasedHybrisEmailStrategy
     *            the fileBasedHybrisEmailStrategy to set
     */
    @Required
    public void setFileBasedHybrisEmailStrategy(final HybrisEmailStrategy fileBasedHybrisEmailStrategy)
    {
        this.fileBasedHybrisEmailStrategy = fileBasedHybrisEmailStrategy;
    }

    /**
     * @param cmsSiteService
     *            the cmsSiteService to set
     */
    @Required
    public void setCmsSiteService(final CMSSiteService cmsSiteService)
    {
        this.cmsSiteService = cmsSiteService;
    }

    /**
     * @return the orderService
     */
    protected TargetOrderService getOrderService()
    {
        return orderService;
    }

    /**
     * @param orderService
     *            the orderService to set
     */
    @Required
    public void setOrderService(final TargetOrderService orderService)
    {
        this.orderService = orderService;
    }

    /**
     * @return the orderProcessParameterHelper
     */
    public OrderProcessParameterHelper getOrderProcessParameterHelper() {
        return orderProcessParameterHelper;
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }

    /**
     * @return the targetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        return targetBusinessProcessService;
    }

    /**
     * @param targetBusinessProcessService
     *            the targetBusinessProcessService to set
     */
    @Required
    public void setTargetBusinessProcessService(final TargetBusinessProcessService targetBusinessProcessService) {
        this.targetBusinessProcessService = targetBusinessProcessService;
    }




}
