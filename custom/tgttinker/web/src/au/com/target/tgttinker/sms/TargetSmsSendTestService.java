/**
 * 
 */
package au.com.target.tgttinker.sms;

import au.com.target.tgttinker.web.forms.SmsHarnessForm;


/**
 * @author bhuang3
 * 
 */
public interface TargetSmsSendTestService {

    /**
     * generate the sms from order id and template name
     * 
     * @param smsHarnessForm
     * @return string sms message
     */
    String generateSmsMessage(SmsHarnessForm smsHarnessForm);
}
