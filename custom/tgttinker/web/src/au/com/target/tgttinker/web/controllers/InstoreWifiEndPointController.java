package au.com.target.tgttinker.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;



/**
 * Store Wifi End Point
 */
@Controller
public class InstoreWifiEndPointController {
    /** URL to categories action. */
    private static final String ACCESS_WIFI = "/access-wifi";

    /** Categories form view name. */
    private static final String INSTORE_WIFI_ACCESS_PAGE = "/pages/inStoreWifiAccessPage";

    /**
     * Determines the category tree to given <code>categoryPath</code> param, sets this to the given model reference and
     * returns the categories view.
     * 
     * @param model
     *            reference to model object where the category tree will be set
     * @return categories view
     */
    @RequestMapping(value = ACCESS_WIFI, method = RequestMethod.GET)
    public String getMockSummary(final ModelMap model,
            @RequestParam(value = "subscriptionResponse", required = false) final String subscriptionResponse,
            @RequestParam(value = "userIdentifier", required = false) final String userIdentifier) {
        model.addAttribute("subscriptionResponse", subscriptionResponse);
        model.addAttribute("userIdentifier", userIdentifier);
        return INSTORE_WIFI_ACCESS_PAGE;
    }

    @RequestMapping(value = ACCESS_WIFI, method = RequestMethod.POST)
    public String getMockWifiSummary(final ModelMap model,
            @RequestParam(value = "redirect_url", required = true) final String redirectUrl,
            @RequestParam(value = "buttonClicked", required = true) final String buttonClicked) {
        model.addAttribute("redirectUrl", redirectUrl);
        model.addAttribute("buttonClicked", buttonClicked);
        return INSTORE_WIFI_ACCESS_PAGE;
    }

}
