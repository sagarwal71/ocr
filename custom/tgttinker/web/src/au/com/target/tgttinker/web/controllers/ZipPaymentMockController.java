/**
 * 
 */
package au.com.target.tgttinker.web.controllers;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

import au.com.target.tgttinker.mock.facades.ZipPaymentMockFacade;

@Controller
public class ZipPaymentMockController {

    /** URL to categories action. */
    private static final String ZIPPAY_ACTION = "/zippay.action";

    /** Categories form view name. */
    private static final String SUMMARY_VIEW = "/pages/zippaySummary";

    @Autowired
    private ZipPaymentMockFacade zipPaymentMockFacade;

    /**
     * Determines the category tree to given <code>categoryPath</code> param, sets this to the given model reference and
     * returns the categories view.
     * 
     * @param model
     *            reference to model object where the category tree will be set
     * @return categories view
     */
    @RequestMapping(value = ZIPPAY_ACTION, method = RequestMethod.GET)
    public String getMockSummary(final ModelMap model) {
        model.addAttribute("zippay", zipPaymentMockFacade.getSummary());
        return SUMMARY_VIEW;
    }


    /**
     * Determines the category tree to given <code>categoryPath</code> param, sets this to the given model reference and
     * returns the categories view.
     * 
     * @param model
     *            reference to model object where the category tree will be set
     * @return categories view
     */
    @RequestMapping(value = ZIPPAY_ACTION, method = RequestMethod.POST)
    public String updateMock(final HttpServletRequest request,
            @RequestParam(value = "enable", required = false) final String enable,
            final ModelMap model) {
        if (enable != null) {
            final boolean enableFlag = BooleanUtils.toBoolean(enable);
            zipPaymentMockFacade.setActive(enableFlag);
            // if mock not enabled then reset all data
            if (!enableFlag) {
                zipPaymentMockFacade.resetDefault();
            }
        }

        zipPaymentMockFacade.setThrowException(getBooleanValue(request, "throwException"));
        zipPaymentMockFacade.setCreateCheckoutException(getBooleanValue(request, "createCheckoutException"));
        zipPaymentMockFacade.setPingActive(getBooleanValue(request, "pingActive"));
        zipPaymentMockFacade.setCreateCheckoutActive(getBooleanValue(request, "createCheckoutActive"));

        zipPaymentMockFacade.setRetrieveCheckoutActive(getBooleanValue(request, "retrieveCheckoutActive"));
        zipPaymentMockFacade.setRetrieveCheckoutException(getBooleanValue(request, "retrieveCheckoutException"));
        zipPaymentMockFacade.setCaptureRequestActive(getBooleanValue(request, "captureRequestActive"));
        zipPaymentMockFacade.setCaptureRequestException(getBooleanValue(request, "captureRequestException"));
        zipPaymentMockFacade.setCaptureTransactionError(getBooleanValue(request, "captureTransactionError"));

        //Refund active and exceptions
        zipPaymentMockFacade.setRefundRequestActive(getBooleanValue(request, "refundRequestActive"));
        zipPaymentMockFacade.setRefundRequestException(getBooleanValue(request, "refundRequestException"));
        zipPaymentMockFacade.setRefundRejectException(getBooleanValue(request, "refundRejectException"));
        zipPaymentMockFacade.setRefundTransErrorException(getBooleanValue(request, "refundTransErrorException"));
        final String mockRefundResponse = request.getParameter("mockRefundResponse");


        // Response attributes
        final String mockCheckoutResponse = request.getParameter("createCheckoutResponse");
        final String retrieveCheckoutResponse = request.getParameter("retrieveCheckoutResponse");
        final String captureRequestResponse = request.getParameter("captureRequestResponse");

		try {
		    if(mockCheckoutResponse != null) {
                zipPaymentMockFacade.setMockCreateCheckoutResponse(mockCheckoutResponse);
            }

            if(retrieveCheckoutResponse != null) {
                zipPaymentMockFacade.setMockRetrieveCheckoutResponse(retrieveCheckoutResponse);
            }

            if(captureRequestResponse != null) {
                zipPaymentMockFacade.setMockCaptureRequestResponse(captureRequestResponse);
            }

            if(mockRefundResponse != null) {
                zipPaymentMockFacade.setMockRefundResponse(mockRefundResponse);
            }

		} catch (IllegalArgumentException ex) {
			model.addAttribute("error", ex.getMessage());
		}

        return getMockSummary(model);
    }

    private boolean getBooleanValue(final HttpServletRequest request, String parameter) {
        return BooleanUtils.toBoolean(request.getParameter(parameter));
    }
}
