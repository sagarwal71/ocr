/**
 * 
 */
package au.com.target.tgttinker.web.controllers.fulfilment;

import de.hybris.platform.core.model.order.OrderModel;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.tgtcore.order.TargetOrderService;


/**
 * @author gbaker2
 * 
 */
@Controller
@Scope("request")
@Lazy(true)
@RequestMapping(value = "/order")
public class OrderConfGeneratorController
{
    @Resource(name = "orderService")
    private TargetOrderService targetOrderService;

    @RequestMapping(value = "/pickconf/{orderCode:.*}", method = RequestMethod.GET)
    public String get(@PathVariable("orderCode") final String orderCode, final Model model,
            final HttpServletResponse response)
    {
        final OrderModel orderModel = targetOrderService.findOrderModelForOrderId(orderCode);
        model.addAttribute("orderData", orderModel);
        response.setContentType("text/plain");
        response.setHeader("Cache-Control", "no-cache");
        return "pages/orders/pickConf";
    }
}