/**
 * 
 */
package au.com.target.tgttinker.web.controllers;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgtverifyaddr.provider.AddressVerificationClient;


/**
 * Generic mock functionality.
 */
@Controller
public class AddressVerificationMockController {

    /** URL to categories action. */
    private static final String ACTION_NAME = "/verifyAddress.action";

    /** Categories form view name. */
    private static final String VIEW_NAME = "/pages/verifyAddr";


    @Resource(name = "addressVerificationClient")
    private AddressVerificationClient addressVerificationClient;


    /**
     * Determines the category tree to given <code>categoryPath</code> param, sets this to the given model reference and
     * returns the categories view.
     * 
     * @param model
     *            reference to model object where the category tree will be set
     * @return categories view
     */
    @SuppressWarnings("boxing")
    @RequestMapping(value = ACTION_NAME, method = RequestMethod.GET)
    public String getMockSummary(final ModelMap model)
    {
        if (addressVerificationClient instanceof MockedByTarget) {
            final MockedByTarget mock = (MockedByTarget)addressVerificationClient;
            model.addAttribute("mockActive", mock.isActive());
            model.addAttribute("mockInUse", true);
        }
        else {
            model.addAttribute("mockInUse", false);
        }
        return VIEW_NAME;
    }

    @RequestMapping(value = ACTION_NAME, method = RequestMethod.POST)
    public String updateMock(@RequestParam(value = "enable", required = false) final Boolean enable,
            final ModelMap model)
    {
        if (enable != null && addressVerificationClient instanceof MockedByTarget) {
            final MockedByTarget mock = (MockedByTarget)addressVerificationClient;
            mock.setActive(enable.booleanValue());
        }
        return getMockSummary(model);
    }
}
