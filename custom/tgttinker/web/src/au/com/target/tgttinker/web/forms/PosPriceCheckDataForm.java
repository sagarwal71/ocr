/**
 * 
 */
package au.com.target.tgttinker.web.forms;

/**
 * @author rmcalave
 *
 */
public class PosPriceCheckDataForm {
    private boolean success;

    private String errorSource;

    private String errorMessage;

    private String itemcode;

    private String description;

    private Integer price;

    private Integer wasPrice;

    private boolean timeout;

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success
     *            the success to set
     */
    public void setSuccess(final boolean success) {
        this.success = success;
    }

    /**
     * @return the errorSource
     */
    public String getErrorSource() {
        return errorSource;
    }

    /**
     * @param errorSource
     *            the errorSource to set
     */
    public void setErrorSource(final String errorSource) {
        this.errorSource = errorSource;
    }

    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage
     *            the errorMessage to set
     */
    public void setErrorMessage(final String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * @return the itemcode
     */
    public String getItemcode() {
        return itemcode;
    }

    /**
     * @param itemcode
     *            the itemcode to set
     */
    public void setItemcode(final String itemcode) {
        this.itemcode = itemcode;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return the price
     */
    public Integer getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(final Integer price) {
        this.price = price;
    }

    /**
     * @return the wasPrice
     */
    public Integer getWasPrice() {
        return wasPrice;
    }

    /**
     * @param wasPrice
     *            the wasPrice to set
     */
    public void setWasPrice(final Integer wasPrice) {
        this.wasPrice = wasPrice;
    }

    /**
     * @return the timeout
     */
    public boolean isTimeout() {
        return timeout;
    }

    /**
     * @param timeout
     *            the timeout to set
     */
    public void setTimeout(final boolean timeout) {
        this.timeout = timeout;
    }
}
