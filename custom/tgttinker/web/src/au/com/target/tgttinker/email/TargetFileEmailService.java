/**
 * 
 */
package au.com.target.tgttinker.email;

import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.media.MediaService;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.email.EmailServiceException;
import au.com.target.tgtbusproc.email.SendHybrisEmailService;
import au.com.target.tgtmail.model.Email;


/**
 * @author rmcalave
 * 
 */
public class TargetFileEmailService implements SendHybrisEmailService
{
    private static final Logger LOG = Logger.getLogger(TargetFileEmailService.class);

    private ConfigurationService configurationService;

    private MediaService mediaService;

    /*
     * (non-Javadoc)
     * 
     * @see
     * au.com.target.tgtbusproc.email.SendHybrisEmailService#sendEmail(de.hybris.platform.acceleratorservices.model.email
     * .EmailMessageModel)
     */
    @Override
    public void sendEmail(final EmailMessageModel email) throws EmailServiceException
    {
        final String mailDirectory = configurationService.getConfiguration().getString("tgttinker.email.directory");

        try
        {
            final File mailFolder = new File(mailDirectory);
            mailFolder.mkdirs();

            final File mailFile = new File(mailFolder, email.getPk() + ".html");
            if (!mailFile.exists())
            {
                mailFile.createNewFile();
            }



            String body = email.getBody();

            if (StringUtils.isEmpty(body)) {
                final MediaModel bodyMedia = email.getBodyMedia();
                if (bodyMedia != null) {
                    body = new Email.Builder().withMediaService(mediaService).convertMediaFileToString(bodyMedia,
                            "UTF-8");
                }

            }

            try (FileWriter mailFileWriter = new FileWriter(mailFile.getAbsoluteFile())) {
                try (BufferedWriter mailFileBufferedWriter = new BufferedWriter(mailFileWriter)) {
                    mailFileBufferedWriter.write(body);
                }
            }
        }
        catch (final IOException ex)
        {
            LOG.error("Unable to write mail message to file: " + ex);
        }
    }

    /**
     * @param configurationService
     *            the configurationService to set
     */
    @Required
    public void setConfigurationService(final ConfigurationService configurationService)
    {
        this.configurationService = configurationService;
    }

    /**
     * @param mediaService
     *            the mediaService to set
     */
    @Required
    public void setMediaService(final MediaService mediaService) {
        this.mediaService = mediaService;
    }
}
