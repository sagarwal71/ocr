/**
 * 
 */
package au.com.target.tgttinker.web.controllers;

import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @author gbaker2
 * 
 */
@Controller
@Scope("request")
@Lazy(true)
@RequestMapping(value = "/http-status")
public class HttpStatusCode {

    @RequestMapping(value = "/{code:.*}", method = RequestMethod.GET)
    @ResponseBody
    public String get(@PathVariable("code") final int code, final HttpServletResponse response)
    {
        HttpStatus httpStatus;
        try {
            httpStatus = HttpStatus.valueOf(code);
        }
        catch (final IllegalArgumentException e) {
            httpStatus = null;
        }

        if (httpStatus != null) {
            response.setStatus(httpStatus.value());
            return httpStatus.getReasonPhrase();
        }

        response.setStatus(HttpStatus.BAD_REQUEST.value());
        return "Code not recognised.";
    }
}
