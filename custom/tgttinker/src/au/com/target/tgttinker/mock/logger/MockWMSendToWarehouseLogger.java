/**
 * 
 */
package au.com.target.tgttinker.mock.logger;

import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseRequest;
import au.com.target.tgtwebmethods.logger.WMSendToWarehouseLogger;


/**
 * @author pratik
 *
 */
public class MockWMSendToWarehouseLogger extends WMSendToWarehouseLogger {

    private String loggedMessage;

    private boolean active;

    @Override
    protected String getLogMessage(final SendToWarehouseRequest sendToWarehouseRequest, final String errorCode,
            final String errorValue, final boolean requireConsignmentDetails) {

        if (active) {
            loggedMessage = super.getLogMessage(sendToWarehouseRequest, errorCode, errorValue, requireConsignmentDetails);
            return loggedMessage;
        }

        return super.getLogMessage(sendToWarehouseRequest, errorCode, errorValue, requireConsignmentDetails);
    }

    /**
     * @return the loggedMessage
     */
    public String getLoggedMessage() {
        return loggedMessage;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param loggedMessage
     *            the loggedMessage to set
     */
    public void setLoggedMessage(final String loggedMessage) {
        this.loggedMessage = loggedMessage;
    }

    /**
     * @param active
     *            the active to set
     */
    public void setActive(final boolean active) {
        this.active = active;
    }

    public void reset() {
        loggedMessage = "";
    }

}
