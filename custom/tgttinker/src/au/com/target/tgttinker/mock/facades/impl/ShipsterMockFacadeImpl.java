/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import de.hybris.platform.core.Registry;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.target.tgtauspost.deliveryclub.client.AusPostShipsterClient;
import au.com.target.tgtauspost.deliveryclub.dto.ShipsterVerifyEmailResponseDTO;
import au.com.target.tgtauspost.integration.dto.ShipsterConfirmOrderResponseDTO;
import au.com.target.tgttinker.mock.auspost.MockAusPostShipsterClient;
import au.com.target.tgttinker.mock.data.CommonSummaryData;
import au.com.target.tgttinker.mock.data.ShipsterSummaryData;
import au.com.target.tgttinker.mock.facades.ShipsterMockFacade;


/**
 * @author rmcalave
 *
 */
public class ShipsterMockFacadeImpl implements ShipsterMockFacade {

    private static final Logger LOG = Logger.getLogger(ShipsterMockFacadeImpl.class);

    private AusPostShipsterClient shipsterClient;

    private final ShipsterSummaryData summaryData = new ShipsterSummaryData();


    @Override
    public CommonSummaryData getSummary() {
        return summaryData;
    }

    @Override
    public void setActive(final boolean enable) {
        LOG.info("Set " + (enable ? "enable" : "disable") + " Shipster client mock");
        summaryData.setMockActive(enable);
        if (!enable) {
            summaryData.resetDefault();
        }
        updateMock();
    }

    @Override
    public void updateVerifyEmail(final boolean verifyEmailMockEnable, final boolean verifyEmailServiceAvailable) {
        summaryData.setVerifyEmailServiceAvailable(verifyEmailServiceAvailable);
        summaryData.setVerifyEmailMock(verifyEmailMockEnable);
        updateMock();
    }


    @Override
    public void verifyEmail(final String verifyEmailResponse) {
        summaryData.setVerifyEmailResponse(verifyEmailResponse);
        updateMock();
    }


    @Override
    public void confirmOrder(final String confirmOrderResponse) {
        summaryData.setConfirmOrderResponse(confirmOrderResponse);
        updateMock();
    }

    @Override
    public void updateConfirmOrder(final boolean confirmOrderMockEnable, final boolean confirmOrderServiceAvailable,
            final boolean confirmOrderTimeOut) {
        summaryData.setConfirmOrderServiceAvailable(confirmOrderServiceAvailable);
        summaryData.setConfirmOrderMock(confirmOrderMockEnable);
        summaryData.setConfirmOrderTimeOut(confirmOrderTimeOut);
        updateMock();
    }

    private void updateMock() {
        if (getShipsterClient() instanceof MockAusPostShipsterClient) {
            final MockAusPostShipsterClient mockShipsterClient = (MockAusPostShipsterClient)getShipsterClient();
            if (summaryData.isMockActive()) {
                mockShipsterClient.setActive(summaryData.isMockActive());
                mockShipsterClient.setVerifyEmailMock(summaryData.isVerifyEmailMock());
                mockShipsterClient.setConfirmOrderMock(summaryData.isConfirmOrderMock());
                mockShipsterClient.setVerifyEmailNotAvailable(!summaryData.isVerifyEmailServiceAvailable());
                mockShipsterClient
                        .setConfirmOrderServiceNotAvailable(!summaryData.isConfirmOrderServiceAvailable());
                mockShipsterClient.setVerifyEmailResponse(
                        createDtoObject(summaryData.getVerifyEmailResponse(),
                                ShipsterVerifyEmailResponseDTO.class));
                mockShipsterClient.setConfirmOrderResponse(createDtoObject(summaryData.getConfirmOrderResponse(),
                        ShipsterConfirmOrderResponseDTO.class));
                mockShipsterClient.setConfirmOrderTimeOut(summaryData.isConfirmOrderTimeOut());
            }
            else {
                mockShipsterClient.resetDefault();
            }
        }
    }

    /**
     * @return the shipsterClient
     */
    public AusPostShipsterClient getShipsterClient() {
        if (shipsterClient == null) {
            shipsterClient = (AusPostShipsterClient)Registry.getApplicationContext().getBean("ausPostShipsterClient");
        }
        return shipsterClient;
    }

    private <T> T createDtoObject(final String response, final Class<T> classType) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            if (StringUtils.isNotEmpty(response)) {
                return mapper.readValue(response, classType);
            }
        }
        catch (final Exception ex) {
            LOG.error("Error happened while mapping the json to object", ex);
            return null;
        }
        return null;
    }
}