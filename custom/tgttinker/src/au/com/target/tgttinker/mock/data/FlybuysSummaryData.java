/**
 * 
 */
package au.com.target.tgttinker.mock.data;

import java.util.Arrays;
import java.util.List;

import au.com.target.tgttinker.mock.flybuys.FlybuysClientMock;


/**
 * @author cwijesu1
 * 
 */
public class FlybuysSummaryData extends CommonSummaryData {
    private String mockResponse = "";
    private String mockStatus = "";
    private String mockPoints = "";
    private String mockConsumeResponse = "";
    private String mockRefundResponse = "";
    private List<String> validMockResponses = FlybuysClientMock.VALID_MOCK_RESPONSE;
    private final String mockDefault = FlybuysClientMock.DEFAULT_RESPONDS_CODE;
    private final String mockDefaultPoints = FlybuysClientMock.DEFAULT_AVAILABLE_POINTS;
    private final String mockDefaultConsumeResponse = FlybuysClientMock.DEFAULT_REDEEM_RESPONDE_CODE;
    private final String mockDefaultRefundResponse = FlybuysClientMock.DEFAULT_REDEEM_RESPONDE_CODE;

    /**
     * @param mockRefundResponse
     *            the mockRefundResponse to set
     */
    public void setMockRefundResponse(final String mockRefundResponse) {
        this.mockRefundResponse = mockRefundResponse;
    }

    /**
     * @return the mockRefundResponse
     */
    public String getMockRefundResponse() {
        return mockRefundResponse;
    }

    /**
     * @return the mockDefaultRefundResponse
     */
    public String getMockDefaultRefundResponse() {
        return mockDefaultRefundResponse;
    }

    /**
     * @return the mockConsumeResponse
     */
    public String getMockConsumeResponse() {
        return mockConsumeResponse;
    }

    /**
     * @param mockConsumeResponse
     *            the mockConsumeResponse to set
     */
    public void setMockConsumeResponse(final String mockConsumeResponse) {
        this.mockConsumeResponse = mockConsumeResponse;
    }

    /**
     * @return the mockResponse
     */
    public String getMockResponse() {
        return mockResponse;
    }

    /**
     * @param mockResponse
     *            the mockResponse to set
     */
    public void setMockResponse(final String mockResponse) {
        this.mockResponse = mockResponse;
    }

    /**
     * @return the validMockResponses
     */
    public String[] getValidMockResponses() {
        return validMockResponses.toArray(new String[] {});
    }

    /**
     * @param validMockResponses
     *            the validMockResponses to set
     */
    public void setValidMockResponses(final String[] validMockResponses) {
        this.validMockResponses = Arrays.asList(validMockResponses);
    }

    /**
     * @return the MockDefaultPoints
     */
    public String getMockDefaultPoints() {
        return mockDefaultPoints;
    }

    /**
     * @return the mockDefaultConsumeResponse
     */
    public String getMockDefaultConsumeResponse() {
        return mockDefaultConsumeResponse;
    }

    /**
     * @return the mockDefault
     */
    public String getMockDefault() {

        return mockDefault;
    }

    /**
     * @param mockStatus
     *            the mockStatus to set
     */
    public void setMockStatus(final String mockStatus) {
        this.mockStatus = mockStatus;
    }

    /**
     * @return the mockStatus
     */
    public String getMockStatus() {
        return mockStatus;
    }

    /**
     * @return the mockPoints
     */
    public String getMockPoints() {
        return mockPoints;
    }

    /**
     * @param mockPoints
     *            the mockPoints to set
     */
    public void setMockPoints(final String mockPoints) {
        this.mockPoints = mockPoints;
    }
}
