/**
 * 
 */
package au.com.target.tgttinker.mock.instore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;

import au.com.target.tgtsale.stock.dto.request.StockUpdateDto;
import au.com.target.tgtsale.stock.dto.request.StockUpdateStoreDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateProductResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateStoreResponseDto;
import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgtwebmethods.exception.TargetWebMethodsClientException;
import au.com.target.tgtwebmethods.stock.client.impl.TargetStockUpdateClientImpl;


/**
 * Mock class for TargetStockUpdateClientImpl
 * 
 * @author jjayawa1
 *
 */
public class MockTargetStockUpdateClient extends TargetStockUpdateClientImpl implements MockedByTarget {

    private StockUpdateResponseDto fixedResponse;
    private final Map<String, StockUpdateStoreResponseDto> storeStock = new HashedMap();
    private boolean active;

    public MockTargetStockUpdateClient() {

    }

    /**
     * Populate the store stock with dummy data.
     * 
     * @param storeData
     */
    public void populateStoreStock(final List<StoreStockData> storeData) {
        if (active) {
            for (final StoreStockData store : storeData) {
                final List<StockUpdateProductResponseDto> stockUpdateProductResponses = new ArrayList<>();
                final StockUpdateStoreResponseDto storeResponseDTO = new StockUpdateStoreResponseDto();
                storeResponseDTO.setStoreNumber(store.getStore());
                for (final ProductData product : store.getProducts()) {
                    final StockUpdateProductResponseDto productDTO = new StockUpdateProductResponseDto();
                    productDTO.setItemcode(product.getCode());
                    productDTO.setSoh(product.getQuantity());
                    stockUpdateProductResponses.add(productDTO);
                }
                storeResponseDTO.setStockUpdateProductResponseDtos(stockUpdateProductResponses);
                storeStock.put(store.getStore(), storeResponseDTO);
            }
        }
    }



    /**
     * @param storeNumber
     * @param itemcode
     * @param soh
     */
    public void addStoreStock(final String storeNumber, final String itemcode, final String soh) {
        StockUpdateStoreResponseDto storeResponseDTO = null;
        if (storeStock.get(storeNumber) == null) {
            storeResponseDTO = new StockUpdateStoreResponseDto();
            storeResponseDTO.setStoreNumber(storeNumber);
            storeResponseDTO.setSuccess(true);
            storeStock.put(storeNumber, storeResponseDTO);
        }
        storeResponseDTO = storeStock.get(storeNumber);
        boolean existingItemCode = false;
        if (storeResponseDTO.getStockUpdateProductResponseDtos() == null) {
            storeResponseDTO.setStockUpdateProductResponseDtos(new ArrayList<StockUpdateProductResponseDto>());
        }
        final List<StockUpdateProductResponseDto> stockUpdateProductResponses = storeResponseDTO
                .getStockUpdateProductResponseDtos();
        for (final StockUpdateProductResponseDto productDTO : stockUpdateProductResponses) {
            if (productDTO.getItemcode().equals(itemcode)) {
                productDTO.setSoh(soh);
                existingItemCode = true;
            }
        }
        if (!existingItemCode) {
            final StockUpdateProductResponseDto productDTO = new StockUpdateProductResponseDto();
            productDTO.setItemcode(itemcode);
            productDTO.setSoh(soh);
            stockUpdateProductResponses.add(productDTO);
        }

    }


    /**
     * Clear dummy data from store stock
     */
    public void clearStoreStock() {
        storeStock.clear();
        fixedResponse = null;
    }


    /**
     * @param response
     *            the response to set
     */
    public void setResponse(final StockUpdateResponseDto response) {
        this.fixedResponse = response;
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtsale.stock.client.TargetStockUpdateClient#getStockLevelsFromPos(au.com.target.tgtsale.stock.dto.request.StockUpdateDto)
     */
    @Override
    public StockUpdateResponseDto getStockLevelsFromPos(final StockUpdateDto stockUpdateDto)
            throws TargetWebMethodsClientException {

        if (!active) {
            return super.getStockLevelsFromPos(stockUpdateDto);
        }

        if (fixedResponse != null) {

            return fixedResponse;
        }

        final StockUpdateResponseDto stockUpdateResponse = new StockUpdateResponseDto();
        final List<StockUpdateStoreDto> stockUpdateDTOS = stockUpdateDto.getStockUpdateStoreDtos();
        final List<StockUpdateStoreResponseDto> stockUpdateStoreResponseDtos = new ArrayList<>();
        for (final StockUpdateStoreDto stockUpdateDTO : stockUpdateDTOS) {
            if (storeStock.containsKey(stockUpdateDTO.getStoreNumber())) {
                stockUpdateStoreResponseDtos.add(storeStock.get(stockUpdateDTO.getStoreNumber()));
            }
        }
        stockUpdateResponse.setStockUpdateStoreResponseDtos(stockUpdateStoreResponseDtos);
        return stockUpdateResponse;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.mock.MockedByTarget#isActive()
     */
    @Override
    public boolean isActive() {
        return active;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.mock.MockedByTarget#setActive(boolean)
     */
    @Override
    public void setActive(final boolean active) {
        this.active = active;
    }
}
