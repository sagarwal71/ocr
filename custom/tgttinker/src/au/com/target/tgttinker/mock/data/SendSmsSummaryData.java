/**
 * 
 */
package au.com.target.tgttinker.mock.data;

import au.com.target.tgttinker.mock.sendSms.TargetSendSmsClientMock;


/**
 * @author mjanarth
 * 
 */
public class SendSmsSummaryData extends CommonSummaryData {
    private String mockResponse = "";
    private String mockStatus = "";
    private String[] validMockResponses = TargetSendSmsClientMock.MOCK_RESPONSE.toArray(new String[] {});
    private String mockDefault = TargetSendSmsClientMock.MOCK_SUCCESS;

    /**
     * @return the mockResponse
     */
    public String getMockResponse() {
        return mockResponse;
    }

    /**
     * @param mockResponse
     *            the mockResponse to set
     */
    public void setMockResponse(final String mockResponse) {
        this.mockResponse = mockResponse;
    }

    /**
     * @return the mockStatus
     */
    public String getMockStatus() {
        return mockStatus;
    }

    /**
     * @param mockStatus
     *            the mockStatus to set
     */
    public void setMockStatus(final String mockStatus) {
        this.mockStatus = mockStatus;
    }

    /**
     * @return the mockDefault
     */
    public String getMockDefault() {
        return mockDefault;
    }

    /**
     * @param mockDefault
     *            the mockDefault to set
     */
    public void setMockDefault(final String mockDefault) {
        this.mockDefault = mockDefault;
    }

    /**
     * @return the validMockResponses
     */
    public String[] getValidMockResponses() {
        return validMockResponses;
    }

    /**
     * @param validMockResponses
     *            the validMockResponses to set
     */
    public void setValidMockResponses(final String[] validMockResponses) {
        this.validMockResponses = validMockResponses;
    }


}
