/**
 * 
 */
package au.com.target.tgttinker.mock.payment;

import de.hybris.platform.payment.AdapterException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.fest.util.Collections;

import au.com.target.tgtpaymentprovider.ipg.client.impl.IPGClientImpl;
import au.com.target.tgtpaymentprovider.ipg.data.CardDetails;
import au.com.target.tgtpaymentprovider.ipg.data.CardResultData;
import au.com.target.tgtpaymentprovider.ipg.data.CardResultsData;
import au.com.target.tgtpaymentprovider.ipg.data.SavedCardsData;
import au.com.target.tgtpaymentprovider.ipg.data.TransactionDetails;
import au.com.target.tgtpaymentprovider.ipg.data.request.SingleVoidRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSinglePaymentRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSingleRefundRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.TokenizeRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.TransactionQueryRequest;
import au.com.target.tgtpaymentprovider.ipg.data.response.SessionResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.SingleVoidResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.SubmitSinglePaymentResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.SubmitSingleRefundResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.TransactionsResultResponse;
import au.com.target.tgtpaymentprovider.ipg.data.soap.QueryResponse;
import au.com.target.tgtpaymentprovider.ipg.data.soap.QueryTransaction;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Response;
import au.com.target.tgtpaymentprovider.ipg.exception.IPGClientException;
import au.com.target.tgttinker.mock.MockedByTarget;


/**
 * @author htan3
 *
 */
public class IpgClientMock extends IPGClientImpl implements MockedByTarget {

    public static final String CC_CARDNUMBER = "5123450000000346";

    private static final String TRANSACTION_TYPE_IMMEDIATE = "1";
    private static final String TRANSACTION_TYPE_DEFERRED = "0";
    private boolean declineRefund = false;
    private boolean active = false;
    private int amount;
    private String checkResponseCode = "1";
    private String singlePayResponseCode;
    private String singlePayDeclinedCode;
    private String singlePayDeclinedMsg;
    private boolean mockFetchToken;
    private SubmitSingleRefundRequest refundRequest;
    private final String[] mockRefundResponseList = { "Success", "Decline" };
    private String defaultRefundResponse = "Success";
    private String declineCode;
    private String declineMessage;
    private String mockRefundResponse = "Success";
    private CardDetails customerCardData;
    private boolean giftCardPayment;
    private List<CardDetails> savedCards = new ArrayList<>();
    private CardResultsData cardResultsData;
    private final Map<String, Boolean> ccSubmitSinglePayResponseMap = new HashMap<String, Boolean>();
    private final Map<String, Boolean> submitSingleRefundResponseMap = new HashMap<String, Boolean>();
    private final Map<String, Boolean> submitSingleVoidResponseMap = new HashMap<String, Boolean>();
    private QueryResponse queryResponse;
    private boolean mockQuery = false;
    private boolean mockSubmitSinglePayment = false;
    private boolean serviceUnavailable = false;

    /**
     * @return the serviceUnavailable
     */
    public boolean isServiceUnavailable() {
        return serviceUnavailable;
    }

    /**
     * @param serviceUnavailable
     *            the serviceUnavailable to set
     */
    public void setServiceUnavailable(final boolean serviceUnavailable) {
        this.serviceUnavailable = serviceUnavailable;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(final boolean active) {
        this.active = active;

    }

    @Override
    public SessionResponse requestIPGSessionToken(final TokenizeRequest request) throws IPGClientException {
        if (active && mockFetchToken) {

            if (serviceUnavailable) {
                throw new IPGClientException("IPG is down");
            }
            amount = Integer.parseInt(request.getTenderAmount());
            final double cartAmount = Double.parseDouble(request.getTenderAmount());
            if (cartAmount < 1000) {
                throw new IPGClientException("Amount < 10 ==> Service Unavailable");
            }
            else {
                final SessionResponse response = new SessionResponse();
                response.setSecureSessionToken(request.getSessionKey());
                response.setSessionStored(true);
                response.setSessionStoredError(null);
                return response;
            }
        }
        return super.requestIPGSessionToken(request);
    }

    @Override
    public TransactionsResultResponse getTransactionResults(final TransactionQueryRequest request)
            throws IPGClientException {

        if (active && mockQuery) {
            if (serviceUnavailable) {
                throw new IPGClientException("IPG is down");
            }
            final TransactionsResultResponse response = new TransactionsResultResponse();
            response.setAmount("" + amount);
            response.setCustomerNumber("12345");
            response.setCustReference("test@test.com");
            response.setSessionId("123-456-789");
            response.setSessionKey(request.getSessionId());
            if (StringUtils.isNotEmpty(checkResponseCode)) {
                response.setTransactionResult(checkResponseCode);
            }
            response.setCardResults(populateCardResults());
            if (CollectionUtils.isNotEmpty(savedCards)) {
                final SavedCardsData savedCardsData = populateSavedCards();
                response.setSavedCards(savedCardsData);
            }
            return response;
        }
        else {
            return super.getTransactionResults(request);
        }

    }

    @Override
    public SubmitSinglePaymentResponse submitSinglePayment(final SubmitSinglePaymentRequest request)
            throws IPGClientException {
        if (active && mockSubmitSinglePayment) {
            if (serviceUnavailable) {
                throw new IPGClientException("IPG is down");
            }
            return populateSubmitSinglePaymentResponse(request);
        }
        else {
            return super.submitSinglePayment(request);
        }

    }

    @Override
    public SubmitSingleRefundResponse submitSingleRefund(final SubmitSingleRefundRequest request)
            throws IPGClientException {
        final SubmitSingleRefundResponse refundResponse = new SubmitSingleRefundResponse();
        final Response response = new Response();
        refundRequest = request;
        if (active) {
            final String receiptNumber = request.getRefundInfo().getReceiptNumber();
            if (submitSingleRefundResponseMap.isEmpty()) {
                setRefundResponse(response, mockRefundResponse.equalsIgnoreCase("decline"));
            }
            else {
                setRefundResponse(response, submitSingleRefundResponseMap.containsKey(receiptNumber)
                        && submitSingleRefundResponseMap.get(receiptNumber).booleanValue());
            }
            response.setReceipt(receiptNumber);
            response.setTimestamp("2038-01-19 03:14:07");
            refundResponse.setResponse(response);
            return refundResponse;
        }
        else {
            return super.submitSingleRefund(request);
        }
    }

    @Override
    public SingleVoidResponse submitSingleVoid(final SingleVoidRequest request)
            throws IPGClientException {
        final SingleVoidResponse voidResponse = new SingleVoidResponse();
        final Response response = new Response();
        if (active) {
            final String receiptNumber = request.getVoidInfo().getReceiptNumber();
            if (submitSingleVoidResponseMap.isEmpty()) {
                setRefundResponse(response, mockRefundResponse.equalsIgnoreCase("decline"));
            }
            else {
                setRefundResponse(response, submitSingleVoidResponseMap.containsKey(receiptNumber)
                        && submitSingleVoidResponseMap.get(receiptNumber).booleanValue());
            }
            response.setReceipt(receiptNumber);
            response.setTimestamp("2038-01-19 03:14:07");
            voidResponse.setResponse(response);
            return voidResponse;
        }
        else {
            return super.submitSingleVoid(request);
        }
    }

    @Override
    public QueryResponse queryTransaction(final QueryTransaction queryRequest) throws IPGClientException {
        if (active && mockQuery) {
            return queryResponse;
        }
        else {
            return super.queryTransaction(queryRequest);
        }
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return "" + amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final String amount) {
        this.amount = Integer.parseInt(amount);
    }

    /**
     * 
     * @return CardResultsData
     */
    private CardResultsData populateCardResults() {
        if (cardResultsData != null) {
            return cardResultsData;
        }
        cardResultsData = new CardResultsData();
        final List<CardResultData> cardResults = new ArrayList<>();
        final CardResultData cardResult = new CardResultData();
        cardResult.setCardDetails(populateCardDetails());
        cardResult.setTransactionDetails(populateTransactionDetails(amount,
                cardResult.getCardDetails().getCardNumber(), isGiftCardPayment()));
        cardResults.add(cardResult);
        cardResultsData.setCardResults(cardResults);
        return cardResultsData;
    }

    public static TransactionDetails populateTransactionDetails(final int amount, final String cardNumber,
            final boolean isGiftcardPayment) {
        final TransactionDetails transactionDetails = new TransactionDetails();
        transactionDetails.setAmount("" + amount);
        transactionDetails.setReceiptNumber(cardNumber);
        transactionDetails.setResponseCode("0");
        transactionDetails.setResponseText("Approved");
        transactionDetails.setTransDateTime("2015-11-11 15:35:12");
        transactionDetails.setSettlementDate("2015-11-12");
        if (isGiftcardPayment) {
            transactionDetails.setTransType(TRANSACTION_TYPE_IMMEDIATE);
        }
        else {
            transactionDetails.setTransType(TRANSACTION_TYPE_DEFERRED);
        }
        return transactionDetails;
    }

    /**
     * Populate payment response
     * 
     * @param request
     * 
     * @return SubmitSinglePaymentResponse
     */
    private SubmitSinglePaymentResponse populateSubmitSinglePaymentResponse(final SubmitSinglePaymentRequest request) {
        final SubmitSinglePaymentResponse paymentResponse = new SubmitSinglePaymentResponse();
        final Response response = new Response();

        final String cardNumber = request.getTransaction().getCreditCard().getMaskedCardNumber();
        if (ccSubmitSinglePayResponseMap.isEmpty()) {
            response.setDeclinedMessage(singlePayDeclinedMsg);
            response.setDeclinedCode("0".equals(singlePayResponseCode) ? null : singlePayDeclinedCode);
            response.setResponseCode(singlePayResponseCode);
        }
        else {
            if (ccSubmitSinglePayResponseMap.containsKey(cardNumber)) {
                final boolean declined = ccSubmitSinglePayResponseMap.get(cardNumber).booleanValue();
                if (declined) {
                    response.setDeclinedMessage(singlePayDeclinedMsg);
                    response.setDeclinedCode(singlePayDeclinedCode);
                    response.setResponseCode("1");
                }
                else {
                    response.setResponseCode("0");
                }
            }
        }
        if ("AdapterException".equals(singlePayDeclinedMsg)) {
            throw new AdapterException(singlePayDeclinedMsg);
        }
        response.setReceipt(cardNumber);
        response.setTimestamp("2038-01-19 03:14:07");
        paymentResponse.setResponse(response);
        return paymentResponse;
    }

    /**
     * @param response
     * @param declined
     */
    private void setRefundResponse(final Response response, final boolean declined) {
        if (declined) {
            response.setResponseCode("1");
            response.setDeclinedCode(declineCode);
            response.setDeclinedMessage(declineMessage);
        }
        else {
            response.setResponseCode("0");
            response.setDeclinedCode(null);
            response.setDeclinedMessage(null);
        }
    }

    /**
     * Populates card details
     * 
     * @return CardDetails
     */
    private CardDetails populateCardDetails() {
        CardDetails cardDetails = new CardDetails();
        if (null != customerCardData) {
            customerCardData.setBin("1234");
            customerCardData.setTokenExpiry("12/2020");
            customerCardData.setIsDefaultCard("0");
            return customerCardData;
        }
        else if (!Collections.isEmpty(savedCards)) {
            cardDetails = savedCards.get(0);
            return cardDetails;
        }
        else {
            if (isGiftCardPayment()) {
                cardDetails.setCardNumber("627345******0002");
                cardDetails.setBin("627345");
                cardDetails.setCardType("Giftcard");
            }
            else {
                cardDetails.setCardNumber(CC_CARDNUMBER);
                cardDetails.setCardType("VISA");
                cardDetails.setToken("99442123456704242");
                cardDetails.setCardExpiry("12/2020");
                cardDetails.setTokenExpiry("12/2020");
                cardDetails.setBin("1234");
                cardDetails.setIsDefaultCard("0");
            }
            return cardDetails;
        }

    }

    /**
     * Populates saved card details
     * 
     * @return SavedCardsData
     */
    private SavedCardsData populateSavedCards() {
        final SavedCardsData savedCardsData = new SavedCardsData();
        savedCardsData.setCardRequests(savedCards);
        return savedCardsData;
    }

    /**
     * @param checkResponseCode
     *            the checkResponseCode to set
     */
    public void setCheckResponseCode(final String checkResponseCode) {
        this.checkResponseCode = checkResponseCode;
    }

    /**
     * @param singlePayResponseCode
     *            the singlePayResponseCode to set
     */
    public void setSinglePayResponseCode(final String singlePayResponseCode) {
        this.singlePayResponseCode = singlePayResponseCode;
    }

    /**
     * @return the declineRefund
     */
    public boolean isDeclineRefund() {
        return declineRefund;
    }

    /**
     * @param declineRefund
     *            the declineRefund to set
     */
    public void setDeclineRefund(final boolean declineRefund) {
        this.declineRefund = declineRefund;
    }

    /**
     * @return the refundRequest
     */
    public SubmitSingleRefundRequest getRefundRequest() {
        return refundRequest;
    }

    /**
     * @param refundRequest
     */
    public void setRefundRequest(final SubmitSingleRefundRequest refundRequest) {
        this.refundRequest = refundRequest;
    }

    /**
     * @return the mockRefundResponse
     */
    public String[] getMockRefundResponseList() {
        return mockRefundResponseList;
    }

    /**
     * Get the current mock response
     * 
     * @return mock response
     */
    public String getRefundResponse() {
        return defaultRefundResponse;
    }

    /**
     * Set the response from future mocking requests
     * 
     * @param mockResponseStr
     *            response to give
     */
    public void setRefundResponse(final String mockResponseStr) {
        for (final String validResponse : mockRefundResponseList) {
            if (validResponse.equalsIgnoreCase(mockResponseStr)) {
                this.defaultRefundResponse = validResponse;
                return;
            }
        }
    }

    /**
     * @return the declineCode
     */
    public String getDeclineCode() {
        return declineCode;
    }

    /**
     * @param declineCode
     *            the declineCode to set
     */
    public void setDeclineCode(final String declineCode) {
        this.declineCode = declineCode;
    }

    /**
     * @return the declineMessage
     */
    public String getDeclineMessage() {
        return declineMessage;
    }

    /**
     * @param declineMessage
     *            the declineMessage to set
     */
    public void setDeclineMessage(final String declineMessage) {
        this.declineMessage = declineMessage;
    }

    /**
     * @return the mockRefundResponse
     */
    public String getMockRefundResponse() {
        return mockRefundResponse;
    }

    /**
     * @param mockRefundResponse
     *            the mockRefundResponse to set
     */
    public void setMockRefundResponse(final String mockRefundResponse) {
        this.mockRefundResponse = mockRefundResponse;
    }

    /**
     * @return the savedCreditCard
     */
    public List<CardDetails> getSavedCards() {
        return savedCards;
    }

    public void resetSavedCards() {
        savedCards = new ArrayList<>();
    }

    /**
     * Set saved credit card information
     * 
     * @param cardType
     * @param cardNumber
     * @param cardExpiry
     * @param token
     * @param tokenExpiry
     * @param bin
     * @param defaultCard
     */
    @SuppressWarnings("boxing")
    public void addSavedCreditCard(final String cardType, final String cardNumber, final String cardExpiry,
            final String token, final String tokenExpiry, final String bin, final Boolean defaultCard) {
        final CardDetails savedCreditCard = new CardDetails();
        savedCreditCard.setCardType(cardType);
        savedCreditCard.setCardNumber(cardNumber);
        savedCreditCard.setCardExpiry(cardExpiry);
        savedCreditCard.setToken(tokenExpiry);
        savedCreditCard.setTokenExpiry(tokenExpiry);
        savedCreditCard.setBin(bin);
        savedCreditCard.setIsDefaultCard(defaultCard ? "1" : "0");
        savedCards.add(savedCreditCard);
    }

    /**
     * @param customerCardData
     *            the customerCardData to set
     */
    public void setCustomerCardData(final CardDetails customerCardData) {
        this.cardResultsData = null;
        this.customerCardData = customerCardData;
    }

    /**
     * @param giftCardPayment
     *            the giftCardPayment to set
     */
    public void setGiftCardPayment(final boolean giftCardPayment) {
        this.giftCardPayment = giftCardPayment;
    }

    /**
     * @return the giftCardPayment
     */
    public boolean isGiftCardPayment() {
        return giftCardPayment;
    }

    /**
     * @return the cardResultsData
     */
    public CardResultsData getCardResultsData() {
        return cardResultsData;
    }

    /**
     * @param cardResultsData
     *            the cardResultData to set
     */
    public void setCardResultsData(final CardResultsData cardResultsData) {
        this.cardResultsData = cardResultsData;
    }

    /**
     * @return the singlePayDeclinedMsg
     */
    public String getSinglePayDeclinedMsg() {
        return singlePayDeclinedMsg;
    }

    /**
     * @param singlePayDeclinedMsg
     *            the singlePayDeclinedMsg to set
     */
    public void setSinglePayDeclinedMsg(final String singlePayDeclinedMsg) {
        this.singlePayDeclinedMsg = singlePayDeclinedMsg;
    }

    /**
     * @return the singlePayResponseCode
     */
    public String getSinglePayResponseCode() {
        return singlePayResponseCode;
    }

    /**
     * @return the singlePayDeclinedCode
     */
    public String getSinglePayDeclinedCode() {
        return singlePayDeclinedCode;
    }

    /**
     * @param singlePayDeclinedCode
     *            the singlePayDeclinedCode to set
     */
    public void setSinglePayDeclinedCode(final String singlePayDeclinedCode) {
        this.singlePayDeclinedCode = singlePayDeclinedCode;
    }

    public void addCardResponse(final String cardNumber, final Boolean declined) {
        ccSubmitSinglePayResponseMap.put(cardNumber, declined);
    }

    public void addRefundResponse(final String cardNumber, final Boolean declined) {
        submitSingleRefundResponseMap.put(cardNumber, declined);
    }

    public void addVoidResponse(final String cardNumber, final Boolean declined) {
        submitSingleVoidResponseMap.put(cardNumber, declined);
    }

    /**
     * @return the mockFetchToken
     */
    public boolean isMockFetchToken() {
        return mockFetchToken;
    }

    /**
     * @param mockFetchToken
     *            the mockFetchToken to set
     */
    public void setMockFetchToken(final boolean mockFetchToken) {
        this.mockFetchToken = mockFetchToken;
    }

    /**
     * @param queryResponse
     */
    public void setQueryResponse(final QueryResponse queryResponse) {
        this.queryResponse = queryResponse;
    }

    /**
     * @return the mockQuery
     */
    public boolean isMockQuery() {
        return mockQuery;
    }

    /**
     * @param mockQuery
     *            the mockQuery to set
     */
    public void setMockQuery(final boolean mockQuery) {
        this.mockQuery = mockQuery;
    }

    /**
     * @return the mockSubmitSinglePayment
     */
    public boolean isMockSubmitSinglePayment() {
        return mockSubmitSinglePayment;
    }

    /**
     * @param mockSubmitSinglePayment
     *            the mockSubmitSinglePayment to set
     */
    public void setMockSubmitSinglePayment(final boolean mockSubmitSinglePayment) {
        this.mockSubmitSinglePayment = mockSubmitSinglePayment;
    }

    public void reset() {
        declineRefund = false;
        active = false;
        amount = 0;
        checkResponseCode = "1";
        singlePayResponseCode = null;
        singlePayDeclinedCode = null;
        singlePayDeclinedMsg = null;
        mockFetchToken = false;
        refundRequest = null;
        defaultRefundResponse = "Success";
        declineCode = null;
        declineMessage = null;
        mockRefundResponse = "Success";
        customerCardData = null;
        giftCardPayment = false;
        savedCards = new ArrayList<>();
        cardResultsData = null;
        ccSubmitSinglePayResponseMap.clear();
        submitSingleRefundResponseMap.clear();
        submitSingleVoidResponseMap.clear();
        mockQuery = false;
        mockSubmitSinglePayment = false;
        queryResponse = null;
        serviceUnavailable = false;
    }

}