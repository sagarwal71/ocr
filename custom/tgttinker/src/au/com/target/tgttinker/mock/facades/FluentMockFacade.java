/**
 * 
 */
package au.com.target.tgttinker.mock.facades;

import java.util.Map;


/**
 * @author mgazal
 *
 */
public interface FluentMockFacade extends ClusterAwareMockFacade {

    /**
     * @param parameterMap
     */
    void updateMock(final Map<String, String> parameterMap);

    /**
     * @param batchActive
     *            the batchActive to set
     */
    void setBatchActive(final String batchActive);

    /**
     * @param createJobResponse
     *            the createJobResponse to set
     */
    void setCreateJobResponse(final String createJobResponse);

    /**
     * @param createBatchResponse
     *            the createBatchResponse to set
     */
    void setCreateBatchResponse(final String createBatchResponse);

    /**
     * @param batchResponse
     *            the batchResponse to set
     */
    void setBatchResponse(final String batchResponse);

    /**
     * @param createCategoryActive
     *            the createCategoryActive to set
     */
    void setCreateCategoryActive(final String createCategoryActive);

    /**
     * @param updateCategoryActive
     *            the updateCategoryActive to set
     */
    void setUpdateCategoryActive(final String updateCategoryActive);

    /**
     * @param searchCategoryActive
     *            the searchCategoryActive to set
     */
    void setSearchCategoryActive(final String searchCategoryActive);

    /**
     * @param createCategoryResponse
     *            the createCategoryResponse to set
     */
    void setCreateCategoryResponse(final String createCategoryResponse);

    /**
     * @param updateCategoryResponse
     *            the updateCategoryResponse to set
     */
    void setUpdateCategoryResponse(final String updateCategoryResponse);

    /**
     * @param searchCategoryResponse
     *            the searchCategoryResponse to set
     */
    void setSearchCategoryResponse(final String searchCategoryResponse);

    /**
     * @param createProductActive
     *            the createProductActive to set
     */
    void setCreateProductActive(final String createProductActive);

    /**
     * @param updateProductActive
     *            the updateProductActive to set
     */
    void setUpdateProductActive(final String updateProductActive);

    /**
     * @param searchProductActive
     *            the searchProductActive to set
     */
    void setSearchProductActive(final String searchProductActive);

    /**
     * @param createProductResponse
     *            the createProductResponse to set
     */
    void setCreateProductResponse(final String createProductResponse);

    /**
     * @param updateProductResponse
     *            the updateProductResponse to set
     */
    void setUpdateProductResponse(final String updateProductResponse);

    /**
     * @param searchProductResponse
     *            the searchProductResponse to set
     */
    void setSearchProductResponse(final String searchProductResponse);

    /**
     * @param createSkuActive
     *            the createSkuActive to set
     */
    void setCreateSkuActive(final String createSkuActive);

    /**
     * @param updateSkuActive
     *            the updateSkuActive to set
     */
    void setUpdateSkuActive(final String updateSkuActive);

    /**
     * @param searchSkuActive
     *            the searchSkuActive to set
     */
    void setSearchSkuActive(final String searchSkuActive);

    /**
     * @param createSkuResponse
     *            the createSkuResponse to set
     */
    void setCreateSkuResponse(final String createSkuResponse);

    /**
     * @param updateSkuResponse
     *            the updateSkuResponse to set
     */
    void setUpdateSkuResponse(final String updateSkuResponse);

    /**
     * @param searchSkuResponse
     *            the searchSkuResponse to set
     */
    void setSearchSkuResponse(final String searchSkuResponse);

    /**
     * @param createFulfilmentActive
     *            the createFulfilmentActive to set
     */
    void setCreateFulfilmentActive(final String createFulfilmentActive);

    /**
     * @param fulfilmentOptionResponse
     */
    void setFulfilmentOptionResponse(final String fulfilmentOptionResponse);

    /**
     * @param fulfilmentOptionMap
     *            the fulfilmentOptionMap to set
     */
    void setFulfilmentOptionMap(final String fulfilmentOptionMap);

    /**
     * 
     * @param createOrderActive
     *            the createOrderActive to set
     */
    void setCreateOrderActive(final String createOrderActive);

    /**
     * 
     * @param createOrderResponse
     *            the createOrderResponse to set
     */
    void setCreateOrderResponse(final String createOrderResponse);

    /**
     * @param eventSyncActive
     *            the eventSyncActive to set
     */
    void setEventSyncActive(final String eventSyncActive);


    /**
     * @param eventResponse
     *            the eventResponse to set
     */
    void setEventResponse(final String eventResponse);

    void reset();

    /**
     * 
     * @param retrieveFulfilmentsByOrderActive
     */
    void setRetrieveFulfilmentsByOrderActive(final String retrieveFulfilmentsByOrderActive);

    /**
     * 
     * @param fulfilmentsResponse
     */
    void setFulfilmentsResponse(final String fulfilmentsResponse);

    /**
     * 
     * @param getOrderActive
     */
    void setViewOrderActive(final String getOrderActive);

    /**
     * 
     * @param getOrderResponse
     */
    void setViewOrderResponse(final String getOrderResponse);
}
