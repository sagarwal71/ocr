/**
 * 
 */
package au.com.target.tgttinker.mock.auspost;

import java.io.File;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.target.tgtauspost.integration.dto.AuspostRequestDTO;
import au.com.target.tgtfulfilment.integration.dto.IntegrationError;
import au.com.target.tgtfulfilment.integration.dto.LabelResponseDTO;
import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgtwebmethods.dispatchlabel.client.impl.AusPostDispatchLabelClientImpl;


/**
 * Mock for DispatchLabelClient
 * 
 * @author sbryan6
 *
 */
public class MockDispatchLabelClient extends AusPostDispatchLabelClientImpl implements MockedByTarget {

    public enum DispatchLabelResponseType {
        SUCCESS, ERROR, UNAVAILABLE
    }

    private static final Logger LOG = Logger.getLogger(MockDispatchLabelClient.class);

    private DispatchLabelResponseType responseType = DispatchLabelResponseType.SUCCESS;

    private AuspostRequestDTO lastRequest;

    private boolean active;

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(final boolean active) {
        this.active = active;
    }


    @Override
    public LabelResponseDTO retrieveDispatchLabel(final AuspostRequestDTO request) {

        if (!active) {
            return super.retrieveDispatchLabel(request);
        }

        lastRequest = request;

        final String requestJSON = getLastResponseAsJSON();
        LOG.info("MockTransmitManifestClientImpl received request. Response=" + responseType
                + ", Request=" + requestJSON);

        return getResponse();
    }


    /**
     * @return the lastRequest
     */
    public AuspostRequestDTO getLastRequest() {
        return lastRequest;
    }

    /**
     * @return the lastRequest as JSON
     */
    public String getLastRequestAsJSON() {
        return getLastResponseAsJSON();
    }

    /**
     * @param responseType
     *            the responseType to set
     */
    public void setResponseType(final DispatchLabelResponseType responseType) {
        this.responseType = responseType;
    }

    /**
     * Reset this mock
     */
    public void reset() {

        lastRequest = null;
        setResponseType(DispatchLabelResponseType.SUCCESS);
    }


    private LabelResponseDTO getResponse() {

        final LabelResponseDTO response;

        switch (responseType) {

            case SUCCESS:
                response = createSuccessResponse();
                break;
            case ERROR:
                response = createErrorResponse();
                break;
            case UNAVAILABLE:
                response = createUnavailableResponse();
                break;
            default:
                throw new IllegalStateException("Unrecognised response type: " + responseType);
        }

        return response;
    }

    private byte[] createDummyPDFData() {

        // Load test PDF
        byte[] data = null;

        try {
            final URL url = getClass().getResource("/tgttinker/test/test.pdf");
            final File pdfFile = new File(url.toURI());
            data = FileUtils.readFileToByteArray(pdfFile);
        }
        catch (final Exception e) {
            LOG.error("Failed to read file data", e);
        }

        return data;
    }



    private LabelResponseDTO createSuccessResponse() {
        final LabelResponseDTO response = new LabelResponseDTO();
        response.setSuccess(true);

        // Add test pdf to response
        response.setData(createDummyPDFData());

        return response;
    }

    private LabelResponseDTO createErrorResponse() {
        final LabelResponseDTO response = new LabelResponseDTO();
        response.setErrorType(IntegrationError.REMOTE_SERVICE_ERROR);
        response.setErrorCode("ERROR");
        response.setErrorValue("Mock Error");
        response.setSuccess(false);
        return response;
    }

    private LabelResponseDTO createUnavailableResponse() {
        final LabelResponseDTO response = new LabelResponseDTO();
        response.setErrorType(IntegrationError.CONNECTION_ERROR);
        response.setErrorCode("UNAVAILABLE");
        response.setErrorValue("WebMethods not available");
        response.setSuccess(false);
        return response;
    }

    private String getLastResponseAsJSON() {

        final String requestJSON;
        try {
            final ObjectMapper mapper = new ObjectMapper();
            requestJSON = mapper.writeValueAsString(lastRequest);
        }
        catch (final Exception e) {
            throw new IllegalStateException("Could not convert request to JSON");
        }

        return requestJSON;
    }


}
