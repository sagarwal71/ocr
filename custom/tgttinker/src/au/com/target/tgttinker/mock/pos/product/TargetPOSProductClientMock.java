/**
 * 
 */
package au.com.target.tgttinker.mock.pos.product;

import au.com.target.tgtsale.product.dto.request.ProductRequestDto;
import au.com.target.tgtsale.product.dto.response.ProductResponseDto;
import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgtwebcore.exception.TargetIntegrationException;
import au.com.target.tgtwebmethods.product.client.impl.TargetPOSProductClientImpl;


/**
 * @author rmcalave
 *
 */
public class TargetPOSProductClientMock extends TargetPOSProductClientImpl implements MockedByTarget {

    private boolean active;

    private ProductResponseDto mockProductResponseDto;

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.product.client.impl.TargetPOSProductClientImpl#getPOSProductDetails(au.com.target.tgtsale.product.dto.request.ProductRequestDto)
     */
    @Override
    public ProductResponseDto getPOSProductDetails(final ProductRequestDto request) throws TargetIntegrationException {
        if (!active) {
            return super.getPOSProductDetails(request);
        }

        return mockProductResponseDto;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.mock.MockedByTarget#isActive()
     */
    @Override
    public boolean isActive() {
        return active;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.mock.MockedByTarget#setActive(boolean)
     */
    @Override
    public void setActive(final boolean active) {
        this.active = active;
    }

    /**
     * @return the mockProductResponseDto
     */
    public ProductResponseDto getMockProductResponseDto() {
        return mockProductResponseDto;
    }

    /**
     * @param mockProductResponseDto
     *            the mockProductResponseDto to set
     */
    public void setMockProductResponseDto(final ProductResponseDto mockProductResponseDto) {
        this.mockProductResponseDto = mockProductResponseDto;
    }

}
