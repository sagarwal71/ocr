/**
 * 
 */
package au.com.target.tgttinker.mock.data;

import au.com.target.tgttinker.mock.fraud.provider.AccertifyFraudServiceProviderMock;



/**
 *
 */
public class AcertifySummaryData extends CommonSummaryData {
    private String mockResponse = "";
    private final String[] validMockResponses = AccertifyFraudServiceProviderMock.VALID_MOCK_RESPONSE
            .toArray(new String[] {});

    /**
     * @return the mockResponse
     */
    public String getMockResponse() {
        return mockResponse;
    }

    /**
     * @param mockResponse
     *            the mockResponse to set
     */
    public void setMockResponse(final String mockResponse) {
        this.mockResponse = mockResponse;
    }

    /**
     * @return the validMockResponses
     */
    public String[] getValidMockResponses() {
        return validMockResponses;
    }
}
