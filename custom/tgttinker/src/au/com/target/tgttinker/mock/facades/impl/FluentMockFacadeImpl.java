/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import de.hybris.platform.core.Registry;

import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfluent.client.FluentClient;
import au.com.target.tgtfluent.data.BatchResponse;
import au.com.target.tgtfluent.data.CategoryResponse;
import au.com.target.tgtfluent.data.EventResponse;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.data.FulfilmentOptionResponse;
import au.com.target.tgtfluent.data.FulfilmentsResponse;
import au.com.target.tgtfluent.data.OrderResponse;
import au.com.target.tgtfluent.data.ProductsResponse;
import au.com.target.tgtfluent.data.SkusResponse;
import au.com.target.tgttinker.mock.data.FluentSummaryData;
import au.com.target.tgttinker.mock.event.MockParameterEventType;
import au.com.target.tgttinker.mock.facades.FluentMockFacade;
import au.com.target.tgttinker.mock.fluent.FluentClientMock;
import au.com.target.tgtutility.util.JsonConversionUtil;


/**
 * @author mgazal
 *
 */
public class FluentMockFacadeImpl extends BaseClusterAwareMockFacade implements FluentMockFacade {

    private FluentClient fluentClient;

    private final FluentSummaryData summaryData = new FluentSummaryData();

    @Override
    public void updateMock(final Map<String, String> parameterMap) {
        setBatchActive(parameterMap.get("batchActive"));
        setCreateJobResponse(parameterMap.get("createJobResponse"));
        setCreateBatchResponse(parameterMap.get("createBatchResponse"));
        setBatchResponse(parameterMap.get("batchResponse"));
        setCreateCategoryActive(parameterMap.get("createCategoryActive"));
        setUpdateCategoryActive(parameterMap.get("updateCategoryActive"));
        setSearchCategoryActive(parameterMap.get("searchCategoryActive"));
        setCreateCategoryResponse(parameterMap.get("createCategoryResponse"));
        setUpdateCategoryResponse(parameterMap.get("updateCategoryResponse"));
        setSearchCategoryResponse(parameterMap.get("searchCategoryResponse"));
        setCreateProductActive(parameterMap.get("createProductActive"));
        setUpdateProductActive(parameterMap.get("updateProductActive"));
        setSearchProductActive(parameterMap.get("searchProductActive"));
        setCreateProductResponse(parameterMap.get("createProductResponse"));
        setUpdateProductResponse(parameterMap.get("updateProductResponse"));
        setSearchProductResponse(parameterMap.get("searchProductResponse"));
        setCreateSkuActive(parameterMap.get("createSkuActive"));
        setUpdateSkuActive(parameterMap.get("updateSkuActive"));
        setSearchSkuActive(parameterMap.get("searchSkuActive"));
        setCreateSkuResponse(parameterMap.get("createSkuResponse"));
        setUpdateSkuResponse(parameterMap.get("updateSkuResponse"));
        setSearchSkuResponse(parameterMap.get("searchSkuResponse"));
        setCreateFulfilmentActive(parameterMap.get("createFulfilmentActive"));
        setFulfilmentOptionResponse(parameterMap.get("fulfilmentOptionResponse"));
        setFulfilmentOptionMap(parameterMap.get("fulfilmentOptionMap"));
        upsertCcAts(parameterMap.get("upsertCcAts"));
        upsertHdAts(parameterMap.get("upsertHdAts"));
        upsertEdAts(parameterMap.get("upsertEdAts"));
        upsertConsolidatedStoreSoh(parameterMap.get("upsertConsolidatedStoreSoh"));
        setCreateOrderActive(parameterMap.get("createOrderActive"));
        setCreateOrderResponse(parameterMap.get("createOrderResponse"));
        setEventSyncActive(parameterMap.get("eventSyncActive"));
        setEventResponse(parameterMap.get("eventResponse"));
        setRetrieveFulfilmentsByOrderActive(parameterMap.get("retrieveFulfilmentsByOrderActive"));
        setFulfilmentsResponse(parameterMap.get("fulfilmentsResponse"));
        setViewOrderActive(parameterMap.get("viewOrderActive"));
        setViewOrderResponse(parameterMap.get("viewOrderResponse"));
    }

    @Override
    public void setActive(final boolean enable) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock.setActive(enable);
            summaryData.setMockActive(enable);
        }
    }

    @Override
    public FluentSummaryData getSummary() {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            summaryData.setBatchActive(fluentClientMock.isBatchActive());
            summaryData.setCreateJobResponse(
                    JsonConversionUtil.convertToPrettyJsonString(fluentClientMock.getCreateJobResponse()));
            summaryData.setCreateBatchResponse(
                    JsonConversionUtil.convertToPrettyJsonString(fluentClientMock.getCreateBatchResponse()));
            summaryData.setCreateBatchResponse(
                    JsonConversionUtil.convertToPrettyJsonString(fluentClientMock.getCreateBatchResponse()));
            summaryData.setBatchResponse(
                    JsonConversionUtil.convertToPrettyJsonString(fluentClientMock.getBatchResponse()));
            summaryData.setCreateCategoryActive(fluentClientMock.isCreateCategoryActive());
            summaryData.setUpdateCategoryActive(fluentClientMock.isUpdateCategoryActive());
            summaryData.setSearchCategoryActive(fluentClientMock.isSearchCategoryActive());
            summaryData.setCreateCategoryResponse(
                    JsonConversionUtil.convertToPrettyJsonString(fluentClientMock.getCreateCategoryResponse()));
            summaryData.setUpdateCategoryResponse(
                    JsonConversionUtil.convertToPrettyJsonString(fluentClientMock.getUpdateCategoryResponse()));
            summaryData.setSearchCategoryResponse(
                    JsonConversionUtil.convertToPrettyJsonString(fluentClientMock.getSearchCategoryResponse()));
            summaryData.setCreateProductActive(fluentClientMock.isCreateProductActive());
            summaryData.setUpdateProductActive(fluentClientMock.isUpdateProductActive());
            summaryData.setSearchProductActive(fluentClientMock.isSearchProductActive());
            summaryData.setCreateProductResponse(
                    JsonConversionUtil.convertToPrettyJsonString(fluentClientMock.getCreateProductResponse()));
            summaryData.setUpdateProductResponse(
                    JsonConversionUtil.convertToPrettyJsonString(fluentClientMock.getUpdateProductResponse()));
            summaryData.setSearchProductResponse(
                    JsonConversionUtil.convertToPrettyJsonString(fluentClientMock.getSearchProductResponse()));
            summaryData.setCreateSkuActive(fluentClientMock.isCreateSkuActive());
            summaryData.setUpdateSkuActive(fluentClientMock.isUpdateSkuActive());
            summaryData.setSearchSkuActive(fluentClientMock.isSearchSkuActive());
            summaryData.setCreateSkuResponse(
                    JsonConversionUtil.convertToPrettyJsonString(fluentClientMock.getCreateSkuResponse()));
            summaryData.setUpdateSkuResponse(
                    JsonConversionUtil.convertToPrettyJsonString(fluentClientMock.getUpdateSkuResponse()));
            summaryData.setSearchSkuResponse(
                    JsonConversionUtil.convertToPrettyJsonString(fluentClientMock.getSearchSkuResponse()));
            summaryData.setCreateFulfilmentActive(fluentClientMock.isCreateFulfilmentActive());
            summaryData.setFulfilmentOptionResponse(
                    JsonConversionUtil.convertToPrettyJsonString(fluentClientMock.getFulfilmentOptionResponse()));
            summaryData.setFulfilmentOptionMap(
                    JsonConversionUtil.convertToPrettyJsonString(fluentClientMock.getFulfilmentOptionMap()));
            summaryData.setCreateOrderActive(fluentClientMock.isCreateOrderActive());
            summaryData.setCreateOrderResponse(
                    JsonConversionUtil.convertToPrettyJsonString(fluentClientMock.getCreateOrderResponse()));
            summaryData.setEventSyncActive(fluentClientMock.isEventSyncActive());
            summaryData.setEventResponse(
                    JsonConversionUtil.convertToPrettyJsonString(fluentClientMock.getEventResponse()));
            summaryData.setRetrieveFulfilmentsByOrderActive(fluentClientMock.isRetrieveFulfilmentsByOrderActive());
            summaryData.setFulfilmentsResponse(
                    JsonConversionUtil.convertToPrettyJsonString(fluentClientMock.getFulfilmentsResponse()));

            summaryData.setViewOrderActive(fluentClientMock.isViewOrderActive());
            summaryData.setViewOrderResponse(
                    JsonConversionUtil.convertToPrettyJsonString(fluentClientMock.getViewOrderResponse()));
        }
        return summaryData;
    }

    @Override
    public void setBatchActive(final String parameter) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            final boolean batchActive = BooleanUtils.toBoolean(parameter);
            fluentClientMock.setBatchActive(batchActive);
        }
    }

    @Override
    public void setCreateJobResponse(final String createJobResponse) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock
                    .setCreateJobResponse(JsonConversionUtil.convertToObject(createJobResponse, FluentResponse.class));
        }
    }

    @Override
    public void setCreateBatchResponse(final String createBatchResponse) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock.setCreateBatchResponse(
                    JsonConversionUtil.convertToObject(createBatchResponse, FluentResponse.class));
        }
    }

    @Override
    public void setBatchResponse(final String batchResponse) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock.setBatchResponse(
                    JsonConversionUtil.convertToObject(batchResponse, BatchResponse.class));
        }
    }

    @Override
    public void setCreateCategoryActive(final String parameter) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            final boolean createCategoryActive = BooleanUtils.toBoolean(parameter);
            fluentClientMock.setCreateCategoryActive(createCategoryActive);
        }
    }

    @Override
    public void setUpdateCategoryActive(final String parameter) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            final boolean updateCategoryActive = BooleanUtils.toBoolean(parameter);
            fluentClientMock.setUpdateCategoryActive(updateCategoryActive);
        }
    }

    @Override
    public void setSearchCategoryActive(final String parameter) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            final boolean searchCategoryActive = BooleanUtils.toBoolean(parameter);
            fluentClientMock.setSearchCategoryActive(searchCategoryActive);
        }
    }

    @Override
    public void setCreateCategoryResponse(final String createCategoryResponse) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock.setCreateCategoryResponse(
                    JsonConversionUtil.convertToObject(createCategoryResponse, FluentResponse.class));
        }
    }

    @Override
    public void setUpdateCategoryResponse(final String updateCategoryResponse) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock.setUpdateCategoryResponse(
                    JsonConversionUtil.convertToObject(updateCategoryResponse, FluentResponse.class));
        }

    }

    @Override
    public void setSearchCategoryResponse(final String searchCategoryResponse) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock.setSearchCategoryResponse(
                    JsonConversionUtil.convertToObject(searchCategoryResponse, CategoryResponse.class));
        }
    }

    @Override
    public void setCreateProductActive(final String parameter) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            final boolean createProductActive = BooleanUtils.toBoolean(parameter);
            fluentClientMock.setCreateProductActive(createProductActive);
        }
    }

    @Override
    public void setUpdateProductActive(final String parameter) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            final boolean updateProductActive = BooleanUtils.toBoolean(parameter);
            fluentClientMock.setUpdateProductActive(updateProductActive);
        }
    }

    @Override
    public void setSearchProductActive(final String parameter) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            final boolean searchProductActive = BooleanUtils.toBoolean(parameter);
            fluentClientMock.setSearchProductActive(searchProductActive);
        }
    }

    @Override
    public void setCreateProductResponse(final String createProductResponse) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock.setCreateProductResponse(
                    JsonConversionUtil.convertToObject(createProductResponse, FluentResponse.class));
        }
    }

    @Override
    public void setUpdateProductResponse(final String updateProductResponse) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock.setUpdateProductResponse(
                    JsonConversionUtil.convertToObject(updateProductResponse, FluentResponse.class));
        }
    }

    @Override
    public void setSearchProductResponse(final String searchProductResponse) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock.setSearchProductResponse(
                    JsonConversionUtil.convertToObject(searchProductResponse, ProductsResponse.class));
        }
    }

    @Override
    public void setCreateSkuActive(final String parameter) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            final boolean createSkuActive = BooleanUtils.toBoolean(parameter);
            fluentClientMock.setCreateSkuActive(createSkuActive);
        }
    }

    @Override
    public void setUpdateSkuActive(final String parameter) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            final boolean updateSkuActive = BooleanUtils.toBoolean(parameter);
            fluentClientMock.setUpdateSkuActive(updateSkuActive);
        }
    }

    @Override
    public void setSearchSkuActive(final String parameter) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            final boolean searchSkuActive = BooleanUtils.toBoolean(parameter);
            fluentClientMock.setSearchSkuActive(searchSkuActive);
        }
    }

    @Override
    public void setCreateSkuResponse(final String createSkuResponse) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock.setCreateSkuResponse(
                    JsonConversionUtil.convertToObject(createSkuResponse, FluentResponse.class));
        }
    }

    @Override
    public void setUpdateSkuResponse(final String updateSkuResponse) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock.setUpdateSkuResponse(
                    JsonConversionUtil.convertToObject(updateSkuResponse, FluentResponse.class));
        }
    }

    @Override
    public void setSearchSkuResponse(final String searchSkuResponse) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock.setSearchSkuResponse(
                    JsonConversionUtil.convertToObject(searchSkuResponse, SkusResponse.class));
        }
    }

    @Override
    public void setCreateFulfilmentActive(final String parameter) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            final boolean createFulfilmentActive = BooleanUtils.toBoolean(parameter);
            fluentClientMock.setCreateFulfilmentActive(createFulfilmentActive);
        }
    }

    @Override
    public void setFulfilmentOptionResponse(final String fulfilmentOptionResponse) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock.setFulfilmentOptionResponse(
                    JsonConversionUtil.convertToObject(fulfilmentOptionResponse, FulfilmentOptionResponse.class));
        }
    }

    @Override
    public void setFulfilmentOptionMap(final String fulfilmentOptionMap) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock.setFulfilmentOptionMap(JsonConversionUtil.convertToObject(fulfilmentOptionMap, Map.class));
        }
    }

    /**
     * @param upsertMapString
     */
    private void upsertCcAts(final String upsertMapString) {
        if (getFluentClient() instanceof FluentClientMock) {
            final Map<String, Map<String, Integer>> fulfilmentOptionMap = ((FluentClientMock)getFluentClient())
                    .getFulfilmentOptionMap();
            if (fulfilmentOptionMap == null) {
                return;
            }
            final Map<String, Integer> deliveryTypeMap = fulfilmentOptionMap.get(TgtCoreConstants.DELIVERY_TYPE.CC);
            if (deliveryTypeMap == null) {
                return;
            }
            final Map upsertMap = JsonConversionUtil.convertToObject(upsertMapString, Map.class);
            if (MapUtils.isNotEmpty(upsertMap)) {
                deliveryTypeMap.putAll(upsertMap);
            }
        }
    }

    /**
     * @param upsertMapString
     */
    private void upsertHdAts(final String upsertMapString) {
        if (getFluentClient() instanceof FluentClientMock) {
            final Map<String, Map<String, Integer>> fulfilmentOptionMap = ((FluentClientMock)getFluentClient())
                    .getFulfilmentOptionMap();
            if (fulfilmentOptionMap == null) {
                return;
            }
            final Map<String, Integer> deliveryTypeMap = fulfilmentOptionMap.get(TgtCoreConstants.DELIVERY_TYPE.HD);
            if (deliveryTypeMap == null) {
                return;
            }
            final Map upsertMap = JsonConversionUtil.convertToObject(upsertMapString, Map.class);
            if (MapUtils.isNotEmpty(upsertMap)) {
                deliveryTypeMap.putAll(upsertMap);
            }
        }
    }

    /**
     * @param upsertMapString
     */
    private void upsertEdAts(final String upsertMapString) {
        if (getFluentClient() instanceof FluentClientMock) {
            final Map<String, Map<String, Integer>> fulfilmentOptionMap = ((FluentClientMock)getFluentClient())
                    .getFulfilmentOptionMap();
            if (fulfilmentOptionMap == null) {
                return;
            }
            final Map<String, Integer> deliveryTypeMap = fulfilmentOptionMap.get(TgtCoreConstants.DELIVERY_TYPE.ED);
            if (deliveryTypeMap == null) {
                return;
            }
            final Map upsertMap = JsonConversionUtil.convertToObject(upsertMapString, Map.class);
            if (MapUtils.isNotEmpty(upsertMap)) {
                deliveryTypeMap.putAll(upsertMap);
            }
        }
    }

    /**
     * @param upsertMapString
     */
    private void upsertConsolidatedStoreSoh(final String upsertMapString) {
        if (getFluentClient() instanceof FluentClientMock) {
            final Map<String, Map<String, Integer>> fulfilmentOptionMap = ((FluentClientMock)getFluentClient())
                    .getFulfilmentOptionMap();
            if (fulfilmentOptionMap == null) {
                return;
            }
            final Map<String, Integer> deliveryTypeMap = fulfilmentOptionMap
                    .get(TgtCoreConstants.DELIVERY_TYPE.CONSOLIDATED_STORES_SOH);
            if (deliveryTypeMap == null) {
                return;
            }
            final Map upsertMap = JsonConversionUtil.convertToObject(upsertMapString, Map.class);
            if (MapUtils.isNotEmpty(upsertMap)) {
                deliveryTypeMap.putAll(upsertMap);
            }
        }
    }

    @Override
    public void setCreateOrderActive(final String parameter) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            final boolean createOrderActive = BooleanUtils.toBoolean(parameter);
            fluentClientMock.setCreateOrderActive(createOrderActive);
        }
    }

    @Override
    public void setCreateOrderResponse(final String createOrderResponse) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock.setCreateOrderResponse(
                    JsonConversionUtil.convertToObject(createOrderResponse, FluentResponse.class));
        }
    }

    @Override
    public void setEventSyncActive(final String parameter) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            final boolean eventSyncActive = BooleanUtils.toBoolean(parameter);
            fluentClientMock.setEventSyncActive(eventSyncActive);
        }
    }

    @Override
    public void setEventResponse(final String eventResponse) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock.setEventResponse(JsonConversionUtil.convertToObject(eventResponse, EventResponse.class));
        }
    }

    @Override
    public void setRetrieveFulfilmentsByOrderActive(final String parameter) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            final boolean retrieveFulfilmentsByOrderActive = BooleanUtils.toBoolean(parameter);
            fluentClientMock.setRetrieveFulfilmentsByOrderActive(retrieveFulfilmentsByOrderActive);
        }
    }

    @Override
    public void setFulfilmentsResponse(final String fulfilmentsResponse) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock.setFulfilmentsResponse(
                    JsonConversionUtil.convertToObject(fulfilmentsResponse, FulfilmentsResponse.class));
        }
    }

    @Override
    public void setViewOrderActive(final String parameter) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            final boolean viewOrderActive = BooleanUtils.toBoolean(parameter);
            fluentClientMock.setViewOrderActive(viewOrderActive);
        }
    }

    @Override
    public void setViewOrderResponse(final String viewOrderResponse) {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock.setViewOrderResponse(
                    JsonConversionUtil.convertToObject(viewOrderResponse, OrderResponse.class));
        }

    }

    @Override
    public void reset() {
        if (getFluentClient() instanceof FluentClientMock) {
            final FluentClientMock fluentClientMock = (FluentClientMock)getFluentClient();
            fluentClientMock.reset();
        }
    }

    /**
     * @return the fluentClient
     */
    public FluentClient getFluentClient() {
        if (fluentClient == null) {
            fluentClient = (FluentClient)Registry.getApplicationContext().getBean("fluentClient");
        }
        return fluentClient;
    }

    @Override
    public MockParameterEventType getEventType() {
        return MockParameterEventType.FLUENT;
    }
}
