/**
 * 
 */
package au.com.target.tgttinker.mock.facades;



/**
 * @author pthoma20
 *
 */
public interface InventoryAdjustmentMockFacade extends CommonMockFacade {

    void setMockValues(final boolean success);
}