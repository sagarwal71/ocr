/**
 * 
 */
package au.com.target.tgttinker.mock.data;

/**
 * @author rmcalave
 *
 */
public class CustomerSubscriptionSummaryData extends CommonSummaryData {
    private Integer responseCode;

    private String responseMessage;

    private String responseType;

    /**
     * @return the responseCode
     */
    public Integer getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode
     *            the responseCode to set
     */
    public void setResponseCode(final Integer responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage
     *            the responseMessage to set
     */
    public void setResponseMessage(final String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the responseType
     */
    public String getResponseType() {
        return responseType;
    }

    /**
     * @param responseType
     *            the responseType to set
     */
    public void setResponseType(final String responseType) {
        this.responseType = responseType;
    }
}
