/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import static org.junit.Assert.fail;

import de.hybris.platform.core.Registry;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.BooleanUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.target.tgtpaymentprovider.afterpay.client.TargetAfterpayClient;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CapturePaymentResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CreateOrderResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CreateRefundResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetConfigurationResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetOrderResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetPaymentResponse;
import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgttinker.mock.data.AfterpaySummaryData;
import au.com.target.tgttinker.mock.facades.AfterpayMockFacade;
import au.com.target.tgttinker.mock.payment.TargetAfterpayClientMock;


/**
 * @author gsing236
 *
 */
public class AfterpayMockFacadeImpl implements AfterpayMockFacade {

    private TargetAfterpayClient afterpayClient;

    private final AfterpaySummaryData summaryData = new AfterpaySummaryData();

    @Override
    public void setActive(final boolean enable) {
        if (getAfterpayClient() instanceof MockedByTarget) {
            final MockedByTarget mockedBean = (MockedByTarget)getAfterpayClient();
            mockedBean.setActive(enable);
            summaryData.setMockActive(enable);
        }
    }

    @Override
    public AfterpaySummaryData getSummary() {
        if (summaryData.getMockedGetConfigurationResponse() == null) {
            // set default data else user populated data remain
            summaryData.setMockedGetConfigurationResponse(readJson("/afterpay/getConfigurationResponse.json"));
        }
        if (summaryData.getMockedGetOrderResponse() == null) {
            summaryData.setMockedGetOrderResponse(readJson("/afterpay/getOrderResponse.json"));
        }
        if (summaryData.getMockedCreateOrderResponse() == null) {
            summaryData.setMockedCreateOrderResponse(readJson("/afterpay/createOrderResponse.json"));
        }
        if (summaryData.getMockedCapturePaymentResponse() == null) {
            summaryData.setMockedCapturePaymentResponse(readJson("/afterpay/capturePaymentResponse.json"));
        }
        if (summaryData.getMockedGetPaymentResponse() == null) {
            summaryData.setMockedGetPaymentResponse(readJson("/afterpay/getPaymentResponse.json"));
        }
        if (summaryData.getMockedCreateRefundResponse() == null) {
            summaryData.setMockedCreateRefundResponse(readJson("/afterpay/createRefundResponse.json"));
        }
        return summaryData;
    }

    @Override
    public void setThrowException(final String parameter) {
        if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
            final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
            final boolean throwException = BooleanUtils.toBoolean(parameter);
            afterpayClientMock.setThrowExpection(throwException);
            summaryData.setThrowException(throwException);
        }
    }

    @Override
    public void setCreateOrderActive(final String parameter) {
        if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
            final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
            final boolean createOrderActive = BooleanUtils.toBoolean(parameter);
            afterpayClientMock.setCreateOrderActive(createOrderActive);
            summaryData.setCreateOrderActive(createOrderActive);
        }
    }

    @Override
    public void mockGetConfigurationResponse(final String mock) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            final GetConfigurationResponse mockedData = mapper.readValue(mock, GetConfigurationResponse.class);
            if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
                final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
                summaryData.setMockedGetConfigurationResponse(mock);
                afterpayClientMock.setGetConfigurationResponse(mockedData);
            }
        }
        catch (final IOException e) {
            throw new IllegalArgumentException("mock data is not in expected format");
        }

    }

    @Override
    public void mockGetOrderResponse(final String mock) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            final GetOrderResponse mockedData = mapper.readValue(mock, GetOrderResponse.class);
            if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
                final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
                summaryData.setMockedGetOrderResponse(mock);
                afterpayClientMock.setGetOrderResponse(mockedData);
            }
        }
        catch (final IOException e) {
            throw new IllegalArgumentException("mock data is not in expected format");
        }

    }

    @Override
    public void mockCreateOrderResponse(final String mock) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            final CreateOrderResponse mockedData = mapper.readValue(mock, CreateOrderResponse.class);
            if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
                final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
                summaryData.setMockedCreateOrderResponse(mock);
                afterpayClientMock.setCreateOrderResponse(mockedData);
            }
        }
        catch (final IOException e) {
            throw new IllegalArgumentException("mock data is not in expected format");
        }

    }

    @Override
    public void mockCapturePaymentResponse(final String mock) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            final CapturePaymentResponse mockedData = mapper.readValue(mock, CapturePaymentResponse.class);
            if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
                final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
                summaryData.setMockedCapturePaymentResponse(mock);
                afterpayClientMock.setCapturePaymentResponse(mockedData);
            }
        }
        catch (final IOException e) {
            throw new IllegalArgumentException("mock data is not in expected format");
        }

    }

    @Override
    public void setCapturePaymentException(final String parameter) {
        if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
            final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
            final boolean capturePaymentException = BooleanUtils.toBoolean(parameter);
            afterpayClientMock.setCapturePaymentException(capturePaymentException);
            summaryData.setCapturePaymentException(capturePaymentException);
        }
    }

    @Override
    public void setGetOrderExpection(final String parameter) {
        if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
            final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
            final boolean getOrderException = BooleanUtils.toBoolean(parameter);
            afterpayClientMock.setGetOrderExpection(getOrderException);
            summaryData.setGetOrderExpection(getOrderException);
        }
    }

    @Override
    public void setGetOrderActive(final String parameter) {
        if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
            final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
            final boolean getorderActive = BooleanUtils.toBoolean(parameter);
            afterpayClientMock.setGetOrderActive(getorderActive);
            summaryData.setGetOrderActive(getorderActive);
        }
    }

    @Override
    public void setCapturePaymentActive(final String parameter) {
        if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
            final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
            final boolean capturePaymentActive = BooleanUtils.toBoolean(parameter);
            afterpayClientMock.setCapturePaymentActive(capturePaymentActive);
            summaryData.setCapturePaymentActive(capturePaymentActive);
        }
    }

    @Override
    public void setCreateOrderException(final String parameter) {
        if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
            final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
            final boolean createOrderException = BooleanUtils.toBoolean(parameter);
            afterpayClientMock.setCreateOrderException(createOrderException);
            summaryData.setCreateOrderException(createOrderException);
        }
    }

    @Override
    public void setPingActive(final String parameter) {
        if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
            final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
            final boolean pingActive = BooleanUtils.toBoolean(parameter);
            afterpayClientMock.setPingActive(pingActive);
            summaryData.setPingActive(pingActive);
        }
    }

    @Override
    public void setGetPaymentActive(final String parameter) {
        if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
            final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
            final boolean getPaymentActive = BooleanUtils.toBoolean(parameter);
            afterpayClientMock.setGetPaymentActive(getPaymentActive);
            summaryData.setGetPaymentActive(getPaymentActive);
        }
    }

    @Override
    public void setGetPaymentException(final String parameter) {
        if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
            final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
            final boolean getPaymentException = BooleanUtils.toBoolean(parameter);
            afterpayClientMock.setGetPaymentException(getPaymentException);
            summaryData.setGetPaymentException(getPaymentException);
        }
    }

    @Override
    public void mockGetPaymentResponse(final String mock) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            final GetPaymentResponse mockedData = mapper.readValue(mock, GetPaymentResponse.class);
            if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
                final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
                summaryData.setMockedGetPaymentResponse(mock);
                afterpayClientMock.setGetPaymentResponse(mockedData);
            }
        }
        catch (final IOException e) {
            throw new IllegalArgumentException("mock data is not in expected format");
        }

    }

    @Override
    public void mockCreateRefundResponse(final String mock) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            final CreateRefundResponse mockedData = mapper.readValue(mock, CreateRefundResponse.class);
            if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
                final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
                summaryData.setMockedCreateRefundResponse(mock);
                afterpayClientMock.setCreateRefundResponse(mockedData);
            }
        }
        catch (final IOException e) {
            throw new IllegalArgumentException("mock data is not in expected format");
        }
    }

    @Override
    public void setCreateRefundException(final String mock) {
        if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
            final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
            final boolean createRefundException = BooleanUtils.toBoolean(mock);
            afterpayClientMock.setCreateRefundException(createRefundException);
            summaryData.setCreateRefundException(createRefundException);
        }
    }

    @Override
    public void setCreateRefundActive(final String mock) {
        if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
            final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
            final boolean createRefundActive = BooleanUtils.toBoolean(mock);
            afterpayClientMock.setCreateRefundActive(createRefundActive);
            summaryData.setCreateRefundActive(createRefundActive);
        }
    }

    @Override
    public void resetDefault() {
        if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
            final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
            afterpayClientMock.resetDefault();
        }
    }

    private String readJson(final String filename) {
        final InputStream is = AfterpayMockFacadeImpl.class.getResourceAsStream(filename);
        String jsonTxt = null;
        try {
            jsonTxt = IOUtils.toString(is);
        }
        catch (final IOException e) {
            fail("fail to read json");
        }
        return jsonTxt;
    }

    @Override
    public void setGetConfigurationActive(final String mock) {
        if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
            final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
            final boolean getConfigurationActive = BooleanUtils.toBoolean(mock);
            afterpayClientMock.setGetConfigurationActive(getConfigurationActive);
            summaryData.setGetConfigurationActive(getConfigurationActive);
        }
    }

    @Override
    public void setGetConfigurationException(final String mock) {
        if (getAfterpayClient() instanceof TargetAfterpayClientMock) {
            final TargetAfterpayClientMock afterpayClientMock = (TargetAfterpayClientMock)getAfterpayClient();
            final boolean getConfigurationException = BooleanUtils.toBoolean(mock);
            afterpayClientMock.setGetConfigurationException(getConfigurationException);
            summaryData.setGetConfigurationException(getConfigurationException);
        }
    }

    /**
     * @return the afterpayClient
     */
    public TargetAfterpayClient getAfterpayClient() {
        if (afterpayClient == null) {
            afterpayClient = (TargetAfterpayClient)Registry.getApplicationContext().getBean("targetAfterpayClient");
        }
        return afterpayClient;
    }

}
