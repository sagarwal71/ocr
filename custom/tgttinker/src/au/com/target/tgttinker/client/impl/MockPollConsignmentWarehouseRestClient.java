/**
 * 
 */
package au.com.target.tgttinker.client.impl;

import au.com.target.tgtfulfilment.integration.dto.IntegrationError;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseRequest;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseResponse;
import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgtwebmethods.client.impl.WMPollConsignmentWarehouseRestClient;


/**
 * @author gsing236
 *
 */
public class MockPollConsignmentWarehouseRestClient extends WMPollConsignmentWarehouseRestClient
        implements MockedByTarget {

    private boolean active;

    private boolean mockSuccess;

    private boolean mockServiceUnavailable;

    @Override
    public PollWarehouseResponse pollConsignments(final PollWarehouseRequest request) {
        if (active) {
            if (mockSuccess) {
                return PollWarehouseResponse.getSuccessResponse();
            }

            if (mockServiceUnavailable) {
                final PollWarehouseResponse response = new PollWarehouseResponse();
                response.setErrorType(IntegrationError.CONNECTION_ERROR);
                response.setSuccess(false);
                return response;
            }
        }
        return super.pollConsignments(request);
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(final boolean active) {
        this.active = active;

    }

    /**
     * @return the mockSuccess
     */
    public boolean isMockSuccess() {
        return mockSuccess;
    }

    /**
     * @param mockSuccess
     *            the mockSuccess to set
     */
    public void setMockSuccess(final boolean mockSuccess) {
        this.mockSuccess = mockSuccess;
    }

    /**
     * @return the mockServiceUnavailable
     */
    public boolean isMockServiceUnavailable() {
        return mockServiceUnavailable;
    }

    /**
     * @param mockServiceUnavailable
     *            the mockServiceUnavailable to set
     */
    public void setMockServiceUnavailable(final boolean mockServiceUnavailable) {
        this.mockServiceUnavailable = mockServiceUnavailable;
    }

    public void resetDefault() {
        active = false;
        mockSuccess = false;
        mockServiceUnavailable = false;
    }

}
