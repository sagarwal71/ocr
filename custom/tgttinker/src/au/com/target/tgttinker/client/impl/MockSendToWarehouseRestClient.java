/**
 * 
 */
package au.com.target.tgttinker.client.impl;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.target.tgtfulfilment.integration.dto.IntegrationError;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseRequest;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseResponse;
import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgtwebmethods.client.impl.WMSendToWarehouseRestClient;


/**
 * Mock for SendToWarehouseRestClient implementation.
 * 
 * @author jjayawa1
 *
 */
public class MockSendToWarehouseRestClient extends WMSendToWarehouseRestClient implements MockedByTarget {

    private static final Logger LOG = Logger.getLogger(MockSendToWarehouseRestClient.class);

    public enum SendToWarehouseResponseType {
        SUCCESS, ERROR, UNAVAILABLE
    }

    private SendToWarehouseResponseType responseType = SendToWarehouseResponseType.SUCCESS;

    private SendToWarehouseRequest lastRequest;

    private boolean active;

    private String errorValue;

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(final boolean active) {
        this.active = active;
    }

    @Override
    public SendToWarehouseResponse sendConsignment(final SendToWarehouseRequest request) {

        if (!active) {
            return super.sendConsignment(request);
        }

        lastRequest = request;

        final String requestJSON = getLastRequestAsJSON();
        LOG.info("MockSendToWarehouseRestClient received request. Response=" + responseType
                + ", Request=" + requestJSON);

        final SendToWarehouseResponse response = getResponse();

        if (!response.isSuccess()) {
            return processErroneousResponse(request, response);
        }

        return response;
    }

    /**
     * @return the lastRequest
     */
    public SendToWarehouseRequest getLastRequest() {
        return lastRequest;
    }

    /**
     * @param responseType
     *            the responseType to set
     */
    public void setResponseType(final SendToWarehouseResponseType responseType) {
        this.responseType = responseType;
    }

    /**
     * Reset this mock
     */
    public void reset() {
        lastRequest = null;
        setResponseType(SendToWarehouseResponseType.SUCCESS);
        setErrorValue(null);
    }


    private SendToWarehouseResponse getResponse() {

        final SendToWarehouseResponse response;

        switch (responseType) {

            case SUCCESS:
                response = createSuccessResponse();
                break;
            case ERROR:
                response = createErrorResponse();
                break;
            case UNAVAILABLE:
                response = createUnavailableResponse();
                break;
            default:
                throw new IllegalStateException("Unrecognised response type: " + responseType);
        }

        return response;
    }

    private SendToWarehouseResponse createSuccessResponse() {
        final SendToWarehouseResponse response = new SendToWarehouseResponse();
        response.setSuccess(true);
        return response;
    }

    private SendToWarehouseResponse createErrorResponse() {
        final SendToWarehouseResponse response = new SendToWarehouseResponse();
        response.setErrorType(IntegrationError.REMOTE_SERVICE_ERROR);
        response.setErrorCode("ERROR");
        response.setErrorValue(errorValue != null ? errorValue : "Test Error occured");
        response.setSuccess(false);
        return response;
    }

    private SendToWarehouseResponse createUnavailableResponse() {
        final SendToWarehouseResponse response = new SendToWarehouseResponse();
        response.setErrorType(IntegrationError.CONNECTION_ERROR);
        response.setErrorCode("UNAVAILABLE");
        response.setErrorValue("WebMethods not available");
        response.setSuccess(false);
        return response;
    }

    private String getLastRequestAsJSON() {

        final String requestJSON;
        try {
            final ObjectMapper mapper = new ObjectMapper();
            requestJSON = mapper.writeValueAsString(lastRequest);
        }
        catch (final Exception e) {
            throw new IllegalStateException("Could not convert request to JSON");
        }

        return requestJSON;
    }

    /**
     * @param errorValue
     *            the errorValue to set
     */
    public void setErrorValue(final String errorValue) {
        this.errorValue = errorValue;
    }

}
