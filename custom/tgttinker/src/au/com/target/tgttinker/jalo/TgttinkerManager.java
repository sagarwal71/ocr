package au.com.target.tgttinker.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;

import au.com.target.tgttinker.constants.TgttinkerConstants;


public class TgttinkerManager extends GeneratedTgttinkerManager {
    public static final TgttinkerManager getInstance() {
        final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
        return (TgttinkerManager)em.getExtension(TgttinkerConstants.EXTENSIONNAME);
    }
}
