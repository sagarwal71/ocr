/**
 * 
 */
package au.com.target.tgttest.selenium.data;

/**
 * Maps a department to a content page.
 * 
 * @author sswor
 * 
 */
public enum DepartmentToContentMapping {

    SCHOOL_ARTICLES(DepartmentType.SCHOOL, ContentType.ARTICLES);

    private final DepartmentType department;
    private final ContentType content;

    private DepartmentToContentMapping(final DepartmentType department, final ContentType content)
    {
        this.department = department;
        this.content = content;
    }

    /**
     * @return the department
     */
    public DepartmentType getDepartment() {
        return department;
    }

    /**
     * @return the content
     */
    public ContentType getContent() {
        return content;
    }


}
