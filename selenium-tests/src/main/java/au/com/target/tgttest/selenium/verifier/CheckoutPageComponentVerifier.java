/**
 * 
 */
package au.com.target.tgttest.selenium.verifier;

import static org.fest.assertions.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.List;

import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.datastore.bean.Product;
import au.com.target.tgttest.selenium.pageobjects.checkout.BaseCheckoutStepPage;
import au.com.target.tgttest.selenium.util.PriceUtil;


/**
 * A util class which helps to verify some re-occuring tests on checkout pages.
 * 
 * @author maesi
 * 
 */
public final class CheckoutPageComponentVerifier {

    /*expected stock on hand change validation stubbs*/
    private static final String EXPECTED_CONTENT_SOH_CHANGED = "Stock levels have changed";
    private static final String EXPECTED_CONTENT_SOH_CLEARED =
            "These products have been removed from your Buy Now basket";

    private static final String EXPECTED_CLICK_AND_COLLECT_DELIVERY_FEE = "FREE";
    private static final String EXPECTED_DELIVERY_FEE_CAPTION_HOME_DELIVERY = "Home Delivery Fee";
    private static final String EXPECTED_DELIVERY_FEE_CAPTION_CLICK_AND_COLLECT = "Click + Collect Fee";


    /**
     * private constructor
     */
    private CheckoutPageComponentVerifier() {

    }


    /**
     * method to verify stock on hand change validation messages.
     * 
     * @param pageWithValidation
     *            the page with the validation error.
     * @param productNamesToTest
     *            a list with product names to test.
     */
    @SuppressWarnings("boxing")
    public static void verifyStockOnHandReducedValidationError(final BaseCheckoutStepPage pageWithValidation,
            final List<String> productNamesToTest) {
        final FluentWebElement errorMessageSOHChanged = pageWithValidation
                .getValidationErrorContainerWithMessageContaining(EXPECTED_CONTENT_SOH_CHANGED);
        errorMessageSOHChanged.isDisplayed().shouldBe(true);

        assertThat(pageWithValidation.isLinkInValidationErrorMessage(errorMessageSOHChanged)).isTrue();

        final String validationMessage = pageWithValidation.getValidationErrorMessage(errorMessageSOHChanged);
        assertThat(validationMessage).contains(EXPECTED_CONTENT_SOH_CHANGED);

        if (productNamesToTest != null && !productNamesToTest.isEmpty()) {
            for (final String name : productNamesToTest) {
                assertThat(
                        pageWithValidation.isProductnameContainedInProductListValidation(name, errorMessageSOHChanged))
                        .as("Expect product name in soh validations: " + name)
                        .isTrue();
            }
        }
    }

    /**
     * tests if the summary table contains a list of product names.
     * 
     * @param checkoutStepPage
     *            the current BaseCheckoutStepPage instance with a summary table on it.
     * @param productNamesToTest
     *            a List of product names which the summary table will be checked for.
     */
    public static void verifySummaryProductNames(final BaseCheckoutStepPage checkoutStepPage,
            final List<String> productNamesToTest) {
        if (productNamesToTest != null && !productNamesToTest.isEmpty()) {
            for (final String name : productNamesToTest) {
                assertThat(checkoutStepPage.isProductnameContainedInSummary(name))
                        .as("expect product name in summary: " + name).isTrue();
            }
        }
    }


    /**
     * checks the summary table product rows for a certain quantity
     * 
     * @param checkoutStepPage
     *            the current BaseCheckoutStepPage instance with a summary table on it.
     * @param expectedQuantity
     *            the expected quantity to test against.
     */
    public static void verifySummaryProductQuantities(final BaseCheckoutStepPage checkoutStepPage,
            final int expectedQuantity) {
        final List<FluentWebElement> allProductRowsFromSummary = checkoutStepPage.getAllProductsFromSummary();
        if (allProductRowsFromSummary != null && !allProductRowsFromSummary.isEmpty()) {
            for (final FluentWebElement productSummaryRow : allProductRowsFromSummary) {
                final String qtyToTest = checkoutStepPage.getSummaryProductItemQuantity(productSummaryRow);
                assertThat(Integer.parseInt(qtyToTest)).isEqualTo(expectedQuantity);
            }
        }
    }

    /**
     * method to verify cleared stock on hand validation messages.
     * 
     * @param pageWithValidation
     *            the page with the validation error.
     */
    @SuppressWarnings("boxing")
    public static void verifyStockOnHandClearedValidationError(final BaseCheckoutStepPage pageWithValidation) {
        final FluentWebElement errorMessageSOHCleared = pageWithValidation
                .getValidationErrorContainerWithMessageContaining(EXPECTED_CONTENT_SOH_CLEARED);
        errorMessageSOHCleared.isDisplayed().shouldBe(true);

        assertThat(pageWithValidation.isLinkInValidationErrorMessage(errorMessageSOHCleared))
                .as("expect not to have link in validations").isFalse();
        final String validationMessage = pageWithValidation.getValidationErrorMessage(errorMessageSOHCleared);
        assertThat(validationMessage).as("expect to have content in validations")
                .contains(EXPECTED_CONTENT_SOH_CLEARED);
    }



    /**
     * A static test method, which checks the Totalisation Summary Table on a certain BaseCheckoutStepPage.
     * 
     * @param page
     *            the base checkout step page (as a BaseCheckoutStepPage instance) with an expected totalisation summary
     *            table.
     * @param allItemsInBasket
     *            a List with all ShoppingBasketItems which belong to this checkout run.
     * @param isHomeDelivery
     *            boolean whether we are in a home delivery or click and collect checkout run.
     */
    public static void verifyTotalisationSummary(final BaseCheckoutStepPage page,
            final List<Product> allItemsInBasket, final boolean isHomeDelivery) {

        assertThat(page.isSummaryTableDisplayed()).as("expect summary table").isTrue();

        // test the item quantity, item total & totalisation...
        // TODO allow for qty > 1
        BigDecimal expectedTotal = BigDecimal.ZERO.setScale(2);

        for (final Product item : allItemsInBasket) {
            final BigDecimal rowTotal = item.getPrice();
            expectedTotal = expectedTotal.add(rowTotal);
        }

        // Both home delivery and cnc may attract a fee
        final BigDecimal deliveryFeeAmount = new BigDecimal(
                PriceUtil.getUnsignedValueFromCurrencyValue(page
                        .getSummaryDeliveryFeeAmount())).setScale(2);
        expectedTotal = expectedTotal.add(deliveryFeeAmount);

        final String expectedTotalPrice = "$" + expectedTotal.setScale(2).toString();

        assertThat(page.getSummaryTotalAmount()).as("summary total").isEqualTo(expectedTotalPrice);

        // test delivery fee and naming...
        assertThat(page.isDeliveryFeeSummaryDisplayed()).isTrue();
        if (isHomeDelivery) {
            assertThat(page.getDeliveryFeeSummaryCaption())
                    .as("delivery fee caption").isEqualTo(EXPECTED_DELIVERY_FEE_CAPTION_HOME_DELIVERY);
        }
        else {
            assertThat(page.getDeliveryFeeSummaryCaption())
                    .as("delivery fee caption").isEqualTo(EXPECTED_DELIVERY_FEE_CAPTION_CLICK_AND_COLLECT);
        }
    }
}
