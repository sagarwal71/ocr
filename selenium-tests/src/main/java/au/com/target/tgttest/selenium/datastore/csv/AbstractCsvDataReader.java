/**
 *
 */
package au.com.target.tgttest.selenium.datastore.csv;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;


/**
 * Base class for reading test data from CSV sources.
 * 
 * @param <T>
 *            the type of object loaded from CSV.
 */
public abstract class AbstractCsvDataReader<T> {

    protected static final Logger LOG = LogManager.getLogger(AbstractCsvDataReader.class);
    private String[] headers = null;

    /**
     * Loads instances from a file.
     * 
     * @param csvFile
     *            the file
     * @param hasHeaders
     *            {@code true} if the first line of CSV input represents field headers and not actual data
     * @return the objects
     * @throws IOException
     *             if bad things happen
     */
    public Collection<T> readFromCsvFile(final File csvFile, final boolean hasHeaders) throws IOException {
        InputStream in = null;
        try {
            in = new FileInputStream(csvFile);
            return readFromCsv(in, hasHeaders);
        }
        finally {
            IOUtils.closeQuietly(in);
        }
    }

    /**
     * Loads instances from a stream.
     * 
     * @param in
     *            the stream
     * @param hasHeaders
     *            {@code true} if the first line of CSV input represents field headers and not actual data
     * @return the objects
     * @throws IOException
     *             if bad things happen
     */
    protected Collection<T> readFromCsv(final InputStream in, final boolean hasHeaders) throws IOException {
        InputStreamReader reader = null;
        try {
            reader = new InputStreamReader(in);
            return readFromCsv(reader, hasHeaders);
        }
        finally {
            IOUtils.closeQuietly(reader);
        }
    }

    /**
     * Loads instances from a reader.
     * 
     * @param reader
     *            the reader
     * @param hasHeaders
     *            {@code true} if the first line of CSV input represents field headers and not actual data
     * @return the objects
     * @throws IOException
     *             if bad things happen
     */
    protected Collection<T> readFromCsv(final Reader reader, final boolean hasHeaders) throws IOException {
        CSVReader csvReader = null;
        try {
            csvReader = new CSVReader(reader, ';', '"', 0);
            return readFromCsv(csvReader, hasHeaders);
        }
        finally {
            IOUtils.closeQuietly(csvReader);
        }
    }

    /**
     * Loads instances from a CSV reader.
     * 
     * @param reader
     *            the reader
     * @return the objects
     * @throws IOException
     *             if bad things happen
     */
    protected Collection<T> readFromCsv(final CSVReader reader, final boolean hasHeaders) throws IOException {
        final Collection<T> results = new ArrayList<T>();
        String[] line = reader.readNext();
        boolean first = true;
        while (line != null) {
            if (line.length > 1) { // Weed out empty rows
                if (first && hasHeaders) {
                    first = false;
                    headers = line;
                }
                else {
                    results.add(createInstanceFromCsv(line));
                }
            }
            line = reader.readNext();
        }
        return results;
    }

    /**
     * Creates an object instance from a single line of CSV data.
     * 
     * @param csvData
     *            the data
     * @return the object
     */
    protected abstract T createInstanceFromCsv(final String[] csvData);


    public String getValueForHeader(final String header, final String[] csvData) {
        int i = 0;
        for (final String item : headers) {
            if (item.equalsIgnoreCase(header)) {
                return csvData[i];
            }
            i++;
        }
        return null;
    }

    public String getNullableValueForHeader(final String header, final String[] csvData) {
        final String value = getValueForHeader(header, csvData);

        if (value == null || value.length() == 0) {
            return null;
        }
        else {
            return value;
        }
    }

    /**
     * Get the price value for the field and return null if empty or cannot be parsed.
     * 
     * @param header
     * @param csvData
     * @return price or null if none
     */
    public BigDecimal getPriceValueForHeader(final String header, final String[] csvData) {
        final String value = getValueForHeader(header, csvData);

        if (value == null) {
            return null;
        }

        double price = 0;

        try {
            price = Double.parseDouble(value);
        }
        catch (final NumberFormatException e) {
            // Leave as 0
            LOG.error("Could not parse header value to double: " + header + ", " + value);
            return null;
        }

        return BigDecimal.valueOf(price).setScale(2);
    }
}
