/**
 * 
 */
package au.com.target.tgttest.selenium.data;

/**
 * Represents an external page type.
 */
public enum ExternalType {
    /** External Page type. */
    CreditLoyaltyCards(null, "Credit + Loyalty Cards", "http://www.target.com.au/html/cards/cards.htm"),
    /** External Page type. */
    DesignersForTarget(null, "Designers For Target", "http://www.designersfortarget.com.au/"),
    /** External Page type. */
    PersonalShopping(null, "Personal Shopping", "http://www.target.com.au/html/personalshopping/howitworks.htm"),
    /** External Page type. */
    Photobooks(null, "Photobooks", "http://www.albumprinter.com.au/target-photobooks/"),
    /** External Page type. */
    Services(null, "Services", "http://www.target.com.au/html/services/services.htm"),
    /** External Page type. */
    Company(null, "Company", "http://www.target.com.au"),
    Stubbies(DepartmentType.SCHOOL, "Stubbies",
            "http://www.target.com.au/school?order=cat,brand-0&brand-0=Stubbies&viewType=grid&sortBy=latest&maxItemsPerPage=36"),
    GiftCards(null, "Target Gift Cards", "https://onlineshopping.target.com.au/index.php"),
    Catalogue(null, "Catalogue", "http://target.dynamiccatalogue.com.au/portal/"),
    TargetCountryCatalogue(null, "Target Country Catalogue", "http://target.dynamiccatalogue.com.au/portal/");

    private DepartmentType departmentType;
    private String name;
    private String externalPageURL;

    private ExternalType(final DepartmentType departmentType, final String name, final String externalPageURL)
    {
        this.departmentType = departmentType;
        this.name = name;
        this.externalPageURL = externalPageURL;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return the externalPageURL
     */
    public String getExternalPageURL()
    {
        return externalPageURL;
    }

    public DepartmentType getDepartmentType()
    {
        return departmentType;
    }
}
