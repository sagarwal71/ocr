/**
 * 
 */
package au.com.target.tgttest.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgttest.cucumber.steps.bean.SignupInfo;
import au.com.target.tgttest.selenium.pageobjects.HomePage;
import au.com.target.tgttest.selenium.pageobjects.SignUpPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author cwijesu1
 * 
 */
public class EnewsSignupSteps extends BaseSteps {

    private SignUpPage signupPage;
    private boolean success;


    @When("^enews sign up with:$")
    public void enewsSignUpWith(final List<SignupInfo> signupDataList) throws Throwable {
        final SignupInfo signupData = signupDataList.get(0);
        signupPage = new SignUpPage(getBrowserDriver());
        navigate(signupPage);
        signupPage.populateSignupForm(signupData.getTitle(), signupData.getFirst(), signupData.getEmail());
        signupPage.SignupForEnew();


    }

    @Then("^enews sign up success is '(.*)'$")
    public void enewsSignUpSuccessIsTrue(final boolean success) throws Throwable {
        this.success = success;
        if (success) {
            final HomePage homepage = new HomePage(getBrowserDriver());
            assertThat(homepage.isCurrentPage()).isTrue();
            final List<String> msgs = homepage.getGlobalMsgs();
            assertThat(msgs.size()).as("Number of globle messages").isGreaterThan(0);
            assertThat(msgs.get(0)).as("Expected the first globle message").contains("Done");
        }
    }

    @Then("^enews validation message is '(.*)'$")
    public void enewsValidationMessageIs(final String msg) throws Throwable {
        if (StringUtils.isNotEmpty(msg)) {
            assertThat(signupPage.getSignupErrorText()).contains(msg);
            assertThat(signupPage.getGlobalMsgs()).contains("Oops!");
        }
    }
}
