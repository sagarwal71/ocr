/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects.checkout;

import java.util.List;

import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.BasePage;
import au.com.target.tgttest.selenium.base.BrowserDriver;


/**
 * A wrapper class for the navigation step panel during a checkout process.
 * 
 * @author maesi
 * 
 */
public class CheckoutNavigationHeader extends BasePage {

    private final By checkoutStepsWrapper = By.cssSelector("div.checkout-progress");


    /**
     * @param driver
     */
    public CheckoutNavigationHeader(final BrowserDriver driver) {
        super(driver);
    }


    private List<FluentWebElement> getCheckoutStepsItems() {

        return fwd().div(checkoutStepsWrapper).lis();
    }

    private int getTotalNumberOfSteps() {
        return getCheckoutStepsItems().size();
    }


}
