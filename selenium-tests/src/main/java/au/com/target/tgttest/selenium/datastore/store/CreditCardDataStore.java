/**
 * 
 */
package au.com.target.tgttest.selenium.datastore.store;

import au.com.target.tgttest.selenium.datastore.bean.CreditCard;


/**
 * Provider for CreditCard data
 * 
 */
public interface CreditCardDataStore {

    /**
     * Get the standard credit card data
     * 
     * @return standard CreditCard
     */
    CreditCard getCreditCard();
}
