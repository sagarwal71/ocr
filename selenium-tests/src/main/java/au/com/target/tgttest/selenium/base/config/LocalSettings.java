/**
 *
 */
package au.com.target.tgttest.selenium.base.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * @author sbryan5
 *
 */
public class LocalSettings implements Settings
{
    private static final Logger LOG = LogManager.getLogger(LocalSettings.class);

    private final Properties props;

    public LocalSettings()
    {
        // Load properties from project.properties
        props = new Properties();
        final InputStream in = TestConfiguration.class.getResourceAsStream("/selenium.properties");
        try {
            props.load(in);
        }
        catch (final IOException e)
        {
            throw new RuntimeException("Cannot load application.properties", e);
        }
        finally
        {
            try {
                in.close();
            }
            catch (final Exception e)
            {
                LOG.warn("Exception while closing stream after reading properties");
            }
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.Settings#getTimeout()
     */
    @Override
    public int getTimeout() {
        return Integer.parseInt(props.getProperty("tgttest.selenium.timeout"));
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.Settings#getRunMode()
     */
    @Override
    public String getRunMode() {
        return props.getProperty("tgttest.selenium.runmode");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.Settings#getDefaultHost()
     */
    @Override
    public String getDefaultHost()
    {
        return props.getProperty("tgttest.selenium.host");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.Settings#getDefaultPort()
     */
    @Override
    public String getDefaultPort() {
        return props.getProperty("tgttest.selenium.port");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.Settings#getDefaultSSLPort()
     */
    @Override
    public String getDefaultSSLPort() {
        return props.getProperty("tgttest.selenium.ssl.port");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.Settings#getDefaultBrowser()
     */
    @Override
    public String getDefaultBrowser() {
        return props.getProperty("tgttest.selenium.browser");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.Settings#getSauceUsername()
     */
    @Override
    public String getSauceUsername() {
        return props.getProperty("tgttest.selenium.sauce-username");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.Settings#getSauceAccessKey()
     */
    @Override
    public String getSauceAccessKey() {
        return props.getProperty("tgttest.selenium.sauce-accesskey");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.Settings#getSauceUrl()
     */
    @Override
    public String getSauceUrl() {
        return props.getProperty("tgttest.selenium.sauce-url");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.Settings#getDefaultTenant()
     */
    @Override
    public String getDefaultTenant() {
        return props.getProperty("tgttest.selenium.tenant");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.Settings#getRecordVideo()
     */
    @Override
    public boolean getRecordVideo() {
        return Boolean.parseBoolean(props.getProperty("tgttest.selenium.recordvideo"));
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.Settings#getSeleniumVersion()
     */
    @Override
    public String getSeleniumVersion() {
        return props.getProperty("tgttest.selenium.version");
    }

    /*
     * (non-Javadoc)
     * @see au.com.ecetera.basetest.lib.selenium.config.Settings#getSauceTunnelIdentifier()
     */
    @Override
    public String getSauceTunnelIdentifier() {
        return props.getProperty("tgttest.selenium.sauce-tunnelId");
    }

    @Override
    public String getSetting(final String name) {
        return props.getProperty(name);
    }

    @Override
    public String getAdminHost()
    {
        return props.getProperty("tgttest.selenium.adminhost");
    }

    @Override
    public String getAdminPort() {
        return props.getProperty("tgttest.selenium.adminport");
    }

    @Override
    public String getAdminUsername() {
        return props.getProperty("tgttest.selenium.adminusername");
    }

    @Override
    public String getAdminPassword() {
        return props.getProperty("tgttest.selenium.adminpassword");
    }

    @Override
    public String getWSUsername() {
        return props.getProperty("tgttest.selenium.wsusername");
    }

    @Override
    public String getWSPassword() {
        return props.getProperty("tgttest.selenium.wspassword");
    }

    @Override
    public String getESBUsername() {
        return props.getProperty("tgttest.selenium.esbusername");
    }

    @Override
    public String getESBPassword() {
        return props.getProperty("tgttest.selenium.esbpassword");
    }

    @Override
    public String getChromeDriverPath() {
        return props.getProperty("tgttest.selenium.chromedriver.location");
    }

    @Override
    public boolean getTakeScreenshotOnErrors() {
        return Boolean.parseBoolean(props.getProperty("tgttest.selenium.take-screenshot-on-errors"));
    }

    @Override
    public String getPhantomjsDriverPath() {
        return props.getProperty("tgttest.selenium.phantomjs.location");
    }

    @Override
    public String getCSCockpitUrl() {
        return props.getProperty("tgttest.selenium.cscockpit.url");
    }

    @Override
    public String getCSCockpitLogin() {
        return props.getProperty("tgttest.selenium.cscockpit.login");
    }

    @Override
    public String getCSCockpitPassword() {
        return props.getProperty("tgttest.selenium.cscockpit.password");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.base.config.Settings#getEndecaENEPort()
     */
    @Override
    public String getEndecaENEPort() {
        return props.getProperty("tgttest.endeca.ene.port");
    }

}
