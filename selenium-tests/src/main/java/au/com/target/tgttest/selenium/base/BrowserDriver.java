/**
 * 
 */
package au.com.target.tgttest.selenium.base;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.seleniumhq.selenium.fluent.FluentWebDriver;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.config.BrowserOptions;
import au.com.target.tgttest.selenium.base.config.TestConfiguration;

import com.google.common.base.Function;


/**
 * Our wrapper to isolate Selenium WebDriver API
 * 
 */
public class BrowserDriver {

    /**
     * NOTE: there is no way in selenium webdriver to read the http code
     * (http://code.google.com/p/selenium/issues/detail?id=141) So we'll need to check for text instead
     * 
     * TODO move to properties file
     **/
    private static final String ERROR_404_PAGE_NOT_FOUND = "404 Page Not Found";
    private static final String ERROR_500 = "HTTP Status 500";

    protected static final Logger LOG = LogManager.getLogger(BrowserDriver.class);

    private WebDriver driver;

    private FluentWebDriver fwd;

    private TestConfiguration getTestConfiguration() {
        return TestConfiguration.getInstance();
    }


    /**
     * Navigate to the given absolute URL
     * 
     * @param url
     */
    public void navigate(final String url) {
        driver.get(url);
    }

    /**
     * @deprecated
     */
    @Deprecated
    public WebDriver getDriver() {
        return driver;
    }


    /**
     * Set up a Web Driver with given configuration
     * 
     * @param options
     * @throws Exception
     */
    public void setUpDriver(final BrowserOptions options, final String sessionName) throws Exception {

        // Construct the browser type
        final BrowserType browserType = getBrowser(options.getBrowser());
        browserType.setPlatform(options.getPlatform());
        browserType.setVersion(options.getVersion());
        browserType.setDeviceOrientation(options.getDeviceOrientation());
        browserType.setDeviceType(options.getDeviceType());

        driver = getTestConfiguration().getDriver(browserType, sessionName);

        setDriverStandardTimeout(driver);

        // Maximise the window so we can see all the links on the right, eg sign in
        if (browserType.isWindowHandlingSupported()) {
            tryToMaximiseBrowserWindow();
        }

        fwd = new FluentWebDriver(driver);
    }


    /**
     * Shut down the web driver
     * 
     * @return jobId if it was a remote session
     */
    public String shutdownDriver()
    {
        String jobId = null;
        if (driver != null)
        {
            if (driver instanceof RemoteWebDriver)
            {
                jobId = ((RemoteWebDriver)driver).getSessionId().toString();
            }

            try {
                driver.quit();
            }
            catch (final Exception e) {
                LOG.info("Exception quitting driver", e);
            }
        }
        else
        {
            LOG.warn("Cannot shut down null driver.");
        }

        return jobId;
    }



    /**
     * Attempts to maximise the browser window. If an exception is thrown by the driver, it will be caught and logged at
     * WARN level, so as not to break the tests.
     */
    @SuppressWarnings("unused")
    private void tryToMaximiseBrowserWindow() {
        try
        {
            driver.manage().window().maximize();
        }
        catch (final WebDriverException ex)
        {
            LOG.warn("Unable to maximise the browser window.", ex);
        }
    }


    /**
     * Return the BrowserType for the given browser name. <br/>
     * If given name is empty then use the configured default.
     */
    private BrowserType getBrowser(final String browser)
    {
        String browserName = browser;
        if (StringUtils.isEmpty(browserName))
        {
            browserName = getTestConfiguration().getDefaultBrowserName();
        }
        final BrowserType browserType = BrowserType.valueOf(browserName);
        return browserType;
    }


    /**
     * Set the standard driver timeout
     * 
     * @param driver
     */
    public void setDriverStandardTimeout(final WebDriver driver) {
        setDriverTimeout(getTestConfiguration().getWaitTimeout(), driver);
    }

    private void setDriverTimeout(final int timeoutInMilliseconds, final WebDriver driver)
    {
        // TODO - can FWD do this more nicely?
        driver.manage().timeouts().implicitlyWait(timeoutInMilliseconds, TimeUnit.MILLISECONDS);
    }


    /**
     * Returns the page title.
     * 
     * @return String.
     */
    public String getTitle()
    {
        return driver.getTitle();
    }

    /**
     * Returns the current page URL.
     * 
     * @return String.
     */
    public String getPageURL()
    {
        return driver.getCurrentUrl();
    }

    public String getPageSource()
    {
        return (driver.getPageSource());
    }

    /**
     * checks the page for loading errors.
     * 
     * @return boolean true/false whether the page had loading errors or not.
     */
    public boolean hasPageLoadErrors() {
        // NOTE: there is no way in selenium webdriver to read the http code so we'll need to check for text instead.
        // (http://code.google.com/p/selenium/issues/detail?id=141)
        // TODO: investigate if this can be improved.
        final String source = driver.getPageSource();

        if (source.contains(ERROR_404_PAGE_NOT_FOUND))
        {
            LOG.warn(ERROR_404_PAGE_NOT_FOUND + " message was found on the page.");
            return true;
        }

        if (source.contains(ERROR_500))
        {
            LOG.warn(ERROR_500 + " message was found on the page.");
            return true;
        }

        return false;
    }


    public FluentWebDriver getFluentWebDriver() {
        return fwd;
    }

    public void clearSessionData()
    {
        driver.manage().deleteAllCookies();
    }


    public boolean checkCookieExists(final String name)
    {
        if (driver.manage().getCookieNamed(name) != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void setCookie(final Cookie cookie)
    {
        driver.manage().addCookie(cookie);
    }

    public Cookie getCookie(final String cookiename)
    {
        return driver.manage().getCookieNamed(cookiename);
    }

    public void mouseover(final By by) {

        final Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(by)).build().perform();
    }

    public void mouseover(final FluentWebElement elt) {

        final Actions actions = new Actions(driver);
        actions.moveToElement(elt.getWebElement()).build().perform();
    }

    public void waitForAnimationsToFinish()
    {
        final int maxSeconds = 3;
        final WebDriverWait wait = new WebDriverWait(driver, maxSeconds);
        final Function<WebDriver, Boolean> animationsHaveStopped = new Function<WebDriver, Boolean>() {
            @Override
            public Boolean apply(final WebDriver driver)
            {
                /*
                 * Assume the animations are done if we cannot actually test for them.
                 */
                Boolean results = Boolean.TRUE;

                if (driver instanceof JavascriptExecutor)
                {
                    /*
                     * Execute JavaScript to check the page for animated elements.
                     */
                    final JavascriptExecutor js = (JavascriptExecutor)driver;
                    final Object animated = js.executeScript("return jQuery(':animated').length==0");
                    if (animated != null && animated instanceof Boolean)
                    {
                        results = (Boolean)animated;
                    }
                }
                return results;
            }
        };
        wait.ignoring(WebDriverException.class).until(animationsHaveStopped);
    }


    /**
     * Waits until an animation has started on the page.
     * 
     */
    public void waitForAnimationsToBegin()
    {
        final int maxSeconds = 3;
        final WebDriverWait wait = new WebDriverWait(driver, maxSeconds);
        final Function<WebDriver, Boolean> animationsHaveStarted = new Function<WebDriver, Boolean>() {
            @Override
            public Boolean apply(final WebDriver driver)
            {
                /*
                 * Assume the animations have started if we cannot test for them.
                 */
                Boolean results = Boolean.TRUE;

                if (driver instanceof JavascriptExecutor)
                {
                    /*
                     * Execute JavaScript to check the page for animated elements.
                     */
                    final JavascriptExecutor js = (JavascriptExecutor)driver;
                    final Object animated = js.executeScript("return jQuery(':animated').length!=0");
                    if (animated != null && animated instanceof Boolean)
                    {
                        results = (Boolean)animated;
                    }
                }
                return results;
            }
        };

        try
        {
            wait.ignoring(WebDriverException.class, TimeoutException.class).until(animationsHaveStarted);
        }
        /*
         * CHECKSTYLE:OFF There's an unavoidable race condition here, so we don't want to fail our tests if we don't see
         * animations starting (they may have already started and finished by the time our javascript code is executed).
         */
        catch (final TimeoutException ex)
        {
            LOG.info("Trapping timeout exception");
        }
        //CHECKSTYLE:ON
    }


    /**
     * Wait for js animations to finish
     */
    public void waitForAnimations() {

        waitForAnimationsToBegin();
        waitForAnimationsToFinish();
    }

    /**
     * Take a screenshot if enabled
     * 
     * @param scenario
     */
    public void takeScreenshot(final String scenario) {



        if (driver instanceof TakesScreenshot) {

            final File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

            try {
                FileUtils.copyFile(scrFile,
                        new File("target/screenshots/" + getScreenshotName(scenario)));
            }
            catch (final IOException e) {
                LOG.warn("Can't copy screenshot file");
            }

        }
    }

    private String getScreenshotName(final String scenario) {

        // make into a usable filename by lowercase, remove non alphas and space to _
        final Date date = new Date();
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

        return dateFormat.format(date) + "_" + scenario.toLowerCase().replaceAll("[^a-z ]", "").replaceAll(" ", "_")
                + ".png";
    }

}
