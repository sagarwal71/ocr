/**
 * 
 */
package au.com.target.tgttest.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;
import au.com.target.tgttest.selenium.data.FooterContentType;
import au.com.target.tgttest.selenium.pageobjects.HomePage;
import cucumber.api.java.en.Then;


/**
 * @author cwijesu1
 * 
 */
public class HomepageSteps extends BaseSteps {
    private HomePage homepage;

    @Then("^target logo should be preset$")
    public void targetLogShouldBePreset() throws Throwable {
        homepage = new HomePage(getBrowserDriver());
        assertThat(homepage.isCurrentPage()).isTrue();
        assertThat(homepage.getHeader().hasTargetLogo()).isTrue();
    }

    @Then("^global header should be present$")
    public void globalHeaderShouldBePresent() throws Throwable {
        assertThat(homepage.getHeader().isDisplayed()).isTrue();
    }

    @Then("^global footer should be present$")
    public void globalFooterShouldBePresent() throws Throwable {

        assertThat(homepage.getFooter().isDisplayed()).isTrue();

        for (final FooterContentType footerContentType : FooterContentType.values()) {

            assertThat(homepage.getFooter().existsContentLink(footerContentType))
                    .as("content link: " + footerContentType.getTitleText()).isTrue();
        }

        // Check contact info contains the right info
        assertThat(homepage.getFooter().getContactInfoText()).contains("Online Shopping Support");
        assertThat(homepage.getFooter().getContactInfoText()).contains("1300 753 567");

        // Check legal info is present
        assertThat(homepage.getFooter().getLegalInfoText())
                .contains("Target Australia Pty Ltd ABN 75 004 250 944");


    }

    @Then("^enews signup should be present$")
    public void enewsSignupShouldBePresent() throws Throwable {
        assertThat(homepage.getEnews().isDisplayed()).isTrue();
    }
}
