/**
 * 
 */
package au.com.target.tgttest.selenium.data;

/**
 * @author SBryan5
 * 
 */
public enum SubCategoryType
{
    // Chosen unique sub cat names so the links will be unique on sitemap for example
    KIDS_SOCKS_TIGHTS(DepartmentType.KIDS, "Socks + Tights", "/c/kids/socks-tights/W160607", "W160607"),
    KIDS_PANTS(DepartmentType.KIDS, "Pants", "/c/kids/boys-7-16/Pants/W94318", "W94318"),
    BABY_FEEDING_CHAIRS(DepartmentType.BABY, "Feeding Chairs", "/c/baby/furniture/feeding-chairs/W201939", "W201939"),
    SCHOOL_UNIFORMS_JUMPERS(DepartmentType.SCHOOL, "Jumpers", "/c/school/school-uniforms/jumpers/W97412", "W97412");

    private DepartmentType dept;
    private String name;
    private String relativeURL;
    private String code;

    private SubCategoryType(final DepartmentType dept, final String name, final String relativeURL, final String code)
    {
        this.dept = dept;
        this.name = name;
        this.relativeURL = relativeURL;
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return the relativeURL
     */
    public String getRelativeURL()
    {
        return relativeURL;
    }

    /**
     * @return the dept
     */
    public DepartmentType getDept() {
        return dept;
    }

    public String getCode() {
        return code;
    }
}
