/**
 * 
 */
package au.com.target.tgttest.selenium.datastore.store;

import au.com.target.tgttest.selenium.datastore.bean.PaypalCreditCard;


/**
 * Provider for PaypalCreditCard data
 * 
 */
public interface PaypalCreditCardDataStore {

    /**
     * Get the standard Paypal credit card data
     * 
     * @return standard PaypalCreditCard
     */
    PaypalCreditCard getPaypalCreditCard();
}
