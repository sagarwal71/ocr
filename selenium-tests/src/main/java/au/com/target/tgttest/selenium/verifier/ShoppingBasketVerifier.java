/**
 * 
 */
package au.com.target.tgttest.selenium.verifier;

import static org.fest.assertions.Assertions.assertThat;

import java.math.BigDecimal;
import java.text.MessageFormat;

import au.com.target.tgttest.selenium.datastore.bean.Product;
import au.com.target.tgttest.selenium.pageobjects.ShoppingBasketPage;
import au.com.target.tgttest.selenium.pageobjects.common.ShoppingBasketItem;


/**
 * Verification utils for shopping basket page
 * 
 */
public final class ShoppingBasketVerifier {

    private ShoppingBasketVerifier() {
        // util
    }

    /**
     * Verify the given product exists in the shopping basket
     * 
     * @param page
     * @param product
     * @param expectedQuantity
     */
    public static void verifyShoppingBasketItem(final ShoppingBasketPage page, final Product product,
            final int expectedQuantity)
    {
        final String productName = product.getName();
        final ShoppingBasketItem item = page.findItemByName(productName);
        assertThat(item).overridingErrorMessage("Item not found in basket: " + product).isNotNull();
        verifyShoppingBasketItem(item, product, expectedQuantity);
    }

    private static void verifyShoppingBasketItem(final ShoppingBasketItem entry, final Product product,
            final int expectedQuantity)
    {
        final String template = MessageFormat.format("minicart product {0} - wrong ", product.getName());

        assertThat(entry.getName()).as(template + "name").isEqualTo(product.getName());
        assertThat(entry.getItemCode()).as(template + "code").isEqualTo(product.getCode());
        assertThat(entry.getPricePerItem()).as(template + "price").isEqualTo(product.getPrice());
        assertThat(entry.getColour()).as(template + "colour").isEqualTo(product.getColour());
        assertThat(entry.getSize()).as(template + "size").isEqualTo(product.getSize());
        assertThat(entry.getQuantity()).as(template + "quantity").isEqualTo(expectedQuantity);

        final BigDecimal expectedTotal = product.getPrice().multiply(BigDecimal.valueOf(expectedQuantity));
        assertThat(entry.getTotalPrice()).as(template + "per price item").isEqualTo(expectedTotal);
    }


}
