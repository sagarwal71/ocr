/**
 * 
 */
package au.com.target.tgttest.selenium.data;

/**
 * 
 */
public enum RelatedArticlesItemType {
    SAMPLE_ARTICLE_ITEM_1_PAGE_LINK("Sample Article Item 1 - Page Link", "8796170158110.jpg",
            "Sample Article Item 1 - Page Link", "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices...",
            "/company/careers"),
    SAMPLE_ARTICLE_ITEM_2_YOUTUBE_LINK("Sample Article Item 2 - YouTube Link", "8796170125342.jpg",
            "Sample Article Item 2 - YouTube Link",
            "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices...",
            "http://www.youtube.com/embed/KTVeSktrGPc?rel=0"),
    SAMPLE_ARTICLE_ITEM_3_EXTERNAL_LINK("Sample Article Item 3 - External Link", "8796170190878.jpg",
            "Sample Article Item 3 - External Link",
            "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices...", "https://www.google.com"),
    SAMPLE_ARTICLE_ITEM_8_INORDINATELY_LONG_TITLE_TO_VALIDATE_LAYOUT_INTEGRITY(
            "Sample Article Item 8 - Inordinately long title to validate layout integrity", "8796170190878.jpg",
            "Sample Article Item 8 - Inordinately long title to validate layout integrity",
            "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices...", "https://www.google.com"),
    SAMPLE_ARTICLE_ITEM_9_MARKUP_IN_EXCERPT("Sample Article Item 9 - Markup in excerpt", "8796170223646.jpg",
            "Sample Article Item 9 - Markup in excerpt",
            "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices...", "https://www.google.com"),
    SAMPLE_ARTICLE_ITEM_10_MARKUP_IN_TITLE("Sample Article Item 10 - Markup in title", "8796170256414.jpg",
            "Sample Article Item 10 - Markup in title",
            "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices...", "https://www.google.com");

    private final String title;
    private final String imagePath;
    private final String imageTitle;
    private final String text;
    private final String linkHref;

    private RelatedArticlesItemType(final String title, final String imagePath, final String imageTitle,
            final String text, final String linkHref)
    {
        this.title = title;
        this.imagePath = imagePath;
        this.imageTitle = imageTitle;
        this.text = text;
        this.linkHref = linkHref;
    }

    public String getTitle()
    {
        return this.title;
    }

    /**
     * @return the imagePath
     */
    public String getImagePath() {
        return imagePath;
    }

    /**
     * @return the imageTitle
     */
    public String getImageTitle() {
        return imageTitle;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @return the linkHref
     */
    public String getLinkHref() {
        return linkHref;
    }
}
