/**
 * 
 */
package au.com.target.tgttest.cucumber.steps.kiosk.bean;

/**
 * Represent info on Pos Price Check page
 * 
 */
public class PosPriceCheckInfo {

    private final String code;
    private final String name;
    private final String price;
    private final String wasPrice;
    private final String imageAlt;

    /**
     * @param code
     * @param name
     * @param price
     * @param wasPrice
     * @param imageAlt
     */
    public PosPriceCheckInfo(final String code, final String name, final String price, final String wasPrice,
            final String imageAlt) {
        super();
        this.code = code;
        this.name = name;
        this.price = price;
        this.wasPrice = wasPrice;
        this.imageAlt = imageAlt;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getWasPrice() {
        return wasPrice;
    }

    public String getImageAlt() {
        return imageAlt;
    }

}
