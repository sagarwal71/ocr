/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects.checkout;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.pageobjects.ShoppingBasketPage;
import au.com.target.tgttest.selenium.pageobjects.TemplatePage;


/**
 * A base selenium page for checkout steps.
 * 
 * @author maesi
 * 
 */
public abstract class BaseCheckoutStepPage extends TemplatePage {

    private static final String CSS_SELECTOR_SUMMARY_TABLE_PRODUCT_ITEM_ROWS = "tr.product-entry";

    private final CheckoutNavigationHeader navigationHeader;

    private final By errorMessageContainers = By.cssSelector("div.feedback-msg.error");
    private final By summaryContainer = By.cssSelector("div.summary-table-container");


    // summary items - row web elements...
    private final By rowSummaryTotal = By.cssSelector("tr.summary-row.row-total");
    private final By rowSummarySubTotal = By.cssSelector("tr.summary-row.row-subtotal");
    private final By rowSummaryGST = By.cssSelector("tr.summary-row.row-modifier.gst");
    private final By rowSummaryDeliveryFee = By.cssSelector("tr.summary-row.row-modifier.delivery");
    private final By deliveryFeeCaption = By.cssSelector("th#st-delivery");


    /**
     * constructor
     * 
     * @param driver
     */
    public BaseCheckoutStepPage(final BrowserDriver driver) {
        super(driver);

        navigationHeader = new CheckoutNavigationHeader(driver);
    }

    /**
     * returns the checkout navigation header component.
     * 
     * @return the checkout header as CheckoutNavigationHeader instance.
     */
    public CheckoutNavigationHeader getCheckoutNavigationHeader() {
        return navigationHeader;
    }


    /**
     * returns true or false whether a validation error message is shown or not.
     * 
     * @return boolean true/false whether error validation message is shown or not.
     */
    protected boolean hasValidationErrorMessageDisplayed() {
        return getNumberOfVisibleValidationMessages() > 0;
    }


    /**
     * returns the number of visible error messages.
     * 
     * @return int the number of visible error/validation messages.
     */
    protected int getNumberOfVisibleValidationMessages() {

        final List<FluentWebElement> errorDivs = fwd().divs(errorMessageContainers);

        int result = 0;
        if (!errorDivs.isEmpty()) {
            for (final FluentWebElement errorMessage : errorDivs) {
                if (isDisplayed(errorMessage)) {
                    result++;
                }
            }
        }
        return result;
    }


    /**
     * returns the first validation error message on a certain checkpout page.
     * 
     * @return The validation error message as a String instance.
     */
    public String getFirstValidationErrorMessage() {
        if (hasValidationErrorMessageDisplayed()) {
            final List<FluentWebElement> errorDivs = fwd().divs(errorMessageContainers);

            return getText(errorDivs.get(0));
        }
        return "";
    }


    /**
     * returns a WebElement representing a certain validation error.
     * 
     * @param errorMessage
     *            a error message stub which should be contained in the error message container we are searching for.
     * @return a WebElement which represents a certain validation error.
     */
    public FluentWebElement getValidationErrorContainerWithMessageContaining(final String errorMessage) {
        if (hasValidationErrorMessageDisplayed()) {

            final List<FluentWebElement> errorDivs = fwd().divs(errorMessageContainers);

            for (final FluentWebElement element : errorDivs) {
                if (getValidationErrorMessage(element).contains(errorMessage)) {
                    return element;
                }
            }
        }
        return null;
    }


    /**
     * returns true/false whether at least one validation error message contains a certain text.
     * 
     * @param errorMessageStub
     *            String a partial error message text which should be looked for.
     * @return boolean true/false whether a certain error message is contained in the error message containers.
     */
    public boolean doesAtLeastOneValidationErrorMessageContains(final String errorMessageStub) {
        for (final String errorMessage : getAllValidationErrorMessages()) {
            if (errorMessage.contains(errorMessageStub)) {
                return true;
            }
        }
        return false;
    }


    /**
     * returns a list with all validation error messages.
     * 
     * @return List<String> a list with all error/validation messages.
     */
    private List<String> getAllValidationErrorMessages() {
        final List<String> errorMessages = new ArrayList();
        if (hasValidationErrorMessageDisplayed()) {
            final List<FluentWebElement> errorDivs = fwd().divs(errorMessageContainers);
            for (final FluentWebElement errorMessage : errorDivs) {
                errorMessages.add(getValidationErrorMessage(errorMessage));
            }
        }
        return errorMessages;
    }


    /**
     * returns a certain validation error message.
     * 
     * @param errorMessageContainer
     *            the WebElement of a error message container.
     * @return String the validation error.
     */
    public String getValidationErrorMessage(final FluentWebElement errorMessageContainer) {

        return getText(errorMessageContainer.p(By.cssSelector("p")));
    }


    /**
     * returns a link from the validation error message if existing, otherwise null will be returned.
     * 
     * @param errorMessageContainer
     *            the WebElement of a error message container.
     * @return WebElement the link in the error message.
     */
    private FluentWebElement getLinkInValidationErrorMessage(final FluentWebElement errorMessageContainer) {
        return errorMessageContainer.link(By.tagName("a"));
    }


    /**
     * returns true or false whether there is a link in the validation message or not.
     * 
     * @param errorMessageContainer
     *            the WebElement of a error message container.
     * @return boolean wheter a link is present in the validation message.
     */
    public boolean isLinkInValidationErrorMessage(final FluentWebElement errorMessageContainer) {
        return getLinkInValidationErrorMessage(errorMessageContainer) != null;
    }


    /**
     * clicks/triggers the link in the validation message to return to the shopping basket page.
     * 
     * @param errorMessageContainer
     *            the WebElement of a error message container.
     * @return returns ShoppingBasketPage instance if everything worked out well, otherwise null will be returned.
     */
    private ShoppingBasketPage clickOnBackToBasketPageLinkInValidationMessage(
            final FluentWebElement errorMessageContainer) {
        final FluentWebElement link = getLinkInValidationErrorMessage(errorMessageContainer);
        link.click();
        return new ShoppingBasketPage(getBrowserDriver());
    }


    /**
     * returns a List of validation messages per product.
     * 
     * @param errorMessageContainer
     *            the WebElement of a error message container.
     * @return List<String> with product related messages.
     */
    private List<String> getProductListFromValidationError(final FluentWebElement errorMessageContainer) {
        if (hasValidationErrorMessageDisplayed()) {
            final List<String> result = new ArrayList<String>();
            final List<FluentWebElement> listWithChangedProducts = errorMessageContainer.lis();
            for (final FluentWebElement productValidation : listWithChangedProducts) {
                result.add(getText(productValidation));
            }
            return result;
        }
        return null;
    }


    /**
     * returns true or false whether a product name is mentioned in the per product validation. needed for soh
     * reductions, etc.
     * 
     * @param productName
     *            the product name to check for.
     * @param errorMessageContainer
     *            the WebElement of a error message container.
     * @return true/false whether found or not.
     */
    public boolean isProductnameContainedInProductListValidation(final String productName,
            final FluentWebElement errorMessageContainer) {
        final List<String> productValidations = getProductListFromValidationError(errorMessageContainer);
        if (productValidations != null && !productValidations.isEmpty()) {
            for (final String validation : productValidations) {
                if (validation.contains(productName)) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * returns true or false whether a product name is mentionned in the product summary table.
     * 
     * @param productName
     *            the product name to check for
     * @return true/false whether found in summary or not.
     */
    public boolean isProductnameContainedInSummary(final String productName) {
        final List<FluentWebElement> summaryProductItems = getAllProductsFromSummary();
        if (summaryProductItems != null && !summaryProductItems.isEmpty()) {
            for (final FluentWebElement productRow : summaryProductItems) {
                if (getSummaryProductItemName(productRow).equals(productName)) {
                    return true;
                }
            }
        }
        return false;
    }



    /**
     * returns the sub total amount as a String.
     * 
     * @return the sub total amount as String.
     */
    public String getSummarySubTotalAmount() {
        return getSummaryAmount(rowSummarySubTotal);
    }


    /**
     * returns the summary total amount as a String.
     * 
     * @return the total from the summary as a String.
     */
    public String getSummaryTotalAmount() {
        return getSummaryAmount(rowSummaryTotal);
    }

    /**
     * returns the GST summary amount as a String.
     * 
     * @return the GST amount as String.
     */
    public String getSummaryGSTAmount() {
        return getSummaryAmount(rowSummaryGST);
    }

    /**
     * returns the delivery fee total/summary as a String.
     * 
     * @return the delivery fees as a String.
     */
    public String getSummaryDeliveryFeeAmount() {
        return getSummaryAmount(rowSummaryDeliveryFee);
    }


    /**
     * helper method to extract the total price from a "Table row (tr)" Summary Element.
     * 
     * @param rowElement
     *            a row element.
     * @return returns the amount as a string, empty string when not found.
     */
    private String getSummaryAmount(final By rowElement) {

        fwd().tr(rowElement).isDisplayed().shouldBe(Boolean.TRUE);

        return getText(fwd().tr(rowElement).td(By.cssSelector("td.price")));
    }


    /**
     * returns all product items from the totalization/summary table.
     * 
     * @return List with products from the totalization/summary table or null if there is not summary table is on the
     *         page.
     */
    public List<FluentWebElement> getAllProductsFromSummary() {

        fwd().tr(summaryContainer).isDisplayed().shouldBe(Boolean.TRUE);

        final List<FluentWebElement> summaryAllProductsList = fwd().div(summaryContainer).trs(By
                .cssSelector(CSS_SELECTOR_SUMMARY_TABLE_PRODUCT_ITEM_ROWS));
        return summaryAllProductsList;
    }


    /**
     * helper method to extract the the product name of a summary product item Element/row.
     * 
     * @param summaryProductItemRow
     *            a row element which represents a product item in the summary table.
     * @return returns the name as a string, empty string when not found.
     */
    private String getSummaryProductItemName(final FluentWebElement summaryProductItemRow) {

        summaryProductItemRow.isDisplayed().shouldBe(Boolean.TRUE);

        return getText(summaryProductItemRow.p(By.cssSelector("p.variant-name")));
    }


    /**
     * helper method to extract the the product quantity of a summary product item Element/row.
     * 
     * @param summaryProductItemRow
     *            a row element which represents a product item in the summary table.
     * @return returns the quantity as a string, empty string when not found.
     */
    public String getSummaryProductItemQuantity(final FluentWebElement summaryProductItemRow) {

        summaryProductItemRow.isDisplayed().shouldBe(Boolean.TRUE);

        return getText(summaryProductItemRow.td(By.cssSelector("td.quantity")));
    }


    /**
     * helper method to extract the the product total amount/price of a summary product item Element/row.
     * 
     * @param summaryProductItemRow
     *            a row element which represents a product item in the summary table.
     * @return returns the total amount as a string, empty string when not found.
     */
    private String getSummaryProductItemTotalAmount(final FluentWebElement summaryProductItemRow) {

        summaryProductItemRow.isDisplayed().shouldBe(Boolean.TRUE);

        return getText(summaryProductItemRow.p(By.cssSelector("td.price")));
    }


    /**
     * checks if certain core elements from the summary table are existing and displayed. The boolean return value the
     * represents true or false whether the summary table is shown on a certain page or not.
     * 
     * @return boolean true/false whether the summary table is shown on a certain page or not.
     */
    public boolean isSummaryTableDisplayed() {
        return isDisplayed(fwd().tr(rowSummaryTotal))
                && isDisplayed(fwd().tr(rowSummarySubTotal))
                && isDisplayed(fwd().tr(rowSummaryGST));
    }


    /**
     * returns true/false whether the delivery fee total is shown/displayed on the page or not.
     * 
     * @return boolean true/false whether the delivery fee totalisation summary is whon or not.
     */
    public boolean isDeliveryFeeSummaryDisplayed() {
        return isDisplayed(fwd().tr(rowSummaryDeliveryFee));
    }


    /**
     * returns the caption for the delivery fee summary.
     * 
     * @return String the caption for the delivery fee summary.
     */
    public String getDeliveryFeeSummaryCaption() {

        return getText(fwd().th(deliveryFeeCaption));
    }



}
