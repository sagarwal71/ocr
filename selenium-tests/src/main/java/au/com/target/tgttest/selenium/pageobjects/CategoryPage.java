/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects;

import org.openqa.selenium.By;

import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.data.CategoryType;


/**
 * Category page
 * 
 */
public class CategoryPage extends BaseProductListPage {

    private final CategoryType categoryType;

    private final By leftNavigation = By.cssSelector("div[id^='test_facetNav_facetBrand_links_']");

    @Override
    public String getPageRelativeURL()
    {
        return categoryType.getCode();
    }

    /**
     * @param driver
     */
    public CategoryPage(final BrowserDriver driver, final CategoryType categoryType) {
        super(driver);
        this.categoryType = categoryType;
    }

    @Override
    public boolean isCurrentPage() {
        return isDisplayed(fwd().div(products));
    }


}
