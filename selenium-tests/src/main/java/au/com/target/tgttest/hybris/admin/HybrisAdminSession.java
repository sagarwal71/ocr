/**
 * 
 */
package au.com.target.tgttest.hybris.admin;

import java.io.IOException;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;


/**
 * A session which can be used to interact with Hybris Admin Console during automated testing.
 * 
 * <p>
 * Usage example: <blockquote>
 * 
 * <pre>
 *   String hostAddress = ...
 *   String port = ...
 *   Map<String, String> postData = ...
 *   ResponseHandler voidHandler = HybrisAdminSession.voidHandler();
 *   
 *   HybrisAdminSession session = new HybrisAdminSession(hostAddress, port);
 *   session.login();
 *   session.post("/admin/console/impex/import", postData, voidHandler);
 * </pre>
 * 
 * </blockquote>
 * </p>
 * 
 * @author sswor
 * 
 */
public class HybrisAdminSession {
    /**
     * A handler to get the session ID from the HTTP response.
     */
    private static final ResponseHandler<String> SESSIONID_HANDLER = new ResponseHandler<String>() {

        @Override
        public String handleResponse(final HttpResponse response) throws IOException
        {
            return response.getFirstHeader("Set-Cookie").getValue();
        }
    };

    /**
     * The server's host address.
     */
    private final String hostAddress;

    /**
     * The server's HTTP port.
     */
    private final String port;

    /**
     * The HTTP session ID.
     */
    private String sessionId;

    private final String username;
    private final String password;

    /**
     * Creates a new HybrisAdminSession.
     * 
     * @param hostAddress
     *            the hostAddress
     * @param port
     *            the port
     */
    public HybrisAdminSession(final String hostAddress, final String port, final String username, final String password) {
        this.hostAddress = hostAddress;
        this.port = port;
        this.username = username;
        this.password = password;
    }

    /**
     * Creates a void handler which returns {@code null}.
     * 
     * @param <T>
     *            any type
     * @return a void handler which always returns {@code null}.
     */
    public static <T> ResponseHandler<T> voidHandler() {
        return new ResponseHandler<T>() {
            @Override
            public T handleResponse(final HttpResponse response) throws IOException {
                return null;
            }
        };
    }

    /**
     * Gets the sesison ID.
     * 
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Resolves the full URL for a path.
     * 
     * @param path
     *            the path
     * @return the HTTP url for the path
     */
    protected String urlForPath(final String path) {
        return new StringBuilder("http://").append(hostAddress).append(":").append(port).append(path).toString();
    }

    /**
     * Logs in and obtains a new session ID.
     * 
     * @throws IOException
     *             if an error occurs
     */
    public void login() throws IOException {
        sessionId = Request.Post(urlForPath("/admin/j_spring_security_check"))
                .bodyForm(Form.form().add("j_username", username).add("j_password", password).build())
                .execute().handleResponse(SESSIONID_HANDLER);
    }

    /**
     * Executes a POST request.
     * 
     * @param path
     *            the URL path
     * @param postData
     *            the data to submit in the POST request
     * @param handler
     *            extracts the result data from the response
     * @param <T>
     *            the return type
     * @return the result data
     * @throws IOException
     *             if an error occurs
     */
    public <T> T post(final String path, final Map<String, String> postData, final ResponseHandler<T> handler)
            throws IOException {
        final Form form = Form.form();
        for (final String key : postData.keySet()) {
            form.add(key, postData.get(key));
        }
        return Request.Post(urlForPath(path))
                .addHeader("Cookie", new StringBuilder("JSESSIONID=").append(sessionId).toString())
                .bodyForm(form.build())
                .execute().handleResponse(handler);
    }

}
