/**
 *
 */
package au.com.target.tgttest.selenium.base.config;

/**
 * @author sbryan5
 *
 */
public interface Settings
{
    int getTimeout();

    String getDefaultHost();

    String getDefaultPort();

    String getDefaultTenant();

    String getDefaultSSLPort();

    String getDefaultBrowser();

    String getSauceUsername();

    String getSauceAccessKey();

    String getSauceUrl();

    String getRunMode();

    boolean getRecordVideo();

    boolean getTakeScreenshotOnErrors();

    String getChromeDriverPath();

    String getPhantomjsDriverPath();

    /**
     * Gets the Selenium version to be used at SauceLabs.
     *
     * @return the Selenium version to be used at SauceLabs
     */
    String getSeleniumVersion();

    /**
     * Gets the {@code tunnel-identifier} for the SauceConnect tunnel. This is required when testing against multiple
     * environments in parallel.
     *
     * @return the SauceConnet {@code tunnel-identifier}
     */
    String getSauceTunnelIdentifier();

    /**
     * Gets a setting.
     *
     * @param name
     *            the name
     * @return the value
     */
    String getSetting(String name);

    String getAdminHost();

    String getAdminPort();

    String getAdminUsername();

    String getAdminPassword();

    String getWSUsername();

    String getWSPassword();

    String getESBUsername();

    String getESBPassword();

    String getCSCockpitUrl();

    String getCSCockpitLogin();

    String getCSCockpitPassword();

    String getEndecaENEPort();


}
