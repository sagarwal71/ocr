/**
 * 
 */
package au.com.target.tgttest.selenium.test.smoke;

import static org.fest.assertions.Assertions.assertThat;

import org.testng.annotations.Test;

import au.com.target.tgttest.selenium.base.BaseSeleniumTest;
import au.com.target.tgttest.selenium.data.CategoryType;
import au.com.target.tgttest.selenium.data.DepartmentType;
import au.com.target.tgttest.selenium.pageobjects.CategoryPage;
import au.com.target.tgttest.selenium.pageobjects.DepartmentPage;
import au.com.target.tgttest.selenium.pageobjects.HomePage;
import au.com.target.tgttest.selenium.pageobjects.LoginPage;
import au.com.target.tgttest.selenium.pageobjects.ProductPage;
import au.com.target.tgttest.selenium.pageobjects.SearchResultsPage;
import au.com.target.tgttest.selenium.verifier.DepartmentPageVerifier;


/**
 * cover main page types
 * 
 */
public class BasicPageTest extends BaseSeleniumTest {
    public BasicPageTest() {
        super();
    }

    @Test
    public void testMainPageTypes()
    {
        // Visit the following pages and ensure they appear with no 404 or other error
        // Home, Baby cat landing page, listing page, product details, login
        HomePage homePage = navigateHome();
        assertThat(homePage.isCurrentPage()).as("expect home page").isTrue();

        // Baby dept page has a banner        
        final DepartmentPage babyLanding = homePage.getMegaMenu().clickOnDepartmentLink(DepartmentType.BABY);
        DepartmentPageVerifier.verifyDeptLanding(babyLanding);

        // Category Listing
        homePage = navigateHome();
        final CategoryPage catPage = homePage.getMegaMenu().clickDepartmentSubLink(DepartmentType.BABY,
                CategoryType.BABY_BABYWEAR);
        assertThat(catPage.isCurrentPage()).as("expect category page").isTrue();

        // Product details - just click the first product
        ProductPage productPage = catPage.clickProduct(0);
        assertThat(productPage.isCurrentPage()).as("expect product page").isTrue();

        // Search
        final SearchResultsPage resultsPage = homePage.getHeader().searchFor("shoes");
        assertThat(resultsPage.isCurrentPage()).as("expect results page").isTrue();

        // Product details - just click the first product
        productPage = resultsPage.clickProduct(0);
        assertThat(productPage.isCurrentPage()).as("expect product page").isTrue();

        // login page
        homePage = navigateHome();
        final LoginPage loginPage = (LoginPage)homePage.getHeader().clickMyaccountLink();
        assertThat(loginPage.isDisplayed()).as("expect login page").isTrue();

        // TODO: check login works
    }


}
