/**
 * 
 */
package au.com.target.tgttest.selenium.test.checkout;

import java.util.Collections;
import java.util.List;

import org.testng.annotations.Test;

import au.com.target.tgttest.selenium.datastore.DataStoreLookup;
import au.com.target.tgttest.selenium.datastore.bean.Customer;
import au.com.target.tgttest.selenium.datastore.bean.PaypalCreditCard;
import au.com.target.tgttest.selenium.datastore.bean.Product;
import au.com.target.tgttest.selenium.pageobjects.ShoppingBasketPage;
import au.com.target.tgttest.selenium.pageobjects.checkout.DeliveryMethodPage;
import au.com.target.tgttest.selenium.pageobjects.checkout.PaymentDetailsPage;
import au.com.target.tgttest.selenium.pageobjects.checkout.ThankyouPage;
import au.com.target.tgttest.selenium.process.ShoppingProcess;
import au.com.target.tgttest.selenium.verifier.CheckoutPageComponentVerifier;


/**
 * Adds a product to the shopping basket, starts the checkout as a guest user (e-mail address) , selects click and
 * collect and pays off over the paypal sandbox with a credit card (paypal guest).
 * 
 * 
 * @author maesi
 * 
 */
public class BuyNowProductGuestUserClickAndCollectPaypalCreditcardPayment extends BaseCheckoutTest {

    /* 
     * Load a single colour variant
     */
    @Override
    public List<Product> loadProducts() {
        return Collections.singletonList(getDataStore().getProductDataStore(DataStoreLookup.PRODUCTS_CV_CNC)
                .getNextProduct());
    }


    @Test
    public void testCheckoutGuestUserClickAndCollectPaypalSandboxCreditCardPayment() {

        final ShoppingProcess shop = getShoppingProcess();

        final ShoppingBasketPage basketPage = shop.addProductsIntoShoppingBasket(getProducts());

        final Customer customer = getDataStore().getStandardRegisteredCustomer();
        final PaypalCreditCard card = getDataStore().getStandardPaypalCreditCard();

        final DeliveryMethodPage deliveryMethodPage = shop.checkoutGuestUser(basketPage);

        final PaymentDetailsPage paymentDetailPage = shop.checkoutClickAndCollect(deliveryMethodPage, customer);

        final ThankyouPage thankyouPage = shop.checkoutPayWithGuestPaypal(paymentDetailPage, card, customer);

        CheckoutPageComponentVerifier.verifyTotalisationSummary(thankyouPage, getProducts(), false);

    }

}
