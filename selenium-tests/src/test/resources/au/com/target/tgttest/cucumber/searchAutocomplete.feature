Feature: search box autocomplete.
Autocomplete gives suggestions for terms likely to get hits to aid customer when entering search terms.

Scenario: Entering part of a search term gives some suggestions
Given go to homepage
When enter search stem 'bab'
Then autocomplete list terms start with stem
And autocomplete list contains 'baby'
