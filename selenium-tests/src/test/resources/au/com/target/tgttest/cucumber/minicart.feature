Feature: Minicart popup.

  Scenario: With empty basket then the minicart shows empty.
  Given go to homepage
  When activate minicart
  Then minicart is displayed with title 'Your shopping basket'
  And minicart is empty with text 'Your basket is empty'
  And minicart total is '$0.00'
  And minicart shows checkout link
  And minicart shows shopping link
  

  Scenario: Add a product to basket, flyout shows with the product.
  Given given 1 products
  When add product number 1 to basket
  Then minicart is displayed with title 'Item added to cart'
  And minicart num items is 1
  And minicart total is the product price
  And minicart has the product 
   
   
  Scenario: Add products to basket, flyout shows running total and most recent product.
  Given given 2 products
  When add product number 1 to basket
  And add product number 2 to basket
  Then minicart is displayed with title 'Item added to cart'
  And minicart num items is 2
  And minicart total is the total product price
  And minicart has the product 
   
   
  Scenario: Add products to basket, activated minicart shows full summary.
  Given given 2 products
  When add all products to basket
  And minicart continue shopping
  And activate minicart
  Then minicart is displayed with title 'Your shopping basket'
  And minicart num items is 2
  And minicart total is the total product price
  And minicart has all the products 
   
   